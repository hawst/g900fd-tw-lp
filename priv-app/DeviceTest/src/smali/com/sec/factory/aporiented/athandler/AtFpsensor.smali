.class public Lcom/sec/factory/aporiented/athandler/AtFpsensor;
.super Lcom/sec/factory/aporiented/athandler/AtCommandHandler;
.source "AtFpsensor.java"

# interfaces
.implements Lcom/validity/fingerprint/FingerprintCore$EventListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;,
        Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method4Data;,
        Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method3Data;,
        Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;
    }
.end annotation


# static fields
.field private static final SENSOR_FW_TN:I = 0x0

.field private static final SENSOR_FW_TS:I = 0x1


# instance fields
.field private final STATE_METHOD3_NOTERM_SCAN:I

.field private final STATE_METHOD3_SCAN_FINISH:I

.field private final STATE_METHOD3_TERM_SCAN:I

.field private final STATE_METHOD4_SCAN:I

.field private final STATE_MRM_FAIL_AND_STOP:I

.field private final STATE_MRM_NOTERM_SCAN:I

.field private final STATE_MRM_SCAN_FINISH:I

.field private final STATE_MRM_TERM_SCAN:I

.field private final STATE_NORMALSCAN:I

.field private final STATE_NORMALSCAN_FINISH:I

.field private final STATE_NORMAL_FAIL_AND_STOP:I

.field handler:Landroid/os/Handler;

.field private mContext:Landroid/content/Context;

.field private mCurrentOperation:I

.field private mFingerprint:Lcom/validity/fingerprint/Fingerprint;

.field private mFwVersion:I

.field private mMRMDataString:Ljava/lang/String;

.field private mMRMPixelString:Ljava/lang/String;

.field private mMRMRawData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;

.field private mMethod3PrimaryData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method3Data;

.field private mMethod3PrimaryRawData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;

.field private mMethod3SecondaryData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method3Data;

.field private mMethod3SecondaryRawData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;

.field private mMethod4PrimaryData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method4Data;

.field private mMethod4SecondaryData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method4Data;

.field private mNNoiseSpec:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private mNoiseSpec:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private mPrimaryAvgSpec:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mPrimaryEndPixel:I

.field private mPrimaryRawData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;

.field private mPrimarySectionRawData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;

.field private mPrimarySpec:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mPrimaryStartPixel:I

.field private mPrimaryVerticalRawData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;

.field private mSNRPrimarySpec:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private mSNRSecondarySpec:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private mScanAverageString:Ljava/lang/String;

.field private mSecondaryAvgSpec:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mSecondaryEndPixel:I

.field private mSecondaryRawData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;

.field private mSecondarySectionRawData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;

.field private mSecondarySpec:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mSecondaryStartPixel:I

.field private mSecondaryVerticalRawData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;

.field private mSensorInfo:Lcom/validity/fingerprint/SensorInfo;

.field private mSignalLossCSTVFailPrimarySpec:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mSignalLossCSTVFailSecondarySpec:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mSignalLossCalSNRPrimarySpec:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mSignalLossCalSNRSecondarySpec:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mSignalLossPixenlFailPrimarySpec:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mSignalLossPixenlFailSecondarySpec:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mSignalLossRatePrimarySpec:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mSignalLossRateSecondarySpec:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mSignalSpec:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private mStdevPSString:Ljava/lang/String;

.field private mStdevPrimaryAvgSpec:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private mStdevPrimaryMaxSpec:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private mStdevSSString:Ljava/lang/String;

.field private mStdevSecondaryAvgSpec:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private mStdevSecondaryMaxSpec:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private mStdevSection1AvgSpec:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private mStdevSection1MaxSpec:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private mStdevSection2AvgSpec:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private mStdevSection2MaxSpec:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private mStdevString:Ljava/lang/String;

.field private mVersion:Ljava/lang/String;

.field private m_ReadMethod3PrimaryData:[Ljava/lang/String;

.field private m_ReadMethod3PrimaryPixel:Ljava/lang/String;

.field private m_ReadMethod3SecondaryData:[Ljava/lang/String;

.field private m_ReadMethod3SecondaryPixel:Ljava/lang/String;

.field private m_ReadMethod4PrimaryData:[Ljava/lang/String;

.field private m_ReadMethod4PrimaryPixel:Ljava/lang/String;

.field private m_ReadMethod4SecondaryData:[Ljava/lang/String;

.field private m_ReadMethod4SecondaryPixel:Ljava/lang/String;

.field private token:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 131
    invoke-direct {p0, p1}, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;-><init>(Landroid/content/Context;)V

    .line 33
    new-instance v1, Landroid/os/Handler;

    iget-object v2, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->context:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->handler:Landroid/os/Handler;

    .line 70
    const/16 v1, 0x3e9

    iput v1, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->STATE_NORMALSCAN:I

    .line 71
    const/16 v1, 0x3ea

    iput v1, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->STATE_NORMALSCAN_FINISH:I

    .line 72
    const/16 v1, 0x3eb

    iput v1, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->STATE_MRM_NOTERM_SCAN:I

    .line 73
    const/16 v1, 0x3ec

    iput v1, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->STATE_MRM_TERM_SCAN:I

    .line 74
    const/16 v1, 0x3ed

    iput v1, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->STATE_MRM_SCAN_FINISH:I

    .line 75
    const/16 v1, 0x3ee

    iput v1, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->STATE_METHOD3_NOTERM_SCAN:I

    .line 76
    const/16 v1, 0x3ef

    iput v1, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->STATE_METHOD3_TERM_SCAN:I

    .line 77
    const/16 v1, 0x3f0

    iput v1, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->STATE_METHOD3_SCAN_FINISH:I

    .line 78
    const/16 v1, 0x3f1

    iput v1, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->STATE_METHOD4_SCAN:I

    .line 79
    const/16 v1, 0x7d1

    iput v1, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->STATE_NORMAL_FAIL_AND_STOP:I

    .line 80
    const/16 v1, 0x7d2

    iput v1, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->STATE_MRM_FAIL_AND_STOP:I

    .line 83
    iput-object v3, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mSensorInfo:Lcom/validity/fingerprint/SensorInfo;

    .line 86
    const-string v1, ""

    iput-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mVersion:Ljava/lang/String;

    .line 88
    const-string v1, ""

    iput-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mScanAverageString:Ljava/lang/String;

    .line 89
    const-string v1, ""

    iput-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mStdevString:Ljava/lang/String;

    .line 90
    const-string v1, ""

    iput-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mStdevPSString:Ljava/lang/String;

    .line 91
    const-string v1, ""

    iput-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mStdevSSString:Ljava/lang/String;

    .line 92
    const-string v1, ""

    iput-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mMRMPixelString:Ljava/lang/String;

    .line 93
    const-string v1, ""

    iput-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mMRMDataString:Ljava/lang/String;

    .line 104
    const-string v1, ""

    iput-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->m_ReadMethod3PrimaryPixel:Ljava/lang/String;

    .line 105
    const-string v1, ""

    iput-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->m_ReadMethod3SecondaryPixel:Ljava/lang/String;

    .line 106
    iput-object v3, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->m_ReadMethod3PrimaryData:[Ljava/lang/String;

    .line 107
    iput-object v3, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->m_ReadMethod3SecondaryData:[Ljava/lang/String;

    .line 111
    const-string v1, ""

    iput-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->m_ReadMethod4PrimaryPixel:Ljava/lang/String;

    .line 112
    const-string v1, ""

    iput-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->m_ReadMethod4SecondaryPixel:Ljava/lang/String;

    .line 113
    iput-object v3, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->m_ReadMethod4PrimaryData:[Ljava/lang/String;

    .line 114
    iput-object v3, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->m_ReadMethod4SecondaryData:[Ljava/lang/String;

    .line 132
    const-string v1, "FPSENSOR"

    iput-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->CMD_NAME:Ljava/lang/String;

    .line 133
    const-string v1, "AtFpsensor"

    iput-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->CLASS_NAME:Ljava/lang/String;

    .line 134
    const/4 v1, 0x3

    iput v1, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->NUM_ARGS:I

    .line 136
    iput-object p1, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mContext:Landroid/content/Context;

    .line 137
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->token:Ljava/lang/Object;

    .line 140
    const-string v1, "FINGERPRINT_PRIMARY_PIXEL_START"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Spec;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mPrimaryStartPixel:I

    .line 141
    const-string v1, "FINGERPRINT_PRIMARY_PIXEL_END"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Spec;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mPrimaryEndPixel:I

    .line 142
    const-string v1, "FINGERPRINT_SECONDARY_PIXEL_START"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Spec;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mSecondaryStartPixel:I

    .line 143
    const-string v1, "FINGERPRINT_SECONDARY_PIXEL_END"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Spec;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mSecondaryEndPixel:I

    .line 145
    new-instance v1, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    const-string v2, "FINGERPRINT_PRIMARY_SPEC_MIN"

    invoke-static {v2}, Lcom/sec/factory/support/Support$Spec;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const-string v3, "FINGERPRINT_PRIMARY_SPEC_MAX"

    invoke-static {v3}, Lcom/sec/factory/support/Support$Spec;->getInt(Ljava/lang/String;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mPrimarySpec:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    .line 148
    new-instance v1, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    const-string v2, "FINGERPRINT_SECONDARY_SPEC_MIN"

    invoke-static {v2}, Lcom/sec/factory/support/Support$Spec;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const-string v3, "FINGERPRINT_SECONDARY_SPEC_MAX"

    invoke-static {v3}, Lcom/sec/factory/support/Support$Spec;->getInt(Ljava/lang/String;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mSecondarySpec:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    .line 152
    const-string v1, "FINGERPRINT_PRIMARY_SPEC_AVG"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Spec;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 153
    .local v0, "temp":Ljava/lang/String;
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    array-length v1, v1

    if-ne v1, v7, :cond_0

    .line 154
    new-instance v1, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    const-string v2, ","

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    aget-object v2, v2, v4

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    aget-object v3, v3, v6

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mPrimaryAvgSpec:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    .line 157
    :goto_0
    const-string v1, "FINGERPRINT_SECONDARY_SPEC_AVG"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Spec;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 158
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    array-length v1, v1

    if-ne v1, v7, :cond_1

    .line 159
    new-instance v1, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    const-string v2, ","

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    aget-object v2, v2, v4

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    aget-object v3, v3, v6

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mSecondaryAvgSpec:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    .line 162
    :goto_1
    const-string v1, "FINGERPRINT_STDEV_PRIMARY_SPEC_AVG"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Spec;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 163
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    array-length v1, v1

    if-ne v1, v7, :cond_2

    .line 164
    new-instance v1, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    const-string v2, ","

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    aget-object v2, v2, v4

    invoke-static {v2}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    aget-object v3, v3, v6

    invoke-static {v3}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mStdevPrimaryAvgSpec:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    .line 167
    :goto_2
    const-string v1, "FINGERPRINT_STDEV_PRIMARY_SPEC_MAX"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Spec;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 168
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    array-length v1, v1

    if-ne v1, v7, :cond_3

    .line 169
    new-instance v1, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    const-string v2, ","

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    aget-object v2, v2, v4

    invoke-static {v2}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    aget-object v3, v3, v6

    invoke-static {v3}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mStdevPrimaryMaxSpec:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    .line 172
    :goto_3
    const-string v1, "FINGERPRINT_STDEV_SECONDARY_SPEC_AVG"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Spec;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 173
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    array-length v1, v1

    if-ne v1, v7, :cond_4

    .line 174
    new-instance v1, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    const-string v2, ","

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    aget-object v2, v2, v4

    invoke-static {v2}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    aget-object v3, v3, v6

    invoke-static {v3}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mStdevSecondaryAvgSpec:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    .line 177
    :goto_4
    const-string v1, "FINGERPRINT_STDEV_SECONDARY_SPEC_MAX"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Spec;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 178
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    array-length v1, v1

    if-ne v1, v7, :cond_5

    .line 179
    new-instance v1, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    const-string v2, ","

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    aget-object v2, v2, v4

    invoke-static {v2}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    aget-object v3, v3, v6

    invoke-static {v3}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mStdevSecondaryMaxSpec:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    .line 182
    :goto_5
    const-string v1, "FINGERPRINT_STDEV_SECTION1_SPEC_AVG"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Spec;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 183
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    array-length v1, v1

    if-ne v1, v7, :cond_6

    .line 184
    new-instance v1, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    const-string v2, ","

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    aget-object v2, v2, v4

    invoke-static {v2}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    aget-object v3, v3, v6

    invoke-static {v3}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mStdevSection1AvgSpec:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    .line 187
    :goto_6
    const-string v1, "FINGERPRINT_STDEV_SECTION1_SPEC_MAX"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Spec;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 188
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    array-length v1, v1

    if-ne v1, v7, :cond_7

    .line 189
    new-instance v1, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    const-string v2, ","

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    aget-object v2, v2, v4

    invoke-static {v2}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    aget-object v3, v3, v6

    invoke-static {v3}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mStdevSection1MaxSpec:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    .line 192
    :goto_7
    const-string v1, "FINGERPRINT_STDEV_SECTION2_SPEC_AVG"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Spec;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 193
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    array-length v1, v1

    if-ne v1, v7, :cond_8

    .line 194
    new-instance v1, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    const-string v2, ","

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    aget-object v2, v2, v4

    invoke-static {v2}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    aget-object v3, v3, v6

    invoke-static {v3}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mStdevSection2AvgSpec:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    .line 197
    :goto_8
    const-string v1, "FINGERPRINT_STDEV_SECTION2_SPEC_MAX"

    invoke-static {v1}, Lcom/sec/factory/support/Support$Spec;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 198
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    array-length v1, v1

    if-ne v1, v7, :cond_9

    .line 199
    new-instance v1, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    const-string v2, ","

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    aget-object v2, v2, v4

    invoke-static {v2}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    aget-object v3, v3, v6

    invoke-static {v3}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mStdevSection2MaxSpec:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    .line 202
    :goto_9
    new-instance v1, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    const/high16 v2, 0x42c80000    # 100.0f

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    const/high16 v3, 0x43800000    # 256.0f

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mSignalSpec:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    .line 203
    new-instance v1, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    const v2, 0x3dcccccd    # 0.1f

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    const/high16 v3, 0x40a00000    # 5.0f

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mNoiseSpec:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    .line 204
    new-instance v1, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    const/high16 v2, 0x42aa0000    # 85.0f

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    const/high16 v3, 0x434d0000    # 205.0f

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mSNRPrimarySpec:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    .line 205
    new-instance v1, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    const/high16 v2, 0x42aa0000    # 85.0f

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    const/high16 v3, 0x434d0000    # 205.0f

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mSNRSecondarySpec:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    .line 206
    new-instance v1, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    const v2, 0x3dcccccd    # 0.1f

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    const/high16 v3, 0x40400000    # 3.0f

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mNNoiseSpec:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    .line 209
    new-instance v1, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    const/16 v2, 0x9b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/16 v3, 0x136

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mSignalLossRatePrimarySpec:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    .line 210
    new-instance v1, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mSignalLossPixenlFailPrimarySpec:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    .line 211
    new-instance v1, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mSignalLossCSTVFailPrimarySpec:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    .line 212
    new-instance v1, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    const/16 v2, 0x6e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/16 v3, 0xe1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mSignalLossCalSNRPrimarySpec:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    .line 213
    new-instance v1, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    const/16 v2, 0xa5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/16 v3, 0x140

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mSignalLossRateSecondarySpec:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    .line 214
    new-instance v1, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mSignalLossPixenlFailSecondarySpec:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    .line 215
    new-instance v1, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mSignalLossCSTVFailSecondarySpec:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    .line 216
    new-instance v1, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    const/16 v2, 0x78

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/16 v3, 0x12c

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mSignalLossCalSNRSecondarySpec:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    .line 217
    return-void

    .line 155
    :cond_0
    new-instance v1, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mPrimaryAvgSpec:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    goto/16 :goto_0

    .line 160
    :cond_1
    new-instance v1, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mSecondaryAvgSpec:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    goto/16 :goto_1

    .line 165
    :cond_2
    new-instance v1, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mStdevPrimaryAvgSpec:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    goto/16 :goto_2

    .line 170
    :cond_3
    new-instance v1, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mStdevPrimaryMaxSpec:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    goto/16 :goto_3

    .line 175
    :cond_4
    new-instance v1, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mStdevSecondaryAvgSpec:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    goto/16 :goto_4

    .line 180
    :cond_5
    new-instance v1, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mStdevSecondaryMaxSpec:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    goto/16 :goto_5

    .line 185
    :cond_6
    new-instance v1, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mStdevSection1AvgSpec:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    goto/16 :goto_6

    .line 190
    :cond_7
    new-instance v1, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mStdevSection1MaxSpec:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    goto/16 :goto_7

    .line 195
    :cond_8
    new-instance v1, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mStdevSection2AvgSpec:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    goto/16 :goto_8

    .line 200
    :cond_9
    new-instance v1, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mStdevSection2MaxSpec:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    goto/16 :goto_9
.end method

.method static synthetic access$000(Lcom/sec/factory/aporiented/athandler/AtFpsensor;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/factory/aporiented/athandler/AtFpsensor;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/factory/aporiented/athandler/AtFpsensor;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/factory/aporiented/athandler/AtFpsensor;

    .prologue
    .line 31
    iget v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mCurrentOperation:I

    return v0
.end method

.method static synthetic access$200(Lcom/sec/factory/aporiented/athandler/AtFpsensor;)Lcom/validity/fingerprint/Fingerprint;
    .locals 1
    .param p0, "x0"    # Lcom/sec/factory/aporiented/athandler/AtFpsensor;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mFingerprint:Lcom/validity/fingerprint/Fingerprint;

    return-object v0
.end method

.method private checkMethod3DataSpec()Z
    .locals 2

    .prologue
    .line 1314
    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mMethod3PrimaryData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method3Data;

    invoke-virtual {v0}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method3Data;->getSignal()F

    move-result v1

    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mSignalSpec:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;->lower:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    cmpg-float v0, v1, v0

    if-ltz v0, :cond_0

    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mMethod3PrimaryData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method3Data;

    invoke-virtual {v0}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method3Data;->getSignal()F

    move-result v1

    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mSignalSpec:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;->upper:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    cmpl-float v0, v1, v0

    if-gtz v0, :cond_0

    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mMethod3SecondaryData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method3Data;

    invoke-virtual {v0}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method3Data;->getSignal()F

    move-result v1

    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mSignalSpec:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;->lower:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    cmpg-float v0, v1, v0

    if-ltz v0, :cond_0

    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mMethod3SecondaryData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method3Data;

    invoke-virtual {v0}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method3Data;->getSignal()F

    move-result v1

    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mSignalSpec:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;->upper:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    cmpl-float v0, v1, v0

    if-gtz v0, :cond_0

    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mMethod3PrimaryData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method3Data;

    invoke-virtual {v0}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method3Data;->getNoise()F

    move-result v1

    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mNoiseSpec:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;->lower:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    cmpg-float v0, v1, v0

    if-ltz v0, :cond_0

    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mMethod3PrimaryData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method3Data;

    invoke-virtual {v0}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method3Data;->getNoise()F

    move-result v1

    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mNoiseSpec:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;->upper:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    cmpl-float v0, v1, v0

    if-gtz v0, :cond_0

    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mMethod3SecondaryData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method3Data;

    invoke-virtual {v0}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method3Data;->getNoise()F

    move-result v1

    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mNoiseSpec:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;->lower:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    cmpg-float v0, v1, v0

    if-ltz v0, :cond_0

    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mMethod3SecondaryData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method3Data;

    invoke-virtual {v0}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method3Data;->getNoise()F

    move-result v1

    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mNoiseSpec:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;->upper:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    cmpl-float v0, v1, v0

    if-gtz v0, :cond_0

    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mMethod3PrimaryData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method3Data;

    invoke-virtual {v0}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method3Data;->getSNR()F

    move-result v1

    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mSNRPrimarySpec:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;->lower:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    cmpg-float v0, v1, v0

    if-ltz v0, :cond_0

    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mMethod3PrimaryData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method3Data;

    invoke-virtual {v0}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method3Data;->getSNR()F

    move-result v1

    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mSNRPrimarySpec:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;->upper:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    cmpl-float v0, v1, v0

    if-gtz v0, :cond_0

    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mMethod3SecondaryData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method3Data;

    invoke-virtual {v0}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method3Data;->getSNR()F

    move-result v1

    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mSNRSecondarySpec:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;->lower:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    cmpg-float v0, v1, v0

    if-ltz v0, :cond_0

    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mMethod3SecondaryData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method3Data;

    invoke-virtual {v0}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method3Data;->getSNR()F

    move-result v1

    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mSNRSecondarySpec:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;->upper:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    cmpl-float v0, v1, v0

    if-gtz v0, :cond_0

    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mMethod3PrimaryData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method3Data;

    invoke-virtual {v0}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method3Data;->getNNoise()F

    move-result v1

    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mNNoiseSpec:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;->lower:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    cmpg-float v0, v1, v0

    if-ltz v0, :cond_0

    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mMethod3PrimaryData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method3Data;

    invoke-virtual {v0}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method3Data;->getNNoise()F

    move-result v1

    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mNNoiseSpec:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;->upper:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    cmpl-float v0, v1, v0

    if-gtz v0, :cond_0

    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mMethod3SecondaryData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method3Data;

    invoke-virtual {v0}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method3Data;->getNNoise()F

    move-result v1

    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mNNoiseSpec:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;->lower:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    cmpg-float v0, v1, v0

    if-ltz v0, :cond_0

    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mMethod3SecondaryData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method3Data;

    invoke-virtual {v0}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method3Data;->getNNoise()F

    move-result v1

    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mNNoiseSpec:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;->upper:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    cmpl-float v0, v1, v0

    if-lez v0, :cond_1

    .line 1322
    :cond_0
    const/4 v0, 0x0

    .line 1323
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private checkMethod4DataSpec()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1352
    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mMethod4PrimaryData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method4Data;

    invoke-virtual {v0}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method4Data;->getRate()I

    move-result v2

    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mSignalLossRatePrimarySpec:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;->lower:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-lt v2, v0, :cond_0

    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mMethod4PrimaryData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method4Data;

    invoke-virtual {v0}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method4Data;->getRate()I

    move-result v2

    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mSignalLossRatePrimarySpec:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;->upper:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-gt v2, v0, :cond_0

    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mMethod4PrimaryData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method4Data;

    invoke-virtual {v0}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method4Data;->getPixelFail()I

    move-result v2

    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mSignalLossPixenlFailPrimarySpec:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;->lower:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-lt v2, v0, :cond_0

    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mMethod4PrimaryData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method4Data;

    invoke-virtual {v0}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method4Data;->getPixelFail()I

    move-result v2

    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mSignalLossPixenlFailPrimarySpec:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;->upper:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-gt v2, v0, :cond_0

    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mMethod4PrimaryData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method4Data;

    invoke-virtual {v0}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method4Data;->getCSTVFail()I

    move-result v2

    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mSignalLossCSTVFailPrimarySpec:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;->lower:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-lt v2, v0, :cond_0

    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mMethod4PrimaryData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method4Data;

    invoke-virtual {v0}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method4Data;->getCSTVFail()I

    move-result v2

    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mSignalLossCSTVFailPrimarySpec:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;->upper:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-gt v2, v0, :cond_0

    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mMethod4PrimaryData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method4Data;

    invoke-virtual {v0}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method4Data;->getCalSNR()I

    move-result v2

    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mSignalLossCalSNRPrimarySpec:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;->lower:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-lt v2, v0, :cond_0

    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mMethod4PrimaryData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method4Data;

    invoke-virtual {v0}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method4Data;->getCalSNR()I

    move-result v2

    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mSignalLossCalSNRPrimarySpec:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;->upper:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-le v2, v0, :cond_1

    :cond_0
    move v0, v1

    .line 1364
    :goto_0
    return v0

    .line 1358
    :cond_1
    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mMethod4SecondaryData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method4Data;

    invoke-virtual {v0}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method4Data;->getRate()I

    move-result v2

    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mSignalLossRateSecondarySpec:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;->lower:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-lt v2, v0, :cond_2

    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mMethod4SecondaryData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method4Data;

    invoke-virtual {v0}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method4Data;->getRate()I

    move-result v2

    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mSignalLossRateSecondarySpec:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;->upper:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-gt v2, v0, :cond_2

    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mMethod4SecondaryData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method4Data;

    invoke-virtual {v0}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method4Data;->getPixelFail()I

    move-result v2

    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mSignalLossPixenlFailSecondarySpec:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;->lower:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-lt v2, v0, :cond_2

    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mMethod4SecondaryData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method4Data;

    invoke-virtual {v0}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method4Data;->getPixelFail()I

    move-result v2

    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mSignalLossPixenlFailSecondarySpec:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;->upper:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-gt v2, v0, :cond_2

    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mMethod4SecondaryData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method4Data;

    invoke-virtual {v0}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method4Data;->getCSTVFail()I

    move-result v2

    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mSignalLossCSTVFailSecondarySpec:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;->lower:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-lt v2, v0, :cond_2

    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mMethod4SecondaryData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method4Data;

    invoke-virtual {v0}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method4Data;->getCSTVFail()I

    move-result v2

    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mSignalLossCSTVFailSecondarySpec:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;->upper:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-gt v2, v0, :cond_2

    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mMethod4SecondaryData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method4Data;

    invoke-virtual {v0}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method4Data;->getCalSNR()I

    move-result v2

    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mSignalLossCalSNRSecondarySpec:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;->lower:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-lt v2, v0, :cond_2

    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mMethod4SecondaryData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method4Data;

    invoke-virtual {v0}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method4Data;->getCalSNR()I

    move-result v2

    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mSignalLossCalSNRSecondarySpec:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;->upper:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-le v2, v0, :cond_3

    :cond_2
    move v0, v1

    .line 1362
    goto/16 :goto_0

    .line 1364
    :cond_3
    const/4 v0, 0x1

    goto/16 :goto_0
.end method

.method private checkNormalScanSpec()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1286
    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mPrimaryRawData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;

    invoke-virtual {v0}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;->getMin()F

    move-result v2

    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mPrimarySpec:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;->lower:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-float v0, v0

    cmpg-float v0, v2, v0

    if-ltz v0, :cond_0

    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mPrimaryRawData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;

    invoke-virtual {v0}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;->getMax()F

    move-result v2

    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mPrimarySpec:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;->upper:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-float v0, v0

    cmpl-float v0, v2, v0

    if-gtz v0, :cond_0

    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mPrimaryRawData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;

    invoke-virtual {v0}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;->getAvg()F

    move-result v2

    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mPrimaryAvgSpec:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;->lower:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-float v0, v0

    cmpg-float v0, v2, v0

    if-ltz v0, :cond_0

    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mPrimaryRawData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;

    invoke-virtual {v0}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;->getAvg()F

    move-result v2

    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mPrimaryAvgSpec:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;->upper:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-float v0, v0

    cmpl-float v0, v2, v0

    if-gtz v0, :cond_0

    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mPrimarySectionRawData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;

    invoke-virtual {v0}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;->getMax()F

    move-result v2

    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mStdevSection1MaxSpec:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;->lower:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    cmpg-float v0, v2, v0

    if-ltz v0, :cond_0

    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mPrimarySectionRawData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;

    invoke-virtual {v0}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;->getMax()F

    move-result v2

    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mStdevSection1MaxSpec:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;->upper:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    cmpl-float v0, v2, v0

    if-gtz v0, :cond_0

    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mPrimarySectionRawData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;

    invoke-virtual {v0}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;->getAvg()F

    move-result v2

    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mStdevSection1AvgSpec:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;->lower:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    cmpg-float v0, v2, v0

    if-ltz v0, :cond_0

    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mPrimarySectionRawData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;

    invoke-virtual {v0}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;->getAvg()F

    move-result v2

    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mStdevSection1AvgSpec:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;->upper:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    cmpl-float v0, v2, v0

    if-gtz v0, :cond_0

    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mPrimaryVerticalRawData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;

    invoke-virtual {v0}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;->getMax()F

    move-result v2

    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mStdevPrimaryMaxSpec:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;->lower:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    cmpg-float v0, v2, v0

    if-ltz v0, :cond_0

    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mPrimaryVerticalRawData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;

    invoke-virtual {v0}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;->getMax()F

    move-result v2

    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mStdevPrimaryMaxSpec:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;->upper:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    cmpl-float v0, v2, v0

    if-gtz v0, :cond_0

    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mPrimaryVerticalRawData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;

    invoke-virtual {v0}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;->getAvg()F

    move-result v2

    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mStdevPrimaryAvgSpec:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;->lower:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    cmpg-float v0, v2, v0

    if-ltz v0, :cond_0

    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mPrimaryVerticalRawData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;

    invoke-virtual {v0}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;->getAvg()F

    move-result v2

    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mStdevPrimaryAvgSpec:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;->upper:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    cmpl-float v0, v2, v0

    if-lez v0, :cond_1

    :cond_0
    move v0, v1

    .line 1302
    :goto_0
    return v0

    .line 1294
    :cond_1
    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mSecondaryRawData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;

    invoke-virtual {v0}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;->getMin()F

    move-result v2

    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mSecondarySpec:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;->lower:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-float v0, v0

    cmpg-float v0, v2, v0

    if-ltz v0, :cond_2

    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mSecondaryRawData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;

    invoke-virtual {v0}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;->getMax()F

    move-result v2

    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mSecondarySpec:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;->upper:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-float v0, v0

    cmpl-float v0, v2, v0

    if-gtz v0, :cond_2

    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mSecondaryRawData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;

    invoke-virtual {v0}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;->getAvg()F

    move-result v2

    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mSecondaryAvgSpec:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;->lower:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-float v0, v0

    cmpg-float v0, v2, v0

    if-ltz v0, :cond_2

    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mSecondaryRawData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;

    invoke-virtual {v0}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;->getAvg()F

    move-result v2

    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mSecondaryAvgSpec:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;->upper:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-float v0, v0

    cmpl-float v0, v2, v0

    if-gtz v0, :cond_2

    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mSecondarySectionRawData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;

    invoke-virtual {v0}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;->getMax()F

    move-result v2

    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mStdevSection2MaxSpec:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;->lower:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    cmpg-float v0, v2, v0

    if-ltz v0, :cond_2

    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mSecondarySectionRawData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;

    invoke-virtual {v0}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;->getMax()F

    move-result v2

    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mStdevSection2MaxSpec:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;->upper:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    cmpl-float v0, v2, v0

    if-gtz v0, :cond_2

    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mSecondarySectionRawData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;

    invoke-virtual {v0}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;->getAvg()F

    move-result v2

    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mStdevSection2AvgSpec:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;->lower:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    cmpg-float v0, v2, v0

    if-ltz v0, :cond_2

    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mSecondarySectionRawData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;

    invoke-virtual {v0}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;->getAvg()F

    move-result v2

    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mStdevSection2AvgSpec:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;->upper:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    cmpl-float v0, v2, v0

    if-gtz v0, :cond_2

    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mSecondaryVerticalRawData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;

    invoke-virtual {v0}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;->getMax()F

    move-result v2

    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mStdevSecondaryMaxSpec:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;->lower:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    cmpg-float v0, v2, v0

    if-ltz v0, :cond_2

    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mSecondaryVerticalRawData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;

    invoke-virtual {v0}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;->getMax()F

    move-result v2

    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mStdevSecondaryMaxSpec:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;->upper:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    cmpl-float v0, v2, v0

    if-gtz v0, :cond_2

    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mSecondaryVerticalRawData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;

    invoke-virtual {v0}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;->getAvg()F

    move-result v2

    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mStdevSecondaryAvgSpec:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;->lower:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    cmpg-float v0, v2, v0

    if-ltz v0, :cond_2

    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mSecondaryVerticalRawData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;

    invoke-virtual {v0}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;->getAvg()F

    move-result v2

    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mStdevSecondaryAvgSpec:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Spec;->upper:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    cmpl-float v0, v2, v0

    if-lez v0, :cond_3

    :cond_2
    move v0, v1

    .line 1300
    goto/16 :goto_0

    .line 1302
    :cond_3
    const/4 v0, 0x1

    goto/16 :goto_0
.end method

.method private createPopup()V
    .locals 2

    .prologue
    .line 609
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/sec/factory/aporiented/athandler/AtFpsensor$1;

    invoke-direct {v1, p0}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$1;-><init>(Lcom/sec/factory/aporiented/athandler/AtFpsensor;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 654
    return-void
.end method

.method private getSensorVersion()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 576
    iput-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mSensorInfo:Lcom/validity/fingerprint/SensorInfo;

    .line 577
    const-string v2, ""

    iput-object v2, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mVersion:Ljava/lang/String;

    .line 578
    iget-object v2, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mFingerprint:Lcom/validity/fingerprint/Fingerprint;

    const/16 v3, 0xc

    invoke-virtual {v2, v3, v1}, Lcom/validity/fingerprint/Fingerprint;->request(ILjava/lang/Object;)I

    .line 580
    iget-object v2, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->token:Ljava/lang/Object;

    monitor-enter v2

    .line 582
    :try_start_0
    iget-object v3, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->token:Ljava/lang/Object;

    const-wide/16 v4, 0xbb8

    invoke-virtual {v3, v4, v5}, Ljava/lang/Object;->wait(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 586
    :goto_0
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 587
    iget-object v2, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mSensorInfo:Lcom/validity/fingerprint/SensorInfo;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mVersion:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 590
    :cond_0
    :goto_1
    return-object v1

    .line 583
    :catch_0
    move-exception v0

    .line 584
    .local v0, "e":Ljava/lang/InterruptedException;
    :try_start_2
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0

    .line 586
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    .line 590
    :cond_1
    iget-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mVersion:Ljava/lang/String;

    goto :goto_1
.end method

.method private parseMRMData()V
    .locals 8

    .prologue
    .line 1215
    iget-object v5, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mMRMDataString:Ljava/lang/String;

    const-string v6, ","

    invoke-virtual {v5, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 1216
    .local v1, "mrmDataArray":[Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mMRMPixelString:Ljava/lang/String;

    const-string v6, ","

    invoke-virtual {v5, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 1218
    .local v2, "mrmPixelArray":[Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 1221
    .local v3, "primaryBuilder":Ljava/lang/StringBuilder;
    const/4 v5, 0x1

    aget-object v5, v2, v5

    const-string v6, "\\s"

    const-string v7, ""

    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    .line 1222
    .local v4, "startPixel":I
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    array-length v5, v1

    if-ge v0, v5, :cond_0

    .line 1223
    aget-object v5, v1, v0

    const-string v6, "\\s"

    const-string v7, ""

    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ","

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1222
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1225
    :cond_0
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    .line 1226
    return-void
.end method

.method private parseMethod3Data()V
    .locals 15

    .prologue
    const/4 v14, 0x3

    const/4 v13, 0x2

    const/4 v12, 0x1

    const/4 v11, 0x0

    const/high16 v3, 0x41200000    # 10.0f

    .line 1230
    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->m_ReadMethod3PrimaryPixel:Ljava/lang/String;

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    .line 1231
    .local v7, "pixelArray1":[Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->m_ReadMethod3SecondaryPixel:Ljava/lang/String;

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v8

    .line 1233
    .local v8, "pixelArray2":[Ljava/lang/String;
    iget v6, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mPrimaryStartPixel:I

    .local v6, "i":I
    :goto_0
    iget v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mPrimaryEndPixel:I

    if-gt v6, v0, :cond_0

    .line 1235
    add-int/lit8 v0, v6, 0x1

    aget-object v0, v7, v0

    const-string v1, "\\s"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    int-to-float v0, v0

    div-float v9, v0, v3

    .line 1236
    .local v9, "temp":F
    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mMethod3PrimaryRawData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;

    invoke-virtual {v0, v9}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;->addData(F)V

    .line 1233
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 1238
    .end local v9    # "temp":F
    :cond_0
    iget v6, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mSecondaryStartPixel:I

    :goto_1
    iget v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mSecondaryEndPixel:I

    if-gt v6, v0, :cond_1

    .line 1240
    add-int/lit8 v0, v6, 0x1

    aget-object v0, v8, v0

    const-string v1, "\\s"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    int-to-float v0, v0

    div-float v9, v0, v3

    .line 1241
    .restart local v9    # "temp":F
    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mMethod3SecondaryRawData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;

    invoke-virtual {v0, v9}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;->addData(F)V

    .line 1238
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 1244
    .end local v9    # "temp":F
    :cond_1
    new-instance v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method3Data;

    iget-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->m_ReadMethod3PrimaryData:[Ljava/lang/String;

    aget-object v1, v1, v11

    iget-object v2, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->m_ReadMethod3PrimaryData:[Ljava/lang/String;

    aget-object v2, v2, v12

    iget-object v3, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->m_ReadMethod3PrimaryData:[Ljava/lang/String;

    aget-object v3, v3, v13

    iget-object v4, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->m_ReadMethod3PrimaryData:[Ljava/lang/String;

    aget-object v4, v4, v14

    iget-object v5, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->m_ReadMethod3PrimaryData:[Ljava/lang/String;

    const/4 v10, 0x4

    aget-object v5, v5, v10

    invoke-direct/range {v0 .. v5}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method3Data;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mMethod3PrimaryData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method3Data;

    .line 1250
    new-instance v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method3Data;

    iget-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->m_ReadMethod3SecondaryData:[Ljava/lang/String;

    aget-object v1, v1, v11

    iget-object v2, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->m_ReadMethod3SecondaryData:[Ljava/lang/String;

    aget-object v2, v2, v12

    iget-object v3, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->m_ReadMethod3SecondaryData:[Ljava/lang/String;

    aget-object v3, v3, v13

    iget-object v4, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->m_ReadMethod3SecondaryData:[Ljava/lang/String;

    aget-object v4, v4, v14

    iget-object v5, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->m_ReadMethod3SecondaryData:[Ljava/lang/String;

    const/4 v10, 0x4

    aget-object v5, v5, v10

    invoke-direct/range {v0 .. v5}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method3Data;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mMethod3SecondaryData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method3Data;

    .line 1256
    return-void
.end method

.method private parseMethod4Data()V
    .locals 9

    .prologue
    const/16 v8, 0xb

    const/16 v7, 0xa

    const/16 v6, 0x9

    const/16 v5, 0x8

    .line 1273
    new-instance v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method4Data;

    iget-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->m_ReadMethod4PrimaryData:[Ljava/lang/String;

    aget-object v1, v1, v5

    iget-object v2, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->m_ReadMethod4PrimaryData:[Ljava/lang/String;

    aget-object v2, v2, v6

    iget-object v3, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->m_ReadMethod4PrimaryData:[Ljava/lang/String;

    aget-object v3, v3, v7

    iget-object v4, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->m_ReadMethod4PrimaryData:[Ljava/lang/String;

    aget-object v4, v4, v8

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method4Data;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mMethod4PrimaryData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method4Data;

    .line 1278
    new-instance v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method4Data;

    iget-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->m_ReadMethod4SecondaryData:[Ljava/lang/String;

    aget-object v1, v1, v5

    iget-object v2, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->m_ReadMethod4SecondaryData:[Ljava/lang/String;

    aget-object v2, v2, v6

    iget-object v3, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->m_ReadMethod4SecondaryData:[Ljava/lang/String;

    aget-object v3, v3, v7

    iget-object v4, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->m_ReadMethod4SecondaryData:[Ljava/lang/String;

    aget-object v4, v4, v8

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method4Data;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mMethod4SecondaryData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method4Data;

    .line 1283
    return-void
.end method

.method private parseNormalScan()V
    .locals 10

    .prologue
    const/high16 v9, 0x41200000    # 10.0f

    .line 1170
    iget-object v6, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mScanAverageString:Ljava/lang/String;

    const-string v7, ","

    invoke-virtual {v6, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 1171
    .local v1, "normalDataArray":[Ljava/lang/String;
    iget-object v6, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mStdevString:Ljava/lang/String;

    const-string v7, ","

    invoke-virtual {v6, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 1174
    .local v2, "normalStdevArray":[Ljava/lang/String;
    iget v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mPrimaryStartPixel:I

    .local v0, "i":I
    :goto_0
    iget v6, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mPrimaryEndPixel:I

    if-gt v0, v6, :cond_0

    .line 1176
    add-int/lit8 v6, v0, 0x1

    aget-object v6, v1, v6

    const-string v7, "\\s"

    const-string v8, ""

    invoke-virtual {v6, v7, v8}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    int-to-float v6, v6

    div-float v5, v6, v9

    .line 1177
    .local v5, "temp":F
    iget-object v6, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mPrimaryRawData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;

    invoke-virtual {v6, v5}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;->addData(F)V

    .line 1174
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1180
    .end local v5    # "temp":F
    :cond_0
    iget v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mSecondaryStartPixel:I

    :goto_1
    iget v6, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mSecondaryEndPixel:I

    if-gt v0, v6, :cond_1

    .line 1182
    add-int/lit8 v6, v0, 0x1

    aget-object v6, v1, v6

    const-string v7, "\\s"

    const-string v8, ""

    invoke-virtual {v6, v7, v8}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    int-to-float v6, v6

    div-float v5, v6, v9

    .line 1183
    .restart local v5    # "temp":F
    iget-object v6, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mSecondaryRawData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;

    invoke-virtual {v6, v5}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;->addData(F)V

    .line 1180
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1186
    .end local v5    # "temp":F
    :cond_1
    iget v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mPrimaryStartPixel:I

    :goto_2
    iget v6, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mPrimaryEndPixel:I

    if-gt v0, v6, :cond_2

    .line 1188
    add-int/lit8 v6, v0, 0x1

    aget-object v6, v2, v6

    const-string v7, "\\s"

    const-string v8, ""

    invoke-virtual {v6, v7, v8}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    int-to-float v6, v6

    div-float v5, v6, v9

    .line 1189
    .restart local v5    # "temp":F
    iget-object v6, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mPrimaryVerticalRawData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;

    invoke-virtual {v6, v5}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;->addData(F)V

    .line 1186
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1192
    .end local v5    # "temp":F
    :cond_2
    iget v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mSecondaryStartPixel:I

    :goto_3
    iget v6, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mSecondaryEndPixel:I

    if-gt v0, v6, :cond_3

    .line 1194
    add-int/lit8 v6, v0, 0x1

    aget-object v6, v2, v6

    const-string v7, "\\s"

    const-string v8, ""

    invoke-virtual {v6, v7, v8}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    int-to-float v6, v6

    div-float v5, v6, v9

    .line 1195
    .restart local v5    # "temp":F
    iget-object v6, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mSecondaryVerticalRawData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;

    invoke-virtual {v6, v5}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;->addData(F)V

    .line 1192
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 1199
    .end local v5    # "temp":F
    :cond_3
    iget-object v6, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mStdevPSString:Ljava/lang/String;

    const-string v7, ","

    invoke-virtual {v6, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 1200
    .local v3, "primarySectionArray":[Ljava/lang/String;
    iget-object v6, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mStdevSSString:Ljava/lang/String;

    const-string v7, ","

    invoke-virtual {v6, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 1202
    .local v4, "secondarySectionArray":[Ljava/lang/String;
    const/4 v0, 0x1

    :goto_4
    array-length v6, v3

    if-ge v0, v6, :cond_4

    .line 1203
    aget-object v6, v3, v0

    const-string v7, "\\s"

    const-string v8, ""

    invoke-virtual {v6, v7, v8}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    int-to-float v6, v6

    div-float v5, v6, v9

    .line 1204
    .restart local v5    # "temp":F
    iget-object v6, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mPrimarySectionRawData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;

    invoke-virtual {v6, v5}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;->addData(F)V

    .line 1202
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 1207
    .end local v5    # "temp":F
    :cond_4
    const/4 v0, 0x1

    :goto_5
    array-length v6, v4

    if-ge v0, v6, :cond_5

    .line 1208
    aget-object v6, v4, v0

    const-string v7, "\\s"

    const-string v8, ""

    invoke-virtual {v6, v7, v8}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    int-to-float v6, v6

    div-float v5, v6, v9

    .line 1209
    .restart local v5    # "temp":F
    iget-object v6, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mSecondarySectionRawData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;

    invoke-virtual {v6, v5}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;->addData(F)V

    .line 1207
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 1211
    .end local v5    # "temp":F
    :cond_5
    return-void
.end method

.method private readMRMLogFile()V
    .locals 10

    .prologue
    .line 859
    const-string v7, "/data/validity/ValiditySnsrTest.log"

    .line 860
    .local v7, "path":Ljava/lang/String;
    const-string v0, "Start of Selected Setting Pixel Data "

    .line 862
    .local v0, "Header":Ljava/lang/String;
    new-instance v6, Ljava/io/File;

    invoke-direct {v6, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 864
    .local v6, "lfile":Ljava/io/File;
    const-string v9, ""

    iput-object v9, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mMRMPixelString:Ljava/lang/String;

    .line 865
    const-string v9, ""

    iput-object v9, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mMRMDataString:Ljava/lang/String;

    .line 867
    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v9

    if-eqz v9, :cond_2

    .line 869
    const/4 v6, 0x0

    .line 870
    const/4 v4, 0x0

    .line 871
    .local v4, "fis":Ljava/io/FileInputStream;
    const/4 v1, 0x0

    .line 874
    .local v1, "bufferReader":Ljava/io/BufferedReader;
    :try_start_0
    new-instance v5, Ljava/io/FileInputStream;

    invoke-direct {v5, v7}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 875
    .end local v4    # "fis":Ljava/io/FileInputStream;
    .local v5, "fis":Ljava/io/FileInputStream;
    :try_start_1
    new-instance v2, Ljava/io/BufferedReader;

    new-instance v9, Ljava/io/InputStreamReader;

    invoke-direct {v9, v5}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v2, v9}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 876
    .end local v1    # "bufferReader":Ljava/io/BufferedReader;
    .local v2, "bufferReader":Ljava/io/BufferedReader;
    :try_start_2
    const-string v8, ""

    .line 878
    .local v8, "readStr":Ljava/lang/String;
    :cond_0
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_1

    .line 879
    invoke-virtual {v8, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 881
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    .line 882
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    .line 883
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    .line 884
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    .line 885
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    .line 886
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v9

    iput-object v9, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mMRMPixelString:Ljava/lang/String;

    .line 887
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    .line 888
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    .line 889
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v9

    iput-object v9, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mMRMDataString:Ljava/lang/String;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_5
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 897
    :cond_1
    :try_start_3
    invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    .line 903
    .end local v2    # "bufferReader":Ljava/io/BufferedReader;
    .end local v5    # "fis":Ljava/io/FileInputStream;
    .end local v8    # "readStr":Ljava/lang/String;
    :cond_2
    :goto_0
    return-void

    .line 898
    .restart local v2    # "bufferReader":Ljava/io/BufferedReader;
    .restart local v5    # "fis":Ljava/io/FileInputStream;
    .restart local v8    # "readStr":Ljava/lang/String;
    :catch_0
    move-exception v3

    .line 899
    .local v3, "e":Ljava/io/IOException;
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 893
    .end local v2    # "bufferReader":Ljava/io/BufferedReader;
    .end local v3    # "e":Ljava/io/IOException;
    .end local v5    # "fis":Ljava/io/FileInputStream;
    .end local v8    # "readStr":Ljava/lang/String;
    .restart local v1    # "bufferReader":Ljava/io/BufferedReader;
    .restart local v4    # "fis":Ljava/io/FileInputStream;
    :catch_1
    move-exception v3

    .line 894
    .restart local v3    # "e":Ljava/io/IOException;
    :goto_1
    :try_start_4
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 897
    :try_start_5
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    goto :goto_0

    .line 898
    :catch_2
    move-exception v3

    .line 899
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 896
    .end local v3    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v9

    .line 897
    :goto_2
    :try_start_6
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    .line 900
    :goto_3
    throw v9

    .line 898
    :catch_3
    move-exception v3

    .line 899
    .restart local v3    # "e":Ljava/io/IOException;
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 896
    .end local v3    # "e":Ljava/io/IOException;
    .end local v4    # "fis":Ljava/io/FileInputStream;
    .restart local v5    # "fis":Ljava/io/FileInputStream;
    :catchall_1
    move-exception v9

    move-object v4, v5

    .end local v5    # "fis":Ljava/io/FileInputStream;
    .restart local v4    # "fis":Ljava/io/FileInputStream;
    goto :goto_2

    .end local v1    # "bufferReader":Ljava/io/BufferedReader;
    .end local v4    # "fis":Ljava/io/FileInputStream;
    .restart local v2    # "bufferReader":Ljava/io/BufferedReader;
    .restart local v5    # "fis":Ljava/io/FileInputStream;
    :catchall_2
    move-exception v9

    move-object v1, v2

    .end local v2    # "bufferReader":Ljava/io/BufferedReader;
    .restart local v1    # "bufferReader":Ljava/io/BufferedReader;
    move-object v4, v5

    .end local v5    # "fis":Ljava/io/FileInputStream;
    .restart local v4    # "fis":Ljava/io/FileInputStream;
    goto :goto_2

    .line 893
    .end local v4    # "fis":Ljava/io/FileInputStream;
    .restart local v5    # "fis":Ljava/io/FileInputStream;
    :catch_4
    move-exception v3

    move-object v4, v5

    .end local v5    # "fis":Ljava/io/FileInputStream;
    .restart local v4    # "fis":Ljava/io/FileInputStream;
    goto :goto_1

    .end local v1    # "bufferReader":Ljava/io/BufferedReader;
    .end local v4    # "fis":Ljava/io/FileInputStream;
    .restart local v2    # "bufferReader":Ljava/io/BufferedReader;
    .restart local v5    # "fis":Ljava/io/FileInputStream;
    :catch_5
    move-exception v3

    move-object v1, v2

    .end local v2    # "bufferReader":Ljava/io/BufferedReader;
    .restart local v1    # "bufferReader":Ljava/io/BufferedReader;
    move-object v4, v5

    .end local v5    # "fis":Ljava/io/FileInputStream;
    .restart local v4    # "fis":Ljava/io/FileInputStream;
    goto :goto_1
.end method

.method private readMethod3LogFile()V
    .locals 23

    .prologue
    .line 906
    const-string v12, "/data/validity/ValiditySnsrTest.log"

    .line 907
    .local v12, "path":Ljava/lang/String;
    const-string v16, "Pri PGA Gain Selected For SNR"

    .line 908
    .local v16, "priHeader":Ljava/lang/String;
    const-string v13, "Stimulus Av (x10)"

    .line 909
    .local v13, "pixelHeader":Ljava/lang/String;
    const-string v14, "No Stimulus Av (x10)"

    .line 910
    .local v14, "pixelHeader_NoStimulus":Ljava/lang/String;
    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    .line 911
    .local v15, "pixelList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v7, Ljava/io/File;

    invoke-direct {v7, v12}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 913
    .local v7, "file":Ljava/io/File;
    const-string v18, ""

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->m_ReadMethod3PrimaryPixel:Ljava/lang/String;

    .line 914
    const-string v18, ""

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->m_ReadMethod3SecondaryPixel:Ljava/lang/String;

    .line 915
    const/16 v18, 0x5

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->m_ReadMethod3PrimaryData:[Ljava/lang/String;

    .line 916
    const/16 v18, 0x5

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->m_ReadMethod3SecondaryData:[Ljava/lang/String;

    .line 917
    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v18

    if-eqz v18, :cond_2

    .line 918
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->CLASS_NAME:Ljava/lang/String;

    move-object/from16 v18, v0

    const-string v19, "readMethod3LogFile"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v20

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, " file Exist"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v18 .. v20}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 919
    const/4 v7, 0x0

    .line 920
    const/4 v8, 0x0

    .line 921
    .local v8, "fis":Ljava/io/FileInputStream;
    const/4 v3, 0x0

    .line 924
    .local v3, "bufferReader":Ljava/io/BufferedReader;
    :try_start_0
    new-instance v9, Ljava/io/FileInputStream;

    invoke-direct {v9, v12}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 925
    .end local v8    # "fis":Ljava/io/FileInputStream;
    .local v9, "fis":Ljava/io/FileInputStream;
    :try_start_1
    new-instance v4, Ljava/io/BufferedReader;

    new-instance v18, Ljava/io/InputStreamReader;

    move-object/from16 v0, v18

    invoke-direct {v0, v9}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    move-object/from16 v0, v18

    invoke-direct {v4, v0}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_5
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 926
    .end local v3    # "bufferReader":Ljava/io/BufferedReader;
    .local v4, "bufferReader":Ljava/io/BufferedReader;
    :try_start_2
    const-string v17, ""

    .line 928
    .local v17, "readStr":Ljava/lang/String;
    :cond_0
    :goto_0
    invoke-virtual {v4}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v17

    if-eqz v17, :cond_c

    .line 929
    move-object/from16 v0, v17

    invoke-virtual {v0, v13}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v18

    if-eqz v18, :cond_1

    move-object/from16 v0, v17

    invoke-virtual {v0, v14}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v18

    if-nez v18, :cond_1

    .line 930
    move-object/from16 v0, v17

    invoke-interface {v15, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 933
    :cond_1
    const-string v2, ""

    .local v2, "address":Ljava/lang/String;
    const-string v5, ""
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 936
    .local v5, "data":Ljava/lang/String;
    :try_start_3
    const-string v18, ","

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v18

    const/16 v19, 0x0

    aget-object v18, v18, v19

    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    .line 937
    const-string v18, ","

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v18

    const/16 v19, 0x1

    aget-object v18, v18, v19

    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->trim()Ljava/lang/String;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_6
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v5

    .line 945
    :goto_1
    :try_start_4
    const-string v18, "2760"

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_3

    .line 947
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->m_ReadMethod3PrimaryData:[Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    aput-object v5, v18, v19

    .line 948
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->CLASS_NAME:Ljava/lang/String;

    move-object/from16 v18, v0

    const-string v19, "readMethod3LogFile"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "Gain 1 : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->m_ReadMethod3PrimaryData:[Ljava/lang/String;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    aget-object v21, v21, v22

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v18 .. v20}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 997
    .end local v2    # "address":Ljava/lang/String;
    .end local v5    # "data":Ljava/lang/String;
    .end local v17    # "readStr":Ljava/lang/String;
    :catch_0
    move-exception v6

    move-object v3, v4

    .end local v4    # "bufferReader":Ljava/io/BufferedReader;
    .restart local v3    # "bufferReader":Ljava/io/BufferedReader;
    move-object v8, v9

    .line 998
    .end local v9    # "fis":Ljava/io/FileInputStream;
    .local v6, "e":Ljava/io/IOException;
    .restart local v8    # "fis":Ljava/io/FileInputStream;
    :goto_2
    :try_start_5
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 1001
    :try_start_6
    invoke-virtual {v8}, Ljava/io/FileInputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2

    .line 1007
    .end local v3    # "bufferReader":Ljava/io/BufferedReader;
    .end local v6    # "e":Ljava/io/IOException;
    .end local v8    # "fis":Ljava/io/FileInputStream;
    :cond_2
    :goto_3
    return-void

    .line 949
    .restart local v2    # "address":Ljava/lang/String;
    .restart local v4    # "bufferReader":Ljava/io/BufferedReader;
    .restart local v5    # "data":Ljava/lang/String;
    .restart local v9    # "fis":Ljava/io/FileInputStream;
    .restart local v17    # "readStr":Ljava/lang/String;
    :cond_3
    :try_start_7
    const-string v18, "2770"

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_4

    .line 951
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->m_ReadMethod3PrimaryData:[Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x1

    aput-object v5, v18, v19

    .line 952
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->CLASS_NAME:Ljava/lang/String;

    move-object/from16 v18, v0

    const-string v19, "readMethod3LogFile"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "Signal 1 : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->m_ReadMethod3PrimaryData:[Ljava/lang/String;

    move-object/from16 v21, v0

    const/16 v22, 0x1

    aget-object v21, v21, v22

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v18 .. v20}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto/16 :goto_0

    .line 1000
    .end local v2    # "address":Ljava/lang/String;
    .end local v5    # "data":Ljava/lang/String;
    .end local v17    # "readStr":Ljava/lang/String;
    :catchall_0
    move-exception v18

    move-object v3, v4

    .end local v4    # "bufferReader":Ljava/io/BufferedReader;
    .restart local v3    # "bufferReader":Ljava/io/BufferedReader;
    move-object v8, v9

    .line 1001
    .end local v9    # "fis":Ljava/io/FileInputStream;
    .restart local v8    # "fis":Ljava/io/FileInputStream;
    :goto_4
    :try_start_8
    invoke-virtual {v8}, Ljava/io/FileInputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_3

    .line 1004
    :goto_5
    throw v18

    .line 953
    .end local v3    # "bufferReader":Ljava/io/BufferedReader;
    .end local v8    # "fis":Ljava/io/FileInputStream;
    .restart local v2    # "address":Ljava/lang/String;
    .restart local v4    # "bufferReader":Ljava/io/BufferedReader;
    .restart local v5    # "data":Ljava/lang/String;
    .restart local v9    # "fis":Ljava/io/FileInputStream;
    .restart local v17    # "readStr":Ljava/lang/String;
    :cond_4
    :try_start_9
    const-string v18, "2780"

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_5

    .line 955
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->m_ReadMethod3PrimaryData:[Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x2

    aput-object v5, v18, v19

    .line 956
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->CLASS_NAME:Ljava/lang/String;

    move-object/from16 v18, v0

    const-string v19, "readMethod3LogFile"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "Noise 1 : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->m_ReadMethod3PrimaryData:[Ljava/lang/String;

    move-object/from16 v21, v0

    const/16 v22, 0x2

    aget-object v21, v21, v22

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v18 .. v20}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 957
    :cond_5
    const-string v18, "2790"

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_6

    .line 959
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->m_ReadMethod3PrimaryData:[Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x3

    aput-object v5, v18, v19

    .line 960
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->CLASS_NAME:Ljava/lang/String;

    move-object/from16 v18, v0

    const-string v19, "readMethod3LogFile"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "SNR 1 : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->m_ReadMethod3PrimaryData:[Ljava/lang/String;

    move-object/from16 v21, v0

    const/16 v22, 0x3

    aget-object v21, v21, v22

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v18 .. v20}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 961
    :cond_6
    const-string v18, "2870"

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_7

    .line 963
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->m_ReadMethod3SecondaryData:[Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    aput-object v5, v18, v19

    .line 964
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->CLASS_NAME:Ljava/lang/String;

    move-object/from16 v18, v0

    const-string v19, "readMethod3LogFile"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "Gain 2 : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->m_ReadMethod3SecondaryData:[Ljava/lang/String;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    aget-object v21, v21, v22

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v18 .. v20}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 965
    :cond_7
    const-string v18, "2880"

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_8

    .line 967
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->m_ReadMethod3SecondaryData:[Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x1

    aput-object v5, v18, v19

    .line 968
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->CLASS_NAME:Ljava/lang/String;

    move-object/from16 v18, v0

    const-string v19, "readMethod3LogFile"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "Signal 2 : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->m_ReadMethod3SecondaryData:[Ljava/lang/String;

    move-object/from16 v21, v0

    const/16 v22, 0x1

    aget-object v21, v21, v22

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v18 .. v20}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 969
    :cond_8
    const-string v18, "2890"

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_9

    .line 971
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->m_ReadMethod3SecondaryData:[Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x2

    aput-object v5, v18, v19

    .line 972
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->CLASS_NAME:Ljava/lang/String;

    move-object/from16 v18, v0

    const-string v19, "readMethod3LogFile"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "Noise 2 : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->m_ReadMethod3SecondaryData:[Ljava/lang/String;

    move-object/from16 v21, v0

    const/16 v22, 0x2

    aget-object v21, v21, v22

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v18 .. v20}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 973
    :cond_9
    const-string v18, "2900"

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_a

    .line 975
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->m_ReadMethod3SecondaryData:[Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x3

    aput-object v5, v18, v19

    .line 976
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->CLASS_NAME:Ljava/lang/String;

    move-object/from16 v18, v0

    const-string v19, "readMethod3LogFile"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "SNR 2 : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->m_ReadMethod3SecondaryData:[Ljava/lang/String;

    move-object/from16 v21, v0

    const/16 v22, 0x3

    aget-object v21, v21, v22

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v18 .. v20}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 977
    :cond_a
    const-string v18, "3220"

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_b

    .line 979
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->m_ReadMethod3PrimaryData:[Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x4

    aput-object v5, v18, v19

    .line 980
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->CLASS_NAME:Ljava/lang/String;

    move-object/from16 v18, v0

    const-string v19, "readMethod3LogFile"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "N.Noise 1 : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->m_ReadMethod3PrimaryData:[Ljava/lang/String;

    move-object/from16 v21, v0

    const/16 v22, 0x4

    aget-object v21, v21, v22

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v18 .. v20}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 981
    :cond_b
    const-string v18, "3310"

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_0

    .line 983
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->m_ReadMethod3SecondaryData:[Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x4

    aput-object v5, v18, v19

    .line 984
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->CLASS_NAME:Ljava/lang/String;

    move-object/from16 v18, v0

    const-string v19, "readMethod3LogFile"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "N.Noise 2 : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->m_ReadMethod3SecondaryData:[Ljava/lang/String;

    move-object/from16 v21, v0

    const/16 v22, 0x4

    aget-object v21, v21, v22

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v18 .. v20}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 990
    .end local v2    # "address":Ljava/lang/String;
    .end local v5    # "data":Ljava/lang/String;
    :cond_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->m_ReadMethod3PrimaryData:[Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    aget-object v10, v18, v19

    .line 991
    .local v10, "gain1":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->m_ReadMethod3SecondaryData:[Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    aget-object v11, v18, v19

    .line 994
    .local v11, "gain2":Ljava/lang/String;
    invoke-static {v10}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v18

    move/from16 v0, v18

    invoke-interface {v15, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/lang/String;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->m_ReadMethod3PrimaryPixel:Ljava/lang/String;

    .line 995
    invoke-static {v11}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v18

    move/from16 v0, v18

    invoke-interface {v15, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/lang/String;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->m_ReadMethod3SecondaryPixel:Ljava/lang/String;
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_0
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 1001
    :try_start_a
    invoke-virtual {v9}, Ljava/io/FileInputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_1

    goto/16 :goto_3

    .line 1002
    :catch_1
    move-exception v6

    .line 1003
    .restart local v6    # "e":Ljava/io/IOException;
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_3

    .line 1002
    .end local v4    # "bufferReader":Ljava/io/BufferedReader;
    .end local v9    # "fis":Ljava/io/FileInputStream;
    .end local v10    # "gain1":Ljava/lang/String;
    .end local v11    # "gain2":Ljava/lang/String;
    .end local v17    # "readStr":Ljava/lang/String;
    .restart local v3    # "bufferReader":Ljava/io/BufferedReader;
    .restart local v8    # "fis":Ljava/io/FileInputStream;
    :catch_2
    move-exception v6

    .line 1003
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_3

    .line 1002
    .end local v6    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v6

    .line 1003
    .restart local v6    # "e":Ljava/io/IOException;
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_5

    .line 1000
    .end local v6    # "e":Ljava/io/IOException;
    :catchall_1
    move-exception v18

    goto/16 :goto_4

    .end local v8    # "fis":Ljava/io/FileInputStream;
    .restart local v9    # "fis":Ljava/io/FileInputStream;
    :catchall_2
    move-exception v18

    move-object v8, v9

    .end local v9    # "fis":Ljava/io/FileInputStream;
    .restart local v8    # "fis":Ljava/io/FileInputStream;
    goto/16 :goto_4

    .line 997
    :catch_4
    move-exception v6

    goto/16 :goto_2

    .end local v8    # "fis":Ljava/io/FileInputStream;
    .restart local v9    # "fis":Ljava/io/FileInputStream;
    :catch_5
    move-exception v6

    move-object v8, v9

    .end local v9    # "fis":Ljava/io/FileInputStream;
    .restart local v8    # "fis":Ljava/io/FileInputStream;
    goto/16 :goto_2

    .line 941
    .end local v3    # "bufferReader":Ljava/io/BufferedReader;
    .end local v8    # "fis":Ljava/io/FileInputStream;
    .restart local v2    # "address":Ljava/lang/String;
    .restart local v4    # "bufferReader":Ljava/io/BufferedReader;
    .restart local v5    # "data":Ljava/lang/String;
    .restart local v9    # "fis":Ljava/io/FileInputStream;
    .restart local v17    # "readStr":Ljava/lang/String;
    :catch_6
    move-exception v18

    goto/16 :goto_1
.end method

.method private readMethod4LogFile()V
    .locals 20

    .prologue
    .line 1010
    const-string v13, "/data/validity/ValiditySnsrTest.log"

    .line 1011
    .local v13, "path":Ljava/lang/String;
    const-string v9, "No Term Signal (x10)"

    .line 1012
    .local v9, "mNoTermSignalHeader":Ljava/lang/String;
    const-string v12, "Term Signal (x10)"

    .line 1013
    .local v12, "mTermSignalHeader":Ljava/lang/String;
    const-string v10, "Term Percent Signal Loss (x10)"

    .line 1014
    .local v10, "mSignalLossHeader":Ljava/lang/String;
    const-string v11, "Region"

    .line 1016
    .local v11, "mSkipHeader":Ljava/lang/String;
    new-instance v6, Ljava/io/File;

    invoke-direct {v6, v13}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1017
    .local v6, "file":Ljava/io/File;
    const/16 v15, 0xc

    new-array v15, v15, [Ljava/lang/String;

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->m_ReadMethod4PrimaryData:[Ljava/lang/String;

    .line 1018
    const/16 v15, 0xc

    new-array v15, v15, [Ljava/lang/String;

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->m_ReadMethod4SecondaryData:[Ljava/lang/String;

    .line 1020
    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v15

    if-eqz v15, :cond_1

    .line 1021
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->CLASS_NAME:Ljava/lang/String;

    const-string v16, "readMethod4LogFile()"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v17

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, " file Exist"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v15 .. v17}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1022
    const/4 v6, 0x0

    .line 1023
    const/4 v7, 0x0

    .line 1024
    .local v7, "fis":Ljava/io/FileInputStream;
    const/4 v2, 0x0

    .line 1027
    .local v2, "bufferReader":Ljava/io/BufferedReader;
    :try_start_0
    new-instance v8, Ljava/io/FileInputStream;

    invoke-direct {v8, v13}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1028
    .end local v7    # "fis":Ljava/io/FileInputStream;
    .local v8, "fis":Ljava/io/FileInputStream;
    :try_start_1
    new-instance v3, Ljava/io/BufferedReader;

    new-instance v15, Ljava/io/InputStreamReader;

    invoke-direct {v15, v8}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v3, v15}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_5
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 1029
    .end local v2    # "bufferReader":Ljava/io/BufferedReader;
    .local v3, "bufferReader":Ljava/io/BufferedReader;
    :try_start_2
    const-string v14, ""

    .line 1031
    .local v14, "readStr":Ljava/lang/String;
    :cond_0
    :goto_0
    invoke-virtual {v3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v14

    if-eqz v14, :cond_1a

    .line 1040
    const-string v1, ""

    .local v1, "address":Ljava/lang/String;
    const-string v4, ""
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1043
    .local v4, "data":Ljava/lang/String;
    :try_start_3
    const-string v15, ","

    invoke-virtual {v14, v15}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v15

    const/16 v16, 0x0

    aget-object v15, v15, v16

    invoke-virtual {v15}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 1044
    const-string v15, ","

    invoke-virtual {v14, v15}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v15

    const/16 v16, 0x1

    aget-object v15, v15, v16

    invoke-virtual {v15}, Ljava/lang/String;->trim()Ljava/lang/String;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_6
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v4

    .line 1054
    :goto_1
    :try_start_4
    const-string v15, "13550"

    invoke-virtual {v15, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_2

    .line 1056
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->m_ReadMethod4PrimaryData:[Ljava/lang/String;

    const/16 v16, 0x0

    aput-object v4, v15, v16

    .line 1057
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->CLASS_NAME:Ljava/lang/String;

    const-string v16, "readMethod4LogFile"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "No Term Signal Pri : "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->m_ReadMethod4PrimaryData:[Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    aget-object v18, v18, v19

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v15 .. v17}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 1155
    .end local v1    # "address":Ljava/lang/String;
    .end local v4    # "data":Ljava/lang/String;
    .end local v14    # "readStr":Ljava/lang/String;
    :catch_0
    move-exception v5

    move-object v2, v3

    .end local v3    # "bufferReader":Ljava/io/BufferedReader;
    .restart local v2    # "bufferReader":Ljava/io/BufferedReader;
    move-object v7, v8

    .line 1156
    .end local v8    # "fis":Ljava/io/FileInputStream;
    .local v5, "e":Ljava/io/IOException;
    .restart local v7    # "fis":Ljava/io/FileInputStream;
    :goto_2
    :try_start_5
    invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 1158
    if-eqz v7, :cond_1

    .line 1160
    :try_start_6
    invoke-virtual {v7}, Ljava/io/FileInputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2

    .line 1167
    .end local v2    # "bufferReader":Ljava/io/BufferedReader;
    .end local v5    # "e":Ljava/io/IOException;
    .end local v7    # "fis":Ljava/io/FileInputStream;
    :cond_1
    :goto_3
    return-void

    .line 1058
    .restart local v1    # "address":Ljava/lang/String;
    .restart local v3    # "bufferReader":Ljava/io/BufferedReader;
    .restart local v4    # "data":Ljava/lang/String;
    .restart local v8    # "fis":Ljava/io/FileInputStream;
    .restart local v14    # "readStr":Ljava/lang/String;
    :cond_2
    :try_start_7
    const-string v15, "13670"

    invoke-virtual {v15, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_4

    .line 1060
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->m_ReadMethod4PrimaryData:[Ljava/lang/String;

    const/16 v16, 0x1

    aput-object v4, v15, v16

    .line 1061
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->CLASS_NAME:Ljava/lang/String;

    const-string v16, "readMethod4LogFile"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "No Term Noise Pri : "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->m_ReadMethod4PrimaryData:[Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x1

    aget-object v18, v18, v19

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v15 .. v17}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto/16 :goto_0

    .line 1158
    .end local v1    # "address":Ljava/lang/String;
    .end local v4    # "data":Ljava/lang/String;
    .end local v14    # "readStr":Ljava/lang/String;
    :catchall_0
    move-exception v15

    move-object v2, v3

    .end local v3    # "bufferReader":Ljava/io/BufferedReader;
    .restart local v2    # "bufferReader":Ljava/io/BufferedReader;
    move-object v7, v8

    .end local v8    # "fis":Ljava/io/FileInputStream;
    .restart local v7    # "fis":Ljava/io/FileInputStream;
    :goto_4
    if-eqz v7, :cond_3

    .line 1160
    :try_start_8
    invoke-virtual {v7}, Ljava/io/FileInputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_3

    .line 1163
    :cond_3
    :goto_5
    throw v15

    .line 1062
    .end local v2    # "bufferReader":Ljava/io/BufferedReader;
    .end local v7    # "fis":Ljava/io/FileInputStream;
    .restart local v1    # "address":Ljava/lang/String;
    .restart local v3    # "bufferReader":Ljava/io/BufferedReader;
    .restart local v4    # "data":Ljava/lang/String;
    .restart local v8    # "fis":Ljava/io/FileInputStream;
    .restart local v14    # "readStr":Ljava/lang/String;
    :cond_4
    :try_start_9
    const-string v15, "13790"

    invoke-virtual {v15, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_5

    .line 1064
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->m_ReadMethod4PrimaryData:[Ljava/lang/String;

    const/16 v16, 0x2

    aput-object v4, v15, v16

    .line 1065
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->CLASS_NAME:Ljava/lang/String;

    const-string v16, "readMethod4LogFile"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "No Term SNR Pri : "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->m_ReadMethod4PrimaryData:[Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x2

    aget-object v18, v18, v19

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v15 .. v17}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1066
    :cond_5
    const-string v15, "13880"

    invoke-virtual {v15, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_6

    .line 1068
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->m_ReadMethod4PrimaryData:[Ljava/lang/String;

    const/16 v16, 0x3

    aput-object v4, v15, v16

    .line 1069
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->CLASS_NAME:Ljava/lang/String;

    const-string v16, "readMethod4LogFile"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "No Term Pixel Fail Pri : "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->m_ReadMethod4PrimaryData:[Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x3

    aget-object v18, v18, v19

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v15 .. v17}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1070
    :cond_6
    const-string v15, "13890"

    invoke-virtual {v15, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_7

    .line 1072
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->m_ReadMethod4PrimaryData:[Ljava/lang/String;

    const/16 v16, 0x4

    aput-object v4, v15, v16

    .line 1073
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->CLASS_NAME:Ljava/lang/String;

    const-string v16, "readMethod4LogFile"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "No Term CSTV Fail Pri : "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->m_ReadMethod4PrimaryData:[Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x4

    aget-object v18, v18, v19

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v15 .. v17}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1074
    :cond_7
    const-string v15, "14390"

    invoke-virtual {v15, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_8

    .line 1076
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->m_ReadMethod4PrimaryData:[Ljava/lang/String;

    const/16 v16, 0x5

    aput-object v4, v15, v16

    .line 1077
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->CLASS_NAME:Ljava/lang/String;

    const-string v16, "readMethod4LogFile"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "Term Signal Pri : "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->m_ReadMethod4PrimaryData:[Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x5

    aget-object v18, v18, v19

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v15 .. v17}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1078
    :cond_8
    const-string v15, "14510"

    invoke-virtual {v15, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_9

    .line 1080
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->m_ReadMethod4PrimaryData:[Ljava/lang/String;

    const/16 v16, 0x6

    aput-object v4, v15, v16

    .line 1081
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->CLASS_NAME:Ljava/lang/String;

    const-string v16, "readMethod4LogFile"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "Term Signal Noise Pri : "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->m_ReadMethod4PrimaryData:[Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x6

    aget-object v18, v18, v19

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v15 .. v17}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1082
    :cond_9
    const-string v15, "14630"

    invoke-virtual {v15, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_a

    .line 1084
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->m_ReadMethod4PrimaryData:[Ljava/lang/String;

    const/16 v16, 0x7

    aput-object v4, v15, v16

    .line 1085
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->CLASS_NAME:Ljava/lang/String;

    const-string v16, "readMethod4LogFile"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "Term Signal SNR Pri : "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->m_ReadMethod4PrimaryData:[Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x7

    aget-object v18, v18, v19

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v15 .. v17}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1086
    :cond_a
    const-string v15, "14750"

    invoke-virtual {v15, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_b

    .line 1088
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->m_ReadMethod4PrimaryData:[Ljava/lang/String;

    const/16 v16, 0x8

    aput-object v4, v15, v16

    .line 1089
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->CLASS_NAME:Ljava/lang/String;

    const-string v16, "readMethod4LogFile"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "Signal Loss Rate Pri : "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->m_ReadMethod4PrimaryData:[Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x8

    aget-object v18, v18, v19

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v15 .. v17}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1090
    :cond_b
    const-string v15, "14840"

    invoke-virtual {v15, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_c

    .line 1092
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->m_ReadMethod4PrimaryData:[Ljava/lang/String;

    const/16 v16, 0x9

    aput-object v4, v15, v16

    .line 1093
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->CLASS_NAME:Ljava/lang/String;

    const-string v16, "readMethod4LogFile"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "Signal Loss Pixel Fail Pri : "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->m_ReadMethod4PrimaryData:[Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x9

    aget-object v18, v18, v19

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v15 .. v17}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1094
    :cond_c
    const-string v15, "14850"

    invoke-virtual {v15, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_d

    .line 1096
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->m_ReadMethod4PrimaryData:[Ljava/lang/String;

    const/16 v16, 0xa

    aput-object v4, v15, v16

    .line 1097
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->CLASS_NAME:Ljava/lang/String;

    const-string v16, "readMethod4LogFile"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "Signal Loss CSTC FAIL Pri : "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->m_ReadMethod4PrimaryData:[Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0xa

    aget-object v18, v18, v19

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v15 .. v17}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1098
    :cond_d
    const-string v15, "14870"

    invoke-virtual {v15, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_e

    .line 1100
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->m_ReadMethod4PrimaryData:[Ljava/lang/String;

    const/16 v16, 0xb

    aput-object v4, v15, v16

    .line 1101
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->CLASS_NAME:Ljava/lang/String;

    const-string v16, "readMethod4LogFile"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "Signal Loss Cal SNR Pri : "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->m_ReadMethod4PrimaryData:[Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0xb

    aget-object v18, v18, v19

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v15 .. v17}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1104
    :cond_e
    const-string v15, "15470"

    invoke-virtual {v15, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_f

    .line 1106
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->m_ReadMethod4SecondaryData:[Ljava/lang/String;

    const/16 v16, 0x0

    aput-object v4, v15, v16

    .line 1107
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->CLASS_NAME:Ljava/lang/String;

    const-string v16, "readMethod4LogFile"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "No Term Signal Sec : "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->m_ReadMethod4SecondaryData:[Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    aget-object v18, v18, v19

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v15 .. v17}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1108
    :cond_f
    const-string v15, "15590"

    invoke-virtual {v15, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_10

    .line 1110
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->m_ReadMethod4SecondaryData:[Ljava/lang/String;

    const/16 v16, 0x1

    aput-object v4, v15, v16

    .line 1111
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->CLASS_NAME:Ljava/lang/String;

    const-string v16, "readMethod4LogFile"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "No Term Noise Sec : "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->m_ReadMethod4SecondaryData:[Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x1

    aget-object v18, v18, v19

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v15 .. v17}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1112
    :cond_10
    const-string v15, "15710"

    invoke-virtual {v15, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_11

    .line 1114
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->m_ReadMethod4SecondaryData:[Ljava/lang/String;

    const/16 v16, 0x2

    aput-object v4, v15, v16

    .line 1115
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->CLASS_NAME:Ljava/lang/String;

    const-string v16, "readMethod4LogFile"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "No Term SNR Sec : "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->m_ReadMethod4SecondaryData:[Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x2

    aget-object v18, v18, v19

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v15 .. v17}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1116
    :cond_11
    const-string v15, "15800"

    invoke-virtual {v15, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_12

    .line 1118
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->m_ReadMethod4SecondaryData:[Ljava/lang/String;

    const/16 v16, 0x3

    aput-object v4, v15, v16

    .line 1119
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->CLASS_NAME:Ljava/lang/String;

    const-string v16, "readMethod4LogFile"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "No Term Pixel Fail Sec : "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->m_ReadMethod4SecondaryData:[Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x3

    aget-object v18, v18, v19

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v15 .. v17}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1120
    :cond_12
    const-string v15, "15810"

    invoke-virtual {v15, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_13

    .line 1122
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->m_ReadMethod4SecondaryData:[Ljava/lang/String;

    const/16 v16, 0x4

    aput-object v4, v15, v16

    .line 1123
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->CLASS_NAME:Ljava/lang/String;

    const-string v16, "readMethod4LogFile"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "No Term CSTV Fail Sec : "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->m_ReadMethod4SecondaryData:[Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x4

    aget-object v18, v18, v19

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v15 .. v17}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1124
    :cond_13
    const-string v15, "16310"

    invoke-virtual {v15, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_14

    .line 1126
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->m_ReadMethod4SecondaryData:[Ljava/lang/String;

    const/16 v16, 0x5

    aput-object v4, v15, v16

    .line 1127
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->CLASS_NAME:Ljava/lang/String;

    const-string v16, "readMethod4LogFile"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "Term Signal Sec : "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->m_ReadMethod4SecondaryData:[Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x5

    aget-object v18, v18, v19

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v15 .. v17}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1128
    :cond_14
    const-string v15, "16430"

    invoke-virtual {v15, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_15

    .line 1130
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->m_ReadMethod4SecondaryData:[Ljava/lang/String;

    const/16 v16, 0x6

    aput-object v4, v15, v16

    .line 1131
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->CLASS_NAME:Ljava/lang/String;

    const-string v16, "readMethod4LogFile"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "Term Signal Noise Sec : "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->m_ReadMethod4SecondaryData:[Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x6

    aget-object v18, v18, v19

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v15 .. v17}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1132
    :cond_15
    const-string v15, "16550"

    invoke-virtual {v15, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_16

    .line 1134
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->m_ReadMethod4SecondaryData:[Ljava/lang/String;

    const/16 v16, 0x7

    aput-object v4, v15, v16

    .line 1135
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->CLASS_NAME:Ljava/lang/String;

    const-string v16, "readMethod4LogFile"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "Term Signal SNR Sec : "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->m_ReadMethod4SecondaryData:[Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x7

    aget-object v18, v18, v19

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v15 .. v17}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1136
    :cond_16
    const-string v15, "16670"

    invoke-virtual {v15, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_17

    .line 1138
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->m_ReadMethod4SecondaryData:[Ljava/lang/String;

    const/16 v16, 0x8

    aput-object v4, v15, v16

    .line 1139
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->CLASS_NAME:Ljava/lang/String;

    const-string v16, "readMethod4LogFile"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "Signal Loss Rate Sec : "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->m_ReadMethod4SecondaryData:[Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x8

    aget-object v18, v18, v19

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v15 .. v17}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1140
    :cond_17
    const-string v15, "16760"

    invoke-virtual {v15, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_18

    .line 1142
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->m_ReadMethod4SecondaryData:[Ljava/lang/String;

    const/16 v16, 0x9

    aput-object v4, v15, v16

    .line 1143
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->CLASS_NAME:Ljava/lang/String;

    const-string v16, "readMethod4LogFile"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "Signal Loss Pixel Fail Sec : "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->m_ReadMethod4SecondaryData:[Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x9

    aget-object v18, v18, v19

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v15 .. v17}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1144
    :cond_18
    const-string v15, "16770"

    invoke-virtual {v15, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_19

    .line 1146
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->m_ReadMethod4SecondaryData:[Ljava/lang/String;

    const/16 v16, 0xa

    aput-object v4, v15, v16

    .line 1147
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->CLASS_NAME:Ljava/lang/String;

    const-string v16, "readMethod4LogFile"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "Signal Loss CSTC FAIL Sec : "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->m_ReadMethod4SecondaryData:[Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0xa

    aget-object v18, v18, v19

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v15 .. v17}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1148
    :cond_19
    const-string v15, "16790"

    invoke-virtual {v15, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_0

    .line 1150
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->m_ReadMethod4SecondaryData:[Ljava/lang/String;

    const/16 v16, 0xb

    aput-object v4, v15, v16

    .line 1151
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->CLASS_NAME:Ljava/lang/String;

    const-string v16, "readMethod4LogFile"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "Signal Loss Cal SNR Sec : "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->m_ReadMethod4SecondaryData:[Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0xb

    aget-object v18, v18, v19

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v15 .. v17}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_0
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 1158
    .end local v1    # "address":Ljava/lang/String;
    .end local v4    # "data":Ljava/lang/String;
    :cond_1a
    if-eqz v8, :cond_1

    .line 1160
    :try_start_a
    invoke-virtual {v8}, Ljava/io/FileInputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_1

    goto/16 :goto_3

    .line 1161
    :catch_1
    move-exception v5

    .line 1162
    .restart local v5    # "e":Ljava/io/IOException;
    invoke-static {v5}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/Exception;)V

    goto/16 :goto_3

    .line 1161
    .end local v3    # "bufferReader":Ljava/io/BufferedReader;
    .end local v8    # "fis":Ljava/io/FileInputStream;
    .end local v14    # "readStr":Ljava/lang/String;
    .restart local v2    # "bufferReader":Ljava/io/BufferedReader;
    .restart local v7    # "fis":Ljava/io/FileInputStream;
    :catch_2
    move-exception v5

    .line 1162
    invoke-static {v5}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/Exception;)V

    goto/16 :goto_3

    .line 1161
    .end local v5    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v5

    .line 1162
    .restart local v5    # "e":Ljava/io/IOException;
    invoke-static {v5}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/Exception;)V

    goto/16 :goto_5

    .line 1158
    .end local v5    # "e":Ljava/io/IOException;
    :catchall_1
    move-exception v15

    goto/16 :goto_4

    .end local v7    # "fis":Ljava/io/FileInputStream;
    .restart local v8    # "fis":Ljava/io/FileInputStream;
    :catchall_2
    move-exception v15

    move-object v7, v8

    .end local v8    # "fis":Ljava/io/FileInputStream;
    .restart local v7    # "fis":Ljava/io/FileInputStream;
    goto/16 :goto_4

    .line 1155
    :catch_4
    move-exception v5

    goto/16 :goto_2

    .end local v7    # "fis":Ljava/io/FileInputStream;
    .restart local v8    # "fis":Ljava/io/FileInputStream;
    :catch_5
    move-exception v5

    move-object v7, v8

    .end local v8    # "fis":Ljava/io/FileInputStream;
    .restart local v7    # "fis":Ljava/io/FileInputStream;
    goto/16 :goto_2

    .line 1050
    .end local v2    # "bufferReader":Ljava/io/BufferedReader;
    .end local v7    # "fis":Ljava/io/FileInputStream;
    .restart local v1    # "address":Ljava/lang/String;
    .restart local v3    # "bufferReader":Ljava/io/BufferedReader;
    .restart local v4    # "data":Ljava/lang/String;
    .restart local v8    # "fis":Ljava/io/FileInputStream;
    .restart local v14    # "readStr":Ljava/lang/String;
    :catch_6
    move-exception v15

    goto/16 :goto_1
.end method

.method private readNormalScanLogFile()V
    .locals 14

    .prologue
    .line 805
    const-string v7, "/data/validity/ValiditySnsrTest.log"

    .line 806
    .local v7, "path":Ljava/lang/String;
    const-string v0, "Start of Normal Scan Waveform Data "

    .line 807
    .local v0, "Header":Ljava/lang/String;
    const/4 v9, 0x0

    .line 808
    .local v9, "sectionCount":I
    new-instance v6, Ljava/io/File;

    invoke-direct {v6, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 809
    .local v6, "lfile":Ljava/io/File;
    const-string v10, ""

    iput-object v10, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mScanAverageString:Ljava/lang/String;

    .line 810
    const-string v10, ""

    iput-object v10, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mStdevString:Ljava/lang/String;

    .line 812
    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v10

    if-eqz v10, :cond_6

    .line 813
    iget-object v10, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->CLASS_NAME:Ljava/lang/String;

    const-string v11, "readNormalScanLogFile"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v12, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " File Exist"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v10, v11, v12}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 814
    const/4 v6, 0x0

    .line 815
    const/4 v4, 0x0

    .line 816
    .local v4, "fis":Ljava/io/FileInputStream;
    const/4 v1, 0x0

    .line 819
    .local v1, "bufferReader":Ljava/io/BufferedReader;
    :try_start_0
    new-instance v5, Ljava/io/FileInputStream;

    invoke-direct {v5, v7}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 820
    .end local v4    # "fis":Ljava/io/FileInputStream;
    .local v5, "fis":Ljava/io/FileInputStream;
    :try_start_1
    new-instance v2, Ljava/io/BufferedReader;

    new-instance v10, Ljava/io/InputStreamReader;

    invoke-direct {v10, v5}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v2, v10}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 821
    .end local v1    # "bufferReader":Ljava/io/BufferedReader;
    .local v2, "bufferReader":Ljava/io/BufferedReader;
    :try_start_2
    const-string v8, ""

    .line 822
    .local v8, "readStr":Ljava/lang/String;
    :cond_0
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_1

    .line 823
    invoke-virtual {v8, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 824
    iget-object v10, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->CLASS_NAME:Ljava/lang/String;

    const-string v11, "readNormalScanLogFile"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Found!! "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v10, v11, v12}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 825
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    .line 826
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v10

    iput-object v10, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mScanAverageString:Ljava/lang/String;

    .line 827
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v10

    iput-object v10, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mStdevString:Ljava/lang/String;

    .line 833
    :cond_1
    const-string v0, "Used Line Index"

    .line 834
    :cond_2
    :goto_0
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_5

    .line 835
    invoke-virtual {v8, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 836
    iget-object v10, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->CLASS_NAME:Ljava/lang/String;

    const-string v11, "readNormalScanLogFile"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Found!! "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v10, v11, v12}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 837
    if-nez v9, :cond_4

    .line 838
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v10

    iput-object v10, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mStdevPSString:Ljava/lang/String;

    .line 843
    :cond_3
    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    .line 839
    :cond_4
    const/4 v10, 0x1

    if-ne v9, v10, :cond_3

    .line 840
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v10

    iput-object v10, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mStdevSSString:Ljava/lang/String;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_5
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 850
    :cond_5
    :try_start_3
    invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    .line 856
    .end local v2    # "bufferReader":Ljava/io/BufferedReader;
    .end local v5    # "fis":Ljava/io/FileInputStream;
    .end local v8    # "readStr":Ljava/lang/String;
    :cond_6
    :goto_1
    return-void

    .line 851
    .restart local v2    # "bufferReader":Ljava/io/BufferedReader;
    .restart local v5    # "fis":Ljava/io/FileInputStream;
    .restart local v8    # "readStr":Ljava/lang/String;
    :catch_0
    move-exception v3

    .line 852
    .local v3, "e":Ljava/io/IOException;
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 846
    .end local v2    # "bufferReader":Ljava/io/BufferedReader;
    .end local v3    # "e":Ljava/io/IOException;
    .end local v5    # "fis":Ljava/io/FileInputStream;
    .end local v8    # "readStr":Ljava/lang/String;
    .restart local v1    # "bufferReader":Ljava/io/BufferedReader;
    .restart local v4    # "fis":Ljava/io/FileInputStream;
    :catch_1
    move-exception v3

    .line 847
    .restart local v3    # "e":Ljava/io/IOException;
    :goto_2
    :try_start_4
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 850
    :try_start_5
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    goto :goto_1

    .line 851
    :catch_2
    move-exception v3

    .line 852
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 849
    .end local v3    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v10

    .line 850
    :goto_3
    :try_start_6
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    .line 853
    :goto_4
    throw v10

    .line 851
    :catch_3
    move-exception v3

    .line 852
    .restart local v3    # "e":Ljava/io/IOException;
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 849
    .end local v3    # "e":Ljava/io/IOException;
    .end local v4    # "fis":Ljava/io/FileInputStream;
    .restart local v5    # "fis":Ljava/io/FileInputStream;
    :catchall_1
    move-exception v10

    move-object v4, v5

    .end local v5    # "fis":Ljava/io/FileInputStream;
    .restart local v4    # "fis":Ljava/io/FileInputStream;
    goto :goto_3

    .end local v1    # "bufferReader":Ljava/io/BufferedReader;
    .end local v4    # "fis":Ljava/io/FileInputStream;
    .restart local v2    # "bufferReader":Ljava/io/BufferedReader;
    .restart local v5    # "fis":Ljava/io/FileInputStream;
    :catchall_2
    move-exception v10

    move-object v1, v2

    .end local v2    # "bufferReader":Ljava/io/BufferedReader;
    .restart local v1    # "bufferReader":Ljava/io/BufferedReader;
    move-object v4, v5

    .end local v5    # "fis":Ljava/io/FileInputStream;
    .restart local v4    # "fis":Ljava/io/FileInputStream;
    goto :goto_3

    .line 846
    .end local v4    # "fis":Ljava/io/FileInputStream;
    .restart local v5    # "fis":Ljava/io/FileInputStream;
    :catch_4
    move-exception v3

    move-object v4, v5

    .end local v5    # "fis":Ljava/io/FileInputStream;
    .restart local v4    # "fis":Ljava/io/FileInputStream;
    goto :goto_2

    .end local v1    # "bufferReader":Ljava/io/BufferedReader;
    .end local v4    # "fis":Ljava/io/FileInputStream;
    .restart local v2    # "bufferReader":Ljava/io/BufferedReader;
    .restart local v5    # "fis":Ljava/io/FileInputStream;
    :catch_5
    move-exception v3

    move-object v1, v2

    .end local v2    # "bufferReader":Ljava/io/BufferedReader;
    .restart local v1    # "bufferReader":Ljava/io/BufferedReader;
    move-object v4, v5

    .end local v5    # "fis":Ljava/io/FileInputStream;
    .restart local v4    # "fis":Ljava/io/FileInputStream;
    goto :goto_2
.end method

.method private requestMRMData()Lcom/validity/fingerprint/SensorTest;
    .locals 8

    .prologue
    .line 692
    new-instance v3, Lcom/validity/fingerprint/SensorTest;

    invoke-direct {v3}, Lcom/validity/fingerprint/SensorTest;-><init>()V

    .line 693
    .local v3, "snrTest":Lcom/validity/fingerprint/SensorTest;
    const/4 v2, 0x0

    .line 694
    .local v2, "opt":I
    const/4 v1, 0x0

    .line 695
    .local v1, "dataLogopt":I
    const/high16 v0, 0x4000000

    .line 699
    .local v0, "VCS_SNSR_TEST_USE_SET_EVENT_WITH_CB_FLAG":I
    const/high16 v4, 0x8000000

    or-int/2addr v2, v4

    .line 700
    const/high16 v4, 0x10000

    or-int/2addr v2, v4

    .line 705
    or-int/2addr v2, v0

    .line 709
    or-int/lit8 v1, v1, 0x1

    .line 710
    or-int/lit8 v1, v1, 0x2

    .line 711
    or-int/lit8 v1, v1, 0x20

    .line 714
    or-int/lit16 v1, v1, 0x100

    .line 715
    or-int/lit16 v1, v1, 0x200

    .line 716
    or-int/lit16 v1, v1, 0x400

    .line 717
    or-int/lit16 v1, v1, 0x1000

    .line 718
    or-int/lit16 v1, v1, 0x2000

    .line 720
    const/high16 v4, 0x10000000

    or-int/lit16 v1, v4, 0x3723

    .line 728
    iput v2, v3, Lcom/validity/fingerprint/SensorTest;->options:I

    .line 729
    iput v1, v3, Lcom/validity/fingerprint/SensorTest;->dataLogOpt:I

    .line 730
    const/4 v4, 0x0

    iput v4, v3, Lcom/validity/fingerprint/SensorTest;->unitId:I

    .line 735
    iget-object v4, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "handleCommand"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "SensorTest["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, v3, Lcom/validity/fingerprint/SensorTest;->scriptId:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, v3, Lcom/validity/fingerprint/SensorTest;->options:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, v3, Lcom/validity/fingerprint/SensorTest;->dataLogOpt:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, v3, Lcom/validity/fingerprint/SensorTest;->unitId:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 738
    return-object v3
.end method

.method private requestMethod3Data()Lcom/validity/fingerprint/SensorTest;
    .locals 7

    .prologue
    .line 742
    new-instance v2, Lcom/validity/fingerprint/SensorTest;

    invoke-direct {v2}, Lcom/validity/fingerprint/SensorTest;-><init>()V

    .line 743
    .local v2, "snrTest":Lcom/validity/fingerprint/SensorTest;
    const/4 v1, 0x0

    .line 744
    .local v1, "opt":I
    const/4 v0, 0x0

    .line 747
    .local v0, "dataLogopt":I
    const/high16 v3, 0x10000

    or-int/2addr v1, v3

    .line 750
    or-int/lit8 v0, v0, 0x1

    .line 751
    const v3, 0x8000

    or-int/lit8 v0, v3, 0x1

    .line 752
    const/high16 v3, 0x10000000

    or-int/2addr v0, v3

    .line 754
    const-string v3, "60"

    iget-object v4, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mVersion:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 755
    const/16 v3, 0x813

    iput v3, v2, Lcom/validity/fingerprint/SensorTest;->scriptId:I

    .line 763
    :cond_0
    :goto_0
    iput v1, v2, Lcom/validity/fingerprint/SensorTest;->options:I

    .line 764
    iput v0, v2, Lcom/validity/fingerprint/SensorTest;->dataLogOpt:I

    .line 765
    const/4 v3, 0x0

    iput v3, v2, Lcom/validity/fingerprint/SensorTest;->unitId:I

    .line 767
    iget-object v3, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "handleCommand"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "SensorTest["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v2, Lcom/validity/fingerprint/SensorTest;->scriptId:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v2, Lcom/validity/fingerprint/SensorTest;->options:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v2, Lcom/validity/fingerprint/SensorTest;->dataLogOpt:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v2, Lcom/validity/fingerprint/SensorTest;->unitId:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 769
    return-object v2

    .line 756
    :cond_1
    const-string v3, "65"

    iget-object v4, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mVersion:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    const-string v3, "68A"

    iget-object v4, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mVersion:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 757
    :cond_2
    iget v3, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mFwVersion:I

    if-nez v3, :cond_3

    .line 758
    const/16 v3, 0x81d

    iput v3, v2, Lcom/validity/fingerprint/SensorTest;->scriptId:I

    goto :goto_0

    .line 760
    :cond_3
    const/16 v3, 0x827

    iput v3, v2, Lcom/validity/fingerprint/SensorTest;->scriptId:I

    goto :goto_0
.end method

.method private requestMethod4Data()Lcom/validity/fingerprint/SensorTest;
    .locals 7

    .prologue
    const/high16 v3, 0x10000

    .line 773
    new-instance v2, Lcom/validity/fingerprint/SensorTest;

    invoke-direct {v2}, Lcom/validity/fingerprint/SensorTest;-><init>()V

    .line 774
    .local v2, "snrTest":Lcom/validity/fingerprint/SensorTest;
    const/4 v1, 0x0

    .line 775
    .local v1, "opt":I
    const/4 v0, 0x0

    .line 778
    .local v0, "dataLogopt":I
    or-int/2addr v1, v3

    .line 781
    or-int/lit8 v0, v0, 0x1

    .line 782
    or-int/lit8 v0, v3, 0x1

    .line 783
    const/high16 v3, 0x20000

    or-int/2addr v0, v3

    .line 784
    const/high16 v3, 0x10000000

    or-int/2addr v0, v3

    .line 786
    const-string v3, "60"

    iget-object v4, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mVersion:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 787
    const/16 v3, 0x813

    iput v3, v2, Lcom/validity/fingerprint/SensorTest;->scriptId:I

    .line 795
    :cond_0
    :goto_0
    iput v1, v2, Lcom/validity/fingerprint/SensorTest;->options:I

    .line 796
    iput v0, v2, Lcom/validity/fingerprint/SensorTest;->dataLogOpt:I

    .line 797
    const/4 v3, 0x0

    iput v3, v2, Lcom/validity/fingerprint/SensorTest;->unitId:I

    .line 799
    iget-object v3, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "handleCommand"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "SensorTest["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v2, Lcom/validity/fingerprint/SensorTest;->scriptId:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v2, Lcom/validity/fingerprint/SensorTest;->options:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v2, Lcom/validity/fingerprint/SensorTest;->dataLogOpt:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v2, Lcom/validity/fingerprint/SensorTest;->unitId:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 801
    return-object v2

    .line 788
    :cond_1
    const-string v3, "65"

    iget-object v4, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mVersion:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    const-string v3, "68A"

    iget-object v4, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mVersion:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 789
    :cond_2
    iget v3, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mFwVersion:I

    if-nez v3, :cond_3

    .line 790
    const/16 v3, 0x81e

    iput v3, v2, Lcom/validity/fingerprint/SensorTest;->scriptId:I

    goto :goto_0

    .line 792
    :cond_3
    const/16 v3, 0x828

    iput v3, v2, Lcom/validity/fingerprint/SensorTest;->scriptId:I

    goto :goto_0
.end method

.method private requestNormalDataScan()Lcom/validity/fingerprint/SensorTest;
    .locals 7

    .prologue
    .line 658
    new-instance v2, Lcom/validity/fingerprint/SensorTest;

    invoke-direct {v2}, Lcom/validity/fingerprint/SensorTest;-><init>()V

    .line 659
    .local v2, "snrTest":Lcom/validity/fingerprint/SensorTest;
    const/4 v1, 0x0

    .line 660
    .local v1, "opt":I
    const/4 v0, 0x0

    .line 663
    .local v0, "dataLogopt":I
    const/high16 v3, 0x10000

    or-int/2addr v1, v3

    .line 666
    or-int/lit8 v0, v0, 0x1

    .line 667
    or-int/lit8 v0, v0, 0x40

    .line 668
    or-int/lit16 v0, v0, 0x80

    .line 669
    const/high16 v3, 0x10000000

    or-int/lit16 v0, v3, 0xc1

    .line 671
    const-string v3, "60"

    iget-object v4, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mVersion:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 672
    const/16 v3, 0x80c

    iput v3, v2, Lcom/validity/fingerprint/SensorTest;->scriptId:I

    .line 680
    :cond_0
    :goto_0
    iput v1, v2, Lcom/validity/fingerprint/SensorTest;->options:I

    .line 681
    iput v0, v2, Lcom/validity/fingerprint/SensorTest;->dataLogOpt:I

    .line 682
    const/4 v3, 0x0

    iput v3, v2, Lcom/validity/fingerprint/SensorTest;->unitId:I

    .line 684
    iget-object v3, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "handleCommand"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "SensorTest["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v2, Lcom/validity/fingerprint/SensorTest;->scriptId:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v2, Lcom/validity/fingerprint/SensorTest;->options:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v2, Lcom/validity/fingerprint/SensorTest;->dataLogOpt:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v2, Lcom/validity/fingerprint/SensorTest;->unitId:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 687
    return-object v2

    .line 673
    :cond_1
    const-string v3, "65"

    iget-object v4, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mVersion:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    const-string v3, "68A"

    iget-object v4, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mVersion:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 674
    :cond_2
    iget v3, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mFwVersion:I

    if-nez v3, :cond_3

    .line 675
    const/16 v3, 0x816

    iput v3, v2, Lcom/validity/fingerprint/SensorTest;->scriptId:I

    goto :goto_0

    .line 677
    :cond_3
    const/16 v3, 0x820

    iput v3, v2, Lcom/validity/fingerprint/SensorTest;->scriptId:I

    goto :goto_0
.end method


# virtual methods
.method public getUserList()Ljava/util/ArrayList;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 594
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 595
    .local v2, "retList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v5, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mFingerprint:Lcom/validity/fingerprint/Fingerprint;

    invoke-virtual {v5}, Lcom/validity/fingerprint/Fingerprint;->getSensorStatus()I

    move-result v5

    if-nez v5, :cond_1

    .line 596
    new-instance v4, Lcom/validity/fingerprint/VcsStringArray;

    invoke-direct {v4}, Lcom/validity/fingerprint/VcsStringArray;-><init>()V

    .line 597
    .local v4, "userList":Lcom/validity/fingerprint/VcsStringArray;
    iget-object v5, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mFingerprint:Lcom/validity/fingerprint/Fingerprint;

    invoke-virtual {v5, v4}, Lcom/validity/fingerprint/Fingerprint;->getUserList(Lcom/validity/fingerprint/VcsStringArray;)I

    move-result v3

    .line 598
    .local v3, "rsp":I
    if-nez v3, :cond_1

    .line 599
    iget-object v5, v4, Lcom/validity/fingerprint/VcsStringArray;->strlist:[Ljava/lang/String;

    if-eqz v5, :cond_0

    iget-object v5, v4, Lcom/validity/fingerprint/VcsStringArray;->strlist:[Ljava/lang/String;

    array-length v0, v5

    .line 600
    .local v0, "count":I
    :goto_0
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v0, :cond_1

    .line 601
    iget-object v5, v4, Lcom/validity/fingerprint/VcsStringArray;->strlist:[Ljava/lang/String;

    aget-object v5, v5, v1

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 600
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 599
    .end local v0    # "count":I
    .end local v1    # "i":I
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 605
    .end local v3    # "rsp":I
    .end local v4    # "userList":Lcom/validity/fingerprint/VcsStringArray;
    :cond_1
    return-object v2
.end method

.method public declared-synchronized handleCommand([Ljava/lang/String;)Ljava/lang/String;
    .locals 23
    .param p1, "argu"    # [Ljava/lang/String;

    .prologue
    .line 221
    monitor-enter p0

    const/4 v15, 0x0

    .line 222
    .local v15, "resData":Ljava/lang/String;
    const/16 v16, -0x1

    .line 224
    .local v16, "result":I
    :try_start_0
    move-object/from16 v0, p1

    array-length v0, v0

    move/from16 v19, v0

    sget-boolean v18, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->STAGE_PARALLEL:Z

    if-eqz v18, :cond_0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->NUM_ARGS:I

    move/from16 v18, v0

    add-int/lit8 v18, v18, 0x1

    :goto_0
    move/from16 v0, v19

    move/from16 v1, v18

    if-eq v0, v1, :cond_1

    .line 225
    invoke-virtual/range {p0 .. p0}, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->responseNA()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v18

    .line 495
    :goto_1
    monitor-exit p0

    return-object v18

    .line 224
    :cond_0
    :try_start_1
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->NUM_ARGS:I

    move/from16 v18, v0

    goto :goto_0

    .line 228
    :cond_1
    const/16 v18, 0x3

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    const-string v20, "1"

    aput-object v20, v18, v19

    const/16 v19, 0x1

    const-string v20, "4"

    aput-object v20, v18, v19

    const/16 v19, 0x2

    const-string v20, "0"

    aput-object v20, v18, v19

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v18

    if-nez v18, :cond_2

    .line 229
    new-instance v18, Lcom/validity/fingerprint/Fingerprint;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->context:Landroid/content/Context;

    move-object/from16 v19, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    move-object/from16 v2, p0

    invoke-direct {v0, v1, v2}, Lcom/validity/fingerprint/Fingerprint;-><init>(Landroid/content/Context;Lcom/validity/fingerprint/FingerprintCore$EventListener;)V

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mFingerprint:Lcom/validity/fingerprint/Fingerprint;

    .line 230
    invoke-direct/range {p0 .. p0}, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->getSensorVersion()Ljava/lang/String;

    .line 234
    :cond_2
    const/16 v18, 0x3

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    const-string v20, "0"

    aput-object v20, v18, v19

    const/16 v19, 0x1

    const-string v20, "0"

    aput-object v20, v18, v19

    const/16 v19, 0x2

    const-string v20, "0"

    aput-object v20, v18, v19

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_4

    .line 235
    invoke-virtual/range {p0 .. p0}, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->responseNA()Ljava/lang/String;

    move-result-object v15

    .line 486
    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mFingerprint:Lcom/validity/fingerprint/Fingerprint;

    move-object/from16 v18, v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    if-eqz v18, :cond_3

    .line 488
    :try_start_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mFingerprint:Lcom/validity/fingerprint/Fingerprint;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/validity/fingerprint/Fingerprint;->cancel()I

    .line 490
    const/16 v18, 0x0

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mFingerprint:Lcom/validity/fingerprint/Fingerprint;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :cond_3
    :goto_3
    move-object/from16 v18, v15

    .line 495
    goto/16 :goto_1

    .line 236
    :cond_4
    const/16 v18, 0x3

    :try_start_3
    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    const-string v20, "1"

    aput-object v20, v18, v19

    const/16 v19, 0x1

    const-string v20, "0"

    aput-object v20, v18, v19

    const/16 v19, 0x2

    const-string v20, "0"

    aput-object v20, v18, v19

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_5

    .line 237
    const/16 v18, 0x0

    aget-object v18, p1, v18

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mVersion:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "_"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mSensorInfo:Lcom/validity/fingerprint/SensorInfo;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/validity/fingerprint/SensorInfo;->productId:I

    move/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "_"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mSensorInfo:Lcom/validity/fingerprint/SensorInfo;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/validity/fingerprint/SensorInfo;->fwVersion:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->responseString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    goto/16 :goto_2

    .line 239
    :cond_5
    const/16 v18, 0x3

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    const-string v20, "1"

    aput-object v20, v18, v19

    const/16 v19, 0x1

    const-string v20, "1"

    aput-object v20, v18, v19

    const/16 v19, 0x2

    const-string v20, "0"

    aput-object v20, v18, v19

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_8

    .line 240
    const/16 v18, 0x3e9

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mCurrentOperation:I

    .line 241
    const-string v18, ""

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mScanAverageString:Ljava/lang/String;

    .line 242
    const-string v18, ""

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mStdevString:Ljava/lang/String;

    .line 243
    new-instance v18, Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;

    invoke-direct/range {v18 .. v18}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;-><init>()V

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mPrimaryRawData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;

    .line 244
    new-instance v18, Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;

    invoke-direct/range {v18 .. v18}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;-><init>()V

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mSecondaryRawData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;

    .line 245
    new-instance v18, Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;

    invoke-direct/range {v18 .. v18}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;-><init>()V

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mPrimarySectionRawData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;

    .line 246
    new-instance v18, Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;

    invoke-direct/range {v18 .. v18}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;-><init>()V

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mSecondarySectionRawData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;

    .line 247
    new-instance v18, Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;

    invoke-direct/range {v18 .. v18}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;-><init>()V

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mPrimaryVerticalRawData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;

    .line 248
    new-instance v18, Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;

    invoke-direct/range {v18 .. v18}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;-><init>()V

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mSecondaryVerticalRawData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;

    .line 249
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mFingerprint:Lcom/validity/fingerprint/Fingerprint;

    move-object/from16 v18, v0

    const/16 v19, 0xb

    invoke-direct/range {p0 .. p0}, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->requestNormalDataScan()Lcom/validity/fingerprint/SensorTest;

    move-result-object v20

    invoke-virtual/range {v18 .. v20}, Lcom/validity/fingerprint/Fingerprint;->request(ILjava/lang/Object;)I

    move-result v16

    .line 250
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->CLASS_NAME:Ljava/lang/String;

    move-object/from16 v18, v0

    const-string v19, "handleCommand"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "Sensor status : OPERATION_REQUEST_NORMALSCAN_DATA ["

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "]"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v18 .. v20}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 252
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->token:Ljava/lang/Object;

    move-object/from16 v19, v0

    monitor-enter v19
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 254
    :try_start_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->token:Ljava/lang/Object;

    move-object/from16 v18, v0

    const-wide/16 v20, 0xbb8

    move-object/from16 v0, v18

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Ljava/lang/Object;->wait(J)V
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 258
    :goto_4
    :try_start_5
    monitor-exit v19
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 260
    :try_start_6
    const-string v18, ""

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mStdevString:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-nez v18, :cond_6

    const-string v18, ""

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mScanAverageString:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_7

    .line 261
    :cond_6
    const/16 v18, 0x0

    aget-object v18, p1, v18

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->responseNG(Ljava/lang/String;)Ljava/lang/String;
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    move-result-object v15

    goto/16 :goto_2

    .line 255
    :catch_0
    move-exception v6

    .line 256
    .local v6, "e":Ljava/lang/InterruptedException;
    :try_start_7
    invoke-virtual {v6}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_4

    .line 258
    .end local v6    # "e":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v18

    monitor-exit v19
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    :try_start_8
    throw v18
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 221
    :catchall_1
    move-exception v18

    monitor-exit p0

    throw v18

    .line 263
    :cond_7
    :try_start_9
    invoke-direct/range {p0 .. p0}, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->parseNormalScan()V

    .line 264
    const/16 v18, 0x0

    aget-object v18, p1, v18

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mPrimaryRawData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    invoke-virtual/range {v20 .. v21}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;->getString(I)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "_"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mSecondaryRawData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    invoke-virtual/range {v20 .. v21}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;->getString(I)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->responseString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    goto/16 :goto_2

    .line 267
    :cond_8
    const/16 v18, 0x3

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    const-string v20, "1"

    aput-object v20, v18, v19

    const/16 v19, 0x1

    const-string v20, "1"

    aput-object v20, v18, v19

    const/16 v19, 0x2

    const-string v20, "1"

    aput-object v20, v18, v19

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_b

    .line 268
    const/16 v18, 0x3ee

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mCurrentOperation:I

    .line 269
    const-string v18, ""

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->m_ReadMethod3PrimaryPixel:Ljava/lang/String;

    .line 270
    const-string v18, ""

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->m_ReadMethod3SecondaryPixel:Ljava/lang/String;

    .line 271
    new-instance v18, Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;

    invoke-direct/range {v18 .. v18}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;-><init>()V

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mMethod3PrimaryRawData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;

    .line 272
    new-instance v18, Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;

    invoke-direct/range {v18 .. v18}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;-><init>()V

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mMethod3SecondaryRawData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;

    .line 274
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mFingerprint:Lcom/validity/fingerprint/Fingerprint;

    move-object/from16 v18, v0

    const/16 v19, 0xb

    invoke-direct/range {p0 .. p0}, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->requestMethod3Data()Lcom/validity/fingerprint/SensorTest;

    move-result-object v20

    invoke-virtual/range {v18 .. v20}, Lcom/validity/fingerprint/Fingerprint;->request(ILjava/lang/Object;)I

    move-result v16

    .line 275
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->CLASS_NAME:Ljava/lang/String;

    move-object/from16 v18, v0

    const-string v19, "handleCommand"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "Sensor status : VCS_REQUEST_COMMAND_SENSOR_TEST ["

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "]"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v18 .. v20}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 277
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->token:Ljava/lang/Object;

    move-object/from16 v19, v0

    monitor-enter v19
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    .line 279
    :try_start_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->token:Ljava/lang/Object;

    move-object/from16 v18, v0

    const-wide/16 v20, 0x2710

    move-object/from16 v0, v18

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Ljava/lang/Object;->wait(J)V
    :try_end_a
    .catch Ljava/lang/InterruptedException; {:try_start_a .. :try_end_a} :catch_5
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    .line 284
    :goto_5
    :try_start_b
    const-string v18, ""

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->m_ReadMethod3PrimaryPixel:Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-nez v18, :cond_9

    const-string v18, ""

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->m_ReadMethod3SecondaryPixel:Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_a

    .line 285
    :cond_9
    const/16 v18, 0x0

    aget-object v18, p1, v18

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->responseNG(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 291
    :goto_6
    monitor-exit v19

    goto/16 :goto_2

    :catchall_2
    move-exception v18

    monitor-exit v19
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_2

    :try_start_c
    throw v18
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    .line 287
    :cond_a
    :try_start_d
    invoke-direct/range {p0 .. p0}, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->parseMethod3Data()V

    .line 288
    const/16 v18, 0x0

    aget-object v18, p1, v18

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mMethod3PrimaryRawData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;

    move-object/from16 v21, v0

    const/16 v22, 0x1

    invoke-virtual/range {v21 .. v22}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;->getString(I)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "_"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mMethod3SecondaryRawData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;

    move-object/from16 v21, v0

    const/16 v22, 0x1

    invoke-virtual/range {v21 .. v22}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;->getString(I)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->responseString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_2

    move-result-object v15

    goto :goto_6

    .line 292
    :cond_b
    const/16 v18, 0x3

    :try_start_e
    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    const-string v20, "1"

    aput-object v20, v18, v19

    const/16 v19, 0x1

    const-string v20, "2"

    aput-object v20, v18, v19

    const/16 v19, 0x2

    const-string v20, "0"

    aput-object v20, v18, v19

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_d

    .line 297
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->CLASS_NAME:Ljava/lang/String;

    move-object/from16 v18, v0

    const-string v19, "getUserList Size"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "Size : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {p0 .. p0}, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->getUserList()Ljava/util/ArrayList;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/util/ArrayList;->size()I

    move-result v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v18 .. v20}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 298
    invoke-virtual/range {p0 .. p0}, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->getUserList()Ljava/util/ArrayList;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->size()I

    move-result v18

    if-nez v18, :cond_c

    .line 299
    const/16 v18, 0x0

    aget-object v18, p1, v18

    const-string v19, "OFF"

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->responseString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    goto/16 :goto_2

    .line 302
    :cond_c
    const/16 v18, 0x0

    aget-object v18, p1, v18

    const-string v19, "ON"

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->responseString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    goto/16 :goto_2

    .line 304
    :cond_d
    const/16 v18, 0x3

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    const-string v20, "1"

    aput-object v20, v18, v19

    const/16 v19, 0x1

    const-string v20, "3"

    aput-object v20, v18, v19

    const/16 v19, 0x2

    const-string v20, "0"

    aput-object v20, v18, v19

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_11

    .line 305
    const/16 v18, 0x3e9

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mCurrentOperation:I

    .line 306
    const-string v18, ""

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mScanAverageString:Ljava/lang/String;

    .line 307
    const-string v18, ""

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mStdevString:Ljava/lang/String;

    .line 308
    new-instance v18, Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;

    invoke-direct/range {v18 .. v18}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;-><init>()V

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mPrimaryRawData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;

    .line 309
    new-instance v18, Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;

    invoke-direct/range {v18 .. v18}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;-><init>()V

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mSecondaryRawData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;

    .line 310
    new-instance v18, Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;

    invoke-direct/range {v18 .. v18}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;-><init>()V

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mPrimarySectionRawData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;

    .line 311
    new-instance v18, Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;

    invoke-direct/range {v18 .. v18}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;-><init>()V

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mSecondarySectionRawData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;

    .line 312
    new-instance v18, Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;

    invoke-direct/range {v18 .. v18}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;-><init>()V

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mPrimaryVerticalRawData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;

    .line 313
    new-instance v18, Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;

    invoke-direct/range {v18 .. v18}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;-><init>()V

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mSecondaryVerticalRawData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;

    .line 315
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mFingerprint:Lcom/validity/fingerprint/Fingerprint;

    move-object/from16 v18, v0

    const/16 v19, 0xb

    invoke-direct/range {p0 .. p0}, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->requestNormalDataScan()Lcom/validity/fingerprint/SensorTest;

    move-result-object v20

    invoke-virtual/range {v18 .. v20}, Lcom/validity/fingerprint/Fingerprint;->request(ILjava/lang/Object;)I

    move-result v16

    .line 316
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->CLASS_NAME:Ljava/lang/String;

    move-object/from16 v18, v0

    const-string v19, "handleCommand"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "Sensor status : STATE_NORMALSCAN ["

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "]"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v18 .. v20}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 318
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->token:Ljava/lang/Object;

    move-object/from16 v19, v0

    monitor-enter v19
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_1

    .line 320
    :try_start_f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->token:Ljava/lang/Object;

    move-object/from16 v18, v0

    const-wide/16 v20, 0xbb8

    move-object/from16 v0, v18

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Ljava/lang/Object;->wait(J)V
    :try_end_f
    .catch Ljava/lang/InterruptedException; {:try_start_f .. :try_end_f} :catch_4
    .catchall {:try_start_f .. :try_end_f} :catchall_3

    .line 324
    :goto_7
    :try_start_10
    monitor-exit v19
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_3

    .line 326
    :try_start_11
    const-string v18, ""

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mStdevString:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-nez v18, :cond_e

    const-string v18, ""

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mScanAverageString:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_f

    .line 327
    :cond_e
    const/16 v18, 0x0

    aget-object v18, p1, v18

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->responseNG(Ljava/lang/String;)Ljava/lang/String;
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_1

    move-result-object v15

    goto/16 :goto_2

    .line 324
    :catchall_3
    move-exception v18

    :try_start_12
    monitor-exit v19
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_3

    :try_start_13
    throw v18

    .line 329
    :cond_f
    invoke-direct/range {p0 .. p0}, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->parseNormalScan()V

    .line 331
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mPrimaryRawData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    invoke-virtual/range {v19 .. v20}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;->getMin(I)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ","

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mPrimaryRawData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    invoke-virtual/range {v19 .. v20}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;->getMax(I)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ","

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mPrimaryRawData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    invoke-virtual/range {v19 .. v20}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;->getAvg(I)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ","

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mPrimaryVerticalRawData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;

    move-object/from16 v19, v0

    const/16 v20, 0x1

    invoke-virtual/range {v19 .. v20}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;->getMax(I)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ","

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mPrimaryVerticalRawData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;

    move-object/from16 v19, v0

    const/16 v20, 0x1

    invoke-virtual/range {v19 .. v20}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;->getAvg(I)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ","

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mPrimarySectionRawData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;

    move-object/from16 v19, v0

    const/16 v20, 0x1

    invoke-virtual/range {v19 .. v20}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;->getMax(I)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ","

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mPrimarySectionRawData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;

    move-object/from16 v19, v0

    const/16 v20, 0x1

    invoke-virtual/range {v19 .. v20}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;->getAvg(I)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "_"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mSecondaryRawData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    invoke-virtual/range {v19 .. v20}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;->getMin(I)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ","

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mSecondaryRawData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    invoke-virtual/range {v19 .. v20}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;->getMax(I)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ","

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mSecondaryRawData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    invoke-virtual/range {v19 .. v20}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;->getAvg(I)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ","

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mSecondaryVerticalRawData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;

    move-object/from16 v19, v0

    const/16 v20, 0x1

    invoke-virtual/range {v19 .. v20}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;->getMax(I)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ","

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mSecondaryVerticalRawData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;

    move-object/from16 v19, v0

    const/16 v20, 0x1

    invoke-virtual/range {v19 .. v20}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;->getAvg(I)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ","

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mSecondarySectionRawData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;

    move-object/from16 v19, v0

    const/16 v20, 0x1

    invoke-virtual/range {v19 .. v20}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;->getMax(I)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ","

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mSecondarySectionRawData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;

    move-object/from16 v19, v0

    const/16 v20, 0x1

    invoke-virtual/range {v19 .. v20}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;->getAvg(I)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    .line 340
    .local v17, "values":Ljava/lang/String;
    invoke-direct/range {p0 .. p0}, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->checkNormalScanSpec()Z

    move-result v18

    if-eqz v18, :cond_10

    .line 341
    const/16 v18, 0x0

    aget-object v18, p1, v18

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "OK_"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->responseString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    goto/16 :goto_2

    .line 343
    :cond_10
    const/16 v18, 0x0

    aget-object v18, p1, v18

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "NG_"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->responseString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    goto/16 :goto_2

    .line 346
    .end local v17    # "values":Ljava/lang/String;
    :cond_11
    const/16 v18, 0x3

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    const-string v20, "1"

    aput-object v20, v18, v19

    const/16 v19, 0x1

    const-string v20, "3"

    aput-object v20, v18, v19

    const/16 v19, 0x2

    const-string v20, "1"

    aput-object v20, v18, v19

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_15

    .line 347
    const/16 v18, 0x3ee

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mCurrentOperation:I

    .line 348
    const/16 v18, 0x0

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->m_ReadMethod3PrimaryData:[Ljava/lang/String;

    .line 349
    const/16 v18, 0x0

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->m_ReadMethod3SecondaryData:[Ljava/lang/String;

    .line 350
    new-instance v18, Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;

    invoke-direct/range {v18 .. v18}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;-><init>()V

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mMethod3PrimaryRawData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;

    .line 351
    new-instance v18, Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;

    invoke-direct/range {v18 .. v18}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;-><init>()V

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mMethod3SecondaryRawData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$RawData;

    .line 352
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mFingerprint:Lcom/validity/fingerprint/Fingerprint;

    move-object/from16 v18, v0

    const/16 v19, 0xb

    invoke-direct/range {p0 .. p0}, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->requestMethod3Data()Lcom/validity/fingerprint/SensorTest;

    move-result-object v20

    invoke-virtual/range {v18 .. v20}, Lcom/validity/fingerprint/Fingerprint;->request(ILjava/lang/Object;)I

    move-result v16

    .line 353
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->CLASS_NAME:Ljava/lang/String;

    move-object/from16 v18, v0

    const-string v19, "handleCommand"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "Sensor status : VCS_REQUEST_COMMAND_SENSOR_TEST ["

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "]"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v18 .. v20}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 355
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->token:Ljava/lang/Object;

    move-object/from16 v19, v0

    monitor-enter v19
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_1

    .line 357
    :try_start_14
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->token:Ljava/lang/Object;

    move-object/from16 v18, v0

    const-wide/16 v20, 0x2710

    move-object/from16 v0, v18

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Ljava/lang/Object;->wait(J)V
    :try_end_14
    .catch Ljava/lang/InterruptedException; {:try_start_14 .. :try_end_14} :catch_3
    .catchall {:try_start_14 .. :try_end_14} :catchall_4

    .line 361
    :goto_8
    :try_start_15
    monitor-exit v19
    :try_end_15
    .catchall {:try_start_15 .. :try_end_15} :catchall_4

    .line 363
    :try_start_16
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->m_ReadMethod3PrimaryData:[Ljava/lang/String;

    move-object/from16 v18, v0

    if-eqz v18, :cond_12

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->m_ReadMethod3SecondaryData:[Ljava/lang/String;

    move-object/from16 v18, v0

    if-nez v18, :cond_13

    .line 364
    :cond_12
    const/16 v18, 0x0

    aget-object v18, p1, v18

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->responseNG(Ljava/lang/String;)Ljava/lang/String;
    :try_end_16
    .catchall {:try_start_16 .. :try_end_16} :catchall_1

    move-result-object v15

    goto/16 :goto_2

    .line 361
    :catchall_4
    move-exception v18

    :try_start_17
    monitor-exit v19
    :try_end_17
    .catchall {:try_start_17 .. :try_end_17} :catchall_4

    :try_start_18
    throw v18

    .line 366
    :cond_13
    invoke-direct/range {p0 .. p0}, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->parseMethod3Data()V

    .line 368
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mMethod3PrimaryData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method3Data;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method3Data;->getGain()I

    move-result v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ","

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "%.1f"

    const/16 v20, 0x1

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mMethod3PrimaryData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method3Data;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method3Data;->getSignal()F

    move-result v22

    invoke-static/range {v22 .. v22}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v22

    aput-object v22, v20, v21

    invoke-static/range {v19 .. v20}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ","

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "%.1f"

    const/16 v20, 0x1

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mMethod3PrimaryData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method3Data;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method3Data;->getNoise()F

    move-result v22

    invoke-static/range {v22 .. v22}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v22

    aput-object v22, v20, v21

    invoke-static/range {v19 .. v20}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ","

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "%.1f"

    const/16 v20, 0x1

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mMethod3PrimaryData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method3Data;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method3Data;->getSNR()F

    move-result v22

    invoke-static/range {v22 .. v22}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v22

    aput-object v22, v20, v21

    invoke-static/range {v19 .. v20}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ","

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "%.1f"

    const/16 v20, 0x1

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mMethod3PrimaryData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method3Data;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method3Data;->getNNoise()F

    move-result v22

    invoke-static/range {v22 .. v22}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v22

    aput-object v22, v20, v21

    invoke-static/range {v19 .. v20}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "_"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mMethod3SecondaryData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method3Data;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method3Data;->getGain()I

    move-result v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ","

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "%.1f"

    const/16 v20, 0x1

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mMethod3SecondaryData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method3Data;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method3Data;->getSignal()F

    move-result v22

    invoke-static/range {v22 .. v22}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v22

    aput-object v22, v20, v21

    invoke-static/range {v19 .. v20}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ","

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "%.1f"

    const/16 v20, 0x1

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mMethod3SecondaryData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method3Data;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method3Data;->getNoise()F

    move-result v22

    invoke-static/range {v22 .. v22}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v22

    aput-object v22, v20, v21

    invoke-static/range {v19 .. v20}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ","

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "%.1f"

    const/16 v20, 0x1

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mMethod3SecondaryData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method3Data;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method3Data;->getSNR()F

    move-result v22

    invoke-static/range {v22 .. v22}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v22

    aput-object v22, v20, v21

    invoke-static/range {v19 .. v20}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ","

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "%.1f"

    const/16 v20, 0x1

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mMethod3SecondaryData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method3Data;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method3Data;->getNNoise()F

    move-result v22

    invoke-static/range {v22 .. v22}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v22

    aput-object v22, v20, v21

    invoke-static/range {v19 .. v20}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    .line 380
    .restart local v17    # "values":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->CLASS_NAME:Ljava/lang/String;

    move-object/from16 v18, v0

    const-string v19, "handleCommand"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "SNR value : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v18 .. v20}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 382
    invoke-direct/range {p0 .. p0}, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->checkMethod3DataSpec()Z

    move-result v18

    if-eqz v18, :cond_14

    .line 383
    const/16 v18, 0x0

    aget-object v18, p1, v18

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "OK_"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->responseString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    goto/16 :goto_2

    .line 385
    :cond_14
    const/16 v18, 0x0

    aget-object v18, p1, v18

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "NG_"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->responseString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    goto/16 :goto_2

    .line 388
    .end local v17    # "values":Ljava/lang/String;
    :cond_15
    const/16 v18, 0x3

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    const-string v20, "1"

    aput-object v20, v18, v19

    const/16 v19, 0x1

    const-string v20, "3"

    aput-object v20, v18, v19

    const/16 v19, 0x2

    const-string v20, "2"

    aput-object v20, v18, v19

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_19

    .line 390
    const/16 v18, 0x0

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->m_ReadMethod4PrimaryData:[Ljava/lang/String;

    .line 391
    const/16 v18, 0x0

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->m_ReadMethod4SecondaryData:[Ljava/lang/String;

    .line 394
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mFingerprint:Lcom/validity/fingerprint/Fingerprint;

    move-object/from16 v18, v0

    const/16 v19, 0xb

    invoke-direct/range {p0 .. p0}, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->requestMethod4Data()Lcom/validity/fingerprint/SensorTest;

    move-result-object v20

    invoke-virtual/range {v18 .. v20}, Lcom/validity/fingerprint/Fingerprint;->request(ILjava/lang/Object;)I

    move-result v16

    .line 395
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->CLASS_NAME:Ljava/lang/String;

    move-object/from16 v18, v0

    const-string v19, "handleCommand"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "Sensor status : VCS_REQUEST_COMMAND_SENSOR_TEST ["

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "]"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v18 .. v20}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 397
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->token:Ljava/lang/Object;

    move-object/from16 v19, v0

    monitor-enter v19
    :try_end_18
    .catchall {:try_start_18 .. :try_end_18} :catchall_1

    .line 399
    :try_start_19
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->token:Ljava/lang/Object;

    move-object/from16 v18, v0

    const-wide/16 v20, 0x2710

    move-object/from16 v0, v18

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Ljava/lang/Object;->wait(J)V
    :try_end_19
    .catch Ljava/lang/InterruptedException; {:try_start_19 .. :try_end_19} :catch_2
    .catchall {:try_start_19 .. :try_end_19} :catchall_5

    .line 403
    :goto_9
    :try_start_1a
    monitor-exit v19
    :try_end_1a
    .catchall {:try_start_1a .. :try_end_1a} :catchall_5

    .line 405
    :try_start_1b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->m_ReadMethod4PrimaryData:[Ljava/lang/String;

    move-object/from16 v18, v0

    if-eqz v18, :cond_16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->m_ReadMethod4SecondaryData:[Ljava/lang/String;

    move-object/from16 v18, v0

    if-nez v18, :cond_17

    .line 406
    :cond_16
    const/16 v18, 0x0

    aget-object v18, p1, v18

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->responseNG(Ljava/lang/String;)Ljava/lang/String;
    :try_end_1b
    .catchall {:try_start_1b .. :try_end_1b} :catchall_1

    move-result-object v15

    goto/16 :goto_2

    .line 403
    :catchall_5
    move-exception v18

    :try_start_1c
    monitor-exit v19
    :try_end_1c
    .catchall {:try_start_1c .. :try_end_1c} :catchall_5

    :try_start_1d
    throw v18

    .line 408
    :cond_17
    invoke-direct/range {p0 .. p0}, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->parseMethod4Data()V

    .line 410
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mMethod4PrimaryData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method4Data;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method4Data;->getRate()I

    move-result v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ","

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mMethod4PrimaryData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method4Data;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method4Data;->getPixelFail()I

    move-result v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ","

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mMethod4PrimaryData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method4Data;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method4Data;->getCSTVFail()I

    move-result v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ","

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mMethod4PrimaryData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method4Data;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method4Data;->getCalSNR()I

    move-result v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "_"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mMethod4SecondaryData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method4Data;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method4Data;->getRate()I

    move-result v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ","

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mMethod4SecondaryData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method4Data;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method4Data;->getPixelFail()I

    move-result v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ","

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mMethod4SecondaryData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method4Data;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method4Data;->getCSTVFail()I

    move-result v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ","

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mMethod4SecondaryData:Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method4Data;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method4Data;->getCalSNR()I

    move-result v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    .line 419
    .restart local v17    # "values":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->CLASS_NAME:Ljava/lang/String;

    move-object/from16 v18, v0

    const-string v19, "handleCommand"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "SNR value : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v18 .. v20}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 421
    invoke-direct/range {p0 .. p0}, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->checkMethod4DataSpec()Z

    move-result v18

    if-eqz v18, :cond_18

    .line 422
    const/16 v18, 0x0

    aget-object v18, p1, v18

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "OK_"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->responseString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    goto/16 :goto_2

    .line 424
    :cond_18
    const/16 v18, 0x0

    aget-object v18, p1, v18

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "NG_"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->responseString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    goto/16 :goto_2

    .line 427
    .end local v17    # "values":Ljava/lang/String;
    :cond_19
    const/16 v18, 0x3

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    const-string v20, "1"

    aput-object v20, v18, v19

    const/16 v19, 0x1

    const-string v20, "4"

    aput-object v20, v18, v19

    const/16 v19, 0x2

    const-string v20, "0"

    aput-object v20, v18, v19

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_21

    .line 428
    const/4 v10, 0x0

    .line 429
    .local v10, "mNormalData":[Ljava/lang/String;
    const/4 v12, 0x0

    .line 430
    .local v12, "mSNRData":[Ljava/lang/String;
    const-string v14, ""

    .line 431
    .local v14, "mTempData":Ljava/lang/String;
    const-string v17, ""

    .line 432
    .restart local v17    # "values":Ljava/lang/String;
    const-string v11, "0,0,0,0,0,0,0_0,0,0,0,0,0,0"

    .line 433
    .local v11, "mNormalvalues":Ljava/lang/String;
    const-string v13, "0,0,0,0_0,0,0,0"

    .line 434
    .local v13, "mSNRvalues":Ljava/lang/String;
    const/4 v8, 0x0

    .line 435
    .local v8, "isNormalDataResult":Z
    const/4 v9, 0x0

    .line 437
    .local v9, "isSNRDataResult":Z
    new-instance v4, Ljava/io/File;

    const-string v18, "/data/log/FingerNormalData.log"

    move-object/from16 v0, v18

    invoke-direct {v4, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 438
    .local v4, "dataNormalFile":Ljava/io/File;
    new-instance v5, Ljava/io/File;

    const-string v18, "/data/log/FingerSNRData.log"

    move-object/from16 v0, v18

    invoke-direct {v5, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 440
    .local v5, "dataSNRFile":Ljava/io/File;
    if-eqz v4, :cond_1c

    .line 441
    invoke-virtual {v4}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v18

    const/16 v19, 0x1

    invoke-static/range {v18 .. v19}, Lcom/sec/factory/support/Support$Kernel;->readFromPath(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v14

    .line 442
    if-eqz v14, :cond_1c

    .line 443
    invoke-virtual {v14}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v18

    const-string v19, ","

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v10

    .line 444
    const-string v11, ""

    .line 445
    const/16 v18, 0x0

    aget-object v18, v10, v18

    const-string v19, "PASS"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    .line 446
    const/4 v7, 0x1

    .local v7, "i":I
    :goto_a
    array-length v0, v10

    move/from16 v18, v0

    move/from16 v0, v18

    if-ge v7, v0, :cond_1c

    .line 447
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v18

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    aget-object v19, v10, v7

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 448
    array-length v0, v10

    move/from16 v18, v0

    div-int/lit8 v18, v18, 0x2

    move/from16 v0, v18

    if-ne v7, v0, :cond_1b

    .line 449
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v18

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "_"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 446
    :cond_1a
    :goto_b
    add-int/lit8 v7, v7, 0x1

    goto :goto_a

    .line 450
    :cond_1b
    array-length v0, v10

    move/from16 v18, v0

    add-int/lit8 v18, v18, -0x1

    move/from16 v0, v18

    if-eq v7, v0, :cond_1a

    .line 451
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v18

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ","

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    goto :goto_b

    .line 457
    .end local v7    # "i":I
    :cond_1c
    if-eqz v5, :cond_1f

    .line 458
    invoke-virtual {v5}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v18

    const/16 v19, 0x1

    invoke-static/range {v18 .. v19}, Lcom/sec/factory/support/Support$Kernel;->readFromPath(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v14

    .line 459
    if-eqz v14, :cond_1f

    .line 460
    invoke-virtual {v14}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v18

    const-string v19, ","

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v12

    .line 461
    const-string v13, ""

    .line 462
    const/16 v18, 0x0

    aget-object v18, v12, v18

    const-string v19, "PASS"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    .line 463
    const/4 v7, 0x1

    .restart local v7    # "i":I
    :goto_c
    array-length v0, v12

    move/from16 v18, v0

    move/from16 v0, v18

    if-ge v7, v0, :cond_1f

    .line 464
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v18

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    aget-object v19, v12, v7

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    .line 465
    array-length v0, v12

    move/from16 v18, v0

    div-int/lit8 v18, v18, 0x2

    move/from16 v0, v18

    if-ne v7, v0, :cond_1e

    .line 466
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v18

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "_"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    .line 463
    :cond_1d
    :goto_d
    add-int/lit8 v7, v7, 0x1

    goto :goto_c

    .line 467
    :cond_1e
    array-length v0, v12

    move/from16 v18, v0

    add-int/lit8 v18, v18, -0x1

    move/from16 v0, v18

    if-eq v7, v0, :cond_1d

    .line 468
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v18

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ","

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    goto :goto_d

    .line 474
    .end local v7    # "i":I
    :cond_1f
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v18

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "_"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    .line 476
    if-eqz v8, :cond_20

    if-eqz v9, :cond_20

    .line 477
    const/16 v18, 0x0

    aget-object v18, p1, v18

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "OK_"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->responseString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    goto/16 :goto_2

    .line 479
    :cond_20
    const/16 v18, 0x0

    aget-object v18, p1, v18

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "NG_"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->responseString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    goto/16 :goto_2

    .line 483
    .end local v4    # "dataNormalFile":Ljava/io/File;
    .end local v5    # "dataSNRFile":Ljava/io/File;
    .end local v8    # "isNormalDataResult":Z
    .end local v9    # "isSNRDataResult":Z
    .end local v10    # "mNormalData":[Ljava/lang/String;
    .end local v11    # "mNormalvalues":Ljava/lang/String;
    .end local v12    # "mSNRData":[Ljava/lang/String;
    .end local v13    # "mSNRvalues":Ljava/lang/String;
    .end local v14    # "mTempData":Ljava/lang/String;
    .end local v17    # "values":Ljava/lang/String;
    :cond_21
    invoke-virtual/range {p0 .. p0}, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->responseNA()Ljava/lang/String;
    :try_end_1d
    .catchall {:try_start_1d .. :try_end_1d} :catchall_1

    move-result-object v15

    goto/16 :goto_2

    .line 491
    :catch_1
    move-exception v18

    goto/16 :goto_3

    .line 400
    :catch_2
    move-exception v18

    goto/16 :goto_9

    .line 358
    :catch_3
    move-exception v18

    goto/16 :goto_8

    .line 321
    :catch_4
    move-exception v18

    goto/16 :goto_7

    .line 280
    :catch_5
    move-exception v18

    goto/16 :goto_5
.end method

.method public onEvent(Lcom/validity/fingerprint/FingerprintEvent;)V
    .locals 8
    .param p1, "event"    # Lcom/validity/fingerprint/FingerprintEvent;

    .prologue
    const/4 v7, 0x0

    const/16 v6, 0x3f1

    const/16 v5, 0x3ef

    const/4 v4, 0x1

    .line 501
    iget v1, p1, Lcom/validity/fingerprint/FingerprintEvent;->eventId:I

    sparse-switch v1, :sswitch_data_0

    .line 572
    :goto_0
    return-void

    .line 504
    :sswitch_0
    iget-object v1, p1, Lcom/validity/fingerprint/FingerprintEvent;->eventData:Ljava/lang/Object;

    check-cast v1, Lcom/validity/fingerprint/SensorInfo;

    iput-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mSensorInfo:Lcom/validity/fingerprint/SensorInfo;

    .line 505
    iget-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mSensorInfo:Lcom/validity/fingerprint/SensorInfo;

    iget v1, v1, Lcom/validity/fingerprint/SensorInfo;->flexId:I

    const/16 v2, 0x7f9

    if-ne v1, v2, :cond_0

    .line 506
    const-string v1, "60"

    iput-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mVersion:Ljava/lang/String;

    .line 514
    :goto_1
    iget-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mSensorInfo:Lcom/validity/fingerprint/SensorInfo;

    iget-object v1, v1, Lcom/validity/fingerprint/SensorInfo;->fwVersion:Ljava/lang/String;

    const-string v2, "04.52"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 515
    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mFwVersion:I

    .line 519
    :goto_2
    iget-object v2, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->token:Ljava/lang/Object;

    monitor-enter v2

    .line 520
    :try_start_0
    iget-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->token:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->notify()V

    .line 521
    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 507
    :cond_0
    iget-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mSensorInfo:Lcom/validity/fingerprint/SensorInfo;

    iget v1, v1, Lcom/validity/fingerprint/SensorInfo;->flexId:I

    const/16 v2, 0x52

    if-ne v1, v2, :cond_1

    .line 508
    const-string v1, "65"

    iput-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mVersion:Ljava/lang/String;

    goto :goto_1

    .line 509
    :cond_1
    iget-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mSensorInfo:Lcom/validity/fingerprint/SensorInfo;

    iget v1, v1, Lcom/validity/fingerprint/SensorInfo;->flexId:I

    const/16 v2, 0x59

    if-ne v1, v2, :cond_2

    .line 510
    const-string v1, "68A"

    iput-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mVersion:Ljava/lang/String;

    goto :goto_1

    .line 512
    :cond_2
    iget-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mSensorInfo:Lcom/validity/fingerprint/SensorInfo;

    iget v1, v1, Lcom/validity/fingerprint/SensorInfo;->flexId:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mVersion:Ljava/lang/String;

    goto :goto_1

    .line 517
    :cond_3
    iput v4, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mFwVersion:I

    goto :goto_2

    .line 524
    :sswitch_1
    iget-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "onEvent"

    const-string v3, "Event is : VCS_EVT_SNSR_TEST_PUT_TERM_BLOCK_ON_SENSOR"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 525
    invoke-direct {p0}, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->createPopup()V

    goto :goto_0

    .line 528
    :sswitch_2
    iget-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "onEvent"

    const-string v3, "Event is : VCS_EVT_SNSR_TEST_PUT_STON_SSS_ON_SENSOR"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 529
    invoke-direct {p0}, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->createPopup()V

    goto :goto_0

    .line 532
    :sswitch_3
    iget-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "onEvent"

    const-string v3, "Sensor status : VCS_EVT_SNSR_TEST_REMOVE_TERM_BLOCK_FROM_SENSOR"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 533
    iput v6, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mCurrentOperation:I

    .line 534
    iget-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mFingerprint:Lcom/validity/fingerprint/Fingerprint;

    invoke-virtual {v1, v4, v7}, Lcom/validity/fingerprint/Fingerprint;->notify(ILjava/lang/Object;)I

    move-result v0

    .line 535
    .local v0, "result":I
    goto/16 :goto_0

    .line 537
    .end local v0    # "result":I
    :sswitch_4
    iget-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "onEvent"

    const-string v3, "Event is : VCS_EVT_SNSR_TEST_REMOVE_STON_SSS_FROM_SENSOR"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 538
    iput v5, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mCurrentOperation:I

    .line 539
    iget-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mFingerprint:Lcom/validity/fingerprint/Fingerprint;

    invoke-virtual {v1, v4, v7}, Lcom/validity/fingerprint/Fingerprint;->notify(ILjava/lang/Object;)I

    move-result v0

    .line 540
    .restart local v0    # "result":I
    goto/16 :goto_0

    .line 542
    .end local v0    # "result":I
    :sswitch_5
    iget v1, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mCurrentOperation:I

    const/16 v2, 0x3e9

    if-ne v1, v2, :cond_5

    .line 543
    invoke-direct {p0}, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->readNormalScanLogFile()V

    .line 544
    iget-object v2, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->token:Ljava/lang/Object;

    monitor-enter v2

    .line 545
    :try_start_1
    iget-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->token:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->notify()V

    .line 546
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 567
    :cond_4
    :goto_3
    iget-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "onEvent"

    const-string v3, "Event is : VCS_EVT_SNSR_TEST_SCRIPT_END"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 546
    :catchall_1
    move-exception v1

    :try_start_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v1

    .line 547
    :cond_5
    iget v1, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mCurrentOperation:I

    const/16 v2, 0x3ec

    if-ne v1, v2, :cond_6

    .line 548
    invoke-direct {p0}, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->readMRMLogFile()V

    .line 549
    iget-object v2, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->token:Ljava/lang/Object;

    monitor-enter v2

    .line 550
    :try_start_3
    iget-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->token:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->notify()V

    .line 551
    monitor-exit v2

    goto :goto_3

    :catchall_2
    move-exception v1

    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    throw v1

    .line 552
    :cond_6
    iget v1, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mCurrentOperation:I

    const/16 v2, 0x3eb

    if-ne v1, v2, :cond_7

    .line 553
    iget-object v2, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->token:Ljava/lang/Object;

    monitor-enter v2

    .line 554
    :try_start_4
    iget-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->token:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->notify()V

    .line 555
    monitor-exit v2

    goto :goto_3

    :catchall_3
    move-exception v1

    monitor-exit v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    throw v1

    .line 556
    :cond_7
    iget v1, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mCurrentOperation:I

    if-ne v1, v5, :cond_8

    .line 557
    invoke-direct {p0}, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->readMethod3LogFile()V

    .line 558
    iget-object v2, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->token:Ljava/lang/Object;

    monitor-enter v2

    .line 559
    :try_start_5
    iget-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->token:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->notify()V

    .line 560
    monitor-exit v2

    goto :goto_3

    :catchall_4
    move-exception v1

    monitor-exit v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_4

    throw v1

    .line 561
    :cond_8
    iget v1, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mCurrentOperation:I

    if-ne v1, v6, :cond_4

    .line 562
    invoke-direct {p0}, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->readMethod4LogFile()V

    .line 563
    iget-object v2, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->token:Ljava/lang/Object;

    monitor-enter v2

    .line 564
    :try_start_6
    iget-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->token:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->notify()V

    .line 565
    monitor-exit v2

    goto :goto_3

    :catchall_5
    move-exception v1

    monitor-exit v2
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_5

    throw v1

    .line 501
    nop

    :sswitch_data_0
    .sparse-switch
        0x1ac -> :sswitch_0
        0x7d4 -> :sswitch_5
        0x7e7 -> :sswitch_1
        0x7e8 -> :sswitch_3
        0x7ec -> :sswitch_2
        0x7ed -> :sswitch_4
    .end sparse-switch
.end method
