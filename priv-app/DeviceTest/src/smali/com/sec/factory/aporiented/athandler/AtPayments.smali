.class public Lcom/sec/factory/aporiented/athandler/AtPayments;
.super Lcom/sec/factory/aporiented/athandler/AtCommandHandler;
.source "AtPayments.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 14
    invoke-direct {p0, p1}, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;-><init>(Landroid/content/Context;)V

    .line 15
    const-string v0, "PAYMENTS"

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtPayments;->CMD_NAME:Ljava/lang/String;

    .line 16
    const-string v0, "AtPayments"

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtPayments;->CLASS_NAME:Ljava/lang/String;

    .line 17
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/factory/aporiented/athandler/AtPayments;->NUM_ARGS:I

    .line 18
    new-instance v7, Ljava/io/File;

    const-string v0, "PRE_PAY"

    invoke-static {v0}, Lcom/sec/factory/support/Support$Kernel;->getFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v7, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 20
    .local v7, "mPrePay":Ljava/io/File;
    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_0

    .line 21
    monitor-enter p0

    .line 22
    :try_start_0
    sget-object v0, Lcom/sec/factory/aporiented/athandler/AtPayments;->mModuleCommon:Lcom/sec/factory/modules/ModuleCommon;

    const-string v1, "false"

    invoke-virtual {v0, v1}, Lcom/sec/factory/modules/ModuleCommon;->writePrePay(Ljava/lang/String;)V

    .line 23
    const-string v0, "PRE_PAY"

    invoke-static {v0}, Lcom/sec/factory/support/Support$Kernel;->getFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x1

    const/4 v3, 0x1

    const/4 v4, 0x1

    const/4 v5, 0x1

    const/4 v6, 0x0

    invoke-static/range {v0 .. v6}, Lcom/sec/factory/support/Support$Kernel;->setPermission(Ljava/lang/String;ZZZZZZ)Z

    .line 25
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 26
    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtPayments;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "AtPayments"

    const-string v2, "PRE_PAY is created..."

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 30
    :goto_0
    return-void

    .line 25
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 28
    :cond_0
    iget-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtPayments;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "AtPayments"

    const-string v2, "PRE_PAYis already existed..."

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public declared-synchronized handleCommand([Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1, "argu"    # [Ljava/lang/String;

    .prologue
    .line 34
    monitor-enter p0

    const/4 v1, 0x0

    .line 36
    .local v1, "resData":Ljava/lang/String;
    :try_start_0
    array-length v4, p1

    sget-boolean v3, Lcom/sec/factory/aporiented/athandler/AtPayments;->STAGE_PARALLEL:Z

    if-eqz v3, :cond_0

    iget v3, p0, Lcom/sec/factory/aporiented/athandler/AtPayments;->NUM_ARGS:I

    add-int/lit8 v3, v3, 0x1

    :goto_0
    if-eq v4, v3, :cond_1

    .line 37
    invoke-virtual {p0}, Lcom/sec/factory/aporiented/athandler/AtPayments;->responseNA()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    .line 93
    :goto_1
    monitor-exit p0

    return-object v3

    .line 36
    :cond_0
    :try_start_1
    iget v3, p0, Lcom/sec/factory/aporiented/athandler/AtPayments;->NUM_ARGS:I

    goto :goto_0

    .line 41
    :cond_1
    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "0"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "0"

    aput-object v5, v3, v4

    invoke-virtual {p0, p1, v3}, Lcom/sec/factory/aporiented/athandler/AtPayments;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v3

    if-eqz v3, :cond_3

    .line 43
    const/4 v2, 0x0

    .line 44
    .local v2, "sResult":Ljava/lang/String;
    :try_start_2
    sget-object v3, Lcom/sec/factory/aporiented/athandler/AtPayments;->mModuleCommon:Lcom/sec/factory/modules/ModuleCommon;

    const-string v4, "false"

    invoke-virtual {v3, v4}, Lcom/sec/factory/modules/ModuleCommon;->writePrePay(Ljava/lang/String;)V

    .line 45
    sget-object v3, Lcom/sec/factory/aporiented/athandler/AtPayments;->mModuleCommon:Lcom/sec/factory/modules/ModuleCommon;

    invoke-virtual {v3}, Lcom/sec/factory/modules/ModuleCommon;->readPrePay()Ljava/lang/String;

    move-result-object v2

    .line 47
    const-string v3, "false"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 48
    const/4 v3, 0x0

    aget-object v3, p1, v3

    invoke-virtual {p0, v3}, Lcom/sec/factory/aporiented/athandler/AtPayments;->responseOK(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .end local v2    # "sResult":Ljava/lang/String;
    :goto_2
    move-object v3, v1

    .line 93
    goto :goto_1

    .line 50
    .restart local v2    # "sResult":Ljava/lang/String;
    :cond_2
    const/4 v3, 0x0

    aget-object v3, p1, v3

    invoke-virtual {p0, v3}, Lcom/sec/factory/aporiented/athandler/AtPayments;->responseNG(Ljava/lang/String;)Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v1

    goto :goto_2

    .line 52
    :catch_0
    move-exception v0

    .line 53
    .local v0, "e":Ljava/lang/Exception;
    const/4 v3, 0x0

    :try_start_3
    aget-object v3, p1, v3

    invoke-virtual {p0, v3}, Lcom/sec/factory/aporiented/athandler/AtPayments;->responseNG(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 54
    goto :goto_2

    .line 57
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v2    # "sResult":Ljava/lang/String;
    :cond_3
    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "0"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "1"

    aput-object v5, v3, v4

    invoke-virtual {p0, p1, v3}, Lcom/sec/factory/aporiented/athandler/AtPayments;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result v3

    if-eqz v3, :cond_5

    .line 59
    const/4 v2, 0x0

    .line 60
    .restart local v2    # "sResult":Ljava/lang/String;
    :try_start_4
    sget-object v3, Lcom/sec/factory/aporiented/athandler/AtPayments;->mModuleCommon:Lcom/sec/factory/modules/ModuleCommon;

    const-string v4, "true"

    invoke-virtual {v3, v4}, Lcom/sec/factory/modules/ModuleCommon;->writePrePay(Ljava/lang/String;)V

    .line 61
    sget-object v3, Lcom/sec/factory/aporiented/athandler/AtPayments;->mModuleCommon:Lcom/sec/factory/modules/ModuleCommon;

    invoke-virtual {v3}, Lcom/sec/factory/modules/ModuleCommon;->readPrePay()Ljava/lang/String;

    move-result-object v2

    .line 63
    const-string v3, "true"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 64
    const/4 v3, 0x0

    aget-object v3, p1, v3

    invoke-virtual {p0, v3}, Lcom/sec/factory/aporiented/athandler/AtPayments;->responseOK(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    .line 66
    :cond_4
    const/4 v3, 0x0

    aget-object v3, p1, v3

    invoke-virtual {p0, v3}, Lcom/sec/factory/aporiented/athandler/AtPayments;->responseNG(Ljava/lang/String;)Ljava/lang/String;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result-object v1

    goto :goto_2

    .line 68
    :catch_1
    move-exception v0

    .line 69
    .restart local v0    # "e":Ljava/lang/Exception;
    const/4 v3, 0x0

    :try_start_5
    aget-object v3, p1, v3

    invoke-virtual {p0, v3}, Lcom/sec/factory/aporiented/athandler/AtPayments;->responseNG(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 70
    goto :goto_2

    .line 73
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v2    # "sResult":Ljava/lang/String;
    :cond_5
    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "1"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "0"

    aput-object v5, v3, v4

    invoke-virtual {p0, p1, v3}, Lcom/sec/factory/aporiented/athandler/AtPayments;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-result v3

    if-eqz v3, :cond_8

    .line 75
    const/4 v2, 0x0

    .line 76
    .restart local v2    # "sResult":Ljava/lang/String;
    :try_start_6
    sget-object v3, Lcom/sec/factory/aporiented/athandler/AtPayments;->mModuleCommon:Lcom/sec/factory/modules/ModuleCommon;

    invoke-virtual {v3}, Lcom/sec/factory/modules/ModuleCommon;->readPrePay()Ljava/lang/String;

    move-result-object v2

    .line 78
    const-string v3, "false"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 79
    const/4 v3, 0x0

    aget-object v3, p1, v3

    const-string v4, "POSTPAY"

    invoke-virtual {p0, v3, v4}, Lcom/sec/factory/aporiented/athandler/AtPayments;->responseString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    .line 80
    :cond_6
    const-string v3, "true"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 81
    const/4 v3, 0x0

    aget-object v3, p1, v3

    const-string v4, "PREPAY"

    invoke-virtual {p0, v3, v4}, Lcom/sec/factory/aporiented/athandler/AtPayments;->responseString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_2

    .line 83
    :cond_7
    const/4 v3, 0x0

    aget-object v3, p1, v3

    invoke-virtual {p0, v3}, Lcom/sec/factory/aporiented/athandler/AtPayments;->responseNG(Ljava/lang/String;)Ljava/lang/String;
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_2
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    move-result-object v1

    goto/16 :goto_2

    .line 85
    :catch_2
    move-exception v0

    .line 86
    .restart local v0    # "e":Ljava/lang/Exception;
    const/4 v3, 0x0

    :try_start_7
    aget-object v3, p1, v3

    invoke-virtual {p0, v3}, Lcom/sec/factory/aporiented/athandler/AtPayments;->responseNG(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 87
    goto/16 :goto_2

    .line 90
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v2    # "sResult":Ljava/lang/String;
    :cond_8
    invoke-virtual {p0}, Lcom/sec/factory/aporiented/athandler/AtPayments;->responseNA()Ljava/lang/String;
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    move-result-object v1

    goto/16 :goto_2

    .line 34
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3
.end method
