.class Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method3Data;
.super Ljava/lang/Object;
.source "AtFpsensor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/factory/aporiented/athandler/AtFpsensor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Method3Data"
.end annotation


# instance fields
.field private mGain:I

.field private mNNoise:F

.field private mNoise:F

.field private mSNR:F

.field private mSignal:F


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "gain"    # Ljava/lang/String;
    .param p2, "signal"    # Ljava/lang/String;
    .param p3, "noise"    # Ljava/lang/String;
    .param p4, "snr"    # Ljava/lang/String;
    .param p5, "n_noise"    # Ljava/lang/String;

    .prologue
    const/high16 v2, 0x42c80000    # 100.0f

    const/high16 v1, 0x41200000    # 10.0f

    .line 1333
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1334
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method3Data;->mGain:I

    .line 1335
    invoke-static {p3}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v0

    div-float/2addr v0, v2

    iput v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method3Data;->mNoise:F

    .line 1336
    invoke-static {p2}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v0

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method3Data;->mSignal:F

    .line 1337
    invoke-static {p4}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v0

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method3Data;->mSNR:F

    .line 1338
    invoke-static {p5}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v0

    div-float/2addr v0, v2

    iput v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method3Data;->mNNoise:F

    .line 1339
    return-void
.end method

.method private round(FI)F
    .locals 6
    .param p1, "t"    # F
    .param p2, "decimal"    # I

    .prologue
    const-wide/high16 v4, 0x4024000000000000L    # 10.0

    .line 1342
    float-to-double v0, p1

    int-to-double v2, p2

    invoke-static {v4, v5, v2, v3}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    mul-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    long-to-float v0, v0

    int-to-double v2, p2

    invoke-static {v4, v5, v2, v3}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    double-to-float v1, v2

    div-float/2addr v0, v1

    return v0
.end method


# virtual methods
.method public getGain()I
    .locals 1

    .prologue
    .line 1344
    iget v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method3Data;->mGain:I

    return v0
.end method

.method public getNNoise()F
    .locals 2

    .prologue
    .line 1348
    iget v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method3Data;->mNNoise:F

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method3Data;->round(FI)F

    move-result v0

    return v0
.end method

.method public getNoise()F
    .locals 2

    .prologue
    .line 1345
    iget v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method3Data;->mNoise:F

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method3Data;->round(FI)F

    move-result v0

    return v0
.end method

.method public getSNR()F
    .locals 2

    .prologue
    .line 1347
    iget v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method3Data;->mSNR:F

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method3Data;->round(FI)F

    move-result v0

    return v0
.end method

.method public getSignal()F
    .locals 2

    .prologue
    .line 1346
    iget v0, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method3Data;->mSignal:F

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/sec/factory/aporiented/athandler/AtFpsensor$Method3Data;->round(FI)F

    move-result v0

    return v0
.end method
