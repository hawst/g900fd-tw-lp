.class public Lcom/sec/factory/aporiented/athandler/AtHDCPtest;
.super Lcom/sec/factory/aporiented/athandler/AtCommandHandler;
.source "AtHDCPtest.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 12
    invoke-direct {p0, p1}, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;-><init>(Landroid/content/Context;)V

    .line 13
    const-string v0, "HDCPTEST"

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtHDCPtest;->CMD_NAME:Ljava/lang/String;

    .line 14
    const-string v0, "AtHDCPtest"

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtHDCPtest;->CLASS_NAME:Ljava/lang/String;

    .line 15
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/factory/aporiented/athandler/AtHDCPtest;->NUM_ARGS:I

    .line 16
    return-void
.end method


# virtual methods
.method public declared-synchronized handleCommand([Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p1, "argu"    # [Ljava/lang/String;

    .prologue
    .line 21
    monitor-enter p0

    const/4 v0, 0x0

    .line 22
    .local v0, "resData":Ljava/lang/String;
    :try_start_0
    const-string v1, ""

    .line 24
    .local v1, "result":Ljava/lang/String;
    array-length v4, p1

    sget-boolean v3, Lcom/sec/factory/aporiented/athandler/AtHDCPtest;->STAGE_PARALLEL:Z

    if-eqz v3, :cond_0

    iget v3, p0, Lcom/sec/factory/aporiented/athandler/AtHDCPtest;->NUM_ARGS:I

    add-int/lit8 v3, v3, 0x1

    :goto_0
    if-eq v4, v3, :cond_1

    .line 25
    invoke-virtual {p0}, Lcom/sec/factory/aporiented/athandler/AtHDCPtest;->responseNA()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    .line 111
    :goto_1
    monitor-exit p0

    return-object v3

    .line 24
    :cond_0
    :try_start_1
    iget v3, p0, Lcom/sec/factory/aporiented/athandler/AtHDCPtest;->NUM_ARGS:I

    goto :goto_0

    .line 28
    :cond_1
    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "0"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "0"

    aput-object v5, v3, v4

    invoke-virtual {p0, p1, v3}, Lcom/sec/factory/aporiented/athandler/AtHDCPtest;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 30
    sget-object v3, Lcom/sec/factory/aporiented/athandler/AtHDCPtest;->mModuleCommon:Lcom/sec/factory/modules/ModuleCommon;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/sec/factory/modules/ModuleCommon;->hdcpCheck(I)Ljava/lang/String;

    move-result-object v1

    .line 32
    if-eqz v1, :cond_3

    .line 33
    const-string v3, "OK"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 34
    const/4 v3, 0x0

    aget-object v3, p1, v3

    invoke-virtual {p0, v3}, Lcom/sec/factory/aporiented/athandler/AtHDCPtest;->responseOK(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_2
    move-object v3, v0

    .line 111
    goto :goto_1

    .line 36
    :cond_2
    const/4 v3, 0x0

    aget-object v3, p1, v3

    invoke-virtual {p0, v3}, Lcom/sec/factory/aporiented/athandler/AtHDCPtest;->responseNG(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 39
    :cond_3
    const/4 v3, 0x0

    aget-object v3, p1, v3

    invoke-virtual {p0, v3}, Lcom/sec/factory/aporiented/athandler/AtHDCPtest;->responseNG(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 42
    :cond_4
    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "0"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "1"

    aput-object v5, v3, v4

    invoke-virtual {p0, p1, v3}, Lcom/sec/factory/aporiented/athandler/AtHDCPtest;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 45
    sget-object v3, Lcom/sec/factory/aporiented/athandler/AtHDCPtest;->mModuleCommon:Lcom/sec/factory/modules/ModuleCommon;

    const/4 v4, 0x2

    invoke-virtual {v3, v4}, Lcom/sec/factory/modules/ModuleCommon;->hdcpCheck(I)Ljava/lang/String;

    move-result-object v1

    .line 47
    if-eqz v1, :cond_6

    .line 48
    const-string v3, "OK"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 49
    const/4 v3, 0x0

    aget-object v3, p1, v3

    invoke-virtual {p0, v3}, Lcom/sec/factory/aporiented/athandler/AtHDCPtest;->responseOK(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 51
    :cond_5
    const/4 v3, 0x0

    aget-object v3, p1, v3

    invoke-virtual {p0, v3}, Lcom/sec/factory/aporiented/athandler/AtHDCPtest;->responseNG(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 54
    :cond_6
    const/4 v3, 0x0

    aget-object v3, p1, v3

    invoke-virtual {p0, v3}, Lcom/sec/factory/aporiented/athandler/AtHDCPtest;->responseNG(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 57
    :cond_7
    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "0"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "2"

    aput-object v5, v3, v4

    invoke-virtual {p0, p1, v3}, Lcom/sec/factory/aporiented/athandler/AtHDCPtest;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 61
    sget-object v3, Lcom/sec/factory/aporiented/athandler/AtHDCPtest;->mModuleCommon:Lcom/sec/factory/modules/ModuleCommon;

    const/4 v4, 0x3

    invoke-virtual {v3, v4}, Lcom/sec/factory/modules/ModuleCommon;->hdcpCheck(I)Ljava/lang/String;

    move-result-object v1

    .line 63
    if-eqz v1, :cond_9

    .line 64
    const-string v3, "OK"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 65
    const/4 v3, 0x0

    aget-object v3, p1, v3

    invoke-virtual {p0, v3}, Lcom/sec/factory/aporiented/athandler/AtHDCPtest;->responseOK(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 67
    :cond_8
    const/4 v3, 0x0

    aget-object v3, p1, v3

    invoke-virtual {p0, v3}, Lcom/sec/factory/aporiented/athandler/AtHDCPtest;->responseNG(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 70
    :cond_9
    const/4 v3, 0x0

    aget-object v3, p1, v3

    invoke-virtual {p0, v3}, Lcom/sec/factory/aporiented/athandler/AtHDCPtest;->responseNG(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 73
    :cond_a
    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "0"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "3"

    aput-object v5, v3, v4

    invoke-virtual {p0, p1, v3}, Lcom/sec/factory/aporiented/athandler/AtHDCPtest;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_d

    .line 76
    sget-object v3, Lcom/sec/factory/aporiented/athandler/AtHDCPtest;->mModuleCommon:Lcom/sec/factory/modules/ModuleCommon;

    const/4 v4, 0x4

    invoke-virtual {v3, v4}, Lcom/sec/factory/modules/ModuleCommon;->hdcpCheck(I)Ljava/lang/String;

    move-result-object v1

    .line 79
    if-eqz v1, :cond_c

    .line 80
    const-string v3, "OK"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 81
    const/4 v3, 0x0

    aget-object v3, p1, v3

    invoke-virtual {p0, v3}, Lcom/sec/factory/aporiented/athandler/AtHDCPtest;->responseOK(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 83
    :cond_b
    const/4 v3, 0x0

    aget-object v3, p1, v3

    invoke-virtual {p0, v3}, Lcom/sec/factory/aporiented/athandler/AtHDCPtest;->responseNG(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 86
    :cond_c
    const/4 v3, 0x0

    aget-object v3, p1, v3

    invoke-virtual {p0, v3}, Lcom/sec/factory/aporiented/athandler/AtHDCPtest;->responseNG(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 89
    :cond_d
    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "1"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "0"

    aput-object v5, v3, v4

    invoke-virtual {p0, p1, v3}, Lcom/sec/factory/aporiented/athandler/AtHDCPtest;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_10

    .line 91
    const-string v2, "0000000000000000"

    .line 92
    .local v2, "temp":Ljava/lang/String;
    sget-object v3, Lcom/sec/factory/aporiented/athandler/AtHDCPtest;->mModuleCommon:Lcom/sec/factory/modules/ModuleCommon;

    invoke-virtual {v3}, Lcom/sec/factory/modules/ModuleCommon;->hdcpReadSN()Ljava/lang/String;

    move-result-object v1

    .line 94
    const-string v3, "Unknown"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_e

    .line 95
    const/4 v3, 0x0

    aget-object v3, p1, v3

    invoke-virtual {p0, v3}, Lcom/sec/factory/aporiented/athandler/AtHDCPtest;->responseNG(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 97
    :cond_e
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    if-ge v3, v4, :cond_f

    .line 98
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v6

    sub-int/2addr v5, v6

    invoke-virtual {v2, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 104
    :cond_f
    const/4 v3, 0x0

    aget-object v3, p1, v3

    invoke-virtual {p0, v3, v1}, Lcom/sec/factory/aporiented/athandler/AtHDCPtest;->responseString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 108
    .end local v2    # "temp":Ljava/lang/String;
    :cond_10
    invoke-virtual {p0}, Lcom/sec/factory/aporiented/athandler/AtHDCPtest;->responseNA()Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto/16 :goto_2

    .line 21
    .end local v1    # "result":Ljava/lang/String;
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3
.end method
