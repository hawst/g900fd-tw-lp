.class Lcom/sec/factory/aporiented/IPCWriterToSecPhoneService$2;
.super Landroid/os/Handler;
.source "IPCWriterToSecPhoneService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/factory/aporiented/IPCWriterToSecPhoneService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/factory/aporiented/IPCWriterToSecPhoneService;


# direct methods
.method constructor <init>(Lcom/sec/factory/aporiented/IPCWriterToSecPhoneService;)V
    .locals 0

    .prologue
    .line 164
    iput-object p1, p0, Lcom/sec/factory/aporiented/IPCWriterToSecPhoneService$2;->this$0:Lcom/sec/factory/aporiented/IPCWriterToSecPhoneService;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 166
    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    .line 188
    :goto_0
    return-void

    .line 168
    :sswitch_0
    const-string v0, "IPCWriterToSecPhoneService"

    const-string v1, "handleMessage"

    const-string v2, "DFMS Confirm received"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 171
    :sswitch_1
    const-string v0, "IPCWriterToSecPhoneService"

    const-string v1, "handleMessage"

    const-string v2, "DFMS Event received"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 174
    :sswitch_2
    const-string v0, "IPCWriterToSecPhoneService"

    const-string v1, "handleMessage"

    const-string v2, "DFT Confirm received"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 177
    :sswitch_3
    const-string v0, "IPCWriterToSecPhoneService"

    const-string v1, "handleMessage"

    const-string v2, "DFT Event received"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 180
    :sswitch_4
    const-string v0, "IPCWriterToSecPhoneService"

    const-string v1, "handleMessage"

    const-string v2, "CP Sysdump done"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 183
    :sswitch_5
    const-string v0, "IPCWriterToSecPhoneService"

    const-string v1, "handleMessage"

    const-string v2, "Send BOOT COMPLETED done"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 166
    :sswitch_data_0
    .sparse-switch
        0x64 -> :sswitch_0
        0x65 -> :sswitch_1
        0x66 -> :sswitch_2
        0x67 -> :sswitch_3
        0x68 -> :sswitch_4
        0x3e8 -> :sswitch_5
    .end sparse-switch
.end method
