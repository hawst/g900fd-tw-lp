.class public Lcom/sec/factory/aporiented/athandler/AtBtidtest;
.super Lcom/sec/factory/aporiented/athandler/AtCommandHandler;
.source "AtBtidtest.java"


# static fields
.field static final SYS_BLTH_ADDR:Ljava/lang/String; = "ril.bt_macaddr"

.field private static final TAG:Ljava/lang/String; = "AtBtidtest"

.field private static subString:Ljava/lang/String;


# instance fields
.field public mReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/factory/aporiented/athandler/AtBtidtest;->subString:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/factory/aporiented/ResponseWriter;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "writer"    # Lcom/sec/factory/aporiented/ResponseWriter;

    .prologue
    .line 19
    invoke-direct {p0, p1}, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;-><init>(Landroid/content/Context;)V

    .line 52
    new-instance v0, Lcom/sec/factory/aporiented/athandler/AtBtidtest$1;

    invoke-direct {v0, p0}, Lcom/sec/factory/aporiented/athandler/AtBtidtest$1;-><init>(Lcom/sec/factory/aporiented/athandler/AtBtidtest;)V

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtBtidtest;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 20
    const-string v0, "BTIDTEST"

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtBtidtest;->CMD_NAME:Ljava/lang/String;

    .line 21
    const-string v0, "AtBtidtest"

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtBtidtest;->CLASS_NAME:Ljava/lang/String;

    .line 22
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/factory/aporiented/athandler/AtBtidtest;->NUM_ARGS:I

    .line 23
    iput-object p2, p0, Lcom/sec/factory/aporiented/athandler/AtBtidtest;->writer:Lcom/sec/factory/aporiented/ResponseWriter;

    .line 24
    const-string v0, "boot"

    sput-object v0, Lcom/sec/factory/aporiented/athandler/AtBtidtest;->subString:Ljava/lang/String;

    .line 29
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/factory/cporiented/ResponseWriterCPO;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "writer"    # Lcom/sec/factory/cporiented/ResponseWriterCPO;

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;-><init>(Landroid/content/Context;)V

    .line 52
    new-instance v0, Lcom/sec/factory/aporiented/athandler/AtBtidtest$1;

    invoke-direct {v0, p0}, Lcom/sec/factory/aporiented/athandler/AtBtidtest$1;-><init>(Lcom/sec/factory/aporiented/athandler/AtBtidtest;)V

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtBtidtest;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 33
    const-string v0, "BTIDTEST"

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtBtidtest;->CMD_NAME:Ljava/lang/String;

    .line 34
    const-string v0, "AtBtidtest"

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtBtidtest;->CLASS_NAME:Ljava/lang/String;

    .line 35
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/factory/aporiented/athandler/AtBtidtest;->NUM_ARGS:I

    .line 36
    iput-object p2, p0, Lcom/sec/factory/aporiented/athandler/AtBtidtest;->writerCpo:Lcom/sec/factory/cporiented/ResponseWriterCPO;

    .line 42
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 14
    sget-object v0, Lcom/sec/factory/aporiented/athandler/AtBtidtest;->subString:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public declared-synchronized handleCommand([Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "argu"    # [Ljava/lang/String;

    .prologue
    .line 110
    monitor-enter p0

    const/4 v0, 0x0

    .line 112
    .local v0, "resData":Ljava/lang/String;
    :try_start_0
    array-length v2, p1

    sget-boolean v1, Lcom/sec/factory/aporiented/athandler/AtBtidtest;->STAGE_PARALLEL:Z

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/sec/factory/aporiented/athandler/AtBtidtest;->NUM_ARGS:I

    add-int/lit8 v1, v1, 0x1

    :goto_0
    if-eq v2, v1, :cond_1

    .line 113
    invoke-virtual {p0}, Lcom/sec/factory/aporiented/athandler/AtBtidtest;->responseNA()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 144
    :goto_1
    monitor-exit p0

    return-object v1

    .line 112
    :cond_0
    :try_start_1
    iget v1, p0, Lcom/sec/factory/aporiented/athandler/AtBtidtest;->NUM_ARGS:I

    goto :goto_0

    .line 116
    :cond_1
    const/4 v1, 0x0

    aget-object v1, p1, v1

    sput-object v1, Lcom/sec/factory/aporiented/athandler/AtBtidtest;->subString:Ljava/lang/String;

    .line 118
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "1"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "0"

    aput-object v3, v1, v2

    invoke-virtual {p0, p1, v1}, Lcom/sec/factory/aporiented/athandler/AtBtidtest;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {p0}, Lcom/sec/factory/aporiented/athandler/AtBtidtest;->getCmdType()I

    move-result v1

    if-nez v1, :cond_4

    const-string v1, "1"

    const/4 v2, 0x0

    aget-object v2, p1, v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 121
    :cond_2
    invoke-virtual {p0}, Lcom/sec/factory/aporiented/athandler/AtBtidtest;->startReceiver()V

    .line 122
    sget-object v1, Lcom/sec/factory/aporiented/athandler/AtBtidtest;->mModuleCommunication:Lcom/sec/factory/modules/ModuleCommunication;

    invoke-virtual {v1}, Lcom/sec/factory/modules/ModuleCommunication;->readBtId()V

    .line 124
    invoke-virtual {p0}, Lcom/sec/factory/aporiented/athandler/AtBtidtest;->getCmdType()I

    move-result v1

    if-nez v1, :cond_3

    .line 125
    const-string v0, "WAIT"

    :cond_3
    :goto_2
    move-object v1, v0

    .line 144
    goto :goto_1

    .line 128
    :cond_4
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "2"

    aput-object v3, v1, v2

    invoke-virtual {p0, p1, v1}, Lcom/sec/factory/aporiented/athandler/AtBtidtest;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 129
    const/4 v1, 0x1

    aget-object v1, p1, v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    const/16 v2, 0xc

    if-eq v1, v2, :cond_5

    .line 130
    const/4 v1, 0x0

    aget-object v1, p1, v1

    invoke-virtual {p0, v1}, Lcom/sec/factory/aporiented/athandler/AtBtidtest;->responseNG(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 132
    :cond_5
    invoke-virtual {p0}, Lcom/sec/factory/aporiented/athandler/AtBtidtest;->startReceiver()V

    .line 133
    sget-object v1, Lcom/sec/factory/aporiented/athandler/AtBtidtest;->mModuleCommunication:Lcom/sec/factory/modules/ModuleCommunication;

    const/4 v2, 0x1

    aget-object v2, p1, v2

    invoke-virtual {v1, v2}, Lcom/sec/factory/modules/ModuleCommunication;->writeBtId(Ljava/lang/String;)V

    .line 135
    invoke-virtual {p0}, Lcom/sec/factory/aporiented/athandler/AtBtidtest;->getCmdType()I

    move-result v1

    if-nez v1, :cond_3

    .line 136
    const-string v0, "WAIT"

    goto :goto_2

    .line 141
    :cond_6
    invoke-virtual {p0}, Lcom/sec/factory/aporiented/athandler/AtBtidtest;->responseNA()Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_2

    .line 110
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public startReceiver()V
    .locals 3

    .prologue
    .line 47
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 48
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "com.sec.android.app.bluetoothtest.BT_ID_RESPONSE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 49
    iget-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtBtidtest;->context:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/factory/aporiented/athandler/AtBtidtest;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 50
    return-void
.end method
