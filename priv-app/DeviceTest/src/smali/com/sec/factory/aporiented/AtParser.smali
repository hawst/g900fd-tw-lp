.class public Lcom/sec/factory/aporiented/AtParser;
.super Ljava/lang/Object;
.source "AtParser.java"

# interfaces
.implements Lcom/sec/factory/aporiented/CommandListener;
.implements Lcom/sec/factory/aporiented/ResponseWriter$ResponseListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/factory/aporiented/AtParser$WaitingNodeInfo;
    }
.end annotation


# static fields
.field public static final ACTION_RECEIVED_FROM_RIL:Ljava/lang/String; = "com.sec.factory.RECEIVED_FROM_RIL"

.field public static final CLASS_NAME:Ljava/lang/String; = "AtParser"


# instance fields
.field private final atHandlers:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/factory/aporiented/athandler/AtCommandHandler;",
            ">;"
        }
    .end annotation
.end field

.field private mAtParallel:Lcom/sec/factory/aporiented/athandler/AtParallel;

.field private mContext:Landroid/content/Context;

.field mFilteredcommandList:[Ljava/lang/String;

.field private mProcessThread:[Lcom/sec/factory/aporiented/ProcessThread;

.field private mWaitingList:Ljava/util/concurrent/ConcurrentLinkedQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentLinkedQueue",
            "<",
            "Lcom/sec/factory/aporiented/AtParser$WaitingNodeInfo;",
            ">;"
        }
    .end annotation
.end field

.field mWriter:Lcom/sec/factory/aporiented/ResponseWriter;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    invoke-static {}, Lcom/sec/factory/support/Support$CommandFilter;->getFilteredCommands()[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/factory/aporiented/AtParser;->mFilteredcommandList:[Ljava/lang/String;

    .line 29
    iput-object v1, p0, Lcom/sec/factory/aporiented/AtParser;->mProcessThread:[Lcom/sec/factory/aporiented/ProcessThread;

    .line 30
    iput-object v1, p0, Lcom/sec/factory/aporiented/AtParser;->mAtParallel:Lcom/sec/factory/aporiented/athandler/AtParallel;

    .line 31
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v0, p0, Lcom/sec/factory/aporiented/AtParser;->mWaitingList:Ljava/util/concurrent/ConcurrentLinkedQueue;

    .line 63
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/factory/aporiented/AtParser;->atHandlers:Ljava/util/HashMap;

    .line 64
    return-void
.end method

.method private compareArgu([Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Z
    .locals 8
    .param p1, "argu"    # [Ljava/lang/String;
    .param p2, "para"    # [Ljava/lang/String;
    .param p3, "count"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 365
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    :try_start_0
    invoke-static {p3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    if-ge v1, v4, :cond_1

    .line 366
    const-string v4, "AtParser"

    const-string v5, "compareArgu"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "compareArgu : argu["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    aget-object v7, p1, v1

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", para["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    aget-object v7, p2, v1

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 369
    aget-object v4, p1, v1

    aget-object v5, p2, v1

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    if-eq v4, v3, :cond_0

    .line 378
    :goto_1
    return v2

    .line 365
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 373
    :catch_0
    move-exception v0

    .line 374
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .end local v0    # "e":Ljava/lang/Exception;
    :cond_1
    move v2, v3

    .line 378
    goto :goto_1
.end method

.method private isFilteredCommand(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 12
    .param p1, "fullCMD"    # Ljava/lang/String;
    .param p2, "commandName"    # Ljava/lang/String;

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 339
    invoke-virtual {p0, p1}, Lcom/sec/factory/aporiented/AtParser;->splitArgu(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 340
    .local v0, "argu":[Ljava/lang/String;
    const/4 v6, 0x0

    .line 342
    .local v6, "para":[Ljava/lang/String;
    iget-object v9, p0, Lcom/sec/factory/aporiented/AtParser;->mFilteredcommandList:[Ljava/lang/String;

    if-nez v9, :cond_1

    .line 360
    :cond_0
    :goto_0
    return v7

    .line 346
    :cond_1
    iget-object v1, p0, Lcom/sec/factory/aporiented/AtParser;->mFilteredcommandList:[Ljava/lang/String;

    .local v1, "arr$":[Ljava/lang/String;
    array-length v5, v1

    .local v5, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_1
    if-ge v3, v5, :cond_0

    aget-object v2, v1, v3

    .line 347
    .local v2, "command":Ljava/lang/String;
    const-string v9, "/"

    invoke-virtual {v2, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 349
    .local v4, "item":[Ljava/lang/String;
    aget-object v9, v4, v7

    invoke-virtual {p2, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 350
    aget-object v9, v4, v8

    const-string v10, ","

    invoke-virtual {v9, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    .line 352
    const/4 v9, 0x2

    aget-object v9, v4, v9

    invoke-direct {p0, v0, v6, v9}, Lcom/sec/factory/aporiented/AtParser;->compareArgu([Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 353
    const-string v7, "AtParser"

    const-string v9, "isFilteredCommand"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "---filtered CMD= "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "  , commandName="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v7, v9, v10}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move v7, v8

    .line 355
    goto :goto_0

    .line 346
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1
.end method


# virtual methods
.method public declared-synchronized onFinishAllCommand(Ljava/lang/String;I)Z
    .locals 9
    .param p1, "cmd"    # Ljava/lang/String;
    .param p2, "id"    # I

    .prologue
    .line 166
    monitor-enter p0

    :try_start_0
    const-string v5, "AtParser"

    const-string v6, "onFinishAllCommand"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", ID = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 167
    iget-object v5, p0, Lcom/sec/factory/aporiented/AtParser;->mProcessThread:[Lcom/sec/factory/aporiented/ProcessThread;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/sec/factory/aporiented/AtParser;->mProcessThread:[Lcom/sec/factory/aporiented/ProcessThread;

    aget-object v5, v5, p2

    invoke-virtual {v5}, Lcom/sec/factory/aporiented/ProcessThread;->isAlive()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 168
    iget-object v5, p0, Lcom/sec/factory/aporiented/AtParser;->mProcessThread:[Lcom/sec/factory/aporiented/ProcessThread;

    aget-object v5, v5, p2

    invoke-virtual {v5}, Lcom/sec/factory/aporiented/ProcessThread;->kill()V

    .line 171
    :cond_0
    iget-object v5, p0, Lcom/sec/factory/aporiented/AtParser;->mProcessThread:[Lcom/sec/factory/aporiented/ProcessThread;

    if-eqz v5, :cond_2

    .line 173
    iget-object v0, p0, Lcom/sec/factory/aporiented/AtParser;->mProcessThread:[Lcom/sec/factory/aporiented/ProcessThread;

    .local v0, "arr$":[Lcom/sec/factory/aporiented/ProcessThread;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_2

    aget-object v4, v0, v2

    .line 174
    .local v4, "p":Lcom/sec/factory/aporiented/ProcessThread;
    if-eqz v4, :cond_1

    iget-boolean v5, v4, Lcom/sec/factory/aporiented/ProcessThread;->mIsKilled:Z

    if-nez v5, :cond_1

    .line 175
    const-string v5, "AtParser"

    const-string v6, "onFinishAllCommand"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Working = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v4}, Lcom/sec/factory/aporiented/ProcessThread;->getCategoryName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 176
    const/4 v5, 0x0

    .line 191
    .end local v0    # "arr$":[Lcom/sec/factory/aporiented/ProcessThread;
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    .end local v4    # "p":Lcom/sec/factory/aporiented/ProcessThread;
    :goto_1
    monitor-exit p0

    return v5

    .line 173
    .restart local v0    # "arr$":[Lcom/sec/factory/aporiented/ProcessThread;
    .restart local v2    # "i$":I
    .restart local v3    # "len$":I
    .restart local v4    # "p":Lcom/sec/factory/aporiented/ProcessThread;
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 181
    .end local v0    # "arr$":[Lcom/sec/factory/aporiented/ProcessThread;
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    .end local v4    # "p":Lcom/sec/factory/aporiented/ProcessThread;
    :cond_2
    :try_start_1
    const-string v5, "AtParser"

    const-string v6, "onFinishAllCommand"

    const-string v7, "All Threads are done"

    invoke-static {v5, v6, v7}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 182
    iget-object v5, p0, Lcom/sec/factory/aporiented/AtParser;->mProcessThread:[Lcom/sec/factory/aporiented/ProcessThread;

    if-eqz v5, :cond_3

    .line 183
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_2
    iget-object v5, p0, Lcom/sec/factory/aporiented/AtParser;->mProcessThread:[Lcom/sec/factory/aporiented/ProcessThread;

    array-length v5, v5

    if-ge v1, v5, :cond_3

    .line 184
    iget-object v5, p0, Lcom/sec/factory/aporiented/AtParser;->mProcessThread:[Lcom/sec/factory/aporiented/ProcessThread;

    const/4 v6, 0x0

    aput-object v6, v5, v1

    .line 183
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 188
    .end local v1    # "i":I
    :cond_3
    const/4 v5, 0x0

    iput-object v5, p0, Lcom/sec/factory/aporiented/AtParser;->mProcessThread:[Lcom/sec/factory/aporiented/ProcessThread;

    .line 189
    iget-object v5, p0, Lcom/sec/factory/aporiented/AtParser;->mAtParallel:Lcom/sec/factory/aporiented/athandler/AtParallel;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Lcom/sec/factory/aporiented/athandler/AtParallel;->onCompleted(Ljava/lang/String;)Z

    .line 190
    iget-object v5, p0, Lcom/sec/factory/aporiented/AtParser;->mWriter:Lcom/sec/factory/aporiented/ResponseWriter;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Lcom/sec/factory/aporiented/ResponseWriter;->setResponseListener(Lcom/sec/factory/aporiented/ResponseWriter$ResponseListener;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 191
    const/4 v5, 0x1

    goto :goto_1

    .line 166
    :catchall_0
    move-exception v5

    monitor-exit p0

    throw v5
.end method

.method public onResponse(Ljava/lang/String;)Z
    .locals 9
    .param p1, "res"    # Ljava/lang/String;

    .prologue
    .line 209
    const-string v6, "AtParser"

    const-string v7, "onResponse"

    if-eqz p1, :cond_2

    const-string v5, "\\s+"

    const-string v8, ""

    invoke-virtual {p1, v5, v8}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    :goto_0
    invoke-static {v6, v7, v5}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 210
    iget-object v5, p0, Lcom/sec/factory/aporiented/AtParser;->mWaitingList:Ljava/util/concurrent/ConcurrentLinkedQueue;

    if-eqz v5, :cond_3

    iget-object v5, p0, Lcom/sec/factory/aporiented/AtParser;->mWaitingList:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v5}, Ljava/util/concurrent/ConcurrentLinkedQueue;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_3

    if-eqz p1, :cond_3

    .line 211
    iget-object v5, p0, Lcom/sec/factory/aporiented/AtParser;->mWaitingList:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v5}, Ljava/util/concurrent/ConcurrentLinkedQueue;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/factory/aporiented/AtParser$WaitingNodeInfo;

    .line 212
    .local v4, "item":Lcom/sec/factory/aporiented/AtParser$WaitingNodeInfo;
    invoke-virtual {v4}, Lcom/sec/factory/aporiented/AtParser$WaitingNodeInfo;->getCMD()Ljava/lang/String;

    move-result-object v0

    .line 213
    .local v0, "cmd":Ljava/lang/String;
    invoke-virtual {v4}, Lcom/sec/factory/aporiented/AtParser$WaitingNodeInfo;->getIndex()Ljava/lang/String;

    move-result-object v3

    .line 214
    .local v3, "index":Ljava/lang/String;
    const-string v5, "AtParser"

    const-string v6, "onResponse"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "itme info : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 215
    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {p1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 216
    const-string v5, "AtParser"

    const-string v6, "onResponse"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "catch response : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 217
    invoke-virtual {v4}, Lcom/sec/factory/aporiented/AtParser$WaitingNodeInfo;->getID()I

    move-result v2

    .line 218
    .local v2, "id":I
    iget-object v5, p0, Lcom/sec/factory/aporiented/AtParser;->mProcessThread:[Lcom/sec/factory/aporiented/ProcessThread;

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/sec/factory/aporiented/AtParser;->mProcessThread:[Lcom/sec/factory/aporiented/ProcessThread;

    aget-object v5, v5, v2

    if-eqz v5, :cond_1

    .line 219
    iget-object v5, p0, Lcom/sec/factory/aporiented/AtParser;->mProcessThread:[Lcom/sec/factory/aporiented/ProcessThread;

    aget-object v5, v5, v2

    invoke-virtual {v5}, Lcom/sec/factory/aporiented/ProcessThread;->isInterrupted()Z

    move-result v5

    if-nez v5, :cond_1

    .line 220
    iget-object v5, p0, Lcom/sec/factory/aporiented/AtParser;->mProcessThread:[Lcom/sec/factory/aporiented/ProcessThread;

    aget-object v5, v5, v2

    invoke-virtual {v5}, Lcom/sec/factory/aporiented/ProcessThread;->interrupt()V

    .line 223
    :cond_1
    iget-object v5, p0, Lcom/sec/factory/aporiented/AtParser;->mWaitingList:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v5, v4}, Ljava/util/concurrent/ConcurrentLinkedQueue;->remove(Ljava/lang/Object;)Z

    goto :goto_1

    .line 209
    .end local v0    # "cmd":Ljava/lang/String;
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "id":I
    .end local v3    # "index":Ljava/lang/String;
    .end local v4    # "item":Lcom/sec/factory/aporiented/AtParser$WaitingNodeInfo;
    :cond_2
    const/4 v5, 0x0

    goto/16 :goto_0

    .line 227
    :cond_3
    const/4 v5, 0x1

    return v5
.end method

.method public onRunCommand(Ljava/lang/String;)Z
    .locals 2
    .param p1, "cmd"    # Ljava/lang/String;

    .prologue
    .line 161
    const-string v0, "AtParser"

    const-string v1, "runCommand"

    invoke-static {v0, v1, p1}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 162
    iget-object v0, p0, Lcom/sec/factory/aporiented/AtParser;->mWriter:Lcom/sec/factory/aporiented/ResponseWriter;

    invoke-virtual {p0, p1, v0}, Lcom/sec/factory/aporiented/AtParser;->runCmd(Ljava/lang/String;Lcom/sec/factory/aporiented/ResponseWriter;)Z

    move-result v0

    return v0
.end method

.method public onWaitingResponse(Ljava/lang/String;I)Z
    .locals 6
    .param p1, "cmd"    # Ljava/lang/String;
    .param p2, "id"    # I

    .prologue
    .line 195
    const-string v2, "AtParser"

    const-string v3, "onWaitingResponse"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", ID = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 197
    if-eqz p1, :cond_0

    .line 198
    const-string v2, "+"

    invoke-virtual {p1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    const-string v3, "="

    invoke-virtual {p1, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {p1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 199
    .local v0, "command":Ljava/lang/String;
    const-string v2, ","

    invoke-virtual {p1, v2}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {p1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 200
    .local v1, "index":Ljava/lang/String;
    const-string v2, "AtParser"

    const-string v3, "onWaitingResponse"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "cmd : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", Index : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", ID = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 201
    iget-object v2, p0, Lcom/sec/factory/aporiented/AtParser;->mWaitingList:Ljava/util/concurrent/ConcurrentLinkedQueue;

    new-instance v3, Lcom/sec/factory/aporiented/AtParser$WaitingNodeInfo;

    invoke-direct {v3, v0, v1, p2}, Lcom/sec/factory/aporiented/AtParser$WaitingNodeInfo;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    invoke-virtual {v2, v3}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z

    .line 202
    const/4 v2, 0x1

    .line 205
    .end local v0    # "command":Ljava/lang/String;
    .end local v1    # "index":Ljava/lang/String;
    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public process(Ljava/lang/String;Lcom/sec/factory/aporiented/ResponseWriter;)Z
    .locals 20
    .param p1, "cmd"    # Ljava/lang/String;
    .param p2, "writer"    # Lcom/sec/factory/aporiented/ResponseWriter;

    .prologue
    .line 85
    const-string v16, "AtParser"

    const-string v17, "process"

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    move-object/from16 v2, p1

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    const/4 v6, 0x0

    .line 87
    .local v6, "cmdList":[Ljava/lang/String;
    const/4 v15, 0x1

    .line 90
    .local v15, "result":Z
    invoke-static {}, Landroid/os/FactoryTest;->isFactoryBinary()Z

    move-result v16

    if-nez v16, :cond_1

    const-string v16, "JIG_CHECK"

    invoke-static/range {v16 .. v16}, Lcom/sec/factory/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v16

    if-eqz v16, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/AtParser;->mContext:Landroid/content/Context;

    move-object/from16 v16, v0

    invoke-static/range {v16 .. v16}, Lcom/sec/factory/modules/ModuleCommon;->instance(Landroid/content/Context;)Lcom/sec/factory/modules/ModuleCommon;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Lcom/sec/factory/modules/ModuleCommon;->connectedJIG()Z

    move-result v16

    if-nez v16, :cond_1

    .line 92
    const-string v16, "AtParser"

    const-string v17, "process"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "Request is rejected. connectedJIG="

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/AtParser;->mContext:Landroid/content/Context;

    move-object/from16 v19, v0

    invoke-static/range {v19 .. v19}, Lcom/sec/factory/modules/ModuleCommon;->instance(Landroid/content/Context;)Lcom/sec/factory/modules/ModuleCommon;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lcom/sec/factory/modules/ModuleCommon;->connectedJIG()Z

    move-result v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ", isFactoryBinary="

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-static {}, Landroid/os/FactoryTest;->isFactoryBinary()Z

    move-result v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v16 .. v18}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    const/4 v15, 0x0

    .line 157
    .end local v15    # "result":Z
    :cond_0
    :goto_0
    return v15

    .line 99
    .restart local v15    # "result":Z
    :cond_1
    if-eqz p1, :cond_2

    :try_start_0
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v16

    const/16 v17, 0x1

    move/from16 v0, v16

    move/from16 v1, v17

    if-ge v0, v1, :cond_3

    .line 100
    :cond_2
    const/4 v15, 0x0

    goto :goto_0

    .line 103
    :cond_3
    const-string v16, ";"

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v16

    if-eqz v16, :cond_d

    .line 104
    const-string v16, ";"

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    .line 106
    if-eqz v6, :cond_4

    array-length v0, v6

    move/from16 v16, v0

    if-gtz v16, :cond_5

    .line 107
    :cond_4
    const-string v16, "AtParser"

    const-string v17, "process done successfully. cmdList is null"

    invoke-static/range {v16 .. v17}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    const/4 v15, 0x0

    goto :goto_0

    .line 111
    :cond_5
    const-string v16, "AT+PARALLEL=0,0,00000"

    const/16 v17, 0x0

    aget-object v17, v6, v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v16

    if-eqz v16, :cond_b

    .line 112
    const/16 v16, 0x1

    sput-boolean v16, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;->STAGE_PARALLEL:Z

    .line 113
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/AtParser;->mWriter:Lcom/sec/factory/aporiented/ResponseWriter;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Lcom/sec/factory/aporiented/ResponseWriter;->setResponseListener(Lcom/sec/factory/aporiented/ResponseWriter$ResponseListener;)V

    .line 115
    new-instance v13, Lcom/sec/factory/aporiented/ParallelCommandsParser;

    invoke-direct {v13}, Lcom/sec/factory/aporiented/ParallelCommandsParser;-><init>()V

    .line 117
    .local v13, "mParallelCommandsParser":Lcom/sec/factory/aporiented/ParallelCommandsParser;
    move-object v3, v6

    .local v3, "arr$":[Ljava/lang/String;
    array-length v12, v3

    .local v12, "len$":I
    const/4 v10, 0x0

    .local v10, "i$":I
    :goto_1
    if-ge v10, v12, :cond_6

    aget-object v7, v3, v10

    .line 118
    .local v7, "command":Ljava/lang/String;
    invoke-virtual {v13, v7}, Lcom/sec/factory/aporiented/ParallelCommandsParser;->add(Ljava/lang/String;)Z

    .line 117
    add-int/lit8 v10, v10, 0x1

    goto :goto_1

    .line 120
    .end local v7    # "command":Ljava/lang/String;
    :cond_6
    invoke-virtual {v13}, Lcom/sec/factory/aporiented/ParallelCommandsParser;->getHitCommandsList()Ljava/util/Collection;

    move-result-object v4

    .line 121
    .local v4, "categories":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/sec/factory/aporiented/ParallelCommandsParser$Category;>;"
    invoke-interface {v4}, Ljava/util/Collection;->size()I

    move-result v16

    move/from16 v0, v16

    new-array v0, v0, [Lcom/sec/factory/aporiented/ProcessThread;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/factory/aporiented/AtParser;->mProcessThread:[Lcom/sec/factory/aporiented/ProcessThread;

    .line 122
    const/4 v11, 0x0

    .line 123
    .local v11, "index":I
    const/4 v9, 0x4

    .end local v10    # "i$":I
    .local v9, "i":I
    :goto_2
    if-ltz v9, :cond_a

    .line 124
    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v10

    .local v10, "i$":Ljava/util/Iterator;
    :cond_7
    :goto_3
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v16

    if-eqz v16, :cond_9

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/factory/aporiented/ParallelCommandsParser$Category;

    .line 125
    .local v5, "category":Lcom/sec/factory/aporiented/ParallelCommandsParser$Category;
    invoke-virtual {v5}, Lcom/sec/factory/aporiented/ParallelCommandsParser$Category;->getPriority()I

    move-result v16

    move/from16 v0, v16

    if-ne v0, v9, :cond_7

    .line 126
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/AtParser;->mProcessThread:[Lcom/sec/factory/aporiented/ProcessThread;

    move-object/from16 v16, v0

    new-instance v17, Lcom/sec/factory/aporiented/ProcessThread;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/AtParser;->mProcessThread:[Lcom/sec/factory/aporiented/ProcessThread;

    move-object/from16 v18, v0

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    move-object/from16 v2, v18

    invoke-direct {v0, v1, v5, v2, v11}, Lcom/sec/factory/aporiented/ProcessThread;-><init>(Lcom/sec/factory/aporiented/CommandListener;Lcom/sec/factory/aporiented/ParallelCommandsParser$Category;[Lcom/sec/factory/aporiented/ProcessThread;I)V

    aput-object v17, v16, v11

    .line 128
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/AtParser;->mProcessThread:[Lcom/sec/factory/aporiented/ProcessThread;

    move-object/from16 v16, v0

    aget-object v16, v16, v11

    invoke-virtual/range {v16 .. v16}, Lcom/sec/factory/aporiented/ProcessThread;->getPriority()I

    move-result v16

    add-int v14, v16, v9

    .line 129
    .local v14, "priority":I
    const/16 v16, 0xa

    move/from16 v0, v16

    if-le v14, v0, :cond_8

    const/16 v14, 0xa

    .line 130
    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/AtParser;->mProcessThread:[Lcom/sec/factory/aporiented/ProcessThread;

    move-object/from16 v16, v0

    aget-object v16, v16, v11

    move-object/from16 v0, v16

    invoke-virtual {v0, v14}, Lcom/sec/factory/aporiented/ProcessThread;->setPriority(I)V

    .line 131
    add-int/lit8 v11, v11, 0x1

    .line 132
    invoke-interface {v4, v5}, Ljava/util/Collection;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/StringIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3

    .line 152
    .end local v3    # "arr$":[Ljava/lang/String;
    .end local v4    # "categories":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/sec/factory/aporiented/ParallelCommandsParser$Category;>;"
    .end local v5    # "category":Lcom/sec/factory/aporiented/ParallelCommandsParser$Category;
    .end local v9    # "i":I
    .end local v10    # "i$":Ljava/util/Iterator;
    .end local v11    # "index":I
    .end local v12    # "len$":I
    .end local v13    # "mParallelCommandsParser":Lcom/sec/factory/aporiented/ParallelCommandsParser;
    .end local v14    # "priority":I
    :catch_0
    move-exception v8

    .line 153
    .local v8, "e":Ljava/lang/StringIndexOutOfBoundsException;
    invoke-static {v8}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/Exception;)V

    .line 154
    const/4 v15, 0x0

    goto/16 :goto_0

    .line 123
    .end local v8    # "e":Ljava/lang/StringIndexOutOfBoundsException;
    .restart local v3    # "arr$":[Ljava/lang/String;
    .restart local v4    # "categories":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/sec/factory/aporiented/ParallelCommandsParser$Category;>;"
    .restart local v9    # "i":I
    .restart local v10    # "i$":Ljava/util/Iterator;
    .restart local v11    # "index":I
    .restart local v12    # "len$":I
    .restart local v13    # "mParallelCommandsParser":Lcom/sec/factory/aporiented/ParallelCommandsParser;
    :cond_9
    add-int/lit8 v9, v9, -0x1

    goto :goto_2

    .line 136
    .end local v10    # "i$":Ljava/util/Iterator;
    :cond_a
    const/4 v9, 0x0

    :goto_4
    if-ge v9, v11, :cond_0

    .line 137
    :try_start_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/factory/aporiented/AtParser;->mProcessThread:[Lcom/sec/factory/aporiented/ProcessThread;

    move-object/from16 v16, v0

    aget-object v16, v16, v9

    invoke-virtual/range {v16 .. v16}, Lcom/sec/factory/aporiented/ProcessThread;->start()V

    .line 136
    add-int/lit8 v9, v9, 0x1

    goto :goto_4

    .line 140
    .end local v3    # "arr$":[Ljava/lang/String;
    .end local v4    # "categories":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/sec/factory/aporiented/ParallelCommandsParser$Category;>;"
    .end local v9    # "i":I
    .end local v11    # "index":I
    .end local v12    # "len$":I
    .end local v13    # "mParallelCommandsParser":Lcom/sec/factory/aporiented/ParallelCommandsParser;
    :cond_b
    const/4 v9, 0x0

    .restart local v9    # "i":I
    :goto_5
    array-length v0, v6

    move/from16 v16, v0

    move/from16 v0, v16

    if-le v0, v9, :cond_0

    aget-object v16, v6, v9

    if-eqz v16, :cond_0

    .line 141
    aget-object v16, v6, v9

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    move-object/from16 v2, p2

    invoke-virtual {v0, v1, v2}, Lcom/sec/factory/aporiented/AtParser;->runCmd(Ljava/lang/String;Lcom/sec/factory/aporiented/ResponseWriter;)Z

    move-result v16

    if-eqz v16, :cond_c

    .line 142
    const-string v16, "AtParser"

    const-string v17, "process done successfully. cmdList = "

    aget-object v18, v6, v9

    invoke-static/range {v16 .. v18}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 140
    :cond_c
    add-int/lit8 v9, v9, 0x1

    goto :goto_5

    .line 148
    .end local v9    # "i":I
    :cond_d
    invoke-virtual/range {p0 .. p2}, Lcom/sec/factory/aporiented/AtParser;->runCmd(Ljava/lang/String;Lcom/sec/factory/aporiented/ResponseWriter;)Z

    move-result v16

    if-eqz v16, :cond_0

    .line 149
    const-string v16, "AtParser"

    const-string v17, "process done successfully. cmd = "

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    move-object/from16 v2, p1

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/StringIndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method

.method public registerAllHandler(Landroid/content/Context;Lcom/sec/factory/aporiented/ResponseWriter;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "writer"    # Lcom/sec/factory/aporiented/ResponseWriter;

    .prologue
    .line 67
    const-string v0, "AtParser"

    const-string v1, "registerAllHandler"

    const-string v2, "Register AT command handler"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    iput-object p1, p0, Lcom/sec/factory/aporiented/AtParser;->mContext:Landroid/content/Context;

    .line 69
    iput-object p2, p0, Lcom/sec/factory/aporiented/AtParser;->mWriter:Lcom/sec/factory/aporiented/ResponseWriter;

    .line 71
    new-instance v7, Lcom/sec/factory/aporiented/RegisterUserBinHandler;

    iget-object v0, p0, Lcom/sec/factory/aporiented/AtParser;->atHandlers:Ljava/util/HashMap;

    invoke-direct {v7, v0, p1, p2}, Lcom/sec/factory/aporiented/RegisterUserBinHandler;-><init>(Ljava/util/HashMap;Landroid/content/Context;Lcom/sec/factory/aporiented/ResponseWriter;)V

    .line 73
    .local v7, "userBinHandler":Lcom/sec/factory/aporiented/RegisterUserBinHandler;
    invoke-virtual {v7}, Lcom/sec/factory/aporiented/RegisterUserBinHandler;->registerHandler()V

    .line 75
    new-instance v6, Lcom/sec/factory/aporiented/RegisterFactoryBinHandler;

    iget-object v0, p0, Lcom/sec/factory/aporiented/AtParser;->atHandlers:Ljava/util/HashMap;

    invoke-direct {v6, v0, p1, p2}, Lcom/sec/factory/aporiented/RegisterFactoryBinHandler;-><init>(Ljava/util/HashMap;Landroid/content/Context;Lcom/sec/factory/aporiented/ResponseWriter;)V

    .line 77
    .local v6, "factoryBinHandler":Lcom/sec/factory/aporiented/RegisterFactoryBinHandler;
    invoke-virtual {v6}, Lcom/sec/factory/aporiented/RegisterFactoryBinHandler;->registerHandler()V

    .line 80
    new-instance v0, Lcom/sec/factory/aporiented/athandler/AtParallel;

    iget-object v1, p0, Lcom/sec/factory/aporiented/AtParser;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/factory/aporiented/AtParser;->mWriter:Lcom/sec/factory/aporiented/ResponseWriter;

    iget-object v3, p0, Lcom/sec/factory/aporiented/AtParser;->mProcessThread:[Lcom/sec/factory/aporiented/ProcessThread;

    iget-object v4, p0, Lcom/sec/factory/aporiented/AtParser;->mWaitingList:Ljava/util/concurrent/ConcurrentLinkedQueue;

    move-object v5, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/factory/aporiented/athandler/AtParallel;-><init>(Landroid/content/Context;Lcom/sec/factory/aporiented/ResponseWriter;[Lcom/sec/factory/aporiented/ProcessThread;Ljava/util/concurrent/ConcurrentLinkedQueue;Lcom/sec/factory/aporiented/CommandListener;)V

    iput-object v0, p0, Lcom/sec/factory/aporiented/AtParser;->mAtParallel:Lcom/sec/factory/aporiented/athandler/AtParallel;

    .line 81
    iget-object v0, p0, Lcom/sec/factory/aporiented/AtParser;->atHandlers:Ljava/util/HashMap;

    const-string v1, "AT+PARALLEL"

    iget-object v2, p0, Lcom/sec/factory/aporiented/AtParser;->mAtParallel:Lcom/sec/factory/aporiented/athandler/AtParallel;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 82
    return-void
.end method

.method public runCmd(Ljava/lang/String;Lcom/sec/factory/aporiented/ResponseWriter;)Z
    .locals 12
    .param p1, "cmd"    # Ljava/lang/String;
    .param p2, "writer"    # Lcom/sec/factory/aporiented/ResponseWriter;

    .prologue
    const/4 v7, 0x0

    .line 245
    const-string v8, "AtParser"

    const-string v9, "runCmd"

    invoke-static {v8, v9, p1}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 246
    const/4 v5, 0x0

    .line 247
    .local v5, "resData":Ljava/lang/String;
    const/4 v0, 0x0

    .line 248
    .local v0, "argu":[Ljava/lang/String;
    const/4 v1, 0x0

    .line 249
    .local v1, "commandName":Ljava/lang/String;
    const/4 v6, 0x0

    .line 252
    .local v6, "result":Z
    if-eqz p1, :cond_0

    :try_start_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v8

    const/4 v9, 0x1

    if-ge v8, v9, :cond_1

    .line 292
    :cond_0
    :goto_0
    return v7

    .line 254
    :cond_1
    const-string v8, "="

    invoke-virtual {p1, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_4

    const-string v8, "="

    invoke-virtual {p1, v8}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v8

    if-eqz v8, :cond_4

    .line 255
    const/4 v8, 0x0

    const-string v9, "="

    invoke-virtual {p1, v9}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v9

    invoke-virtual {p1, v8, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    .line 262
    :goto_1
    const-string v8, "AtParser"

    const-string v9, "process"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "command : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v10}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 264
    if-eqz v1, :cond_6

    iget-object v8, p0, Lcom/sec/factory/aporiented/AtParser;->atHandlers:Ljava/util/HashMap;

    invoke-virtual {v8, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_6

    invoke-direct {p0, p1, v1}, Lcom/sec/factory/aporiented/AtParser;->isFilteredCommand(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_6

    .line 266
    iget-object v8, p0, Lcom/sec/factory/aporiented/AtParser;->atHandlers:Ljava/util/HashMap;

    invoke-virtual {v8, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;

    .line 267
    .local v3, "handler":Lcom/sec/factory/aporiented/athandler/AtCommandHandler;
    invoke-virtual {p0, p1}, Lcom/sec/factory/aporiented/AtParser;->splitArgu(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 268
    invoke-virtual {v3, v0}, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;->handleCommand([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 269
    const-string v8, "AtParser"

    const-string v9, "runCmd: "

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "CMD : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ", result : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v10}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 270
    invoke-static {}, Lcom/sec/factory/aporiented/AtCommandInfo;->getInstance()Lcom/sec/factory/aporiented/AtCommandInfo;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Registered CMD : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/sec/factory/aporiented/AtCommandInfo;->addCommandHistory(Ljava/lang/String;)Z

    .line 281
    .end local v3    # "handler":Lcom/sec/factory/aporiented/athandler/AtCommandHandler;
    :goto_2
    if-eqz v5, :cond_3

    .line 282
    const-string v8, "NOT_APPLICABLE"

    invoke-virtual {v8, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_2

    .line 283
    invoke-virtual {p2, v5}, Lcom/sec/factory/aporiented/ResponseWriter;->write(Ljava/lang/String;)Z

    .line 285
    :cond_2
    const/4 v6, 0x1

    :cond_3
    move v7, v6

    .line 292
    goto/16 :goto_0

    .line 256
    :cond_4
    const-string v8, "?"

    invoke-virtual {p1, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 257
    const/4 v8, 0x0

    const-string v9, "?"

    invoke-virtual {p1, v9}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v9

    invoke-virtual {p1, v8, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_1

    .line 259
    :cond_5
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v10

    invoke-virtual {v8, v9, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_1

    .line 272
    :cond_6
    const-string v8, "AtParser"

    const-string v9, "runCmd: "

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " is not registered in the atHandler."

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v10}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 273
    new-instance v4, Landroid/content/Intent;

    const-string v8, "com.sec.factory.RECEIVED_FROM_RIL"

    invoke-direct {v4, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 274
    .local v4, "intent":Landroid/content/Intent;
    const-string v8, "command"

    invoke-virtual {v4, v8, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 275
    iget-object v8, p0, Lcom/sec/factory/aporiented/AtParser;->mContext:Landroid/content/Context;

    invoke-virtual {v8, v4}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 276
    const-string v8, "AtParser"

    const-string v9, "runCmd: "

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Broadcast unregistered command: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v10}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 277
    const/4 v6, 0x1

    .line 278
    invoke-static {}, Lcom/sec/factory/aporiented/AtCommandInfo;->getInstance()Lcom/sec/factory/aporiented/AtCommandInfo;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Not registered CMD : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/sec/factory/aporiented/AtCommandInfo;->addCommandHistory(Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/StringIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_2

    .line 287
    .end local v4    # "intent":Landroid/content/Intent;
    :catch_0
    move-exception v2

    .line 288
    .local v2, "e":Ljava/lang/StringIndexOutOfBoundsException;
    invoke-static {v2}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/Exception;)V

    goto/16 :goto_0
.end method

.method public splitArgu(Ljava/lang/String;)[Ljava/lang/String;
    .locals 13
    .param p1, "cmd"    # Ljava/lang/String;

    .prologue
    .line 298
    const-string v9, "="

    invoke-virtual {p1, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 299
    const-string v9, "="

    invoke-virtual {p1, v9}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v9

    add-int/lit8 v9, v9, 0x1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v10

    invoke-virtual {p1, v9, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 301
    .local v1, "args":Ljava/lang/String;
    const/16 v9, 0xa

    new-array v2, v9, [Ljava/lang/String;

    .line 302
    .local v2, "deposit":[Ljava/lang/String;
    const-string v0, "__ARG__"

    .line 303
    .local v0, "REPLACEMENT_STR":Ljava/lang/String;
    const/4 v5, 0x0

    .line 304
    .local v5, "index":I
    :goto_0
    const-string v9, "\""

    invoke-virtual {v1, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 305
    const-string v9, "\""

    invoke-virtual {v1, v9}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    .line 306
    .local v3, "fIndex":I
    const-string v9, "\""

    add-int/lit8 v10, v3, 0x1

    invoke-virtual {v1, v9, v10}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v9

    add-int/lit8 v9, v9, 0x1

    invoke-virtual {v1, v3, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    .line 307
    .local v8, "subStr":Ljava/lang/String;
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "__ARG__"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v1, v8, v9}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    .line 308
    const-string v9, "AtParser"

    const-string v10, "splitArgu()"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "subStr : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ", REPLACE : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "__ARG__"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ", args : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v9, v10, v11}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 310
    add-int/lit8 v6, v5, 0x1

    .end local v5    # "index":I
    .local v6, "index":I
    aput-object v8, v2, v5

    move v5, v6

    .line 311
    .end local v6    # "index":I
    .restart local v5    # "index":I
    goto :goto_0

    .line 313
    .end local v3    # "fIndex":I
    .end local v8    # "subStr":Ljava/lang/String;
    :cond_0
    const-string v9, ","

    invoke-virtual {v1, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    .local v7, "res":[Ljava/lang/String;
    move v6, v5

    .line 315
    .end local v5    # "index":I
    .restart local v6    # "index":I
    :goto_1
    add-int/lit8 v5, v6, -0x1

    .end local v6    # "index":I
    .restart local v5    # "index":I
    if-lez v6, :cond_3

    if-eqz v7, :cond_3

    .line 316
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_2
    array-length v9, v7

    if-ge v4, v9, :cond_1

    .line 317
    aget-object v9, v7, v4

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "__ARG__"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 318
    const-string v9, "AtParser"

    const-string v10, "splitArgu()"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "res : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    aget-object v12, v7, v4

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "__ARG__"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    aget-object v12, v2, v5

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v9, v10, v11}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 320
    aget-object v9, v2, v5

    aput-object v9, v7, v4

    .line 321
    const-string v9, "AtParser"

    const-string v10, "splitArgu()"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "res : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    aget-object v12, v7, v4

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v9, v10, v11}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    move v6, v5

    .line 316
    .end local v5    # "index":I
    .restart local v6    # "index":I
    goto :goto_1

    .end local v6    # "index":I
    .restart local v5    # "index":I
    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 327
    .end local v4    # "i":I
    :cond_3
    if-eqz v7, :cond_4

    .line 328
    const-string v9, "AtParser"

    const-string v10, "splitArgu()"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "args : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    array-length v12, v7

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-static {v7}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v9, v10, v11}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 335
    .end local v0    # "REPLACEMENT_STR":Ljava/lang/String;
    .end local v1    # "args":Ljava/lang/String;
    .end local v2    # "deposit":[Ljava/lang/String;
    .end local v5    # "index":I
    :cond_4
    :goto_3
    return-object v7

    .line 332
    .end local v7    # "res":[Ljava/lang/String;
    :cond_5
    const/4 v9, 0x0

    new-array v7, v9, [Ljava/lang/String;

    .restart local v7    # "res":[Ljava/lang/String;
    goto :goto_3
.end method

.method public terminateThreads()Z
    .locals 6

    .prologue
    .line 231
    const-string v4, "AtParser"

    const-string v5, "terminateThreads"

    invoke-static {v4, v5}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;)V

    .line 232
    iget-object v4, p0, Lcom/sec/factory/aporiented/AtParser;->mProcessThread:[Lcom/sec/factory/aporiented/ProcessThread;

    if-eqz v4, :cond_2

    .line 233
    iget-object v0, p0, Lcom/sec/factory/aporiented/AtParser;->mProcessThread:[Lcom/sec/factory/aporiented/ProcessThread;

    .local v0, "arr$":[Lcom/sec/factory/aporiented/ProcessThread;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 234
    .local v3, "p":Lcom/sec/factory/aporiented/ProcessThread;
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Lcom/sec/factory/aporiented/ProcessThread;->isAlive()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 235
    invoke-virtual {v3}, Lcom/sec/factory/aporiented/ProcessThread;->kill()V

    .line 233
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 239
    .end local v3    # "p":Lcom/sec/factory/aporiented/ProcessThread;
    :cond_1
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/sec/factory/aporiented/AtParser;->mProcessThread:[Lcom/sec/factory/aporiented/ProcessThread;

    .line 241
    .end local v0    # "arr$":[Lcom/sec/factory/aporiented/ProcessThread;
    .end local v1    # "i$":I
    .end local v2    # "len$":I
    :cond_2
    const/4 v4, 0x1

    return v4
.end method
