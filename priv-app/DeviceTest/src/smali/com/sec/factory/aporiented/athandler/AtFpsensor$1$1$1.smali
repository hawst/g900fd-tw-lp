.class Lcom/sec/factory/aporiented/athandler/AtFpsensor$1$1$1;
.super Ljava/lang/Object;
.source "AtFpsensor.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/factory/aporiented/athandler/AtFpsensor$1$1;->onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$2:Lcom/sec/factory/aporiented/athandler/AtFpsensor$1$1;


# direct methods
.method constructor <init>(Lcom/sec/factory/aporiented/athandler/AtFpsensor$1$1;)V
    .locals 0

    .prologue
    .line 629
    iput-object p1, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor$1$1$1;->this$2:Lcom/sec/factory/aporiented/athandler/AtFpsensor$1$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 632
    iget-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor$1$1$1;->this$2:Lcom/sec/factory/aporiented/athandler/AtFpsensor$1$1;

    iget-object v1, v1, Lcom/sec/factory/aporiented/athandler/AtFpsensor$1$1;->this$1:Lcom/sec/factory/aporiented/athandler/AtFpsensor$1;

    iget-object v1, v1, Lcom/sec/factory/aporiented/athandler/AtFpsensor$1;->this$0:Lcom/sec/factory/aporiented/athandler/AtFpsensor;

    # getter for: Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mFingerprint:Lcom/validity/fingerprint/Fingerprint;
    invoke-static {v1}, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->access$200(Lcom/sec/factory/aporiented/athandler/AtFpsensor;)Lcom/validity/fingerprint/Fingerprint;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 633
    iget-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor$1$1$1;->this$2:Lcom/sec/factory/aporiented/athandler/AtFpsensor$1$1;

    iget-object v1, v1, Lcom/sec/factory/aporiented/athandler/AtFpsensor$1$1;->this$1:Lcom/sec/factory/aporiented/athandler/AtFpsensor$1;

    iget-object v1, v1, Lcom/sec/factory/aporiented/athandler/AtFpsensor$1;->this$0:Lcom/sec/factory/aporiented/athandler/AtFpsensor;

    # getter for: Lcom/sec/factory/aporiented/athandler/AtFpsensor;->mFingerprint:Lcom/validity/fingerprint/Fingerprint;
    invoke-static {v1}, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->access$200(Lcom/sec/factory/aporiented/athandler/AtFpsensor;)Lcom/validity/fingerprint/Fingerprint;

    move-result-object v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/validity/fingerprint/Fingerprint;->notify(ILjava/lang/Object;)I

    move-result v0

    .line 634
    .local v0, "result":I
    iget-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtFpsensor$1$1$1;->this$2:Lcom/sec/factory/aporiented/athandler/AtFpsensor$1$1;

    iget-object v1, v1, Lcom/sec/factory/aporiented/athandler/AtFpsensor$1$1;->this$1:Lcom/sec/factory/aporiented/athandler/AtFpsensor$1;

    iget-object v1, v1, Lcom/sec/factory/aporiented/athandler/AtFpsensor$1;->this$0:Lcom/sec/factory/aporiented/athandler/AtFpsensor;

    iget-object v1, v1, Lcom/sec/factory/aporiented/athandler/AtFpsensor;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "createPopup"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Sensor status : VCS_NOTIFY_SNSR_TEST_CONTINUE ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 636
    .end local v0    # "result":I
    :cond_0
    return-void
.end method
