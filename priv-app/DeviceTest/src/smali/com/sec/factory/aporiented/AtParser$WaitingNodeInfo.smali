.class public final Lcom/sec/factory/aporiented/AtParser$WaitingNodeInfo;
.super Ljava/lang/Object;
.source "AtParser.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/factory/aporiented/AtParser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "WaitingNodeInfo"
.end annotation


# instance fields
.field private CMD:Ljava/lang/String;

.field private ID:I

.field private mIndex:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 0
    .param p1, "cmd"    # Ljava/lang/String;
    .param p2, "index"    # Ljava/lang/String;
    .param p3, "id"    # I

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object p1, p0, Lcom/sec/factory/aporiented/AtParser$WaitingNodeInfo;->CMD:Ljava/lang/String;

    .line 40
    iput-object p2, p0, Lcom/sec/factory/aporiented/AtParser$WaitingNodeInfo;->mIndex:Ljava/lang/String;

    .line 41
    iput p3, p0, Lcom/sec/factory/aporiented/AtParser$WaitingNodeInfo;->ID:I

    .line 42
    return-void
.end method


# virtual methods
.method public getCMD()Ljava/lang/String;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/factory/aporiented/AtParser$WaitingNodeInfo;->CMD:Ljava/lang/String;

    return-object v0
.end method

.method public getID()I
    .locals 1

    .prologue
    .line 53
    iget v0, p0, Lcom/sec/factory/aporiented/AtParser$WaitingNodeInfo;->ID:I

    return v0
.end method

.method public getIndex()Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/factory/aporiented/AtParser$WaitingNodeInfo;->mIndex:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 58
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/sec/factory/aporiented/AtParser$WaitingNodeInfo;->CMD:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/factory/aporiented/AtParser$WaitingNodeInfo;->mIndex:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", ID="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/factory/aporiented/AtParser$WaitingNodeInfo;->ID:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
