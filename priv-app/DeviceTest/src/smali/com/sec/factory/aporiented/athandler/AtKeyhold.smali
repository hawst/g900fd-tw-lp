.class public Lcom/sec/factory/aporiented/athandler/AtKeyhold;
.super Lcom/sec/factory/aporiented/athandler/AtCommandHandler;
.source "AtKeyhold.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 8
    invoke-direct {p0, p1}, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;-><init>(Landroid/content/Context;)V

    .line 9
    const-string v0, "KEYHOLD"

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtKeyhold;->CMD_NAME:Ljava/lang/String;

    .line 10
    const-string v0, "AtKeyHold"

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtKeyhold;->CLASS_NAME:Ljava/lang/String;

    .line 11
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/factory/aporiented/athandler/AtKeyhold;->NUM_ARGS:I

    .line 12
    return-void
.end method


# virtual methods
.method public declared-synchronized handleCommand([Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "argu"    # [Ljava/lang/String;

    .prologue
    .line 16
    monitor-enter p0

    const/4 v0, 0x0

    .line 18
    .local v0, "resData":Ljava/lang/String;
    :try_start_0
    array-length v2, p1

    sget-boolean v1, Lcom/sec/factory/aporiented/athandler/AtKeyhold;->STAGE_PARALLEL:Z

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/sec/factory/aporiented/athandler/AtKeyhold;->NUM_ARGS:I

    add-int/lit8 v1, v1, 0x1

    :goto_0
    if-eq v2, v1, :cond_1

    .line 19
    const-string v1, "wrong"
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 28
    :goto_1
    monitor-exit p0

    return-object v1

    .line 18
    :cond_0
    :try_start_1
    iget v1, p0, Lcom/sec/factory/aporiented/athandler/AtKeyhold;->NUM_ARGS:I

    goto :goto_0

    .line 22
    :cond_1
    iget-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtKeyhold;->context:Landroid/content/Context;

    invoke-static {v1}, Landroid/os/FactoryTest;->isAutomaticTestMode(Landroid/content/Context;)Z

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    .line 23
    const/4 v1, 0x0

    goto :goto_1

    .line 26
    :cond_2
    sget-object v1, Lcom/sec/factory/aporiented/athandler/AtKeyhold;->mModuleDFT:Lcom/sec/factory/modules/ModuleDFT;

    const/4 v2, 0x0

    aget-object v2, p1, v2

    invoke-virtual {v1, v2}, Lcom/sec/factory/modules/ModuleDFT;->DftKeyHold(Ljava/lang/String;)V

    .line 27
    const-string v0, "OK\r\n"
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object v1, v0

    .line 28
    goto :goto_1

    .line 16
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method
