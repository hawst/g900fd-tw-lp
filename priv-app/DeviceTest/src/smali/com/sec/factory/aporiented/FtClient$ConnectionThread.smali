.class Lcom/sec/factory/aporiented/FtClient$ConnectionThread;
.super Ljava/lang/Thread;
.source "FtClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/factory/aporiented/FtClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ConnectionThread"
.end annotation


# instance fields
.field private active:Z

.field private final context:Landroid/content/Context;

.field private in:Ljava/io/BufferedReader;

.field final synthetic this$0:Lcom/sec/factory/aporiented/FtClient;


# direct methods
.method public constructor <init>(Lcom/sec/factory/aporiented/FtClient;Landroid/content/Context;)V
    .locals 1
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 347
    iput-object p1, p0, Lcom/sec/factory/aporiented/FtClient$ConnectionThread;->this$0:Lcom/sec/factory/aporiented/FtClient;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 348
    iput-object p2, p0, Lcom/sec/factory/aporiented/FtClient$ConnectionThread;->context:Landroid/content/Context;

    .line 349
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/factory/aporiented/FtClient$ConnectionThread;->active:Z

    .line 350
    return-void
.end method


# virtual methods
.method public kill()V
    .locals 3

    .prologue
    .line 470
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/factory/aporiented/FtClient$ConnectionThread;->active:Z

    .line 471
    iget-object v0, p0, Lcom/sec/factory/aporiented/FtClient$ConnectionThread;->this$0:Lcom/sec/factory/aporiented/FtClient;

    # getter for: Lcom/sec/factory/aporiented/FtClient;->connectionThread:Lcom/sec/factory/aporiented/FtClient$ConnectionThread;
    invoke-static {v0}, Lcom/sec/factory/aporiented/FtClient;->access$1400(Lcom/sec/factory/aporiented/FtClient;)Lcom/sec/factory/aporiented/FtClient$ConnectionThread;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/factory/aporiented/FtClient$ConnectionThread;->interrupt()V

    .line 472
    const-string v0, "FtClient"

    const-string v1, "run"

    const-string v2, "Kill ConnectionThread"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 473
    return-void
.end method

.method public run()V
    .locals 13

    .prologue
    const/4 v12, 0x1

    const/4 v11, 0x0

    .line 354
    const-string v7, "FtClient"

    const-string v8, "run"

    const-string v9, "ConnectionThread start"

    invoke-static {v7, v8, v9}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 355
    const/4 v0, 0x0

    .line 356
    .local v0, "connected":Z
    const/4 v2, 0x0

    .line 357
    .local v2, "garbage_count":I
    new-instance v4, Landroid/net/LocalSocketAddress;

    const-string v7, "FactoryClientRecv"

    invoke-direct {v4, v7}, Landroid/net/LocalSocketAddress;-><init>(Ljava/lang/String;)V

    .line 358
    .local v4, "lsa_recv":Landroid/net/LocalSocketAddress;
    new-instance v5, Landroid/net/LocalSocketAddress;

    const-string v7, "FactoryClientSend"

    invoke-direct {v5, v7}, Landroid/net/LocalSocketAddress;-><init>(Ljava/lang/String;)V

    .line 360
    .local v5, "lsa_send":Landroid/net/LocalSocketAddress;
    :cond_0
    if-nez v0, :cond_b

    .line 362
    :try_start_0
    iget-object v7, p0, Lcom/sec/factory/aporiented/FtClient$ConnectionThread;->this$0:Lcom/sec/factory/aporiented/FtClient;

    iget-object v7, v7, Lcom/sec/factory/aporiented/FtClient;->clientSocket_recv:Landroid/net/LocalSocket;

    invoke-virtual {v7}, Landroid/net/LocalSocket;->isConnected()Z

    move-result v7

    if-nez v7, :cond_1

    .line 363
    iget-object v7, p0, Lcom/sec/factory/aporiented/FtClient$ConnectionThread;->this$0:Lcom/sec/factory/aporiented/FtClient;

    iget-object v7, v7, Lcom/sec/factory/aporiented/FtClient;->clientSocket_recv:Landroid/net/LocalSocket;

    invoke-virtual {v7, v4}, Landroid/net/LocalSocket;->connect(Landroid/net/LocalSocketAddress;)V

    .line 364
    const-string v7, "FtClient"

    const-string v8, "run"

    const-string v9, "Connect client socket(receiver)"

    invoke-static {v7, v8, v9}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 367
    :cond_1
    iget-object v7, p0, Lcom/sec/factory/aporiented/FtClient$ConnectionThread;->this$0:Lcom/sec/factory/aporiented/FtClient;

    iget-object v7, v7, Lcom/sec/factory/aporiented/FtClient;->clientSocket_send:Landroid/net/LocalSocket;

    invoke-virtual {v7}, Landroid/net/LocalSocket;->isConnected()Z

    move-result v7

    if-nez v7, :cond_2

    .line 368
    iget-object v7, p0, Lcom/sec/factory/aporiented/FtClient$ConnectionThread;->this$0:Lcom/sec/factory/aporiented/FtClient;

    iget-object v7, v7, Lcom/sec/factory/aporiented/FtClient;->clientSocket_send:Landroid/net/LocalSocket;

    invoke-virtual {v7, v5}, Landroid/net/LocalSocket;->connect(Landroid/net/LocalSocketAddress;)V

    .line 369
    const-string v7, "FtClient"

    const-string v8, "run"

    const-string v9, "Connect client socket(sender)"

    invoke-static {v7, v8, v9}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 377
    :cond_2
    :goto_0
    iget-object v7, p0, Lcom/sec/factory/aporiented/FtClient$ConnectionThread;->this$0:Lcom/sec/factory/aporiented/FtClient;

    iget-object v7, v7, Lcom/sec/factory/aporiented/FtClient;->clientSocket_recv:Landroid/net/LocalSocket;

    invoke-virtual {v7}, Landroid/net/LocalSocket;->isConnected()Z

    move-result v7

    if-eqz v7, :cond_8

    iget-object v7, p0, Lcom/sec/factory/aporiented/FtClient$ConnectionThread;->this$0:Lcom/sec/factory/aporiented/FtClient;

    iget-object v7, v7, Lcom/sec/factory/aporiented/FtClient;->clientSocket_send:Landroid/net/LocalSocket;

    invoke-virtual {v7}, Landroid/net/LocalSocket;->isConnected()Z

    move-result v7

    if-eqz v7, :cond_8

    .line 378
    const/4 v0, 0x1

    .line 379
    const-string v7, "FtClient"

    const-string v8, "run"

    const-string v9, "Client socket connect success"

    invoke-static {v7, v8, v9}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 390
    :goto_1
    if-eqz v0, :cond_0

    .line 391
    const-string v7, "FtClient"

    const-string v8, "run"

    const-string v9, "Connected to AT Core"

    invoke-static {v7, v8, v9}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 392
    iput-boolean v12, p0, Lcom/sec/factory/aporiented/FtClient$ConnectionThread;->active:Z

    .line 394
    iget-object v7, p0, Lcom/sec/factory/aporiented/FtClient$ConnectionThread;->this$0:Lcom/sec/factory/aporiented/FtClient;

    iget-object v7, v7, Lcom/sec/factory/aporiented/FtClient;->writer:Lcom/sec/factory/aporiented/ResponseWriter;

    if-nez v7, :cond_3

    .line 395
    iget-object v7, p0, Lcom/sec/factory/aporiented/FtClient$ConnectionThread;->this$0:Lcom/sec/factory/aporiented/FtClient;

    new-instance v8, Lcom/sec/factory/aporiented/ResponseWriter;

    iget-object v9, p0, Lcom/sec/factory/aporiented/FtClient$ConnectionThread;->this$0:Lcom/sec/factory/aporiented/FtClient;

    iget-object v9, v9, Lcom/sec/factory/aporiented/FtClient;->clientSocket_send:Landroid/net/LocalSocket;

    invoke-direct {v8, v9}, Lcom/sec/factory/aporiented/ResponseWriter;-><init>(Landroid/net/LocalSocket;)V

    iput-object v8, v7, Lcom/sec/factory/aporiented/FtClient;->writer:Lcom/sec/factory/aporiented/ResponseWriter;

    .line 398
    :cond_3
    iget-object v7, p0, Lcom/sec/factory/aporiented/FtClient$ConnectionThread;->this$0:Lcom/sec/factory/aporiented/FtClient;

    iget-object v7, v7, Lcom/sec/factory/aporiented/FtClient;->parser:Lcom/sec/factory/aporiented/AtParser;

    if-nez v7, :cond_4

    .line 399
    iget-object v7, p0, Lcom/sec/factory/aporiented/FtClient$ConnectionThread;->this$0:Lcom/sec/factory/aporiented/FtClient;

    new-instance v8, Lcom/sec/factory/aporiented/AtParser;

    invoke-direct {v8}, Lcom/sec/factory/aporiented/AtParser;-><init>()V

    iput-object v8, v7, Lcom/sec/factory/aporiented/FtClient;->parser:Lcom/sec/factory/aporiented/AtParser;

    .line 400
    iget-object v7, p0, Lcom/sec/factory/aporiented/FtClient$ConnectionThread;->this$0:Lcom/sec/factory/aporiented/FtClient;

    iget-object v7, v7, Lcom/sec/factory/aporiented/FtClient;->parser:Lcom/sec/factory/aporiented/AtParser;

    iget-object v8, p0, Lcom/sec/factory/aporiented/FtClient$ConnectionThread;->context:Landroid/content/Context;

    iget-object v9, p0, Lcom/sec/factory/aporiented/FtClient$ConnectionThread;->this$0:Lcom/sec/factory/aporiented/FtClient;

    iget-object v9, v9, Lcom/sec/factory/aporiented/FtClient;->writer:Lcom/sec/factory/aporiented/ResponseWriter;

    invoke-virtual {v7, v8, v9}, Lcom/sec/factory/aporiented/AtParser;->registerAllHandler(Landroid/content/Context;Lcom/sec/factory/aporiented/ResponseWriter;)V

    .line 404
    :cond_4
    :try_start_1
    new-instance v7, Ljava/io/BufferedReader;

    new-instance v8, Ljava/io/InputStreamReader;

    iget-object v9, p0, Lcom/sec/factory/aporiented/FtClient$ConnectionThread;->this$0:Lcom/sec/factory/aporiented/FtClient;

    iget-object v9, v9, Lcom/sec/factory/aporiented/FtClient;->clientSocket_recv:Landroid/net/LocalSocket;

    invoke-virtual {v9}, Landroid/net/LocalSocket;->getInputStream()Ljava/io/InputStream;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v7, v8}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    iput-object v7, p0, Lcom/sec/factory/aporiented/FtClient$ConnectionThread;->in:Ljava/io/BufferedReader;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3

    .line 412
    :goto_2
    invoke-static {}, Landroid/os/FactoryTest;->isFactoryBinary()Z

    move-result v7

    if-eqz v7, :cond_5

    .line 413
    iget-object v7, p0, Lcom/sec/factory/aporiented/FtClient$ConnectionThread;->this$0:Lcom/sec/factory/aporiented/FtClient;

    # invokes: Lcom/sec/factory/aporiented/FtClient;->sendBootCompleted()V
    invoke-static {v7}, Lcom/sec/factory/aporiented/FtClient;->access$200(Lcom/sec/factory/aporiented/FtClient;)V

    .line 417
    :cond_5
    const-string v7, "camera"

    const-string v8, "DEVICE_TYPE"

    invoke-static {v8}, Lcom/sec/factory/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 418
    iget-object v7, p0, Lcom/sec/factory/aporiented/FtClient$ConnectionThread;->this$0:Lcom/sec/factory/aporiented/FtClient;

    new-instance v8, Lcom/sec/factory/aporiented/DIServiceConnectionManager;

    iget-object v9, p0, Lcom/sec/factory/aporiented/FtClient$ConnectionThread;->context:Landroid/content/Context;

    iget-object v10, p0, Lcom/sec/factory/aporiented/FtClient$ConnectionThread;->this$0:Lcom/sec/factory/aporiented/FtClient;

    iget-object v10, v10, Lcom/sec/factory/aporiented/FtClient;->writer:Lcom/sec/factory/aporiented/ResponseWriter;

    invoke-direct {v8, v9, v10}, Lcom/sec/factory/aporiented/DIServiceConnectionManager;-><init>(Landroid/content/Context;Lcom/sec/factory/aporiented/ResponseWriter;)V

    iput-object v8, v7, Lcom/sec/factory/aporiented/FtClient;->diCommandManager:Lcom/sec/factory/aporiented/DIServiceConnectionManager;

    .line 419
    iget-object v7, p0, Lcom/sec/factory/aporiented/FtClient$ConnectionThread;->this$0:Lcom/sec/factory/aporiented/FtClient;

    iget-object v7, v7, Lcom/sec/factory/aporiented/FtClient;->diCommandManager:Lcom/sec/factory/aporiented/DIServiceConnectionManager;

    if-eqz v7, :cond_6

    .line 420
    iget-object v7, p0, Lcom/sec/factory/aporiented/FtClient$ConnectionThread;->this$0:Lcom/sec/factory/aporiented/FtClient;

    iget-object v7, v7, Lcom/sec/factory/aporiented/FtClient;->diCommandManager:Lcom/sec/factory/aporiented/DIServiceConnectionManager;

    invoke-virtual {v7}, Lcom/sec/factory/aporiented/DIServiceConnectionManager;->connectToDIAtCommandService()V

    .line 424
    :cond_6
    :goto_3
    iget-boolean v7, p0, Lcom/sec/factory/aporiented/FtClient$ConnectionThread;->active:Z

    if-eqz v7, :cond_a

    .line 426
    :try_start_2
    iget-object v7, p0, Lcom/sec/factory/aporiented/FtClient$ConnectionThread;->in:Ljava/io/BufferedReader;

    invoke-virtual {v7}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v3

    .line 428
    .local v3, "incoming":Ljava/lang/String;
    if-eqz v3, :cond_9

    .line 429
    iget-object v7, p0, Lcom/sec/factory/aporiented/FtClient$ConnectionThread;->this$0:Lcom/sec/factory/aporiented/FtClient;

    iget-object v7, v7, Lcom/sec/factory/aporiented/FtClient;->diCommandManager:Lcom/sec/factory/aporiented/DIServiceConnectionManager;

    if-eqz v7, :cond_7

    .line 430
    iget-object v7, p0, Lcom/sec/factory/aporiented/FtClient$ConnectionThread;->this$0:Lcom/sec/factory/aporiented/FtClient;

    iget-object v7, v7, Lcom/sec/factory/aporiented/FtClient;->diCommandManager:Lcom/sec/factory/aporiented/DIServiceConnectionManager;

    invoke-virtual {v7, v3}, Lcom/sec/factory/aporiented/DIServiceConnectionManager;->send(Ljava/lang/String;)Z

    .line 433
    :cond_7
    iget-object v7, p0, Lcom/sec/factory/aporiented/FtClient$ConnectionThread;->this$0:Lcom/sec/factory/aporiented/FtClient;

    iget-object v7, v7, Lcom/sec/factory/aporiented/FtClient;->parser:Lcom/sec/factory/aporiented/AtParser;

    iget-object v8, p0, Lcom/sec/factory/aporiented/FtClient$ConnectionThread;->this$0:Lcom/sec/factory/aporiented/FtClient;

    iget-object v8, v8, Lcom/sec/factory/aporiented/FtClient;->writer:Lcom/sec/factory/aporiented/ResponseWriter;

    invoke-virtual {v7, v3, v8}, Lcom/sec/factory/aporiented/AtParser;->process(Ljava/lang/String;Lcom/sec/factory/aporiented/ResponseWriter;)Z

    move-result v6

    .line 435
    .local v6, "result":Z
    if-ne v6, v12, :cond_6

    iget-object v7, p0, Lcom/sec/factory/aporiented/FtClient$ConnectionThread;->this$0:Lcom/sec/factory/aporiented/FtClient;

    # getter for: Lcom/sec/factory/aporiented/FtClient;->mIsFirstCmd:Z
    invoke-static {v7}, Lcom/sec/factory/aporiented/FtClient;->access$1000(Lcom/sec/factory/aporiented/FtClient;)Z

    move-result v7

    if-nez v7, :cond_6

    .line 438
    iget-object v7, p0, Lcom/sec/factory/aporiented/FtClient$ConnectionThread;->this$0:Lcom/sec/factory/aporiented/FtClient;

    # invokes: Lcom/sec/factory/aporiented/FtClient;->setForeGroundService()V
    invoke-static {v7}, Lcom/sec/factory/aporiented/FtClient;->access$1100(Lcom/sec/factory/aporiented/FtClient;)V

    .line 439
    iget-object v7, p0, Lcom/sec/factory/aporiented/FtClient$ConnectionThread;->this$0:Lcom/sec/factory/aporiented/FtClient;

    # invokes: Lcom/sec/factory/aporiented/FtClient;->setPendingIntent()V
    invoke-static {v7}, Lcom/sec/factory/aporiented/FtClient;->access$1200(Lcom/sec/factory/aporiented/FtClient;)V

    .line 440
    iget-object v7, p0, Lcom/sec/factory/aporiented/FtClient$ConnectionThread;->this$0:Lcom/sec/factory/aporiented/FtClient;

    # getter for: Lcom/sec/factory/aporiented/FtClient;->mContext:Landroid/content/Context;
    invoke-static {v7}, Lcom/sec/factory/aporiented/FtClient;->access$1300(Lcom/sec/factory/aporiented/FtClient;)Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/factory/modules/ModuleCommon;->instance(Landroid/content/Context;)Lcom/sec/factory/modules/ModuleCommon;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/factory/modules/ModuleCommon;->enableFtClient()Z

    .line 441
    iget-object v7, p0, Lcom/sec/factory/aporiented/FtClient$ConnectionThread;->this$0:Lcom/sec/factory/aporiented/FtClient;

    const/4 v8, 0x1

    # setter for: Lcom/sec/factory/aporiented/FtClient;->mIsFirstCmd:Z
    invoke-static {v7, v8}, Lcom/sec/factory/aporiented/FtClient;->access$1002(Lcom/sec/factory/aporiented/FtClient;Z)Z

    .line 442
    const-string v7, "FtClient"

    const-string v8, "handleMessage"

    const-string v9, "FirstCMDReceived: Noti ON"

    invoke-static {v7, v8, v9}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_3

    .line 456
    .end local v3    # "incoming":Ljava/lang/String;
    .end local v6    # "result":Z
    :catch_0
    move-exception v1

    .line 457
    .local v1, "e":Ljava/io/IOException;
    invoke-static {v1}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/Exception;)V

    .line 458
    iput-boolean v11, p0, Lcom/sec/factory/aporiented/FtClient$ConnectionThread;->active:Z

    .line 459
    const/4 v0, 0x0

    .line 460
    goto :goto_3

    .line 371
    .end local v1    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v1

    .line 372
    .restart local v1    # "e":Ljava/io/IOException;
    invoke-static {}, Landroid/os/FactoryTest;->isFactoryBinary()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 373
    invoke-static {v1}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/Exception;)V

    goto/16 :goto_0

    .line 381
    .end local v1    # "e":Ljava/io/IOException;
    :cond_8
    const/4 v0, 0x0

    .line 384
    const-wide/16 v8, 0xbb8

    :try_start_3
    invoke-static {v8, v9}, Ljava/lang/Thread;->sleep(J)V
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_2

    goto/16 :goto_1

    .line 385
    :catch_2
    move-exception v1

    .line 386
    .local v1, "e":Ljava/lang/InterruptedException;
    invoke-static {v1}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/Exception;)V

    goto/16 :goto_1

    .line 406
    .end local v1    # "e":Ljava/lang/InterruptedException;
    :catch_3
    move-exception v1

    .line 407
    .local v1, "e":Ljava/io/IOException;
    invoke-static {v1}, Lcom/sec/factory/support/FtUtil;->log_e(Ljava/lang/Exception;)V

    .line 408
    iput-boolean v11, p0, Lcom/sec/factory/aporiented/FtClient$ConnectionThread;->active:Z

    .line 409
    const/4 v0, 0x0

    goto/16 :goto_2

    .line 446
    .end local v1    # "e":Ljava/io/IOException;
    .restart local v3    # "incoming":Ljava/lang/String;
    :cond_9
    add-int/lit8 v2, v2, 0x1

    .line 447
    :try_start_4
    const-string v7, "FtClient"

    const-string v8, "run"

    const-string v9, "Garbage data incoming..."

    invoke-static {v7, v8, v9}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 449
    const/16 v7, 0xa

    if-le v2, v7, :cond_6

    .line 450
    const-string v7, "FtClient"

    const-string v8, "run"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Stop FtClient(Garbage data count="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ")"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v8, v9}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 453
    const/4 v7, 0x0

    iput-boolean v7, p0, Lcom/sec/factory/aporiented/FtClient$ConnectionThread;->active:Z
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    goto/16 :goto_3

    .line 463
    .end local v3    # "incoming":Ljava/lang/String;
    :cond_a
    const-string v7, "FtClient"

    const-string v8, "killed thread"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "connected value : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v8, v9}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 467
    :cond_b
    return-void
.end method
