.class public Lcom/sec/factory/aporiented/athandler/AtKstringb;
.super Lcom/sec/factory/aporiented/athandler/AtCommandHandler;
.source "AtKstringb.java"


# static fields
.field public static final JIG_ON:B = 0x1ct


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 19
    invoke-direct {p0, p1}, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;-><init>(Landroid/content/Context;)V

    .line 20
    const-string v0, "KSTRINGB"

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtKstringb;->CMD_NAME:Ljava/lang/String;

    .line 21
    const-string v0, "AtKstringb"

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtKstringb;->CLASS_NAME:Ljava/lang/String;

    .line 22
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/factory/aporiented/athandler/AtKstringb;->NUM_ARGS:I

    .line 23
    return-void
.end method


# virtual methods
.method public declared-synchronized handleCommand([Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p1, "argu"    # [Ljava/lang/String;

    .prologue
    .line 28
    monitor-enter p0

    const/4 v0, -0x1

    .line 29
    .local v0, "nResult":I
    const/4 v1, 0x0

    .line 31
    .local v1, "resData":Ljava/lang/String;
    :try_start_0
    array-length v4, p1

    sget-boolean v3, Lcom/sec/factory/aporiented/athandler/AtKstringb;->STAGE_PARALLEL:Z

    if-eqz v3, :cond_0

    iget v3, p0, Lcom/sec/factory/aporiented/athandler/AtKstringb;->NUM_ARGS:I

    add-int/lit8 v3, v3, 0x1

    :goto_0
    if-eq v4, v3, :cond_1

    .line 32
    invoke-virtual {p0}, Lcom/sec/factory/aporiented/athandler/AtKstringb;->responseNA()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    .line 86
    :goto_1
    monitor-exit p0

    return-object v3

    .line 31
    :cond_0
    :try_start_1
    iget v3, p0, Lcom/sec/factory/aporiented/athandler/AtKstringb;->NUM_ARGS:I

    goto :goto_0

    .line 35
    :cond_1
    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "1"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "0"

    aput-object v5, v3, v4

    invoke-virtual {p0, p1, v3}, Lcom/sec/factory/aporiented/athandler/AtKstringb;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 37
    sget-object v3, Lcom/sec/factory/aporiented/athandler/AtKstringb;->mModuleCommon:Lcom/sec/factory/modules/ModuleCommon;

    invoke-virtual {v3}, Lcom/sec/factory/modules/ModuleCommon;->readKeyStringBlock()Ljava/lang/String;

    move-result-object v2

    .line 38
    .local v2, "value":Ljava/lang/String;
    const/4 v3, 0x0

    aget-object v3, p1, v3

    invoke-virtual {p0, v3, v2}, Lcom/sec/factory/aporiented/athandler/AtKstringb;->responseString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .end local v2    # "value":Ljava/lang/String;
    :goto_2
    move-object v3, v1

    .line 86
    goto :goto_1

    .line 40
    :cond_2
    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "0"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "1"

    aput-object v5, v3, v4

    invoke-virtual {p0, p1, v3}, Lcom/sec/factory/aporiented/athandler/AtKstringb;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 42
    sget-object v3, Lcom/sec/factory/aporiented/athandler/AtKstringb;->mModuleCommon:Lcom/sec/factory/modules/ModuleCommon;

    const-string v4, "ON"

    invoke-virtual {v3, v4}, Lcom/sec/factory/modules/ModuleCommon;->writeKeyStringBlock(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 43
    const/4 v3, 0x0

    aget-object v3, p1, v3

    invoke-virtual {p0, v3}, Lcom/sec/factory/aporiented/athandler/AtKstringb;->responseOK(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    .line 45
    :cond_3
    const/4 v3, 0x0

    aget-object v3, p1, v3

    invoke-virtual {p0, v3}, Lcom/sec/factory/aporiented/athandler/AtKstringb;->responseNG(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    .line 48
    :cond_4
    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "0"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "0"

    aput-object v5, v3, v4

    invoke-virtual {p0, p1, v3}, Lcom/sec/factory/aporiented/athandler/AtKstringb;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-static {}, Landroid/os/FactoryTest;->isFactoryBinary()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 50
    sget-object v3, Lcom/sec/factory/aporiented/athandler/AtKstringb;->mModuleCommon:Lcom/sec/factory/modules/ModuleCommon;

    const-string v4, "OFF"

    invoke-virtual {v3, v4}, Lcom/sec/factory/modules/ModuleCommon;->writeKeyStringBlock(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 51
    const/4 v3, 0x0

    aget-object v3, p1, v3

    invoke-virtual {p0, v3}, Lcom/sec/factory/aporiented/athandler/AtKstringb;->responseOK(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    .line 53
    :cond_5
    const/4 v3, 0x0

    aget-object v3, p1, v3

    invoke-virtual {p0, v3}, Lcom/sec/factory/aporiented/athandler/AtKstringb;->responseNG(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    .line 62
    :cond_6
    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "0"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "4"

    aput-object v5, v3, v4

    invoke-virtual {p0, p1, v3}, Lcom/sec/factory/aporiented/athandler/AtKstringb;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 63
    sget-object v3, Lcom/sec/factory/aporiented/athandler/AtKstringb;->mModuleCommon:Lcom/sec/factory/modules/ModuleCommon;

    const-string v4, "OFF"

    invoke-virtual {v3, v4}, Lcom/sec/factory/modules/ModuleCommon;->writeFactoryMode(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 64
    const/4 v3, 0x0

    aget-object v3, p1, v3

    invoke-virtual {p0, v3}, Lcom/sec/factory/aporiented/athandler/AtKstringb;->responseOK(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_2

    .line 66
    :cond_7
    const/4 v3, 0x0

    aget-object v3, p1, v3

    invoke-virtual {p0, v3}, Lcom/sec/factory/aporiented/athandler/AtKstringb;->responseNG(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_2

    .line 69
    :cond_8
    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "0"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "5"

    aput-object v5, v3, v4

    invoke-virtual {p0, p1, v3}, Lcom/sec/factory/aporiented/athandler/AtKstringb;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 70
    sget-object v3, Lcom/sec/factory/aporiented/athandler/AtKstringb;->mModuleCommon:Lcom/sec/factory/modules/ModuleCommon;

    const-string v4, "ON"

    invoke-virtual {v3, v4}, Lcom/sec/factory/modules/ModuleCommon;->writeFactoryMode(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 71
    const/4 v3, 0x0

    aget-object v3, p1, v3

    invoke-virtual {p0, v3}, Lcom/sec/factory/aporiented/athandler/AtKstringb;->responseOK(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 72
    sget-object v3, Lcom/sec/factory/aporiented/athandler/AtKstringb;->mModuleCommon:Lcom/sec/factory/modules/ModuleCommon;

    invoke-virtual {v3}, Lcom/sec/factory/modules/ModuleCommon;->fsync()I

    move-result v0

    .line 73
    iget-object v3, p0, Lcom/sec/factory/aporiented/athandler/AtKstringb;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "DoShellCmd"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "sync result is "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_2

    .line 28
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3

    .line 75
    :cond_9
    const/4 v3, 0x0

    :try_start_2
    aget-object v3, p1, v3

    invoke-virtual {p0, v3}, Lcom/sec/factory/aporiented/athandler/AtKstringb;->responseNG(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_2

    .line 78
    :cond_a
    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "1"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "2"

    aput-object v5, v3, v4

    invoke-virtual {p0, p1, v3}, Lcom/sec/factory/aporiented/athandler/AtKstringb;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 79
    sget-object v3, Lcom/sec/factory/aporiented/athandler/AtKstringb;->mModuleCommon:Lcom/sec/factory/modules/ModuleCommon;

    invoke-virtual {v3}, Lcom/sec/factory/modules/ModuleCommon;->readFactoryMode()Ljava/lang/String;

    move-result-object v2

    .line 80
    .restart local v2    # "value":Ljava/lang/String;
    const/4 v3, 0x0

    aget-object v3, p1, v3

    invoke-virtual {p0, v3, v2}, Lcom/sec/factory/aporiented/athandler/AtKstringb;->responseString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 81
    goto/16 :goto_2

    .line 83
    .end local v2    # "value":Ljava/lang/String;
    :cond_b
    invoke-virtual {p0}, Lcom/sec/factory/aporiented/athandler/AtKstringb;->responseNA()Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v1

    goto/16 :goto_2
.end method
