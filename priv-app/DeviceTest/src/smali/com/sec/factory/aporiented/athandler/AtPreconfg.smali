.class public Lcom/sec/factory/aporiented/athandler/AtPreconfg;
.super Lcom/sec/factory/aporiented/athandler/AtCommandHandler;
.source "AtPreconfg.java"


# static fields
.field static final PRE_CODE:Ljava/lang/String; = "efs/imei/mps_code.dat"


# instance fields
.field private code:Ljava/lang/String;

.field public mReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/factory/aporiented/ResponseWriter;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "writer"    # Lcom/sec/factory/aporiented/ResponseWriter;

    .prologue
    .line 21
    invoke-direct {p0, p1}, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;-><init>(Landroid/content/Context;)V

    .line 16
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtPreconfg;->code:Ljava/lang/String;

    .line 105
    new-instance v0, Lcom/sec/factory/aporiented/athandler/AtPreconfg$1;

    invoke-direct {v0, p0}, Lcom/sec/factory/aporiented/athandler/AtPreconfg$1;-><init>(Lcom/sec/factory/aporiented/athandler/AtPreconfg;)V

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtPreconfg;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 22
    const-string v0, "PRECONFG"

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtPreconfg;->CMD_NAME:Ljava/lang/String;

    .line 23
    const-string v0, "AtPreconfg"

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtPreconfg;->CLASS_NAME:Ljava/lang/String;

    .line 24
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/factory/aporiented/athandler/AtPreconfg;->NUM_ARGS:I

    .line 25
    iput-object p2, p0, Lcom/sec/factory/aporiented/athandler/AtPreconfg;->writer:Lcom/sec/factory/aporiented/ResponseWriter;

    .line 33
    return-void
.end method


# virtual methods
.method public declared-synchronized handleCommand([Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p1, "argu"    # [Ljava/lang/String;

    .prologue
    .line 42
    monitor-enter p0

    const/4 v2, 0x0

    .line 44
    .local v2, "resData":Ljava/lang/String;
    :try_start_0
    array-length v5, p1

    sget-boolean v4, Lcom/sec/factory/aporiented/athandler/AtPreconfg;->STAGE_PARALLEL:Z

    if-eqz v4, :cond_0

    iget v4, p0, Lcom/sec/factory/aporiented/athandler/AtPreconfg;->NUM_ARGS:I

    add-int/lit8 v4, v4, 0x1

    :goto_0
    if-eq v5, v4, :cond_1

    .line 45
    invoke-virtual {p0}, Lcom/sec/factory/aporiented/athandler/AtPreconfg;->responseNA()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v4

    .line 90
    :goto_1
    monitor-exit p0

    return-object v4

    .line 44
    :cond_0
    :try_start_1
    iget v4, p0, Lcom/sec/factory/aporiented/athandler/AtPreconfg;->NUM_ARGS:I

    goto :goto_0

    .line 48
    :cond_1
    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "0"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "0"

    aput-object v6, v4, v5

    invoke-virtual {p0, p1, v4}, Lcom/sec/factory/aporiented/athandler/AtPreconfg;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 49
    iget-object v4, p0, Lcom/sec/factory/aporiented/athandler/AtPreconfg;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "handleCommand"

    const-string v6, "AtPreconfg=0,0"

    invoke-static {v4, v5, v6}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    invoke-virtual {p0}, Lcom/sec/factory/aporiented/athandler/AtPreconfg;->startReceiver()V

    .line 51
    new-instance v1, Landroid/content/Intent;

    const-string v4, "android.intent.action.CSC_COMPARE"

    invoke-direct {v1, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 52
    .local v1, "i":Landroid/content/Intent;
    iget-object v4, p0, Lcom/sec/factory/aporiented/athandler/AtPreconfg;->context:Landroid/content/Context;

    invoke-virtual {v4, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .end local v1    # "i":Landroid/content/Intent;
    :goto_2
    move-object v4, v2

    .line 90
    goto :goto_1

    .line 53
    :cond_2
    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "1"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "0"

    aput-object v6, v4, v5

    invoke-virtual {p0, p1, v4}, Lcom/sec/factory/aporiented/athandler/AtPreconfg;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 54
    iget-object v4, p0, Lcom/sec/factory/aporiented/athandler/AtPreconfg;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "handleCommand"

    const-string v6, "AtPreconfg=1,0"

    invoke-static {v4, v5, v6}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    const-string v4, "efs/imei/mps_code.dat"

    invoke-virtual {p0, v4}, Lcom/sec/factory/aporiented/athandler/AtPreconfg;->isFileExist(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 57
    const/4 v4, 0x0

    aget-object v4, p1, v4

    invoke-virtual {p0, v4}, Lcom/sec/factory/aporiented/athandler/AtPreconfg;->responseNG(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_2

    .line 59
    :cond_3
    const-string v4, "efs/imei/mps_code.dat"

    invoke-virtual {p0, v4}, Lcom/sec/factory/aporiented/athandler/AtPreconfg;->readOneLine(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 60
    .local v3, "value":Ljava/lang/String;
    const/4 v4, 0x0

    aget-object v4, p1, v4

    invoke-virtual {p0, v4, v3}, Lcom/sec/factory/aporiented/athandler/AtPreconfg;->responseString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 61
    goto :goto_2

    .line 62
    .end local v3    # "value":Ljava/lang/String;
    :cond_4
    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "2"

    aput-object v6, v4, v5

    invoke-virtual {p0, p1, v4}, Lcom/sec/factory/aporiented/athandler/AtPreconfg;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 63
    iget-object v4, p0, Lcom/sec/factory/aporiented/athandler/AtPreconfg;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "handleCommand"

    const-string v6, "AtPreconfg=2,STRING"

    invoke-static {v4, v5, v6}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    const/4 v4, 0x1

    aget-object v0, p1, v4

    .line 66
    .local v0, "buyerCode":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    const/4 v5, 0x3

    if-eq v4, v5, :cond_5

    .line 67
    const/4 v4, 0x0

    aget-object v4, p1, v4

    invoke-virtual {p0, v4}, Lcom/sec/factory/aporiented/athandler/AtPreconfg;->responseNG(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_2

    .line 74
    :cond_5
    const-string v4, "efs/imei/mps_code.dat"

    invoke-virtual {p0, v4}, Lcom/sec/factory/aporiented/athandler/AtPreconfg;->isFileExist(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_6

    .line 75
    const/4 v4, 0x0

    aget-object v4, p1, v4

    invoke-virtual {p0, v4}, Lcom/sec/factory/aporiented/athandler/AtPreconfg;->responseNG(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_2

    .line 77
    :cond_6
    const-string v4, "efs/imei/mps_code.dat"

    const/4 v5, 0x1

    aget-object v5, p1, v5

    invoke-virtual {p0, v4, v5}, Lcom/sec/factory/aporiented/athandler/AtPreconfg;->writeFile(Ljava/lang/String;Ljava/lang/String;)Z

    .line 79
    const/4 v4, 0x0

    aget-object v4, p1, v4

    invoke-virtual {p0, v4}, Lcom/sec/factory/aporiented/athandler/AtPreconfg;->responseOK(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_2

    .line 87
    .end local v0    # "buyerCode":Ljava/lang/String;
    :cond_7
    invoke-virtual {p0}, Lcom/sec/factory/aporiented/athandler/AtPreconfg;->responseNA()Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v2

    goto/16 :goto_2

    .line 42
    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4
.end method

.method public isFileExist(Ljava/lang/String;)Z
    .locals 2
    .param p1, "aFile"    # Ljava/lang/String;

    .prologue
    .line 94
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 95
    .local v0, "f":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    return v1
.end method

.method public startReceiver()V
    .locals 3

    .prologue
    .line 99
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 101
    .local v0, "mFilter":Landroid/content/IntentFilter;
    const-string v1, "com.sec.factory.aporiented.athandler.atpreconfg"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 102
    iget-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtPreconfg;->context:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/factory/aporiented/athandler/AtPreconfg;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 103
    return-void
.end method
