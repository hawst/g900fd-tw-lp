.class public Lcom/sec/factory/aporiented/RegisterFactoryBinHandler;
.super Ljava/lang/Object;
.source "RegisterFactoryBinHandler.java"


# static fields
.field public static final CLASS_NAME:Ljava/lang/String; = "RegisterFactoryBinHandler"


# direct methods
.method public constructor <init>(Ljava/util/HashMap;Landroid/content/Context;Lcom/sec/factory/aporiented/ResponseWriter;)V
    .locals 0
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "writer"    # Lcom/sec/factory/aporiented/ResponseWriter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/factory/aporiented/athandler/AtCommandHandler;",
            ">;",
            "Landroid/content/Context;",
            "Lcom/sec/factory/aporiented/ResponseWriter;",
            ")V"
        }
    .end annotation

    .prologue
    .line 14
    .local p1, "atHandlers":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/sec/factory/aporiented/athandler/AtCommandHandler;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    return-void
.end method


# virtual methods
.method public registerHandler()V
    .locals 3

    .prologue
    .line 19
    const-string v0, "RegisterFactoryBinHandler"

    const-string v1, "registerHandler"

    const-string v2, "Register AT command handler for FACTORY BIN"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 20
    const-string v0, "RegisterFactoryBinHandler"

    const-string v1, "registerHandler"

    const-string v2, "Do nothing!!!"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    return-void
.end method
