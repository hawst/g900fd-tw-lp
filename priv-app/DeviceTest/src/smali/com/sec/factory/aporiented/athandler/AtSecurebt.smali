.class public Lcom/sec/factory/aporiented/athandler/AtSecurebt;
.super Lcom/sec/factory/aporiented/athandler/AtCommandHandler;
.source "AtSecurebt.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 8
    invoke-direct {p0, p1}, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;-><init>(Landroid/content/Context;)V

    .line 9
    const-string v0, "SECUREBT"

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtSecurebt;->CMD_NAME:Ljava/lang/String;

    .line 10
    const-string v0, "AtSecurebt"

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtSecurebt;->CLASS_NAME:Ljava/lang/String;

    .line 11
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/factory/aporiented/athandler/AtSecurebt;->NUM_ARGS:I

    .line 12
    return-void
.end method


# virtual methods
.method public declared-synchronized handleCommand([Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p1, "argu"    # [Ljava/lang/String;

    .prologue
    .line 16
    monitor-enter p0

    const/4 v2, 0x0

    .line 17
    .local v2, "resData":Ljava/lang/String;
    const/4 v3, 0x0

    .line 19
    .local v3, "result":Ljava/lang/String;
    :try_start_0
    array-length v5, p1

    sget-boolean v4, Lcom/sec/factory/aporiented/athandler/AtSecurebt;->STAGE_PARALLEL:Z

    if-eqz v4, :cond_0

    iget v4, p0, Lcom/sec/factory/aporiented/athandler/AtSecurebt;->NUM_ARGS:I

    add-int/lit8 v4, v4, 0x1

    :goto_0
    if-eq v5, v4, :cond_1

    .line 20
    invoke-virtual {p0}, Lcom/sec/factory/aporiented/athandler/AtSecurebt;->responseNA()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v4

    .line 56
    :goto_1
    monitor-exit p0

    return-object v4

    .line 19
    :cond_0
    :try_start_1
    iget v4, p0, Lcom/sec/factory/aporiented/athandler/AtSecurebt;->NUM_ARGS:I

    goto :goto_0

    .line 23
    :cond_1
    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "1"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "1"

    aput-object v6, v4, v5

    const/4 v5, 0x2

    const-string v6, "0"

    aput-object v6, v4, v5

    invoke-virtual {p0, p1, v4}, Lcom/sec/factory/aporiented/athandler/AtSecurebt;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 24
    const-string v4, "ro.csb_val"

    invoke-static {v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 26
    .local v1, "keyIndex":Ljava/lang/String;
    const-string v4, "1"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 27
    const/4 v4, 0x0

    aget-object v4, p1, v4

    const-string v5, "NO"

    invoke-virtual {p0, v4, v5}, Lcom/sec/factory/aporiented/athandler/AtSecurebt;->responseString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .end local v1    # "keyIndex":Ljava/lang/String;
    :goto_2
    move-object v4, v2

    .line 56
    goto :goto_1

    .line 28
    .restart local v1    # "keyIndex":Ljava/lang/String;
    :cond_2
    const-string v4, "2"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 29
    const/4 v4, 0x0

    aget-object v4, p1, v4

    const-string v5, "K1"

    invoke-virtual {p0, v4, v5}, Lcom/sec/factory/aporiented/athandler/AtSecurebt;->responseString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_2

    .line 30
    :cond_3
    const-string v4, "3"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 31
    const/4 v4, 0x0

    aget-object v4, p1, v4

    const-string v5, "K2"

    invoke-virtual {p0, v4, v5}, Lcom/sec/factory/aporiented/athandler/AtSecurebt;->responseString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_2

    .line 32
    :cond_4
    const-string v4, "4"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 33
    const/4 v4, 0x0

    aget-object v4, p1, v4

    const-string v5, "K3"

    invoke-virtual {p0, v4, v5}, Lcom/sec/factory/aporiented/athandler/AtSecurebt;->responseString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_2

    .line 41
    :cond_5
    invoke-virtual {p0}, Lcom/sec/factory/aporiented/athandler/AtSecurebt;->responseNA()Ljava/lang/String;

    move-result-object v2

    goto :goto_2

    .line 45
    .end local v1    # "keyIndex":Ljava/lang/String;
    :cond_6
    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "1"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "2"

    aput-object v6, v4, v5

    const/4 v5, 0x2

    const-string v6, "0"

    aput-object v6, v4, v5

    invoke-virtual {p0, p1, v4}, Lcom/sec/factory/aporiented/athandler/AtSecurebt;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v4

    if-eqz v4, :cond_7

    .line 47
    :try_start_2
    sget-object v4, Lcom/sec/factory/aporiented/athandler/AtSecurebt;->mModuleDevice:Lcom/sec/factory/modules/ModuleDevice;

    invoke-virtual {v4}, Lcom/sec/factory/modules/ModuleDevice;->checkWarrnatyBit()Ljava/lang/String;

    move-result-object v3

    .line 48
    const/4 v4, 0x0

    aget-object v4, p1, v4

    invoke-virtual {p0, v4, v3}, Lcom/sec/factory/aporiented/athandler/AtSecurebt;->responseString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v2

    goto :goto_2

    .line 49
    :catch_0
    move-exception v0

    .line 50
    .local v0, "e":Ljava/lang/NullPointerException;
    const/4 v4, 0x0

    :try_start_3
    aget-object v4, p1, v4

    invoke-virtual {p0, v4}, Lcom/sec/factory/aporiented/athandler/AtSecurebt;->responseNG(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 51
    goto :goto_2

    .line 54
    .end local v0    # "e":Ljava/lang/NullPointerException;
    :cond_7
    invoke-virtual {p0}, Lcom/sec/factory/aporiented/athandler/AtSecurebt;->responseNA()Ljava/lang/String;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v2

    goto :goto_2

    .line 16
    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4
.end method
