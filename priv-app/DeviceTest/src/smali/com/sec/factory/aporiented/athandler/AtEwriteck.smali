.class public Lcom/sec/factory/aporiented/athandler/AtEwriteck;
.super Lcom/sec/factory/aporiented/athandler/AtCommandHandler;
.source "AtEwriteck.java"


# static fields
.field static rcvIntent:Z


# instance fields
.field SpenDetectionReceiver:Landroid/content/BroadcastReceiver;

.field spenInserted:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/factory/aporiented/athandler/AtEwriteck;->rcvIntent:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/factory/aporiented/ResponseWriter;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "writer"    # Lcom/sec/factory/aporiented/ResponseWriter;

    .prologue
    .line 24
    invoke-direct {p0, p1}, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;-><init>(Landroid/content/Context;)V

    .line 19
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/factory/aporiented/athandler/AtEwriteck;->spenInserted:Z

    .line 25
    const-string v0, "EWRITECK"

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtEwriteck;->CMD_NAME:Ljava/lang/String;

    .line 26
    const-string v0, "AtEwriteck"

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtEwriteck;->CLASS_NAME:Ljava/lang/String;

    .line 27
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/factory/aporiented/athandler/AtEwriteck;->NUM_ARGS:I

    .line 28
    iput-object p2, p0, Lcom/sec/factory/aporiented/athandler/AtEwriteck;->writer:Lcom/sec/factory/aporiented/ResponseWriter;

    .line 29
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/factory/cporiented/ResponseWriterCPO;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "writer"    # Lcom/sec/factory/cporiented/ResponseWriterCPO;

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;-><init>(Landroid/content/Context;)V

    .line 19
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/factory/aporiented/athandler/AtEwriteck;->spenInserted:Z

    .line 33
    const-string v0, "EWRITECK"

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtEwriteck;->CMD_NAME:Ljava/lang/String;

    .line 34
    const-string v0, "AtEwriteck"

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtEwriteck;->CLASS_NAME:Ljava/lang/String;

    .line 35
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/factory/aporiented/athandler/AtEwriteck;->NUM_ARGS:I

    .line 36
    iput-object p2, p0, Lcom/sec/factory/aporiented/athandler/AtEwriteck;->writerCpo:Lcom/sec/factory/cporiented/ResponseWriterCPO;

    .line 37
    return-void
.end method


# virtual methods
.method public declared-synchronized handleCommand([Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p1, "argu"    # [Ljava/lang/String;

    .prologue
    .line 41
    monitor-enter p0

    const/4 v2, 0x0

    .line 42
    .local v2, "resData":Ljava/lang/String;
    :try_start_0
    new-instance v4, Lcom/sec/factory/aporiented/athandler/AtEwriteck$1;

    invoke-direct {v4, p0}, Lcom/sec/factory/aporiented/athandler/AtEwriteck$1;-><init>(Lcom/sec/factory/aporiented/athandler/AtEwriteck;)V

    iput-object v4, p0, Lcom/sec/factory/aporiented/athandler/AtEwriteck;->SpenDetectionReceiver:Landroid/content/BroadcastReceiver;

    .line 62
    new-instance v1, Landroid/content/IntentFilter;

    const-string v4, "com.samsung.pen.INSERT"

    invoke-direct {v1, v4}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 63
    .local v1, "filter":Landroid/content/IntentFilter;
    iget-object v4, p0, Lcom/sec/factory/aporiented/athandler/AtEwriteck;->context:Landroid/content/Context;

    iget-object v5, p0, Lcom/sec/factory/aporiented/athandler/AtEwriteck;->SpenDetectionReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v4, v5, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 64
    iget-object v4, p0, Lcom/sec/factory/aporiented/athandler/AtEwriteck;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "handleCommand"

    const-string v6, "registerReceiver"

    invoke-static {v4, v5, v6}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    array-length v5, p1

    sget-boolean v4, Lcom/sec/factory/aporiented/athandler/AtEwriteck;->STAGE_PARALLEL:Z

    if-eqz v4, :cond_0

    iget v4, p0, Lcom/sec/factory/aporiented/athandler/AtEwriteck;->NUM_ARGS:I

    add-int/lit8 v4, v4, 0x1

    :goto_0
    if-eq v5, v4, :cond_1

    .line 67
    invoke-virtual {p0}, Lcom/sec/factory/aporiented/athandler/AtEwriteck;->responseNA()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v4

    .line 128
    :goto_1
    monitor-exit p0

    return-object v4

    .line 66
    :cond_0
    :try_start_1
    iget v4, p0, Lcom/sec/factory/aporiented/athandler/AtEwriteck;->NUM_ARGS:I

    goto :goto_0

    .line 70
    :cond_1
    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "0"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "0"

    aput-object v6, v4, v5

    invoke-virtual {p0, p1, v4}, Lcom/sec/factory/aporiented/athandler/AtEwriteck;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 73
    invoke-virtual {p0}, Lcom/sec/factory/aporiented/athandler/AtEwriteck;->responseNA()Ljava/lang/String;

    move-result-object v2

    .line 121
    :cond_2
    :goto_2
    invoke-virtual {p0}, Lcom/sec/factory/aporiented/athandler/AtEwriteck;->getCmdType()I

    move-result v4

    if-eqz v4, :cond_3

    .line 122
    sget-boolean v4, Lcom/sec/factory/aporiented/athandler/AtEwriteck;->rcvIntent:Z

    if-eqz v4, :cond_3

    .line 123
    iget-object v4, p0, Lcom/sec/factory/aporiented/athandler/AtEwriteck;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "handleCommand"

    const-string v6, "unregisterReceiver"

    invoke-static {v4, v5, v6}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    const/4 v4, 0x0

    sput-boolean v4, Lcom/sec/factory/aporiented/athandler/AtEwriteck;->rcvIntent:Z

    .line 125
    iget-object v4, p0, Lcom/sec/factory/aporiented/athandler/AtEwriteck;->context:Landroid/content/Context;

    iget-object v5, p0, Lcom/sec/factory/aporiented/athandler/AtEwriteck;->SpenDetectionReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v4, v5}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    :cond_3
    move-object v4, v2

    .line 128
    goto :goto_1

    .line 74
    :cond_4
    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "0"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "1"

    aput-object v6, v4, v5

    invoke-virtual {p0, p1, v4}, Lcom/sec/factory/aporiented/athandler/AtEwriteck;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 77
    const-string v4, "EPEN_DIGITIZER_CHECK"

    invoke-static {v4}, Lcom/sec/factory/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 79
    .local v3, "result":Ljava/lang/String;
    const-string v4, "OK"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 80
    const/4 v4, 0x0

    aget-object v4, p1, v4

    invoke-virtual {p0, v4}, Lcom/sec/factory/aporiented/athandler/AtEwriteck;->responseOK(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_2

    .line 82
    :cond_5
    iget-object v4, p0, Lcom/sec/factory/aporiented/athandler/AtEwriteck;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "AtEwriteck"

    const-string v6, " Sysfs file\'s value is not OK"

    invoke-static {v4, v5, v6}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    const/4 v4, 0x0

    aget-object v4, p1, v4

    invoke-virtual {p0, v4}, Lcom/sec/factory/aporiented/athandler/AtEwriteck;->responseNG(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_2

    .line 85
    .end local v3    # "result":Ljava/lang/String;
    :cond_6
    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "0"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "2"

    aput-object v6, v4, v5

    invoke-virtual {p0, p1, v4}, Lcom/sec/factory/aporiented/athandler/AtEwriteck;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 88
    new-instance v0, Landroid/content/Intent;

    const-string v4, "com.sec.factory.athandler.SpenLock"

    invoke-direct {v0, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 89
    .local v0, "SpenLockIntent":Landroid/content/Intent;
    const-string v4, "data"

    const/4 v5, 0x1

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 90
    iget-object v4, p0, Lcom/sec/factory/aporiented/athandler/AtEwriteck;->context:Landroid/content/Context;

    invoke-virtual {v4, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 91
    const/4 v4, 0x0

    aget-object v4, p1, v4

    invoke-virtual {p0, v4}, Lcom/sec/factory/aporiented/athandler/AtEwriteck;->responseOK(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 92
    goto/16 :goto_2

    .end local v0    # "SpenLockIntent":Landroid/content/Intent;
    :cond_7
    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "0"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "3"

    aput-object v6, v4, v5

    invoke-virtual {p0, p1, v4}, Lcom/sec/factory/aporiented/athandler/AtEwriteck;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 95
    new-instance v0, Landroid/content/Intent;

    const-string v4, "com.sec.factory.athandler.SpenLock"

    invoke-direct {v0, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 96
    .restart local v0    # "SpenLockIntent":Landroid/content/Intent;
    const-string v4, "data"

    const/4 v5, 0x0

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 97
    iget-object v4, p0, Lcom/sec/factory/aporiented/athandler/AtEwriteck;->context:Landroid/content/Context;

    invoke-virtual {v4, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 98
    const/4 v4, 0x0

    aget-object v4, p1, v4

    invoke-virtual {p0, v4}, Lcom/sec/factory/aporiented/athandler/AtEwriteck;->responseOK(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 99
    goto/16 :goto_2

    .end local v0    # "SpenLockIntent":Landroid/content/Intent;
    :cond_8
    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "1"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "0"

    aput-object v6, v4, v5

    invoke-virtual {p0, p1, v4}, Lcom/sec/factory/aporiented/athandler/AtEwriteck;->checkArgu([Ljava/lang/String;[Ljava/lang/String;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v4

    if-eqz v4, :cond_a

    .line 104
    const-wide/16 v4, 0x5dc

    :try_start_2
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 108
    :goto_3
    :try_start_3
    iget-boolean v4, p0, Lcom/sec/factory/aporiented/athandler/AtEwriteck;->spenInserted:Z

    if-eqz v4, :cond_9

    .line 109
    const/4 v4, 0x0

    aget-object v4, p1, v4

    invoke-virtual {p0, v4}, Lcom/sec/factory/aporiented/athandler/AtEwriteck;->responseOK(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 114
    :goto_4
    invoke-virtual {p0}, Lcom/sec/factory/aporiented/athandler/AtEwriteck;->getCmdType()I

    move-result v4

    if-nez v4, :cond_2

    .line 115
    const-string v2, "WAIT"

    goto/16 :goto_2

    .line 111
    :cond_9
    const/4 v4, 0x0

    aget-object v4, p1, v4

    invoke-virtual {p0, v4}, Lcom/sec/factory/aporiented/athandler/AtEwriteck;->responseNG(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_4

    .line 118
    :cond_a
    invoke-virtual {p0}, Lcom/sec/factory/aporiented/athandler/AtEwriteck;->responseNA()Ljava/lang/String;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v2

    goto/16 :goto_2

    .line 105
    :catch_0
    move-exception v4

    goto :goto_3

    .line 41
    .end local v1    # "filter":Landroid/content/IntentFilter;
    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4
.end method
