.class Lcom/sec/factory/aporiented/FtClient$3;
.super Ljava/lang/Object;
.source "FtClient.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/factory/aporiented/FtClient;->sendBootCompleted()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/factory/aporiented/FtClient;


# direct methods
.method constructor <init>(Lcom/sec/factory/aporiented/FtClient;)V
    .locals 0

    .prologue
    .line 323
    iput-object p1, p0, Lcom/sec/factory/aporiented/FtClient$3;->this$0:Lcom/sec/factory/aporiented/FtClient;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 325
    iget-object v0, p0, Lcom/sec/factory/aporiented/FtClient$3;->this$0:Lcom/sec/factory/aporiented/FtClient;

    iget-object v0, v0, Lcom/sec/factory/aporiented/FtClient;->clientSocket_recv:Landroid/net/LocalSocket;

    invoke-virtual {v0}, Landroid/net/LocalSocket;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/factory/aporiented/FtClient$3;->this$0:Lcom/sec/factory/aporiented/FtClient;

    iget-object v0, v0, Lcom/sec/factory/aporiented/FtClient;->clientSocket_send:Landroid/net/LocalSocket;

    invoke-virtual {v0}, Landroid/net/LocalSocket;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 327
    const-string v0, "FtClient"

    const-string v1, "mBroadcastReceiver"

    const-string v2, "BOOTING COMPLETED!!"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 329
    iget-object v0, p0, Lcom/sec/factory/aporiented/FtClient$3;->this$0:Lcom/sec/factory/aporiented/FtClient;

    iget-object v0, v0, Lcom/sec/factory/aporiented/FtClient;->writer:Lcom/sec/factory/aporiented/ResponseWriter;

    const-string v1, "BOOTING COMPLETED\r\n"

    invoke-virtual {v0, v1}, Lcom/sec/factory/aporiented/ResponseWriter;->write(Ljava/lang/String;)Z

    .line 335
    :goto_0
    return-void

    .line 331
    :cond_0
    const-string v0, "FtClient"

    const-string v1, "mBroadcastReceiver"

    const-string v2, "clientSocket is not Ready"

    invoke-static {v0, v1, v2}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 333
    iget-object v0, p0, Lcom/sec/factory/aporiented/FtClient$3;->this$0:Lcom/sec/factory/aporiented/FtClient;

    # getter for: Lcom/sec/factory/aporiented/FtClient;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/factory/aporiented/FtClient;->access$000(Lcom/sec/factory/aporiented/FtClient;)Landroid/os/Handler;

    move-result-object v0

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, p0, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method
