.class public Lcom/sec/factory/aporiented/athandler/AtFactolog;
.super Lcom/sec/factory/aporiented/athandler/AtCommandHandler;
.source "AtFactolog.java"


# static fields
.field public static final REQUEST_UPDATE:Ljava/lang/String; = "com.sec.factory.app.systeminfo.update"

.field private static final STAGE_PROPERTY:Ljava/lang/String; = "ril.factory_mode"


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 15
    invoke-direct {p0, p1}, Lcom/sec/factory/aporiented/athandler/AtCommandHandler;-><init>(Landroid/content/Context;)V

    .line 16
    const-string v0, "FACTOLOG"

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFactolog;->CMD_NAME:Ljava/lang/String;

    .line 17
    const-string v0, "AtFactolog"

    iput-object v0, p0, Lcom/sec/factory/aporiented/athandler/AtFactolog;->CLASS_NAME:Ljava/lang/String;

    .line 18
    const/4 v0, 0x4

    iput v0, p0, Lcom/sec/factory/aporiented/athandler/AtFactolog;->NUM_ARGS:I

    .line 19
    iput-object p1, p0, Lcom/sec/factory/aporiented/athandler/AtFactolog;->mContext:Landroid/content/Context;

    .line 20
    return-void
.end method

.method private sendSystemInfoViewUpdate()V
    .locals 4

    .prologue
    .line 112
    iget-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtFactolog;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "sendSystemInfoViewUpdate"

    const-string v3, "com.sec.factory.app.systeminfo.update"

    invoke-static {v1, v2, v3}, Lcom/sec/factory/support/FtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.factory.app.systeminfo.update"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 115
    .local v0, "updateIntent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/sec/factory/aporiented/athandler/AtFactolog;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 116
    return-void
.end method


# virtual methods
.method public declared-synchronized handleCommand([Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1, "argu"    # [Ljava/lang/String;

    .prologue
    .line 28
    monitor-enter p0

    const/4 v1, 0x0

    .line 30
    .local v1, "resData":Ljava/lang/String;
    :try_start_0
    array-length v3, p1

    sget-boolean v2, Lcom/sec/factory/aporiented/athandler/AtFactolog;->STAGE_PARALLEL:Z

    if-eqz v2, :cond_0

    iget v2, p0, Lcom/sec/factory/aporiented/athandler/AtFactolog;->NUM_ARGS:I

    add-int/lit8 v2, v2, 0x1

    :goto_0
    if-eq v3, v2, :cond_1

    .line 31
    invoke-virtual {p0}, Lcom/sec/factory/aporiented/athandler/AtFactolog;->responseNA()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 107
    :goto_1
    monitor-exit p0

    return-object v2

    .line 30
    :cond_0
    :try_start_1
    iget v2, p0, Lcom/sec/factory/aporiented/athandler/AtFactolog;->NUM_ARGS:I

    goto :goto_0

    .line 51
    :cond_1
    const/4 v2, 0x2

    aget-object v2, p1, v2

    const-string v3, "1"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 55
    const/4 v2, 0x0

    aget-object v2, p1, v2

    invoke-virtual {p0, v2}, Lcom/sec/factory/aporiented/athandler/AtFactolog;->responseOK(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 56
    sget-object v2, Lcom/sec/factory/aporiented/athandler/AtFactolog;->mModulePower:Lcom/sec/factory/modules/ModulePower;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/sec/factory/modules/ModulePower;->doWakeLock(Z)V

    .line 57
    sget-object v2, Lcom/sec/factory/aporiented/athandler/AtFactolog;->mModulePower:Lcom/sec/factory/modules/ModulePower;

    invoke-virtual {v2}, Lcom/sec/factory/modules/ModulePower;->sendDvfsLockIntent()V

    .line 58
    sget-object v2, Lcom/sec/factory/aporiented/athandler/AtFactolog;->mModulePower:Lcom/sec/factory/modules/ModulePower;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/sec/factory/modules/ModulePower;->sendAlarmManagerOnOff(Z)V

    .line 59
    sget-object v2, Lcom/sec/factory/aporiented/athandler/AtFactolog;->mModulePower:Lcom/sec/factory/modules/ModulePower;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/sec/factory/modules/ModulePower;->setFactoryModeAtBatteryNode(Z)V

    .line 60
    sget-object v2, Lcom/sec/factory/aporiented/athandler/AtFactolog;->mModuleCommon:Lcom/sec/factory/modules/ModuleCommon;

    invoke-virtual {v2}, Lcom/sec/factory/modules/ModuleCommon;->setSwitchFactoryState()V

    .line 62
    const-string v2, "NEED_NOTI_AUDIO_MANAGER"

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/sec/factory/support/Support$Feature;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 63
    sget-object v2, Lcom/sec/factory/aporiented/athandler/AtFactolog;->mModuleAudio:Lcom/sec/factory/modules/ModuleAudio;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/sec/factory/modules/ModuleAudio;->sendToAudioManagerFTAOnOff(Z)V

    .line 68
    :goto_2
    const-string v2, "NEED_LPM_MODE_SET"

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/sec/factory/support/Support$Feature;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 69
    sget-object v2, Lcom/sec/factory/aporiented/athandler/AtFactolog;->mModuleDevice:Lcom/sec/factory/modules/ModuleDevice;

    const-string v3, "0"

    invoke-virtual {v2, v3}, Lcom/sec/factory/modules/ModuleDevice;->setLPMmode(Ljava/lang/String;)V

    .line 73
    :cond_2
    const-string v2, "SUPPORT_ADC_ERROR_SYSFS"

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/sec/factory/support/Support$Feature;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 74
    iget-object v2, p0, Lcom/sec/factory/aporiented/athandler/AtFactolog;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "handleCommand()"

    const-string v4, "Temporary Logic for S LTE model (Final Test - Start)"

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    const-string v2, "ADC_ERR_CONTROL"

    const-string v3, "1"

    invoke-static {v2, v3}, Lcom/sec/factory/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    .line 78
    :cond_3
    const-string v2, "ril.factory_mode"

    const-string v3, "none"

    invoke-static {v2, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 79
    .local v0, "currentStage":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/factory/aporiented/athandler/AtFactolog;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "handleCommand()"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "FT TYPE: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .end local v0    # "currentStage":Ljava/lang/String;
    :cond_4
    :goto_3
    move-object v2, v1

    .line 107
    goto/16 :goto_1

    .line 65
    :cond_5
    iget-object v2, p0, Lcom/sec/factory/aporiented/athandler/AtFactolog;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "handleCommand()"

    const-string v4, "sendToAudioManagerFTAOnOff : M0_EUR case"

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 28
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 80
    :cond_6
    const/4 v2, 0x2

    :try_start_2
    aget-object v2, p1, v2

    const-string v3, "0"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 81
    const/4 v2, 0x0

    aget-object v2, p1, v2

    invoke-virtual {p0, v2}, Lcom/sec/factory/aporiented/athandler/AtFactolog;->responseOK(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 82
    sget-object v2, Lcom/sec/factory/aporiented/athandler/AtFactolog;->mModulePower:Lcom/sec/factory/modules/ModulePower;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/sec/factory/modules/ModulePower;->setFactoryModeAtBatteryNode(Z)V

    .line 84
    const-string v2, "NEED_NOTI_AUDIO_MANAGER"

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/sec/factory/support/Support$Feature;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 85
    sget-object v2, Lcom/sec/factory/aporiented/athandler/AtFactolog;->mModuleAudio:Lcom/sec/factory/modules/ModuleAudio;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/sec/factory/modules/ModuleAudio;->sendToAudioManagerFTAOnOff(Z)V

    .line 90
    :goto_4
    const-string v2, "FACTOLOG_SYSTEM_INFO_UPDATE"

    invoke-static {v2}, Lcom/sec/factory/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-static {}, Landroid/os/FactoryTest;->isFactoryBinary()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 92
    invoke-direct {p0}, Lcom/sec/factory/aporiented/athandler/AtFactolog;->sendSystemInfoViewUpdate()V

    .line 96
    :cond_7
    const-string v2, "SUPPORT_ADC_ERROR_SYSFS"

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/sec/factory/support/Support$Feature;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 97
    iget-object v2, p0, Lcom/sec/factory/aporiented/athandler/AtFactolog;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "handleCommand()"

    const-string v4, "Temporary Logic for S LTE model (Final Test - End)"

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    const-string v2, "ADC_ERR_CONTROL"

    const-string v3, "0"

    invoke-static {v2, v3}, Lcom/sec/factory/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    goto :goto_3

    .line 87
    :cond_8
    iget-object v2, p0, Lcom/sec/factory/aporiented/athandler/AtFactolog;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "handleCommand()"

    const-string v4, "sendToAudioManagerFTAOnOff : M0_EUR case"

    invoke-static {v2, v3, v4}, Lcom/sec/factory/support/FtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    .line 104
    :cond_9
    invoke-virtual {p0}, Lcom/sec/factory/aporiented/athandler/AtFactolog;->responseNA()Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v1

    goto :goto_3
.end method
