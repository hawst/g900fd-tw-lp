.class public Lcom/sec/dsm/system/DSMTrackingService;
.super Landroid/app/Service;
.source "DSMTrackingService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/dsm/system/DSMTrackingService$1;,
        Lcom/sec/dsm/system/DSMTrackingService$MobileTrackingTimer;,
        Lcom/sec/dsm/system/DSMTrackingService$GPSTrackingTimer;
    }
.end annotation


# static fields
.field public static final DATE_FORMAT:Ljava/lang/String; = "yyyy-MM-dd\'T\'HH:mm:ss\'Z\'"

.field public static final DSM_TRACKING_SERVICE:Ljava/lang/String; = "android.intent.action.dsm.TRACKING_SERVICE"

.field public static final TRACKING_DATE_FORMAT:Ljava/lang/String; = "yyyyMMdd HH:mm:ss"

.field private static TrackingWakeLock:Landroid/os/PowerManager$WakeLock;

.field static accuracy:Ljava/lang/String;

.field static altitude:Ljava/lang/String;

.field static course:Ljava/lang/String;

.field static date:Ljava/lang/String;

.field static endTime:Ljava/lang/String;

.field private static gpsData:Lcom/sec/dsm/system/GPSUtil;

.field static interval:Ljava/lang/String;

.field static latitude:Ljava/lang/String;

.field static longitude:Ljava/lang/String;

.field static mReTrackingStatus:Z

.field static mobileTrackingTimer:Lcom/sec/dsm/system/DSMTrackingService$MobileTrackingTimer;

.field static speed:Ljava/lang/String;

.field static startTime:Ljava/lang/String;


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 46
    sput-object v0, Lcom/sec/dsm/system/DSMTrackingService;->interval:Ljava/lang/String;

    .line 47
    sput-object v0, Lcom/sec/dsm/system/DSMTrackingService;->startTime:Ljava/lang/String;

    .line 48
    sput-object v0, Lcom/sec/dsm/system/DSMTrackingService;->endTime:Ljava/lang/String;

    .line 49
    sput-object v0, Lcom/sec/dsm/system/DSMTrackingService;->latitude:Ljava/lang/String;

    .line 50
    sput-object v0, Lcom/sec/dsm/system/DSMTrackingService;->longitude:Ljava/lang/String;

    .line 51
    sput-object v0, Lcom/sec/dsm/system/DSMTrackingService;->altitude:Ljava/lang/String;

    .line 52
    sput-object v0, Lcom/sec/dsm/system/DSMTrackingService;->speed:Ljava/lang/String;

    .line 53
    sput-object v0, Lcom/sec/dsm/system/DSMTrackingService;->course:Ljava/lang/String;

    .line 54
    sput-object v0, Lcom/sec/dsm/system/DSMTrackingService;->accuracy:Ljava/lang/String;

    .line 55
    sput-object v0, Lcom/sec/dsm/system/DSMTrackingService;->date:Ljava/lang/String;

    .line 56
    sput-object v0, Lcom/sec/dsm/system/DSMTrackingService;->TrackingWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 57
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/dsm/system/DSMTrackingService;->mReTrackingStatus:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 306
    return-void
.end method

.method static synthetic access$100()V
    .locals 0

    .prologue
    .line 38
    invoke-static {}, Lcom/sec/dsm/system/DSMTrackingService;->wsMobileTrackingAdpSetTrackingLocation()V

    return-void
.end method

.method static synthetic access$600()Landroid/os/PowerManager$WakeLock;
    .locals 1

    .prologue
    .line 38
    sget-object v0, Lcom/sec/dsm/system/DSMTrackingService;->TrackingWakeLock:Landroid/os/PowerManager$WakeLock;

    return-object v0
.end method

.method static synthetic access$602(Landroid/os/PowerManager$WakeLock;)Landroid/os/PowerManager$WakeLock;
    .locals 0
    .param p0, "x0"    # Landroid/os/PowerManager$WakeLock;

    .prologue
    .line 38
    sput-object p0, Lcom/sec/dsm/system/DSMTrackingService;->TrackingWakeLock:Landroid/os/PowerManager$WakeLock;

    return-object p0
.end method

.method private getServerAI()Ljava/lang/String;
    .locals 5

    .prologue
    .line 595
    const v2, 0x1450929

    .line 596
    .local v2, "sum":I
    const-string v1, "1"

    .line 597
    .local v1, "out":Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/16 v3, 0x9

    if-ge v0, v3, :cond_1

    .line 598
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    rem-int/lit8 v4, v2, 0x44

    add-int/lit8 v4, v4, 0x33

    int-to-char v4, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 599
    if-nez v2, :cond_0

    .line 600
    const v2, 0xe192cf

    .line 597
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 602
    :cond_0
    div-int/lit8 v2, v2, 0x44

    goto :goto_1

    .line 605
    :cond_1
    return-object v1
.end method

.method private getServerAS()Ljava/lang/String;
    .locals 4

    .prologue
    .line 611
    const v1, 0x109e2830

    .line 612
    .local v1, "sum":I
    const-string v0, "1"

    .line 613
    .local v0, "out":Ljava/lang/String;
    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    .line 614
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const v3, -0x1f624cc0

    invoke-static {v3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 615
    const v2, 0xffc6490

    sub-int/2addr v1, v2

    .line 616
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const v3, -0x6db47542

    invoke-static {v3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "0"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 617
    const v2, 0x6d15b6e2

    sub-int/2addr v1, v2

    .line 618
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const v3, 0xd6290c3

    invoke-static {v3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 619
    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method private getServerURI()Ljava/lang/String;
    .locals 5

    .prologue
    .line 568
    const/4 v3, 0x0

    .line 570
    .local v3, "url":Ljava/lang/String;
    :try_start_0
    new-instance v0, Lcom/sec/dsm/system/DSMManager;

    invoke-direct {v0, p0}, Lcom/sec/dsm/system/DSMManager;-><init>(Landroid/content/Context;)V

    .line 571
    .local v0, "dsmManager":Lcom/sec/dsm/system/DSMManager;
    invoke-virtual {v0}, Lcom/sec/dsm/system/DSMManager;->getServerUrl()Ljava/lang/String;
    :try_end_0
    .catch Lcom/sec/dsm/system/DSMException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 576
    .end local v0    # "dsmManager":Lcom/sec/dsm/system/DSMManager;
    :goto_0
    if-nez v3, :cond_2

    .line 577
    const-string v4, "No URL DSMManager"

    invoke-static {v4}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 578
    const-string v4, "ro.csc.sales_code"

    invoke-static {v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 579
    .local v2, "salesCode":Ljava/lang/String;
    const-string v4, "CHN"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string v4, "CHU"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string v4, "CHM"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string v4, "CTC"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string v4, "CHC"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 581
    :cond_0
    const-string v4, "https://chn.ospserver.net/location/location/locations"

    .line 589
    .end local v2    # "salesCode":Ljava/lang/String;
    :goto_1
    return-object v4

    .line 572
    :catch_0
    move-exception v1

    .line 573
    .local v1, "e":Lcom/sec/dsm/system/DSMException;
    invoke-static {v1}, Lcom/sec/dsm/system/Util;->Loge(Ljava/lang/Exception;)V

    goto :goto_0

    .line 583
    .end local v1    # "e":Lcom/sec/dsm/system/DSMException;
    .restart local v2    # "salesCode":Ljava/lang/String;
    :cond_1
    const-string v4, "https://www.ospserver.net/location/location/locations"

    goto :goto_1

    .line 585
    .end local v2    # "salesCode":Ljava/lang/String;
    :cond_2
    const-string v4, "chn"

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 586
    const-string v4, "https://chn.ospserver.net/location/location/locations"

    goto :goto_1

    .line 589
    :cond_3
    const-string v4, "https://www.ospserver.net/location/location/locations"

    goto :goto_1
.end method

.method public static isCheckedRemoteControl(Landroid/content/Context;)Z
    .locals 7
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 434
    if-nez p0, :cond_1

    .line 459
    :cond_0
    :goto_0
    return v1

    .line 436
    :cond_1
    const/4 v1, 0x0

    .line 438
    .local v1, "retState":Z
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "remote_control"

    const/4 v6, 0x0

    invoke-static {v4, v5, v6}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    .line 439
    .local v2, "state":I
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "device_provisioned"

    const/4 v6, 0x0

    invoke-static {v4, v5, v6}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    .line 441
    .local v3, "stateSetupWizard":I
    packed-switch v2, :pswitch_data_0

    .line 453
    const/4 v1, 0x0

    goto :goto_0

    .line 443
    :pswitch_0
    const/4 v1, 0x0

    .line 444
    if-nez v3, :cond_0

    .line 445
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[isCheckedRemoteControl]stateSetupWizard : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 446
    const/4 v1, 0x1

    goto :goto_0

    .line 450
    :pswitch_1
    const/4 v1, 0x1

    .line 451
    goto :goto_0

    .line 456
    .end local v2    # "state":I
    .end local v3    # "stateSetupWizard":I
    :catch_0
    move-exception v0

    .line 457
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 441
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static isWifiOnly(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 463
    const-string v2, "connectivity"

    invoke-virtual {p0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 464
    .local v0, "cm":Landroid/net/ConnectivityManager;
    invoke-virtual {v0, v1}, Landroid/net/ConnectivityManager;->isNetworkSupported(I)Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1
.end method

.method public static wsMobileTrackingAdpCheckDate(Ljava/lang/String;Ljava/lang/String;)I
    .locals 10
    .param p0, "szStartDate"    # Ljava/lang/String;
    .param p1, "szEndDate"    # Ljava/lang/String;

    .prologue
    const/4 v9, 0x0

    .line 196
    sget v0, Lcom/sec/dsm/system/Constants;->OSPS_MOBILE_TRACKING_OPERATION_TRACKING_DONE:I

    .line 197
    .local v0, "bRet":I
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 200
    .local v2, "nCurrentTime":J
    new-instance v1, Landroid/text/format/Time;

    invoke-direct {v1}, Landroid/text/format/Time;-><init>()V

    .line 201
    .local v1, "time":Landroid/text/format/Time;
    invoke-virtual {p0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1, v8}, Landroid/text/format/Time;->parse(Ljava/lang/String;)Z

    .line 202
    invoke-virtual {v1, v9}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v6

    .line 203
    .local v6, "nStartDate":J
    invoke-virtual {p1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1, v8}, Landroid/text/format/Time;->parse(Ljava/lang/String;)Z

    .line 204
    invoke-virtual {v1, v9}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v4

    .line 205
    .local v4, "nEndDate":J
    cmp-long v8, v2, v6

    if-ltz v8, :cond_1

    .line 206
    cmp-long v8, v2, v4

    if-gez v8, :cond_0

    .line 207
    sget v0, Lcom/sec/dsm/system/Constants;->OSPS_MOBILE_TRACKING_OPERATION_TRACKING_OK:I

    .line 208
    const-string v8, "TRACKING OPERATION OK"

    invoke-static {v8}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 217
    :goto_0
    return v0

    .line 210
    :cond_0
    sget v0, Lcom/sec/dsm/system/Constants;->OSPS_MOBILE_TRACKING_OPERATION_TRACKING_DONE:I

    .line 211
    const-string v8, "TRACKING OPERATION DONE"

    invoke-static {v8}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    goto :goto_0

    .line 214
    :cond_1
    sget v0, Lcom/sec/dsm/system/Constants;->OSPS_MOBILE_TRACKING_OPERATION_TRACKING_CONTINUE:I

    .line 215
    const-string v8, "TRACKING OPERATION CONTINUE"

    invoke-static {v8}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static wsMobileTrackingAdpGetGpsAccuracy()D
    .locals 2

    .prologue
    .line 135
    sget-object v0, Lcom/sec/dsm/system/DSMTrackingService;->gpsData:Lcom/sec/dsm/system/GPSUtil;

    iget-wide v0, v0, Lcom/sec/dsm/system/GPSUtil;->mAccuracy:D

    return-wide v0
.end method

.method public static wsMobileTrackingAdpGetGpsAltitude()D
    .locals 2

    .prologue
    .line 123
    sget-object v0, Lcom/sec/dsm/system/DSMTrackingService;->gpsData:Lcom/sec/dsm/system/GPSUtil;

    iget-wide v0, v0, Lcom/sec/dsm/system/GPSUtil;->mAltitude:D

    return-wide v0
.end method

.method public static wsMobileTrackingAdpGetGpsCourse()D
    .locals 2

    .prologue
    .line 131
    sget-object v0, Lcom/sec/dsm/system/DSMTrackingService;->gpsData:Lcom/sec/dsm/system/GPSUtil;

    iget-wide v0, v0, Lcom/sec/dsm/system/GPSUtil;->mBearing:D

    return-wide v0
.end method

.method public static wsMobileTrackingAdpGetGpsLatitude()D
    .locals 2

    .prologue
    .line 115
    sget-object v0, Lcom/sec/dsm/system/DSMTrackingService;->gpsData:Lcom/sec/dsm/system/GPSUtil;

    iget-wide v0, v0, Lcom/sec/dsm/system/GPSUtil;->mLatitude:D

    return-wide v0
.end method

.method public static wsMobileTrackingAdpGetGpsLongitude()D
    .locals 2

    .prologue
    .line 119
    sget-object v0, Lcom/sec/dsm/system/DSMTrackingService;->gpsData:Lcom/sec/dsm/system/GPSUtil;

    iget-wide v0, v0, Lcom/sec/dsm/system/GPSUtil;->mLongitude:D

    return-wide v0
.end method

.method public static wsMobileTrackingAdpGetGpsSpeed()D
    .locals 2

    .prologue
    .line 127
    sget-object v0, Lcom/sec/dsm/system/DSMTrackingService;->gpsData:Lcom/sec/dsm/system/GPSUtil;

    iget-wide v0, v0, Lcom/sec/dsm/system/GPSUtil;->mSpeed:D

    return-wide v0
.end method

.method public static wsMobileTrackingAdpGetGpsValid()Z
    .locals 1

    .prologue
    .line 108
    sget-object v0, Lcom/sec/dsm/system/DSMTrackingService;->gpsData:Lcom/sec/dsm/system/GPSUtil;

    if-eqz v0, :cond_0

    .line 109
    sget-object v0, Lcom/sec/dsm/system/DSMTrackingService;->gpsData:Lcom/sec/dsm/system/GPSUtil;

    invoke-virtual {v0}, Lcom/sec/dsm/system/GPSUtil;->GetGPSLocation()Z

    move-result v0

    .line 111
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static wsMobileTrackingAdpGetReTrackingActive()Z
    .locals 2

    .prologue
    .line 180
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Tracking2 status= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-boolean v1, Lcom/sec/dsm/system/DSMTrackingService;->mReTrackingStatus:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 181
    sget-boolean v0, Lcom/sec/dsm/system/DSMTrackingService;->mReTrackingStatus:Z

    return v0
.end method

.method public static wsMobileTrackingAdpGetStrTimeData(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "szTimeDate"    # Ljava/lang/String;

    .prologue
    .line 221
    const-string v0, "-"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 222
    const-string v0, ":"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 223
    return-object p0
.end method

.method public static wsMobileTrackingAdpSetReTrackingActive(Z)V
    .locals 2
    .param p0, "szStatus"    # Z

    .prologue
    .line 175
    sput-boolean p0, Lcom/sec/dsm/system/DSMTrackingService;->mReTrackingStatus:Z

    .line 176
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Tracking1 status = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-boolean v1, Lcom/sec/dsm/system/DSMTrackingService;->mReTrackingStatus:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 177
    return-void
.end method

.method private static wsMobileTrackingAdpSetTrackingLocation()V
    .locals 9

    .prologue
    const/16 v8, 0x2e

    const/4 v7, 0x7

    const/4 v6, 0x0

    .line 227
    invoke-static {}, Lcom/sec/dsm/system/DSMTrackingService;->wsMobileTrackingAdpGetGpsLatitude()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v3

    sput-object v3, Lcom/sec/dsm/system/DSMTrackingService;->latitude:Ljava/lang/String;

    .line 228
    invoke-static {}, Lcom/sec/dsm/system/DSMTrackingService;->wsMobileTrackingAdpGetGpsLongitude()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v3

    sput-object v3, Lcom/sec/dsm/system/DSMTrackingService;->longitude:Ljava/lang/String;

    .line 229
    invoke-static {}, Lcom/sec/dsm/system/DSMTrackingService;->wsMobileTrackingAdpGetGpsAltitude()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v3

    sput-object v3, Lcom/sec/dsm/system/DSMTrackingService;->altitude:Ljava/lang/String;

    .line 230
    invoke-static {}, Lcom/sec/dsm/system/DSMTrackingService;->wsMobileTrackingAdpGetGpsSpeed()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v3

    sput-object v3, Lcom/sec/dsm/system/DSMTrackingService;->speed:Ljava/lang/String;

    .line 231
    invoke-static {}, Lcom/sec/dsm/system/DSMTrackingService;->wsMobileTrackingAdpGetGpsCourse()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v3

    sput-object v3, Lcom/sec/dsm/system/DSMTrackingService;->course:Ljava/lang/String;

    .line 232
    invoke-static {}, Lcom/sec/dsm/system/DSMTrackingService;->wsMobileTrackingAdpGetGpsAccuracy()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v3

    sput-object v3, Lcom/sec/dsm/system/DSMTrackingService;->accuracy:Ljava/lang/String;

    .line 233
    sget-object v3, Lcom/sec/dsm/system/DSMTrackingService;->latitude:Ljava/lang/String;

    const-string v4, "."

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 234
    sget-object v3, Lcom/sec/dsm/system/DSMTrackingService;->latitude:Ljava/lang/String;

    invoke-virtual {v3, v8}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    .line 235
    .local v1, "index":I
    sget-object v3, Lcom/sec/dsm/system/DSMTrackingService;->latitude:Ljava/lang/String;

    sget-object v4, Lcom/sec/dsm/system/DSMTrackingService;->latitude:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {v3, v1, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 236
    .local v2, "str":Ljava/lang/String;
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-le v3, v7, :cond_0

    .line 238
    sget-object v3, Lcom/sec/dsm/system/DSMTrackingService;->latitude:Ljava/lang/String;

    invoke-virtual {v3, v6, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 239
    sget-object v3, Lcom/sec/dsm/system/DSMTrackingService;->latitude:Ljava/lang/String;

    add-int/lit8 v4, v1, 0x7

    invoke-virtual {v3, v1, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 240
    sput-object v2, Lcom/sec/dsm/system/DSMTrackingService;->latitude:Ljava/lang/String;

    .line 243
    :cond_0
    sget-object v3, Lcom/sec/dsm/system/DSMTrackingService;->longitude:Ljava/lang/String;

    const-string v4, "."

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 244
    sget-object v3, Lcom/sec/dsm/system/DSMTrackingService;->longitude:Ljava/lang/String;

    invoke-virtual {v3, v8}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    .line 245
    sget-object v3, Lcom/sec/dsm/system/DSMTrackingService;->longitude:Ljava/lang/String;

    sget-object v4, Lcom/sec/dsm/system/DSMTrackingService;->longitude:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {v3, v1, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 246
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-le v3, v7, :cond_1

    .line 248
    sget-object v3, Lcom/sec/dsm/system/DSMTrackingService;->longitude:Ljava/lang/String;

    invoke-virtual {v3, v6, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 249
    sget-object v3, Lcom/sec/dsm/system/DSMTrackingService;->longitude:Ljava/lang/String;

    add-int/lit8 v4, v1, 0x7

    invoke-virtual {v3, v1, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 250
    sput-object v2, Lcom/sec/dsm/system/DSMTrackingService;->longitude:Ljava/lang/String;

    .line 253
    :cond_1
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v3, "yyyyMMdd HH:mm:ss"

    invoke-direct {v0, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 254
    .local v0, "dateFormat":Ljava/text/SimpleDateFormat;
    const-string v3, "GMT+0"

    invoke-static {v3}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 255
    new-instance v3, Ljava/util/Date;

    invoke-direct {v3}, Ljava/util/Date;-><init>()V

    invoke-virtual {v0, v3}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    sput-object v3, Lcom/sec/dsm/system/DSMTrackingService;->date:Ljava/lang/String;

    .line 256
    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 86
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 61
    const-string v0, " onCreate begin"

    invoke-static {v0}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 62
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 63
    iput-object p0, p0, Lcom/sec/dsm/system/DSMTrackingService;->mContext:Landroid/content/Context;

    .line 64
    new-instance v0, Lcom/sec/dsm/system/GPSUtil;

    invoke-direct {v0}, Lcom/sec/dsm/system/GPSUtil;-><init>()V

    sput-object v0, Lcom/sec/dsm/system/DSMTrackingService;->gpsData:Lcom/sec/dsm/system/GPSUtil;

    .line 65
    const-string v0, " onCreate end"

    invoke-static {v0}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 66
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 91
    const-string v0, "onDestory"

    invoke-static {v0}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 92
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 93
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    const/4 v1, 0x2

    .line 70
    if-nez p1, :cond_0

    .line 81
    :goto_0
    return v1

    .line 73
    :cond_0
    const-string v0, "interval"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/dsm/system/DSMTrackingService;->interval:Ljava/lang/String;

    .line 74
    const-string v0, "starttime"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/dsm/system/DSMTrackingService;->startTime:Ljava/lang/String;

    .line 75
    const-string v0, "stoptime"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/dsm/system/DSMTrackingService;->endTime:Ljava/lang/String;

    .line 76
    sget-object v0, Lcom/sec/dsm/system/DSMTrackingService;->interval:Ljava/lang/String;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/sec/dsm/system/DSMTrackingService;->startTime:Ljava/lang/String;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/sec/dsm/system/DSMTrackingService;->endTime:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 77
    invoke-virtual {p0}, Lcom/sec/dsm/system/DSMTrackingService;->wsMobileTrackingAdpOerationTracking()V

    goto :goto_0

    .line 79
    :cond_1
    invoke-virtual {p0}, Lcom/sec/dsm/system/DSMTrackingService;->wsMobileTrackingAdpStopGPSTracking()V

    goto :goto_0
.end method

.method public wsMobileTrackingAdpCallLocationLocations()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/dsm/system/osp/DeviceException;
        }
    .end annotation

    .prologue
    .line 413
    iget-object v2, p0, Lcom/sec/dsm/system/DSMTrackingService;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/dsm/system/DSMTrackingService;->isCheckedRemoteControl(Landroid/content/Context;)Z

    move-result v0

    .line 414
    .local v0, "bAMTStatus":Z
    if-eqz v0, :cond_1

    .line 415
    invoke-virtual {p0}, Lcom/sec/dsm/system/DSMTrackingService;->wsMobileTrackingAdpCheckAccessToken()Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-nez v2, :cond_0

    .line 416
    const-string v2, "Samsung Account send AutoSignIn Intent"

    invoke-static {v2}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 417
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.DM_ACCOUNT_SIGNIN_CHECK"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 418
    .local v1, "intent":Landroid/content/Intent;
    const/16 v2, 0x20

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 419
    iget-object v2, p0, Lcom/sec/dsm/system/DSMTrackingService;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 430
    .end local v1    # "intent":Landroid/content/Intent;
    :goto_0
    const-string v2, "locationLocations end"

    invoke-static {v2}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 431
    return-void

    .line 421
    :cond_0
    invoke-direct {p0}, Lcom/sec/dsm/system/DSMTrackingService;->getServerAI()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0}, Lcom/sec/dsm/system/DSMTrackingService;->getServerAS()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v2, v3}, Lcom/sec/dsm/system/DSMTrackingService;->wsMobileTrackingAdpRequestLocationLocations(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 424
    :cond_1
    sget-object v2, Lcom/sec/dsm/system/DSMTrackingService;->mobileTrackingTimer:Lcom/sec/dsm/system/DSMTrackingService$MobileTrackingTimer;

    if-eqz v2, :cond_2

    .line 425
    sget-object v2, Lcom/sec/dsm/system/DSMTrackingService;->mobileTrackingTimer:Lcom/sec/dsm/system/DSMTrackingService$MobileTrackingTimer;

    invoke-virtual {v2}, Lcom/sec/dsm/system/DSMTrackingService$MobileTrackingTimer;->endTimer()V

    .line 427
    :cond_2
    invoke-virtual {p0}, Lcom/sec/dsm/system/DSMTrackingService;->wsMobileTrackingAdpStopGPSTracking()V

    .line 428
    const-string v2, "AMT Not Enabled"

    invoke-static {v2}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public wsMobileTrackingAdpCheckAccessToken()Ljava/lang/Boolean;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 552
    :try_start_0
    new-instance v0, Lcom/sec/dsm/system/osp/CredentialManager;

    iget-object v3, p0, Lcom/sec/dsm/system/DSMTrackingService;->mContext:Landroid/content/Context;

    invoke-direct {v0, v3}, Lcom/sec/dsm/system/osp/CredentialManager;-><init>(Landroid/content/Context;)V

    .line 553
    .local v0, "credentialManager":Lcom/sec/dsm/system/osp/CredentialManager;
    invoke-direct {p0}, Lcom/sec/dsm/system/DSMTrackingService;->getServerAI()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/sec/dsm/system/osp/CredentialManager;->containsAccessToken(Ljava/lang/String;)Z

    move-result v2

    .line 554
    .local v2, "hasAccessToken":Z
    if-eqz v2, :cond_0

    .line 555
    const-string v3, "AccessToken is TRUE"

    invoke-static {v3}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 556
    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    .line 563
    .end local v0    # "credentialManager":Lcom/sec/dsm/system/osp/CredentialManager;
    .end local v2    # "hasAccessToken":Z
    :goto_0
    return-object v3

    .line 558
    .restart local v0    # "credentialManager":Lcom/sec/dsm/system/osp/CredentialManager;
    .restart local v2    # "hasAccessToken":Z
    :cond_0
    const-string v3, "AccessToken is FALSE"

    invoke-static {v3}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 559
    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    goto :goto_0

    .line 561
    .end local v0    # "credentialManager":Lcom/sec/dsm/system/osp/CredentialManager;
    .end local v2    # "hasAccessToken":Z
    :catch_0
    move-exception v1

    .line 562
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 563
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    goto :goto_0
.end method

.method public wsMobileTrackingAdpOerationTracking()V
    .locals 7

    .prologue
    .line 139
    iget-object v5, p0, Lcom/sec/dsm/system/DSMTrackingService;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/sec/dsm/system/DSMTrackingService;->isCheckedRemoteControl(Landroid/content/Context;)Z

    move-result v0

    .line 140
    .local v0, "bAMTStatus":Z
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "AMT Status = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 141
    if-eqz v0, :cond_5

    .line 142
    sget-object v3, Lcom/sec/dsm/system/DSMTrackingService;->interval:Ljava/lang/String;

    .line 143
    .local v3, "szInterval":Ljava/lang/String;
    sget-object v4, Lcom/sec/dsm/system/DSMTrackingService;->startTime:Ljava/lang/String;

    .line 144
    .local v4, "szStartDate":Ljava/lang/String;
    sget-object v2, Lcom/sec/dsm/system/DSMTrackingService;->endTime:Ljava/lang/String;

    .line 145
    .local v2, "szEndDate":Ljava/lang/String;
    if-eqz v3, :cond_0

    if-eqz v4, :cond_0

    if-nez v2, :cond_1

    .line 146
    :cond_0
    const-string v5, "[Error] Data is null "

    invoke-static {v5}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 147
    invoke-virtual {p0}, Lcom/sec/dsm/system/DSMTrackingService;->wsMobileTrackingAdpOperationStop()V

    .line 172
    .end local v2    # "szEndDate":Ljava/lang/String;
    .end local v3    # "szInterval":Ljava/lang/String;
    .end local v4    # "szStartDate":Ljava/lang/String;
    :goto_0
    return-void

    .line 149
    .restart local v2    # "szEndDate":Ljava/lang/String;
    .restart local v3    # "szInterval":Ljava/lang/String;
    .restart local v4    # "szStartDate":Ljava/lang/String;
    :cond_1
    const-string v5, "Start GPS tracking"

    invoke-static {v5}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 150
    invoke-virtual {p0}, Lcom/sec/dsm/system/DSMTrackingService;->wsMobileTrackingAdpStartGPSTracking()V

    .line 151
    sget-object v5, Lcom/sec/dsm/system/DSMTrackingService;->TrackingWakeLock:Landroid/os/PowerManager$WakeLock;

    if-nez v5, :cond_2

    .line 152
    iget-object v5, p0, Lcom/sec/dsm/system/DSMTrackingService;->mContext:Landroid/content/Context;

    const-string v6, "power"

    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/PowerManager;

    .line 153
    .local v1, "pm":Landroid/os/PowerManager;
    const/4 v5, 0x1

    const-string v6, "TrackingWakeLock"

    invoke-virtual {v1, v5, v6}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v5

    sput-object v5, Lcom/sec/dsm/system/DSMTrackingService;->TrackingWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 154
    sget-object v5, Lcom/sec/dsm/system/DSMTrackingService;->TrackingWakeLock:Landroid/os/PowerManager$WakeLock;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V

    .line 155
    sget-object v5, Lcom/sec/dsm/system/DSMTrackingService;->TrackingWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v5}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 156
    const-string v5, "wakelock start"

    invoke-static {v5}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 158
    .end local v1    # "pm":Landroid/os/PowerManager;
    :cond_2
    if-eqz v4, :cond_3

    .line 159
    invoke-static {v4}, Lcom/sec/dsm/system/DSMTrackingService;->wsMobileTrackingAdpGetStrTimeData(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 161
    :cond_3
    if-eqz v2, :cond_4

    .line 162
    invoke-static {v2}, Lcom/sec/dsm/system/DSMTrackingService;->wsMobileTrackingAdpGetStrTimeData(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 164
    :cond_4
    new-instance v5, Lcom/sec/dsm/system/DSMTrackingService$MobileTrackingTimer;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-direct {v5, p0, v6, v4, v2}, Lcom/sec/dsm/system/DSMTrackingService$MobileTrackingTimer;-><init>(Lcom/sec/dsm/system/DSMTrackingService;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v5, Lcom/sec/dsm/system/DSMTrackingService;->mobileTrackingTimer:Lcom/sec/dsm/system/DSMTrackingService$MobileTrackingTimer;

    .line 165
    const-string v5, "Start mobile tracking timer"

    invoke-static {v5}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 166
    sget-object v5, Lcom/sec/dsm/system/DSMTrackingService;->mobileTrackingTimer:Lcom/sec/dsm/system/DSMTrackingService$MobileTrackingTimer;

    invoke-virtual {v5}, Lcom/sec/dsm/system/DSMTrackingService$MobileTrackingTimer;->startTimer()V

    goto :goto_0

    .line 169
    .end local v2    # "szEndDate":Ljava/lang/String;
    .end local v3    # "szInterval":Ljava/lang/String;
    .end local v4    # "szStartDate":Ljava/lang/String;
    :cond_5
    const-string v5, "Please remote control on"

    invoke-static {v5}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 170
    invoke-virtual {p0}, Lcom/sec/dsm/system/DSMTrackingService;->wsMobileTrackingAdpOperationStop()V

    goto :goto_0
.end method

.method public wsMobileTrackingAdpOperationStop()V
    .locals 1

    .prologue
    .line 185
    sget-object v0, Lcom/sec/dsm/system/DSMTrackingService;->mobileTrackingTimer:Lcom/sec/dsm/system/DSMTrackingService$MobileTrackingTimer;

    if-eqz v0, :cond_0

    .line 186
    sget-object v0, Lcom/sec/dsm/system/DSMTrackingService;->mobileTrackingTimer:Lcom/sec/dsm/system/DSMTrackingService$MobileTrackingTimer;

    invoke-virtual {v0}, Lcom/sec/dsm/system/DSMTrackingService$MobileTrackingTimer;->endTimer()V

    .line 188
    :cond_0
    invoke-virtual {p0}, Lcom/sec/dsm/system/DSMTrackingService;->wsMobileTrackingAdpStopGPSTracking()V

    .line 189
    sget-object v0, Lcom/sec/dsm/system/DSMTrackingService;->TrackingWakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_1

    .line 190
    sget-object v0, Lcom/sec/dsm/system/DSMTrackingService;->TrackingWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 191
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/dsm/system/DSMTrackingService;->TrackingWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 193
    :cond_1
    return-void
.end method

.method public wsMobileTrackingAdpRequestLocationLocations(Ljava/lang/String;Ljava/lang/String;)V
    .locals 21
    .param p1, "appID"    # Ljava/lang/String;
    .param p2, "appSecret"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/dsm/system/osp/DeviceException;
        }
    .end annotation

    .prologue
    .line 478
    const/4 v9, 0x0

    .line 480
    .local v9, "httpEntity":Lorg/apache/http/HttpEntity;
    :try_start_0
    invoke-direct/range {p0 .. p0}, Lcom/sec/dsm/system/DSMTrackingService;->getServerURI()Ljava/lang/String;

    move-result-object v13

    .line 481
    .local v13, "requestURI":Ljava/lang/String;
    new-instance v4, Lcom/sec/dsm/system/osp/RestClient;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/dsm/system/DSMTrackingService;->mContext:Landroid/content/Context;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-direct {v4, v0, v1, v2}, Lcom/sec/dsm/system/osp/RestClient;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 482
    .local v4, "client":Lcom/sec/dsm/system/osp/RestClient;
    new-instance v12, Lcom/sec/dsm/system/wsMobileTrackingLocation;

    invoke-direct {v12}, Lcom/sec/dsm/system/wsMobileTrackingLocation;-><init>()V

    .line 483
    .local v12, "mobileTrackingLocationRequest":Lcom/sec/dsm/system/wsMobileTrackingLocation;
    const-string v6, ""

    .line 484
    .local v6, "deviceId":Ljava/lang/String;
    const-string v7, ""

    .line 485
    .local v7, "deviceUniqueID":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/dsm/system/DSMTrackingService;->mContext:Landroid/content/Context;

    move-object/from16 v18, v0

    const-string v19, "phone"

    invoke-virtual/range {v18 .. v19}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Landroid/telephony/TelephonyManager;

    .line 486
    .local v17, "telephonyManager":Landroid/telephony/TelephonyManager;
    invoke-virtual/range {v17 .. v17}, Landroid/telephony/TelephonyManager;->getDeviceId()Ljava/lang/String;

    move-result-object v6

    .line 487
    invoke-static/range {p0 .. p0}, Lcom/sec/dsm/system/DSMTrackingService;->isWifiOnly(Landroid/content/Context;)Z

    move-result v18

    if-nez v18, :cond_0

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v18

    if-eqz v18, :cond_4

    .line 488
    :cond_0
    const-string v18, "ro.serialno"

    invoke-static/range {v18 .. v18}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 489
    invoke-static {v6}, Lcom/sec/dsm/system/osp/DUIDUtil;->getDUIDfromTWID(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 493
    :goto_0
    const-string v11, "1"

    .line 494
    .local v11, "locationMethod":Ljava/lang/String;
    invoke-virtual {v12, v7}, Lcom/sec/dsm/system/wsMobileTrackingLocation;->setDeviceUniqueID(Ljava/lang/String;)V

    .line 495
    invoke-virtual {v12, v11}, Lcom/sec/dsm/system/wsMobileTrackingLocation;->setLocationMethod(Ljava/lang/String;)V

    .line 496
    sget-object v18, Lcom/sec/dsm/system/DSMTrackingService;->latitude:Ljava/lang/String;

    move-object/from16 v0, v18

    invoke-virtual {v12, v0}, Lcom/sec/dsm/system/wsMobileTrackingLocation;->setLatitude(Ljava/lang/String;)V

    .line 497
    sget-object v18, Lcom/sec/dsm/system/DSMTrackingService;->longitude:Ljava/lang/String;

    move-object/from16 v0, v18

    invoke-virtual {v12, v0}, Lcom/sec/dsm/system/wsMobileTrackingLocation;->setLongitude(Ljava/lang/String;)V

    .line 498
    sget-object v18, Lcom/sec/dsm/system/DSMTrackingService;->altitude:Ljava/lang/String;

    move-object/from16 v0, v18

    invoke-virtual {v12, v0}, Lcom/sec/dsm/system/wsMobileTrackingLocation;->setAltitude(Ljava/lang/String;)V

    .line 499
    sget-object v18, Lcom/sec/dsm/system/DSMTrackingService;->speed:Ljava/lang/String;

    move-object/from16 v0, v18

    invoke-virtual {v12, v0}, Lcom/sec/dsm/system/wsMobileTrackingLocation;->setSpeed(Ljava/lang/String;)V

    .line 500
    sget-object v18, Lcom/sec/dsm/system/DSMTrackingService;->course:Ljava/lang/String;

    move-object/from16 v0, v18

    invoke-virtual {v12, v0}, Lcom/sec/dsm/system/wsMobileTrackingLocation;->setCourse(Ljava/lang/String;)V

    .line 501
    sget-object v18, Lcom/sec/dsm/system/DSMTrackingService;->accuracy:Ljava/lang/String;

    move-object/from16 v0, v18

    invoke-virtual {v12, v0}, Lcom/sec/dsm/system/wsMobileTrackingLocation;->setHorizontalAccuracy(Ljava/lang/String;)V

    .line 502
    sget-object v18, Lcom/sec/dsm/system/DSMTrackingService;->date:Ljava/lang/String;

    move-object/from16 v0, v18

    invoke-virtual {v12, v0}, Lcom/sec/dsm/system/wsMobileTrackingLocation;->setTimestamp(Ljava/lang/String;)V

    .line 504
    invoke-virtual {v12}, Lcom/sec/dsm/system/wsMobileTrackingLocation;->toXML()Ljava/lang/String;

    move-result-object v3

    .line 505
    .local v3, "body":Ljava/lang/String;
    sget-object v18, Lcom/sec/dsm/system/osp/RestClient$HttpMethod;->PUT:Lcom/sec/dsm/system/osp/RestClient$HttpMethod;

    sget-object v19, Lcom/sec/dsm/system/osp/HeaderUtil$KindOfSignKey;->ACCESS_TOKEN:Lcom/sec/dsm/system/osp/HeaderUtil$KindOfSignKey;

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v4, v0, v13, v3, v1}, Lcom/sec/dsm/system/osp/RestClient;->execute(Lcom/sec/dsm/system/osp/RestClient$HttpMethod;Ljava/lang/String;Ljava/lang/String;Lcom/sec/dsm/system/osp/HeaderUtil$KindOfSignKey;)Lorg/apache/http/HttpResponse;

    move-result-object v14

    .line 506
    .local v14, "response":Lorg/apache/http/HttpResponse;
    const/4 v5, 0x0

    .line 507
    .local v5, "content":Ljava/io/InputStream;
    const/16 v16, 0x0

    .line 508
    .local v16, "statusLine":Lorg/apache/http/StatusLine;
    const/4 v15, 0x0

    .line 509
    .local v15, "statusCode":I
    if-eqz v14, :cond_2

    .line 510
    invoke-interface {v14}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v9

    .line 511
    if-eqz v9, :cond_1

    .line 512
    invoke-interface {v9}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v5

    .line 514
    :cond_1
    invoke-interface {v14}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v16

    .line 515
    if-eqz v16, :cond_2

    .line 516
    invoke-interface/range {v16 .. v16}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v15

    .line 520
    :cond_2
    const/16 v18, 0xc8

    move/from16 v0, v18

    if-ne v15, v0, :cond_5

    .line 521
    const-string v18, "Response is normal"

    invoke-static/range {v18 .. v18}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 522
    if-nez v5, :cond_6

    .line 523
    new-instance v18, Ljava/lang/Exception;

    const-string v19, "Response content is null."

    invoke-direct/range {v18 .. v19}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v18
    :try_end_0
    .catch Lcom/sec/dsm/system/osp/ErrorResultException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 533
    .end local v3    # "body":Ljava/lang/String;
    .end local v4    # "client":Lcom/sec/dsm/system/osp/RestClient;
    .end local v5    # "content":Ljava/io/InputStream;
    .end local v6    # "deviceId":Ljava/lang/String;
    .end local v7    # "deviceUniqueID":Ljava/lang/String;
    .end local v11    # "locationMethod":Ljava/lang/String;
    .end local v12    # "mobileTrackingLocationRequest":Lcom/sec/dsm/system/wsMobileTrackingLocation;
    .end local v13    # "requestURI":Ljava/lang/String;
    .end local v14    # "response":Lorg/apache/http/HttpResponse;
    .end local v15    # "statusCode":I
    .end local v16    # "statusLine":Lorg/apache/http/StatusLine;
    .end local v17    # "telephonyManager":Landroid/telephony/TelephonyManager;
    :catch_0
    move-exception v8

    .line 534
    .local v8, "e":Lcom/sec/dsm/system/osp/ErrorResultException;
    :try_start_1
    invoke-virtual {v8}, Lcom/sec/dsm/system/osp/ErrorResultException;->printStackTrace()V

    .line 535
    new-instance v18, Lcom/sec/dsm/system/osp/DeviceException;

    invoke-virtual {v8}, Lcom/sec/dsm/system/osp/ErrorResultException;->getMessage()Ljava/lang/String;

    move-result-object v19

    invoke-virtual {v8}, Lcom/sec/dsm/system/osp/ErrorResultException;->getFaultCode()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    move-object/from16 v2, v20

    invoke-direct {v0, v1, v8, v2}, Lcom/sec/dsm/system/osp/DeviceException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)V

    throw v18
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 540
    .end local v8    # "e":Lcom/sec/dsm/system/osp/ErrorResultException;
    :catchall_0
    move-exception v18

    if-eqz v9, :cond_3

    .line 542
    :try_start_2
    invoke-interface {v9}, Lorg/apache/http/HttpEntity;->consumeContent()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    .line 545
    :cond_3
    :goto_1
    throw v18

    .line 491
    .restart local v4    # "client":Lcom/sec/dsm/system/osp/RestClient;
    .restart local v6    # "deviceId":Ljava/lang/String;
    .restart local v7    # "deviceUniqueID":Ljava/lang/String;
    .restart local v12    # "mobileTrackingLocationRequest":Lcom/sec/dsm/system/wsMobileTrackingLocation;
    .restart local v13    # "requestURI":Ljava/lang/String;
    .restart local v17    # "telephonyManager":Landroid/telephony/TelephonyManager;
    :cond_4
    :try_start_3
    invoke-static {v6}, Lcom/sec/dsm/system/osp/DUIDUtil;->getDUIDfromIMEI(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    goto/16 :goto_0

    .line 525
    .restart local v3    # "body":Ljava/lang/String;
    .restart local v5    # "content":Ljava/io/InputStream;
    .restart local v11    # "locationMethod":Ljava/lang/String;
    .restart local v14    # "response":Lorg/apache/http/HttpResponse;
    .restart local v15    # "statusCode":I
    .restart local v16    # "statusLine":Lorg/apache/http/StatusLine;
    :cond_5
    const/16 v18, 0x191

    move/from16 v0, v18

    if-ne v15, v0, :cond_8

    .line 526
    const-string v18, "Samsung Account send AutoSignIn Intent"

    invoke-static/range {v18 .. v18}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 527
    new-instance v10, Landroid/content/Intent;

    const-string v18, "android.intent.action.DM_ACCOUNT_SIGNIN_CHECK"

    move-object/from16 v0, v18

    invoke-direct {v10, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 528
    .local v10, "intent":Landroid/content/Intent;
    const/16 v18, 0x20

    move/from16 v0, v18

    invoke-virtual {v10, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 529
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/dsm/system/DSMTrackingService;->mContext:Landroid/content/Context;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v10}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V
    :try_end_3
    .catch Lcom/sec/dsm/system/osp/ErrorResultException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 540
    .end local v10    # "intent":Landroid/content/Intent;
    :cond_6
    :goto_2
    if-eqz v9, :cond_7

    .line 542
    :try_start_4
    invoke-interface {v9}, Lorg/apache/http/HttpEntity;->consumeContent()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    .line 548
    :cond_7
    :goto_3
    return-void

    .line 531
    :cond_8
    :try_start_5
    const-string v18, "Location server error"

    invoke-static/range {v18 .. v18}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V
    :try_end_5
    .catch Lcom/sec/dsm/system/osp/ErrorResultException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_2

    .line 536
    .end local v3    # "body":Ljava/lang/String;
    .end local v4    # "client":Lcom/sec/dsm/system/osp/RestClient;
    .end local v5    # "content":Ljava/io/InputStream;
    .end local v6    # "deviceId":Ljava/lang/String;
    .end local v7    # "deviceUniqueID":Ljava/lang/String;
    .end local v11    # "locationMethod":Ljava/lang/String;
    .end local v12    # "mobileTrackingLocationRequest":Lcom/sec/dsm/system/wsMobileTrackingLocation;
    .end local v13    # "requestURI":Ljava/lang/String;
    .end local v14    # "response":Lorg/apache/http/HttpResponse;
    .end local v15    # "statusCode":I
    .end local v16    # "statusLine":Lorg/apache/http/StatusLine;
    .end local v17    # "telephonyManager":Landroid/telephony/TelephonyManager;
    :catch_1
    move-exception v8

    .line 537
    .local v8, "e":Ljava/lang/Exception;
    :try_start_6
    invoke-virtual {v8}, Ljava/lang/Exception;->printStackTrace()V

    .line 538
    new-instance v18, Lcom/sec/dsm/system/osp/DeviceException;

    invoke-virtual {v8}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-direct {v0, v1, v8}, Lcom/sec/dsm/system/osp/DeviceException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v18
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 543
    .end local v8    # "e":Ljava/lang/Exception;
    .restart local v3    # "body":Ljava/lang/String;
    .restart local v4    # "client":Lcom/sec/dsm/system/osp/RestClient;
    .restart local v5    # "content":Ljava/io/InputStream;
    .restart local v6    # "deviceId":Ljava/lang/String;
    .restart local v7    # "deviceUniqueID":Ljava/lang/String;
    .restart local v11    # "locationMethod":Ljava/lang/String;
    .restart local v12    # "mobileTrackingLocationRequest":Lcom/sec/dsm/system/wsMobileTrackingLocation;
    .restart local v13    # "requestURI":Ljava/lang/String;
    .restart local v14    # "response":Lorg/apache/http/HttpResponse;
    .restart local v15    # "statusCode":I
    .restart local v16    # "statusLine":Lorg/apache/http/StatusLine;
    .restart local v17    # "telephonyManager":Landroid/telephony/TelephonyManager;
    :catch_2
    move-exception v8

    .line 544
    .local v8, "e":Ljava/io/IOException;
    invoke-virtual {v8}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 543
    .end local v3    # "body":Ljava/lang/String;
    .end local v4    # "client":Lcom/sec/dsm/system/osp/RestClient;
    .end local v5    # "content":Ljava/io/InputStream;
    .end local v6    # "deviceId":Ljava/lang/String;
    .end local v7    # "deviceUniqueID":Ljava/lang/String;
    .end local v8    # "e":Ljava/io/IOException;
    .end local v11    # "locationMethod":Ljava/lang/String;
    .end local v12    # "mobileTrackingLocationRequest":Lcom/sec/dsm/system/wsMobileTrackingLocation;
    .end local v13    # "requestURI":Ljava/lang/String;
    .end local v14    # "response":Lorg/apache/http/HttpResponse;
    .end local v15    # "statusCode":I
    .end local v16    # "statusLine":Lorg/apache/http/StatusLine;
    .end local v17    # "telephonyManager":Landroid/telephony/TelephonyManager;
    :catch_3
    move-exception v8

    .line 544
    .restart local v8    # "e":Ljava/io/IOException;
    invoke-virtual {v8}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1
.end method

.method public wsMobileTrackingAdpStartGPSTracking()V
    .locals 2

    .prologue
    .line 96
    sget-object v0, Lcom/sec/dsm/system/DSMTrackingService;->gpsData:Lcom/sec/dsm/system/GPSUtil;

    if-eqz v0, :cond_0

    .line 97
    sget-object v0, Lcom/sec/dsm/system/DSMTrackingService;->gpsData:Lcom/sec/dsm/system/GPSUtil;

    iget-object v1, p0, Lcom/sec/dsm/system/DSMTrackingService;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/dsm/system/GPSUtil;->DSMstartGPSTracking(Landroid/content/Context;)V

    .line 98
    :cond_0
    return-void
.end method

.method public wsMobileTrackingAdpStopGPSTracking()V
    .locals 2

    .prologue
    .line 101
    sget-object v1, Lcom/sec/dsm/system/DSMTrackingService;->gpsData:Lcom/sec/dsm/system/GPSUtil;

    if-eqz v1, :cond_0

    .line 102
    sget-object v1, Lcom/sec/dsm/system/DSMTrackingService;->gpsData:Lcom/sec/dsm/system/GPSUtil;

    invoke-virtual {v1}, Lcom/sec/dsm/system/GPSUtil;->DSMremoveListener()V

    .line 103
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.dsm.TRACKING_SERVICE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 104
    .local v0, "i":Landroid/content/Intent;
    iget-object v1, p0, Lcom/sec/dsm/system/DSMTrackingService;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->stopService(Landroid/content/Intent;)Z

    .line 105
    return-void
.end method
