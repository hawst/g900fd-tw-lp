.class public Lcom/sec/dsm/system/DSMRingMyMobile;
.super Landroid/app/Activity;
.source "DSMRingMyMobile.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/dsm/system/DSMRingMyMobile$WakeLock;
    }
.end annotation


# static fields
.field private static final DESTROY_BY_TIMEOUT:Ljava/lang/String; = "android.intent.action.DSM_RINGTONE_TIMEOUT"

.field private static final TAG:Ljava/lang/String; = "PopupDlgPCW"

.field private static backIntMode:I

.field private static callState:I

.field private static mAlarmRingtone:Landroid/media/Ringtone;

.field private static mAudioManager:Landroid/media/AudioManager;

.field private static mOldVolume:I

.field private static registerFlag:Z


# instance fields
.field private mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

.field private mFrameAnimation:Landroid/graphics/drawable/AnimationDrawable;

.field private mPaused:Z

.field mPhoneListener:Landroid/telephony/PhoneStateListener;

.field private mPlaypcwView:Landroid/widget/ImageView;

.field private mRingBg:Landroid/widget/ImageView;

.field private mRingStarted:Z

.field private mRingermode:Z

.field private mSilentmode:Z

.field private mWindowManager:Landroid/view/IWindowManager;

.field private quit_receiver:Landroid/content/BroadcastReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 45
    sput v1, Lcom/sec/dsm/system/DSMRingMyMobile;->mOldVolume:I

    .line 46
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/dsm/system/DSMRingMyMobile;->mAudioManager:Landroid/media/AudioManager;

    .line 57
    sput v1, Lcom/sec/dsm/system/DSMRingMyMobile;->callState:I

    .line 58
    sput-boolean v1, Lcom/sec/dsm/system/DSMRingMyMobile;->registerFlag:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 42
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 48
    iput-boolean v0, p0, Lcom/sec/dsm/system/DSMRingMyMobile;->mRingermode:Z

    .line 49
    iput-boolean v0, p0, Lcom/sec/dsm/system/DSMRingMyMobile;->mSilentmode:Z

    .line 53
    iput-object v1, p0, Lcom/sec/dsm/system/DSMRingMyMobile;->mWindowManager:Landroid/view/IWindowManager;

    .line 54
    iput-object v1, p0, Lcom/sec/dsm/system/DSMRingMyMobile;->mPhoneListener:Landroid/telephony/PhoneStateListener;

    .line 55
    iput-boolean v0, p0, Lcom/sec/dsm/system/DSMRingMyMobile;->mRingStarted:Z

    .line 56
    iput-boolean v0, p0, Lcom/sec/dsm/system/DSMRingMyMobile;->mPaused:Z

    .line 381
    new-instance v0, Lcom/sec/dsm/system/DSMRingMyMobile$3;

    invoke-direct {v0, p0}, Lcom/sec/dsm/system/DSMRingMyMobile$3;-><init>(Lcom/sec/dsm/system/DSMRingMyMobile;)V

    iput-object v0, p0, Lcom/sec/dsm/system/DSMRingMyMobile;->quit_receiver:Landroid/content/BroadcastReceiver;

    .line 485
    new-instance v0, Lcom/sec/dsm/system/DSMRingMyMobile$4;

    invoke-direct {v0, p0}, Lcom/sec/dsm/system/DSMRingMyMobile$4;-><init>(Lcom/sec/dsm/system/DSMRingMyMobile;)V

    iput-object v0, p0, Lcom/sec/dsm/system/DSMRingMyMobile;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    return-void
.end method

.method public static RegisterRepeatingAlarmTime(Landroid/content/Context;ZI)V
    .locals 12
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "isBootUp"    # Z
    .param p2, "nextTime"    # I

    .prologue
    .line 264
    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "[RegisterRepeatingAlarmTime] Input interval Time (MIN)= "

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 265
    new-instance v8, Landroid/content/Intent;

    const-string v1, "android.intent.action.DSM_RINGTONE_TIMEOUT"

    invoke-direct {v8, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 266
    .local v8, "intent":Landroid/content/Intent;
    const/4 v1, 0x0

    const/4 v9, 0x0

    invoke-static {p0, v1, v8, v9}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v6

    .line 267
    .local v6, "sender":Landroid/app/PendingIntent;
    mul-int/lit16 v1, p2, 0x3e8

    mul-int/lit8 v1, v1, 0x3c

    int-to-long v4, v1

    .line 268
    .local v4, "intervalTime":J
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "[RegisterRepeatingAlarmTime] interval Time: "

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 269
    const-string v1, "alarm"

    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 270
    .local v0, "am":Landroid/app/AlarmManager;
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    .line 271
    .local v2, "starttime":J
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "starttime : "

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 272
    const/4 v1, 0x1

    if-ne p1, v1, :cond_0

    .line 273
    const-wide/32 v10, 0xea60

    add-long/2addr v2, v10

    .line 274
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "[Boot up,starttime] starttime : "

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 279
    :goto_0
    const/4 v1, 0x3

    invoke-virtual/range {v0 .. v6}, Landroid/app/AlarmManager;->setRepeating(IJJLandroid/app/PendingIntent;)V

    .line 283
    .end local v0    # "am":Landroid/app/AlarmManager;
    .end local v2    # "starttime":J
    .end local v4    # "intervalTime":J
    .end local v6    # "sender":Landroid/app/PendingIntent;
    .end local v8    # "intent":Landroid/content/Intent;
    :goto_1
    return-void

    .line 276
    .restart local v0    # "am":Landroid/app/AlarmManager;
    .restart local v2    # "starttime":J
    .restart local v4    # "intervalTime":J
    .restart local v6    # "sender":Landroid/app/PendingIntent;
    .restart local v8    # "intent":Landroid/content/Intent;
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v10

    add-long v2, v10, v4

    .line 277
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "[starttime] starttime : "

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 280
    .end local v0    # "am":Landroid/app/AlarmManager;
    .end local v2    # "starttime":J
    .end local v4    # "intervalTime":J
    .end local v6    # "sender":Landroid/app/PendingIntent;
    .end local v8    # "intent":Landroid/content/Intent;
    :catch_0
    move-exception v7

    .line 281
    .local v7, "e":Ljava/lang/Exception;
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method static synthetic access$000(Lcom/sec/dsm/system/DSMRingMyMobile;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/dsm/system/DSMRingMyMobile;

    .prologue
    .line 42
    iget-boolean v0, p0, Lcom/sec/dsm/system/DSMRingMyMobile;->mRingStarted:Z

    return v0
.end method

.method static synthetic access$002(Lcom/sec/dsm/system/DSMRingMyMobile;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/dsm/system/DSMRingMyMobile;
    .param p1, "x1"    # Z

    .prologue
    .line 42
    iput-boolean p1, p0, Lcom/sec/dsm/system/DSMRingMyMobile;->mRingStarted:Z

    return p1
.end method

.method static synthetic access$100()I
    .locals 1

    .prologue
    .line 42
    sget v0, Lcom/sec/dsm/system/DSMRingMyMobile;->callState:I

    return v0
.end method

.method static synthetic access$102(I)I
    .locals 0
    .param p0, "x0"    # I

    .prologue
    .line 42
    sput p0, Lcom/sec/dsm/system/DSMRingMyMobile;->callState:I

    return p0
.end method

.method static synthetic access$200(Lcom/sec/dsm/system/DSMRingMyMobile;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/dsm/system/DSMRingMyMobile;
    .param p1, "x1"    # Z

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lcom/sec/dsm/system/DSMRingMyMobile;->lockSystemKey(Z)V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/dsm/system/DSMRingMyMobile;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/dsm/system/DSMRingMyMobile;
    .param p1, "x1"    # Z

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lcom/sec/dsm/system/DSMRingMyMobile;->setStatusBarCanHide(Z)V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/dsm/system/DSMRingMyMobile;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/dsm/system/DSMRingMyMobile;

    .prologue
    .line 42
    iget-boolean v0, p0, Lcom/sec/dsm/system/DSMRingMyMobile;->mPaused:Z

    return v0
.end method

.method static synthetic access$500(Lcom/sec/dsm/system/DSMRingMyMobile;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/dsm/system/DSMRingMyMobile;
    .param p1, "x1"    # I

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lcom/sec/dsm/system/DSMRingMyMobile;->playMaxAudio(I)V

    return-void
.end method

.method private getTopPackageName()Ljava/lang/String;
    .locals 6

    .prologue
    .line 469
    const-string v4, ""

    .line 470
    .local v4, "topPackageName":Ljava/lang/String;
    const-string v5, "activity"

    invoke-virtual {p0, v5}, Lcom/sec/dsm/system/DSMRingMyMobile;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 471
    .local v0, "actMgr":Landroid/app/ActivityManager;
    if-eqz v0, :cond_0

    .line 472
    const/4 v5, 0x1

    invoke-virtual {v0, v5}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v3

    .line 474
    .local v3, "runningTasks":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    if-eqz v3, :cond_0

    .line 475
    const/4 v5, 0x0

    :try_start_0
    invoke-interface {v3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/ActivityManager$RunningTaskInfo;

    iget-object v1, v5, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    .line 476
    .local v1, "component":Landroid/content/ComponentName;
    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 482
    .end local v1    # "component":Landroid/content/ComponentName;
    .end local v3    # "runningTasks":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    :cond_0
    :goto_0
    return-object v4

    .line 478
    .restart local v3    # "runningTasks":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    :catch_0
    move-exception v2

    .line 479
    .local v2, "e":Ljava/lang/Exception;
    const-string v4, "NoPackage"

    goto :goto_0
.end method

.method private lockSystemKey(Z)V
    .locals 1
    .param p1, "lock"    # Z

    .prologue
    .line 443
    const/4 v0, 0x3

    invoke-direct {p0, v0, p1}, Lcom/sec/dsm/system/DSMRingMyMobile;->requestSystemKeyEvent(IZ)Z

    move-result v0

    if-nez v0, :cond_0

    .line 444
    const-string v0, "KEYCODE_HOME set fail"

    invoke-static {v0}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 446
    :cond_0
    const/16 v0, 0x1a

    invoke-direct {p0, v0, p1}, Lcom/sec/dsm/system/DSMRingMyMobile;->requestSystemKeyEvent(IZ)Z

    move-result v0

    if-nez v0, :cond_1

    .line 447
    const-string v0, "KEYCODE_POWER set fail"

    invoke-static {v0}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 449
    :cond_1
    const/16 v0, 0xbb

    invoke-direct {p0, v0, p1}, Lcom/sec/dsm/system/DSMRingMyMobile;->requestSystemKeyEvent(IZ)Z

    move-result v0

    if-nez v0, :cond_2

    .line 450
    const-string v0, "KEYCODE_APP_SWITCH set fail"

    invoke-static {v0}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 452
    :cond_2
    return-void
.end method

.method private playMaxAudio(I)V
    .locals 5
    .param p1, "nChoice"    # I

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 314
    if-ne p1, v3, :cond_2

    .line 315
    const-string v1, "playMaxAudio +"

    invoke-static {v1}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 316
    sget-object v1, Lcom/sec/dsm/system/DSMRingMyMobile;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v1, v4}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result v0

    .line 317
    .local v0, "maxVolume":I
    sget-object v1, Lcom/sec/dsm/system/DSMRingMyMobile;->mAudioManager:Landroid/media/AudioManager;

    const/4 v2, 0x0

    invoke-virtual {v1, v4, v0, v2}, Landroid/media/AudioManager;->setStreamVolume(III)V

    .line 318
    const/4 v1, 0x4

    invoke-static {v1}, Landroid/media/RingtoneManager;->getDefaultUri(I)Landroid/net/Uri;

    move-result-object v1

    invoke-static {p0, v1}, Landroid/media/RingtoneManager;->getRingtone(Landroid/content/Context;Landroid/net/Uri;)Landroid/media/Ringtone;

    move-result-object v1

    sput-object v1, Lcom/sec/dsm/system/DSMRingMyMobile;->mAlarmRingtone:Landroid/media/Ringtone;

    .line 319
    sget-object v1, Lcom/sec/dsm/system/DSMRingMyMobile;->mAlarmRingtone:Landroid/media/Ringtone;

    if-nez v1, :cond_0

    .line 320
    const-string v1, "fail to get default Alarm Ringtone"

    invoke-static {v1}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 321
    invoke-static {v3}, Landroid/media/RingtoneManager;->getDefaultUri(I)Landroid/net/Uri;

    move-result-object v1

    invoke-static {p0, v1}, Landroid/media/RingtoneManager;->getRingtone(Landroid/content/Context;Landroid/net/Uri;)Landroid/media/Ringtone;

    move-result-object v1

    sput-object v1, Lcom/sec/dsm/system/DSMRingMyMobile;->mAlarmRingtone:Landroid/media/Ringtone;

    .line 323
    :cond_0
    sget-object v1, Lcom/sec/dsm/system/DSMRingMyMobile;->mAlarmRingtone:Landroid/media/Ringtone;

    if-eqz v1, :cond_1

    .line 324
    sget-object v1, Lcom/sec/dsm/system/DSMRingMyMobile;->mAlarmRingtone:Landroid/media/Ringtone;

    invoke-virtual {v1}, Landroid/media/Ringtone;->play()V

    .line 326
    :cond_1
    const-string v1, "playMaxAudio -"

    invoke-static {v1}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 328
    .end local v0    # "maxVolume":I
    :cond_2
    return-void
.end method

.method private requestSystemKeyEvent(IZ)Z
    .locals 3
    .param p1, "keyCode"    # I
    .param p2, "request"    # Z

    .prologue
    .line 455
    iget-object v1, p0, Lcom/sec/dsm/system/DSMRingMyMobile;->mWindowManager:Landroid/view/IWindowManager;

    if-nez v1, :cond_0

    .line 456
    const-string v1, "window"

    invoke-static {v1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v1

    invoke-static {v1}, Landroid/view/IWindowManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/IWindowManager;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/dsm/system/DSMRingMyMobile;->mWindowManager:Landroid/view/IWindowManager;

    .line 459
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/sec/dsm/system/DSMRingMyMobile;->mWindowManager:Landroid/view/IWindowManager;

    invoke-virtual {p0}, Lcom/sec/dsm/system/DSMRingMyMobile;->getComponentName()Landroid/content/ComponentName;

    move-result-object v2

    invoke-interface {v1, p1, v2, p2}, Landroid/view/IWindowManager;->requestSystemKeyEvent(ILandroid/content/ComponentName;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result v1

    .line 465
    :goto_0
    return v1

    .line 460
    :catch_0
    move-exception v0

    .line 461
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    .line 465
    .end local v0    # "e":Landroid/os/RemoteException;
    :goto_1
    const/4 v1, 0x0

    goto :goto_0

    .line 462
    :catch_1
    move-exception v0

    .line 463
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method private setStatusBarCanHide(Z)V
    .locals 2
    .param p1, "canHide"    # Z

    .prologue
    .line 406
    const-string v1, "statusbar"

    invoke-virtual {p0, v1}, Lcom/sec/dsm/system/DSMRingMyMobile;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/StatusBarManager;

    .line 407
    .local v0, "statusBar":Landroid/app/StatusBarManager;
    if-eqz p1, :cond_1

    .line 408
    if-eqz v0, :cond_0

    .line 409
    const/high16 v1, 0x10000

    invoke-virtual {v0, v1}, Landroid/app/StatusBarManager;->disable(I)V

    .line 414
    :cond_0
    :goto_0
    return-void

    .line 411
    :cond_1
    if-eqz v0, :cond_0

    .line 412
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/StatusBarManager;->disable(I)V

    goto :goto_0
.end method

.method private unregisterReceiver()V
    .locals 3

    .prologue
    .line 373
    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "unregisterReceiver : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v2, Lcom/sec/dsm/system/DSMRingMyMobile;->registerFlag:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 374
    iget-object v1, p0, Lcom/sec/dsm/system/DSMRingMyMobile;->quit_receiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1}, Lcom/sec/dsm/system/DSMRingMyMobile;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 375
    const/4 v1, 0x0

    sput-boolean v1, Lcom/sec/dsm/system/DSMRingMyMobile;->registerFlag:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 379
    :goto_0
    return-void

    .line 376
    :catch_0
    move-exception v0

    .line 377
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "UnregisterReceiver Exception : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method getCurrentVolume()V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 354
    const-string v0, "getCurrentVolume +"

    invoke-static {v0}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 355
    const-string v0, "audio"

    invoke-virtual {p0, v0}, Lcom/sec/dsm/system/DSMRingMyMobile;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    sput-object v0, Lcom/sec/dsm/system/DSMRingMyMobile;->mAudioManager:Landroid/media/AudioManager;

    .line 356
    sget-object v0, Lcom/sec/dsm/system/DSMRingMyMobile;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->getRingerMode()I

    move-result v0

    if-nez v0, :cond_1

    .line 357
    const-string v0, "Slient mode check +"

    invoke-static {v0}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 358
    iput-boolean v1, p0, Lcom/sec/dsm/system/DSMRingMyMobile;->mSilentmode:Z

    .line 359
    sget-object v0, Lcom/sec/dsm/system/DSMRingMyMobile;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0, v2}, Landroid/media/AudioManager;->setRingerMode(I)V

    .line 360
    const-string v0, "Slient mode check -"

    invoke-static {v0}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 367
    :cond_0
    :goto_0
    sget-object v0, Lcom/sec/dsm/system/DSMRingMyMobile;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0, v2}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v0

    sput v0, Lcom/sec/dsm/system/DSMRingMyMobile;->mOldVolume:I

    .line 368
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "getCurrentVolume -"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v1, Lcom/sec/dsm/system/DSMRingMyMobile;->mOldVolume:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 369
    return-void

    .line 361
    :cond_1
    sget-object v0, Lcom/sec/dsm/system/DSMRingMyMobile;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->getRingerMode()I

    move-result v0

    if-ne v0, v1, :cond_0

    .line 362
    const-string v0, "manner mode check +"

    invoke-static {v0}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 363
    iput-boolean v1, p0, Lcom/sec/dsm/system/DSMRingMyMobile;->mRingermode:Z

    .line 364
    sget-object v0, Lcom/sec/dsm/system/DSMRingMyMobile;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0, v2}, Landroid/media/AudioManager;->setRingerMode(I)V

    .line 365
    const-string v0, "manner mode check -"

    invoke-static {v0}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 0

    .prologue
    .line 435
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 17
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 62
    invoke-super/range {p0 .. p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 63
    const-string v13, "OnCreate"

    invoke-static {v13}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 64
    new-instance v2, Landroid/util/DisplayMetrics;

    invoke-direct {v2}, Landroid/util/DisplayMetrics;-><init>()V

    .line 65
    .local v2, "displayMetrics":Landroid/util/DisplayMetrics;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/dsm/system/DSMRingMyMobile;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v13

    invoke-interface {v13}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v13

    invoke-virtual {v13, v2}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 66
    iget v13, v2, Landroid/util/DisplayMetrics;->densityDpi:I

    int-to-float v1, v13

    .line 67
    .local v1, "deviceDPI":F
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Device DPI : "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 69
    invoke-virtual/range {p0 .. p0}, Lcom/sec/dsm/system/DSMRingMyMobile;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v13

    const-string v14, "zen_mode"

    const/4 v15, 0x0

    invoke-static {v13, v14, v15}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v13

    sput v13, Lcom/sec/dsm/system/DSMRingMyMobile;->backIntMode:I

    .line 70
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "backIntMode : "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    sget v14, Lcom/sec/dsm/system/DSMRingMyMobile;->backIntMode:I

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 71
    sget v13, Lcom/sec/dsm/system/DSMRingMyMobile;->backIntMode:I

    const/4 v14, 0x1

    if-ne v13, v14, :cond_0

    .line 72
    invoke-virtual/range {p0 .. p0}, Lcom/sec/dsm/system/DSMRingMyMobile;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v13

    const-string v14, "zen_mode"

    const/4 v15, 0x0

    invoke-static {v13, v14, v15}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 74
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/sec/dsm/system/DSMRingMyMobile;->getIntent()Landroid/content/Intent;

    move-result-object v13

    const-string v14, "message"

    invoke-virtual {v13, v14}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 75
    .local v6, "messageRing":Ljava/lang/String;
    const/4 v9, 0x0

    .line 77
    .local v9, "temp":I
    :try_start_0
    const-class v13, Landroid/view/View;

    const-string v14, "SYSTEM_UI_FLAG_REMOVE_NAVIGATION"

    invoke-virtual {v13, v14}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v4

    .line 78
    .local v4, "field":Ljava/lang/reflect/Field;
    if-eqz v4, :cond_1

    .line 79
    invoke-virtual {v4, v4}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v9

    .line 84
    .end local v4    # "field":Ljava/lang/reflect/Field;
    :cond_1
    :goto_0
    invoke-virtual/range {p0 .. p0}, Lcom/sec/dsm/system/DSMRingMyMobile;->getCurrentVolume()V

    .line 85
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/sec/dsm/system/DSMRingMyMobile;->mRingStarted:Z

    .line 86
    invoke-virtual/range {p0 .. p0}, Lcom/sec/dsm/system/DSMRingMyMobile;->getWindow()Landroid/view/Window;

    move-result-object v12

    .line 87
    .local v12, "win":Landroid/view/Window;
    const v13, 0x280480

    invoke-virtual {v12, v13}, Landroid/view/Window;->addFlags(I)V

    .line 93
    invoke-virtual {v12}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v5

    .line 94
    .local v5, "lp":Landroid/view/WindowManager$LayoutParams;
    iget v13, v5, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    const/high16 v14, 0x100000

    or-int/2addr v13, v14

    iput v13, v5, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    .line 95
    invoke-virtual {v12, v5}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 96
    const-string v13, "ro.build.scafe.cream"

    invoke-static {v13}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 97
    .local v11, "theme":Ljava/lang/String;
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "onCreate (theme) : "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 99
    const/high16 v13, 0x7f030000

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/dsm/system/DSMRingMyMobile;->setContentView(I)V

    .line 100
    const v13, 0x7f070002

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/dsm/system/DSMRingMyMobile;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/TextView;

    .line 101
    .local v10, "textMessage":Landroid/widget/TextView;
    invoke-virtual {v10, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 103
    const-string v13, "white"

    invoke-virtual {v11, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_3

    .line 104
    const/high16 v13, 0x7f070000

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/dsm/system/DSMRingMyMobile;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/dsm/system/DSMRingMyMobile;->mRingBg:Landroid/widget/ImageView;

    .line 105
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/dsm/system/DSMRingMyMobile;->mRingBg:Landroid/widget/ImageView;

    const v14, 0x7f020010

    invoke-virtual {v13, v14}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 106
    const-string v13, "#000000"

    invoke-static {v13}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v13

    invoke-virtual {v10, v13}, Landroid/widget/TextView;->setTextColor(I)V

    .line 113
    :goto_1
    const v13, 0x7f070001

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/dsm/system/DSMRingMyMobile;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/dsm/system/DSMRingMyMobile;->mPlaypcwView:Landroid/widget/ImageView;

    .line 114
    if-eqz v9, :cond_2

    .line 115
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/dsm/system/DSMRingMyMobile;->mPlaypcwView:Landroid/widget/ImageView;

    invoke-virtual {v13, v9}, Landroid/widget/ImageView;->setSystemUiVisibility(I)V

    .line 117
    :cond_2
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/dsm/system/DSMRingMyMobile;->mPlaypcwView:Landroid/widget/ImageView;

    invoke-virtual {v13}, Landroid/widget/ImageView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v13

    check-cast v13, Landroid/graphics/drawable/AnimationDrawable;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/dsm/system/DSMRingMyMobile;->mFrameAnimation:Landroid/graphics/drawable/AnimationDrawable;

    .line 119
    const v13, 0x7f070003

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/dsm/system/DSMRingMyMobile;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/sec/dsm/system/SlidingTab;

    .line 120
    .local v7, "tab":Lcom/sec/dsm/system/SlidingTab;
    new-instance v13, Lcom/sec/dsm/system/DSMRingMyMobile$1;

    move-object/from16 v0, p0

    invoke-direct {v13, v0}, Lcom/sec/dsm/system/DSMRingMyMobile$1;-><init>(Lcom/sec/dsm/system/DSMRingMyMobile;)V

    invoke-virtual {v7, v13}, Lcom/sec/dsm/system/SlidingTab;->setOnTriggerListener(Lcom/sec/dsm/system/SlidingTab$OnTriggerListener;)V

    .line 143
    new-instance v13, Lcom/sec/dsm/system/DSMRingMyMobile$2;

    move-object/from16 v0, p0

    invoke-direct {v13, v0}, Lcom/sec/dsm/system/DSMRingMyMobile$2;-><init>(Lcom/sec/dsm/system/DSMRingMyMobile;)V

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/dsm/system/DSMRingMyMobile;->mPhoneListener:Landroid/telephony/PhoneStateListener;

    .line 174
    const-string v13, "phone"

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/dsm/system/DSMRingMyMobile;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/telephony/TelephonyManager;

    .line 175
    .local v8, "telephonyManager":Landroid/telephony/TelephonyManager;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/dsm/system/DSMRingMyMobile;->mPhoneListener:Landroid/telephony/PhoneStateListener;

    const/16 v14, 0x20

    invoke-virtual {v8, v13, v14}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    .line 177
    sget-object v13, Lcom/sec/dsm/system/DSMRingMyMobile;->mAudioManager:Landroid/media/AudioManager;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/dsm/system/DSMRingMyMobile;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    const/4 v15, 0x3

    const/16 v16, 0x1

    invoke-virtual/range {v13 .. v16}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    .line 178
    return-void

    .line 81
    .end local v5    # "lp":Landroid/view/WindowManager$LayoutParams;
    .end local v7    # "tab":Lcom/sec/dsm/system/SlidingTab;
    .end local v8    # "telephonyManager":Landroid/telephony/TelephonyManager;
    .end local v10    # "textMessage":Landroid/widget/TextView;
    .end local v11    # "theme":Ljava/lang/String;
    .end local v12    # "win":Landroid/view/Window;
    :catch_0
    move-exception v3

    .line 82
    .local v3, "e":Ljava/lang/Exception;
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    .line 108
    .end local v3    # "e":Ljava/lang/Exception;
    .restart local v5    # "lp":Landroid/view/WindowManager$LayoutParams;
    .restart local v10    # "textMessage":Landroid/widget/TextView;
    .restart local v11    # "theme":Ljava/lang/String;
    .restart local v12    # "win":Landroid/view/Window;
    :cond_3
    const/high16 v13, 0x7f070000

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/dsm/system/DSMRingMyMobile;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/dsm/system/DSMRingMyMobile;->mRingBg:Landroid/widget/ImageView;

    .line 109
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/dsm/system/DSMRingMyMobile;->mRingBg:Landroid/widget/ImageView;

    const v14, 0x7f02000f

    invoke-virtual {v13, v14}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 110
    const-string v13, "#ebebeb"

    invoke-static {v13}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v13

    invoke-virtual {v10, v13}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_1
.end method

.method protected onDestroy()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 232
    const-string v2, "onDestroy begin"

    invoke-static {v2}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 233
    sget-object v2, Lcom/sec/dsm/system/DSMRingMyMobile;->mAudioManager:Landroid/media/AudioManager;

    iget-object v3, p0, Lcom/sec/dsm/system/DSMRingMyMobile;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v2, v3}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    .line 234
    const-string v2, "phone"

    invoke-virtual {p0, v2}, Lcom/sec/dsm/system/DSMRingMyMobile;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/TelephonyManager;

    .line 235
    .local v1, "telephonyManager":Landroid/telephony/TelephonyManager;
    iget-object v2, p0, Lcom/sec/dsm/system/DSMRingMyMobile;->mPhoneListener:Landroid/telephony/PhoneStateListener;

    invoke-virtual {v1, v2, v4}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    .line 236
    invoke-direct {p0, v4}, Lcom/sec/dsm/system/DSMRingMyMobile;->setStatusBarCanHide(Z)V

    .line 237
    invoke-direct {p0, v4}, Lcom/sec/dsm/system/DSMRingMyMobile;->lockSystemKey(Z)V

    .line 238
    iget-boolean v2, p0, Lcom/sec/dsm/system/DSMRingMyMobile;->mRingStarted:Z

    if-ne v2, v5, :cond_0

    .line 239
    invoke-virtual {p0, v5}, Lcom/sec/dsm/system/DSMRingMyMobile;->setOriginalVolume(I)V

    .line 241
    :cond_0
    iput-boolean v4, p0, Lcom/sec/dsm/system/DSMRingMyMobile;->mRingermode:Z

    .line 242
    iget-object v2, p0, Lcom/sec/dsm/system/DSMRingMyMobile;->mFrameAnimation:Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/AnimationDrawable;->stop()V

    .line 243
    sput v4, Lcom/sec/dsm/system/DSMRingMyMobile;->mOldVolume:I

    .line 244
    iput-boolean v4, p0, Lcom/sec/dsm/system/DSMRingMyMobile;->mRingStarted:Z

    .line 245
    sget-boolean v2, Lcom/sec/dsm/system/DSMRingMyMobile;->registerFlag:Z

    if-ne v2, v5, :cond_1

    .line 246
    const-string v2, "Receiver is registed"

    invoke-static {v2}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 253
    :goto_0
    invoke-direct {p0}, Lcom/sec/dsm/system/DSMRingMyMobile;->unregisterReceiver()V

    .line 255
    invoke-virtual {p0}, Lcom/sec/dsm/system/DSMRingMyMobile;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "zen_mode"

    sget v4, Lcom/sec/dsm/system/DSMRingMyMobile;->backIntMode:I

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 256
    const-string v2, "onDestroy end"

    invoke-static {v2}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 257
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 258
    return-void

    .line 249
    :cond_1
    const-string v2, "Receiver regist"

    invoke-static {v2}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 250
    new-instance v0, Landroid/content/IntentFilter;

    const-string v2, "android.intent.action.DSM_RINGTONE_TIMEOUT"

    invoke-direct {v0, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 251
    .local v0, "quitfilter":Landroid/content/IntentFilter;
    iget-object v2, p0, Lcom/sec/dsm/system/DSMRingMyMobile;->quit_receiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v2, v0}, Lcom/sec/dsm/system/DSMRingMyMobile;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    goto :goto_0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 430
    const/4 v0, 0x1

    return v0
.end method

.method public onKeyLongPress(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 439
    const/4 v0, 0x1

    return v0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 425
    const/4 v0, 0x1

    return v0
.end method

.method protected onPause()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 210
    const-string v2, "onPause"

    invoke-static {v2}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 211
    iput-boolean v5, p0, Lcom/sec/dsm/system/DSMRingMyMobile;->mPaused:Z

    .line 212
    invoke-direct {p0, v4}, Lcom/sec/dsm/system/DSMRingMyMobile;->setStatusBarCanHide(Z)V

    .line 213
    invoke-direct {p0, v4}, Lcom/sec/dsm/system/DSMRingMyMobile;->lockSystemKey(Z)V

    .line 214
    iget-boolean v2, p0, Lcom/sec/dsm/system/DSMRingMyMobile;->mRingStarted:Z

    if-ne v2, v5, :cond_0

    const-string v2, "com.sec.dsm.system"

    invoke-direct {p0}, Lcom/sec/dsm/system/DSMRingMyMobile;->getTopPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 215
    iput-boolean v4, p0, Lcom/sec/dsm/system/DSMRingMyMobile;->mRingStarted:Z

    .line 216
    const-string v2, "when onPause, pause ring my mobile"

    invoke-static {v2}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 219
    :try_start_0
    new-instance v1, Lcom/sec/dsm/system/DSMManager;

    invoke-direct {v1, p0}, Lcom/sec/dsm/system/DSMManager;-><init>(Landroid/content/Context;)V

    .line 220
    .local v1, "im":Lcom/sec/dsm/system/DSMManager;
    invoke-virtual {v1}, Lcom/sec/dsm/system/DSMManager;->removeDSMRing()V
    :try_end_0
    .catch Lcom/sec/dsm/system/DSMException; {:try_start_0 .. :try_end_0} :catch_0

    .line 224
    .end local v1    # "im":Lcom/sec/dsm/system/DSMManager;
    :goto_0
    invoke-virtual {p0, v5}, Lcom/sec/dsm/system/DSMRingMyMobile;->setOriginalVolume(I)V

    .line 225
    invoke-direct {p0, v4}, Lcom/sec/dsm/system/DSMRingMyMobile;->setStatusBarCanHide(Z)V

    .line 227
    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 228
    return-void

    .line 221
    :catch_0
    move-exception v0

    .line 222
    .local v0, "e":Lcom/sec/dsm/system/DSMException;
    invoke-virtual {v0}, Lcom/sec/dsm/system/DSMException;->printStackTrace()V

    goto :goto_0
.end method

.method protected onResume()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 182
    const-string v2, "onResume"

    invoke-static {v2}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 183
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onCallStateChanged (state) : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget v3, Lcom/sec/dsm/system/DSMRingMyMobile;->callState:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 184
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 185
    sget v2, Lcom/sec/dsm/system/DSMRingMyMobile;->callState:I

    if-nez v2, :cond_1

    .line 186
    iput-boolean v5, p0, Lcom/sec/dsm/system/DSMRingMyMobile;->mPaused:Z

    .line 187
    invoke-direct {p0, v4}, Lcom/sec/dsm/system/DSMRingMyMobile;->lockSystemKey(Z)V

    .line 188
    invoke-direct {p0, v4}, Lcom/sec/dsm/system/DSMRingMyMobile;->setStatusBarCanHide(Z)V

    .line 189
    const v2, 0x7f070003

    invoke-virtual {p0, v2}, Lcom/sec/dsm/system/DSMRingMyMobile;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/sec/dsm/system/SlidingTab;

    .line 190
    .local v1, "tab":Lcom/sec/dsm/system/SlidingTab;
    invoke-virtual {v1}, Lcom/sec/dsm/system/SlidingTab;->reInit()V

    .line 191
    iget-boolean v2, p0, Lcom/sec/dsm/system/DSMRingMyMobile;->mRingStarted:Z

    if-nez v2, :cond_0

    .line 192
    iput-boolean v4, p0, Lcom/sec/dsm/system/DSMRingMyMobile;->mRingStarted:Z

    .line 193
    invoke-static {p0, v4, v4}, Lcom/sec/dsm/system/DSMRingMyMobile;->RegisterRepeatingAlarmTime(Landroid/content/Context;ZI)V

    .line 194
    invoke-direct {p0, v4}, Lcom/sec/dsm/system/DSMRingMyMobile;->playMaxAudio(I)V

    .line 197
    new-instance v0, Landroid/content/IntentFilter;

    const-string v2, "android.intent.action.DSM_RINGTONE_TIMEOUT"

    invoke-direct {v0, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 198
    .local v0, "quitfilter":Landroid/content/IntentFilter;
    iget-object v2, p0, Lcom/sec/dsm/system/DSMRingMyMobile;->quit_receiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v2, v0}, Lcom/sec/dsm/system/DSMRingMyMobile;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 199
    sput-boolean v4, Lcom/sec/dsm/system/DSMRingMyMobile;->registerFlag:Z

    .line 201
    .end local v0    # "quitfilter":Landroid/content/IntentFilter;
    :cond_0
    const-string v2, "Register IntentFilter : Register IntentFilter onResume()"

    invoke-static {v2}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 206
    .end local v1    # "tab":Lcom/sec/dsm/system/SlidingTab;
    :goto_0
    return-void

    .line 203
    :cond_1
    iput-boolean v5, p0, Lcom/sec/dsm/system/DSMRingMyMobile;->mPaused:Z

    .line 204
    const-string v2, "Call is not end"

    invoke-static {v2}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onWindowFocusChanged(Z)V
    .locals 1
    .param p1, "hasFocus"    # Z

    .prologue
    .line 419
    iget-object v0, p0, Lcom/sec/dsm/system/DSMRingMyMobile;->mFrameAnimation:Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/AnimationDrawable;->start()V

    .line 420
    invoke-super {p0, p1}, Landroid/app/Activity;->onWindowFocusChanged(Z)V

    .line 421
    return-void
.end method

.method setOriginalVolume(I)V
    .locals 5
    .param p1, "nChoice"    # I

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 331
    const-string v0, "setOriginalVolume +"

    invoke-static {v0}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 332
    const-string v0, "audio"

    invoke-virtual {p0, v0}, Lcom/sec/dsm/system/DSMRingMyMobile;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    sput-object v0, Lcom/sec/dsm/system/DSMRingMyMobile;->mAudioManager:Landroid/media/AudioManager;

    .line 333
    sget-object v0, Lcom/sec/dsm/system/DSMRingMyMobile;->mAudioManager:Landroid/media/AudioManager;

    if-eqz v0, :cond_1

    .line 334
    if-ne p1, v3, :cond_0

    .line 335
    sget-object v0, Lcom/sec/dsm/system/DSMRingMyMobile;->mAudioManager:Landroid/media/AudioManager;

    const/4 v1, 0x2

    sget v2, Lcom/sec/dsm/system/DSMRingMyMobile;->mOldVolume:I

    invoke-virtual {v0, v1, v2, v4}, Landroid/media/AudioManager;->setStreamVolume(III)V

    .line 337
    :cond_0
    iget-boolean v0, p0, Lcom/sec/dsm/system/DSMRingMyMobile;->mRingermode:Z

    if-ne v0, v3, :cond_3

    .line 338
    const-string v0, "return manner mode true +"

    invoke-static {v0}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 339
    sget-object v0, Lcom/sec/dsm/system/DSMRingMyMobile;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0, v3}, Landroid/media/AudioManager;->setRingerMode(I)V

    .line 340
    const-string v0, "return manner mode true -"

    invoke-static {v0}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 347
    :cond_1
    :goto_0
    sget-object v0, Lcom/sec/dsm/system/DSMRingMyMobile;->mAlarmRingtone:Landroid/media/Ringtone;

    if-eqz v0, :cond_2

    .line 348
    sget-object v0, Lcom/sec/dsm/system/DSMRingMyMobile;->mAlarmRingtone:Landroid/media/Ringtone;

    invoke-virtual {v0}, Landroid/media/Ringtone;->stop()V

    .line 350
    :cond_2
    const-string v0, "setOriginalVolume -"

    invoke-static {v0}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 351
    return-void

    .line 341
    :cond_3
    iget-boolean v0, p0, Lcom/sec/dsm/system/DSMRingMyMobile;->mSilentmode:Z

    if-ne v0, v3, :cond_1

    .line 342
    const-string v0, "return Slient mode true +"

    invoke-static {v0}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 343
    sget-object v0, Lcom/sec/dsm/system/DSMRingMyMobile;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0, v4}, Landroid/media/AudioManager;->setRingerMode(I)V

    .line 344
    const-string v0, "return Slient mode true -"

    invoke-static {v0}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    goto :goto_0
.end method
