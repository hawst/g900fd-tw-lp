.class Lcom/sec/dsm/system/DSMTrackingService$MobileTrackingTimer$MobileTrackingTimerTask;
.super Ljava/util/TimerTask;
.source "DSMTrackingService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/dsm/system/DSMTrackingService$MobileTrackingTimer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MobileTrackingTimerTask"
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/dsm/system/DSMTrackingService$MobileTrackingTimer;


# direct methods
.method private constructor <init>(Lcom/sec/dsm/system/DSMTrackingService$MobileTrackingTimer;)V
    .locals 0

    .prologue
    .line 359
    iput-object p1, p0, Lcom/sec/dsm/system/DSMTrackingService$MobileTrackingTimer$MobileTrackingTimerTask;->this$1:Lcom/sec/dsm/system/DSMTrackingService$MobileTrackingTimer;

    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/dsm/system/DSMTrackingService$MobileTrackingTimer;Lcom/sec/dsm/system/DSMTrackingService$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/dsm/system/DSMTrackingService$MobileTrackingTimer;
    .param p2, "x1"    # Lcom/sec/dsm/system/DSMTrackingService$1;

    .prologue
    .line 359
    invoke-direct {p0, p1}, Lcom/sec/dsm/system/DSMTrackingService$MobileTrackingTimer$MobileTrackingTimerTask;-><init>(Lcom/sec/dsm/system/DSMTrackingService$MobileTrackingTimer;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 20

    .prologue
    .line 362
    const-string v16, "=========[ MobileTrackingTimerTask Running ]=========="

    invoke-static/range {v16 .. v16}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 363
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    .line 364
    .local v8, "nCurrentTime":J
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/dsm/system/DSMTrackingService$MobileTrackingTimer$MobileTrackingTimerTask;->this$1:Lcom/sec/dsm/system/DSMTrackingService$MobileTrackingTimer;

    move-object/from16 v16, v0

    # getter for: Lcom/sec/dsm/system/DSMTrackingService$MobileTrackingTimer;->mInterval:I
    invoke-static/range {v16 .. v16}, Lcom/sec/dsm/system/DSMTrackingService$MobileTrackingTimer;->access$300(Lcom/sec/dsm/system/DSMTrackingService$MobileTrackingTimer;)I

    move-result v16

    mul-int/lit8 v16, v16, 0x3c

    move/from16 v0, v16

    mul-int/lit16 v12, v0, 0x3e8

    .line 365
    .local v12, "tInterval":I
    new-instance v13, Landroid/text/format/Time;

    invoke-direct {v13}, Landroid/text/format/Time;-><init>()V

    .line 366
    .local v13, "time":Landroid/text/format/Time;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/dsm/system/DSMTrackingService$MobileTrackingTimer$MobileTrackingTimerTask;->this$1:Lcom/sec/dsm/system/DSMTrackingService$MobileTrackingTimer;

    move-object/from16 v16, v0

    # getter for: Lcom/sec/dsm/system/DSMTrackingService$MobileTrackingTimer;->mStartDate:Ljava/lang/String;
    invoke-static/range {v16 .. v16}, Lcom/sec/dsm/system/DSMTrackingService$MobileTrackingTimer;->access$400(Lcom/sec/dsm/system/DSMTrackingService$MobileTrackingTimer;)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v13, v0}, Landroid/text/format/Time;->parse(Ljava/lang/String;)Z

    .line 367
    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-virtual {v13, v0}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v14

    .line 368
    .local v14, "tStartDate":J
    sub-long v10, v8, v14

    .line 369
    .local v10, "nSyncTime":J
    int-to-long v0, v12

    move-wide/from16 v16, v0

    rem-long v16, v10, v16

    const-wide/16 v18, 0x3e8

    cmp-long v16, v16, v18

    if-lez v16, :cond_1

    .line 408
    :cond_0
    :goto_0
    return-void

    .line 373
    :cond_1
    const/16 v16, 0x0

    invoke-static/range {v16 .. v16}, Lcom/sec/dsm/system/DSMTrackingService;->wsMobileTrackingAdpSetReTrackingActive(Z)V

    .line 377
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/dsm/system/DSMTrackingService$MobileTrackingTimer$MobileTrackingTimerTask;->this$1:Lcom/sec/dsm/system/DSMTrackingService$MobileTrackingTimer;

    move-object/from16 v16, v0

    # getter for: Lcom/sec/dsm/system/DSMTrackingService$MobileTrackingTimer;->mStartDate:Ljava/lang/String;
    invoke-static/range {v16 .. v16}, Lcom/sec/dsm/system/DSMTrackingService$MobileTrackingTimer;->access$400(Lcom/sec/dsm/system/DSMTrackingService$MobileTrackingTimer;)Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/dsm/system/DSMTrackingService$MobileTrackingTimer$MobileTrackingTimerTask;->this$1:Lcom/sec/dsm/system/DSMTrackingService$MobileTrackingTimer;

    move-object/from16 v17, v0

    # getter for: Lcom/sec/dsm/system/DSMTrackingService$MobileTrackingTimer;->mEndDate:Ljava/lang/String;
    invoke-static/range {v17 .. v17}, Lcom/sec/dsm/system/DSMTrackingService$MobileTrackingTimer;->access$500(Lcom/sec/dsm/system/DSMTrackingService$MobileTrackingTimer;)Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Lcom/sec/dsm/system/DSMTrackingService;->wsMobileTrackingAdpCheckDate(Ljava/lang/String;Ljava/lang/String;)I

    move-result v7

    .line 378
    .local v7, "nCheckDate":I
    sget v16, Lcom/sec/dsm/system/Constants;->OSPS_MOBILE_TRACKING_OPERATION_TRACKING_OK:I

    move/from16 v0, v16

    if-ne v7, v0, :cond_3

    .line 379
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/dsm/system/DSMTrackingService$MobileTrackingTimer$MobileTrackingTimerTask;->this$1:Lcom/sec/dsm/system/DSMTrackingService$MobileTrackingTimer;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/sec/dsm/system/DSMTrackingService$MobileTrackingTimer;->this$0:Lcom/sec/dsm/system/DSMTrackingService;

    move-object/from16 v16, v0

    const-string v17, "location"

    invoke-virtual/range {v16 .. v17}, Lcom/sec/dsm/system/DSMTrackingService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/location/LocationManager;

    .line 380
    .local v6, "lm":Landroid/location/LocationManager;
    const-string v16, "gps"

    move-object/from16 v0, v16

    invoke-virtual {v6, v0}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v16

    const/16 v17, 0x1

    move/from16 v0, v16

    move/from16 v1, v17

    if-ne v0, v1, :cond_2

    .line 381
    invoke-static {}, Lcom/sec/dsm/system/DSMTrackingService;->wsMobileTrackingAdpGetGpsValid()Z

    move-result v3

    .line 382
    .local v3, "bGPSVaild":Z
    if-eqz v3, :cond_2

    .line 383
    # invokes: Lcom/sec/dsm/system/DSMTrackingService;->wsMobileTrackingAdpSetTrackingLocation()V
    invoke-static {}, Lcom/sec/dsm/system/DSMTrackingService;->access$100()V

    .line 385
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/dsm/system/DSMTrackingService$MobileTrackingTimer$MobileTrackingTimerTask;->this$1:Lcom/sec/dsm/system/DSMTrackingService$MobileTrackingTimer;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/sec/dsm/system/DSMTrackingService$MobileTrackingTimer;->this$0:Lcom/sec/dsm/system/DSMTrackingService;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/sec/dsm/system/DSMTrackingService;->wsMobileTrackingAdpCallLocationLocations()V
    :try_end_0
    .catch Lcom/sec/dsm/system/osp/DeviceException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 386
    :catch_0
    move-exception v4

    .line 387
    .local v4, "e":Lcom/sec/dsm/system/osp/DeviceException;
    invoke-virtual {v4}, Lcom/sec/dsm/system/osp/DeviceException;->printStackTrace()V

    goto :goto_0

    .line 392
    .end local v3    # "bGPSVaild":Z
    .end local v4    # "e":Lcom/sec/dsm/system/osp/DeviceException;
    :cond_2
    const-string v16, "GPS Not Valid. GPS Restart"

    invoke-static/range {v16 .. v16}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 393
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/dsm/system/DSMTrackingService$MobileTrackingTimer$MobileTrackingTimerTask;->this$1:Lcom/sec/dsm/system/DSMTrackingService$MobileTrackingTimer;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/sec/dsm/system/DSMTrackingService$MobileTrackingTimer;->this$0:Lcom/sec/dsm/system/DSMTrackingService;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/sec/dsm/system/DSMTrackingService;->wsMobileTrackingAdpStartGPSTracking()V

    .line 394
    const/16 v2, 0x1388

    .line 395
    .local v2, "GPS_GET_INTERVAL":I
    new-instance v5, Lcom/sec/dsm/system/DSMTrackingService$GPSTrackingTimer;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/dsm/system/DSMTrackingService$MobileTrackingTimer$MobileTrackingTimerTask;->this$1:Lcom/sec/dsm/system/DSMTrackingService$MobileTrackingTimer;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/sec/dsm/system/DSMTrackingService$MobileTrackingTimer;->this$0:Lcom/sec/dsm/system/DSMTrackingService;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-direct {v5, v0, v2}, Lcom/sec/dsm/system/DSMTrackingService$GPSTrackingTimer;-><init>(Lcom/sec/dsm/system/DSMTrackingService;I)V

    .line 396
    .local v5, "gpsTimer":Lcom/sec/dsm/system/DSMTrackingService$GPSTrackingTimer;
    invoke-virtual {v5}, Lcom/sec/dsm/system/DSMTrackingService$GPSTrackingTimer;->startTimer()V

    goto/16 :goto_0

    .line 397
    .end local v2    # "GPS_GET_INTERVAL":I
    .end local v5    # "gpsTimer":Lcom/sec/dsm/system/DSMTrackingService$GPSTrackingTimer;
    .end local v6    # "lm":Landroid/location/LocationManager;
    :cond_3
    sget v16, Lcom/sec/dsm/system/Constants;->OSPS_MOBILE_TRACKING_OPERATION_TRACKING_DONE:I

    move/from16 v0, v16

    if-ne v7, v0, :cond_4

    .line 398
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/dsm/system/DSMTrackingService$MobileTrackingTimer$MobileTrackingTimerTask;->this$1:Lcom/sec/dsm/system/DSMTrackingService$MobileTrackingTimer;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/sec/dsm/system/DSMTrackingService$MobileTrackingTimer;->endTimer()V

    .line 399
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/dsm/system/DSMTrackingService$MobileTrackingTimer$MobileTrackingTimerTask;->this$1:Lcom/sec/dsm/system/DSMTrackingService$MobileTrackingTimer;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/sec/dsm/system/DSMTrackingService$MobileTrackingTimer;->this$0:Lcom/sec/dsm/system/DSMTrackingService;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/sec/dsm/system/DSMTrackingService;->wsMobileTrackingAdpStopGPSTracking()V

    .line 400
    # getter for: Lcom/sec/dsm/system/DSMTrackingService;->TrackingWakeLock:Landroid/os/PowerManager$WakeLock;
    invoke-static {}, Lcom/sec/dsm/system/DSMTrackingService;->access$600()Landroid/os/PowerManager$WakeLock;

    move-result-object v16

    if-eqz v16, :cond_0

    .line 401
    # getter for: Lcom/sec/dsm/system/DSMTrackingService;->TrackingWakeLock:Landroid/os/PowerManager$WakeLock;
    invoke-static {}, Lcom/sec/dsm/system/DSMTrackingService;->access$600()Landroid/os/PowerManager$WakeLock;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 402
    const/16 v16, 0x0

    # setter for: Lcom/sec/dsm/system/DSMTrackingService;->TrackingWakeLock:Landroid/os/PowerManager$WakeLock;
    invoke-static/range {v16 .. v16}, Lcom/sec/dsm/system/DSMTrackingService;->access$602(Landroid/os/PowerManager$WakeLock;)Landroid/os/PowerManager$WakeLock;

    goto/16 :goto_0

    .line 406
    :cond_4
    const-string v16, "MobileTrackingTimerTask Running Continue"

    invoke-static/range {v16 .. v16}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    goto/16 :goto_0
.end method
