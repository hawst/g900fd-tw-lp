.class public Lcom/sec/dsm/system/GPSUtil;
.super Ljava/lang/Object;
.source "GPSUtil.java"


# static fields
.field static Gloc:Landroid/location/Location;

.field static LGloc:Landroid/location/Location;

.field static LNloc:Landroid/location/Location;

.field static Nloc:Landroid/location/Location;

.field public static cellid:Ljava/lang/String;

.field static expired:Z

.field static isGpsValid:Z

.field static isNonGpsSIMValid:Z

.field static isNonGpsValid:Z

.field public static lac:Ljava/lang/String;

.field static lm:Landroid/location/LocationManager;

.field static mcontext:Landroid/content/Context;

.field static timerrun:Z


# instance fields
.field GPSLocationListener:Landroid/location/LocationListener;

.field NGPSLocationListener:Landroid/location/LocationListener;

.field public mAccuracy:D

.field public mAltitude:D

.field public mBearing:D

.field public mLatitude:D

.field public mLongitude:D

.field public mSpeed:D


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 11
    sput-boolean v0, Lcom/sec/dsm/system/GPSUtil;->isNonGpsValid:Z

    .line 12
    sput-boolean v0, Lcom/sec/dsm/system/GPSUtil;->isGpsValid:Z

    .line 13
    sput-boolean v0, Lcom/sec/dsm/system/GPSUtil;->isNonGpsSIMValid:Z

    .line 22
    sput-boolean v0, Lcom/sec/dsm/system/GPSUtil;->expired:Z

    .line 23
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/dsm/system/GPSUtil;->timerrun:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const-wide/16 v0, 0x0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-wide v0, p0, Lcom/sec/dsm/system/GPSUtil;->mLatitude:D

    .line 15
    iput-wide v0, p0, Lcom/sec/dsm/system/GPSUtil;->mLongitude:D

    .line 16
    iput-wide v0, p0, Lcom/sec/dsm/system/GPSUtil;->mAltitude:D

    .line 17
    iput-wide v0, p0, Lcom/sec/dsm/system/GPSUtil;->mBearing:D

    .line 18
    iput-wide v0, p0, Lcom/sec/dsm/system/GPSUtil;->mSpeed:D

    .line 19
    iput-wide v0, p0, Lcom/sec/dsm/system/GPSUtil;->mAccuracy:D

    .line 114
    new-instance v0, Lcom/sec/dsm/system/GPSUtil$1;

    invoke-direct {v0, p0}, Lcom/sec/dsm/system/GPSUtil$1;-><init>(Lcom/sec/dsm/system/GPSUtil;)V

    iput-object v0, p0, Lcom/sec/dsm/system/GPSUtil;->GPSLocationListener:Landroid/location/LocationListener;

    .line 136
    new-instance v0, Lcom/sec/dsm/system/GPSUtil$2;

    invoke-direct {v0, p0}, Lcom/sec/dsm/system/GPSUtil$2;-><init>(Lcom/sec/dsm/system/GPSUtil;)V

    iput-object v0, p0, Lcom/sec/dsm/system/GPSUtil;->NGPSLocationListener:Landroid/location/LocationListener;

    return-void
.end method


# virtual methods
.method public DSMgetGPSData()[D
    .locals 6

    .prologue
    .line 54
    sget-object v2, Lcom/sec/dsm/system/GPSUtil;->Nloc:Landroid/location/Location;

    if-eqz v2, :cond_1

    sget-object v2, Lcom/sec/dsm/system/GPSUtil;->Gloc:Landroid/location/Location;

    if-eqz v2, :cond_1

    .line 55
    sget-object v2, Lcom/sec/dsm/system/GPSUtil;->Nloc:Landroid/location/Location;

    invoke-virtual {v2}, Landroid/location/Location;->getTime()J

    move-result-wide v2

    sget-object v4, Lcom/sec/dsm/system/GPSUtil;->Gloc:Landroid/location/Location;

    invoke-virtual {v4}, Landroid/location/Location;->getTime()J

    move-result-wide v4

    sub-long v0, v2, v4

    .line 56
    .local v0, "timeDiff":J
    const-wide/16 v2, 0x7530

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    .line 57
    sget-object v2, Lcom/sec/dsm/system/GPSUtil;->Nloc:Landroid/location/Location;

    invoke-virtual {p0, v2}, Lcom/sec/dsm/system/GPSUtil;->DSMgetLastGPSData(Landroid/location/Location;)[D

    move-result-object v2

    .line 65
    .end local v0    # "timeDiff":J
    :goto_0
    return-object v2

    .line 59
    .restart local v0    # "timeDiff":J
    :cond_0
    sget-object v2, Lcom/sec/dsm/system/GPSUtil;->Gloc:Landroid/location/Location;

    invoke-virtual {p0, v2}, Lcom/sec/dsm/system/GPSUtil;->DSMgetLastGPSData(Landroid/location/Location;)[D

    move-result-object v2

    goto :goto_0

    .line 60
    .end local v0    # "timeDiff":J
    :cond_1
    sget-object v2, Lcom/sec/dsm/system/GPSUtil;->Gloc:Landroid/location/Location;

    if-eqz v2, :cond_2

    .line 61
    sget-object v2, Lcom/sec/dsm/system/GPSUtil;->Gloc:Landroid/location/Location;

    invoke-virtual {p0, v2}, Lcom/sec/dsm/system/GPSUtil;->DSMgetLastGPSData(Landroid/location/Location;)[D

    move-result-object v2

    goto :goto_0

    .line 62
    :cond_2
    sget-object v2, Lcom/sec/dsm/system/GPSUtil;->Nloc:Landroid/location/Location;

    if-eqz v2, :cond_3

    .line 63
    sget-object v2, Lcom/sec/dsm/system/GPSUtil;->Nloc:Landroid/location/Location;

    invoke-virtual {p0, v2}, Lcom/sec/dsm/system/GPSUtil;->DSMgetLastGPSData(Landroid/location/Location;)[D

    move-result-object v2

    goto :goto_0

    .line 65
    :cond_3
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public DSMgetLastGPSData(Landroid/location/Location;)[D
    .locals 4
    .param p1, "location"    # Landroid/location/Location;

    .prologue
    .line 69
    const/4 v1, 0x6

    new-array v0, v1, [D

    fill-array-data v0, :array_0

    .line 71
    .local v0, "gpsdata":[D
    const/4 v1, 0x0

    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    aput-wide v2, v0, v1

    .line 72
    const/4 v1, 0x1

    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v2

    aput-wide v2, v0, v1

    .line 73
    const/4 v1, 0x2

    invoke-virtual {p1}, Landroid/location/Location;->getAltitude()D

    move-result-wide v2

    aput-wide v2, v0, v1

    .line 74
    const/4 v1, 0x3

    invoke-virtual {p1}, Landroid/location/Location;->getBearing()F

    move-result v2

    float-to-double v2, v2

    aput-wide v2, v0, v1

    .line 75
    const/4 v1, 0x4

    invoke-virtual {p1}, Landroid/location/Location;->getSpeed()F

    move-result v2

    float-to-double v2, v2

    aput-wide v2, v0, v1

    .line 76
    const/4 v1, 0x5

    invoke-virtual {p1}, Landroid/location/Location;->getAccuracy()F

    move-result v2

    float-to-double v2, v2

    aput-wide v2, v0, v1

    .line 77
    return-object v0

    .line 69
    :array_0
    .array-data 8
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
    .end array-data
.end method

.method public DSMremoveListener()V
    .locals 3

    .prologue
    .line 159
    :try_start_0
    sget-object v1, Lcom/sec/dsm/system/GPSUtil;->lm:Landroid/location/LocationManager;

    if-eqz v1, :cond_0

    .line 160
    sget-object v1, Lcom/sec/dsm/system/GPSUtil;->lm:Landroid/location/LocationManager;

    iget-object v2, p0, Lcom/sec/dsm/system/GPSUtil;->GPSLocationListener:Landroid/location/LocationListener;

    invoke-virtual {v1, v2}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    .line 161
    sget-object v1, Lcom/sec/dsm/system/GPSUtil;->lm:Landroid/location/LocationManager;

    iget-object v2, p0, Lcom/sec/dsm/system/GPSUtil;->NGPSLocationListener:Landroid/location/LocationListener;

    invoke-virtual {v1, v2}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    .line 168
    :goto_0
    return-void

    .line 163
    :cond_0
    const-string v1, "null"

    invoke-static {v1}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 165
    :catch_0
    move-exception v0

    .line 166
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public DSMstartGPSTracking(Landroid/content/Context;)V
    .locals 7
    .param p1, "pcontext"    # Landroid/content/Context;

    .prologue
    .line 82
    :try_start_0
    const-string v0, "location"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    sput-object v0, Lcom/sec/dsm/system/GPSUtil;->lm:Landroid/location/LocationManager;

    .line 84
    sget-object v0, Lcom/sec/dsm/system/GPSUtil;->lm:Landroid/location/LocationManager;

    const-string v1, "network"

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 85
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "network"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Secure;->setLocationProviderEnabled(Landroid/content/ContentResolver;Ljava/lang/String;Z)V

    .line 88
    const-string v0, "enable the network provider"

    invoke-static {v0}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 90
    :cond_0
    sget-object v0, Lcom/sec/dsm/system/GPSUtil;->lm:Landroid/location/LocationManager;

    const-string v1, "gps"

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 91
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "gps"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Secure;->setLocationProviderEnabled(Landroid/content/ContentResolver;Ljava/lang/String;Z)V

    .line 93
    const-string v0, "enable the GPS_PROVIDER"

    invoke-static {v0}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 95
    :cond_1
    sget-object v0, Lcom/sec/dsm/system/GPSUtil;->lm:Landroid/location/LocationManager;

    const-string v1, "network"

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 96
    sget-object v0, Lcom/sec/dsm/system/GPSUtil;->lm:Landroid/location/LocationManager;

    const-string v1, "network"

    const-wide/16 v2, 0x7d0

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/sec/dsm/system/GPSUtil;->NGPSLocationListener:Landroid/location/LocationListener;

    invoke-virtual/range {v0 .. v5}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;)V

    .line 98
    const-string v0, "Network GPS listener is starting"

    invoke-static {v0}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 100
    :cond_2
    sget-object v0, Lcom/sec/dsm/system/GPSUtil;->lm:Landroid/location/LocationManager;

    const-string v1, "gps"

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 101
    sget-object v0, Lcom/sec/dsm/system/GPSUtil;->lm:Landroid/location/LocationManager;

    const-string v1, "gps"

    const-wide/16 v2, 0x7d0

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/sec/dsm/system/GPSUtil;->GPSLocationListener:Landroid/location/LocationListener;

    invoke-virtual/range {v0 .. v5}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;)V

    .line 103
    const-string v0, "GPS listener is starting"

    invoke-static {v0}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 105
    :cond_3
    sget-object v0, Lcom/sec/dsm/system/GPSUtil;->lm:Landroid/location/LocationManager;

    const-string v1, "gps"

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    sget-object v0, Lcom/sec/dsm/system/GPSUtil;->lm:Landroid/location/LocationManager;

    const-string v1, "network"

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 107
    const-string v0, "All Providers are disabled "

    invoke-static {v0}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 112
    :cond_4
    :goto_0
    return-void

    .line 109
    :catch_0
    move-exception v6

    .line 110
    .local v6, "e":Ljava/lang/Exception;
    invoke-virtual {v6}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public GetGPSLocation()Z
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 32
    const/4 v0, 0x0

    .line 33
    .local v0, "gpsdata":[D
    invoke-virtual {p0}, Lcom/sec/dsm/system/GPSUtil;->DSMgetGPSData()[D

    move-result-object v0

    .line 35
    if-nez v0, :cond_0

    .line 36
    sput-boolean v1, Lcom/sec/dsm/system/GPSUtil;->isGpsValid:Z

    .line 37
    const-string v2, "GPSdata is null"

    invoke-static {v2}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 49
    :goto_0
    return v1

    .line 40
    :cond_0
    sput-boolean v2, Lcom/sec/dsm/system/GPSUtil;->isGpsValid:Z

    .line 41
    aget-wide v4, v0, v1

    iput-wide v4, p0, Lcom/sec/dsm/system/GPSUtil;->mLatitude:D

    .line 42
    aget-wide v4, v0, v2

    iput-wide v4, p0, Lcom/sec/dsm/system/GPSUtil;->mLongitude:D

    .line 43
    const/4 v1, 0x2

    aget-wide v4, v0, v1

    iput-wide v4, p0, Lcom/sec/dsm/system/GPSUtil;->mAltitude:D

    .line 44
    const/4 v1, 0x3

    aget-wide v4, v0, v1

    iput-wide v4, p0, Lcom/sec/dsm/system/GPSUtil;->mBearing:D

    .line 45
    const/4 v1, 0x4

    aget-wide v4, v0, v1

    iput-wide v4, p0, Lcom/sec/dsm/system/GPSUtil;->mSpeed:D

    .line 46
    const/4 v1, 0x5

    aget-wide v4, v0, v1

    iput-wide v4, p0, Lcom/sec/dsm/system/GPSUtil;->mAccuracy:D

    move v1, v2

    .line 49
    goto :goto_0
.end method
