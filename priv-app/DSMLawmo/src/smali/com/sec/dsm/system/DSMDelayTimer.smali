.class public Lcom/sec/dsm/system/DSMDelayTimer;
.super Ljava/lang/Object;
.source "DSMDelayTimer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/dsm/system/DSMDelayTimer$DSMDelayNetProviderTimerTask;
    }
.end annotation


# instance fields
.field private mDelayTimer:Ljava/util/Timer;


# direct methods
.method constructor <init>()V
    .locals 4

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/dsm/system/DSMDelayTimer;->mDelayTimer:Ljava/util/Timer;

    .line 14
    new-instance v1, Ljava/util/Timer;

    invoke-direct {v1}, Ljava/util/Timer;-><init>()V

    iput-object v1, p0, Lcom/sec/dsm/system/DSMDelayTimer;->mDelayTimer:Ljava/util/Timer;

    .line 15
    new-instance v0, Lcom/sec/dsm/system/DSMDelayTimer$DSMDelayNetProviderTimerTask;

    invoke-direct {v0, p0}, Lcom/sec/dsm/system/DSMDelayTimer$DSMDelayNetProviderTimerTask;-><init>(Lcom/sec/dsm/system/DSMDelayTimer;)V

    .line 16
    .local v0, "mTask":Lcom/sec/dsm/system/DSMDelayTimer$DSMDelayNetProviderTimerTask;
    iget-object v1, p0, Lcom/sec/dsm/system/DSMDelayTimer;->mDelayTimer:Ljava/util/Timer;

    const-wide/16 v2, 0x7d0

    invoke-virtual {v1, v0, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 17
    return-void
.end method

.method static synthetic access$002(Lcom/sec/dsm/system/DSMDelayTimer;Ljava/util/Timer;)Ljava/util/Timer;
    .locals 0
    .param p0, "x0"    # Lcom/sec/dsm/system/DSMDelayTimer;
    .param p1, "x1"    # Ljava/util/Timer;

    .prologue
    .line 8
    iput-object p1, p0, Lcom/sec/dsm/system/DSMDelayTimer;->mDelayTimer:Ljava/util/Timer;

    return-object p1
.end method


# virtual methods
.method public endDelayTimer()V
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/sec/dsm/system/DSMDelayTimer;->mDelayTimer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 23
    iget-object v0, p0, Lcom/sec/dsm/system/DSMDelayTimer;->mDelayTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 24
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/dsm/system/DSMDelayTimer;->mDelayTimer:Ljava/util/Timer;

    .line 26
    :cond_0
    return-void
.end method
