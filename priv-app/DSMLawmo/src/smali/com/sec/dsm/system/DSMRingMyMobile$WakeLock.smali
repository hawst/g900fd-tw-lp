.class Lcom/sec/dsm/system/DSMRingMyMobile$WakeLock;
.super Ljava/lang/Object;
.source "DSMRingMyMobile.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/dsm/system/DSMRingMyMobile;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "WakeLock"
.end annotation


# static fields
.field private static sWakeLock:Landroid/os/PowerManager$WakeLock;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 285
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static acquire(Landroid/content/Context;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 288
    sget-object v1, Lcom/sec/dsm/system/DSMRingMyMobile$WakeLock;->sWakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v1, :cond_0

    .line 289
    sget-object v1, Lcom/sec/dsm/system/DSMRingMyMobile$WakeLock;->sWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 291
    :cond_0
    const-string v1, "power"

    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 293
    .local v0, "pm":Landroid/os/PowerManager;
    const v1, 0x3000000a

    const-string v2, "PopupDlgPCW"

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    sput-object v1, Lcom/sec/dsm/system/DSMRingMyMobile$WakeLock;->sWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 296
    sget-object v1, Lcom/sec/dsm/system/DSMRingMyMobile$WakeLock;->sWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 297
    return-void
.end method

.method static goToSleepLCD(Landroid/content/Context;)V
    .locals 8
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 299
    const-string v3, "gotoSleep + "

    invoke-static {v3}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 300
    const-string v3, "power"

    invoke-virtual {p0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/PowerManager;

    .line 301
    .local v2, "pm":Landroid/os/PowerManager;
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    const-wide/16 v6, 0x2

    add-long v0, v4, v6

    .line 303
    .local v0, "offtime":J
    const-string v3, "gotoSleep - "

    invoke-static {v3}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 304
    return-void
.end method

.method static release()V
    .locals 1

    .prologue
    .line 306
    sget-object v0, Lcom/sec/dsm/system/DSMRingMyMobile$WakeLock;->sWakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_0

    .line 308
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/dsm/system/DSMRingMyMobile$WakeLock;->sWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 310
    :cond_0
    return-void
.end method
