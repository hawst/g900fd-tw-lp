.class Lcom/sec/dsm/system/GPSUtil$2;
.super Ljava/lang/Object;
.source "GPSUtil.java"

# interfaces
.implements Landroid/location/LocationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/dsm/system/GPSUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/dsm/system/GPSUtil;


# direct methods
.method constructor <init>(Lcom/sec/dsm/system/GPSUtil;)V
    .locals 0

    .prologue
    .line 136
    iput-object p1, p0, Lcom/sec/dsm/system/GPSUtil$2;->this$0:Lcom/sec/dsm/system/GPSUtil;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLocationChanged(Landroid/location/Location;)V
    .locals 10
    .param p1, "location"    # Landroid/location/Location;

    .prologue
    .line 138
    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v6

    .line 139
    .local v6, "latitute":D
    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v8

    .line 140
    .local v8, "longitude":D
    invoke-virtual {p1}, Landroid/location/Location;->getAltitude()D

    move-result-wide v2

    .line 141
    .local v2, "Altitute":D
    invoke-virtual {p1}, Landroid/location/Location;->getBearing()F

    move-result v1

    .line 142
    .local v1, "Bearing":F
    invoke-virtual {p1}, Landroid/location/Location;->getSpeed()F

    move-result v4

    .line 143
    .local v4, "Speed":F
    invoke-virtual {p1}, Landroid/location/Location;->getAccuracy()F

    move-result v0

    .line 147
    .local v0, "Accuracy":F
    sput-object p1, Lcom/sec/dsm/system/GPSUtil;->Nloc:Landroid/location/Location;

    .line 148
    return-void
.end method

.method public onProviderDisabled(Ljava/lang/String;)V
    .locals 0
    .param p1, "provider"    # Ljava/lang/String;

    .prologue
    .line 150
    return-void
.end method

.method public onProviderEnabled(Ljava/lang/String;)V
    .locals 0
    .param p1, "provider"    # Ljava/lang/String;

    .prologue
    .line 152
    return-void
.end method

.method public onStatusChanged(Ljava/lang/String;ILandroid/os/Bundle;)V
    .locals 0
    .param p1, "provider"    # Ljava/lang/String;
    .param p2, "status"    # I
    .param p3, "extras"    # Landroid/os/Bundle;

    .prologue
    .line 154
    return-void
.end method
