.class public Lcom/sec/dsm/system/osp/PropertyManager;
.super Ljava/lang/Object;
.source "PropertyManager.java"


# instance fields
.field private context:Landroid/content/Context;

.field private propertyRepository:Lcom/sec/dsm/system/osp/PropertyRepository;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/dsm/system/osp/PropertyException;
        }
    .end annotation

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    :try_start_0
    iput-object p1, p0, Lcom/sec/dsm/system/osp/PropertyManager;->context:Landroid/content/Context;

    .line 31
    new-instance v1, Lcom/sec/dsm/system/osp/PropertyRepository;

    iget-object v2, p0, Lcom/sec/dsm/system/osp/PropertyManager;->context:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/sec/dsm/system/osp/PropertyRepository;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/dsm/system/osp/PropertyManager;->propertyRepository:Lcom/sec/dsm/system/osp/PropertyRepository;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 41
    return-void

    .line 37
    :catch_0
    move-exception v0

    .line 38
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 39
    new-instance v1, Lcom/sec/dsm/system/osp/PropertyException;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/sec/dsm/system/osp/PropertyException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method public containsKey(Ljava/lang/String;)Z
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/dsm/system/osp/PropertyException;
        }
    .end annotation

    .prologue
    .line 50
    if-nez p1, :cond_0

    .line 51
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 53
    :cond_0
    iget-object v0, p0, Lcom/sec/dsm/system/osp/PropertyManager;->propertyRepository:Lcom/sec/dsm/system/osp/PropertyRepository;

    invoke-virtual {v0, p1}, Lcom/sec/dsm/system/osp/PropertyRepository;->containsKey(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public get(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/dsm/system/osp/PropertyException;
        }
    .end annotation

    .prologue
    .line 63
    if-nez p1, :cond_0

    .line 64
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 66
    :cond_0
    iget-object v0, p0, Lcom/sec/dsm/system/osp/PropertyManager;->propertyRepository:Lcom/sec/dsm/system/osp/PropertyRepository;

    invoke-virtual {v0, p1}, Lcom/sec/dsm/system/osp/PropertyRepository;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "defaultValue"    # Ljava/lang/String;

    .prologue
    .line 77
    const/4 v1, 0x0

    .line 79
    .local v1, "value":Ljava/lang/String;
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/sec/dsm/system/osp/PropertyManager;->get(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Lcom/sec/dsm/system/osp/PropertyException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 83
    :goto_0
    if-nez v1, :cond_0

    .line 84
    move-object v1, p2

    .line 86
    :cond_0
    return-object v1

    .line 80
    :catch_0
    move-exception v0

    .line 81
    .local v0, "e":Lcom/sec/dsm/system/osp/PropertyException;
    invoke-virtual {v0}, Lcom/sec/dsm/system/osp/PropertyException;->printStackTrace()V

    goto :goto_0
.end method

.method public put(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/dsm/system/osp/PropertyException;
        }
    .end annotation

    .prologue
    .line 97
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 98
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 100
    :cond_1
    iget-object v0, p0, Lcom/sec/dsm/system/osp/PropertyManager;->propertyRepository:Lcom/sec/dsm/system/osp/PropertyRepository;

    invoke-virtual {v0, p1, p2}, Lcom/sec/dsm/system/osp/PropertyRepository;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    return-void
.end method

.method public remove(Ljava/lang/String;)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/dsm/system/osp/PropertyException;
        }
    .end annotation

    .prologue
    .line 110
    if-nez p1, :cond_0

    .line 111
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 113
    :cond_0
    iget-object v0, p0, Lcom/sec/dsm/system/osp/PropertyManager;->propertyRepository:Lcom/sec/dsm/system/osp/PropertyRepository;

    invoke-virtual {v0, p1}, Lcom/sec/dsm/system/osp/PropertyRepository;->remove(Ljava/lang/String;)V

    .line 114
    return-void
.end method
