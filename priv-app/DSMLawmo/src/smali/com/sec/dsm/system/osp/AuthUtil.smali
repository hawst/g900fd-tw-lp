.class public Lcom/sec/dsm/system/osp/AuthUtil;
.super Ljava/lang/Object;
.source "AuthUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/dsm/system/osp/AuthUtil$ComparableParameter;,
        Lcom/sec/dsm/system/osp/AuthUtil$Parameter;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 416
    return-void
.end method

.method public static computeAuthTokenSign(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 11
    .param p0, "authToken"    # Ljava/lang/String;
    .param p1, "authTokenSecret"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/UnsupportedEncodingException;,
            Ljava/security/InvalidKeyException;,
            Ljava/lang/IllegalStateException;,
            Ljava/security/GeneralSecurityException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 59
    :try_start_0
    const-string v8, "UTF-8"

    invoke-virtual {p1, v8}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v8

    invoke-static {v8}, Lcom/sec/dsm/system/osp/Base64;->decode([B)[B

    move-result-object v1

    .line 60
    .local v1, "byteAuthTokenSecret":[B
    const-string v6, "e5d0e4cb-b0cc-4390-9cd6-138710d35ce4"

    .line 61
    .local v6, "vendorKey":Ljava/lang/String;
    const-string v8, "UTF-8"

    invoke-virtual {v6, v8}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v2

    .line 63
    .local v2, "byteVendorKey":[B
    if-eqz v1, :cond_0

    if-eqz v2, :cond_0

    .line 64
    array-length v8, v1

    array-length v9, v2

    add-int/2addr v8, v9

    new-array v5, v8, [B

    .line 65
    .local v5, "key":[B
    const/4 v8, 0x0

    const/4 v9, 0x0

    array-length v10, v1

    invoke-static {v1, v8, v5, v9, v10}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 66
    const/4 v8, 0x0

    array-length v9, v1

    array-length v10, v2

    invoke-static {v2, v8, v5, v9, v10}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 67
    invoke-static {v5, p0}, Lcom/sec/dsm/system/osp/AuthUtil;->computeSignature([BLjava/lang/String;)[B

    move-result-object v4

    .line 68
    .local v4, "expected":[B
    if-eqz v4, :cond_0

    .line 69
    new-instance v0, Ljava/lang/String;

    invoke-static {v4}, Lcom/sec/dsm/system/osp/Base64;->encode([B)[B

    move-result-object v8

    const-string v9, "UTF-8"

    invoke-direct {v0, v8, v9}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/InvalidKeyException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/security/GeneralSecurityException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_4

    .line 88
    .end local v1    # "byteAuthTokenSecret":[B
    .end local v2    # "byteVendorKey":[B
    .end local v4    # "expected":[B
    .end local v5    # "key":[B
    .end local v6    # "vendorKey":Ljava/lang/String;
    :goto_0
    return-object v0

    .restart local v1    # "byteAuthTokenSecret":[B
    .restart local v2    # "byteVendorKey":[B
    .restart local v6    # "vendorKey":Ljava/lang/String;
    :cond_0
    move-object v0, v7

    .line 73
    goto :goto_0

    .line 74
    .end local v1    # "byteAuthTokenSecret":[B
    .end local v2    # "byteVendorKey":[B
    .end local v6    # "vendorKey":Ljava/lang/String;
    :catch_0
    move-exception v3

    .line 75
    .local v3, "e":Ljava/io/UnsupportedEncodingException;
    invoke-virtual {v3}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    move-object v0, v7

    .line 76
    goto :goto_0

    .line 77
    .end local v3    # "e":Ljava/io/UnsupportedEncodingException;
    :catch_1
    move-exception v3

    .line 78
    .local v3, "e":Ljava/security/InvalidKeyException;
    invoke-virtual {v3}, Ljava/security/InvalidKeyException;->printStackTrace()V

    move-object v0, v7

    .line 79
    goto :goto_0

    .line 80
    .end local v3    # "e":Ljava/security/InvalidKeyException;
    :catch_2
    move-exception v3

    .line 81
    .local v3, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v3}, Ljava/lang/IllegalStateException;->printStackTrace()V

    move-object v0, v7

    .line 82
    goto :goto_0

    .line 83
    .end local v3    # "e":Ljava/lang/IllegalStateException;
    :catch_3
    move-exception v3

    .line 84
    .local v3, "e":Ljava/security/GeneralSecurityException;
    invoke-virtual {v3}, Ljava/security/GeneralSecurityException;->printStackTrace()V

    move-object v0, v7

    .line 85
    goto :goto_0

    .line 86
    .end local v3    # "e":Ljava/security/GeneralSecurityException;
    :catch_4
    move-exception v3

    .line 87
    .local v3, "e":Ljava/lang/Exception;
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    move-object v0, v7

    .line 88
    goto :goto_0
.end method

.method protected static computeSignature(Ljava/lang/String;Ljava/lang/String;Z)[B
    .locals 5
    .param p0, "keyStr"    # Ljava/lang/String;
    .param p1, "src"    # Ljava/lang/String;
    .param p2, "isKeyBase64Encoded"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 132
    const/4 v0, 0x0

    .line 133
    .local v0, "key":Ljavax/crypto/SecretKey;
    const/4 v1, 0x0

    .line 134
    .local v1, "keyBytes":[B
    if-eqz p2, :cond_0

    .line 135
    const-string v4, "UTF-8"

    invoke-virtual {p0, v4}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v4

    invoke-static {v4}, Lcom/sec/dsm/system/osp/Base64;->decode([B)[B

    move-result-object v1

    .line 139
    :goto_0
    new-instance v0, Ljavax/crypto/spec/SecretKeySpec;

    .end local v0    # "key":Ljavax/crypto/SecretKey;
    const-string v4, "HmacSHA1"

    invoke-direct {v0, v1, v4}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    .line 141
    .restart local v0    # "key":Ljavax/crypto/SecretKey;
    const-string v4, "HmacSHA1"

    invoke-static {v4}, Ljavax/crypto/Mac;->getInstance(Ljava/lang/String;)Ljavax/crypto/Mac;

    move-result-object v2

    .line 142
    .local v2, "mac":Ljavax/crypto/Mac;
    invoke-virtual {v2, v0}, Ljavax/crypto/Mac;->init(Ljava/security/Key;)V

    .line 143
    const-string v4, "UTF-8"

    invoke-virtual {p1, v4}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v3

    .line 144
    .local v3, "text":[B
    invoke-virtual {v2, v3}, Ljavax/crypto/Mac;->doFinal([B)[B

    move-result-object v4

    return-object v4

    .line 137
    .end local v2    # "mac":Ljavax/crypto/Mac;
    .end local v3    # "text":[B
    :cond_0
    const-string v4, "UTF-8"

    invoke-virtual {p0, v4}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v1

    goto :goto_0
.end method

.method private static computeSignature([BLjava/lang/String;)[B
    .locals 7
    .param p0, "keyBytes"    # [B
    .param p1, "src"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/InvalidKeyException;,
            Ljava/security/GeneralSecurityException;,
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 556
    const/4 v1, 0x0

    .line 558
    .local v1, "key":Ljavax/crypto/SecretKey;
    :try_start_0
    new-instance v2, Ljavax/crypto/spec/SecretKeySpec;

    const-string v6, "HmacSHA1"

    invoke-direct {v2, p0, v6}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/security/InvalidKeyException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/GeneralSecurityException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_3

    .line 560
    .end local v1    # "key":Ljavax/crypto/SecretKey;
    .local v2, "key":Ljavax/crypto/SecretKey;
    :try_start_1
    const-string v6, "HmacSHA1"

    invoke-static {v6}, Ljavax/crypto/Mac;->getInstance(Ljava/lang/String;)Ljavax/crypto/Mac;

    move-result-object v3

    .line 561
    .local v3, "mac":Ljavax/crypto/Mac;
    invoke-virtual {v3, v2}, Ljavax/crypto/Mac;->init(Ljava/security/Key;)V

    .line 562
    const-string v6, "UTF-8"

    invoke-virtual {p1, v6}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v4

    .line 563
    .local v4, "text":[B
    invoke-virtual {v3, v4}, Ljavax/crypto/Mac;->doFinal([B)[B
    :try_end_1
    .catch Ljava/security/InvalidKeyException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Ljava/security/GeneralSecurityException; {:try_start_1 .. :try_end_1} :catch_6
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_4

    move-result-object v5

    .line 575
    .end local v2    # "key":Ljavax/crypto/SecretKey;
    .end local v3    # "mac":Ljavax/crypto/Mac;
    .end local v4    # "text":[B
    :goto_0
    return-object v5

    .line 564
    .restart local v1    # "key":Ljavax/crypto/SecretKey;
    :catch_0
    move-exception v0

    .line 565
    .local v0, "e":Ljava/security/InvalidKeyException;
    :goto_1
    invoke-virtual {v0}, Ljava/security/InvalidKeyException;->printStackTrace()V

    goto :goto_0

    .line 567
    .end local v0    # "e":Ljava/security/InvalidKeyException;
    :catch_1
    move-exception v0

    .line 568
    .local v0, "e":Ljava/security/GeneralSecurityException;
    :goto_2
    invoke-virtual {v0}, Ljava/security/GeneralSecurityException;->printStackTrace()V

    goto :goto_0

    .line 570
    .end local v0    # "e":Ljava/security/GeneralSecurityException;
    :catch_2
    move-exception v0

    .line 571
    .local v0, "e":Ljava/lang/IllegalStateException;
    :goto_3
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0

    .line 573
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :catch_3
    move-exception v0

    .line 574
    .local v0, "e":Ljava/io/UnsupportedEncodingException;
    :goto_4
    invoke-virtual {v0}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto :goto_0

    .line 573
    .end local v0    # "e":Ljava/io/UnsupportedEncodingException;
    .end local v1    # "key":Ljavax/crypto/SecretKey;
    .restart local v2    # "key":Ljavax/crypto/SecretKey;
    :catch_4
    move-exception v0

    move-object v1, v2

    .end local v2    # "key":Ljavax/crypto/SecretKey;
    .restart local v1    # "key":Ljavax/crypto/SecretKey;
    goto :goto_4

    .line 570
    .end local v1    # "key":Ljavax/crypto/SecretKey;
    .restart local v2    # "key":Ljavax/crypto/SecretKey;
    :catch_5
    move-exception v0

    move-object v1, v2

    .end local v2    # "key":Ljavax/crypto/SecretKey;
    .restart local v1    # "key":Ljavax/crypto/SecretKey;
    goto :goto_3

    .line 567
    .end local v1    # "key":Ljavax/crypto/SecretKey;
    .restart local v2    # "key":Ljavax/crypto/SecretKey;
    :catch_6
    move-exception v0

    move-object v1, v2

    .end local v2    # "key":Ljavax/crypto/SecretKey;
    .restart local v1    # "key":Ljavax/crypto/SecretKey;
    goto :goto_2

    .line 564
    .end local v1    # "key":Ljavax/crypto/SecretKey;
    .restart local v2    # "key":Ljavax/crypto/SecretKey;
    :catch_7
    move-exception v0

    move-object v1, v2

    .end local v2    # "key":Ljavax/crypto/SecretKey;
    .restart local v1    # "key":Ljavax/crypto/SecretKey;
    goto :goto_1
.end method

.method private static decodePercent(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    .line 377
    :try_start_0
    const-string v1, "UTF-8"

    invoke-static {p0, v1}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    .line 379
    :catch_0
    move-exception v0

    .line 380
    .local v0, "wow":Ljava/io/UnsupportedEncodingException;
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-virtual {v0}, Ljava/io/UnsupportedEncodingException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method private static formEncode(Ljava/lang/Iterable;)Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Ljava/util/Map$Entry;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 481
    .local p0, "parameters":Ljava/lang/Iterable;, "Ljava/lang/Iterable<+Ljava/util/Map$Entry;>;"
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 482
    .local v0, "b":Ljava/io/ByteArrayOutputStream;
    invoke-static {p0, v0}, Lcom/sec/dsm/system/osp/AuthUtil;->formEncode(Ljava/lang/Iterable;Ljava/io/OutputStream;)V

    .line 483
    new-instance v1, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v2

    const-string v3, "UTF-8"

    invoke-direct {v1, v2, v3}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    return-object v1
.end method

.method private static formEncode(Ljava/lang/Iterable;Ljava/io/OutputStream;)V
    .locals 6
    .param p1, "into"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Ljava/util/Map$Entry;",
            ">;",
            "Ljava/io/OutputStream;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 502
    .local p0, "parameters":Ljava/lang/Iterable;, "Ljava/lang/Iterable<+Ljava/util/Map$Entry;>;"
    if-eqz p0, :cond_3

    .line 503
    const/4 v1, 0x1

    .line 504
    .local v1, "first":Z
    :try_start_0
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    .line 505
    .local v3, "parameter":Ljava/util/Map$Entry;
    if-eqz v1, :cond_2

    .line 506
    const/4 v1, 0x0

    .line 510
    :goto_1
    if-eqz v3, :cond_1

    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    if-eqz v5, :cond_1

    .line 511
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/dsm/system/osp/AuthUtil;->toString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/dsm/system/osp/AuthUtil;->percentEncode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 512
    .local v0, "encodedValue":Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 513
    const-string v5, "UTF-8"

    invoke-virtual {v0, v5}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v5

    invoke-virtual {p1, v5}, Ljava/io/OutputStream;->write([B)V

    .line 516
    .end local v0    # "encodedValue":Ljava/lang/String;
    :cond_1
    if-eqz v3, :cond_0

    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 517
    const/16 v5, 0x3d

    invoke-virtual {p1, v5}, Ljava/io/OutputStream;->write(I)V

    .line 518
    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/dsm/system/osp/AuthUtil;->toString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 519
    .local v4, "str":Ljava/lang/String;
    if-eqz v4, :cond_0

    .line 520
    const-string v5, "UTF-8"

    invoke-virtual {v4, v5}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v5

    invoke-virtual {p1, v5}, Ljava/io/OutputStream;->write([B)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 526
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "parameter":Ljava/util/Map$Entry;
    .end local v4    # "str":Ljava/lang/String;
    :catchall_0
    move-exception v5

    invoke-virtual {p1}, Ljava/io/OutputStream;->close()V

    throw v5

    .line 508
    .restart local v2    # "i$":Ljava/util/Iterator;
    .restart local v3    # "parameter":Ljava/util/Map$Entry;
    :cond_2
    const/16 v5, 0x26

    :try_start_1
    invoke-virtual {p1, v5}, Ljava/io/OutputStream;->write(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 526
    .end local v1    # "first":Z
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "parameter":Ljava/util/Map$Entry;
    :cond_3
    invoke-virtual {p1}, Ljava/io/OutputStream;->close()V

    .line 528
    return-void
.end method

.method protected static generateRandomToken(I)Ljava/lang/String;
    .locals 4
    .param p0, "nLength"    # I

    .prologue
    .line 102
    :try_start_0
    const-string v3, "SHA1PRNG"

    invoke-static {v3}, Ljava/security/SecureRandom;->getInstance(Ljava/lang/String;)Ljava/security/SecureRandom;

    move-result-object v2

    .line 103
    .local v2, "securerandom":Ljava/security/SecureRandom;
    div-int/lit8 v3, p0, 0x2

    new-array v0, v3, [B

    .line 104
    .local v0, "byRandom":[B
    invoke-virtual {v2, v0}, Ljava/security/SecureRandom;->nextBytes([B)V

    .line 106
    invoke-static {v0}, Lcom/sec/dsm/system/osp/AuthUtil;->toHex([B)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 111
    .end local v0    # "byRandom":[B
    .end local v2    # "securerandom":Ljava/security/SecureRandom;
    :goto_0
    return-object v3

    .line 108
    :catch_0
    move-exception v1

    .line 109
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 111
    const/4 v3, 0x0

    goto :goto_0
.end method

.method private static getOSPAuthHttpHeadersFromString(Ljava/lang/String;)[Ljava/lang/String;
    .locals 4
    .param p0, "sAuth"    # Ljava/lang/String;

    .prologue
    .line 321
    const-string v2, "OAuth"

    const-string v3, ""

    invoke-virtual {p0, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 326
    const-string v2, ","

    invoke-virtual {p0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 327
    .local v0, "authEntries":[Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v2, v0

    if-ge v1, v2, :cond_0

    .line 328
    aget-object v2, v0, v1

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 327
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 330
    :cond_0
    return-object v0
.end method

.method private static getParameters(Ljava/util/Collection;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/sec/dsm/system/osp/AuthUtil$ComparableParameter;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Ljava/util/Map$Entry;",
            ">;"
        }
    .end annotation

    .prologue
    .line 458
    .local p0, "parameters":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/sec/dsm/system/osp/AuthUtil$ComparableParameter;>;"
    if-nez p0, :cond_1

    .line 459
    const/4 v1, 0x0

    .line 465
    :cond_0
    return-object v1

    .line 461
    :cond_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {p0}, Ljava/util/Collection;->size()I

    move-result v3

    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 462
    .local v1, "list":Ljava/util/List;, "Ljava/util/List<Ljava/util/Map$Entry;>;"
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/dsm/system/osp/AuthUtil$ComparableParameter;

    .line 463
    .local v2, "parameter":Lcom/sec/dsm/system/osp/AuthUtil$ComparableParameter;
    iget-object v3, v2, Lcom/sec/dsm/system/osp/AuthUtil$ComparableParameter;->value:Ljava/util/Map$Entry;

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method protected static getSignSourceString(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 20
    .param p0, "httpMethod"    # Ljava/lang/String;
    .param p1, "requestURL"    # Ljava/lang/String;
    .param p2, "queryParam"    # Ljava/lang/String;
    .param p3, "sAuthorization"    # Ljava/lang/String;
    .param p4, "body"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 169
    new-instance v14, Ljava/lang/StringBuffer;

    invoke-direct {v14}, Ljava/lang/StringBuffer;-><init>()V

    .line 171
    .local v14, "sbSrc":Ljava/lang/StringBuffer;
    invoke-virtual/range {p0 .. p0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v14, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 172
    const-string v16, "&"

    move-object/from16 v0, v16

    invoke-virtual {v14, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 173
    invoke-static/range {p1 .. p1}, Lcom/sec/dsm/system/osp/AuthUtil;->normalizeUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v14, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 175
    invoke-static/range {p3 .. p3}, Lcom/sec/dsm/system/osp/AuthUtil;->getOSPAuthHttpHeadersFromString(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 176
    .local v4, "headers":[Ljava/lang/String;
    if-nez v4, :cond_0

    .line 177
    const-string v16, ""

    .line 226
    :goto_0
    return-object v16

    .line 181
    :cond_0
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    .line 183
    .local v13, "parameters":Ljava/util/List;, "Ljava/util/List<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;>;"
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_1
    array-length v0, v4

    move/from16 v16, v0

    move/from16 v0, v16

    if-ge v5, v0, :cond_3

    .line 185
    aget-object v16, v4, v5

    const/16 v17, 0x3d

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->indexOf(I)I

    move-result v9

    .line 187
    .local v9, "nPos":I
    if-gez v9, :cond_2

    .line 183
    :cond_1
    :goto_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 191
    :cond_2
    aget-object v16, v4, v5

    const/16 v17, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v17

    invoke-virtual {v0, v1, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    .line 192
    .local v11, "pairKey":Ljava/lang/String;
    aget-object v16, v4, v5

    add-int/lit8 v17, v9, 0x1

    aget-object v18, v4, v5

    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->length()I

    move-result v18

    invoke-virtual/range {v16 .. v18}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v12

    .line 194
    .local v12, "pairValue":Ljava/lang/String;
    if-eqz v11, :cond_1

    invoke-virtual {v11}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v16

    const-string v17, "oauth_signature"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-nez v16, :cond_1

    const-string v16, "realm"

    move-object/from16 v0, v16

    invoke-virtual {v11, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v16

    if-nez v16, :cond_1

    const-string v16, "OAuth realm"

    move-object/from16 v0, v16

    invoke-virtual {v11, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v16

    if-nez v16, :cond_1

    .line 198
    new-instance v16, Lcom/sec/dsm/system/osp/AuthUtil$Parameter;

    const-string v17, "\""

    const-string v18, ""

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v12, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    const-string v18, "&quot;"

    const-string v19, ""

    invoke-virtual/range {v17 .. v19}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-direct {v0, v11, v1}, Lcom/sec/dsm/system/osp/AuthUtil$Parameter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v16

    invoke-interface {v13, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 203
    .end local v9    # "nPos":I
    .end local v11    # "pairKey":Ljava/lang/String;
    .end local v12    # "pairValue":Ljava/lang/String;
    :cond_3
    if-eqz p2, :cond_5

    .line 204
    const-string v16, "\\&"

    move-object/from16 v0, p2

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .local v2, "arr$":[Ljava/lang/String;
    array-length v8, v2

    .local v8, "len$":I
    const/4 v6, 0x0

    .local v6, "i$":I
    :goto_3
    if-ge v6, v8, :cond_5

    aget-object v7, v2, v6

    .line 205
    .local v7, "item":Ljava/lang/String;
    const/16 v16, 0x3d

    move/from16 v0, v16

    invoke-virtual {v7, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v3

    .line 208
    .local v3, "equals":I
    if-gez v3, :cond_4

    .line 209
    invoke-static {v7}, Lcom/sec/dsm/system/osp/AuthUtil;->decodePercent(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 210
    .local v10, "name":Ljava/lang/String;
    const/4 v15, 0x0

    .line 215
    .local v15, "value":Ljava/lang/String;
    :goto_4
    new-instance v16, Lcom/sec/dsm/system/osp/AuthUtil$Parameter;

    move-object/from16 v0, v16

    invoke-direct {v0, v10, v15}, Lcom/sec/dsm/system/osp/AuthUtil$Parameter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v16

    invoke-interface {v13, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 204
    add-int/lit8 v6, v6, 0x1

    goto :goto_3

    .line 212
    .end local v10    # "name":Ljava/lang/String;
    .end local v15    # "value":Ljava/lang/String;
    :cond_4
    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-virtual {v7, v0, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    .line 213
    .restart local v10    # "name":Ljava/lang/String;
    add-int/lit8 v16, v3, 0x1

    move/from16 v0, v16

    invoke-virtual {v7, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v15

    .restart local v15    # "value":Ljava/lang/String;
    goto :goto_4

    .line 218
    .end local v2    # "arr$":[Ljava/lang/String;
    .end local v3    # "equals":I
    .end local v6    # "i$":I
    .end local v7    # "item":Ljava/lang/String;
    .end local v8    # "len$":I
    .end local v10    # "name":Ljava/lang/String;
    .end local v15    # "value":Ljava/lang/String;
    :cond_5
    const-string v16, "&"

    move-object/from16 v0, v16

    invoke-virtual {v14, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 219
    invoke-static {v13}, Lcom/sec/dsm/system/osp/AuthUtil;->normalizeParameters(Ljava/util/Collection;)Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v16 .. v16}, Lcom/sec/dsm/system/osp/AuthUtil;->percentEncode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v14, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 221
    if-eqz p4, :cond_6

    invoke-virtual/range {p4 .. p4}, Ljava/lang/String;->length()I

    move-result v16

    if-lez v16, :cond_6

    .line 222
    const-string v16, "&"

    move-object/from16 v0, v16

    invoke-virtual {v14, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 223
    move-object/from16 v0, p4

    invoke-virtual {v14, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 226
    :cond_6
    invoke-virtual {v14}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v16

    goto/16 :goto_0
.end method

.method private static isValidURI(Ljava/lang/String;)Z
    .locals 1
    .param p0, "uri"    # Ljava/lang/String;

    .prologue
    .line 289
    const/4 v0, 0x1

    return v0
.end method

.method private static normalizeParameters(Ljava/util/Collection;)Ljava/lang/String;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<+",
            "Ljava/util/Map$Entry;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 396
    .local p0, "parameters":Ljava/util/Collection;, "Ljava/util/Collection<+Ljava/util/Map$Entry;>;"
    if-nez p0, :cond_0

    .line 397
    const-string v3, ""

    .line 406
    :goto_0
    return-object v3

    .line 399
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {p0}, Ljava/util/Collection;->size()I

    move-result v3

    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 400
    .local v1, "p":Ljava/util/List;, "Ljava/util/List<Lcom/sec/dsm/system/osp/AuthUtil$ComparableParameter;>;"
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 401
    .local v2, "parameter":Ljava/util/Map$Entry;
    const-string v3, "oauth_signature"

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 402
    new-instance v3, Lcom/sec/dsm/system/osp/AuthUtil$ComparableParameter;

    invoke-direct {v3, v2}, Lcom/sec/dsm/system/osp/AuthUtil$ComparableParameter;-><init>(Ljava/util/Map$Entry;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 405
    .end local v2    # "parameter":Ljava/util/Map$Entry;
    :cond_2
    invoke-static {v1}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 406
    invoke-static {v1}, Lcom/sec/dsm/system/osp/AuthUtil;->getParameters(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/dsm/system/osp/AuthUtil;->formEncode(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method

.method private static normalizeUrl(Ljava/lang/String;)Ljava/lang/String;
    .locals 9
    .param p0, "url"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 256
    invoke-static {p0}, Lcom/sec/dsm/system/osp/AuthUtil;->isValidURI(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 257
    const/4 v6, 0x0

    .line 277
    :goto_0
    return-object v6

    .line 260
    :cond_0
    new-instance v5, Ljava/net/URI;

    invoke-direct {v5, p0}, Ljava/net/URI;-><init>(Ljava/lang/String;)V

    .line 261
    .local v5, "uri":Ljava/net/URI;
    invoke-virtual {v5}, Ljava/net/URI;->getScheme()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    .line 262
    .local v4, "scheme":Ljava/lang/String;
    invoke-virtual {v5}, Ljava/net/URI;->getAuthority()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    .line 263
    .local v0, "authority":Ljava/lang/String;
    const-string v7, "http"

    invoke-virtual {v4, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-virtual {v5}, Ljava/net/URI;->getPort()I

    move-result v7

    const/16 v8, 0x50

    if-eq v7, v8, :cond_2

    :cond_1
    const-string v7, "https"

    invoke-virtual {v4, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    invoke-virtual {v5}, Ljava/net/URI;->getPort()I

    move-result v7

    const/16 v8, 0x1bb

    if-ne v7, v8, :cond_6

    :cond_2
    const/4 v1, 0x1

    .line 265
    .local v1, "dropPort":Z
    :goto_1
    if-eqz v1, :cond_3

    .line 267
    const-string v7, ":"

    invoke-virtual {v0, v7}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v2

    .line 268
    .local v2, "index":I
    if-ltz v2, :cond_3

    .line 269
    invoke-virtual {v0, v6, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 272
    .end local v2    # "index":I
    :cond_3
    invoke-virtual {v5}, Ljava/net/URI;->getRawPath()Ljava/lang/String;

    move-result-object v3

    .line 273
    .local v3, "path":Ljava/lang/String;
    if-eqz v3, :cond_4

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v6

    if-gtz v6, :cond_5

    .line 274
    :cond_4
    const-string v3, "/"

    .line 277
    :cond_5
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "://"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/dsm/system/osp/AuthUtil;->percentEncode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    goto :goto_0

    .end local v1    # "dropPort":Z
    .end local v3    # "path":Ljava/lang/String;
    :cond_6
    move v1, v6

    .line 263
    goto :goto_1
.end method

.method private static percentEncode(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    .line 294
    if-nez p0, :cond_0

    .line 295
    const-string v1, ""

    .line 303
    :goto_0
    return-object v1

    .line 298
    :cond_0
    :try_start_0
    const-string v1, "UTF-8"

    invoke-static {p0, v1}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "+"

    const-string v3, "%20"

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "*"

    const-string v3, "%2A"

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "%7E"

    const-string v3, "~"

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "%25"

    const-string v3, "%"

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_0

    .line 302
    :catch_0
    move-exception v0

    .line 303
    .local v0, "e":Ljava/io/UnsupportedEncodingException;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private static toHex([B)Ljava/lang/String;
    .locals 6
    .param p0, "digest"    # [B

    .prologue
    .line 236
    const-string v0, "0123456789abcdef"

    .line 238
    .local v0, "HEX_DIGITS":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuffer;

    array-length v4, p0

    mul-int/lit8 v4, v4, 0x2

    invoke-direct {v3, v4}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 239
    .local v3, "sb":Ljava/lang/StringBuffer;
    const/4 v2, 0x0

    .local v2, "index":I
    :goto_0
    array-length v4, p0

    if-ge v2, v4, :cond_0

    .line 240
    aget-byte v4, p0, v2

    and-int/lit16 v1, v4, 0xff

    .line 241
    .local v1, "b":I
    ushr-int/lit8 v4, v1, 0x4

    invoke-virtual {v0, v4}, Ljava/lang/String;->charAt(I)C

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v4

    and-int/lit8 v5, v1, 0xf

    invoke-virtual {v0, v5}, Ljava/lang/String;->charAt(I)C

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 239
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 243
    .end local v1    # "b":I
    :cond_0
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method

.method private static final toString(Ljava/lang/Object;)Ljava/lang/String;
    .locals 1
    .param p0, "from"    # Ljava/lang/Object;

    .prologue
    .line 536
    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
