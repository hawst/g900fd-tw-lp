.class public Lcom/sec/dsm/system/SlidingTab$Handle$TabCircle;
.super Landroid/view/View;
.source "SlidingTab.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/dsm/system/SlidingTab$Handle;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "TabCircle"
.end annotation


# instance fields
.field private mLinePaint:Landroid/graphics/Paint;

.field private mRadius:F

.field final synthetic this$1:Lcom/sec/dsm/system/SlidingTab$Handle;


# direct methods
.method public constructor <init>(Lcom/sec/dsm/system/SlidingTab$Handle;Landroid/content/Context;F)V
    .locals 0
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "radius"    # F

    .prologue
    .line 769
    iput-object p1, p0, Lcom/sec/dsm/system/SlidingTab$Handle$TabCircle;->this$1:Lcom/sec/dsm/system/SlidingTab$Handle;

    .line 770
    invoke-direct {p0, p2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 771
    invoke-direct {p0, p3}, Lcom/sec/dsm/system/SlidingTab$Handle$TabCircle;->init(F)V

    .line 772
    return-void
.end method

.method private init(F)V
    .locals 2
    .param p1, "radius"    # F

    .prologue
    .line 786
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/dsm/system/SlidingTab$Handle$TabCircle;->mLinePaint:Landroid/graphics/Paint;

    .line 787
    iget-object v0, p0, Lcom/sec/dsm/system/SlidingTab$Handle$TabCircle;->mLinePaint:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 788
    iget-object v0, p0, Lcom/sec/dsm/system/SlidingTab$Handle$TabCircle;->mLinePaint:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/sec/dsm/system/SlidingTab$Handle$TabCircle;->this$1:Lcom/sec/dsm/system/SlidingTab$Handle;

    # getter for: Lcom/sec/dsm/system/SlidingTab$Handle;->mCircleColor:I
    invoke-static {v1}, Lcom/sec/dsm/system/SlidingTab$Handle;->access$1300(Lcom/sec/dsm/system/SlidingTab$Handle;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 789
    iget-object v0, p0, Lcom/sec/dsm/system/SlidingTab$Handle$TabCircle;->mLinePaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 790
    iget-object v0, p0, Lcom/sec/dsm/system/SlidingTab$Handle$TabCircle;->this$1:Lcom/sec/dsm/system/SlidingTab$Handle;

    iget-object v0, v0, Lcom/sec/dsm/system/SlidingTab$Handle;->this$0:Lcom/sec/dsm/system/SlidingTab;

    # getter for: Lcom/sec/dsm/system/SlidingTab;->mDisplayMetrics:Landroid/util/DisplayMetrics;
    invoke-static {v0}, Lcom/sec/dsm/system/SlidingTab;->access$1200(Lcom/sec/dsm/system/SlidingTab;)Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    const/16 v1, 0xa0

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/dsm/system/SlidingTab$Handle$TabCircle;->this$1:Lcom/sec/dsm/system/SlidingTab$Handle;

    iget-object v0, v0, Lcom/sec/dsm/system/SlidingTab$Handle;->this$0:Lcom/sec/dsm/system/SlidingTab;

    # getter for: Lcom/sec/dsm/system/SlidingTab;->mDisplayMetrics:Landroid/util/DisplayMetrics;
    invoke-static {v0}, Lcom/sec/dsm/system/SlidingTab;->access$1200(Lcom/sec/dsm/system/SlidingTab;)Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    const/16 v1, 0x78

    if-ne v0, v1, :cond_1

    .line 791
    :cond_0
    iget-object v0, p0, Lcom/sec/dsm/system/SlidingTab$Handle$TabCircle;->mLinePaint:Landroid/graphics/Paint;

    const/high16 v1, 0x40400000    # 3.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 795
    :goto_0
    iput p1, p0, Lcom/sec/dsm/system/SlidingTab$Handle$TabCircle;->mRadius:F

    .line 796
    return-void

    .line 793
    :cond_1
    iget-object v0, p0, Lcom/sec/dsm/system/SlidingTab$Handle$TabCircle;->mLinePaint:Landroid/graphics/Paint;

    const/high16 v1, 0x40c00000    # 6.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    goto :goto_0
.end method


# virtual methods
.method public isInCircle(FF)Z
    .locals 20
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 802
    const-wide v2, 0x3ff4cccccccccccdL    # 1.3

    .line 804
    .local v2, "ACTIVE_RATIO":D
    const/4 v15, 0x2

    new-array v14, v15, [I

    .line 805
    .local v14, "tmpPos":[I
    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/sec/dsm/system/SlidingTab$Handle$TabCircle;->getLocationOnScreen([I)V

    .line 807
    const/4 v15, 0x0

    aget v15, v14, v15

    invoke-virtual/range {p0 .. p0}, Lcom/sec/dsm/system/SlidingTab$Handle$TabCircle;->getWidth()I

    move-result v16

    div-int/lit8 v16, v16, 0x2

    add-int v15, v15, v16

    int-to-float v8, v15

    .line 808
    .local v8, "pivotX":F
    const/4 v15, 0x1

    aget v15, v14, v15

    invoke-virtual/range {p0 .. p0}, Lcom/sec/dsm/system/SlidingTab$Handle$TabCircle;->getHeight()I

    move-result v16

    div-int/lit8 v16, v16, 0x2

    add-int v15, v15, v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/dsm/system/SlidingTab$Handle$TabCircle;->this$1:Lcom/sec/dsm/system/SlidingTab$Handle;

    move-object/from16 v16, v0

    # getter for: Lcom/sec/dsm/system/SlidingTab$Handle;->mTopOffset:I
    invoke-static/range {v16 .. v16}, Lcom/sec/dsm/system/SlidingTab$Handle;->access$1400(Lcom/sec/dsm/system/SlidingTab$Handle;)I

    move-result v16

    sub-int v15, v15, v16

    int-to-float v9, v15

    .line 810
    .local v9, "pivotY":F
    sub-float v15, p1, v8

    float-to-double v0, v15

    move-wide/from16 v16, v0

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->abs(D)D

    move-result-wide v4

    .line 811
    .local v4, "dx":D
    sub-float v15, p2, v9

    float-to-double v0, v15

    move-wide/from16 v16, v0

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->abs(D)D

    move-result-wide v6

    .line 813
    .local v6, "dy":D
    mul-double v16, v4, v4

    mul-double v18, v6, v6

    add-double v16, v16, v18

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v10

    .line 815
    .local v10, "posLength":D
    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/dsm/system/SlidingTab$Handle$TabCircle;->mRadius:F

    float-to-double v0, v15

    move-wide/from16 v16, v0

    div-double v12, v10, v16

    .line 817
    .local v12, "ratio":D
    const-wide v16, 0x3ff4cccccccccccdL    # 1.3

    cmpg-double v15, v12, v16

    if-gez v15, :cond_0

    .line 818
    const/4 v15, 0x1

    .line 820
    :goto_0
    return v15

    :cond_0
    const/4 v15, 0x0

    goto :goto_0
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 4
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/high16 v2, 0x40000000    # 2.0f

    .line 826
    invoke-virtual {p0}, Lcom/sec/dsm/system/SlidingTab$Handle$TabCircle;->getWidth()I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v2

    invoke-virtual {p0}, Lcom/sec/dsm/system/SlidingTab$Handle$TabCircle;->getHeight()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v1, v2

    iget v2, p0, Lcom/sec/dsm/system/SlidingTab$Handle$TabCircle;->mRadius:F

    iget-object v3, p0, Lcom/sec/dsm/system/SlidingTab$Handle$TabCircle;->mLinePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 827
    return-void
.end method

.method public setAlpha(I)V
    .locals 1
    .param p1, "alpha"    # I

    .prologue
    .line 775
    iget-object v0, p0, Lcom/sec/dsm/system/SlidingTab$Handle$TabCircle;->mLinePaint:Landroid/graphics/Paint;

    if-eqz v0, :cond_0

    .line 776
    iget-object v0, p0, Lcom/sec/dsm/system/SlidingTab$Handle$TabCircle;->mLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 777
    :cond_0
    return-void
.end method

.method public setStrokeWidth(F)V
    .locals 1
    .param p1, "width"    # F

    .prologue
    .line 780
    iget-object v0, p0, Lcom/sec/dsm/system/SlidingTab$Handle$TabCircle;->mLinePaint:Landroid/graphics/Paint;

    if-eqz v0, :cond_0

    .line 781
    iget-object v0, p0, Lcom/sec/dsm/system/SlidingTab$Handle$TabCircle;->mLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 783
    :cond_0
    return-void
.end method
