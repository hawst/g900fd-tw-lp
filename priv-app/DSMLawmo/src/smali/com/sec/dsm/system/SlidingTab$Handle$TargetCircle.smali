.class public Lcom/sec/dsm/system/SlidingTab$Handle$TargetCircle;
.super Landroid/view/View;
.source "SlidingTab.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/dsm/system/SlidingTab$Handle;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "TargetCircle"
.end annotation


# instance fields
.field private mLinePaint:Landroid/graphics/Paint;

.field private mRadius:F

.field final synthetic this$1:Lcom/sec/dsm/system/SlidingTab$Handle;


# direct methods
.method public constructor <init>(Lcom/sec/dsm/system/SlidingTab$Handle;Landroid/content/Context;F)V
    .locals 0
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "radius"    # F

    .prologue
    .line 837
    iput-object p1, p0, Lcom/sec/dsm/system/SlidingTab$Handle$TargetCircle;->this$1:Lcom/sec/dsm/system/SlidingTab$Handle;

    .line 838
    invoke-direct {p0, p2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 839
    invoke-direct {p0, p3}, Lcom/sec/dsm/system/SlidingTab$Handle$TargetCircle;->init(F)V

    .line 840
    return-void
.end method

.method private init(F)V
    .locals 2
    .param p1, "radius"    # F

    .prologue
    .line 843
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/dsm/system/SlidingTab$Handle$TargetCircle;->mLinePaint:Landroid/graphics/Paint;

    .line 844
    iget-object v0, p0, Lcom/sec/dsm/system/SlidingTab$Handle$TargetCircle;->mLinePaint:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 845
    iget-object v0, p0, Lcom/sec/dsm/system/SlidingTab$Handle$TargetCircle;->mLinePaint:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/sec/dsm/system/SlidingTab$Handle$TargetCircle;->this$1:Lcom/sec/dsm/system/SlidingTab$Handle;

    # getter for: Lcom/sec/dsm/system/SlidingTab$Handle;->mCircleColor:I
    invoke-static {v1}, Lcom/sec/dsm/system/SlidingTab$Handle;->access$1300(Lcom/sec/dsm/system/SlidingTab$Handle;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 846
    iget-object v0, p0, Lcom/sec/dsm/system/SlidingTab$Handle$TargetCircle;->mLinePaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 847
    iget-object v0, p0, Lcom/sec/dsm/system/SlidingTab$Handle$TargetCircle;->mLinePaint:Landroid/graphics/Paint;

    const/high16 v1, 0x40400000    # 3.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 849
    iget-object v0, p0, Lcom/sec/dsm/system/SlidingTab$Handle$TargetCircle;->mLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v0

    const/high16 v1, 0x40800000    # 4.0f

    mul-float/2addr v0, v1

    sub-float v0, p1, v0

    iput v0, p0, Lcom/sec/dsm/system/SlidingTab$Handle$TargetCircle;->mRadius:F

    .line 850
    return-void
.end method


# virtual methods
.method public getTargetProximityRatio(FF)D
    .locals 14
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 879
    const/4 v9, 0x2

    new-array v8, v9, [I

    .line 880
    .local v8, "tmpPos":[I
    invoke-virtual {p0, v8}, Lcom/sec/dsm/system/SlidingTab$Handle$TargetCircle;->getLocationOnScreen([I)V

    .line 881
    const/4 v9, 0x0

    aget v9, v8, v9

    invoke-virtual {p0}, Lcom/sec/dsm/system/SlidingTab$Handle$TargetCircle;->getWidth()I

    move-result v10

    div-int/lit8 v10, v10, 0x2

    add-int/2addr v9, v10

    int-to-float v4, v9

    .line 882
    .local v4, "pivotX":F
    const/4 v9, 0x1

    aget v9, v8, v9

    invoke-virtual {p0}, Lcom/sec/dsm/system/SlidingTab$Handle$TargetCircle;->getHeight()I

    move-result v10

    div-int/lit8 v10, v10, 0x2

    add-int/2addr v9, v10

    iget-object v10, p0, Lcom/sec/dsm/system/SlidingTab$Handle$TargetCircle;->this$1:Lcom/sec/dsm/system/SlidingTab$Handle;

    # getter for: Lcom/sec/dsm/system/SlidingTab$Handle;->mTopOffset:I
    invoke-static {v10}, Lcom/sec/dsm/system/SlidingTab$Handle;->access$1400(Lcom/sec/dsm/system/SlidingTab$Handle;)I

    move-result v10

    sub-int/2addr v9, v10

    int-to-float v5, v9

    .line 884
    .local v5, "pivotY":F
    sub-float v9, p1, v4

    float-to-double v10, v9

    invoke-static {v10, v11}, Ljava/lang/Math;->abs(D)D

    move-result-wide v0

    .line 885
    .local v0, "dx":D
    sub-float v9, p2, v5

    float-to-double v10, v9

    invoke-static {v10, v11}, Ljava/lang/Math;->abs(D)D

    move-result-wide v2

    .line 887
    .local v2, "dy":D
    mul-double v10, v0, v0

    mul-double v12, v2, v2

    add-double/2addr v10, v12

    invoke-static {v10, v11}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v6

    .line 889
    .local v6, "posLength":D
    iget v9, p0, Lcom/sec/dsm/system/SlidingTab$Handle$TargetCircle;->mRadius:F

    float-to-double v10, v9

    div-double v10, v6, v10

    return-wide v10
.end method

.method public isThresholdReached(FF)Z
    .locals 6
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 867
    const-wide v0, 0x3fee666666666666L    # 0.95

    .line 869
    .local v0, "REACHED_RATIO":D
    invoke-virtual {p0, p1, p2}, Lcom/sec/dsm/system/SlidingTab$Handle$TargetCircle;->getTargetProximityRatio(FF)D

    move-result-wide v2

    .line 870
    .local v2, "ratio":D
    const-wide v4, 0x3fee666666666666L    # 0.95

    cmpl-double v4, v2, v4

    if-ltz v4, :cond_0

    const/4 v4, 0x1

    :goto_0
    return v4

    :cond_0
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 4
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/high16 v2, 0x40000000    # 2.0f

    .line 894
    invoke-virtual {p0}, Lcom/sec/dsm/system/SlidingTab$Handle$TargetCircle;->getWidth()I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v2

    invoke-virtual {p0}, Lcom/sec/dsm/system/SlidingTab$Handle$TargetCircle;->getHeight()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v1, v2

    iget v2, p0, Lcom/sec/dsm/system/SlidingTab$Handle$TargetCircle;->mRadius:F

    iget-object v3, p0, Lcom/sec/dsm/system/SlidingTab$Handle$TargetCircle;->mLinePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 896
    return-void
.end method

.method public setAlpha(I)V
    .locals 1
    .param p1, "alpha"    # I

    .prologue
    .line 853
    iget-object v0, p0, Lcom/sec/dsm/system/SlidingTab$Handle$TargetCircle;->mLinePaint:Landroid/graphics/Paint;

    if-eqz v0, :cond_0

    .line 854
    iget-object v0, p0, Lcom/sec/dsm/system/SlidingTab$Handle$TargetCircle;->mLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 855
    :cond_0
    return-void
.end method
