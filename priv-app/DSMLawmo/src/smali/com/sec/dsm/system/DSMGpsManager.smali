.class public Lcom/sec/dsm/system/DSMGpsManager;
.super Ljava/lang/Object;
.source "DSMGpsManager.java"


# static fields
.field public static SIMmcc:I

.field public static SIMmnc:I

.field public static g_szCellId:Ljava/lang/String;

.field public static g_szLac:Ljava/lang/String;

.field public static g_szMsisdn:Ljava/lang/String;

.field public static mAccuracy:D

.field public static mAltitude:D

.field public static mBearing:D

.field public static mLatitude:D

.field public static mLongitude:D

.field public static mSpeed:D

.field public static mcc:I

.field public static mnc:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const-wide/16 v0, 0x0

    .line 8
    sput-wide v0, Lcom/sec/dsm/system/DSMGpsManager;->mLatitude:D

    .line 9
    sput-wide v0, Lcom/sec/dsm/system/DSMGpsManager;->mLongitude:D

    .line 10
    sput-wide v0, Lcom/sec/dsm/system/DSMGpsManager;->mAltitude:D

    .line 11
    sput-wide v0, Lcom/sec/dsm/system/DSMGpsManager;->mBearing:D

    .line 12
    sput-wide v0, Lcom/sec/dsm/system/DSMGpsManager;->mSpeed:D

    .line 13
    sput-wide v0, Lcom/sec/dsm/system/DSMGpsManager;->mAccuracy:D

    .line 14
    sput v2, Lcom/sec/dsm/system/DSMGpsManager;->mcc:I

    .line 15
    sput v2, Lcom/sec/dsm/system/DSMGpsManager;->mnc:I

    .line 18
    sput v2, Lcom/sec/dsm/system/DSMGpsManager;->SIMmcc:I

    .line 19
    sput v2, Lcom/sec/dsm/system/DSMGpsManager;->SIMmnc:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static DSMGpsInitLocation()V
    .locals 2

    .prologue
    const-wide/16 v0, 0x0

    .line 24
    sput-wide v0, Lcom/sec/dsm/system/DSMGpsManager;->mLatitude:D

    .line 25
    sput-wide v0, Lcom/sec/dsm/system/DSMGpsManager;->mLongitude:D

    .line 26
    sput-wide v0, Lcom/sec/dsm/system/DSMGpsManager;->mAltitude:D

    .line 27
    sput-wide v0, Lcom/sec/dsm/system/DSMGpsManager;->mBearing:D

    .line 28
    sput-wide v0, Lcom/sec/dsm/system/DSMGpsManager;->mSpeed:D

    .line 29
    sput-wide v0, Lcom/sec/dsm/system/DSMGpsManager;->mAccuracy:D

    .line 30
    return-void
.end method

.method public static DSMGpsSetLocation(Landroid/location/Location;)V
    .locals 2
    .param p0, "location"    # Landroid/location/Location;

    .prologue
    .line 34
    invoke-virtual {p0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v0

    sput-wide v0, Lcom/sec/dsm/system/DSMGpsManager;->mLatitude:D

    .line 35
    invoke-virtual {p0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v0

    sput-wide v0, Lcom/sec/dsm/system/DSMGpsManager;->mLongitude:D

    .line 36
    invoke-virtual {p0}, Landroid/location/Location;->getAltitude()D

    move-result-wide v0

    sput-wide v0, Lcom/sec/dsm/system/DSMGpsManager;->mAltitude:D

    .line 37
    invoke-virtual {p0}, Landroid/location/Location;->getBearing()F

    move-result v0

    float-to-double v0, v0

    sput-wide v0, Lcom/sec/dsm/system/DSMGpsManager;->mBearing:D

    .line 38
    invoke-virtual {p0}, Landroid/location/Location;->getSpeed()F

    move-result v0

    float-to-double v0, v0

    sput-wide v0, Lcom/sec/dsm/system/DSMGpsManager;->mSpeed:D

    .line 39
    invoke-virtual {p0}, Landroid/location/Location;->getAccuracy()F

    move-result v0

    float-to-double v0, v0

    sput-wide v0, Lcom/sec/dsm/system/DSMGpsManager;->mAccuracy:D

    .line 40
    return-void
.end method
