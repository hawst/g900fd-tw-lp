.class public Lcom/sec/dsm/system/osp/PropertyException;
.super Lcom/sec/dsm/system/osp/AbstractOSPException;
.source "PropertyException.java"


# static fields
.field private static final serialVersionUID:J = 0x3dbc13270d7e8778L


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/sec/dsm/system/osp/AbstractOSPException;-><init>()V

    .line 22
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "detailedMessage"    # Ljava/lang/String;

    .prologue
    .line 41
    invoke-direct {p0, p1}, Lcom/sec/dsm/system/osp/AbstractOSPException;-><init>(Ljava/lang/String;)V

    .line 42
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "detailedMessage"    # Ljava/lang/String;
    .param p2, "faultCode"    # Ljava/lang/String;

    .prologue
    .line 63
    invoke-direct {p0, p1, p2}, Lcom/sec/dsm/system/osp/AbstractOSPException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 0
    .param p1, "detailedMessage"    # Ljava/lang/String;
    .param p2, "throwable"    # Ljava/lang/Throwable;

    .prologue
    .line 52
    invoke-direct {p0, p1, p2}, Lcom/sec/dsm/system/osp/AbstractOSPException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 53
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)V
    .locals 0
    .param p1, "detailedMessage"    # Ljava/lang/String;
    .param p2, "throwable"    # Ljava/lang/Throwable;
    .param p3, "faultCode"    # Ljava/lang/String;

    .prologue
    .line 88
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/dsm/system/osp/AbstractOSPException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 89
    return-void
.end method

.method public constructor <init>(Ljava/lang/Throwable;)V
    .locals 0
    .param p1, "throwable"    # Ljava/lang/Throwable;

    .prologue
    .line 31
    invoke-direct {p0, p1}, Lcom/sec/dsm/system/osp/AbstractOSPException;-><init>(Ljava/lang/Throwable;)V

    .line 32
    return-void
.end method

.method public constructor <init>(Ljava/lang/Throwable;Ljava/lang/String;)V
    .locals 0
    .param p1, "throwable"    # Ljava/lang/Throwable;
    .param p2, "faultCode"    # Ljava/lang/String;

    .prologue
    .line 74
    invoke-direct {p0, p1, p2}, Lcom/sec/dsm/system/osp/AbstractOSPException;-><init>(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 75
    return-void
.end method


# virtual methods
.method public getFaultCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 106
    invoke-super {p0}, Lcom/sec/dsm/system/osp/AbstractOSPException;->getFaultCode()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public setFaultCode(Ljava/lang/String;)V
    .locals 0
    .param p1, "faultCode"    # Ljava/lang/String;

    .prologue
    .line 97
    invoke-super {p0, p1}, Lcom/sec/dsm/system/osp/AbstractOSPException;->setFaultCode(Ljava/lang/String;)V

    .line 98
    return-void
.end method
