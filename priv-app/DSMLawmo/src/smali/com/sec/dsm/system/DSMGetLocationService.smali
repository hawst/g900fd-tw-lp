.class public Lcom/sec/dsm/system/DSMGetLocationService;
.super Ljava/lang/Object;
.source "DSMGetLocationService.java"


# static fields
.field static DSMGpsLocListener:Lcom/sec/dsm/system/DSMGpsListener;

.field static DSMNetLocListener:Lcom/sec/dsm/system/DSMGpsListener;

.field static DSM_GPS_LOCATION_TIME_OUT:I

.field static DSM_NETWORK_LOCATION_TIME_OUT:I

.field static DSM_START_LOCATION_ALL:I

.field static DSM_START_LOCATION_GPS:I

.field static DSM_START_LOCATION_NETWORK:I

.field static getGPSLoc:Z

.field static getNetLoc:Z

.field static lm:Landroid/location/LocationManager;

.field private static mContext:Landroid/content/Context;

.field static mDSMDelayTimer:Lcom/sec/dsm/system/DSMDelayTimer;

.field static mLocation:Landroid/location/Location;

.field static preGPSEnabled:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 15
    sput-object v1, Lcom/sec/dsm/system/DSMGetLocationService;->mLocation:Landroid/location/Location;

    .line 16
    sput-boolean v0, Lcom/sec/dsm/system/DSMGetLocationService;->preGPSEnabled:Z

    .line 18
    sput-boolean v0, Lcom/sec/dsm/system/DSMGetLocationService;->getNetLoc:Z

    .line 19
    sput-boolean v0, Lcom/sec/dsm/system/DSMGetLocationService;->getGPSLoc:Z

    .line 21
    sput-object v1, Lcom/sec/dsm/system/DSMGetLocationService;->mDSMDelayTimer:Lcom/sec/dsm/system/DSMDelayTimer;

    .line 23
    sput v0, Lcom/sec/dsm/system/DSMGetLocationService;->DSM_START_LOCATION_ALL:I

    .line 24
    const/4 v0, 0x1

    sput v0, Lcom/sec/dsm/system/DSMGetLocationService;->DSM_START_LOCATION_GPS:I

    .line 25
    const/4 v0, 0x2

    sput v0, Lcom/sec/dsm/system/DSMGetLocationService;->DSM_START_LOCATION_NETWORK:I

    .line 26
    const v0, 0xea60

    sput v0, Lcom/sec/dsm/system/DSMGetLocationService;->DSM_GPS_LOCATION_TIME_OUT:I

    .line 27
    const/16 v0, 0x2710

    sput v0, Lcom/sec/dsm/system/DSMGetLocationService;->DSM_NETWORK_LOCATION_TIME_OUT:I

    .line 116
    new-instance v0, Lcom/sec/dsm/system/DSMGetLocationService$1;

    invoke-direct {v0}, Lcom/sec/dsm/system/DSMGetLocationService$1;-><init>()V

    sput-object v0, Lcom/sec/dsm/system/DSMGetLocationService;->DSMGpsLocListener:Lcom/sec/dsm/system/DSMGpsListener;

    .line 154
    new-instance v0, Lcom/sec/dsm/system/DSMGetLocationService$2;

    invoke-direct {v0}, Lcom/sec/dsm/system/DSMGetLocationService$2;-><init>()V

    sput-object v0, Lcom/sec/dsm/system/DSMGetLocationService;->DSMNetLocListener:Lcom/sec/dsm/system/DSMGpsListener;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "mContext"    # Landroid/content/Context;

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    sput-object p1, Lcom/sec/dsm/system/DSMGetLocationService;->mContext:Landroid/content/Context;

    .line 37
    return-void
.end method

.method public static DSMGpsStartTracking(ILandroid/location/LocationManager;)V
    .locals 6
    .param p0, "nStartMode"    # I
    .param p1, "lm"    # Landroid/location/LocationManager;

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 84
    if-nez p1, :cond_0

    .line 86
    const-string v0, "LocationManager is null!!"

    invoke-static {v0}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 114
    :goto_0
    return-void

    .line 90
    :cond_0
    sget v0, Lcom/sec/dsm/system/DSMGetLocationService;->DSM_START_LOCATION_GPS:I

    if-ne p0, v0, :cond_1

    .line 92
    sput-boolean v4, Lcom/sec/dsm/system/DSMGetLocationService;->getGPSLoc:Z

    .line 93
    sget-object v0, Lcom/sec/dsm/system/DSMGetLocationService;->DSMGpsLocListener:Lcom/sec/dsm/system/DSMGpsListener;

    sget v1, Lcom/sec/dsm/system/DSMGetLocationService;->DSM_GPS_LOCATION_TIME_OUT:I

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Lcom/sec/dsm/system/DSMGpsListener;->startTimer(J)V

    .line 94
    const-string v0, "gps"

    sget-object v1, Lcom/sec/dsm/system/DSMGetLocationService;->DSMGpsLocListener:Lcom/sec/dsm/system/DSMGpsListener;

    invoke-virtual {p1, v0, v1, v5}, Landroid/location/LocationManager;->requestSingleUpdate(Ljava/lang/String;Landroid/location/LocationListener;Landroid/os/Looper;)V

    .line 95
    const-string v0, "GPS listener is starting"

    invoke-static {v0}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    goto :goto_0

    .line 97
    :cond_1
    sget v0, Lcom/sec/dsm/system/DSMGetLocationService;->DSM_START_LOCATION_NETWORK:I

    if-ne p0, v0, :cond_2

    .line 99
    sput-boolean v4, Lcom/sec/dsm/system/DSMGetLocationService;->getNetLoc:Z

    .line 100
    sget-object v0, Lcom/sec/dsm/system/DSMGetLocationService;->DSMNetLocListener:Lcom/sec/dsm/system/DSMGpsListener;

    sget v1, Lcom/sec/dsm/system/DSMGetLocationService;->DSM_NETWORK_LOCATION_TIME_OUT:I

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Lcom/sec/dsm/system/DSMGpsListener;->startTimer(J)V

    .line 101
    const-string v0, "network"

    sget-object v1, Lcom/sec/dsm/system/DSMGetLocationService;->DSMNetLocListener:Lcom/sec/dsm/system/DSMGpsListener;

    invoke-virtual {p1, v0, v1, v5}, Landroid/location/LocationManager;->requestSingleUpdate(Ljava/lang/String;Landroid/location/LocationListener;Landroid/os/Looper;)V

    .line 102
    const-string v0, "Network GPS listener is starting"

    invoke-static {v0}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    goto :goto_0

    .line 106
    :cond_2
    sput-boolean v4, Lcom/sec/dsm/system/DSMGetLocationService;->getGPSLoc:Z

    .line 107
    sget-object v0, Lcom/sec/dsm/system/DSMGetLocationService;->DSMGpsLocListener:Lcom/sec/dsm/system/DSMGpsListener;

    sget v1, Lcom/sec/dsm/system/DSMGetLocationService;->DSM_GPS_LOCATION_TIME_OUT:I

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Lcom/sec/dsm/system/DSMGpsListener;->startTimer(J)V

    .line 108
    const-string v0, "gps"

    sget-object v1, Lcom/sec/dsm/system/DSMGetLocationService;->DSMGpsLocListener:Lcom/sec/dsm/system/DSMGpsListener;

    invoke-virtual {p1, v0, v1, v5}, Landroid/location/LocationManager;->requestSingleUpdate(Ljava/lang/String;Landroid/location/LocationListener;Landroid/os/Looper;)V

    .line 109
    const-string v0, "GPS listener is starting"

    invoke-static {v0}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 111
    sput-boolean v4, Lcom/sec/dsm/system/DSMGetLocationService;->getNetLoc:Z

    .line 112
    new-instance v0, Lcom/sec/dsm/system/DSMDelayTimer;

    invoke-direct {v0}, Lcom/sec/dsm/system/DSMDelayTimer;-><init>()V

    sput-object v0, Lcom/sec/dsm/system/DSMGetLocationService;->mDSMDelayTimer:Lcom/sec/dsm/system/DSMDelayTimer;

    goto :goto_0
.end method

.method public static DSMRemoveListener()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 194
    sget-object v0, Lcom/sec/dsm/system/DSMGetLocationService;->lm:Landroid/location/LocationManager;

    if-nez v0, :cond_1

    .line 196
    const-string v0, "LocationManager is null!!"

    invoke-static {v0}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 222
    :cond_0
    :goto_0
    return-void

    .line 200
    :cond_1
    sget-boolean v0, Lcom/sec/dsm/system/DSMGetLocationService;->getGPSLoc:Z

    if-eqz v0, :cond_2

    .line 202
    sput-boolean v2, Lcom/sec/dsm/system/DSMGetLocationService;->getGPSLoc:Z

    .line 203
    sget-object v0, Lcom/sec/dsm/system/DSMGetLocationService;->DSMGpsLocListener:Lcom/sec/dsm/system/DSMGpsListener;

    invoke-virtual {v0}, Lcom/sec/dsm/system/DSMGpsListener;->endTimer()V

    .line 204
    sget-object v0, Lcom/sec/dsm/system/DSMGetLocationService;->lm:Landroid/location/LocationManager;

    sget-object v1, Lcom/sec/dsm/system/DSMGetLocationService;->DSMGpsLocListener:Lcom/sec/dsm/system/DSMGpsListener;

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    .line 206
    :cond_2
    sget-boolean v0, Lcom/sec/dsm/system/DSMGetLocationService;->getNetLoc:Z

    if-eqz v0, :cond_4

    .line 208
    sput-boolean v2, Lcom/sec/dsm/system/DSMGetLocationService;->getNetLoc:Z

    .line 209
    sget-object v0, Lcom/sec/dsm/system/DSMGetLocationService;->mDSMDelayTimer:Lcom/sec/dsm/system/DSMDelayTimer;

    if-eqz v0, :cond_3

    .line 211
    sget-object v0, Lcom/sec/dsm/system/DSMGetLocationService;->mDSMDelayTimer:Lcom/sec/dsm/system/DSMDelayTimer;

    invoke-virtual {v0}, Lcom/sec/dsm/system/DSMDelayTimer;->endDelayTimer()V

    .line 212
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/dsm/system/DSMGetLocationService;->mDSMDelayTimer:Lcom/sec/dsm/system/DSMDelayTimer;

    .line 214
    :cond_3
    sget-object v0, Lcom/sec/dsm/system/DSMGetLocationService;->DSMNetLocListener:Lcom/sec/dsm/system/DSMGpsListener;

    invoke-virtual {v0}, Lcom/sec/dsm/system/DSMGpsListener;->endTimer()V

    .line 215
    sget-object v0, Lcom/sec/dsm/system/DSMGetLocationService;->lm:Landroid/location/LocationManager;

    sget-object v1, Lcom/sec/dsm/system/DSMGetLocationService;->DSMNetLocListener:Lcom/sec/dsm/system/DSMGpsListener;

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    .line 218
    :cond_4
    sget-boolean v0, Lcom/sec/dsm/system/DSMGetLocationService;->preGPSEnabled:Z

    if-nez v0, :cond_0

    .line 220
    const-string v0, "disable the GPS_PROVIDER"

    invoke-static {v0}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static DSMSetupGpsTracking(IZ)V
    .locals 3
    .param p0, "nLocCount"    # I
    .param p1, "netOnly"    # Z

    .prologue
    const/4 v2, 0x1

    .line 41
    sget-object v0, Lcom/sec/dsm/system/DSMGetLocationService;->mContext:Landroid/content/Context;

    const-string v1, "location"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    sput-object v0, Lcom/sec/dsm/system/DSMGetLocationService;->lm:Landroid/location/LocationManager;

    .line 42
    sget-object v0, Lcom/sec/dsm/system/DSMGetLocationService;->lm:Landroid/location/LocationManager;

    if-nez v0, :cond_1

    .line 44
    const-string v0, "LocationManager is null!!"

    invoke-static {v0}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 80
    :cond_0
    :goto_0
    return-void

    .line 47
    :cond_1
    sget-object v0, Lcom/sec/dsm/system/DSMGetLocationService;->lm:Landroid/location/LocationManager;

    const-string v1, "network"

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 49
    sget-object v0, Lcom/sec/dsm/system/DSMGetLocationService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "network"

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Secure;->setLocationProviderEnabled(Landroid/content/ContentResolver;Ljava/lang/String;Z)V

    .line 50
    const-string v0, "enable the network provider"

    invoke-static {v0}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 53
    :cond_2
    sget-object v0, Lcom/sec/dsm/system/DSMGetLocationService;->lm:Landroid/location/LocationManager;

    const-string v1, "gps"

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 55
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/dsm/system/DSMGetLocationService;->preGPSEnabled:Z

    .line 56
    sget-object v0, Lcom/sec/dsm/system/DSMGetLocationService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "gps"

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Secure;->setLocationProviderEnabled(Landroid/content/ContentResolver;Ljava/lang/String;Z)V

    .line 57
    const-string v0, "enable the GPS_PROVIDER"

    invoke-static {v0}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 64
    :goto_1
    invoke-static {}, Lcom/sec/dsm/system/DSMGpsManager;->DSMGpsInitLocation()V

    .line 67
    if-nez p1, :cond_5

    .line 68
    if-nez p0, :cond_4

    .line 70
    sget v0, Lcom/sec/dsm/system/DSMGetLocationService;->DSM_START_LOCATION_ALL:I

    sget-object v1, Lcom/sec/dsm/system/DSMGetLocationService;->lm:Landroid/location/LocationManager;

    invoke-static {v0, v1}, Lcom/sec/dsm/system/DSMGetLocationService;->DSMGpsStartTracking(ILandroid/location/LocationManager;)V

    goto :goto_0

    .line 61
    :cond_3
    sput-boolean v2, Lcom/sec/dsm/system/DSMGetLocationService;->preGPSEnabled:Z

    goto :goto_1

    .line 72
    :cond_4
    if-ne p0, v2, :cond_0

    .line 74
    sget v0, Lcom/sec/dsm/system/DSMGetLocationService;->DSM_START_LOCATION_GPS:I

    sget-object v1, Lcom/sec/dsm/system/DSMGetLocationService;->lm:Landroid/location/LocationManager;

    invoke-static {v0, v1}, Lcom/sec/dsm/system/DSMGetLocationService;->DSMGpsStartTracking(ILandroid/location/LocationManager;)V

    goto :goto_0

    .line 77
    :cond_5
    sget v0, Lcom/sec/dsm/system/DSMGetLocationService;->DSM_START_LOCATION_NETWORK:I

    sget-object v1, Lcom/sec/dsm/system/DSMGetLocationService;->lm:Landroid/location/LocationManager;

    invoke-static {v0, v1}, Lcom/sec/dsm/system/DSMGetLocationService;->DSMGpsStartTracking(ILandroid/location/LocationManager;)V

    goto :goto_0
.end method

.method public static DSMStartNetworkGPSListener()V
    .locals 5

    .prologue
    .line 226
    sget-boolean v1, Lcom/sec/dsm/system/DSMGetLocationService;->getNetLoc:Z

    if-nez v1, :cond_0

    .line 228
    const-string v1, "NetLoc is false, Not start"

    invoke-static {v1}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 242
    .local v0, "e":Ljava/lang/Exception;
    :goto_0
    return-void

    .line 234
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    :try_start_0
    sget-object v1, Lcom/sec/dsm/system/DSMGetLocationService;->DSMNetLocListener:Lcom/sec/dsm/system/DSMGpsListener;

    sget v2, Lcom/sec/dsm/system/DSMGetLocationService;->DSM_NETWORK_LOCATION_TIME_OUT:I

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3}, Lcom/sec/dsm/system/DSMGpsListener;->startTimer(J)V

    .line 235
    sget-object v1, Lcom/sec/dsm/system/DSMGetLocationService;->lm:Landroid/location/LocationManager;

    const-string v2, "network"

    sget-object v3, Lcom/sec/dsm/system/DSMGetLocationService;->DSMNetLocListener:Lcom/sec/dsm/system/DSMGpsListener;

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Landroid/location/LocationManager;->requestSingleUpdate(Ljava/lang/String;Landroid/location/LocationListener;Landroid/os/Looper;)V

    .line 236
    const-string v1, "Network GPS listener is starting"

    invoke-static {v1}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 238
    :catch_0
    move-exception v0

    .line 240
    .restart local v0    # "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic access$000()Landroid/content/Context;
    .locals 1

    .prologue
    .line 12
    sget-object v0, Lcom/sec/dsm/system/DSMGetLocationService;->mContext:Landroid/content/Context;

    return-object v0
.end method
