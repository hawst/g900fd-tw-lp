.class Lcom/sec/dsm/system/DSMTrackingService$MobileTrackingTimer;
.super Ljava/lang/Object;
.source "DSMTrackingService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/dsm/system/DSMTrackingService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MobileTrackingTimer"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/dsm/system/DSMTrackingService$MobileTrackingTimer$MobileTrackingTimerTask;
    }
.end annotation


# instance fields
.field private mEndDate:Ljava/lang/String;

.field private mInterval:I

.field private mIntervalTimer:Ljava/util/Timer;

.field private mStartDate:Ljava/lang/String;

.field private mTrackingTimerTask:Ljava/util/TimerTask;

.field final synthetic this$0:Lcom/sec/dsm/system/DSMTrackingService;


# direct methods
.method public constructor <init>(Lcom/sec/dsm/system/DSMTrackingService;ILjava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p2, "interval"    # I
    .param p3, "startdate"    # Ljava/lang/String;
    .param p4, "enddate"    # Ljava/lang/String;

    .prologue
    .line 313
    iput-object p1, p0, Lcom/sec/dsm/system/DSMTrackingService$MobileTrackingTimer;->this$0:Lcom/sec/dsm/system/DSMTrackingService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 314
    new-instance v0, Lcom/sec/dsm/system/DSMTrackingService$MobileTrackingTimer$MobileTrackingTimerTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/dsm/system/DSMTrackingService$MobileTrackingTimer$MobileTrackingTimerTask;-><init>(Lcom/sec/dsm/system/DSMTrackingService$MobileTrackingTimer;Lcom/sec/dsm/system/DSMTrackingService$1;)V

    iput-object v0, p0, Lcom/sec/dsm/system/DSMTrackingService$MobileTrackingTimer;->mTrackingTimerTask:Ljava/util/TimerTask;

    .line 315
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/sec/dsm/system/DSMTrackingService$MobileTrackingTimer;->mIntervalTimer:Ljava/util/Timer;

    .line 316
    iput p2, p0, Lcom/sec/dsm/system/DSMTrackingService$MobileTrackingTimer;->mInterval:I

    .line 317
    iput-object p3, p0, Lcom/sec/dsm/system/DSMTrackingService$MobileTrackingTimer;->mStartDate:Ljava/lang/String;

    .line 318
    iput-object p4, p0, Lcom/sec/dsm/system/DSMTrackingService$MobileTrackingTimer;->mEndDate:Ljava/lang/String;

    .line 321
    return-void
.end method

.method static synthetic access$300(Lcom/sec/dsm/system/DSMTrackingService$MobileTrackingTimer;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/dsm/system/DSMTrackingService$MobileTrackingTimer;

    .prologue
    .line 306
    iget v0, p0, Lcom/sec/dsm/system/DSMTrackingService$MobileTrackingTimer;->mInterval:I

    return v0
.end method

.method static synthetic access$400(Lcom/sec/dsm/system/DSMTrackingService$MobileTrackingTimer;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/dsm/system/DSMTrackingService$MobileTrackingTimer;

    .prologue
    .line 306
    iget-object v0, p0, Lcom/sec/dsm/system/DSMTrackingService$MobileTrackingTimer;->mStartDate:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/dsm/system/DSMTrackingService$MobileTrackingTimer;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/dsm/system/DSMTrackingService$MobileTrackingTimer;

    .prologue
    .line 306
    iget-object v0, p0, Lcom/sec/dsm/system/DSMTrackingService$MobileTrackingTimer;->mEndDate:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public endTimer()V
    .locals 1

    .prologue
    .line 353
    const-string v0, "TrackingTimer cancel"

    invoke-static {v0}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 354
    iget-object v0, p0, Lcom/sec/dsm/system/DSMTrackingService$MobileTrackingTimer;->mIntervalTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 355
    iget-object v0, p0, Lcom/sec/dsm/system/DSMTrackingService$MobileTrackingTimer;->mTrackingTimerTask:Ljava/util/TimerTask;

    invoke-virtual {v0}, Ljava/util/TimerTask;->cancel()Z

    .line 356
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/dsm/system/DSMTrackingService;->mobileTrackingTimer:Lcom/sec/dsm/system/DSMTrackingService$MobileTrackingTimer;

    .line 357
    return-void
.end method

.method public startTimer()V
    .locals 14

    .prologue
    .line 325
    iget v8, p0, Lcom/sec/dsm/system/DSMTrackingService$MobileTrackingTimer;->mInterval:I

    mul-int/lit8 v8, v8, 0x3c

    mul-int/lit16 v3, v8, 0x3e8

    .line 327
    .local v3, "nInterval":I
    const/4 v1, 0x0

    .line 329
    .local v1, "dStartDate":Ljava/util/Date;
    :try_start_0
    new-instance v5, Landroid/text/format/Time;

    invoke-direct {v5}, Landroid/text/format/Time;-><init>()V

    .line 330
    .local v5, "time":Landroid/text/format/Time;
    iget-object v8, p0, Lcom/sec/dsm/system/DSMTrackingService$MobileTrackingTimer;->mStartDate:Ljava/lang/String;

    invoke-virtual {v8}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Landroid/text/format/Time;->parse(Ljava/lang/String;)Z

    .line 331
    const/4 v8, 0x0

    invoke-virtual {v5, v8}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v6

    .line 335
    .local v6, "tStartDate":J
    new-instance v8, Ljava/text/SimpleDateFormat;

    const-string v9, "yyyyMMdd\'T\'HHmmss\'Z\'"

    invoke-direct {v8, v9}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    new-instance v9, Ljava/util/Date;

    invoke-direct {v9, v6, v7}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v8, v9}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 336
    .local v0, "cDateFormat":Ljava/lang/String;
    new-instance v4, Ljava/text/SimpleDateFormat;

    const-string v8, "yyyyMMdd\'T\'HHmmss\'Z\'"

    invoke-direct {v4, v8}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 337
    .local v4, "tDateFormat":Ljava/text/SimpleDateFormat;
    invoke-virtual {v4, v0}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v1

    .line 340
    iget-object v8, p0, Lcom/sec/dsm/system/DSMTrackingService$MobileTrackingTimer;->mIntervalTimer:Ljava/util/Timer;

    iget-object v9, p0, Lcom/sec/dsm/system/DSMTrackingService$MobileTrackingTimer;->mTrackingTimerTask:Ljava/util/TimerTask;

    int-to-long v10, v3

    invoke-virtual {v8, v9, v1, v10, v11}, Ljava/util/Timer;->scheduleAtFixedRate(Ljava/util/TimerTask;Ljava/util/Date;J)V
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_2

    .line 350
    .end local v0    # "cDateFormat":Ljava/lang/String;
    .end local v4    # "tDateFormat":Ljava/text/SimpleDateFormat;
    .end local v5    # "time":Landroid/text/format/Time;
    .end local v6    # "tStartDate":J
    :goto_0
    return-void

    .line 342
    :catch_0
    move-exception v2

    .line 343
    .local v2, "e":Ljava/text/ParseException;
    invoke-virtual {v2}, Ljava/text/ParseException;->printStackTrace()V

    .line 344
    iget-object v8, p0, Lcom/sec/dsm/system/DSMTrackingService$MobileTrackingTimer;->mIntervalTimer:Ljava/util/Timer;

    iget-object v9, p0, Lcom/sec/dsm/system/DSMTrackingService$MobileTrackingTimer;->mTrackingTimerTask:Ljava/util/TimerTask;

    new-instance v10, Ljava/util/Date;

    invoke-direct {v10}, Ljava/util/Date;-><init>()V

    int-to-long v12, v3

    invoke-virtual {v8, v9, v10, v12, v13}, Ljava/util/Timer;->scheduleAtFixedRate(Ljava/util/TimerTask;Ljava/util/Date;J)V

    goto :goto_0

    .line 345
    .end local v2    # "e":Ljava/text/ParseException;
    :catch_1
    move-exception v2

    .line 346
    .local v2, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v2}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 347
    .end local v2    # "e":Ljava/lang/IllegalArgumentException;
    :catch_2
    move-exception v2

    .line 348
    .local v2, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v2}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0
.end method
