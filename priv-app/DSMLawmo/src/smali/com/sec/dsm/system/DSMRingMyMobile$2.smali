.class Lcom/sec/dsm/system/DSMRingMyMobile$2;
.super Landroid/telephony/PhoneStateListener;
.source "DSMRingMyMobile.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/dsm/system/DSMRingMyMobile;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/dsm/system/DSMRingMyMobile;


# direct methods
.method constructor <init>(Lcom/sec/dsm/system/DSMRingMyMobile;)V
    .locals 0

    .prologue
    .line 143
    iput-object p1, p0, Lcom/sec/dsm/system/DSMRingMyMobile$2;->this$0:Lcom/sec/dsm/system/DSMRingMyMobile;

    invoke-direct {p0}, Landroid/telephony/PhoneStateListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onCallStateChanged(ILjava/lang/String;)V
    .locals 6
    .param p1, "state"    # I
    .param p2, "incommingNumber"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 146
    if-ne p1, v4, :cond_1

    .line 147
    # setter for: Lcom/sec/dsm/system/DSMRingMyMobile;->callState:I
    invoke-static {v4}, Lcom/sec/dsm/system/DSMRingMyMobile;->access$102(I)I

    .line 148
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onCallStateChanged (state) : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    # getter for: Lcom/sec/dsm/system/DSMRingMyMobile;->callState:I
    invoke-static {}, Lcom/sec/dsm/system/DSMRingMyMobile;->access$100()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 149
    iget-object v2, p0, Lcom/sec/dsm/system/DSMRingMyMobile$2;->this$0:Lcom/sec/dsm/system/DSMRingMyMobile;

    # getter for: Lcom/sec/dsm/system/DSMRingMyMobile;->mRingStarted:Z
    invoke-static {v2}, Lcom/sec/dsm/system/DSMRingMyMobile;->access$000(Lcom/sec/dsm/system/DSMRingMyMobile;)Z

    move-result v2

    if-ne v2, v4, :cond_0

    .line 150
    iget-object v2, p0, Lcom/sec/dsm/system/DSMRingMyMobile$2;->this$0:Lcom/sec/dsm/system/DSMRingMyMobile;

    # setter for: Lcom/sec/dsm/system/DSMRingMyMobile;->mRingStarted:Z
    invoke-static {v2, v5}, Lcom/sec/dsm/system/DSMRingMyMobile;->access$002(Lcom/sec/dsm/system/DSMRingMyMobile;Z)Z

    .line 151
    const-string v2, "when call incomming, pause ring my mobile"

    invoke-static {v2}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 154
    :try_start_0
    new-instance v1, Lcom/sec/dsm/system/DSMManager;

    iget-object v2, p0, Lcom/sec/dsm/system/DSMRingMyMobile$2;->this$0:Lcom/sec/dsm/system/DSMRingMyMobile;

    invoke-direct {v1, v2}, Lcom/sec/dsm/system/DSMManager;-><init>(Landroid/content/Context;)V

    .line 155
    .local v1, "im":Lcom/sec/dsm/system/DSMManager;
    invoke-virtual {v1}, Lcom/sec/dsm/system/DSMManager;->removeDSMRing()V
    :try_end_0
    .catch Lcom/sec/dsm/system/DSMException; {:try_start_0 .. :try_end_0} :catch_0

    .line 159
    .end local v1    # "im":Lcom/sec/dsm/system/DSMManager;
    :goto_0
    iget-object v2, p0, Lcom/sec/dsm/system/DSMRingMyMobile$2;->this$0:Lcom/sec/dsm/system/DSMRingMyMobile;

    # invokes: Lcom/sec/dsm/system/DSMRingMyMobile;->lockSystemKey(Z)V
    invoke-static {v2, v5}, Lcom/sec/dsm/system/DSMRingMyMobile;->access$200(Lcom/sec/dsm/system/DSMRingMyMobile;Z)V

    .line 160
    iget-object v2, p0, Lcom/sec/dsm/system/DSMRingMyMobile$2;->this$0:Lcom/sec/dsm/system/DSMRingMyMobile;

    invoke-virtual {v2, v5}, Lcom/sec/dsm/system/DSMRingMyMobile;->setOriginalVolume(I)V

    .line 161
    iget-object v2, p0, Lcom/sec/dsm/system/DSMRingMyMobile$2;->this$0:Lcom/sec/dsm/system/DSMRingMyMobile;

    # invokes: Lcom/sec/dsm/system/DSMRingMyMobile;->setStatusBarCanHide(Z)V
    invoke-static {v2, v5}, Lcom/sec/dsm/system/DSMRingMyMobile;->access$300(Lcom/sec/dsm/system/DSMRingMyMobile;Z)V

    .line 172
    :cond_0
    :goto_1
    return-void

    .line 156
    :catch_0
    move-exception v0

    .line 157
    .local v0, "e":Lcom/sec/dsm/system/DSMException;
    invoke-virtual {v0}, Lcom/sec/dsm/system/DSMException;->printStackTrace()V

    goto :goto_0

    .line 163
    .end local v0    # "e":Lcom/sec/dsm/system/DSMException;
    :cond_1
    if-nez p1, :cond_0

    .line 164
    # setter for: Lcom/sec/dsm/system/DSMRingMyMobile;->callState:I
    invoke-static {v5}, Lcom/sec/dsm/system/DSMRingMyMobile;->access$102(I)I

    .line 165
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onCallStateChanged (state) : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    # getter for: Lcom/sec/dsm/system/DSMRingMyMobile;->callState:I
    invoke-static {}, Lcom/sec/dsm/system/DSMRingMyMobile;->access$100()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 166
    iget-object v2, p0, Lcom/sec/dsm/system/DSMRingMyMobile$2;->this$0:Lcom/sec/dsm/system/DSMRingMyMobile;

    # getter for: Lcom/sec/dsm/system/DSMRingMyMobile;->mRingStarted:Z
    invoke-static {v2}, Lcom/sec/dsm/system/DSMRingMyMobile;->access$000(Lcom/sec/dsm/system/DSMRingMyMobile;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/sec/dsm/system/DSMRingMyMobile$2;->this$0:Lcom/sec/dsm/system/DSMRingMyMobile;

    # getter for: Lcom/sec/dsm/system/DSMRingMyMobile;->mPaused:Z
    invoke-static {v2}, Lcom/sec/dsm/system/DSMRingMyMobile;->access$400(Lcom/sec/dsm/system/DSMRingMyMobile;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 167
    iget-object v2, p0, Lcom/sec/dsm/system/DSMRingMyMobile$2;->this$0:Lcom/sec/dsm/system/DSMRingMyMobile;

    # setter for: Lcom/sec/dsm/system/DSMRingMyMobile;->mRingStarted:Z
    invoke-static {v2, v4}, Lcom/sec/dsm/system/DSMRingMyMobile;->access$002(Lcom/sec/dsm/system/DSMRingMyMobile;Z)Z

    .line 168
    iget-object v2, p0, Lcom/sec/dsm/system/DSMRingMyMobile$2;->this$0:Lcom/sec/dsm/system/DSMRingMyMobile;

    invoke-static {v2, v4, v4}, Lcom/sec/dsm/system/DSMRingMyMobile;->RegisterRepeatingAlarmTime(Landroid/content/Context;ZI)V

    .line 169
    iget-object v2, p0, Lcom/sec/dsm/system/DSMRingMyMobile$2;->this$0:Lcom/sec/dsm/system/DSMRingMyMobile;

    # invokes: Lcom/sec/dsm/system/DSMRingMyMobile;->playMaxAudio(I)V
    invoke-static {v2, v4}, Lcom/sec/dsm/system/DSMRingMyMobile;->access$500(Lcom/sec/dsm/system/DSMRingMyMobile;I)V

    goto :goto_1
.end method
