.class public abstract Lcom/sec/dsm/system/DSMGpsListener;
.super Ljava/lang/Object;
.source "DSMGpsListener.java"

# interfaces
.implements Landroid/location/LocationListener;


# instance fields
.field public mLocation:Landroid/location/Location;

.field public mTask:Ljava/util/TimerTask;

.field public mTimer:Ljava/util/Timer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/dsm/system/DSMGpsListener;->mLocation:Landroid/location/Location;

    return-void
.end method


# virtual methods
.method public endTimer()V
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/dsm/system/DSMGpsListener;->mTimer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 39
    iget-object v0, p0, Lcom/sec/dsm/system/DSMGpsListener;->mTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 40
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/dsm/system/DSMGpsListener;->mTimer:Ljava/util/Timer;

    .line 42
    :cond_0
    return-void
.end method

.method public onLocationChanged(Landroid/location/Location;)V
    .locals 2
    .param p1, "location"    # Landroid/location/Location;

    .prologue
    .line 47
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onLocationChanged : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 49
    iput-object p1, p0, Lcom/sec/dsm/system/DSMGpsListener;->mLocation:Landroid/location/Location;

    .line 50
    iget-object v0, p0, Lcom/sec/dsm/system/DSMGpsListener;->mTimer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 52
    iget-object v0, p0, Lcom/sec/dsm/system/DSMGpsListener;->mTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 53
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/dsm/system/DSMGpsListener;->mTimer:Ljava/util/Timer;

    .line 55
    :cond_0
    iget-object v0, p0, Lcom/sec/dsm/system/DSMGpsListener;->mLocation:Landroid/location/Location;

    invoke-virtual {p0, v0}, Lcom/sec/dsm/system/DSMGpsListener;->onReceiveData(Landroid/location/Location;)V

    .line 56
    return-void
.end method

.method public onProviderDisabled(Ljava/lang/String;)V
    .locals 0
    .param p1, "provider"    # Ljava/lang/String;

    .prologue
    .line 71
    return-void
.end method

.method public onProviderEnabled(Ljava/lang/String;)V
    .locals 0
    .param p1, "provider"    # Ljava/lang/String;

    .prologue
    .line 66
    return-void
.end method

.method public abstract onReceiveData(Landroid/location/Location;)V
.end method

.method public onStatusChanged(Ljava/lang/String;ILandroid/os/Bundle;)V
    .locals 0
    .param p1, "provider"    # Ljava/lang/String;
    .param p2, "status"    # I
    .param p3, "extras"    # Landroid/os/Bundle;

    .prologue
    .line 61
    return-void
.end method

.method public abstract onTimeout()V
.end method

.method public startTimer(J)V
    .locals 3
    .param p1, "ms"    # J

    .prologue
    .line 22
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/dsm/system/DSMGpsListener;->mLocation:Landroid/location/Location;

    .line 23
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/sec/dsm/system/DSMGpsListener;->mTimer:Ljava/util/Timer;

    .line 24
    new-instance v0, Lcom/sec/dsm/system/DSMGpsListener$1;

    invoke-direct {v0, p0}, Lcom/sec/dsm/system/DSMGpsListener$1;-><init>(Lcom/sec/dsm/system/DSMGpsListener;)V

    iput-object v0, p0, Lcom/sec/dsm/system/DSMGpsListener;->mTask:Ljava/util/TimerTask;

    .line 32
    iget-object v0, p0, Lcom/sec/dsm/system/DSMGpsListener;->mTimer:Ljava/util/Timer;

    iget-object v1, p0, Lcom/sec/dsm/system/DSMGpsListener;->mTask:Ljava/util/TimerTask;

    invoke-virtual {v0, v1, p1, p2}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 33
    return-void
.end method
