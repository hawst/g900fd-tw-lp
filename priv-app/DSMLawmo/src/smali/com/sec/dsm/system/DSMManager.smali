.class public Lcom/sec/dsm/system/DSMManager;
.super Ljava/lang/Object;
.source "DSMManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/dsm/system/DSMManager$ValueIndex;
    }
.end annotation


# instance fields
.field private dSMRepository:Lcom/sec/dsm/system/DSMRepository;

.field private mcontext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/dsm/system/DSMException;
        }
    .end annotation

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/dsm/system/DSMManager;->mcontext:Landroid/content/Context;

    .line 54
    :try_start_0
    iput-object p1, p0, Lcom/sec/dsm/system/DSMManager;->mcontext:Landroid/content/Context;

    .line 55
    new-instance v1, Lcom/sec/dsm/system/DSMRepository;

    iget-object v2, p0, Lcom/sec/dsm/system/DSMManager;->mcontext:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/sec/dsm/system/DSMRepository;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/dsm/system/DSMManager;->dSMRepository:Lcom/sec/dsm/system/DSMRepository;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 60
    return-void

    .line 56
    :catch_0
    move-exception v0

    .line 57
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 58
    new-instance v1, Lcom/sec/dsm/system/DSMException;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/sec/dsm/system/DSMException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method private containsKey(Lcom/sec/dsm/system/DSMManager$ValueIndex;)Z
    .locals 1
    .param p1, "key"    # Lcom/sec/dsm/system/DSMManager$ValueIndex;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/dsm/system/DSMException;
        }
    .end annotation

    .prologue
    .line 344
    if-nez p1, :cond_0

    .line 345
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 348
    :cond_0
    invoke-virtual {p1}, Lcom/sec/dsm/system/DSMManager$ValueIndex;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/dsm/system/DSMManager;->containsKey(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private containsKey(Ljava/lang/String;)Z
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/dsm/system/DSMException;
        }
    .end annotation

    .prologue
    .line 351
    if-nez p1, :cond_0

    .line 352
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 354
    :cond_0
    iget-object v0, p0, Lcom/sec/dsm/system/DSMManager;->dSMRepository:Lcom/sec/dsm/system/DSMRepository;

    invoke-virtual {v0, p1}, Lcom/sec/dsm/system/DSMRepository;->containsKey(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private get(Lcom/sec/dsm/system/DSMManager$ValueIndex;)Ljava/lang/String;
    .locals 1
    .param p1, "key"    # Lcom/sec/dsm/system/DSMManager$ValueIndex;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/dsm/system/DSMException;
        }
    .end annotation

    .prologue
    .line 358
    if-nez p1, :cond_0

    .line 359
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 361
    :cond_0
    invoke-virtual {p1}, Lcom/sec/dsm/system/DSMManager$ValueIndex;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/dsm/system/DSMManager;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private get(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/dsm/system/DSMException;
        }
    .end annotation

    .prologue
    .line 365
    if-nez p1, :cond_0

    .line 366
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 369
    :cond_0
    iget-object v0, p0, Lcom/sec/dsm/system/DSMManager;->dSMRepository:Lcom/sec/dsm/system/DSMRepository;

    invoke-virtual {v0, p1}, Lcom/sec/dsm/system/DSMRepository;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private put(Lcom/sec/dsm/system/DSMManager$ValueIndex;Ljava/lang/String;)V
    .locals 1
    .param p1, "key"    # Lcom/sec/dsm/system/DSMManager$ValueIndex;
    .param p2, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/dsm/system/DSMException;
        }
    .end annotation

    .prologue
    .line 373
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 374
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 376
    :cond_1
    invoke-virtual {p1}, Lcom/sec/dsm/system/DSMManager$ValueIndex;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lcom/sec/dsm/system/DSMManager;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 377
    return-void
.end method

.method private put(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/dsm/system/DSMException;
        }
    .end annotation

    .prologue
    .line 380
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 381
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 383
    :cond_1
    iget-object v0, p0, Lcom/sec/dsm/system/DSMManager;->dSMRepository:Lcom/sec/dsm/system/DSMRepository;

    invoke-virtual {v0, p1, p2}, Lcom/sec/dsm/system/DSMRepository;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 384
    return-void
.end method

.method private remove(Lcom/sec/dsm/system/DSMManager$ValueIndex;)V
    .locals 1
    .param p1, "key"    # Lcom/sec/dsm/system/DSMManager$ValueIndex;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/dsm/system/DSMException;
        }
    .end annotation

    .prologue
    .line 387
    if-nez p1, :cond_0

    .line 388
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 390
    :cond_0
    invoke-virtual {p1}, Lcom/sec/dsm/system/DSMManager$ValueIndex;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/dsm/system/DSMManager;->remove(Ljava/lang/String;)V

    .line 391
    return-void
.end method

.method private remove(Ljava/lang/String;)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/dsm/system/DSMException;
        }
    .end annotation

    .prologue
    .line 394
    if-nez p1, :cond_0

    .line 395
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 397
    :cond_0
    iget-object v0, p0, Lcom/sec/dsm/system/DSMManager;->dSMRepository:Lcom/sec/dsm/system/DSMRepository;

    invoke-virtual {v0, p1}, Lcom/sec/dsm/system/DSMRepository;->remove(Ljava/lang/String;)V

    .line 398
    return-void
.end method


# virtual methods
.method public containsCALLForwarding()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/dsm/system/DSMException;
        }
    .end annotation

    .prologue
    .line 283
    sget-object v0, Lcom/sec/dsm/system/DSMManager$ValueIndex;->CALLForwarding:Lcom/sec/dsm/system/DSMManager$ValueIndex;

    invoke-direct {p0, v0}, Lcom/sec/dsm/system/DSMManager;->containsKey(Lcom/sec/dsm/system/DSMManager$ValueIndex;)Z

    move-result v0

    return v0
.end method

.method public containsDSMForwardingResult()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/dsm/system/DSMException;
        }
    .end annotation

    .prologue
    .line 259
    sget-object v0, Lcom/sec/dsm/system/DSMManager$ValueIndex;->DSMForwardingResult:Lcom/sec/dsm/system/DSMManager$ValueIndex;

    invoke-direct {p0, v0}, Lcom/sec/dsm/system/DSMManager;->containsKey(Lcom/sec/dsm/system/DSMManager$ValueIndex;)Z

    move-result v0

    return v0
.end method

.method public containsDSMIMSI()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/dsm/system/DSMException;
        }
    .end annotation

    .prologue
    .line 271
    sget-object v0, Lcom/sec/dsm/system/DSMManager$ValueIndex;->DSMIMSI:Lcom/sec/dsm/system/DSMManager$ValueIndex;

    invoke-direct {p0, v0}, Lcom/sec/dsm/system/DSMManager;->containsKey(Lcom/sec/dsm/system/DSMManager$ValueIndex;)Z

    move-result v0

    return v0
.end method

.method public containsDSMInterval()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/dsm/system/DSMException;
        }
    .end annotation

    .prologue
    .line 247
    sget-object v0, Lcom/sec/dsm/system/DSMManager$ValueIndex;->DSMInterval:Lcom/sec/dsm/system/DSMManager$ValueIndex;

    invoke-direct {p0, v0}, Lcom/sec/dsm/system/DSMManager;->containsKey(Lcom/sec/dsm/system/DSMManager$ValueIndex;)Z

    move-result v0

    return v0
.end method

.method public containsDSMRing()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/dsm/system/DSMException;
        }
    .end annotation

    .prologue
    .line 263
    sget-object v0, Lcom/sec/dsm/system/DSMManager$ValueIndex;->DSMRing:Lcom/sec/dsm/system/DSMManager$ValueIndex;

    invoke-direct {p0, v0}, Lcom/sec/dsm/system/DSMManager;->containsKey(Lcom/sec/dsm/system/DSMManager$ValueIndex;)Z

    move-result v0

    return v0
.end method

.method public containsDSMRingMessage()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/dsm/system/DSMException;
        }
    .end annotation

    .prologue
    .line 267
    sget-object v0, Lcom/sec/dsm/system/DSMManager$ValueIndex;->DSMRingMessage:Lcom/sec/dsm/system/DSMManager$ValueIndex;

    invoke-direct {p0, v0}, Lcom/sec/dsm/system/DSMManager;->containsKey(Lcom/sec/dsm/system/DSMManager$ValueIndex;)Z

    move-result v0

    return v0
.end method

.method public containsDSMStartTime()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/dsm/system/DSMException;
        }
    .end annotation

    .prologue
    .line 239
    sget-object v0, Lcom/sec/dsm/system/DSMManager$ValueIndex;->DSMStartTime:Lcom/sec/dsm/system/DSMManager$ValueIndex;

    invoke-direct {p0, v0}, Lcom/sec/dsm/system/DSMManager;->containsKey(Lcom/sec/dsm/system/DSMManager$ValueIndex;)Z

    move-result v0

    return v0
.end method

.method public containsDSMStopTime()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/dsm/system/DSMException;
        }
    .end annotation

    .prologue
    .line 243
    sget-object v0, Lcom/sec/dsm/system/DSMManager$ValueIndex;->DSMStopTime:Lcom/sec/dsm/system/DSMManager$ValueIndex;

    invoke-direct {p0, v0}, Lcom/sec/dsm/system/DSMManager;->containsKey(Lcom/sec/dsm/system/DSMManager$ValueIndex;)Z

    move-result v0

    return v0
.end method

.method public containsDSMTracking()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/dsm/system/DSMException;
        }
    .end annotation

    .prologue
    .line 235
    sget-object v0, Lcom/sec/dsm/system/DSMManager$ValueIndex;->DSMTracking:Lcom/sec/dsm/system/DSMManager$ValueIndex;

    invoke-direct {p0, v0}, Lcom/sec/dsm/system/DSMManager;->containsKey(Lcom/sec/dsm/system/DSMManager$ValueIndex;)Z

    move-result v0

    return v0
.end method

.method public containsDSMWipeOut()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/dsm/system/DSMException;
        }
    .end annotation

    .prologue
    .line 251
    sget-object v0, Lcom/sec/dsm/system/DSMManager$ValueIndex;->DSMWipeOut:Lcom/sec/dsm/system/DSMManager$ValueIndex;

    invoke-direct {p0, v0}, Lcom/sec/dsm/system/DSMManager;->containsKey(Lcom/sec/dsm/system/DSMManager$ValueIndex;)Z

    move-result v0

    return v0
.end method

.method public containsDSMWipeOutResult()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/dsm/system/DSMException;
        }
    .end annotation

    .prologue
    .line 255
    sget-object v0, Lcom/sec/dsm/system/DSMManager$ValueIndex;->DSMWipeOutResult:Lcom/sec/dsm/system/DSMManager$ValueIndex;

    invoke-direct {p0, v0}, Lcom/sec/dsm/system/DSMManager;->containsKey(Lcom/sec/dsm/system/DSMManager$ValueIndex;)Z

    move-result v0

    return v0
.end method

.method public containsSMSForwarding()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/dsm/system/DSMException;
        }
    .end annotation

    .prologue
    .line 275
    sget-object v0, Lcom/sec/dsm/system/DSMManager$ValueIndex;->SMSForwarding:Lcom/sec/dsm/system/DSMManager$ValueIndex;

    invoke-direct {p0, v0}, Lcom/sec/dsm/system/DSMManager;->containsKey(Lcom/sec/dsm/system/DSMManager$ValueIndex;)Z

    move-result v0

    return v0
.end method

.method public containsSMSRecipient()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/dsm/system/DSMException;
        }
    .end annotation

    .prologue
    .line 279
    sget-object v0, Lcom/sec/dsm/system/DSMManager$ValueIndex;->SMSRecipient:Lcom/sec/dsm/system/DSMManager$ValueIndex;

    invoke-direct {p0, v0}, Lcom/sec/dsm/system/DSMManager;->containsKey(Lcom/sec/dsm/system/DSMManager$ValueIndex;)Z

    move-result v0

    return v0
.end method

.method public getCALLForwarding()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/dsm/system/DSMException;
        }
    .end annotation

    .prologue
    .line 111
    sget-object v0, Lcom/sec/dsm/system/DSMManager$ValueIndex;->CALLForwarding:Lcom/sec/dsm/system/DSMManager$ValueIndex;

    invoke-direct {p0, v0}, Lcom/sec/dsm/system/DSMManager;->get(Lcom/sec/dsm/system/DSMManager$ValueIndex;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDMServer()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/dsm/system/DSMException;
        }
    .end annotation

    .prologue
    .line 119
    sget-object v0, Lcom/sec/dsm/system/DSMManager$ValueIndex;->DMServer:Lcom/sec/dsm/system/DSMManager$ValueIndex;

    invoke-direct {p0, v0}, Lcom/sec/dsm/system/DSMManager;->get(Lcom/sec/dsm/system/DSMManager$ValueIndex;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDSMForwardingResult()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/dsm/system/DSMException;
        }
    .end annotation

    .prologue
    .line 87
    sget-object v0, Lcom/sec/dsm/system/DSMManager$ValueIndex;->DSMForwardingResult:Lcom/sec/dsm/system/DSMManager$ValueIndex;

    invoke-direct {p0, v0}, Lcom/sec/dsm/system/DSMManager;->get(Lcom/sec/dsm/system/DSMManager$ValueIndex;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDSMIMSI()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/dsm/system/DSMException;
        }
    .end annotation

    .prologue
    .line 99
    sget-object v0, Lcom/sec/dsm/system/DSMManager$ValueIndex;->DSMIMSI:Lcom/sec/dsm/system/DSMManager$ValueIndex;

    invoke-direct {p0, v0}, Lcom/sec/dsm/system/DSMManager;->get(Lcom/sec/dsm/system/DSMManager$ValueIndex;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDSMInterval()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/dsm/system/DSMException;
        }
    .end annotation

    .prologue
    .line 75
    sget-object v0, Lcom/sec/dsm/system/DSMManager$ValueIndex;->DSMInterval:Lcom/sec/dsm/system/DSMManager$ValueIndex;

    invoke-direct {p0, v0}, Lcom/sec/dsm/system/DSMManager;->get(Lcom/sec/dsm/system/DSMManager$ValueIndex;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDSMRing()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/dsm/system/DSMException;
        }
    .end annotation

    .prologue
    .line 91
    sget-object v0, Lcom/sec/dsm/system/DSMManager$ValueIndex;->DSMRing:Lcom/sec/dsm/system/DSMManager$ValueIndex;

    invoke-direct {p0, v0}, Lcom/sec/dsm/system/DSMManager;->get(Lcom/sec/dsm/system/DSMManager$ValueIndex;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDSMRingMessage()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/dsm/system/DSMException;
        }
    .end annotation

    .prologue
    .line 95
    sget-object v0, Lcom/sec/dsm/system/DSMManager$ValueIndex;->DSMRingMessage:Lcom/sec/dsm/system/DSMManager$ValueIndex;

    invoke-direct {p0, v0}, Lcom/sec/dsm/system/DSMManager;->get(Lcom/sec/dsm/system/DSMManager$ValueIndex;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDSMStartTime()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/dsm/system/DSMException;
        }
    .end annotation

    .prologue
    .line 67
    sget-object v0, Lcom/sec/dsm/system/DSMManager$ValueIndex;->DSMStartTime:Lcom/sec/dsm/system/DSMManager$ValueIndex;

    invoke-direct {p0, v0}, Lcom/sec/dsm/system/DSMManager;->get(Lcom/sec/dsm/system/DSMManager$ValueIndex;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDSMStopTime()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/dsm/system/DSMException;
        }
    .end annotation

    .prologue
    .line 71
    sget-object v0, Lcom/sec/dsm/system/DSMManager$ValueIndex;->DSMStopTime:Lcom/sec/dsm/system/DSMManager$ValueIndex;

    invoke-direct {p0, v0}, Lcom/sec/dsm/system/DSMManager;->get(Lcom/sec/dsm/system/DSMManager$ValueIndex;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDSMTracking()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/dsm/system/DSMException;
        }
    .end annotation

    .prologue
    .line 63
    sget-object v0, Lcom/sec/dsm/system/DSMManager$ValueIndex;->DSMTracking:Lcom/sec/dsm/system/DSMManager$ValueIndex;

    invoke-direct {p0, v0}, Lcom/sec/dsm/system/DSMManager;->get(Lcom/sec/dsm/system/DSMManager$ValueIndex;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDSMWipeOut()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/dsm/system/DSMException;
        }
    .end annotation

    .prologue
    .line 79
    sget-object v0, Lcom/sec/dsm/system/DSMManager$ValueIndex;->DSMWipeOut:Lcom/sec/dsm/system/DSMManager$ValueIndex;

    invoke-direct {p0, v0}, Lcom/sec/dsm/system/DSMManager;->get(Lcom/sec/dsm/system/DSMManager$ValueIndex;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDSMWipeOutResult()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/dsm/system/DSMException;
        }
    .end annotation

    .prologue
    .line 83
    sget-object v0, Lcom/sec/dsm/system/DSMManager$ValueIndex;->DSMWipeOutResult:Lcom/sec/dsm/system/DSMManager$ValueIndex;

    invoke-direct {p0, v0}, Lcom/sec/dsm/system/DSMManager;->get(Lcom/sec/dsm/system/DSMManager$ValueIndex;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDSServer()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/dsm/system/DSMException;
        }
    .end annotation

    .prologue
    .line 123
    sget-object v0, Lcom/sec/dsm/system/DSMManager$ValueIndex;->DSServer:Lcom/sec/dsm/system/DSMManager$ValueIndex;

    invoke-direct {p0, v0}, Lcom/sec/dsm/system/DSMManager;->get(Lcom/sec/dsm/system/DSMManager$ValueIndex;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSMSForwarding()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/dsm/system/DSMException;
        }
    .end annotation

    .prologue
    .line 103
    sget-object v0, Lcom/sec/dsm/system/DSMManager$ValueIndex;->SMSForwarding:Lcom/sec/dsm/system/DSMManager$ValueIndex;

    invoke-direct {p0, v0}, Lcom/sec/dsm/system/DSMManager;->get(Lcom/sec/dsm/system/DSMManager$ValueIndex;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSMSRecipient()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/dsm/system/DSMException;
        }
    .end annotation

    .prologue
    .line 107
    sget-object v0, Lcom/sec/dsm/system/DSMManager$ValueIndex;->SMSRecipient:Lcom/sec/dsm/system/DSMManager$ValueIndex;

    invoke-direct {p0, v0}, Lcom/sec/dsm/system/DSMManager;->get(Lcom/sec/dsm/system/DSMManager$ValueIndex;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getServerUrl()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/dsm/system/DSMException;
        }
    .end annotation

    .prologue
    .line 115
    sget-object v0, Lcom/sec/dsm/system/DSMManager$ValueIndex;->ServerUrl:Lcom/sec/dsm/system/DSMManager$ValueIndex;

    invoke-direct {p0, v0}, Lcom/sec/dsm/system/DSMManager;->get(Lcom/sec/dsm/system/DSMManager$ValueIndex;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public putCALLForwarding(Ljava/lang/String;)V
    .locals 1
    .param p1, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/dsm/system/DSMException;
        }
    .end annotation

    .prologue
    .line 191
    if-nez p1, :cond_0

    .line 192
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 194
    :cond_0
    sget-object v0, Lcom/sec/dsm/system/DSMManager$ValueIndex;->CALLForwarding:Lcom/sec/dsm/system/DSMManager$ValueIndex;

    invoke-direct {p0, v0, p1}, Lcom/sec/dsm/system/DSMManager;->put(Lcom/sec/dsm/system/DSMManager$ValueIndex;Ljava/lang/String;)V

    .line 195
    return-void
.end method

.method public putDSMForwardingResult(Ljava/lang/String;)V
    .locals 1
    .param p1, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/dsm/system/DSMException;
        }
    .end annotation

    .prologue
    .line 170
    if-nez p1, :cond_0

    .line 171
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 173
    :cond_0
    sget-object v0, Lcom/sec/dsm/system/DSMManager$ValueIndex;->DSMForwardingResult:Lcom/sec/dsm/system/DSMManager$ValueIndex;

    invoke-direct {p0, v0, p1}, Lcom/sec/dsm/system/DSMManager;->put(Lcom/sec/dsm/system/DSMManager$ValueIndex;Ljava/lang/String;)V

    .line 174
    return-void
.end method

.method public putDSMIMSI(Ljava/lang/String;)V
    .locals 1
    .param p1, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/dsm/system/DSMException;
        }
    .end annotation

    .prologue
    .line 212
    if-nez p1, :cond_0

    .line 213
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 215
    :cond_0
    sget-object v0, Lcom/sec/dsm/system/DSMManager$ValueIndex;->DSMIMSI:Lcom/sec/dsm/system/DSMManager$ValueIndex;

    invoke-direct {p0, v0, p1}, Lcom/sec/dsm/system/DSMManager;->put(Lcom/sec/dsm/system/DSMManager$ValueIndex;Ljava/lang/String;)V

    .line 216
    return-void
.end method

.method public putDSMInterval(Ljava/lang/String;)V
    .locals 1
    .param p1, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/dsm/system/DSMException;
        }
    .end annotation

    .prologue
    .line 149
    if-nez p1, :cond_0

    .line 150
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 152
    :cond_0
    sget-object v0, Lcom/sec/dsm/system/DSMManager$ValueIndex;->DSMInterval:Lcom/sec/dsm/system/DSMManager$ValueIndex;

    invoke-direct {p0, v0, p1}, Lcom/sec/dsm/system/DSMManager;->put(Lcom/sec/dsm/system/DSMManager$ValueIndex;Ljava/lang/String;)V

    .line 153
    return-void
.end method

.method public putDSMRing(Ljava/lang/String;)V
    .locals 1
    .param p1, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/dsm/system/DSMException;
        }
    .end annotation

    .prologue
    .line 177
    if-nez p1, :cond_0

    .line 178
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 180
    :cond_0
    sget-object v0, Lcom/sec/dsm/system/DSMManager$ValueIndex;->DSMRing:Lcom/sec/dsm/system/DSMManager$ValueIndex;

    invoke-direct {p0, v0, p1}, Lcom/sec/dsm/system/DSMManager;->put(Lcom/sec/dsm/system/DSMManager$ValueIndex;Ljava/lang/String;)V

    .line 181
    return-void
.end method

.method public putDSMRingMessage(Ljava/lang/String;)V
    .locals 1
    .param p1, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/dsm/system/DSMException;
        }
    .end annotation

    .prologue
    .line 184
    if-nez p1, :cond_0

    .line 185
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 187
    :cond_0
    sget-object v0, Lcom/sec/dsm/system/DSMManager$ValueIndex;->DSMRingMessage:Lcom/sec/dsm/system/DSMManager$ValueIndex;

    invoke-direct {p0, v0, p1}, Lcom/sec/dsm/system/DSMManager;->put(Lcom/sec/dsm/system/DSMManager$ValueIndex;Ljava/lang/String;)V

    .line 188
    return-void
.end method

.method public putDSMStartTime(Ljava/lang/String;)V
    .locals 1
    .param p1, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/dsm/system/DSMException;
        }
    .end annotation

    .prologue
    .line 135
    if-nez p1, :cond_0

    .line 136
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 138
    :cond_0
    sget-object v0, Lcom/sec/dsm/system/DSMManager$ValueIndex;->DSMStartTime:Lcom/sec/dsm/system/DSMManager$ValueIndex;

    invoke-direct {p0, v0, p1}, Lcom/sec/dsm/system/DSMManager;->put(Lcom/sec/dsm/system/DSMManager$ValueIndex;Ljava/lang/String;)V

    .line 139
    return-void
.end method

.method public putDSMStopTime(Ljava/lang/String;)V
    .locals 1
    .param p1, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/dsm/system/DSMException;
        }
    .end annotation

    .prologue
    .line 142
    if-nez p1, :cond_0

    .line 143
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 145
    :cond_0
    sget-object v0, Lcom/sec/dsm/system/DSMManager$ValueIndex;->DSMStopTime:Lcom/sec/dsm/system/DSMManager$ValueIndex;

    invoke-direct {p0, v0, p1}, Lcom/sec/dsm/system/DSMManager;->put(Lcom/sec/dsm/system/DSMManager$ValueIndex;Ljava/lang/String;)V

    .line 146
    return-void
.end method

.method public putDSMTracking(Ljava/lang/String;)V
    .locals 1
    .param p1, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/dsm/system/DSMException;
        }
    .end annotation

    .prologue
    .line 128
    if-nez p1, :cond_0

    .line 129
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 131
    :cond_0
    sget-object v0, Lcom/sec/dsm/system/DSMManager$ValueIndex;->DSMTracking:Lcom/sec/dsm/system/DSMManager$ValueIndex;

    invoke-direct {p0, v0, p1}, Lcom/sec/dsm/system/DSMManager;->put(Lcom/sec/dsm/system/DSMManager$ValueIndex;Ljava/lang/String;)V

    .line 132
    return-void
.end method

.method public putDSMWipeOut(Ljava/lang/String;)V
    .locals 1
    .param p1, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/dsm/system/DSMException;
        }
    .end annotation

    .prologue
    .line 156
    if-nez p1, :cond_0

    .line 157
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 159
    :cond_0
    sget-object v0, Lcom/sec/dsm/system/DSMManager$ValueIndex;->DSMWipeOut:Lcom/sec/dsm/system/DSMManager$ValueIndex;

    invoke-direct {p0, v0, p1}, Lcom/sec/dsm/system/DSMManager;->put(Lcom/sec/dsm/system/DSMManager$ValueIndex;Ljava/lang/String;)V

    .line 160
    return-void
.end method

.method public putDSMWipeOutResult(Ljava/lang/String;)V
    .locals 1
    .param p1, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/dsm/system/DSMException;
        }
    .end annotation

    .prologue
    .line 163
    if-nez p1, :cond_0

    .line 164
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 166
    :cond_0
    sget-object v0, Lcom/sec/dsm/system/DSMManager$ValueIndex;->DSMWipeOutResult:Lcom/sec/dsm/system/DSMManager$ValueIndex;

    invoke-direct {p0, v0, p1}, Lcom/sec/dsm/system/DSMManager;->put(Lcom/sec/dsm/system/DSMManager$ValueIndex;Ljava/lang/String;)V

    .line 167
    return-void
.end method

.method public putSMSForwarding(Ljava/lang/String;)V
    .locals 1
    .param p1, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/dsm/system/DSMException;
        }
    .end annotation

    .prologue
    .line 198
    if-nez p1, :cond_0

    .line 199
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 201
    :cond_0
    sget-object v0, Lcom/sec/dsm/system/DSMManager$ValueIndex;->SMSForwarding:Lcom/sec/dsm/system/DSMManager$ValueIndex;

    invoke-direct {p0, v0, p1}, Lcom/sec/dsm/system/DSMManager;->put(Lcom/sec/dsm/system/DSMManager$ValueIndex;Ljava/lang/String;)V

    .line 202
    return-void
.end method

.method public putSMSRecipient(Ljava/lang/String;)V
    .locals 1
    .param p1, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/dsm/system/DSMException;
        }
    .end annotation

    .prologue
    .line 205
    if-nez p1, :cond_0

    .line 206
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 208
    :cond_0
    sget-object v0, Lcom/sec/dsm/system/DSMManager$ValueIndex;->SMSRecipient:Lcom/sec/dsm/system/DSMManager$ValueIndex;

    invoke-direct {p0, v0, p1}, Lcom/sec/dsm/system/DSMManager;->put(Lcom/sec/dsm/system/DSMManager$ValueIndex;Ljava/lang/String;)V

    .line 209
    return-void
.end method

.method public putServerUrl(Ljava/lang/String;)V
    .locals 2
    .param p1, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/dsm/system/DSMException;
        }
    .end annotation

    .prologue
    .line 219
    if-nez p1, :cond_0

    .line 220
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 222
    :cond_0
    sget-object v0, Lcom/sec/dsm/system/DSMManager$ValueIndex;->ServerUrl:Lcom/sec/dsm/system/DSMManager$ValueIndex;

    invoke-direct {p0, v0, p1}, Lcom/sec/dsm/system/DSMManager;->put(Lcom/sec/dsm/system/DSMManager$ValueIndex;Ljava/lang/String;)V

    .line 223
    const-string v0, "chn"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 224
    sget-object v0, Lcom/sec/dsm/system/DSMManager$ValueIndex;->DMServer:Lcom/sec/dsm/system/DSMManager$ValueIndex;

    const-string v1, "https://dm.samsungdive.cn/v1/sdm/magicsync/dm"

    invoke-direct {p0, v0, v1}, Lcom/sec/dsm/system/DSMManager;->put(Lcom/sec/dsm/system/DSMManager$ValueIndex;Ljava/lang/String;)V

    .line 225
    sget-object v0, Lcom/sec/dsm/system/DSMManager$ValueIndex;->DSServer:Lcom/sec/dsm/system/DSMManager$ValueIndex;

    const-string v1, "syn.samsungdive.cn"

    invoke-direct {p0, v0, v1}, Lcom/sec/dsm/system/DSMManager;->put(Lcom/sec/dsm/system/DSMManager$ValueIndex;Ljava/lang/String;)V

    .line 230
    :goto_0
    return-void

    .line 227
    :cond_1
    sget-object v0, Lcom/sec/dsm/system/DSMManager$ValueIndex;->DMServer:Lcom/sec/dsm/system/DSMManager$ValueIndex;

    const-string v1, "https://dm.samsungdive.com/v1/sdm/magicsync/dm"

    invoke-direct {p0, v0, v1}, Lcom/sec/dsm/system/DSMManager;->put(Lcom/sec/dsm/system/DSMManager$ValueIndex;Ljava/lang/String;)V

    .line 228
    sget-object v0, Lcom/sec/dsm/system/DSMManager$ValueIndex;->DSServer:Lcom/sec/dsm/system/DSMManager$ValueIndex;

    const-string v1, "syn.samsungdive.com"

    invoke-direct {p0, v0, v1}, Lcom/sec/dsm/system/DSMManager;->put(Lcom/sec/dsm/system/DSMManager$ValueIndex;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public removeCALLForwarding()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/dsm/system/DSMException;
        }
    .end annotation

    .prologue
    .line 314
    sget-object v0, Lcom/sec/dsm/system/DSMManager$ValueIndex;->CALLForwarding:Lcom/sec/dsm/system/DSMManager$ValueIndex;

    invoke-direct {p0, v0}, Lcom/sec/dsm/system/DSMManager;->remove(Lcom/sec/dsm/system/DSMManager$ValueIndex;)V

    .line 315
    return-void
.end method

.method public removeDSMForwarding()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/dsm/system/DSMException;
        }
    .end annotation

    .prologue
    .line 301
    sget-object v0, Lcom/sec/dsm/system/DSMManager$ValueIndex;->DSMForwardingResult:Lcom/sec/dsm/system/DSMManager$ValueIndex;

    invoke-direct {p0, v0}, Lcom/sec/dsm/system/DSMManager;->remove(Lcom/sec/dsm/system/DSMManager$ValueIndex;)V

    .line 302
    return-void
.end method

.method public removeDSMIMSI()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/dsm/system/DSMException;
        }
    .end annotation

    .prologue
    .line 318
    sget-object v0, Lcom/sec/dsm/system/DSMManager$ValueIndex;->DSMIMSI:Lcom/sec/dsm/system/DSMManager$ValueIndex;

    invoke-direct {p0, v0}, Lcom/sec/dsm/system/DSMManager;->remove(Lcom/sec/dsm/system/DSMManager$ValueIndex;)V

    .line 319
    return-void
.end method

.method public removeDSMRing()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/dsm/system/DSMException;
        }
    .end annotation

    .prologue
    .line 305
    sget-object v0, Lcom/sec/dsm/system/DSMManager$ValueIndex;->DSMRing:Lcom/sec/dsm/system/DSMManager$ValueIndex;

    invoke-direct {p0, v0}, Lcom/sec/dsm/system/DSMManager;->remove(Lcom/sec/dsm/system/DSMManager$ValueIndex;)V

    .line 306
    sget-object v0, Lcom/sec/dsm/system/DSMManager$ValueIndex;->DSMRingMessage:Lcom/sec/dsm/system/DSMManager$ValueIndex;

    invoke-direct {p0, v0}, Lcom/sec/dsm/system/DSMManager;->remove(Lcom/sec/dsm/system/DSMManager$ValueIndex;)V

    .line 307
    return-void
.end method

.method public removeDSMTracking()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/dsm/system/DSMException;
        }
    .end annotation

    .prologue
    .line 289
    sget-object v0, Lcom/sec/dsm/system/DSMManager$ValueIndex;->DSMTracking:Lcom/sec/dsm/system/DSMManager$ValueIndex;

    invoke-direct {p0, v0}, Lcom/sec/dsm/system/DSMManager;->remove(Lcom/sec/dsm/system/DSMManager$ValueIndex;)V

    .line 290
    sget-object v0, Lcom/sec/dsm/system/DSMManager$ValueIndex;->DSMStartTime:Lcom/sec/dsm/system/DSMManager$ValueIndex;

    invoke-direct {p0, v0}, Lcom/sec/dsm/system/DSMManager;->remove(Lcom/sec/dsm/system/DSMManager$ValueIndex;)V

    .line 291
    sget-object v0, Lcom/sec/dsm/system/DSMManager$ValueIndex;->DSMStopTime:Lcom/sec/dsm/system/DSMManager$ValueIndex;

    invoke-direct {p0, v0}, Lcom/sec/dsm/system/DSMManager;->remove(Lcom/sec/dsm/system/DSMManager$ValueIndex;)V

    .line 292
    sget-object v0, Lcom/sec/dsm/system/DSMManager$ValueIndex;->DSMInterval:Lcom/sec/dsm/system/DSMManager$ValueIndex;

    invoke-direct {p0, v0}, Lcom/sec/dsm/system/DSMManager;->remove(Lcom/sec/dsm/system/DSMManager$ValueIndex;)V

    .line 293
    return-void
.end method

.method public removeDSMWipeOut()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/dsm/system/DSMException;
        }
    .end annotation

    .prologue
    .line 296
    sget-object v0, Lcom/sec/dsm/system/DSMManager$ValueIndex;->DSMWipeOut:Lcom/sec/dsm/system/DSMManager$ValueIndex;

    invoke-direct {p0, v0}, Lcom/sec/dsm/system/DSMManager;->remove(Lcom/sec/dsm/system/DSMManager$ValueIndex;)V

    .line 297
    sget-object v0, Lcom/sec/dsm/system/DSMManager$ValueIndex;->DSMWipeOutResult:Lcom/sec/dsm/system/DSMManager$ValueIndex;

    invoke-direct {p0, v0}, Lcom/sec/dsm/system/DSMManager;->remove(Lcom/sec/dsm/system/DSMManager$ValueIndex;)V

    .line 298
    return-void
.end method

.method public removeInitialization()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/dsm/system/DSMException;
        }
    .end annotation

    .prologue
    .line 326
    sget-object v0, Lcom/sec/dsm/system/DSMManager$ValueIndex;->DSMTracking:Lcom/sec/dsm/system/DSMManager$ValueIndex;

    invoke-direct {p0, v0}, Lcom/sec/dsm/system/DSMManager;->remove(Lcom/sec/dsm/system/DSMManager$ValueIndex;)V

    .line 327
    sget-object v0, Lcom/sec/dsm/system/DSMManager$ValueIndex;->DSMStartTime:Lcom/sec/dsm/system/DSMManager$ValueIndex;

    invoke-direct {p0, v0}, Lcom/sec/dsm/system/DSMManager;->remove(Lcom/sec/dsm/system/DSMManager$ValueIndex;)V

    .line 328
    sget-object v0, Lcom/sec/dsm/system/DSMManager$ValueIndex;->DSMStopTime:Lcom/sec/dsm/system/DSMManager$ValueIndex;

    invoke-direct {p0, v0}, Lcom/sec/dsm/system/DSMManager;->remove(Lcom/sec/dsm/system/DSMManager$ValueIndex;)V

    .line 329
    sget-object v0, Lcom/sec/dsm/system/DSMManager$ValueIndex;->DSMInterval:Lcom/sec/dsm/system/DSMManager$ValueIndex;

    invoke-direct {p0, v0}, Lcom/sec/dsm/system/DSMManager;->remove(Lcom/sec/dsm/system/DSMManager$ValueIndex;)V

    .line 330
    sget-object v0, Lcom/sec/dsm/system/DSMManager$ValueIndex;->DSMRing:Lcom/sec/dsm/system/DSMManager$ValueIndex;

    invoke-direct {p0, v0}, Lcom/sec/dsm/system/DSMManager;->remove(Lcom/sec/dsm/system/DSMManager$ValueIndex;)V

    .line 331
    sget-object v0, Lcom/sec/dsm/system/DSMManager$ValueIndex;->DSMRingMessage:Lcom/sec/dsm/system/DSMManager$ValueIndex;

    invoke-direct {p0, v0}, Lcom/sec/dsm/system/DSMManager;->remove(Lcom/sec/dsm/system/DSMManager$ValueIndex;)V

    .line 332
    sget-object v0, Lcom/sec/dsm/system/DSMManager$ValueIndex;->DSMWipeOut:Lcom/sec/dsm/system/DSMManager$ValueIndex;

    invoke-direct {p0, v0}, Lcom/sec/dsm/system/DSMManager;->remove(Lcom/sec/dsm/system/DSMManager$ValueIndex;)V

    .line 333
    sget-object v0, Lcom/sec/dsm/system/DSMManager$ValueIndex;->DSMWipeOutResult:Lcom/sec/dsm/system/DSMManager$ValueIndex;

    invoke-direct {p0, v0}, Lcom/sec/dsm/system/DSMManager;->remove(Lcom/sec/dsm/system/DSMManager$ValueIndex;)V

    .line 334
    sget-object v0, Lcom/sec/dsm/system/DSMManager$ValueIndex;->DSMForwardingResult:Lcom/sec/dsm/system/DSMManager$ValueIndex;

    invoke-direct {p0, v0}, Lcom/sec/dsm/system/DSMManager;->remove(Lcom/sec/dsm/system/DSMManager$ValueIndex;)V

    .line 335
    sget-object v0, Lcom/sec/dsm/system/DSMManager$ValueIndex;->SMSForwarding:Lcom/sec/dsm/system/DSMManager$ValueIndex;

    invoke-direct {p0, v0}, Lcom/sec/dsm/system/DSMManager;->remove(Lcom/sec/dsm/system/DSMManager$ValueIndex;)V

    .line 336
    sget-object v0, Lcom/sec/dsm/system/DSMManager$ValueIndex;->DSMIMSI:Lcom/sec/dsm/system/DSMManager$ValueIndex;

    invoke-direct {p0, v0}, Lcom/sec/dsm/system/DSMManager;->remove(Lcom/sec/dsm/system/DSMManager$ValueIndex;)V

    .line 337
    sget-object v0, Lcom/sec/dsm/system/DSMManager$ValueIndex;->SMSRecipient:Lcom/sec/dsm/system/DSMManager$ValueIndex;

    invoke-direct {p0, v0}, Lcom/sec/dsm/system/DSMManager;->remove(Lcom/sec/dsm/system/DSMManager$ValueIndex;)V

    .line 338
    sget-object v0, Lcom/sec/dsm/system/DSMManager$ValueIndex;->ServerUrl:Lcom/sec/dsm/system/DSMManager$ValueIndex;

    invoke-direct {p0, v0}, Lcom/sec/dsm/system/DSMManager;->remove(Lcom/sec/dsm/system/DSMManager$ValueIndex;)V

    .line 339
    sget-object v0, Lcom/sec/dsm/system/DSMManager$ValueIndex;->DMServer:Lcom/sec/dsm/system/DSMManager$ValueIndex;

    invoke-direct {p0, v0}, Lcom/sec/dsm/system/DSMManager;->remove(Lcom/sec/dsm/system/DSMManager$ValueIndex;)V

    .line 340
    return-void
.end method

.method public removeSMSForwarding()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/dsm/system/DSMException;
        }
    .end annotation

    .prologue
    .line 310
    sget-object v0, Lcom/sec/dsm/system/DSMManager$ValueIndex;->SMSForwarding:Lcom/sec/dsm/system/DSMManager$ValueIndex;

    invoke-direct {p0, v0}, Lcom/sec/dsm/system/DSMManager;->remove(Lcom/sec/dsm/system/DSMManager$ValueIndex;)V

    .line 311
    return-void
.end method

.method public removeServerUrl()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/dsm/system/DSMException;
        }
    .end annotation

    .prologue
    .line 322
    sget-object v0, Lcom/sec/dsm/system/DSMManager$ValueIndex;->ServerUrl:Lcom/sec/dsm/system/DSMManager$ValueIndex;

    invoke-direct {p0, v0}, Lcom/sec/dsm/system/DSMManager;->remove(Lcom/sec/dsm/system/DSMManager$ValueIndex;)V

    .line 323
    return-void
.end method
