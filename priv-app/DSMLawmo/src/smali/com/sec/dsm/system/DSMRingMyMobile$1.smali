.class Lcom/sec/dsm/system/DSMRingMyMobile$1;
.super Ljava/lang/Object;
.source "DSMRingMyMobile.java"

# interfaces
.implements Lcom/sec/dsm/system/SlidingTab$OnTriggerListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/dsm/system/DSMRingMyMobile;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/dsm/system/DSMRingMyMobile;


# direct methods
.method constructor <init>(Lcom/sec/dsm/system/DSMRingMyMobile;)V
    .locals 0

    .prologue
    .line 120
    iput-object p1, p0, Lcom/sec/dsm/system/DSMRingMyMobile$1;->this$0:Lcom/sec/dsm/system/DSMRingMyMobile;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGrabbedStateChange(Landroid/view/View;I)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;
    .param p2, "grabbedState"    # I

    .prologue
    .line 140
    return-void
.end method

.method public onTrigger(Landroid/view/View;I)V
    .locals 4
    .param p1, "arg0"    # Landroid/view/View;
    .param p2, "arg1"    # I

    .prologue
    const/4 v2, 0x1

    .line 122
    if-ne v2, p2, :cond_0

    .line 124
    :try_start_0
    new-instance v1, Lcom/sec/dsm/system/DSMManager;

    iget-object v2, p0, Lcom/sec/dsm/system/DSMRingMyMobile$1;->this$0:Lcom/sec/dsm/system/DSMRingMyMobile;

    invoke-direct {v1, v2}, Lcom/sec/dsm/system/DSMManager;-><init>(Landroid/content/Context;)V

    .line 125
    .local v1, "im":Lcom/sec/dsm/system/DSMManager;
    invoke-virtual {v1}, Lcom/sec/dsm/system/DSMManager;->removeDSMRing()V

    .line 126
    iget-object v2, p0, Lcom/sec/dsm/system/DSMRingMyMobile$1;->this$0:Lcom/sec/dsm/system/DSMRingMyMobile;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/sec/dsm/system/DSMRingMyMobile;->setOriginalVolume(I)V

    .line 127
    invoke-static {}, Lcom/sec/dsm/system/DSMRingMyMobile$WakeLock;->release()V
    :try_end_0
    .catch Lcom/sec/dsm/system/DSMException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 135
    .end local v1    # "im":Lcom/sec/dsm/system/DSMManager;
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/sec/dsm/system/DSMRingMyMobile$1;->this$0:Lcom/sec/dsm/system/DSMRingMyMobile;

    const/4 v3, 0x0

    # setter for: Lcom/sec/dsm/system/DSMRingMyMobile;->mRingStarted:Z
    invoke-static {v2, v3}, Lcom/sec/dsm/system/DSMRingMyMobile;->access$002(Lcom/sec/dsm/system/DSMRingMyMobile;Z)Z

    .line 136
    iget-object v2, p0, Lcom/sec/dsm/system/DSMRingMyMobile$1;->this$0:Lcom/sec/dsm/system/DSMRingMyMobile;

    invoke-virtual {v2}, Lcom/sec/dsm/system/DSMRingMyMobile;->finish()V

    .line 137
    return-void

    .line 129
    :catch_0
    move-exception v0

    .line 130
    .local v0, "e":Lcom/sec/dsm/system/DSMException;
    invoke-virtual {v0}, Lcom/sec/dsm/system/DSMException;->printStackTrace()V

    goto :goto_0

    .line 131
    .end local v0    # "e":Lcom/sec/dsm/system/DSMException;
    :catch_1
    move-exception v0

    .line 132
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
