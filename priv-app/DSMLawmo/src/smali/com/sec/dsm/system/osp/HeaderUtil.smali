.class public Lcom/sec/dsm/system/osp/HeaderUtil;
.super Ljava/lang/Object;
.source "HeaderUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/dsm/system/osp/HeaderUtil$1;,
        Lcom/sec/dsm/system/osp/HeaderUtil$KindOfSignKey;
    }
.end annotation


# static fields
.field private static final OAUTH_SIGNATURE_METHOD:Ljava/lang/String; = "HmacSHA1"

.field private static final OAUTH_VERSION:Ljava/lang/String; = "1.0"


# instance fields
.field private appID:Ljava/lang/String;

.field private appSecret:Ljava/lang/String;

.field private context:Landroid/content/Context;

.field private queryParam:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private timeManager:Lcom/sec/dsm/system/osp/ServerTimeManager;

.field private uriPathTimeInitialization:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "appID"    # Ljava/lang/String;
    .param p3, "appSecret"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    :try_start_0
    iput-object p1, p0, Lcom/sec/dsm/system/osp/HeaderUtil;->context:Landroid/content/Context;

    .line 49
    iput-object p2, p0, Lcom/sec/dsm/system/osp/HeaderUtil;->appID:Ljava/lang/String;

    .line 50
    iput-object p3, p0, Lcom/sec/dsm/system/osp/HeaderUtil;->appSecret:Ljava/lang/String;

    .line 52
    new-instance v1, Lcom/sec/dsm/system/osp/PropertyManager;

    invoke-direct {v1, p1}, Lcom/sec/dsm/system/osp/PropertyManager;-><init>(Landroid/content/Context;)V

    .line 54
    .local v1, "propertyManager":Lcom/sec/dsm/system/osp/PropertyManager;
    const-string v2, "uri.path.time.initialization"

    invoke-virtual {v1, v2}, Lcom/sec/dsm/system/osp/PropertyManager;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/dsm/system/osp/HeaderUtil;->uriPathTimeInitialization:Ljava/lang/String;

    .line 55
    new-instance v2, Lcom/sec/dsm/system/osp/ServerTimeManager;

    invoke-direct {v2, p1}, Lcom/sec/dsm/system/osp/ServerTimeManager;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/sec/dsm/system/osp/HeaderUtil;->timeManager:Lcom/sec/dsm/system/osp/ServerTimeManager;

    .line 56
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    iput-object v2, p0, Lcom/sec/dsm/system/osp/HeaderUtil;->queryParam:Ljava/util/HashMap;
    :try_end_0
    .catch Lcom/sec/dsm/system/osp/PropertyException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 64
    return-void

    .line 58
    .end local v1    # "propertyManager":Lcom/sec/dsm/system/osp/PropertyManager;
    :catch_0
    move-exception v0

    .line 59
    .local v0, "e":Lcom/sec/dsm/system/osp/PropertyException;
    invoke-virtual {v0}, Lcom/sec/dsm/system/osp/PropertyException;->printStackTrace()V

    .line 60
    new-instance v2, Ljava/lang/Exception;

    invoke-virtual {v0}, Lcom/sec/dsm/system/osp/PropertyException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v0}, Ljava/lang/Exception;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2

    .line 61
    .end local v0    # "e":Lcom/sec/dsm/system/osp/PropertyException;
    :catch_1
    move-exception v0

    .line 62
    .local v0, "e":Ljava/lang/Exception;
    throw v0
.end method


# virtual methods
.method public addQueryParam(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 299
    if-eqz p1, :cond_0

    .line 300
    iget-object v0, p0, Lcom/sec/dsm/system/osp/HeaderUtil;->queryParam:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 302
    :cond_0
    return-void
.end method

.method public generateAuthorizationHeader(Lcom/sec/dsm/system/osp/RestClient$HttpMethod;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "method"    # Lcom/sec/dsm/system/osp/RestClient$HttpMethod;
    .param p2, "uri"    # Ljava/lang/String;
    .param p3, "body"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 132
    sget-object v0, Lcom/sec/dsm/system/osp/HeaderUtil$KindOfSignKey;->ACCESS_TOKEN:Lcom/sec/dsm/system/osp/HeaderUtil$KindOfSignKey;

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/sec/dsm/system/osp/HeaderUtil;->generateAuthorizationHeader(Lcom/sec/dsm/system/osp/RestClient$HttpMethod;Ljava/lang/String;Ljava/lang/String;Lcom/sec/dsm/system/osp/HeaderUtil$KindOfSignKey;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public generateAuthorizationHeader(Lcom/sec/dsm/system/osp/RestClient$HttpMethod;Ljava/lang/String;Ljava/lang/String;Lcom/sec/dsm/system/osp/HeaderUtil$KindOfSignKey;)Ljava/lang/String;
    .locals 26
    .param p1, "method"    # Lcom/sec/dsm/system/osp/RestClient$HttpMethod;
    .param p2, "uri"    # Ljava/lang/String;
    .param p3, "body"    # Ljava/lang/String;
    .param p4, "kindOfSignKey"    # Lcom/sec/dsm/system/osp/HeaderUtil$KindOfSignKey;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 177
    const/4 v4, 0x0

    .line 180
    .local v4, "authorization":Ljava/lang/String;
    :try_start_0
    const-string v19, "HeaderUtil"

    const-string v22, "generateAuthorizationHeader begin"

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 182
    const/4 v15, 0x0

    .line 185
    .local v15, "queryParamStr":Ljava/lang/String;
    const/16 v19, 0xa

    invoke-static/range {v19 .. v19}, Lcom/sec/dsm/system/osp/AuthUtil;->generateRandomToken(I)Ljava/lang/String;

    move-result-object v12

    .line 189
    .local v12, "oauthNonce":Ljava/lang/String;
    const-wide/16 v20, 0x0

    .line 190
    .local v20, "timestamp":J
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/dsm/system/osp/HeaderUtil;->uriPathTimeInitialization:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, p2

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v19

    const/16 v22, 0x1

    move/from16 v0, v19

    move/from16 v1, v22

    if-ne v0, v1, :cond_1

    .line 191
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v22

    const-wide/16 v24, 0x3e8

    div-long v20, v22, v24

    .line 195
    :goto_0
    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v13

    .line 197
    .local v13, "oauthTimestamp":Ljava/lang/String;
    const-string v14, ""

    .line 198
    .local v14, "oauthToken":Ljava/lang/String;
    const-string v17, ""

    .line 199
    .local v17, "signSecret":Ljava/lang/String;
    const/4 v11, 0x0

    .line 201
    .local v11, "isBase64Encoded":Z
    sget-object v19, Lcom/sec/dsm/system/osp/HeaderUtil$1;->$SwitchMap$com$sec$dsm$system$osp$HeaderUtil$KindOfSignKey:[I

    invoke-virtual/range {p4 .. p4}, Lcom/sec/dsm/system/osp/HeaderUtil$KindOfSignKey;->ordinal()I

    move-result v22

    aget v19, v19, v22

    packed-switch v19, :pswitch_data_0

    .line 227
    :cond_0
    :goto_1
    const/4 v9, 0x0

    .line 228
    .local v9, "i":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/dsm/system/osp/HeaderUtil;->queryParam:Ljava/util/HashMap;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v19

    invoke-interface/range {v19 .. v19}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v10

    .local v10, "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v19

    if-eqz v19, :cond_4

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/util/Map$Entry;

    .line 229
    .local v8, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    if-nez v9, :cond_3

    .line 230
    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v8}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/String;

    move-object/from16 v0, v22

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v22, "="

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-interface {v8}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/String;

    move-object/from16 v0, v22

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    goto :goto_2

    .line 193
    .end local v8    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v9    # "i":I
    .end local v10    # "i$":Ljava/util/Iterator;
    .end local v11    # "isBase64Encoded":Z
    .end local v13    # "oauthTimestamp":Ljava/lang/String;
    .end local v14    # "oauthToken":Ljava/lang/String;
    .end local v17    # "signSecret":Ljava/lang/String;
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/dsm/system/osp/HeaderUtil;->timeManager:Lcom/sec/dsm/system/osp/ServerTimeManager;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/dsm/system/osp/ServerTimeManager;->getServerTime()J

    move-result-wide v20

    goto :goto_0

    .line 203
    .restart local v11    # "isBase64Encoded":Z
    .restart local v13    # "oauthTimestamp":Ljava/lang/String;
    .restart local v14    # "oauthToken":Ljava/lang/String;
    .restart local v17    # "signSecret":Ljava/lang/String;
    :pswitch_0
    const-string v14, ""

    .line 204
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/dsm/system/osp/HeaderUtil;->appSecret:Ljava/lang/String;

    move-object/from16 v17, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 205
    const/4 v11, 0x0

    .line 206
    goto :goto_1

    .line 210
    :pswitch_1
    :try_start_1
    new-instance v6, Lcom/sec/dsm/system/osp/CredentialManager;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/dsm/system/osp/HeaderUtil;->context:Landroid/content/Context;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-direct {v6, v0}, Lcom/sec/dsm/system/osp/CredentialManager;-><init>(Landroid/content/Context;)V

    .line 213
    .local v6, "credentialManager":Lcom/sec/dsm/system/osp/CredentialManager;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/dsm/system/osp/HeaderUtil;->appID:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v6, v0}, Lcom/sec/dsm/system/osp/CredentialManager;->getOauthToken(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 214
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/dsm/system/osp/HeaderUtil;->appID:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v6, v0}, Lcom/sec/dsm/system/osp/CredentialManager;->getOauthTokenSecret(Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v17

    .line 215
    const/4 v11, 0x1

    .line 219
    if-eqz v14, :cond_2

    if-eqz v17, :cond_2

    :try_start_2
    const-string v19, ""

    move-object/from16 v0, v19

    invoke-virtual {v0, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    const/16 v22, 0x1

    move/from16 v0, v19

    move/from16 v1, v22

    if-eq v0, v1, :cond_2

    const-string v19, ""

    move-object/from16 v0, v19

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    const/16 v22, 0x1

    move/from16 v0, v19

    move/from16 v1, v22

    if-ne v0, v1, :cond_0

    .line 222
    :cond_2
    new-instance v19, Ljava/lang/Exception;

    const-string v22, "App wasn\'t signed in"

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v19
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 280
    .end local v6    # "credentialManager":Lcom/sec/dsm/system/osp/CredentialManager;
    .end local v11    # "isBase64Encoded":Z
    .end local v12    # "oauthNonce":Ljava/lang/String;
    .end local v13    # "oauthTimestamp":Ljava/lang/String;
    .end local v14    # "oauthToken":Ljava/lang/String;
    .end local v15    # "queryParamStr":Ljava/lang/String;
    .end local v17    # "signSecret":Ljava/lang/String;
    .end local v20    # "timestamp":J
    :catch_0
    move-exception v7

    .line 281
    .local v7, "e":Ljava/lang/Exception;
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V

    .line 282
    throw v7

    .line 216
    .end local v7    # "e":Ljava/lang/Exception;
    .restart local v11    # "isBase64Encoded":Z
    .restart local v12    # "oauthNonce":Ljava/lang/String;
    .restart local v13    # "oauthTimestamp":Ljava/lang/String;
    .restart local v14    # "oauthToken":Ljava/lang/String;
    .restart local v15    # "queryParamStr":Ljava/lang/String;
    .restart local v17    # "signSecret":Ljava/lang/String;
    .restart local v20    # "timestamp":J
    :catch_1
    move-exception v7

    .line 217
    .restart local v7    # "e":Ljava/lang/Exception;
    :try_start_3
    new-instance v19, Ljava/lang/Exception;

    const-string v22, "Can\'t get the App credential token."

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    invoke-direct {v0, v1, v7}, Ljava/lang/Exception;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v19

    .line 232
    .end local v7    # "e":Ljava/lang/Exception;
    .restart local v8    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    .restart local v9    # "i":I
    .restart local v10    # "i$":Ljava/util/Iterator;
    :cond_3
    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v19

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v22, "&"

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-interface {v8}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/String;

    move-object/from16 v0, v22

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v22, "="

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-interface {v8}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/String;

    move-object/from16 v0, v22

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    goto/16 :goto_2

    .line 236
    .end local v8    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_4
    if-eqz v15, :cond_5

    .line 237
    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v19

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v22, "?"

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    .line 241
    :cond_5
    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    .line 243
    .local v16, "sb":Ljava/lang/StringBuilder;
    const-string v19, "oauth_consumer_key="

    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 244
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/dsm/system/osp/HeaderUtil;->appID:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 245
    const-string v19, ","

    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 246
    const-string v19, "oauth_signature_method="

    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 247
    const-string v19, "HmacSHA1"

    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 248
    const-string v19, ","

    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 249
    const-string v19, "oauth_timestamp="

    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 250
    move-object/from16 v0, v16

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 251
    const-string v19, ","

    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 252
    const-string v19, "oauth_nonce="

    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 253
    move-object/from16 v0, v16

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 254
    const-string v19, ","

    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 255
    const-string v19, "oauth_version="

    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 256
    const-string v19, "1.0"

    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 258
    sget-object v19, Lcom/sec/dsm/system/osp/HeaderUtil$KindOfSignKey;->APP_SECRET:Lcom/sec/dsm/system/osp/HeaderUtil$KindOfSignKey;

    move-object/from16 v0, p4

    move-object/from16 v1, v19

    if-eq v0, v1, :cond_6

    .line 259
    const-string v19, ","

    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 260
    const-string v19, "oauth_token="

    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 261
    move-object/from16 v0, v16

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    .line 264
    :cond_6
    const/4 v5, 0x0

    .line 265
    .local v5, "baseStr":Ljava/lang/String;
    const/16 v18, 0x0

    .line 268
    .local v18, "signature":Ljava/lang/String;
    :try_start_4
    invoke-virtual/range {p1 .. p1}, Lcom/sec/dsm/system/osp/RestClient$HttpMethod;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v19

    move-object/from16 v1, p2

    move-object/from16 v2, v22

    move-object/from16 v3, p3

    invoke-static {v0, v1, v15, v2, v3}, Lcom/sec/dsm/system/osp/AuthUtil;->getSignSourceString(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 269
    new-instance v18, Ljava/lang/String;

    .end local v18    # "signature":Ljava/lang/String;
    move-object/from16 v0, v17

    invoke-static {v0, v5, v11}, Lcom/sec/dsm/system/osp/AuthUtil;->computeSignature(Ljava/lang/String;Ljava/lang/String;Z)[B

    move-result-object v19

    invoke-static/range {v19 .. v19}, Lcom/sec/dsm/system/osp/Base64;->encode([B)[B

    move-result-object v19

    const-string v22, "UTF-8"

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    move-object/from16 v2, v22

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    .line 273
    .restart local v18    # "signature":Ljava/lang/String;
    :try_start_5
    const-string v19, ","

    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 274
    const-string v19, "oauth_signature="

    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 275
    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 277
    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 279
    const-string v19, "HeaderUtil"

    const-string v22, "generateAuthorizationHeader end"

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 285
    return-object v4

    .line 270
    .end local v18    # "signature":Ljava/lang/String;
    :catch_2
    move-exception v7

    .line 271
    .restart local v7    # "e":Ljava/lang/Exception;
    new-instance v19, Ljava/lang/Exception;

    const-string v22, "Can\'t make a signed header."

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    invoke-direct {v0, v1, v7}, Ljava/lang/Exception;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v19
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0

    .line 201
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public generateAuthorizationHeader(Lcom/sec/dsm/system/osp/RestClient$HttpMethod;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;
    .locals 1
    .param p1, "method"    # Lcom/sec/dsm/system/osp/RestClient$HttpMethod;
    .param p2, "uri"    # Ljava/lang/String;
    .param p3, "body"    # Ljava/lang/String;
    .param p4, "needAuth"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 152
    const/4 v0, 0x1

    if-ne p4, v0, :cond_0

    .line 153
    sget-object v0, Lcom/sec/dsm/system/osp/HeaderUtil$KindOfSignKey;->ACCESS_TOKEN:Lcom/sec/dsm/system/osp/HeaderUtil$KindOfSignKey;

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/sec/dsm/system/osp/HeaderUtil;->generateAuthorizationHeader(Lcom/sec/dsm/system/osp/RestClient$HttpMethod;Ljava/lang/String;Ljava/lang/String;Lcom/sec/dsm/system/osp/HeaderUtil$KindOfSignKey;)Ljava/lang/String;

    move-result-object v0

    .line 156
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/sec/dsm/system/osp/HeaderUtil$KindOfSignKey;->APP_SECRET:Lcom/sec/dsm/system/osp/HeaderUtil$KindOfSignKey;

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/sec/dsm/system/osp/HeaderUtil;->generateAuthorizationHeader(Lcom/sec/dsm/system/osp/RestClient$HttpMethod;Ljava/lang/String;Ljava/lang/String;Lcom/sec/dsm/system/osp/HeaderUtil$KindOfSignKey;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public generateAuthorizationHeader(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "uri"    # Ljava/lang/String;
    .param p2, "body"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 79
    sget-object v0, Lcom/sec/dsm/system/osp/RestClient$HttpMethod;->POST:Lcom/sec/dsm/system/osp/RestClient$HttpMethod;

    sget-object v1, Lcom/sec/dsm/system/osp/HeaderUtil$KindOfSignKey;->ACCESS_TOKEN:Lcom/sec/dsm/system/osp/HeaderUtil$KindOfSignKey;

    invoke-virtual {p0, v0, p1, p2, v1}, Lcom/sec/dsm/system/osp/HeaderUtil;->generateAuthorizationHeader(Lcom/sec/dsm/system/osp/RestClient$HttpMethod;Ljava/lang/String;Ljava/lang/String;Lcom/sec/dsm/system/osp/HeaderUtil$KindOfSignKey;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public generateAuthorizationHeader(Ljava/lang/String;Ljava/lang/String;Lcom/sec/dsm/system/osp/HeaderUtil$KindOfSignKey;)Ljava/lang/String;
    .locals 1
    .param p1, "uri"    # Ljava/lang/String;
    .param p2, "body"    # Ljava/lang/String;
    .param p3, "kindOfSignKey"    # Lcom/sec/dsm/system/osp/HeaderUtil$KindOfSignKey;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 114
    sget-object v0, Lcom/sec/dsm/system/osp/RestClient$HttpMethod;->POST:Lcom/sec/dsm/system/osp/RestClient$HttpMethod;

    invoke-virtual {p0, v0, p1, p2, p3}, Lcom/sec/dsm/system/osp/HeaderUtil;->generateAuthorizationHeader(Lcom/sec/dsm/system/osp/RestClient$HttpMethod;Ljava/lang/String;Ljava/lang/String;Lcom/sec/dsm/system/osp/HeaderUtil$KindOfSignKey;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public generateAuthorizationHeader(Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;
    .locals 1
    .param p1, "uri"    # Ljava/lang/String;
    .param p2, "body"    # Ljava/lang/String;
    .param p3, "needAuth"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 97
    sget-object v0, Lcom/sec/dsm/system/osp/RestClient$HttpMethod;->POST:Lcom/sec/dsm/system/osp/RestClient$HttpMethod;

    invoke-virtual {p0, v0, p1, p2, p3}, Lcom/sec/dsm/system/osp/HeaderUtil;->generateAuthorizationHeader(Lcom/sec/dsm/system/osp/RestClient$HttpMethod;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
