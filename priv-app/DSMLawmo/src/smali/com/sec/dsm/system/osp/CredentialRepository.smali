.class public Lcom/sec/dsm/system/osp/CredentialRepository;
.super Ljava/lang/Object;
.source "CredentialRepository.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/dsm/system/osp/CredentialRepository$CredentialColumns;
    }
.end annotation


# static fields
.field private static final CONTENT_URI:Landroid/net/Uri;


# instance fields
.field private context:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const-string v0, "content://com.osp.contentprovider.ospcontentprovider/credential"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/dsm/system/osp/CredentialRepository;->CONTENT_URI:Landroid/net/Uri;

    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    iput-object p1, p0, Lcom/sec/dsm/system/osp/CredentialRepository;->context:Landroid/content/Context;

    .line 47
    return-void
.end method


# virtual methods
.method protected clearCredentials()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/dsm/system/osp/CredentialException;
        }
    .end annotation

    .prologue
    .line 208
    :try_start_0
    iget-object v1, p0, Lcom/sec/dsm/system/osp/CredentialRepository;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/sec/dsm/system/osp/CredentialRepository;->CONTENT_URI:Landroid/net/Uri;

    const-string v3, ""

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/String;

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 212
    return-void

    .line 209
    :catch_0
    move-exception v0

    .line 210
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Lcom/sec/dsm/system/osp/CredentialException;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/sec/dsm/system/osp/CredentialException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method protected containsCredential(Ljava/lang/String;)Z
    .locals 11
    .param p1, "appID"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/dsm/system/osp/CredentialException;
        }
    .end annotation

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 50
    const/4 v8, 0x0

    .line 52
    .local v8, "ret":Z
    const/4 v6, 0x0

    .line 54
    .local v6, "cursor":Landroid/database/Cursor;
    const/4 v0, 0x1

    :try_start_0
    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "appID"

    aput-object v1, v2, v0

    .line 55
    .local v2, "projection":[Ljava/lang/String;
    const-string v3, "appID = ?"

    .line 56
    .local v3, "selection":Ljava/lang/String;
    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    aput-object p1, v4, v0

    .line 58
    .local v4, "selectionArgs":[Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/dsm/system/osp/CredentialRepository;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/dsm/system/osp/CredentialRepository;->CONTENT_URI:Landroid/net/Uri;

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 61
    if-eqz v6, :cond_0

    .line 62
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_2

    move v8, v9

    .line 67
    :cond_0
    :goto_0
    if-eqz v6, :cond_1

    .line 68
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 72
    :cond_1
    return v8

    :cond_2
    move v8, v10

    .line 62
    goto :goto_0

    .line 64
    .end local v2    # "projection":[Ljava/lang/String;
    .end local v3    # "selection":Ljava/lang/String;
    .end local v4    # "selectionArgs":[Ljava/lang/String;
    :catch_0
    move-exception v7

    .line 65
    .local v7, "e":Ljava/lang/Exception;
    :try_start_1
    new-instance v0, Lcom/sec/dsm/system/osp/CredentialException;

    invoke-virtual {v7}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, v7}, Lcom/sec/dsm/system/osp/CredentialException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 67
    .end local v7    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_3

    .line 68
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0
.end method

.method protected getAppSecret(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1, "appID"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/dsm/system/osp/CredentialException;
        }
    .end annotation

    .prologue
    .line 76
    const/4 v0, 0x0

    .line 79
    .local v0, "appSecret":Ljava/lang/String;
    :try_start_0
    const-string v3, "appSecret"

    invoke-virtual {p0, p1, v3}, Lcom/sec/dsm/system/osp/CredentialRepository;->getCredential(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 81
    iget-object v3, p0, Lcom/sec/dsm/system/osp/CredentialRepository;->context:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/dsm/system/osp/AESCrypto;->generateContentKey(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 82
    .local v1, "contentKey":Ljava/lang/String;
    if-eqz v0, :cond_0

    const-string v3, ""

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 83
    invoke-static {v1, v0}, Lcom/sec/dsm/system/osp/AESCrypto;->decryptAES(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 89
    :cond_0
    return-object v0

    .line 85
    .end local v1    # "contentKey":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 86
    .local v2, "e":Ljava/lang/Exception;
    new-instance v3, Lcom/sec/dsm/system/osp/CredentialException;

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4, v2}, Lcom/sec/dsm/system/osp/CredentialException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3
.end method

.method protected getCredential(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 9
    .param p1, "appID"    # Ljava/lang/String;
    .param p2, "column"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/dsm/system/osp/CredentialException;
        }
    .end annotation

    .prologue
    .line 122
    const/4 v6, 0x0

    .line 124
    .local v6, "credData":Ljava/lang/String;
    const/4 v7, 0x0

    .line 126
    .local v7, "cursor":Landroid/database/Cursor;
    const/4 v0, 0x1

    :try_start_0
    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    aput-object p2, v2, v0

    .line 127
    .local v2, "projection":[Ljava/lang/String;
    const-string v3, "appID = ?"

    .line 128
    .local v3, "selection":Ljava/lang/String;
    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    aput-object p1, v4, v0

    .line 130
    .local v4, "selectionArgs":[Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/dsm/system/osp/CredentialRepository;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/dsm/system/osp/CredentialRepository;->CONTENT_URI:Landroid/net/Uri;

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 133
    if-eqz v7, :cond_0

    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 134
    const/4 v0, 0x0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v6

    .line 139
    :cond_0
    if-eqz v7, :cond_1

    .line 140
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 144
    :cond_1
    return-object v6

    .line 136
    .end local v2    # "projection":[Ljava/lang/String;
    .end local v3    # "selection":Ljava/lang/String;
    .end local v4    # "selectionArgs":[Ljava/lang/String;
    :catch_0
    move-exception v8

    .line 137
    .local v8, "e":Ljava/lang/Exception;
    :try_start_1
    new-instance v0, Lcom/sec/dsm/system/osp/CredentialException;

    invoke-virtual {v8}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, v8}, Lcom/sec/dsm/system/osp/CredentialException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 139
    .end local v8    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    if-eqz v7, :cond_2

    .line 140
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
.end method

.method protected getOauthToken(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "appID"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/dsm/system/osp/CredentialException;
        }
    .end annotation

    .prologue
    .line 93
    const/4 v1, 0x0

    .line 96
    .local v1, "oauthToken":Ljava/lang/String;
    :try_start_0
    const-string v2, "oauthToken"

    invoke-virtual {p0, p1, v2}, Lcom/sec/dsm/system/osp/CredentialRepository;->getCredential(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 101
    return-object v1

    .line 97
    :catch_0
    move-exception v0

    .line 98
    .local v0, "e":Ljava/lang/Exception;
    new-instance v2, Lcom/sec/dsm/system/osp/CredentialException;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v0}, Lcom/sec/dsm/system/osp/CredentialException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
.end method

.method protected getOauthTokenSecret(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1, "appID"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/dsm/system/osp/CredentialException;
        }
    .end annotation

    .prologue
    .line 105
    const/4 v2, 0x0

    .line 108
    .local v2, "oauthTokenSecret":Ljava/lang/String;
    :try_start_0
    const-string v3, "oauthTokenSecret"

    invoke-virtual {p0, p1, v3}, Lcom/sec/dsm/system/osp/CredentialRepository;->getCredential(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 110
    iget-object v3, p0, Lcom/sec/dsm/system/osp/CredentialRepository;->context:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/dsm/system/osp/AESCrypto;->generateContentKey(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 111
    .local v0, "contentKey":Ljava/lang/String;
    if-eqz v2, :cond_0

    const-string v3, ""

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 112
    invoke-static {v0, v2}, Lcom/sec/dsm/system/osp/AESCrypto;->decryptAES(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 118
    :cond_0
    return-object v2

    .line 114
    .end local v0    # "contentKey":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 115
    .local v1, "e":Ljava/lang/Exception;
    new-instance v3, Lcom/sec/dsm/system/osp/CredentialException;

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4, v1}, Lcom/sec/dsm/system/osp/CredentialException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3
.end method

.method protected initializeCredential(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p1, "appID"    # Ljava/lang/String;
    .param p2, "appSecret"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/dsm/system/osp/CredentialException;
        }
    .end annotation

    .prologue
    .line 150
    if-eqz p1, :cond_2

    .line 151
    :goto_0
    if-eqz p2, :cond_3

    .line 152
    :goto_1
    :try_start_0
    iget-object v3, p0, Lcom/sec/dsm/system/osp/CredentialRepository;->context:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/dsm/system/osp/AESCrypto;->generateContentKey(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 153
    .local v0, "contentKey":Ljava/lang/String;
    const-string v3, ""

    invoke-virtual {v3, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 154
    invoke-static {v0, p2}, Lcom/sec/dsm/system/osp/AESCrypto;->encryptAES(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    .line 156
    :cond_0
    invoke-virtual {p0, p1}, Lcom/sec/dsm/system/osp/CredentialRepository;->containsCredential(Ljava/lang/String;)Z

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_1

    .line 157
    invoke-virtual {p0, p1}, Lcom/sec/dsm/system/osp/CredentialRepository;->removeCredential(Ljava/lang/String;)V

    .line 159
    :cond_1
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 160
    .local v2, "values":Landroid/content/ContentValues;
    const-string v3, "appID"

    invoke-virtual {v2, v3, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 161
    const-string v3, "appSecret"

    invoke-virtual {v2, v3, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 162
    const-string v3, "oauthToken"

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    const-string v3, "oauthTokenSecret"

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 165
    iget-object v3, p0, Lcom/sec/dsm/system/osp/CredentialRepository;->context:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v4, Lcom/sec/dsm/system/osp/CredentialRepository;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v3, v4, v2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 169
    return-void

    .line 150
    .end local v0    # "contentKey":Ljava/lang/String;
    .end local v2    # "values":Landroid/content/ContentValues;
    :cond_2
    const-string p1, ""

    goto :goto_0

    .line 151
    :cond_3
    const-string p2, ""
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 166
    :catch_0
    move-exception v1

    .line 167
    .local v1, "e":Ljava/lang/Exception;
    new-instance v3, Lcom/sec/dsm/system/osp/CredentialException;

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4, v1}, Lcom/sec/dsm/system/osp/CredentialException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3
.end method

.method protected removeCredential(Ljava/lang/String;)V
    .locals 5
    .param p1, "appID"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/dsm/system/osp/CredentialException;
        }
    .end annotation

    .prologue
    .line 197
    :try_start_0
    const-string v2, "appID = ?"

    .line 198
    .local v2, "where":Ljava/lang/String;
    const/4 v3, 0x1

    new-array v1, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object p1, v1, v3

    .line 200
    .local v1, "selectionArgs":[Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/dsm/system/osp/CredentialRepository;->context:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v4, Lcom/sec/dsm/system/osp/CredentialRepository;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v3, v4, v2, v1}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 204
    return-void

    .line 201
    .end local v1    # "selectionArgs":[Ljava/lang/String;
    .end local v2    # "where":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 202
    .local v0, "e":Ljava/lang/Exception;
    new-instance v3, Lcom/sec/dsm/system/osp/CredentialException;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4, v0}, Lcom/sec/dsm/system/osp/CredentialException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3
.end method

.method protected updateCredential(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p1, "appID"    # Ljava/lang/String;
    .param p2, "token"    # Ljava/lang/String;
    .param p3, "tokenSecret"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/dsm/system/osp/CredentialException;
        }
    .end annotation

    .prologue
    .line 174
    if-eqz p2, :cond_1

    .line 175
    :goto_0
    if-eqz p3, :cond_2

    .line 176
    :goto_1
    :try_start_0
    iget-object v5, p0, Lcom/sec/dsm/system/osp/CredentialRepository;->context:Landroid/content/Context;

    invoke-static {v5}, Lcom/sec/dsm/system/osp/AESCrypto;->generateContentKey(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 177
    .local v0, "contentKey":Ljava/lang/String;
    const-string v5, ""

    invoke-virtual {v5, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 178
    invoke-static {v0, p3}, Lcom/sec/dsm/system/osp/AESCrypto;->encryptAES(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p3

    .line 181
    :cond_0
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 183
    .local v3, "values":Landroid/content/ContentValues;
    const-string v5, "oauthToken"

    invoke-virtual {v3, v5, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 184
    const-string v5, "oauthTokenSecret"

    invoke-virtual {v3, v5, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 186
    const-string v4, "appID = ?"

    .line 187
    .local v4, "where":Ljava/lang/String;
    const/4 v5, 0x1

    new-array v2, v5, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v2, v5

    .line 189
    .local v2, "selectionArgs":[Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/dsm/system/osp/CredentialRepository;->context:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/sec/dsm/system/osp/CredentialRepository;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v5, v6, v3, v4, v2}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 193
    return-void

    .line 174
    .end local v0    # "contentKey":Ljava/lang/String;
    .end local v2    # "selectionArgs":[Ljava/lang/String;
    .end local v3    # "values":Landroid/content/ContentValues;
    .end local v4    # "where":Ljava/lang/String;
    :cond_1
    const-string p2, ""

    goto :goto_0

    .line 175
    :cond_2
    const-string p3, ""
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 190
    :catch_0
    move-exception v1

    .line 191
    .local v1, "e":Ljava/lang/Exception;
    new-instance v5, Lcom/sec/dsm/system/osp/CredentialException;

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6, v1}, Lcom/sec/dsm/system/osp/CredentialException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v5
.end method
