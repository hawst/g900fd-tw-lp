.class public Lcom/sec/dsm/system/DSMContentProvider;
.super Landroid/content/ContentProvider;
.source "DSMContentProvider.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/dsm/system/DSMContentProvider$DatabaseHelper;
    }
.end annotation


# static fields
.field private static final DB_NAME:Ljava/lang/String; = "profile.db"

.field private static final DB_VERSION:I = 0x1


# instance fields
.field private DSMProvider:Lcom/sec/dsm/system/DSMProvider;

.field private openHelper:Lcom/sec/dsm/system/DSMContentProvider$DatabaseHelper;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    .line 24
    new-instance v0, Lcom/sec/dsm/system/DSMProvider;

    invoke-direct {v0}, Lcom/sec/dsm/system/DSMProvider;-><init>()V

    iput-object v0, p0, Lcom/sec/dsm/system/DSMContentProvider;->DSMProvider:Lcom/sec/dsm/system/DSMProvider;

    .line 25
    return-void
.end method

.method static synthetic access$000(Lcom/sec/dsm/system/DSMContentProvider;)Lcom/sec/dsm/system/DSMProvider;
    .locals 1
    .param p0, "x0"    # Lcom/sec/dsm/system/DSMContentProvider;

    .prologue
    .line 12
    iget-object v0, p0, Lcom/sec/dsm/system/DSMContentProvider;->DSMProvider:Lcom/sec/dsm/system/DSMProvider;

    return-object v0
.end method


# virtual methods
.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 8
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "where"    # Ljava/lang/String;
    .param p3, "whereArgs"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/database/SQLException;
        }
    .end annotation

    .prologue
    .line 168
    iget-object v7, p0, Lcom/sec/dsm/system/DSMContentProvider;->openHelper:Lcom/sec/dsm/system/DSMContentProvider$DatabaseHelper;

    monitor-enter v7

    .line 169
    :try_start_0
    iget-object v0, p0, Lcom/sec/dsm/system/DSMContentProvider;->openHelper:Lcom/sec/dsm/system/DSMContentProvider$DatabaseHelper;

    invoke-virtual {v0}, Lcom/sec/dsm/system/DSMContentProvider$DatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 170
    .local v2, "db":Landroid/database/sqlite/SQLiteDatabase;
    iget-object v0, p0, Lcom/sec/dsm/system/DSMContentProvider;->DSMProvider:Lcom/sec/dsm/system/DSMProvider;

    invoke-virtual {p0}, Lcom/sec/dsm/system/DSMContentProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Lcom/sec/dsm/system/DSMProvider;->delete(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    .line 171
    .local v6, "result":I
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 172
    monitor-exit v7

    return v6

    .line 173
    .end local v2    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .end local v6    # "result":I
    :catchall_0
    move-exception v0

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 104
    iget-object v0, p0, Lcom/sec/dsm/system/DSMContentProvider;->DSMProvider:Lcom/sec/dsm/system/DSMProvider;

    invoke-virtual {v0, p1}, Lcom/sec/dsm/system/DSMProvider;->getType(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 5
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 118
    iget-object v3, p0, Lcom/sec/dsm/system/DSMContentProvider;->openHelper:Lcom/sec/dsm/system/DSMContentProvider$DatabaseHelper;

    monitor-enter v3

    .line 119
    :try_start_0
    iget-object v2, p0, Lcom/sec/dsm/system/DSMContentProvider;->openHelper:Lcom/sec/dsm/system/DSMContentProvider$DatabaseHelper;

    invoke-virtual {v2}, Lcom/sec/dsm/system/DSMContentProvider$DatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 120
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    iget-object v2, p0, Lcom/sec/dsm/system/DSMContentProvider;->DSMProvider:Lcom/sec/dsm/system/DSMProvider;

    invoke-virtual {p0}, Lcom/sec/dsm/system/DSMContentProvider;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v2, v4, v0, p1, p2}, Lcom/sec/dsm/system/DSMProvider;->insert(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v1

    .line 121
    .local v1, "result":Landroid/net/Uri;
    monitor-exit v3

    return-object v1

    .line 122
    .end local v0    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .end local v1    # "result":Landroid/net/Uri;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public onCreate()Z
    .locals 2

    .prologue
    .line 56
    new-instance v0, Lcom/sec/dsm/system/DSMContentProvider$DatabaseHelper;

    invoke-virtual {p0}, Lcom/sec/dsm/system/DSMContentProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/dsm/system/DSMContentProvider$DatabaseHelper;-><init>(Lcom/sec/dsm/system/DSMContentProvider;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/dsm/system/DSMContentProvider;->openHelper:Lcom/sec/dsm/system/DSMContentProvider$DatabaseHelper;

    .line 57
    const/4 v0, 0x1

    return v0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 10
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "sortOrder"    # Ljava/lang/String;

    .prologue
    .line 84
    iget-object v9, p0, Lcom/sec/dsm/system/DSMContentProvider;->openHelper:Lcom/sec/dsm/system/DSMContentProvider$DatabaseHelper;

    monitor-enter v9

    .line 85
    :try_start_0
    iget-object v0, p0, Lcom/sec/dsm/system/DSMContentProvider;->openHelper:Lcom/sec/dsm/system/DSMContentProvider$DatabaseHelper;

    invoke-virtual {v0}, Lcom/sec/dsm/system/DSMContentProvider$DatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 86
    .local v2, "db":Landroid/database/sqlite/SQLiteDatabase;
    iget-object v0, p0, Lcom/sec/dsm/system/DSMContentProvider;->DSMProvider:Lcom/sec/dsm/system/DSMProvider;

    invoke-virtual {p0}, Lcom/sec/dsm/system/DSMContentProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    move-object v7, p5

    invoke-virtual/range {v0 .. v7}, Lcom/sec/dsm/system/DSMProvider;->query(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 88
    .local v8, "result":Landroid/database/Cursor;
    monitor-exit v9

    return-object v8

    .line 89
    .end local v2    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .end local v8    # "result":Landroid/database/Cursor;
    :catchall_0
    move-exception v0

    monitor-exit v9
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 9
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;
    .param p3, "where"    # Ljava/lang/String;
    .param p4, "whereArgs"    # [Ljava/lang/String;

    .prologue
    .line 142
    iget-object v8, p0, Lcom/sec/dsm/system/DSMContentProvider;->openHelper:Lcom/sec/dsm/system/DSMContentProvider$DatabaseHelper;

    monitor-enter v8

    .line 143
    :try_start_0
    iget-object v0, p0, Lcom/sec/dsm/system/DSMContentProvider;->openHelper:Lcom/sec/dsm/system/DSMContentProvider$DatabaseHelper;

    invoke-virtual {v0}, Lcom/sec/dsm/system/DSMContentProvider$DatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 144
    .local v2, "db":Landroid/database/sqlite/SQLiteDatabase;
    iget-object v0, p0, Lcom/sec/dsm/system/DSMContentProvider;->DSMProvider:Lcom/sec/dsm/system/DSMProvider;

    invoke-virtual {p0}, Lcom/sec/dsm/system/DSMContentProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    invoke-virtual/range {v0 .. v6}, Lcom/sec/dsm/system/DSMProvider;->update(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v7

    .line 145
    .local v7, "result":I
    monitor-exit v8

    return v7

    .line 146
    .end local v2    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .end local v7    # "result":I
    :catchall_0
    move-exception v0

    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
