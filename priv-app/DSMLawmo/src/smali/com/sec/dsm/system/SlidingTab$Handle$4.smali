.class Lcom/sec/dsm/system/SlidingTab$Handle$4;
.super Landroid/os/Handler;
.source "SlidingTab.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/dsm/system/SlidingTab$Handle;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/dsm/system/SlidingTab$Handle;


# direct methods
.method constructor <init>(Lcom/sec/dsm/system/SlidingTab$Handle;)V
    .locals 0

    .prologue
    .line 529
    iput-object p1, p0, Lcom/sec/dsm/system/SlidingTab$Handle$4;->this$1:Lcom/sec/dsm/system/SlidingTab$Handle;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 7
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/16 v6, 0x64

    const/4 v5, 0x4

    .line 531
    iget-object v4, p0, Lcom/sec/dsm/system/SlidingTab$Handle$4;->this$1:Lcom/sec/dsm/system/SlidingTab$Handle;

    # getter for: Lcom/sec/dsm/system/SlidingTab$Handle;->mLeftArrowImageViews:[Landroid/widget/ImageView;
    invoke-static {v4}, Lcom/sec/dsm/system/SlidingTab$Handle;->access$600(Lcom/sec/dsm/system/SlidingTab$Handle;)[Landroid/widget/ImageView;

    move-result-object v4

    if-nez v4, :cond_0

    .line 562
    :goto_0
    return-void

    .line 534
    :cond_0
    iget v4, p1, Landroid/os/Message;->what:I

    packed-switch v4, :pswitch_data_0

    .line 559
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    goto :goto_0

    .line 536
    :pswitch_0
    const/4 v1, 0x0

    .line 538
    .local v1, "arrowImage":Landroid/widget/ImageView;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-ge v2, v5, :cond_2

    .line 539
    rsub-int/lit8 v4, v2, 0x4

    add-int/lit8 v3, v4, -0x1

    .line 540
    .local v3, "index":I
    iget-object v4, p0, Lcom/sec/dsm/system/SlidingTab$Handle$4;->this$1:Lcom/sec/dsm/system/SlidingTab$Handle;

    # getter for: Lcom/sec/dsm/system/SlidingTab$Handle;->mLeftArrowImageViews:[Landroid/widget/ImageView;
    invoke-static {v4}, Lcom/sec/dsm/system/SlidingTab$Handle;->access$600(Lcom/sec/dsm/system/SlidingTab$Handle;)[Landroid/widget/ImageView;

    move-result-object v4

    aget-object v1, v4, v3

    .line 541
    invoke-virtual {v1}, Landroid/widget/ImageView;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v0

    .line 543
    .local v0, "ani":Landroid/view/animation/Animation;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/view/animation/Animation;->hasStarted()Z

    move-result v4

    if-nez v4, :cond_4

    .line 544
    :cond_1
    add-int/lit8 v4, v2, 0x1

    if-eq v5, v4, :cond_3

    .line 545
    const-wide/16 v4, 0x96

    invoke-virtual {p0, v6, v4, v5}, Lcom/sec/dsm/system/SlidingTab$Handle$4;->sendEmptyMessageDelayed(IJ)Z

    .line 554
    .end local v0    # "ani":Landroid/view/animation/Animation;
    .end local v3    # "index":I
    :cond_2
    :goto_2
    iget-object v4, p0, Lcom/sec/dsm/system/SlidingTab$Handle$4;->this$1:Lcom/sec/dsm/system/SlidingTab$Handle;

    # getter for: Lcom/sec/dsm/system/SlidingTab$Handle;->mTabCircle:Lcom/sec/dsm/system/SlidingTab$Handle$TabCircle;
    invoke-static {v4}, Lcom/sec/dsm/system/SlidingTab$Handle;->access$400(Lcom/sec/dsm/system/SlidingTab$Handle;)Lcom/sec/dsm/system/SlidingTab$Handle$TabCircle;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/sec/dsm/system/SlidingTab$Handle$TabCircle;->setVisibility(I)V

    .line 555
    iget-object v4, p0, Lcom/sec/dsm/system/SlidingTab$Handle$4;->this$1:Lcom/sec/dsm/system/SlidingTab$Handle;

    # invokes: Lcom/sec/dsm/system/SlidingTab$Handle;->makeArrowAnimation()Landroid/view/animation/AlphaAnimation;
    invoke-static {v4}, Lcom/sec/dsm/system/SlidingTab$Handle;->access$500(Lcom/sec/dsm/system/SlidingTab$Handle;)Landroid/view/animation/AlphaAnimation;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0

    .line 548
    .restart local v0    # "ani":Landroid/view/animation/Animation;
    .restart local v3    # "index":I
    :cond_3
    const-wide/16 v4, 0x384

    invoke-virtual {p0, v6, v4, v5}, Lcom/sec/dsm/system/SlidingTab$Handle$4;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_2

    .line 538
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 534
    nop

    :pswitch_data_0
    .packed-switch 0x64
        :pswitch_0
    .end packed-switch
.end method
