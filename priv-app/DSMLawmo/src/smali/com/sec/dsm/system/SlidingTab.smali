.class public Lcom/sec/dsm/system/SlidingTab;
.super Landroid/widget/RelativeLayout;
.source "SlidingTab.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/dsm/system/SlidingTab$Handle;,
        Lcom/sec/dsm/system/SlidingTab$OnTriggerListener;
    }
.end annotation


# instance fields
.field private final VIBRATE_LONG:J

.field private final VIBRATE_SHORT:J

.field private mContext:Landroid/content/Context;

.field private mDisplayMetrics:Landroid/util/DisplayMetrics;

.field private mGrabbedState:I

.field private mIsTablet:Z

.field private mIsTracking:Z

.field private mLeftHandle:Lcom/sec/dsm/system/SlidingTab$Handle;

.field private mOnTriggerListener:Lcom/sec/dsm/system/SlidingTab$OnTriggerListener;

.field private mVibrator:Landroid/os/Vibrator;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 41
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 29
    const-wide/16 v0, 0x1e

    iput-wide v0, p0, Lcom/sec/dsm/system/SlidingTab;->VIBRATE_SHORT:J

    .line 30
    const-wide/16 v0, 0x28

    iput-wide v0, p0, Lcom/sec/dsm/system/SlidingTab;->VIBRATE_LONG:J

    .line 34
    iput v2, p0, Lcom/sec/dsm/system/SlidingTab;->mGrabbedState:I

    .line 38
    iput-boolean v2, p0, Lcom/sec/dsm/system/SlidingTab;->mIsTablet:Z

    .line 42
    iput-object p1, p0, Lcom/sec/dsm/system/SlidingTab;->mContext:Landroid/content/Context;

    .line 43
    invoke-direct {p0}, Lcom/sec/dsm/system/SlidingTab;->init()V

    .line 44
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v2, 0x0

    .line 47
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 29
    const-wide/16 v0, 0x1e

    iput-wide v0, p0, Lcom/sec/dsm/system/SlidingTab;->VIBRATE_SHORT:J

    .line 30
    const-wide/16 v0, 0x28

    iput-wide v0, p0, Lcom/sec/dsm/system/SlidingTab;->VIBRATE_LONG:J

    .line 34
    iput v2, p0, Lcom/sec/dsm/system/SlidingTab;->mGrabbedState:I

    .line 38
    iput-boolean v2, p0, Lcom/sec/dsm/system/SlidingTab;->mIsTablet:Z

    .line 48
    iput-object p1, p0, Lcom/sec/dsm/system/SlidingTab;->mContext:Landroid/content/Context;

    .line 49
    invoke-direct {p0}, Lcom/sec/dsm/system/SlidingTab;->init()V

    .line 50
    return-void
.end method

.method static synthetic access$1000(Lcom/sec/dsm/system/SlidingTab;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/dsm/system/SlidingTab;
    .param p1, "x1"    # I

    .prologue
    .line 28
    invoke-direct {p0, p1}, Lcom/sec/dsm/system/SlidingTab;->setGrabbedState(I)V

    return-void
.end method

.method static synthetic access$1100(Lcom/sec/dsm/system/SlidingTab;)Lcom/sec/dsm/system/SlidingTab$Handle;
    .locals 1
    .param p0, "x0"    # Lcom/sec/dsm/system/SlidingTab;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/dsm/system/SlidingTab;->mLeftHandle:Lcom/sec/dsm/system/SlidingTab$Handle;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/sec/dsm/system/SlidingTab;)Landroid/util/DisplayMetrics;
    .locals 1
    .param p0, "x0"    # Lcom/sec/dsm/system/SlidingTab;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/dsm/system/SlidingTab;->mDisplayMetrics:Landroid/util/DisplayMetrics;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/dsm/system/SlidingTab;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/dsm/system/SlidingTab;

    .prologue
    .line 28
    iget-boolean v0, p0, Lcom/sec/dsm/system/SlidingTab;->mIsTablet:Z

    return v0
.end method

.method static synthetic access$802(Lcom/sec/dsm/system/SlidingTab;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/dsm/system/SlidingTab;
    .param p1, "x1"    # Z

    .prologue
    .line 28
    iput-boolean p1, p0, Lcom/sec/dsm/system/SlidingTab;->mIsTracking:Z

    return p1
.end method

.method static synthetic access$900(Lcom/sec/dsm/system/SlidingTab;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/dsm/system/SlidingTab;
    .param p1, "x1"    # I

    .prologue
    .line 28
    invoke-direct {p0, p1}, Lcom/sec/dsm/system/SlidingTab;->dispatchTriggerEvent(I)V

    return-void
.end method

.method private dispatchTriggerEvent(I)V
    .locals 2
    .param p1, "whichHandle"    # I

    .prologue
    .line 136
    const-wide/16 v0, 0x28

    invoke-direct {p0, v0, v1}, Lcom/sec/dsm/system/SlidingTab;->vibrate(J)V

    .line 137
    iget-object v0, p0, Lcom/sec/dsm/system/SlidingTab;->mOnTriggerListener:Lcom/sec/dsm/system/SlidingTab$OnTriggerListener;

    if-eqz v0, :cond_0

    .line 138
    iget-object v0, p0, Lcom/sec/dsm/system/SlidingTab;->mOnTriggerListener:Lcom/sec/dsm/system/SlidingTab$OnTriggerListener;

    invoke-interface {v0, p0, p1}, Lcom/sec/dsm/system/SlidingTab$OnTriggerListener;->onTrigger(Landroid/view/View;I)V

    .line 140
    :cond_0
    return-void
.end method

.method private init()V
    .locals 3

    .prologue
    .line 53
    invoke-virtual {p0}, Lcom/sec/dsm/system/SlidingTab;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v1, 0x7f050000

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/dsm/system/SlidingTab;->mIsTablet:Z

    .line 54
    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    iput-object v0, p0, Lcom/sec/dsm/system/SlidingTab;->mDisplayMetrics:Landroid/util/DisplayMetrics;

    .line 55
    iget-object v0, p0, Lcom/sec/dsm/system/SlidingTab;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/dsm/system/SlidingTab;->mDisplayMetrics:Landroid/util/DisplayMetrics;

    invoke-virtual {v0, v1}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 56
    new-instance v0, Lcom/sec/dsm/system/SlidingTab$Handle;

    iget-object v1, p0, Lcom/sec/dsm/system/SlidingTab;->mContext:Landroid/content/Context;

    const/4 v2, 0x1

    invoke-direct {v0, p0, v1, p0, v2}, Lcom/sec/dsm/system/SlidingTab$Handle;-><init>(Lcom/sec/dsm/system/SlidingTab;Landroid/content/Context;Landroid/view/ViewGroup;I)V

    iput-object v0, p0, Lcom/sec/dsm/system/SlidingTab;->mLeftHandle:Lcom/sec/dsm/system/SlidingTab$Handle;

    .line 57
    return-void
.end method

.method private setGrabbedState(I)V
    .locals 2
    .param p1, "newState"    # I

    .prologue
    .line 148
    iget v0, p0, Lcom/sec/dsm/system/SlidingTab;->mGrabbedState:I

    if-eq p1, v0, :cond_0

    .line 149
    iput p1, p0, Lcom/sec/dsm/system/SlidingTab;->mGrabbedState:I

    .line 150
    iget-object v0, p0, Lcom/sec/dsm/system/SlidingTab;->mOnTriggerListener:Lcom/sec/dsm/system/SlidingTab$OnTriggerListener;

    if-eqz v0, :cond_0

    .line 151
    iget-object v0, p0, Lcom/sec/dsm/system/SlidingTab;->mOnTriggerListener:Lcom/sec/dsm/system/SlidingTab$OnTriggerListener;

    iget v1, p0, Lcom/sec/dsm/system/SlidingTab;->mGrabbedState:I

    invoke-interface {v0, p0, v1}, Lcom/sec/dsm/system/SlidingTab$OnTriggerListener;->onGrabbedStateChange(Landroid/view/View;I)V

    .line 154
    :cond_0
    return-void
.end method

.method private declared-synchronized vibrate(J)V
    .locals 3
    .param p1, "duration"    # J

    .prologue
    .line 222
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/dsm/system/SlidingTab;->mVibrator:Landroid/os/Vibrator;

    if-nez v0, :cond_0

    .line 223
    invoke-virtual {p0}, Lcom/sec/dsm/system/SlidingTab;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "vibrator"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Vibrator;

    iput-object v0, p0, Lcom/sec/dsm/system/SlidingTab;->mVibrator:Landroid/os/Vibrator;

    .line 226
    :cond_0
    iget-object v0, p0, Lcom/sec/dsm/system/SlidingTab;->mVibrator:Landroid/os/Vibrator;

    invoke-virtual {v0, p1, p2}, Landroid/os/Vibrator;->vibrate(J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 227
    monitor-exit p0

    return-void

    .line 222
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public activeHandle()V
    .locals 2

    .prologue
    .line 909
    iget-object v0, p0, Lcom/sec/dsm/system/SlidingTab;->mLeftHandle:Lcom/sec/dsm/system/SlidingTab$Handle;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/dsm/system/SlidingTab$Handle;->setState(I)V

    .line 910
    return-void
.end method

.method public inactiveHandle()V
    .locals 2

    .prologue
    .line 905
    iget-object v0, p0, Lcom/sec/dsm/system/SlidingTab;->mLeftHandle:Lcom/sec/dsm/system/SlidingTab$Handle;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/sec/dsm/system/SlidingTab$Handle;->setState(I)V

    .line 906
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 8
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v4, 0x1

    .line 170
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 171
    .local v0, "action":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    .line 172
    .local v2, "x":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    .line 173
    .local v3, "y":F
    iget-object v5, p0, Lcom/sec/dsm/system/SlidingTab;->mLeftHandle:Lcom/sec/dsm/system/SlidingTab$Handle;

    invoke-virtual {v5, v2, v3}, Lcom/sec/dsm/system/SlidingTab$Handle;->isHandleSelected(FF)Z

    move-result v1

    .line 174
    .local v1, "leftHit":Z
    iget-boolean v5, p0, Lcom/sec/dsm/system/SlidingTab;->mIsTracking:Z

    if-nez v5, :cond_1

    if-nez v1, :cond_1

    .line 175
    const/4 v4, 0x0

    .line 189
    :cond_0
    :goto_0
    return v4

    .line 177
    :cond_1
    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 179
    :pswitch_0
    const-wide/16 v6, 0x1e

    invoke-direct {p0, v6, v7}, Lcom/sec/dsm/system/SlidingTab;->vibrate(J)V

    .line 180
    iput-boolean v4, p0, Lcom/sec/dsm/system/SlidingTab;->mIsTracking:Z

    .line 181
    if-eqz v1, :cond_0

    .line 182
    iget-object v5, p0, Lcom/sec/dsm/system/SlidingTab;->mLeftHandle:Lcom/sec/dsm/system/SlidingTab$Handle;

    invoke-virtual {v5, v4}, Lcom/sec/dsm/system/SlidingTab$Handle;->setState(I)V

    .line 183
    invoke-direct {p0, v4}, Lcom/sec/dsm/system/SlidingTab;->setGrabbedState(I)V

    goto :goto_0

    .line 177
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method protected onLayout(ZIIII)V
    .locals 1
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 158
    invoke-super/range {p0 .. p5}, Landroid/widget/RelativeLayout;->onLayout(ZIIII)V

    .line 159
    iget-object v0, p0, Lcom/sec/dsm/system/SlidingTab;->mLeftHandle:Lcom/sec/dsm/system/SlidingTab$Handle;

    invoke-virtual {v0}, Lcom/sec/dsm/system/SlidingTab$Handle;->makeLayout()V

    .line 160
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 197
    iget-boolean v3, p0, Lcom/sec/dsm/system/SlidingTab;->mIsTracking:Z

    if-eqz v3, :cond_0

    .line 198
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 199
    .local v0, "action":I
    packed-switch v0, :pswitch_data_0

    .line 216
    .end local v0    # "action":I
    :cond_0
    :pswitch_0
    iget-boolean v3, p0, Lcom/sec/dsm/system/SlidingTab;->mIsTracking:Z

    if-nez v3, :cond_1

    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v3

    if-eqz v3, :cond_2

    :cond_1
    move v1, v2

    :cond_2
    move v2, v1

    :cond_3
    :goto_0
    return v2

    .line 201
    .restart local v0    # "action":I
    :pswitch_1
    iget-object v1, p0, Lcom/sec/dsm/system/SlidingTab;->mLeftHandle:Lcom/sec/dsm/system/SlidingTab$Handle;

    invoke-virtual {v1}, Lcom/sec/dsm/system/SlidingTab$Handle;->getState()I

    move-result v1

    if-ne v1, v2, :cond_3

    .line 202
    iget-object v1, p0, Lcom/sec/dsm/system/SlidingTab;->mLeftHandle:Lcom/sec/dsm/system/SlidingTab$Handle;

    invoke-virtual {v1, p1}, Lcom/sec/dsm/system/SlidingTab$Handle;->processMoveEvent(Landroid/view/MotionEvent;)V

    goto :goto_0

    .line 208
    :pswitch_2
    iput-boolean v1, p0, Lcom/sec/dsm/system/SlidingTab;->mIsTracking:Z

    .line 209
    iget-object v3, p0, Lcom/sec/dsm/system/SlidingTab;->mLeftHandle:Lcom/sec/dsm/system/SlidingTab$Handle;

    invoke-virtual {v3, v1}, Lcom/sec/dsm/system/SlidingTab$Handle;->setState(I)V

    .line 210
    invoke-direct {p0, v1}, Lcom/sec/dsm/system/SlidingTab;->setGrabbedState(I)V

    goto :goto_0

    .line 199
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public reInit()V
    .locals 1

    .prologue
    .line 901
    iget-object v0, p0, Lcom/sec/dsm/system/SlidingTab;->mLeftHandle:Lcom/sec/dsm/system/SlidingTab$Handle;

    # invokes: Lcom/sec/dsm/system/SlidingTab$Handle;->reInit()V
    invoke-static {v0}, Lcom/sec/dsm/system/SlidingTab$Handle;->access$1500(Lcom/sec/dsm/system/SlidingTab$Handle;)V

    .line 902
    return-void
.end method

.method public setLeftHintText(I)V
    .locals 0
    .param p1, "resId"    # I

    .prologue
    .line 117
    return-void
.end method

.method public setLeftTabResources(IIII)V
    .locals 0
    .param p1, "iconId"    # I
    .param p2, "targetId"    # I
    .param p3, "barId"    # I
    .param p4, "tabId"    # I

    .prologue
    .line 113
    return-void
.end method

.method public setOnTriggerListener(Lcom/sec/dsm/system/SlidingTab$OnTriggerListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/dsm/system/SlidingTab$OnTriggerListener;

    .prologue
    .line 128
    iput-object p1, p0, Lcom/sec/dsm/system/SlidingTab;->mOnTriggerListener:Lcom/sec/dsm/system/SlidingTab$OnTriggerListener;

    .line 129
    return-void
.end method

.method public setRightHintText(I)V
    .locals 0
    .param p1, "resId"    # I

    .prologue
    .line 121
    return-void
.end method

.method public setRightTabResources(IIII)V
    .locals 0
    .param p1, "iconId"    # I
    .param p2, "targetId"    # I
    .param p3, "barId"    # I
    .param p4, "tabId"    # I

    .prologue
    .line 109
    return-void
.end method
