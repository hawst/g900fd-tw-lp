.class Lcom/sec/dsm/system/SlidingTab$Handle;
.super Ljava/lang/Object;
.source "SlidingTab.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/dsm/system/SlidingTab;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "Handle"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/dsm/system/SlidingTab$Handle$TargetCircle;,
        Lcom/sec/dsm/system/SlidingTab$Handle$TabCircle;
    }
.end annotation


# static fields
.field private static final MAKE_TARGET_DURATION:I = 0x190

.field private static final RESET_DURATION:I = 0xc8

.field public static final STATE_ACTIVE:I = 0x1

.field public static final STATE_INACTIVE:I = 0x2

.field public static final STATE_NORMAL:I


# instance fields
.field private final ARROW_ANI_DURATION:I

.field final ARROW_COUNT:I

.field private final FIRST_WAVE_DELAY:I

.field private final SECOND_WAVE_DELAY:I

.field private final START_WAVE:I

.field private mArrowContainer:Landroid/widget/LinearLayout;

.field private mArrowImageViews:[Landroid/widget/ImageView;

.field private mCircleColor:I

.field private mContainer:Landroid/widget/FrameLayout;

.field private mContext:Landroid/content/Context;

.field private mCurrentState:I

.field private mHandleType:I

.field private mHandler:Landroid/os/Handler;

.field private mLeftArrowContainer:Landroid/widget/LinearLayout;

.field private mLeftArrowImageViews:[Landroid/widget/ImageView;

.field private mLeftHandler:Landroid/os/Handler;

.field private mParent:Landroid/view/ViewGroup;

.field private mTab:Landroid/widget/ImageView;

.field private mTabCircle:Lcom/sec/dsm/system/SlidingTab$Handle$TabCircle;

.field private mTargetCircle:Lcom/sec/dsm/system/SlidingTab$Handle$TargetCircle;

.field private mTopOffset:I

.field final synthetic this$0:Lcom/sec/dsm/system/SlidingTab;


# direct methods
.method public constructor <init>(Lcom/sec/dsm/system/SlidingTab;Landroid/content/Context;Landroid/view/ViewGroup;I)V
    .locals 3
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "parent"    # Landroid/view/ViewGroup;
    .param p4, "handleType"    # I

    .prologue
    const/4 v2, 0x0

    const/16 v1, 0x384

    .line 264
    iput-object p1, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->this$0:Lcom/sec/dsm/system/SlidingTab;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 244
    iput v1, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->FIRST_WAVE_DELAY:I

    .line 245
    const/16 v0, 0x96

    iput v0, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->SECOND_WAVE_DELAY:I

    .line 246
    const/16 v0, 0x64

    iput v0, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->START_WAVE:I

    .line 247
    iput v1, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->ARROW_ANI_DURATION:I

    .line 253
    const/4 v0, 0x4

    iput v0, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->ARROW_COUNT:I

    .line 255
    iput-object v2, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mLeftArrowContainer:Landroid/widget/LinearLayout;

    .line 256
    iput-object v2, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mLeftArrowImageViews:[Landroid/widget/ImageView;

    .line 260
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mHandleType:I

    .line 493
    new-instance v0, Lcom/sec/dsm/system/SlidingTab$Handle$3;

    invoke-direct {v0, p0}, Lcom/sec/dsm/system/SlidingTab$Handle$3;-><init>(Lcom/sec/dsm/system/SlidingTab$Handle;)V

    iput-object v0, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mHandler:Landroid/os/Handler;

    .line 529
    new-instance v0, Lcom/sec/dsm/system/SlidingTab$Handle$4;

    invoke-direct {v0, p0}, Lcom/sec/dsm/system/SlidingTab$Handle$4;-><init>(Lcom/sec/dsm/system/SlidingTab$Handle;)V

    iput-object v0, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mLeftHandler:Landroid/os/Handler;

    .line 265
    iput-object p2, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mContext:Landroid/content/Context;

    .line 266
    iput p4, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mHandleType:I

    .line 267
    iput-object p3, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mParent:Landroid/view/ViewGroup;

    .line 268
    invoke-direct {p0}, Lcom/sec/dsm/system/SlidingTab$Handle;->init()V

    .line 269
    return-void
.end method

.method static synthetic access$000(Lcom/sec/dsm/system/SlidingTab$Handle;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/dsm/system/SlidingTab$Handle;

    .prologue
    .line 240
    invoke-direct {p0}, Lcom/sec/dsm/system/SlidingTab$Handle;->showArrow()V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/dsm/system/SlidingTab$Handle;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/dsm/system/SlidingTab$Handle;

    .prologue
    .line 240
    invoke-direct {p0}, Lcom/sec/dsm/system/SlidingTab$Handle;->hideArrow()V

    return-void
.end method

.method static synthetic access$1300(Lcom/sec/dsm/system/SlidingTab$Handle;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/dsm/system/SlidingTab$Handle;

    .prologue
    .line 240
    iget v0, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mCircleColor:I

    return v0
.end method

.method static synthetic access$1400(Lcom/sec/dsm/system/SlidingTab$Handle;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/dsm/system/SlidingTab$Handle;

    .prologue
    .line 240
    iget v0, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mTopOffset:I

    return v0
.end method

.method static synthetic access$1500(Lcom/sec/dsm/system/SlidingTab$Handle;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/dsm/system/SlidingTab$Handle;

    .prologue
    .line 240
    invoke-direct {p0}, Lcom/sec/dsm/system/SlidingTab$Handle;->reInit()V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/dsm/system/SlidingTab$Handle;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/dsm/system/SlidingTab$Handle;

    .prologue
    .line 240
    iget v0, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mHandleType:I

    return v0
.end method

.method static synthetic access$300(Lcom/sec/dsm/system/SlidingTab$Handle;)[Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/dsm/system/SlidingTab$Handle;

    .prologue
    .line 240
    iget-object v0, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mArrowImageViews:[Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/dsm/system/SlidingTab$Handle;)Lcom/sec/dsm/system/SlidingTab$Handle$TabCircle;
    .locals 1
    .param p0, "x0"    # Lcom/sec/dsm/system/SlidingTab$Handle;

    .prologue
    .line 240
    iget-object v0, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mTabCircle:Lcom/sec/dsm/system/SlidingTab$Handle$TabCircle;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/dsm/system/SlidingTab$Handle;)Landroid/view/animation/AlphaAnimation;
    .locals 1
    .param p0, "x0"    # Lcom/sec/dsm/system/SlidingTab$Handle;

    .prologue
    .line 240
    invoke-direct {p0}, Lcom/sec/dsm/system/SlidingTab$Handle;->makeArrowAnimation()Landroid/view/animation/AlphaAnimation;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/dsm/system/SlidingTab$Handle;)[Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/dsm/system/SlidingTab$Handle;

    .prologue
    .line 240
    iget-object v0, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mLeftArrowImageViews:[Landroid/widget/ImageView;

    return-object v0
.end method

.method private hideArrow()V
    .locals 7

    .prologue
    const/16 v6, 0x64

    const/4 v5, 0x4

    .line 476
    iget-object v4, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mHandler:Landroid/os/Handler;

    invoke-virtual {v4, v6}, Landroid/os/Handler;->removeMessages(I)V

    .line 477
    iget-object v4, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mLeftHandler:Landroid/os/Handler;

    invoke-virtual {v4, v6}, Landroid/os/Handler;->removeMessages(I)V

    .line 478
    iget-object v4, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mLeftArrowContainer:Landroid/widget/LinearLayout;

    if-eqz v4, :cond_0

    .line 479
    iget-object v4, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mLeftArrowContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 480
    iget-object v4, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mLeftArrowImageViews:[Landroid/widget/ImageView;

    if-eqz v4, :cond_0

    .line 481
    iget-object v0, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mLeftArrowImageViews:[Landroid/widget/ImageView;

    .local v0, "arr$":[Landroid/widget/ImageView;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v2, v0, v1

    .line 482
    .local v2, "imageView":Landroid/widget/ImageView;
    invoke-virtual {v2}, Landroid/widget/ImageView;->clearAnimation()V

    .line 481
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 487
    .end local v0    # "arr$":[Landroid/widget/ImageView;
    .end local v1    # "i$":I
    .end local v2    # "imageView":Landroid/widget/ImageView;
    .end local v3    # "len$":I
    :cond_0
    iget-object v4, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mArrowContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 488
    iget-object v0, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mArrowImageViews:[Landroid/widget/ImageView;

    .restart local v0    # "arr$":[Landroid/widget/ImageView;
    array-length v3, v0

    .restart local v3    # "len$":I
    const/4 v1, 0x0

    .restart local v1    # "i$":I
    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v2, v0, v1

    .line 489
    .restart local v2    # "imageView":Landroid/widget/ImageView;
    invoke-virtual {v2}, Landroid/widget/ImageView;->clearAnimation()V

    .line 488
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 491
    .end local v2    # "imageView":Landroid/widget/ImageView;
    :cond_1
    return-void
.end method

.method private init()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v6, -0x2

    .line 272
    iput v8, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mCurrentState:I

    .line 274
    new-instance v4, Lcom/sec/dsm/system/SlidingTab$Handle$1;

    iget-object v5, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mContext:Landroid/content/Context;

    invoke-direct {v4, p0, v5}, Lcom/sec/dsm/system/SlidingTab$Handle$1;-><init>(Lcom/sec/dsm/system/SlidingTab$Handle;Landroid/content/Context;)V

    iput-object v4, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mContainer:Landroid/widget/FrameLayout;

    .line 287
    iget-object v4, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    .line 288
    .local v3, "metrics":Landroid/util/DisplayMetrics;
    iget v4, v3, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v5, v3, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v4

    mul-int/lit8 v4, v4, 0x4

    div-int/lit8 v1, v4, 0x5

    .line 289
    .local v1, "diameter":I
    iget v4, v3, Landroid/util/DisplayMetrics;->widthPixels:I

    const/16 v5, 0x1e0

    if-ne v4, v5, :cond_0

    iget v4, v3, Landroid/util/DisplayMetrics;->heightPixels:I

    const/16 v5, 0x168

    if-ne v4, v5, :cond_0

    .line 290
    mul-int/lit8 v4, v1, 0x6

    div-int/lit8 v1, v4, 0x7

    .line 292
    :cond_0
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 293
    .local v0, "containerParams":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v4, 0xc

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 294
    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    const/16 v4, 0x11

    invoke-direct {v2, v6, v6, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    .line 297
    .local v2, "layoutParams":Landroid/widget/FrameLayout$LayoutParams;
    new-instance v4, Landroid/widget/ImageView;

    iget-object v5, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mContext:Landroid/content/Context;

    invoke-direct {v4, v5}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mTab:Landroid/widget/ImageView;

    .line 298
    iget-object v4, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mContainer:Landroid/widget/FrameLayout;

    iget-object v5, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mTab:Landroid/widget/ImageView;

    invoke-virtual {v4, v5, v2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 299
    const/16 v4, 0x9

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 300
    iget-object v4, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mTab:Landroid/widget/ImageView;

    const v5, 0x7f020009

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 301
    iget-object v4, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mTab:Landroid/widget/ImageView;

    iget-object v5, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f060003

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 302
    iget-object v4, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mTab:Landroid/widget/ImageView;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setFocusableInTouchMode(Z)V

    .line 303
    const v4, -0x57e5f1

    iput v4, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mCircleColor:I

    .line 305
    iget-object v4, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mContainer:Landroid/widget/FrameLayout;

    invoke-direct {p0, v4, v1}, Lcom/sec/dsm/system/SlidingTab$Handle;->makeAlarmLeftArrowViews(Landroid/widget/FrameLayout;I)V

    .line 307
    new-instance v4, Lcom/sec/dsm/system/SlidingTab$Handle$TabCircle;

    iget-object v5, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mContext:Landroid/content/Context;

    int-to-float v6, v1

    const/high16 v7, 0x40c00000    # 6.0f

    div-float/2addr v6, v7

    invoke-direct {v4, p0, v5, v6}, Lcom/sec/dsm/system/SlidingTab$Handle$TabCircle;-><init>(Lcom/sec/dsm/system/SlidingTab$Handle;Landroid/content/Context;F)V

    iput-object v4, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mTabCircle:Lcom/sec/dsm/system/SlidingTab$Handle$TabCircle;

    .line 308
    iget-object v4, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mTabCircle:Lcom/sec/dsm/system/SlidingTab$Handle$TabCircle;

    invoke-virtual {v4, v8}, Lcom/sec/dsm/system/SlidingTab$Handle$TabCircle;->setVisibility(I)V

    .line 309
    iget-object v4, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mContainer:Landroid/widget/FrameLayout;

    iget-object v5, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mTabCircle:Lcom/sec/dsm/system/SlidingTab$Handle$TabCircle;

    invoke-virtual {v4, v5, v2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 312
    iget-object v4, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mContainer:Landroid/widget/FrameLayout;

    invoke-direct {p0, v4, v1}, Lcom/sec/dsm/system/SlidingTab$Handle;->makeArrowViews(Landroid/widget/FrameLayout;I)V

    .line 313
    new-instance v4, Lcom/sec/dsm/system/SlidingTab$Handle$TargetCircle;

    iget-object v5, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mContext:Landroid/content/Context;

    int-to-float v6, v1

    const/high16 v7, 0x40000000    # 2.0f

    div-float/2addr v6, v7

    invoke-direct {v4, p0, v5, v6}, Lcom/sec/dsm/system/SlidingTab$Handle$TargetCircle;-><init>(Lcom/sec/dsm/system/SlidingTab$Handle;Landroid/content/Context;F)V

    iput-object v4, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mTargetCircle:Lcom/sec/dsm/system/SlidingTab$Handle$TargetCircle;

    .line 314
    iget-object v4, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mTargetCircle:Lcom/sec/dsm/system/SlidingTab$Handle$TargetCircle;

    const/4 v5, 0x4

    invoke-virtual {v4, v5}, Lcom/sec/dsm/system/SlidingTab$Handle$TargetCircle;->setVisibility(I)V

    .line 315
    iget-object v4, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mContainer:Landroid/widget/FrameLayout;

    iget-object v5, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mTargetCircle:Lcom/sec/dsm/system/SlidingTab$Handle$TargetCircle;

    invoke-virtual {v4, v5, v2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 316
    iget-object v4, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mContainer:Landroid/widget/FrameLayout;

    invoke-virtual {v4, v0}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 317
    iget-object v4, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mParent:Landroid/view/ViewGroup;

    iget-object v5, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mContainer:Landroid/widget/FrameLayout;

    invoke-virtual {v4, v5}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 318
    return-void
.end method

.method private makeAlarmLeftArrowViews(Landroid/widget/FrameLayout;I)V
    .locals 12
    .param p1, "container"    # Landroid/widget/FrameLayout;
    .param p2, "diameter"    # I

    .prologue
    .line 376
    new-instance v8, Landroid/widget/LinearLayout;

    iget-object v9, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mContext:Landroid/content/Context;

    invoke-direct {v8, v9}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object v8, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mLeftArrowContainer:Landroid/widget/LinearLayout;

    .line 377
    const/high16 v0, 0x41a00000    # 20.0f

    .line 378
    .local v0, "ALPHA_GAP":F
    const/4 v8, 0x4

    new-array v8, v8, [Landroid/widget/ImageView;

    iput-object v8, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mLeftArrowImageViews:[Landroid/widget/ImageView;

    .line 379
    const/4 v2, 0x0

    .line 380
    .local v2, "arrowResId":I
    const/4 v4, 0x0

    .line 382
    .local v4, "layoutGravity":I
    new-instance v7, Landroid/widget/LinearLayout$LayoutParams;

    mul-int/lit8 v8, p2, 0x3

    div-int/lit8 v8, v8, 0x3f

    mul-int/lit8 v9, p2, 0x3

    div-int/lit8 v9, v9, 0x50

    const/4 v10, 0x0

    invoke-direct {v7, v8, v9, v10}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    .line 385
    .local v7, "paddingLp":Landroid/widget/LinearLayout$LayoutParams;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    const/4 v8, 0x4

    if-ge v3, v8, :cond_1

    .line 386
    iget-object v8, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mLeftArrowImageViews:[Landroid/widget/ImageView;

    new-instance v9, Landroid/widget/ImageView;

    iget-object v10, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mContext:Landroid/content/Context;

    invoke-direct {v9, v10}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    aput-object v9, v8, v3

    .line 387
    const/4 v1, 0x0

    .line 389
    .local v1, "alpha":I
    const/high16 v8, 0x437f0000    # 255.0f

    const/high16 v9, 0x3f800000    # 1.0f

    const/high16 v10, 0x41a00000    # 20.0f

    int-to-float v11, v3

    mul-float/2addr v10, v11

    const/high16 v11, 0x42c80000    # 100.0f

    div-float/2addr v10, v11

    sub-float/2addr v9, v10

    mul-float/2addr v8, v9

    float-to-int v1, v8

    .line 390
    const/high16 v2, 0x7f020000

    .line 391
    const/16 v4, 0x13

    .line 393
    iget-object v8, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mLeftArrowContainer:Landroid/widget/LinearLayout;

    iget-object v9, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mLeftArrowImageViews:[Landroid/widget/ImageView;

    aget-object v9, v9, v3

    invoke-virtual {v8, v9}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 396
    const/4 v8, 0x3

    if-eq v3, v8, :cond_0

    .line 397
    iget-object v8, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mLeftArrowContainer:Landroid/widget/LinearLayout;

    new-instance v9, Landroid/view/View;

    iget-object v10, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mContext:Landroid/content/Context;

    invoke-direct {v9, v10}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    invoke-virtual {v8, v9, v7}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 400
    :cond_0
    iget-object v8, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mLeftArrowImageViews:[Landroid/widget/ImageView;

    aget-object v8, v8, v3

    invoke-virtual {v8, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 401
    iget-object v8, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mLeftArrowImageViews:[Landroid/widget/ImageView;

    aget-object v8, v8, v3

    invoke-virtual {v8, v1}, Landroid/widget/ImageView;->setAlpha(I)V

    .line 385
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 403
    .end local v1    # "alpha":I
    :cond_1
    new-instance v5, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v8, -0x2

    const/4 v9, -0x2

    invoke-direct {v5, v8, v9, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    .line 405
    .local v5, "layoutParams":Landroid/widget/FrameLayout$LayoutParams;
    iget-object v8, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mLeftArrowContainer:Landroid/widget/LinearLayout;

    invoke-virtual {p1, v8, v5}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 406
    iget-object v8, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mLeftArrowContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v8}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    check-cast v6, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 408
    .local v6, "lp":Landroid/view/ViewGroup$MarginLayoutParams;
    mul-int/lit8 v8, p2, 0xb

    div-int/lit16 v8, v8, 0xb4

    div-int/lit8 v8, v8, 0x4

    iput v8, v6, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 409
    iget-object v8, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mLeftArrowContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v8, v6}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 410
    return-void
.end method

.method private makeArrowAnimation()Landroid/view/animation/AlphaAnimation;
    .locals 4

    .prologue
    .line 566
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/high16 v1, 0x3f800000    # 1.0f

    const v2, 0x3f19999a    # 0.6f

    invoke-direct {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 567
    .local v0, "alphaAni":Landroid/view/animation/AlphaAnimation;
    const-wide/16 v2, 0x384

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 568
    iget-object v1, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mContext:Landroid/content/Context;

    const v2, 0x10a0005

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadInterpolator(Landroid/content/Context;I)Landroid/view/animation/Interpolator;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/animation/AlphaAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 570
    return-object v0
.end method

.method private makeArrowViews(Landroid/widget/FrameLayout;I)V
    .locals 14
    .param p1, "container"    # Landroid/widget/FrameLayout;
    .param p2, "diameter"    # I

    .prologue
    .line 413
    new-instance v10, Landroid/widget/LinearLayout;

    iget-object v11, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mContext:Landroid/content/Context;

    invoke-direct {v10, v11}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object v10, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mArrowContainer:Landroid/widget/LinearLayout;

    .line 414
    const/high16 v0, 0x41a00000    # 20.0f

    .line 415
    .local v0, "ALPHA_GAP":F
    const/4 v10, 0x4

    new-array v10, v10, [Landroid/widget/ImageView;

    iput-object v10, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mArrowImageViews:[Landroid/widget/ImageView;

    .line 416
    const/4 v2, 0x0

    .line 417
    .local v2, "arrowResId":I
    const/4 v4, 0x0

    .line 418
    .local v4, "layoutGravity":I
    const/4 v9, 0x0

    .line 419
    .local v9, "paddingLpValue":I
    const/4 v7, 0x0

    .line 421
    .local v7, "marginValue":I
    const/16 v9, 0x3f

    .line 422
    mul-int/lit8 v10, p2, 0xb

    div-int/lit16 v10, v10, 0xb4

    div-int/lit8 v7, v10, 0x4

    .line 424
    new-instance v8, Landroid/widget/LinearLayout$LayoutParams;

    mul-int/lit8 v10, p2, 0x3

    div-int/2addr v10, v9

    mul-int/lit8 v11, p2, 0x3

    div-int/lit8 v11, v11, 0x50

    const/4 v12, 0x0

    invoke-direct {v8, v10, v11, v12}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    .line 427
    .local v8, "paddingLp":Landroid/widget/LinearLayout$LayoutParams;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    const/4 v10, 0x4

    if-ge v3, v10, :cond_2

    .line 428
    iget-object v10, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mArrowImageViews:[Landroid/widget/ImageView;

    new-instance v11, Landroid/widget/ImageView;

    iget-object v12, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mContext:Landroid/content/Context;

    invoke-direct {v11, v12}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    aput-object v11, v10, v3

    .line 429
    const/4 v1, 0x0

    .line 431
    .local v1, "alpha":I
    iget v10, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mHandleType:I

    const/4 v11, 0x1

    if-ne v10, v11, :cond_1

    .line 432
    const v2, 0x7f020001

    .line 433
    const/16 v4, 0x15

    .line 434
    const/high16 v10, 0x437f0000    # 255.0f

    const/high16 v11, 0x3f800000    # 1.0f

    const/high16 v12, 0x41a00000    # 20.0f

    rsub-int/lit8 v13, v3, 0x4

    add-int/lit8 v13, v13, -0x1

    int-to-float v13, v13

    mul-float/2addr v12, v13

    const/high16 v13, 0x42c80000    # 100.0f

    div-float/2addr v12, v13

    sub-float/2addr v11, v12

    mul-float/2addr v10, v11

    float-to-int v1, v10

    .line 437
    if-eqz v3, :cond_0

    .line 438
    iget-object v10, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mArrowContainer:Landroid/widget/LinearLayout;

    new-instance v11, Landroid/view/View;

    iget-object v12, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mContext:Landroid/content/Context;

    invoke-direct {v11, v12}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    invoke-virtual {v10, v11, v8}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 440
    :cond_0
    iget-object v10, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mArrowContainer:Landroid/widget/LinearLayout;

    iget-object v11, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mArrowImageViews:[Landroid/widget/ImageView;

    aget-object v11, v11, v3

    invoke-virtual {v10, v11}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 442
    :cond_1
    iget-object v10, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mArrowImageViews:[Landroid/widget/ImageView;

    aget-object v10, v10, v3

    invoke-virtual {v10, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 443
    iget-object v10, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mArrowImageViews:[Landroid/widget/ImageView;

    aget-object v10, v10, v3

    invoke-virtual {v10, v1}, Landroid/widget/ImageView;->setAlpha(I)V

    .line 427
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 446
    .end local v1    # "alpha":I
    :cond_2
    new-instance v5, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v10, -0x2

    const/4 v11, -0x2

    invoke-direct {v5, v10, v11, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    .line 449
    .local v5, "layoutParams":Landroid/widget/FrameLayout$LayoutParams;
    iget-object v10, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mArrowContainer:Landroid/widget/LinearLayout;

    invoke-virtual {p1, v10, v5}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 451
    iget-object v10, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mArrowContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v10}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    check-cast v6, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 454
    .local v6, "lp":Landroid/view/ViewGroup$MarginLayoutParams;
    iget v10, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mHandleType:I

    const/4 v11, 0x1

    if-ne v10, v11, :cond_3

    .line 455
    iput v7, v6, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    .line 458
    :cond_3
    iget-object v10, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mArrowContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v10, v6}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 459
    return-void
.end method

.method private reInit()V
    .locals 8

    .prologue
    const/4 v7, -0x2

    const/4 v6, 0x0

    .line 323
    iget-object v4, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mParent:Landroid/view/ViewGroup;

    iget-object v5, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mContainer:Landroid/widget/FrameLayout;

    invoke-virtual {v4, v5}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 324
    iput-object v6, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mTab:Landroid/widget/ImageView;

    .line 325
    iput-object v6, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mContainer:Landroid/widget/FrameLayout;

    .line 326
    iput-object v6, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mLeftArrowContainer:Landroid/widget/LinearLayout;

    .line 327
    iput-object v6, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mLeftArrowImageViews:[Landroid/widget/ImageView;

    .line 328
    new-instance v4, Lcom/sec/dsm/system/SlidingTab$Handle$2;

    iget-object v5, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mContext:Landroid/content/Context;

    invoke-direct {v4, p0, v5}, Lcom/sec/dsm/system/SlidingTab$Handle$2;-><init>(Lcom/sec/dsm/system/SlidingTab$Handle;Landroid/content/Context;)V

    iput-object v4, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mContainer:Landroid/widget/FrameLayout;

    .line 342
    iget-object v4, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    .line 343
    .local v3, "metrics":Landroid/util/DisplayMetrics;
    iget v4, v3, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v5, v3, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v4

    mul-int/lit8 v4, v4, 0x4

    div-int/lit8 v1, v4, 0x5

    .line 344
    .local v1, "diameter":I
    iget v4, v3, Landroid/util/DisplayMetrics;->widthPixels:I

    const/16 v5, 0x1e0

    if-ne v4, v5, :cond_0

    iget v4, v3, Landroid/util/DisplayMetrics;->heightPixels:I

    const/16 v5, 0x168

    if-ne v4, v5, :cond_0

    .line 345
    mul-int/lit8 v4, v1, 0x6

    div-int/lit8 v1, v4, 0x7

    .line 347
    :cond_0
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 348
    .local v0, "containerParams":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v4, 0xc

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 349
    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    const/16 v4, 0x11

    invoke-direct {v2, v7, v7, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    .line 352
    .local v2, "layoutParams":Landroid/widget/FrameLayout$LayoutParams;
    new-instance v4, Landroid/widget/ImageView;

    iget-object v5, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mContext:Landroid/content/Context;

    invoke-direct {v4, v5}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mTab:Landroid/widget/ImageView;

    .line 353
    iget-object v4, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mContainer:Landroid/widget/FrameLayout;

    iget-object v5, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mTab:Landroid/widget/ImageView;

    invoke-virtual {v4, v5, v2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 354
    const/16 v4, 0x9

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 355
    iget-object v4, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mTab:Landroid/widget/ImageView;

    const v5, 0x7f020009

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 356
    iget-object v4, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mTab:Landroid/widget/ImageView;

    iget-object v5, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f060003

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 357
    iget-object v4, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mTab:Landroid/widget/ImageView;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setFocusableInTouchMode(Z)V

    .line 360
    iget-object v4, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mContainer:Landroid/widget/FrameLayout;

    invoke-direct {p0, v4, v1}, Lcom/sec/dsm/system/SlidingTab$Handle;->makeAlarmLeftArrowViews(Landroid/widget/FrameLayout;I)V

    .line 362
    new-instance v4, Lcom/sec/dsm/system/SlidingTab$Handle$TabCircle;

    iget-object v5, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mContext:Landroid/content/Context;

    int-to-float v6, v1

    const/high16 v7, 0x40c00000    # 6.0f

    div-float/2addr v6, v7

    invoke-direct {v4, p0, v5, v6}, Lcom/sec/dsm/system/SlidingTab$Handle$TabCircle;-><init>(Lcom/sec/dsm/system/SlidingTab$Handle;Landroid/content/Context;F)V

    iput-object v4, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mTabCircle:Lcom/sec/dsm/system/SlidingTab$Handle$TabCircle;

    .line 363
    iget-object v4, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mTabCircle:Lcom/sec/dsm/system/SlidingTab$Handle$TabCircle;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/sec/dsm/system/SlidingTab$Handle$TabCircle;->setVisibility(I)V

    .line 364
    iget-object v4, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mContainer:Landroid/widget/FrameLayout;

    iget-object v5, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mTabCircle:Lcom/sec/dsm/system/SlidingTab$Handle$TabCircle;

    invoke-virtual {v4, v5, v2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 367
    iget-object v4, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mContainer:Landroid/widget/FrameLayout;

    invoke-direct {p0, v4, v1}, Lcom/sec/dsm/system/SlidingTab$Handle;->makeArrowViews(Landroid/widget/FrameLayout;I)V

    .line 368
    new-instance v4, Lcom/sec/dsm/system/SlidingTab$Handle$TargetCircle;

    iget-object v5, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mContext:Landroid/content/Context;

    int-to-float v6, v1

    const/high16 v7, 0x40000000    # 2.0f

    div-float/2addr v6, v7

    invoke-direct {v4, p0, v5, v6}, Lcom/sec/dsm/system/SlidingTab$Handle$TargetCircle;-><init>(Lcom/sec/dsm/system/SlidingTab$Handle;Landroid/content/Context;F)V

    iput-object v4, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mTargetCircle:Lcom/sec/dsm/system/SlidingTab$Handle$TargetCircle;

    .line 369
    iget-object v4, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mTargetCircle:Lcom/sec/dsm/system/SlidingTab$Handle$TargetCircle;

    const/4 v5, 0x4

    invoke-virtual {v4, v5}, Lcom/sec/dsm/system/SlidingTab$Handle$TargetCircle;->setVisibility(I)V

    .line 370
    iget-object v4, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mContainer:Landroid/widget/FrameLayout;

    iget-object v5, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mTargetCircle:Lcom/sec/dsm/system/SlidingTab$Handle$TargetCircle;

    invoke-virtual {v4, v5, v2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 371
    iget-object v4, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mContainer:Landroid/widget/FrameLayout;

    invoke-virtual {v4, v0}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 372
    iget-object v4, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mParent:Landroid/view/ViewGroup;

    iget-object v5, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mContainer:Landroid/widget/FrameLayout;

    invoke-virtual {v4, v5}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 373
    return-void
.end method

.method private reset()V
    .locals 9

    .prologue
    const/high16 v4, 0x40000000    # 2.0f

    const/high16 v2, 0x3f000000    # 0.5f

    const/high16 v1, 0x3f800000    # 1.0f

    .line 673
    new-instance v8, Landroid/view/animation/AnimationSet;

    const/4 v3, 0x1

    invoke-direct {v8, v3}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 674
    .local v8, "aniSet":Landroid/view/animation/AnimationSet;
    new-instance v0, Landroid/view/animation/ScaleAnimation;

    iget-object v3, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mTargetCircle:Lcom/sec/dsm/system/SlidingTab$Handle$TargetCircle;

    invoke-virtual {v3}, Lcom/sec/dsm/system/SlidingTab$Handle$TargetCircle;->getWidth()I

    move-result v3

    int-to-float v3, v3

    div-float v5, v3, v4

    iget-object v3, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mTargetCircle:Lcom/sec/dsm/system/SlidingTab$Handle$TargetCircle;

    invoke-virtual {v3}, Lcom/sec/dsm/system/SlidingTab$Handle$TargetCircle;->getWidth()I

    move-result v3

    int-to-float v3, v3

    div-float v6, v3, v4

    move v3, v1

    move v4, v2

    invoke-direct/range {v0 .. v6}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFFF)V

    .line 676
    .local v0, "scaleAni":Landroid/view/animation/ScaleAnimation;
    invoke-virtual {v8, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 677
    new-instance v7, Landroid/view/animation/AlphaAnimation;

    const/4 v2, 0x0

    invoke-direct {v7, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 678
    .local v7, "alphaAni":Landroid/view/animation/AlphaAnimation;
    invoke-virtual {v8, v7}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 679
    const-wide/16 v2, 0x190

    invoke-virtual {v8, v2, v3}, Landroid/view/animation/AnimationSet;->setDuration(J)V

    .line 680
    iget-object v1, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mContext:Landroid/content/Context;

    const v2, 0x10a0005

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadInterpolator(Landroid/content/Context;I)Landroid/view/animation/Interpolator;

    move-result-object v1

    invoke-virtual {v8, v1}, Landroid/view/animation/AnimationSet;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 683
    iget-object v1, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mTargetCircle:Lcom/sec/dsm/system/SlidingTab$Handle$TargetCircle;

    invoke-virtual {v1}, Lcom/sec/dsm/system/SlidingTab$Handle$TargetCircle;->clearAnimation()V

    .line 684
    iget-object v1, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mTargetCircle:Lcom/sec/dsm/system/SlidingTab$Handle$TargetCircle;

    invoke-virtual {v1, v8}, Lcom/sec/dsm/system/SlidingTab$Handle$TargetCircle;->startAnimation(Landroid/view/animation/Animation;)V

    .line 685
    iget-object v1, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mTargetCircle:Lcom/sec/dsm/system/SlidingTab$Handle$TargetCircle;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lcom/sec/dsm/system/SlidingTab$Handle$TargetCircle;->setVisibility(I)V

    .line 687
    iget-object v1, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mTabCircle:Lcom/sec/dsm/system/SlidingTab$Handle$TabCircle;

    const/16 v2, 0xff

    invoke-virtual {v1, v2}, Lcom/sec/dsm/system/SlidingTab$Handle$TabCircle;->setAlpha(I)V

    .line 688
    iget-object v1, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mTabCircle:Lcom/sec/dsm/system/SlidingTab$Handle$TabCircle;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/dsm/system/SlidingTab$Handle$TabCircle;->setVisibility(I)V

    .line 689
    invoke-direct {p0}, Lcom/sec/dsm/system/SlidingTab$Handle;->showArrow()V

    .line 690
    return-void
.end method

.method private setDisable()V
    .locals 4

    .prologue
    .line 696
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/high16 v1, 0x3f800000    # 1.0f

    const v2, 0x3eb33333    # 0.35f

    invoke-direct {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 697
    .local v0, "alphaAni":Landroid/view/animation/AlphaAnimation;
    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 698
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/animation/AlphaAnimation;->setFillAfter(Z)V

    .line 699
    iget-object v1, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mTab:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 700
    iget-object v1, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mTabCircle:Lcom/sec/dsm/system/SlidingTab$Handle$TabCircle;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lcom/sec/dsm/system/SlidingTab$Handle$TabCircle;->setVisibility(I)V

    .line 701
    invoke-direct {p0}, Lcom/sec/dsm/system/SlidingTab$Handle;->hideArrow()V

    .line 702
    return-void
.end method

.method private setEnable()V
    .locals 4

    .prologue
    .line 708
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const v1, 0x3eb33333    # 0.35f

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 709
    .local v0, "tabAni":Landroid/view/animation/AlphaAnimation;
    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 710
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/animation/AlphaAnimation;->setFillAfter(Z)V

    .line 711
    iget-object v1, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mTab:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 712
    iget-object v1, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mTabCircle:Lcom/sec/dsm/system/SlidingTab$Handle$TabCircle;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/dsm/system/SlidingTab$Handle$TabCircle;->setVisibility(I)V

    .line 713
    invoke-direct {p0}, Lcom/sec/dsm/system/SlidingTab$Handle;->showArrow()V

    .line 714
    return-void
.end method

.method private showArrow()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/16 v1, 0x64

    .line 462
    iget-object v0, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mContainer:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->isShown()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 463
    iget-object v0, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mLeftArrowContainer:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    .line 464
    iget-object v0, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mLeftArrowContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 465
    :cond_0
    iget-object v0, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mArrowContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 466
    iget-object v0, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 467
    iget-object v0, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 469
    :cond_1
    iget-object v0, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mLeftHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-nez v0, :cond_2

    .line 470
    iget-object v0, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mLeftHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 473
    :cond_2
    return-void
.end method

.method private showTarget()V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/high16 v4, 0x40000000    # 2.0f

    const/high16 v1, 0x3f000000    # 0.5f

    const/high16 v2, 0x3f800000    # 1.0f

    .line 655
    new-instance v8, Landroid/view/animation/AnimationSet;

    invoke-direct {v8, v9}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 656
    .local v8, "aniSet":Landroid/view/animation/AnimationSet;
    new-instance v0, Landroid/view/animation/ScaleAnimation;

    iget-object v3, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mTargetCircle:Lcom/sec/dsm/system/SlidingTab$Handle$TargetCircle;

    invoke-virtual {v3}, Lcom/sec/dsm/system/SlidingTab$Handle$TargetCircle;->getWidth()I

    move-result v3

    int-to-float v3, v3

    div-float v5, v3, v4

    iget-object v3, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mTargetCircle:Lcom/sec/dsm/system/SlidingTab$Handle$TargetCircle;

    invoke-virtual {v3}, Lcom/sec/dsm/system/SlidingTab$Handle$TargetCircle;->getWidth()I

    move-result v3

    int-to-float v3, v3

    div-float v6, v3, v4

    move v3, v1

    move v4, v2

    invoke-direct/range {v0 .. v6}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFFF)V

    .line 658
    .local v0, "scaleAni":Landroid/view/animation/ScaleAnimation;
    invoke-virtual {v8, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 659
    new-instance v7, Landroid/view/animation/AlphaAnimation;

    const/4 v1, 0x0

    invoke-direct {v7, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 660
    .local v7, "alphaAni":Landroid/view/animation/AlphaAnimation;
    invoke-virtual {v8, v7}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 661
    const-wide/16 v2, 0x190

    invoke-virtual {v8, v2, v3}, Landroid/view/animation/AnimationSet;->setDuration(J)V

    .line 662
    iget-object v1, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mContext:Landroid/content/Context;

    const v2, 0x10a0006

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadInterpolator(Landroid/content/Context;I)Landroid/view/animation/Interpolator;

    move-result-object v1

    invoke-virtual {v8, v1}, Landroid/view/animation/AnimationSet;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 664
    invoke-virtual {v8, v9}, Landroid/view/animation/AnimationSet;->setFillAfter(Z)V

    .line 665
    iget-object v1, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mTargetCircle:Lcom/sec/dsm/system/SlidingTab$Handle$TargetCircle;

    invoke-virtual {v1, v8}, Lcom/sec/dsm/system/SlidingTab$Handle$TargetCircle;->startAnimation(Landroid/view/animation/Animation;)V

    .line 666
    iget-object v1, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mTargetCircle:Lcom/sec/dsm/system/SlidingTab$Handle$TargetCircle;

    const/16 v2, 0x61

    invoke-virtual {v1, v2}, Lcom/sec/dsm/system/SlidingTab$Handle$TargetCircle;->setAlpha(I)V

    .line 667
    iget-object v1, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mTargetCircle:Lcom/sec/dsm/system/SlidingTab$Handle$TargetCircle;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/dsm/system/SlidingTab$Handle$TargetCircle;->setVisibility(I)V

    .line 668
    return-void
.end method


# virtual methods
.method public getState()I
    .locals 1

    .prologue
    .line 644
    iget v0, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mCurrentState:I

    return v0
.end method

.method public isHandleSelected(FF)Z
    .locals 2
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 752
    iget-object v0, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->this$0:Lcom/sec/dsm/system/SlidingTab;

    # getter for: Lcom/sec/dsm/system/SlidingTab;->mDisplayMetrics:Landroid/util/DisplayMetrics;
    invoke-static {v1}, Lcom/sec/dsm/system/SlidingTab;->access$1200(Lcom/sec/dsm/system/SlidingTab;)Landroid/util/DisplayMetrics;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 755
    iget-object v0, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->this$0:Lcom/sec/dsm/system/SlidingTab;

    # getter for: Lcom/sec/dsm/system/SlidingTab;->mDisplayMetrics:Landroid/util/DisplayMetrics;
    invoke-static {v0}, Lcom/sec/dsm/system/SlidingTab;->access$1200(Lcom/sec/dsm/system/SlidingTab;)Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    iget-object v1, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->this$0:Lcom/sec/dsm/system/SlidingTab;

    invoke-virtual {v1}, Lcom/sec/dsm/system/SlidingTab;->getMeasuredHeight()I

    move-result v1

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mTopOffset:I

    .line 756
    iget v0, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mTopOffset:I

    if-gez v0, :cond_0

    .line 757
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mTopOffset:I

    .line 759
    :cond_0
    iget-object v0, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mTabCircle:Lcom/sec/dsm/system/SlidingTab$Handle$TabCircle;

    invoke-virtual {v0, p1, p2}, Lcom/sec/dsm/system/SlidingTab$Handle$TabCircle;->isInCircle(FF)Z

    move-result v0

    return v0
.end method

.method public makeLayout()V
    .locals 7

    .prologue
    const/16 v6, 0x1e0

    const/16 v5, 0x168

    .line 577
    iget-object v3, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->this$0:Lcom/sec/dsm/system/SlidingTab;

    invoke-virtual {v3}, Lcom/sec/dsm/system/SlidingTab;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    .line 578
    .local v2, "metrics":Landroid/util/DisplayMetrics;
    const/4 v1, 0x0

    .line 579
    .local v1, "leftHandleTop":I
    const/4 v0, 0x0

    .line 580
    .local v0, "leftHandleLeft":I
    iget-object v3, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->this$0:Lcom/sec/dsm/system/SlidingTab;

    invoke-virtual {v3}, Lcom/sec/dsm/system/SlidingTab;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget v3, v3, Landroid/content/res/Configuration;->orientation:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_2

    .line 581
    iget-object v3, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mContainer:Landroid/widget/FrameLayout;

    invoke-virtual {v3}, Landroid/widget/FrameLayout;->getLeft()I

    move-result v3

    iget-object v4, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mContainer:Landroid/widget/FrameLayout;

    invoke-virtual {v4}, Landroid/widget/FrameLayout;->getWidth()I

    move-result v4

    div-int/lit8 v4, v4, 0x8

    add-int v0, v3, v4

    .line 582
    iget-object v3, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->this$0:Lcom/sec/dsm/system/SlidingTab;

    # getter for: Lcom/sec/dsm/system/SlidingTab;->mIsTablet:Z
    invoke-static {v3}, Lcom/sec/dsm/system/SlidingTab;->access$700(Lcom/sec/dsm/system/SlidingTab;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 583
    iget-object v3, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mContainer:Landroid/widget/FrameLayout;

    invoke-virtual {v3}, Landroid/widget/FrameLayout;->getTop()I

    move-result v3

    iget-object v4, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mContainer:Landroid/widget/FrameLayout;

    invoke-virtual {v4}, Landroid/widget/FrameLayout;->getTop()I

    move-result v4

    div-int/lit8 v4, v4, 0xa

    add-int v1, v3, v4

    .line 610
    :goto_0
    iget-object v3, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mContainer:Landroid/widget/FrameLayout;

    iget-object v4, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mContainer:Landroid/widget/FrameLayout;

    invoke-virtual {v4}, Landroid/widget/FrameLayout;->getWidth()I

    move-result v4

    add-int/2addr v4, v0

    iget-object v5, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mContainer:Landroid/widget/FrameLayout;

    invoke-virtual {v5}, Landroid/widget/FrameLayout;->getHeight()I

    move-result v5

    add-int/2addr v5, v1

    invoke-virtual {v3, v0, v1, v4, v5}, Landroid/widget/FrameLayout;->layout(IIII)V

    .line 613
    return-void

    .line 585
    :cond_0
    iget v3, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    const/16 v4, 0xf0

    if-ne v3, v4, :cond_1

    iget v3, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    const/16 v4, 0x140

    if-ne v3, v4, :cond_1

    .line 587
    iget-object v3, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mContainer:Landroid/widget/FrameLayout;

    invoke-virtual {v3}, Landroid/widget/FrameLayout;->getTop()I

    move-result v3

    iget-object v4, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mContainer:Landroid/widget/FrameLayout;

    invoke-virtual {v4}, Landroid/widget/FrameLayout;->getTop()I

    move-result v4

    div-int/lit8 v4, v4, 0xa

    add-int/2addr v3, v4

    add-int/lit8 v1, v3, 0x2f

    goto :goto_0

    .line 589
    :cond_1
    iget-object v3, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mContainer:Landroid/widget/FrameLayout;

    invoke-virtual {v3}, Landroid/widget/FrameLayout;->getTop()I

    move-result v3

    iget-object v4, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mContainer:Landroid/widget/FrameLayout;

    invoke-virtual {v4}, Landroid/widget/FrameLayout;->getTop()I

    move-result v4

    div-int/lit8 v4, v4, 0xf

    add-int/2addr v3, v4

    add-int/lit8 v1, v3, 0x19

    goto :goto_0

    .line 593
    :cond_2
    iget v3, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    if-ne v3, v6, :cond_3

    iget v3, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    if-ne v3, v5, :cond_3

    .line 595
    iget-object v3, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mContainer:Landroid/widget/FrameLayout;

    invoke-virtual {v3}, Landroid/widget/FrameLayout;->getLeft()I

    move-result v3

    iget-object v4, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mContainer:Landroid/widget/FrameLayout;

    invoke-virtual {v4}, Landroid/widget/FrameLayout;->getWidth()I

    move-result v4

    mul-int/lit8 v4, v4, 0x9

    div-int/lit8 v4, v4, 0x10

    add-int/2addr v3, v4

    add-int/lit8 v0, v3, -0xf

    .line 599
    :goto_1
    iget v3, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    if-ne v3, v6, :cond_4

    iget v3, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    if-ne v3, v5, :cond_4

    .line 601
    iget-object v3, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mContainer:Landroid/widget/FrameLayout;

    invoke-virtual {v3}, Landroid/widget/FrameLayout;->getTop()I

    move-result v3

    iget-object v4, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mContainer:Landroid/widget/FrameLayout;

    invoke-virtual {v4}, Landroid/widget/FrameLayout;->getHeight()I

    move-result v4

    div-int/lit8 v4, v4, 0xa

    add-int/2addr v3, v4

    add-int/lit8 v1, v3, 0xd

    goto :goto_0

    .line 597
    :cond_3
    iget-object v3, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mContainer:Landroid/widget/FrameLayout;

    invoke-virtual {v3}, Landroid/widget/FrameLayout;->getLeft()I

    move-result v3

    iget-object v4, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mContainer:Landroid/widget/FrameLayout;

    invoke-virtual {v4}, Landroid/widget/FrameLayout;->getWidth()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    add-int v0, v3, v4

    goto :goto_1

    .line 603
    :cond_4
    iget-object v3, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->this$0:Lcom/sec/dsm/system/SlidingTab;

    # getter for: Lcom/sec/dsm/system/SlidingTab;->mIsTablet:Z
    invoke-static {v3}, Lcom/sec/dsm/system/SlidingTab;->access$700(Lcom/sec/dsm/system/SlidingTab;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 604
    iget-object v3, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mContainer:Landroid/widget/FrameLayout;

    invoke-virtual {v3}, Landroid/widget/FrameLayout;->getTop()I

    move-result v3

    iget-object v4, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mContainer:Landroid/widget/FrameLayout;

    invoke-virtual {v4}, Landroid/widget/FrameLayout;->getHeight()I

    move-result v4

    mul-int/lit8 v4, v4, 0x2

    div-int/lit8 v4, v4, 0xa

    add-int v1, v3, v4

    goto/16 :goto_0

    .line 606
    :cond_5
    iget-object v3, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mContainer:Landroid/widget/FrameLayout;

    invoke-virtual {v3}, Landroid/widget/FrameLayout;->getTop()I

    move-result v3

    iget-object v4, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mContainer:Landroid/widget/FrameLayout;

    invoke-virtual {v4}, Landroid/widget/FrameLayout;->getHeight()I

    move-result v4

    div-int/lit8 v4, v4, 0xa

    add-int v1, v3, v4

    goto/16 :goto_0
.end method

.method public processMoveEvent(Landroid/view/MotionEvent;)V
    .locals 8
    .param p1, "motion"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v6, 0x0

    .line 721
    iget v4, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mCurrentState:I

    const/4 v5, 0x1

    if-ne v4, v5, :cond_0

    .line 722
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    .line 723
    .local v2, "x":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    .line 725
    .local v3, "y":F
    iget-object v4, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mTargetCircle:Lcom/sec/dsm/system/SlidingTab$Handle$TargetCircle;

    invoke-virtual {v4, v2, v3}, Lcom/sec/dsm/system/SlidingTab$Handle$TargetCircle;->isThresholdReached(FF)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 726
    iget-object v4, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mTargetCircle:Lcom/sec/dsm/system/SlidingTab$Handle$TargetCircle;

    invoke-virtual {v4}, Lcom/sec/dsm/system/SlidingTab$Handle$TargetCircle;->clearAnimation()V

    .line 727
    iget-object v4, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mTabCircle:Lcom/sec/dsm/system/SlidingTab$Handle$TabCircle;

    invoke-virtual {v4}, Lcom/sec/dsm/system/SlidingTab$Handle$TabCircle;->clearAnimation()V

    .line 728
    iget-object v4, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mTargetCircle:Lcom/sec/dsm/system/SlidingTab$Handle$TargetCircle;

    const/16 v5, 0xff

    invoke-virtual {v4, v5}, Lcom/sec/dsm/system/SlidingTab$Handle$TargetCircle;->setAlpha(I)V

    .line 729
    iget-object v4, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mTabCircle:Lcom/sec/dsm/system/SlidingTab$Handle$TabCircle;

    invoke-virtual {v4, v6}, Lcom/sec/dsm/system/SlidingTab$Handle$TabCircle;->setAlpha(I)V

    .line 730
    iget-object v4, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->this$0:Lcom/sec/dsm/system/SlidingTab;

    # setter for: Lcom/sec/dsm/system/SlidingTab;->mIsTracking:Z
    invoke-static {v4, v6}, Lcom/sec/dsm/system/SlidingTab;->access$802(Lcom/sec/dsm/system/SlidingTab;Z)Z

    .line 731
    iget-object v4, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->this$0:Lcom/sec/dsm/system/SlidingTab;

    iget v5, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mHandleType:I

    # invokes: Lcom/sec/dsm/system/SlidingTab;->dispatchTriggerEvent(I)V
    invoke-static {v4, v5}, Lcom/sec/dsm/system/SlidingTab;->access$900(Lcom/sec/dsm/system/SlidingTab;I)V

    .line 732
    iget-object v4, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->this$0:Lcom/sec/dsm/system/SlidingTab;

    # invokes: Lcom/sec/dsm/system/SlidingTab;->setGrabbedState(I)V
    invoke-static {v4, v6}, Lcom/sec/dsm/system/SlidingTab;->access$1000(Lcom/sec/dsm/system/SlidingTab;I)V

    .line 733
    iget-object v4, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mHandler:Landroid/os/Handler;

    new-instance v5, Lcom/sec/dsm/system/SlidingTab$Handle$5;

    invoke-direct {v5, p0}, Lcom/sec/dsm/system/SlidingTab$Handle$5;-><init>(Lcom/sec/dsm/system/SlidingTab$Handle;)V

    const-wide/16 v6, 0x1f4

    invoke-virtual {v4, v5, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 750
    .end local v2    # "x":F
    .end local v3    # "y":F
    :cond_0
    :goto_0
    return-void

    .line 739
    .restart local v2    # "x":F
    .restart local v3    # "y":F
    :cond_1
    iget-object v4, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mTabCircle:Lcom/sec/dsm/system/SlidingTab$Handle$TabCircle;

    invoke-virtual {v4, v2, v3}, Lcom/sec/dsm/system/SlidingTab$Handle$TabCircle;->isInCircle(FF)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 740
    invoke-direct {p0}, Lcom/sec/dsm/system/SlidingTab$Handle;->showArrow()V

    .line 744
    :goto_1
    iget-object v4, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mTargetCircle:Lcom/sec/dsm/system/SlidingTab$Handle$TargetCircle;

    invoke-virtual {v4, v2, v3}, Lcom/sec/dsm/system/SlidingTab$Handle$TargetCircle;->getTargetProximityRatio(FF)D

    move-result-wide v0

    .line 745
    .local v0, "ratio":D
    iget-object v4, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mTabCircle:Lcom/sec/dsm/system/SlidingTab$Handle$TabCircle;

    const-wide v6, 0x406fe00000000000L    # 255.0

    mul-double/2addr v6, v0

    double-to-int v5, v6

    rsub-int v5, v5, 0xff

    invoke-virtual {v4, v5}, Lcom/sec/dsm/system/SlidingTab$Handle$TabCircle;->setAlpha(I)V

    .line 746
    iget-object v4, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mTargetCircle:Lcom/sec/dsm/system/SlidingTab$Handle$TargetCircle;

    const-wide v6, 0x4063c00000000000L    # 158.0

    mul-double/2addr v6, v0

    double-to-int v5, v6

    add-int/lit8 v5, v5, 0x61

    invoke-virtual {v4, v5}, Lcom/sec/dsm/system/SlidingTab$Handle$TargetCircle;->setAlpha(I)V

    .line 747
    iget-object v4, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mTabCircle:Lcom/sec/dsm/system/SlidingTab$Handle$TabCircle;

    invoke-virtual {v4}, Lcom/sec/dsm/system/SlidingTab$Handle$TabCircle;->invalidate()V

    goto :goto_0

    .line 742
    .end local v0    # "ratio":D
    :cond_2
    invoke-direct {p0}, Lcom/sec/dsm/system/SlidingTab$Handle;->hideArrow()V

    goto :goto_1
.end method

.method public setState(I)V
    .locals 2
    .param p1, "newState"    # I

    .prologue
    .line 619
    iget v0, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mCurrentState:I

    .line 620
    .local v0, "preState":I
    iput p1, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mCurrentState:I

    .line 621
    iget v1, p0, Lcom/sec/dsm/system/SlidingTab$Handle;->mCurrentState:I

    packed-switch v1, :pswitch_data_0

    .line 638
    :cond_0
    :goto_0
    return-void

    .line 623
    :pswitch_0
    invoke-direct {p0}, Lcom/sec/dsm/system/SlidingTab$Handle;->showTarget()V

    goto :goto_0

    .line 626
    :pswitch_1
    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 627
    invoke-direct {p0}, Lcom/sec/dsm/system/SlidingTab$Handle;->reset()V

    goto :goto_0

    .line 628
    :cond_1
    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 629
    invoke-direct {p0}, Lcom/sec/dsm/system/SlidingTab$Handle;->setEnable()V

    goto :goto_0

    .line 633
    :pswitch_2
    invoke-direct {p0}, Lcom/sec/dsm/system/SlidingTab$Handle;->setDisable()V

    goto :goto_0

    .line 621
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
