.class public Lcom/sec/dsm/system/osp/CredentialRepository$CredentialColumns;
.super Ljava/lang/Object;
.source "CredentialRepository.java"

# interfaces
.implements Landroid/provider/BaseColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/dsm/system/osp/CredentialRepository;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "CredentialColumns"
.end annotation


# static fields
.field public static final APP_ID:Ljava/lang/String; = "appID"

.field public static final APP_SECRET:Ljava/lang/String; = "appSecret"

.field public static final OAUTH_TOKEN:Ljava/lang/String; = "oauthToken"

.field public static final OAUTH_TOKEN_SECRET:Ljava/lang/String; = "oauthTokenSecret"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
