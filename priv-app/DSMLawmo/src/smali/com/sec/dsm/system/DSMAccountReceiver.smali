.class public Lcom/sec/dsm/system/DSMAccountReceiver;
.super Landroid/content/BroadcastReceiver;
.source "DSMAccountReceiver.java"


# static fields
.field public static final SAMSUNG_ACCOUNT_REGISTRATION_CANCELED:Ljava/lang/String; = "android.intent.action.REGISTRATION_CANCELED"

.field public static final SAMSUNG_ACCOUNT_REGISTRATION_COMPLETED:Ljava/lang/String; = "android.intent.action.REGISTRATION_COMPLETED"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 14
    const-string v1, "android.accounts.LOGIN_ACCOUNTS_CHANGED"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "android.intent.action.REGISTRATION_COMPLETED"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "android.intent.action.REGISTRATION_CANCELED"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 17
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/dsm/system/DSMAccountService;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 18
    .local v0, "i":Landroid/content/Intent;
    invoke-virtual {p1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 19
    const-string v1, "account changed || boot completed"

    invoke-static {v1}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 21
    .end local v0    # "i":Landroid/content/Intent;
    :cond_1
    return-void
.end method
