.class public Lcom/sec/dsm/system/osp/ServerTimeManager;
.super Ljava/lang/Object;
.source "ServerTimeManager.java"


# instance fields
.field private baseAppId:Ljava/lang/String;

.field private baseAppSecret:Ljava/lang/String;

.field private context:Landroid/content/Context;

.field private offsetTime:J

.field private uriPathTimeInitialization:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/dsm/system/osp/ServerTimeException;
        }
    .end annotation

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/sec/dsm/system/osp/ServerTimeManager;->offsetTime:J

    .line 45
    :try_start_0
    iput-object p1, p0, Lcom/sec/dsm/system/osp/ServerTimeManager;->context:Landroid/content/Context;

    .line 47
    new-instance v1, Lcom/sec/dsm/system/osp/PropertyManager;

    invoke-direct {v1, p1}, Lcom/sec/dsm/system/osp/PropertyManager;-><init>(Landroid/content/Context;)V

    .line 49
    .local v1, "propertyManager":Lcom/sec/dsm/system/osp/PropertyManager;
    const-string v2, "uri.path.time.initialization"

    invoke-virtual {v1, v2}, Lcom/sec/dsm/system/osp/PropertyManager;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/dsm/system/osp/ServerTimeManager;->uriPathTimeInitialization:Ljava/lang/String;

    .line 51
    const-string v2, "base.app.id"

    invoke-virtual {v1, v2}, Lcom/sec/dsm/system/osp/PropertyManager;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/dsm/system/osp/ServerTimeManager;->baseAppId:Ljava/lang/String;

    .line 52
    const-string v2, "base.app.secret"

    invoke-virtual {v1, v2}, Lcom/sec/dsm/system/osp/PropertyManager;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/dsm/system/osp/ServerTimeManager;->baseAppSecret:Ljava/lang/String;
    :try_end_0
    .catch Lcom/sec/dsm/system/osp/PropertyException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 58
    return-void

    .line 53
    .end local v1    # "propertyManager":Lcom/sec/dsm/system/osp/PropertyManager;
    :catch_0
    move-exception v0

    .line 54
    .local v0, "e":Lcom/sec/dsm/system/osp/PropertyException;
    new-instance v2, Lcom/sec/dsm/system/osp/ServerTimeException;

    const-string v3, "Can\'t read the properties."

    invoke-direct {v2, v3, v0}, Lcom/sec/dsm/system/osp/ServerTimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2

    .line 55
    .end local v0    # "e":Lcom/sec/dsm/system/osp/PropertyException;
    :catch_1
    move-exception v0

    .line 56
    .local v0, "e":Ljava/lang/Exception;
    new-instance v2, Lcom/sec/dsm/system/osp/ServerTimeException;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v0}, Lcom/sec/dsm/system/osp/ServerTimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
.end method

.method private requestServerTime()V
    .locals 14
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/dsm/system/osp/ServerTimeException;
        }
    .end annotation

    .prologue
    .line 114
    const/4 v4, 0x0

    .line 117
    .local v4, "httpEntity":Lorg/apache/http/HttpEntity;
    :try_start_0
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "http://"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/sec/dsm/system/osp/ServerTimeManager;->context:Landroid/content/Context;

    invoke-static {v11}, Lcom/sec/dsm/system/osp/ServerURIUtil;->getServerURI(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/sec/dsm/system/osp/ServerTimeManager;->uriPathTimeInitialization:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 120
    .local v5, "requestURI":Ljava/lang/String;
    new-instance v0, Lcom/sec/dsm/system/osp/RestClient;

    iget-object v10, p0, Lcom/sec/dsm/system/osp/ServerTimeManager;->context:Landroid/content/Context;

    iget-object v11, p0, Lcom/sec/dsm/system/osp/ServerTimeManager;->baseAppId:Ljava/lang/String;

    iget-object v12, p0, Lcom/sec/dsm/system/osp/ServerTimeManager;->baseAppSecret:Ljava/lang/String;

    invoke-direct {v0, v10, v11, v12}, Lcom/sec/dsm/system/osp/RestClient;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    .local v0, "client":Lcom/sec/dsm/system/osp/RestClient;
    sget-object v10, Lcom/sec/dsm/system/osp/RestClient$HttpMethod;->GET:Lcom/sec/dsm/system/osp/RestClient$HttpMethod;

    const-string v11, ""

    sget-object v12, Lcom/sec/dsm/system/osp/HeaderUtil$KindOfSignKey;->APP_SECRET:Lcom/sec/dsm/system/osp/HeaderUtil$KindOfSignKey;

    invoke-virtual {v0, v10, v5, v11, v12}, Lcom/sec/dsm/system/osp/RestClient;->execute(Lcom/sec/dsm/system/osp/RestClient$HttpMethod;Ljava/lang/String;Ljava/lang/String;Lcom/sec/dsm/system/osp/HeaderUtil$KindOfSignKey;)Lorg/apache/http/HttpResponse;

    move-result-object v6

    .line 124
    .local v6, "response":Lorg/apache/http/HttpResponse;
    const/4 v1, 0x0

    .line 125
    .local v1, "content":Ljava/io/InputStream;
    const/4 v9, 0x0

    .line 126
    .local v9, "statusLine":Lorg/apache/http/StatusLine;
    const/4 v8, 0x0

    .line 128
    .local v8, "statusCode":I
    if-eqz v6, :cond_1

    .line 129
    invoke-interface {v6}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v4

    .line 130
    if-eqz v4, :cond_0

    .line 131
    invoke-interface {v4}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v1

    .line 133
    :cond_0
    invoke-interface {v6}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v9

    .line 134
    if-eqz v9, :cond_1

    .line 135
    invoke-interface {v9}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v8

    .line 139
    :cond_1
    const/16 v10, 0xc8

    if-ne v8, v10, :cond_4

    .line 140
    if-nez v1, :cond_3

    .line 141
    new-instance v10, Ljava/lang/Exception;

    const-string v11, "Response content is null."

    invoke-direct {v10, v11}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v10
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 154
    .end local v0    # "client":Lcom/sec/dsm/system/osp/RestClient;
    .end local v1    # "content":Ljava/io/InputStream;
    .end local v5    # "requestURI":Ljava/lang/String;
    .end local v6    # "response":Lorg/apache/http/HttpResponse;
    .end local v8    # "statusCode":I
    .end local v9    # "statusLine":Lorg/apache/http/StatusLine;
    :catch_0
    move-exception v2

    .line 155
    .local v2, "e":Ljava/lang/Exception;
    const-wide/16 v10, 0x0

    :try_start_1
    invoke-direct {p0, v10, v11}, Lcom/sec/dsm/system/osp/ServerTimeManager;->setServerTime(J)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 157
    if-eqz v4, :cond_2

    .line 159
    :try_start_2
    invoke-interface {v4}, Lorg/apache/http/HttpEntity;->consumeContent()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    .line 165
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_2
    :goto_0
    return-void

    .line 143
    .restart local v0    # "client":Lcom/sec/dsm/system/osp/RestClient;
    .restart local v1    # "content":Ljava/io/InputStream;
    .restart local v5    # "requestURI":Ljava/lang/String;
    .restart local v6    # "response":Lorg/apache/http/HttpResponse;
    .restart local v8    # "statusCode":I
    .restart local v9    # "statusLine":Lorg/apache/http/StatusLine;
    :cond_3
    :try_start_3
    new-instance v7, Lcom/sec/dsm/system/osp/ServerTimeResult;

    invoke-direct {v7}, Lcom/sec/dsm/system/osp/ServerTimeResult;-><init>()V

    .line 144
    .local v7, "serverTimeResult":Lcom/sec/dsm/system/osp/ServerTimeResult;
    invoke-virtual {v7, v1}, Lcom/sec/dsm/system/osp/ServerTimeResult;->fromXML(Ljava/io/InputStream;)V

    .line 146
    invoke-virtual {v7}, Lcom/sec/dsm/system/osp/ServerTimeResult;->getCurrentServerTime()J

    move-result-wide v10

    const-wide/16 v12, 0x3e8

    div-long/2addr v10, v12

    invoke-direct {p0, v10, v11}, Lcom/sec/dsm/system/osp/ServerTimeManager;->setServerTime(J)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 157
    .end local v7    # "serverTimeResult":Lcom/sec/dsm/system/osp/ServerTimeResult;
    :goto_1
    if-eqz v4, :cond_2

    .line 159
    :try_start_4
    invoke-interface {v4}, Lorg/apache/http/HttpEntity;->consumeContent()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_0

    .line 160
    :catch_1
    move-exception v2

    .line 161
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 148
    .end local v2    # "e":Ljava/io/IOException;
    :cond_4
    if-nez v1, :cond_6

    .line 149
    :try_start_5
    new-instance v10, Ljava/lang/Exception;

    const-string v11, "Response content is null."

    invoke-direct {v10, v11}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v10
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 157
    .end local v0    # "client":Lcom/sec/dsm/system/osp/RestClient;
    .end local v1    # "content":Ljava/io/InputStream;
    .end local v5    # "requestURI":Ljava/lang/String;
    .end local v6    # "response":Lorg/apache/http/HttpResponse;
    .end local v8    # "statusCode":I
    .end local v9    # "statusLine":Lorg/apache/http/StatusLine;
    :catchall_0
    move-exception v10

    if-eqz v4, :cond_5

    .line 159
    :try_start_6
    invoke-interface {v4}, Lorg/apache/http/HttpEntity;->consumeContent()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    .line 162
    :cond_5
    :goto_2
    throw v10

    .line 151
    .restart local v0    # "client":Lcom/sec/dsm/system/osp/RestClient;
    .restart local v1    # "content":Ljava/io/InputStream;
    .restart local v5    # "requestURI":Ljava/lang/String;
    .restart local v6    # "response":Lorg/apache/http/HttpResponse;
    .restart local v8    # "statusCode":I
    .restart local v9    # "statusLine":Lorg/apache/http/StatusLine;
    :cond_6
    :try_start_7
    new-instance v3, Lcom/sec/dsm/system/osp/ErrorResultHandler;

    iget-object v10, p0, Lcom/sec/dsm/system/osp/ServerTimeManager;->context:Landroid/content/Context;

    invoke-direct {v3, v10}, Lcom/sec/dsm/system/osp/ErrorResultHandler;-><init>(Landroid/content/Context;)V

    .line 152
    .local v3, "errorResultUtil":Lcom/sec/dsm/system/osp/ErrorResultHandler;
    invoke-virtual {v3, v1}, Lcom/sec/dsm/system/osp/ErrorResultHandler;->handleErrorResult(Ljava/io/InputStream;)V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_1

    .line 160
    .end local v0    # "client":Lcom/sec/dsm/system/osp/RestClient;
    .end local v1    # "content":Ljava/io/InputStream;
    .end local v3    # "errorResultUtil":Lcom/sec/dsm/system/osp/ErrorResultHandler;
    .end local v5    # "requestURI":Ljava/lang/String;
    .end local v6    # "response":Lorg/apache/http/HttpResponse;
    .end local v8    # "statusCode":I
    .end local v9    # "statusLine":Lorg/apache/http/StatusLine;
    .local v2, "e":Ljava/lang/Exception;
    :catch_2
    move-exception v2

    .line 161
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 160
    .end local v2    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v2

    .line 161
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2
.end method

.method private setServerTime(J)V
    .locals 11
    .param p1, "currentTimestamp"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/dsm/system/osp/ServerTimeException;
        }
    .end annotation

    .prologue
    const-wide/16 v6, 0x0

    .line 168
    const-wide/16 v0, 0x0

    .line 169
    .local v0, "deviceTime":J
    const-wide/16 v4, 0x0

    .line 172
    .local v4, "settingTime":J
    cmp-long v6, p1, v6

    if-eqz v6, :cond_0

    .line 173
    :try_start_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    const-wide/16 v8, 0x3e8

    div-long v0, v6, v8

    .line 174
    sub-long v6, p1, v0

    iput-wide v6, p0, Lcom/sec/dsm/system/osp/ServerTimeManager;->offsetTime:J

    .line 175
    move-wide v4, v0

    .line 181
    :goto_0
    new-instance v3, Lcom/sec/dsm/system/osp/PropertyManager;

    iget-object v6, p0, Lcom/sec/dsm/system/osp/ServerTimeManager;->context:Landroid/content/Context;

    invoke-direct {v3, v6}, Lcom/sec/dsm/system/osp/PropertyManager;-><init>(Landroid/content/Context;)V

    .line 183
    .local v3, "propertyManager":Lcom/sec/dsm/system/osp/PropertyManager;
    const-string v6, "ServerTime.settingTime"

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v6, v7}, Lcom/sec/dsm/system/osp/PropertyManager;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 184
    const-string v6, "ServerTime.offsetTime"

    iget-wide v8, p0, Lcom/sec/dsm/system/osp/ServerTimeManager;->offsetTime:J

    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v6, v7}, Lcom/sec/dsm/system/osp/PropertyManager;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 189
    return-void

    .line 177
    .end local v3    # "propertyManager":Lcom/sec/dsm/system/osp/PropertyManager;
    :cond_0
    const-wide/16 v6, 0x0

    iput-wide v6, p0, Lcom/sec/dsm/system/osp/ServerTimeManager;->offsetTime:J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 178
    const-wide/16 v4, 0x0

    goto :goto_0

    .line 185
    :catch_0
    move-exception v2

    .line 186
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    .line 187
    new-instance v6, Lcom/sec/dsm/system/osp/ServerTimeException;

    const-string v7, "Can\'t set the Sever Time."

    invoke-direct {v6, v7, v2}, Lcom/sec/dsm/system/osp/ServerTimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v6
.end method


# virtual methods
.method public getServerTime()J
    .locals 14

    .prologue
    .line 66
    const-wide/16 v6, 0x0

    .line 67
    .local v6, "ret":J
    const-wide/16 v8, 0x0

    .line 68
    .local v8, "settingTime":J
    const-wide/16 v2, 0x0

    .line 69
    .local v2, "deviceTime":J
    const-wide/16 v0, 0x0

    .line 72
    .local v0, "dayTime":J
    :try_start_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    const-wide/16 v12, 0x3e8

    div-long v2, v10, v12

    .line 73
    const-wide/32 v0, 0x15180

    .line 75
    new-instance v5, Lcom/sec/dsm/system/osp/PropertyManager;

    iget-object v10, p0, Lcom/sec/dsm/system/osp/ServerTimeManager;->context:Landroid/content/Context;

    invoke-direct {v5, v10}, Lcom/sec/dsm/system/osp/PropertyManager;-><init>(Landroid/content/Context;)V

    .line 77
    .local v5, "propertyManager":Lcom/sec/dsm/system/osp/PropertyManager;
    const-string v10, "ServerTime.settingTime"

    const-string v11, "0"

    invoke-virtual {v5, v10, v11}, Lcom/sec/dsm/system/osp/PropertyManager;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    .line 79
    const-string v10, "ServerTime.offsetTime"

    const-string v11, "0"

    invoke-virtual {v5, v10, v11}, Lcom/sec/dsm/system/osp/PropertyManager;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    iput-wide v10, p0, Lcom/sec/dsm/system/osp/ServerTimeManager;->offsetTime:J

    .line 82
    const-wide/16 v10, 0x0

    cmp-long v10, v8, v10

    if-eqz v10, :cond_0

    add-long v10, v8, v0

    cmp-long v10, v10, v2

    if-gez v10, :cond_1

    .line 83
    :cond_0
    invoke-direct {p0}, Lcom/sec/dsm/system/osp/ServerTimeManager;->requestServerTime()V

    .line 86
    :cond_1
    iget-wide v10, p0, Lcom/sec/dsm/system/osp/ServerTimeManager;->offsetTime:J
    :try_end_0
    .catch Lcom/sec/dsm/system/osp/PropertyException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/sec/dsm/system/osp/ServerTimeException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    add-long v6, v10, v2

    .line 95
    .end local v5    # "propertyManager":Lcom/sec/dsm/system/osp/PropertyManager;
    :goto_0
    return-wide v6

    .line 87
    :catch_0
    move-exception v4

    .line 88
    .local v4, "e":Lcom/sec/dsm/system/osp/PropertyException;
    move-wide v6, v2

    .line 93
    goto :goto_0

    .line 89
    .end local v4    # "e":Lcom/sec/dsm/system/osp/PropertyException;
    :catch_1
    move-exception v4

    .line 90
    .local v4, "e":Lcom/sec/dsm/system/osp/ServerTimeException;
    move-wide v6, v2

    .line 93
    goto :goto_0

    .line 91
    .end local v4    # "e":Lcom/sec/dsm/system/osp/ServerTimeException;
    :catch_2
    move-exception v4

    .line 92
    .local v4, "e":Ljava/lang/Exception;
    move-wide v6, v2

    goto :goto_0
.end method

.method public resetServerTime()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/dsm/system/osp/ServerTimeException;
        }
    .end annotation

    .prologue
    .line 103
    :try_start_0
    new-instance v1, Lcom/sec/dsm/system/osp/PropertyManager;

    iget-object v2, p0, Lcom/sec/dsm/system/osp/ServerTimeManager;->context:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/sec/dsm/system/osp/PropertyManager;-><init>(Landroid/content/Context;)V

    .line 104
    .local v1, "propertyManager":Lcom/sec/dsm/system/osp/PropertyManager;
    const-string v2, "ServerTime.settingTime"

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/dsm/system/osp/PropertyManager;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    const-string v2, "ServerTime.offsetTime"

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/dsm/system/osp/PropertyManager;->put(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 110
    return-void

    .line 106
    .end local v1    # "propertyManager":Lcom/sec/dsm/system/osp/PropertyManager;
    :catch_0
    move-exception v0

    .line 107
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 108
    new-instance v2, Lcom/sec/dsm/system/osp/ServerTimeException;

    const-string v3, "Can\'t set the Sever Time."

    invoke-direct {v2, v3, v0}, Lcom/sec/dsm/system/osp/ServerTimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
.end method
