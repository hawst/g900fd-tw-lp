.class public Lcom/sec/dsm/system/osp/Base32;
.super Ljava/lang/Object;
.source "Base32.java"


# static fields
.field private static final DECODE_TABLE:[B

.field private static final ENCODE_TABLE:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    const/16 v0, 0x20

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/sec/dsm/system/osp/Base32;->ENCODE_TABLE:[B

    .line 35
    const/16 v0, 0x5b

    new-array v0, v0, [B

    fill-array-data v0, :array_1

    sput-object v0, Lcom/sec/dsm/system/osp/Base32;->DECODE_TABLE:[B

    return-void

    .line 25
    nop

    :array_0
    .array-data 1
        0x41t
        0x42t
        0x43t
        0x44t
        0x45t
        0x46t
        0x47t
        0x48t
        0x49t
        0x4at
        0x4bt
        0x4ct
        0x4dt
        0x4et
        0x4ft
        0x50t
        0x51t
        0x52t
        0x53t
        0x54t
        0x55t
        0x56t
        0x57t
        0x58t
        0x59t
        0x5at
        0x32t
        0x33t
        0x34t
        0x35t
        0x36t
        0x37t
    .end array-data

    .line 35
    :array_1
    .array-data 1
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        0x1at
        0x1bt
        0x1ct
        0x1dt
        0x1et
        0x1ft
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        0x0t
        0x1t
        0x2t
        0x3t
        0x4t
        0x5t
        0x6t
        0x7t
        0x8t
        0x9t
        0xat
        0xbt
        0xct
        0xdt
        0xet
        0xft
        0x10t
        0x11t
        0x12t
        0x13t
        0x14t
        0x15t
        0x16t
        0x17t
        0x18t
        0x19t
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static correctInput([B)[B
    .locals 8
    .param p0, "in"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 112
    const/4 v2, 0x0

    .line 115
    .local v2, "in2":[B
    :try_start_0
    array-length v5, p0

    rem-int/lit8 v5, v5, 0x8

    rsub-int/lit8 v4, v5, 0x8

    .line 116
    .local v4, "padN":I
    array-length v5, p0

    add-int/2addr v5, v4

    new-array v2, v5, [B

    .line 117
    const/4 v5, 0x0

    const/4 v6, 0x0

    array-length v7, p0

    invoke-static {p0, v5, v2, v6, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 118
    array-length v3, v2

    .line 119
    .local v3, "j":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v4, :cond_0

    .line 120
    add-int/lit8 v3, v3, -0x1

    const/16 v5, 0x3d

    aput-byte v5, v2, v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 119
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 122
    .end local v1    # "i":I
    .end local v3    # "j":I
    .end local v4    # "padN":I
    :catch_0
    move-exception v0

    .line 123
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 124
    throw v0

    .line 127
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v1    # "i":I
    .restart local v3    # "j":I
    .restart local v4    # "padN":I
    :cond_0
    return-object v2
.end method

.method public static decode(Ljava/lang/String;)[B
    .locals 3
    .param p0, "in"    # Ljava/lang/String;

    .prologue
    .line 191
    const/4 v1, 0x0

    .line 194
    .local v1, "out":[B
    :try_start_0
    const-string v2, "UTF-8"

    invoke-virtual {p0, v2}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v2

    invoke-static {v2}, Lcom/sec/dsm/system/osp/Base32;->decode([B)[B
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 199
    :goto_0
    return-object v1

    .line 195
    :catch_0
    move-exception v0

    .line 196
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static decode([B)[B
    .locals 15
    .param p0, "in"    # [B

    .prologue
    const/4 v7, 0x4

    const/4 v11, 0x0

    const/16 v14, 0x3d

    const/4 v9, 0x3

    const/4 v10, 0x1

    .line 137
    const/4 v5, 0x0

    .line 140
    .local v5, "out":[B
    :try_start_0
    array-length v12, p0

    if-nez v12, :cond_0

    .line 141
    const/4 v9, 0x0

    new-array v9, v9, [B

    .line 181
    :goto_0
    return-object v9

    .line 143
    :cond_0
    array-length v12, p0

    rem-int/lit8 v12, v12, 0x8

    if-eq v12, v10, :cond_1

    array-length v12, p0

    rem-int/lit8 v12, v12, 0x8

    if-eq v12, v9, :cond_1

    array-length v12, p0

    rem-int/lit8 v12, v12, 0x8

    const/4 v13, 0x6

    if-ne v12, v13, :cond_3

    .line 144
    :cond_1
    new-instance v9, Ljava/lang/Exception;

    const-string v10, "Input length is invalid."

    invoke-direct {v9, v10}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v9

    .line 177
    :catch_0
    move-exception v0

    .line 178
    .local v0, "e":Ljava/lang/Exception;
    const/4 v5, 0x0

    .end local v0    # "e":Ljava/lang/Exception;
    :cond_2
    move-object v9, v5

    .line 181
    goto :goto_0

    .line 146
    :cond_3
    array-length v12, p0

    rem-int/lit8 v12, v12, 0x8

    if-eqz v12, :cond_4

    .line 147
    invoke-static {p0}, Lcom/sec/dsm/system/osp/Base32;->correctInput([B)[B

    move-result-object p0

    .line 150
    :cond_4
    array-length v2, p0

    .line 151
    .local v2, "inN":I
    add-int/lit8 v12, v2, -0x6

    aget-byte v12, p0, v12

    if-ne v12, v14, :cond_7

    .line 153
    .local v7, "padN":I
    :goto_1
    mul-int/lit8 v9, v2, 0x5

    div-int/lit8 v9, v9, 0x8

    sub-int v6, v9, v7

    .line 155
    .local v6, "outN":I
    new-array v5, v6, [B

    .line 157
    const/16 v9, 0x8

    new-array v8, v9, [B

    .line 158
    .local v8, "temp":[B
    const/4 v1, 0x0

    .local v1, "i":I
    const/4 v3, 0x0

    .line 159
    .local v3, "j":I
    const/4 v1, 0x0

    move v4, v3

    .end local v3    # "j":I
    .local v4, "j":I
    :goto_2
    if-ge v1, v2, :cond_2

    .line 160
    rem-int/lit8 v9, v1, 0x8

    sget-object v10, Lcom/sec/dsm/system/osp/Base32;->DECODE_TABLE:[B

    aget-byte v11, p0, v1

    aget-byte v10, v10, v11

    aput-byte v10, v8, v9

    .line 161
    rem-int/lit8 v9, v1, 0x8

    const/4 v10, 0x7

    if-ne v9, v10, :cond_b

    .line 162
    add-int/lit8 v3, v4, 0x1

    .end local v4    # "j":I
    .restart local v3    # "j":I
    const/4 v9, 0x0

    aget-byte v9, v8, v9

    shl-int/lit8 v9, v9, 0x3

    and-int/lit16 v9, v9, 0xf8

    const/4 v10, 0x1

    aget-byte v10, v8, v10

    shr-int/lit8 v10, v10, 0x2

    and-int/lit8 v10, v10, 0x7

    or-int/2addr v9, v10

    int-to-byte v9, v9

    aput-byte v9, v5, v4

    .line 163
    if-ge v3, v6, :cond_c

    .line 164
    add-int/lit8 v4, v3, 0x1

    .end local v3    # "j":I
    .restart local v4    # "j":I
    const/4 v9, 0x1

    aget-byte v9, v8, v9

    shl-int/lit8 v9, v9, 0x6

    and-int/lit16 v9, v9, 0xc0

    const/4 v10, 0x2

    aget-byte v10, v8, v10

    shl-int/lit8 v10, v10, 0x1

    and-int/lit8 v10, v10, 0x3e

    or-int/2addr v9, v10

    const/4 v10, 0x3

    aget-byte v10, v8, v10

    shr-int/lit8 v10, v10, 0x4

    and-int/lit8 v10, v10, 0x1

    or-int/2addr v9, v10

    int-to-byte v9, v9

    aput-byte v9, v5, v3

    .line 166
    :goto_3
    if-ge v4, v6, :cond_5

    .line 167
    add-int/lit8 v3, v4, 0x1

    .end local v4    # "j":I
    .restart local v3    # "j":I
    const/4 v9, 0x3

    aget-byte v9, v8, v9

    shl-int/lit8 v9, v9, 0x4

    and-int/lit16 v9, v9, 0xf0

    const/4 v10, 0x4

    aget-byte v10, v8, v10

    shr-int/lit8 v10, v10, 0x1

    and-int/lit8 v10, v10, 0xf

    or-int/2addr v9, v10

    int-to-byte v9, v9

    aput-byte v9, v5, v4

    move v4, v3

    .line 169
    .end local v3    # "j":I
    .restart local v4    # "j":I
    :cond_5
    if-ge v4, v6, :cond_6

    .line 170
    add-int/lit8 v3, v4, 0x1

    .end local v4    # "j":I
    .restart local v3    # "j":I
    const/4 v9, 0x4

    aget-byte v9, v8, v9

    shl-int/lit8 v9, v9, 0x7

    and-int/lit16 v9, v9, 0x80

    const/4 v10, 0x5

    aget-byte v10, v8, v10

    shl-int/lit8 v10, v10, 0x2

    and-int/lit8 v10, v10, 0x7c

    or-int/2addr v9, v10

    const/4 v10, 0x6

    aget-byte v10, v8, v10

    shr-int/lit8 v10, v10, 0x3

    and-int/lit8 v10, v10, 0x3

    or-int/2addr v9, v10

    int-to-byte v9, v9

    aput-byte v9, v5, v4

    move v4, v3

    .line 172
    .end local v3    # "j":I
    .restart local v4    # "j":I
    :cond_6
    if-ge v4, v6, :cond_b

    .line 173
    add-int/lit8 v3, v4, 0x1

    .end local v4    # "j":I
    .restart local v3    # "j":I
    const/4 v9, 0x6

    aget-byte v9, v8, v9

    shl-int/lit8 v9, v9, 0x5

    and-int/lit16 v9, v9, 0xe0

    const/4 v10, 0x7

    aget-byte v10, v8, v10

    and-int/lit8 v10, v10, 0x1f

    or-int/2addr v9, v10

    int-to-byte v9, v9

    aput-byte v9, v5, v4

    .line 159
    :goto_4
    add-int/lit8 v1, v1, 0x1

    move v4, v3

    .end local v3    # "j":I
    .restart local v4    # "j":I
    goto/16 :goto_2

    .line 151
    .end local v1    # "i":I
    .end local v4    # "j":I
    .end local v6    # "outN":I
    .end local v7    # "padN":I
    .end local v8    # "temp":[B
    :cond_7
    add-int/lit8 v12, v2, -0x4

    aget-byte v12, p0, v12

    if-ne v12, v14, :cond_8

    move v7, v9

    goto/16 :goto_1

    :cond_8
    add-int/lit8 v9, v2, -0x3

    aget-byte v9, p0, v9

    if-ne v9, v14, :cond_9

    const/4 v7, 0x2

    goto/16 :goto_1

    :cond_9
    add-int/lit8 v9, v2, -0x1

    aget-byte v9, p0, v9
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-ne v9, v14, :cond_a

    move v7, v10

    goto/16 :goto_1

    :cond_a
    move v7, v11

    goto/16 :goto_1

    .restart local v1    # "i":I
    .restart local v4    # "j":I
    .restart local v6    # "outN":I
    .restart local v7    # "padN":I
    .restart local v8    # "temp":[B
    :cond_b
    move v3, v4

    .end local v4    # "j":I
    .restart local v3    # "j":I
    goto :goto_4

    :cond_c
    move v4, v3

    .end local v3    # "j":I
    .restart local v4    # "j":I
    goto :goto_3
.end method

.method public static encode(Ljava/lang/String;)[B
    .locals 3
    .param p0, "in"    # Ljava/lang/String;

    .prologue
    .line 93
    const/4 v1, 0x0

    .line 96
    .local v1, "out":[B
    :try_start_0
    const-string v2, "UTF-8"

    invoke-virtual {p0, v2}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v2

    invoke-static {v2}, Lcom/sec/dsm/system/osp/Base32;->encode([B)[B
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 101
    :goto_0
    return-object v1

    .line 97
    :catch_0
    move-exception v0

    .line 98
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static encode([B)[B
    .locals 11
    .param p0, "in"    # [B

    .prologue
    .line 55
    const/4 v4, 0x0

    .line 56
    .local v4, "out":[B
    array-length v1, p0

    .line 57
    .local v1, "inN":I
    add-int/lit8 v8, v1, 0x4

    div-int/lit8 v8, v8, 0x5

    mul-int/lit8 v8, v8, 0x5

    sub-int v6, v8, v1

    .line 58
    .local v6, "padN":I
    add-int/lit8 v8, v1, 0x4

    add-int/lit8 v9, v1, 0x4

    rem-int/lit8 v9, v9, 0x5

    sub-int/2addr v8, v9

    mul-int/lit8 v8, v8, 0x8

    div-int/lit8 v5, v8, 0x5

    .line 59
    .local v5, "outN":I
    new-array v4, v5, [B

    .line 60
    const/4 v8, 0x5

    new-array v7, v8, [B

    .line 61
    .local v7, "temp":[B
    const/4 v0, 0x0

    .local v0, "i":I
    const/4 v2, 0x0

    .line 62
    .local v2, "j":I
    const/4 v0, 0x0

    move v3, v2

    .end local v2    # "j":I
    .local v3, "j":I
    :goto_0
    add-int v8, v1, v6

    if-ge v0, v8, :cond_2

    .line 63
    if-ge v0, v1, :cond_1

    .line 64
    rem-int/lit8 v8, v0, 0x5

    aget-byte v9, p0, v0

    aput-byte v9, v7, v8

    .line 68
    :goto_1
    rem-int/lit8 v8, v0, 0x5

    const/4 v9, 0x4

    if-ne v8, v9, :cond_0

    .line 69
    add-int/lit8 v2, v3, 0x1

    .end local v3    # "j":I
    .restart local v2    # "j":I
    sget-object v8, Lcom/sec/dsm/system/osp/Base32;->ENCODE_TABLE:[B

    const/4 v9, 0x0

    aget-byte v9, v7, v9

    shr-int/lit8 v9, v9, 0x3

    and-int/lit8 v9, v9, 0x1f

    aget-byte v8, v8, v9

    aput-byte v8, v4, v3

    .line 70
    add-int/lit8 v3, v2, 0x1

    .end local v2    # "j":I
    .restart local v3    # "j":I
    sget-object v8, Lcom/sec/dsm/system/osp/Base32;->ENCODE_TABLE:[B

    const/4 v9, 0x0

    aget-byte v9, v7, v9

    shl-int/lit8 v9, v9, 0x2

    and-int/lit8 v9, v9, 0x1c

    const/4 v10, 0x1

    aget-byte v10, v7, v10

    shr-int/lit8 v10, v10, 0x6

    and-int/lit8 v10, v10, 0x3

    or-int/2addr v9, v10

    aget-byte v8, v8, v9

    aput-byte v8, v4, v2

    .line 71
    add-int/lit8 v2, v3, 0x1

    .end local v3    # "j":I
    .restart local v2    # "j":I
    sget-object v8, Lcom/sec/dsm/system/osp/Base32;->ENCODE_TABLE:[B

    const/4 v9, 0x1

    aget-byte v9, v7, v9

    shr-int/lit8 v9, v9, 0x1

    and-int/lit8 v9, v9, 0x1f

    aget-byte v8, v8, v9

    aput-byte v8, v4, v3

    .line 72
    add-int/lit8 v3, v2, 0x1

    .end local v2    # "j":I
    .restart local v3    # "j":I
    sget-object v8, Lcom/sec/dsm/system/osp/Base32;->ENCODE_TABLE:[B

    const/4 v9, 0x1

    aget-byte v9, v7, v9

    shl-int/lit8 v9, v9, 0x4

    and-int/lit8 v9, v9, 0x10

    const/4 v10, 0x2

    aget-byte v10, v7, v10

    shr-int/lit8 v10, v10, 0x4

    and-int/lit8 v10, v10, 0xf

    or-int/2addr v9, v10

    aget-byte v8, v8, v9

    aput-byte v8, v4, v2

    .line 73
    add-int/lit8 v2, v3, 0x1

    .end local v3    # "j":I
    .restart local v2    # "j":I
    sget-object v8, Lcom/sec/dsm/system/osp/Base32;->ENCODE_TABLE:[B

    const/4 v9, 0x2

    aget-byte v9, v7, v9

    shl-int/lit8 v9, v9, 0x1

    and-int/lit8 v9, v9, 0x1e

    const/4 v10, 0x3

    aget-byte v10, v7, v10

    shr-int/lit8 v10, v10, 0x7

    and-int/lit8 v10, v10, 0x1

    or-int/2addr v9, v10

    aget-byte v8, v8, v9

    aput-byte v8, v4, v3

    .line 74
    add-int/lit8 v3, v2, 0x1

    .end local v2    # "j":I
    .restart local v3    # "j":I
    sget-object v8, Lcom/sec/dsm/system/osp/Base32;->ENCODE_TABLE:[B

    const/4 v9, 0x3

    aget-byte v9, v7, v9

    shr-int/lit8 v9, v9, 0x2

    and-int/lit8 v9, v9, 0x1f

    aget-byte v8, v8, v9

    aput-byte v8, v4, v2

    .line 75
    add-int/lit8 v2, v3, 0x1

    .end local v3    # "j":I
    .restart local v2    # "j":I
    sget-object v8, Lcom/sec/dsm/system/osp/Base32;->ENCODE_TABLE:[B

    const/4 v9, 0x3

    aget-byte v9, v7, v9

    shl-int/lit8 v9, v9, 0x3

    and-int/lit8 v9, v9, 0x18

    const/4 v10, 0x4

    aget-byte v10, v7, v10

    shr-int/lit8 v10, v10, 0x5

    and-int/lit8 v10, v10, 0x7

    or-int/2addr v9, v10

    aget-byte v8, v8, v9

    aput-byte v8, v4, v3

    .line 76
    add-int/lit8 v3, v2, 0x1

    .end local v2    # "j":I
    .restart local v3    # "j":I
    sget-object v8, Lcom/sec/dsm/system/osp/Base32;->ENCODE_TABLE:[B

    const/4 v9, 0x4

    aget-byte v9, v7, v9

    and-int/lit8 v9, v9, 0x1f

    aget-byte v8, v8, v9

    aput-byte v8, v4, v2

    :cond_0
    move v2, v3

    .line 62
    .end local v3    # "j":I
    .restart local v2    # "j":I
    add-int/lit8 v0, v0, 0x1

    move v3, v2

    .end local v2    # "j":I
    .restart local v3    # "j":I
    goto/16 :goto_0

    .line 66
    :cond_1
    rem-int/lit8 v8, v0, 0x5

    const/4 v9, 0x0

    aput-byte v9, v7, v8

    goto/16 :goto_1

    .line 79
    :cond_2
    const/4 v0, 0x0

    move v2, v3

    .end local v3    # "j":I
    .restart local v2    # "j":I
    :goto_2
    mul-int/lit8 v8, v6, 0x3

    div-int/lit8 v8, v8, 0x2

    if-ge v0, v8, :cond_3

    .line 80
    add-int/lit8 v2, v2, -0x1

    const/16 v8, 0x3d

    aput-byte v8, v4, v2

    .line 79
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 83
    :cond_3
    return-object v4
.end method
