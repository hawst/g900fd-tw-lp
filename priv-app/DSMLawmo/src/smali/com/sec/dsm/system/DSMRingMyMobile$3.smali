.class Lcom/sec/dsm/system/DSMRingMyMobile$3;
.super Landroid/content/BroadcastReceiver;
.source "DSMRingMyMobile.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/dsm/system/DSMRingMyMobile;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/dsm/system/DSMRingMyMobile;


# direct methods
.method constructor <init>(Lcom/sec/dsm/system/DSMRingMyMobile;)V
    .locals 0

    .prologue
    .line 381
    iput-object p1, p0, Lcom/sec/dsm/system/DSMRingMyMobile$3;->this$0:Lcom/sec/dsm/system/DSMRingMyMobile;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 384
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string v3, "android.intent.action.DSM_RINGTONE_TIMEOUT"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 385
    const-string v2, "receiver +"

    invoke-static {v2}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 387
    :try_start_0
    new-instance v1, Lcom/sec/dsm/system/DSMManager;

    invoke-direct {v1, p1}, Lcom/sec/dsm/system/DSMManager;-><init>(Landroid/content/Context;)V

    .line 388
    .local v1, "im":Lcom/sec/dsm/system/DSMManager;
    invoke-virtual {v1}, Lcom/sec/dsm/system/DSMManager;->removeDSMRing()V

    .line 389
    iget-object v2, p0, Lcom/sec/dsm/system/DSMRingMyMobile$3;->this$0:Lcom/sec/dsm/system/DSMRingMyMobile;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/sec/dsm/system/DSMRingMyMobile;->setOriginalVolume(I)V

    .line 390
    invoke-static {}, Lcom/sec/dsm/system/DSMRingMyMobile$WakeLock;->release()V
    :try_end_0
    .catch Lcom/sec/dsm/system/DSMException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 397
    .end local v1    # "im":Lcom/sec/dsm/system/DSMManager;
    :goto_0
    iget-object v2, p0, Lcom/sec/dsm/system/DSMRingMyMobile$3;->this$0:Lcom/sec/dsm/system/DSMRingMyMobile;

    const/4 v3, 0x0

    # setter for: Lcom/sec/dsm/system/DSMRingMyMobile;->mRingStarted:Z
    invoke-static {v2, v3}, Lcom/sec/dsm/system/DSMRingMyMobile;->access$002(Lcom/sec/dsm/system/DSMRingMyMobile;Z)Z

    .line 398
    iget-object v2, p0, Lcom/sec/dsm/system/DSMRingMyMobile$3;->this$0:Lcom/sec/dsm/system/DSMRingMyMobile;

    invoke-virtual {v2}, Lcom/sec/dsm/system/DSMRingMyMobile;->finish()V

    .line 399
    const-string v2, "receiver -"

    invoke-static {v2}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 401
    :cond_0
    return-void

    .line 392
    :catch_0
    move-exception v0

    .line 393
    .local v0, "e":Lcom/sec/dsm/system/DSMException;
    invoke-virtual {v0}, Lcom/sec/dsm/system/DSMException;->printStackTrace()V

    goto :goto_0

    .line 394
    .end local v0    # "e":Lcom/sec/dsm/system/DSMException;
    :catch_1
    move-exception v0

    .line 395
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
