.class public Lcom/sec/dsm/system/DSMAccountService;
.super Landroid/app/IntentService;
.source "DSMAccountService.java"


# static fields
.field private static final DMSERVER:Ljava/lang/String; = "DMServer"

.field private static final DSSERVER:Ljava/lang/String; = "DSServer"

.field private static final TAG:Ljava/lang/String; = "DSM"

.field private static final UPDATE_URL:Ljava/lang/String; = "android.intent.action.dsm.UPDATE_URL"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 30
    const-string v0, "DSMAccountService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 31
    return-void
.end method


# virtual methods
.method public isSamsungAccount()Z
    .locals 3

    .prologue
    .line 42
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 43
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    const-string v2, "com.osp.app.signin"

    invoke-virtual {v1, v2}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 45
    .local v0, "samsungAccounts":[Landroid/accounts/Account;
    array-length v1, v0

    if-eqz v1, :cond_0

    .line 47
    const/4 v1, 0x1

    .line 50
    .end local v0    # "samsungAccounts":[Landroid/accounts/Account;
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 35
    invoke-virtual {p0}, Lcom/sec/dsm/system/DSMAccountService;->isSamsungAccount()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 36
    invoke-virtual {p0}, Lcom/sec/dsm/system/DSMAccountService;->setServerURI()V

    .line 38
    :cond_0
    return-void
.end method

.method public setServerURI()V
    .locals 21

    .prologue
    .line 54
    const-string v2, "content://com.osp.contentprovider.ospcontentprovider/identity"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 55
    .local v3, "USERID_URI":Landroid/net/Uri;
    const-string v8, "Key"

    .line 56
    .local v8, "COLUMN_OAUTH_KEY":Ljava/lang/String;
    const-string v9, "Value"

    .line 57
    .local v9, "COLUMN_OAUTH_VALUE":Ljava/lang/String;
    const-string v10, "ServerUrl"

    .line 59
    .local v10, "COLUMN_SERVERURL":Ljava/lang/String;
    const/4 v2, 0x1

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object v9, v4, v2

    .line 60
    .local v4, "projection":[Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v7, " = ?"

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 61
    .local v5, "selection":Ljava/lang/String;
    const/4 v2, 0x1

    new-array v6, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object v10, v6, v2

    .line 62
    .local v6, "selectionArgs":[Ljava/lang/String;
    const/16 v19, 0x0

    .line 64
    .local v19, "url":Ljava/lang/String;
    const/4 v12, 0x0

    .line 66
    .local v12, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/sec/dsm/system/DSMAccountService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 67
    if-eqz v12, :cond_0

    invoke-interface {v12}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 68
    const/4 v2, 0x0

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 69
    .local v11, "cryptedServer":Ljava/lang/String;
    invoke-static/range {p0 .. p0}, Lcom/sec/dsm/system/osp/AESCrypto;->generateContentKey(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v16

    .line 70
    .local v16, "key":Ljava/lang/String;
    move-object/from16 v0, v16

    invoke-static {v0, v11}, Lcom/sec/dsm/system/osp/AESCrypto;->decryptAES(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    .line 71
    const-string v2, "DSM"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "Server URL:"

    move-object/from16 v0, v20

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, v19

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v2, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 76
    .end local v11    # "cryptedServer":Ljava/lang/String;
    .end local v16    # "key":Ljava/lang/String;
    :cond_0
    if-eqz v12, :cond_1

    .line 77
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 82
    :cond_1
    :goto_0
    :try_start_1
    new-instance v13, Lcom/sec/dsm/system/DSMManager;

    move-object/from16 v0, p0

    invoke-direct {v13, v0}, Lcom/sec/dsm/system/DSMManager;-><init>(Landroid/content/Context;)V

    .line 83
    .local v13, "dsmManager":Lcom/sec/dsm/system/DSMManager;
    invoke-virtual {v13}, Lcom/sec/dsm/system/DSMManager;->getServerUrl()Ljava/lang/String;

    move-result-object v17

    .line 84
    .local v17, "prev":Ljava/lang/String;
    if-eqz v19, :cond_5

    .line 85
    move-object/from16 v0, v19

    invoke-virtual {v13, v0}, Lcom/sec/dsm/system/DSMManager;->putServerUrl(Ljava/lang/String;)V

    .line 97
    :goto_1
    if-eqz v17, :cond_2

    move-object/from16 v0, v19

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-eqz v2, :cond_3

    .line 98
    :cond_2
    const-string v2, "DSM"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "broadcast url:"

    move-object/from16 v0, v20

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, v19

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v2, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 99
    new-instance v15, Landroid/content/Intent;

    const-string v2, "android.intent.action.dsm.UPDATE_URL"

    invoke-direct {v15, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 100
    .local v15, "i":Landroid/content/Intent;
    const-string v2, "DMServer"

    invoke-virtual {v13}, Lcom/sec/dsm/system/DSMManager;->getDMServer()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v15, v2, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 101
    const-string v2, "DSServer"

    invoke-virtual {v13}, Lcom/sec/dsm/system/DSMManager;->getDSServer()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v15, v2, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 102
    const/16 v2, 0x20

    invoke-virtual {v15, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 103
    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lcom/sec/dsm/system/DSMAccountService;->sendBroadcast(Landroid/content/Intent;)V
    :try_end_1
    .catch Lcom/sec/dsm/system/DSMException; {:try_start_1 .. :try_end_1} :catch_1

    .line 108
    .end local v13    # "dsmManager":Lcom/sec/dsm/system/DSMManager;
    .end local v15    # "i":Landroid/content/Intent;
    .end local v17    # "prev":Ljava/lang/String;
    :cond_3
    :goto_2
    return-void

    .line 73
    :catch_0
    move-exception v14

    .line 74
    .local v14, "e":Ljava/lang/Exception;
    :try_start_2
    const-string v2, "DSM"

    const-string v7, "Failed to get URL."

    invoke-static {v2, v7, v14}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 76
    if-eqz v12, :cond_1

    .line 77
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 76
    .end local v14    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    if-eqz v12, :cond_4

    .line 77
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v2

    .line 87
    .restart local v13    # "dsmManager":Lcom/sec/dsm/system/DSMManager;
    .restart local v17    # "prev":Ljava/lang/String;
    :cond_5
    :try_start_3
    const-string v2, "ro.csc.sales_code"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    .line 88
    .local v18, "salesCode":Ljava/lang/String;
    const-string v2, "CHN"

    move-object/from16 v0, v18

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    const-string v2, "CHU"

    move-object/from16 v0, v18

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    const-string v2, "CHM"

    move-object/from16 v0, v18

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    const-string v2, "CTC"

    move-object/from16 v0, v18

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    const-string v2, "CHC"

    move-object/from16 v0, v18

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 90
    :cond_6
    const-string v19, "chn.ospserver.net"

    .line 91
    move-object/from16 v0, v19

    invoke-virtual {v13, v0}, Lcom/sec/dsm/system/DSMManager;->putServerUrl(Ljava/lang/String;)V
    :try_end_3
    .catch Lcom/sec/dsm/system/DSMException; {:try_start_3 .. :try_end_3} :catch_1

    goto/16 :goto_1

    .line 105
    .end local v13    # "dsmManager":Lcom/sec/dsm/system/DSMManager;
    .end local v17    # "prev":Ljava/lang/String;
    .end local v18    # "salesCode":Ljava/lang/String;
    :catch_1
    move-exception v14

    .line 106
    .local v14, "e":Lcom/sec/dsm/system/DSMException;
    const-string v2, "DSM"

    const-string v7, "Failed dsmRepository"

    invoke-static {v2, v7, v14}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2

    .line 93
    .end local v14    # "e":Lcom/sec/dsm/system/DSMException;
    .restart local v13    # "dsmManager":Lcom/sec/dsm/system/DSMManager;
    .restart local v17    # "prev":Ljava/lang/String;
    .restart local v18    # "salesCode":Ljava/lang/String;
    :cond_7
    :try_start_4
    const-string v19, "www.ospserver.net"

    .line 94
    move-object/from16 v0, v19

    invoke-virtual {v13, v0}, Lcom/sec/dsm/system/DSMManager;->putServerUrl(Ljava/lang/String;)V
    :try_end_4
    .catch Lcom/sec/dsm/system/DSMException; {:try_start_4 .. :try_end_4} :catch_1

    goto/16 :goto_1
.end method
