.class public Lcom/sec/dsm/system/DSMWipeOutService;
.super Landroid/app/Service;
.source "DSMWipeOutService.java"


# static fields
.field private static final CONTACT_ICC_URI:Landroid/net/Uri;

.field private static final CONTACT_ICC_URI2:Landroid/net/Uri;

.field private static final CONTACT_ICC_URI2_2:Landroid/net/Uri;

.field private static final CONTACT_ICC_URI_2:Landroid/net/Uri;

.field private static final DM_WIPE_RESULT:Ljava/lang/String; = "android.intent.action.dsm.DM_WIPE_RESULT"

.field private static final FACTORYRESET_COMMAND:Ljava/lang/String; = "android.intent.action.MASTER_CLEAR"

.field private static final INTERNAL_SDCARD:Ljava/lang/String; = "/sdcard/"

.field private static final SIM2_ACCOUNT_NAME:Ljava/lang/String; = "primary.sim2.account_name"

.field private static final SIM2_ACCOUNT_TYPE:Ljava/lang/String; = "vnd.sec.contact.sim2"

.field private static final SIM2_STATE_CHECK:Ljava/lang/String; = "gsm.sim.state2"

.field private static final SIM_ACCOUNT_NAME:Ljava/lang/String; = "primary.sim.account_name"

.field private static final SIM_ACCOUNT_TYPE:Ljava/lang/String; = "vnd.sec.contact.sim"

.field private static final SIM_STATE:Ljava/lang/String; = "READY"

.field private static final SIM_STATE_CHECK:Ljava/lang/String; = "gsm.sim.state"

.field private static final SMS_ICC_URI:Landroid/net/Uri;

.field private static final SMS_ICC_URI_2:Landroid/net/Uri;

.field protected static mStorageVolumes:[Landroid/os/storage/StorageVolume;

.field private static mcontext:Landroid/content/Context;


# instance fields
.field private final ADN_PROJECTION:[Ljava/lang/String;

.field private final DISABLE:Z

.field private final ENABLE:Z

.field private mAppOpsManager:Landroid/app/AppOpsManager;

.field private mContentResolver:Landroid/content/ContentResolver;

.field private mStorageManager:Landroid/os/storage/StorageManager;

.field private rawContactsWipeOutUri:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const-string v0, "content://sms/icc"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/dsm/system/DSMWipeOutService;->SMS_ICC_URI:Landroid/net/Uri;

    .line 35
    const-string v0, "content://icc/adn"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/dsm/system/DSMWipeOutService;->CONTACT_ICC_URI:Landroid/net/Uri;

    .line 39
    const-string v0, "content://icc/adn/from_contacts"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/dsm/system/DSMWipeOutService;->CONTACT_ICC_URI2:Landroid/net/Uri;

    .line 43
    const-string v0, "content://sms/icc2"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/dsm/system/DSMWipeOutService;->SMS_ICC_URI_2:Landroid/net/Uri;

    .line 46
    const-string v0, "content://icc/adn/subId/2"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/dsm/system/DSMWipeOutService;->CONTACT_ICC_URI_2:Landroid/net/Uri;

    .line 50
    const-string v0, "content://icc/adn/from_contacts/subId/2"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/dsm/system/DSMWipeOutService;->CONTACT_ICC_URI2_2:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 25
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 29
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "name"

    aput-object v1, v0, v3

    const-string v1, "number"

    aput-object v1, v0, v4

    const/4 v1, 0x2

    const-string v2, "adn_index"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/sec/dsm/system/DSMWipeOutService;->ADN_PROJECTION:[Ljava/lang/String;

    .line 56
    const-string v0, "content://com.android.contacts/raw_contacts/wipe_out"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/dsm/system/DSMWipeOutService;->rawContactsWipeOutUri:Landroid/net/Uri;

    .line 65
    iput-boolean v4, p0, Lcom/sec/dsm/system/DSMWipeOutService;->ENABLE:Z

    .line 66
    iput-boolean v3, p0, Lcom/sec/dsm/system/DSMWipeOutService;->DISABLE:Z

    .line 70
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/dsm/system/DSMWipeOutService;->mStorageManager:Landroid/os/storage/StorageManager;

    return-void
.end method

.method private FactoryReset()V
    .locals 4

    .prologue
    .line 292
    invoke-static {}, Landroid/os/Environment;->isExternalStorageEmulated()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 293
    const-string v1, "Send Intent for SecFactoryReset : end"

    invoke-static {v1}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 294
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.MASTER_CLEAR"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 295
    .local v0, "intent":Landroid/content/Intent;
    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 296
    sget-object v1, Lcom/sec/dsm/system/DSMWipeOutService;->mcontext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 303
    :goto_0
    return-void

    .line 298
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    const-string v1, "Send Intent for erase internalsd and SecFactoryReset : end"

    invoke-static {v1}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 299
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.android.internal.os.storage.FORMAT_AND_FACTORY_RESET"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 300
    .restart local v0    # "intent":Landroid/content/Intent;
    new-instance v1, Landroid/content/ComponentName;

    const-string v2, "android"

    const-string v3, "ExternalStorageFormatter"

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 301
    invoke-virtual {p0, v0}, Lcom/sec/dsm/system/DSMWipeOutService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0
.end method

.method private deleteAllContactsFromSIMCard()Z
    .locals 18

    .prologue
    .line 465
    const-string v1, "Delete all contacts from SIM card begin"

    invoke-static {v1}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 466
    const/4 v9, 0x0

    .line 467
    .local v9, "cursor":Landroid/database/Cursor;
    const/4 v13, 0x0

    .line 468
    .local v13, "state":Z
    const-string v1, "gsm.sim.state"

    const/4 v2, 0x0

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 469
    .local v11, "simstate":Ljava/lang/String;
    const-string v1, "gsm.sim.state2"

    const/4 v2, 0x0

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 475
    .local v12, "simstate2":Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/telephony/MultiSimManager;->getSimSlotCount()I

    move-result v1

    const/4 v2, 0x1

    if-le v1, v2, :cond_0

    .line 476
    const-string v1, ","

    invoke-virtual {v11, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v15

    .line 477
    .local v15, "values":[Ljava/lang/String;
    if-eqz v15, :cond_0

    array-length v1, v15

    const/4 v2, 0x1

    if-le v1, v2, :cond_0

    .line 478
    const/4 v1, 0x0

    aget-object v11, v15, v1

    .line 479
    const/4 v1, 0x1

    aget-object v12, v15, v1

    .line 480
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SimState 1: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 481
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SimState 2: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 496
    .end local v15    # "values":[Ljava/lang/String;
    :cond_0
    invoke-static {}, Lcom/samsung/android/telephony/MultiSimManager;->getSimSlotCount()I

    move-result v1

    const/4 v2, 0x1

    if-le v1, v2, :cond_1

    .line 497
    const-string v1, "READY"

    invoke-virtual {v1, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "READY"

    invoke-virtual {v1, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    move v14, v13

    .line 533
    .end local v13    # "state":Z
    .local v14, "state":I
    :goto_0
    return v14

    .line 502
    .end local v14    # "state":I
    .restart local v13    # "state":Z
    :cond_1
    :try_start_0
    const-string v1, "READY"

    invoke-virtual {v1, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 503
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/dsm/system/DSMWipeOutService;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v2, Lcom/sec/dsm/system/DSMWipeOutService;->CONTACT_ICC_URI:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/dsm/system/DSMWipeOutService;->ADN_PROJECTION:[Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 504
    const/16 v16, 0x0

    .line 505
    .local v16, "where":Ljava/lang/String;
    if-eqz v9, :cond_3

    .line 506
    :goto_1
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 507
    const-string v1, "adn_index"

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 508
    .local v7, "AdnIdx":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "adn_index = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    .line 509
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/dsm/system/DSMWipeOutService;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v2, Lcom/sec/dsm/system/DSMWipeOutService;->CONTACT_ICC_URI2:Landroid/net/Uri;

    const/4 v3, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 527
    .end local v7    # "AdnIdx":Ljava/lang/String;
    .end local v16    # "where":Ljava/lang/String;
    :catch_0
    move-exception v10

    .line 528
    .local v10, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v10}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 530
    if-eqz v9, :cond_2

    .line 531
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .end local v10    # "e":Ljava/lang/Exception;
    :cond_2
    :goto_2
    move v14, v13

    .line 533
    .restart local v14    # "state":I
    goto :goto_0

    .line 513
    .end local v14    # "state":I
    :cond_3
    :try_start_2
    invoke-static {}, Lcom/samsung/android/telephony/MultiSimManager;->getSimSlotCount()I

    move-result v1

    const/4 v2, 0x1

    if-le v1, v2, :cond_5

    .line 514
    const-string v1, "READY"

    invoke-virtual {v1, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 515
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/dsm/system/DSMWipeOutService;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v2, Lcom/sec/dsm/system/DSMWipeOutService;->CONTACT_ICC_URI_2:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/dsm/system/DSMWipeOutService;->ADN_PROJECTION:[Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 516
    const/16 v17, 0x0

    .line 517
    .local v17, "where2":Ljava/lang/String;
    if-eqz v9, :cond_5

    .line 518
    :goto_3
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 519
    const-string v1, "adn_index"

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 520
    .local v8, "AdnIdx2":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "adn_index = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    .line 521
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/dsm/system/DSMWipeOutService;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v2, Lcom/sec/dsm/system/DSMWipeOutService;->CONTACT_ICC_URI2_2:Landroid/net/Uri;

    const/4 v3, 0x0

    move-object/from16 v0, v17

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_3

    .line 530
    .end local v8    # "AdnIdx2":Ljava/lang/String;
    .end local v17    # "where2":Ljava/lang/String;
    :catchall_0
    move-exception v1

    if-eqz v9, :cond_4

    .line 531
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v1

    .line 526
    :cond_5
    const/4 v13, 0x1

    .line 530
    if-eqz v9, :cond_2

    .line 531
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    goto :goto_2
.end method

.method private deleteAllMessagesFromSIMCard()Z
    .locals 21

    .prologue
    .line 362
    const-string v1, "Delete all message from SIM card begin"

    invoke-static {v1}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 363
    const/4 v9, 0x0

    .line 364
    .local v9, "cursor":Landroid/database/Cursor;
    const/16 v18, 0x0

    .line 365
    .local v18, "state":Z
    const-string v1, "gsm.sim.state"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 366
    .local v16, "simstate":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SimState : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, v16

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 367
    const-string v1, "gsm.sim.state2"

    const/4 v2, 0x0

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    .line 369
    .local v17, "simstate2":Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/telephony/MultiSimManager;->getSimSlotCount()I

    move-result v1

    const/4 v2, 0x1

    if-le v1, v2, :cond_0

    .line 370
    const-string v1, ","

    move-object/from16 v0, v16

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v20

    .line 371
    .local v20, "values":[Ljava/lang/String;
    if-eqz v20, :cond_0

    move-object/from16 v0, v20

    array-length v1, v0

    const/4 v2, 0x1

    if-le v1, v2, :cond_0

    .line 372
    const/4 v1, 0x0

    aget-object v16, v20, v1

    .line 373
    const/4 v1, 0x1

    aget-object v17, v20, v1

    .line 374
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SimState 1: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, v16

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 375
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SimState 2: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, v17

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 379
    .end local v20    # "values":[Ljava/lang/String;
    :cond_0
    invoke-direct/range {p0 .. p0}, Lcom/sec/dsm/system/DSMWipeOutService;->getApplicationOperationMode()I

    move-result v1

    if-eqz v1, :cond_1

    .line 380
    const/4 v1, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/sec/dsm/system/DSMWipeOutService;->setWriteMessage(Z)V

    .line 398
    :cond_1
    invoke-static {}, Lcom/samsung/android/telephony/MultiSimManager;->getSimSlotCount()I

    move-result v1

    const/4 v2, 0x1

    if-le v1, v2, :cond_2

    .line 399
    const-string v1, "READY"

    move-object/from16 v0, v16

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "READY"

    move-object/from16 v0, v17

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    move/from16 v19, v18

    .line 460
    .end local v18    # "state":Z
    .local v19, "state":I
    :goto_0
    return v19

    .line 404
    .end local v19    # "state":I
    .restart local v18    # "state":Z
    :cond_2
    :try_start_0
    const-string v1, "READY"

    move-object/from16 v0, v16

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 405
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/dsm/system/DSMWipeOutService;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v2, Lcom/sec/dsm/system/DSMWipeOutService;->SMS_ICC_URI:Landroid/net/Uri;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 406
    if-eqz v9, :cond_3

    .line 407
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 408
    invoke-interface {v9}, Landroid/database/Cursor;->getCount()I

    move-result v7

    .line 409
    .local v7, "count":I
    const/4 v13, 0x0

    .local v13, "i":I
    :goto_1
    if-ge v13, v7, :cond_3

    .line 410
    const-string v1, "index_on_icc"

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    .line 412
    .local v14, "messageIndexString":Ljava/lang/String;
    sget-object v1, Lcom/sec/dsm/system/DSMWipeOutService;->SMS_ICC_URI:Landroid/net/Uri;

    move-object/from16 v0, p0

    invoke-direct {v0, v1, v14}, Lcom/sec/dsm/system/DSMWipeOutService;->deleteSMSFromSim(Landroid/net/Uri;Ljava/lang/String;)V

    .line 413
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    .line 409
    add-int/lit8 v13, v13, 0x1

    goto :goto_1

    .line 417
    .end local v7    # "count":I
    .end local v13    # "i":I
    .end local v14    # "messageIndexString":Ljava/lang/String;
    :cond_3
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/dsm/system/DSMWipeOutService;->rawContactsWipeOutUri:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "account_name"

    const-string v3, "primary.sim.account_name"

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "account_type"

    const-string v3, "vnd.sec.contact.sim"

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v10

    .line 420
    .local v10, "deleteUri":Landroid/net/Uri;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/dsm/system/DSMWipeOutService;->mContentResolver:Landroid/content/ContentResolver;

    const-string v2, ""

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/String;

    invoke-virtual {v1, v10, v2, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 422
    .end local v10    # "deleteUri":Landroid/net/Uri;
    :cond_4
    invoke-static {}, Lcom/samsung/android/telephony/MultiSimManager;->getSimSlotCount()I

    move-result v1

    const/4 v2, 0x1

    if-le v1, v2, :cond_6

    const-string v1, "READY"

    move-object/from16 v0, v17

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 424
    const-string v1, "READY"

    move-object/from16 v0, v17

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 428
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/dsm/system/DSMWipeOutService;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v2, Lcom/sec/dsm/system/DSMWipeOutService;->SMS_ICC_URI_2:Landroid/net/Uri;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 430
    if-eqz v9, :cond_5

    .line 431
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 432
    invoke-interface {v9}, Landroid/database/Cursor;->getCount()I

    move-result v8

    .line 433
    .local v8, "count2":I
    const/4 v13, 0x0

    .restart local v13    # "i":I
    :goto_2
    if-ge v13, v8, :cond_5

    .line 434
    const-string v1, "index_on_icc"

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 439
    .local v15, "messageIndexString2":Ljava/lang/String;
    sget-object v1, Lcom/sec/dsm/system/DSMWipeOutService;->SMS_ICC_URI_2:Landroid/net/Uri;

    move-object/from16 v0, p0

    invoke-direct {v0, v1, v15}, Lcom/sec/dsm/system/DSMWipeOutService;->deleteSMSFromSim(Landroid/net/Uri;Ljava/lang/String;)V

    .line 441
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    .line 433
    add-int/lit8 v13, v13, 0x1

    goto :goto_2

    .line 446
    .end local v8    # "count2":I
    .end local v13    # "i":I
    .end local v15    # "messageIndexString2":Ljava/lang/String;
    :cond_5
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/dsm/system/DSMWipeOutService;->rawContactsWipeOutUri:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "account_name"

    const-string v3, "primary.sim2.account_name"

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "account_type"

    const-string v3, "vnd.sec.contact.sim2"

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v11

    .line 449
    .local v11, "deleteUri2":Landroid/net/Uri;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/dsm/system/DSMWipeOutService;->mContentResolver:Landroid/content/ContentResolver;

    const-string v2, ""

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/String;

    invoke-virtual {v1, v11, v2, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 451
    .end local v11    # "deleteUri2":Landroid/net/Uri;
    :cond_6
    const/16 v18, 0x1

    .line 455
    if-eqz v9, :cond_7

    .line 456
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 458
    :cond_7
    :goto_3
    const/4 v1, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/sec/dsm/system/DSMWipeOutService;->setWriteMessage(Z)V

    move/from16 v19, v18

    .line 460
    .restart local v19    # "state":I
    goto/16 :goto_0

    .line 452
    .end local v19    # "state":I
    :catch_0
    move-exception v12

    .line 453
    .local v12, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v12}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 455
    if-eqz v9, :cond_7

    .line 456
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    goto :goto_3

    .line 455
    .end local v12    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    if-eqz v9, :cond_8

    .line 456
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    :cond_8
    throw v1
.end method

.method private deleteSMSFromSim(Landroid/net/Uri;Ljava/lang/String;)V
    .locals 6
    .param p1, "SMS_Uri"    # Landroid/net/Uri;
    .param p2, "messageIndexString"    # Ljava/lang/String;

    .prologue
    .line 537
    :try_start_0
    invoke-virtual {p1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2, p2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 538
    .local v1, "simUri":Landroid/net/Uri;
    sget-object v2, Lcom/sec/dsm/system/DSMWipeOutService;->mcontext:Landroid/content/Context;

    iget-object v3, p0, Lcom/sec/dsm/system/DSMWipeOutService;->mContentResolver:Landroid/content/ContentResolver;

    const-string v4, ""

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/String;

    invoke-static {v2, v3, v1, v4, v5}, Landroid/database/sqlite/SqliteWrapper;->delete(Landroid/content/Context;Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 542
    .end local v1    # "simUri":Landroid/net/Uri;
    :goto_0
    return-void

    .line 539
    :catch_0
    move-exception v0

    .line 540
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private externalSdExist()Z
    .locals 9

    .prologue
    .line 240
    const/4 v0, 0x0

    .line 241
    .local v0, "ex_sd":Z
    iget-object v7, p0, Lcom/sec/dsm/system/DSMWipeOutService;->mStorageManager:Landroid/os/storage/StorageManager;

    if-nez v7, :cond_0

    .line 242
    const-string v7, "storage"

    invoke-virtual {p0, v7}, Lcom/sec/dsm/system/DSMWipeOutService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/os/storage/StorageManager;

    iput-object v7, p0, Lcom/sec/dsm/system/DSMWipeOutService;->mStorageManager:Landroid/os/storage/StorageManager;

    .line 244
    :cond_0
    iget-object v7, p0, Lcom/sec/dsm/system/DSMWipeOutService;->mStorageManager:Landroid/os/storage/StorageManager;

    invoke-virtual {v7}, Landroid/os/storage/StorageManager;->getVolumeList()[Landroid/os/storage/StorageVolume;

    move-result-object v5

    .line 245
    .local v5, "storageVolumes":[Landroid/os/storage/StorageVolume;
    array-length v2, v5

    .line 246
    .local v2, "length":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_2

    .line 247
    aget-object v4, v5, v1

    .line 248
    .local v4, "storageVolume":Landroid/os/storage/StorageVolume;
    invoke-virtual {v4}, Landroid/os/storage/StorageVolume;->getSubSystem()Ljava/lang/String;

    move-result-object v6

    .line 249
    .local v6, "subsystem":Ljava/lang/String;
    if-eqz v6, :cond_1

    .line 250
    const-string v7, "sd"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 251
    iget-object v7, p0, Lcom/sec/dsm/system/DSMWipeOutService;->mStorageManager:Landroid/os/storage/StorageManager;

    invoke-virtual {v4}, Landroid/os/storage/StorageVolume;->getPath()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/os/storage/StorageManager;->getVolumeState(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 252
    .local v3, "state":Ljava/lang/String;
    const-string v7, "mounted"

    invoke-virtual {v7, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 253
    const/4 v0, 0x1

    .line 246
    .end local v3    # "state":Ljava/lang/String;
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 258
    .end local v4    # "storageVolume":Landroid/os/storage/StorageVolume;
    .end local v6    # "subsystem":Ljava/lang/String;
    :cond_2
    return v0
.end method

.method private externalSdFormat()Z
    .locals 10

    .prologue
    .line 262
    const-string v8, "External SDcard format : start"

    invoke-static {v8}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 263
    const/4 v3, 0x0

    .line 264
    .local v3, "state":Z
    iget-object v8, p0, Lcom/sec/dsm/system/DSMWipeOutService;->mStorageManager:Landroid/os/storage/StorageManager;

    if-nez v8, :cond_0

    .line 265
    const-string v8, "storage"

    invoke-virtual {p0, v8}, Lcom/sec/dsm/system/DSMWipeOutService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/os/storage/StorageManager;

    iput-object v8, p0, Lcom/sec/dsm/system/DSMWipeOutService;->mStorageManager:Landroid/os/storage/StorageManager;

    .line 267
    :cond_0
    iget-object v8, p0, Lcom/sec/dsm/system/DSMWipeOutService;->mStorageManager:Landroid/os/storage/StorageManager;

    invoke-virtual {v8}, Landroid/os/storage/StorageManager;->getVolumeList()[Landroid/os/storage/StorageVolume;

    move-result-object v6

    .line 268
    .local v6, "storageVolumes":[Landroid/os/storage/StorageVolume;
    array-length v2, v6

    .line 269
    .local v2, "length":I
    const-string v0, ""

    .line 270
    .local v0, "EXTERNAL_SDCARD_PATH":Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_2

    .line 271
    aget-object v5, v6, v1

    .line 272
    .local v5, "storageVolume":Landroid/os/storage/StorageVolume;
    invoke-virtual {v5}, Landroid/os/storage/StorageVolume;->getSubSystem()Ljava/lang/String;

    move-result-object v7

    .line 273
    .local v7, "subsystem":Ljava/lang/String;
    if-eqz v7, :cond_1

    .line 274
    const-string v8, "sd"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 275
    iget-object v8, p0, Lcom/sec/dsm/system/DSMWipeOutService;->mStorageManager:Landroid/os/storage/StorageManager;

    invoke-virtual {v5}, Landroid/os/storage/StorageVolume;->getPath()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/os/storage/StorageManager;->getVolumeState(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 276
    .local v4, "states":Ljava/lang/String;
    const-string v8, "mounted"

    invoke-virtual {v8, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 277
    invoke-virtual {v5}, Landroid/os/storage/StorageVolume;->getPath()Ljava/lang/String;

    move-result-object v0

    .line 270
    .end local v4    # "states":Ljava/lang/String;
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 282
    .end local v5    # "storageVolume":Landroid/os/storage/StorageVolume;
    .end local v7    # "subsystem":Ljava/lang/String;
    :cond_2
    new-instance v8, Ljava/io/File;

    invoke-direct {v8, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v8}, Lcom/sec/dsm/system/DSMWipeOutService;->deleteExtSdCardFiles(Ljava/io/File;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 283
    const/4 v3, 0x1

    .line 288
    :goto_1
    return v3

    .line 286
    :cond_3
    const-string v8, "External sdcard unmounted"

    invoke-static {v8}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    goto :goto_1
.end method

.method private getApplicationOperationMode()I
    .locals 5

    .prologue
    .line 556
    sget-object v2, Lcom/sec/dsm/system/DSMWipeOutService;->mcontext:Landroid/content/Context;

    const-string v2, "appops"

    invoke-virtual {p0, v2}, Lcom/sec/dsm/system/DSMWipeOutService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AppOpsManager;

    .line 557
    .local v0, "appOps":Landroid/app/AppOpsManager;
    const/16 v2, 0xf

    invoke-virtual {p0}, Lcom/sec/dsm/system/DSMWipeOutService;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v3

    iget v3, v3, Landroid/content/pm/ApplicationInfo;->uid:I

    invoke-virtual {p0}, Lcom/sec/dsm/system/DSMWipeOutService;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v4

    iget-object v4, v4, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v0, v2, v3, v4}, Landroid/app/AppOpsManager;->checkOp(IILjava/lang/String;)I

    move-result v1

    .line 558
    .local v1, "isAllowAppOps":I
    return v1
.end method

.method private setWriteMessage(Z)V
    .locals 5
    .param p1, "setEnable"    # Z

    .prologue
    const/16 v4, 0xf

    .line 545
    iget-object v0, p0, Lcom/sec/dsm/system/DSMWipeOutService;->mAppOpsManager:Landroid/app/AppOpsManager;

    if-nez v0, :cond_0

    .line 554
    :goto_0
    return-void

    .line 549
    :cond_0
    if-eqz p1, :cond_1

    .line 550
    iget-object v0, p0, Lcom/sec/dsm/system/DSMWipeOutService;->mAppOpsManager:Landroid/app/AppOpsManager;

    invoke-virtual {p0}, Lcom/sec/dsm/system/DSMWipeOutService;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    iget v1, v1, Landroid/content/pm/ApplicationInfo;->uid:I

    invoke-virtual {p0}, Lcom/sec/dsm/system/DSMWipeOutService;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v2

    iget-object v2, v2, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {v0, v4, v1, v2, v3}, Landroid/app/AppOpsManager;->setMode(IILjava/lang/String;I)V

    goto :goto_0

    .line 552
    :cond_1
    iget-object v0, p0, Lcom/sec/dsm/system/DSMWipeOutService;->mAppOpsManager:Landroid/app/AppOpsManager;

    invoke-virtual {p0}, Lcom/sec/dsm/system/DSMWipeOutService;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    iget v1, v1, Landroid/content/pm/ApplicationInfo;->uid:I

    invoke-virtual {p0}, Lcom/sec/dsm/system/DSMWipeOutService;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v2

    iget-object v2, v2, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    const/4 v3, 0x1

    invoke-virtual {v0, v4, v1, v2, v3}, Landroid/app/AppOpsManager;->setMode(IILjava/lang/String;I)V

    goto :goto_0
.end method

.method private wipeOut(I)V
    .locals 13
    .param p1, "status"    # I

    .prologue
    const/16 v11, 0x4b0

    const/16 v12, 0x20

    const/4 v10, 0x1

    .line 95
    new-instance v1, Landroid/content/Intent;

    const-string v8, "android.intent.action.dsm.DM_WIPE_RESULT"

    invoke-direct {v1, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 96
    .local v1, "i":Landroid/content/Intent;
    const/4 v7, -0x1

    .line 97
    .local v7, "state":I
    const/4 v3, 0x0

    .line 98
    .local v3, "sdExt":Z
    const/4 v4, 0x0

    .line 99
    .local v4, "simCont":Z
    const/4 v5, 0x0

    .line 100
    .local v5, "simMesg":Z
    const/4 v6, 0x0

    .line 101
    .local v6, "simResult":Z
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "[WipeOut] SimSlotCount : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {}, Lcom/samsung/android/telephony/MultiSimManager;->getSimSlotCount()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 102
    sparse-switch p1, :sswitch_data_0

    .line 233
    const-string v8, "Unsupported case"

    invoke-static {v8}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 234
    invoke-virtual {p0}, Lcom/sec/dsm/system/DSMWipeOutService;->stopSelf()V

    .line 237
    :goto_0
    return-void

    .line 104
    :sswitch_0
    invoke-direct {p0}, Lcom/sec/dsm/system/DSMWipeOutService;->externalSdExist()Z

    move-result v8

    if-eqz v8, :cond_4

    .line 105
    invoke-direct {p0}, Lcom/sec/dsm/system/DSMWipeOutService;->externalSdFormat()Z

    move-result v8

    if-eqz v8, :cond_3

    .line 106
    const-string v8, "External SD Format is true"

    invoke-static {v8}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 107
    const/4 v3, 0x1

    .line 116
    :goto_1
    invoke-direct {p0}, Lcom/sec/dsm/system/DSMWipeOutService;->deleteAllMessagesFromSIMCard()Z

    move-result v8

    if-eqz v8, :cond_5

    .line 117
    const-string v8, "Delete all messages from SIMcard is true"

    invoke-static {v8}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 118
    const/4 v5, 0x1

    .line 123
    :goto_2
    invoke-direct {p0}, Lcom/sec/dsm/system/DSMWipeOutService;->deleteAllContactsFromSIMCard()Z

    move-result v8

    if-eqz v8, :cond_6

    .line 124
    const-string v8, "Delete all contacts from SIM card is true"

    invoke-static {v8}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 125
    const/4 v4, 0x1

    .line 130
    :goto_3
    if-ne v5, v10, :cond_0

    if-ne v4, v10, :cond_0

    .line 131
    const/4 v6, 0x1

    .line 132
    :cond_0
    if-ne v3, v10, :cond_7

    if-ne v6, v10, :cond_7

    .line 133
    const-string v8, "wipe all success"

    invoke-static {v8}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 134
    const/16 v7, 0x4b0

    .line 145
    :goto_4
    :try_start_0
    new-instance v2, Lcom/sec/dsm/system/DSMManager;

    sget-object v8, Lcom/sec/dsm/system/DSMWipeOutService;->mcontext:Landroid/content/Context;

    invoke-direct {v2, v8}, Lcom/sec/dsm/system/DSMManager;-><init>(Landroid/content/Context;)V

    .line 146
    .local v2, "im":Lcom/sec/dsm/system/DSMManager;
    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v8}, Lcom/sec/dsm/system/DSMManager;->putDSMWipeOutResult(Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/sec/dsm/system/DSMException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 152
    .end local v2    # "im":Lcom/sec/dsm/system/DSMManager;
    :goto_5
    if-eq v7, v11, :cond_1

    const/16 v8, 0x5b6

    if-ne v7, v8, :cond_2

    .line 153
    :cond_1
    new-instance v8, Landroid/content/Intent;

    const-string v9, "android.intent.action.MEDIA_MOUNTED"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "file://"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v10

    invoke-direct {v8, v9, v10}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {p0, v8}, Lcom/sec/dsm/system/DSMWipeOutService;->sendBroadcast(Landroid/content/Intent;)V

    .line 154
    const-string v8, "media scanning start 1"

    invoke-static {v8}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 156
    :cond_2
    const-string v8, "result"

    invoke-virtual {v1, v8, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 157
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "result is : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 158
    invoke-virtual {v1, v12}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 159
    sget-object v8, Landroid/os/UserHandle;->OWNER:Landroid/os/UserHandle;

    invoke-virtual {p0, v1, v8}, Lcom/sec/dsm/system/DSMWipeOutService;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    .line 160
    invoke-virtual {p0}, Lcom/sec/dsm/system/DSMWipeOutService;->stopSelf()V

    goto/16 :goto_0

    .line 109
    :cond_3
    const-string v8, "External SD Format is false"

    invoke-static {v8}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 110
    const/4 v3, 0x0

    goto/16 :goto_1

    .line 113
    :cond_4
    const/4 v3, 0x0

    .line 114
    const-string v8, "External sdcard not mounted"

    invoke-static {v8}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 120
    :cond_5
    const-string v8, "Delete all messages from SIMcard is false"

    invoke-static {v8}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 121
    const/4 v5, 0x0

    goto/16 :goto_2

    .line 127
    :cond_6
    const-string v8, "Delete all contacts from SIM card is false"

    invoke-static {v8}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 128
    const/4 v4, 0x0

    goto/16 :goto_3

    .line 135
    :cond_7
    if-nez v3, :cond_8

    if-ne v6, v10, :cond_8

    .line 136
    const/16 v7, 0x5b5

    goto/16 :goto_4

    .line 137
    :cond_8
    if-ne v3, v10, :cond_9

    if-nez v6, :cond_9

    .line 138
    const/16 v7, 0x5b6

    goto/16 :goto_4

    .line 139
    :cond_9
    if-nez v3, :cond_a

    if-nez v6, :cond_a

    .line 140
    const/16 v7, 0x5b4

    goto/16 :goto_4

    .line 142
    :cond_a
    const/4 v7, -0x1

    goto/16 :goto_4

    .line 147
    :catch_0
    move-exception v0

    .line 148
    .local v0, "e":Lcom/sec/dsm/system/DSMException;
    invoke-virtual {v0}, Lcom/sec/dsm/system/DSMException;->printStackTrace()V

    goto/16 :goto_5

    .line 149
    .end local v0    # "e":Lcom/sec/dsm/system/DSMException;
    :catch_1
    move-exception v0

    .line 150
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_5

    .line 163
    .end local v0    # "e":Ljava/lang/Exception;
    :sswitch_1
    invoke-direct {p0}, Lcom/sec/dsm/system/DSMWipeOutService;->externalSdExist()Z

    move-result v8

    if-eqz v8, :cond_d

    .line 164
    invoke-direct {p0}, Lcom/sec/dsm/system/DSMWipeOutService;->externalSdFormat()Z

    move-result v8

    if-eqz v8, :cond_c

    .line 165
    const-string v8, "External sdcard Format success"

    invoke-static {v8}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 166
    const/16 v7, 0x4b0

    .line 170
    :goto_6
    const-string v8, "External sd is exist "

    invoke-static {v8}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 176
    :goto_7
    :try_start_1
    new-instance v2, Lcom/sec/dsm/system/DSMManager;

    sget-object v8, Lcom/sec/dsm/system/DSMWipeOutService;->mcontext:Landroid/content/Context;

    invoke-direct {v2, v8}, Lcom/sec/dsm/system/DSMManager;-><init>(Landroid/content/Context;)V

    .line 177
    .restart local v2    # "im":Lcom/sec/dsm/system/DSMManager;
    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v8}, Lcom/sec/dsm/system/DSMManager;->putDSMWipeOutResult(Ljava/lang/String;)V
    :try_end_1
    .catch Lcom/sec/dsm/system/DSMException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3

    .line 183
    .end local v2    # "im":Lcom/sec/dsm/system/DSMManager;
    :goto_8
    if-ne v7, v11, :cond_b

    .line 184
    new-instance v8, Landroid/content/Intent;

    const-string v9, "android.intent.action.MEDIA_MOUNTED"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "file://"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v10

    invoke-direct {v8, v9, v10}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {p0, v8}, Lcom/sec/dsm/system/DSMWipeOutService;->sendBroadcast(Landroid/content/Intent;)V

    .line 185
    const-string v8, "media scanning start 2"

    invoke-static {v8}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 187
    :cond_b
    const-string v8, "result"

    invoke-virtual {v1, v8, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 188
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "result is : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 189
    invoke-virtual {v1, v12}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 190
    sget-object v8, Landroid/os/UserHandle;->OWNER:Landroid/os/UserHandle;

    invoke-virtual {p0, v1, v8}, Lcom/sec/dsm/system/DSMWipeOutService;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    .line 191
    invoke-virtual {p0}, Lcom/sec/dsm/system/DSMWipeOutService;->stopSelf()V

    goto/16 :goto_0

    .line 168
    :cond_c
    const/16 v7, 0x5b5

    goto :goto_6

    .line 172
    :cond_d
    const/16 v7, 0x5b5

    .line 173
    const-string v8, "External sd is not exist "

    invoke-static {v8}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    goto :goto_7

    .line 178
    :catch_2
    move-exception v0

    .line 179
    .local v0, "e":Lcom/sec/dsm/system/DSMException;
    invoke-virtual {v0}, Lcom/sec/dsm/system/DSMException;->printStackTrace()V

    goto :goto_8

    .line 180
    .end local v0    # "e":Lcom/sec/dsm/system/DSMException;
    :catch_3
    move-exception v0

    .line 181
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_8

    .line 194
    .end local v0    # "e":Ljava/lang/Exception;
    :sswitch_2
    invoke-direct {p0}, Lcom/sec/dsm/system/DSMWipeOutService;->deleteAllMessagesFromSIMCard()Z

    move-result v8

    if-eqz v8, :cond_e

    .line 195
    const-string v8, "Delete all messages from SIMcard is true"

    invoke-static {v8}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 196
    const/4 v5, 0x1

    .line 201
    :goto_9
    invoke-direct {p0}, Lcom/sec/dsm/system/DSMWipeOutService;->deleteAllContactsFromSIMCard()Z

    move-result v8

    if-eqz v8, :cond_f

    .line 202
    const-string v8, "Delete all contacts from SIM card is true"

    invoke-static {v8}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 203
    const/4 v4, 0x1

    .line 208
    :goto_a
    if-ne v5, v10, :cond_10

    if-ne v4, v10, :cond_10

    .line 209
    const-string v8, "SIM clear success"

    invoke-static {v8}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 210
    const/16 v7, 0x4b0

    .line 215
    :goto_b
    :try_start_2
    new-instance v2, Lcom/sec/dsm/system/DSMManager;

    sget-object v8, Lcom/sec/dsm/system/DSMWipeOutService;->mcontext:Landroid/content/Context;

    invoke-direct {v2, v8}, Lcom/sec/dsm/system/DSMManager;-><init>(Landroid/content/Context;)V

    .line 216
    .restart local v2    # "im":Lcom/sec/dsm/system/DSMManager;
    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v8}, Lcom/sec/dsm/system/DSMManager;->putDSMWipeOutResult(Ljava/lang/String;)V
    :try_end_2
    .catch Lcom/sec/dsm/system/DSMException; {:try_start_2 .. :try_end_2} :catch_4
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_5

    .line 222
    .end local v2    # "im":Lcom/sec/dsm/system/DSMManager;
    :goto_c
    const-string v8, "result"

    invoke-virtual {v1, v8, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 223
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "result is : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 224
    invoke-virtual {v1, v12}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 225
    sget-object v8, Landroid/os/UserHandle;->OWNER:Landroid/os/UserHandle;

    invoke-virtual {p0, v1, v8}, Lcom/sec/dsm/system/DSMWipeOutService;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    .line 226
    invoke-virtual {p0}, Lcom/sec/dsm/system/DSMWipeOutService;->stopSelf()V

    goto/16 :goto_0

    .line 198
    :cond_e
    const-string v8, "Delete all messages from SIMcard is false"

    invoke-static {v8}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 199
    const/4 v5, 0x0

    goto :goto_9

    .line 205
    :cond_f
    const-string v8, "Delete all contacts from SIM card is false"

    invoke-static {v8}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 206
    const/4 v4, 0x0

    goto :goto_a

    .line 212
    :cond_10
    const/16 v7, 0x5b6

    goto :goto_b

    .line 217
    :catch_4
    move-exception v0

    .line 218
    .local v0, "e":Lcom/sec/dsm/system/DSMException;
    invoke-virtual {v0}, Lcom/sec/dsm/system/DSMException;->printStackTrace()V

    goto :goto_c

    .line 219
    .end local v0    # "e":Lcom/sec/dsm/system/DSMException;
    :catch_5
    move-exception v0

    .line 220
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_c

    .line 229
    .end local v0    # "e":Ljava/lang/Exception;
    :sswitch_3
    invoke-direct {p0}, Lcom/sec/dsm/system/DSMWipeOutService;->FactoryReset()V

    .line 230
    invoke-virtual {p0}, Lcom/sec/dsm/system/DSMWipeOutService;->stopSelf()V

    goto/16 :goto_0

    .line 102
    nop

    :sswitch_data_0
    .sparse-switch
        0xa -> :sswitch_0
        0x14 -> :sswitch_1
        0x1e -> :sswitch_2
        0x28 -> :sswitch_3
    .end sparse-switch
.end method


# virtual methods
.method deleteExtSdCardFiles(Ljava/io/File;)Z
    .locals 7
    .param p1, "file"    # Ljava/io/File;

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 333
    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v6

    if-nez v6, :cond_0

    .line 334
    const-string v4, "deleteExtSdCardFiles 1"

    invoke-static {v4}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 357
    :goto_0
    return v5

    .line 337
    :cond_0
    invoke-virtual {p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    .line 338
    .local v0, "files":[Ljava/io/File;
    if-nez v0, :cond_1

    .line 339
    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    .line 340
    const-string v5, "deleteExtSdCardFiles 2"

    invoke-static {v5}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    move v5, v4

    .line 341
    goto :goto_0

    .line 344
    :cond_1
    const/4 v2, 0x1

    .line 345
    .local v2, "success":Z
    const-string v3, ""

    .line 346
    .local v3, "temp":Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    array-length v6, v0

    if-ge v1, v6, :cond_7

    .line 347
    aget-object v6, v0, v1

    invoke-virtual {v6}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    .line 348
    const-string v6, ".android_secure"

    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_3

    const-string v6, "android_secure"

    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_3

    const-string v6, "autorun.inf"

    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    aget-object v6, v0, v1

    invoke-virtual {v6}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_3

    .line 349
    :cond_2
    aget-object v6, v0, v1

    invoke-virtual {v6}, Ljava/io/File;->isDirectory()Z

    move-result v6

    if-eqz v6, :cond_5

    .line 350
    if-eqz v2, :cond_4

    aget-object v6, v0, v1

    invoke-virtual {p0, v6}, Lcom/sec/dsm/system/DSMWipeOutService;->deleteExtSdCardFiles(Ljava/io/File;)Z

    move-result v6

    if-eqz v6, :cond_4

    move v2, v4

    .line 346
    :cond_3
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_4
    move v2, v5

    .line 350
    goto :goto_2

    .line 352
    :cond_5
    if-eqz v2, :cond_6

    aget-object v6, v0, v1

    invoke-virtual {v6}, Ljava/io/File;->delete()Z

    move-result v6

    if-eqz v6, :cond_6

    move v2, v4

    :goto_3
    goto :goto_2

    :cond_6
    move v2, v5

    goto :goto_3

    .line 356
    :cond_7
    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    move v5, v2

    .line 357
    goto :goto_0
.end method

.method deleteIntSdCardFiles(Ljava/io/File;)Z
    .locals 10
    .param p1, "file"    # Ljava/io/File;

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 306
    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v8

    if-nez v8, :cond_1

    .line 329
    :cond_0
    :goto_0
    return v7

    .line 309
    :cond_1
    invoke-virtual {p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    .line 310
    .local v0, "files":[Ljava/io/File;
    if-eqz v0, :cond_0

    .line 313
    const-string v2, "sdcard"

    .line 314
    .local v2, "mSdCard":Ljava/lang/String;
    const/4 v5, 0x1

    .line 315
    .local v5, "success":Z
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    array-length v8, v0

    if-ge v1, v8, :cond_6

    .line 316
    aget-object v8, v0, v1

    invoke-virtual {v8}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v3

    .line 317
    .local v3, "path":Ljava/lang/String;
    const-string v8, "/"

    invoke-virtual {v3, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 319
    .local v4, "pathlist":[Ljava/lang/String;
    aget-object v8, v4, v6

    invoke-virtual {v8, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    const/4 v8, 0x2

    aget-object v8, v4, v8

    const-string v9, "external_sd"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 315
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 321
    :cond_2
    aget-object v8, v0, v1

    invoke-virtual {v8}, Ljava/io/File;->isDirectory()Z

    move-result v8

    if-eqz v8, :cond_4

    .line 322
    if-eqz v5, :cond_3

    aget-object v8, v0, v1

    invoke-virtual {p0, v8}, Lcom/sec/dsm/system/DSMWipeOutService;->deleteIntSdCardFiles(Ljava/io/File;)Z

    move-result v8

    if-eqz v8, :cond_3

    move v5, v6

    :goto_3
    goto :goto_2

    :cond_3
    move v5, v7

    goto :goto_3

    .line 324
    :cond_4
    if-eqz v5, :cond_5

    aget-object v8, v0, v1

    invoke-virtual {v8}, Ljava/io/File;->delete()Z

    move-result v8

    if-eqz v8, :cond_5

    move v5, v6

    :goto_4
    goto :goto_2

    :cond_5
    move v5, v7

    goto :goto_4

    .line 328
    .end local v3    # "path":Ljava/lang/String;
    .end local v4    # "pathlist":[Ljava/lang/String;
    :cond_6
    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    move v7, v5

    .line 329
    goto :goto_0
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 92
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 73
    const-string v0, " onCreate begin"

    invoke-static {v0}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 74
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 75
    sput-object p0, Lcom/sec/dsm/system/DSMWipeOutService;->mcontext:Landroid/content/Context;

    .line 76
    sget-object v0, Lcom/sec/dsm/system/DSMWipeOutService;->mcontext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/dsm/system/DSMWipeOutService;->mContentResolver:Landroid/content/ContentResolver;

    .line 77
    sget-object v0, Lcom/sec/dsm/system/DSMWipeOutService;->mcontext:Landroid/content/Context;

    const-string v0, "appops"

    invoke-virtual {p0, v0}, Lcom/sec/dsm/system/DSMWipeOutService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AppOpsManager;

    iput-object v0, p0, Lcom/sec/dsm/system/DSMWipeOutService;->mAppOpsManager:Landroid/app/AppOpsManager;

    .line 78
    const-string v0, " onCreate end"

    invoke-static {v0}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 79
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    const/4 v3, 0x2

    .line 82
    if-nez p1, :cond_0

    .line 88
    :goto_0
    return v3

    .line 85
    :cond_0
    const-string v1, " onStartCommand begin"

    invoke-static {v1}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 86
    const-string v1, "status"

    const/16 v2, 0x64

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 87
    .local v0, "status":I
    invoke-direct {p0, v0}, Lcom/sec/dsm/system/DSMWipeOutService;->wipeOut(I)V

    goto :goto_0
.end method
