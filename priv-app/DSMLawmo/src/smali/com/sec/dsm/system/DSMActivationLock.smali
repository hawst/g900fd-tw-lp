.class public Lcom/sec/dsm/system/DSMActivationLock;
.super Ljava/lang/Object;
.source "DSMActivationLock.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/dsm/system/DSMActivationLock$uncaughtExceptionHandler;
    }
.end annotation


# static fields
.field private static loadSuccess:Z

.field private static rsm:Lcom/samsung/android/service/reactive/ReactiveServiceManager;

.field private static singleton:Lcom/sec/dsm/system/DSMActivationLock;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/dsm/system/DSMActivationLock;->singleton:Lcom/sec/dsm/system/DSMActivationLock;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    const-string v0, "[DSMActivationLock]Construct"

    invoke-static {v0}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 24
    new-instance v0, Lcom/samsung/android/service/reactive/ReactiveServiceManager;

    invoke-direct {v0, p1}, Lcom/samsung/android/service/reactive/ReactiveServiceManager;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/dsm/system/DSMActivationLock;->rsm:Lcom/samsung/android/service/reactive/ReactiveServiceManager;

    .line 25
    return-void
.end method

.method private native getRequiredAuthFlag()I
.end method

.method private native getStolenFlag()I
.end method

.method public static isActivationLocked()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 29
    sget-object v1, Lcom/sec/dsm/system/DSMActivationLock;->rsm:Lcom/samsung/android/service/reactive/ReactiveServiceManager;

    invoke-virtual {v1}, Lcom/samsung/android/service/reactive/ReactiveServiceManager;->getServiceSupport()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 47
    :goto_0
    :pswitch_0
    return v0

    .line 33
    :pswitch_1
    const-string v1, "[DSMActivationLock]Reactivation lock service is not supported."

    invoke-static {v1}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    goto :goto_0

    .line 37
    :pswitch_2
    invoke-static {}, Lcom/sec/dsm/system/DSMActivationLock;->readReactivationFlag()I

    move-result v1

    packed-switch v1, :pswitch_data_1

    goto :goto_0

    .line 41
    :pswitch_3
    const/4 v0, 0x1

    goto :goto_0

    .line 29
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    .line 37
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public static readReactivationFlag()I
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 52
    const-string v3, "[DSMActivationLock]read Reactivation Lock Flag"

    invoke-static {v3}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 53
    sget-object v3, Lcom/sec/dsm/system/DSMActivationLock;->rsm:Lcom/samsung/android/service/reactive/ReactiveServiceManager;

    invoke-virtual {v3, v1}, Lcom/samsung/android/service/reactive/ReactiveServiceManager;->getFlag(I)I

    move-result v0

    .line 55
    .local v0, "flagResult":I
    if-nez v0, :cond_0

    .line 57
    const-string v2, "[DSMActivationLock]flag deactivated."

    invoke-static {v2}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 67
    :goto_0
    return v1

    .line 60
    :cond_0
    if-ne v0, v2, :cond_1

    .line 61
    const-string v1, "[DSMActivationLock]flag activated"

    invoke-static {v1}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    move v1, v2

    .line 62
    goto :goto_0

    .line 66
    :cond_1
    const-string v1, "[DSMActivationLock]read reactivation lock exception."

    invoke-static {v1}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 67
    const/4 v1, -0x1

    goto :goto_0
.end method

.method private native setStolenFlagOn()I
.end method
