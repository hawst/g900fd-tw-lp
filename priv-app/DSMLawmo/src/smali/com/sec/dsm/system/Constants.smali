.class public Lcom/sec/dsm/system/Constants;
.super Ljava/lang/Object;
.source "Constants.java"


# static fields
.field public static OSPS_MOBILE_TRACKING_EXT_STATUS_AMT_OFF:Ljava/lang/String; = null

.field public static OSPS_MOBILE_TRACKING_EXT_STATUS_STOP:Ljava/lang/String; = null

.field public static OSPS_MOBILE_TRACKING_EXT_STATUS_TRACKING:Ljava/lang/String; = null

.field public static OSPS_MOBILE_TRACKING_OPERATION_DONE:I = 0x0

.field public static OSPS_MOBILE_TRACKING_OPERATION_GET_LOCATION:I = 0x0

.field public static OSPS_MOBILE_TRACKING_OPERATION_NONE:I = 0x0

.field public static OSPS_MOBILE_TRACKING_OPERATION_STOP:I = 0x0

.field public static OSPS_MOBILE_TRACKING_OPERATION_TRACKING:I = 0x0

.field public static OSPS_MOBILE_TRACKING_OPERATION_TRACKING_CONTINUE:I = 0x0

.field public static OSPS_MOBILE_TRACKING_OPERATION_TRACKING_DONE:I = 0x0

.field public static OSPS_MOBILE_TRACKING_OPERATION_TRACKING_OK:I = 0x0

.field public static final TAG:Ljava/lang/String; = "DSM"


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 5
    sput v1, Lcom/sec/dsm/system/Constants;->OSPS_MOBILE_TRACKING_OPERATION_NONE:I

    .line 6
    sput v2, Lcom/sec/dsm/system/Constants;->OSPS_MOBILE_TRACKING_OPERATION_GET_LOCATION:I

    .line 7
    sput v3, Lcom/sec/dsm/system/Constants;->OSPS_MOBILE_TRACKING_OPERATION_TRACKING:I

    .line 8
    const/4 v0, 0x3

    sput v0, Lcom/sec/dsm/system/Constants;->OSPS_MOBILE_TRACKING_OPERATION_STOP:I

    .line 9
    const/16 v0, 0xa

    sput v0, Lcom/sec/dsm/system/Constants;->OSPS_MOBILE_TRACKING_OPERATION_DONE:I

    .line 11
    sput v1, Lcom/sec/dsm/system/Constants;->OSPS_MOBILE_TRACKING_OPERATION_TRACKING_DONE:I

    .line 12
    sput v2, Lcom/sec/dsm/system/Constants;->OSPS_MOBILE_TRACKING_OPERATION_TRACKING_OK:I

    .line 13
    sput v3, Lcom/sec/dsm/system/Constants;->OSPS_MOBILE_TRACKING_OPERATION_TRACKING_CONTINUE:I

    .line 16
    const-string v0, "10"

    sput-object v0, Lcom/sec/dsm/system/Constants;->OSPS_MOBILE_TRACKING_EXT_STATUS_STOP:Ljava/lang/String;

    .line 17
    const-string v0, "20"

    sput-object v0, Lcom/sec/dsm/system/Constants;->OSPS_MOBILE_TRACKING_EXT_STATUS_TRACKING:Ljava/lang/String;

    .line 18
    const-string v0, "30"

    sput-object v0, Lcom/sec/dsm/system/Constants;->OSPS_MOBILE_TRACKING_EXT_STATUS_AMT_OFF:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
