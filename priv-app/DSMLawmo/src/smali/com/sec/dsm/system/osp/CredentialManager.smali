.class public Lcom/sec/dsm/system/osp/CredentialManager;
.super Ljava/lang/Object;
.source "CredentialManager.java"


# instance fields
.field private credentialRepository:Lcom/sec/dsm/system/osp/CredentialRepository;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/dsm/system/osp/CredentialException;
        }
    .end annotation

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    :try_start_0
    new-instance v1, Lcom/sec/dsm/system/osp/CredentialRepository;

    invoke-direct {v1, p1}, Lcom/sec/dsm/system/osp/CredentialRepository;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/dsm/system/osp/CredentialManager;->credentialRepository:Lcom/sec/dsm/system/osp/CredentialRepository;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 32
    return-void

    .line 28
    :catch_0
    move-exception v0

    .line 29
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 30
    new-instance v1, Lcom/sec/dsm/system/osp/CredentialException;

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/sec/dsm/system/osp/CredentialException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method public clearCredentials()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/dsm/system/osp/CredentialException;
        }
    .end annotation

    .prologue
    .line 129
    iget-object v0, p0, Lcom/sec/dsm/system/osp/CredentialManager;->credentialRepository:Lcom/sec/dsm/system/osp/CredentialRepository;

    invoke-virtual {v0}, Lcom/sec/dsm/system/osp/CredentialRepository;->clearCredentials()V

    .line 130
    return-void
.end method

.method public containsAccessToken(Ljava/lang/String;)Z
    .locals 6
    .param p1, "appID"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/dsm/system/osp/CredentialException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    .line 139
    if-nez p1, :cond_0

    .line 140
    new-instance v4, Ljava/lang/IllegalArgumentException;

    invoke-direct {v4}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v4

    .line 143
    :cond_0
    const/4 v3, 0x0

    .line 146
    .local v3, "ret":Z
    const/4 v1, 0x0

    .line 147
    .local v1, "oauthToken":Ljava/lang/String;
    const/4 v2, 0x0

    .line 149
    .local v2, "oauthTokenSecret":Ljava/lang/String;
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/sec/dsm/system/osp/CredentialManager;->getOauthToken(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 150
    invoke-virtual {p0, p1}, Lcom/sec/dsm/system/osp/CredentialManager;->getOauthTokenSecret(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 152
    if-eqz v1, :cond_1

    if-eqz v2, :cond_1

    const-string v4, ""

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eq v4, v5, :cond_1

    const-string v4, ""

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    if-ne v4, v5, :cond_2

    .line 155
    :cond_1
    const/4 v3, 0x0

    .line 164
    :goto_0
    return v3

    .line 157
    :cond_2
    const/4 v3, 0x1

    goto :goto_0

    .line 159
    :catch_0
    move-exception v0

    .line 160
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 161
    new-instance v4, Lcom/sec/dsm/system/osp/CredentialException;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5, v0}, Lcom/sec/dsm/system/osp/CredentialException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v4
.end method

.method public getAppSecret(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "appID"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/dsm/system/osp/CredentialException;
        }
    .end annotation

    .prologue
    .line 41
    if-nez p1, :cond_0

    .line 42
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 45
    :cond_0
    iget-object v0, p0, Lcom/sec/dsm/system/osp/CredentialManager;->credentialRepository:Lcom/sec/dsm/system/osp/CredentialRepository;

    invoke-virtual {v0, p1}, Lcom/sec/dsm/system/osp/CredentialRepository;->getAppSecret(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getOauthToken(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "appID"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/dsm/system/osp/CredentialException;
        }
    .end annotation

    .prologue
    .line 55
    if-nez p1, :cond_0

    .line 56
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 59
    :cond_0
    iget-object v0, p0, Lcom/sec/dsm/system/osp/CredentialManager;->credentialRepository:Lcom/sec/dsm/system/osp/CredentialRepository;

    invoke-virtual {v0, p1}, Lcom/sec/dsm/system/osp/CredentialRepository;->getOauthToken(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getOauthTokenSecret(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "appID"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/dsm/system/osp/CredentialException;
        }
    .end annotation

    .prologue
    .line 69
    if-nez p1, :cond_0

    .line 70
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 73
    :cond_0
    iget-object v0, p0, Lcom/sec/dsm/system/osp/CredentialManager;->credentialRepository:Lcom/sec/dsm/system/osp/CredentialRepository;

    invoke-virtual {v0, p1}, Lcom/sec/dsm/system/osp/CredentialRepository;->getOauthTokenSecret(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public initializeCredential(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "appID"    # Ljava/lang/String;
    .param p2, "appSecret"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/dsm/system/osp/CredentialException;
        }
    .end annotation

    .prologue
    .line 85
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 86
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 89
    :cond_1
    iget-object v0, p0, Lcom/sec/dsm/system/osp/CredentialManager;->credentialRepository:Lcom/sec/dsm/system/osp/CredentialRepository;

    invoke-virtual {v0, p1, p2}, Lcom/sec/dsm/system/osp/CredentialRepository;->initializeCredential(Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    return-void
.end method

.method public removeCredential(Ljava/lang/String;)V
    .locals 1
    .param p1, "appID"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/dsm/system/osp/CredentialException;
        }
    .end annotation

    .prologue
    .line 116
    if-nez p1, :cond_0

    .line 117
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 120
    :cond_0
    iget-object v0, p0, Lcom/sec/dsm/system/osp/CredentialManager;->credentialRepository:Lcom/sec/dsm/system/osp/CredentialRepository;

    invoke-virtual {v0, p1}, Lcom/sec/dsm/system/osp/CredentialRepository;->removeCredential(Ljava/lang/String;)V

    .line 121
    return-void
.end method

.method public updateCredential(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "appID"    # Ljava/lang/String;
    .param p2, "token"    # Ljava/lang/String;
    .param p3, "tokenSecret"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/dsm/system/osp/CredentialException;
        }
    .end annotation

    .prologue
    .line 102
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    if-nez p3, :cond_1

    .line 103
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 106
    :cond_1
    iget-object v0, p0, Lcom/sec/dsm/system/osp/CredentialManager;->credentialRepository:Lcom/sec/dsm/system/osp/CredentialRepository;

    invoke-virtual {v0, p1, p2, p3}, Lcom/sec/dsm/system/osp/CredentialRepository;->updateCredential(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 107
    return-void
.end method
