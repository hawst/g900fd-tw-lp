.class final Lcom/sec/dsm/system/DSMGetLocationService$1;
.super Lcom/sec/dsm/system/DSMGpsListener;
.source "DSMGetLocationService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/dsm/system/DSMGetLocationService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 117
    invoke-direct {p0}, Lcom/sec/dsm/system/DSMGpsListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceiveData(Landroid/location/Location;)V
    .locals 11
    .param p1, "location"    # Landroid/location/Location;

    .prologue
    .line 121
    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v6

    .line 122
    .local v6, "latitute":D
    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v8

    .line 123
    .local v8, "longitude":D
    invoke-virtual {p1}, Landroid/location/Location;->getAltitude()D

    move-result-wide v2

    .line 124
    .local v2, "Altitute":D
    invoke-virtual {p1}, Landroid/location/Location;->getBearing()F

    move-result v1

    .line 125
    .local v1, "Bearing":F
    invoke-virtual {p1}, Landroid/location/Location;->getSpeed()F

    move-result v4

    .line 126
    .local v4, "Speed":F
    invoke-virtual {p1}, Landroid/location/Location;->getAccuracy()F

    move-result v0

    .line 128
    .local v0, "Accuracy":F
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "GPS data: lat="

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v10, ", lon="

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v8, v9}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v10, ", alt="

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v10, ", bearing="

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v10, ", speed="

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v10, "accura"

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 130
    invoke-static {}, Lcom/sec/dsm/system/DSMGetLocationService;->DSMRemoveListener()V

    .line 131
    invoke-static {p1}, Lcom/sec/dsm/system/DSMGpsManager;->DSMGpsSetLocation(Landroid/location/Location;)V

    .line 132
    # getter for: Lcom/sec/dsm/system/DSMGetLocationService;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/sec/dsm/system/DSMGetLocationService;->access$000()Landroid/content/Context;

    move-result-object v5

    const/4 v10, 0x1

    invoke-static {v5, p1, v10}, Lcom/sec/dsm/system/DSMAdapter;->DSMAdpSetGetLocation(Landroid/content/Context;Landroid/location/Location;Z)V

    .line 133
    return-void
.end method

.method public onTimeout()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 138
    const-string v0, "GpsLocListener Time Out"

    invoke-static {v0}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 140
    sget-object v0, Lcom/sec/dsm/system/DSMGetLocationService;->lm:Landroid/location/LocationManager;

    if-eqz v0, :cond_1

    .line 141
    sget-object v0, Lcom/sec/dsm/system/DSMGetLocationService;->lm:Landroid/location/LocationManager;

    sget-object v1, Lcom/sec/dsm/system/DSMGetLocationService;->DSMGpsLocListener:Lcom/sec/dsm/system/DSMGpsListener;

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    .line 144
    :goto_0
    sput-boolean v2, Lcom/sec/dsm/system/DSMGetLocationService;->getGPSLoc:Z

    .line 146
    sget-boolean v0, Lcom/sec/dsm/system/DSMGetLocationService;->getNetLoc:Z

    if-nez v0, :cond_0

    .line 148
    invoke-static {}, Lcom/sec/dsm/system/DSMGetLocationService;->DSMRemoveListener()V

    .line 150
    :cond_0
    # getter for: Lcom/sec/dsm/system/DSMGetLocationService;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/sec/dsm/system/DSMGetLocationService;->access$000()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/dsm/system/DSMAdapter;->DSMAdpSetGetLocation(Landroid/content/Context;Landroid/location/Location;Z)V

    .line 151
    return-void

    .line 143
    :cond_1
    const-string v0, "LocationManager is null"

    invoke-static {v0}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    goto :goto_0
.end method
