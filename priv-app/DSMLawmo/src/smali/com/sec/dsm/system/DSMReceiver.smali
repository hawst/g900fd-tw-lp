.class public Lcom/sec/dsm/system/DSMReceiver;
.super Landroid/content/BroadcastReceiver;
.source "DSMReceiver.java"


# static fields
.field public static final DEFAULT_NUMBER:Ljava/lang/String; = "111111111111111"

.field public static final DSM_BROADCAST_FACTORY_RECEIVED:Ljava/lang/String; = "android.intent.action.dsm.DM_FACTORY_RESET"

.field public static final DSM_BROADCAST_LOCATION_RECEIVED:Ljava/lang/String; = "android.intent.action.dsm.DM_FIND_MY_PHONE"

.field public static final DSM_BROADCAST_LOCK_RECEIVED:Ljava/lang/String; = "android.intent.action.dsm.DM_LOCK_MY_PHONE"

.field public static final DSM_BROADCAST_RING_RECEIVED:Ljava/lang/String; = "android.intent.action.dsm.DM_RING_MY_PHONE"

.field public static final DSM_BROADCAST_SET_LOCK:Ljava/lang/String; = "com.android.internal.policy.impl.Keyguard.PCW_LOCKED"

.field public static final DSM_BROADCAST_SET_RING:Ljava/lang/String; = "android.intent.action.PCW_RINGTONE_START"

.field public static final DSM_BROADCAST_SET_UNLOCK:Ljava/lang/String; = "com.android.internal.policy.impl.Keyguard.PCW_UNLOCKED"

.field public static final DSM_BROADCAST_TRACKING_START_RECEIVED:Ljava/lang/String; = "android.intent.action.dsm.DM_MOBILE_TRACKING_START"

.field public static final DSM_BROADCAST_TRACKING_STOP_RECEIVED:Ljava/lang/String; = "android.intent.action.dsm.DM_MOBILE_TRACKING_STOP"

.field public static final DSM_BROADCAST_UNLOCK_RECEIVED:Ljava/lang/String; = "android.intent.action.dsm.DM_LOCK_RELEASE"

.field public static final DSM_BROADCAST_WIPE_RECEIVED:Ljava/lang/String; = "android.intent.action.dsm.DM_WIPE"

.field public static final DSM_REMOTECONTROL_OFF:Ljava/lang/String; = "android.intent.action.REMOTE_CONTROL_OFF"

.field public static final DSM_SAMSUNGACCOUNT_REMOVE_COMPLETE:Ljava/lang/String; = "osp.signin.SAMSUNG_ACCOUNT_SIGNOUT"

.field public static final DSM_SAMSUNGACCOUNT_SIGNOUT_RESULT1:Ljava/lang/String; = "android.intent.action.SAMSUNG_ACCOUNT_SIGNOUT_FAILED"

.field public static final DSM_SAMSUNGACCOUNT_SIGNOUT_RESULT2:Ljava/lang/String; = "android.intent.action.SAMSUNG_ACCOUNT_SIGNOUT_SUCCESS"

.field public static final DSM_TRACKING_SERVICE:Ljava/lang/String; = "android.intent.action.dsm.TRACKING_SERVICE"

.field public static final DSM_WIPEOUT_SERVICE:Ljava/lang/String; = "android.intent.action.dsm.WIPEOUT_SERVICE"

.field public static final KEYGUARD_SERVICE:Ljava/lang/String; = "keyguard"

.field public static final SETTINGS_LOCATION_SECURITY:Ljava/lang/String; = "android.settings.SECURITY_SETTINGS"

.field public static final SETTINGS_PACKAGE_NAME:Ljava/lang/String; = "com.android.settings"

.field public static final SETTINGS_SCREEN_UNLOCK:Ljava/lang/String; = "com.android.settings.ChooseLockGeneric"

.field private static m_BackupIndicatorState:I

.field private static mcontext:Landroid/content/Context;


# instance fields
.field private interval:Ljava/lang/String;

.field private mKeyguardLock:Landroid/app/KeyguardManager$KeyguardLock;

.field private mKeyguardManager:Landroid/app/KeyguardManager;

.field private mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

.field private message_ring:Ljava/lang/String;

.field private nm:Landroid/app/NotificationManager;

.field phone:Lcom/android/internal/telephony/Phone;

.field private starttime:Ljava/lang/String;

.field private stoptime:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const/4 v0, 0x0

    sput v0, Lcom/sec/dsm/system/DSMReceiver;->m_BackupIndicatorState:I

    .line 30
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/dsm/system/DSMReceiver;->mcontext:Landroid/content/Context;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 20
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/dsm/system/DSMReceiver;->starttime:Ljava/lang/String;

    .line 21
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/dsm/system/DSMReceiver;->stoptime:Ljava/lang/String;

    .line 22
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/dsm/system/DSMReceiver;->interval:Ljava/lang/String;

    .line 23
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/dsm/system/DSMReceiver;->message_ring:Ljava/lang/String;

    .line 24
    iput-object v1, p0, Lcom/sec/dsm/system/DSMReceiver;->mKeyguardManager:Landroid/app/KeyguardManager;

    .line 25
    iput-object v1, p0, Lcom/sec/dsm/system/DSMReceiver;->mKeyguardLock:Landroid/app/KeyguardManager$KeyguardLock;

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 30
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 56
    sput-object p1, Lcom/sec/dsm/system/DSMReceiver;->mcontext:Landroid/content/Context;

    .line 57
    const-string v26, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_9

    .line 59
    :try_start_0
    new-instance v9, Lcom/sec/dsm/system/DSMManager;

    sget-object v26, Lcom/sec/dsm/system/DSMReceiver;->mcontext:Landroid/content/Context;

    move-object/from16 v0, v26

    invoke-direct {v9, v0}, Lcom/sec/dsm/system/DSMManager;-><init>(Landroid/content/Context;)V

    .line 60
    .local v9, "im":Lcom/sec/dsm/system/DSMManager;
    const-string v26, "phone"

    move-object/from16 v0, p1

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Landroid/telephony/TelephonyManager;

    .line 61
    .local v21, "telephony":Landroid/telephony/TelephonyManager;
    invoke-virtual/range {v21 .. v21}, Landroid/telephony/TelephonyManager;->getSubscriberId()Ljava/lang/String;

    move-result-object v18

    .line 62
    .local v18, "sId":Ljava/lang/String;
    invoke-virtual {v9}, Lcom/sec/dsm/system/DSMManager;->containsSMSForwarding()Z

    move-result v26

    if-eqz v26, :cond_0

    .line 63
    invoke-static/range {v18 .. v18}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v26

    if-eqz v26, :cond_5

    .line 64
    const-string v26, "No SIM mode"

    invoke-static/range {v26 .. v26}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 80
    :cond_0
    :goto_0
    invoke-virtual {v9}, Lcom/sec/dsm/system/DSMManager;->containsDSMRing()Z

    move-result v26

    if-eqz v26, :cond_2

    .line 81
    const-string v26, "Restart ring service"

    invoke-static/range {v26 .. v26}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 82
    new-instance v8, Landroid/content/Intent;

    sget-object v26, Lcom/sec/dsm/system/DSMReceiver;->mcontext:Landroid/content/Context;

    const-class v27, Lcom/sec/dsm/system/DSMRingMyMobile;

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    invoke-direct {v8, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 83
    .local v8, "i":Landroid/content/Intent;
    invoke-virtual {v9}, Lcom/sec/dsm/system/DSMManager;->containsDSMRingMessage()Z

    move-result v26

    if-eqz v26, :cond_1

    .line 84
    const-string v26, "message"

    invoke-virtual {v9}, Lcom/sec/dsm/system/DSMManager;->getDSMRingMessage()Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    invoke-virtual {v8, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 87
    :cond_1
    const/high16 v26, 0x10000000

    move/from16 v0, v26

    invoke-virtual {v8, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 88
    sget-object v26, Lcom/sec/dsm/system/DSMReceiver;->mcontext:Landroid/content/Context;

    move-object/from16 v0, v26

    invoke-virtual {v0, v8}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 90
    .end local v8    # "i":Landroid/content/Intent;
    :cond_2
    invoke-virtual {v9}, Lcom/sec/dsm/system/DSMManager;->containsDSMTracking()Z

    move-result v26

    if-eqz v26, :cond_3

    .line 91
    new-instance v8, Landroid/content/Intent;

    sget-object v26, Lcom/sec/dsm/system/DSMReceiver;->mcontext:Landroid/content/Context;

    const-class v27, Lcom/sec/dsm/system/DSMTrackingService;

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    invoke-direct {v8, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 92
    .restart local v8    # "i":Landroid/content/Intent;
    invoke-virtual {v9}, Lcom/sec/dsm/system/DSMManager;->getDSMStartTime()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/dsm/system/DSMReceiver;->starttime:Ljava/lang/String;

    .line 93
    invoke-virtual {v9}, Lcom/sec/dsm/system/DSMManager;->getDSMStopTime()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/dsm/system/DSMReceiver;->stoptime:Ljava/lang/String;

    .line 94
    invoke-virtual {v9}, Lcom/sec/dsm/system/DSMManager;->getDSMInterval()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/dsm/system/DSMReceiver;->interval:Ljava/lang/String;

    .line 95
    const-string v26, "starttime"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/dsm/system/DSMReceiver;->starttime:Ljava/lang/String;

    move-object/from16 v27, v0

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    invoke-virtual {v8, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 96
    const-string v26, "stoptime"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/dsm/system/DSMReceiver;->stoptime:Ljava/lang/String;

    move-object/from16 v27, v0

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    invoke-virtual {v8, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 97
    const-string v26, "interval"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/dsm/system/DSMReceiver;->interval:Ljava/lang/String;

    move-object/from16 v27, v0

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    invoke-virtual {v8, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 98
    const-string v26, "Restart Tracking service"

    invoke-static/range {v26 .. v26}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 99
    sget-object v26, Lcom/sec/dsm/system/DSMReceiver;->mcontext:Landroid/content/Context;

    move-object/from16 v0, v26

    invoke-virtual {v0, v8}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 101
    .end local v8    # "i":Landroid/content/Intent;
    :cond_3
    invoke-virtual {v9}, Lcom/sec/dsm/system/DSMManager;->containsDSMWipeOut()Z

    move-result v26

    if-eqz v26, :cond_8

    .line 102
    invoke-virtual {v9}, Lcom/sec/dsm/system/DSMManager;->getDSMWipeOut()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v26 .. v26}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v19

    .line 103
    .local v19, "status":I
    new-instance v8, Landroid/content/Intent;

    sget-object v26, Lcom/sec/dsm/system/DSMReceiver;->mcontext:Landroid/content/Context;

    const-class v27, Lcom/sec/dsm/system/DSMWipeOutService;

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    invoke-direct {v8, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 104
    .restart local v8    # "i":Landroid/content/Intent;
    const-string v26, "status"

    move-object/from16 v0, v26

    move/from16 v1, v19

    invoke-virtual {v8, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 105
    const-string v26, "Restart wipeout service"

    invoke-static/range {v26 .. v26}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 106
    sget-object v26, Lcom/sec/dsm/system/DSMReceiver;->mcontext:Landroid/content/Context;

    move-object/from16 v0, v26

    invoke-virtual {v0, v8}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
    :try_end_0
    .catch Lcom/sec/dsm/system/DSMException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 115
    .end local v8    # "i":Landroid/content/Intent;
    .end local v9    # "im":Lcom/sec/dsm/system/DSMManager;
    .end local v18    # "sId":Ljava/lang/String;
    .end local v19    # "status":I
    .end local v21    # "telephony":Landroid/telephony/TelephonyManager;
    :goto_1
    const-string v26, "Boot completed"

    invoke-static/range {v26 .. v26}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 355
    :cond_4
    :goto_2
    return-void

    .line 66
    .restart local v9    # "im":Lcom/sec/dsm/system/DSMManager;
    .restart local v18    # "sId":Ljava/lang/String;
    .restart local v21    # "telephony":Landroid/telephony/TelephonyManager;
    :cond_5
    :try_start_1
    invoke-virtual {v9}, Lcom/sec/dsm/system/DSMManager;->getDSMIMSI()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_6

    .line 67
    const-string v26, "Restart SMS forwarding service "

    invoke-static/range {v26 .. v26}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V
    :try_end_1
    .catch Lcom/sec/dsm/system/DSMException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_0

    .line 110
    .end local v9    # "im":Lcom/sec/dsm/system/DSMManager;
    .end local v18    # "sId":Ljava/lang/String;
    .end local v21    # "telephony":Landroid/telephony/TelephonyManager;
    :catch_0
    move-exception v7

    .line 111
    .local v7, "e":Lcom/sec/dsm/system/DSMException;
    invoke-virtual {v7}, Lcom/sec/dsm/system/DSMException;->printStackTrace()V

    goto :goto_1

    .line 69
    .end local v7    # "e":Lcom/sec/dsm/system/DSMException;
    .restart local v9    # "im":Lcom/sec/dsm/system/DSMManager;
    .restart local v18    # "sId":Ljava/lang/String;
    .restart local v21    # "telephony":Landroid/telephony/TelephonyManager;
    :cond_6
    :try_start_2
    invoke-virtual {v9}, Lcom/sec/dsm/system/DSMManager;->getDSMIMSI()Ljava/lang/String;

    move-result-object v26

    const-string v27, "111111111111111"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_7

    .line 70
    invoke-virtual {v9}, Lcom/sec/dsm/system/DSMManager;->removeSMSForwarding()V

    .line 76
    :goto_3
    const-string v26, "State of SIM change,SMS forwarding service stop"

    invoke-static/range {v26 .. v26}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V
    :try_end_2
    .catch Lcom/sec/dsm/system/DSMException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_0

    .line 112
    .end local v9    # "im":Lcom/sec/dsm/system/DSMManager;
    .end local v18    # "sId":Ljava/lang/String;
    .end local v21    # "telephony":Landroid/telephony/TelephonyManager;
    :catch_1
    move-exception v7

    .line 113
    .local v7, "e":Ljava/lang/Exception;
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 72
    .end local v7    # "e":Ljava/lang/Exception;
    .restart local v9    # "im":Lcom/sec/dsm/system/DSMManager;
    .restart local v18    # "sId":Ljava/lang/String;
    .restart local v21    # "telephony":Landroid/telephony/TelephonyManager;
    :cond_7
    :try_start_3
    invoke-virtual {v9}, Lcom/sec/dsm/system/DSMManager;->removeSMSForwarding()V

    .line 73
    invoke-virtual {v9}, Lcom/sec/dsm/system/DSMManager;->removeDSMIMSI()V

    .line 74
    const-string v26, "111111111111111"

    move-object/from16 v0, v26

    invoke-virtual {v9, v0}, Lcom/sec/dsm/system/DSMManager;->putDSMIMSI(Ljava/lang/String;)V

    goto :goto_3

    .line 108
    :cond_8
    const-string v26, "Normal booted"

    invoke-static/range {v26 .. v26}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V
    :try_end_3
    .catch Lcom/sec/dsm/system/DSMException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_1

    .line 116
    .end local v9    # "im":Lcom/sec/dsm/system/DSMManager;
    .end local v18    # "sId":Ljava/lang/String;
    .end local v21    # "telephony":Landroid/telephony/TelephonyManager;
    :cond_9
    const-string v26, "osp.signin.SAMSUNG_ACCOUNT_SIGNOUT"

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_a

    .line 117
    const-string v26, "Samsung Account is removed"

    invoke-static/range {v26 .. v26}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 119
    :try_start_4
    new-instance v9, Lcom/sec/dsm/system/DSMManager;

    sget-object v26, Lcom/sec/dsm/system/DSMReceiver;->mcontext:Landroid/content/Context;

    move-object/from16 v0, v26

    invoke-direct {v9, v0}, Lcom/sec/dsm/system/DSMManager;-><init>(Landroid/content/Context;)V

    .line 120
    .restart local v9    # "im":Lcom/sec/dsm/system/DSMManager;
    invoke-virtual {v9}, Lcom/sec/dsm/system/DSMManager;->removeInitialization()V
    :try_end_4
    .catch Lcom/sec/dsm/system/DSMException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3

    .line 126
    .end local v9    # "im":Lcom/sec/dsm/system/DSMManager;
    :goto_4
    const-string v26, "Initialization of index "

    invoke-static/range {v26 .. v26}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    goto :goto_2

    .line 121
    :catch_2
    move-exception v7

    .line 122
    .local v7, "e":Lcom/sec/dsm/system/DSMException;
    invoke-virtual {v7}, Lcom/sec/dsm/system/DSMException;->printStackTrace()V

    goto :goto_4

    .line 123
    .end local v7    # "e":Lcom/sec/dsm/system/DSMException;
    :catch_3
    move-exception v7

    .line 124
    .local v7, "e":Ljava/lang/Exception;
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_4

    .line 127
    .end local v7    # "e":Ljava/lang/Exception;
    :cond_a
    const-string v26, "android.intent.action.REMOTE_CONTROL_OFF"

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_d

    .line 128
    const-string v26, "Remote control is off mode"

    invoke-static/range {v26 .. v26}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 130
    :try_start_5
    new-instance v9, Lcom/sec/dsm/system/DSMManager;

    sget-object v26, Lcom/sec/dsm/system/DSMReceiver;->mcontext:Landroid/content/Context;

    move-object/from16 v0, v26

    invoke-direct {v9, v0}, Lcom/sec/dsm/system/DSMManager;-><init>(Landroid/content/Context;)V

    .line 131
    .restart local v9    # "im":Lcom/sec/dsm/system/DSMManager;
    invoke-virtual {v9}, Lcom/sec/dsm/system/DSMManager;->containsDSMTracking()Z

    move-result v26

    if-eqz v26, :cond_c

    .line 132
    new-instance v8, Landroid/content/Intent;

    const-string v26, "android.intent.action.dsm.DM_MOBILE_TRACKING_STOP"

    move-object/from16 v0, v26

    invoke-direct {v8, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 133
    .restart local v8    # "i":Landroid/content/Intent;
    sget-object v26, Lcom/sec/dsm/system/DSMReceiver;->mcontext:Landroid/content/Context;

    move-object/from16 v0, v26

    invoke-virtual {v0, v8}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V
    :try_end_5
    .catch Lcom/sec/dsm/system/DSMException; {:try_start_5 .. :try_end_5} :catch_4
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_5

    .line 143
    .end local v8    # "i":Landroid/content/Intent;
    .end local v9    # "im":Lcom/sec/dsm/system/DSMManager;
    :cond_b
    :goto_5
    const-string v26, "service stop"

    invoke-static/range {v26 .. v26}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 134
    .restart local v9    # "im":Lcom/sec/dsm/system/DSMManager;
    :cond_c
    :try_start_6
    invoke-virtual {v9}, Lcom/sec/dsm/system/DSMManager;->containsSMSForwarding()Z

    move-result v26

    if-eqz v26, :cond_b

    .line 135
    invoke-virtual {v9}, Lcom/sec/dsm/system/DSMManager;->removeSMSForwarding()V

    .line 136
    const-string v26, "Stop SMS forwarding"

    invoke-static/range {v26 .. v26}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V
    :try_end_6
    .catch Lcom/sec/dsm/system/DSMException; {:try_start_6 .. :try_end_6} :catch_4
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_5

    goto :goto_5

    .line 138
    .end local v9    # "im":Lcom/sec/dsm/system/DSMManager;
    :catch_4
    move-exception v7

    .line 139
    .local v7, "e":Lcom/sec/dsm/system/DSMException;
    invoke-virtual {v7}, Lcom/sec/dsm/system/DSMException;->printStackTrace()V

    goto :goto_5

    .line 140
    .end local v7    # "e":Lcom/sec/dsm/system/DSMException;
    :catch_5
    move-exception v7

    .line 141
    .local v7, "e":Ljava/lang/Exception;
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_5

    .line 144
    .end local v7    # "e":Ljava/lang/Exception;
    :cond_d
    const-string v26, "android.intent.action.dsm.DM_LOCK_MY_PHONE"

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_f

    .line 145
    const-string v26, "password"

    move-object/from16 v0, p2

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 146
    .local v15, "password":Ljava/lang/String;
    const-string v26, "status"

    move-object/from16 v0, p2

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    .line 147
    .local v19, "status":Ljava/lang/String;
    const-string v26, "message"

    move-object/from16 v0, p2

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 148
    .local v10, "message":Ljava/lang/String;
    const-string v26, "number"

    move-object/from16 v0, p2

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 149
    .local v14, "number":Ljava/lang/String;
    const-string v26, "reactivationlock"

    const/16 v27, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, v26

    move/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v17

    .line 151
    .local v17, "reactivationlock":Z
    new-instance v26, Lcom/android/internal/widget/LockPatternUtils;

    sget-object v27, Lcom/sec/dsm/system/DSMReceiver;->mcontext:Landroid/content/Context;

    invoke-direct/range {v26 .. v27}, Lcom/android/internal/widget/LockPatternUtils;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/dsm/system/DSMReceiver;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    .line 152
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/dsm/system/DSMReceiver;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    move-object/from16 v0, v26

    move/from16 v1, v27

    invoke-virtual {v0, v1, v15}, Lcom/android/internal/widget/LockPatternUtils;->saveRemoteLockPassword(ILjava/lang/String;)V

    .line 154
    sget-object v26, Lcom/sec/dsm/system/DSMReceiver;->mcontext:Landroid/content/Context;

    invoke-virtual/range {v26 .. v26}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v26

    const-string v27, "lock_fmm_enabled"

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    move-object/from16 v2, v19

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 155
    sget-object v26, Lcom/sec/dsm/system/DSMReceiver;->mcontext:Landroid/content/Context;

    invoke-virtual/range {v26 .. v26}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v26

    const-string v27, "lock_fmm_Message"

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    invoke-static {v0, v1, v10}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 156
    sget-object v26, Lcom/sec/dsm/system/DSMReceiver;->mcontext:Landroid/content/Context;

    invoke-virtual/range {v26 .. v26}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v26

    const-string v27, "lock_fmm_phone"

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    invoke-static {v0, v1, v14}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 157
    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "Reactivation Lock Flag : "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v26 .. v26}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 158
    const/16 v26, 0x1

    move/from16 v0, v17

    move/from16 v1, v26

    if-ne v0, v1, :cond_e

    .line 159
    const-string v26, "Reactivation Lock On method start"

    invoke-static/range {v26 .. v26}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 162
    :cond_e
    new-instance v8, Landroid/content/Intent;

    const-string v26, "com.android.internal.policy.impl.Keyguard.PCW_LOCKED"

    move-object/from16 v0, v26

    invoke-direct {v8, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 163
    .restart local v8    # "i":Landroid/content/Intent;
    const/16 v26, 0x20

    move/from16 v0, v26

    invoke-virtual {v8, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 164
    sget-object v26, Lcom/sec/dsm/system/DSMReceiver;->mcontext:Landroid/content/Context;

    move-object/from16 v0, v26

    invoke-virtual {v0, v8}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 165
    const-string v26, "Start Lock My Phone service"

    invoke-static/range {v26 .. v26}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 166
    .end local v8    # "i":Landroid/content/Intent;
    .end local v10    # "message":Ljava/lang/String;
    .end local v14    # "number":Ljava/lang/String;
    .end local v15    # "password":Ljava/lang/String;
    .end local v17    # "reactivationlock":Z
    .end local v19    # "status":Ljava/lang/String;
    :cond_f
    const-string v26, "android.intent.action.dsm.DM_RING_MY_PHONE"

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_11

    .line 169
    :try_start_7
    new-instance v9, Lcom/sec/dsm/system/DSMManager;

    sget-object v26, Lcom/sec/dsm/system/DSMReceiver;->mcontext:Landroid/content/Context;

    move-object/from16 v0, v26

    invoke-direct {v9, v0}, Lcom/sec/dsm/system/DSMManager;-><init>(Landroid/content/Context;)V

    .line 170
    .restart local v9    # "im":Lcom/sec/dsm/system/DSMManager;
    invoke-virtual {v9}, Lcom/sec/dsm/system/DSMManager;->containsDSMRing()Z

    move-result v26

    if-eqz v26, :cond_10

    .line 171
    const-string v26, "Already ring service running"

    invoke-static/range {v26 .. v26}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V
    :try_end_7
    .catch Lcom/sec/dsm/system/DSMException; {:try_start_7 .. :try_end_7} :catch_6
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_7

    .line 188
    .end local v9    # "im":Lcom/sec/dsm/system/DSMManager;
    :goto_6
    const-string v26, "Start Ring service"

    invoke-static/range {v26 .. v26}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 173
    .restart local v9    # "im":Lcom/sec/dsm/system/DSMManager;
    :cond_10
    :try_start_8
    const-string v26, "Ring"

    move-object/from16 v0, v26

    invoke-virtual {v9, v0}, Lcom/sec/dsm/system/DSMManager;->putDSMRing(Ljava/lang/String;)V

    .line 174
    const-string v26, "message"

    move-object/from16 v0, p2

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/dsm/system/DSMReceiver;->message_ring:Ljava/lang/String;

    .line 175
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/dsm/system/DSMReceiver;->message_ring:Ljava/lang/String;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    invoke-virtual {v9, v0}, Lcom/sec/dsm/system/DSMManager;->putDSMRingMessage(Ljava/lang/String;)V

    .line 176
    new-instance v8, Landroid/content/Intent;

    sget-object v26, Lcom/sec/dsm/system/DSMReceiver;->mcontext:Landroid/content/Context;

    const-class v27, Lcom/sec/dsm/system/DSMRingMyMobile;

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    invoke-direct {v8, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 177
    .restart local v8    # "i":Landroid/content/Intent;
    const-string v26, "message"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/dsm/system/DSMReceiver;->message_ring:Ljava/lang/String;

    move-object/from16 v27, v0

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    invoke-virtual {v8, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 178
    const/16 v26, 0x20

    move/from16 v0, v26

    invoke-virtual {v8, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 179
    const/high16 v26, 0x10000000

    move/from16 v0, v26

    invoke-virtual {v8, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 180
    sget-object v26, Lcom/sec/dsm/system/DSMReceiver;->mcontext:Landroid/content/Context;

    move-object/from16 v0, v26

    invoke-virtual {v0, v8}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_8
    .catch Lcom/sec/dsm/system/DSMException; {:try_start_8 .. :try_end_8} :catch_6
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_7

    goto :goto_6

    .line 183
    .end local v8    # "i":Landroid/content/Intent;
    .end local v9    # "im":Lcom/sec/dsm/system/DSMManager;
    :catch_6
    move-exception v7

    .line 184
    .local v7, "e":Lcom/sec/dsm/system/DSMException;
    invoke-virtual {v7}, Lcom/sec/dsm/system/DSMException;->printStackTrace()V

    goto :goto_6

    .line 185
    .end local v7    # "e":Lcom/sec/dsm/system/DSMException;
    :catch_7
    move-exception v7

    .line 186
    .local v7, "e":Ljava/lang/Exception;
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_6

    .line 190
    .end local v7    # "e":Ljava/lang/Exception;
    :cond_11
    const-string v26, "android.intent.action.dsm.DM_WIPE"

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_12

    .line 192
    :try_start_9
    const-string v26, "status"

    const/16 v27, 0x64

    move-object/from16 v0, p2

    move-object/from16 v1, v26

    move/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v19

    .line 193
    .local v19, "status":I
    new-instance v8, Landroid/content/Intent;

    sget-object v26, Lcom/sec/dsm/system/DSMReceiver;->mcontext:Landroid/content/Context;

    const-class v27, Lcom/sec/dsm/system/DSMWipeOutService;

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    invoke-direct {v8, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 194
    .restart local v8    # "i":Landroid/content/Intent;
    const-string v26, "status"

    move-object/from16 v0, v26

    move/from16 v1, v19

    invoke-virtual {v8, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 195
    const-string v26, "Start remote Wipe out service"

    invoke-static/range {v26 .. v26}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 196
    sget-object v26, Lcom/sec/dsm/system/DSMReceiver;->mcontext:Landroid/content/Context;

    move-object/from16 v0, v26

    invoke-virtual {v0, v8}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_8

    .line 200
    .end local v8    # "i":Landroid/content/Intent;
    .end local v19    # "status":I
    :goto_7
    const-string v26, "Wipe out sended "

    invoke-static/range {v26 .. v26}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 197
    :catch_8
    move-exception v7

    .line 198
    .restart local v7    # "e":Ljava/lang/Exception;
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_7

    .line 201
    .end local v7    # "e":Ljava/lang/Exception;
    :cond_12
    const-string v26, "android.intent.action.SAMSUNG_ACCOUNT_SIGNOUT_FAILED"

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-nez v26, :cond_13

    const-string v26, "android.intent.action.SAMSUNG_ACCOUNT_SIGNOUT_SUCCESS"

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_14

    .line 204
    :cond_13
    const/16 v19, 0x28

    .line 205
    .restart local v19    # "status":I
    :try_start_a
    new-instance v8, Landroid/content/Intent;

    sget-object v26, Lcom/sec/dsm/system/DSMReceiver;->mcontext:Landroid/content/Context;

    const-class v27, Lcom/sec/dsm/system/DSMWipeOutService;

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    invoke-direct {v8, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 206
    .restart local v8    # "i":Landroid/content/Intent;
    const-string v26, "status"

    move-object/from16 v0, v26

    move/from16 v1, v19

    invoke-virtual {v8, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 207
    sget-object v26, Lcom/sec/dsm/system/DSMReceiver;->mcontext:Landroid/content/Context;

    move-object/from16 v0, v26

    invoke-virtual {v0, v8}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_9

    .line 211
    .end local v8    # "i":Landroid/content/Intent;
    :goto_8
    const-string v26, "Receive result of sign out "

    invoke-static/range {v26 .. v26}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 208
    :catch_9
    move-exception v7

    .line 209
    .restart local v7    # "e":Ljava/lang/Exception;
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_8

    .line 213
    .end local v7    # "e":Ljava/lang/Exception;
    .end local v19    # "status":I
    :cond_14
    const-string v26, "android.intent.action.dsm.DM_FACTORY_RESET"

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_16

    .line 215
    :try_start_b
    new-instance v5, Lcom/sec/dsm/system/DSMActivationLock;

    sget-object v26, Lcom/sec/dsm/system/DSMReceiver;->mcontext:Landroid/content/Context;

    invoke-virtual/range {v26 .. v26}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-direct {v5, v0}, Lcom/sec/dsm/system/DSMActivationLock;-><init>(Landroid/content/Context;)V

    .line 216
    .local v5, "dal":Lcom/sec/dsm/system/DSMActivationLock;
    invoke-static {}, Lcom/sec/dsm/system/DSMActivationLock;->isActivationLocked()Z

    move-result v4

    .line 217
    .local v4, "activationLock":Z
    if-nez v4, :cond_15

    .line 218
    new-instance v8, Landroid/content/Intent;

    const-string v26, "android.intent.action.DM_FACTORY_DATA_RESET"

    move-object/from16 v0, v26

    invoke-direct {v8, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 219
    .restart local v8    # "i":Landroid/content/Intent;
    const/16 v26, 0x20

    move/from16 v0, v26

    invoke-virtual {v8, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 220
    sget-object v26, Lcom/sec/dsm/system/DSMReceiver;->mcontext:Landroid/content/Context;

    move-object/from16 v0, v26

    invoke-virtual {v0, v8}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_a

    .line 229
    .end local v4    # "activationLock":Z
    .end local v5    # "dal":Lcom/sec/dsm/system/DSMActivationLock;
    .end local v8    # "i":Landroid/content/Intent;
    :goto_9
    const-string v26, "Start Factory reset service"

    invoke-static/range {v26 .. v26}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 222
    .restart local v4    # "activationLock":Z
    .restart local v5    # "dal":Lcom/sec/dsm/system/DSMActivationLock;
    :cond_15
    :try_start_c
    new-instance v8, Landroid/content/Intent;

    const-string v26, "android.intent.action.MASTER_CLEAR"

    move-object/from16 v0, v26

    invoke-direct {v8, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 223
    .restart local v8    # "i":Landroid/content/Intent;
    const/16 v26, 0x20

    move/from16 v0, v26

    invoke-virtual {v8, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 224
    sget-object v26, Lcom/sec/dsm/system/DSMReceiver;->mcontext:Landroid/content/Context;

    move-object/from16 v0, v26

    invoke-virtual {v0, v8}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_a

    goto :goto_9

    .line 226
    .end local v4    # "activationLock":Z
    .end local v5    # "dal":Lcom/sec/dsm/system/DSMActivationLock;
    .end local v8    # "i":Landroid/content/Intent;
    :catch_a
    move-exception v7

    .line 227
    .restart local v7    # "e":Ljava/lang/Exception;
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_9

    .line 230
    .end local v7    # "e":Ljava/lang/Exception;
    :cond_16
    const-string v26, "android.intent.action.dsm.DM_MOBILE_TRACKING_START"

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_17

    .line 232
    :try_start_d
    sget-object v26, Lcom/sec/dsm/system/DSMReceiver;->mcontext:Landroid/content/Context;

    const-string v27, "notification"

    invoke-virtual/range {v26 .. v27}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/app/NotificationManager;

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/dsm/system/DSMReceiver;->nm:Landroid/app/NotificationManager;

    .line 233
    sget-object v26, Lcom/sec/dsm/system/DSMReceiver;->mcontext:Landroid/content/Context;

    const/16 v27, 0x0

    new-instance v28, Landroid/content/Intent;

    const-string v29, "android.settings.SECURITY_SETTINGS"

    invoke-direct/range {v28 .. v29}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/16 v29, 0x0

    invoke-static/range {v26 .. v29}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v16

    .line 234
    .local v16, "pendingIntent":Landroid/app/PendingIntent;
    sget-object v26, Lcom/sec/dsm/system/DSMReceiver;->mcontext:Landroid/content/Context;

    const v27, 0x7f060004

    invoke-virtual/range {v26 .. v27}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v24

    .line 235
    .local v24, "ticker":Ljava/lang/String;
    sget-object v26, Lcom/sec/dsm/system/DSMReceiver;->mcontext:Landroid/content/Context;

    const v27, 0x7f060005

    invoke-virtual/range {v26 .. v27}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v25

    .line 236
    .local v25, "title":Ljava/lang/String;
    sget-object v26, Lcom/sec/dsm/system/DSMReceiver;->mcontext:Landroid/content/Context;

    const v27, 0x7f060009

    invoke-virtual/range {v26 .. v27}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v23

    .line 237
    .local v23, "text":Ljava/lang/String;
    new-instance v13, Landroid/app/Notification;

    const v26, 0x7f02000a

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v28

    move/from16 v0, v26

    move-object/from16 v1, v24

    move-wide/from16 v2, v28

    invoke-direct {v13, v0, v1, v2, v3}, Landroid/app/Notification;-><init>(ILjava/lang/CharSequence;J)V

    .line 238
    .local v13, "notification":Landroid/app/Notification;
    const/16 v26, 0x2

    move/from16 v0, v26

    iput v0, v13, Landroid/app/Notification;->flags:I

    .line 239
    sget-object v26, Lcom/sec/dsm/system/DSMReceiver;->mcontext:Landroid/content/Context;

    move-object/from16 v0, v26

    move-object/from16 v1, v25

    move-object/from16 v2, v23

    move-object/from16 v3, v16

    invoke-virtual {v13, v0, v1, v2, v3}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    .line 240
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/dsm/system/DSMReceiver;->nm:Landroid/app/NotificationManager;

    move-object/from16 v26, v0

    const/16 v27, 0x7d0

    move-object/from16 v0, v26

    move/from16 v1, v27

    invoke-virtual {v0, v1, v13}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 241
    const-string v26, "starttime"

    move-object/from16 v0, p2

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/dsm/system/DSMReceiver;->starttime:Ljava/lang/String;

    .line 242
    const-string v26, "stoptime"

    move-object/from16 v0, p2

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/dsm/system/DSMReceiver;->stoptime:Ljava/lang/String;

    .line 243
    const-string v26, "interval"

    move-object/from16 v0, p2

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/dsm/system/DSMReceiver;->interval:Ljava/lang/String;

    .line 244
    new-instance v9, Lcom/sec/dsm/system/DSMManager;

    sget-object v26, Lcom/sec/dsm/system/DSMReceiver;->mcontext:Landroid/content/Context;

    move-object/from16 v0, v26

    invoke-direct {v9, v0}, Lcom/sec/dsm/system/DSMManager;-><init>(Landroid/content/Context;)V

    .line 245
    .restart local v9    # "im":Lcom/sec/dsm/system/DSMManager;
    const-string v26, "Tracking"

    move-object/from16 v0, v26

    invoke-virtual {v9, v0}, Lcom/sec/dsm/system/DSMManager;->putDSMTracking(Ljava/lang/String;)V

    .line 246
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/dsm/system/DSMReceiver;->starttime:Ljava/lang/String;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    invoke-virtual {v9, v0}, Lcom/sec/dsm/system/DSMManager;->putDSMStartTime(Ljava/lang/String;)V

    .line 247
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/dsm/system/DSMReceiver;->stoptime:Ljava/lang/String;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    invoke-virtual {v9, v0}, Lcom/sec/dsm/system/DSMManager;->putDSMStopTime(Ljava/lang/String;)V

    .line 248
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/dsm/system/DSMReceiver;->interval:Ljava/lang/String;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    invoke-virtual {v9, v0}, Lcom/sec/dsm/system/DSMManager;->putDSMInterval(Ljava/lang/String;)V

    .line 249
    new-instance v8, Landroid/content/Intent;

    sget-object v26, Lcom/sec/dsm/system/DSMReceiver;->mcontext:Landroid/content/Context;

    const-class v27, Lcom/sec/dsm/system/DSMTrackingService;

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    invoke-direct {v8, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 250
    .restart local v8    # "i":Landroid/content/Intent;
    const-string v26, "starttime"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/dsm/system/DSMReceiver;->starttime:Ljava/lang/String;

    move-object/from16 v27, v0

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    invoke-virtual {v8, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 251
    const-string v26, "stoptime"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/dsm/system/DSMReceiver;->stoptime:Ljava/lang/String;

    move-object/from16 v27, v0

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    invoke-virtual {v8, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 252
    const-string v26, "interval"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/dsm/system/DSMReceiver;->interval:Ljava/lang/String;

    move-object/from16 v27, v0

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    invoke-virtual {v8, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 253
    sget-object v26, Lcom/sec/dsm/system/DSMReceiver;->mcontext:Landroid/content/Context;

    move-object/from16 v0, v26

    invoke-virtual {v0, v8}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
    :try_end_d
    .catch Lcom/sec/dsm/system/DSMException; {:try_start_d .. :try_end_d} :catch_b
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_c

    .line 259
    .end local v8    # "i":Landroid/content/Intent;
    .end local v9    # "im":Lcom/sec/dsm/system/DSMManager;
    .end local v13    # "notification":Landroid/app/Notification;
    .end local v16    # "pendingIntent":Landroid/app/PendingIntent;
    .end local v23    # "text":Ljava/lang/String;
    .end local v24    # "ticker":Ljava/lang/String;
    .end local v25    # "title":Ljava/lang/String;
    :goto_a
    const-string v26, "Start tracking service"

    invoke-static/range {v26 .. v26}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 254
    :catch_b
    move-exception v7

    .line 255
    .local v7, "e":Lcom/sec/dsm/system/DSMException;
    invoke-virtual {v7}, Lcom/sec/dsm/system/DSMException;->printStackTrace()V

    goto :goto_a

    .line 256
    .end local v7    # "e":Lcom/sec/dsm/system/DSMException;
    :catch_c
    move-exception v7

    .line 257
    .local v7, "e":Ljava/lang/Exception;
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_a

    .line 261
    .end local v7    # "e":Ljava/lang/Exception;
    :cond_17
    const-string v26, "android.intent.action.dsm.DM_MOBILE_TRACKING_STOP"

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_18

    .line 263
    :try_start_e
    sget-object v26, Lcom/sec/dsm/system/DSMReceiver;->mcontext:Landroid/content/Context;

    const-string v27, "notification"

    invoke-virtual/range {v26 .. v27}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/app/NotificationManager;

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/dsm/system/DSMReceiver;->nm:Landroid/app/NotificationManager;

    .line 264
    sget-object v26, Lcom/sec/dsm/system/DSMReceiver;->mcontext:Landroid/content/Context;

    const/16 v27, 0x0

    new-instance v28, Landroid/content/Intent;

    const-string v29, "android.settings.SECURITY_SETTINGS"

    invoke-direct/range {v28 .. v29}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/16 v29, 0x0

    invoke-static/range {v26 .. v29}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v16

    .line 265
    .restart local v16    # "pendingIntent":Landroid/app/PendingIntent;
    sget-object v26, Lcom/sec/dsm/system/DSMReceiver;->mcontext:Landroid/content/Context;

    const v27, 0x7f060004

    invoke-virtual/range {v26 .. v27}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v24

    .line 266
    .restart local v24    # "ticker":Ljava/lang/String;
    sget-object v26, Lcom/sec/dsm/system/DSMReceiver;->mcontext:Landroid/content/Context;

    const v27, 0x7f060006

    invoke-virtual/range {v26 .. v27}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v25

    .line 267
    .restart local v25    # "title":Ljava/lang/String;
    sget-object v26, Lcom/sec/dsm/system/DSMReceiver;->mcontext:Landroid/content/Context;

    const v27, 0x7f060009

    invoke-virtual/range {v26 .. v27}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v23

    .line 268
    .restart local v23    # "text":Ljava/lang/String;
    new-instance v13, Landroid/app/Notification;

    const v26, 0x7f02000a

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v28

    move/from16 v0, v26

    move-object/from16 v1, v24

    move-wide/from16 v2, v28

    invoke-direct {v13, v0, v1, v2, v3}, Landroid/app/Notification;-><init>(ILjava/lang/CharSequence;J)V

    .line 269
    .restart local v13    # "notification":Landroid/app/Notification;
    const/16 v26, 0x10

    move/from16 v0, v26

    iput v0, v13, Landroid/app/Notification;->flags:I

    .line 270
    sget-object v26, Lcom/sec/dsm/system/DSMReceiver;->mcontext:Landroid/content/Context;

    move-object/from16 v0, v26

    move-object/from16 v1, v25

    move-object/from16 v2, v23

    move-object/from16 v3, v16

    invoke-virtual {v13, v0, v1, v2, v3}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    .line 271
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/dsm/system/DSMReceiver;->nm:Landroid/app/NotificationManager;

    move-object/from16 v26, v0

    const/16 v27, 0x7d0

    move-object/from16 v0, v26

    move/from16 v1, v27

    invoke-virtual {v0, v1, v13}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 272
    new-instance v9, Lcom/sec/dsm/system/DSMManager;

    sget-object v26, Lcom/sec/dsm/system/DSMReceiver;->mcontext:Landroid/content/Context;

    move-object/from16 v0, v26

    invoke-direct {v9, v0}, Lcom/sec/dsm/system/DSMManager;-><init>(Landroid/content/Context;)V

    .line 273
    .restart local v9    # "im":Lcom/sec/dsm/system/DSMManager;
    invoke-virtual {v9}, Lcom/sec/dsm/system/DSMManager;->removeDSMTracking()V

    .line 274
    new-instance v8, Landroid/content/Intent;

    sget-object v26, Lcom/sec/dsm/system/DSMReceiver;->mcontext:Landroid/content/Context;

    const-class v27, Lcom/sec/dsm/system/DSMTrackingService;

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    invoke-direct {v8, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 275
    .restart local v8    # "i":Landroid/content/Intent;
    sget-object v26, Lcom/sec/dsm/system/DSMReceiver;->mcontext:Landroid/content/Context;

    move-object/from16 v0, v26

    invoke-virtual {v0, v8}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
    :try_end_e
    .catch Lcom/sec/dsm/system/DSMException; {:try_start_e .. :try_end_e} :catch_d
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_e

    .line 282
    .end local v8    # "i":Landroid/content/Intent;
    .end local v9    # "im":Lcom/sec/dsm/system/DSMManager;
    .end local v13    # "notification":Landroid/app/Notification;
    .end local v16    # "pendingIntent":Landroid/app/PendingIntent;
    .end local v23    # "text":Ljava/lang/String;
    .end local v24    # "ticker":Ljava/lang/String;
    .end local v25    # "title":Ljava/lang/String;
    :goto_b
    const-string v26, "Stop tracking service"

    invoke-static/range {v26 .. v26}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 276
    :catch_d
    move-exception v7

    .line 277
    .local v7, "e":Lcom/sec/dsm/system/DSMException;
    invoke-virtual {v7}, Lcom/sec/dsm/system/DSMException;->printStackTrace()V

    goto :goto_b

    .line 278
    .end local v7    # "e":Lcom/sec/dsm/system/DSMException;
    :catch_e
    move-exception v7

    .line 279
    .local v7, "e":Ljava/lang/Exception;
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_b

    .line 283
    .end local v7    # "e":Ljava/lang/Exception;
    :cond_18
    const-string v26, "android.intent.action.dsm.DM_LOCK_RELEASE"

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_19

    .line 284
    const-string v26, "Start unlock service"

    invoke-static/range {v26 .. v26}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 286
    const/16 v26, 0x1

    :try_start_f
    sput v26, Lcom/sec/dsm/system/DSMReceiver;->m_BackupIndicatorState:I

    .line 287
    sget-object v26, Lcom/sec/dsm/system/DSMReceiver;->mcontext:Landroid/content/Context;

    const-string v27, "notification"

    invoke-virtual/range {v26 .. v27}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/app/NotificationManager;

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/dsm/system/DSMReceiver;->nm:Landroid/app/NotificationManager;

    .line 288
    new-instance v20, Landroid/content/Intent;

    invoke-direct/range {v20 .. v20}, Landroid/content/Intent;-><init>()V

    .line 289
    .local v20, "sub_intent":Landroid/content/Intent;
    const-string v26, "com.android.settings"

    const-string v27, "com.android.settings.ChooseLockGeneric"

    move-object/from16 v0, v20

    move-object/from16 v1, v26

    move-object/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 290
    sget-object v26, Lcom/sec/dsm/system/DSMReceiver;->mcontext:Landroid/content/Context;

    const/16 v27, 0x0

    const/16 v28, 0x0

    move-object/from16 v0, v26

    move/from16 v1, v27

    move-object/from16 v2, v20

    move/from16 v3, v28

    invoke-static {v0, v1, v2, v3}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v16

    .line 291
    .restart local v16    # "pendingIntent":Landroid/app/PendingIntent;
    sget-object v26, Lcom/sec/dsm/system/DSMReceiver;->mcontext:Landroid/content/Context;

    const v27, 0x7f060007

    invoke-virtual/range {v26 .. v27}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v24

    .line 292
    .restart local v24    # "ticker":Ljava/lang/String;
    sget-object v26, Lcom/sec/dsm/system/DSMReceiver;->mcontext:Landroid/content/Context;

    const v27, 0x7f060008

    invoke-virtual/range {v26 .. v27}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v25

    .line 293
    .restart local v25    # "title":Ljava/lang/String;
    sget-object v26, Lcom/sec/dsm/system/DSMReceiver;->mcontext:Landroid/content/Context;

    const v27, 0x7f060009

    invoke-virtual/range {v26 .. v27}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v23

    .line 294
    .restart local v23    # "text":Ljava/lang/String;
    new-instance v13, Landroid/app/Notification;

    const v26, 0x7f02000b

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v28

    move/from16 v0, v26

    move-object/from16 v1, v24

    move-wide/from16 v2, v28

    invoke-direct {v13, v0, v1, v2, v3}, Landroid/app/Notification;-><init>(ILjava/lang/CharSequence;J)V

    .line 295
    .restart local v13    # "notification":Landroid/app/Notification;
    const/16 v26, 0x10

    move/from16 v0, v26

    iput v0, v13, Landroid/app/Notification;->flags:I

    .line 296
    sget-object v26, Lcom/sec/dsm/system/DSMReceiver;->mcontext:Landroid/content/Context;

    move-object/from16 v0, v26

    move-object/from16 v1, v25

    move-object/from16 v2, v23

    move-object/from16 v3, v16

    invoke-virtual {v13, v0, v1, v2, v3}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    .line 297
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/dsm/system/DSMReceiver;->nm:Landroid/app/NotificationManager;

    move-object/from16 v26, v0

    const/16 v27, 0x3e8

    move-object/from16 v0, v26

    move/from16 v1, v27

    invoke-virtual {v0, v1, v13}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 298
    new-instance v26, Lcom/android/internal/widget/LockPatternUtils;

    sget-object v27, Lcom/sec/dsm/system/DSMReceiver;->mcontext:Landroid/content/Context;

    invoke-direct/range {v26 .. v27}, Lcom/android/internal/widget/LockPatternUtils;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/dsm/system/DSMReceiver;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    .line 299
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/dsm/system/DSMReceiver;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    move-object/from16 v26, v0

    const/16 v27, 0x1

    invoke-virtual/range {v26 .. v27}, Lcom/android/internal/widget/LockPatternUtils;->clearLock(Z)V

    .line 300
    new-instance v8, Landroid/content/Intent;

    const-string v26, "com.android.internal.policy.impl.Keyguard.PCW_UNLOCKED"

    move-object/from16 v0, v26

    invoke-direct {v8, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 301
    .restart local v8    # "i":Landroid/content/Intent;
    sget-object v26, Lcom/sec/dsm/system/DSMReceiver;->mcontext:Landroid/content/Context;

    move-object/from16 v0, v26

    invoke-virtual {v0, v8}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V
    :try_end_f
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_f} :catch_f

    goto/16 :goto_2

    .line 302
    .end local v8    # "i":Landroid/content/Intent;
    .end local v13    # "notification":Landroid/app/Notification;
    .end local v16    # "pendingIntent":Landroid/app/PendingIntent;
    .end local v20    # "sub_intent":Landroid/content/Intent;
    .end local v23    # "text":Ljava/lang/String;
    .end local v24    # "ticker":Ljava/lang/String;
    .end local v25    # "title":Ljava/lang/String;
    :catch_f
    move-exception v7

    .line 303
    .restart local v7    # "e":Ljava/lang/Exception;
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_2

    .line 305
    .end local v7    # "e":Ljava/lang/Exception;
    :cond_19
    const-string v26, "android.intent.action.dsm.DM_FIND_MY_PHONE"

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_1b

    .line 307
    :try_start_10
    const-string v26, "[DSMReceiver] Start get location service"

    invoke-static/range {v26 .. v26}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 308
    new-instance v6, Lcom/sec/dsm/system/DSMGetLocationService;

    sget-object v26, Lcom/sec/dsm/system/DSMReceiver;->mcontext:Landroid/content/Context;

    move-object/from16 v0, v26

    invoke-direct {v6, v0}, Lcom/sec/dsm/system/DSMGetLocationService;-><init>(Landroid/content/Context;)V

    .line 309
    .local v6, "dsmGetLocationService":Lcom/sec/dsm/system/DSMGetLocationService;
    const/4 v11, 0x0

    .line 310
    .local v11, "nLocCount":I
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v26

    if-eqz v26, :cond_1a

    .line 311
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v26

    const-string v27, "LocCount"

    invoke-virtual/range {v26 .. v27}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v11

    .line 313
    :cond_1a
    const-string v26, "NetOnly"

    const/16 v27, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, v26

    move/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v12

    .line 315
    .local v12, "netOnly":Z
    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "[DSMReceiver] nLocCount : "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v26 .. v26}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 316
    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "[DSMReceiver] netOnly   : "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v26 .. v26}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 317
    invoke-static {v11, v12}, Lcom/sec/dsm/system/DSMGetLocationService;->DSMSetupGpsTracking(IZ)V
    :try_end_10
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_10} :catch_10

    .line 322
    .end local v6    # "dsmGetLocationService":Lcom/sec/dsm/system/DSMGetLocationService;
    .end local v11    # "nLocCount":I
    .end local v12    # "netOnly":Z
    :goto_c
    const-string v26, "Stop tracking service"

    invoke-static/range {v26 .. v26}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 318
    :catch_10
    move-exception v7

    .line 319
    .restart local v7    # "e":Ljava/lang/Exception;
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_c

    .line 323
    .end local v7    # "e":Ljava/lang/Exception;
    :cond_1b
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v26

    const-string v27, "android.intent.action.LOCALE_CHANGED"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_1e

    .line 324
    const-string v26, "[DSMReceiver] Noti status is chagned"

    invoke-static/range {v26 .. v26}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 326
    :try_start_11
    sget-object v26, Lcom/sec/dsm/system/DSMReceiver;->mcontext:Landroid/content/Context;

    const-string v27, "notification"

    invoke-virtual/range {v26 .. v27}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/app/NotificationManager;

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/dsm/system/DSMReceiver;->nm:Landroid/app/NotificationManager;

    .line 327
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/dsm/system/DSMReceiver;->nm:Landroid/app/NotificationManager;

    move-object/from16 v26, v0

    if-eqz v26, :cond_1d

    .line 328
    sget v26, Lcom/sec/dsm/system/DSMReceiver;->m_BackupIndicatorState:I

    const/16 v27, 0x1

    move/from16 v0, v26

    move/from16 v1, v27

    if-ne v0, v1, :cond_1c

    .line 329
    new-instance v20, Landroid/content/Intent;

    const-string v26, "android.settings.SECURITY_SETTINGS"

    move-object/from16 v0, v20

    move-object/from16 v1, v26

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 330
    .restart local v20    # "sub_intent":Landroid/content/Intent;
    sget-object v26, Lcom/sec/dsm/system/DSMReceiver;->mcontext:Landroid/content/Context;

    const/16 v27, 0x0

    const/16 v28, 0x0

    move-object/from16 v0, v26

    move/from16 v1, v27

    move-object/from16 v2, v20

    move/from16 v3, v28

    invoke-static {v0, v1, v2, v3}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v16

    .line 331
    .restart local v16    # "pendingIntent":Landroid/app/PendingIntent;
    sget-object v26, Lcom/sec/dsm/system/DSMReceiver;->mcontext:Landroid/content/Context;

    const v27, 0x7f060007

    invoke-virtual/range {v26 .. v27}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v24

    .line 332
    .restart local v24    # "ticker":Ljava/lang/String;
    sget-object v26, Lcom/sec/dsm/system/DSMReceiver;->mcontext:Landroid/content/Context;

    const v27, 0x7f060008

    invoke-virtual/range {v26 .. v27}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v25

    .line 333
    .restart local v25    # "title":Ljava/lang/String;
    sget-object v26, Lcom/sec/dsm/system/DSMReceiver;->mcontext:Landroid/content/Context;

    const v27, 0x7f060009

    invoke-virtual/range {v26 .. v27}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v23

    .line 334
    .restart local v23    # "text":Ljava/lang/String;
    new-instance v13, Landroid/app/Notification;

    const v26, 0x7f02000b

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v28

    move/from16 v0, v26

    move-object/from16 v1, v24

    move-wide/from16 v2, v28

    invoke-direct {v13, v0, v1, v2, v3}, Landroid/app/Notification;-><init>(ILjava/lang/CharSequence;J)V

    .line 335
    .restart local v13    # "notification":Landroid/app/Notification;
    const/16 v26, 0x10

    move/from16 v0, v26

    iput v0, v13, Landroid/app/Notification;->flags:I

    .line 336
    sget-object v26, Lcom/sec/dsm/system/DSMReceiver;->mcontext:Landroid/content/Context;

    move-object/from16 v0, v26

    move-object/from16 v1, v25

    move-object/from16 v2, v23

    move-object/from16 v3, v16

    invoke-virtual {v13, v0, v1, v2, v3}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    .line 338
    new-instance v22, Landroid/content/Intent;

    sget-object v26, Lcom/sec/dsm/system/DSMReceiver;->mcontext:Landroid/content/Context;

    const-class v27, Lcom/sec/dsm/system/DSMReceiver;

    move-object/from16 v0, v22

    move-object/from16 v1, v26

    move-object/from16 v2, v27

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 339
    .local v22, "tempIntent":Landroid/content/Intent;
    const-string v26, "android.intent.action.NOTI_CANCELED"

    move-object/from16 v0, v22

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 340
    sget-object v26, Lcom/sec/dsm/system/DSMReceiver;->mcontext:Landroid/content/Context;

    const/16 v27, 0x0

    const/high16 v28, 0x10000000

    move-object/from16 v0, v26

    move/from16 v1, v27

    move-object/from16 v2, v22

    move/from16 v3, v28

    invoke-static {v0, v1, v2, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v26

    move-object/from16 v0, v26

    iput-object v0, v13, Landroid/app/Notification;->deleteIntent:Landroid/app/PendingIntent;

    .line 341
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/dsm/system/DSMReceiver;->nm:Landroid/app/NotificationManager;

    move-object/from16 v26, v0

    const/16 v27, 0x3e8

    move-object/from16 v0, v26

    move/from16 v1, v27

    invoke-virtual {v0, v1, v13}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 343
    .end local v13    # "notification":Landroid/app/Notification;
    .end local v16    # "pendingIntent":Landroid/app/PendingIntent;
    .end local v20    # "sub_intent":Landroid/content/Intent;
    .end local v22    # "tempIntent":Landroid/content/Intent;
    .end local v23    # "text":Ljava/lang/String;
    .end local v24    # "ticker":Ljava/lang/String;
    .end local v25    # "title":Ljava/lang/String;
    :cond_1c
    const-string v26, "Notification is not exist"

    invoke-static/range {v26 .. v26}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V
    :try_end_11
    .catch Ljava/lang/Exception; {:try_start_11 .. :try_end_11} :catch_11

    goto/16 :goto_2

    .line 347
    :catch_11
    move-exception v7

    .line 348
    .restart local v7    # "e":Ljava/lang/Exception;
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_2

    .line 345
    .end local v7    # "e":Ljava/lang/Exception;
    :cond_1d
    :try_start_12
    const-string v26, "Notification is Null"

    invoke-static/range {v26 .. v26}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V
    :try_end_12
    .catch Ljava/lang/Exception; {:try_start_12 .. :try_end_12} :catch_11

    goto/16 :goto_2

    .line 350
    :cond_1e
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v26

    const-string v27, "android.intent.action.NOTI_CANCELED"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_4

    .line 351
    const-string v26, "[DSMReceiver] Noti is removed"

    invoke-static/range {v26 .. v26}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 352
    const/16 v26, 0x0

    sput v26, Lcom/sec/dsm/system/DSMReceiver;->m_BackupIndicatorState:I

    goto/16 :goto_2
.end method
