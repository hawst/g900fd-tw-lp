.class public Lcom/sec/dsm/system/DSMAdapter;
.super Landroid/app/Service;
.source "DSMAdapter.java"


# static fields
.field public static final XCOMMON_INTENT_LOCATION_RESULT:Ljava/lang/String; = "android.intent.action.dsm.DM_LOCATION_RESULT"

.field public static final XCOMMON_INTENT_LOCK_RESULT:Ljava/lang/String; = "android.intent.action.dsm.DM_LOCK_RESULT"

.field private static mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method

.method public static DSMAdpSendLockInfo(Landroid/content/Context;ZZ)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "RLisSucceed"    # Z
    .param p2, "LSisSucceed"    # Z

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 16
    sput-object p0, Lcom/sec/dsm/system/DSMAdapter;->mContext:Landroid/content/Context;

    .line 17
    const-string v1, "Send LockScreen Data"

    invoke-static {v1}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 18
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.dsm.DM_LOCK_RESULT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 19
    .local v0, "intent":Landroid/content/Intent;
    if-ne p1, v2, :cond_0

    .line 20
    const-string v1, "RL on is succeed"

    invoke-static {v1}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 21
    const-string v1, "RLisSucceed"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 27
    :goto_0
    if-ne p2, v2, :cond_1

    .line 28
    const-string v1, "LS on is succeed"

    invoke-static {v1}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 29
    const-string v1, "LSisSucceed"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 35
    :goto_1
    sget-object v1, Lcom/sec/dsm/system/DSMAdapter;->mContext:Landroid/content/Context;

    sget-object v2, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    .line 36
    const-string v1, "Send Lock Data to DM Client"

    invoke-static {v1}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 37
    return-void

    .line 23
    :cond_0
    const-string v1, "RL on is faild"

    invoke-static {v1}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 24
    const-string v1, "RLisSucceed"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto :goto_0

    .line 31
    :cond_1
    const-string v1, "LS on is faild"

    invoke-static {v1}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 32
    const-string v1, "LSisSucceed"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto :goto_1
.end method

.method public static DSMAdpSetGetLocation(Landroid/content/Context;Landroid/location/Location;Z)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "location"    # Landroid/location/Location;
    .param p2, "isSucceed"    # Z

    .prologue
    .line 40
    sput-object p0, Lcom/sec/dsm/system/DSMAdapter;->mContext:Landroid/content/Context;

    .line 41
    const-string v1, "Send Location Data"

    invoke-static {v1}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 42
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.dsm.DM_LOCATION_RESULT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 43
    .local v0, "intent":Landroid/content/Intent;
    if-eqz p2, :cond_0

    .line 44
    const-string v1, "Location Data finding is succeed"

    invoke-static {v1}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 45
    const-string v1, "isSucceed"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 46
    const-string v1, "Longitude"

    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;D)Landroid/content/Intent;

    .line 47
    const-string v1, "Altitude"

    invoke-virtual {p1}, Landroid/location/Location;->getAltitude()D

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;D)Landroid/content/Intent;

    .line 48
    const-string v1, "Latitude"

    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;D)Landroid/content/Intent;

    .line 49
    const-string v1, "Bearing"

    invoke-virtual {p1}, Landroid/location/Location;->getBearing()F

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;F)Landroid/content/Intent;

    .line 50
    const-string v1, "Speed"

    invoke-virtual {p1}, Landroid/location/Location;->getSpeed()F

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;F)Landroid/content/Intent;

    .line 51
    const-string v1, "Accuracy"

    invoke-virtual {p1}, Landroid/location/Location;->getAccuracy()F

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;F)Landroid/content/Intent;

    .line 56
    :goto_0
    sget-object v1, Lcom/sec/dsm/system/DSMAdapter;->mContext:Landroid/content/Context;

    sget-object v2, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    .line 57
    const-string v1, "Send Location Data to DM Client"

    invoke-static {v1}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 58
    return-void

    .line 53
    :cond_0
    const-string v1, "Location Data finding is failed"

    invoke-static {v1}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 54
    const-string v1, "isSucceed"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto :goto_0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "arg0"    # Landroid/content/Intent;

    .prologue
    .line 63
    const/4 v0, 0x0

    return-object v0
.end method
