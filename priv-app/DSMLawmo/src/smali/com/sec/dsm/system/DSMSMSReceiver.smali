.class public Lcom/sec/dsm/system/DSMSMSReceiver;
.super Landroid/content/BroadcastReceiver;
.source "DSMSMSReceiver.java"


# static fields
.field public static final DSM_SMS_RECEIVED:Ljava/lang/String; = "android.provider.Telephony.SMS_RECEIVED"

.field public static final MAX_DOMESTIC_SMS_SIZE:I = 0x50


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method private static cutFirstStrInByte(Ljava/lang/String;I)Ljava/lang/String;
    .locals 7
    .param p0, "str"    # Ljava/lang/String;
    .param p1, "endIndex"    # I

    .prologue
    .line 200
    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5, p1}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 201
    .local v5, "sbStr":Ljava/lang/StringBuffer;
    const/4 v3, 0x0

    .line 202
    .local v3, "iTotal":I
    invoke-virtual {p0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    .local v0, "arr$":[C
    array-length v4, v0

    .local v4, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v4, :cond_0

    aget-char v1, v0, v2

    .line 203
    .local v1, "c":C
    invoke-static {v1}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->getBytes()[B

    move-result-object v6

    array-length v6, v6

    add-int/2addr v3, v6

    .line 204
    if-le v3, p1, :cond_1

    .line 209
    .end local v1    # "c":C
    :cond_0
    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    return-object v6

    .line 207
    .restart local v1    # "c":C
    :cond_1
    invoke-virtual {v5, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 202
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public static final getMessagesFromIntent(Landroid/content/Intent;)[Landroid/telephony/SmsMessage;
    .locals 7
    .param p0, "intent"    # Landroid/content/Intent;

    .prologue
    .line 179
    const-string v6, "Recive SMS message"

    invoke-static {v6}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 180
    const-string v6, "pdus"

    invoke-virtual {p0, v6}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v6

    check-cast v6, [Ljava/lang/Object;

    move-object v1, v6

    check-cast v1, [Ljava/lang/Object;

    .line 181
    .local v1, "messages":[Ljava/lang/Object;
    if-eqz v1, :cond_1

    .line 182
    array-length v6, v1

    new-array v4, v6, [[B

    .line 183
    .local v4, "pduObjs":[[B
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v6, v1

    if-ge v0, v6, :cond_0

    .line 184
    aget-object v6, v1, v0

    check-cast v6, [B

    check-cast v6, [B

    aput-object v6, v4, v0

    .line 183
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 186
    :cond_0
    array-length v6, v4

    new-array v5, v6, [[B

    .line 187
    .local v5, "pdus":[[B
    array-length v3, v5

    .line 188
    .local v3, "pduCount":I
    new-array v2, v3, [Landroid/telephony/SmsMessage;

    .line 189
    .local v2, "msgs":[Landroid/telephony/SmsMessage;
    const/4 v0, 0x0

    :goto_1
    if-ge v0, v3, :cond_2

    .line 190
    aget-object v6, v4, v0

    aput-object v6, v5, v0

    .line 191
    aget-object v6, v5, v0

    invoke-static {v6}, Landroid/telephony/SmsMessage;->createFromPdu([B)Landroid/telephony/SmsMessage;

    move-result-object v6

    aput-object v6, v2, v0

    .line 189
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 196
    .end local v0    # "i":I
    .end local v2    # "msgs":[Landroid/telephony/SmsMessage;
    .end local v3    # "pduCount":I
    .end local v4    # "pduObjs":[[B
    .end local v5    # "pdus":[[B
    :cond_1
    const/4 v2, 0x0

    :cond_2
    return-object v2
.end method

.method public static isDomesticModel()Z
    .locals 2

    .prologue
    .line 213
    const-string v1, "ro.csc.sales_code"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 214
    .local v0, "salesCode":Ljava/lang/String;
    if-eqz v0, :cond_1

    const-string v1, "SKT"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "KTT"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "LGT"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public isRepeatedMessage(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "receivedMessage"    # Ljava/lang/String;
    .param p3, "sendMessage"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 219
    invoke-static {p1, p2}, Lcom/sec/dsm/system/PreferenceHelper;->isRepeatedMessage(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 236
    :cond_0
    :goto_0
    return v1

    .line 222
    :cond_1
    invoke-static {p1}, Lcom/sec/dsm/system/PreferenceHelper;->getLastForwardingTime(Landroid/content/Context;)J

    move-result-wide v4

    const-wide/16 v6, 0x7530

    add-long/2addr v4, v6

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    cmp-long v3, v4, v6

    if-lez v3, :cond_5

    .line 223
    invoke-static {p1}, Lcom/sec/dsm/system/PreferenceHelper;->getLastMessage(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 224
    .local v0, "msg":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_2

    invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_3

    :cond_2
    move v1, v2

    .line 225
    goto :goto_0

    .line 227
    :cond_3
    invoke-virtual {v0, p2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    if-eqz v3, :cond_0

    .line 229
    invoke-virtual {v0, p2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v3

    const/16 v4, 0x28

    if-ge v3, v4, :cond_0

    :cond_4
    move v1, v2

    .line 232
    goto :goto_0

    .line 234
    .end local v0    # "msg":Ljava/lang/String;
    :cond_5
    invoke-static {p1, p3}, Lcom/sec/dsm/system/PreferenceHelper;->setLastMessage(Landroid/content/Context;Ljava/lang/String;)V

    move v1, v2

    .line 236
    goto :goto_0
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 22
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 28
    new-instance v13, Lcom/sec/dsm/system/DSMRepository;

    move-object/from16 v0, p1

    invoke-direct {v13, v0}, Lcom/sec/dsm/system/DSMRepository;-><init>(Landroid/content/Context;)V

    .line 29
    .local v13, "dsmRep":Lcom/sec/dsm/system/DSMRepository;
    const-string v4, "android.provider.Telephony.SMS_RECEIVED"

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 30
    const-string v4, "Start to SMS forwarding service"

    invoke-static {v4}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 31
    const-string v4, "SMSForwarding"

    invoke-virtual {v13, v4}, Lcom/sec/dsm/system/DSMRepository;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 32
    const-string v18, ""

    .line 33
    .local v18, "senderAddress":Ljava/lang/String;
    const-string v15, ""

    .line 34
    .local v15, "message":Ljava/lang/String;
    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    .line 35
    .local v16, "messageBody":Ljava/lang/StringBuilder;
    invoke-static/range {p2 .. p2}, Lcom/sec/dsm/system/DSMSMSReceiver;->getMessagesFromIntent(Landroid/content/Intent;)[Landroid/telephony/SmsMessage;

    move-result-object v17

    .line 36
    .local v17, "msgs":[Landroid/telephony/SmsMessage;
    if-eqz v17, :cond_0

    .line 37
    const/4 v4, 0x0

    aget-object v19, v17, v4

    .line 38
    .local v19, "sms":Landroid/telephony/SmsMessage;
    invoke-static {}, Landroid/telephony/SmsManager;->getDefault()Landroid/telephony/SmsManager;

    move-result-object v2

    .line 39
    .local v2, "smsManager":Landroid/telephony/SmsManager;
    const-string v4, "SMSRecipient"

    invoke-virtual {v13, v4}, Lcom/sec/dsm/system/DSMRepository;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 40
    .local v3, "recipient":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "+"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 41
    invoke-virtual/range {v19 .. v19}, Landroid/telephony/SmsMessage;->getOriginatingAddress()Ljava/lang/String;

    move-result-object v18

    .line 42
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "From<"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, ">"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    .line 43
    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 44
    const/16 v4, 0xa

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 45
    invoke-virtual/range {v19 .. v19}, Landroid/telephony/SmsMessage;->getMessageBody()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 47
    invoke-static {}, Lcom/sec/dsm/system/DSMSMSReceiver;->isDomesticModel()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 48
    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/16 v6, 0x50

    invoke-static {v4, v6}, Lcom/sec/dsm/system/DSMSMSReceiver;->cutFirstStrInByte(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v15

    .line 53
    :goto_0
    invoke-virtual/range {v19 .. v19}, Landroid/telephony/SmsMessage;->getMessageBody()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v4, v15}, Lcom/sec/dsm/system/DSMSMSReceiver;->isRepeatedMessage(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 54
    const-string v4, "ignore repeated message. It may be forwarded by my phone, or 5times within 30sec"

    invoke-static {v4}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    .line 55
    const-string v4, "SMSForwarding"

    invoke-virtual {v13, v4}, Lcom/sec/dsm/system/DSMRepository;->remove(Ljava/lang/String;)V

    .line 176
    .end local v2    # "smsManager":Landroid/telephony/SmsManager;
    .end local v3    # "recipient":Ljava/lang/String;
    .end local v15    # "message":Ljava/lang/String;
    .end local v16    # "messageBody":Ljava/lang/StringBuilder;
    .end local v17    # "msgs":[Landroid/telephony/SmsMessage;
    .end local v18    # "senderAddress":Ljava/lang/String;
    .end local v19    # "sms":Landroid/telephony/SmsMessage;
    :cond_0
    :goto_1
    return-void

    .line 50
    .restart local v2    # "smsManager":Landroid/telephony/SmsManager;
    .restart local v3    # "recipient":Ljava/lang/String;
    .restart local v15    # "message":Ljava/lang/String;
    .restart local v16    # "messageBody":Ljava/lang/StringBuilder;
    .restart local v17    # "msgs":[Landroid/telephony/SmsMessage;
    .restart local v18    # "senderAddress":Ljava/lang/String;
    .restart local v19    # "sms":Landroid/telephony/SmsMessage;
    :cond_1
    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    goto :goto_0

    .line 58
    :cond_2
    invoke-virtual {v2, v15}, Landroid/telephony/SmsManager;->divideMessage(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v5

    .line 59
    .local v5, "sendMessage":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v6, 0x1

    if-le v4, v6, :cond_3

    .line 60
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v12

    .line 61
    .local v12, "count":I
    const/4 v14, 0x1

    .local v14, "i":I
    :goto_2
    if-ge v14, v12, :cond_3

    .line 62
    const/4 v4, 0x1

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 61
    add-int/lit8 v14, v14, 0x1

    goto :goto_2

    .line 65
    .end local v12    # "count":I
    .end local v14    # "i":I
    :cond_3
    const-string v4, "phone"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Landroid/telephony/TelephonyManager;

    .line 66
    .local v20, "tm":Landroid/telephony/TelephonyManager;
    if-eqz v20, :cond_4

    .line 67
    invoke-virtual/range {v20 .. v20}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    move-result v21

    .line 76
    .local v21, "type":I
    packed-switch v21, :pswitch_data_0

    .line 159
    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v9, 0xff

    const/4 v10, 0x0

    const/4 v11, 0x2

    invoke-virtual/range {v2 .. v11}, Landroid/telephony/SmsManager;->sendMultipartTextMessage(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;ZIII)V

    .line 168
    .end local v21    # "type":I
    :goto_3
    const-string v4, "SMS forwarding enable"

    invoke-static {v4}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    goto :goto_1

    .line 78
    .restart local v21    # "type":I
    :pswitch_0
    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/telephony/SmsManager;->sendMultipartTextMessage(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    goto :goto_3

    .line 165
    .end local v21    # "type":I
    :cond_4
    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v9, 0xff

    const/4 v10, 0x0

    const/4 v11, 0x2

    invoke-virtual/range {v2 .. v11}, Landroid/telephony/SmsManager;->sendMultipartTextMessage(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;ZIII)V

    goto :goto_3

    .line 172
    .end local v2    # "smsManager":Landroid/telephony/SmsManager;
    .end local v3    # "recipient":Ljava/lang/String;
    .end local v5    # "sendMessage":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v15    # "message":Ljava/lang/String;
    .end local v16    # "messageBody":Ljava/lang/StringBuilder;
    .end local v17    # "msgs":[Landroid/telephony/SmsMessage;
    .end local v18    # "senderAddress":Ljava/lang/String;
    .end local v19    # "sms":Landroid/telephony/SmsMessage;
    .end local v20    # "tm":Landroid/telephony/TelephonyManager;
    :cond_5
    const-string v4, "SMS forwarding Disable"

    invoke-static {v4}, Lcom/sec/dsm/system/Util;->Logd(Ljava/lang/String;)V

    goto :goto_1

    .line 76
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method
