.class Lcom/sec/dsm/system/SlidingTab$Handle$3;
.super Landroid/os/Handler;
.source "SlidingTab.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/dsm/system/SlidingTab$Handle;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/dsm/system/SlidingTab$Handle;


# direct methods
.method constructor <init>(Lcom/sec/dsm/system/SlidingTab$Handle;)V
    .locals 0

    .prologue
    .line 493
    iput-object p1, p0, Lcom/sec/dsm/system/SlidingTab$Handle$3;->this$1:Lcom/sec/dsm/system/SlidingTab$Handle;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 8
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/16 v7, 0x64

    const/4 v6, 0x4

    .line 495
    iget v4, p1, Landroid/os/Message;->what:I

    packed-switch v4, :pswitch_data_0

    .line 523
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 526
    :goto_0
    return-void

    .line 497
    :pswitch_0
    const/4 v1, 0x0

    .line 499
    .local v1, "arrowImage":Landroid/widget/ImageView;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-ge v2, v6, :cond_2

    .line 500
    const/4 v3, 0x0

    .line 501
    .local v3, "index":I
    iget-object v4, p0, Lcom/sec/dsm/system/SlidingTab$Handle$3;->this$1:Lcom/sec/dsm/system/SlidingTab$Handle;

    # getter for: Lcom/sec/dsm/system/SlidingTab$Handle;->mHandleType:I
    invoke-static {v4}, Lcom/sec/dsm/system/SlidingTab$Handle;->access$200(Lcom/sec/dsm/system/SlidingTab$Handle;)I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_0

    .line 502
    move v3, v2

    .line 504
    :cond_0
    iget-object v4, p0, Lcom/sec/dsm/system/SlidingTab$Handle$3;->this$1:Lcom/sec/dsm/system/SlidingTab$Handle;

    # getter for: Lcom/sec/dsm/system/SlidingTab$Handle;->mArrowImageViews:[Landroid/widget/ImageView;
    invoke-static {v4}, Lcom/sec/dsm/system/SlidingTab$Handle;->access$300(Lcom/sec/dsm/system/SlidingTab$Handle;)[Landroid/widget/ImageView;

    move-result-object v4

    aget-object v1, v4, v3

    .line 505
    invoke-virtual {v1}, Landroid/widget/ImageView;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v0

    .line 507
    .local v0, "ani":Landroid/view/animation/Animation;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/view/animation/Animation;->hasStarted()Z

    move-result v4

    if-nez v4, :cond_4

    .line 508
    :cond_1
    add-int/lit8 v4, v2, 0x1

    if-eq v6, v4, :cond_3

    .line 509
    const-wide/16 v4, 0x96

    invoke-virtual {p0, v7, v4, v5}, Lcom/sec/dsm/system/SlidingTab$Handle$3;->sendEmptyMessageDelayed(IJ)Z

    .line 518
    .end local v0    # "ani":Landroid/view/animation/Animation;
    .end local v3    # "index":I
    :cond_2
    :goto_2
    iget-object v4, p0, Lcom/sec/dsm/system/SlidingTab$Handle$3;->this$1:Lcom/sec/dsm/system/SlidingTab$Handle;

    # getter for: Lcom/sec/dsm/system/SlidingTab$Handle;->mTabCircle:Lcom/sec/dsm/system/SlidingTab$Handle$TabCircle;
    invoke-static {v4}, Lcom/sec/dsm/system/SlidingTab$Handle;->access$400(Lcom/sec/dsm/system/SlidingTab$Handle;)Lcom/sec/dsm/system/SlidingTab$Handle$TabCircle;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/sec/dsm/system/SlidingTab$Handle$TabCircle;->setVisibility(I)V

    .line 519
    iget-object v4, p0, Lcom/sec/dsm/system/SlidingTab$Handle$3;->this$1:Lcom/sec/dsm/system/SlidingTab$Handle;

    # invokes: Lcom/sec/dsm/system/SlidingTab$Handle;->makeArrowAnimation()Landroid/view/animation/AlphaAnimation;
    invoke-static {v4}, Lcom/sec/dsm/system/SlidingTab$Handle;->access$500(Lcom/sec/dsm/system/SlidingTab$Handle;)Landroid/view/animation/AlphaAnimation;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0

    .line 512
    .restart local v0    # "ani":Landroid/view/animation/Animation;
    .restart local v3    # "index":I
    :cond_3
    const-wide/16 v4, 0x384

    invoke-virtual {p0, v7, v4, v5}, Lcom/sec/dsm/system/SlidingTab$Handle$3;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_2

    .line 499
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 495
    nop

    :pswitch_data_0
    .packed-switch 0x64
        :pswitch_0
    .end packed-switch
.end method
