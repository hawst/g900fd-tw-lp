.class public Lcom/sec/android/app/SecProductFeature_RIL;
.super Ljava/lang/Object;
.source "SecProductFeature_RIL.java"


# static fields
.field public static final SEC_PRODUCT_FEATURE_RIL_AAB_PHONEBOOK_SYNC_MSG:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_ALARM_VOICEMAIL_WITH_CHECKING_CHANGE:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_ALWAYS_DISPLAY_GEODESCRIPTION:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_ALWAYS_SEND_DTMF:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_ALWAYS_SET_IND_ENABLE:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_AP_CFUN:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_AP_SIM_PIN:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_ATT_OPNAME_LIST:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_AT_DISTRIBUTOR_UART_PATH:Ljava/lang/String; = "/dev/ttyHSL0"

.field public static final SEC_PRODUCT_FEATURE_RIL_AT_DISTRIBUTOR_WITHOUT_MULTICLIENT:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_AUTO_REDIAL:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_BAND_SUPPORT_3G_ONLY:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_BIP_AP_HANDLING_FOR_QCOM:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_BIP_OTA_APN_NAME:Ljava/lang/String; = ""

.field public static final SEC_PRODUCT_FEATURE_RIL_BIP_OTA_USIM_REGISTRATION:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_BIP_OTA_USIM_REGISTRATION_CMC:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_BIP_SEND_IMMEDIATE_TR_AFTER_DISCONNECT_HIPRI:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_BIP_SKIP_LENGH_CHECK_FOR_ACCESSING_ISIS_BADAPN:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_BIP_USE_APN_FOR_BEARER_DEFAULT:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_BLOCK_ATCOMMAND_ONSHIP:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_BLOCK_AUTO_APN_CHANGE:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_BLOCK_DATASETUP_BY_CDMA_LOCK_AUTH_FAIL:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_BLOCK_FDD_LTE:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_BLOCK_FD_DURING_CALL:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_BLOCK_FD_DURING_TETHERING:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_BLOCK_FD_DURING_VOLTE_CALL_WITH_SLICK3:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_BLOCK_NITZ_INFO:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_BLOCK_OTASP_NOTIFY_IN_CDMALTE:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_BLOCK_OTHER_CARRIER_USIM:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_BLOCK_PARTIAL_RETRY:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_BLOCK_SCREEN_WHEN_PUK10:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_BLOCK_SMS_DURING_ECC_CALL:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_BLOCK_SPECIAL_NETWROK_BY_MOBILE_DATA_DB:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_BLOCK_TETHERING_ONWIFI:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_BLOCK_UPDATE_PINPUK_RETRY:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_BLOCK_USB_AUTO_SUSPEND:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_BLOCK_USER_OTASP_DIALING:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_CACHE_DATA_REG_STATUS:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_CALIBRATION_TIME_ZONE:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_CALL_ANSWERING_MESSAGE:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_CALL_BARRING_API:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_CALL_CDMA_ONLY:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_CALL_CHANGE_PDN_BY_ROAMSTATE:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_CALL_CLEAR_SELECTION_MANUAL:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_CALL_CLIR_CALIBRATION:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_CALL_CNAP_KSC5601:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_CALL_CS_VT_SUPPORT:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_CALL_CS_VT_TYPE_VIDEO:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_CALL_CURRENT_CALL_LIST_CALIBRATION:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_CALL_DEFLECTION:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_CALL_DEVICE_QUALITY_STATISTICS_LOGGING:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_CALL_DISPLAY_SIGNAL_ICON_ON_LTE_LIMITED:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_CALL_DONOT_INTERNALCLEARDISCONNECTED_WHEN_USER_HANGUP:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_CALL_DO_NOT_SUPPORT_SUBADDRESS:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_CALL_DUALMODE_CDMAGSM:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_CALL_DUALMODE_GSMGSM:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_CALL_DUALMODE_ONECP_GSMGSM:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_CALL_DUALMODE_OPEN_GSMGSM:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_CALL_DUAL_PHONENUMBER:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_CALL_DUOS_CDMAGSM:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_CALL_DUOS_CGG:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_CALL_DUOS_COMMON:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_CALL_DUOS_GSMGSM:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_CALL_DUOS_TDSCDMA:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_CALL_ECCLIST_WITH_PLMN_FOR_CDMA:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_CALL_ENABLE_NETWORKING_INDI:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_CALL_EXTENDED_MMICODE:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_CALL_GCF_FT:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_CALL_HANGUP_AFTER_PLATFORM_RESET:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_CALL_HANGUP_DETECT_PLATFORM_RESET:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_CALL_INSERT_CALLFAILREASON_4CIQ:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_CALL_LTE_SINGLEMODE_SUPPORT:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_CALL_MODIFY_RSSI_THRESHOLE:Ljava/lang/String; = ""

.field public static final SEC_PRODUCT_FEATURE_RIL_CALL_NEXTI:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_CALL_NOT_SEND_CALLSTATECHANGED_EVENT:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_CALL_NO_DISP_INFO_WHEN_INCOMING_WAITING:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_CALL_ONGOING_EMERGENCY_CALL_STATE:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_CALL_SEND_RESPONSE_AFTER_CALL_STATUS_INDI:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_CALL_SET_EXTENDED_DIALING_WHEN_EMERGENCYCALL:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_CALL_SET_NO_SIM_ECCLIST_UNDER_AIRPLANE_MODE:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_CALL_SET_VOLTECALLLIST_TO_QC_CP:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_CALL_SHOW_CDMA_RAT:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_CALL_SHOW_EMERGENCY:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_CALL_SHOW_NUMBER_FORMAT:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_CALL_SINGLEPHONE_DUALMODE_CDMAGSM:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_CALL_SPR_FAKE_OPERATOR_NUMERIC:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_CALL_SPR_GLOBAL_CHECK_DIALING:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_CALL_SUPS_NOTI_EXCEPTION:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_CALL_TRACK_NOTIFYCALLSTATE:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_CALL_UPDATE_TIME_FOR_QCTIMESERVICE:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_CALL_USE_CURRENT_PLMN_NAME:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_CALL_USE_QCOM_IMS_AUDIO_MODE:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_CALL_USSD_EXCEPTION:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_CALL_USSD_KSC5601:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_CALL_US_INTERNATIONAL_DIALING:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_CALL_VIDEOCALL_REJECT_WHEN_SIPCALL_CONNECTED:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_CALL_VISUAL_EXPRESSION:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_CALL_WATCH_RIL_RESET:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_CB:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_CDMAHIDDEN_ON_QCRIL:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_CDMALTE_TELEPHONY_COMMON:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_CDMA_CALL_ACTIVE_AFTER_DATA_BURST:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_CDMA_ID_EXTENED:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_CDMA_ONLY_DEVICE_ALLOW_SVDO:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_CDMA_ONLY_DEVICE_DUN_TETHER_ALLOWED:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_CDMA_RAT_SET:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_CDMA_SUPPORT_GLOBAL_MODE:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_CELLBROADCAST_WITHOUT_SIM:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_CFG_TTY:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_CHANGE_DIAL_OTASP:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_CHANGE_FTA_VERSION:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_CHANGE_PAUSE_DELAY:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_CHECK_CALL_FORWARDING:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_CHECK_CALL_FORWARDING_IN_ROAMING:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_CHECK_CBM_MODE_FOR_GLOBAL:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_CHECK_CREDIT_BY_DNS_QUERY:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_CHECK_DATAROAMING:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_CHECK_IMS_REG_DURING_E911:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_CHECK_INTERNATION_PHONE_NUMBER:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_CHECK_MCC:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_CHECK_MMI_OR_PHONENUMBER:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_CHECK_PLMN_FOR_SPR:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_CHECK_PLMN_WHEN_MANUAL_SELECTION:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_CHECK_SID_FOR_WRLONG_PLMN:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_CHECK_TECHNOLOGY_FOR_CDMA:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_CHECK_TTY_MODE_WHEN_E911:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_CHECK_VOICEMAIL_ROAMING:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_CLAT_ENABLE_FOR_DATAUSAGE:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_CLEAR_CS_CALL_LIST_WHEN_RF_ON:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_CLIR_ERR:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_CLIR_OVERRIDE:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_CLIR_RECALCULATION:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_CMC_POWER_OFF:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_CNAP:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_CNAP_DECODE_UCS2_TO_UTF8:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_COMMON_TXN_DEBUG:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_CONFIG_RTRE_FROM_PST:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_CONNECT_TO_QUALCOMM_PATCH:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_CONTROL_DEBUG_LEVEL:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_CONVERT_PLUS_PREFIX_NUMBER_TO_MNO_CODE:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_COPY_TO_SIM:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_COUNTER_TETHER_UPSTREAM_RETRY_LIMIT:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_CP_BASE_FD:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_CP_NOT_SUPPORT_LTE_ROAMING_IN_FUSION_TWOCHIP:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_CP_PRL_VERSION_NOT_SUPPORT:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_CSC_SPRINT_CHAMELEON:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_CTCMTR_IR_DRIVEN_TYPE:Ljava/lang/String; = ""

.field public static final SEC_PRODUCT_FEATURE_RIL_CTCMTR_IR_SPEC:Ljava/lang/String; = ""

.field public static final SEC_PRODUCT_FEATURE_RIL_DATAONLY_DSDS_ONECHIP:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_DATA_1XCDMA_ONLY_MMS_ROAMING:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_DATA_ALLOW_1x_DATA_CALL:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_DATA_AUTO_ROAMING_APN_CHANGE:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_DATA_BLOCK_CIMI:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_DATA_CDMA_ONLY:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_DATA_CONN_ERRCODE_NOTI:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_DATA_DSDA_ONECHIP:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_DATA_DSDS_ONECHIP:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_DATA_DUALMODE_CDMAGSM:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_DATA_DUALMODE_GSMGSM:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_DATA_DUOS_CDMAGSM:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_DATA_DUOS_CGG:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_DATA_DUOS_COMMON:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_DATA_DUOS_GSMGSM:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_DATA_ENHANCED_ROAM_GUARD:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_DATA_FAKE_APN_ON_GSM:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_DATA_FORCE_IPV4_PROTOCOL:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_DATA_HANDLE_CDMA_APN_JB43:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_DATA_KEEP_MOBILE_EVEN_WIFI_ON:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_DATA_LTE_CDMAGSM:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_DATA_ONECHIP_CGG:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_DATA_PREFDATATECH_SNAPSHOT:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_DATA_RETRY_MAX_CONN:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_DATA_ROAMING_POPUP_ON_BOOT:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_DATA_SINGLEPHONE_DUALMODE_CDMAGSM:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_DATA_SPR_GLOBALROAMING_APN:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_DATA_TECH_UPDATE:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_DATA_USE_VPN_PROXY:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_DEACTIVEPDP_AFTER_HANGUP_WHEN_RADIOPOWEROFF:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_DEFAULT_FD_TIME_MILLIS:Ljava/lang/String; = "5000,5000,5000,5000"

.field public static final SEC_PRODUCT_FEATURE_RIL_DEFAULT_NETWORK_CDMA_ONLY:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_DEFAULT_NETWORK_MODE_LTE_CDMA_AND_EVDO:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_DEFAULT_NETWORK_MODE_LTE_CDMA_EVDO_GSM_WCDMA:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_DELAYED_TIMEZONE_UPDATE:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_DELAY_SMD_INIT:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_DELAY_SS_MT_NOTIFICATION:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_DETECTED_ISIMSTATE_AS_READY:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_DIAL_FOR_SHORTCODE:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_DISABLE_UPDATEITEM_CDMA:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_DISABLE_USB30_WHEN_RSSI_LOW:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_DISABLE_WCDMA19002100_BAND:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_DISPLAY_ANTENNA_LIMITED:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_DISPLAY_CLI_USING_REC:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_DISPLAY_CNAP:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_DISPLAY_COUNTRY_CHANGE_POPUP_IN_MANUAL_MODE:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_DISPLAY_EMERGENCYCALLONLY:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_DISPLAY_FOTA_TYPE:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_DISPLAY_FROM_OPLIST_ON_NETWORKLIST:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_DISPLAY_HIPRI:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_DISPLAY_NETSTAT_PS_ONLY_REGISTRATION:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_DISPLAY_NITZ_POPUP:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_DISPLAY_PDP_REJECT_CAUSE_NUMBER:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_DISPLAY_PLMNNAME_SIMLOAD:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_DISPLAY_PLMN_BY_LU_REJECT_CAUSE:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_DISPLAY_PRESENTATION_INDICATOR:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_DISPLAY_RESTRICT_NOTI:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_DISPLAY_RSSI_LTE_ONLY:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_DISPLAY_SS_CW_ERR:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_DISPLAY_SS_EUR:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_DISPLAY_SS_JPN:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_DISP_RSSI_ONE_STEP_PER_SEC:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_DM_LOGGING_SERVICE:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_DNS_FOR_KNOX:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_DNS_RETRY_TIMEOUT:Ljava/lang/String; = "5"

.field public static final SEC_PRODUCT_FEATURE_RIL_DNS_RETRY_TO_SECONDARY:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_DONOT_CONTROL_CP_POWER:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_DONOT_RESET_DB_WHEN_APN_XML_CHANGES:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_DONOT_UPDATE_ICON_DURING_CALL_WITH_OTHER_SIM:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_DONOT_USE_FAKE_DATA_TECH:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_DONOT_USE_RFS:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_DO_NOT_CONVERT_CNAP:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_DO_NOT_DISPLAY_EONS_ON_NETWORK_LIST:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_DO_NOT_REFER_SPN:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_DO_NOT_REREGISTER_WHEN_DATA_STALL:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_DO_NOT_SEND_SCREEN_STATE:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_DR_NO_INTERFACE:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_DR_USE_DIAG:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_DR_USE_DIAG_VIA:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_DSAC:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_DSDA_ONECHIP_CDMAGSM:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_DSDA_ONECHIP_GSMGSM:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_DSDS_ON_MARVELL:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_DSDS_ON_QCRIL:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_DTCPLIB_USING_MOBICORE_TZ_OS:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_DUALMODE_CARD_RADIO_STATE_MANAGEMENT:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_DUAL_VOICECALL_RF_BACKOFF:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_DVFS_BUS_MODE_ON:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_E911_REDIAL_EXCEPTION_NUMBERS:Ljava/lang/String; = ""

.field public static final SEC_PRODUCT_FEATURE_RIL_E911_REDIAL_POLICY:Ljava/lang/String; = "default"

.field public static final SEC_PRODUCT_FEATURE_RIL_E911_ROUTING_POLICY:Ljava/lang/String; = "default"

.field public static final SEC_PRODUCT_FEATURE_RIL_EMBMS_NOTI_FOR_SIPC:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_EMERGENCY_CALL:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_EMERGENCY_ONLY_WITH_NOSIM:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_ENABLE_AP_ORIENTED:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_ENABLE_AUTO_RETRIEVE_MMS:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_ENABLE_CHANGE_DEFAULT_PDN:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_ENABLE_CHANGE_DEFAULT_PDN_RECONNECT_SAME_PDN:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_ENABLE_CHANGE_DEFAULT_PDN_SETTING_DELAY:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_ENABLE_CLAT:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_ENABLE_DHCPV6_WITHOUT_IP:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_ENABLE_DIVERSITY_HIDDEN_MENU:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_ENABLE_ECCLIST_WITH_PLMN:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_ENABLE_FAST_DORMANCY:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_ENABLE_FAST_DORMANCY_ONLY_FOR_CDMA:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_ENABLE_INDIRECT_ATCMD_INPUT:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_ENABLE_LTE_SPECIFIC_SETTING:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_ENABLE_MULTIPLE_APNTYPE:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_ENABLE_PST_SUPPORT:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_ENABLE_REL8_FAST_DORMANCY:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_ENABLE_ROAMINGDIALOG:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_ENABLE_SEND_REJECT_CAUSE:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_ENABLE_SMART_BONDING:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_ENABLE_TETHERING_ONGROUPPLAY:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_ENABLE_TETHERING_OTP:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_ENABLE_VTFALLBACK:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_ERI_POLICY:Ljava/lang/String; = "0"

.field public static final SEC_PRODUCT_FEATURE_RIL_ERI_VZW:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_EXCEPTION_SID:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_EXTRA_PREFIX_EMGERENCY_CALL:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_FACTORY_OLD_CONCEPTS:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_FACTORY_PXA986_CONCEPTS:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_FACTORY_USE_INTERNAL_MODEL_NAME:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_FAST_DORMANCY_TIME_CHANGE:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_FD_DEFAULT_ON:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_FIND_WHO_CHANGED_TELEPHONY_DB:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_FIXED_BAND_TABLE:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_FORCED_IMEI_LOCK_VERIFICATION:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_FORCE_CONNECT_MMS_FORBID_IN_ROAMING:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_FORCE_OTASP_UPDATE:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_FORCE_SET_DNS:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_FORWARD_IMS_REGI_INFO:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_FTA_CAL_DATE:Ljava/lang/String; = ""

.field public static final SEC_PRODUCT_FEATURE_RIL_FTA_HW_VERSION:Ljava/lang/String; = ""

.field public static final SEC_PRODUCT_FEATURE_RIL_FTA_SW_VERSION:Ljava/lang/String; = ""

.field public static final SEC_PRODUCT_FEATURE_RIL_GCF_SOR:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_GCF_SS:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_GET_EONS_AFTER_SIM_LOADED:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_GGSM_OPNAME:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_GLOBALMODE_SMS_ADDRESS_RULE:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_GPRS_CHECK_DTM_SUPPORT:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_GSM7BIT_INPUT_METHOD:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_HANDLE_VZW_SDM:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_HANDSET_PROPERTY:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_HANGUP_EXCEPTION_BY_NETWORK_NSS_RELEASE:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_HOTSPOT_MSISDN_RESET:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_IGNORE_PS_VOICE_INDICATION:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_IGNORE_PS_VOICE_INDICATION_DURING_VOLTE:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_IMEI_SUPPORT_FACTORY_RESET_IND:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_IMPLTEMENT_SEQ_DTMF_FOR_MULTI_TOUCH:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_IMS_ACCEPT_ATCMD:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_IMS_APN_NAME:Ljava/lang/String; = ""

.field public static final SEC_PRODUCT_FEATURE_RIL_IMS_PDN_REQUEST_AT_FIRST_AFTER_SERVICE_ACQUISITION:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_IMS_PDN_RETRY_TIME_CHANGE:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_IMS_USING_PCSCF:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_IMS_VOICE_DOMAIN_SETTING:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_IPBASED_CDMADATA:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_IPC40_CDMA:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_IPTABLES_CMD_RETRY:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_ISDB_TMM:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_JPN_SERVICE_CATEGORY:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_KAF_CHECK_DATA_AVAILABLE:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_KEEP_LTE_ICON_CSFB:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_KOR_USIM_PERSONALIZATION_QMI:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_L3_ACK:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_LATIN_OPNAME_LIST:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_LINK_DEVICE_PLD:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_LOAD_TRANSPARENT_EF_LONGER_THAN_255:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_LTE_ACQORDER_CSFB_TDSCDMA:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_LTE_CAP_CHECK_ON_BOOT_TIME:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_LTE_CDMAGSM:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_LTE_DISABLE_FUNC:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_LTE_INTERNAL_ATTACH_FOR_CMC_MODEM:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_LTE_ON_CDMA_GLOBAL_MODE:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_LTE_ROAMING_APN_NAME:Ljava/lang/String; = "lte-roaming.sktelecom.com"

.field public static final SEC_PRODUCT_FEATURE_RIL_LTE_ROAMING_ATTACH_APN_TYPE:Ljava/lang/String; = "default"

.field public static final SEC_PRODUCT_FEATURE_RIL_MAKE_MNC_3DIGITS:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_MANUAL_SELECT_REJECT_IND_CHECK:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_MASK_MNCMCC_FOR_APN:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_MDM6600_ONEDRAM_IF:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_MEID_BY_CUTTING_IMEI:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_MMS_2G_AUTODOWNLOAD:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_MMS_ALTERNATIVE_APN:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_MMS_BYPASS_PROXY:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_MMS_DELIVERYREPORT:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_MMS_FROM_FIELD_MDN:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_MMS_HTTP_USERNAI_HEADER:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_MMS_INCLUDE_CC_FOR_GROUP_MESSAGE:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_MMS_LOCAL_TIME:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_MMS_MDN_TAG_NAME:Ljava/lang/String; = ""

.field public static final SEC_PRODUCT_FEATURE_RIL_MMS_NON_OMA:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_MMS_READREPORT:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_MMS_RESIZING:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_MMS_SEND_THROUGH_TETHER:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_MMS_SEND_WITH_DISABLE_MOBILE_DATA:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_MMS_SUBJECT:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_MMS_SUPPORT_CONTENT_DISPOSITION:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_MMS_THROUGH_WIFI:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_MMS_THROUGH_WIMAX:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_MMS_UAP_BUILDID:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_MMS_UAP_TAG_NAME:Ljava/lang/String; = "x-wap-profile"

.field public static final SEC_PRODUCT_FEATURE_RIL_MMS_UAP_URL:Ljava/lang/String; = "http://wap.samsungmobile.com/uaprof/SM-G906S.xml"

.field public static final SEC_PRODUCT_FEATURE_RIL_MMS_UA_STRING:Ljava/lang/String; = "Android-Mms/2.0"

.field public static final SEC_PRODUCT_FEATURE_RIL_MMS_WAIT_FOR_IPV6:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_MNC_2DIGIT3DIGIT_SAMEOPNAME:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_MTU_SIZE:Ljava/lang/String; = "1440"

.field public static final SEC_PRODUCT_FEATURE_RIL_MULTISIM_CP_VENDOR_CONFIG:Ljava/lang/String; = ""

.field public static final SEC_PRODUCT_FEATURE_RIL_MULTISIM_DATA_CONTROL:Ljava/lang/String; = "ALL_SLOT_BASED_CONTROL"

.field public static final SEC_PRODUCT_FEATURE_RIL_MULTISIM_DUOS_CDMAGSM:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_MULTISIM_DUOS_CGG:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_MULTISIM_DUOS_GSMGSM:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_MULTISIM_SET_NETWORK_MODE:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_MULTISIM_SUPPORT_NETWORK_MODE_TYPE_LTE:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_MULTI_IMSI:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_NAM_SUPPORT:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_NAS_DONOT_UPDATE_PLMN_INFO:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_NATIONAL_LANGUAGE_SINGLE_SHIFT_TABLES:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_NCKBLOCK_USING_SECRIL:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_NETWORKLOCK_REBOOT:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_NETWORK_INFO_RAT:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_NETWORK_INFO_RAT_ACQ_ORDER_PREF:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_NETWORK_INFO_RAT_AUS:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_NETWORK_LTEON:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_NETWORK_SELECTION_RESULT:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_NFC_SECURED_LOG:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_NITZ_UPDATE_NETWORK_MCCMNC:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_NOTIFICATION_INVALID_SIM:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_NOTIFICATION_NWLOCK_SIM:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_NOTIFY_SIM_STATUS_CHANGE_WHEN_AIRPLANEMODE:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_NOTI_TETHERING_DENIED:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_NOTI_WHEN_DATA_SETUP_FAIL:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_NOT_COUNT_VOICEMAIL:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_NOT_STORING_VOICE_MAIL_IN_DEVICE:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_NOT_SUPPORT_CUG:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_NOT_SUPPORT_EXT_BRST_INTL:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_NO_DISPLAY_OPERATOR_NAME:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_NO_SIM_DISCARD_NOTI:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_NSRI_SECURITY_SOLUTION:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_NTT_EMERGENCY_CALL_FEATURE:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_NTT_STK:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_NV_2ND_PART_BACKUP:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_NV_2ND_PART_PATH:Ljava/lang/String; = "/dev/bml17"

.field public static final SEC_PRODUCT_FEATURE_RIL_NV_PART_PATH:Ljava/lang/String; = "/dev/mbin0"

.field public static final SEC_PRODUCT_FEATURE_RIL_NV_REBACKUP_INCLUDE_PRODCODE:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_NW_AP_CP_TIME_SYNC:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_NW_CDMA_CSFB:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_NW_CONVERT_PREFERRED_NETWORK_TYPE_WITH_BITMASK:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_NW_DWORD_ETSCHANNEL:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_NW_EXTENDED_SPN_OVERRIDE:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_NW_FAKE_OPERATOR_NUMERIC:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_NW_LTEONLY_WITH_CDMALTE_TELEPHONY:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_NW_SEND_CDMAEVDO_SIGINFO:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_NW_SIGNAL_BARS_FROM_CP:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_NW_SUPPORT_TDSCDMA:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_NW_VOICEREG_SNAPSHOT:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_NW_VRTE_WITHOUT_MODEPREF:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_OPERATOR_NAME_RULE_FOR_DCM:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_OPERATOR_RESERVED_PCO:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_OPNAME_LIST_SE13:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_OPTIS_TEST:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_OTASP_EXTENDED_DIGITS_ALLOWED:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_OTASP_STATUS_CHECK_NOT_NEEDED:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_OTA_USIM_REGISTRATION:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_OTA_USIM_REGISTRATION_KT:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_OTA_USIM_REGISTRATION_LGT:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_OTA_USIM_REGISTRATION_SKT:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_PARSE_OTASP_SCHEMA_FROM_RO_PROP:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_PATH_KERNEL_WAKELOCK_TIME:Ljava/lang/String; = "/sys/devices/platform/mdm_hsic_pm0/waketime"

.field public static final SEC_PRODUCT_FEATURE_RIL_PB_SDN:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_PCSCF_UPDATE:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_PERMANENT_FAILURES_STOP_RETRY:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_PERSIST_APN_DATABASE_CHANGES:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_PERSO_BLOCK:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_PHONEBOOK_SUPPORT_ANR:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_PHONE_TYPE_PS_ONLY_PROXIMITY_SYSFS:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_POWER_BACKOFF_SUPPORT:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_POWER_ON_OFF_ATTACH_DETACH:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_PREFERRED_CHANGE_BY_MOBILE_DATA:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_PREFERRED_CHANGE_BY_MOBILE_DATA_EXCEPTIONAL_CASE:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_PREF_NETWORK_MODE_SET_ON_SIM_READY:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_PROTOBUF_PLMN_LIST:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_PS_BARRING_FOR_VOLTE:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_PWR_RADIO_OFF:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_QC_MSM8960_ETS_CHANNEL_IO:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_QC_MSM8960_TIME_SERVICES:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_QMI_CDMAGSM:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_QSPIDER_API:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_QUALCOMM_RSSI_CODE:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_RAT_CDMA:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_RAT_CDMA_MSM8X55:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_RAT_LTE:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_RAT_UMTS:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_READ_MDN_FOR_GLOBAL_MODE:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_READ_ONLY_ERI_TEXT:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_REATTACH_FOR_IMS_REG_FAIL:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_REFER_EONS_WITHOUT_LAC:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_REMOVE_ECB_EXIT_CALLBACK:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_REMOVE_RESTRICT_POPUP_ON_LTE:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_REPLACE_IMEI_SALES_CODE:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_REPLACE_USSD_FAIL:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_REPLY_PATH:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_RESET_CACHE_REG_STATE_WHEN_NETWORK_SELECTION:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_RESET_CURRENT_DATA_TECH:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_RESET_NOTIFY_TO_MULTICLIENT:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_RESET_RIL_REQUEST_PENDING:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_RESTORE_APN_AT_CSC_UPDATE:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_RESTORE_DEFAULT_APN_WHEN_APNS_XML_CHANGES:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_RESTRICT_DATA_DURING_VOICE_CALL:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_ROAMING_REG_WARNING_NOTI:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_RSSI_CDMA_ONLY:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_RTC_CP_BACKUP:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_RTS_INDICATION:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_RUIM_CDMA_ONLY:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SAMSUNG_LIBRIL_INTF_EXTENSION:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_SAN_PHONEBOOK_SYNC_MSG:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SAVE_CP_ONBOARD_LOG:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SECPHONESERVICE_TRIGGERED_BY_BOOTING_COMPLETE_INTENT:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_SEGMENTED_SMS:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SELECT_SMS_SENDING_ROUTE_IN_TELEPHONY:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SEND_APDU_FOR_ISIS:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SEND_DTMF_DIALING_STATE:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SEND_ECN:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SEND_FACTORY_MODE_INFO:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_SEND_SMS_BY_CP_IN_IMS_REGI:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SEND_SMS_CS_ALWAYS:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SET_CURRENTPLMN:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_SET_CURRENTPLMN_FOR_DUAL_IMSI:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SET_FD_FROM_SIM_OPERATOR:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SET_INTERNATION_ROAM:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SET_ISO_USING_MCC:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_SET_MTU_FROM_MODEM:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SET_NETWORKTYPE_WHEN_SEARCH_NETWORKLIST:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SET_NETWORK_SELECTION_AUTO_WHEN_RADIO_OFF:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SET_NETWORK_TYPE_WHEN_RADIO_OFF:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SET_PCSCF_PROPERTY:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SET_PREFERRED_NETWORK:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SET_SECURECLOCK:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_SET_SOUND_PATH_BY_PHONETYPE:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SET_VOLTE_AUDIO_PATH:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_SFR_FEMTO_NITZ_EXCEPTION:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SGLTE_ON_IPC:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SGLTE_ON_QCRIL:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SHOW_AUTOMATIC_TIME_POPUP:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SHOW_DATA_SELECT_POPUP_ON_BOOTUP:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_SHOW_HIDDEN_MENU:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SHOW_HIDDEN_MENU_DEBUG_SCREEN:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SHOW_HSDPA_ONLY:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SHOW_OPNAME_LIST:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_SHOW_RESCANDIALOG:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SHOW_SHORT_OPNAME:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_SHOW_UA_FIELD:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_SHUTDOWN_WAIT_FOR_MODEM_STATE:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SIGNALSTRENGTH_POLICY:Ljava/lang/String; = "0"

.field public static final SEC_PRODUCT_FEATURE_RIL_SIGNALSTRENGTH_USC:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SILENTLOG_STE:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SIM_BLOCK_IMSI_CSIM:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_SIM_BROADCAST_NO_SIM_NOTY:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_SIM_DSDA_ONECHIP:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SIM_DUALMODE_CDMAGSM:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SIM_DUALMODE_GSMGSM:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SIM_DUOS_CDMAGSM:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SIM_DUOS_CGG:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SIM_DUOS_COMMON:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SIM_DUOS_GSMGSM:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SIM_EF_EXT5_ADF:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SIM_GETDEVICEID_RETURN_IMEI:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SIM_HOT_SWAP:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SIM_IMS_CALL_CONTROL:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SIM_ISPSERVICE:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SIM_KDDI_OTA:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SIM_LANGUAGE:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_SIM_LTE_CDMAGSM:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SIM_MSISDN_SUPPORT_WILDCHAR:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SIM_MULTI_APPS_SUPPORT:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_SIM_PB_DCS_SUPPORT:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SIM_PERMANENT_BLOCK:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SIM_PHONEBOOKAPI:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_SIM_PLMNACTION_INTENT:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SIM_PROFILE_POPUP:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_SIM_RSIM_DONT_USE_APPTYPE_FIELD:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SIM_SINGLEPHONE_DUALMODE_CDMAGSM:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SIM_SLOTSWITCH:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SIM_SUPPORT_3GPP_CDMA:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SIM_SUPPORT_ICCID_CDMA:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SIM_TYPE:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_SIM_UPDATE_OTASP_USING_MIN:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SIM_USA_CDMA_ID_EXTENED:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SINGLE_ON_MARVELL:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SIPC40_INCOMING_SMS:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SKIP_EONS_INFO:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SKT_OPNAME_LIST:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_SMARTMXUICC:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SMSC:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SMSIP_PDU_FORMAT:Ljava/lang/String; = "default"

.field public static final SEC_PRODUCT_FEATURE_RIL_SMS_ADDRESS_SEPARATOR:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_SMS_ALLOW_EMAILSMS_ADDRESS:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SMS_CBM:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SMS_CB_DEACT_WHEN_BOOT:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SMS_CB_PERSISTENT:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SMS_CDMA_COPY_TO_RUIM:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SMS_CDMA_NANP_USA:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SMS_CDMA_ONLY:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SMS_CDMA_REASSEMBLY:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SMS_CDMA_SENT_FAIL_DISPLAY:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SMS_CDMA_WRITE_TO_USIM:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SMS_CMAS:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SMS_CML:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SMS_CP_NOT_SUPPORTED:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SMS_DELIVERYREPORT:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SMS_DUALMODE_CDMAGSM:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SMS_DUALMODE_GSMGSM:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SMS_DUOS_CDMAGSM:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SMS_DUOS_CGG:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SMS_DUOS_COMMON:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SMS_DUOS_GSMGSM:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SMS_DUPLICATE_ONLY_USERDATA_TIMESTAMP_ADDRESS:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SMS_DUPLICATE_PORT_WAP_PUSH:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_SMS_EMERGENCY_ALERT:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SMS_EMPTYBODY_SUPPORT:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SMS_ERROR_CLASS_RETRY:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SMS_EUALERT:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SMS_FOLLOW_ON_REQ:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SMS_FORCED_ALLOW_SHORTCODE_ADDRESS:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SMS_GCF_PDA_MEMORY_FULL_TEST:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SMS_GET_DOMESTIC_TIME_INFO:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_SMS_INTERNATIONAL_TYPE_REPLY_ADDRESS_REPLACE_TO_ZERO:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_SMS_KPAS_TEST_MESSAGE:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_SMS_KSC5601_SEND:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SMS_LGT_BROADCAST_MESSAGE:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SMS_LGT_BROADCAST_MESSAGE_VIA:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SMS_LOAD_SHORTCODE_FROM_RESOURCE_ONLY:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SMS_LOCAL_TIME:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SMS_LTE_SINGLEMODE_SUPPORT:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SMS_MAX_RETRIES_ONE:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SMS_MAX_RETRIES_ZERO:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SMS_MOVE_TO_USIM:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SMS_NON_OMA_DCS:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SMS_NON_OMA_VOICE_VIDEO_MSG:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SMS_NOT_USED_STATUS_REPORT_REQUEST:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_SMS_NOT_USED_VALIDITY_PERIOD_FORMAT:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_SMS_NTT_REQUIREMENT:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SMS_OTA_MESSAGE:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SMS_PATTERN_LOCK_MESSAGE:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SMS_PID_MESSAGE:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SMS_PORT_ADDRESSED_MESSAGE:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_SMS_PS_DOMAIN_SUPPORT:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SMS_READ_CONFIRM:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SMS_READ_SIM_FROM_FIRST_INDEX:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SMS_REPLACE_DTMF_ADDRESS:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SMS_RETRY_VOICECALL_ERROR:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SMS_SEND_ACK_TIMEOUT:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SMS_SEND_CONNECTED_RRC_STATUS:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_SMS_SEND_WITH_LANGUAGE_SELECTION:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SMS_SHOW_HIDDEN_MENU_SMSC_SETTING:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_SMS_SHOW_HIDDEN_MENU_SMS_PREFMODE:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_SMS_SINGLEPHONE_DUALMODE_CDMAGSM:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SMS_SMSP:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SMS_SUPPORT_HEXTYPE_NUM_OF_MESSAGE:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SMS_SUPPORT_KSC5601:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_SMS_SUPPORT_KSC5601_USA:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SMS_SUPPORT_REPLY_ADDRESS:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_SMS_SUPPORT_REPLY_ADDRESS_WITHOUT_RAE:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SMS_SUPPORT_SENDER_ADDRESS_CHANGE:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SMS_SYNCHRONOUS_SENDING:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SMS_TABLET_DISABLE_MESSAGE:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SMS_TID_MESSAGE:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SMS_UNKNOWN_SUBPARAMETER:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SMS_WAPPUSH_MANAGER:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SPECIFIC_TETHER_APN:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SPEECH_CODEC_EVENTS_DISABLE:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SPLIT_AUTO_MANUAL_TIMEZONE:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SPLIT_SIGNAL_BARS_AND_RSSI:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_SPLIT_SIGNAL_BARS_AND_RSSI_LTE:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SPRINT_EXTENTION:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SPRINT_OTASP_STATUS_NOT_NEEDED:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SPR_ECM_INCOMING_SMS:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SPR_SET_ATTACH_APN:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SRVCC_REDUCE_VOICE_GAP_DURING_HANDOVER:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SRVCC_REL:Ljava/lang/String; = "8"

.field public static final SEC_PRODUCT_FEATURE_RIL_SSAC_INFO_TO_IMS:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SS_BARRING_PASSWORD:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_SS_FAIL_NOTIFY:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SS_FORWARDING_ICON:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_SS_ROUTING_POLICY:Ljava/lang/String; = "cs"

.field public static final SEC_PRODUCT_FEATURE_RIL_SS_USE_USSD_WHEN_USSDUTF16_ERROR:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SS_USING_UT:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_STE_ISSW_FOR_ICS:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_STE_SECURITY_AUTHORIZATION:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_STK_DISPLAYTEXT_UI_TIMEOUT:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_STK_LAUNCH_BROWSER_MULTIPLE_TABS_SUPPORT:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_STK_QMI_RIL:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_STK_REFRESH_MSG:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_STK_REFRESH_TR:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_STK_REFRESH_UPDATE_EXT_EF:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_STK_SETUP_IDLE_MODE_TEXT_SUPPORT:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_STK_SIM_RESET_REFRESH:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_STK_SUPPORT:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_STK_SUPPORT_LAUNCH_BROWSER:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SUPPORT_BATTARY_FACTORYRESET:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SUPPORT_BIP:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_SUPPORT_CATSERVICE_IN_RIL:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SUPPORT_CDMA_CMAS:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SUPPORT_CDMA_DUN:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SUPPORT_COMMON_COEX_CONTROL:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SUPPORT_DONGLE_FUNCTION:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SUPPORT_DTMF_NOTI:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SUPPORT_DUAL_MODE:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SUPPORT_EPDG:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SUPPORT_IMSCALLAPP:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SUPPORT_IMS_SERVICE:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_SUPPORT_LTE_ROAMING:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_SUPPORT_MWIS_CFIS:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SUPPORT_OTASP_MODE_IN_GOOGLE_SETUP_WIZARD:Ljava/lang/String; = ""

.field public static final SEC_PRODUCT_FEATURE_RIL_SUPPORT_PC_MODEM:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SUPPORT_QCOM_IMS:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SUPPORT_QCRIL_USIM_ANR_TYPE:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SUPPORT_QMI_CHANGE_IMS_PROFILE_NAME:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SUPPORT_QUALCOMM_IMS:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SUPPORT_QUALCOMM_IMS_OVER_IPC:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SUPPORT_REMOTE_SIM_UNLOCK:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SUPPORT_SAMSUNG_OEMHOOKRAW:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_SUPPORT_SIPC_USIM_ANR_TYPE:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SUPPORT_SLOT2_DATA:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SUPPORT_SRVCC:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_SUPPORT_USIM_MOBILITY:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SUPPORT_USIM_MOBILITY_EPS_ONLY_SUPPORT:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SUPPORT_VOLTE:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_SUPPORT_VOLTEVTCALL:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SUPPORT_VOLTE_USSD:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SUPPORT_VT_EMERGENCY_CALL:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_SUPPORT_VT_EMERGENCY_CALL_WITH_EPDN:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SWITCH_PHYSICAL_UART_FOR_CP_CAL:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SYNC_APNS_FOR_GENERIC:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SYNC_CS_PS:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_SYNC_DORMANT_STATE_WITH_MODEM:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_TCP_SETTING_WHEN_NW_TYPE_CHANGED:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_TDSCDMA_SPRDTRUM:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_TDSCDMA_SPRDTRUM_ONECHIP:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_TESTNV_EXTENSION:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_TETHERING_ALWAYSON_USE_DIAG_FOR_DM:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_TETHERING_DEFAULT_ON:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_TETHERING_IPV6:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_TETHERING_IPV6_SEC:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_TETHERING_NOT_DUN_REQUIRED_ON_ROAM:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_TETHER_APN_NAME:Ljava/lang/String; = ""

.field public static final SEC_PRODUCT_FEATURE_RIL_TIMEZONE_UPDATE_BY_CACHED_MCC:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_TMO_OPNAME_LIST:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_UART_ERICSSON:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_UPDATE_AND_RESTORE_APNS_FOR_GENERIC:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_UPDATE_CSC_APN_AFTER_SWITCHING_IMSI:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_UPDATE_GPRS_STATE_WHEN_AIRPLANEMODE:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_UPDATE_NITZ_ZONEID:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_UPDATE_OPERATOR_NUMERIC_FOR_CDMAONLY_PHONE:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_UPDATE_PLMN_AFTER_SIM_REFRESH:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_UPDATE_PROPERTY_FOR_PLMN:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_UPDATE_RAT_INFO_FOR_CSREG:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_UPDATE_SPN_FROM_EF:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_UPDATE_SUBCRIBERID_FOR_SPR:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_USED_NETWORK_TYPE:Ljava/lang/String; = ""

.field public static final SEC_PRODUCT_FEATURE_RIL_USE_CP_RTC:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_USE_LEGACY_PCSCF_NAMES:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_USE_LIMITED_SERVICE_STATUS:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_USE_LTE_ACQORDER_NETWORKSEARCH_PLMNSELECTION:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_USE_MODEM_INTERFACE_ID_FOR_IPV6:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_USE_RATINFO_NETWORKSEARCH_PLMNSELECTION:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_USE_RAT_FOR_MANUAL_SELECTION:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_USE_SECRIL_WITH_QCRIL:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_USE_SEC_7BIT_DECODE_FUNC:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_USE_SIM_DETECT:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_USIM_PERSONALIZATION:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_USING_DUN_APN:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_USSD_ALERT:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_USSD_EXCEPTION:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_USSD_ROUTING_POLICY:Ljava/lang/String; = "cs"

.field public static final SEC_PRODUCT_FEATURE_RIL_VENCUVER_NITZ_TIMEZONE:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_VIDEO_MESSAGE_NAME:Z = true

.field public static final SEC_PRODUCT_FEATURE_RIL_VM_DEFAULT_AS_MDN:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_VOICELESS_OTA_PROVISIONING:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_VOICEMAIL:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_VOLTE_INVITE_FLUSH_REQUEST:Z = false

.field public static final SEC_PRODUCT_FEATURE_RIL_VOLTE_REDIAL_POLICY:Ljava/lang/String; = "skt"

.field public static final SEC_PRODUCT_FEATURE_RIL_VZW_GLOBAL_DIALING:Z

.field public static final SEC_PRODUCT_FEATURE_RIL_VZW_PDN_THROTTLE:Z

.field public static final SEC_PRODUCT_FEATURE_RIL_WBAMR_DEFAULT_ON:Z

.field public static final SEC_PRODUCT_FEATURE_RIL_WHEN_TECH_IS_LTE_ALLOW_DATACALL:Z

.field public static final SEC_PRODUCT_FEATURE_RIL_WIFI_CHANGED_TO_MOBILE:Z

.field public static final SEC_PRODUCT_FEATURE_RIL_WIFI_ONLY:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
