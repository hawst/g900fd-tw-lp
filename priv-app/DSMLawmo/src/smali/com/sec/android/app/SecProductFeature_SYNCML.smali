.class public Lcom/sec/android/app/SecProductFeature_SYNCML;
.super Ljava/lang/Object;
.source "SecProductFeature_SYNCML.java"


# static fields
.field public static final SEC_PRODUCT_FEATURE_SYNCML_BUILD_TYPE:Ljava/lang/String; = "STATIC"

.field public static final SEC_PRODUCT_FEATURE_SYNCML_CMCC_DM_VER:Ljava/lang/String; = "1"

.field public static final SEC_PRODUCT_FEATURE_SYNCML_CP_MULTISIM:Z = false

.field public static final SEC_PRODUCT_FEATURE_SYNCML_CP_VER:Ljava/lang/String; = "v3"

.field public static final SEC_PRODUCT_FEATURE_SYNCML_DM_DOWNLOAD_WIFI_ONLY:Z = false

.field public static final SEC_PRODUCT_FEATURE_SYNCML_DM_LIGHT_THEME_RES:Ljava/lang/String; = ""

.field public static final SEC_PRODUCT_FEATURE_SYNCML_DM_MOBILE_NETWORK_WARNING:Z = false

.field public static final SEC_PRODUCT_FEATURE_SYNCML_DM_NO_KIES:Z = false

.field public static final SEC_PRODUCT_FEATURE_SYNCML_DM_NO_SCHEDULE:Z = false

.field public static final SEC_PRODUCT_FEATURE_SYNCML_DM_OPERATOR:Ljava/lang/String; = "KOR"

.field public static final SEC_PRODUCT_FEATURE_SYNCML_DM_OPERATOR_KOR:Ljava/lang/String; = "SKT"

.field public static final SEC_PRODUCT_FEATURE_SYNCML_DM_PREDEFINED_PHONE_TYPE:Ljava/lang/String; = ""

.field public static final SEC_PRODUCT_FEATURE_SYNCML_DM_ROAMING_UNABLENETWORK_MSG:Z = true

.field public static final SEC_PRODUCT_FEATURE_SYNCML_DM_ROAMING_WIFI_ONLY:Z = true

.field public static final SEC_PRODUCT_FEATURE_SYNCML_DM_SW_UPDATE_MENU_IN_SETTINGS:Z = true

.field public static final SEC_PRODUCT_FEATURE_SYNCML_DM_VER:Ljava/lang/String; = "v3"

.field public static final SEC_PRODUCT_FEATURE_SYNCML_DS_LIGHT_THEME_RES:Ljava/lang/String; = ""

.field public static final SEC_PRODUCT_FEATURE_SYNCML_DS_MEMO_ENABLE:Z = false

.field public static final SEC_PRODUCT_FEATURE_SYNCML_DS_MEMO_TITLE_SELECT:Z = false

.field public static final SEC_PRODUCT_FEATURE_SYNCML_DS_OPERATOR:Ljava/lang/String; = "OPEN"

.field public static final SEC_PRODUCT_FEATURE_SYNCML_FCDMDSCP_THEME_RES:Ljava/lang/String; = ""

.field public static final SEC_PRODUCT_FEATURE_SYNCML_FOTACLIENT_DEFAULT_AUTO:Z = true

.field public static final SEC_PRODUCT_FEATURE_SYNCML_FOTACLIENT_DEFAULT_COUNTRYCODE:Ljava/lang/String; = "gb"

.field public static final SEC_PRODUCT_FEATURE_SYNCML_FOTACLIENT_DEFAULT_COUNTRYNAME:Ljava/lang/String; = "United Kingdom"

.field public static final SEC_PRODUCT_FEATURE_SYNCML_FOTACLIENT_DEFAULT_MCC:Ljava/lang/String; = "234"

.field public static final SEC_PRODUCT_FEATURE_SYNCML_FOTACLIENT_DEFAULT_WIFI:Z = false

.field public static final SEC_PRODUCT_FEATURE_SYNCML_FOTACLIENT_NEWCOTMO_CUST:Z = false

.field public static final SEC_PRODUCT_FEATURE_SYNCML_FOTACLIENT_NO_COUNTRY_LIST:Z = false

.field public static final SEC_PRODUCT_FEATURE_SYNCML_FOTACLIENT_PUSH_TYPE:Ljava/lang/String; = "GCM"

.field public static final SEC_PRODUCT_FEATURE_SYNCML_FOTACLIENT_URL_PREFIX:Ljava/lang/String; = "www"

.field public static final SEC_PRODUCT_FEATURE_SYNCML_FOTACLIENT_VER:Ljava/lang/String; = "v3"

.field public static final SEC_PRODUCT_FEATURE_SYNCML_LAWMO_LANDSCAPE_ONLY:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
