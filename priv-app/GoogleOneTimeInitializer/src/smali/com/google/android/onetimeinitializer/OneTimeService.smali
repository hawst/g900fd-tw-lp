.class public Lcom/google/android/onetimeinitializer/OneTimeService;
.super Landroid/app/IntentService;
.source "OneTimeService.java"


# static fields
.field private static final LAUNCHER_CONTENT_URI:Landroid/net/Uri;

.field private static final LAUNCHER_NOW_CONTENT_URI:Landroid/net/Uri;

.field private static final mDmAgentComponentName:Landroid/content/ComponentName;


# instance fields
.field private final mIntentOneTimeInitialized:Landroid/content/Intent;

.field private mMappingTable:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/content/ComponentName;",
            "Landroid/content/ComponentName;",
            ">;"
        }
    .end annotation
.end field

.field private mPreferences:Landroid/content/SharedPreferences;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 95
    const-string v0, "content://com.android.launcher2.settings/favorites?notify=true"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/onetimeinitializer/OneTimeService;->LAUNCHER_CONTENT_URI:Landroid/net/Uri;

    .line 105
    const-string v0, "content://com.google.android.launcher.settings/favorites"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/onetimeinitializer/OneTimeService;->LAUNCHER_NOW_CONTENT_URI:Landroid/net/Uri;

    .line 118
    new-instance v0, Landroid/content/ComponentName;

    const-string v1, "com.google.android.apps.enterprise.dmagent"

    const-string v2, "com.google.android.apps.enterprise.dmagent.DeviceAdminReceiver"

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/onetimeinitializer/OneTimeService;->mDmAgentComponentName:Landroid/content/ComponentName;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 136
    const-string v0, "OneTimeInitializer Service"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 131
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.onetimeinitializer.ONE_TIME_INITIALIZED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/onetimeinitializer/OneTimeService;->mIntentOneTimeInitialized:Landroid/content/Intent;

    .line 137
    return-void
.end method

.method private broadcastAndIncrement()V
    .locals 4

    .prologue
    .line 164
    iget-object v1, p0, Lcom/google/android/onetimeinitializer/OneTimeService;->mPreferences:Landroid/content/SharedPreferences;

    const-string v2, "run_count"

    const/4 v3, -0x1

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    add-int/lit8 v0, v1, 0x1

    .line 165
    .local v0, "currentRunCount":I
    iget-object v1, p0, Lcom/google/android/onetimeinitializer/OneTimeService;->mIntentOneTimeInitialized:Landroid/content/Intent;

    const-string v2, "run_count"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 166
    iget-object v1, p0, Lcom/google/android/onetimeinitializer/OneTimeService;->mPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "run_count"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 167
    iget-object v1, p0, Lcom/google/android/onetimeinitializer/OneTimeService;->mIntentOneTimeInitialized:Landroid/content/Intent;

    invoke-virtual {p0, v1}, Lcom/google/android/onetimeinitializer/OneTimeService;->sendBroadcast(Landroid/content/Intent;)V

    .line 168
    return-void
.end method

.method private disableDevicePolicy()V
    .locals 11

    .prologue
    const/4 v10, 0x0

    .line 375
    const-string v7, "device_policy"

    invoke-virtual {p0, v7}, Lcom/google/android/onetimeinitializer/OneTimeService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/admin/DevicePolicyManager;

    .line 378
    .local v1, "devicePolicyManager":Landroid/app/admin/DevicePolicyManager;
    const/4 v6, 0x0

    .line 379
    .local v6, "isDevicePolicyInstalled":Z
    invoke-virtual {p0}, Lcom/google/android/onetimeinitializer/OneTimeService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v7

    invoke-virtual {v7, v10}, Landroid/content/pm/PackageManager;->getInstalledApplications(I)Ljava/util/List;

    move-result-object v3

    .line 380
    .local v3, "installedApps":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ApplicationInfo;>;"
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ApplicationInfo;

    .line 381
    .local v0, "app":Landroid/content/pm/ApplicationInfo;
    const-string v7, "com.google.android.apps.enterprise.dmagent"

    iget-object v8, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 382
    const/4 v6, 0x1

    goto :goto_0

    .line 385
    .end local v0    # "app":Landroid/content/pm/ApplicationInfo;
    :cond_1
    if-eqz v6, :cond_2

    .line 386
    const-string v7, "com.google.android.apps.enterprise.dmagent"

    invoke-virtual {v1}, Landroid/app/admin/DevicePolicyManager;->getDeviceOwner()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    .line 387
    .local v5, "isDeviceOwner":Z
    sget-object v7, Lcom/google/android/onetimeinitializer/OneTimeService;->mDmAgentComponentName:Landroid/content/ComponentName;

    invoke-virtual {v1, v7}, Landroid/app/admin/DevicePolicyManager;->isAdminActive(Landroid/content/ComponentName;)Z

    move-result v4

    .line 388
    .local v4, "isDeviceAdmin":Z
    const-string v7, "OneTimeService"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Device Policy is owner: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", is admin:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 391
    if-nez v5, :cond_2

    if-nez v4, :cond_2

    .line 392
    const-string v7, "OneTimeService"

    const-string v8, "Disabling Device Policy, as it is not in use."

    invoke-static {v7, v8}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 393
    invoke-virtual {p0}, Lcom/google/android/onetimeinitializer/OneTimeService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v7

    const-string v8, "com.google.android.apps.enterprise.dmagent"

    const/4 v9, 0x4

    invoke-virtual {v7, v8, v9, v10}, Landroid/content/pm/PackageManager;->setApplicationEnabledSetting(Ljava/lang/String;II)V

    .line 398
    .end local v4    # "isDeviceAdmin":Z
    .end local v5    # "isDeviceOwner":Z
    :cond_2
    return-void
.end method

.method private doesComponentExist(Landroid/content/ComponentName;)Z
    .locals 3
    .param p1, "component"    # Landroid/content/ComponentName;

    .prologue
    const/4 v1, 0x0

    .line 264
    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.MAIN"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 265
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {v0, p1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 266
    invoke-virtual {p0}, Lcom/google/android/onetimeinitializer/OneTimeService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1
.end method

.method private generateQuickContactIcon(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 14
    .param p1, "sourceBitmap"    # Landroid/graphics/Bitmap;

    .prologue
    const/4 v13, 0x0

    const/4 v12, 0x1

    const/4 v11, 0x0

    .line 404
    const-string v9, "activity"

    invoke-virtual {p0, v9}, Lcom/google/android/onetimeinitializer/OneTimeService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 405
    .local v0, "am":Landroid/app/ActivityManager;
    invoke-virtual {v0}, Landroid/app/ActivityManager;->getLauncherLargeIconSize()I

    move-result v5

    .line 408
    .local v5, "iconSize":I
    sget-object v9, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v5, v5, v9}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 409
    .local v1, "bitmap":Landroid/graphics/Bitmap;
    new-instance v3, Landroid/graphics/Canvas;

    invoke-direct {v3, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 415
    .local v3, "canvas":Landroid/graphics/Canvas;
    const/high16 v9, 0x40000000    # 2.0f

    invoke-virtual {p0}, Lcom/google/android/onetimeinitializer/OneTimeService;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    invoke-virtual {v10}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v10

    invoke-static {v12, v9, v10}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v9

    float-to-int v2, v9

    .line 417
    .local v2, "borderWidth":I
    new-instance v8, Landroid/graphics/Rect;

    sub-int v9, v5, v2

    sub-int v10, v5, v2

    invoke-direct {v8, v2, v2, v9, v10}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 419
    .local v8, "src":Landroid/graphics/Rect;
    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4, v11, v11, v5, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 420
    .local v4, "dst":Landroid/graphics/Rect;
    invoke-virtual {v3, p1, v8, v4, v13}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 423
    invoke-virtual {p0}, Lcom/google/android/onetimeinitializer/OneTimeService;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-static {v9, v1}, Landroid/support/v4/graphics/drawable/RoundedBitmapDrawableFactory;->create(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)Landroid/support/v4/graphics/drawable/RoundedBitmapDrawable;

    move-result-object v7

    .line 425
    .local v7, "roundedDrawable":Landroid/support/v4/graphics/drawable/RoundedBitmapDrawable;
    invoke-virtual {v7, v12}, Landroid/support/v4/graphics/drawable/RoundedBitmapDrawable;->setAntiAlias(Z)V

    .line 426
    div-int/lit8 v9, v5, 0x2

    int-to-float v9, v9

    invoke-virtual {v7, v9}, Landroid/support/v4/graphics/drawable/RoundedBitmapDrawable;->setCornerRadius(F)V

    .line 427
    sget-object v9, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v5, v5, v9}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 428
    .local v6, "roundedBitmap":Landroid/graphics/Bitmap;
    invoke-virtual {v3, v6}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 429
    invoke-virtual {v7, v4}, Landroid/support/v4/graphics/drawable/RoundedBitmapDrawable;->setBounds(Landroid/graphics/Rect;)V

    .line 430
    invoke-virtual {v7, v3}, Landroid/support/v4/graphics/drawable/RoundedBitmapDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 431
    invoke-virtual {v3, v13}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 433
    return-object v6
.end method

.method private getMappingVersion()I
    .locals 3

    .prologue
    .line 179
    iget-object v0, p0, Lcom/google/android/onetimeinitializer/OneTimeService;->mPreferences:Landroid/content/SharedPreferences;

    const-string v1, "mapping_version"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method private hasRunForThisOsVersion()Z
    .locals 4

    .prologue
    .line 171
    sget-object v0, Landroid/os/Build;->ID:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/onetimeinitializer/OneTimeService;->mPreferences:Landroid/content/SharedPreferences;

    const-string v2, "one_time_completed_os_ver"

    const-string v3, ""

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private prepareTable()V
    .locals 21

    .prologue
    .line 189
    new-instance v19, Ljava/util/HashMap;

    invoke-direct/range {v19 .. v19}, Ljava/util/HashMap;-><init>()V

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/onetimeinitializer/OneTimeService;->mMappingTable:Ljava/util/HashMap;

    .line 192
    const-string v16, "com.google.android.finsky"

    .line 193
    .local v16, "oldPkg":Ljava/lang/String;
    const-string v7, "com.android.vending"

    .line 195
    .local v7, "newPkg":Ljava/lang/String;
    new-instance v14, Landroid/content/ComponentName;

    const-string v19, "com.google.android.finsky.activities.MainActivity"

    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-direct {v14, v0, v1}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 197
    .local v14, "oldMarket":Landroid/content/ComponentName;
    new-instance v15, Landroid/content/ComponentName;

    const-string v19, ".activities.MainActivity"

    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-direct {v15, v0, v1}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 199
    .local v15, "oldMarketShort":Landroid/content/ComponentName;
    new-instance v6, Landroid/content/ComponentName;

    const-string v19, "com.android.vending.AssetBrowserActivity"

    move-object/from16 v0, v19

    invoke-direct {v6, v7, v0}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 201
    .local v6, "newMarket":Landroid/content/ComponentName;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/onetimeinitializer/OneTimeService;->mMappingTable:Ljava/util/HashMap;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v14, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 202
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/onetimeinitializer/OneTimeService;->mMappingTable:Ljava/util/HashMap;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v15, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 205
    const-string v16, "com.google.android.camera"

    .line 206
    const-string v7, "com.google.android.gallery3d"

    .line 208
    new-instance v9, Landroid/content/ComponentName;

    const-string v19, "com.android.camera.Camera"

    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-direct {v9, v0, v1}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 210
    .local v9, "oldCamera":Landroid/content/ComponentName;
    new-instance v10, Landroid/content/ComponentName;

    const-string v19, ".Camera"

    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-direct {v10, v0, v1}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 212
    .local v10, "oldCameraShort":Landroid/content/ComponentName;
    new-instance v3, Landroid/content/ComponentName;

    const-string v19, "com.android.camera.Camera"

    move-object/from16 v0, v19

    invoke-direct {v3, v7, v0}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 214
    .local v3, "newCamera":Landroid/content/ComponentName;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/onetimeinitializer/OneTimeService;->mMappingTable:Ljava/util/HashMap;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v9, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 215
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/onetimeinitializer/OneTimeService;->mMappingTable:Ljava/util/HashMap;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v10, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 218
    new-instance v17, Landroid/content/ComponentName;

    const-string v19, "com.android.voicedialer"

    const-string v20, "com.android.voicedialer.VoiceDialerActivity"

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    move-object/from16 v2, v20

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 221
    .local v17, "oldVoiceDialer":Landroid/content/ComponentName;
    new-instance v8, Landroid/content/ComponentName;

    const-string v19, "com.google.android.googlequicksearchbox"

    const-string v20, "com.google.android.googlequicksearchbox.VoiceSearchActivity"

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-direct {v8, v0, v1}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 224
    .local v8, "newVoiceSearch":Landroid/content/ComponentName;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/onetimeinitializer/OneTimeService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v18

    .line 225
    .local v18, "pm":Landroid/content/pm/PackageManager;
    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/google/android/onetimeinitializer/OneTimeService;->doesComponentExist(Landroid/content/ComponentName;)Z

    move-result v19

    if-nez v19, :cond_0

    .line 226
    move-object/from16 v0, p0

    invoke-direct {v0, v8}, Lcom/google/android/onetimeinitializer/OneTimeService;->doesComponentExist(Landroid/content/ComponentName;)Z

    move-result v19

    if-eqz v19, :cond_1

    .line 228
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/onetimeinitializer/OneTimeService;->mMappingTable:Ljava/util/HashMap;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v17

    invoke-virtual {v0, v1, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 236
    :cond_0
    :goto_0
    const-string v7, "com.google.android.dialer"

    .line 237
    new-instance v5, Landroid/content/ComponentName;

    const-string v19, "com.android.dialer.DialtactsActivity"

    move-object/from16 v0, v19

    invoke-direct {v5, v7, v0}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 241
    .local v5, "newDialer":Landroid/content/ComponentName;
    const-string v16, "com.android.contacts"

    .line 242
    new-instance v13, Landroid/content/ComponentName;

    const-string v19, "com.android.contacts.activities.DialtactsActivity"

    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-direct {v13, v0, v1}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 244
    .local v13, "oldDialerInContacts":Landroid/content/ComponentName;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/onetimeinitializer/OneTimeService;->mMappingTable:Ljava/util/HashMap;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v13, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 247
    const-string v16, "com.android.dialer"

    .line 248
    new-instance v12, Landroid/content/ComponentName;

    const-string v19, "com.android.dialer.DialtactsActivity"

    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-direct {v12, v0, v1}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 251
    .local v12, "oldDialer":Landroid/content/ComponentName;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/onetimeinitializer/OneTimeService;->mMappingTable:Ljava/util/HashMap;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v12, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 254
    const-string v7, "com.google.android.contacts"

    .line 255
    const-string v16, "com.android.contacts"

    .line 256
    new-instance v4, Landroid/content/ComponentName;

    const-string v19, "com.android.contacts.activities.PeopleActivity"

    move-object/from16 v0, v19

    invoke-direct {v4, v7, v0}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 258
    .local v4, "newContacts":Landroid/content/ComponentName;
    new-instance v11, Landroid/content/ComponentName;

    const-string v19, "com.android.contacts.activities.PeopleActivity"

    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-direct {v11, v0, v1}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 260
    .local v11, "oldContacts":Landroid/content/ComponentName;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/onetimeinitializer/OneTimeService;->mMappingTable:Ljava/util/HashMap;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v11, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 261
    return-void

    .line 231
    .end local v4    # "newContacts":Landroid/content/ComponentName;
    .end local v5    # "newDialer":Landroid/content/ComponentName;
    .end local v11    # "oldContacts":Landroid/content/ComponentName;
    .end local v12    # "oldDialer":Landroid/content/ComponentName;
    .end local v13    # "oldDialerInContacts":Landroid/content/ComponentName;
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/onetimeinitializer/OneTimeService;->mMappingTable:Ljava/util/HashMap;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    move-object/from16 v0, v19

    move-object/from16 v1, v17

    move-object/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method private setHasRunForThisOsVersion()V
    .locals 3

    .prologue
    .line 175
    iget-object v0, p0, Lcom/google/android/onetimeinitializer/OneTimeService;->mPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "one_time_completed_os_ver"

    sget-object v2, Landroid/os/Build;->ID:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 176
    return-void
.end method

.method private updateMappingVersion()V
    .locals 3

    .prologue
    .line 183
    iget-object v1, p0, Lcom/google/android/onetimeinitializer/OneTimeService;->mPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 184
    .local v0, "ed":Landroid/content/SharedPreferences$Editor;
    const-string v1, "mapping_version"

    const/4 v2, 0x5

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 185
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 186
    return-void
.end method

.method private updateShortcut()V
    .locals 3

    .prologue
    .line 270
    invoke-direct {p0}, Lcom/google/android/onetimeinitializer/OneTimeService;->prepareTable()V

    .line 276
    const-string v0, "OneTimeService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "updateShortcut: uri="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/google/android/onetimeinitializer/OneTimeService;->LAUNCHER_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 277
    sget-object v0, Lcom/google/android/onetimeinitializer/OneTimeService;->LAUNCHER_CONTENT_URI:Landroid/net/Uri;

    invoke-direct {p0, v0}, Lcom/google/android/onetimeinitializer/OneTimeService;->updateShortcutLauncher(Landroid/net/Uri;)V

    .line 278
    const-string v0, "OneTimeService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "updateShortcut: uri="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/google/android/onetimeinitializer/OneTimeService;->LAUNCHER_NOW_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 279
    sget-object v0, Lcom/google/android/onetimeinitializer/OneTimeService;->LAUNCHER_NOW_CONTENT_URI:Landroid/net/Uri;

    invoke-direct {p0, v0}, Lcom/google/android/onetimeinitializer/OneTimeService;->updateShortcutLauncher(Landroid/net/Uri;)V

    .line 280
    return-void
.end method

.method private updateShortcutLauncher(Landroid/net/Uri;)V
    .locals 23
    .param p1, "launcherUri"    # Landroid/net/Uri;

    .prologue
    .line 283
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/onetimeinitializer/OneTimeService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 285
    .local v2, "cr":Landroid/content/ContentResolver;
    const/4 v3, 0x1

    new-array v0, v3, [Ljava/lang/String;

    move-object/from16 v19, v0

    .line 287
    .local v19, "selectionArg":[Ljava/lang/String;
    const/4 v10, 0x0

    .line 289
    .local v10, "c":Landroid/database/Cursor;
    const/4 v3, 0x3

    :try_start_0
    new-array v4, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v5, "_id"

    aput-object v5, v4, v3

    const/4 v3, 0x1

    const-string v5, "intent"

    aput-object v5, v4, v3

    const/4 v3, 0x2

    const-string v5, "icon"

    aput-object v5, v4, v3

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object/from16 v3, p1

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 292
    if-eqz v10, :cond_0

    invoke-interface {v10}, Landroid/database/Cursor;->getCount()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Landroid/content/OperationApplicationException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    if-nez v3, :cond_2

    .line 368
    :cond_0
    if-eqz v10, :cond_1

    .line 369
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 372
    :cond_1
    :goto_0
    return-void

    .line 296
    :cond_2
    :try_start_1
    new-instance v18, Ljava/util/ArrayList;

    invoke-direct/range {v18 .. v18}, Ljava/util/ArrayList;-><init>()V

    .line 298
    .local v18, "ops":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    :cond_3
    :goto_1
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Landroid/content/OperationApplicationException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v3

    if-eqz v3, :cond_8

    .line 300
    const/4 v3, 0x1

    :try_start_2
    invoke-interface {v10, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    .line 301
    .local v16, "intentUri":Ljava/lang/String;
    if-eqz v16, :cond_3

    .line 304
    const/4 v3, 0x0

    move-object/from16 v0, v16

    invoke-static {v0, v3}, Landroid/content/Intent;->parseUri(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v20

    .line 305
    .local v20, "shortcut":Landroid/content/Intent;
    invoke-virtual/range {v20 .. v20}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v12

    .line 306
    .local v12, "comp":Landroid/content/ComponentName;
    const/4 v3, 0x0

    invoke-interface {v10, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    .line 307
    .local v14, "id":J
    const/4 v3, 0x0

    invoke-static {v14, v15}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v19, v3

    .line 310
    invoke-direct/range {p0 .. p0}, Lcom/google/android/onetimeinitializer/OneTimeService;->getMappingVersion()I

    move-result v3

    const/4 v4, 0x4

    if-ge v3, v4, :cond_5

    const-string v3, "android.provider.action.QUICK_CONTACT"

    invoke-virtual/range {v20 .. v20}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    const-string v3, "com.android.contacts.action.QUICK_CONTACT"

    invoke-virtual/range {v20 .. v20}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 314
    :cond_4
    const/4 v3, 0x2

    invoke-interface {v10, v3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x2

    invoke-interface {v10, v5}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v5

    array-length v5, v5

    invoke-static {v3, v4, v5}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v21

    .line 316
    .local v21, "sourceBitmap":Landroid/graphics/Bitmap;
    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v1}, Lcom/google/android/onetimeinitializer/OneTimeService;->generateQuickContactIcon(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v11

    .line 317
    .local v11, "circularBitmap":Landroid/graphics/Bitmap;
    new-instance v9, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v9}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 318
    .local v9, "byteStream":Ljava/io/ByteArrayOutputStream;
    sget-object v3, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v4, 0x64

    invoke-virtual {v11, v3, v4, v9}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 319
    invoke-static/range {p1 .. p1}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    const-string v4, "icon"

    invoke-virtual {v9}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    const-string v4, "_id = ?"

    move-object/from16 v0, v19

    invoke-virtual {v3, v4, v0}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v8

    .line 322
    .local v8, "b":Landroid/content/ContentProviderOperation$Builder;
    invoke-virtual {v8}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v3

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Landroid/content/OperationApplicationException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_1

    .line 354
    .end local v8    # "b":Landroid/content/ContentProviderOperation$Builder;
    .end local v9    # "byteStream":Ljava/io/ByteArrayOutputStream;
    .end local v11    # "circularBitmap":Landroid/graphics/Bitmap;
    .end local v12    # "comp":Landroid/content/ComponentName;
    .end local v14    # "id":J
    .end local v16    # "intentUri":Ljava/lang/String;
    .end local v20    # "shortcut":Landroid/content/Intent;
    .end local v21    # "sourceBitmap":Landroid/graphics/Bitmap;
    :catch_0
    move-exception v22

    .line 356
    .local v22, "t":Ljava/lang/Throwable;
    :try_start_3
    const-string v3, "OneTimeService"

    const-string v4, "can\'t parse a shortcut entry"

    move-object/from16 v0, v22

    invoke-static {v3, v4, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Landroid/content/OperationApplicationException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_1

    .line 363
    .end local v18    # "ops":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .end local v22    # "t":Ljava/lang/Throwable;
    :catch_1
    move-exception v13

    .line 364
    .local v13, "e":Landroid/os/RemoteException;
    :try_start_4
    const-string v3, "OneTimeService"

    invoke-virtual {v13}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 368
    if-eqz v10, :cond_1

    .line 369
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 326
    .end local v13    # "e":Landroid/os/RemoteException;
    .restart local v12    # "comp":Landroid/content/ComponentName;
    .restart local v14    # "id":J
    .restart local v16    # "intentUri":Ljava/lang/String;
    .restart local v18    # "ops":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .restart local v20    # "shortcut":Landroid/content/Intent;
    :cond_5
    :try_start_5
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/onetimeinitializer/OneTimeService;->mMappingTable:Ljava/util/HashMap;

    invoke-virtual {v3, v12}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 330
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/onetimeinitializer/OneTimeService;->mMappingTable:Ljava/util/HashMap;

    invoke-virtual {v3, v12}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Landroid/content/ComponentName;

    .line 333
    .local v17, "newComp":Landroid/content/ComponentName;
    if-nez v17, :cond_6

    .line 334
    const-string v3, "OneTimeService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "removing shortcut id "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 335
    invoke-static/range {p1 .. p1}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    const-string v4, "_id = ?"

    move-object/from16 v0, v19

    invoke-virtual {v3, v4, v0}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v8

    .line 337
    .restart local v8    # "b":Landroid/content/ContentProviderOperation$Builder;
    invoke-virtual {v8}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v3

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Landroid/content/OperationApplicationException; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_1

    .line 365
    .end local v8    # "b":Landroid/content/ContentProviderOperation$Builder;
    .end local v12    # "comp":Landroid/content/ComponentName;
    .end local v14    # "id":J
    .end local v16    # "intentUri":Ljava/lang/String;
    .end local v17    # "newComp":Landroid/content/ComponentName;
    .end local v18    # "ops":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .end local v20    # "shortcut":Landroid/content/Intent;
    :catch_2
    move-exception v13

    .line 366
    .local v13, "e":Landroid/content/OperationApplicationException;
    :try_start_6
    const-string v3, "OneTimeService"

    invoke-virtual {v13}, Landroid/content/OperationApplicationException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 368
    if-eqz v10, :cond_1

    .line 369
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 339
    .end local v13    # "e":Landroid/content/OperationApplicationException;
    .restart local v12    # "comp":Landroid/content/ComponentName;
    .restart local v14    # "id":J
    .restart local v16    # "intentUri":Ljava/lang/String;
    .restart local v17    # "newComp":Landroid/content/ComponentName;
    .restart local v18    # "ops":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .restart local v20    # "shortcut":Landroid/content/Intent;
    :cond_6
    :try_start_7
    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/google/android/onetimeinitializer/OneTimeService;->doesComponentExist(Landroid/content/ComponentName;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 347
    const-string v3, "OneTimeService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "fix shortcut id "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " from "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v12}, Landroid/content/ComponentName;->toShortString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " to "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {v17 .. v17}, Landroid/content/ComponentName;->toShortString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 349
    move-object/from16 v0, v20

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 350
    invoke-static/range {p1 .. p1}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    const-string v4, "intent"

    const/4 v5, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v0, v5}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    const-string v4, "_id = ?"

    move-object/from16 v0, v19

    invoke-virtual {v3, v4, v0}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v8

    .line 353
    .restart local v8    # "b":Landroid/content/ContentProviderOperation$Builder;
    invoke-virtual {v8}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v3

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_7 .. :try_end_7} :catch_1
    .catch Landroid/content/OperationApplicationException; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto/16 :goto_1

    .line 368
    .end local v8    # "b":Landroid/content/ContentProviderOperation$Builder;
    .end local v12    # "comp":Landroid/content/ComponentName;
    .end local v14    # "id":J
    .end local v16    # "intentUri":Ljava/lang/String;
    .end local v17    # "newComp":Landroid/content/ComponentName;
    .end local v18    # "ops":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .end local v20    # "shortcut":Landroid/content/Intent;
    :catchall_0
    move-exception v3

    if-eqz v10, :cond_7

    .line 369
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    :cond_7
    throw v3

    .line 360
    .restart local v18    # "ops":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    :cond_8
    :try_start_8
    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_9

    .line 361
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v18

    invoke-virtual {v2, v3, v0}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    :try_end_8
    .catch Landroid/os/RemoteException; {:try_start_8 .. :try_end_8} :catch_1
    .catch Landroid/content/OperationApplicationException; {:try_start_8 .. :try_end_8} :catch_2
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 368
    :cond_9
    if-eqz v10, :cond_1

    .line 369
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0
.end method


# virtual methods
.method public onCreate()V
    .locals 2

    .prologue
    .line 141
    invoke-super {p0}, Landroid/app/IntentService;->onCreate()V

    .line 142
    const-string v0, "oti"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/onetimeinitializer/OneTimeService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/onetimeinitializer/OneTimeService;->mPreferences:Landroid/content/SharedPreferences;

    .line 143
    return-void
.end method

.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 147
    const-string v0, "OneTimeService"

    const-string v1, "OneTimeService.onHandleIntent"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 149
    invoke-direct {p0}, Lcom/google/android/onetimeinitializer/OneTimeService;->getMappingVersion()I

    move-result v0

    const/4 v1, 0x5

    if-ge v0, v1, :cond_0

    .line 150
    const-string v0, "OneTimeService"

    const-string v1, "Updating shortcuts"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 151
    invoke-direct {p0}, Lcom/google/android/onetimeinitializer/OneTimeService;->updateShortcut()V

    .line 152
    invoke-direct {p0}, Lcom/google/android/onetimeinitializer/OneTimeService;->updateMappingVersion()V

    .line 155
    :cond_0
    invoke-direct {p0}, Lcom/google/android/onetimeinitializer/OneTimeService;->hasRunForThisOsVersion()Z

    move-result v0

    if-nez v0, :cond_1

    .line 156
    invoke-direct {p0}, Lcom/google/android/onetimeinitializer/OneTimeService;->broadcastAndIncrement()V

    .line 157
    invoke-direct {p0}, Lcom/google/android/onetimeinitializer/OneTimeService;->disableDevicePolicy()V

    .line 158
    invoke-direct {p0}, Lcom/google/android/onetimeinitializer/OneTimeService;->setHasRunForThisOsVersion()V

    .line 160
    :cond_1
    return-void
.end method
