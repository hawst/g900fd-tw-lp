.class public Lcom/validity/fingerprint/IdentifyResult;
.super Ljava/lang/Object;
.source "IdentifyResult.java"


# instance fields
.field public appData:Ljava/lang/String;

.field public corrMinuCount:[I

.field public fMinuCount:I

.field public fingerIndex:I

.field public matchRate:F

.field public matchScore:I

.field public matchedFingerIndexes:[I

.field public templateId:[B

.field public templateUpdated:I

.field public userId:Ljava/lang/String;

.field public vMinuCount:[I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x4

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    const/16 v0, 0x10

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/validity/fingerprint/IdentifyResult;->templateId:[B

    .line 35
    const/16 v0, 0x14

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/validity/fingerprint/IdentifyResult;->matchedFingerIndexes:[I

    .line 56
    new-array v0, v1, [I

    iput-object v0, p0, Lcom/validity/fingerprint/IdentifyResult;->corrMinuCount:[I

    .line 61
    new-array v0, v1, [I

    iput-object v0, p0, Lcom/validity/fingerprint/IdentifyResult;->vMinuCount:[I

    return-void
.end method
