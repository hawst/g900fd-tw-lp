.class public Lcom/validity/fingerprint/SensorView;
.super Landroid/widget/RelativeLayout;
.source "SensorView.java"


# static fields
.field public static final FP_SWIPE_BOTTOM_TO_TOP:I = 0x3

.field public static final FP_SWIPE_LEFT_TO_RIGHT:I = 0x0

.field public static final FP_SWIPE_RIGHT_TO_LEFT:I = 0x1

.field public static final FP_SWIPE_TOP_TO_BOTTOM:I = 0x2

.field private static final TAG:Ljava/lang/String; = "SensorView"


# instance fields
.field private mBlinkAnimation:Landroid/view/animation/Animation;

.field private mContext:Landroid/content/Context;

.field private mFingerOutlineBmp:Landroid/graphics/Bitmap;

.field private mFingerOutlineImage:Landroid/widget/ImageView;

.field private mFingerSwipeBmp:Landroid/graphics/Bitmap;

.field private mFingerSwipeImage:Landroid/widget/ImageView;

.field private mFingerSwipeOutlineVisible:Z

.field private mFingerSwipeVisible:Z

.field private mOutlineAnimation:Landroid/view/animation/TranslateAnimation;

.field private mScreenHeight:I

.field private mScreenWidth:I

.field private mSensorLED:Landroid/view/View;

.field private mSensorVisible:Z

.field private mSwipeAnimation:Landroid/view/animation/TranslateAnimation;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 12
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 60
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 51
    const/4 v8, 0x0

    iput v8, p0, Lcom/validity/fingerprint/SensorView;->mScreenWidth:I

    .line 52
    const/4 v8, 0x0

    iput v8, p0, Lcom/validity/fingerprint/SensorView;->mScreenHeight:I

    .line 53
    const/4 v8, 0x0

    iput-boolean v8, p0, Lcom/validity/fingerprint/SensorView;->mSensorVisible:Z

    .line 54
    const/4 v8, 0x0

    iput-boolean v8, p0, Lcom/validity/fingerprint/SensorView;->mFingerSwipeVisible:Z

    .line 55
    const/4 v8, 0x0

    iput-boolean v8, p0, Lcom/validity/fingerprint/SensorView;->mFingerSwipeOutlineVisible:Z

    .line 61
    iput-object p1, p0, Lcom/validity/fingerprint/SensorView;->mContext:Landroid/content/Context;

    .line 64
    new-instance v8, Landroid/view/animation/AlphaAnimation;

    const v9, 0x3e99999a    # 0.3f

    const/high16 v10, 0x3f800000    # 1.0f

    invoke-direct {v8, v9, v10}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    iput-object v8, p0, Lcom/validity/fingerprint/SensorView;->mBlinkAnimation:Landroid/view/animation/Animation;

    .line 65
    iget-object v8, p0, Lcom/validity/fingerprint/SensorView;->mBlinkAnimation:Landroid/view/animation/Animation;

    const-wide/16 v10, 0x258

    invoke-virtual {v8, v10, v11}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 66
    iget-object v8, p0, Lcom/validity/fingerprint/SensorView;->mBlinkAnimation:Landroid/view/animation/Animation;

    new-instance v9, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v9}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v8, v9}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 67
    iget-object v8, p0, Lcom/validity/fingerprint/SensorView;->mBlinkAnimation:Landroid/view/animation/Animation;

    const/4 v9, -0x1

    invoke-virtual {v8, v9}, Landroid/view/animation/Animation;->setRepeatCount(I)V

    .line 68
    iget-object v8, p0, Lcom/validity/fingerprint/SensorView;->mBlinkAnimation:Landroid/view/animation/Animation;

    const/4 v9, 0x2

    invoke-virtual {v8, v9}, Landroid/view/animation/Animation;->setRepeatMode(I)V

    .line 70
    const/16 v6, 0xc8

    .line 73
    .local v6, "vId":I
    move-object v4, p0

    .line 74
    .local v4, "root":Landroid/widget/RelativeLayout;
    invoke-virtual {v4, v6}, Landroid/widget/RelativeLayout;->setId(I)V

    .line 77
    const-class v8, Lcom/validity/fingerprint/SensorView;

    invoke-virtual {v8}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v8

    const-string v9, "res/drawable/finger_tip_outline.png"

    invoke-virtual {v8, v9}, Ljava/lang/ClassLoader;->getResourceAsStream(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v2

    .line 79
    .local v2, "is":Ljava/io/InputStream;
    if-eqz v2, :cond_0

    .line 80
    invoke-static {v2}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v8

    iput-object v8, p0, Lcom/validity/fingerprint/SensorView;->mFingerOutlineBmp:Landroid/graphics/Bitmap;

    .line 81
    new-instance v8, Landroid/widget/ImageView;

    invoke-direct {v8, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v8, p0, Lcom/validity/fingerprint/SensorView;->mFingerOutlineImage:Landroid/widget/ImageView;

    .line 82
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v8, -0x2

    const/4 v9, -0x2

    invoke-direct {v1, v8, v9}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 85
    .local v1, "ilp":Landroid/widget/RelativeLayout$LayoutParams;
    const/4 v8, 0x3

    invoke-virtual {v4}, Landroid/widget/RelativeLayout;->getId()I

    move-result v9

    invoke-virtual {v1, v8, v9}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 86
    iget-object v8, p0, Lcom/validity/fingerprint/SensorView;->mFingerOutlineImage:Landroid/widget/ImageView;

    invoke-virtual {v8, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 87
    iget-object v8, p0, Lcom/validity/fingerprint/SensorView;->mFingerOutlineImage:Landroid/widget/ImageView;

    add-int/lit8 v7, v6, 0x1

    .end local v6    # "vId":I
    .local v7, "vId":I
    invoke-virtual {v8, v6}, Landroid/widget/ImageView;->setId(I)V

    .line 88
    iget-object v8, p0, Lcom/validity/fingerprint/SensorView;->mFingerOutlineImage:Landroid/widget/ImageView;

    iget-object v9, p0, Lcom/validity/fingerprint/SensorView;->mFingerOutlineBmp:Landroid/graphics/Bitmap;

    invoke-virtual {v8, v9}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 89
    iget-object v8, p0, Lcom/validity/fingerprint/SensorView;->mFingerOutlineImage:Landroid/widget/ImageView;

    invoke-virtual {v4, v8}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    move v6, v7

    .line 93
    .end local v1    # "ilp":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v7    # "vId":I
    .restart local v6    # "vId":I
    :cond_0
    new-instance v8, Landroid/view/View;

    invoke-direct {v8, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v8, p0, Lcom/validity/fingerprint/SensorView;->mSensorLED:Landroid/view/View;

    .line 94
    new-instance v5, Landroid/widget/RelativeLayout$LayoutParams;

    const/16 v8, 0x19

    const/16 v9, 0x64

    invoke-direct {v5, v8, v9}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 95
    .local v5, "slp":Landroid/widget/RelativeLayout$LayoutParams;
    const/4 v8, 0x3

    invoke-virtual {v4}, Landroid/widget/RelativeLayout;->getId()I

    move-result v9

    invoke-virtual {v5, v8, v9}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 96
    iget-object v8, p0, Lcom/validity/fingerprint/SensorView;->mSensorLED:Landroid/view/View;

    invoke-virtual {v8, v5}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 97
    iget-object v8, p0, Lcom/validity/fingerprint/SensorView;->mSensorLED:Landroid/view/View;

    add-int/lit8 v7, v6, 0x1

    .end local v6    # "vId":I
    .restart local v7    # "vId":I
    invoke-virtual {v8, v6}, Landroid/view/View;->setId(I)V

    .line 98
    iget-object v8, p0, Lcom/validity/fingerprint/SensorView;->mSensorLED:Landroid/view/View;

    const v9, -0xff0100

    invoke-virtual {v8, v9}, Landroid/view/View;->setBackgroundColor(I)V

    .line 99
    iget-object v8, p0, Lcom/validity/fingerprint/SensorView;->mSensorLED:Landroid/view/View;

    invoke-virtual {v4, v8}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 102
    const-class v8, Lcom/validity/fingerprint/SensorView;

    invoke-virtual {v8}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v8

    const-string v9, "res/drawable/finger_tip.png"

    invoke-virtual {v8, v9}, Ljava/lang/ClassLoader;->getResourceAsStream(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v3

    .line 104
    .local v3, "isFingerSwipe":Ljava/io/InputStream;
    if-eqz v3, :cond_1

    .line 105
    invoke-static {v3}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v8

    iput-object v8, p0, Lcom/validity/fingerprint/SensorView;->mFingerSwipeBmp:Landroid/graphics/Bitmap;

    .line 106
    new-instance v8, Landroid/widget/ImageView;

    invoke-direct {v8, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v8, p0, Lcom/validity/fingerprint/SensorView;->mFingerSwipeImage:Landroid/widget/ImageView;

    .line 107
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v8, -0x2

    const/4 v9, -0x2

    invoke-direct {v0, v8, v9}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 110
    .local v0, "fslp":Landroid/widget/RelativeLayout$LayoutParams;
    const/4 v8, 0x3

    invoke-virtual {v4}, Landroid/widget/RelativeLayout;->getId()I

    move-result v9

    invoke-virtual {v0, v8, v9}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 111
    iget-object v8, p0, Lcom/validity/fingerprint/SensorView;->mFingerSwipeImage:Landroid/widget/ImageView;

    invoke-virtual {v8, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 112
    iget-object v8, p0, Lcom/validity/fingerprint/SensorView;->mFingerSwipeImage:Landroid/widget/ImageView;

    add-int/lit8 v6, v7, 0x1

    .end local v7    # "vId":I
    .restart local v6    # "vId":I
    invoke-virtual {v8, v7}, Landroid/widget/ImageView;->setId(I)V

    .line 113
    iget-object v8, p0, Lcom/validity/fingerprint/SensorView;->mFingerSwipeImage:Landroid/widget/ImageView;

    iget-object v9, p0, Lcom/validity/fingerprint/SensorView;->mFingerSwipeBmp:Landroid/graphics/Bitmap;

    invoke-virtual {v8, v9}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 114
    iget-object v8, p0, Lcom/validity/fingerprint/SensorView;->mFingerSwipeImage:Landroid/widget/ImageView;

    invoke-virtual {v4, v8}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 117
    .end local v0    # "fslp":Landroid/widget/RelativeLayout$LayoutParams;
    :goto_0
    return-void

    .end local v6    # "vId":I
    .restart local v7    # "vId":I
    :cond_1
    move v6, v7

    .end local v7    # "vId":I
    .restart local v6    # "vId":I
    goto :goto_0
.end method


# virtual methods
.method public getScreenSize()V
    .locals 5

    .prologue
    .line 351
    iget-object v2, p0, Lcom/validity/fingerprint/SensorView;->mContext:Landroid/content/Context;

    const-string v3, "window"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/WindowManager;

    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 353
    .local v0, "display":Landroid/view/Display;
    invoke-virtual {v0}, Landroid/view/Display;->getWidth()I

    move-result v2

    iput v2, p0, Lcom/validity/fingerprint/SensorView;->mScreenWidth:I

    .line 354
    invoke-virtual {v0}, Landroid/view/Display;->getHeight()I

    move-result v2

    iput v2, p0, Lcom/validity/fingerprint/SensorView;->mScreenHeight:I

    .line 356
    new-instance v1, Landroid/util/DisplayMetrics;

    invoke-direct {v1}, Landroid/util/DisplayMetrics;-><init>()V

    .line 357
    .local v1, "metrics":Landroid/util/DisplayMetrics;
    invoke-virtual {v0, v1}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 358
    iget v2, v1, Landroid/util/DisplayMetrics;->densityDpi:I

    sparse-switch v2, :sswitch_data_0

    .line 369
    const-string v2, "SensorView"

    const-string v3, "Unknown density"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 372
    :goto_0
    const-string v2, "SensorView"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ScreenSize: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/validity/fingerprint/SensorView;->mScreenWidth:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/validity/fingerprint/SensorView;->mScreenHeight:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 373
    return-void

    .line 360
    :sswitch_0
    invoke-virtual {v0}, Landroid/view/Display;->getHeight()I

    move-result v2

    add-int/lit8 v2, v2, -0x26

    iput v2, p0, Lcom/validity/fingerprint/SensorView;->mScreenHeight:I

    goto :goto_0

    .line 363
    :sswitch_1
    invoke-virtual {v0}, Landroid/view/Display;->getHeight()I

    move-result v2

    add-int/lit8 v2, v2, -0x19

    iput v2, p0, Lcom/validity/fingerprint/SensorView;->mScreenHeight:I

    goto :goto_0

    .line 366
    :sswitch_2
    invoke-virtual {v0}, Landroid/view/Display;->getHeight()I

    move-result v2

    add-int/lit8 v2, v2, -0x13

    iput v2, p0, Lcom/validity/fingerprint/SensorView;->mScreenHeight:I

    goto :goto_0

    .line 358
    :sswitch_data_0
    .sparse-switch
        0x78 -> :sswitch_2
        0xa0 -> :sswitch_1
        0xf0 -> :sswitch_0
    .end sparse-switch
.end method

.method protected onFinishInflate()V
    .locals 0

    .prologue
    .line 337
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 338
    invoke-virtual {p0}, Lcom/validity/fingerprint/SensorView;->getScreenSize()V

    .line 339
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 0
    .param p1, "hasWindowFocus"    # Z

    .prologue
    .line 343
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->onWindowFocusChanged(Z)V

    .line 345
    return-void
.end method

.method public setLocation(Lcom/validity/fingerprint/ConfigReader$ConfigData;)Z
    .locals 36
    .param p1, "configData"    # Lcom/validity/fingerprint/ConfigReader$ConfigData;

    .prologue
    .line 124
    if-nez p1, :cond_0

    .line 125
    const-string v3, "SensorView"

    const-string v4, "Invalid configurations"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 126
    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/validity/fingerprint/SensorView;->showAnimations(Z)V

    .line 127
    const/4 v3, 0x0

    .line 283
    :goto_0
    return v3

    .line 130
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/validity/fingerprint/SensorView;->getScreenSize()V

    .line 131
    invoke-virtual/range {p1 .. p1}, Lcom/validity/fingerprint/ConfigReader$ConfigData;->toStringDebug()V

    .line 134
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/validity/fingerprint/ConfigReader$ConfigData;->sensorBar:Lcom/validity/fingerprint/ConfigReader$ConfigData$SensorBar;

    iget-boolean v3, v3, Lcom/validity/fingerprint/ConfigReader$ConfigData$SensorBar;->visible:Z

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/validity/fingerprint/SensorView;->mSensorVisible:Z

    .line 135
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/validity/fingerprint/ConfigReader$ConfigData;->fingerSwipeAnimation:Lcom/validity/fingerprint/ConfigReader$ConfigData$FingerSwipeAnimation;

    iget-boolean v3, v3, Lcom/validity/fingerprint/ConfigReader$ConfigData$FingerSwipeAnimation;->visible:Z

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/validity/fingerprint/SensorView;->mFingerSwipeVisible:Z

    .line 136
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/validity/fingerprint/ConfigReader$ConfigData;->fingerSwipeAnimation:Lcom/validity/fingerprint/ConfigReader$ConfigData$FingerSwipeAnimation;

    iget-boolean v3, v3, Lcom/validity/fingerprint/ConfigReader$ConfigData$FingerSwipeAnimation;->outlineVisible:Z

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/validity/fingerprint/SensorView;->mFingerSwipeOutlineVisible:Z

    .line 141
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/validity/fingerprint/SensorView;->mSensorVisible:Z

    if-eqz v3, :cond_a

    .line 143
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/validity/fingerprint/SensorView;->mSensorLED:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v25

    check-cast v25, Landroid/widget/RelativeLayout$LayoutParams;

    .line 144
    .local v25, "params":Landroid/widget/RelativeLayout$LayoutParams;
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/validity/fingerprint/ConfigReader$ConfigData;->sensorBar:Lcom/validity/fingerprint/ConfigReader$ConfigData$SensorBar;

    iget v3, v3, Lcom/validity/fingerprint/ConfigReader$ConfigData$SensorBar;->height:I

    move-object/from16 v0, v25

    iput v3, v0, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 145
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/validity/fingerprint/ConfigReader$ConfigData;->sensorBar:Lcom/validity/fingerprint/ConfigReader$ConfigData$SensorBar;

    iget v3, v3, Lcom/validity/fingerprint/ConfigReader$ConfigData$SensorBar;->width:I

    move-object/from16 v0, v25

    iput v3, v0, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 146
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/validity/fingerprint/ConfigReader$ConfigData;->sensorBar:Lcom/validity/fingerprint/ConfigReader$ConfigData$SensorBar;

    iget v3, v3, Lcom/validity/fingerprint/ConfigReader$ConfigData$SensorBar;->xPos:I

    move-object/from16 v0, v25

    iput v3, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 147
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/validity/fingerprint/ConfigReader$ConfigData;->sensorBar:Lcom/validity/fingerprint/ConfigReader$ConfigData$SensorBar;

    iget v3, v3, Lcom/validity/fingerprint/ConfigReader$ConfigData$SensorBar;->yPos:I

    move-object/from16 v0, v25

    iput v3, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 150
    move-object/from16 v0, v25

    iget v3, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    if-gez v3, :cond_1

    const/4 v3, 0x0

    move-object/from16 v0, v25

    iput v3, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 151
    :cond_1
    move-object/from16 v0, v25

    iget v3, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    move-object/from16 v0, v25

    iget v4, v0, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    add-int/2addr v3, v4

    move-object/from16 v0, p0

    iget v4, v0, Lcom/validity/fingerprint/SensorView;->mScreenWidth:I

    if-lt v3, v4, :cond_2

    .line 152
    move-object/from16 v0, p0

    iget v3, v0, Lcom/validity/fingerprint/SensorView;->mScreenWidth:I

    move-object/from16 v0, v25

    iget v4, v0, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    sub-int/2addr v3, v4

    move-object/from16 v0, v25

    iput v3, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 156
    :cond_2
    move-object/from16 v0, v25

    iget v3, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    if-gez v3, :cond_3

    const/4 v3, 0x0

    move-object/from16 v0, v25

    iput v3, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 157
    :cond_3
    move-object/from16 v0, v25

    iget v3, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    move-object/from16 v0, v25

    iget v4, v0, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    add-int/2addr v3, v4

    move-object/from16 v0, p0

    iget v4, v0, Lcom/validity/fingerprint/SensorView;->mScreenHeight:I

    if-lt v3, v4, :cond_4

    .line 158
    move-object/from16 v0, p0

    iget v3, v0, Lcom/validity/fingerprint/SensorView;->mScreenHeight:I

    move-object/from16 v0, v25

    iget v4, v0, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    sub-int/2addr v3, v4

    move-object/from16 v0, v25

    iput v3, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 162
    :cond_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/validity/fingerprint/SensorView;->mSensorLED:Landroid/view/View;

    move-object/from16 v0, v25

    invoke-virtual {v3, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 163
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/validity/fingerprint/SensorView;->mSensorLED:Landroid/view/View;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/validity/fingerprint/SensorView;->mBlinkAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v3, v4}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 164
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/validity/fingerprint/SensorView;->mSensorLED:Landroid/view/View;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 173
    .end local v25    # "params":Landroid/widget/RelativeLayout$LayoutParams;
    :goto_1
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/validity/fingerprint/SensorView;->mFingerSwipeVisible:Z

    if-eqz v3, :cond_10

    .line 177
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/validity/fingerprint/ConfigReader$ConfigData;->fingerSwipeAnimation:Lcom/validity/fingerprint/ConfigReader$ConfigData$FingerSwipeAnimation;

    iget v0, v3, Lcom/validity/fingerprint/ConfigReader$ConfigData$FingerSwipeAnimation;->xPos:I

    move/from16 v32, v0

    .line 178
    .local v32, "xPos":I
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/validity/fingerprint/ConfigReader$ConfigData;->fingerSwipeAnimation:Lcom/validity/fingerprint/ConfigReader$ConfigData$FingerSwipeAnimation;

    iget v0, v3, Lcom/validity/fingerprint/ConfigReader$ConfigData$FingerSwipeAnimation;->yPos:I

    move/from16 v34, v0

    .line 179
    .local v34, "yPos":I
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/validity/fingerprint/ConfigReader$ConfigData;->fingerSwipeAnimation:Lcom/validity/fingerprint/ConfigReader$ConfigData$FingerSwipeAnimation;

    iget v0, v3, Lcom/validity/fingerprint/ConfigReader$ConfigData$FingerSwipeAnimation;->xScale:F

    move/from16 v33, v0

    .line 180
    .local v33, "xScale":F
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/validity/fingerprint/ConfigReader$ConfigData;->fingerSwipeAnimation:Lcom/validity/fingerprint/ConfigReader$ConfigData$FingerSwipeAnimation;

    iget v0, v3, Lcom/validity/fingerprint/ConfigReader$ConfigData$FingerSwipeAnimation;->yScale:F

    move/from16 v35, v0

    .line 181
    .local v35, "yScale":F
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/validity/fingerprint/ConfigReader$ConfigData;->fingerSwipeAnimation:Lcom/validity/fingerprint/ConfigReader$ConfigData$FingerSwipeAnimation;

    iget v0, v3, Lcom/validity/fingerprint/ConfigReader$ConfigData$FingerSwipeAnimation;->orientation:I

    move/from16 v29, v0

    .line 184
    .local v29, "swipeOrientation":I
    const/16 v26, -0x1

    .line 185
    .local v26, "rotate":I
    const/4 v3, 0x1

    move/from16 v0, v29

    if-ne v0, v3, :cond_b

    const/16 v26, 0x5a

    .line 190
    :cond_5
    :goto_2
    new-instance v7, Landroid/graphics/Matrix;

    invoke-direct {v7}, Landroid/graphics/Matrix;-><init>()V

    .line 193
    .local v7, "matrix":Landroid/graphics/Matrix;
    move/from16 v0, v33

    move/from16 v1, v35

    invoke-virtual {v7, v0, v1}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 196
    const/4 v3, -0x1

    move/from16 v0, v26

    if-eq v0, v3, :cond_6

    .line 197
    move/from16 v0, v26

    int-to-float v3, v0

    invoke-virtual {v7, v3}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 203
    :cond_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/validity/fingerprint/SensorView;->mFingerSwipeBmp:Landroid/graphics/Bitmap;

    .line 204
    .local v2, "bitmapOrg":Landroid/graphics/Bitmap;
    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    const/4 v8, 0x1

    invoke-static/range {v2 .. v8}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v18

    .line 209
    .local v18, "bitmapResized":Landroid/graphics/Bitmap;
    new-instance v17, Landroid/graphics/drawable/BitmapDrawable;

    invoke-direct/range {v17 .. v18}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/graphics/Bitmap;)V

    .line 210
    .local v17, "bitmapDrawable":Landroid/graphics/drawable/BitmapDrawable;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/validity/fingerprint/SensorView;->mFingerSwipeImage:Landroid/widget/ImageView;

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 211
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/validity/fingerprint/SensorView;->mFingerSwipeImage:Landroid/widget/ImageView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 213
    invoke-virtual/range {v17 .. v17}, Landroid/graphics/drawable/BitmapDrawable;->getIntrinsicWidth()I

    move-result v28

    .line 214
    .local v28, "swipeImageWidth":I
    invoke-virtual/range {v17 .. v17}, Landroid/graphics/drawable/BitmapDrawable;->getIntrinsicHeight()I

    move-result v27

    .line 217
    .local v27, "swipeImageHeight":I
    move/from16 v22, v32

    .line 218
    .local v22, "fromXDelta":I
    move/from16 v23, v34

    .line 219
    .local v23, "fromYDelta":I
    move/from16 v30, v22

    .line 220
    .local v30, "toXDelta":I
    move/from16 v31, v23

    .line 223
    .local v31, "toYDelta":I
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/validity/fingerprint/ConfigReader$ConfigData;->fingerSwipeAnimation:Lcom/validity/fingerprint/ConfigReader$ConfigData$FingerSwipeAnimation;

    iget v0, v3, Lcom/validity/fingerprint/ConfigReader$ConfigData$FingerSwipeAnimation;->offsetLength:I

    move/from16 v21, v0

    .line 224
    .local v21, "fingerOffset":I
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/validity/fingerprint/ConfigReader$ConfigData;->fingerSwipeAnimation:Lcom/validity/fingerprint/ConfigReader$ConfigData$FingerSwipeAnimation;

    iget v15, v3, Lcom/validity/fingerprint/ConfigReader$ConfigData$FingerSwipeAnimation;->animationSpeed:I

    .line 225
    .local v15, "animationSpeed":I
    const/4 v3, 0x2

    move/from16 v0, v29

    if-ne v0, v3, :cond_d

    .line 226
    add-int v23, v23, v21

    .line 227
    add-int v3, v31, v27

    add-int v31, v3, v21

    .line 239
    :cond_7
    :goto_3
    new-instance v3, Landroid/view/animation/TranslateAnimation;

    move/from16 v0, v22

    int-to-float v4, v0

    move/from16 v0, v30

    int-to-float v5, v0

    move/from16 v0, v23

    int-to-float v6, v0

    move/from16 v0, v31

    int-to-float v9, v0

    invoke-direct {v3, v4, v5, v6, v9}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/validity/fingerprint/SensorView;->mSwipeAnimation:Landroid/view/animation/TranslateAnimation;

    .line 240
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/validity/fingerprint/SensorView;->mSwipeAnimation:Landroid/view/animation/TranslateAnimation;

    int-to-long v4, v15

    invoke-virtual {v3, v4, v5}, Landroid/view/animation/TranslateAnimation;->setDuration(J)V

    .line 241
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/validity/fingerprint/SensorView;->mSwipeAnimation:Landroid/view/animation/TranslateAnimation;

    const/4 v4, -0x1

    invoke-virtual {v3, v4}, Landroid/view/animation/TranslateAnimation;->setRepeatCount(I)V

    .line 242
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/validity/fingerprint/SensorView;->mFingerSwipeImage:Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/validity/fingerprint/SensorView;->mSwipeAnimation:Landroid/view/animation/TranslateAnimation;

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 247
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/validity/fingerprint/SensorView;->mFingerSwipeOutlineVisible:Z

    if-eqz v3, :cond_8

    .line 249
    const/16 v24, 0x14

    .line 250
    .local v24, "offSet":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/validity/fingerprint/SensorView;->mFingerOutlineImage:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v16

    check-cast v16, Landroid/widget/RelativeLayout$LayoutParams;

    .line 251
    .local v16, "bgparams":Landroid/widget/RelativeLayout$LayoutParams;
    add-int/lit8 v3, v27, 0x28

    move-object/from16 v0, v16

    iput v3, v0, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 252
    add-int/lit8 v3, v28, 0x28

    move-object/from16 v0, v16

    iput v3, v0, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 253
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/validity/fingerprint/SensorView;->mFingerOutlineImage:Landroid/widget/ImageView;

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 257
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/validity/fingerprint/SensorView;->mFingerOutlineBmp:Landroid/graphics/Bitmap;

    .line 258
    .local v8, "bmpOrg":Landroid/graphics/Bitmap;
    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v11

    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v12

    const/4 v14, 0x1

    move-object v13, v7

    invoke-static/range {v8 .. v14}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v20

    .line 261
    .local v20, "bmpResized":Landroid/graphics/Bitmap;
    new-instance v19, Landroid/graphics/drawable/BitmapDrawable;

    invoke-direct/range {v19 .. v20}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/graphics/Bitmap;)V

    .line 262
    .local v19, "bmpDrawable":Landroid/graphics/drawable/BitmapDrawable;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/validity/fingerprint/SensorView;->mFingerOutlineImage:Landroid/widget/ImageView;

    move-object/from16 v0, v19

    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 263
    new-instance v3, Landroid/view/animation/TranslateAnimation;

    move/from16 v0, v22

    int-to-float v4, v0

    sub-int v5, v22, v24

    int-to-float v5, v5

    move/from16 v0, v23

    int-to-float v6, v0

    sub-int v9, v23, v24

    int-to-float v9, v9

    invoke-direct {v3, v4, v5, v6, v9}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/validity/fingerprint/SensorView;->mOutlineAnimation:Landroid/view/animation/TranslateAnimation;

    .line 265
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/validity/fingerprint/SensorView;->mOutlineAnimation:Landroid/view/animation/TranslateAnimation;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/view/animation/TranslateAnimation;->setFillAfter(Z)V

    .line 266
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/validity/fingerprint/SensorView;->mFingerOutlineImage:Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/validity/fingerprint/SensorView;->mOutlineAnimation:Landroid/view/animation/TranslateAnimation;

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 267
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/validity/fingerprint/SensorView;->mFingerOutlineImage:Landroid/widget/ImageView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 278
    .end local v2    # "bitmapOrg":Landroid/graphics/Bitmap;
    .end local v7    # "matrix":Landroid/graphics/Matrix;
    .end local v8    # "bmpOrg":Landroid/graphics/Bitmap;
    .end local v15    # "animationSpeed":I
    .end local v16    # "bgparams":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v17    # "bitmapDrawable":Landroid/graphics/drawable/BitmapDrawable;
    .end local v18    # "bitmapResized":Landroid/graphics/Bitmap;
    .end local v19    # "bmpDrawable":Landroid/graphics/drawable/BitmapDrawable;
    .end local v20    # "bmpResized":Landroid/graphics/Bitmap;
    .end local v21    # "fingerOffset":I
    .end local v22    # "fromXDelta":I
    .end local v23    # "fromYDelta":I
    .end local v24    # "offSet":I
    .end local v26    # "rotate":I
    .end local v27    # "swipeImageHeight":I
    .end local v28    # "swipeImageWidth":I
    .end local v29    # "swipeOrientation":I
    .end local v30    # "toXDelta":I
    .end local v31    # "toYDelta":I
    .end local v32    # "xPos":I
    .end local v33    # "xScale":F
    .end local v34    # "yPos":I
    .end local v35    # "yScale":F
    :cond_8
    :goto_4
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/validity/fingerprint/SensorView;->mFingerSwipeOutlineVisible:Z

    if-nez v3, :cond_9

    .line 279
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/validity/fingerprint/SensorView;->mFingerOutlineImage:Landroid/widget/ImageView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setAnimation(Landroid/view/animation/Animation;)V

    .line 280
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/validity/fingerprint/SensorView;->mFingerOutlineImage:Landroid/widget/ImageView;

    const/4 v4, 0x4

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 283
    :cond_9
    const/4 v3, 0x1

    goto/16 :goto_0

    .line 166
    :cond_a
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/validity/fingerprint/SensorView;->mSensorLED:Landroid/view/View;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/view/View;->setAnimation(Landroid/view/animation/Animation;)V

    .line 167
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/validity/fingerprint/SensorView;->mSensorLED:Landroid/view/View;

    const/4 v4, 0x4

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_1

    .line 186
    .restart local v26    # "rotate":I
    .restart local v29    # "swipeOrientation":I
    .restart local v32    # "xPos":I
    .restart local v33    # "xScale":F
    .restart local v34    # "yPos":I
    .restart local v35    # "yScale":F
    :cond_b
    const/4 v3, 0x3

    move/from16 v0, v29

    if-ne v0, v3, :cond_c

    const/16 v26, 0xb4

    goto/16 :goto_2

    .line 187
    :cond_c
    if-nez v29, :cond_5

    const/16 v26, 0x10e

    goto/16 :goto_2

    .line 228
    .restart local v2    # "bitmapOrg":Landroid/graphics/Bitmap;
    .restart local v7    # "matrix":Landroid/graphics/Matrix;
    .restart local v15    # "animationSpeed":I
    .restart local v17    # "bitmapDrawable":Landroid/graphics/drawable/BitmapDrawable;
    .restart local v18    # "bitmapResized":Landroid/graphics/Bitmap;
    .restart local v21    # "fingerOffset":I
    .restart local v22    # "fromXDelta":I
    .restart local v23    # "fromYDelta":I
    .restart local v27    # "swipeImageHeight":I
    .restart local v28    # "swipeImageWidth":I
    .restart local v30    # "toXDelta":I
    .restart local v31    # "toYDelta":I
    :cond_d
    const/4 v3, 0x3

    move/from16 v0, v29

    if-ne v0, v3, :cond_e

    .line 229
    sub-int v23, v23, v21

    .line 230
    sub-int v3, v31, v27

    sub-int v31, v3, v21

    goto/16 :goto_3

    .line 231
    :cond_e
    const/4 v3, 0x1

    move/from16 v0, v29

    if-ne v0, v3, :cond_f

    .line 232
    sub-int v22, v22, v21

    .line 233
    sub-int v3, v30, v27

    sub-int v30, v3, v21

    goto/16 :goto_3

    .line 234
    :cond_f
    if-nez v29, :cond_7

    .line 235
    add-int v22, v22, v21

    .line 236
    add-int v3, v30, v27

    add-int v30, v3, v21

    goto/16 :goto_3

    .line 271
    .end local v2    # "bitmapOrg":Landroid/graphics/Bitmap;
    .end local v7    # "matrix":Landroid/graphics/Matrix;
    .end local v15    # "animationSpeed":I
    .end local v17    # "bitmapDrawable":Landroid/graphics/drawable/BitmapDrawable;
    .end local v18    # "bitmapResized":Landroid/graphics/Bitmap;
    .end local v21    # "fingerOffset":I
    .end local v22    # "fromXDelta":I
    .end local v23    # "fromYDelta":I
    .end local v26    # "rotate":I
    .end local v27    # "swipeImageHeight":I
    .end local v28    # "swipeImageWidth":I
    .end local v29    # "swipeOrientation":I
    .end local v30    # "toXDelta":I
    .end local v31    # "toYDelta":I
    .end local v32    # "xPos":I
    .end local v33    # "xScale":F
    .end local v34    # "yPos":I
    .end local v35    # "yScale":F
    :cond_10
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/validity/fingerprint/SensorView;->mFingerSwipeImage:Landroid/widget/ImageView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setAnimation(Landroid/view/animation/Animation;)V

    .line 272
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/validity/fingerprint/SensorView;->mFingerSwipeImage:Landroid/widget/ImageView;

    const/4 v4, 0x4

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 274
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/validity/fingerprint/SensorView;->mFingerOutlineImage:Landroid/widget/ImageView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setAnimation(Landroid/view/animation/Animation;)V

    .line 275
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/validity/fingerprint/SensorView;->mFingerOutlineImage:Landroid/widget/ImageView;

    const/4 v4, 0x4

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_4
.end method

.method public showAnimations(Z)V
    .locals 6
    .param p1, "visible"    # Z

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x4

    const/4 v3, 0x0

    .line 291
    const-string v0, "SensorView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "showAnimations: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 293
    if-eqz p1, :cond_5

    .line 295
    iget-object v0, p0, Lcom/validity/fingerprint/SensorView;->mSensorLED:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/validity/fingerprint/SensorView;->mSensorVisible:Z

    if-eqz v0, :cond_0

    .line 296
    iget-object v0, p0, Lcom/validity/fingerprint/SensorView;->mSensorLED:Landroid/view/View;

    iget-object v1, p0, Lcom/validity/fingerprint/SensorView;->mBlinkAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 297
    iget-object v0, p0, Lcom/validity/fingerprint/SensorView;->mSensorLED:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 300
    :cond_0
    iget-object v0, p0, Lcom/validity/fingerprint/SensorView;->mFingerSwipeImage:Landroid/widget/ImageView;

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/validity/fingerprint/SensorView;->mFingerSwipeVisible:Z

    if-eqz v0, :cond_2

    .line 301
    iget-object v0, p0, Lcom/validity/fingerprint/SensorView;->mSwipeAnimation:Landroid/view/animation/TranslateAnimation;

    if-eqz v0, :cond_1

    .line 302
    iget-object v0, p0, Lcom/validity/fingerprint/SensorView;->mFingerSwipeImage:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/validity/fingerprint/SensorView;->mSwipeAnimation:Landroid/view/animation/TranslateAnimation;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 304
    :cond_1
    iget-object v0, p0, Lcom/validity/fingerprint/SensorView;->mFingerSwipeImage:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 307
    :cond_2
    iget-object v0, p0, Lcom/validity/fingerprint/SensorView;->mFingerOutlineImage:Landroid/widget/ImageView;

    if-eqz v0, :cond_4

    iget-boolean v0, p0, Lcom/validity/fingerprint/SensorView;->mFingerSwipeVisible:Z

    if-eqz v0, :cond_4

    iget-boolean v0, p0, Lcom/validity/fingerprint/SensorView;->mFingerSwipeOutlineVisible:Z

    if-eqz v0, :cond_4

    .line 308
    iget-object v0, p0, Lcom/validity/fingerprint/SensorView;->mOutlineAnimation:Landroid/view/animation/TranslateAnimation;

    if-eqz v0, :cond_3

    .line 309
    iget-object v0, p0, Lcom/validity/fingerprint/SensorView;->mFingerOutlineImage:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/validity/fingerprint/SensorView;->mOutlineAnimation:Landroid/view/animation/TranslateAnimation;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 311
    :cond_3
    iget-object v0, p0, Lcom/validity/fingerprint/SensorView;->mFingerOutlineImage:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 330
    :cond_4
    :goto_0
    return-void

    .line 315
    :cond_5
    iget-object v0, p0, Lcom/validity/fingerprint/SensorView;->mSensorLED:Landroid/view/View;

    if-eqz v0, :cond_6

    .line 316
    iget-object v0, p0, Lcom/validity/fingerprint/SensorView;->mSensorLED:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setAnimation(Landroid/view/animation/Animation;)V

    .line 317
    iget-object v0, p0, Lcom/validity/fingerprint/SensorView;->mSensorLED:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 320
    :cond_6
    iget-object v0, p0, Lcom/validity/fingerprint/SensorView;->mFingerSwipeImage:Landroid/widget/ImageView;

    if-eqz v0, :cond_7

    .line 321
    iget-object v0, p0, Lcom/validity/fingerprint/SensorView;->mFingerSwipeImage:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setAnimation(Landroid/view/animation/Animation;)V

    .line 322
    iget-object v0, p0, Lcom/validity/fingerprint/SensorView;->mFingerSwipeImage:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 325
    :cond_7
    iget-object v0, p0, Lcom/validity/fingerprint/SensorView;->mFingerOutlineImage:Landroid/widget/ImageView;

    if-eqz v0, :cond_4

    .line 326
    iget-object v0, p0, Lcom/validity/fingerprint/SensorView;->mFingerOutlineImage:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setAnimation(Landroid/view/animation/Animation;)V

    .line 327
    iget-object v0, p0, Lcom/validity/fingerprint/SensorView;->mFingerOutlineImage:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method
