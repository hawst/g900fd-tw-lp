.class Lcom/validity/fingerprint/IdentifyActivity$4;
.super Ljava/lang/Object;
.source "IdentifyActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/validity/fingerprint/IdentifyActivity;->startIdentify()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/validity/fingerprint/IdentifyActivity;


# direct methods
.method constructor <init>(Lcom/validity/fingerprint/IdentifyActivity;)V
    .locals 0

    .prologue
    .line 274
    iput-object p1, p0, Lcom/validity/fingerprint/IdentifyActivity$4;->this$0:Lcom/validity/fingerprint/IdentifyActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 277
    iget-object v1, p0, Lcom/validity/fingerprint/IdentifyActivity$4;->this$0:Lcom/validity/fingerprint/IdentifyActivity;

    # getter for: Lcom/validity/fingerprint/IdentifyActivity;->mFingerprint:Lcom/validity/fingerprint/Fingerprint;
    invoke-static {v1}, Lcom/validity/fingerprint/IdentifyActivity;->access$300(Lcom/validity/fingerprint/IdentifyActivity;)Lcom/validity/fingerprint/Fingerprint;

    move-result-object v1

    if-nez v1, :cond_0

    .line 278
    iget-object v1, p0, Lcom/validity/fingerprint/IdentifyActivity$4;->this$0:Lcom/validity/fingerprint/IdentifyActivity;

    new-instance v2, Lcom/validity/fingerprint/Fingerprint;

    iget-object v3, p0, Lcom/validity/fingerprint/IdentifyActivity$4;->this$0:Lcom/validity/fingerprint/IdentifyActivity;

    iget-object v4, p0, Lcom/validity/fingerprint/IdentifyActivity$4;->this$0:Lcom/validity/fingerprint/IdentifyActivity;

    invoke-direct {v2, v3, v4}, Lcom/validity/fingerprint/Fingerprint;-><init>(Landroid/content/Context;Lcom/validity/fingerprint/FingerprintCore$EventListener;)V

    # setter for: Lcom/validity/fingerprint/IdentifyActivity;->mFingerprint:Lcom/validity/fingerprint/Fingerprint;
    invoke-static {v1, v2}, Lcom/validity/fingerprint/IdentifyActivity;->access$302(Lcom/validity/fingerprint/IdentifyActivity;Lcom/validity/fingerprint/Fingerprint;)Lcom/validity/fingerprint/Fingerprint;

    .line 281
    :cond_0
    iget-object v1, p0, Lcom/validity/fingerprint/IdentifyActivity$4;->this$0:Lcom/validity/fingerprint/IdentifyActivity;

    # getter for: Lcom/validity/fingerprint/IdentifyActivity;->mFingerprint:Lcom/validity/fingerprint/Fingerprint;
    invoke-static {v1}, Lcom/validity/fingerprint/IdentifyActivity;->access$300(Lcom/validity/fingerprint/IdentifyActivity;)Lcom/validity/fingerprint/Fingerprint;

    move-result-object v1

    iget-object v2, p0, Lcom/validity/fingerprint/IdentifyActivity$4;->this$0:Lcom/validity/fingerprint/IdentifyActivity;

    # getter for: Lcom/validity/fingerprint/IdentifyActivity;->mUserId:Ljava/lang/String;
    invoke-static {v2}, Lcom/validity/fingerprint/IdentifyActivity;->access$400(Lcom/validity/fingerprint/IdentifyActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/validity/fingerprint/Fingerprint;->identify(Ljava/lang/String;)I

    move-result v0

    .line 282
    .local v0, "result":I
    if-nez v0, :cond_1

    .line 283
    iget-object v1, p0, Lcom/validity/fingerprint/IdentifyActivity$4;->this$0:Lcom/validity/fingerprint/IdentifyActivity;

    const-string v2, "Please wait"

    # invokes: Lcom/validity/fingerprint/IdentifyActivity;->setUiString(Ljava/lang/String;)V
    invoke-static {v1, v2}, Lcom/validity/fingerprint/IdentifyActivity;->access$500(Lcom/validity/fingerprint/IdentifyActivity;Ljava/lang/String;)V

    .line 291
    :goto_0
    iget-object v1, p0, Lcom/validity/fingerprint/IdentifyActivity$4;->this$0:Lcom/validity/fingerprint/IdentifyActivity;

    # getter for: Lcom/validity/fingerprint/IdentifyActivity;->mLock:Ljava/util/concurrent/Semaphore;
    invoke-static {v1}, Lcom/validity/fingerprint/IdentifyActivity;->access$600(Lcom/validity/fingerprint/IdentifyActivity;)Ljava/util/concurrent/Semaphore;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    .line 293
    return-void

    .line 284
    :cond_1
    const/16 v1, 0x3ec

    if-ne v0, v1, :cond_2

    .line 285
    const-string v1, "IdentifyActivity"

    const-string v2, "Identification already in progress!"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 287
    :cond_2
    const-string v1, "IdentifyActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Identify() failed, result = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 288
    iget-object v1, p0, Lcom/validity/fingerprint/IdentifyActivity$4;->this$0:Lcom/validity/fingerprint/IdentifyActivity;

    const-string v2, "Sensor problem"

    # invokes: Lcom/validity/fingerprint/IdentifyActivity;->setUiString(Ljava/lang/String;)V
    invoke-static {v1, v2}, Lcom/validity/fingerprint/IdentifyActivity;->access$500(Lcom/validity/fingerprint/IdentifyActivity;Ljava/lang/String;)V

    goto :goto_0
.end method
