.class public Lcom/validity/fingerprint/ConfigReader$ConfigData;
.super Ljava/lang/Object;
.source "ConfigReader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/validity/fingerprint/ConfigReader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ConfigData"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/validity/fingerprint/ConfigReader$ConfigData$DisableButtons;,
        Lcom/validity/fingerprint/ConfigReader$ConfigData$FpDisplay;,
        Lcom/validity/fingerprint/ConfigReader$ConfigData$FingerSwipeAnimation;,
        Lcom/validity/fingerprint/ConfigReader$ConfigData$SensorBar;
    }
.end annotation


# instance fields
.field public disableButton:Ljava/lang/String;

.field public disableButtons:Lcom/validity/fingerprint/ConfigReader$ConfigData$DisableButtons;

.field public fingerActionGenericLabel:Ljava/lang/String;

.field public fingerLiftLabel:Ljava/lang/String;

.field public fingerPlaceOrSwipeLabel:Ljava/lang/String;

.field public fingerSwipeAnimation:Lcom/validity/fingerprint/ConfigReader$ConfigData$FingerSwipeAnimation;

.field public fpDisplay:Lcom/validity/fingerprint/ConfigReader$ConfigData$FpDisplay;

.field public hapticFeedback:Z

.field public practiceMode:Z

.field public screenOrientation:I

.field public sensorBar:Lcom/validity/fingerprint/ConfigReader$ConfigData$SensorBar;

.field public sensorType:I

.field public showVideo:Z


# direct methods
.method constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 128
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    iput v2, p0, Lcom/validity/fingerprint/ConfigReader$ConfigData;->screenOrientation:I

    .line 79
    iput v3, p0, Lcom/validity/fingerprint/ConfigReader$ConfigData;->sensorType:I

    .line 80
    const-string v0, "0000"

    iput-object v0, p0, Lcom/validity/fingerprint/ConfigReader$ConfigData;->disableButton:Ljava/lang/String;

    .line 81
    iput-boolean v2, p0, Lcom/validity/fingerprint/ConfigReader$ConfigData;->showVideo:Z

    .line 82
    iput-boolean v2, p0, Lcom/validity/fingerprint/ConfigReader$ConfigData;->practiceMode:Z

    .line 83
    iput-boolean v3, p0, Lcom/validity/fingerprint/ConfigReader$ConfigData;->hapticFeedback:Z

    .line 84
    const-string v0, "Please swipe"

    iput-object v0, p0, Lcom/validity/fingerprint/ConfigReader$ConfigData;->fingerActionGenericLabel:Ljava/lang/String;

    .line 85
    const-string v0, "Place finger"

    iput-object v0, p0, Lcom/validity/fingerprint/ConfigReader$ConfigData;->fingerPlaceOrSwipeLabel:Ljava/lang/String;

    .line 86
    const-string v0, "Please lift your finger"

    iput-object v0, p0, Lcom/validity/fingerprint/ConfigReader$ConfigData;->fingerLiftLabel:Ljava/lang/String;

    .line 123
    iput-object v1, p0, Lcom/validity/fingerprint/ConfigReader$ConfigData;->sensorBar:Lcom/validity/fingerprint/ConfigReader$ConfigData$SensorBar;

    .line 124
    iput-object v1, p0, Lcom/validity/fingerprint/ConfigReader$ConfigData;->fingerSwipeAnimation:Lcom/validity/fingerprint/ConfigReader$ConfigData$FingerSwipeAnimation;

    .line 125
    iput-object v1, p0, Lcom/validity/fingerprint/ConfigReader$ConfigData;->disableButtons:Lcom/validity/fingerprint/ConfigReader$ConfigData$DisableButtons;

    .line 126
    iput-object v1, p0, Lcom/validity/fingerprint/ConfigReader$ConfigData;->fpDisplay:Lcom/validity/fingerprint/ConfigReader$ConfigData$FpDisplay;

    .line 129
    new-instance v0, Lcom/validity/fingerprint/ConfigReader$ConfigData$SensorBar;

    invoke-direct {v0, p0}, Lcom/validity/fingerprint/ConfigReader$ConfigData$SensorBar;-><init>(Lcom/validity/fingerprint/ConfigReader$ConfigData;)V

    iput-object v0, p0, Lcom/validity/fingerprint/ConfigReader$ConfigData;->sensorBar:Lcom/validity/fingerprint/ConfigReader$ConfigData$SensorBar;

    .line 130
    new-instance v0, Lcom/validity/fingerprint/ConfigReader$ConfigData$FingerSwipeAnimation;

    invoke-direct {v0, p0}, Lcom/validity/fingerprint/ConfigReader$ConfigData$FingerSwipeAnimation;-><init>(Lcom/validity/fingerprint/ConfigReader$ConfigData;)V

    iput-object v0, p0, Lcom/validity/fingerprint/ConfigReader$ConfigData;->fingerSwipeAnimation:Lcom/validity/fingerprint/ConfigReader$ConfigData$FingerSwipeAnimation;

    .line 131
    new-instance v0, Lcom/validity/fingerprint/ConfigReader$ConfigData$FpDisplay;

    invoke-direct {v0, p0}, Lcom/validity/fingerprint/ConfigReader$ConfigData$FpDisplay;-><init>(Lcom/validity/fingerprint/ConfigReader$ConfigData;)V

    iput-object v0, p0, Lcom/validity/fingerprint/ConfigReader$ConfigData;->fpDisplay:Lcom/validity/fingerprint/ConfigReader$ConfigData$FpDisplay;

    .line 132
    new-instance v0, Lcom/validity/fingerprint/ConfigReader$ConfigData$DisableButtons;

    invoke-direct {v0, p0}, Lcom/validity/fingerprint/ConfigReader$ConfigData$DisableButtons;-><init>(Lcom/validity/fingerprint/ConfigReader$ConfigData;)V

    iput-object v0, p0, Lcom/validity/fingerprint/ConfigReader$ConfigData;->disableButtons:Lcom/validity/fingerprint/ConfigReader$ConfigData$DisableButtons;

    .line 133
    return-void
.end method


# virtual methods
.method public toStringDebug()V
    .locals 3

    .prologue
    .line 137
    const-string v0, "ConfigReader"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ConfigData():: Screen Orientation="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/validity/fingerprint/ConfigReader$ConfigData;->screenOrientation:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", SensorType="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/validity/fingerprint/ConfigReader$ConfigData;->sensorType:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", disabled buttons="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/validity/fingerprint/ConfigReader$ConfigData;->disableButton:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", fingerPlaceOrSwipeLabel="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/validity/fingerprint/ConfigReader$ConfigData;->fingerPlaceOrSwipeLabel:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", fingerLiftLabel="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/validity/fingerprint/ConfigReader$ConfigData;->fingerLiftLabel:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", fingerActionGenericLabel="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/validity/fingerprint/ConfigReader$ConfigData;->fingerActionGenericLabel:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", showVideo="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/validity/fingerprint/ConfigReader$ConfigData;->showVideo:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", practiceMode="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/validity/fingerprint/ConfigReader$ConfigData;->practiceMode:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", hapticFeedback="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/validity/fingerprint/ConfigReader$ConfigData;->hapticFeedback:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 147
    const-string v0, "ConfigReader"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Fp Display :: left = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/validity/fingerprint/ConfigReader$ConfigData;->fpDisplay:Lcom/validity/fingerprint/ConfigReader$ConfigData$FpDisplay;

    iget v2, v2, Lcom/validity/fingerprint/ConfigReader$ConfigData$FpDisplay;->xPos:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", top = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/validity/fingerprint/ConfigReader$ConfigData;->fpDisplay:Lcom/validity/fingerprint/ConfigReader$ConfigData$FpDisplay;

    iget v2, v2, Lcom/validity/fingerprint/ConfigReader$ConfigData$FpDisplay;->yPos:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", width = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/validity/fingerprint/ConfigReader$ConfigData;->fpDisplay:Lcom/validity/fingerprint/ConfigReader$ConfigData$FpDisplay;

    iget v2, v2, Lcom/validity/fingerprint/ConfigReader$ConfigData$FpDisplay;->width:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", height = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/validity/fingerprint/ConfigReader$ConfigData;->fpDisplay:Lcom/validity/fingerprint/ConfigReader$ConfigData$FpDisplay;

    iget v2, v2, Lcom/validity/fingerprint/ConfigReader$ConfigData$FpDisplay;->height:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 152
    const-string v0, "ConfigReader"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DisableButtons =>  home="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/validity/fingerprint/ConfigReader$ConfigData;->disableButtons:Lcom/validity/fingerprint/ConfigReader$ConfigData$DisableButtons;

    iget-boolean v2, v2, Lcom/validity/fingerprint/ConfigReader$ConfigData$DisableButtons;->home:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", menu="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/validity/fingerprint/ConfigReader$ConfigData;->disableButtons:Lcom/validity/fingerprint/ConfigReader$ConfigData$DisableButtons;

    iget-boolean v2, v2, Lcom/validity/fingerprint/ConfigReader$ConfigData$DisableButtons;->menu:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", search="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/validity/fingerprint/ConfigReader$ConfigData;->disableButtons:Lcom/validity/fingerprint/ConfigReader$ConfigData$DisableButtons;

    iget-boolean v2, v2, Lcom/validity/fingerprint/ConfigReader$ConfigData$DisableButtons;->search:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", back="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/validity/fingerprint/ConfigReader$ConfigData;->disableButtons:Lcom/validity/fingerprint/ConfigReader$ConfigData$DisableButtons;

    iget-boolean v2, v2, Lcom/validity/fingerprint/ConfigReader$ConfigData$DisableButtons;->back:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 156
    const-string v0, "ConfigReader"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Finger swipe animation =>  visible="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/validity/fingerprint/ConfigReader$ConfigData;->fingerSwipeAnimation:Lcom/validity/fingerprint/ConfigReader$ConfigData$FingerSwipeAnimation;

    iget-boolean v2, v2, Lcom/validity/fingerprint/ConfigReader$ConfigData$FingerSwipeAnimation;->visible:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", orientation="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/validity/fingerprint/ConfigReader$ConfigData;->fingerSwipeAnimation:Lcom/validity/fingerprint/ConfigReader$ConfigData$FingerSwipeAnimation;

    iget v2, v2, Lcom/validity/fingerprint/ConfigReader$ConfigData$FingerSwipeAnimation;->orientation:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", left="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/validity/fingerprint/ConfigReader$ConfigData;->fingerSwipeAnimation:Lcom/validity/fingerprint/ConfigReader$ConfigData$FingerSwipeAnimation;

    iget v2, v2, Lcom/validity/fingerprint/ConfigReader$ConfigData$FingerSwipeAnimation;->xPos:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", top="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/validity/fingerprint/ConfigReader$ConfigData;->fingerSwipeAnimation:Lcom/validity/fingerprint/ConfigReader$ConfigData$FingerSwipeAnimation;

    iget v2, v2, Lcom/validity/fingerprint/ConfigReader$ConfigData$FingerSwipeAnimation;->yPos:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", scale width = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/validity/fingerprint/ConfigReader$ConfigData;->fingerSwipeAnimation:Lcom/validity/fingerprint/ConfigReader$ConfigData$FingerSwipeAnimation;

    iget v2, v2, Lcom/validity/fingerprint/ConfigReader$ConfigData$FingerSwipeAnimation;->xScale:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", scale height = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/validity/fingerprint/ConfigReader$ConfigData;->fingerSwipeAnimation:Lcom/validity/fingerprint/ConfigReader$ConfigData$FingerSwipeAnimation;

    iget v2, v2, Lcom/validity/fingerprint/ConfigReader$ConfigData$FingerSwipeAnimation;->yScale:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", offset length = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/validity/fingerprint/ConfigReader$ConfigData;->fingerSwipeAnimation:Lcom/validity/fingerprint/ConfigReader$ConfigData$FingerSwipeAnimation;

    iget v2, v2, Lcom/validity/fingerprint/ConfigReader$ConfigData$FingerSwipeAnimation;->offsetLength:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", outline visible = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/validity/fingerprint/ConfigReader$ConfigData;->fingerSwipeAnimation:Lcom/validity/fingerprint/ConfigReader$ConfigData$FingerSwipeAnimation;

    iget-boolean v2, v2, Lcom/validity/fingerprint/ConfigReader$ConfigData$FingerSwipeAnimation;->outlineVisible:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", animationSpeed = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/validity/fingerprint/ConfigReader$ConfigData;->fingerSwipeAnimation:Lcom/validity/fingerprint/ConfigReader$ConfigData$FingerSwipeAnimation;

    iget v2, v2, Lcom/validity/fingerprint/ConfigReader$ConfigData$FingerSwipeAnimation;->animationSpeed:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 167
    const-string v0, "ConfigReader"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Fp Display :: left = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/validity/fingerprint/ConfigReader$ConfigData;->fpDisplay:Lcom/validity/fingerprint/ConfigReader$ConfigData$FpDisplay;

    iget v2, v2, Lcom/validity/fingerprint/ConfigReader$ConfigData$FpDisplay;->xPos:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", top = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/validity/fingerprint/ConfigReader$ConfigData;->fpDisplay:Lcom/validity/fingerprint/ConfigReader$ConfigData$FpDisplay;

    iget v2, v2, Lcom/validity/fingerprint/ConfigReader$ConfigData$FpDisplay;->yPos:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", width = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/validity/fingerprint/ConfigReader$ConfigData;->fpDisplay:Lcom/validity/fingerprint/ConfigReader$ConfigData$FpDisplay;

    iget v2, v2, Lcom/validity/fingerprint/ConfigReader$ConfigData$FpDisplay;->width:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", height = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/validity/fingerprint/ConfigReader$ConfigData;->fpDisplay:Lcom/validity/fingerprint/ConfigReader$ConfigData$FpDisplay;

    iget v2, v2, Lcom/validity/fingerprint/ConfigReader$ConfigData$FpDisplay;->height:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", showStartupVideo = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/validity/fingerprint/ConfigReader$ConfigData;->fpDisplay:Lcom/validity/fingerprint/ConfigReader$ConfigData$FpDisplay;

    iget-boolean v2, v2, Lcom/validity/fingerprint/ConfigReader$ConfigData$FpDisplay;->showStartupVideo:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 173
    return-void
.end method
