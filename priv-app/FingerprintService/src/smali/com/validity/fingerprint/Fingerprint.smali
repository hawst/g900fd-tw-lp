.class public Lcom/validity/fingerprint/Fingerprint;
.super Lcom/validity/fingerprint/FingerprintCore;
.source "Fingerprint.java"


# static fields
.field public static final VCS_ENROLL_MODE_DEFAULT:I = 0x1

.field public static final VCS_ENROLL_MODE_REENROLL:I = 0x2

.field public static final VCS_ENROLL_MODE_SECONDARY:I = 0x3

.field public static final VCS_EVT_SNSR_TEST_DATALOG_DATA:I = 0x7da

.field public static final VCS_EVT_SNSR_TEST_PUT_STIMULUS_ON_SENSOR:I = 0x7e9

.field public static final VCS_EVT_SNSR_TEST_PUT_STON_SSS_ON_SENSOR:I = 0x7ec

.field public static final VCS_EVT_SNSR_TEST_PUT_TERM_BLOCK_ON_SENSOR:I = 0x7e7

.field public static final VCS_EVT_SNSR_TEST_REMOVE_STIMULUS_FROM_SENSOR:I = 0x7ea

.field public static final VCS_EVT_SNSR_TEST_REMOVE_STON_SSS_FROM_SENSOR:I = 0x7ed

.field public static final VCS_EVT_SNSR_TEST_REMOVE_TERM_BLOCK_FROM_SENSOR:I = 0x7e8

.field public static final VCS_EVT_SNSR_TEST_RESET_AFTER_TEST_RES:I = 0x7e6

.field public static final VCS_EVT_SNSR_TEST_SCRIPT_END:I = 0x7d4

.field public static final VCS_EVT_SNSR_TEST_SCRIPT_START:I = 0x7d1

.field public static final VCS_EVT_SNSR_TEST_SECTION_END:I = 0x7d3

.field public static final VCS_EVT_SNSR_TEST_SECTION_START:I = 0x7d2

.field public static final VCS_EVT_SNSR_TEST_SNR_DATA:I = 0x7eb

.field public static final VCS_FINGER_POSITION_FEEDBACK_SWIPE_ANY:I = 0x29

.field public static final VCS_FINGER_POSITION_FEEDBACK_SWIPE_LEFT:I = 0x15

.field public static final VCS_FINGER_POSITION_FEEDBACK_SWIPE_MIDDLE:I = 0xb

.field public static final VCS_FINGER_POSITION_FEEDBACK_SWIPE_MIDDLE_FULL:I = 0xc

.field public static final VCS_FINGER_POSITION_FEEDBACK_SWIPE_RIGHT:I = 0x1f

.field public static final VCS_NOTIFY_ENROLL_BEGIN:I = 0x3

.field public static final VCS_NOTIFY_ENROLL_END:I = 0x4

.field public static final VCS_NOTIFY_SNSR_TEST_CONTINUE:I = 0x1

.field public static final VCS_NOTIFY_SNSR_TEST_STOP:I = 0x2

.field public static final VCS_REQUEST_COMMAND_SENSOR_TEST:I = 0xb

.field private static final VCS_REQUEST_ENABLE_WOF:I = 0x15

.field public static final VCS_REQUEST_GET_ENROLL_REPEAT_COUNT:I = 0xd

.field public static final VCS_REQUEST_GET_SENSOR_INFO:I = 0xc

.field private static final VCS_REQUEST_GET_WOF_STATE:I = 0x16

.field public static final VCS_REQUEST_PROCESS_ALIPAY:I = 0x2

.field public static final VCS_REQUEST_PROCESS_FIDO:I = 0x1

.field public static final VCS_SENSOR_STATUS_FAILURE:I = 0x217

.field public static final VCS_SENSOR_STATUS_INITIALISING:I = 0x166

.field public static final VCS_SENSOR_STATUS_INITIALIZING:I = 0x166

.field public static final VCS_SENSOR_STATUS_MALFUNCTIONED:I = 0x216

.field public static final VCS_SENSOR_STATUS_OK:I = 0x0

.field public static final VCS_SENSOR_STATUS_OUT_OF_ORDER:I = 0x168

.field public static final VCS_SENSOR_STATUS_WORKING:I = 0x2e

.field public static final VCS_SNSR_TEST_65AB_TN_NOFLASH_SCRIPT_ID:I = 0x816

.field public static final VCS_SNSR_TEST_65AB_TN_STON_CAL_SIG3_CHCK_SCRIPT_ID:I = 0x81e

.field public static final VCS_SNSR_TEST_65AB_TN_STON_CHCK_SCRIPT_ID:I = 0x81c

.field public static final VCS_SNSR_TEST_65AB_TN_STON_SSS_CHCK_SCRIPT_ID:I = 0x81d

.field public static final VCS_SNSR_TEST_65AB_TN_STON_SSS_CHCK_TZ_SCRIPT_ID:I = 0xfed

.field public static final VCS_SNSR_TEST_65AB_TS_NOFLASH_SCRIPT_ID:I = 0x820

.field public static final VCS_SNSR_TEST_65AB_TS_STON_CAL_SIG3_CHCK_SCRIPT_ID:I = 0x828

.field public static final VCS_SNSR_TEST_65AB_TS_STON_CHCK_SCRIPT_ID:I = 0x826

.field public static final VCS_SNSR_TEST_65AB_TS_STON_SSS_CHCK_SCRIPT_ID:I = 0x827

.field public static final VCS_SNSR_TEST_CHCK_RAPT_CAL_FAIL_BIN:I = 0x14

.field public static final VCS_SNSR_TEST_CHECK_SCRIPT_CRC16_FLAG:I = 0x40

.field public static final VCS_SNSR_TEST_CODE_FAIL_BIN:I = 0x2

.field public static final VCS_SNSR_TEST_DATALOG_CALL_INFO_FLAG:I = 0x80000

.field public static final VCS_SNSR_TEST_DATALOG_DATA_ONLY_TESTS_FLAG:I = 0x2

.field public static final VCS_SNSR_TEST_DATALOG_FILE_APPEND_FLAG:I = 0x20000000

.field public static final VCS_SNSR_TEST_DATALOG_FILE_CREATE_FLAG:I = 0x10000000

.field public static final VCS_SNSR_TEST_DATALOG_GET_PRINT_FLAG:I = 0x2000

.field public static final VCS_SNSR_TEST_DATALOG_INC_BIN_DESC_FLAG:I = 0x100

.field public static final VCS_SNSR_TEST_DATALOG_INC_NOTES_FLAG:I = 0x400

.field public static final VCS_SNSR_TEST_DATALOG_INC_SCRIPT_DB_LIST_FLAG:I = 0x800

.field public static final VCS_SNSR_TEST_DATALOG_INC_TEST_LIST_FLAG:I = 0x200

.field public static final VCS_SNSR_TEST_DATALOG_INTERMEIDATE_FLAG:I = 0x100000

.field public static final VCS_SNSR_TEST_DATALOG_NS_REGION_DATA_FLAG:I = 0x80

.field public static final VCS_SNSR_TEST_DATALOG_NS_WAVEFORM_FLAG:I = 0x40

.field public static final VCS_SNSR_TEST_DATALOG_RAW_DEC_DATA_FLAG:I = 0x40000

.field public static final VCS_SNSR_TEST_DATALOG_RAW_SENSOR_FLAG:I = 0x20

.field public static final VCS_SNSR_TEST_DATALOG_SENSOR_INFO_FLAG:I = 0x4

.field public static final VCS_SNSR_TEST_DATALOG_STON_CAL_SIG_DATA_FLAG:I = 0x10000

.field public static final VCS_SNSR_TEST_DATALOG_STON_CAL_SIG_REG_DATA_FLAG:I = 0x20000

.field public static final VCS_SNSR_TEST_DATALOG_STON_PIXEL_DATA_FLAG:I = 0x4000

.field public static final VCS_SNSR_TEST_DATALOG_STON_SSS_DATA_FLAG:I = 0x8000

.field public static final VCS_SNSR_TEST_DATALOG_TESTS_FLAG:I = 0x1

.field public static final VCS_SNSR_TEST_DATALOG_THK_WAVEFORM_FLAG:I = 0x1000

.field public static final VCS_SNSR_TEST_DATA_CLCT_FAIL_BIN:I = 0x8

.field public static final VCS_SNSR_TEST_DATA_MISSING_FAIL_BIN:I = 0x9

.field public static final VCS_SNSR_TEST_INFO_CHCK_FAIL_BIN:I = 0x6

.field public static final VCS_SNSR_TEST_NO_CALLBACK_FAIL_BIN:I = 0x5

.field public static final VCS_SNSR_TEST_NO_SENSOR_BIN:I = 0xfa

.field public static final VCS_SNSR_TEST_NO_TEST_SCRIPT_FAIL_BIN:I = 0x4

.field public static final VCS_SNSR_TEST_NS_ENV_FAIL_BIN:I = 0x20

.field public static final VCS_SNSR_TEST_NS_FAIL_BIN:I = 0x1e

.field public static final VCS_SNSR_TEST_NS_NOISE_FAIL_BIN:I = 0x22

.field public static final VCS_SNSR_TEST_NS_REGION_FAIL_BIN:I = 0x1f

.field public static final VCS_SNSR_TEST_NS_UNIFORMITY_FAIL_BIN:I = 0x21

.field public static final VCS_SNSR_TEST_OPTIONS_ATTEMPTS_MASK:I = 0xf

.field public static final VCS_SNSR_TEST_OTP_FAIL_BIN:I = 0x28

.field public static final VCS_SNSR_TEST_PASS_BIN:I = 0x1

.field public static final VCS_SNSR_TEST_RESET_SENSOR_AFTER_TEST_FLAG:I = 0x20

.field public static final VCS_SNSR_TEST_ROM_CHECKSUM_BIN:I = 0x2d

.field public static final VCS_SNSR_TEST_SCRIPT_DB_EXT_INT_FLAG:I = 0x0

.field public static final VCS_SNSR_TEST_SCRIPT_DB_EXT_ONLY_FLAG:I = 0x300

.field public static final VCS_SNSR_TEST_SCRIPT_DB_INT_EXT_FLAG:I = 0x100

.field public static final VCS_SNSR_TEST_SCRIPT_DB_INT_ONLY_FLAG:I = 0x200

.field public static final VCS_SNSR_TEST_SEND_CB_DATALOG_FLAG:I = 0x40000

.field public static final VCS_SNSR_TEST_SEND_CB_SCRIPT_START_STOP_FLAG:I = 0x10000

.field public static final VCS_SNSR_TEST_SEND_CB_SECT_START_STOP_FLAG:I = 0x20000

.field public static final VCS_SNSR_TEST_SEND_CB_SNR_INFO_FLAG:I = 0x100000

.field public static final VCS_SNSR_TEST_SEND_CB_TEST_INFO_FLAG:I = 0x80000

.field public static final VCS_SNSR_TEST_SER_NUM_FAIL_BIN:I = 0xb

.field public static final VCS_SNSR_TEST_SNSR_RUN_BIN:I = 0x46

.field public static final VCS_SNSR_TEST_SPI42A46B_STON_CHCK_SCRIPT_ID:I = 0x7d6

.field public static final VCS_SNSR_TEST_SPI42A46B_STON_SSS_CHCK_SCRIPT_ID:I = 0x7d7

.field public static final VCS_SNSR_TEST_SPI46B_ID47_NOFLASH_SCRIPT_ID:I = 0x7da

.field public static final VCS_SNSR_TEST_SPI48B_ID17_NOFLASH_SCRIPT_ID:I = 0x7e4

.field public static final VCS_SNSR_TEST_SPI48B_STON_CHCK_SCRIPT_ID:I = 0x7ea

.field public static final VCS_SNSR_TEST_SPI48B_STON_SSS_CHCK_SCRIPT_ID:I = 0x7eb

.field public static final VCS_SNSR_TEST_SPI48B_THK_CHCK2_SCRIPT_ID:I = 0x7e8

.field public static final VCS_SNSR_TEST_SPI60A_ID41_NOFLASH_SCRIPT_ID:I = 0x7ee

.field public static final VCS_SNSR_TEST_SPI60A_ID41_NOFLASH_TZ_SCRIPT_ID:I = 0xfbe

.field public static final VCS_SNSR_TEST_SPI60A_STON_CHCK_SCRIPT_ID:I = 0x7f4

.field public static final VCS_SNSR_TEST_SPI60A_STON_SSS_CHCK_SCRIPT_ID:I = 0x7f5

.field public static final VCS_SNSR_TEST_SPI60A_STON_SSS_CHCK_TZ_SCRIPT_ID:I = 0xfc5

.field public static final VCS_SNSR_TEST_SPI60A_THK_CHCK2_SCRIPT_ID:I = 0x7f2

.field public static final VCS_SNSR_TEST_SPI60M_ID2041_NOFLASH_SCRIPT_ID:I = 0x80c

.field public static final VCS_SNSR_TEST_SPI60M_ID2041_NOFLASH_TZ_SCRIPT_ID:I = 0xfdc

.field public static final VCS_SNSR_TEST_SPI60M_STON_CAL_SIG2_CHCK_SCRIPT_ID:I = 0x814

.field public static final VCS_SNSR_TEST_SPI60M_STON_CHCK_SCRIPT_ID:I = 0x812

.field public static final VCS_SNSR_TEST_SPI60M_STON_SSS_CHCK_SCRIPT_ID:I = 0x813

.field public static final VCS_SNSR_TEST_SPI60M_STON_SSS_CHCK_TZ_SCRIPT_ID:I = 0xfe3

.field public static final VCS_SNSR_TEST_SPI65AB_TN_ID82_NOFLASH_TZ_SCRIPT_ID:I = 0xfe6

.field public static final VCS_SNSR_TEST_SPI65AB_TS_ID2082_NOFLASH_TZ_SCRIPT_ID:I = 0xff0

.field public static final VCS_SNSR_TEST_STON_FAIL_BASE_BIN:I = 0x3c

.field public static final VCS_SNSR_TEST_STON_FAIL_DATA_CHCK_BIN:I = 0x40

.field public static final VCS_SNSR_TEST_STON_FAIL_NOISE_BIN:I = 0x3d

.field public static final VCS_SNSR_TEST_STON_FAIL_SIGNAL_BIN:I = 0x3e

.field public static final VCS_SNSR_TEST_STON_FAIL_STON_BIN:I = 0x3f

.field public static final VCS_SNSR_TEST_STOP_ON_FAIL_FLAG:I = 0x10

.field public static final VCS_SNSR_TEST_THK_FAIL_BIN:I = 0x33

.field public static final VCS_SNSR_TEST_THK_SETUP_FAIL_BIN:I = 0x32

.field public static final VCS_SNSR_TEST_USER_STOP_FAIL_BIN:I = 0x3

.field public static final VCS_SNSR_TEST_VAL_SNSR_FAIL_BIN:I = 0xa

.field public static final VCS_SNSR_TEST_WRONG_SECT_TYPE_VER_BIN:I = 0x7

.field public static final VCS_WOF_STATE_ACTIVE:I = 0x1

.field public static final VCS_WOF_STATE_INACTIVE:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "ctx"    # Landroid/content/Context;

    .prologue
    .line 809
    invoke-direct {p0, p1}, Lcom/validity/fingerprint/FingerprintCore;-><init>(Landroid/content/Context;)V

    .line 810
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/validity/fingerprint/FingerprintCore$EventListener;)V
    .locals 0
    .param p1, "ctx"    # Landroid/content/Context;
    .param p2, "listener"    # Lcom/validity/fingerprint/FingerprintCore$EventListener;

    .prologue
    .line 819
    invoke-direct {p0, p1, p2}, Lcom/validity/fingerprint/FingerprintCore;-><init>(Landroid/content/Context;Lcom/validity/fingerprint/FingerprintCore$EventListener;)V

    .line 820
    return-void
.end method

.method private native jniEnableSensorDevice(I)I
.end method

.method private native jniEnrollUser(Ljava/lang/Object;)I
.end method

.method private native jniEnrollUser(Ljava/lang/String;II)I
.end method

.method private native jniEnrollUser(Ljava/lang/String;Ljava/lang/String;I)I
.end method

.method private native jniGetEnrolledTemplateIdByFinger(Ljava/lang/String;ILjava/lang/Object;)I
.end method

.method private native jniGetEnrolledTemplateIds(Ljava/lang/String;Ljava/lang/Object;)I
.end method

.method private native jniGetSensorStatus()I
.end method

.method private native jniNotify(ILjava/lang/Object;)I
.end method

.method private native jniProcessFIDO([BLcom/validity/fingerprint/VcsByteArray;)I
.end method

.method private native jniReEnrollUser(Ljava/lang/String;Ljava/lang/String;II)I
.end method

.method private native jniRemoveEnrolledFinger(Ljava/lang/Object;)I
.end method

.method private native jniRemoveEnrolledFinger(Ljava/lang/String;I)I
.end method

.method private native jniRequest(ILjava/lang/Object;)I
.end method

.method private native jniSetPassword(Ljava/lang/String;[B[B)I
.end method

.method private native jniVerifyPassword(Ljava/lang/String;[B)I
.end method


# virtual methods
.method public enableSensorDevice(Z)I
    .locals 2
    .param p1, "enable"    # Z

    .prologue
    .line 1015
    iget v0, p0, Lcom/validity/fingerprint/Fingerprint;->mOperation:I

    const/16 v1, 0x96

    if-eq v0, v1, :cond_0

    .line 1016
    const/16 v0, 0x3ec

    .line 1018
    :goto_0
    return v0

    :cond_0
    if-eqz p1, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-direct {p0, v0}, Lcom/validity/fingerprint/Fingerprint;->jniEnableSensorDevice(I)I

    move-result v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public enableWakeOnFinger()I
    .locals 2

    .prologue
    .line 1095
    const/16 v0, 0x15

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/validity/fingerprint/Fingerprint;->jniRequest(ILjava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public enroll(Lcom/validity/fingerprint/EnrollUser;)I
    .locals 4
    .param p1, "enrollInfo"    # Lcom/validity/fingerprint/EnrollUser;

    .prologue
    .line 830
    const/4 v0, -0x1

    .line 832
    .local v0, "ret":I
    iget v2, p0, Lcom/validity/fingerprint/Fingerprint;->mOperation:I

    const/16 v3, 0x96

    if-eq v2, v3, :cond_0

    move v1, v0

    .line 840
    .end local v0    # "ret":I
    .local v1, "ret":I
    :goto_0
    return v1

    .line 834
    .end local v1    # "ret":I
    .restart local v0    # "ret":I
    :cond_0
    invoke-direct {p0, p1}, Lcom/validity/fingerprint/Fingerprint;->jniEnrollUser(Ljava/lang/Object;)I

    move-result v0

    .line 836
    if-nez v0, :cond_1

    .line 837
    const/16 v2, 0x97

    iput v2, p0, Lcom/validity/fingerprint/Fingerprint;->mOperation:I

    :cond_1
    move v1, v0

    .line 840
    .end local v0    # "ret":I
    .restart local v1    # "ret":I
    goto :goto_0
.end method

.method public enroll(Ljava/lang/String;I)I
    .locals 4
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "fingerIndex"    # I

    .prologue
    .line 916
    const/4 v0, -0x1

    .line 918
    .local v0, "ret":I
    iget v2, p0, Lcom/validity/fingerprint/Fingerprint;->mOperation:I

    const/16 v3, 0x96

    if-eq v2, v3, :cond_0

    move v1, v0

    .line 928
    .end local v0    # "ret":I
    .local v1, "ret":I
    :goto_0
    return v1

    .line 920
    .end local v1    # "ret":I
    .restart local v0    # "ret":I
    :cond_0
    if-nez p1, :cond_1

    const-string p1, ""

    .line 922
    :cond_1
    const/4 v2, 0x1

    invoke-direct {p0, p1, p2, v2}, Lcom/validity/fingerprint/Fingerprint;->jniEnrollUser(Ljava/lang/String;II)I

    move-result v0

    .line 924
    if-nez v0, :cond_2

    .line 925
    const/16 v2, 0x97

    iput v2, p0, Lcom/validity/fingerprint/Fingerprint;->mOperation:I

    :cond_2
    move v1, v0

    .line 928
    .end local v0    # "ret":I
    .restart local v1    # "ret":I
    goto :goto_0
.end method

.method public enroll(Ljava/lang/String;II)I
    .locals 4
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "fingerIndex"    # I
    .param p3, "mode"    # I

    .prologue
    .line 893
    const/4 v0, -0x1

    .line 895
    .local v0, "ret":I
    iget v2, p0, Lcom/validity/fingerprint/Fingerprint;->mOperation:I

    const/16 v3, 0x96

    if-eq v2, v3, :cond_0

    move v1, v0

    .line 905
    .end local v0    # "ret":I
    .local v1, "ret":I
    :goto_0
    return v1

    .line 897
    .end local v1    # "ret":I
    .restart local v0    # "ret":I
    :cond_0
    if-nez p1, :cond_1

    const-string p1, ""

    .line 899
    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/validity/fingerprint/Fingerprint;->jniEnrollUser(Ljava/lang/String;II)I

    move-result v0

    .line 901
    if-nez v0, :cond_2

    .line 902
    const/16 v2, 0x97

    iput v2, p0, Lcom/validity/fingerprint/Fingerprint;->mOperation:I

    :cond_2
    move v1, v0

    .line 905
    .end local v0    # "ret":I
    .restart local v1    # "ret":I
    goto :goto_0
.end method

.method public enroll(Ljava/lang/String;Ljava/lang/String;I)I
    .locals 4
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "appData"    # Ljava/lang/String;
    .param p3, "fingerIndex"    # I

    .prologue
    .line 863
    const/4 v0, -0x1

    .line 865
    .local v0, "ret":I
    iget v2, p0, Lcom/validity/fingerprint/Fingerprint;->mOperation:I

    const/16 v3, 0x96

    if-eq v2, v3, :cond_0

    move v1, v0

    .line 878
    .end local v0    # "ret":I
    .local v1, "ret":I
    :goto_0
    return v1

    .line 867
    .end local v1    # "ret":I
    .restart local v0    # "ret":I
    :cond_0
    if-nez p1, :cond_1

    const-string p1, ""

    .line 868
    :cond_1
    if-nez p2, :cond_2

    const-string p2, ""

    .line 870
    :cond_2
    invoke-direct {p0, p1, p2, p3}, Lcom/validity/fingerprint/Fingerprint;->jniEnrollUser(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    .line 873
    if-nez v0, :cond_3

    .line 874
    const/16 v2, 0x97

    iput v2, p0, Lcom/validity/fingerprint/Fingerprint;->mOperation:I

    :cond_3
    move v1, v0

    .line 878
    .end local v0    # "ret":I
    .restart local v1    # "ret":I
    goto :goto_0
.end method

.method public enroll(Ljava/lang/String;Ljava/lang/String;II)I
    .locals 4
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "appData"    # Ljava/lang/String;
    .param p3, "fingerIndex"    # I
    .param p4, "mode"    # I

    .prologue
    .line 945
    const/4 v0, -0x1

    .line 947
    .local v0, "ret":I
    iget v2, p0, Lcom/validity/fingerprint/Fingerprint;->mOperation:I

    const/16 v3, 0x96

    if-eq v2, v3, :cond_0

    move v1, v0

    .line 958
    .end local v0    # "ret":I
    .local v1, "ret":I
    :goto_0
    return v1

    .line 949
    .end local v1    # "ret":I
    .restart local v0    # "ret":I
    :cond_0
    if-nez p1, :cond_1

    const-string p1, ""

    .line 950
    :cond_1
    if-nez p2, :cond_2

    const-string p2, ""

    .line 952
    :cond_2
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/validity/fingerprint/Fingerprint;->jniReEnrollUser(Ljava/lang/String;Ljava/lang/String;II)I

    move-result v0

    .line 954
    if-nez v0, :cond_3

    .line 955
    const/16 v2, 0x97

    iput v2, p0, Lcom/validity/fingerprint/Fingerprint;->mOperation:I

    :cond_3
    move v1, v0

    .line 958
    .end local v0    # "ret":I
    .restart local v1    # "ret":I
    goto :goto_0
.end method

.method public getEnrolledTemplateIdByFinger(Ljava/lang/String;ILcom/validity/fingerprint/VcsTemplateIds;)I
    .locals 2
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "fingerIndex"    # I
    .param p3, "vcsTemplateIds"    # Lcom/validity/fingerprint/VcsTemplateIds;

    .prologue
    .line 1130
    iget v0, p0, Lcom/validity/fingerprint/Fingerprint;->mOperation:I

    const/16 v1, 0x96

    if-eq v0, v1, :cond_0

    .line 1131
    const/16 v0, 0x3ec

    .line 1133
    :goto_0
    return v0

    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/validity/fingerprint/Fingerprint;->jniGetEnrolledTemplateIdByFinger(Ljava/lang/String;ILjava/lang/Object;)I

    move-result v0

    goto :goto_0
.end method

.method public getEnrolledTemplateIds(Ljava/lang/String;Lcom/validity/fingerprint/VcsTemplateIds;)I
    .locals 2
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "vcsTemplateIds"    # Lcom/validity/fingerprint/VcsTemplateIds;

    .prologue
    .line 1117
    iget v0, p0, Lcom/validity/fingerprint/Fingerprint;->mOperation:I

    const/16 v1, 0x96

    if-eq v0, v1, :cond_0

    .line 1118
    const/16 v0, 0x3ec

    .line 1120
    :goto_0
    return v0

    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/validity/fingerprint/Fingerprint;->jniGetEnrolledTemplateIds(Ljava/lang/String;Ljava/lang/Object;)I

    move-result v0

    goto :goto_0
.end method

.method public getSensorStatus()I
    .locals 2

    .prologue
    .line 1003
    iget v0, p0, Lcom/validity/fingerprint/Fingerprint;->mOperation:I

    const/16 v1, 0x96

    if-eq v0, v1, :cond_0

    .line 1004
    const/16 v0, 0x3ec

    .line 1006
    :goto_0
    return v0

    :cond_0
    invoke-direct {p0}, Lcom/validity/fingerprint/Fingerprint;->jniGetSensorStatus()I

    move-result v0

    goto :goto_0
.end method

.method public getWakeOnFingerState(Lcom/validity/fingerprint/VcsInt;)I
    .locals 1
    .param p1, "wofState"    # Lcom/validity/fingerprint/VcsInt;

    .prologue
    .line 1105
    if-nez p1, :cond_0

    .line 1106
    const/16 v0, 0x3ee

    .line 1108
    :goto_0
    return v0

    :cond_0
    const/16 v0, 0x16

    invoke-direct {p0, v0, p1}, Lcom/validity/fingerprint/Fingerprint;->jniRequest(ILjava/lang/Object;)I

    move-result v0

    goto :goto_0
.end method

.method public notify(ILjava/lang/Object;)I
    .locals 1
    .param p1, "code"    # I
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 1069
    invoke-direct {p0, p1, p2}, Lcom/validity/fingerprint/Fingerprint;->jniNotify(ILjava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public processFIDO([BLcom/validity/fingerprint/VcsByteArray;)I
    .locals 1
    .param p1, "requestData"    # [B
    .param p2, "responseData"    # Lcom/validity/fingerprint/VcsByteArray;

    .prologue
    .line 1081
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 1082
    :cond_0
    const/16 v0, 0x3ee

    .line 1085
    :goto_0
    return v0

    :cond_1
    invoke-direct {p0, p1, p2}, Lcom/validity/fingerprint/Fingerprint;->jniProcessFIDO([BLcom/validity/fingerprint/VcsByteArray;)I

    move-result v0

    goto :goto_0
.end method

.method public removeEnrolledFinger(Lcom/validity/fingerprint/RemoveEnroll;)I
    .locals 1
    .param p1, "enrollInfo"    # Lcom/validity/fingerprint/RemoveEnroll;

    .prologue
    .line 850
    invoke-direct {p0, p1}, Lcom/validity/fingerprint/Fingerprint;->jniRemoveEnrolledFinger(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public removeEnrolledFinger(Ljava/lang/String;I)I
    .locals 2
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "fingerIndex"    # I

    .prologue
    .line 988
    iget v0, p0, Lcom/validity/fingerprint/Fingerprint;->mOperation:I

    const/16 v1, 0x96

    if-eq v0, v1, :cond_0

    .line 989
    const/16 v0, 0x3ec

    .line 994
    :goto_0
    return v0

    .line 992
    :cond_0
    if-nez p1, :cond_1

    const-string p1, ""

    .line 994
    :cond_1
    invoke-direct {p0, p1, p2}, Lcom/validity/fingerprint/Fingerprint;->jniRemoveEnrolledFinger(Ljava/lang/String;I)I

    move-result v0

    goto :goto_0
.end method

.method public removeEnrolledUser(Ljava/lang/String;)I
    .locals 2
    .param p1, "userId"    # Ljava/lang/String;

    .prologue
    .line 968
    iget v0, p0, Lcom/validity/fingerprint/Fingerprint;->mOperation:I

    const/16 v1, 0x96

    if-eq v0, v1, :cond_0

    .line 969
    const/16 v0, 0x3ec

    .line 974
    :goto_0
    return v0

    .line 972
    :cond_0
    if-nez p1, :cond_1

    const-string p1, ""

    .line 974
    :cond_1
    const/16 v0, 0xb

    invoke-direct {p0, p1, v0}, Lcom/validity/fingerprint/Fingerprint;->jniRemoveEnrolledFinger(Ljava/lang/String;I)I

    move-result v0

    goto :goto_0
.end method

.method public request(ILjava/lang/Object;)I
    .locals 1
    .param p1, "command"    # I
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 1059
    invoke-direct {p0, p1, p2}, Lcom/validity/fingerprint/Fingerprint;->jniRequest(ILjava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public setPassword(Ljava/lang/String;[B)I
    .locals 1
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "newPwdHash"    # [B

    .prologue
    .line 1029
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 1030
    :cond_0
    const/16 v0, 0x3ee

    .line 1033
    :goto_0
    return v0

    :cond_1
    const-string v0, ""

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lcom/validity/fingerprint/Fingerprint;->jniSetPassword(Ljava/lang/String;[B[B)I

    move-result v0

    goto :goto_0
.end method

.method public verifyPassword(Ljava/lang/String;[B)I
    .locals 1
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "pwdHash"    # [B

    .prologue
    .line 1045
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 1046
    :cond_0
    const/16 v0, 0x3ee

    .line 1049
    :goto_0
    return v0

    :cond_1
    invoke-direct {p0, p1, p2}, Lcom/validity/fingerprint/Fingerprint;->jniVerifyPassword(Ljava/lang/String;[B)I

    move-result v0

    goto :goto_0
.end method
