.class Lcom/validity/fingerprint/ConfigReader$ConfigData$FingerSwipeAnimation;
.super Ljava/lang/Object;
.source "ConfigReader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/validity/fingerprint/ConfigReader$ConfigData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "FingerSwipeAnimation"
.end annotation


# instance fields
.field animationSpeed:I

.field offsetLength:I

.field orientation:I

.field outlineVisible:Z

.field final synthetic this$0:Lcom/validity/fingerprint/ConfigReader$ConfigData;

.field visible:Z

.field xPos:I

.field xScale:F

.field yPos:I

.field yScale:F


# direct methods
.method constructor <init>(Lcom/validity/fingerprint/ConfigReader$ConfigData;)V
    .locals 3

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    .line 96
    iput-object p1, p0, Lcom/validity/fingerprint/ConfigReader$ConfigData$FingerSwipeAnimation;->this$0:Lcom/validity/fingerprint/ConfigReader$ConfigData;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 97
    iput-boolean v1, p0, Lcom/validity/fingerprint/ConfigReader$ConfigData$FingerSwipeAnimation;->visible:Z

    .line 98
    iput v1, p0, Lcom/validity/fingerprint/ConfigReader$ConfigData$FingerSwipeAnimation;->orientation:I

    .line 99
    const/16 v0, 0x258

    iput v0, p0, Lcom/validity/fingerprint/ConfigReader$ConfigData$FingerSwipeAnimation;->xPos:I

    .line 100
    const/16 v0, 0x320

    iput v0, p0, Lcom/validity/fingerprint/ConfigReader$ConfigData$FingerSwipeAnimation;->yPos:I

    .line 101
    iput v2, p0, Lcom/validity/fingerprint/ConfigReader$ConfigData$FingerSwipeAnimation;->xScale:F

    .line 102
    iput v2, p0, Lcom/validity/fingerprint/ConfigReader$ConfigData$FingerSwipeAnimation;->yScale:F

    .line 103
    iput v1, p0, Lcom/validity/fingerprint/ConfigReader$ConfigData$FingerSwipeAnimation;->offsetLength:I

    .line 104
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/validity/fingerprint/ConfigReader$ConfigData$FingerSwipeAnimation;->outlineVisible:Z

    .line 105
    const/16 v0, 0x5dc

    iput v0, p0, Lcom/validity/fingerprint/ConfigReader$ConfigData$FingerSwipeAnimation;->animationSpeed:I

    return-void
.end method
