.class public Lcom/validity/fingerprint/ConfigReader;
.super Ljava/lang/Object;
.source "ConfigReader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/validity/fingerprint/ConfigReader$DataHandler;,
        Lcom/validity/fingerprint/ConfigReader$ConfigData;
    }
.end annotation


# static fields
.field public static final CONFIG_FILE:Ljava/lang/String; = "/system/etc/ValidityConfig.xml"

.field public static final TAG:Ljava/lang/String; = "ConfigReader"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 179
    return-void
.end method

.method public static getData()Lcom/validity/fingerprint/ConfigReader$ConfigData;
    .locals 12

    .prologue
    .line 39
    const/4 v0, 0x0

    .line 43
    .local v0, "configData":Lcom/validity/fingerprint/ConfigReader$ConfigData;
    :try_start_0
    new-instance v3, Ljava/io/FileInputStream;

    const-string v10, "/system/etc/ValidityConfig.xml"

    invoke-direct {v3, v10}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    .line 44
    .local v3, "inputStream":Ljava/io/InputStream;
    invoke-virtual {v3}, Ljava/io/InputStream;->available()I

    move-result v10

    if-lez v10, :cond_0

    .line 45
    new-instance v1, Lcom/validity/fingerprint/ConfigReader$DataHandler;

    invoke-direct {v1}, Lcom/validity/fingerprint/ConfigReader$DataHandler;-><init>()V

    .line 46
    .local v1, "dataHandler":Lcom/validity/fingerprint/ConfigReader$DataHandler;
    invoke-static {}, Ljavax/xml/parsers/SAXParserFactory;->newInstance()Ljavax/xml/parsers/SAXParserFactory;

    move-result-object v8

    .line 47
    .local v8, "spf":Ljavax/xml/parsers/SAXParserFactory;
    invoke-virtual {v8}, Ljavax/xml/parsers/SAXParserFactory;->newSAXParser()Ljavax/xml/parsers/SAXParser;

    move-result-object v7

    .line 48
    .local v7, "sp":Ljavax/xml/parsers/SAXParser;
    invoke-virtual {v7}, Ljavax/xml/parsers/SAXParser;->getXMLReader()Lorg/xml/sax/XMLReader;

    move-result-object v9

    .line 49
    .local v9, "xr":Lorg/xml/sax/XMLReader;
    invoke-interface {v9, v1}, Lorg/xml/sax/XMLReader;->setContentHandler(Lorg/xml/sax/ContentHandler;)V

    .line 50
    new-instance v10, Lorg/xml/sax/InputSource;

    invoke-direct {v10, v3}, Lorg/xml/sax/InputSource;-><init>(Ljava/io/InputStream;)V

    invoke-interface {v9, v10}, Lorg/xml/sax/XMLReader;->parse(Lorg/xml/sax/InputSource;)V

    .line 53
    invoke-virtual {v1}, Lcom/validity/fingerprint/ConfigReader$DataHandler;->getData()Lcom/validity/fingerprint/ConfigReader$ConfigData;

    move-result-object v0

    .line 55
    .end local v1    # "dataHandler":Lcom/validity/fingerprint/ConfigReader$DataHandler;
    .end local v7    # "sp":Ljavax/xml/parsers/SAXParser;
    .end local v8    # "spf":Ljavax/xml/parsers/SAXParserFactory;
    .end local v9    # "xr":Lorg/xml/sax/XMLReader;
    :cond_0
    if-eqz v3, :cond_1

    .line 56
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljavax/xml/parsers/ParserConfigurationException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lorg/xml/sax/SAXException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3

    .line 69
    .end local v3    # "inputStream":Ljava/io/InputStream;
    :cond_1
    :goto_0
    return-object v0

    .line 59
    :catch_0
    move-exception v2

    .line 60
    .local v2, "fnfe":Ljava/io/FileNotFoundException;
    const-string v10, "ConfigReader"

    const-string v11, "Configuration file[/system/etc/ValidityConfig.xml] not found"

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 61
    .end local v2    # "fnfe":Ljava/io/FileNotFoundException;
    :catch_1
    move-exception v5

    .line 62
    .local v5, "pce":Ljavax/xml/parsers/ParserConfigurationException;
    const-string v10, "ConfigReader"

    const-string v11, "SAX parse error"

    invoke-static {v10, v11, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 63
    .end local v5    # "pce":Ljavax/xml/parsers/ParserConfigurationException;
    :catch_2
    move-exception v6

    .line 64
    .local v6, "se":Lorg/xml/sax/SAXException;
    const-string v10, "ConfigReader"

    const-string v11, "SAX error"

    invoke-static {v10, v11, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 65
    .end local v6    # "se":Lorg/xml/sax/SAXException;
    :catch_3
    move-exception v4

    .line 66
    .local v4, "ioe":Ljava/io/IOException;
    const-string v10, "ConfigReader"

    const-string v11, "SAX parse io error"

    invoke-static {v10, v11, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method
