.class Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog$9;
.super Ljava/lang/Object;
.source "IdentifyFingerprintDialog.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->resetErrorMsgAndRequest()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;


# direct methods
.method constructor <init>(Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;)V
    .locals 0

    .prologue
    .line 1044
    iput-object p1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog$9;->this$0:Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 1047
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog$9;->this$0:Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;

    # invokes: Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->identify()Z
    invoke-static {v0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->access$600(Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;)Z

    .line 1048
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog$9;->this$0:Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;

    iget-object v0, v0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mScrollImage:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 1049
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog$9;->this$0:Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;

    iget-object v0, v0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mScrollImage:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1050
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog$9;->this$0:Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;

    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog$9;->this$0:Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;

    iget-object v1, v1, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mContext:Landroid/content/Context;

    const v2, 0x7f04000c

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    # setter for: Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mTweenAni:Landroid/view/animation/Animation;
    invoke-static {v0, v1}, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->access$702(Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;Landroid/view/animation/Animation;)Landroid/view/animation/Animation;

    .line 1051
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog$9;->this$0:Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;

    iget-object v0, v0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mScrollImage:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog$9;->this$0:Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;

    # getter for: Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mTweenAni:Landroid/view/animation/Animation;
    invoke-static {v1}, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->access$700(Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1053
    :cond_0
    return-void
.end method
