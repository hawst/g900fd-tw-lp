.class public Lcom/samsung/android/fingerprint/service/vendor/FingerprintValues$Generic2Validity;
.super Ljava/lang/Object;
.source "FingerprintValues.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/fingerprint/service/vendor/FingerprintValues;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Generic2Validity"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 373
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getAppData(I)Ljava/lang/String;
    .locals 1
    .param p0, "fingerIndex"    # I

    .prologue
    .line 394
    const-string v0, ""

    return-object v0
.end method

.method public static getEnrollMode(I)I
    .locals 2
    .param p0, "enroll"    # I

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    .line 409
    if-ne p0, v1, :cond_0

    .line 415
    :goto_0
    return v0

    .line 412
    :cond_0
    if-ne p0, v0, :cond_1

    .line 413
    const/4 v0, 0x3

    goto :goto_0

    :cond_1
    move v0, v1

    .line 415
    goto :goto_0
.end method

.method public static getFingerIndex(I)I
    .locals 6
    .param p0, "genericFingerIndex"    # I

    .prologue
    const/4 v4, 0x0

    .line 375
    # getter for: Lcom/samsung/android/fingerprint/service/vendor/FingerprintValues;->FINGER_INDEX_MAP:[[I
    invoke-static {}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintValues;->access$000()[[I

    move-result-object v0

    .local v0, "arr$":[[I
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v1, v0, v2

    .line 376
    .local v1, "entry":[I
    aget v5, v1, v4

    if-ne v5, p0, :cond_1

    .line 377
    const/4 v4, 0x1

    aget v4, v1, v4

    .line 381
    .end local v1    # "entry":[I
    :cond_0
    return v4

    .line 375
    .restart local v1    # "entry":[I
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public static getNotifyType(I)I
    .locals 1
    .param p0, "notiType"    # I

    .prologue
    .line 419
    const/4 v0, 0x0

    .line 420
    .local v0, "retType":I
    packed-switch p0, :pswitch_data_0

    .line 434
    :goto_0
    return v0

    .line 422
    :pswitch_0
    const/4 v0, 0x3

    .line 423
    goto :goto_0

    .line 425
    :pswitch_1
    const/4 v0, 0x4

    .line 426
    goto :goto_0

    .line 428
    :pswitch_2
    const/4 v0, 0x5

    .line 429
    goto :goto_0

    .line 431
    :pswitch_3
    const/4 v0, 0x6

    goto :goto_0

    .line 420
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static getSecurityLevel(I)I
    .locals 6
    .param p0, "securityLevel"    # I

    .prologue
    const/4 v4, 0x1

    .line 399
    # getter for: Lcom/samsung/android/fingerprint/service/vendor/FingerprintValues;->FINGER_SECURITY_LEVEL_MAP:[[I
    invoke-static {}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintValues;->access$500()[[I

    move-result-object v0

    .local v0, "arr$":[[I
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v1, v0, v2

    .line 400
    .local v1, "entry":[I
    const/4 v5, 0x0

    aget v5, v1, v5

    if-ne v5, p0, :cond_1

    .line 401
    aget v4, v1, v4

    .line 405
    .end local v1    # "entry":[I
    :cond_0
    return v4

    .line 399
    .restart local v1    # "entry":[I
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method
