.class Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog$11;
.super Ljava/lang/Object;
.source "IdentifyBackupDialog.java"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->setEditBoxListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;


# direct methods
.method constructor <init>(Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;)V
    .locals 0

    .prologue
    .line 698
    iput-object p1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog$11;->this$0:Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGlobalLayout()V
    .locals 6

    .prologue
    .line 702
    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    .line 703
    .local v3, "r":Landroid/graphics/Rect;
    iget-object v4, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog$11;->this$0:Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;

    invoke-virtual {v4}, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->getWindow()Landroid/view/Window;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v2

    .line 704
    .local v2, "parent":Landroid/view/View;
    invoke-virtual {v2, v3}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 706
    invoke-virtual {v2}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getHeight()I

    move-result v4

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v5

    sub-int/2addr v4, v5

    iget v5, v3, Landroid/graphics/Rect;->top:I

    sub-int v0, v4, v5

    .line 707
    .local v0, "mKeypadPxSize":I
    iget-object v4, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog$11;->this$0:Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;

    iget-object v4, v4, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPasswordBox:Landroid/widget/LinearLayout;

    if-eqz v4, :cond_1

    .line 708
    iget-object v4, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog$11;->this$0:Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;

    iget-object v4, v4, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPasswordBox:Landroid/widget/LinearLayout;

    invoke-virtual {v4}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 709
    .local v1, "params":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v4, 0xc8

    if-le v0, v4, :cond_0

    .line 710
    iput v0, v1, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    .line 711
    invoke-virtual {v2}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getHeight()I

    move-result v4

    sub-int/2addr v4, v0

    iget v5, v1, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    sub-int/2addr v4, v5

    iput v4, v1, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 713
    :cond_0
    iget-object v4, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog$11;->this$0:Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;

    iget-object v4, v4, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPasswordBox:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 715
    .end local v1    # "params":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_1
    return-void
.end method
