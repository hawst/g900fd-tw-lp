.class Lcom/samsung/android/fingerprint/service/FingerprintManagerService$IdentifyDialogObject;
.super Ljava/lang/Object;
.source "FingerprintManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/fingerprint/service/FingerprintManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "IdentifyDialogObject"
.end annotation


# instance fields
.field private mBundle:Landroid/os/Bundle;

.field private mRecord:Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 3004
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/fingerprint/service/FingerprintManagerService$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/android/fingerprint/service/FingerprintManagerService$1;

    .prologue
    .line 3004
    invoke-direct {p0}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService$IdentifyDialogObject;-><init>()V

    return-void
.end method


# virtual methods
.method public getAttribute()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 3015
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService$IdentifyDialogObject;->mBundle:Landroid/os/Bundle;

    return-object v0
.end method

.method public getRecord()Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;
    .locals 1

    .prologue
    .line 3019
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService$IdentifyDialogObject;->mRecord:Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

    return-object v0
.end method

.method public setAttributeAndRecord(Landroid/os/Bundle;Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;)Lcom/samsung/android/fingerprint/service/FingerprintManagerService$IdentifyDialogObject;
    .locals 0
    .param p1, "bundle"    # Landroid/os/Bundle;
    .param p2, "record"    # Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

    .prologue
    .line 3009
    iput-object p1, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService$IdentifyDialogObject;->mBundle:Landroid/os/Bundle;

    .line 3010
    iput-object p2, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService$IdentifyDialogObject;->mRecord:Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

    .line 3011
    return-object p0
.end method
