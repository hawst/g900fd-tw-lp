.class Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$5;
.super Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$ImmediateCommand;
.source "FingerprintLibraryImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;


# direct methods
.method constructor <init>(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p2, "x0"    # Ljava/lang/String;
    .param p3, "x1"    # Ljava/lang/String;

    .prologue
    .line 1201
    iput-object p1, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$5;->this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$ImmediateCommand;-><init>(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public doCommand()Ljava/lang/Object;
    .locals 5

    .prologue
    .line 1204
    new-instance v1, Lcom/validity/fingerprint/VcsInt;

    invoke-direct {v1}, Lcom/validity/fingerprint/VcsInt;-><init>()V

    .line 1206
    .local v1, "validityFingermask":Lcom/validity/fingerprint/VcsInt;
    const-string v2, "ValidityFingerprintLibraryImpl"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getEnrolledFingerList(userId="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$5;->stringUserId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1208
    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$5;->this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    # getter for: Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mFingerprint:Lcom/samsung/android/fingerprint/service/vendor/FingerprintProxy;
    invoke-static {v2}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->access$100(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;)Lcom/samsung/android/fingerprint/service/vendor/FingerprintProxy;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$5;->stringUserId:Ljava/lang/String;

    invoke-interface {v2, v3, v1}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintProxy;->getEnrolledFingerList(Ljava/lang/String;Lcom/validity/fingerprint/VcsInt;)I

    move-result v0

    .line 1211
    .local v0, "result":I
    if-eqz v0, :cond_0

    .line 1212
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 1216
    :goto_0
    return-object v2

    :cond_0
    iget v2, v1, Lcom/validity/fingerprint/VcsInt;->num:I

    invoke-static {v2}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintValues$Validity2Generic;->getFingerMask(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    goto :goto_0
.end method
