.class Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter$FingerprintDBThread;
.super Landroid/os/HandlerThread;
.source "FingerprintDBAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "FingerprintDBThread"
.end annotation


# instance fields
.field private mFingerprintDBHandler:Landroid/os/Handler;

.field final synthetic this$0:Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;


# direct methods
.method public constructor <init>(Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;Ljava/lang/String;)V
    .locals 1
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 80
    iput-object p1, p0, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter$FingerprintDBThread;->this$0:Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;

    .line 81
    invoke-direct {p0, p2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 82
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter$FingerprintDBThread;->setDaemon(Z)V

    .line 83
    return-void
.end method


# virtual methods
.method protected onLooperPrepared()V
    .locals 1

    .prologue
    .line 87
    new-instance v0, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter$FingerprintDBThread$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter$FingerprintDBThread$1;-><init>(Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter$FingerprintDBThread;)V

    iput-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter$FingerprintDBThread;->mFingerprintDBHandler:Landroid/os/Handler;

    .line 126
    return-void
.end method

.method public sendMessage(Landroid/os/Message;)V
    .locals 1
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 129
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter$FingerprintDBThread;->mFingerprintDBHandler:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 130
    return-void
.end method

.method public stopLooper()V
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter$FingerprintDBThread;->mFingerprintDBHandler:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    .line 134
    return-void
.end method
