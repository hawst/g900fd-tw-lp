.class Lcom/samsung/android/fingerprint/service/FingerprintManagerService$H;
.super Landroid/os/Handler;
.source "FingerprintManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/fingerprint/service/FingerprintManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "H"
.end annotation


# static fields
.field private static final MSG_BIND_SENSOR_DAEMON:I = 0x230

.field private static final MSG_CHANGE_PRIORITY:I = 0x226

.field private static final MSG_CHECK_SENSOR_READY:I = 0x209

.field private static final MSG_FINGER_PRINT_EVENT:I = 0x20a

.field private static final MSG_IDENITFY_UI:I = 0x264

.field private static final MSG_NOTIFY_BACKGROUND_CLIENTS:I = 0x200

.field private static final MSG_NOTIFY_FINGER_ADDED:I = 0x2c9

.field private static final MSG_NOTIFY_FINGER_PASSWORD_UPDATED:I = 0x2ca

.field private static final MSG_NOTIFY_FINGER_REMOVED:I = 0x2c8

.field private static final MSG_PACKAGE_REMOVED:I = 0x321

.field private static final MSG_START_INIT_SERVICE:I = 0x208

.field private static final MSG_USER_REMOVED:I = 0x320

.field private static final NOTIFY_BACKGROUND_DELAY_TIME:I = 0x12c


# instance fields
.field private final mMsgCodes:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/samsung/android/fingerprint/service/FingerprintManagerService;


# direct methods
.method public constructor <init>(Lcom/samsung/android/fingerprint/service/FingerprintManagerService;)V
    .locals 3

    .prologue
    .line 211
    iput-object p1, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService$H;->this$0:Lcom/samsung/android/fingerprint/service/FingerprintManagerService;

    .line 212
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 209
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService$H;->mMsgCodes:Landroid/util/SparseArray;

    .line 214
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService$H;->mMsgCodes:Landroid/util/SparseArray;

    const/16 v1, 0x208

    const-string v2, "MSG_START_INIT_SERVICE"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 215
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService$H;->mMsgCodes:Landroid/util/SparseArray;

    const/16 v1, 0x209

    const-string v2, "MSG_CHECK_SENSOR_READY"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 216
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService$H;->mMsgCodes:Landroid/util/SparseArray;

    const/16 v1, 0x200

    const-string v2, "MSG_NOTIFY_BACKGROUND_CLIENTS"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 217
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService$H;->mMsgCodes:Landroid/util/SparseArray;

    const/16 v1, 0x20a

    const-string v2, "MSG_FINGER_PRINT_EVENT"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 218
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService$H;->mMsgCodes:Landroid/util/SparseArray;

    const/16 v1, 0x226

    const-string v2, "MSG_CHANGE_PRIORITY"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 219
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService$H;->mMsgCodes:Landroid/util/SparseArray;

    const/16 v1, 0x230

    const-string v2, "MSG_REBIND_SENSOR_SVC"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 220
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService$H;->mMsgCodes:Landroid/util/SparseArray;

    const/16 v1, 0x264

    const-string v2, "MSG_IDENITFY_UI"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 221
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService$H;->mMsgCodes:Landroid/util/SparseArray;

    const/16 v1, 0x2c8

    const-string v2, "MSG_NOTIFY_FINGER_REMOVED"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 222
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService$H;->mMsgCodes:Landroid/util/SparseArray;

    const/16 v1, 0x2c9

    const-string v2, "MSG_NOTIFY_FINGER_ADDED"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 223
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService$H;->mMsgCodes:Landroid/util/SparseArray;

    const/16 v1, 0x2ca

    const-string v2, "MSG_NOTIFY_FINGER_PASSWORD_UPDATED"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 224
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService$H;->mMsgCodes:Landroid/util/SparseArray;

    const/16 v1, 0x320

    const-string v2, "MSG_USER_REMOVED"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 225
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService$H;->mMsgCodes:Landroid/util/SparseArray;

    const/16 v1, 0x321

    const-string v2, "MSG_PACKAGE_REMOVED"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 227
    return-void
.end method

.method private codeToString(I)Ljava/lang/String;
    .locals 1
    .param p1, "msg"    # I

    .prologue
    .line 230
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService$H;->mMsgCodes:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 12
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v11, 0x0

    const/16 v10, 0xa

    const-wide/16 v8, -0x1

    .line 234
    const-string v5, "FingerprintManagerService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "handleMessage: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p1, Landroid/os/Message;->what:I

    invoke-direct {p0, v7}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService$H;->codeToString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 236
    iget v5, p1, Landroid/os/Message;->what:I

    sparse-switch v5, :sswitch_data_0

    .line 317
    :cond_0
    :goto_0
    return-void

    .line 238
    :sswitch_0
    iget-object v5, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService$H;->this$0:Lcom/samsung/android/fingerprint/service/FingerprintManagerService;

    # invokes: Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->handleStartInitService()V
    invoke-static {v5}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->access$200(Lcom/samsung/android/fingerprint/service/FingerprintManagerService;)V

    goto :goto_0

    .line 242
    :sswitch_1
    iget-object v5, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService$H;->this$0:Lcom/samsung/android/fingerprint/service/FingerprintManagerService;

    # invokes: Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->handleCheckSensorReady()V
    invoke-static {v5}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->access$300(Lcom/samsung/android/fingerprint/service/FingerprintManagerService;)V

    goto :goto_0

    .line 246
    :sswitch_2
    iget-object v6, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService$H;->this$0:Lcom/samsung/android/fingerprint/service/FingerprintManagerService;

    iget-object v5, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v5, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

    invoke-virtual {v6, v5}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->handleNotifyBackgroundClients(Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;)V

    goto :goto_0

    .line 250
    :sswitch_3
    iget-object v5, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    if-eqz v5, :cond_0

    .line 251
    iget-object v5, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService$H;->this$0:Lcom/samsung/android/fingerprint/service/FingerprintManagerService;

    # getter for: Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mFocusedClientSync:Ljava/lang/Object;
    invoke-static {v5}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->access$400(Lcom/samsung/android/fingerprint/service/FingerprintManagerService;)Ljava/lang/Object;

    move-result-object v6

    monitor-enter v6

    .line 252
    :try_start_0
    iget-object v5, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService$H;->this$0:Lcom/samsung/android/fingerprint/service/FingerprintManagerService;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v7

    # setter for: Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mFocusedClientSyncThread:Ljava/lang/Thread;
    invoke-static {v5, v7}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->access$502(Lcom/samsung/android/fingerprint/service/FingerprintManagerService;Ljava/lang/Thread;)Ljava/lang/Thread;

    .line 254
    iget-object v5, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService$H;->this$0:Lcom/samsung/android/fingerprint/service/FingerprintManagerService;

    iget-object v7, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    # invokes: Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->handleFingerprintEvent(Ljava/lang/Object;)V
    invoke-static {v5, v7}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->access$600(Lcom/samsung/android/fingerprint/service/FingerprintManagerService;Ljava/lang/Object;)V

    .line 256
    iget-object v5, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService$H;->this$0:Lcom/samsung/android/fingerprint/service/FingerprintManagerService;

    const/4 v7, 0x0

    # setter for: Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mFocusedClientSyncThread:Ljava/lang/Thread;
    invoke-static {v5, v7}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->access$502(Lcom/samsung/android/fingerprint/service/FingerprintManagerService;Ljava/lang/Thread;)Ljava/lang/Thread;

    .line 257
    monitor-exit v6

    goto :goto_0

    :catchall_0
    move-exception v5

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v5

    .line 261
    :sswitch_4
    iget-object v5, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService$H;->this$0:Lcom/samsung/android/fingerprint/service/FingerprintManagerService;

    # invokes: Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->handleChangePriority()V
    invoke-static {v5}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->access$700(Lcom/samsung/android/fingerprint/service/FingerprintManagerService;)V

    goto :goto_0

    .line 264
    :sswitch_5
    iget-object v5, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService$H;->this$0:Lcom/samsung/android/fingerprint/service/FingerprintManagerService;

    invoke-virtual {v5}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->cleanUpService()V

    goto :goto_0

    .line 267
    :sswitch_6
    new-instance v0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService$IdentifyDialogObject;

    invoke-direct {v0, v11}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService$IdentifyDialogObject;-><init>(Lcom/samsung/android/fingerprint/service/FingerprintManagerService$1;)V

    .line 268
    .local v0, "Object":Lcom/samsung/android/fingerprint/service/FingerprintManagerService$IdentifyDialogObject;
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .end local v0    # "Object":Lcom/samsung/android/fingerprint/service/FingerprintManagerService$IdentifyDialogObject;
    check-cast v0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService$IdentifyDialogObject;

    .line 269
    .restart local v0    # "Object":Lcom/samsung/android/fingerprint/service/FingerprintManagerService$IdentifyDialogObject;
    iget-object v5, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService$H;->this$0:Lcom/samsung/android/fingerprint/service/FingerprintManagerService;

    invoke-virtual {v0}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService$IdentifyDialogObject;->getAttribute()Landroid/os/Bundle;

    move-result-object v6

    invoke-virtual {v0}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService$IdentifyDialogObject;->getRecord()Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

    move-result-object v7

    # invokes: Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->handleshowIdentifyDialog(Landroid/os/Bundle;Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;)V
    invoke-static {v5, v6, v7}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->access$900(Lcom/samsung/android/fingerprint/service/FingerprintManagerService;Landroid/os/Bundle;Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;)V

    goto :goto_0

    .line 272
    .end local v0    # "Object":Lcom/samsung/android/fingerprint/service/FingerprintManagerService$IdentifyDialogObject;
    :sswitch_7
    iget v5, p1, Landroid/os/Message;->arg2:I

    if-nez v5, :cond_1

    .line 273
    new-instance v4, Landroid/content/Intent;

    const-string v5, "com.samsung.android.intent.action.FINGERPRINT_RESET"

    invoke-direct {v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 274
    .local v4, "intentReset":Landroid/content/Intent;
    iget-object v5, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService$H;->this$0:Lcom/samsung/android/fingerprint/service/FingerprintManagerService;

    # getter for: Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->access$1000(Lcom/samsung/android/fingerprint/service/FingerprintManagerService;)Landroid/content/Context;

    move-result-object v5

    sget-object v6, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    invoke-virtual {v5, v4, v6}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    .line 275
    iget-object v5, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService$H;->this$0:Lcom/samsung/android/fingerprint/service/FingerprintManagerService;

    const-string v6, "FPRM"

    const-string v7, "ALL"

    # invokes: Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->insertSurveyLog(Ljava/lang/String;Ljava/lang/String;J)V
    invoke-static {v5, v6, v7, v8, v9}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->access$1100(Lcom/samsung/android/fingerprint/service/FingerprintManagerService;Ljava/lang/String;Ljava/lang/String;J)V

    .line 277
    .end local v4    # "intentReset":Landroid/content/Intent;
    :cond_1
    iget v5, p1, Landroid/os/Message;->arg1:I

    const/4 v6, -0x1

    if-eq v5, v6, :cond_0

    .line 278
    new-instance v3, Landroid/content/Intent;

    const-string v5, "com.samsung.android.intent.action.FINGERPRINT_REMOVED"

    invoke-direct {v3, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 279
    .local v3, "intentRemoved":Landroid/content/Intent;
    const-string v5, "fingerIndex"

    iget v6, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v3, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 280
    iget-object v5, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService$H;->this$0:Lcom/samsung/android/fingerprint/service/FingerprintManagerService;

    # getter for: Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->access$1000(Lcom/samsung/android/fingerprint/service/FingerprintManagerService;)Landroid/content/Context;

    move-result-object v5

    sget-object v6, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    invoke-virtual {v5, v3, v6}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    .line 281
    iget-object v5, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService$H;->this$0:Lcom/samsung/android/fingerprint/service/FingerprintManagerService;

    const-string v6, "FPRM"

    iget v7, p1, Landroid/os/Message;->arg1:I

    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    # invokes: Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->insertSurveyLog(Ljava/lang/String;Ljava/lang/String;J)V
    invoke-static {v5, v6, v7, v8, v9}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->access$1100(Lcom/samsung/android/fingerprint/service/FingerprintManagerService;Ljava/lang/String;Ljava/lang/String;J)V

    goto/16 :goto_0

    .line 286
    .end local v3    # "intentRemoved":Landroid/content/Intent;
    :sswitch_8
    new-instance v2, Landroid/content/Intent;

    const-string v5, "com.samsung.android.intent.action.FINGERPRINT_ADDED"

    invoke-direct {v2, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 287
    .local v2, "intent":Landroid/content/Intent;
    iget v1, p1, Landroid/os/Message;->arg1:I

    .line 288
    .local v1, "fingerIdx":I
    if-le v1, v10, :cond_2

    .line 289
    add-int/lit8 v1, v1, -0xa

    .line 291
    :cond_2
    const-string v5, "fingerIndex"

    invoke-virtual {v2, v5, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 292
    iget v5, p1, Landroid/os/Message;->arg2:I

    const/4 v6, 0x1

    if-ne v5, v6, :cond_4

    .line 293
    const-string v5, "verificationType"

    const-string v6, "fingerprint"

    invoke-virtual {v2, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 297
    :cond_3
    :goto_1
    iget-object v5, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService$H;->this$0:Lcom/samsung/android/fingerprint/service/FingerprintManagerService;

    # getter for: Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->access$1000(Lcom/samsung/android/fingerprint/service/FingerprintManagerService;)Landroid/content/Context;

    move-result-object v5

    sget-object v6, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    invoke-virtual {v5, v2, v6}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    .line 298
    iget-object v5, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService$H;->this$0:Lcom/samsung/android/fingerprint/service/FingerprintManagerService;

    const-string v6, "FPEN"

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    # invokes: Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->insertSurveyLog(Ljava/lang/String;Ljava/lang/String;J)V
    invoke-static {v5, v6, v7, v8, v9}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->access$1100(Lcom/samsung/android/fingerprint/service/FingerprintManagerService;Ljava/lang/String;Ljava/lang/String;J)V

    .line 299
    # getter for: Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mEnrolledFingerCount:I
    invoke-static {}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->access$1200()I

    move-result v5

    if-nez v5, :cond_0

    iget v5, p1, Landroid/os/Message;->arg1:I

    if-le v5, v10, :cond_0

    .line 300
    iget-object v5, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService$H;->this$0:Lcom/samsung/android/fingerprint/service/FingerprintManagerService;

    const-string v6, "FPEN"

    iget v7, p1, Landroid/os/Message;->arg1:I

    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    # invokes: Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->insertSurveyLog(Ljava/lang/String;Ljava/lang/String;J)V
    invoke-static {v5, v6, v7, v8, v9}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->access$1100(Lcom/samsung/android/fingerprint/service/FingerprintManagerService;Ljava/lang/String;Ljava/lang/String;J)V

    goto/16 :goto_0

    .line 294
    :cond_4
    iget v5, p1, Landroid/os/Message;->arg2:I

    const/4 v6, 0x2

    if-ne v5, v6, :cond_3

    .line 295
    const-string v5, "verificationType"

    const-string v6, "password"

    invoke-virtual {v2, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_1

    .line 306
    .end local v1    # "fingerIdx":I
    .end local v2    # "intent":Landroid/content/Intent;
    :sswitch_9
    new-instance v2, Landroid/content/Intent;

    const-string v5, "com.samsung.android.intent.action.FINGERPRINT_PASSWORD_UPDATED"

    invoke-direct {v2, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 307
    .restart local v2    # "intent":Landroid/content/Intent;
    iget-object v5, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService$H;->this$0:Lcom/samsung/android/fingerprint/service/FingerprintManagerService;

    # getter for: Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->access$1000(Lcom/samsung/android/fingerprint/service/FingerprintManagerService;)Landroid/content/Context;

    move-result-object v5

    sget-object v6, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    invoke-virtual {v5, v2, v6}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    goto/16 :goto_0

    .line 311
    .end local v2    # "intent":Landroid/content/Intent;
    :sswitch_a
    iget-object v5, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService$H;->this$0:Lcom/samsung/android/fingerprint/service/FingerprintManagerService;

    iget v6, p1, Landroid/os/Message;->arg1:I

    # invokes: Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->removeEnrolledUser(I)Z
    invoke-static {v5, v6}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->access$1300(Lcom/samsung/android/fingerprint/service/FingerprintManagerService;I)Z

    goto/16 :goto_0

    .line 314
    :sswitch_b
    iget-object v6, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService$H;->this$0:Lcom/samsung/android/fingerprint/service/FingerprintManagerService;

    iget-object v5, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v5, Ljava/lang/String;

    # invokes: Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->removeEnrolledApp(Ljava/lang/String;)Z
    invoke-static {v6, v5}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->access$1400(Lcom/samsung/android/fingerprint/service/FingerprintManagerService;Ljava/lang/String;)Z

    goto/16 :goto_0

    .line 236
    :sswitch_data_0
    .sparse-switch
        0x200 -> :sswitch_2
        0x208 -> :sswitch_0
        0x209 -> :sswitch_1
        0x20a -> :sswitch_3
        0x226 -> :sswitch_4
        0x230 -> :sswitch_5
        0x264 -> :sswitch_6
        0x2c8 -> :sswitch_7
        0x2c9 -> :sswitch_8
        0x2ca -> :sswitch_9
        0x320 -> :sswitch_a
        0x321 -> :sswitch_b
    .end sparse-switch
.end method
