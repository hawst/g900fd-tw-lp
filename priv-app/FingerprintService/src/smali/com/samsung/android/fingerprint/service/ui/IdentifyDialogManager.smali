.class public Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;
.super Ljava/lang/Object;
.source "IdentifyDialogManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager$H;
    }
.end annotation


# static fields
.field public static final DIA_TYPE_BACKUP:I = 0x2

.field public static final DIA_TYPE_FINGERPINT:I = 0x1

.field public static final LOCK_BIOMETRIC_BACKUP_TYPE_PASSWORD:I = 0x1

.field public static final LOCK_BIOMETRIC_BACKUP_TYPE_PATTERN:I = 0x3

.field public static final LOCK_BIOMETRIC_BACKUP_TYPE_PIN:I = 0x2

.field private static final TAG:Ljava/lang/String; = "IdentifyDialogManager"

.field public static final VIEW_DEVICE_DEFAULT_DARK:I = 0x1

.field public static final VIEW_DEVICE_DEFAULT_LIGHT:I = 0x2


# instance fields
.field private mCallerBundle:Landroid/os/Bundle;

.field private mCallerClient:Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

.field private mContext:Landroid/content/Context;

.field private mDialogClient:Lcom/samsung/android/fingerprint/IFingerprintClient;

.field private mFpmsService:Lcom/samsung/android/fingerprint/service/FingerprintManagerService;

.field private mH:Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager$H;

.field private mIdentifyDialog:Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog;

.field private mIsSwitch:Z

.field private mSecurityLevel:I

.field private mToken:Landroid/os/IBinder;


# direct methods
.method public constructor <init>(Lcom/samsung/android/fingerprint/service/FingerprintManagerService;Landroid/content/Context;)V
    .locals 2
    .param p1, "service"    # Lcom/samsung/android/fingerprint/service/FingerprintManagerService;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 91
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    iput-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;->mFpmsService:Lcom/samsung/android/fingerprint/service/FingerprintManagerService;

    .line 51
    iput-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;->mCallerClient:Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

    .line 52
    iput-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;->mCallerBundle:Landroid/os/Bundle;

    .line 54
    iput-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;->mIdentifyDialog:Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog;

    .line 56
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;->mIsSwitch:Z

    .line 58
    iput-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;->mToken:Landroid/os/IBinder;

    .line 59
    const/4 v0, 0x1

    iput v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;->mSecurityLevel:I

    .line 60
    new-instance v0, Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager$1;-><init>(Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;)V

    iput-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;->mDialogClient:Lcom/samsung/android/fingerprint/IFingerprintClient;

    .line 74
    new-instance v0, Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager$H;

    invoke-direct {v0, p0, v1}, Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager$H;-><init>(Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager$1;)V

    iput-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;->mH:Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager$H;

    .line 92
    iput-object p1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;->mFpmsService:Lcom/samsung/android/fingerprint/service/FingerprintManagerService;

    .line 93
    iput-object p2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;->mContext:Landroid/content/Context;

    .line 94
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;)Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;->mIdentifyDialog:Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog;

    return-object v0
.end method

.method static synthetic access$002(Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog;)Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;
    .param p1, "x1"    # Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog;

    .prologue
    .line 37
    iput-object p1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;->mIdentifyDialog:Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog;

    return-object p1
.end method

.method static synthetic access$200(Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;

    .prologue
    .line 37
    iget-boolean v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;->mIsSwitch:Z

    return v0
.end method

.method static synthetic access$202(Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;
    .param p1, "x1"    # Z

    .prologue
    .line 37
    iput-boolean p1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;->mIsSwitch:Z

    return p1
.end method

.method private registerDialogClient()V
    .locals 6

    .prologue
    .line 110
    const-string v1, "IdentifyDialogManager"

    const-string v2, "registerDialogClient: called"

    invoke-static {v1, v2}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;->mCallerClient:Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

    invoke-virtual {v1}, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->getClientSpec()Landroid/os/Bundle;

    move-result-object v0

    .line 113
    .local v0, "clientSpec":Landroid/os/Bundle;
    if-nez v0, :cond_0

    .line 114
    new-instance v0, Landroid/os/Bundle;

    .end local v0    # "clientSpec":Landroid/os/Bundle;
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 115
    .restart local v0    # "clientSpec":Landroid/os/Bundle;
    const-string v1, "appName"

    const-string v2, "Identify Dialog client"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 116
    const-string v1, "securityLevel"

    iget v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;->mSecurityLevel:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 118
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;->mFpmsService:Lcom/samsung/android/fingerprint/service/FingerprintManagerService;

    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;->mDialogClient:Lcom/samsung/android/fingerprint/IFingerprintClient;

    invoke-virtual {v1, v2, v0}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->registerClient(Lcom/samsung/android/fingerprint/IFingerprintClient;Landroid/os/Bundle;)Landroid/os/IBinder;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;->mToken:Landroid/os/IBinder;

    .line 119
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;->mToken:Landroid/os/IBinder;

    if-nez v1, :cond_1

    .line 120
    const-string v1, "IdentifyDialogManager"

    const-string v2, "registerDialogClient: Token is null"

    invoke-static {v1, v2}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 129
    :goto_0
    return-void

    .line 123
    :cond_1
    invoke-static {}, Lcom/samsung/android/fingerprint/service/util/Config;->getOverallFailedFingerprintAttempts()I

    move-result v1

    const/4 v2, 0x4

    if-lt v1, v2, :cond_2

    invoke-static {}, Lcom/samsung/android/fingerprint/service/util/Config;->getRemaingTimeToUnlock()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    cmp-long v1, v2, v4

    if-gez v1, :cond_2

    .line 125
    const/4 v1, 0x2

    invoke-virtual {p0, v1}, Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;->createIdentifyDialog(I)V

    goto :goto_0

    .line 127
    :cond_2
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;->createIdentifyDialog(I)V

    goto :goto_0
.end method

.method private unregisterDialogClient()V
    .locals 2

    .prologue
    .line 132
    const-string v0, "IdentifyDialogManager"

    const-string v1, "unregisterDialogClient: called"

    invoke-static {v0, v1}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 133
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;->mFpmsService:Lcom/samsung/android/fingerprint/service/FingerprintManagerService;

    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;->mToken:Landroid/os/IBinder;

    invoke-virtual {v0, v1}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->unregisterClient(Landroid/os/IBinder;)Z

    .line 134
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;->mToken:Landroid/os/IBinder;

    .line 135
    return-void
.end method


# virtual methods
.method public closeDialog()V
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;->mIdentifyDialog:Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog;

    if-eqz v0, :cond_0

    .line 145
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;->mIdentifyDialog:Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog;

    invoke-virtual {v0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog;->dismiss()V

    .line 147
    :cond_0
    return-void
.end method

.method public createIdentifyDialog(I)V
    .locals 5
    .param p1, "type"    # I

    .prologue
    .line 150
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    .line 151
    new-instance v0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;

    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;->mFpmsService:Lcom/samsung/android/fingerprint/service/FingerprintManagerService;

    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;->mContext:Landroid/content/Context;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;->mCallerBundle:Landroid/os/Bundle;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;-><init>(Lcom/samsung/android/fingerprint/service/FingerprintManagerService;Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)V

    iput-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;->mIdentifyDialog:Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog;

    .line 152
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;->mIdentifyDialog:Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog;

    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;->mToken:Landroid/os/IBinder;

    invoke-virtual {v0, v1}, Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog;->setToken(Landroid/os/IBinder;)V

    .line 153
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;->mIdentifyDialog:Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog;

    new-instance v1, Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager$2;

    invoke-direct {v1, p0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager$2;-><init>(Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;)V

    invoke-virtual {v0, v1}, Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 180
    :cond_0
    :goto_0
    return-void

    .line 165
    :cond_1
    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    .line 166
    new-instance v0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;

    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;->mFpmsService:Lcom/samsung/android/fingerprint/service/FingerprintManagerService;

    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;-><init>(Lcom/samsung/android/fingerprint/service/FingerprintManagerService;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;->mIdentifyDialog:Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog;

    .line 167
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;->mIdentifyDialog:Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog;

    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;->mToken:Landroid/os/IBinder;

    invoke-virtual {v0, v1}, Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog;->setToken(Landroid/os/IBinder;)V

    .line 168
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;->mIdentifyDialog:Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog;

    new-instance v1, Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager$3;

    invoke-direct {v1, p0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager$3;-><init>(Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;)V

    invoke-virtual {v0, v1}, Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    goto :goto_0
.end method

.method public deliverIdentifyEventToClient(Lcom/samsung/android/fingerprint/FingerprintEvent;)V
    .locals 2
    .param p1, "event"    # Lcom/samsung/android/fingerprint/FingerprintEvent;

    .prologue
    .line 191
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;->mCallerClient:Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

    if-eqz v0, :cond_0

    .line 192
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;->mCallerClient:Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p1}, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->onEvent(Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;Lcom/samsung/android/fingerprint/FingerprintEvent;)V

    .line 194
    :cond_0
    return-void
.end method

.method public getCallerClient()Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;
    .locals 1

    .prologue
    .line 197
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;->mCallerClient:Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

    return-object v0
.end method

.method public setClientRecord(Landroid/os/Bundle;Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;)V
    .locals 2
    .param p1, "bundle"    # Landroid/os/Bundle;
    .param p2, "clientRecord"    # Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

    .prologue
    .line 97
    if-nez p2, :cond_0

    .line 98
    const-string v0, "IdentifyDialogManager"

    const-string v1, "setClientRecord: clientRecord is null"

    invoke-static {v0, v1}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 107
    :goto_0
    return-void

    .line 101
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;->mToken:Landroid/os/IBinder;

    if-eqz v0, :cond_1

    .line 102
    invoke-direct {p0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;->unregisterDialogClient()V

    .line 104
    :cond_1
    iput-object p1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;->mCallerBundle:Landroid/os/Bundle;

    .line 105
    iput-object p2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;->mCallerClient:Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

    .line 106
    invoke-direct {p0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;->registerDialogClient()V

    goto :goto_0
.end method

.method public showDialog()V
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;->mIdentifyDialog:Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog;

    if-eqz v0, :cond_0

    .line 139
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;->mIdentifyDialog:Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog;

    invoke-virtual {v0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog;->show()V

    .line 141
    :cond_0
    return-void
.end method

.method public switchDialog(I)V
    .locals 3
    .param p1, "type"    # I

    .prologue
    .line 183
    const-string v0, "IdentifyDialogManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "switchDialog called: type = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 184
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;->mIdentifyDialog:Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog;

    if-eqz v0, :cond_0

    .line 185
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;->mH:Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager$H;

    const/16 v1, 0x208

    const/4 v2, 0x0

    invoke-static {v0, v1, p1, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 186
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;->mIsSwitch:Z

    .line 188
    :cond_0
    return-void
.end method
