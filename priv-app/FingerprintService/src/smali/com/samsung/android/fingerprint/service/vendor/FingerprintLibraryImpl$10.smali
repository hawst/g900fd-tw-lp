.class Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$10;
.super Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$ValiditySyncCommand;
.source "FingerprintLibraryImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;


# direct methods
.method constructor <init>(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p2, "x0"    # Ljava/lang/String;
    .param p3, "x1"    # Ljava/lang/String;

    .prologue
    .line 1305
    iput-object p1, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$10;->this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$ValiditySyncCommand;-><init>(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public doCommand()Ljava/lang/Object;
    .locals 5

    .prologue
    .line 1308
    const-string v2, "ValidityFingerprintLibraryImpl"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "removeAllEnrolledFingers: userId="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$10;->stringUserId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", fingerIndex=FINGER_ALL"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1309
    const/16 v0, 0x1fe

    .line 1312
    .local v0, "result":I
    const/4 v1, 0x0

    .line 1313
    .local v1, "tempTicket":Z
    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$10;->stringUserId:Ljava/lang/String;

    const-string v3, "FPMS_"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1314
    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$10;->stringUserId:Ljava/lang/String;

    const/4 v3, 0x5

    invoke-virtual {v2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$10;->stringUserId:Ljava/lang/String;

    .line 1315
    const/4 v1, 0x1

    .line 1318
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$10;->this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    # getter for: Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mIsVerifiedInEnrollSession:Z
    invoke-static {v2}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->access$1100(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;)Z

    move-result v2

    if-nez v2, :cond_1

    if-eqz v1, :cond_4

    .line 1320
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$10;->this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    # getter for: Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mFingerprint:Lcom/samsung/android/fingerprint/service/vendor/FingerprintProxy;
    invoke-static {v2}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->access$100(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;)Lcom/samsung/android/fingerprint/service/vendor/FingerprintProxy;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$10;->stringUserId:Ljava/lang/String;

    const/16 v4, 0x15

    invoke-interface {v2, v3, v4}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintProxy;->removeEnrolledFinger(Ljava/lang/String;I)I

    move-result v0

    .line 1322
    if-eqz v0, :cond_2

    const/16 v2, 0x41

    if-ne v0, v2, :cond_3

    .line 1324
    :cond_2
    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$10;->this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    # getter for: Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mDataManager:Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;
    invoke-static {v2}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->access$2100(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;)Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$10;->stringUserId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->removeAllEnrolledFingers(Ljava/lang/String;)I

    .line 1331
    :cond_3
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$10;->notifyCommandCompleted()V

    .line 1333
    invoke-static {v0}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintValues$Validity2Generic;->getResult(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    return-object v2

    .line 1328
    :cond_4
    const-string v2, "ValidityFingerprintLibraryImpl"

    const-string v3, "Not verified in Enroll Session"

    invoke-static {v2, v3}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
