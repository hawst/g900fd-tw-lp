.class Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog$7;
.super Landroid/os/CountDownTimer;
.source "IdentifyFingerprintDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->handleLockoutFingerprint()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;


# direct methods
.method constructor <init>(Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;JJ)V
    .locals 0
    .param p2, "x0"    # J
    .param p4, "x1"    # J

    .prologue
    .line 819
    iput-object p1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog$7;->this$0:Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;

    invoke-direct {p0, p2, p3, p4, p5}, Landroid/os/CountDownTimer;-><init>(JJ)V

    return-void
.end method


# virtual methods
.method public onFinish()V
    .locals 2

    .prologue
    .line 837
    const-string v0, "IdentifyFingerprintDialog"

    const-string v1, "CountdownTimer: onFinish"

    invoke-static {v0, v1}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 839
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog$7;->this$0:Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;

    iget-object v0, v0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mCountdownTimer:Landroid/os/CountDownTimer;

    if-eqz v0, :cond_0

    .line 840
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog$7;->this$0:Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;

    iget-object v0, v0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mCountdownTimer:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->cancel()V

    .line 841
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog$7;->this$0:Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mCountdownTimer:Landroid/os/CountDownTimer;

    .line 844
    :cond_0
    invoke-static {}, Lcom/samsung/android/fingerprint/service/util/Config;->getOverallFailedFingerprintAttempts()I

    move-result v0

    const/4 v1, 0x4

    if-lt v0, v1, :cond_1

    .line 846
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog$7;->this$0:Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;

    # invokes: Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->showBackupDialog()V
    invoke-static {v0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->access$100(Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;)V

    .line 850
    :goto_0
    return-void

    .line 848
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog$7;->this$0:Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;

    sget-object v1, Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog$Stage;->UnlockFingerprint:Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog$Stage;

    # invokes: Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->updateStage(Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog$Stage;)V
    invoke-static {v0, v1}, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->access$300(Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog$Stage;)V

    goto :goto_0
.end method

.method public onTick(J)V
    .locals 9
    .param p1, "millisUntilFinished"    # J

    .prologue
    const/4 v6, 0x1

    .line 822
    const-wide/16 v4, 0x3e8

    div-long v4, p1, v4

    long-to-int v0, v4

    .line 823
    .local v0, "secondsCountdown":I
    iget-object v3, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog$7;->this$0:Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;

    iget-object v3, v3, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mInfoText:Landroid/widget/TextView;

    if-eqz v3, :cond_0

    .line 824
    const/4 v2, 0x0

    .line 825
    .local v2, "string2":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog$7;->this$0:Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;

    iget-object v4, v4, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mContext:Landroid/content/Context;

    const v5, 0x7f060015

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "line.separator"

    invoke-static {v4}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 826
    .local v1, "string1":Ljava/lang/String;
    if-ne v0, v6, :cond_1

    .line 827
    iget-object v3, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog$7;->this$0:Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;

    iget-object v3, v3, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mContext:Landroid/content/Context;

    const v4, 0x7f060014

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 831
    :goto_0
    iget-object v3, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog$7;->this$0:Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;

    iget-object v3, v3, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mInfoText:Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 833
    .end local v1    # "string1":Ljava/lang/String;
    .end local v2    # "string2":Ljava/lang/String;
    :cond_0
    return-void

    .line 829
    .restart local v1    # "string1":Ljava/lang/String;
    .restart local v2    # "string2":Ljava/lang/String;
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog$7;->this$0:Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;

    iget-object v3, v3, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mContext:Landroid/content/Context;

    const v4, 0x7f060013

    new-array v5, v6, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method
