.class Lcom/samsung/android/fingerprint/service/FingerprintManagerService$SensorReadyCheckScheduler;
.super Ljava/lang/Object;
.source "FingerprintManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/fingerprint/service/FingerprintManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SensorReadyCheckScheduler"
.end annotation


# static fields
.field private static final DELAY_TIME_LIMIT:I = 0x493e0

.field private static final INITIAL_DELAY_TIME:I = 0x3e8


# instance fields
.field private mDelayTimeTable:[[I

.field private mFailedCount:I

.field private mNextDelayTime:I


# direct methods
.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x2

    .line 2222
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2226
    iput v2, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService$SensorReadyCheckScheduler;->mFailedCount:I

    .line 2227
    const/16 v0, 0x3e8

    iput v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService$SensorReadyCheckScheduler;->mNextDelayTime:I

    .line 2240
    const/4 v0, 0x6

    new-array v0, v0, [[I

    new-array v1, v3, [I

    fill-array-data v1, :array_0

    aput-object v1, v0, v2

    const/4 v1, 0x1

    new-array v2, v3, [I

    fill-array-data v2, :array_1

    aput-object v2, v0, v1

    new-array v1, v3, [I

    fill-array-data v1, :array_2

    aput-object v1, v0, v3

    const/4 v1, 0x3

    new-array v2, v3, [I

    fill-array-data v2, :array_3

    aput-object v2, v0, v1

    const/4 v1, 0x4

    new-array v2, v3, [I

    fill-array-data v2, :array_4

    aput-object v2, v0, v1

    const/4 v1, 0x5

    new-array v2, v3, [I

    fill-array-data v2, :array_5

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService$SensorReadyCheckScheduler;->mDelayTimeTable:[[I

    return-void

    nop

    :array_0
    .array-data 4
        0x5
        0x3e8
    .end array-data

    :array_1
    .array-data 4
        0xa
        0xbb8
    .end array-data

    :array_2
    .array-data 4
        0xc
        0x2710
    .end array-data

    :array_3
    .array-data 4
        0xd
        0x7530
    .end array-data

    :array_4
    .array-data 4
        0xe
        0x15f90
    .end array-data

    :array_5
    .array-data 4
        0xf
        0x2bf20
    .end array-data
.end method

.method synthetic constructor <init>(Lcom/samsung/android/fingerprint/service/FingerprintManagerService$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/android/fingerprint/service/FingerprintManagerService$1;

    .prologue
    .line 2222
    invoke-direct {p0}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService$SensorReadyCheckScheduler;-><init>()V

    return-void
.end method


# virtual methods
.method getFailedCount()I
    .locals 1

    .prologue
    .line 2269
    iget v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService$SensorReadyCheckScheduler;->mFailedCount:I

    return v0
.end method

.method getNextDelayTime()I
    .locals 3

    .prologue
    .line 2263
    const-string v0, "FingerprintManagerService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SensorReadyCheckScheduler: getNextDelayTime()="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService$SensorReadyCheckScheduler;->mNextDelayTime:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mFailedCount="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService$SensorReadyCheckScheduler;->mFailedCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2265
    iget v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService$SensorReadyCheckScheduler;->mNextDelayTime:I

    return v0
.end method

.method notifyFailed()V
    .locals 6

    .prologue
    .line 2250
    const v4, 0x493e0

    iput v4, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService$SensorReadyCheckScheduler;->mNextDelayTime:I

    .line 2252
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService$SensorReadyCheckScheduler;->mDelayTimeTable:[[I

    .local v0, "arr$":[[I
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v1, v0, v2

    .line 2253
    .local v1, "entry":[I
    iget v4, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService$SensorReadyCheckScheduler;->mFailedCount:I

    const/4 v5, 0x0

    aget v5, v1, v5

    if-ge v4, v5, :cond_1

    .line 2254
    const/4 v4, 0x1

    aget v4, v1, v4

    iput v4, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService$SensorReadyCheckScheduler;->mNextDelayTime:I

    .line 2259
    .end local v1    # "entry":[I
    :cond_0
    iget v4, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService$SensorReadyCheckScheduler;->mFailedCount:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService$SensorReadyCheckScheduler;->mFailedCount:I

    .line 2260
    return-void

    .line 2252
    .restart local v1    # "entry":[I
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method
