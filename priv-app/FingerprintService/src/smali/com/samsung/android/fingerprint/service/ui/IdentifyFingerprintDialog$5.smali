.class Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog$5;
.super Ljava/lang/Object;
.source "IdentifyFingerprintDialog.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->initLayout()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;


# direct methods
.method constructor <init>(Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;)V
    .locals 0

    .prologue
    .line 706
    iput-object p1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog$5;->this$0:Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "arg0"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 709
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog$5;->this$0:Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;

    iget-object v0, v0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mUnlockButton:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 710
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_1

    .line 711
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog$5;->this$0:Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;

    iget-object v0, v0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mUnlockButton:Landroid/widget/ImageView;

    const v1, 0x7f020021

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 720
    :cond_0
    :goto_0
    const/4 v0, 0x0

    return v0

    .line 712
    :cond_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 713
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog$5;->this$0:Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;

    iget v0, v0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mDeviceTheme:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 714
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog$5;->this$0:Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;

    iget-object v0, v0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mUnlockButton:Landroid/widget/ImageView;

    const v1, 0x7f020020

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 716
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog$5;->this$0:Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;

    iget-object v0, v0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mUnlockButton:Landroid/widget/ImageView;

    const v1, 0x7f020028

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0
.end method
