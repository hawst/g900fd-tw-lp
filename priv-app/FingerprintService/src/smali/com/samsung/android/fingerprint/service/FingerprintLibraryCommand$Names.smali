.class public Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand$Names;
.super Ljava/lang/Object;
.source "FingerprintLibraryCommand.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Names"
.end annotation


# static fields
.field public static final ADD_PERMISSION:Ljava/lang/String; = "addPermission"

.field public static final CANCEL:Ljava/lang/String; = "cancel"

.field public static final CLEANUP:Ljava/lang/String; = "cleanUp"

.field public static final ENROLL:Ljava/lang/String; = "enroll"

.field public static final GET_ENROLLED_FINGERS:Ljava/lang/String; = "getEnrolledFingers"

.field public static final GET_ENROLLED_PERMISSIONS:Ljava/lang/String; = "getEnrolledPermissions"

.field public static final GET_ENROLLED_USERS:Ljava/lang/String; = "getEnrolledUsers"

.field public static final GET_ENROLL_REPEAT_COUNT:Ljava/lang/String; = "getEnrollRepeatCount"

.field public static final GET_FINGERPRINT_IDS:Ljava/lang/String; = "getFingerprintIds"

.field public static final GET_FINGERPRINT_ID_BY_FINGER:Ljava/lang/String; = "getFingerprintIdByFinger"

.field public static final GET_SENSOR_INFO:Ljava/lang/String; = "getSensorInfo"

.field public static final IDENTIFY:Ljava/lang/String; = "identify"

.field public static final IS_ENROLLED_PERMISSION:Ljava/lang/String; = "isEnrolledPermission"

.field public static final LOCK:Ljava/lang/String; = "lock"

.field public static final NOTIFY:Ljava/lang/String; = "notify"

.field public static final PROCESS_FIDO:Ljava/lang/String; = "processFIDO"

.field public static final PROTECT:Ljava/lang/String; = "protect"

.field public static final REMOVE_ALL_ENROLLED_FINGERS:Ljava/lang/String; = "removeAllEnrolledFingers"

.field public static final REMOVE_ENROLLED_FINGER:Ljava/lang/String; = "removeEnrolledFinger"

.field public static final REMOVE_ENROLLED_PERMISSION:Ljava/lang/String; = "removeEnrolledPermission"

.field public static final REMOVE_ENROLLED_PERMISSIONS:Ljava/lang/String; = "removeEnrolledPermissions"

.field public static final SET_PASSWORD:Ljava/lang/String; = "setPassword"

.field public static final UNPROTECT:Ljava/lang/String; = "unProtect"

.field public static final VERIFY_PASSWORD:Ljava/lang/String; = "verifyPassword"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 474
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
