.class Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2$1;
.super Ljava/lang/Object;
.source "FingerprintLibraryImpl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->doCommand()Ljava/lang/Object;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;


# direct methods
.method constructor <init>(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;)V
    .locals 0

    .prologue
    .line 710
    iput-object p1, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2$1;->this$1:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 713
    const-string v0, "ValidityFingerprintLibraryImpl"

    const-string v1, "identify: timed-out [20000ms]"

    invoke-static {v0, v1}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 715
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2$1;->this$1:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;

    # setter for: Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->shouldDropEvents:Z
    invoke-static {v0, v2}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->access$702(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;Z)Z

    .line 716
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2$1;->this$1:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;

    # setter for: Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->isResulted:Z
    invoke-static {v0, v2}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->access$802(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;Z)Z

    .line 718
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2$1;->this$1:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;

    # invokes: Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->timedOut()V
    invoke-static {v0}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->access$900(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;)V

    .line 719
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2$1;->this$1:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;

    invoke-virtual {v0}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->requestCancel()Z

    .line 720
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2$1;->this$1:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;

    invoke-virtual {v0}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->notifyCommandCompleted()V

    .line 721
    return-void
.end method
