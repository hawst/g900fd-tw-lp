.class Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog$H;
.super Landroid/os/Handler;
.source "IdentifyBackupDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "H"
.end annotation


# static fields
.field private static final MSG_VERIFY_SEND:I = 0x291c


# instance fields
.field final synthetic this$0:Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;


# direct methods
.method public constructor <init>(Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;Landroid/os/Looper;)V
    .locals 0
    .param p2, "looper"    # Landroid/os/Looper;

    .prologue
    .line 110
    iput-object p1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog$H;->this$0:Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;

    .line 111
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 112
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 116
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 126
    const-string v0, "IdentifyBackupDialog"

    const-string v1, "There is no matched handle case"

    invoke-static {v0, v1}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 129
    :goto_0
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 130
    return-void

    .line 119
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog$H;->this$0:Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;

    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog$H;->this$0:Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;

    # getter for: Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mEvent:Lcom/samsung/android/fingerprint/FingerprintEvent;
    invoke-static {v1}, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->access$000(Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;)Lcom/samsung/android/fingerprint/FingerprintEvent;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->deliverEventToClient(Lcom/samsung/android/fingerprint/FingerprintEvent;)V

    .line 120
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog$H;->this$0:Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;

    const/4 v1, 0x0

    # setter for: Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mIsPendingEvent:Z
    invoke-static {v0, v1}, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->access$102(Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;Z)Z

    .line 122
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog$H;->this$0:Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;

    invoke-virtual {v0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->dismiss()V

    goto :goto_0

    .line 116
    nop

    :pswitch_data_0
    .packed-switch 0x291c
        :pswitch_0
    .end packed-switch
.end method
