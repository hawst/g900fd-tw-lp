.class Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager$3;
.super Ljava/lang/Object;
.source "IdentifyDialogManager.java"

# interfaces
.implements Landroid/content/DialogInterface$OnDismissListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;->createIdentifyDialog(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;


# direct methods
.method constructor <init>(Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;)V
    .locals 0

    .prologue
    .line 168
    iput-object p1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager$3;->this$0:Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 2
    .param p1, "dialogInterface"    # Landroid/content/DialogInterface;

    .prologue
    .line 171
    const-string v0, "IdentifyDialogManager"

    const-string v1, "=== IdentifyBackupDialog Dialog : onDismiss ==="

    invoke-static {v0, v1}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 172
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager$3;->this$0:Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;

    # getter for: Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;->mIsSwitch:Z
    invoke-static {v0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;->access$200(Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 173
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager$3;->this$0:Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;

    const/4 v1, 0x0

    # setter for: Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;->mIsSwitch:Z
    invoke-static {v0, v1}, Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;->access$202(Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;Z)Z

    .line 177
    :goto_0
    return-void

    .line 175
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager$3;->this$0:Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;

    const/4 v1, 0x0

    # setter for: Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;->mIdentifyDialog:Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog;
    invoke-static {v0, v1}, Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;->access$002(Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog;)Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog;

    goto :goto_0
.end method
