.class Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$19;
.super Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$ValiditySyncCommand;
.source "FingerprintLibraryImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;


# direct methods
.method constructor <init>(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p2, "x0"    # Ljava/lang/String;
    .param p3, "x1"    # Ljava/lang/String;

    .prologue
    .line 1540
    iput-object p1, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$19;->this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$ValiditySyncCommand;-><init>(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public doCommand()Ljava/lang/Object;
    .locals 6

    .prologue
    .line 1543
    const-string v4, "ValidityFingerprintLibraryImpl"

    const-string v5, "unprotect: BEGIN"

    invoke-static {v4, v5}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1544
    const/4 v3, 0x0

    .line 1545
    .local v3, "returnObj":Ljava/lang/Object;
    new-instance v2, Lcom/validity/fingerprint/VcsByteArray;

    invoke-direct {v2}, Lcom/validity/fingerprint/VcsByteArray;-><init>()V

    .line 1546
    .local v2, "resultVcsByteArray":Lcom/validity/fingerprint/VcsByteArray;
    new-instance v0, Lcom/validity/fingerprint/VcsAddInfo;

    const/4 v4, 0x1

    invoke-direct {v0, v4}, Lcom/validity/fingerprint/VcsAddInfo;-><init>(I)V

    .line 1548
    .local v0, "addInfo":Lcom/validity/fingerprint/VcsAddInfo;
    iget-object v4, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$19;->this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    # getter for: Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mFingerprint:Lcom/samsung/android/fingerprint/service/vendor/FingerprintProxy;
    invoke-static {v4}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->access$100(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;)Lcom/samsung/android/fingerprint/service/vendor/FingerprintProxy;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$19;->inputByteData:[B

    invoke-interface {v4, v5, v0, v2}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintProxy;->unProtect([BLcom/validity/fingerprint/VcsAddInfo;Lcom/validity/fingerprint/VcsByteArray;)I

    move-result v1

    .line 1550
    .local v1, "result":I
    invoke-virtual {p0}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$19;->notifyCommandCompleted()V

    .line 1552
    if-eqz v2, :cond_0

    if-nez v1, :cond_0

    .line 1553
    iget-object v3, v2, Lcom/validity/fingerprint/VcsByteArray;->data:[B

    .line 1556
    .end local v3    # "returnObj":Ljava/lang/Object;
    :cond_0
    const-string v4, "ValidityFingerprintLibraryImpl"

    const-string v5, "unprotect: END"

    invoke-static {v4, v5}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1557
    return-object v3
.end method
