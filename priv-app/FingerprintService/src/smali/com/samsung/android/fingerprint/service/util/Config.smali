.class public Lcom/samsung/android/fingerprint/service/util/Config;
.super Ljava/lang/Object;
.source "Config.java"


# static fields
.field public static final ATTEMPT_OVERALL_MAX:I = 0x4

.field public static final DEFAULT_TRY_COUNT:I = 0x5

.field public static final FAILED_ATTEMPT_COUNTDOWN_INTERVAL_MS:J = 0x3e8L

.field public static final FAILED_ATTEMPT_TIMEOUT_MARGIN:J = 0x3e8L

.field public static final FAILED_ATTEMPT_TIMEOUT_MS:J = 0x7530L

.field public static final IS_DEVICE_WHITE_THEME:Z

.field public static final PREF_ATTEMPT_DEADLINE:Ljava/lang/String; = "pref_attempt_deadline"

.field public static final PREF_ATTEMPT_OVERALL_COUNT:Ljava/lang/String; = "pref_attempt_overall_count"

.field public static final PREF_ATTEMPT_REMAIN_COUNT:Ljava/lang/String; = "pref_attempt_remain_count"

.field public static final SENSOR_HW_ERROR:I = 0x3

.field public static final SENSOR_HW_RAPTOR:I = 0x2

.field public static final SENSOR_HW_VIPER:I = 0x1

.field private static final TAG:Ljava/lang/String; = "Config"

.field private static mFailedFingerprintAttempts:I

.field private static mOverallFailedFingerprintAttempts:I

.field private static mPrefrences:Landroid/content/SharedPreferences;

.field private static mRemaingTimeToUnlock:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 51
    const-string v0, "white"

    const-string v1, "ro.build.scafe.cream"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/samsung/android/fingerprint/service/util/Config;->IS_DEVICE_WHITE_THEME:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static declared-synchronized getFailedFingerprintAttempts()I
    .locals 4

    .prologue
    .line 73
    const-class v1, Lcom/samsung/android/fingerprint/service/util/Config;

    monitor-enter v1

    :try_start_0
    const-string v0, "Config"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getFailedFingerprintAttempts: return "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget v3, Lcom/samsung/android/fingerprint/service/util/Config;->mFailedFingerprintAttempts:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    sget v0, Lcom/samsung/android/fingerprint/service/util/Config;->mFailedFingerprintAttempts:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return v0

    .line 73
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized getOverallFailedFingerprintAttempts()I
    .locals 4

    .prologue
    .line 78
    const-class v1, Lcom/samsung/android/fingerprint/service/util/Config;

    monitor-enter v1

    :try_start_0
    const-string v0, "Config"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getOverallFailedFingerprintAttempts: return "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget v3, Lcom/samsung/android/fingerprint/service/util/Config;->mOverallFailedFingerprintAttempts:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    sget v0, Lcom/samsung/android/fingerprint/service/util/Config;->mOverallFailedFingerprintAttempts:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return v0

    .line 78
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized getRemaingTimeToUnlock()J
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    .line 83
    const-class v3, Lcom/samsung/android/fingerprint/service/util/Config;

    monitor-enter v3

    const-wide/16 v0, 0x0

    .line 84
    .local v0, "retTime":J
    :try_start_0
    sget-wide v4, Lcom/samsung/android/fingerprint/service/util/Config;->mRemaingTimeToUnlock:J

    cmp-long v2, v4, v8

    if-eqz v2, :cond_0

    .line 85
    sget-wide v4, Lcom/samsung/android/fingerprint/service/util/Config;->mRemaingTimeToUnlock:J

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    sub-long v0, v4, v6

    .line 86
    cmp-long v2, v0, v8

    if-gtz v2, :cond_1

    .line 87
    const-wide/16 v4, 0x0

    sput-wide v4, Lcom/samsung/android/fingerprint/service/util/Config;->mRemaingTimeToUnlock:J

    .line 88
    const-wide/16 v0, 0x0

    .line 94
    :cond_0
    :goto_0
    const-string v2, "Config"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getRemaingTimeToUnlock: return "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 95
    monitor-exit v3

    return-wide v0

    .line 89
    :cond_1
    const-wide/16 v4, 0x7530

    cmp-long v2, v0, v4

    if-lez v2, :cond_0

    .line 90
    :try_start_1
    invoke-static {}, Lcom/samsung/android/fingerprint/service/util/Config;->setRemaingTimeToUnlock()V

    .line 91
    sget-wide v0, Lcom/samsung/android/fingerprint/service/util/Config;->mRemaingTimeToUnlock:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 83
    :catchall_0
    move-exception v2

    monitor-exit v3

    throw v2
.end method

.method public static declared-synchronized initFailedFingerprintAttemptsFromPrefrence(Landroid/content/SharedPreferences;)V
    .locals 6
    .param p0, "pref"    # Landroid/content/SharedPreferences;

    .prologue
    .line 121
    const-class v1, Lcom/samsung/android/fingerprint/service/util/Config;

    monitor-enter v1

    if-eqz p0, :cond_1

    .line 122
    :try_start_0
    sput-object p0, Lcom/samsung/android/fingerprint/service/util/Config;->mPrefrences:Landroid/content/SharedPreferences;

    .line 123
    sget-object v0, Lcom/samsung/android/fingerprint/service/util/Config;->mPrefrences:Landroid/content/SharedPreferences;

    const-string v2, "pref_attempt_remain_count"

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/samsung/android/fingerprint/service/util/Config;->mFailedFingerprintAttempts:I

    .line 124
    sget-object v0, Lcom/samsung/android/fingerprint/service/util/Config;->mPrefrences:Landroid/content/SharedPreferences;

    const-string v2, "pref_attempt_overall_count"

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/samsung/android/fingerprint/service/util/Config;->mOverallFailedFingerprintAttempts:I

    .line 125
    sget-object v0, Lcom/samsung/android/fingerprint/service/util/Config;->mPrefrences:Landroid/content/SharedPreferences;

    const-string v2, "pref_attempt_deadline"

    const-wide/16 v4, 0x0

    invoke-interface {v0, v2, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    sput-wide v2, Lcom/samsung/android/fingerprint/service/util/Config;->mRemaingTimeToUnlock:J

    .line 126
    sget-wide v2, Lcom/samsung/android/fingerprint/service/util/Config;->mRemaingTimeToUnlock:J

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    sub-long/2addr v2, v4

    const-wide/16 v4, 0x7530

    cmp-long v0, v2, v4

    if-lez v0, :cond_0

    .line 127
    invoke-static {}, Lcom/samsung/android/fingerprint/service/util/Config;->setRemaingTimeToUnlock()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 132
    :cond_0
    :goto_0
    monitor-exit v1

    return-void

    .line 130
    :cond_1
    :try_start_1
    const-string v0, "Config"

    const-string v2, "SharedPreferences is null"

    invoke-static {v0, v2}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 121
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized reportFailedFingerprintAttempts()V
    .locals 6

    .prologue
    .line 54
    const-class v1, Lcom/samsung/android/fingerprint/service/util/Config;

    monitor-enter v1

    :try_start_0
    sget v0, Lcom/samsung/android/fingerprint/service/util/Config;->mFailedFingerprintAttempts:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/samsung/android/fingerprint/service/util/Config;->mFailedFingerprintAttempts:I

    .line 55
    sget v0, Lcom/samsung/android/fingerprint/service/util/Config;->mFailedFingerprintAttempts:I

    const/4 v2, 0x5

    if-lt v0, v2, :cond_1

    .line 56
    sget v0, Lcom/samsung/android/fingerprint/service/util/Config;->mOverallFailedFingerprintAttempts:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/samsung/android/fingerprint/service/util/Config;->mOverallFailedFingerprintAttempts:I

    .line 57
    const/4 v0, 0x0

    sput v0, Lcom/samsung/android/fingerprint/service/util/Config;->mFailedFingerprintAttempts:I

    .line 58
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    const-wide/16 v4, 0x7530

    add-long/2addr v2, v4

    sput-wide v2, Lcom/samsung/android/fingerprint/service/util/Config;->mRemaingTimeToUnlock:J

    .line 59
    sget-object v0, Lcom/samsung/android/fingerprint/service/util/Config;->mPrefrences:Landroid/content/SharedPreferences;

    if-eqz v0, :cond_0

    .line 60
    sget-object v0, Lcom/samsung/android/fingerprint/service/util/Config;->mPrefrences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "pref_attempt_deadline"

    sget-wide v4, Lcom/samsung/android/fingerprint/service/util/Config;->mRemaingTimeToUnlock:J

    invoke-interface {v0, v2, v4, v5}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "pref_attempt_remain_count"

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "pref_attempt_overall_count"

    sget v3, Lcom/samsung/android/fingerprint/service/util/Config;->mOverallFailedFingerprintAttempts:I

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 70
    :cond_0
    :goto_0
    monitor-exit v1

    return-void

    .line 66
    :cond_1
    :try_start_1
    sget-object v0, Lcom/samsung/android/fingerprint/service/util/Config;->mPrefrences:Landroid/content/SharedPreferences;

    if-eqz v0, :cond_0

    .line 67
    sget-object v0, Lcom/samsung/android/fingerprint/service/util/Config;->mPrefrences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "pref_attempt_remain_count"

    sget v3, Lcom/samsung/android/fingerprint/service/util/Config;->mFailedFingerprintAttempts:I

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 54
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized resetFailedFingerprintAttempts()V
    .locals 6

    .prologue
    .line 106
    const-class v1, Lcom/samsung/android/fingerprint/service/util/Config;

    monitor-enter v1

    const/4 v0, 0x0

    :try_start_0
    sput v0, Lcom/samsung/android/fingerprint/service/util/Config;->mFailedFingerprintAttempts:I

    .line 107
    const/4 v0, 0x0

    sput v0, Lcom/samsung/android/fingerprint/service/util/Config;->mOverallFailedFingerprintAttempts:I

    .line 108
    const-wide/16 v2, 0x0

    sput-wide v2, Lcom/samsung/android/fingerprint/service/util/Config;->mRemaingTimeToUnlock:J

    .line 109
    sget-object v0, Lcom/samsung/android/fingerprint/service/util/Config;->mPrefrences:Landroid/content/SharedPreferences;

    if-eqz v0, :cond_0

    .line 110
    sget-object v0, Lcom/samsung/android/fingerprint/service/util/Config;->mPrefrences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "pref_attempt_deadline"

    const-wide/16 v4, 0x0

    invoke-interface {v0, v2, v4, v5}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "pref_attempt_remain_count"

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "pref_attempt_overall_count"

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 118
    :goto_0
    monitor-exit v1

    return-void

    .line 116
    :cond_0
    :try_start_1
    const-string v0, "Config"

    const-string v2, "resetNoMatchCount: SharedPreferences is null"

    invoke-static {v0, v2}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 106
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized setRemaingTimeToUnlock()V
    .locals 6

    .prologue
    .line 99
    const-class v1, Lcom/samsung/android/fingerprint/service/util/Config;

    monitor-enter v1

    :try_start_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    const-wide/16 v4, 0x7530

    add-long/2addr v2, v4

    sput-wide v2, Lcom/samsung/android/fingerprint/service/util/Config;->mRemaingTimeToUnlock:J

    .line 100
    sget-object v0, Lcom/samsung/android/fingerprint/service/util/Config;->mPrefrences:Landroid/content/SharedPreferences;

    if-eqz v0, :cond_0

    .line 101
    sget-object v0, Lcom/samsung/android/fingerprint/service/util/Config;->mPrefrences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "pref_attempt_deadline"

    sget-wide v4, Lcom/samsung/android/fingerprint/service/util/Config;->mRemaingTimeToUnlock:J

    invoke-interface {v0, v2, v4, v5}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 103
    :cond_0
    monitor-exit v1

    return-void

    .line 99
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
