.class Lcom/samsung/android/fingerprint/service/FingerprintManagerService$2;
.super Landroid/content/BroadcastReceiver;
.source "FingerprintManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/fingerprint/service/FingerprintManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/fingerprint/service/FingerprintManagerService;


# direct methods
.method constructor <init>(Lcom/samsung/android/fingerprint/service/FingerprintManagerService;)V
    .locals 0

    .prologue
    .line 359
    iput-object p1, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService$2;->this$0:Lcom/samsung/android/fingerprint/service/FingerprintManagerService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 362
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 363
    .local v0, "action":Ljava/lang/String;
    const-string v5, "FingerprintManagerService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "onReceive: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 364
    const-string v5, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 365
    iget-object v5, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService$2;->this$0:Lcom/samsung/android/fingerprint/service/FingerprintManagerService;

    # setter for: Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mIsLcdOn:Z
    invoke-static {v5, v8}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->access$1502(Lcom/samsung/android/fingerprint/service/FingerprintManagerService;Z)Z

    .line 366
    iget-object v5, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService$2;->this$0:Lcom/samsung/android/fingerprint/service/FingerprintManagerService;

    # invokes: Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->requestChangePriority()V
    invoke-static {v5}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->access$1600(Lcom/samsung/android/fingerprint/service/FingerprintManagerService;)V

    .line 395
    :cond_0
    :goto_0
    return-void

    .line 368
    :cond_1
    const-string v5, "android.intent.action.SCREEN_ON"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 369
    iget-object v5, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService$2;->this$0:Lcom/samsung/android/fingerprint/service/FingerprintManagerService;

    # setter for: Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mIsLcdOn:Z
    invoke-static {v5, v9}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->access$1502(Lcom/samsung/android/fingerprint/service/FingerprintManagerService;Z)Z

    goto :goto_0

    .line 371
    :cond_2
    const-string v5, "android.intent.action.CLOSE_SYSTEM_DIALOGS"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 372
    iget-object v5, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService$2;->this$0:Lcom/samsung/android/fingerprint/service/FingerprintManagerService;

    # getter for: Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mIdentifyDialogManager:Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;
    invoke-static {v5}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->access$100(Lcom/samsung/android/fingerprint/service/FingerprintManagerService;)Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 373
    iget-object v5, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService$2;->this$0:Lcom/samsung/android/fingerprint/service/FingerprintManagerService;

    # getter for: Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mIdentifyDialogManager:Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;
    invoke-static {v5}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->access$100(Lcom/samsung/android/fingerprint/service/FingerprintManagerService;)Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;->closeDialog()V

    goto :goto_0

    .line 375
    :cond_3
    const-string v5, "android.intent.action.USER_PRESENT"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 376
    iget-object v5, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService$2;->this$0:Lcom/samsung/android/fingerprint/service/FingerprintManagerService;

    # setter for: Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mIsUserPresent:Z
    invoke-static {v5, v9}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->access$1702(Lcom/samsung/android/fingerprint/service/FingerprintManagerService;Z)Z

    .line 377
    iget-object v5, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService$2;->this$0:Lcom/samsung/android/fingerprint/service/FingerprintManagerService;

    # invokes: Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->requestChangePriority()V
    invoke-static {v5}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->access$1600(Lcom/samsung/android/fingerprint/service/FingerprintManagerService;)V

    goto :goto_0

    .line 378
    :cond_4
    const-string v5, "android.intent.action.USER_REMOVED"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 379
    const-string v5, "android.intent.extra.user_handle"

    const/4 v6, -0x1

    invoke-virtual {p2, v5, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    .line 380
    .local v4, "userId":I
    const-string v5, "FingerprintManagerService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "ACTION_USER_REMOVED: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 381
    iget-object v5, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService$2;->this$0:Lcom/samsung/android/fingerprint/service/FingerprintManagerService;

    iget-object v5, v5, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mH:Lcom/samsung/android/fingerprint/service/FingerprintManagerService$H;

    const/16 v6, 0x320

    invoke-virtual {v5, v6, v4, v8}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService$H;->obtainMessage(III)Landroid/os/Message;

    move-result-object v5

    invoke-virtual {v5}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0

    .line 382
    .end local v4    # "userId":I
    :cond_5
    const-string v5, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 383
    const-string v5, "VZW"

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 384
    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v3

    .line 385
    .local v3, "uri":Landroid/net/Uri;
    if-eqz v3, :cond_6

    invoke-virtual {v3}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v1

    .line 386
    .local v1, "packageName":Ljava/lang/String;
    :goto_1
    const-string v5, "android.intent.extra.REPLACING"

    invoke-virtual {p2, v5, v8}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    .line 387
    .local v2, "replacing":Z
    const-string v5, "FingerprintManagerService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "ACTION_PACKAGE_REMOVED: packageName="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", replace="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 388
    if-nez v2, :cond_0

    .line 389
    iget-object v5, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService$2;->this$0:Lcom/samsung/android/fingerprint/service/FingerprintManagerService;

    iget-object v5, v5, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mH:Lcom/samsung/android/fingerprint/service/FingerprintManagerService$H;

    const/16 v6, 0x321

    invoke-virtual {v5, v6, v1}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService$H;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v5

    invoke-virtual {v5}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_0

    .line 385
    .end local v1    # "packageName":Ljava/lang/String;
    .end local v2    # "replacing":Z
    :cond_6
    const/4 v1, 0x0

    goto :goto_1

    .line 392
    .end local v3    # "uri":Landroid/net/Uri;
    :cond_7
    const-string v5, "FingerprintManagerService"

    const-string v6, "ACTION_PACKAGE_REMOVED: skip"

    invoke-static {v5, v6}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method
