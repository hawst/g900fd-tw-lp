.class public abstract Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog;
.super Landroid/app/Dialog;
.source "IdentifyBaseDialog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog$Stage;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "IdentifyBaseDialog"


# instance fields
.field protected mContext:Landroid/content/Context;

.field protected mCountdownTimer:Landroid/os/CountDownTimer;

.field protected mDeviceTheme:I

.field protected mFpmsService:Lcom/samsung/android/fingerprint/service/FingerprintManagerService;

.field protected mIdentifyDialogManager:Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;

.field protected mToastContext:Landroid/content/Context;

.field protected mToken:Landroid/os/IBinder;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/samsung/android/fingerprint/service/FingerprintManagerService;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "service"    # Lcom/samsung/android/fingerprint/service/FingerprintManagerService;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 61
    const/high16 v1, 0x7f050000

    invoke-direct {p0, p1, v1}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    .line 40
    iput-object v3, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog;->mContext:Landroid/content/Context;

    .line 41
    iput-object v3, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog;->mToastContext:Landroid/content/Context;

    .line 58
    iput v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog;->mDeviceTheme:I

    .line 63
    iput-object p1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog;->mContext:Landroid/content/Context;

    .line 64
    iput-object p2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog;->mFpmsService:Lcom/samsung/android/fingerprint/service/FingerprintManagerService;

    .line 65
    sget-boolean v1, Lcom/samsung/android/fingerprint/service/util/Config;->IS_DEVICE_WHITE_THEME:Z

    if-eqz v1, :cond_1

    .line 66
    const/4 v1, 0x2

    iput v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog;->mDeviceTheme:I

    .line 67
    new-instance v1, Landroid/view/ContextThemeWrapper;

    const v2, 0x103012b

    invoke-direct {v1, p1, v2}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    iput-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog;->mToastContext:Landroid/content/Context;

    .line 72
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog;->mFpmsService:Lcom/samsung/android/fingerprint/service/FingerprintManagerService;

    if-eqz v1, :cond_0

    .line 73
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog;->mFpmsService:Lcom/samsung/android/fingerprint/service/FingerprintManagerService;

    invoke-virtual {v1}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->getIdentifyDialogManager()Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog;->mIdentifyDialogManager:Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;

    .line 75
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 76
    .local v0, "window":Landroid/view/Window;
    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/view/View;->setSystemUiVisibility(I)V

    .line 77
    const/16 v1, 0x7d8

    invoke-virtual {v0, v1}, Landroid/view/Window;->setType(I)V

    .line 78
    return-void

    .line 69
    .end local v0    # "window":Landroid/view/Window;
    :cond_1
    iput v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog;->mDeviceTheme:I

    .line 70
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog;->mContext:Landroid/content/Context;

    iput-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog;->mToastContext:Landroid/content/Context;

    goto :goto_0
.end method


# virtual methods
.method protected deliverEventToClient(Lcom/samsung/android/fingerprint/FingerprintEvent;)V
    .locals 1
    .param p1, "event"    # Lcom/samsung/android/fingerprint/FingerprintEvent;

    .prologue
    .line 85
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog;->mIdentifyDialogManager:Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;

    if-nez v0, :cond_0

    .line 86
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog;->mFpmsService:Lcom/samsung/android/fingerprint/service/FingerprintManagerService;

    invoke-virtual {v0}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->getIdentifyDialogManager()Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog;->mIdentifyDialogManager:Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;

    .line 88
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog;->mIdentifyDialogManager:Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;

    if-eqz v0, :cond_1

    .line 89
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog;->mIdentifyDialogManager:Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;

    invoke-virtual {v0, p1}, Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;->deliverIdentifyEventToClient(Lcom/samsung/android/fingerprint/FingerprintEvent;)V

    .line 91
    :cond_1
    return-void
.end method

.method public abstract onFingerprintEvent(Lcom/samsung/android/fingerprint/FingerprintEvent;)V
.end method

.method public setToken(Landroid/os/IBinder;)V
    .locals 0
    .param p1, "token"    # Landroid/os/IBinder;

    .prologue
    .line 81
    iput-object p1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog;->mToken:Landroid/os/IBinder;

    .line 82
    return-void
.end method
