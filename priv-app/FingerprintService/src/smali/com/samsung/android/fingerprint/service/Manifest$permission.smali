.class public final Lcom/samsung/android/fingerprint/service/Manifest$permission;
.super Ljava/lang/Object;
.source "Manifest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/fingerprint/service/Manifest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "permission"
.end annotation


# static fields
.field public static final ENROLL_FINGERPRINT:Ljava/lang/String; = "com.samsung.android.permission.ENROLL_FINGERPRINT"

.field public static final LAUNCH_FINGERPRINT_SERVICE:Ljava/lang/String; = "com.samsung.android.permission.LAUNCH_FINGERPRINT_SERVICE"

.field public static final PAYLOAD_FINGERPRINT:Ljava/lang/String; = "com.samsung.android.permission.PAYLOAD_FINGERPRINT"

.field public static final READ_PASSWORD_FINGERPRINT:Ljava/lang/String; = "com.samsung.android.permission.READ_PASSWORD_FINGERPRINT"

.field public static final REQUEST_PROCESS_FIDO:Ljava/lang/String; = "com.samsung.android.permission.REQUEST_PROCESS_FIDO"

.field public static final WRITE_PASSWORD_FINGERPRINT:Ljava/lang/String; = "com.samsung.android.permission.WRITE_PASSWORD_FINGERPRINT"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
