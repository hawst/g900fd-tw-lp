.class public Lcom/samsung/android/fingerprint/service/FingerprintManagerService;
.super Lcom/samsung/android/fingerprint/IFingerprintManager$Stub;
.source "FingerprintManagerService.java"

# interfaces
.implements Lcom/samsung/android/fingerprint/service/FingerprintLibraryListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/fingerprint/service/FingerprintManagerService$IdentifyDialogObject;,
        Lcom/samsung/android/fingerprint/service/FingerprintManagerService$SensorReadyCheckScheduler;,
        Lcom/samsung/android/fingerprint/service/FingerprintManagerService$H;
    }
.end annotation


# static fields
.field private static final ACTION_FINGERPRINT_ADDED:Ljava/lang/String; = "com.samsung.android.intent.action.FINGERPRINT_ADDED"

.field private static final ACTION_FINGERPRINT_PASSWORD_UPDATED:Ljava/lang/String; = "com.samsung.android.intent.action.FINGERPRINT_PASSWORD_UPDATED"

.field private static final ACTION_FINGERPRINT_REMOVED:Ljava/lang/String; = "com.samsung.android.intent.action.FINGERPRINT_REMOVED"

.field private static final ACTION_FINGERPRINT_RESET:Ljava/lang/String; = "com.samsung.android.intent.action.FINGERPRINT_RESET"

.field private static final ACTION_FINGERPRINT_SERVICE_STARTED:Ljava/lang/String; = "com.samsung.android.fingerprint.action.SERVICE_STARTED"

.field static final CHECK_PERMISSION:Z = true

.field private static final CONFIG_SUPPORT_BACKGROUND_CLIENT:Z = false

.field private static final DEBUG:Z

.field private static final PERMISSION_ENROLL_FINGERPRINT:Ljava/lang/String; = "com.samsung.android.permission.ENROLL_FINGERPRINT"

.field private static final PERMISSION_PAYLOAD_FINGERPRINT:Ljava/lang/String; = "com.samsung.android.permission.PAYLOAD_FINGERPRINT"

.field private static final PERMISSION_READ_PASSWORD_FINGERPRINT:Ljava/lang/String; = "com.samsung.android.permission.READ_PASSWORD_FINGERPRINT"

.field private static final PERMISSION_WRITE_PASSWORD_FINGERPRINT:Ljava/lang/String; = "com.samsung.android.permission.WRITE_PASSWORD_FINGERPRINT"

.field public static final SERVICE_VERSION:I = 0x1020000

.field static final TAG:Ljava/lang/String; = "FingerprintManagerService"

.field private static final VERIFICATION_TYPE_FINGERPINT:I = 0x1

.field private static final VERIFICATION_TYPE_PASSWORD:I = 0x2

.field private static final isEnableSurveyMode:Z

.field private static mEnrolledFingerCount:I

.field private static mHWStatus:I

.field private static mSensorReady:Z

.field private static sInstance:Lcom/samsung/android/fingerprint/IFingerprintManager$Stub;


# instance fields
.field private mClientList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;",
            ">;"
        }
    .end annotation
.end field

.field private final mContext:Landroid/content/Context;

.field private mFactory:Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommandFactory;

.field private mFocusedClient:Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

.field private mFocusedClientSync:Ljava/lang/Object;

.field private mFocusedClientSyncThread:Ljava/lang/Thread;

.field private mFpsReceiver:Landroid/content/BroadcastReceiver;

.field final mH:Lcom/samsung/android/fingerprint/service/FingerprintManagerService$H;

.field private mIdentifyDialogManager:Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;

.field private mImpl:Lcom/samsung/android/fingerprint/service/FingerprintLibraryImplAbstractClass;

.field private mImplVersion:Ljava/lang/String;

.field private mIsEnrollSession:Z

.field private mIsLcdOn:Z

.field private mIsUserPresent:Z

.field private mLastImageQuality:I

.field private mLastImageQualityMessage:Ljava/lang/String;

.field private mLatestEnrolledFingerIndex:I

.field private mLibraryVersion:Ljava/lang/String;

.field private mNextFocusedClient:Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

.field private mPersonaManager:Landroid/os/PersonaManager;

.field private mPhoneStateListener:Landroid/telephony/PhoneStateListener;

.field private mPreferences:Landroid/content/SharedPreferences;

.field private mSensorChecker:Lcom/samsung/android/fingerprint/service/FingerprintManagerService$SensorReadyCheckScheduler;

.field private mSilentClient:Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

.field private mSilentClientToken:Landroid/os/IBinder;

.field private mTelMgr:Landroid/telephony/TelephonyManager;

.field private mTokenMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Landroid/os/IBinder;",
            "Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;",
            ">;"
        }
    .end annotation
.end field

.field private mVerificationTypeToEnroll:I

.field private mWhiteAppListForKeyguard:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 90
    invoke-static {}, Landroid/os/Debug;->isProductShip()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->DEBUG:Z

    .line 114
    const/4 v0, -0x1

    sput v0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mEnrolledFingerCount:I

    .line 138
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->sInstance:Lcom/samsung/android/fingerprint/IFingerprintManager$Stub;

    .line 143
    sput-boolean v1, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mSensorReady:Z

    .line 146
    sput v1, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mHWStatus:I

    .line 148
    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v0

    const-string v1, "SEC_FLOATING_FEATURE_CONTEXTSERVICE_ENABLE_SURVEY_MODE"

    invoke-virtual {v0, v1}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->isEnableSurveyMode:Z

    return-void

    :cond_0
    move v0, v1

    .line 90
    goto :goto_0
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/16 v5, 0x208

    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 437
    invoke-direct {p0}, Lcom/samsung/android/fingerprint/IFingerprintManager$Stub;-><init>()V

    .line 118
    iput v3, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mVerificationTypeToEnroll:I

    .line 119
    iput v3, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mLatestEnrolledFingerIndex:I

    .line 120
    iput-boolean v3, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mIsEnrollSession:Z

    .line 129
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mTokenMap:Ljava/util/Map;

    .line 130
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mClientList:Ljava/util/List;

    .line 134
    new-instance v0, Landroid/os/Binder;

    invoke-direct {v0}, Landroid/os/Binder;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mSilentClientToken:Landroid/os/IBinder;

    .line 136
    iput-object v4, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mIdentifyDialogManager:Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;

    .line 144
    new-instance v0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService$SensorReadyCheckScheduler;

    invoke-direct {v0, v4}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService$SensorReadyCheckScheduler;-><init>(Lcom/samsung/android/fingerprint/service/FingerprintManagerService$1;)V

    iput-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mSensorChecker:Lcom/samsung/android/fingerprint/service/FingerprintManagerService$SensorReadyCheckScheduler;

    .line 151
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "com.android.keyguard"

    aput-object v1, v0, v3

    const/4 v1, 0x1

    const-string v2, "com.android.systemui"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "com.sec.knox.containeragent2"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mWhiteAppListForKeyguard:[Ljava/lang/String;

    .line 162
    iput v3, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mLastImageQuality:I

    .line 165
    iput-object v4, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mPersonaManager:Landroid/os/PersonaManager;

    .line 170
    iput-boolean v3, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mIsLcdOn:Z

    .line 171
    iput-boolean v3, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mIsUserPresent:Z

    .line 173
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mFocusedClientSync:Ljava/lang/Object;

    .line 174
    iput-object v4, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mFocusedClientSyncThread:Ljava/lang/Thread;

    .line 178
    new-instance v0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService$1;-><init>(Lcom/samsung/android/fingerprint/service/FingerprintManagerService;)V

    iput-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    .line 188
    new-instance v0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService$H;

    invoke-direct {v0, p0}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService$H;-><init>(Lcom/samsung/android/fingerprint/service/FingerprintManagerService;)V

    iput-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mH:Lcom/samsung/android/fingerprint/service/FingerprintManagerService$H;

    .line 359
    new-instance v0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService$2;

    invoke-direct {v0, p0}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService$2;-><init>(Lcom/samsung/android/fingerprint/service/FingerprintManagerService;)V

    iput-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mFpsReceiver:Landroid/content/BroadcastReceiver;

    .line 438
    const-string v0, "FingerprintManagerService"

    const-string v1, "constructor() called."

    invoke-static {v0, v1}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 440
    iput-object p1, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mContext:Landroid/content/Context;

    .line 441
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mPreferences:Landroid/content/SharedPreferences;

    .line 444
    new-instance v0, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommandFactory;

    invoke-direct {v0}, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommandFactory;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mFactory:Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommandFactory;

    .line 446
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mH:Lcom/samsung/android/fingerprint/service/FingerprintManagerService$H;

    invoke-virtual {v0, v5}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService$H;->removeMessages(I)V

    .line 447
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mH:Lcom/samsung/android/fingerprint/service/FingerprintManagerService$H;

    invoke-virtual {v0, v5}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService$H;->sendEmptyMessage(I)Z

    .line 449
    new-instance v0, Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;

    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mContext:Landroid/content/Context;

    invoke-direct {v0, p0, v1}, Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;-><init>(Lcom/samsung/android/fingerprint/service/FingerprintManagerService;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mIdentifyDialogManager:Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;

    .line 450
    invoke-direct {p0}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->registerFpsReceiver()V

    .line 451
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mPreferences:Landroid/content/SharedPreferences;

    invoke-static {v0}, Lcom/samsung/android/fingerprint/service/util/Config;->initFailedFingerprintAttemptsFromPrefrence(Landroid/content/SharedPreferences;)V

    .line 453
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mContext:Landroid/content/Context;

    const-string v1, "phone"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    iput-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mTelMgr:Landroid/telephony/TelephonyManager;

    .line 454
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mTelMgr:Landroid/telephony/TelephonyManager;

    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    const/16 v2, 0x20

    invoke-virtual {v0, v1, v2}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    .line 456
    return-void
.end method

.method static synthetic access$100(Lcom/samsung/android/fingerprint/service/FingerprintManagerService;)Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/fingerprint/service/FingerprintManagerService;

    .prologue
    .line 85
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mIdentifyDialogManager:Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/samsung/android/fingerprint/service/FingerprintManagerService;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/fingerprint/service/FingerprintManagerService;

    .prologue
    .line 85
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/samsung/android/fingerprint/service/FingerprintManagerService;Ljava/lang/String;Ljava/lang/String;J)V
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/fingerprint/service/FingerprintManagerService;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # J

    .prologue
    .line 85
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->insertSurveyLog(Ljava/lang/String;Ljava/lang/String;J)V

    return-void
.end method

.method static synthetic access$1200()I
    .locals 1

    .prologue
    .line 85
    sget v0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mEnrolledFingerCount:I

    return v0
.end method

.method static synthetic access$1300(Lcom/samsung/android/fingerprint/service/FingerprintManagerService;I)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/fingerprint/service/FingerprintManagerService;
    .param p1, "x1"    # I

    .prologue
    .line 85
    invoke-direct {p0, p1}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->removeEnrolledUser(I)Z

    move-result v0

    return v0
.end method

.method static synthetic access$1400(Lcom/samsung/android/fingerprint/service/FingerprintManagerService;Ljava/lang/String;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/fingerprint/service/FingerprintManagerService;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 85
    invoke-direct {p0, p1}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->removeEnrolledApp(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$1502(Lcom/samsung/android/fingerprint/service/FingerprintManagerService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/fingerprint/service/FingerprintManagerService;
    .param p1, "x1"    # Z

    .prologue
    .line 85
    iput-boolean p1, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mIsLcdOn:Z

    return p1
.end method

.method static synthetic access$1600(Lcom/samsung/android/fingerprint/service/FingerprintManagerService;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/fingerprint/service/FingerprintManagerService;

    .prologue
    .line 85
    invoke-direct {p0}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->requestChangePriority()V

    return-void
.end method

.method static synthetic access$1702(Lcom/samsung/android/fingerprint/service/FingerprintManagerService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/fingerprint/service/FingerprintManagerService;
    .param p1, "x1"    # Z

    .prologue
    .line 85
    iput-boolean p1, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mIsUserPresent:Z

    return p1
.end method

.method static synthetic access$200(Lcom/samsung/android/fingerprint/service/FingerprintManagerService;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/fingerprint/service/FingerprintManagerService;

    .prologue
    .line 85
    invoke-direct {p0}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->handleStartInitService()V

    return-void
.end method

.method static synthetic access$300(Lcom/samsung/android/fingerprint/service/FingerprintManagerService;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/fingerprint/service/FingerprintManagerService;

    .prologue
    .line 85
    invoke-direct {p0}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->handleCheckSensorReady()V

    return-void
.end method

.method static synthetic access$400(Lcom/samsung/android/fingerprint/service/FingerprintManagerService;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/fingerprint/service/FingerprintManagerService;

    .prologue
    .line 85
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mFocusedClientSync:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$502(Lcom/samsung/android/fingerprint/service/FingerprintManagerService;Ljava/lang/Thread;)Ljava/lang/Thread;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/fingerprint/service/FingerprintManagerService;
    .param p1, "x1"    # Ljava/lang/Thread;

    .prologue
    .line 85
    iput-object p1, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mFocusedClientSyncThread:Ljava/lang/Thread;

    return-object p1
.end method

.method static synthetic access$600(Lcom/samsung/android/fingerprint/service/FingerprintManagerService;Ljava/lang/Object;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/fingerprint/service/FingerprintManagerService;
    .param p1, "x1"    # Ljava/lang/Object;

    .prologue
    .line 85
    invoke-direct {p0, p1}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->handleFingerprintEvent(Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic access$700(Lcom/samsung/android/fingerprint/service/FingerprintManagerService;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/fingerprint/service/FingerprintManagerService;

    .prologue
    .line 85
    invoke-direct {p0}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->handleChangePriority()V

    return-void
.end method

.method static synthetic access$900(Lcom/samsung/android/fingerprint/service/FingerprintManagerService;Landroid/os/Bundle;Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/fingerprint/service/FingerprintManagerService;
    .param p1, "x1"    # Landroid/os/Bundle;
    .param p2, "x2"    # Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

    .prologue
    .line 85
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->handleshowIdentifyDialog(Landroid/os/Bundle;Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;)V

    return-void
.end method

.method private checkDigitalBackup(Ljava/lang/String;)Z
    .locals 2
    .param p1, "password"    # Ljava/lang/String;

    .prologue
    .line 2044
    const-string v0, "FingerprintManagerService"

    const-string v1, "checkDigitalBackup: called"

    invoke-static {v0, v1}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2060
    new-instance v0, Lcom/android/internal/widget/LockPatternUtils;

    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/android/internal/widget/LockPatternUtils;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, p1}, Lcom/android/internal/widget/LockPatternUtils;->checkFingerprintPassword(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private createBlackholeClient()V
    .locals 4

    .prologue
    .line 551
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 552
    .local v0, "clientSpec":Landroid/os/Bundle;
    const-string v1, "appName"

    const-string v2, "Silent Client"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 553
    new-instance v1, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mSilentClientToken:Landroid/os/IBinder;

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v3

    invoke-direct {v1, p0, v2, v0, v3}, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;-><init>(Lcom/samsung/android/fingerprint/service/FingerprintManagerService;Landroid/os/IBinder;Landroid/os/Bundle;I)V

    iput-object v1, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mSilentClient:Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

    .line 554
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mTokenMap:Ljava/util/Map;

    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mSilentClientToken:Landroid/os/IBinder;

    iget-object v3, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mSilentClient:Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 555
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mClientList:Ljava/util/List;

    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mSilentClient:Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 556
    return-void
.end method

.method private createFingerprintLibraryImpl(Ljava/lang/String;Landroid/content/Context;Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommandFactory;)Lcom/samsung/android/fingerprint/service/FingerprintLibraryImplAbstractClass;
    .locals 8
    .param p1, "className"    # Ljava/lang/String;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "factory"    # Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommandFactory;

    .prologue
    .line 536
    const/4 v4, 0x0

    .line 539
    .local v4, "retval":Lcom/samsung/android/fingerprint/service/FingerprintLibraryImplAbstractClass;
    :try_start_0
    invoke-static {p1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    .line 540
    .local v1, "cls":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Class;

    const/4 v6, 0x0

    const-class v7, Landroid/content/Context;

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const-class v7, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommandFactory;

    aput-object v7, v5, v6

    invoke-virtual {v1, v5}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v2

    .line 542
    .local v2, "constructor":Ljava/lang/reflect/Constructor;, "Ljava/lang/reflect/Constructor<*>;"
    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object p2, v5, v6

    const/4 v6, 0x1

    aput-object p3, v5, v6

    invoke-virtual {v2, v5}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    move-object v0, v5

    check-cast v0, Lcom/samsung/android/fingerprint/service/FingerprintLibraryImplAbstractClass;

    move-object v4, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 547
    .end local v1    # "cls":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v2    # "constructor":Ljava/lang/reflect/Constructor;, "Ljava/lang/reflect/Constructor<*>;"
    :goto_0
    return-object v4

    .line 543
    :catch_0
    move-exception v3

    .line 544
    .local v3, "e":Ljava/lang/Exception;
    const-string v5, "FingerprintManagerService"

    const-string v6, "createFingerprintLibraryImpl: failed: "

    invoke-static {v5, v6, v3}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private enqueueCommand(Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;)Ljava/lang/Object;
    .locals 4
    .param p1, "record"    # Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;
    .param p2, "cmd"    # Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;

    .prologue
    const/4 v1, 0x0

    .line 1281
    if-nez p2, :cond_0

    .line 1282
    const-string v2, "FingerprintManagerService"

    const-string v3, "enqueueCommand: cmd may not be null."

    invoke-static {v2, v3}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1310
    :goto_0
    return-object v1

    .line 1286
    :cond_0
    if-nez p1, :cond_1

    .line 1287
    const-string v2, "FingerprintManagerService"

    const-string v3, "enqueueCommand: record may not be null."

    invoke-static {v2, v3}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1291
    :cond_1
    const-string v1, "FingerprintManagerService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "enqueueCommand: cmd="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p2, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;->commandName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1293
    invoke-virtual {p2}, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;->getCommandType()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    .line 1294
    invoke-virtual {p2}, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;->execute()Ljava/lang/Object;

    move-result-object v1

    goto :goto_0

    .line 1296
    :cond_2
    invoke-virtual {p1}, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->getCommand()Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;

    move-result-object v0

    .line 1297
    .local v0, "curCmd":Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;
    if-eqz v0, :cond_3

    .line 1298
    iget-object v1, v0, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;->commandName:Ljava/lang/String;

    iget-object v2, p2, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;->commandName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1299
    const-string v1, "FingerprintManagerService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "enqueueCommand: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p2, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;->commandName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " is already in progress"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1300
    const/4 v1, -0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto :goto_0

    .line 1304
    :cond_3
    invoke-virtual {p2, p1}, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;->setClientRecord(Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;)V

    .line 1305
    invoke-virtual {p1, p2}, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->setCommand(Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;)V

    .line 1308
    invoke-virtual {p0, p1}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->setFocusedClient(Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;)V

    .line 1310
    invoke-virtual {p2}, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;->execute()Ljava/lang/Object;

    move-result-object v1

    goto :goto_0
.end method

.method private findClientRecord(Landroid/os/IBinder;)Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;
    .locals 3
    .param p1, "token"    # Landroid/os/IBinder;

    .prologue
    const/4 v0, 0x0

    .line 1341
    const-string v1, "FingerprintManagerService"

    const-string v2, "findClientRecord: LOCK: wait to obtain"

    invoke-static {v1, v2}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->lock(Ljava/lang/String;Ljava/lang/String;)V

    .line 1343
    if-nez p1, :cond_0

    .line 1344
    const-string v1, "FingerprintManagerService"

    const-string v2, "findClientRecord: LOCK: released."

    invoke-static {v1, v2}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->lock(Ljava/lang/String;Ljava/lang/String;)V

    .line 1359
    :goto_0
    return-object v0

    .line 1348
    :cond_0
    monitor-enter p0

    .line 1349
    :try_start_0
    const-string v1, "FingerprintManagerService"

    const-string v2, "findClientRecord: LOCK: obtained."

    invoke-static {v1, v2}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->lock(Ljava/lang/String;Ljava/lang/String;)V

    .line 1351
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mTokenMap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1352
    const-string v0, "FingerprintManagerService"

    const-string v1, "findClientRecord: LOCK: released."

    invoke-static {v0, v1}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->lock(Ljava/lang/String;Ljava/lang/String;)V

    .line 1353
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mTokenMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

    monitor-exit p0

    goto :goto_0

    .line 1355
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1357
    const-string v1, "FingerprintManagerService"

    const-string v2, "findClientRecord: LOCK: released."

    invoke-static {v1, v2}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->lock(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private getEnrolledFingers(ILjava/lang/String;)Ljava/util/ArrayList;
    .locals 11
    .param p1, "uid"    # I
    .param p2, "ownName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v10, 0x1

    .line 604
    iget-object v6, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mImpl:Lcom/samsung/android/fingerprint/service/FingerprintLibraryImplAbstractClass;

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mImpl:Lcom/samsung/android/fingerprint/service/FingerprintLibraryImplAbstractClass;

    invoke-virtual {v6}, Lcom/samsung/android/fingerprint/service/FingerprintLibraryImplAbstractClass;->isSensorReady()Z

    move-result v6

    if-nez v6, :cond_1

    .line 605
    :cond_0
    const-string v6, "FingerprintManagerService"

    const-string v7, "Sensor is not ready!!!"

    invoke-static {v6, v7}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 628
    :goto_0
    return-object v1

    .line 609
    :cond_1
    invoke-static {p1, p2}, Lcom/samsung/android/fingerprint/service/util/ValueUtils;->getStringUserId(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 611
    .local v5, "stringUserId":Ljava/lang/String;
    iget-object v6, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mFactory:Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommandFactory;

    const-string v7, "getEnrolledFingers"

    new-array v8, v10, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v5, v8, v9

    invoke-virtual {v6, v7, v8}, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommandFactory;->create(Ljava/lang/String;[Ljava/lang/Object;)Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;

    move-result-object v0

    .line 615
    .local v0, "cmd":Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;
    iget-object v6, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mSilentClient:Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

    invoke-direct {p0, v6, v0}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->enqueueCommand(Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;)Ljava/lang/Object;

    move-result-object v4

    .line 616
    .local v4, "retObj":Ljava/lang/Object;
    if-nez v4, :cond_2

    .line 617
    const-string v6, "FingerprintManagerService"

    const-string v7, "enqueueCommand result is null"

    invoke-static {v6, v7}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 620
    :cond_2
    check-cast v4, Ljava/lang/Integer;

    .end local v4    # "retObj":Ljava/lang/Object;
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 621
    .local v3, "result":I
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 622
    .local v1, "fa":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const/4 v2, 0x1

    .local v2, "i":I
    :goto_1
    const/16 v6, 0xa

    if-gt v2, v6, :cond_4

    .line 623
    shl-int v6, v10, v2

    and-int/2addr v6, v3

    if-eqz v6, :cond_3

    .line 624
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 622
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 627
    :cond_4
    const-string v6, "FingerprintManagerService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "getEnrolledFingers:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", fingers "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v1}, Ljava/util/ArrayList;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static declared-synchronized getInstance(Landroid/content/Context;Z)Lcom/samsung/android/fingerprint/IFingerprintManager$Stub;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "startFlag"    # Z

    .prologue
    .line 559
    const-class v1, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->sInstance:Lcom/samsung/android/fingerprint/IFingerprintManager$Stub;

    if-nez v0, :cond_0

    .line 560
    new-instance v0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;

    invoke-direct {v0, p0}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->sInstance:Lcom/samsung/android/fingerprint/IFingerprintManager$Stub;

    .line 562
    :cond_0
    sget-object v0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->sInstance:Lcom/samsung/android/fingerprint/IFingerprintManager$Stub;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 559
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private getSPKey(I)Ljava/lang/String;
    .locals 3
    .param p1, "idx"    # I

    .prologue
    .line 1841
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-direct {p0}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->getUserId()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1842
    .local v0, "value":Ljava/lang/String;
    return-object v0
.end method

.method private getSPValue(ILandroid/content/Context;)Ljava/lang/String;
    .locals 3
    .param p1, "idx"    # I
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 1784
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mPreferences:Landroid/content/SharedPreferences;

    invoke-direct {p0, p1}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->getSPKey(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getSensorInfo()V
    .locals 5

    .prologue
    .line 3078
    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mFactory:Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommandFactory;

    const-string v3, "getSensorInfo"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v2, v3, v4}, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommandFactory;->create(Ljava/lang/String;[Ljava/lang/Object;)Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;

    move-result-object v0

    .line 3081
    .local v0, "cmd":Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;
    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mSilentClient:Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

    invoke-direct {p0, v2, v0}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->enqueueCommand(Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 3082
    .local v1, "result":Ljava/lang/Integer;
    const-string v2, "FingerprintManagerService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getSensorInfo: called, result="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->da(Ljava/lang/String;Ljava/lang/String;)V

    .line 3083
    return-void
.end method

.method private getServiceVersion()Ljava/lang/String;
    .locals 8

    .prologue
    .line 1924
    const/4 v0, 0x1

    .line 1925
    .local v0, "major":I
    const/4 v1, 0x2

    .line 1926
    .local v1, "minor":I
    const/4 v2, 0x0

    .line 1928
    .local v2, "seq":I
    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v4, "%d.%d.%d"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v3, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method private getSignatureHash([B)Ljava/lang/String;
    .locals 9
    .param p1, "arr"    # [B

    .prologue
    .line 2486
    const-string v3, ""

    .line 2488
    .local v3, "result":Ljava/lang/String;
    :try_start_0
    const-string v6, "SHA-256"

    invoke-static {v6}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v5

    .line 2489
    .local v5, "sh":Ljava/security/MessageDigest;
    invoke-virtual {v5, p1}, Ljava/security/MessageDigest;->update([B)V

    .line 2490
    invoke-virtual {v5}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v0

    .line 2491
    .local v0, "byteData":[B
    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    .line 2492
    .local v4, "sb":Ljava/lang/StringBuffer;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v6, v0

    if-ge v2, v6, :cond_0

    .line 2493
    aget-byte v6, v0, v2

    and-int/lit16 v6, v6, 0xff

    add-int/lit16 v6, v6, 0x100

    const/16 v7, 0x10

    invoke-static {v6, v7}, Ljava/lang/Integer;->toString(II)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 2492
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 2495
    :cond_0
    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 2499
    .end local v0    # "byteData":[B
    .end local v2    # "i":I
    .end local v4    # "sb":Ljava/lang/StringBuffer;
    .end local v5    # "sh":Ljava/security/MessageDigest;
    :goto_1
    const-string v6, "FingerprintManagerService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "getSignatureHash: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2500
    return-object v3

    .line 2496
    :catch_0
    move-exception v1

    .line 2497
    .local v1, "e":Ljava/security/NoSuchAlgorithmException;
    const/4 v3, 0x0

    goto :goto_1
.end method

.method private getUserId()I
    .locals 6

    .prologue
    .line 2639
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v4

    invoke-static {v4}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v0

    .line 2641
    .local v0, "callerUserId":I
    invoke-static {}, Landroid/app/ActivityManager;->getCurrentUser()I

    move-result v1

    .line 2643
    .local v1, "currentUserId":I
    if-eqz v1, :cond_0

    .line 2644
    move v0, v1

    .line 2647
    :cond_0
    move v2, v0

    .line 2650
    .local v2, "userId":I
    invoke-static {}, Landroid/os/PersonaManager;->getKnoxInfo()Landroid/os/Bundle;

    move-result-object v3

    .line 2652
    .local v3, "versionInfo":Landroid/os/Bundle;
    if-eqz v3, :cond_2

    const-string v4, "2.0"

    const-string v5, "version"

    invoke-virtual {v3, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 2653
    iget-object v4, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mPersonaManager:Landroid/os/PersonaManager;

    if-nez v4, :cond_1

    .line 2654
    iget-object v4, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mContext:Landroid/content/Context;

    const-string v5, "persona"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/os/PersonaManager;

    iput-object v4, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mPersonaManager:Landroid/os/PersonaManager;

    .line 2657
    :cond_1
    iget-object v4, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mPersonaManager:Landroid/os/PersonaManager;

    if-eqz v4, :cond_3

    .line 2658
    iget-object v4, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mPersonaManager:Landroid/os/PersonaManager;

    invoke-virtual {v4, v0}, Landroid/os/PersonaManager;->getParentId(I)I

    move-result v2

    .line 2666
    :cond_2
    :goto_0
    return v2

    .line 2661
    :cond_3
    const-string v4, "FingerprintManagerService"

    const-string v5, "getUserId: Illegal access to Persona."

    invoke-static {v4, v5}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private handleChangePriority()V
    .locals 4

    .prologue
    .line 404
    invoke-static {}, Landroid/os/Process;->myTid()I

    move-result v0

    .line 405
    .local v0, "tid":I
    iget-boolean v1, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mIsLcdOn:Z

    if-nez v1, :cond_0

    invoke-direct {p0}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->isFingerprintLockType()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 406
    const/4 v1, -0x2

    invoke-static {v1}, Landroid/os/Process;->setThreadPriority(I)V

    .line 407
    const-string v1, "FingerprintManagerService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "priority after change = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v0}, Landroid/os/Process;->getThreadPriority(I)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 409
    invoke-static {}, Lcom/samsung/android/fingerprint/service/util/Config;->resetFailedFingerprintAttempts()V

    .line 419
    :goto_0
    return-void

    .line 411
    :cond_0
    iget-boolean v1, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mIsUserPresent:Z

    if-eqz v1, :cond_1

    .line 412
    const/4 v1, -0x1

    invoke-static {v1}, Landroid/os/Process;->setThreadPriority(I)V

    .line 413
    const-string v1, "FingerprintManagerService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "priority after change = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v0}, Landroid/os/Process;->getThreadPriority(I)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 414
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mIsUserPresent:Z

    goto :goto_0

    .line 417
    :cond_1
    const-string v1, "FingerprintManagerService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "priority NO change = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v0}, Landroid/os/Process;->getThreadPriority(I)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private handleCheckSensorReady()V
    .locals 5

    .prologue
    const/16 v4, 0x209

    .line 511
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mImpl:Lcom/samsung/android/fingerprint/service/FingerprintLibraryImplAbstractClass;

    if-eqz v0, :cond_0

    .line 512
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mImpl:Lcom/samsung/android/fingerprint/service/FingerprintLibraryImplAbstractClass;

    invoke-virtual {v0}, Lcom/samsung/android/fingerprint/service/FingerprintLibraryImplAbstractClass;->isSensorReady()Z

    move-result v0

    sput-boolean v0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mSensorReady:Z

    .line 514
    :cond_0
    const-string v0, "FingerprintManagerService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handleCheckSensorReady()="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v2, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mSensorReady:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 515
    sget-boolean v0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mSensorReady:Z

    if-eqz v0, :cond_1

    .line 516
    invoke-direct {p0}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->initSensorInfo()V

    .line 522
    :goto_0
    return-void

    .line 518
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mSensorChecker:Lcom/samsung/android/fingerprint/service/FingerprintManagerService$SensorReadyCheckScheduler;

    invoke-virtual {v0}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService$SensorReadyCheckScheduler;->notifyFailed()V

    .line 519
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mH:Lcom/samsung/android/fingerprint/service/FingerprintManagerService$H;

    invoke-virtual {v0, v4}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService$H;->removeMessages(I)V

    .line 520
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mH:Lcom/samsung/android/fingerprint/service/FingerprintManagerService$H;

    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mSensorChecker:Lcom/samsung/android/fingerprint/service/FingerprintManagerService$SensorReadyCheckScheduler;

    invoke-virtual {v1}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService$SensorReadyCheckScheduler;->getNextDelayTime()I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {v0, v4, v2, v3}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService$H;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0
.end method

.method private handleFingerprintEvent(Ljava/lang/Object;)V
    .locals 14
    .param p1, "evt"    # Ljava/lang/Object;

    .prologue
    const/16 v13, 0x8

    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v12, -0x1

    const-wide/16 v10, -0x1

    .line 1644
    iget-object v3, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mFocusedClient:Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

    .line 1646
    .local v3, "focusedClient":Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;
    if-nez v3, :cond_1

    .line 1647
    const-string v6, "FingerprintManagerService"

    const-string v7, "handleFingerprintEvent: mFocusedClient is null so this event will be skipped."

    invoke-static {v6, v7}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1724
    :cond_0
    :goto_0
    return-void

    .line 1651
    :cond_1
    invoke-virtual {v3}, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->getCommand()Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;

    move-result-object v2

    .line 1653
    .local v2, "focusedCilentCmd":Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;
    if-nez v2, :cond_2

    .line 1654
    const-string v6, "FingerprintManagerService"

    const-string v7, "handleFingerprintEvent: mFocusedClient has no command which is running currently, so this event will be skipped."

    invoke-static {v6, v7}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1658
    :cond_2
    invoke-virtual {v2, p1}, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;->onEvent(Ljava/lang/Object;)Z

    .line 1660
    invoke-virtual {v2}, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;->getFingerprivetEvent()Lcom/samsung/android/fingerprint/FingerprintEvent;

    move-result-object v0

    .line 1661
    .local v0, "fingerEvent":Lcom/samsung/android/fingerprint/FingerprintEvent;
    invoke-virtual {v3}, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->getOwnName()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_3

    move v4, v6

    .line 1663
    .local v4, "isVzwApp":Z
    :goto_1
    if-eqz v0, :cond_0

    .line 1665
    const-string v8, "enroll"

    iget-object v9, v2, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;->commandName:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 1666
    iget v6, v0, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventId:I

    const/4 v7, 0x4

    if-ne v6, v7, :cond_0

    .line 1667
    iget v6, v0, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventResult:I

    if-nez v6, :cond_5

    .line 1668
    iget-object v6, v0, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventData:Landroid/os/Bundle;

    const-string v7, "fingerIndex"

    invoke-virtual {v6, v7, v12}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v6

    iput v6, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mLatestEnrolledFingerIndex:I

    .line 1669
    const-string v6, "FingerprintManagerService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "handleFingerprintEvent: latest enrolled finger Idx: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mLatestEnrolledFingerIndex:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1670
    iget v6, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mVerificationTypeToEnroll:I

    if-eqz v6, :cond_0

    if-nez v4, :cond_0

    .line 1671
    iget v6, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mLatestEnrolledFingerIndex:I

    const/16 v7, 0xa

    if-gt v6, v7, :cond_4

    .line 1672
    iget v6, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mLatestEnrolledFingerIndex:I

    iget v7, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mVerificationTypeToEnroll:I

    invoke-direct {p0, v6, v7}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->requestNotifyFingerAdded(II)V

    goto :goto_0

    .end local v4    # "isVzwApp":Z
    :cond_3
    move v4, v7

    .line 1661
    goto :goto_1

    .line 1674
    .restart local v4    # "isVzwApp":Z
    :cond_4
    const-string v6, "FPEN"

    iget v7, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mLatestEnrolledFingerIndex:I

    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0, v6, v7, v10, v11}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->insertSurveyLog(Ljava/lang/String;Ljava/lang/String;J)V

    goto :goto_0

    .line 1677
    :cond_5
    iget v6, v0, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventResult:I

    if-ne v6, v12, :cond_0

    iget v6, v0, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventStatus:I

    if-eq v6, v13, :cond_0

    .line 1679
    iget-object v6, v0, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventData:Landroid/os/Bundle;

    const-string v7, "fingerIndex"

    invoke-virtual {v6, v7, v12}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 1680
    .local v1, "fingerIndex":I
    const-string v6, "FPEF"

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0, v6, v7, v10, v11}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->insertSurveyLog(Ljava/lang/String;Ljava/lang/String;J)V

    goto/16 :goto_0

    .line 1684
    .end local v1    # "fingerIndex":I
    :cond_6
    const-string v8, "identify"

    iget-object v9, v2, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;->commandName:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 1685
    iget v8, v0, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventId:I

    const/16 v9, 0xd

    if-ne v8, v9, :cond_c

    .line 1686
    invoke-virtual {v0}, Lcom/samsung/android/fingerprint/FingerprintEvent;->getImageQuality()I

    move-result v8

    iput v8, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mLastImageQuality:I

    .line 1687
    invoke-virtual {v0}, Lcom/samsung/android/fingerprint/FingerprintEvent;->getImageQualityFeedback()Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mLastImageQualityMessage:Ljava/lang/String;

    .line 1688
    iget v8, v0, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventResult:I

    if-nez v8, :cond_a

    .line 1689
    iget-boolean v8, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mIsEnrollSession:Z

    if-eqz v8, :cond_8

    .line 1690
    iput v7, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mVerificationTypeToEnroll:I

    .line 1694
    :goto_2
    if-nez v4, :cond_7

    .line 1695
    iget v6, v0, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventStatus:I

    const/16 v7, 0x64

    if-ne v6, v7, :cond_9

    .line 1696
    const-string v6, "FPIP"

    invoke-virtual {v3}, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->getPackageName()Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0, v6, v7, v10, v11}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->insertSurveyLog(Ljava/lang/String;Ljava/lang/String;J)V

    .line 1701
    :cond_7
    :goto_3
    invoke-static {}, Lcom/samsung/android/fingerprint/service/util/Config;->resetFailedFingerprintAttempts()V

    .line 1702
    iget-object v6, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mContext:Landroid/content/Context;

    const-string v7, "vibrator"

    invoke-virtual {v6, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/os/Vibrator;

    .line 1703
    .local v5, "vib":Landroid/os/Vibrator;
    if-eqz v5, :cond_0

    .line 1704
    const-wide/16 v6, 0x32

    invoke-virtual {v5, v6, v7}, Landroid/os/Vibrator;->vibrate(J)V

    goto/16 :goto_0

    .line 1692
    .end local v5    # "vib":Landroid/os/Vibrator;
    :cond_8
    iput v6, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mVerificationTypeToEnroll:I

    goto :goto_2

    .line 1698
    :cond_9
    const-string v6, "FPIS"

    invoke-virtual {v3}, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->getPackageName()Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0, v6, v7, v10, v11}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->insertSurveyLog(Ljava/lang/String;Ljava/lang/String;J)V

    goto :goto_3

    .line 1707
    :cond_a
    iget v6, v0, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventResult:I

    if-ne v6, v12, :cond_0

    .line 1708
    iget v6, v0, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventStatus:I

    if-eq v6, v13, :cond_b

    .line 1709
    if-nez v4, :cond_b

    .line 1710
    const-string v6, "FPIF"

    invoke-virtual {v3}, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->getPackageName()Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0, v6, v7, v10, v11}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->insertSurveyLog(Ljava/lang/String;Ljava/lang/String;J)V

    .line 1713
    :cond_b
    iget v6, v0, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventStatus:I

    const/16 v7, 0xb

    if-ne v6, v7, :cond_0

    .line 1714
    invoke-static {}, Lcom/samsung/android/fingerprint/service/util/Config;->reportFailedFingerprintAttempts()V

    goto/16 :goto_0

    .line 1718
    :cond_c
    iget v6, v0, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventId:I

    const/16 v7, 0xe

    if-ne v6, v7, :cond_0

    .line 1719
    invoke-virtual {v0}, Lcom/samsung/android/fingerprint/FingerprintEvent;->getImageQuality()I

    move-result v6

    iput v6, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mLastImageQuality:I

    .line 1720
    invoke-virtual {v0}, Lcom/samsung/android/fingerprint/FingerprintEvent;->getImageQualityFeedback()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mLastImageQualityMessage:Ljava/lang/String;

    goto/16 :goto_0
.end method

.method private handleStartInitService()V
    .locals 3

    .prologue
    .line 478
    const-string v0, "FingerprintManagerService"

    const-string v1, "handleStartInitService: called."

    invoke-static {v0, v1}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 480
    const-string v0, "com.samsung.android.fingerprint.service.vendor.FingerprintLibraryImpl"

    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mFactory:Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommandFactory;

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->createFingerprintLibraryImpl(Ljava/lang/String;Landroid/content/Context;Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommandFactory;)Lcom/samsung/android/fingerprint/service/FingerprintLibraryImplAbstractClass;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mImpl:Lcom/samsung/android/fingerprint/service/FingerprintLibraryImplAbstractClass;

    .line 483
    invoke-direct {p0}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->readSensorHWStatus()V

    .line 484
    invoke-direct {p0}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->createBlackholeClient()V

    .line 485
    const-string v0, "fingerprint_service"

    invoke-static {v0, p0}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V

    .line 486
    const-string v0, "FingerprintManagerService"

    const-string v1, "handleFinishInitService: FingerprintManagerService has been registerd in ServiceManager successfully..."

    invoke-static {v0, v1}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 488
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mImpl:Lcom/samsung/android/fingerprint/service/FingerprintLibraryImplAbstractClass;

    if-eqz v0, :cond_0

    .line 489
    invoke-direct {p0}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->handleCheckSensorReady()V

    .line 493
    :goto_0
    invoke-direct {p0}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->sendServiceStartedBroadcast()V

    .line 494
    return-void

    .line 491
    :cond_0
    const-string v0, "FingerprintManagerService"

    const-string v1, "handleFinishInitService: impl is null"

    invoke-static {v0, v1}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->da(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private handleshowIdentifyDialog(Landroid/os/Bundle;Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;)V
    .locals 4
    .param p1, "bundle"    # Landroid/os/Bundle;
    .param p2, "record"    # Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

    .prologue
    .line 787
    const-string v1, "FingerprintManagerService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "handleshowIdentifyDialog record="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p2}, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->getClientSpec()Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 789
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mIdentifyDialogManager:Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;

    if-eqz v1, :cond_1

    .line 790
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mIdentifyDialogManager:Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;

    invoke-virtual {v1}, Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;->closeDialog()V

    .line 791
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mIdentifyDialogManager:Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;

    invoke-virtual {v1, p1, p2}, Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;->setClientRecord(Landroid/os/Bundle;Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;)V

    .line 792
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mIdentifyDialogManager:Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;

    invoke-virtual {v1}, Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;->showDialog()V

    .line 813
    :cond_0
    :goto_0
    const-string v1, "FingerprintManagerService"

    const-string v2, "handleshowIdentifyDialog: Finished"

    invoke-static {v1, v2}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 814
    return-void

    .line 794
    :cond_1
    const-string v1, "FingerprintManagerService"

    const-string v2, "handleshowIdentifyDialog: mIdentifyDialogManager is null"

    invoke-static {v1, v2}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 795
    sget v1, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mHWStatus:I

    const/4 v2, 0x3

    if-eq v1, v2, :cond_2

    invoke-virtual {p0}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->isSensorReady()Z

    move-result v1

    if-nez v1, :cond_0

    .line 798
    :cond_2
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mIdentifyDialogManager:Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;

    if-nez v1, :cond_3

    .line 799
    new-instance v1, Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;

    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mContext:Landroid/content/Context;

    invoke-direct {v1, p0, v2}, Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;-><init>(Lcom/samsung/android/fingerprint/service/FingerprintManagerService;Landroid/content/Context;)V

    iput-object v1, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mIdentifyDialogManager:Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;

    .line 801
    :cond_3
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mIdentifyDialogManager:Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;

    if-eqz v1, :cond_0

    .line 802
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mIdentifyDialogManager:Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;

    invoke-virtual {v1}, Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;->closeDialog()V

    .line 803
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mIdentifyDialogManager:Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;

    invoke-virtual {v1, p1, p2}, Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;->setClientRecord(Landroid/os/Bundle;Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;)V

    .line 804
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mIdentifyDialogManager:Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;

    invoke-virtual {v1}, Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;->showDialog()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 806
    :catch_0
    move-exception v0

    .line 807
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "FingerprintManagerService"

    const-string v2, "handleshowIdentifyDialog: unable to make dialog"

    invoke-static {v1, v2, v0}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private declared-synchronized initSensorInfo()V
    .locals 2

    .prologue
    .line 497
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mImpl:Lcom/samsung/android/fingerprint/service/FingerprintLibraryImplAbstractClass;

    if-eqz v0, :cond_0

    .line 498
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mImpl:Lcom/samsung/android/fingerprint/service/FingerprintLibraryImplAbstractClass;

    invoke-virtual {v0}, Lcom/samsung/android/fingerprint/service/FingerprintLibraryImplAbstractClass;->getImplVersion()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mImplVersion:Ljava/lang/String;

    .line 499
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mImpl:Lcom/samsung/android/fingerprint/service/FingerprintLibraryImplAbstractClass;

    invoke-virtual {v0}, Lcom/samsung/android/fingerprint/service/FingerprintLibraryImplAbstractClass;->getLibraryVersion()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mLibraryVersion:Ljava/lang/String;

    .line 500
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mImpl:Lcom/samsung/android/fingerprint/service/FingerprintLibraryImplAbstractClass;

    invoke-virtual {v0}, Lcom/samsung/android/fingerprint/service/FingerprintLibraryImplAbstractClass;->checkIntegrity()V

    .line 501
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mImpl:Lcom/samsung/android/fingerprint/service/FingerprintLibraryImplAbstractClass;

    invoke-virtual {v0, p0}, Lcom/samsung/android/fingerprint/service/FingerprintLibraryImplAbstractClass;->registerListener(Lcom/samsung/android/fingerprint/service/FingerprintLibraryListener;)Z

    .line 502
    invoke-direct {p0}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->getSensorInfo()V

    .line 503
    invoke-direct {p0}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->updateFingerprintNames()V

    .line 504
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->getEnrolledFingers(Ljava/lang/String;)I

    move-result v0

    sput v0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mEnrolledFingerCount:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 508
    :goto_0
    monitor-exit p0

    return-void

    .line 506
    :cond_0
    :try_start_1
    const-string v0, "FingerprintManagerService"

    const-string v1, "initSensorInfo: impl is null"

    invoke-static {v0, v1}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->da(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 497
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private insertSurveyLog(Ljava/lang/String;Ljava/lang/String;J)V
    .locals 5
    .param p1, "feature"    # Ljava/lang/String;
    .param p2, "extra"    # Ljava/lang/String;
    .param p3, "value"    # J

    .prologue
    .line 2794
    const-string v2, "FingerprintManagerService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "insertSurveyLog: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-boolean v4, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->isEnableSurveyMode:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 2796
    sget-boolean v2, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->isEnableSurveyMode:Z

    if-eqz v2, :cond_0

    .line 2797
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 2798
    .local v1, "cv":Landroid/content/ContentValues;
    const-string v2, "app_id"

    const-string v3, "com.samsung.android.fingerprint.service"

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2799
    const-string v2, "feature"

    invoke-virtual {v1, v2, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2808
    const-string v2, "extra"

    invoke-virtual {v1, v2, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2811
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 2812
    .local v0, "broadcastIntent":Landroid/content/Intent;
    const-string v2, "com.samsung.android.providers.context.log.action.USE_APP_FEATURE_SURVEY"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 2813
    const-string v2, "data"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2814
    const-string v2, "com.samsung.android.providers.context"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 2815
    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mContext:Landroid/content/Context;

    if-eqz v2, :cond_0

    .line 2816
    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 2819
    .end local v0    # "broadcastIntent":Landroid/content/Intent;
    .end local v1    # "cv":Landroid/content/ContentValues;
    :cond_0
    return-void
.end method

.method private isEnrolledFinger(I)Z
    .locals 3
    .param p1, "index"    # I

    .prologue
    const/4 v1, 0x1

    .line 1736
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->getEnrolledFingers(Ljava/lang/String;)I

    move-result v0

    .line 1738
    .local v0, "fingers":I
    shl-int v2, v1, p1

    and-int/2addr v2, v0

    if-eqz v2, :cond_0

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private isFidoSignature()Z
    .locals 25

    .prologue
    .line 2423
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mContext:Landroid/content/Context;

    move-object/from16 v22, v0

    const-string v23, "com.samsung.android.permission.REQUEST_PROCESS_FIDO"

    invoke-virtual/range {v22 .. v23}, Landroid/content/Context;->checkCallingPermission(Ljava/lang/String;)I

    move-result v22

    if-nez v22, :cond_0

    .line 2424
    const-string v22, "FingerprintManagerService"

    const-string v23, "isFidoSignature: GRANTED for FIDO permission"

    invoke-static/range {v22 .. v23}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2425
    const/16 v22, 0x1

    .line 2482
    :goto_0
    return v22

    .line 2428
    :cond_0
    const/4 v10, 0x0

    .line 2429
    .local v10, "isLegacyMfacClient":Z
    const-string v12, "e644981a852cc131a4df6d00c182fa07887082d4e02ddfc6242c8e9962f06558"

    .line 2430
    .local v12, "legacyPreset":Ljava/lang/String;
    const-string v11, "com.noknok.android.framework.service"

    .line 2431
    .local v11, "legacyPkgName":Ljava/lang/String;
    const/16 v22, 0x6

    move/from16 v0, v22

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v19, v0

    const/16 v22, 0x0

    const-string v23, "786f6d842e7017f0bbbd9f67f88af54e5bcd038b70c35afe813dd6f1cb852866"

    aput-object v23, v19, v22

    const/16 v22, 0x1

    const-string v23, "c56f2444887926c42c74f164923be50e179d40a0dd03979718b68e617916ef6f"

    aput-object v23, v19, v22

    const/16 v22, 0x2

    const-string v23, "fc905761bb85ec5733e8ca2e27bfc327f73d83bc9c495f457e6e550a1866796d"

    aput-object v23, v19, v22

    const/16 v22, 0x3

    const-string v23, "08af8b5ea703ab3460efd6948e5a8943b95913e70d66f5988dc802e0d72affa3"

    aput-object v23, v19, v22

    const/16 v22, 0x4

    const-string v23, "a707aa388c0068a3ba1e8da0cd129f4a903675d49ad7082f07ab4f2a094d4a5e"

    aput-object v23, v19, v22

    const/16 v22, 0x5

    const-string v23, "b0f28302dd7b64d991639c122eebbdcb3bd97f85535e7b020267f1c7ea3ac076"

    aput-object v23, v19, v22

    .line 2441
    .local v19, "presets":[Ljava/lang/String;
    :try_start_0
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v21

    .line 2443
    .local v21, "uid":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mContext:Landroid/content/Context;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v17

    .line 2444
    .local v17, "pm":Landroid/content/pm/PackageManager;
    move-object/from16 v0, v17

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;

    move-result-object v15

    .line 2445
    .local v15, "packagenames":[Ljava/lang/String;
    array-length v0, v15

    move/from16 v22, v0

    if-nez v22, :cond_1

    .line 2447
    const-string v22, "FingerprintManagerService"

    const-string v23, "isFidoSignature: Invailed package name"

    invoke-static/range {v22 .. v23}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 2448
    const/16 v22, 0x0

    goto :goto_0

    .line 2452
    :cond_1
    const/16 v22, 0x0

    aget-object v22, v15, v22

    const/16 v23, 0x40

    move-object/from16 v0, v17

    move-object/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v9

    .line 2454
    .local v9, "info":Landroid/content/pm/PackageInfo;
    move-object v3, v15

    .local v3, "arr$":[Ljava/lang/String;
    array-length v13, v3

    .local v13, "len$":I
    const/4 v7, 0x0

    .local v7, "i$":I
    :goto_1
    if-ge v7, v13, :cond_3

    aget-object v16, v3, v7

    .line 2455
    .local v16, "pkg":Ljava/lang/String;
    const-string v22, "FingerprintManagerService"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "isFidoSignature:"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2456
    move-object/from16 v0, v16

    invoke-virtual {v11, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_2

    .line 2457
    const/4 v10, 0x1

    .line 2454
    :cond_2
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 2460
    .end local v16    # "pkg":Ljava/lang/String;
    :cond_3
    if-eqz v10, :cond_5

    .line 2461
    iget-object v3, v9, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    .local v3, "arr$":[Landroid/content/pm/Signature;
    array-length v13, v3

    const/4 v7, 0x0

    :goto_2
    if-ge v7, v13, :cond_5

    aget-object v20, v3, v7

    .line 2462
    .local v20, "sig":Landroid/content/pm/Signature;
    invoke-virtual/range {v20 .. v20}, Landroid/content/pm/Signature;->toByteArray()[B

    move-result-object v22

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-direct {v0, v1}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->getSignatureHash([B)Ljava/lang/String;

    move-result-object v6

    .line 2463
    .local v6, "hash":Ljava/lang/String;
    invoke-virtual {v12, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_4

    .line 2464
    const-string v22, "FingerprintManagerService"

    const-string v23, "isFidoSignature: legacy client"

    invoke-static/range {v22 .. v23}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 2465
    const/16 v22, 0x1

    goto/16 :goto_0

    .line 2461
    :cond_4
    add-int/lit8 v7, v7, 0x1

    goto :goto_2

    .line 2469
    .end local v3    # "arr$":[Landroid/content/pm/Signature;
    .end local v6    # "hash":Ljava/lang/String;
    .end local v20    # "sig":Landroid/content/pm/Signature;
    :cond_5
    iget-object v3, v9, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    .restart local v3    # "arr$":[Landroid/content/pm/Signature;
    array-length v13, v3

    const/4 v7, 0x0

    move v8, v7

    .end local v3    # "arr$":[Landroid/content/pm/Signature;
    .end local v7    # "i$":I
    .end local v13    # "len$":I
    .local v8, "i$":I
    :goto_3
    if-ge v8, v13, :cond_8

    aget-object v20, v3, v8

    .line 2470
    .restart local v20    # "sig":Landroid/content/pm/Signature;
    invoke-virtual/range {v20 .. v20}, Landroid/content/pm/Signature;->toByteArray()[B

    move-result-object v22

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-direct {v0, v1}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->getSignatureHash([B)Ljava/lang/String;

    move-result-object v6

    .line 2471
    .restart local v6    # "hash":Ljava/lang/String;
    move-object/from16 v4, v19

    .local v4, "arr$":[Ljava/lang/String;
    array-length v14, v4

    .local v14, "len$":I
    const/4 v7, 0x0

    .end local v8    # "i$":I
    .restart local v7    # "i$":I
    :goto_4
    if-ge v7, v14, :cond_7

    aget-object v18, v4, v7

    .line 2472
    .local v18, "presetone":Ljava/lang/String;
    move-object/from16 v0, v18

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_6

    .line 2473
    const-string v22, "FingerprintManagerService"

    const-string v23, "isFidoSignature: true"

    invoke-static/range {v22 .. v23}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2474
    const/16 v22, 0x1

    goto/16 :goto_0

    .line 2471
    :cond_6
    add-int/lit8 v7, v7, 0x1

    goto :goto_4

    .line 2469
    .end local v18    # "presetone":Ljava/lang/String;
    :cond_7
    add-int/lit8 v7, v8, 0x1

    move v8, v7

    .end local v7    # "i$":I
    .restart local v8    # "i$":I
    goto :goto_3

    .line 2478
    .end local v4    # "arr$":[Ljava/lang/String;
    .end local v6    # "hash":Ljava/lang/String;
    .end local v8    # "i$":I
    .end local v9    # "info":Landroid/content/pm/PackageInfo;
    .end local v14    # "len$":I
    .end local v15    # "packagenames":[Ljava/lang/String;
    .end local v17    # "pm":Landroid/content/pm/PackageManager;
    .end local v20    # "sig":Landroid/content/pm/Signature;
    .end local v21    # "uid":I
    :catch_0
    move-exception v5

    .line 2480
    .local v5, "e":Ljava/lang/Exception;
    const-string v22, "FingerprintManagerService"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "isFidoSignature: "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 2482
    .end local v5    # "e":Ljava/lang/Exception;
    :cond_8
    const/16 v22, 0x0

    goto/16 :goto_0
.end method

.method private isFingerprintLockType()Z
    .locals 4

    .prologue
    .line 422
    new-instance v1, Lcom/android/internal/widget/LockPatternUtils;

    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/android/internal/widget/LockPatternUtils;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1}, Lcom/android/internal/widget/LockPatternUtils;->isLockFingerprintEnabled()Z

    move-result v0

    .line 423
    .local v0, "retVal":Z
    const-string v1, "FingerprintManagerService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isFingerprintLockType return :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 424
    return v0
.end method

.method private isKeyGuardLocked()Z
    .locals 5

    .prologue
    .line 428
    const/4 v1, 0x0

    .line 429
    .local v1, "retVal":Z
    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mContext:Landroid/content/Context;

    const-string v3, "keyguard"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/KeyguardManager;

    .line 430
    .local v0, "kgm":Landroid/app/KeyguardManager;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/KeyguardManager;->isKeyguardLocked()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 431
    const/4 v1, 0x1

    .line 433
    :cond_0
    const-string v2, "FingerprintManagerService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "isKeyGuardLocked return :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 434
    return v1
.end method

.method private isKioskModeEnabled(I)Z
    .locals 5
    .param p1, "userId"    # I

    .prologue
    .line 2671
    const/4 v0, 0x0

    .line 2672
    .local v0, "retVal":Z
    invoke-static {}, Landroid/os/PersonaManager;->getKnoxInfo()Landroid/os/Bundle;

    move-result-object v1

    .line 2673
    .local v1, "versionInfo":Landroid/os/Bundle;
    if-eqz v1, :cond_2

    const-string v2, "2.0"

    const-string v3, "version"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2674
    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mPersonaManager:Landroid/os/PersonaManager;

    if-nez v2, :cond_0

    .line 2675
    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mContext:Landroid/content/Context;

    const-string v3, "persona"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/PersonaManager;

    iput-object v2, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mPersonaManager:Landroid/os/PersonaManager;

    .line 2677
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mPersonaManager:Landroid/os/PersonaManager;

    if-eqz v2, :cond_1

    .line 2678
    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mPersonaManager:Landroid/os/PersonaManager;

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v3

    invoke-static {v3}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/os/PersonaManager;->isKioskModeEnabled(I)Z

    move-result v0

    .line 2680
    :cond_1
    const-string v2, "FingerprintManagerService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "isKioskModeEnabled return :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 2684
    :cond_2
    return v0
.end method

.method private isPackageAVSAuthorized(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 10
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "api"    # Ljava/lang/String;

    .prologue
    const/4 v8, 0x0

    .line 2936
    const-string v1, "FingerprintManagerService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isPackageAVSAuthorized: called packageName="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", API="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 2938
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2966
    :cond_0
    :goto_0
    return v8

    .line 2942
    :cond_1
    const/4 v8, 0x0

    .line 2943
    .local v8, "result":Z
    const/4 v6, 0x0

    .line 2947
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 2948
    .local v0, "cr":Landroid/content/ContentResolver;
    const-string v1, "content://com.verizon.vzwavs.provider/apis"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v3, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 2950
    if-eqz v6, :cond_3

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2951
    const/4 v1, 0x0

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 2952
    .local v9, "str":Ljava/lang/String;
    const-string v1, "FingerprintManagerService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "cursor is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2953
    invoke-virtual {v9, p2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v8

    .line 2961
    .end local v9    # "str":Ljava/lang/String;
    :goto_1
    if-eqz v6, :cond_2

    .line 2962
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 2965
    .end local v0    # "cr":Landroid/content/ContentResolver;
    :cond_2
    :goto_2
    const-string v1, "FingerprintManagerService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isPackageAVSAuthorized: result="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->da(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2955
    .restart local v0    # "cr":Landroid/content/ContentResolver;
    :cond_3
    :try_start_1
    const-string v1, "FingerprintManagerService"

    const-string v2, "cursor is null"

    invoke-static {v1, v2}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 2958
    .end local v0    # "cr":Landroid/content/ContentResolver;
    :catch_0
    move-exception v7

    .line 2959
    .local v7, "e":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2961
    if-eqz v6, :cond_2

    .line 2962
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_2

    .line 2961
    .end local v7    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    if-eqz v6, :cond_4

    .line 2962
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v1
.end method

.method private readSensorHWStatus()V
    .locals 11

    .prologue
    .line 2970
    const/4 v8, 0x3

    sput v8, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mHWStatus:I

    .line 2971
    new-instance v3, Ljava/io/File;

    const-string v8, "/sys/class/fingerprint/fingerprint/type_check"

    invoke-direct {v3, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2972
    .local v3, "file":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 2973
    const/4 v6, 0x0

    .line 2975
    .local v6, "is":Ljava/io/InputStream;
    :try_start_0
    new-instance v7, Ljava/io/FileInputStream;

    invoke-direct {v7, v3}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_a
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_5
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2976
    .end local v6    # "is":Ljava/io/InputStream;
    .local v7, "is":Ljava/io/InputStream;
    const/4 v8, 0x1

    :try_start_1
    new-array v0, v8, [B

    const/4 v8, 0x0

    const/4 v9, -0x1

    aput-byte v9, v0, v8

    .line 2977
    .local v0, "buff":[B
    const/4 v8, 0x0

    const/4 v9, 0x1

    invoke-virtual {v7, v0, v8, v9}, Ljava/io/InputStream;->read([BII)I

    move-result v8

    if-lez v8, :cond_0

    .line 2978
    const/4 v8, 0x0

    aget-byte v8, v0, v8

    const/16 v9, 0x30

    if-ne v8, v9, :cond_2

    .line 2979
    const/4 v8, 0x1

    sput v8, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mHWStatus:I
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_9
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_8
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2991
    :cond_0
    :goto_0
    if-eqz v7, :cond_1

    .line 2993
    :try_start_2
    invoke-virtual {v7}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    .line 3001
    .end local v0    # "buff":[B
    .end local v7    # "is":Ljava/io/InputStream;
    :cond_1
    :goto_1
    const-string v8, "FingerprintManagerService"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "readSensorHWStatus: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    sget v10, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mHWStatus:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->da(Ljava/lang/String;Ljava/lang/String;)V

    .line 3002
    return-void

    .line 2980
    .restart local v0    # "buff":[B
    .restart local v7    # "is":Ljava/io/InputStream;
    :cond_2
    const/4 v8, 0x0

    :try_start_3
    aget-byte v8, v0, v8

    const/16 v9, 0x31

    if-ne v8, v9, :cond_0

    .line 2981
    const/4 v8, 0x2

    sput v8, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mHWStatus:I
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_9
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_8
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_0

    .line 2984
    .end local v0    # "buff":[B
    :catch_0
    move-exception v4

    move-object v6, v7

    .line 2985
    .end local v7    # "is":Ljava/io/InputStream;
    .local v4, "fnfe":Ljava/io/FileNotFoundException;
    .restart local v6    # "is":Ljava/io/InputStream;
    :goto_2
    :try_start_4
    const-string v8, "FingerprintManagerService"

    const-string v9, "readSensorHWStatus: FileNotFoundException"

    invoke-static {v8, v9, v4}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 2991
    if-eqz v6, :cond_1

    .line 2993
    :try_start_5
    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1

    goto :goto_1

    .line 2994
    :catch_1
    move-exception v2

    .line 2995
    .local v2, "e1":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 2994
    .end local v2    # "e1":Ljava/io/IOException;
    .end local v4    # "fnfe":Ljava/io/FileNotFoundException;
    .end local v6    # "is":Ljava/io/InputStream;
    .restart local v0    # "buff":[B
    .restart local v7    # "is":Ljava/io/InputStream;
    :catch_2
    move-exception v2

    .line 2995
    .restart local v2    # "e1":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 2986
    .end local v0    # "buff":[B
    .end local v2    # "e1":Ljava/io/IOException;
    .end local v7    # "is":Ljava/io/InputStream;
    .restart local v6    # "is":Ljava/io/InputStream;
    :catch_3
    move-exception v5

    .line 2987
    .local v5, "ie":Ljava/io/IOException;
    :goto_3
    :try_start_6
    const-string v8, "FingerprintManagerService"

    const-string v9, "readSensorHWStatus: IOException"

    invoke-static {v8, v9, v5}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 2991
    if-eqz v6, :cond_1

    .line 2993
    :try_start_7
    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4

    goto :goto_1

    .line 2994
    :catch_4
    move-exception v2

    .line 2995
    .restart local v2    # "e1":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 2988
    .end local v2    # "e1":Ljava/io/IOException;
    .end local v5    # "ie":Ljava/io/IOException;
    :catch_5
    move-exception v1

    .line 2989
    .local v1, "e":Ljava/lang/Exception;
    :goto_4
    :try_start_8
    const-string v8, "FingerprintManagerService"

    const-string v9, "readSensorHWStatus: Unexpected Exception"

    invoke-static {v8, v9, v1}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 2991
    if-eqz v6, :cond_1

    .line 2993
    :try_start_9
    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_6

    goto :goto_1

    .line 2994
    :catch_6
    move-exception v2

    .line 2995
    .restart local v2    # "e1":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 2991
    .end local v1    # "e":Ljava/lang/Exception;
    .end local v2    # "e1":Ljava/io/IOException;
    :catchall_0
    move-exception v8

    :goto_5
    if-eqz v6, :cond_3

    .line 2993
    :try_start_a
    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_7

    .line 2996
    :cond_3
    :goto_6
    throw v8

    .line 2994
    :catch_7
    move-exception v2

    .line 2995
    .restart local v2    # "e1":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_6

    .line 2991
    .end local v2    # "e1":Ljava/io/IOException;
    .end local v6    # "is":Ljava/io/InputStream;
    .restart local v7    # "is":Ljava/io/InputStream;
    :catchall_1
    move-exception v8

    move-object v6, v7

    .end local v7    # "is":Ljava/io/InputStream;
    .restart local v6    # "is":Ljava/io/InputStream;
    goto :goto_5

    .line 2988
    .end local v6    # "is":Ljava/io/InputStream;
    .restart local v7    # "is":Ljava/io/InputStream;
    :catch_8
    move-exception v1

    move-object v6, v7

    .end local v7    # "is":Ljava/io/InputStream;
    .restart local v6    # "is":Ljava/io/InputStream;
    goto :goto_4

    .line 2986
    .end local v6    # "is":Ljava/io/InputStream;
    .restart local v7    # "is":Ljava/io/InputStream;
    :catch_9
    move-exception v5

    move-object v6, v7

    .end local v7    # "is":Ljava/io/InputStream;
    .restart local v6    # "is":Ljava/io/InputStream;
    goto :goto_3

    .line 2984
    :catch_a
    move-exception v4

    goto :goto_2
.end method

.method private registerFpsReceiver()V
    .locals 4

    .prologue
    .line 333
    const-string v2, "FingerprintManagerService"

    const-string v3, "register receiver called."

    invoke-static {v2, v3}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 334
    new-instance v0, Landroid/content/IntentFilter;

    const-string v2, "android.intent.action.SCREEN_OFF"

    invoke-direct {v0, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 335
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v2, "android.intent.action.SCREEN_ON"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 336
    const-string v2, "android.intent.action.CLOSE_SYSTEM_DIALOGS"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 337
    const-string v2, "android.intent.action.USER_PRESENT"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 338
    const-string v2, "android.intent.action.USER_REMOVED"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 339
    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mFpsReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v2, v3, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 341
    const-string v2, "VZW"

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 342
    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "android.intent.action.PACKAGE_REMOVED"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 343
    .local v1, "filterPkg":Landroid/content/IntentFilter;
    const-string v2, "package"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 344
    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mFpsReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v2, v3, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 347
    .end local v1    # "filterPkg":Landroid/content/IntentFilter;
    :cond_0
    return-void
.end method

.method private removeEnrolledApp(Ljava/lang/String;)Z
    .locals 8
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1204
    const-string v5, "FingerprintManagerService"

    const-string v6, "removeEnrolledApp"

    invoke-static {v5, v6}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1207
    iget-object v5, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mContext:Landroid/content/Context;

    const-string v6, "com.samsung.android.permission.ENROLL_FINGERPRINT"

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    .line 1210
    const-string v5, "FingerprintManagerService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "removeEnrolledApp: package name="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1212
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v5

    if-nez v5, :cond_2

    .line 1213
    :cond_0
    const-string v4, "FingerprintManagerService"

    const-string v5, "removeEnrolledApp: invalid package name"

    invoke-static {v4, v5}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1238
    :cond_1
    :goto_0
    return v3

    .line 1218
    :cond_2
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "FPMS_"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-direct {p0}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->getUserId()I

    move-result v6

    invoke-static {v6, p1}, Lcom/samsung/android/fingerprint/service/util/ValueUtils;->getStringUserId(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1220
    .local v2, "stringUserId":Ljava/lang/String;
    iget-object v5, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mFactory:Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommandFactory;

    const-string v6, "removeAllEnrolledFingers"

    new-array v7, v4, [Ljava/lang/Object;

    aput-object v2, v7, v3

    invoke-virtual {v5, v6, v7}, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommandFactory;->create(Ljava/lang/String;[Ljava/lang/Object;)Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;

    move-result-object v0

    .line 1224
    .local v0, "cmd":Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;
    iget-object v5, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mSilentClient:Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

    if-nez v5, :cond_3

    .line 1225
    const-string v4, "FingerprintManagerService"

    const-string v5, "removeEnrolledApp: Fingerprint msc is not ready"

    invoke-static {v4, v5}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1229
    :cond_3
    iget-object v5, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mSilentClient:Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

    invoke-direct {p0, v5, v0}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->enqueueCommand(Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 1230
    .local v1, "result":Ljava/lang/Integer;
    if-nez v1, :cond_4

    .line 1231
    const-string v4, "FingerprintManagerService"

    const-string v5, "enqueueCommand result is null"

    invoke-static {v4, v5}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1235
    :cond_4
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v5

    if-nez v5, :cond_1

    move v3, v4

    .line 1236
    goto :goto_0
.end method

.method private removeEnrolledUser(I)Z
    .locals 10
    .param p1, "id"    # I

    .prologue
    const/4 v9, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1242
    const-string v1, "removeEnrolledUser"

    .line 1244
    .local v1, "functionName":Ljava/lang/String;
    const-string v6, "FingerprintManagerService"

    const-string v7, "removeEnrolledUser"

    invoke-static {v6, v7}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1247
    iget-object v6, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mContext:Landroid/content/Context;

    const-string v7, "com.samsung.android.permission.ENROLL_FINGERPRINT"

    invoke-virtual {v6, v7, v9}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    .line 1250
    const-string v6, "FingerprintManagerService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "removeEnrolledUser: user id="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1252
    if-gez p1, :cond_1

    .line 1253
    const-string v5, "FingerprintManagerService"

    const-string v6, "removeEnrolledUser: invalid id"

    invoke-static {v5, v6}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1277
    :cond_0
    :goto_0
    return v4

    .line 1257
    :cond_1
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "FPMS_"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {p1, v9}, Lcom/samsung/android/fingerprint/service/util/ValueUtils;->getStringUserId(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1259
    .local v3, "stringUserId":Ljava/lang/String;
    iget-object v6, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mFactory:Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommandFactory;

    const-string v7, "removeAllEnrolledFingers"

    new-array v8, v5, [Ljava/lang/Object;

    aput-object v3, v8, v4

    invoke-virtual {v6, v7, v8}, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommandFactory;->create(Ljava/lang/String;[Ljava/lang/Object;)Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;

    move-result-object v0

    .line 1263
    .local v0, "cmd":Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;
    iget-object v6, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mSilentClient:Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

    if-nez v6, :cond_2

    .line 1264
    const-string v5, "FingerprintManagerService"

    const-string v6, "removeEnrolledUser: Fingerprint msc is not ready"

    invoke-static {v5, v6}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1268
    :cond_2
    iget-object v6, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mSilentClient:Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

    invoke-direct {p0, v6, v0}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->enqueueCommand(Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    .line 1269
    .local v2, "result":Ljava/lang/Integer;
    if-nez v2, :cond_3

    .line 1270
    const-string v5, "FingerprintManagerService"

    const-string v6, "enqueueCommand result is null"

    invoke-static {v5, v6}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1274
    :cond_3
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v6

    if-nez v6, :cond_0

    move v4, v5

    .line 1275
    goto :goto_0
.end method

.method private requestChangePriority()V
    .locals 2

    .prologue
    const/16 v1, 0x226

    .line 399
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mH:Lcom/samsung/android/fingerprint/service/FingerprintManagerService$H;

    invoke-virtual {v0, v1}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService$H;->removeMessages(I)V

    .line 400
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mH:Lcom/samsung/android/fingerprint/service/FingerprintManagerService$H;

    invoke-virtual {v0, v1}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService$H;->sendEmptyMessage(I)Z

    .line 401
    return-void
.end method

.method private requestNotifyFingerAdded(II)V
    .locals 5
    .param p1, "fingerIdx"    # I
    .param p2, "veriType"    # I

    .prologue
    const/16 v4, 0x2c9

    .line 465
    const-string v1, "FingerprintManagerService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "requestNotifyFingerAdded: Idx="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", Type="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 466
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mH:Lcom/samsung/android/fingerprint/service/FingerprintManagerService$H;

    invoke-virtual {v1, v4}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService$H;->removeMessages(I)V

    .line 467
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mH:Lcom/samsung/android/fingerprint/service/FingerprintManagerService$H;

    invoke-virtual {v1, v4, p1, p2}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService$H;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    .line 468
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mH:Lcom/samsung/android/fingerprint/service/FingerprintManagerService$H;

    invoke-virtual {v1, v0}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService$H;->sendMessage(Landroid/os/Message;)Z

    .line 469
    return-void
.end method

.method private requestNotifyFingerPasswordUpdated()V
    .locals 3

    .prologue
    const/16 v2, 0x2ca

    .line 472
    const-string v0, "FingerprintManagerService"

    const-string v1, "requestNotifyFingerPasswordUpdated"

    invoke-static {v0, v1}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 473
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mH:Lcom/samsung/android/fingerprint/service/FingerprintManagerService$H;

    invoke-virtual {v0, v2}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService$H;->removeMessages(I)V

    .line 474
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mH:Lcom/samsung/android/fingerprint/service/FingerprintManagerService$H;

    invoke-virtual {v0, v2}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService$H;->sendEmptyMessage(I)Z

    .line 475
    return-void
.end method

.method private requestNotifyFingerRemoved(II)V
    .locals 3
    .param p1, "fingerIdx"    # I
    .param p2, "enrolledFingerNum"    # I

    .prologue
    const/16 v2, 0x2c8

    .line 459
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mH:Lcom/samsung/android/fingerprint/service/FingerprintManagerService$H;

    invoke-virtual {v1, v2}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService$H;->removeMessages(I)V

    .line 460
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mH:Lcom/samsung/android/fingerprint/service/FingerprintManagerService$H;

    invoke-virtual {v1, v2, p1, p2}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService$H;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    .line 461
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mH:Lcom/samsung/android/fingerprint/service/FingerprintManagerService$H;

    invoke-virtual {v1, v0}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService$H;->sendMessage(Landroid/os/Message;)Z

    .line 462
    return-void
.end method

.method private sendServiceStartedBroadcast()V
    .locals 4

    .prologue
    .line 525
    const-string v2, "FingerprintManagerService"

    const-string v3, "sendServiceStartedBroadcast: called."

    invoke-static {v2, v3}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 528
    :try_start_0
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.samsung.android.fingerprint.action.SERVICE_STARTED"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 529
    .local v1, "intent":Landroid/content/Intent;
    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mContext:Landroid/content/Context;

    sget-object v3, Landroid/os/UserHandle;->OWNER:Landroid/os/UserHandle;

    invoke-virtual {v2, v1, v3}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 533
    .end local v1    # "intent":Landroid/content/Intent;
    :goto_0
    return-void

    .line 530
    :catch_0
    move-exception v0

    .line 531
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "FingerprintManagerService"

    const-string v3, "sendServiceStartedBroadcast failed: "

    invoke-static {v2, v3, v0}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private sensorWarningToast(I)V
    .locals 2
    .param p1, "stringId"    # I

    .prologue
    .line 1932
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 1933
    .local v0, "handler":Landroid/os/Handler;
    new-instance v1, Lcom/samsung/android/fingerprint/service/FingerprintManagerService$4;

    invoke-direct {v1, p0, p1}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService$4;-><init>(Lcom/samsung/android/fingerprint/service/FingerprintManagerService;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1951
    return-void
.end method

.method private setSPValue(ILjava/lang/String;Landroid/content/Context;)V
    .locals 2
    .param p1, "idx"    # I
    .param p2, "value"    # Ljava/lang/String;
    .param p3, "context"    # Landroid/content/Context;

    .prologue
    .line 1773
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 1774
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    if-nez p2, :cond_0

    .line 1775
    invoke-direct {p0, p1}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->getSPKey(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 1780
    :goto_0
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1781
    return-void

    .line 1778
    :cond_0
    invoke-direct {p0, p1}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->getSPKey(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    goto :goto_0
.end method

.method public static setSensorReady(Z)V
    .locals 0
    .param p0, "isReady"    # Z

    .prologue
    .line 3117
    sput-boolean p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mSensorReady:Z

    .line 3118
    return-void
.end method

.method private unregisterFpsReceiver()V
    .locals 3

    .prologue
    .line 351
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mContext:Landroid/content/Context;

    if-eqz v1, :cond_0

    .line 352
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mFpsReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 357
    :cond_0
    :goto_0
    return-void

    .line 354
    :catch_0
    move-exception v0

    .line 355
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private updateFingerprintNames()V
    .locals 19

    .prologue
    .line 1788
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mContext:Landroid/content/Context;

    move-object/from16 v17, v0

    if-nez v17, :cond_1

    .line 1789
    const-string v17, "FingerprintManagerService"

    const-string v18, "updateFingerprintNames: mContext is null"

    invoke-static/range {v17 .. v18}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1838
    :cond_0
    :goto_0
    return-void

    .line 1793
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mPreferences:Landroid/content/SharedPreferences;

    move-object/from16 v17, v0

    if-nez v17, :cond_2

    .line 1794
    const-string v17, "FingerprintManagerService"

    const-string v18, "updateFingerprintNames: preferences is null"

    invoke-static/range {v17 .. v18}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1798
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mPreferences:Landroid/content/SharedPreferences;

    move-object/from16 v17, v0

    invoke-interface/range {v17 .. v17}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    move-result-object v11

    .line 1799
    .local v11, "keys":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;*>;"
    if-nez v11, :cond_3

    .line 1800
    const-string v17, "FingerprintManagerService"

    const-string v18, "updateFingerprintNames: keys is null"

    invoke-static/range {v17 .. v18}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1804
    :cond_3
    const/4 v13, 0x0

    .line 1805
    .local v13, "needUpdate":Z
    invoke-interface {v11}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v17

    invoke-interface/range {v17 .. v17}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, "i$":Ljava/util/Iterator;
    :cond_4
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-eqz v17, :cond_5

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map$Entry;

    .line 1806
    .local v4, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;*>;"
    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    .line 1807
    .local v10, "idx":Ljava/lang/String;
    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v17

    const/16 v18, 0x1

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_4

    .line 1808
    const/4 v13, 0x1

    .line 1813
    .end local v4    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;*>;"
    .end local v10    # "idx":Ljava/lang/String;
    :cond_5
    if-eqz v13, :cond_0

    .line 1814
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mPreferences:Landroid/content/SharedPreferences;

    move-object/from16 v17, v0

    invoke-interface/range {v17 .. v17}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    .line 1815
    .local v3, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    .line 1816
    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1818
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mContext:Landroid/content/Context;

    move-object/from16 v17, v0

    const-string v18, "user"

    invoke-virtual/range {v17 .. v18}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Landroid/os/UserManager;

    .line 1819
    .local v14, "um":Landroid/os/UserManager;
    const/16 v17, 0x1

    move/from16 v0, v17

    invoke-virtual {v14, v0}, Landroid/os/UserManager;->getUsers(Z)Ljava/util/List;

    move-result-object v16

    .line 1820
    .local v16, "users":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/UserInfo;>;"
    invoke-interface/range {v16 .. v16}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .end local v7    # "i$":Ljava/util/Iterator;
    :cond_6
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-eqz v17, :cond_9

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Landroid/content/pm/UserInfo;

    .line 1821
    .local v15, "user":Landroid/content/pm/UserInfo;
    iget v0, v15, Landroid/content/pm/UserInfo;->id:I

    move/from16 v17, v0

    const/16 v18, 0x64

    move/from16 v0, v17

    move/from16 v1, v18

    if-ge v0, v1, :cond_6

    .line 1822
    iget v0, v15, Landroid/content/pm/UserInfo;->id:I

    move/from16 v17, v0

    const/16 v18, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v17

    move-object/from16 v2, v18

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->getEnrolledFingers(ILjava/lang/String;)Ljava/util/ArrayList;

    move-result-object v6

    .line 1823
    .local v6, "fingers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    if-eqz v6, :cond_6

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v17

    if-lez v17, :cond_6

    .line 1824
    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_7
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-eqz v17, :cond_6

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    .line 1825
    .local v5, "finger":Ljava/lang/Integer;
    invoke-interface {v11}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v17

    invoke-interface/range {v17 .. v17}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .local v9, "i$":Ljava/util/Iterator;
    :cond_8
    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-eqz v17, :cond_7

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map$Entry;

    .line 1826
    .restart local v4    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;*>;"
    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    .line 1827
    .restart local v10    # "idx":Ljava/lang/String;
    invoke-virtual {v5}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_8

    .line 1828
    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v12

    .line 1829
    .local v12, "name":Ljava/lang/String;
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    iget v0, v15, Landroid/content/pm/UserInfo;->id:I

    move/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, "_"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-interface {v3, v0, v12}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    goto :goto_1

    .line 1836
    .end local v4    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;*>;"
    .end local v5    # "finger":Ljava/lang/Integer;
    .end local v6    # "fingers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .end local v9    # "i$":Ljava/util/Iterator;
    .end local v10    # "idx":Ljava/lang/String;
    .end local v12    # "name":Ljava/lang/String;
    .end local v15    # "user":Landroid/content/pm/UserInfo;
    :cond_9
    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto/16 :goto_0
.end method


# virtual methods
.method public addPermission(Ljava/lang/String;Ljava/lang/String;I)Z
    .locals 9
    .param p1, "appName"    # Ljava/lang/String;
    .param p2, "permissionName"    # Ljava/lang/String;
    .param p3, "fingerIndex"    # I

    .prologue
    const/4 v8, 0x1

    const/4 v3, 0x0

    .line 1535
    const-string v4, "FingerprintManagerService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "addPermission: appName="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", permissionName="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", fingerIndex="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1539
    invoke-direct {p0}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->getUserId()I

    move-result v2

    .line 1543
    .local v2, "userId":I
    iget-object v4, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mContext:Landroid/content/Context;

    const-string v5, "com.samsung.android.permission.ENROLL_FINGERPRINT"

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    .line 1546
    if-nez p1, :cond_0

    .line 1547
    const-string v4, "FingerprintManagerService"

    const-string v5, "addPermission: appName may not be null"

    invoke-static {v4, v5}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1580
    :goto_0
    return v3

    .line 1551
    :cond_0
    if-nez p2, :cond_1

    .line 1552
    const-string v4, "FingerprintManagerService"

    const-string v5, "addPermission: permissionName may not be null"

    invoke-static {v4, v5}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1556
    :cond_1
    const/16 v4, 0xb

    if-lt p3, v4, :cond_2

    const/16 v4, 0x14

    if-gt p3, v4, :cond_2

    .line 1558
    const-string v4, "FingerprintManagerService"

    const-string v5, "addPermission: not necessary for 2nd enroll"

    invoke-static {v4, v5}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->da(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1562
    :cond_2
    if-lt p3, v8, :cond_3

    const/16 v4, 0xa

    if-le p3, v4, :cond_4

    .line 1564
    :cond_3
    const-string v4, "FingerprintManagerService"

    const-string v5, "addPermission: wrong fingerIndex was given"

    invoke-static {v4, v5}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1568
    :cond_4
    iget-object v4, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mFactory:Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommandFactory;

    const-string v5, "addPermission"

    const/4 v6, 0x4

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v3

    aput-object p1, v6, v8

    const/4 v7, 0x2

    aput-object p2, v6, v7

    const/4 v7, 0x3

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v4, v5, v6}, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommandFactory;->create(Ljava/lang/String;[Ljava/lang/Object;)Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;

    move-result-object v0

    .line 1575
    .local v0, "cmd":Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;
    iget-object v4, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mSilentClient:Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

    invoke-direct {p0, v4, v0}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->enqueueCommand(Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;)Ljava/lang/Object;

    move-result-object v1

    .line 1576
    .local v1, "retObj":Ljava/lang/Object;
    if-nez v1, :cond_5

    .line 1577
    const-string v4, "FingerprintManagerService"

    const-string v5, "enqueueCommand result is null"

    invoke-static {v4, v5}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1580
    :cond_5
    check-cast v1, Ljava/lang/Boolean;

    .end local v1    # "retObj":Ljava/lang/Object;
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    goto :goto_0
.end method

.method public attachClient(Landroid/os/IBinder;Lcom/samsung/android/fingerprint/IFingerprintClient;)Landroid/os/IBinder;
    .locals 8
    .param p1, "token"    # Landroid/os/IBinder;
    .param p2, "client"    # Lcom/samsung/android/fingerprint/IFingerprintClient;

    .prologue
    const/4 v4, 0x0

    .line 1427
    invoke-static {p2}, Lcom/samsung/android/fingerprint/service/util/ClientInfoHelper;->simplifyObjectInfo(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 1428
    .local v1, "clientInfo":Ljava/lang/String;
    invoke-static {p1}, Lcom/samsung/android/fingerprint/service/util/ClientInfoHelper;->simplifyObjectInfo(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 1430
    .local v3, "tokenInfo":Ljava/lang/String;
    const-string v5, "FingerprintManagerService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "attachClient: token="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", client="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " from pid="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1434
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 1435
    :cond_0
    const-string v5, "FingerprintManagerService"

    const-string v6, "attachClient: both token and client may not be null."

    invoke-static {v5, v6}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v4

    .line 1460
    :goto_0
    return-object v0

    .line 1439
    :cond_1
    monitor-enter p0

    .line 1440
    :try_start_0
    invoke-interface {p2}, Lcom/samsung/android/fingerprint/IFingerprintClient;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    .line 1442
    .local v0, "attachedToken":Landroid/os/IBinder;
    invoke-direct {p0, p1}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->findClientRecord(Landroid/os/IBinder;)Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

    move-result-object v2

    .line 1444
    .local v2, "record":Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;
    if-nez v2, :cond_2

    .line 1445
    const-string v5, "FingerprintManagerService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "attachClient: invalid token("

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ") was given."

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1446
    monitor-exit p0

    move-object v0, v4

    goto :goto_0

    .line 1449
    :cond_2
    iget-object v5, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mFocusedClient:Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

    if-eq v5, v2, :cond_3

    .line 1450
    const-string v5, "FingerprintManagerService"

    const-string v6, "attachClient: This method is supported only for focused client."

    invoke-static {v5, v6}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1451
    monitor-exit p0

    move-object v0, v4

    goto :goto_0

    .line 1454
    :cond_3
    invoke-virtual {v2, p2}, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->register(Lcom/samsung/android/fingerprint/IFingerprintClient;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 1455
    iget-object v4, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mTokenMap:Ljava/util/Map;

    invoke-interface {v4, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1456
    monitor-exit p0

    goto :goto_0

    .line 1458
    .end local v0    # "attachedToken":Landroid/os/IBinder;
    .end local v2    # "record":Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;
    :catchall_0
    move-exception v4

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4

    .restart local v0    # "attachedToken":Landroid/os/IBinder;
    .restart local v2    # "record":Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;
    :cond_4
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object v0, v4

    .line 1460
    goto :goto_0
.end method

.method public cancel(Landroid/os/IBinder;)I
    .locals 8
    .param p1, "token"    # Landroid/os/IBinder;

    .prologue
    const/4 v3, -0x1

    .line 663
    invoke-static {p1}, Lcom/samsung/android/fingerprint/service/util/ClientInfoHelper;->simplifyObjectInfo(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 665
    .local v2, "tokenInfo":Ljava/lang/String;
    const-string v4, "FingerprintManagerService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "cancel: token="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " from pid="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 669
    invoke-direct {p0, p1}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->findClientRecord(Landroid/os/IBinder;)Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

    move-result-object v1

    .line 671
    .local v1, "record":Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;
    if-nez v1, :cond_0

    .line 672
    const-string v3, "FingerprintManagerService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "cancel: invalid token("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ") was given. return Invalid token"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 673
    const/4 v3, -0x3

    .line 699
    :goto_0
    return v3

    .line 683
    :cond_0
    iget-object v4, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mFocusedClient:Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

    if-eq v4, v1, :cond_1

    .line 684
    const-string v4, "FingerprintManagerService"

    const-string v5, "cancel: Given ClientRecord is not FocusedClient, so your cancel request is skipped."

    invoke-static {v4, v5}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 688
    :cond_1
    invoke-virtual {v1}, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->getCommand()Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;

    move-result-object v0

    .line 689
    .local v0, "cmd":Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;
    if-nez v0, :cond_2

    .line 690
    const-string v4, "FingerprintManagerService"

    const-string v5, "cancel: Given ClientRecord doesn\'t have any commands, so your cancel request is skipped."

    invoke-static {v4, v5}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 693
    :cond_2
    invoke-virtual {v0}, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;->requestCancel()Z

    move-result v4

    if-nez v4, :cond_3

    .line 694
    const-string v4, "FingerprintManagerService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "cancel: Failed to cancel ["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v0, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;->commandName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "] command"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 696
    :cond_3
    iget-object v3, v0, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;->commandName:Ljava/lang/String;

    const-string v4, "enroll"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 697
    const-string v3, "FPEF"

    iget v4, v0, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;->fingerIndex:I

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    const-wide/16 v6, -0x1

    invoke-direct {p0, v3, v4, v6, v7}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->insertSurveyLog(Ljava/lang/String;Ljava/lang/String;J)V

    .line 699
    :cond_4
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public cleanUpService()V
    .locals 2

    .prologue
    .line 321
    invoke-direct {p0}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->unregisterFpsReceiver()V

    .line 322
    sget-object v0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->sInstance:Lcom/samsung/android/fingerprint/IFingerprintManager$Stub;

    if-eqz v0, :cond_0

    .line 323
    sget-object v1, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->sInstance:Lcom/samsung/android/fingerprint/IFingerprintManager$Stub;

    monitor-enter v1

    .line 324
    const/4 v0, 0x0

    :try_start_0
    sput-object v0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->sInstance:Lcom/samsung/android/fingerprint/IFingerprintManager$Stub;

    .line 325
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 327
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_1

    .line 328
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/samsung/android/fingerprint/service/FingerprintServiceStarter;

    invoke-virtual {v0}, Lcom/samsung/android/fingerprint/service/FingerprintServiceStarter;->stopSelf()V

    .line 330
    :cond_1
    return-void

    .line 325
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public closeTransaction(Landroid/os/IBinder;)Z
    .locals 1
    .param p1, "token"    # Landroid/os/IBinder;

    .prologue
    .line 1627
    const/4 v0, 0x1

    return v0
.end method

.method public detachClient(Landroid/os/IBinder;)Z
    .locals 6
    .param p1, "token"    # Landroid/os/IBinder;

    .prologue
    const/4 v2, 0x0

    .line 1464
    invoke-static {p1}, Lcom/samsung/android/fingerprint/service/util/ClientInfoHelper;->simplifyObjectInfo(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 1466
    .local v1, "tokenInfo":Ljava/lang/String;
    const-string v3, "FingerprintManagerService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "detachClient: token="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " from pid="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1469
    if-nez p1, :cond_0

    .line 1470
    const-string v3, "FingerprintManagerService"

    const-string v4, "detachClient: token may not be null."

    invoke-static {v3, v4}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1488
    :goto_0
    return v2

    .line 1474
    :cond_0
    monitor-enter p0

    .line 1475
    :try_start_0
    invoke-direct {p0, p1}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->findClientRecord(Landroid/os/IBinder;)Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

    move-result-object v0

    .line 1477
    .local v0, "record":Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;
    if-nez v0, :cond_1

    .line 1478
    const-string v3, "FingerprintManagerService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "detachClient: invalid token("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ") was given."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1479
    monitor-exit p0

    goto :goto_0

    .line 1486
    .end local v0    # "record":Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;
    :catchall_0
    move-exception v2

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 1482
    .restart local v0    # "record":Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;
    :cond_1
    :try_start_1
    invoke-virtual {v0, p1}, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->unregister(Landroid/os/IBinder;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1483
    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mTokenMap:Ljava/util/Map;

    invoke-interface {v2, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1484
    const/4 v2, 0x1

    monitor-exit p0

    goto :goto_0

    .line 1486
    :cond_2
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method protected dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 8
    .param p1, "fd"    # Ljava/io/FileDescriptor;
    .param p2, "pw"    # Ljava/io/PrintWriter;
    .param p3, "args"    # [Ljava/lang/String;

    .prologue
    .line 2164
    iget-object v6, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mContext:Landroid/content/Context;

    const-string v7, "android.permission.DUMP"

    invoke-virtual {v6, v7}, Landroid/content/Context;->checkCallingPermission(Ljava/lang/String;)I

    move-result v6

    if-eqz v6, :cond_0

    .line 2165
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Permission Denial: can\'t dump FingerprintManagerService from from pid="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", uid="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " without permission "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "android.permission.DUMP"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p2, v6}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 2220
    :goto_0
    return-void

    .line 2174
    :cond_0
    const-string v6, "FingerprintManagerService"

    invoke-virtual {p2, v6}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 2175
    const-string v6, "==========================================================================="

    invoke-virtual {p2, v6}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 2176
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Service Version : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-direct {p0}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->getServiceVersion()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p2, v6}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 2178
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Impl Version : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mImplVersion:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p2, v6}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 2179
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Lirbary Version : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mLibraryVersion:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p2, v6}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 2180
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Count of initialization fails : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mSensorChecker:Lcom/samsung/android/fingerprint/service/FingerprintManagerService$SensorReadyCheckScheduler;

    invoke-virtual {v7}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService$SensorReadyCheckScheduler;->getFailedCount()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p2, v6}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 2182
    iget-object v6, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mImpl:Lcom/samsung/android/fingerprint/service/FingerprintLibraryImplAbstractClass;

    if-nez v6, :cond_1

    .line 2183
    const-string v6, "No specific fingerprint library implementation exists."

    invoke-virtual {p2, v6}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto :goto_0

    .line 2187
    :cond_1
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v4

    .line 2189
    .local v4, "origId":J
    invoke-virtual {p2}, Ljava/io/PrintWriter;->println()V

    .line 2190
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "  Focused Client: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mFocusedClient:Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p2, v6}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 2191
    iget-object v6, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mFocusedClient:Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

    if-eqz v6, :cond_2

    .line 2192
    iget-object v6, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mFocusedClient:Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

    invoke-virtual {v6}, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->getCommand()Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;

    move-result-object v0

    .line 2194
    .local v0, "command":Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;
    if-eqz v0, :cond_2

    .line 2195
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "    Running command { name="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, v0, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;->commandName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " }"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p2, v6}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 2198
    .end local v0    # "command":Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;
    :cond_2
    invoke-virtual {p2}, Ljava/io/PrintWriter;->println()V

    .line 2199
    const-string v6, "  Active client records:"

    invoke-virtual {p2, v6}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 2201
    iget-object v6, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mClientList:Ljava/util/List;

    if-eqz v6, :cond_3

    iget-object v6, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mClientList:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    if-nez v6, :cond_5

    .line 2202
    :cond_3
    const-string v6, "    None."

    invoke-virtual {p2, v6}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 2203
    invoke-virtual {p2}, Ljava/io/PrintWriter;->println()V

    .line 2213
    :cond_4
    iget-object v6, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mImpl:Lcom/samsung/android/fingerprint/service/FingerprintLibraryImplAbstractClass;

    invoke-virtual {v6, p2}, Lcom/samsung/android/fingerprint/service/FingerprintLibraryImplAbstractClass;->dump(Ljava/io/PrintWriter;)V

    .line 2215
    invoke-virtual {p2}, Ljava/io/PrintWriter;->println()V

    .line 2217
    invoke-virtual {p2}, Ljava/io/PrintWriter;->flush()V

    .line 2219
    invoke-static {v4, v5}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    goto/16 :goto_0

    .line 2205
    :cond_5
    const/4 v2, 0x1

    .line 2206
    .local v2, "index":I
    iget-object v6, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mClientList:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

    .line 2207
    .local v3, "record":Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "  * Client #"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ": "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p2, v6}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 2208
    invoke-virtual {v3, p2}, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->dump(Ljava/io/PrintWriter;)V

    .line 2209
    add-int/lit8 v2, v2, 0x1

    .line 2210
    goto :goto_1
.end method

.method public enroll(Landroid/os/IBinder;Ljava/lang/String;I)I
    .locals 2
    .param p1, "token"    # Landroid/os/IBinder;
    .param p2, "permissionName"    # Ljava/lang/String;
    .param p3, "fingerIndex"    # I

    .prologue
    .line 703
    const-string v0, "FingerprintManagerService"

    const-string v1, "enroll: called"

    invoke-static {v0, v1}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 704
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->moreEnroll(Landroid/os/IBinder;Ljava/lang/String;II)I

    move-result v0

    return v0
.end method

.method public enrollForMultiUser(Landroid/os/IBinder;ILjava/lang/String;I)I
    .locals 11
    .param p1, "token"    # Landroid/os/IBinder;
    .param p2, "userId"    # I
    .param p3, "permissionName"    # Ljava/lang/String;
    .param p4, "fingerIndex"    # I

    .prologue
    .line 920
    invoke-static {p1}, Lcom/samsung/android/fingerprint/service/util/ClientInfoHelper;->simplifyObjectInfo(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 922
    .local v5, "tokenInfo":Ljava/lang/String;
    const-string v6, "FingerprintManagerService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "enrollForMultiUser: userId="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", token="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " from pid="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 926
    iget-object v6, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mContext:Landroid/content/Context;

    const-string v7, "com.samsung.android.permission.ENROLL_FINGERPRINT"

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    .line 929
    invoke-direct {p0, p1}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->findClientRecord(Landroid/os/IBinder;)Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

    move-result-object v2

    .line 931
    .local v2, "record":Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;
    if-nez v2, :cond_0

    .line 932
    const-string v6, "FingerprintManagerService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "enrollForMultiUser: invalid token("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ") was given."

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 933
    const/4 v6, -0x1

    .line 969
    :goto_0
    return v6

    .line 936
    :cond_0
    invoke-virtual {v2}, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->getAppName()Ljava/lang/String;

    move-result-object v6

    if-nez v6, :cond_1

    .line 937
    const-string v6, "FingerprintManagerService"

    const-string v7, "enrollForMultiUser: appName may not be null"

    invoke-static {v6, v7}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 938
    const/4 v6, -0x1

    goto :goto_0

    .line 941
    :cond_1
    if-nez p3, :cond_2

    .line 942
    const-string v6, "FingerprintManagerService"

    const-string v7, "enrollForMultiUser: permissionName may not be null"

    invoke-static {v6, v7}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 943
    const/4 v6, -0x1

    goto :goto_0

    .line 946
    :cond_2
    const/4 v6, 0x1

    if-lt p4, v6, :cond_3

    const/16 v6, 0x14

    if-le p4, v6, :cond_4

    .line 947
    :cond_3
    const-string v6, "FingerprintManagerService"

    const-string v7, "enrollForMultiUser: wrong fingerIndex was given"

    invoke-static {v6, v7}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 948
    const/4 v6, -0x1

    goto :goto_0

    .line 951
    :cond_4
    iget-object v6, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mImpl:Lcom/samsung/android/fingerprint/service/FingerprintLibraryImplAbstractClass;

    invoke-virtual {v6}, Lcom/samsung/android/fingerprint/service/FingerprintLibraryImplAbstractClass;->isSensorReady()Z

    move-result v6

    if-nez v6, :cond_5

    .line 952
    const-string v6, "FingerprintManagerService"

    const-string v7, "Sensor is not ready!!!"

    invoke-static {v6, v7}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 953
    const/4 v6, -0x1

    goto :goto_0

    .line 956
    :cond_5
    invoke-virtual {v2}, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->getAppName()Ljava/lang/String;

    move-result-object v0

    .line 957
    .local v0, "appName":Ljava/lang/String;
    invoke-virtual {v2}, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->getOwnName()Ljava/lang/String;

    move-result-object v6

    invoke-static {p2, v6}, Lcom/samsung/android/fingerprint/service/util/ValueUtils;->getStringUserId(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 959
    .local v4, "stringUserId":Ljava/lang/String;
    iget-object v6, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mFactory:Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommandFactory;

    const-string v7, "enroll"

    const/4 v8, 0x5

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v4, v8, v9

    const/4 v9, 0x1

    aput-object v0, v8, v9

    const/4 v9, 0x2

    aput-object p3, v8, v9

    const/4 v9, 0x3

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x4

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-virtual {v6, v7, v8}, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommandFactory;->create(Ljava/lang/String;[Ljava/lang/Object;)Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;

    move-result-object v1

    .line 967
    .local v1, "cmd":Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;
    invoke-direct {p0, v2, v1}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->enqueueCommand(Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    .line 969
    .local v3, "result":Ljava/lang/Integer;
    if-nez v3, :cond_6

    const/4 v6, -0x1

    goto :goto_0

    :cond_6
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v6

    goto :goto_0
.end method

.method public finishIdentify(Landroid/os/IBinder;Ljava/lang/String;)Z
    .locals 8
    .param p1, "token"    # Landroid/os/IBinder;
    .param p2, "password"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 896
    invoke-direct {p0}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->getUserId()I

    move-result v3

    .line 898
    .local v3, "userId":I
    invoke-static {p1}, Lcom/samsung/android/fingerprint/service/util/ClientInfoHelper;->simplifyObjectInfo(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 900
    .local v2, "tokenInfo":Ljava/lang/String;
    const-string v5, "FingerprintManagerService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "finishIdentify: userId="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", token="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " from pid="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 902
    invoke-direct {p0, p1}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->findClientRecord(Landroid/os/IBinder;)Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

    move-result-object v1

    .line 904
    .local v1, "record":Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;
    if-nez v1, :cond_0

    .line 905
    const-string v5, "FingerprintManagerService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "finishIdentify: invalid token("

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ") was given."

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 916
    :goto_0
    return v4

    .line 909
    :cond_0
    invoke-virtual {v1}, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->getCommand()Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;

    move-result-object v0

    .line 911
    .local v0, "cmd":Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;
    iget-object v5, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mFocusedClient:Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

    if-ne v5, v1, :cond_1

    if-nez v0, :cond_2

    .line 912
    :cond_1
    const-string v5, "FingerprintManagerService"

    const-string v6, "finishIdentify: should be called during identify!!!"

    invoke-static {v5, v6}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 916
    :cond_2
    invoke-virtual {v0, v4, v3, p2}, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;->interrupt(IILjava/lang/Object;)Z

    move-result v4

    goto :goto_0
.end method

.method public getEnrollRepeatCount()I
    .locals 5

    .prologue
    .line 3086
    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mFactory:Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommandFactory;

    const-string v3, "getEnrollRepeatCount"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v2, v3, v4}, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommandFactory;->create(Ljava/lang/String;[Ljava/lang/Object;)Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;

    move-result-object v0

    .line 3089
    .local v0, "cmd":Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;
    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mSilentClient:Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

    invoke-direct {p0, v2, v0}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->enqueueCommand(Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 3091
    .local v1, "result":Ljava/lang/Integer;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-gtz v2, :cond_1

    .line 3092
    :cond_0
    sget v2, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mHWStatus:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_2

    .line 3093
    const/16 v2, 0x12

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 3098
    :cond_1
    :goto_0
    const-string v2, "FingerprintManagerService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getEnrollRepeatCount: result count="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 3099
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    return v2

    .line 3095
    :cond_2
    const/16 v2, 0x8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto :goto_0
.end method

.method public getEnrolledFingers(Ljava/lang/String;)I
    .locals 11
    .param p1, "ownName"    # Ljava/lang/String;

    .prologue
    const/4 v10, 0x1

    const/4 v4, 0x0

    .line 570
    iget-object v7, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mImpl:Lcom/samsung/android/fingerprint/service/FingerprintLibraryImplAbstractClass;

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mImpl:Lcom/samsung/android/fingerprint/service/FingerprintLibraryImplAbstractClass;

    invoke-virtual {v7}, Lcom/samsung/android/fingerprint/service/FingerprintLibraryImplAbstractClass;->isSensorReady()Z

    move-result v7

    if-nez v7, :cond_1

    .line 571
    :cond_0
    const-string v7, "FingerprintManagerService"

    const-string v8, "Sensor is not ready!!!"

    invoke-static {v7, v8}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 600
    :goto_0
    return v4

    .line 575
    :cond_1
    invoke-direct {p0}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->getUserId()I

    move-result v7

    invoke-static {v7, p1}, Lcom/samsung/android/fingerprint/service/util/ValueUtils;->getStringUserId(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 577
    .local v6, "stringUserId":Ljava/lang/String;
    iget-object v7, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mFactory:Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommandFactory;

    const-string v8, "getEnrolledFingers"

    new-array v9, v10, [Ljava/lang/Object;

    aput-object v6, v9, v4

    invoke-virtual {v7, v8, v9}, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommandFactory;->create(Ljava/lang/String;[Ljava/lang/Object;)Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;

    move-result-object v0

    .line 581
    .local v0, "cmd":Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;
    iget-object v7, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mSilentClient:Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

    invoke-direct {p0, v7, v0}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->enqueueCommand(Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;)Ljava/lang/Object;

    move-result-object v5

    .line 582
    .local v5, "retObj":Ljava/lang/Object;
    if-nez v5, :cond_2

    .line 583
    const-string v7, "FingerprintManagerService"

    const-string v8, "enqueueCommand result is null"

    invoke-static {v7, v8}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 587
    :cond_2
    check-cast v5, Ljava/lang/Integer;

    .end local v5    # "retObj":Ljava/lang/Object;
    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 588
    .local v4, "result":I
    sget-boolean v7, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->DEBUG:Z

    if-eqz v7, :cond_5

    .line 589
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 590
    .local v1, "fa":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const/4 v3, 0x1

    .local v3, "i":I
    :goto_1
    const/16 v7, 0xa

    if-gt v3, v7, :cond_4

    .line 591
    const/4 v2, 0x0

    .line 592
    .local v2, "finger":I
    shl-int v7, v10, v3

    and-int/2addr v7, v4

    if-eqz v7, :cond_3

    .line 593
    const/4 v2, 0x1

    .line 595
    :cond_3
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v1, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 590
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 597
    .end local v2    # "finger":I
    :cond_4
    const-string v7, "FingerprintManagerService"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "getEnrolledFingers:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", fingers "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v1}, Ljava/util/ArrayList;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 599
    .end local v1    # "fa":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .end local v3    # "i":I
    :cond_5
    sput v4, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mEnrolledFingerCount:I

    goto :goto_0
.end method

.method public getEnrolledPermissions(Ljava/lang/String;)Ljava/util/List;
    .locals 7
    .param p1, "appName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 634
    const-string v2, "FingerprintManagerService"

    const-string v3, "getEnrolledPermissions: called"

    invoke-static {v2, v3}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 636
    invoke-direct {p0}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->getUserId()I

    move-result v1

    .line 638
    .local v1, "userId":I
    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mFactory:Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommandFactory;

    const-string v3, "getEnrolledPermissions"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    aput-object p1, v4, v5

    invoke-virtual {v2, v3, v4}, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommandFactory;->create(Ljava/lang/String;[Ljava/lang/Object;)Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;

    move-result-object v0

    .line 643
    .local v0, "cmd":Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;
    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mSilentClient:Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

    invoke-direct {p0, v2, v0}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->enqueueCommand(Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    return-object v2
.end method

.method public getEnrolledUserCount()I
    .locals 4

    .prologue
    .line 2739
    const-string v1, "FingerprintManagerService"

    const-string v2, "getEnrolledUserCount: called."

    invoke-static {v1, v2}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 2741
    invoke-virtual {p0}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->getEnrolledUsers()Ljava/util/List;

    move-result-object v0

    .line 2743
    .local v0, "enrolledUsers":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-nez v0, :cond_0

    .line 2744
    const-string v1, "FingerprintManagerService"

    const-string v2, "getEnrolledUserCount: failed to get user list"

    invoke-static {v1, v2}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 2745
    const/4 v1, -0x1

    .line 2748
    :goto_0
    return v1

    .line 2747
    :cond_0
    const-string v1, "FingerprintManagerService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getEnrolledUserCount "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2748
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    goto :goto_0
.end method

.method public getEnrolledUsers()Ljava/util/List;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 2711
    const-string v8, "FingerprintManagerService"

    const-string v9, "getEnrolledUserCount: called."

    invoke-static {v8, v9}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 2714
    iget-object v8, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mContext:Landroid/content/Context;

    const-string v9, "com.samsung.android.permission.ENROLL_FINGERPRINT"

    invoke-virtual {v8, v9, v6}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    .line 2717
    iget-object v8, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mImpl:Lcom/samsung/android/fingerprint/service/FingerprintLibraryImplAbstractClass;

    invoke-virtual {v8}, Lcom/samsung/android/fingerprint/service/FingerprintLibraryImplAbstractClass;->isSensorReady()Z

    move-result v8

    if-nez v8, :cond_1

    .line 2718
    const-string v8, "FingerprintManagerService"

    const-string v9, "Sensor is not ready!!!"

    invoke-static {v8, v9}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 2735
    :cond_0
    return-object v6

    .line 2722
    :cond_1
    iget-object v8, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mFactory:Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommandFactory;

    const-string v9, "getEnrolledUsers"

    const/4 v10, 0x0

    new-array v10, v10, [Ljava/lang/Object;

    invoke-virtual {v8, v9, v10}, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommandFactory;->create(Ljava/lang/String;[Ljava/lang/Object;)Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;

    move-result-object v1

    .line 2723
    .local v1, "cmd":Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;
    iget-object v8, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mSilentClient:Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

    invoke-direct {p0, v8, v1}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->enqueueCommand(Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;)Ljava/lang/Object;

    move-result-object v2

    .line 2725
    .local v2, "commandResult":Ljava/lang/Object;
    if-eqz v2, :cond_0

    .line 2726
    check-cast v2, [Ljava/lang/String;

    .end local v2    # "commandResult":Ljava/lang/Object;
    move-object v7, v2

    check-cast v7, [Ljava/lang/String;

    .line 2727
    .local v7, "userStringArray":[Ljava/lang/String;
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 2728
    .local v6, "userList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    move-object v0, v7

    .local v0, "arr$":[Ljava/lang/String;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_0

    aget-object v5, v0, v3

    .line 2729
    .local v5, "user":Ljava/lang/String;
    invoke-interface {v6, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2728
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method public getFingerprintIdByFinger(ILjava/lang/String;)Ljava/lang/String;
    .locals 9
    .param p1, "fingerindex"    # I
    .param p2, "ownName"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 3053
    iget-object v4, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mImpl:Lcom/samsung/android/fingerprint/service/FingerprintLibraryImplAbstractClass;

    invoke-virtual {v4}, Lcom/samsung/android/fingerprint/service/FingerprintLibraryImplAbstractClass;->isSensorReady()Z

    move-result v4

    if-nez v4, :cond_0

    .line 3054
    const-string v4, "FingerprintManagerService"

    const-string v5, "Sensor is not ready!!!"

    invoke-static {v4, v5}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 3074
    :goto_0
    return-object v1

    .line 3058
    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->getUserId()I

    move-result v4

    invoke-static {v4, p2}, Lcom/samsung/android/fingerprint/service/util/ValueUtils;->getStringUserId(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 3060
    .local v3, "stringUserId":Ljava/lang/String;
    iget-object v4, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mFactory:Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommandFactory;

    const-string v5, "getFingerprintIdByFinger"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v3, v6, v7

    const/4 v7, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v4, v5, v6}, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommandFactory;->create(Ljava/lang/String;[Ljava/lang/Object;)Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;

    move-result-object v0

    .line 3065
    .local v0, "cmd":Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;
    iget-object v4, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mSilentClient:Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

    invoke-direct {p0, v4, v0}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->enqueueCommand(Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;)Ljava/lang/Object;

    move-result-object v2

    .line 3066
    .local v2, "retObj":Ljava/lang/Object;
    if-nez v2, :cond_1

    .line 3067
    const-string v4, "FingerprintManagerService"

    const-string v5, "enqueueCommand result is null"

    invoke-static {v4, v5}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    move-object v1, v2

    .line 3071
    check-cast v1, Ljava/lang/String;

    .line 3072
    .local v1, "result":Ljava/lang/String;
    const-string v4, "FingerprintManagerService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "getFingerprintIdByFinger: fingerIndex="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", id="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getFingerprintIds(Ljava/lang/String;)[Ljava/lang/String;
    .locals 14
    .param p1, "ownName"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x0

    .line 3024
    iget-object v10, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mImpl:Lcom/samsung/android/fingerprint/service/FingerprintLibraryImplAbstractClass;

    invoke-virtual {v10}, Lcom/samsung/android/fingerprint/service/FingerprintLibraryImplAbstractClass;->isSensorReady()Z

    move-result v10

    if-nez v10, :cond_1

    .line 3025
    const-string v10, "FingerprintManagerService"

    const-string v11, "Sensor is not ready!!!"

    invoke-static {v10, v11}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 3049
    :cond_0
    :goto_0
    return-object v7

    .line 3029
    :cond_1
    invoke-direct {p0}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->getUserId()I

    move-result v10

    invoke-static {v10, p1}, Lcom/samsung/android/fingerprint/service/util/ValueUtils;->getStringUserId(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 3031
    .local v9, "stringUserId":Ljava/lang/String;
    iget-object v10, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mFactory:Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommandFactory;

    const-string v11, "getFingerprintIds"

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    aput-object v9, v12, v13

    invoke-virtual {v10, v11, v12}, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommandFactory;->create(Ljava/lang/String;[Ljava/lang/Object;)Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;

    move-result-object v1

    .line 3035
    .local v1, "cmd":Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;
    iget-object v10, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mSilentClient:Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

    invoke-direct {p0, v10, v1}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->enqueueCommand(Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;)Ljava/lang/Object;

    move-result-object v8

    .line 3036
    .local v8, "retObj":Ljava/lang/Object;
    if-nez v8, :cond_2

    .line 3037
    const-string v10, "FingerprintManagerService"

    const-string v11, "enqueueCommand result is null"

    invoke-static {v10, v11}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 3041
    :cond_2
    check-cast v8, [Ljava/lang/String;

    .end local v8    # "retObj":Ljava/lang/Object;
    move-object v7, v8

    check-cast v7, [Ljava/lang/String;

    .line 3042
    .local v7, "result":[Ljava/lang/String;
    sget-boolean v10, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->DEBUG:Z

    if-eqz v10, :cond_0

    .line 3043
    const/4 v2, 0x1

    .line 3044
    .local v2, "i":I
    move-object v0, v7

    .local v0, "arr$":[Ljava/lang/String;
    array-length v6, v0

    .local v6, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    move v3, v2

    .end local v2    # "i":I
    .local v3, "i":I
    :goto_1
    if-ge v4, v6, :cond_0

    aget-object v5, v0, v4

    .line 3045
    .local v5, "id":Ljava/lang/String;
    const-string v10, "FingerprintManagerService"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "getFingerprintIds: fingerIndex"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    add-int/lit8 v2, v3, 0x1

    .end local v3    # "i":I
    .restart local v2    # "i":I
    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ", id"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3044
    add-int/lit8 v4, v4, 0x1

    move v3, v2

    .end local v2    # "i":I
    .restart local v3    # "i":I
    goto :goto_1
.end method

.method public getIdentifyDialogManager()Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;
    .locals 1

    .prologue
    .line 2790
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mIdentifyDialogManager:Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;

    return-object v0
.end method

.method public getIndexName(I)Ljava/lang/String;
    .locals 4
    .param p1, "index"    # I

    .prologue
    const/4 v0, 0x0

    .line 1742
    const-string v1, "FingerprintManagerService"

    const-string v2, "getIndexName: called."

    invoke-static {v1, v2}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1744
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mContext:Landroid/content/Context;

    if-nez v1, :cond_1

    .line 1745
    const-string v1, "FingerprintManagerService"

    const-string v2, "getIndexName: mContext is null!!!"

    invoke-static {v1, v2}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1755
    :cond_0
    :goto_0
    return-object v0

    .line 1749
    :cond_1
    invoke-direct {p0, p1}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->isEnrolledFinger(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1750
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mContext:Landroid/content/Context;

    invoke-direct {p0, p1, v1}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->getSPValue(ILandroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 1751
    .local v0, "result":Ljava/lang/String;
    const-string v1, "FingerprintManagerService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getIndexName: idx="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " name="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getLastImageQuality(Ljava/lang/String;)I
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 1731
    const-string v0, "FingerprintManagerService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getLastImageQuality:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mLastImageQuality:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1732
    iget v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mLastImageQuality:I

    return v0
.end method

.method public getLastImageQualityMessage(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 1727
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mLastImageQualityMessage:Ljava/lang/String;

    return-object v0
.end method

.method public getSensorServiceVersion()Ljava/lang/String;
    .locals 3

    .prologue
    .line 2706
    const-string v0, "FingerprintManagerService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getSensorServiceVersion "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mLibraryVersion:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2707
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mLibraryVersion:Ljava/lang/String;

    return-object v0
.end method

.method public getVersion()I
    .locals 1

    .prologue
    .line 566
    const/high16 v0, 0x1020000

    return v0
.end method

.method public handleNotifyBackgroundClients(Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;)V
    .locals 6
    .param p1, "caller"    # Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

    .prologue
    .line 1323
    iget-object v3, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mFocusedClient:Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mFocusedClient:Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

    invoke-virtual {v3}, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->getCommand()Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 1338
    :cond_0
    return-void

    .line 1329
    :cond_1
    new-instance v0, Lcom/samsung/android/fingerprint/FingerprintEvent;

    const/16 v3, 0x2710

    invoke-direct {v0, v3}, Lcom/samsung/android/fingerprint/FingerprintEvent;-><init>(I)V

    .line 1331
    .local v0, "gev":Lcom/samsung/android/fingerprint/FingerprintEvent;
    iget-object v3, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mClientList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

    .line 1332
    .local v2, "record":Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;
    invoke-virtual {v2}, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->isBackground()Z

    move-result v3

    if-eqz v3, :cond_2

    if-eq v2, p1, :cond_2

    .line 1333
    const-string v3, "FingerprintManagerService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "notifyBackgroundClients: Sending event to bacgkround record "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1335
    const/4 v3, 0x0

    invoke-virtual {v2, v3, v0}, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->onEvent(Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;Lcom/samsung/android/fingerprint/FingerprintEvent;)V

    goto :goto_0
.end method

.method public hasPendingCommand()Z
    .locals 2

    .prologue
    .line 1421
    const-string v0, "FingerprintManagerService"

    const-string v1, "hasPendingCommand: called."

    invoke-static {v0, v1}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1423
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mFocusedClient:Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mFocusedClient:Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

    invoke-virtual {v0}, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->getCommand()Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public identify(Landroid/os/IBinder;Ljava/lang/String;)I
    .locals 3
    .param p1, "token"    # Landroid/os/IBinder;
    .param p2, "permissionName"    # Ljava/lang/String;

    .prologue
    .line 891
    const-string v0, "FingerprintManagerService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "identify: called from pid="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 892
    invoke-direct {p0}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->getUserId()I

    move-result v0

    invoke-virtual {p0, p1, v0, p2}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->identifyForMultiUser(Landroid/os/IBinder;ILjava/lang/String;)I

    move-result v0

    return v0
.end method

.method public identifyForMultiUser(Landroid/os/IBinder;ILjava/lang/String;)I
    .locals 16
    .param p1, "token"    # Landroid/os/IBinder;
    .param p2, "userId"    # I
    .param p3, "permissionName"    # Ljava/lang/String;

    .prologue
    .line 973
    invoke-static/range {p1 .. p1}, Lcom/samsung/android/fingerprint/service/util/ClientInfoHelper;->simplifyObjectInfo(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    .line 974
    .local v11, "tokenInfo":Ljava/lang/String;
    const-string v12, "FingerprintManagerService"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "identifyForMultiUser: token="

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ", userId="

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move/from16 v0, p2

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ", permissionName="

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p3

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " from pid="

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 976
    invoke-direct/range {p0 .. p1}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->findClientRecord(Landroid/os/IBinder;)Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

    move-result-object v8

    .line 978
    .local v8, "record":Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;
    if-nez v8, :cond_0

    .line 979
    const-string v12, "FingerprintManagerService"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "identifyForMultiUser: invalid token("

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ") was given. return Invalid token"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 980
    const/4 v12, -0x3

    .line 1031
    :goto_0
    return v12

    .line 983
    :cond_0
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mImpl:Lcom/samsung/android/fingerprint/service/FingerprintLibraryImplAbstractClass;

    invoke-virtual {v12}, Lcom/samsung/android/fingerprint/service/FingerprintLibraryImplAbstractClass;->isSensorReady()Z

    move-result v12

    if-nez v12, :cond_1

    .line 984
    const-string v12, "FingerprintManagerService"

    const-string v13, "Sensor is not ready!!!"

    invoke-static {v12, v13}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 985
    const/4 v12, -0x1

    goto :goto_0

    .line 988
    :cond_1
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->isKeyGuardLocked()Z

    move-result v6

    .line 990
    .local v6, "isLocked":Z
    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-direct {v0, v1}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->isKioskModeEnabled(I)Z

    move-result v5

    .line 992
    .local v5, "isKioskMode":Z
    if-eqz v6, :cond_2

    invoke-virtual {v8}, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->isKeyguardClient()Z

    move-result v12

    if-nez v12, :cond_2

    if-nez v5, :cond_2

    .line 993
    const-string v12, "FingerprintManagerService"

    const-string v13, "keyguard is currently locked"

    invoke-static {v12, v13}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 994
    const/4 v12, -0x1

    goto :goto_0

    .line 997
    :cond_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mFocusedClient:Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

    .line 998
    .local v4, "focusedClient":Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;
    if-eqz v4, :cond_3

    .line 999
    invoke-virtual {v4}, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->isKeyguardClient()Z

    move-result v12

    if-eqz v12, :cond_3

    if-eqz v6, :cond_3

    .line 1000
    invoke-virtual {v4}, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->getCommand()Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;

    move-result-object v3

    .line 1001
    .local v3, "focusCmd":Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;
    if-eqz v3, :cond_3

    .line 1002
    const-string v12, "identify"

    iget-object v13, v3, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;->commandName:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 1003
    const-string v12, "FingerprintManagerService"

    const-string v13, "Keyguard client is in progress"

    invoke-static {v12, v13}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1004
    const/4 v12, -0x2

    goto :goto_0

    .line 1010
    .end local v3    # "focusCmd":Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;
    :cond_3
    invoke-virtual {v8}, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->isKeyguardClient()Z

    move-result v12

    if-nez v12, :cond_5

    if-nez v5, :cond_5

    .line 1011
    invoke-static {}, Lcom/samsung/android/fingerprint/service/util/Config;->getOverallFailedFingerprintAttempts()I

    move-result v12

    const/4 v13, 0x4

    if-ge v12, v13, :cond_4

    invoke-static {}, Lcom/samsung/android/fingerprint/service/util/Config;->getRemaingTimeToUnlock()J

    move-result-wide v12

    const-wide/16 v14, 0x0

    cmp-long v12, v12, v14

    if-lez v12, :cond_5

    .line 1013
    :cond_4
    const-string v12, "FingerprintManagerService"

    const-string v13, "Identify Command is not available because of too many failed fingeprint attempts"

    invoke-static {v12, v13}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1014
    const/16 v12, 0x33

    goto :goto_0

    .line 1017
    :cond_5
    invoke-virtual {v8}, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->getOwnName()Ljava/lang/String;

    move-result-object v7

    .line 1018
    .local v7, "ownName":Ljava/lang/String;
    move/from16 v0, p2

    invoke-static {v0, v7}, Lcom/samsung/android/fingerprint/service/util/ValueUtils;->getStringUserId(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 1020
    .local v10, "stringUserId":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mFactory:Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommandFactory;

    const-string v13, "identify"

    const/4 v14, 0x3

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    aput-object v10, v14, v15

    const/4 v15, 0x1

    aput-object v8, v14, v15

    const/4 v15, 0x2

    aput-object p3, v14, v15

    invoke-virtual {v12, v13, v14}, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommandFactory;->create(Ljava/lang/String;[Ljava/lang/Object;)Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;

    move-result-object v2

    .line 1026
    .local v2, "cmd":Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;
    move-object/from16 v0, p0

    invoke-direct {v0, v8, v2}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->enqueueCommand(Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;)Ljava/lang/Object;

    move-result-object v9

    .line 1027
    .local v9, "retObj":Ljava/lang/Object;
    if-nez v9, :cond_6

    .line 1028
    const-string v12, "FingerprintManagerService"

    const-string v13, "enqueueCommand result is null"

    invoke-static {v12, v13}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1029
    const/4 v12, -0x1

    goto/16 :goto_0

    .line 1031
    :cond_6
    check-cast v9, Ljava/lang/Integer;

    .end local v9    # "retObj":Ljava/lang/Object;
    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v12

    goto/16 :goto_0
.end method

.method public identifyWithDialog(Ljava/lang/String;Landroid/content/ComponentName;Lcom/samsung/android/fingerprint/IFingerprintClient;Landroid/os/Bundle;)I
    .locals 9
    .param p1, "pkgName"    # Ljava/lang/String;
    .param p2, "comName"    # Landroid/content/ComponentName;
    .param p3, "client"    # Lcom/samsung/android/fingerprint/IFingerprintClient;
    .param p4, "attribute"    # Landroid/os/Bundle;

    .prologue
    const/4 v5, -0x1

    const/4 v6, 0x0

    .line 862
    if-nez p3, :cond_0

    .line 863
    const-string v6, "FingerprintManagerService"

    const-string v7, "identifyWithDialog: client may not be null."

    invoke-static {v6, v7}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 887
    :goto_0
    return v5

    .line 866
    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->isKeyGuardLocked()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 867
    const-string v6, "FingerprintManagerService"

    const-string v7, "identifyWithDialog: KeyGuardLocked!!"

    invoke-static {v6, v7}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 870
    :cond_1
    invoke-interface {p3}, Lcom/samsung/android/fingerprint/IFingerprintClient;->asBinder()Landroid/os/IBinder;

    move-result-object v3

    .line 872
    .local v3, "token":Landroid/os/IBinder;
    invoke-static {p3}, Lcom/samsung/android/fingerprint/service/util/ClientInfoHelper;->simplifyObjectInfo(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 873
    .local v1, "clientInfo":Ljava/lang/String;
    invoke-static {v3}, Lcom/samsung/android/fingerprint/service/util/ClientInfoHelper;->simplifyObjectInfo(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 875
    .local v4, "tokenInfo":Ljava/lang/String;
    const-string v5, "FingerprintManagerService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "identifyWithDialog: client="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " as token="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " from pid="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 879
    new-instance v2, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v5

    invoke-direct {v2, p0, p3, p4, v5}, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;-><init>(Lcom/samsung/android/fingerprint/service/FingerprintManagerService;Lcom/samsung/android/fingerprint/IFingerprintClient;Landroid/os/Bundle;I)V

    .line 881
    .local v2, "record":Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;
    const-string v5, "FingerprintManagerService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "identifyWithDialog record=)"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v2}, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->getClientSpec()Landroid/os/Bundle;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 883
    invoke-virtual {v2, p2}, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->setComponentName(Landroid/content/ComponentName;)V

    .line 884
    new-instance v0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService$IdentifyDialogObject;

    const/4 v5, 0x0

    invoke-direct {v0, v5}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService$IdentifyDialogObject;-><init>(Lcom/samsung/android/fingerprint/service/FingerprintManagerService$1;)V

    .line 885
    .local v0, "IdentifyDialogObject":Lcom/samsung/android/fingerprint/service/FingerprintManagerService$IdentifyDialogObject;
    invoke-virtual {v0, p4, v2}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService$IdentifyDialogObject;->setAttributeAndRecord(Landroid/os/Bundle;Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;)Lcom/samsung/android/fingerprint/service/FingerprintManagerService$IdentifyDialogObject;

    .line 886
    iget-object v5, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mH:Lcom/samsung/android/fingerprint/service/FingerprintManagerService$H;

    const/16 v7, 0x264

    invoke-static {v5, v7, v6, v6, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v5

    invoke-virtual {v5}, Landroid/os/Message;->sendToTarget()V

    move v5, v6

    .line 887
    goto/16 :goto_0
.end method

.method public isEnrollSession()Z
    .locals 1

    .prologue
    .line 2753
    iget-boolean v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mIsEnrollSession:Z

    return v0
.end method

.method public isEnrolledPermission(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 8
    .param p1, "appName"    # Ljava/lang/String;
    .param p2, "permissionName"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 1035
    const-string v4, "FingerprintManagerService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "isEnrolledPermission: appName="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", permissionName="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1037
    invoke-direct {p0}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->getUserId()I

    move-result v2

    .line 1039
    .local v2, "userId":I
    iget-object v4, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mFactory:Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommandFactory;

    const-string v5, "isEnrolledPermission"

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v3

    const/4 v7, 0x1

    aput-object p1, v6, v7

    const/4 v7, 0x2

    aput-object p2, v6, v7

    invoke-virtual {v4, v5, v6}, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommandFactory;->create(Ljava/lang/String;[Ljava/lang/Object;)Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;

    move-result-object v0

    .line 1044
    .local v0, "cmd":Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;
    iget-object v4, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mSilentClient:Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

    invoke-direct {p0, v4, v0}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->enqueueCommand(Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;)Ljava/lang/Object;

    move-result-object v1

    .line 1045
    .local v1, "retObj":Ljava/lang/Object;
    if-nez v1, :cond_0

    .line 1046
    const-string v4, "FingerprintManagerService"

    const-string v5, "enqueueCommand result is null"

    invoke-static {v4, v5}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1049
    .end local v1    # "retObj":Ljava/lang/Object;
    :goto_0
    return v3

    .restart local v1    # "retObj":Ljava/lang/Object;
    :cond_0
    check-cast v1, Ljava/lang/Boolean;

    .end local v1    # "retObj":Ljava/lang/Object;
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    goto :goto_0
.end method

.method public isSensorReady()Z
    .locals 4

    .prologue
    .line 647
    sget-boolean v1, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mSensorReady:Z

    if-eqz v1, :cond_0

    .line 648
    const-string v1, "FingerprintManagerService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mSensorReady="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-boolean v3, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mSensorReady:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 650
    sget-boolean v0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mSensorReady:Z

    .line 657
    :goto_0
    return v0

    .line 653
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mImpl:Lcom/samsung/android/fingerprint/service/FingerprintLibraryImplAbstractClass;

    invoke-virtual {v1}, Lcom/samsung/android/fingerprint/service/FingerprintLibraryImplAbstractClass;->isSensorReady()Z

    move-result v0

    .line 655
    .local v0, "isReady":Z
    const-string v1, "FingerprintManagerService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mSensorReady="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-boolean v3, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mSensorReady:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", isSensorReady()="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public isSupportFingerprintIds()Z
    .locals 5

    .prologue
    .line 3103
    const/4 v1, 0x1

    .line 3105
    .local v1, "mSupportFingerprintIds":Z
    const-string v2, "ro.product.device"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 3107
    .local v0, "deviceName":Ljava/lang/String;
    const-string v2, "klte"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "k3g"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "kmini"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "chagall"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "klimt"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "slte"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "lentislte"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "kccat6"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "hestialte"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 3110
    :cond_0
    const/4 v1, 0x0

    .line 3112
    :cond_1
    const-string v2, "FingerprintManagerService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "isSupportFingerprintIds: deviceName : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", mSupportFingerprintIds : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 3113
    return v1
.end method

.method public isVZWPermissionGranted(Ljava/lang/String;)Z
    .locals 23
    .param p1, "ownName"    # Ljava/lang/String;

    .prologue
    .line 2822
    const-string v20, "FingerprintManagerService"

    const-string v21, "isVZWPermissionGranted: called"

    invoke-static/range {v20 .. v21}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 2824
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    if-nez v20, :cond_0

    .line 2825
    const-string v20, "FingerprintManagerService"

    const-string v21, "isVZWPermissionGranted: mContext is null"

    invoke-static/range {v20 .. v21}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 2826
    const/16 v20, 0x0

    .line 2932
    :goto_0
    return v20

    .line 2828
    :cond_0
    invoke-static/range {p1 .. p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v20

    if-eqz v20, :cond_1

    .line 2829
    const-string v20, "FingerprintManagerService"

    const-string v21, "isVZWPermissionGranted: ownName is empty"

    invoke-static/range {v20 .. v21}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 2830
    const/16 v20, 0x0

    goto :goto_0

    .line 2833
    :cond_1
    const/4 v3, 0x0

    .line 2834
    .local v3, "callingPackageInfo":Landroid/content/pm/PackageInfo;
    const/4 v15, 0x0

    .line 2835
    .local v15, "permissionPackageInfo":Landroid/content/pm/PackageInfo;
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v17

    .line 2837
    .local v17, "uid":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v11

    .line 2838
    .local v11, "mPackageManager":Landroid/content/pm/PackageManager;
    move/from16 v0, v17

    invoke-virtual {v11, v0}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;

    move-result-object v4

    .line 2839
    .local v4, "callingPackageName":[Ljava/lang/String;
    const-string v20, "FingerprintManagerService"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "isVZWPermissionGranted: callingPackageName="

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const/16 v22, 0x0

    aget-object v22, v4, v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2841
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v18

    .line 2844
    .local v18, "token":J
    const/16 v20, 0x0

    :try_start_0
    aget-object v20, v4, v20

    const/16 v21, 0x40

    move-object/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v11, v0, v1}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v3

    .line 2845
    const-string v20, "com.verizon.permissions.vzwappapn"

    const/16 v21, 0x40

    move-object/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v11, v0, v1}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v15

    .line 2852
    invoke-static/range {v18 .. v19}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 2856
    iget-object v0, v3, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Landroid/content/pm/ApplicationInfo;->flags:I

    move/from16 v20, v0

    and-int/lit8 v20, v20, 0x1

    if-eqz v20, :cond_2

    .line 2857
    const/16 v20, 0x1

    goto :goto_0

    .line 2847
    :catch_0
    move-exception v6

    .line 2848
    .local v6, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :try_start_1
    invoke-virtual {v6}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    .line 2849
    const-string v20, "FingerprintManagerService"

    const-string v21, "isVZWPermissionGranted: calling package NameNotFoundException"

    invoke-static/range {v20 .. v21}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2850
    const/16 v20, 0x0

    .line 2852
    invoke-static/range {v18 .. v19}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    goto/16 :goto_0

    .end local v6    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :catchall_0
    move-exception v20

    invoke-static/range {v18 .. v19}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    throw v20

    .line 2871
    :cond_2
    iget-object v5, v3, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    .line 2873
    .local v5, "callingPackageSignatures":[Landroid/content/pm/Signature;
    if-eqz v5, :cond_5

    .line 2875
    iget-object v0, v15, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    move-object/from16 v16, v0

    .line 2877
    .local v16, "permissionPackageSignatures":[Landroid/content/pm/Signature;
    array-length v13, v5

    .line 2878
    .local v13, "numberOfCallingPackageSignatures":I
    move-object/from16 v0, v16

    array-length v14, v0

    .line 2880
    .local v14, "numberOfPermissionPackageSignatures":I
    const-string v20, "FingerprintManagerService"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "isVZWPermissionGranted: numberOfCallingPackageSignatures="

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2881
    const-string v20, "FingerprintManagerService"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "isVZWPermissionGranted: numberOfPermissionPackageSignatures="

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2883
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_1
    if-ge v7, v13, :cond_6

    .line 2884
    const-string v20, "FingerprintManagerService"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "isVZWPermissionGranted: callingPackageSignatures["

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, "]="

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    aget-object v22, v5, v7

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2885
    const/4 v10, 0x0

    .local v10, "j":I
    :goto_2
    if-ge v10, v14, :cond_4

    .line 2886
    const-string v20, "FingerprintManagerService"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "isVZWPermissionGranted: permissionPackageSignatures["

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, "]="

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    aget-object v22, v16, v10

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2887
    aget-object v20, v5, v7

    aget-object v21, v16, v10

    invoke-virtual/range {v20 .. v21}, Landroid/content/pm/Signature;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_3

    .line 2888
    const-string v20, "FingerprintManagerService"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "isVZWPermissionGranted: Signature of the calling post loaded application matched with verizon provided signatures "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    aget-object v22, v5, v7

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2891
    const/16 v20, 0x1

    goto/16 :goto_0

    .line 2885
    :cond_3
    add-int/lit8 v10, v10, 0x1

    goto :goto_2

    .line 2883
    :cond_4
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 2896
    .end local v7    # "i":I
    .end local v10    # "j":I
    .end local v13    # "numberOfCallingPackageSignatures":I
    .end local v14    # "numberOfPermissionPackageSignatures":I
    .end local v16    # "permissionPackageSignatures":[Landroid/content/pm/Signature;
    :cond_5
    const-string v20, "FingerprintManagerService"

    const-string v21, "isVZWPermissionGranted: callingPackageSignatures is null"

    invoke-static/range {v20 .. v21}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2900
    :cond_6
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v8

    .line 2902
    .local v8, "identity":J
    :try_start_2
    iget-object v0, v3, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Landroid/content/pm/ApplicationInfo;->flags:I

    move/from16 v20, v0

    and-int/lit8 v20, v20, 0x1

    if-eqz v20, :cond_9

    .line 2904
    const-string v20, "FingerprintManagerService"

    const-string v21, "isVZWPermissionGranted: It is a preloaded application"

    invoke-static/range {v20 .. v21}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2905
    const/16 v20, 0x0

    aget-object v20, v4, v20

    const/16 v21, 0x80

    move-object/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v11, v0, v1}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v20

    move-object/from16 v0, v20

    iget-object v2, v0, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    .line 2908
    .local v2, "bundle":Landroid/os/Bundle;
    if-nez v2, :cond_7

    .line 2909
    const-string v20, "FingerprintManagerService"

    const-string v21, "isVZWPermissionGranted: This preloaded application does not have the meta-data tag."

    invoke-static/range {v20 .. v21}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2910
    const/16 v20, 0x0

    .line 2928
    invoke-static {v8, v9}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    goto/16 :goto_0

    .line 2913
    :cond_7
    :try_start_3
    const-string v20, "vzwapi"

    move-object/from16 v0, v20

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 2914
    .local v12, "metaDataValue":Ljava/lang/String;
    const-string v20, "FingerprintManagerService"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "isPackageAVSAuthorized: metadata="

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2916
    if-eqz v12, :cond_a

    const-string v20, ""

    move-object/from16 v0, v20

    invoke-virtual {v12, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-nez v20, :cond_8

    const-string v20, "VerizonDefinedPermission"

    move-object/from16 v0, v20

    invoke-virtual {v12, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_a

    .line 2918
    :cond_8
    const-string v20, "FingerprintManagerService"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "Its a preloaded application with metadata "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 2919
    const/16 v20, 0x1

    .line 2928
    invoke-static {v8, v9}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    goto/16 :goto_0

    .line 2922
    .end local v2    # "bundle":Landroid/os/Bundle;
    .end local v12    # "metaDataValue":Ljava/lang/String;
    :cond_9
    :try_start_4
    const-string v20, "FingerprintManagerService"

    const-string v21, "isVZWPermissionGranted: FLAG_SYSTEM  is null"

    invoke-static/range {v20 .. v21}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2928
    :cond_a
    invoke-static {v8, v9}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 2931
    :goto_3
    const-string v20, "FingerprintManagerService"

    const-string v21, "isVZWPermissionGranted: It is neither a preloaded app nor a post loaded app with correct signature"

    invoke-static/range {v20 .. v21}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2932
    const/16 v20, 0x0

    goto/16 :goto_0

    .line 2924
    :catch_1
    move-exception v6

    .line 2925
    .restart local v6    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :try_start_5
    invoke-virtual {v6}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    .line 2926
    const-string v20, "FingerprintManagerService"

    invoke-virtual {v6}, Landroid/content/pm/PackageManager$NameNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 2928
    invoke-static {v8, v9}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    goto :goto_3

    .end local v6    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :catchall_1
    move-exception v20

    invoke-static {v8, v9}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    throw v20
.end method

.method public lock()V
    .locals 4
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1585
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mFactory:Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommandFactory;

    const-string v2, "lock"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommandFactory;->create(Ljava/lang/String;[Ljava/lang/Object;)Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;

    move-result-object v0

    .line 1586
    .local v0, "cmd":Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mSilentClient:Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

    invoke-direct {p0, v1, v0}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->enqueueCommand(Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;)Ljava/lang/Object;

    .line 1587
    return-void
.end method

.method public moreEnroll(Landroid/os/IBinder;Ljava/lang/String;II)I
    .locals 15
    .param p1, "token"    # Landroid/os/IBinder;
    .param p2, "permissionName"    # Ljava/lang/String;
    .param p3, "fingerIndex"    # I
    .param p4, "enrollMode"    # I

    .prologue
    .line 718
    const-string v10, "FingerprintManagerService"

    const-string v11, "moreEnroll: called"

    invoke-static {v10, v11}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 720
    invoke-direct {p0}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->getUserId()I

    move-result v9

    .line 722
    .local v9, "userId":I
    invoke-static/range {p1 .. p1}, Lcom/samsung/android/fingerprint/service/util/ClientInfoHelper;->simplifyObjectInfo(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 724
    .local v8, "tokenInfo":Ljava/lang/String;
    const-string v10, "FingerprintManagerService"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "moreEnroll: userId="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ", token="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ", permissionName="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, p2

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ", fingertIndex="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move/from16 v0, p3

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ", enrollMode="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move/from16 v0, p4

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " from pid="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 733
    iget-object v10, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mContext:Landroid/content/Context;

    const-string v11, "com.samsung.android.permission.ENROLL_FINGERPRINT"

    const/4 v12, 0x0

    invoke-virtual {v10, v11, v12}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    .line 736
    invoke-direct/range {p0 .. p1}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->findClientRecord(Landroid/os/IBinder;)Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

    move-result-object v5

    .line 738
    .local v5, "record":Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;
    if-nez v5, :cond_0

    .line 739
    const-string v10, "FingerprintManagerService"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "moreEnroll: invalid token("

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ") was given. return Invalid token"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 740
    const/4 v10, -0x3

    .line 782
    :goto_0
    return v10

    .line 743
    :cond_0
    invoke-virtual {v5}, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->getAppName()Ljava/lang/String;

    move-result-object v1

    .line 744
    .local v1, "appName":Ljava/lang/String;
    if-eqz p2, :cond_1

    .line 745
    if-nez v1, :cond_1

    .line 746
    const-string v10, "FingerprintManagerService"

    const-string v11, "moreEnroll: appName may not be null"

    invoke-static {v10, v11}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 747
    const/4 v10, -0x1

    goto :goto_0

    .line 751
    :cond_1
    const/4 v10, 0x1

    move/from16 v0, p3

    if-lt v0, v10, :cond_2

    const/16 v10, 0x14

    move/from16 v0, p3

    if-le v0, v10, :cond_3

    .line 753
    :cond_2
    const-string v10, "FingerprintManagerService"

    const-string v11, "moreEnroll: wrong fingerIndex was given"

    invoke-static {v10, v11}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 754
    const/4 v10, -0x1

    goto :goto_0

    .line 757
    :cond_3
    iget-object v10, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mImpl:Lcom/samsung/android/fingerprint/service/FingerprintLibraryImplAbstractClass;

    invoke-virtual {v10}, Lcom/samsung/android/fingerprint/service/FingerprintLibraryImplAbstractClass;->isSensorReady()Z

    move-result v10

    if-nez v10, :cond_4

    .line 758
    const-string v10, "FingerprintManagerService"

    const-string v11, "moreEnroll: Sensor is not ready!!!"

    invoke-static {v10, v11}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 759
    const/4 v10, -0x1

    goto :goto_0

    .line 762
    :cond_4
    invoke-virtual {v5}, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->getOwnName()Ljava/lang/String;

    move-result-object v4

    .line 763
    .local v4, "ownName":Ljava/lang/String;
    invoke-static {v9, v4}, Lcom/samsung/android/fingerprint/service/util/ValueUtils;->getStringUserId(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 764
    .local v7, "stringUserId":Ljava/lang/String;
    move-object/from16 v3, p2

    .line 765
    .local v3, "newPermName":Ljava/lang/String;
    if-eqz v4, :cond_5

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v10

    if-lez v10, :cond_5

    if-eqz p2, :cond_5

    const-string v10, "com.android.settings.permission.unlock"

    move-object/from16 v0, p2

    invoke-virtual {v0, v10}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v10

    if-nez v10, :cond_5

    .line 768
    const-string v10, "FingerprintManagerService"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Change permissionName from "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, p2

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " to "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 769
    move-object v3, v4

    .line 772
    :cond_5
    iget-object v10, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mFactory:Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommandFactory;

    const-string v11, "enroll"

    const/4 v12, 0x5

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    aput-object v7, v12, v13

    const/4 v13, 0x1

    aput-object v1, v12, v13

    const/4 v13, 0x2

    aput-object v3, v12, v13

    const/4 v13, 0x3

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    aput-object v14, v12, v13

    const/4 v13, 0x4

    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    aput-object v14, v12, v13

    invoke-virtual {v10, v11, v12}, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommandFactory;->create(Ljava/lang/String;[Ljava/lang/Object;)Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;

    move-result-object v2

    .line 780
    .local v2, "cmd":Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;
    invoke-direct {p0, v5, v2}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->enqueueCommand(Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    .line 782
    .local v6, "result":Ljava/lang/Integer;
    if-nez v6, :cond_6

    const/4 v10, -0x1

    goto/16 :goto_0

    :cond_6
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v10

    goto/16 :goto_0
.end method

.method public notifyAlternativePasswordBegin()V
    .locals 2

    .prologue
    .line 2034
    const-string v0, "FingerprintManagerService"

    const-string v1, "notifyAlternativePasswordBegin: called"

    invoke-static {v0, v1}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2041
    return-void
.end method

.method public notifyApplicationState(ILandroid/os/Bundle;)V
    .locals 3
    .param p1, "state"    # I
    .param p2, "extInfo"    # Landroid/os/Bundle;

    .prologue
    .line 817
    const-string v0, "FingerprintManagerService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "notifyApplicationState: called, state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 818
    const/4 v0, 0x1

    if-eq p1, v0, :cond_0

    const/4 v0, 0x2

    if-eq p1, v0, :cond_0

    const/4 v0, 0x4

    if-ne p1, v0, :cond_1

    .line 819
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mH:Lcom/samsung/android/fingerprint/service/FingerprintManagerService$H;

    new-instance v1, Lcom/samsung/android/fingerprint/service/FingerprintManagerService$3;

    invoke-direct {v1, p0}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService$3;-><init>(Lcom/samsung/android/fingerprint/service/FingerprintManagerService;)V

    invoke-virtual {v0, v1}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService$H;->post(Ljava/lang/Runnable;)Z

    .line 828
    :cond_1
    return-void
.end method

.method public notifyAuthSessionBegin()Z
    .locals 8

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1996
    const-string v4, "FingerprintManagerService"

    const-string v5, "notifyAuthSessionBegin: called"

    invoke-static {v4, v5}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 2002
    iget-object v4, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mImpl:Lcom/samsung/android/fingerprint/service/FingerprintLibraryImplAbstractClass;

    invoke-virtual {v4}, Lcom/samsung/android/fingerprint/service/FingerprintLibraryImplAbstractClass;->isSensorReady()Z

    move-result v4

    if-nez v4, :cond_1

    .line 2003
    const-string v3, "FingerprintManagerService"

    const-string v4, "notifyAuthSessionBegin: Sensor is not ready!!!"

    invoke-static {v3, v4}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 2013
    :cond_0
    :goto_0
    return v2

    .line 2007
    :cond_1
    iget-object v4, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mFactory:Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommandFactory;

    const-string v5, "notify"

    new-array v6, v3, [Ljava/lang/Object;

    const/4 v7, 0x3

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v2

    invoke-virtual {v4, v5, v6}, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommandFactory;->create(Ljava/lang/String;[Ljava/lang/Object;)Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;

    move-result-object v0

    .line 2011
    .local v0, "cmd":Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;
    iget-object v4, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mSilentClient:Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

    invoke-direct {p0, v4, v0}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->enqueueCommand(Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 2013
    .local v1, "result":Ljava/lang/Integer;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-nez v4, :cond_0

    move v2, v3

    goto :goto_0
.end method

.method public notifyAuthSessionEnd()Z
    .locals 8

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2017
    const-string v4, "FingerprintManagerService"

    const-string v5, "notifyAuthSessionEnd: called"

    invoke-static {v4, v5}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 2024
    iget-object v4, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mFactory:Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommandFactory;

    const-string v5, "notify"

    new-array v6, v3, [Ljava/lang/Object;

    const/4 v7, 0x4

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v2

    invoke-virtual {v4, v5, v6}, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommandFactory;->create(Ljava/lang/String;[Ljava/lang/Object;)Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;

    move-result-object v0

    .line 2028
    .local v0, "cmd":Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;
    iget-object v4, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mSilentClient:Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

    invoke-direct {p0, v4, v0}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->enqueueCommand(Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 2030
    .local v1, "result":Ljava/lang/Integer;
    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v2

    :cond_1
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-nez v4, :cond_0

    move v2, v3

    goto :goto_0
.end method

.method public notifyEnrollBegin()Z
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 1954
    const-string v4, "FingerprintManagerService"

    const-string v5, "notifyEnrollBegin: called"

    invoke-static {v4, v5}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1956
    iget-object v4, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mImpl:Lcom/samsung/android/fingerprint/service/FingerprintLibraryImplAbstractClass;

    invoke-virtual {v4}, Lcom/samsung/android/fingerprint/service/FingerprintLibraryImplAbstractClass;->isSensorReady()Z

    move-result v4

    if-nez v4, :cond_1

    .line 1957
    const-string v3, "FingerprintManagerService"

    const-string v4, "notifyEnrollBegin: Sensor is not ready!!!"

    invoke-static {v3, v4}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1973
    :cond_0
    :goto_0
    return v2

    .line 1961
    :cond_1
    iget-object v4, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mFactory:Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommandFactory;

    const-string v5, "notify"

    new-array v6, v3, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v2

    invoke-virtual {v4, v5, v6}, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommandFactory;->create(Ljava/lang/String;[Ljava/lang/Object;)Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;

    move-result-object v0

    .line 1965
    .local v0, "cmd":Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;
    iget-object v4, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mSilentClient:Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

    invoke-direct {p0, v4, v0}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->enqueueCommand(Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 1967
    .local v1, "result":Ljava/lang/Integer;
    if-eqz v1, :cond_0

    .line 1968
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-nez v4, :cond_0

    .line 1969
    iput-boolean v3, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mIsEnrollSession:Z

    move v2, v3

    .line 1970
    goto :goto_0
.end method

.method public notifyEnrollEnd()Z
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1977
    const-string v4, "FingerprintManagerService"

    const-string v5, "notifyEnrollEnd: called"

    invoke-static {v4, v5}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1979
    iget-object v4, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mFactory:Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommandFactory;

    const-string v5, "notify"

    new-array v6, v2, [Ljava/lang/Object;

    const/4 v7, 0x2

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v3

    invoke-virtual {v4, v5, v6}, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommandFactory;->create(Ljava/lang/String;[Ljava/lang/Object;)Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;

    move-result-object v0

    .line 1983
    .local v0, "cmd":Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;
    iget-object v4, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mSilentClient:Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

    invoke-direct {p0, v4, v0}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->enqueueCommand(Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 1985
    .local v1, "result":Ljava/lang/Integer;
    if-eqz v1, :cond_0

    .line 1986
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-nez v4, :cond_0

    .line 1987
    iput-boolean v3, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mIsEnrollSession:Z

    .line 1988
    iput v3, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mVerificationTypeToEnroll:I

    .line 1992
    :goto_0
    return v2

    :cond_0
    move v2, v3

    goto :goto_0
.end method

.method public onClientDied(Landroid/os/IBinder;Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;)V
    .locals 4
    .param p1, "token"    # Landroid/os/IBinder;
    .param p2, "record"    # Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

    .prologue
    .line 1899
    invoke-static {p1}, Lcom/samsung/android/fingerprint/service/util/ClientInfoHelper;->simplifyObjectInfo(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1900
    .local v0, "tokenInfo":Ljava/lang/String;
    const-string v1, "FingerprintManagerService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onClientDied: token="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", record="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1901
    const-string v1, "FingerprintManagerService"

    const-string v2, "onClientDied: LOCK: wait to obtain."

    invoke-static {v1, v2}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->lock(Ljava/lang/String;Ljava/lang/String;)V

    .line 1903
    monitor-enter p0

    .line 1904
    :try_start_0
    const-string v1, "FingerprintManagerService"

    const-string v2, "onClientDied: LOCK: obtained."

    invoke-static {v1, v2}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->lock(Ljava/lang/String;Ljava/lang/String;)V

    .line 1906
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mFocusedClient:Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

    if-ne v1, p2, :cond_1

    .line 1907
    invoke-virtual {p2}, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->getCommand()Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1908
    invoke-virtual {p2}, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->getCommand()Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;->requestCancel()Z

    .line 1911
    :cond_0
    const-string v1, "FingerprintManagerService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "FOCUS: onClientDied: mFocusedClient is changed from "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mFocusedClient:Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " to null"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1913
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mFocusedClient:Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

    .line 1916
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mClientList:Ljava/util/List;

    invoke-interface {v1, p2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1917
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mTokenMap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1918
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1920
    const-string v1, "FingerprintManagerService"

    const-string v2, "onClientDied: LOCK: released."

    invoke-static {v1, v2}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->lock(Ljava/lang/String;Ljava/lang/String;)V

    .line 1921
    return-void

    .line 1918
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public onEvent(Ljava/lang/Object;)V
    .locals 3
    .param p1, "evt"    # Ljava/lang/Object;

    .prologue
    .line 1639
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mH:Lcom/samsung/android/fingerprint/service/FingerprintManagerService$H;

    const/16 v2, 0x20a

    invoke-virtual {v1, v2, p1}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService$H;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 1640
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mH:Lcom/samsung/android/fingerprint/service/FingerprintManagerService$H;

    invoke-virtual {v1, v0}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService$H;->sendMessage(Landroid/os/Message;)Z

    .line 1641
    return-void
.end method

.method public onFpDaemonDied()V
    .locals 5

    .prologue
    const/16 v4, 0x230

    .line 1632
    const-string v0, "FingerprintManagerService"

    const-string v1, "FP daemone died"

    invoke-static {v0, v1}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1633
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mH:Lcom/samsung/android/fingerprint/service/FingerprintManagerService$H;

    invoke-virtual {v0, v4}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService$H;->removeMessages(I)V

    .line 1634
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mH:Lcom/samsung/android/fingerprint/service/FingerprintManagerService$H;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v4, v2, v3}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService$H;->sendEmptyMessageDelayed(IJ)Z

    .line 1635
    return-void
.end method

.method public openTransaction(Landroid/os/IBinder;)Z
    .locals 1
    .param p1, "token"    # Landroid/os/IBinder;

    .prologue
    .line 1604
    const/4 v0, 0x1

    return v0
.end method

.method public processFIDO(Landroid/os/IBinder;Ljava/lang/String;Ljava/lang/String;[B)[B
    .locals 14
    .param p1, "token"    # Landroid/os/IBinder;
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "permissionName"    # Ljava/lang/String;
    .param p4, "requestData"    # [B

    .prologue
    .line 2372
    const-string v3, "processFIDO"

    .line 2373
    .local v3, "methodName":Ljava/lang/String;
    const/4 v5, 0x0

    .line 2374
    .local v5, "resonseData":[B
    const/4 v2, 0x0

    .line 2375
    .local v2, "isAllowed":Z
    invoke-direct {p0}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->getUserId()I

    move-result v8

    .line 2376
    .local v8, "userId":I
    invoke-static {p1}, Lcom/samsung/android/fingerprint/service/util/ClientInfoHelper;->simplifyObjectInfo(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    .line 2378
    .local v7, "tokenInfo":Ljava/lang/String;
    const-string v9, "FingerprintManagerService"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "processFIDO: userId="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ", token="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ", permissionName="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move-object/from16 v0, p3

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " from pid="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2380
    invoke-direct {p0, p1}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->findClientRecord(Landroid/os/IBinder;)Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

    move-result-object v4

    .line 2382
    .local v4, "record":Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;
    if-nez v4, :cond_0

    .line 2383
    const-string v9, "FingerprintManagerService"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "processFIDO: invalid token("

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ") was given."

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    move-object v6, v5

    .line 2418
    .end local v5    # "resonseData":[B
    .local v6, "resonseData":[B
    :goto_0
    return-object v6

    .line 2386
    .end local v6    # "resonseData":[B
    .restart local v5    # "resonseData":[B
    :cond_0
    sget-boolean v9, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->DEBUG:Z

    if-eqz v9, :cond_1

    .line 2387
    const/4 v2, 0x1

    .line 2391
    :goto_1
    if-nez v2, :cond_2

    .line 2392
    const-string v9, "FingerprintManagerService"

    const-string v10, "processFIDO: Not allowed to use processFIDO"

    invoke-static {v9, v10}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 2393
    new-instance v9, Ljava/lang/SecurityException;

    const-string v10, "Permission Denial, Not allowed to use processFIDO"

    invoke-direct {v9, v10}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v9

    .line 2389
    :cond_1
    invoke-direct {p0}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->isFidoSignature()Z

    move-result v2

    goto :goto_1

    .line 2396
    :cond_2
    iget-object v9, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mImpl:Lcom/samsung/android/fingerprint/service/FingerprintLibraryImplAbstractClass;

    invoke-virtual {v9}, Lcom/samsung/android/fingerprint/service/FingerprintLibraryImplAbstractClass;->isSensorReady()Z

    move-result v9

    if-nez v9, :cond_3

    .line 2397
    const-string v9, "FingerprintManagerService"

    const-string v10, "processFIDO: Sensor is not ready!!!"

    invoke-static {v9, v10}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    move-object v6, v5

    .line 2398
    .end local v5    # "resonseData":[B
    .restart local v6    # "resonseData":[B
    goto :goto_0

    .line 2400
    .end local v6    # "resonseData":[B
    .restart local v5    # "resonseData":[B
    :cond_3
    if-eqz p4, :cond_4

    move-object/from16 v0, p4

    array-length v9, v0

    if-nez v9, :cond_5

    .line 2401
    :cond_4
    const-string v9, "FingerprintManagerService"

    const-string v10, "processFIDO: invalid data"

    invoke-static {v9, v10}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    move-object v6, v5

    .line 2402
    .end local v5    # "resonseData":[B
    .restart local v6    # "resonseData":[B
    goto :goto_0

    .line 2405
    .end local v6    # "resonseData":[B
    .restart local v5    # "resonseData":[B
    :cond_5
    const-string v9, "FingerprintManagerService"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "processFIDO: requestData="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    new-instance v11, Ljava/lang/String;

    const-string v12, "UTF-8"

    invoke-static {v12}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v12

    move-object/from16 v0, p4

    invoke-direct {v11, v0, v12}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2407
    iget-object v9, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mFactory:Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommandFactory;

    const-string v10, "processFIDO"

    const/4 v11, 0x4

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v11, v12

    const/4 v12, 0x1

    invoke-virtual {v4}, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->getAppName()Ljava/lang/String;

    move-result-object v13

    aput-object v13, v11, v12

    const/4 v12, 0x2

    aput-object p3, v11, v12

    const/4 v12, 0x3

    aput-object p4, v11, v12

    invoke-virtual {v9, v10, v11}, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommandFactory;->create(Ljava/lang/String;[Ljava/lang/Object;)Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;

    move-result-object v1

    .line 2414
    .local v1, "cmd":Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;
    invoke-direct {p0, v4, v1}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->enqueueCommand(Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, [B

    move-object v5, v9

    check-cast v5, [B

    .line 2416
    const-string v10, "FingerprintManagerService"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "processFIDO: result="

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    if-nez v5, :cond_6

    const-string v9, "NULL"

    :goto_2
    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v10, v9}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move-object v6, v5

    .line 2418
    .end local v5    # "resonseData":[B
    .restart local v6    # "resonseData":[B
    goto/16 :goto_0

    .line 2416
    .end local v6    # "resonseData":[B
    .restart local v5    # "resonseData":[B
    :cond_6
    new-instance v9, Ljava/lang/String;

    const-string v12, "UTF-8"

    invoke-static {v12}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v12

    invoke-direct {v9, v5, v12}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    goto :goto_2
.end method

.method public protect(Landroid/os/IBinder;Ljava/lang/String;[B)[B
    .locals 12
    .param p1, "token"    # Landroid/os/IBinder;
    .param p2, "permissionName"    # Ljava/lang/String;
    .param p3, "unprotectedData"    # [B

    .prologue
    .line 2274
    const-string v1, "protect"

    .line 2275
    .local v1, "methodName":Ljava/lang/String;
    const/4 v2, 0x0

    .line 2277
    .local v2, "protectedData":[B
    invoke-direct {p0}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->getUserId()I

    move-result v6

    .line 2279
    .local v6, "userId":I
    invoke-static {p1}, Lcom/samsung/android/fingerprint/service/util/ClientInfoHelper;->simplifyObjectInfo(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 2281
    .local v5, "tokenInfo":Ljava/lang/String;
    const-string v7, "FingerprintManagerService"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "protect: userId="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", token="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", permissionName="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " from pid="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2285
    sget-boolean v7, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->DEBUG:Z

    if-nez v7, :cond_0

    .line 2286
    iget-object v7, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mContext:Landroid/content/Context;

    const-string v8, "com.samsung.android.permission.PAYLOAD_FINGERPRINT"

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v9}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    .line 2289
    :cond_0
    invoke-direct {p0, p1}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->findClientRecord(Landroid/os/IBinder;)Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

    move-result-object v4

    .line 2291
    .local v4, "record":Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;
    if-nez v4, :cond_1

    .line 2292
    const-string v7, "FingerprintManagerService"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "identify: invalid token("

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ") was given."

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    move-object v3, v2

    .line 2319
    .end local v2    # "protectedData":[B
    .local v3, "protectedData":[B
    :goto_0
    return-object v3

    .line 2296
    .end local v3    # "protectedData":[B
    .restart local v2    # "protectedData":[B
    :cond_1
    iget-object v7, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mImpl:Lcom/samsung/android/fingerprint/service/FingerprintLibraryImplAbstractClass;

    invoke-virtual {v7}, Lcom/samsung/android/fingerprint/service/FingerprintLibraryImplAbstractClass;->isSensorReady()Z

    move-result v7

    if-nez v7, :cond_2

    .line 2297
    const-string v7, "FingerprintManagerService"

    const-string v8, "Sensor is not ready!!!"

    invoke-static {v7, v8}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    move-object v3, v2

    .line 2298
    .end local v2    # "protectedData":[B
    .restart local v3    # "protectedData":[B
    goto :goto_0

    .line 2301
    .end local v3    # "protectedData":[B
    .restart local v2    # "protectedData":[B
    :cond_2
    if-eqz p3, :cond_3

    array-length v7, p3

    if-nez v7, :cond_4

    .line 2302
    :cond_3
    const-string v7, "FingerprintManagerService"

    const-string v8, "protect: invalid data"

    invoke-static {v7, v8}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    move-object v3, v2

    .line 2303
    .end local v2    # "protectedData":[B
    .restart local v3    # "protectedData":[B
    goto :goto_0

    .line 2306
    .end local v3    # "protectedData":[B
    .restart local v2    # "protectedData":[B
    :cond_4
    const-string v7, "FingerprintManagerService"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "protect: unprotectedData="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    new-instance v9, Ljava/lang/String;

    const-string v10, "UTF-8"

    invoke-static {v10}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v10

    invoke-direct {v9, p3, v10}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2308
    iget-object v7, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mFactory:Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommandFactory;

    const-string v8, "protect"

    const/4 v9, 0x4

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x1

    invoke-virtual {v4}, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->getAppName()Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x2

    aput-object p2, v9, v10

    const/4 v10, 0x3

    aput-object p3, v9, v10

    invoke-virtual {v7, v8, v9}, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommandFactory;->create(Ljava/lang/String;[Ljava/lang/Object;)Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;

    move-result-object v0

    .line 2315
    .local v0, "cmd":Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;
    invoke-direct {p0, v4, v0}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->enqueueCommand(Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, [B

    move-object v2, v7

    check-cast v2, [B

    .line 2317
    const-string v8, "FingerprintManagerService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "protect: result="

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    if-nez v2, :cond_5

    const-string v7, "NULL"

    :goto_1
    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v8, v7}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move-object v3, v2

    .line 2319
    .end local v2    # "protectedData":[B
    .restart local v3    # "protectedData":[B
    goto/16 :goto_0

    .line 2317
    .end local v3    # "protectedData":[B
    .restart local v2    # "protectedData":[B
    :cond_5
    new-instance v7, Ljava/lang/String;

    const-string v10, "UTF-8"

    invoke-static {v10}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v10

    invoke-direct {v7, v2, v10}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    goto :goto_1
.end method

.method public reEnroll(Landroid/os/IBinder;I)I
    .locals 2
    .param p1, "token"    # Landroid/os/IBinder;
    .param p2, "fingerIndex"    # I

    .prologue
    .line 708
    const-string v0, "FingerprintManagerService"

    const-string v1, "reEnroll: called"

    invoke-static {v0, v1}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 709
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, p1, v0, p2, v1}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->moreEnroll(Landroid/os/IBinder;Ljava/lang/String;II)I

    move-result v0

    return v0
.end method

.method public registerClient(Lcom/samsung/android/fingerprint/IFingerprintClient;Landroid/os/Bundle;)Landroid/os/IBinder;
    .locals 12
    .param p1, "client"    # Lcom/samsung/android/fingerprint/IFingerprintClient;
    .param p2, "clientSpec"    # Landroid/os/Bundle;

    .prologue
    const/4 v7, 0x0

    .line 1363
    if-nez p1, :cond_0

    .line 1364
    const-string v9, "FingerprintManagerService"

    const-string v10, "registerClient: client may not be null."

    invoke-static {v9, v10}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1417
    :goto_0
    return-object v7

    .line 1368
    :cond_0
    if-nez p2, :cond_1

    .line 1369
    const-string v9, "FingerprintManagerService"

    const-string v10, "registerClient: clientSpec may not be null."

    invoke-static {v9, v10}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1373
    :cond_1
    invoke-interface {p1}, Lcom/samsung/android/fingerprint/IFingerprintClient;->asBinder()Landroid/os/IBinder;

    move-result-object v7

    .line 1375
    .local v7, "token":Landroid/os/IBinder;
    invoke-static {p1}, Lcom/samsung/android/fingerprint/service/util/ClientInfoHelper;->simplifyObjectInfo(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 1376
    .local v1, "clientInfo":Ljava/lang/String;
    invoke-static {v7}, Lcom/samsung/android/fingerprint/service/util/ClientInfoHelper;->simplifyObjectInfo(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 1378
    .local v8, "tokenInfo":Ljava/lang/String;
    const-string v9, "FingerprintManagerService"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "registerClient: client="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " as token="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " from pid="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1382
    const-string v9, "FingerprintManagerService"

    const-string v10, "registerClient: LOCK: wait to obtain."

    invoke-static {v9, v10}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->lock(Ljava/lang/String;Ljava/lang/String;)V

    .line 1384
    monitor-enter p0

    .line 1385
    :try_start_0
    const-string v9, "FingerprintManagerService"

    const-string v10, "registerClient: LOCK: obtained."

    invoke-static {v9, v10}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->lock(Ljava/lang/String;Ljava/lang/String;)V

    .line 1386
    const/4 v6, 0x0

    .line 1387
    .local v6, "record":Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;
    iget-object v9, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mTokenMap:Ljava/util/Map;

    invoke-interface {v9, v7}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 1388
    const-string v9, "FingerprintManagerService"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "registerClient: client("

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ") already registered, so attempt to update clientSpec"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 1390
    iget-object v9, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mTokenMap:Ljava/util/Map;

    invoke-interface {v9, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    .end local v6    # "record":Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;
    check-cast v6, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

    .line 1392
    .restart local v6    # "record":Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;
    if-eqz v6, :cond_2

    invoke-virtual {v6, p2}, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->setClientSpecFromBundle(Landroid/os/Bundle;)Z

    move-result v9

    if-nez v9, :cond_3

    .line 1393
    :cond_2
    const-string v9, "FingerprintManagerService"

    const-string v10, "registerClient: setClientSpecFromBundle failed."

    invoke-static {v9, v10}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 1401
    :cond_3
    :goto_1
    if-eqz v6, :cond_4

    .line 1402
    invoke-virtual {v6}, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->getPackageName()Ljava/lang/String;

    move-result-object v5

    .line 1403
    .local v5, "packageName":Ljava/lang/String;
    if-eqz v5, :cond_4

    .line 1404
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mWhiteAppListForKeyguard:[Ljava/lang/String;

    .local v0, "arr$":[Ljava/lang/String;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_2
    if-ge v2, v3, :cond_4

    aget-object v4, v0, v2

    .line 1405
    .local v4, "name":Ljava/lang/String;
    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 1406
    const-string v9, "FingerprintManagerService"

    const-string v10, "registerClient: Keyguard client"

    invoke-static {v9, v10}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1407
    invoke-virtual {v6}, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->setKeyguardClient()V

    .line 1413
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    .end local v4    # "name":Ljava/lang/String;
    .end local v5    # "packageName":Ljava/lang/String;
    :cond_4
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1415
    const-string v9, "FingerprintManagerService"

    const-string v10, "registerClient: LOCK: released."

    invoke-static {v9, v10}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->lock(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1396
    :cond_5
    :try_start_1
    new-instance v6, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

    .end local v6    # "record":Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v9

    invoke-direct {v6, p0, p1, p2, v9}, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;-><init>(Lcom/samsung/android/fingerprint/service/FingerprintManagerService;Lcom/samsung/android/fingerprint/IFingerprintClient;Landroid/os/Bundle;I)V

    .line 1398
    .restart local v6    # "record":Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;
    iget-object v9, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mTokenMap:Ljava/util/Map;

    invoke-interface {v9, v7, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1399
    iget-object v9, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mClientList:Ljava/util/List;

    invoke-interface {v9, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1413
    .end local v6    # "record":Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;
    :catchall_0
    move-exception v9

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v9

    .line 1404
    .restart local v0    # "arr$":[Ljava/lang/String;
    .restart local v2    # "i$":I
    .restart local v3    # "len$":I
    .restart local v4    # "name":Ljava/lang/String;
    .restart local v5    # "packageName":Ljava/lang/String;
    .restart local v6    # "record":Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;
    :cond_6
    add-int/lit8 v2, v2, 0x1

    goto :goto_2
.end method

.method public removeAllEnrolledFingers()Z
    .locals 1

    .prologue
    .line 1167
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->removeAllEnrolledFingers(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public removeAllEnrolledFingers(Ljava/lang/String;)Z
    .locals 10
    .param p1, "ownName"    # Ljava/lang/String;

    .prologue
    const/4 v9, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1172
    const-string v6, "FingerprintManagerService"

    const-string v7, "removeAllEnrolledFingers: called."

    invoke-static {v6, v7}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1176
    iget-object v6, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mContext:Landroid/content/Context;

    const-string v7, "com.samsung.android.permission.ENROLL_FINGERPRINT"

    invoke-virtual {v6, v7, v9}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    .line 1179
    invoke-direct {p0}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->getUserId()I

    move-result v6

    invoke-static {v6, p1}, Lcom/samsung/android/fingerprint/service/util/ValueUtils;->getStringUserId(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1181
    .local v3, "stringUserId":Ljava/lang/String;
    iget-object v6, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mFactory:Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommandFactory;

    const-string v7, "removeAllEnrolledFingers"

    new-array v8, v5, [Ljava/lang/Object;

    aput-object v3, v8, v4

    invoke-virtual {v6, v7, v8}, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommandFactory;->create(Ljava/lang/String;[Ljava/lang/Object;)Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;

    move-result-object v0

    .line 1185
    .local v0, "cmd":Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;
    iget-object v6, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mSilentClient:Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

    invoke-direct {p0, v6, v0}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->enqueueCommand(Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    .line 1186
    .local v2, "result":Ljava/lang/Integer;
    if-nez v2, :cond_1

    .line 1187
    const-string v5, "FingerprintManagerService"

    const-string v6, "enqueueCommand result is null"

    invoke-static {v5, v6}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1200
    :cond_0
    :goto_0
    return v4

    .line 1191
    :cond_1
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v6

    if-nez v6, :cond_0

    .line 1192
    if-nez p1, :cond_3

    .line 1193
    const/4 v1, 0x1

    .local v1, "i":I
    :goto_1
    const/16 v6, 0x15

    if-ge v1, v6, :cond_2

    .line 1194
    invoke-virtual {p0, v1, v9}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->setIndexName(ILjava/lang/String;)Z

    .line 1193
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1196
    :cond_2
    const/4 v6, -0x1

    invoke-direct {p0, v6, v4}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->requestNotifyFingerRemoved(II)V

    .end local v1    # "i":I
    :cond_3
    move v4, v5

    .line 1198
    goto :goto_0
.end method

.method public removeEnrolledFinger(I)Z
    .locals 1
    .param p1, "fingerIndex"    # I

    .prologue
    .line 1130
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->removeEnrolledFinger(ILjava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public removeEnrolledFinger(ILjava/lang/String;)Z
    .locals 11
    .param p1, "fingerIndex"    # I
    .param p2, "ownName"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x1

    const/4 v10, 0x0

    const/4 v4, 0x0

    .line 1134
    const-string v6, "FingerprintManagerService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "removeEnrolledFinger: fingerIndex="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", ownName="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1138
    iget-object v6, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mContext:Landroid/content/Context;

    const-string v7, "com.samsung.android.permission.ENROLL_FINGERPRINT"

    invoke-virtual {v6, v7, v10}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    .line 1141
    invoke-direct {p0}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->getUserId()I

    move-result v6

    invoke-static {v6, p2}, Lcom/samsung/android/fingerprint/service/util/ValueUtils;->getStringUserId(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1143
    .local v3, "stringUserId":Ljava/lang/String;
    iget-object v6, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mFactory:Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommandFactory;

    const-string v7, "removeEnrolledFinger"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    aput-object v3, v8, v4

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v5

    invoke-virtual {v6, v7, v8}, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommandFactory;->create(Ljava/lang/String;[Ljava/lang/Object;)Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;

    move-result-object v0

    .line 1148
    .local v0, "cmd":Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;
    iget-object v6, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mSilentClient:Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

    invoke-direct {p0, v6, v0}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->enqueueCommand(Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    .line 1150
    .local v2, "result":Ljava/lang/Integer;
    if-nez v2, :cond_1

    .line 1151
    const-string v5, "FingerprintManagerService"

    const-string v6, "enqueueCommand result is null"

    invoke-static {v5, v6}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1163
    :cond_0
    :goto_0
    return v4

    .line 1155
    :cond_1
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v6

    if-nez v6, :cond_0

    .line 1156
    if-nez p2, :cond_2

    .line 1157
    invoke-virtual {p0, p1, v10}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->setIndexName(ILjava/lang/String;)Z

    .line 1158
    invoke-virtual {p0, v10}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->getEnrolledFingers(Ljava/lang/String;)I

    move-result v1

    .line 1159
    .local v1, "enrolledFingerNumber":I
    invoke-direct {p0, p1, v1}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->requestNotifyFingerRemoved(II)V

    .end local v1    # "enrolledFingerNumber":I
    :cond_2
    move v4, v5

    .line 1161
    goto :goto_0
.end method

.method public removeEnrolledPermission(Ljava/lang/String;Ljava/lang/String;I)Z
    .locals 9
    .param p1, "appName"    # Ljava/lang/String;
    .param p2, "permissionName"    # Ljava/lang/String;
    .param p3, "fingerIndex"    # I

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1078
    const-string v5, "FingerprintManagerService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "removeEnrolledPermission: appName="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", permissionName="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", fingerIndex="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1082
    invoke-direct {p0}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->getUserId()I

    move-result v2

    .line 1086
    .local v2, "userId":I
    iget-object v5, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mContext:Landroid/content/Context;

    const-string v6, "com.samsung.android.permission.ENROLL_FINGERPRINT"

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    .line 1089
    if-nez p1, :cond_0

    .line 1090
    const-string v4, "FingerprintManagerService"

    const-string v5, "removeEnrolledPermission: appName may not be null"

    invoke-static {v4, v5}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1125
    :goto_0
    return v3

    .line 1094
    :cond_0
    if-nez p2, :cond_1

    .line 1095
    const-string v4, "FingerprintManagerService"

    const-string v5, "removeEnrolledPermission: permissionName may not be null"

    invoke-static {v4, v5}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1099
    :cond_1
    const/16 v5, 0xb

    if-lt p3, v5, :cond_2

    const/16 v5, 0x14

    if-gt p3, v5, :cond_2

    .line 1102
    const-string v3, "FingerprintManagerService"

    const-string v5, "removeEnrolledPermission: 2nd enroll is skipped"

    invoke-static {v3, v5}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->da(Ljava/lang/String;Ljava/lang/String;)V

    move v3, v4

    .line 1103
    goto :goto_0

    .line 1106
    :cond_2
    if-lt p3, v4, :cond_3

    const/16 v5, 0xa

    if-le p3, v5, :cond_4

    .line 1109
    :cond_3
    const-string v4, "FingerprintManagerService"

    const-string v5, "removeEnrolledPermission: wrong fingerIndex was given"

    invoke-static {v4, v5}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1113
    :cond_4
    iget-object v5, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mFactory:Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommandFactory;

    const-string v6, "removeEnrolledPermission"

    const/4 v7, 0x4

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v3

    aput-object p1, v7, v4

    const/4 v4, 0x2

    aput-object p2, v7, v4

    const/4 v4, 0x3

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v4

    invoke-virtual {v5, v6, v7}, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommandFactory;->create(Ljava/lang/String;[Ljava/lang/Object;)Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;

    move-result-object v0

    .line 1120
    .local v0, "cmd":Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;
    iget-object v4, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mSilentClient:Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

    invoke-direct {p0, v4, v0}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->enqueueCommand(Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;)Ljava/lang/Object;

    move-result-object v1

    .line 1121
    .local v1, "retObj":Ljava/lang/Object;
    if-nez v1, :cond_5

    .line 1122
    const-string v4, "FingerprintManagerService"

    const-string v5, "enqueueCommand result is null"

    invoke-static {v4, v5}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1125
    :cond_5
    check-cast v1, Ljava/lang/Boolean;

    .end local v1    # "retObj":Ljava/lang/Object;
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    goto :goto_0
.end method

.method public removeEnrolledPermissions(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 9
    .param p1, "appName"    # Ljava/lang/String;
    .param p2, "permissionName"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 1053
    const-string v5, "FingerprintManagerService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "removeEnrolledPermissions: appName="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", permissionName="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1058
    iget-object v5, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mContext:Landroid/content/Context;

    const-string v6, "com.samsung.android.permission.ENROLL_FINGERPRINT"

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    .line 1061
    invoke-direct {p0}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->getUserId()I

    move-result v2

    .line 1063
    .local v2, "userId":I
    iget-object v5, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mFactory:Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommandFactory;

    const-string v6, "removeEnrolledPermissions"

    const/4 v7, 0x3

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v4

    aput-object p1, v7, v3

    const/4 v8, 0x2

    aput-object p2, v7, v8

    invoke-virtual {v5, v6, v7}, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommandFactory;->create(Ljava/lang/String;[Ljava/lang/Object;)Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;

    move-result-object v0

    .line 1069
    .local v0, "cmd":Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;
    iget-object v5, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mSilentClient:Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

    invoke-direct {p0, v5, v0}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->enqueueCommand(Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;)Ljava/lang/Object;

    move-result-object v1

    .line 1070
    .local v1, "retObj":Ljava/lang/Object;
    if-nez v1, :cond_0

    .line 1071
    const-string v3, "FingerprintManagerService"

    const-string v5, "enqueueCommand result is null"

    invoke-static {v3, v5}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1074
    .end local v1    # "retObj":Ljava/lang/Object;
    :goto_0
    return v4

    .restart local v1    # "retObj":Ljava/lang/Object;
    :cond_0
    check-cast v1, Ljava/lang/Integer;

    .end local v1    # "retObj":Ljava/lang/Object;
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v5

    if-nez v5, :cond_1

    :goto_1
    move v4, v3

    goto :goto_0

    :cond_1
    move v3, v4

    goto :goto_1
.end method

.method public scheduleNotifyBackgroundClients(Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;)V
    .locals 0
    .param p1, "caller"    # Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

    .prologue
    .line 1320
    return-void
.end method

.method public secondaryEnroll(Landroid/os/IBinder;I)I
    .locals 2
    .param p1, "token"    # Landroid/os/IBinder;
    .param p2, "fingerIndex"    # I

    .prologue
    .line 713
    const-string v0, "FingerprintManagerService"

    const-string v1, "secondaryEnroll: called"

    invoke-static {v0, v1}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 714
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, p1, v0, p2, v1}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->moreEnroll(Landroid/os/IBinder;Ljava/lang/String;II)I

    move-result v0

    return v0
.end method

.method setFocusedClient(Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;)V
    .locals 6
    .param p1, "record"    # Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

    .prologue
    .line 1861
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mFocusedClient:Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

    .line 1864
    .local v1, "focusedClient":Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;
    iget-object v3, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mFocusedClientSyncThread:Ljava/lang/Thread;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v4

    if-ne v3, v4, :cond_1

    new-instance v2, Ljava/lang/Object;

    invoke-direct {v2}, Ljava/lang/Object;-><init>()V

    .line 1865
    .local v2, "syncObj":Ljava/lang/Object;
    :goto_0
    monitor-enter v2

    .line 1867
    if-eqz v1, :cond_4

    .line 1868
    :try_start_0
    invoke-virtual {v1}, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->getCommand()Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;

    move-result-object v0

    .line 1869
    .local v0, "cmd":Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;
    if-eqz v0, :cond_3

    .line 1870
    const-string v3, "FingerprintManagerService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "setFocusedClient: mFocusedClient Command is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v0, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;->commandName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1871
    invoke-virtual {v0}, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;->requestCancel()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1872
    if-eqz p1, :cond_0

    .line 1873
    const-string v3, "FingerprintManagerService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "setFocusedClient: mNextFocusedClient is changed from ["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mNextFocusedClient:Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "] to ["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1874
    iput-object p1, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mNextFocusedClient:Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

    .line 1895
    .end local v0    # "cmd":Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;
    :cond_0
    :goto_1
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1896
    return-void

    .line 1864
    .end local v2    # "syncObj":Ljava/lang/Object;
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mFocusedClientSync:Ljava/lang/Object;

    goto :goto_0

    .line 1878
    .restart local v0    # "cmd":Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;
    .restart local v2    # "syncObj":Ljava/lang/Object;
    :cond_2
    :try_start_1
    const-string v3, "FingerprintManagerService"

    const-string v4, "setFocusedClient: Failed to cancel current command"

    invoke-static {v3, v4}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 1895
    .end local v0    # "cmd":Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;
    :catchall_0
    move-exception v3

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v3

    .line 1881
    .restart local v0    # "cmd":Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;
    :cond_3
    :try_start_2
    const-string v3, "FingerprintManagerService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "setFocusedClient: mFocusedClient is changed from ["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "] to ["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1882
    iput-object p1, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mFocusedClient:Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

    goto :goto_1

    .line 1886
    .end local v0    # "cmd":Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;
    :cond_4
    const-string v3, "FingerprintManagerService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "setFocusedClient: mFocusedClient is changed from ["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "] to ["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1887
    iput-object p1, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mFocusedClient:Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method public setIndexName(ILjava/lang/String;)Z
    .locals 3
    .param p1, "index"    # I
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 1759
    const-string v0, "FingerprintManagerService"

    const-string v1, "setIndexName: called."

    invoke-static {v0, v1}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1761
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mContext:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 1762
    const-string v0, "FingerprintManagerService"

    const-string v1, "setIndexName: mContext is null!!!"

    invoke-static {v0, v1}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1763
    const/4 v0, 0x0

    .line 1769
    :goto_0
    return v0

    .line 1766
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mContext:Landroid/content/Context;

    invoke-direct {p0, p1, p2, v0}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->setSPValue(ILjava/lang/String;Landroid/content/Context;)V

    .line 1767
    const-string v0, "FingerprintManagerService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setIndexName: idx="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " name="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1769
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public setPassword(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 8
    .param p1, "newPassword"    # Ljava/lang/String;
    .param p2, "ownName"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2128
    const-string v5, "FingerprintManagerService"

    const-string v6, "setPassword: called"

    invoke-static {v5, v6}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2131
    iget-object v5, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mContext:Landroid/content/Context;

    const-string v6, "com.samsung.android.permission.WRITE_PASSWORD_FINGERPRINT"

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    .line 2134
    iget-object v5, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mImpl:Lcom/samsung/android/fingerprint/service/FingerprintLibraryImplAbstractClass;

    invoke-virtual {v5}, Lcom/samsung/android/fingerprint/service/FingerprintLibraryImplAbstractClass;->isSensorReady()Z

    move-result v5

    if-nez v5, :cond_1

    .line 2135
    const-string v4, "FingerprintManagerService"

    const-string v5, "Sensor is not ready!!!"

    invoke-static {v4, v5}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 2156
    :cond_0
    :goto_0
    return v3

    .line 2139
    :cond_1
    invoke-direct {p0}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->getUserId()I

    move-result v5

    invoke-static {v5, p2}, Lcom/samsung/android/fingerprint/service/util/ValueUtils;->getStringUserId(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2141
    .local v1, "mUserId":Ljava/lang/String;
    iget-object v5, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mFactory:Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommandFactory;

    const-string v6, "setPassword"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    aput-object v1, v7, v3

    aput-object p1, v7, v4

    invoke-virtual {v5, v6, v7}, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommandFactory;->create(Ljava/lang/String;[Ljava/lang/Object;)Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;

    move-result-object v0

    .line 2146
    .local v0, "cmd":Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;
    iget-object v5, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mSilentClient:Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

    invoke-direct {p0, v5, v0}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->enqueueCommand(Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    .line 2147
    .local v2, "result":Ljava/lang/Integer;
    if-eqz v2, :cond_0

    .line 2148
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v5

    if-nez v5, :cond_0

    .line 2149
    invoke-direct {p0}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->requestNotifyFingerPasswordUpdated()V

    .line 2150
    sget v5, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mEnrolledFingerCount:I

    if-nez v5, :cond_2

    .line 2151
    iget v5, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mLatestEnrolledFingerIndex:I

    invoke-direct {p0, v5, v3}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->requestNotifyFingerAdded(II)V

    :cond_2
    move v3, v4

    .line 2153
    goto :goto_0
.end method

.method public showIdentifyDialog(Landroid/os/IBinder;Landroid/content/ComponentName;Ljava/lang/String;Z)I
    .locals 10
    .param p1, "token"    # Landroid/os/IBinder;
    .param p2, "comName"    # Landroid/content/ComponentName;
    .param p3, "permissionName"    # Ljava/lang/String;
    .param p4, "password"    # Z

    .prologue
    const/4 v5, -0x3

    const/4 v6, 0x0

    .line 831
    invoke-direct {p0}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->isKeyGuardLocked()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 832
    const-string v5, "FingerprintManagerService"

    const-string v6, "showIdentifyDialog: KeyGuardLocked!!"

    invoke-static {v5, v6}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 833
    const/4 v5, -0x1

    .line 858
    :goto_0
    return v5

    .line 835
    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->getUserId()I

    move-result v4

    .line 836
    .local v4, "userId":I
    invoke-static {p1}, Lcom/samsung/android/fingerprint/service/util/ClientInfoHelper;->simplifyObjectInfo(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 837
    .local v3, "tokenInfo":Ljava/lang/String;
    const-string v7, "FingerprintManagerService"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "showIdentifyDialog: userId="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", token="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", permissionName="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " from pid="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 842
    invoke-direct {p0, p1}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->findClientRecord(Landroid/os/IBinder;)Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

    move-result-object v2

    .line 843
    .local v2, "record":Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;
    if-nez v2, :cond_1

    .line 844
    const-string v6, "FingerprintManagerService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "showIdentifyDialog: invalid token("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ") was given. return Invalid token"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 847
    :cond_1
    const-string v7, "com.samsung.android.fingerprint.FingerprintIdentifyDialog"

    invoke-virtual {v2}, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->getAppName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_2

    .line 848
    const-string v6, "FingerprintManagerService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "showIdentifyDialog: invalid clientspec:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v2}, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->getAppName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 851
    :cond_2
    invoke-virtual {v2, p2}, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->setComponentName(Landroid/content/ComponentName;)V

    .line 852
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 853
    .local v1, "attribute":Landroid/os/Bundle;
    const-string v5, "password"

    invoke-virtual {v1, v5, p4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 855
    new-instance v0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService$IdentifyDialogObject;

    const/4 v5, 0x0

    invoke-direct {v0, v5}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService$IdentifyDialogObject;-><init>(Lcom/samsung/android/fingerprint/service/FingerprintManagerService$1;)V

    .line 856
    .local v0, "IdentifyDialogObject":Lcom/samsung/android/fingerprint/service/FingerprintManagerService$IdentifyDialogObject;
    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService$IdentifyDialogObject;->setAttributeAndRecord(Landroid/os/Bundle;Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;)Lcom/samsung/android/fingerprint/service/FingerprintManagerService$IdentifyDialogObject;

    .line 857
    iget-object v5, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mH:Lcom/samsung/android/fingerprint/service/FingerprintManagerService$H;

    const/16 v7, 0x264

    invoke-static {v5, v7, v6, v6, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v5

    invoke-virtual {v5}, Landroid/os/Message;->sendToTarget()V

    move v5, v6

    .line 858
    goto/16 :goto_0
.end method

.method public unprotect(Landroid/os/IBinder;Ljava/lang/String;[B)[B
    .locals 12
    .param p1, "token"    # Landroid/os/IBinder;
    .param p2, "permissionName"    # Ljava/lang/String;
    .param p3, "protectedData"    # [B

    .prologue
    .line 2323
    const-string v1, "unprotect"

    .line 2324
    .local v1, "methodName":Ljava/lang/String;
    const/4 v4, 0x0

    .line 2326
    .local v4, "unprotectedData":[B
    invoke-direct {p0}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->getUserId()I

    move-result v6

    .line 2328
    .local v6, "userId":I
    invoke-static {p1}, Lcom/samsung/android/fingerprint/service/util/ClientInfoHelper;->simplifyObjectInfo(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 2330
    .local v3, "tokenInfo":Ljava/lang/String;
    const-string v7, "FingerprintManagerService"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "unprotect: userId="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", token="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", permissionName="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " from pid="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2334
    sget-boolean v7, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->DEBUG:Z

    if-nez v7, :cond_0

    .line 2335
    iget-object v7, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mContext:Landroid/content/Context;

    const-string v8, "com.samsung.android.permission.PAYLOAD_FINGERPRINT"

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v9}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    .line 2338
    :cond_0
    invoke-direct {p0, p1}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->findClientRecord(Landroid/os/IBinder;)Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

    move-result-object v2

    .line 2340
    .local v2, "record":Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;
    if-nez v2, :cond_1

    .line 2341
    const-string v7, "FingerprintManagerService"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "unprotect: invalid token("

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ") was given."

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    move-object v5, v4

    .line 2368
    .end local v4    # "unprotectedData":[B
    .local v5, "unprotectedData":[B
    :goto_0
    return-object v5

    .line 2345
    .end local v5    # "unprotectedData":[B
    .restart local v4    # "unprotectedData":[B
    :cond_1
    iget-object v7, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mImpl:Lcom/samsung/android/fingerprint/service/FingerprintLibraryImplAbstractClass;

    invoke-virtual {v7}, Lcom/samsung/android/fingerprint/service/FingerprintLibraryImplAbstractClass;->isSensorReady()Z

    move-result v7

    if-nez v7, :cond_2

    .line 2346
    const-string v7, "FingerprintManagerService"

    const-string v8, "unprotect: Sensor is not ready!!!"

    invoke-static {v7, v8}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    move-object v5, v4

    .line 2347
    .end local v4    # "unprotectedData":[B
    .restart local v5    # "unprotectedData":[B
    goto :goto_0

    .line 2350
    .end local v5    # "unprotectedData":[B
    .restart local v4    # "unprotectedData":[B
    :cond_2
    if-eqz p3, :cond_3

    array-length v7, p3

    if-nez v7, :cond_4

    .line 2351
    :cond_3
    const-string v7, "FingerprintManagerService"

    const-string v8, "unprotect: invalid data"

    invoke-static {v7, v8}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    move-object v5, v4

    .line 2352
    .end local v4    # "unprotectedData":[B
    .restart local v5    # "unprotectedData":[B
    goto :goto_0

    .line 2355
    .end local v5    # "unprotectedData":[B
    .restart local v4    # "unprotectedData":[B
    :cond_4
    const-string v7, "FingerprintManagerService"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "unprotect: protectedData="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    new-instance v9, Ljava/lang/String;

    const-string v10, "UTF-8"

    invoke-static {v10}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v10

    invoke-direct {v9, p3, v10}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2357
    iget-object v7, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mFactory:Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommandFactory;

    const-string v8, "unProtect"

    const/4 v9, 0x4

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x1

    invoke-virtual {v2}, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->getAppName()Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x2

    aput-object p2, v9, v10

    const/4 v10, 0x3

    aput-object p3, v9, v10

    invoke-virtual {v7, v8, v9}, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommandFactory;->create(Ljava/lang/String;[Ljava/lang/Object;)Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;

    move-result-object v0

    .line 2364
    .local v0, "cmd":Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;
    invoke-direct {p0, v2, v0}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->enqueueCommand(Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, [B

    move-object v4, v7

    check-cast v4, [B

    .line 2366
    const-string v8, "FingerprintManagerService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "unprotect: result="

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    if-nez v4, :cond_5

    const-string v7, "NULL"

    :goto_1
    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v8, v7}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move-object v5, v4

    .line 2368
    .end local v4    # "unprotectedData":[B
    .restart local v5    # "unprotectedData":[B
    goto/16 :goto_0

    .line 2366
    .end local v5    # "unprotectedData":[B
    .restart local v4    # "unprotectedData":[B
    :cond_5
    new-instance v7, Ljava/lang/String;

    const-string v10, "UTF-8"

    invoke-static {v10}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v10

    invoke-direct {v7, v4, v10}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    goto :goto_1
.end method

.method public unregisterClient(Landroid/os/IBinder;)Z
    .locals 8
    .param p1, "token"    # Landroid/os/IBinder;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1492
    invoke-static {p1}, Lcom/samsung/android/fingerprint/service/util/ClientInfoHelper;->simplifyObjectInfo(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 1494
    .local v2, "tokenInfo":Ljava/lang/String;
    const-string v5, "FingerprintManagerService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "unregisterClient: token="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " from pid="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1497
    if-nez p1, :cond_0

    .line 1498
    const-string v4, "FingerprintManagerService"

    const-string v5, "unregisterClient: token may noy be null!!!"

    invoke-static {v4, v5}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1531
    :goto_0
    return v3

    .line 1502
    :cond_0
    const-string v5, "FingerprintManagerService"

    const-string v6, "unregisterClient: LOCK: wait to obtain."

    invoke-static {v5, v6}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->lock(Ljava/lang/String;Ljava/lang/String;)V

    .line 1504
    monitor-enter p0

    .line 1505
    :try_start_0
    const-string v5, "FingerprintManagerService"

    const-string v6, "unregisterClient: LOCK: obtained."

    invoke-static {v5, v6}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->lock(Ljava/lang/String;Ljava/lang/String;)V

    .line 1507
    iget-object v5, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mTokenMap:Ljava/util/Map;

    invoke-interface {v5, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 1508
    const-string v4, "FingerprintManagerService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "unregisterClient: client("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ") is not registered!!!"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 1509
    monitor-exit p0

    goto :goto_0

    .line 1527
    :catchall_0
    move-exception v3

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    .line 1512
    :cond_1
    :try_start_1
    iget-object v3, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mTokenMap:Ljava/util/Map;

    invoke-interface {v3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

    .line 1513
    .local v1, "record":Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;
    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->setCancelState(Z)V

    .line 1514
    iget-object v3, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mNextFocusedClient:Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

    if-ne v3, v1, :cond_2

    .line 1515
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mNextFocusedClient:Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

    .line 1517
    :cond_2
    invoke-virtual {v1}, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->getCommand()Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;

    move-result-object v0

    .line 1518
    .local v0, "cmd":Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;
    if-eqz v0, :cond_4

    .line 1519
    invoke-virtual {v0}, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;->requestCancel()Z

    move-result v3

    if-nez v3, :cond_3

    .line 1520
    const-string v3, "FingerprintManagerService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "unregisterClient: Failed to cancel ["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v0, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;->commandName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "] command"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1522
    :cond_3
    invoke-virtual {p0}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->updateFocusedClient()V

    .line 1524
    :cond_4
    invoke-virtual {v1}, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->cleanUp()V

    .line 1525
    iget-object v3, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mTokenMap:Ljava/util/Map;

    invoke-interface {v3, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1526
    iget-object v3, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mClientList:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1527
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1529
    const-string v3, "FingerprintManagerService"

    const-string v5, "unregisterClient: LOCK: released."

    invoke-static {v3, v5}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->lock(Ljava/lang/String;Ljava/lang/String;)V

    move v3, v4

    .line 1531
    goto/16 :goto_0
.end method

.method declared-synchronized updateFocusedClient()V
    .locals 3

    .prologue
    .line 1853
    monitor-enter p0

    :try_start_0
    const-string v0, "FingerprintManagerService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "updateFocusedClient: mFocusedClient is changed from ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mFocusedClient:Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] to ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mNextFocusedClient:Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1854
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mNextFocusedClient:Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

    iput-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mFocusedClient:Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

    .line 1855
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mNextFocusedClient:Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1856
    monitor-exit p0

    return-void

    .line 1853
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public verifyPassword(Landroid/os/IBinder;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 12
    .param p1, "token"    # Landroid/os/IBinder;
    .param p2, "password"    # Ljava/lang/String;
    .param p3, "ownName"    # Ljava/lang/String;

    .prologue
    .line 2064
    const-string v7, "FingerprintManagerService"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "verifyPassword: called from pid="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2067
    iget-object v7, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mContext:Landroid/content/Context;

    const-string v8, "com.samsung.android.permission.READ_PASSWORD_FINGERPRINT"

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v9}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    .line 2070
    const/4 v5, 0x0

    .line 2071
    .local v5, "retVal":Z
    const/4 v1, 0x0

    .line 2072
    .local v1, "isSettingApp":Z
    iget-object v7, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mIdentifyDialogManager:Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;

    if-eqz v7, :cond_0

    .line 2073
    iget-object v7, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mIdentifyDialogManager:Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;

    invoke-virtual {v7}, Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;->getCallerClient()Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

    move-result-object v7

    if-eqz v7, :cond_0

    .line 2074
    const-string v7, "com.android.settings"

    iget-object v8, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mIdentifyDialogManager:Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;

    invoke-virtual {v8}, Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;->getCallerClient()Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->getPackageName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 2077
    :cond_0
    iget-object v7, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mImpl:Lcom/samsung/android/fingerprint/service/FingerprintLibraryImplAbstractClass;

    invoke-virtual {v7}, Lcom/samsung/android/fingerprint/service/FingerprintLibraryImplAbstractClass;->isSensorReady()Z

    move-result v7

    if-nez v7, :cond_5

    .line 2078
    const-string v7, "FingerprintManagerService"

    const-string v8, "Sensor is not ready!!!"

    invoke-static {v7, v8}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 2079
    iget-boolean v7, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mIsEnrollSession:Z

    if-eqz v7, :cond_1

    if-nez v1, :cond_2

    .line 2080
    :cond_1
    invoke-direct {p0, p2}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->checkDigitalBackup(Ljava/lang/String;)Z

    move-result v5

    .line 2116
    :cond_2
    :goto_0
    if-eqz v5, :cond_4

    .line 2117
    const/4 v7, 0x0

    iput v7, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mVerificationTypeToEnroll:I

    .line 2118
    iget-boolean v7, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mIsEnrollSession:Z

    if-eqz v7, :cond_3

    .line 2119
    const/4 v7, 0x2

    iput v7, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mVerificationTypeToEnroll:I

    .line 2121
    :cond_3
    invoke-static {}, Lcom/samsung/android/fingerprint/service/util/Config;->resetFailedFingerprintAttempts()V

    .line 2124
    :cond_4
    return v5

    .line 2084
    :cond_5
    invoke-direct {p0, p1}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->findClientRecord(Landroid/os/IBinder;)Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

    move-result-object v3

    .line 2085
    .local v3, "record":Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;
    move-object v2, p3

    .line 2086
    .local v2, "mOwnName":Ljava/lang/String;
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_6

    if-eqz v3, :cond_6

    .line 2087
    invoke-virtual {v3}, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->getOwnName()Ljava/lang/String;

    move-result-object v2

    .line 2088
    const-string v7, "FingerprintManagerService"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "verifyPassword: update ownName="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 2090
    :cond_6
    invoke-direct {p0}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->getUserId()I

    move-result v7

    invoke-static {v7, v2}, Lcom/samsung/android/fingerprint/service/util/ValueUtils;->getStringUserId(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 2092
    .local v6, "stringUserId":Ljava/lang/String;
    iget-object v7, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mFactory:Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommandFactory;

    const-string v8, "verifyPassword"

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    aput-object v6, v9, v10

    const/4 v10, 0x1

    aput-object p2, v9, v10

    invoke-virtual {v7, v8, v9}, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommandFactory;->create(Ljava/lang/String;[Ljava/lang/Object;)Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;

    move-result-object v0

    .line 2097
    .local v0, "cmd":Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;
    iget-object v7, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mSilentClient:Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

    invoke-direct {p0, v7, v0}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->enqueueCommand(Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    .line 2098
    .local v4, "result":Ljava/lang/Integer;
    if-eqz v4, :cond_2

    .line 2099
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v7

    if-nez v7, :cond_8

    .line 2100
    if-eqz v3, :cond_7

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 2101
    const-string v7, "FPVP"

    invoke-virtual {v3}, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->getPackageName()Ljava/lang/String;

    move-result-object v8

    const-wide/16 v10, -0x1

    invoke-direct {p0, v7, v8, v10, v11}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->insertSurveyLog(Ljava/lang/String;Ljava/lang/String;J)V

    .line 2103
    :cond_7
    const/4 v5, 0x1

    goto :goto_0

    .line 2104
    :cond_8
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v7

    const/4 v8, 0x7

    if-ne v7, v8, :cond_a

    .line 2105
    iget-boolean v7, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mIsEnrollSession:Z

    if-eqz v7, :cond_9

    if-nez v1, :cond_2

    .line 2106
    :cond_9
    invoke-direct {p0, p2}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->checkDigitalBackup(Ljava/lang/String;)Z

    move-result v5

    goto/16 :goto_0

    .line 2109
    :cond_a
    if-eqz v3, :cond_2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 2110
    const-string v7, "FPVF"

    invoke-virtual {v3}, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->getPackageName()Ljava/lang/String;

    move-result-object v8

    const-wide/16 v10, -0x1

    invoke-direct {p0, v7, v8, v10, v11}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->insertSurveyLog(Ljava/lang/String;Ljava/lang/String;J)V

    goto/16 :goto_0
.end method
