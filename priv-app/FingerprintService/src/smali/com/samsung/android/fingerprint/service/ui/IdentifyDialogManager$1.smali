.class Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager$1;
.super Lcom/samsung/android/fingerprint/IFingerprintClient$Stub;
.source "IdentifyDialogManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;


# direct methods
.method constructor <init>(Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;)V
    .locals 0

    .prologue
    .line 60
    iput-object p1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager$1;->this$0:Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;

    invoke-direct {p0}, Lcom/samsung/android/fingerprint/IFingerprintClient$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public onFingerprintEvent(Lcom/samsung/android/fingerprint/FingerprintEvent;)V
    .locals 2
    .param p1, "event"    # Lcom/samsung/android/fingerprint/FingerprintEvent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 64
    if-nez p1, :cond_1

    .line 65
    const-string v0, "IdentifyDialogManager"

    const-string v1, "onFingerprintEvent: Invalid Event"

    invoke-static {v0, v1}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    :cond_0
    :goto_0
    return-void

    .line 68
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager$1;->this$0:Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;

    # getter for: Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;->mIdentifyDialog:Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog;
    invoke-static {v0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;->access$000(Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;)Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 69
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager$1;->this$0:Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;

    # getter for: Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;->mIdentifyDialog:Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog;
    invoke-static {v0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;->access$000(Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;)Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog;->onFingerprintEvent(Lcom/samsung/android/fingerprint/FingerprintEvent;)V

    goto :goto_0
.end method
