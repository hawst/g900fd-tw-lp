.class Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$20;
.super Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$ValiditySyncCommand;
.source "FingerprintLibraryImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;


# direct methods
.method constructor <init>(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p2, "x0"    # Ljava/lang/String;
    .param p3, "x1"    # Ljava/lang/String;

    .prologue
    .line 1563
    iput-object p1, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$20;->this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$ValiditySyncCommand;-><init>(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public doCommand()Ljava/lang/Object;
    .locals 5

    .prologue
    .line 1566
    const/4 v2, 0x0

    .line 1567
    .local v2, "returnObj":Ljava/lang/Object;
    const-string v3, "ValidityFingerprintLibraryImpl"

    const-string v4, "processFIDO: BEGIN"

    invoke-static {v3, v4}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1568
    new-instance v1, Lcom/validity/fingerprint/VcsByteArray;

    invoke-direct {v1}, Lcom/validity/fingerprint/VcsByteArray;-><init>()V

    .line 1569
    .local v1, "resultVcsByteArray":Lcom/validity/fingerprint/VcsByteArray;
    iget-object v3, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$20;->this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    # getter for: Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mFingerprint:Lcom/samsung/android/fingerprint/service/vendor/FingerprintProxy;
    invoke-static {v3}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->access$100(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;)Lcom/samsung/android/fingerprint/service/vendor/FingerprintProxy;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$20;->inputByteData:[B

    invoke-interface {v3, v4, v1}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintProxy;->processFIDO([BLcom/validity/fingerprint/VcsByteArray;)I

    move-result v0

    .line 1571
    .local v0, "result":I
    invoke-virtual {p0}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$20;->notifyCommandCompleted()V

    .line 1573
    if-eqz v1, :cond_0

    if-nez v0, :cond_0

    .line 1574
    iget-object v2, v1, Lcom/validity/fingerprint/VcsByteArray;->data:[B

    .line 1577
    .end local v2    # "returnObj":Ljava/lang/Object;
    :cond_0
    const-string v3, "ValidityFingerprintLibraryImpl"

    const-string v4, "processFIDO: END"

    invoke-static {v3, v4}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1578
    return-object v2
.end method
