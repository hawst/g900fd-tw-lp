.class public Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;
.super Ljava/lang/Object;
.source "FingerprintDBAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter$FingerptintData;,
        Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter$FingerprintDBThread;
    }
.end annotation


# static fields
.field private static final DEBUG_DUMP_PERMISSION_MAP:Z = true

.field public static final INSERT_FAIL_DUPLECATED_DATA:I = 0x0

.field public static final INSERT_FAIL_PERMISSION_IS_NULL:I = -0x1

.field private static final INSERT_FINGERPRINT_DATA:I = 0x0

.field public static final INSERT_SUCCESS:I = 0x1

.field private static final REMOVE_ALL_ENROLLED_FINGER:I = 0x4

.field private static final REMOVE_ENROLLED_FINGER:I = 0x2

.field private static final REMOVE_ENROLLED_PERMISSION:I = 0x3

.field private static final REMOVE_ENROLLED_PERMISSIONS:I = 0x1

.field public static final REMOVE_FAIL_NO_SUCH_DATA:I = 0x2

.field public static final REMOVE_FAIL_USERID_DATA_EMPTY:I = 0x3

.field public static final REMOVE_SUCCESS:I = 0x1

.field private static final TAG:Ljava/lang/String; = "FingerprintDBAdapter"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mFingerprintDB:Landroid/database/sqlite/SQLiteDatabase;

.field private mFingerprintDBHelper:Lcom/samsung/android/fingerprint/service/FingerprintDBHelper;

.field public mFingerprintDBThread:Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter$FingerprintDBThread;

.field private mPermissionMapArray:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Landroid/util/SparseArray",
            "<",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 151
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->mContext:Landroid/content/Context;

    .line 74
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->mPermissionMapArray:Landroid/util/SparseArray;

    .line 152
    iput-object p1, p0, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->mContext:Landroid/content/Context;

    .line 154
    new-instance v0, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter$FingerprintDBThread;

    const-string v1, "FingerprintDBThread"

    invoke-direct {v0, p0, v1}, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter$FingerprintDBThread;-><init>(Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->mFingerprintDBThread:Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter$FingerprintDBThread;

    .line 156
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;ILjava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;
    .param p1, "x1"    # I
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # I
    .param p4, "x4"    # Ljava/lang/String;

    .prologue
    .line 44
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->insertLocked(ILjava/lang/String;ILjava/lang/String;)V

    return-void
.end method

.method static synthetic access$100(Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;ILjava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;
    .param p1, "x1"    # I
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Ljava/lang/String;

    .prologue
    .line 44
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->removeEnrolledPermissionsLocked(ILjava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$200(Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;ILjava/lang/String;I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;
    .param p1, "x1"    # I
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # I

    .prologue
    .line 44
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->removeEnrolledFingerLocked(ILjava/lang/String;I)V

    return-void
.end method

.method static synthetic access$300(Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;ILjava/lang/String;Ljava/lang/String;I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;
    .param p1, "x1"    # I
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Ljava/lang/String;
    .param p4, "x4"    # I

    .prologue
    .line 44
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->removeEnrolledPermissionLocked(ILjava/lang/String;Ljava/lang/String;I)V

    return-void
.end method

.method static synthetic access$400(Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;ILjava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;
    .param p1, "x1"    # I
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 44
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->removeAllEnrolledFingersLocked(ILjava/lang/String;)V

    return-void
.end method

.method static synthetic access$500(Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;)Landroid/util/SparseArray;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->getPermissionMap()Landroid/util/SparseArray;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$600(Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;Landroid/util/SparseArray;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;
    .param p1, "x1"    # Landroid/util/SparseArray;

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->dumpPermissionMap(Landroid/util/SparseArray;)V

    return-void
.end method

.method private close()V
    .locals 1

    .prologue
    .line 183
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->mFingerprintDB:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 184
    return-void
.end method

.method private dumpPermissionMap(Landroid/util/SparseArray;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Landroid/util/SparseArray",
            "<",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;>;>;)V"
        }
    .end annotation

    .prologue
    .line 274
    .local p1, "mPermissionMap":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/Map<Ljava/lang/String;Landroid/util/SparseArray<Ljava/util/List<Ljava/lang/String;>;>;>;>;"
    invoke-direct {p0, p1}, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->permissionMapToString(Landroid/util/SparseArray;)Ljava/lang/String;

    move-result-object v0

    .line 276
    .local v0, "dumpPermissionMapString":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 277
    const-string v1, "FingerprintDBAdapter"

    const-string v2, "PermissionMap: Failed to dump"

    invoke-static {v1, v2}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 281
    :goto_0
    return-void

    .line 279
    :cond_0
    const-string v1, "FingerprintDBAdapter"

    invoke-static {v1, v0}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private getDBName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 187
    const-string v0, "fingerprint.db"

    return-object v0
.end method

.method private getPermissionMap()Landroid/util/SparseArray;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/SparseArray",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Landroid/util/SparseArray",
            "<",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;>;>;"
        }
    .end annotation

    .prologue
    .line 284
    new-instance v1, Landroid/util/SparseArray;

    invoke-direct {v1}, Landroid/util/SparseArray;-><init>()V

    .line 286
    .local v1, "permissionMap":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/Map<Ljava/lang/String;Landroid/util/SparseArray<Ljava/util/List<Ljava/lang/String;>;>;>;>;"
    invoke-direct {p0}, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->open()Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;

    .line 287
    invoke-direct {p0}, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->query()Landroid/database/Cursor;

    move-result-object v6

    .line 289
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 291
    :cond_0
    const-string v0, "USER_ID"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 292
    .local v2, "userId":I
    const-string v0, "APP_NAME"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 293
    .local v3, "appName":Ljava/lang/String;
    const-string v0, "FINGER_INDEX"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    .line 294
    .local v4, "fingerIndex":I
    const-string v0, "PERMISSION"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .local v5, "permission":Ljava/lang/String;
    move-object v0, p0

    .line 296
    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->makePermissionMap(Landroid/util/SparseArray;ILjava/lang/String;ILjava/lang/String;)Landroid/util/SparseArray;

    move-result-object v1

    .line 298
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 300
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 301
    invoke-direct {p0}, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->close()V

    move-object v7, v1

    .line 311
    .end local v1    # "permissionMap":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/Map<Ljava/lang/String;Landroid/util/SparseArray<Ljava/util/List<Ljava/lang/String;>;>;>;>;"
    .end local v2    # "userId":I
    .end local v3    # "appName":Ljava/lang/String;
    .end local v4    # "fingerIndex":I
    .end local v5    # "permission":Ljava/lang/String;
    .local v7, "permissionMap":Ljava/lang/Object;, "Landroid/util/SparseArray<Ljava/util/Map<Ljava/lang/String;Landroid/util/SparseArray<Ljava/util/List<Ljava/lang/String;>;>;>;>;"
    :goto_0
    return-object v7

    .line 305
    .end local v7    # "permissionMap":Ljava/lang/Object;, "Landroid/util/SparseArray<Ljava/util/Map<Ljava/lang/String;Landroid/util/SparseArray<Ljava/util/List<Ljava/lang/String;>;>;>;>;"
    .restart local v1    # "permissionMap":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/Map<Ljava/lang/String;Landroid/util/SparseArray<Ljava/util/List<Ljava/lang/String;>;>;>;>;"
    :cond_1
    const-string v0, "FingerprintDBAdapter"

    const-string v8, "No Data on DataBase"

    invoke-static {v0, v8}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->da(Ljava/lang/String;Ljava/lang/String;)V

    .line 307
    if-eqz v6, :cond_2

    .line 308
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    move-object v7, v1

    .line 311
    .restart local v7    # "permissionMap":Ljava/lang/Object;, "Landroid/util/SparseArray<Ljava/util/Map<Ljava/lang/String;Landroid/util/SparseArray<Ljava/util/List<Ljava/lang/String;>;>;>;>;"
    goto :goto_0
.end method

.method private insertLocked(ILjava/lang/String;ILjava/lang/String;)V
    .locals 4
    .param p1, "userId"    # I
    .param p2, "appName"    # Ljava/lang/String;
    .param p3, "fingerIndex"    # I
    .param p4, "permission"    # Ljava/lang/String;

    .prologue
    .line 409
    const-string v1, "FingerprintDBAdapter"

    const-string v2, "insertLocked BEGIN"

    invoke-static {v1, v2}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 410
    const-string v1, "FingerprintDBAdapter"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Insert data : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 412
    invoke-direct {p0}, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->open()Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;

    .line 415
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->mFingerprintDB:Landroid/database/sqlite/SQLiteDatabase;

    const-string v2, "INSERT INTO fingerprint_table VALUES(NULL, ?, ?, ?, ?)"

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v0

    .line 417
    .local v0, "st":Landroid/database/sqlite/SQLiteStatement;
    if-eqz v0, :cond_0

    .line 418
    const/4 v1, 0x1

    int-to-long v2, p1

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 419
    const/4 v1, 0x2

    invoke-virtual {v0, v1, p2}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 420
    const/4 v1, 0x3

    int-to-long v2, p3

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 421
    const/4 v1, 0x4

    invoke-virtual {v0, v1, p4}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 422
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteStatement;->executeInsert()J

    .line 424
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteStatement;->close()V

    .line 428
    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->close()V

    .line 429
    const-string v1, "FingerprintDBAdapter"

    const-string v2, "insertLocked END"

    invoke-static {v1, v2}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 430
    return-void
.end method

.method private makePermissionMap(Landroid/util/SparseArray;ILjava/lang/String;ILjava/lang/String;)Landroid/util/SparseArray;
    .locals 6
    .param p2, "userId"    # I
    .param p3, "appName"    # Ljava/lang/String;
    .param p4, "fingerIndex"    # I
    .param p5, "permission"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Landroid/util/SparseArray",
            "<",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;>;>;I",
            "Ljava/lang/String;",
            "I",
            "Ljava/lang/String;",
            ")",
            "Landroid/util/SparseArray",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Landroid/util/SparseArray",
            "<",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;>;>;"
        }
    .end annotation

    .prologue
    .local p1, "permissionMapArray":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/Map<Ljava/lang/String;Landroid/util/SparseArray<Ljava/util/List<Ljava/lang/String;>;>;>;>;"
    const/4 v3, 0x0

    .line 317
    const-string v4, "FingerprintDBAdapter"

    const-string v5, "makePermissionMap BEGIN"

    invoke-static {v4, v5}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 319
    if-nez p1, :cond_0

    .line 320
    const-string v4, "FingerprintDBAdapter"

    const-string v5, "makePermissionMap END : first parameter should not be null"

    invoke-static {v4, v5}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    move-object p1, v3

    .line 374
    .end local p1    # "permissionMapArray":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/Map<Ljava/lang/String;Landroid/util/SparseArray<Ljava/util/List<Ljava/lang/String;>;>;>;>;"
    :goto_0
    return-object p1

    .line 324
    .restart local p1    # "permissionMapArray":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/Map<Ljava/lang/String;Landroid/util/SparseArray<Ljava/util/List<Ljava/lang/String;>;>;>;>;"
    :cond_0
    invoke-virtual {p1, p2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 326
    .local v0, "appNameMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Landroid/util/SparseArray<Ljava/util/List<Ljava/lang/String;>;>;>;"
    if-nez v0, :cond_1

    .line 327
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 328
    .local v2, "permissionList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v2, p5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 330
    new-instance v1, Landroid/util/SparseArray;

    invoke-direct {v1}, Landroid/util/SparseArray;-><init>()V

    .line 331
    .local v1, "fingerIndexSparseArray":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Ljava/lang/String;>;>;"
    invoke-virtual {v1, p4, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 333
    new-instance v0, Ljava/util/HashMap;

    .end local v0    # "appNameMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Landroid/util/SparseArray<Ljava/util/List<Ljava/lang/String;>;>;>;"
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 334
    .restart local v0    # "appNameMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Landroid/util/SparseArray<Ljava/util/List<Ljava/lang/String;>;>;>;"
    invoke-interface {v0, p3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 336
    invoke-virtual {p1, p2, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 373
    :goto_1
    const-string v3, "FingerprintDBAdapter"

    const-string v4, "makePermissionMap END"

    invoke-static {v3, v4}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 338
    .end local v1    # "fingerIndexSparseArray":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Ljava/lang/String;>;>;"
    .end local v2    # "permissionList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_1
    invoke-interface {v0, p3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/util/SparseArray;

    .line 339
    .restart local v1    # "fingerIndexSparseArray":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Ljava/lang/String;>;>;"
    if-nez v1, :cond_2

    .line 341
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 342
    .restart local v2    # "permissionList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v2, p5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 344
    new-instance v1, Landroid/util/SparseArray;

    .end local v1    # "fingerIndexSparseArray":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Ljava/lang/String;>;>;"
    invoke-direct {v1}, Landroid/util/SparseArray;-><init>()V

    .line 345
    .restart local v1    # "fingerIndexSparseArray":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Ljava/lang/String;>;>;"
    invoke-virtual {v1, p4, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 347
    invoke-interface {v0, p3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 349
    invoke-virtual {p1, p2, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_1

    .line 351
    .end local v2    # "permissionList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_2
    invoke-virtual {v1, p4}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    .line 352
    .restart local v2    # "permissionList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-nez v2, :cond_3

    .line 353
    new-instance v2, Ljava/util/ArrayList;

    .end local v2    # "permissionList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 354
    .restart local v2    # "permissionList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v2, p5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 356
    invoke-virtual {v1, p4, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 357
    invoke-interface {v0, p3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 358
    invoke-virtual {p1, p2, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_1

    .line 360
    :cond_3
    invoke-interface {v2, p5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_4

    .line 361
    const-string v4, "FingerprintDBAdapter"

    const-string v5, "makePermissionMap END: duplicated"

    invoke-static {v4, v5}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move-object p1, v3

    .line 362
    goto :goto_0

    .line 365
    :cond_4
    invoke-interface {v2, p5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 366
    invoke-virtual {v1, p4, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 367
    invoke-interface {v0, p3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 368
    invoke-virtual {p1, p2, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_1
.end method

.method private open()Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/database/SQLException;
        }
    .end annotation

    .prologue
    .line 176
    new-instance v0, Lcom/samsung/android/fingerprint/service/FingerprintDBHelper;

    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/samsung/android/fingerprint/service/FingerprintDBHelper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->mFingerprintDBHelper:Lcom/samsung/android/fingerprint/service/FingerprintDBHelper;

    .line 177
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->mFingerprintDBHelper:Lcom/samsung/android/fingerprint/service/FingerprintDBHelper;

    invoke-virtual {v0}, Lcom/samsung/android/fingerprint/service/FingerprintDBHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->mFingerprintDB:Landroid/database/sqlite/SQLiteDatabase;

    .line 179
    return-object p0
.end method

.method private permissionMapToString(Landroid/util/SparseArray;)Ljava/lang/String;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Landroid/util/SparseArray",
            "<",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;>;>;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 224
    .local p1, "mPermissionMap":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/Map<Ljava/lang/String;Landroid/util/SparseArray<Ljava/util/List<Ljava/lang/String;>;>;>;>;"
    new-instance v12, Ljava/lang/StringBuffer;

    invoke-direct {v12}, Ljava/lang/StringBuffer;-><init>()V

    .line 226
    .local v12, "resultSB":Ljava/lang/StringBuffer;
    invoke-virtual/range {p1 .. p1}, Landroid/util/SparseArray;->size()I

    move-result v14

    if-gtz v14, :cond_0

    .line 227
    const-string v14, "PermissionMap : empty - DB has no data!"

    invoke-virtual {v12, v14}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 228
    invoke-virtual {v12}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v14

    .line 268
    :goto_0
    return-object v14

    .line 231
    :cond_0
    const/4 v8, 0x0

    .local v8, "mPermissionMapIdx":I
    :goto_1
    invoke-virtual/range {p1 .. p1}, Landroid/util/SparseArray;->size()I

    move-result v14

    if-ge v8, v14, :cond_6

    .line 232
    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v13

    .line 234
    .local v13, "userId":I
    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map;

    .line 235
    .local v4, "appNameMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Landroid/util/SparseArray<Ljava/util/List<Ljava/lang/String;>;>;>;"
    if-nez v4, :cond_2

    .line 231
    :cond_1
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    .line 239
    :cond_2
    invoke-interface {v4}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v14

    invoke-interface {v14}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 241
    .local v3, "appNameIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Landroid/util/SparseArray<Ljava/util/List<Ljava/lang/String;>;>;>;>;"
    :cond_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_1

    .line 242
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 244
    .local v2, "appNameEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Landroid/util/SparseArray<Ljava/util/List<Ljava/lang/String;>;>;>;"
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 245
    .local v1, "appName":Ljava/lang/String;
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/util/SparseArray;

    .line 247
    .local v6, "fingerIndexSparseArray":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Ljava/lang/String;>;>;"
    const/4 v11, 0x0

    .local v11, "permissionListIdx":I
    :goto_2
    invoke-virtual {v6}, Landroid/util/SparseArray;->size()I

    move-result v14

    if-ge v11, v14, :cond_3

    .line 248
    invoke-virtual {v6, v11}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v5

    .line 249
    .local v5, "fingerIndex":I
    invoke-virtual {v6, v5}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/util/List;

    .line 251
    .local v10, "permissionList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-nez v10, :cond_5

    .line 247
    :cond_4
    add-int/lit8 v11, v11, 0x1

    goto :goto_2

    .line 255
    :cond_5
    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, "i$":Ljava/util/Iterator;
    :goto_3
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_4

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    .line 256
    .local v9, "permission":Ljava/lang/String;
    const-string v14, " userId:"

    invoke-virtual {v12, v14}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v14

    invoke-virtual {v14, v13}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v14

    const-string v15, ", appName:"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v14

    invoke-virtual {v14, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v14

    const-string v15, ", fingerIndex:"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v14

    invoke-virtual {v14, v5}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v14

    const-string v15, ", permission:"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v14

    invoke-virtual {v14, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v14

    const/16 v15, 0xa

    invoke-virtual {v14, v15}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_3

    .line 265
    .end local v1    # "appName":Ljava/lang/String;
    .end local v2    # "appNameEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Landroid/util/SparseArray<Ljava/util/List<Ljava/lang/String;>;>;>;"
    .end local v3    # "appNameIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Landroid/util/SparseArray<Ljava/util/List<Ljava/lang/String;>;>;>;>;"
    .end local v4    # "appNameMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Landroid/util/SparseArray<Ljava/util/List<Ljava/lang/String;>;>;>;"
    .end local v5    # "fingerIndex":I
    .end local v6    # "fingerIndexSparseArray":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Ljava/lang/String;>;>;"
    .end local v7    # "i$":Ljava/util/Iterator;
    .end local v9    # "permission":Ljava/lang/String;
    .end local v10    # "permissionList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v11    # "permissionListIdx":I
    .end local v13    # "userId":I
    :cond_6
    invoke-virtual {v12}, Ljava/lang/StringBuffer;->length()I

    move-result v14

    if-eqz v14, :cond_7

    .line 266
    invoke-virtual {v12}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v14

    goto/16 :goto_0

    .line 268
    :cond_7
    const/4 v14, 0x0

    goto/16 :goto_0
.end method

.method private query()Landroid/database/Cursor;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 191
    const/4 v0, 0x0

    .line 194
    .local v0, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v3, p0, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->mFingerprintDB:Landroid/database/sqlite/SQLiteDatabase;

    const-string v4, "SELECT * FROM fingerprint_table;"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    move-object v2, v0

    .line 201
    :goto_0
    return-object v2

    .line 195
    :catch_0
    move-exception v1

    .line 196
    .local v1, "e":Landroid/database/SQLException;
    const-string v3, "FingerprintDBAdapter"

    const-string v4, "query: failed"

    invoke-static {v3, v4, v1}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private removeAllEnrolledFingersLocked(ILjava/lang/String;)V
    .locals 6
    .param p1, "userId"    # I
    .param p2, "appName"    # Ljava/lang/String;

    .prologue
    .line 1036
    const-string v2, "FingerprintDBAdapter"

    const-string v3, "removeAllEnrolledFingersLocked BEGIN"

    invoke-static {v2, v3}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1037
    invoke-direct {p0}, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->open()Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;

    .line 1038
    const-string v0, "DELETE FROM fingerprint_table WHERE USER_ID=?"

    .line 1042
    .local v0, "sqlorder":Ljava/lang/String;
    if-eqz p2, :cond_0

    .line 1043
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " AND APP_NAME=?"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1045
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->mFingerprintDB:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v2, v0}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v1

    .line 1047
    .local v1, "st":Landroid/database/sqlite/SQLiteStatement;
    if-eqz v1, :cond_2

    .line 1048
    const/4 v2, 0x1

    int-to-long v4, p1

    invoke-virtual {v1, v2, v4, v5}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 1049
    if-eqz p2, :cond_1

    .line 1050
    const/4 v2, 0x2

    invoke-virtual {v1, v2, p2}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 1052
    :cond_1
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteStatement;->executeUpdateDelete()I

    .line 1053
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteStatement;->close()V

    .line 1056
    :cond_2
    invoke-direct {p0}, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->close()V

    .line 1057
    const-string v2, "FingerprintDBAdapter"

    const-string v3, "removeAllEnrolledFingersLocked END"

    invoke-static {v2, v3}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1058
    return-void
.end method

.method private removeEnrolledFingerLocked(ILjava/lang/String;I)V
    .locals 6
    .param p1, "userId"    # I
    .param p2, "appName"    # Ljava/lang/String;
    .param p3, "fingerIndex"    # I

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 941
    const-string v1, "FingerprintDBAdapter"

    const-string v2, "removeEnrolledFingerLocked BEGIN"

    invoke-static {v1, v2}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 942
    invoke-direct {p0}, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->open()Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;

    .line 943
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 944
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->mFingerprintDB:Landroid/database/sqlite/SQLiteDatabase;

    const-string v2, "DELETE FROM fingerprint_table WHERE USER_ID=? AND FINGER_INDEX=?"

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v0

    .line 950
    .local v0, "st":Landroid/database/sqlite/SQLiteStatement;
    if-eqz v0, :cond_0

    .line 951
    int-to-long v2, p1

    invoke-virtual {v0, v4, v2, v3}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 952
    int-to-long v2, p3

    invoke-virtual {v0, v5, v2, v3}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 953
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteStatement;->executeUpdateDelete()I

    .line 954
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteStatement;->close()V

    .line 972
    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->close()V

    .line 973
    const-string v1, "FingerprintDBAdapter"

    const-string v2, "removeEnrolledFingerLocked END"

    invoke-static {v1, v2}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 974
    return-void

    .line 957
    .end local v0    # "st":Landroid/database/sqlite/SQLiteStatement;
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->mFingerprintDB:Landroid/database/sqlite/SQLiteDatabase;

    const-string v2, "DELETE FROM fingerprint_table WHERE USER_ID=? AND APP_NAME=? AND FINGER_INDEX=?"

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v0

    .line 964
    .restart local v0    # "st":Landroid/database/sqlite/SQLiteStatement;
    if-eqz v0, :cond_0

    .line 965
    int-to-long v2, p1

    invoke-virtual {v0, v4, v2, v3}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 966
    invoke-virtual {v0, v5, p2}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 967
    const/4 v1, 0x3

    int-to-long v2, p3

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 968
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteStatement;->executeUpdateDelete()I

    .line 969
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteStatement;->close()V

    goto :goto_0
.end method

.method private removeEnrolledPermissionLocked(ILjava/lang/String;Ljava/lang/String;I)V
    .locals 4
    .param p1, "userId"    # I
    .param p2, "appName"    # Ljava/lang/String;
    .param p3, "permissionName"    # Ljava/lang/String;
    .param p4, "fingerIndex"    # I

    .prologue
    .line 868
    const-string v1, "FingerprintDBAdapter"

    const-string v2, "removeEnrolledPermissionLocked BEGIN"

    invoke-static {v1, v2}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 869
    invoke-direct {p0}, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->open()Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;

    .line 872
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->mFingerprintDB:Landroid/database/sqlite/SQLiteDatabase;

    const-string v2, "DELETE FROM fingerprint_table WHERE USER_ID=? AND APP_NAME=? AND FINGER_INDEX=? AND PERMISSION=?"

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v0

    .line 880
    .local v0, "st":Landroid/database/sqlite/SQLiteStatement;
    if-eqz v0, :cond_0

    .line 881
    const/4 v1, 0x1

    int-to-long v2, p1

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 882
    const/4 v1, 0x2

    invoke-virtual {v0, v1, p2}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 883
    const/4 v1, 0x3

    int-to-long v2, p4

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 884
    const/4 v1, 0x4

    invoke-virtual {v0, v1, p3}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 885
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteStatement;->executeUpdateDelete()I

    .line 887
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteStatement;->close()V

    .line 891
    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->close()V

    .line 892
    const-string v1, "FingerprintDBAdapter"

    const-string v2, "removeEnrolledPermissionLocked END"

    invoke-static {v1, v2}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 893
    return-void
.end method

.method private removeEnrolledPermissionsLocked(ILjava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "userId"    # I
    .param p2, "appName"    # Ljava/lang/String;
    .param p3, "permissionName"    # Ljava/lang/String;

    .prologue
    .line 790
    const-string v1, "FingerprintDBAdapter"

    const-string v2, "removeEnrolledPermissionsLocked BEGIN"

    invoke-static {v1, v2}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 791
    invoke-direct {p0}, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->open()Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;

    .line 793
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->mFingerprintDB:Landroid/database/sqlite/SQLiteDatabase;

    const-string v2, "DELETE FROM fingerprint_table WHERE USER_ID=? AND APP_NAME=? AND PERMISSION=?"

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v0

    .line 801
    .local v0, "st":Landroid/database/sqlite/SQLiteStatement;
    if-eqz v0, :cond_0

    .line 802
    const/4 v1, 0x1

    int-to-long v2, p1

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 803
    const/4 v1, 0x2

    invoke-virtual {v0, v1, p2}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 804
    const/4 v1, 0x3

    invoke-virtual {v0, v1, p3}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 805
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteStatement;->executeUpdateDelete()I

    .line 807
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteStatement;->close()V

    .line 812
    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->close()V

    .line 813
    const-string v1, "FingerprintDBAdapter"

    const-string v2, "removeEnrolledPermissionsLocked END"

    invoke-static {v1, v2}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 814
    return-void
.end method


# virtual methods
.method public dump(Ljava/io/PrintWriter;)V
    .locals 2
    .param p1, "pw"    # Ljava/io/PrintWriter;

    .prologue
    .line 205
    const-string v0, "  * Data of memory:"

    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->mPermissionMapArray:Landroid/util/SparseArray;

    invoke-virtual {p0, v0, p1, v1}, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->dumpPermissionMap(Ljava/lang/String;Ljava/io/PrintWriter;Landroid/util/SparseArray;)V

    .line 206
    const-string v0, ""

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 207
    const-string v0, "  * Data of database:"

    invoke-direct {p0}, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->getPermissionMap()Landroid/util/SparseArray;

    move-result-object v1

    invoke-virtual {p0, v0, p1, v1}, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->dumpPermissionMap(Ljava/lang/String;Ljava/io/PrintWriter;Landroid/util/SparseArray;)V

    .line 208
    return-void
.end method

.method public dumpPermissionMap(Ljava/lang/String;Ljava/io/PrintWriter;Landroid/util/SparseArray;)V
    .locals 2
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "pw"    # Ljava/io/PrintWriter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/io/PrintWriter;",
            "Landroid/util/SparseArray",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Landroid/util/SparseArray",
            "<",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;>;>;)V"
        }
    .end annotation

    .prologue
    .line 212
    .local p3, "map":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/Map<Ljava/lang/String;Landroid/util/SparseArray<Ljava/util/List<Ljava/lang/String;>;>;>;>;"
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 214
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->mPermissionMapArray:Landroid/util/SparseArray;

    invoke-direct {p0, v1}, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->permissionMapToString(Landroid/util/SparseArray;)Ljava/lang/String;

    move-result-object v0

    .line 216
    .local v0, "dumpPermissionMapString":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 217
    const-string v1, "    map is broken"

    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 221
    :goto_0
    return-void

    .line 219
    :cond_0
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getEnrolledFingers(I)I
    .locals 8
    .param p1, "requestedUserId"    # I

    .prologue
    .line 435
    const/4 v3, 0x0

    .line 437
    .local v3, "fingers":I
    iget-object v7, p0, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->mPermissionMapArray:Landroid/util/SparseArray;

    invoke-virtual {v7}, Landroid/util/SparseArray;->size()I

    move-result v7

    if-gtz v7, :cond_0

    move v4, v3

    .line 451
    .end local v3    # "fingers":I
    .local v4, "fingers":I
    :goto_0
    return v4

    .line 441
    .end local v4    # "fingers":I
    .restart local v3    # "fingers":I
    :cond_0
    iget-object v7, p0, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->mPermissionMapArray:Landroid/util/SparseArray;

    invoke-virtual {v7, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 442
    .local v0, "appNameMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Landroid/util/SparseArray<Ljava/util/List<Ljava/lang/String;>;>;>;"
    if-eqz v0, :cond_2

    .line 443
    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .local v6, "i$":Ljava/util/Iterator;
    :cond_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/util/SparseArray;

    .line 444
    .local v2, "fingerIndexSparseArray":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Ljava/lang/String;>;>;"
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_1
    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    move-result v7

    if-ge v5, v7, :cond_1

    .line 445
    invoke-virtual {v2, v5}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v1

    .line 446
    .local v1, "fingerIndex":I
    const/4 v7, 0x1

    shl-int/2addr v7, v1

    or-int/2addr v3, v7

    .line 444
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .end local v1    # "fingerIndex":I
    .end local v2    # "fingerIndexSparseArray":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Ljava/lang/String;>;>;"
    .end local v5    # "i":I
    .end local v6    # "i$":Ljava/util/Iterator;
    :cond_2
    move v4, v3

    .line 451
    .end local v3    # "fingers":I
    .restart local v4    # "fingers":I
    goto :goto_0
.end method

.method public getEnrolledPermissions(ILjava/lang/String;)Ljava/util/List;
    .locals 20
    .param p1, "userId"    # I
    .param p2, "appName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 457
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->mPermissionMapArray:Landroid/util/SparseArray;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/util/SparseArray;->size()I

    move-result v17

    if-gtz v17, :cond_1

    .line 458
    const/4 v3, 0x0

    .line 536
    :cond_0
    :goto_0
    return-object v3

    .line 461
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->mPermissionMapArray:Landroid/util/SparseArray;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map;

    .line 462
    .local v2, "appNameMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Landroid/util/SparseArray<Ljava/util/List<Ljava/lang/String;>;>;>;"
    if-nez v2, :cond_2

    .line 463
    const/4 v3, 0x0

    goto :goto_0

    .line 466
    :cond_2
    const/4 v9, 0x0

    .line 468
    .local v9, "intermediates":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Ljava/lang/String;>;>;"
    if-nez p2, :cond_8

    .line 469
    new-instance v9, Landroid/util/SparseArray;

    .end local v9    # "intermediates":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Ljava/lang/String;>;>;"
    invoke-direct {v9}, Landroid/util/SparseArray;-><init>()V

    .line 471
    .restart local v9    # "intermediates":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Ljava/lang/String;>;>;"
    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v17

    invoke-interface/range {v17 .. v17}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_3
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-eqz v17, :cond_9

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Landroid/util/SparseArray;

    .line 472
    .local v15, "permissions":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Ljava/lang/String;>;>;"
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_1
    invoke-virtual {v15}, Landroid/util/SparseArray;->size()I

    move-result v17

    move/from16 v0, v17

    if-ge v5, v0, :cond_3

    .line 473
    invoke-virtual {v15, v5}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v4

    .line 474
    .local v4, "fingerIndex":I
    invoke-virtual {v15, v4}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/util/List;

    .line 475
    .local v13, "permissionList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-nez v13, :cond_5

    .line 472
    :cond_4
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 479
    :cond_5
    invoke-virtual {v9, v4}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/util/List;

    .line 480
    .local v10, "merged":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-nez v10, :cond_6

    .line 481
    new-instance v10, Ljava/util/ArrayList;

    .end local v10    # "merged":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 482
    .restart local v10    # "merged":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {v9, v4, v10}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 485
    :cond_6
    invoke-interface {v13}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, "i$":Ljava/util/Iterator;
    :cond_7
    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-eqz v17, :cond_4

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/String;

    .line 486
    .local v12, "permission":Ljava/lang/String;
    invoke-interface {v10, v12}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v17

    if-nez v17, :cond_7

    .line 487
    invoke-interface {v10, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 493
    .end local v4    # "fingerIndex":I
    .end local v5    # "i":I
    .end local v7    # "i$":Ljava/util/Iterator;
    .end local v10    # "merged":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v12    # "permission":Ljava/lang/String;
    .end local v13    # "permissionList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v15    # "permissions":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Ljava/lang/String;>;>;"
    :cond_8
    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    .end local v9    # "intermediates":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Ljava/lang/String;>;>;"
    check-cast v9, Landroid/util/SparseArray;

    .line 495
    .restart local v9    # "intermediates":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Ljava/lang/String;>;>;"
    if-nez v9, :cond_9

    .line 496
    const/4 v3, 0x0

    goto :goto_0

    .line 500
    :cond_9
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 502
    .local v3, "enrolledPermissions":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v5, 0x0

    .restart local v5    # "i":I
    :goto_3
    invoke-virtual {v9}, Landroid/util/SparseArray;->size()I

    move-result v17

    move/from16 v0, v17

    if-ge v5, v0, :cond_d

    .line 503
    invoke-virtual {v9, v5}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v4

    .line 504
    .restart local v4    # "fingerIndex":I
    invoke-virtual {v9, v4}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/util/List;

    .line 505
    .restart local v13    # "permissionList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-nez v13, :cond_a

    .line 502
    :goto_4
    add-int/lit8 v5, v5, 0x1

    goto :goto_3

    .line 508
    :cond_a
    new-instance v17, Ljava/lang/StringBuffer;

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v18

    invoke-direct/range {v17 .. v18}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    const/16 v18, 0x3a

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v16

    .line 510
    .local v16, "sbPermissions":Ljava/lang/StringBuffer;
    invoke-interface {v13}, Ljava/util/List;->size()I

    move-result v14

    .line 511
    .local v14, "permissionListSize":I
    const/4 v8, 0x0

    .local v8, "idx":I
    :goto_5
    if-ge v8, v14, :cond_c

    .line 512
    invoke-interface {v13, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/lang/String;

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 513
    add-int/lit8 v17, v14, -0x1

    move/from16 v0, v17

    if-ge v8, v0, :cond_b

    .line 514
    const/16 v17, 0x2c

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 511
    :cond_b
    add-int/lit8 v8, v8, 0x1

    goto :goto_5

    .line 527
    :cond_c
    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 531
    .end local v4    # "fingerIndex":I
    .end local v8    # "idx":I
    .end local v13    # "permissionList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v14    # "permissionListSize":I
    .end local v16    # "sbPermissions":Ljava/lang/StringBuffer;
    :cond_d
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .local v6, "i$":Ljava/util/Iterator;
    :goto_6
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-eqz v17, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    .line 532
    .local v11, "perm":Ljava/lang/String;
    const-string v17, "FingerprintDBAdapter"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "getEnrolledPermissions: "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_6
.end method

.method public getEnrolledPermissions(ILjava/lang/String;I)Ljava/util/List;
    .locals 10
    .param p1, "userId"    # I
    .param p2, "appName"    # Ljava/lang/String;
    .param p3, "fingerIndex"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "I)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    .line 540
    iget-object v9, p0, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->mPermissionMapArray:Landroid/util/SparseArray;

    invoke-virtual {v9}, Landroid/util/SparseArray;->size()I

    move-result v9

    if-gtz v9, :cond_1

    .line 590
    :cond_0
    :goto_0
    return-object v8

    .line 544
    :cond_1
    iget-object v9, p0, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->mPermissionMapArray:Landroid/util/SparseArray;

    invoke-virtual {v9, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map;

    .line 546
    .local v2, "appNameMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Landroid/util/SparseArray<Ljava/util/List<Ljava/lang/String;>;>;>;"
    if-eqz v2, :cond_0

    .line 550
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 552
    .local v7, "result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-nez p2, :cond_4

    .line 553
    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 555
    .local v1, "appNameIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Landroid/util/SparseArray<Ljava/util/List<Ljava/lang/String;>;>;>;>;"
    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_6

    .line 556
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 558
    .local v0, "appNameEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Landroid/util/SparseArray<Ljava/util/List<Ljava/lang/String;>;>;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/util/SparseArray;

    .line 560
    .local v3, "fingerIndexSparseArray":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Ljava/lang/String;>;>;"
    invoke-virtual {v3, p3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/List;

    .line 562
    .local v6, "permissionList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz v6, :cond_2

    .line 563
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :cond_3
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 564
    .local v5, "permName":Ljava/lang/String;
    invoke-interface {v7, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_3

    .line 565
    invoke-interface {v7, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 571
    .end local v0    # "appNameEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Landroid/util/SparseArray<Ljava/util/List<Ljava/lang/String;>;>;>;"
    .end local v1    # "appNameIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Landroid/util/SparseArray<Ljava/util/List<Ljava/lang/String;>;>;>;>;"
    .end local v3    # "fingerIndexSparseArray":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Ljava/lang/String;>;>;"
    .end local v4    # "i$":Ljava/util/Iterator;
    .end local v5    # "permName":Ljava/lang/String;
    .end local v6    # "permissionList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_4
    invoke-interface {v2, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/util/SparseArray;

    .line 573
    .restart local v3    # "fingerIndexSparseArray":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Ljava/lang/String;>;>;"
    if-eqz v3, :cond_0

    .line 577
    invoke-virtual {v3, p3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/List;

    .line 579
    .restart local v6    # "permissionList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz v6, :cond_0

    .line 583
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .restart local v4    # "i$":Ljava/util/Iterator;
    :cond_5
    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_6

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 584
    .restart local v5    # "permName":Ljava/lang/String;
    invoke-interface {v7, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_5

    .line 585
    invoke-interface {v7, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 590
    .end local v3    # "fingerIndexSparseArray":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Ljava/lang/String;>;>;"
    .end local v4    # "i$":Ljava/util/Iterator;
    .end local v5    # "permName":Ljava/lang/String;
    .end local v6    # "permissionList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_6
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v9

    if-nez v9, :cond_7

    move-object v7, v8

    .end local v7    # "result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_7
    move-object v8, v7

    goto :goto_0
.end method

.method public insert(ILjava/lang/String;ILjava/lang/String;)I
    .locals 9
    .param p1, "userId"    # I
    .param p2, "appName"    # Ljava/lang/String;
    .param p3, "fingerIndex"    # I
    .param p4, "permission"    # Ljava/lang/String;

    .prologue
    const/4 v8, 0x0

    .line 378
    const-string v0, "FingerprintDBAdapter"

    const-string v1, "insert BEGIN"

    invoke-static {v0, v1}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 380
    if-eqz p4, :cond_0

    invoke-virtual {p4}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 381
    :cond_0
    const-string v0, "FingerprintDBAdapter"

    const-string v1, "insert END: INSERT_FAIL_PERMISSION_IS_NULL"

    invoke-static {v0, v1}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 382
    const/4 v0, -0x1

    .line 405
    :goto_0
    return v0

    .line 385
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->mPermissionMapArray:Landroid/util/SparseArray;

    move-object v0, p0

    move v2, p1

    move-object v3, p2

    move v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->makePermissionMap(Landroid/util/SparseArray;ILjava/lang/String;ILjava/lang/String;)Landroid/util/SparseArray;

    move-result-object v7

    .line 387
    .local v7, "permissionMap":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/Map<Ljava/lang/String;Landroid/util/SparseArray<Ljava/util/List<Ljava/lang/String;>;>;>;>;"
    if-nez v7, :cond_2

    .line 388
    const-string v0, "FingerprintDBAdapter"

    const-string v1, "insert END: INSERT_FAIL_DUPLECATED_DATA"

    invoke-static {v0, v1}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v8

    .line 389
    goto :goto_0

    .line 391
    :cond_2
    iput-object v7, p0, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->mPermissionMapArray:Landroid/util/SparseArray;

    .line 395
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->mPermissionMapArray:Landroid/util/SparseArray;

    invoke-direct {p0, v0}, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->dumpPermissionMap(Landroid/util/SparseArray;)V

    .line 399
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v6

    .line 400
    .local v6, "msg":Landroid/os/Message;
    iput v8, v6, Landroid/os/Message;->what:I

    .line 401
    new-instance v0, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter$FingerptintData;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter$FingerptintData;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    iput-object v0, v6, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 402
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->mFingerprintDBThread:Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter$FingerprintDBThread;

    invoke-virtual {v0, v6}, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter$FingerprintDBThread;->sendMessage(Landroid/os/Message;)V

    .line 404
    const-string v0, "FingerprintDBAdapter"

    const-string v1, "insert END"

    invoke-static {v0, v1}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 405
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isEnrolledFinger(II)Z
    .locals 5
    .param p1, "userId"    # I
    .param p2, "fingerIndex"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 669
    iget-object v4, p0, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->mPermissionMapArray:Landroid/util/SparseArray;

    invoke-virtual {v4}, Landroid/util/SparseArray;->size()I

    move-result v4

    if-gtz v4, :cond_1

    .line 684
    :cond_0
    :goto_0
    return v3

    .line 673
    :cond_1
    iget-object v4, p0, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->mPermissionMapArray:Landroid/util/SparseArray;

    invoke-virtual {v4, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 674
    .local v0, "appNameMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Landroid/util/SparseArray<Ljava/util/List<Ljava/lang/String;>;>;>;"
    if-eqz v0, :cond_0

    .line 678
    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/util/SparseArray;

    .line 679
    .local v1, "fingerIndexSparseArray":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Ljava/lang/String;>;>;"
    invoke-virtual {v1, p2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 680
    const/4 v3, 0x1

    goto :goto_0
.end method

.method public isEnrolledFinger(ILjava/lang/String;I)Z
    .locals 4
    .param p1, "userId"    # I
    .param p2, "appName"    # Ljava/lang/String;
    .param p3, "fingerIndex"    # I

    .prologue
    const/4 v2, 0x0

    .line 688
    iget-object v3, p0, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->mPermissionMapArray:Landroid/util/SparseArray;

    invoke-virtual {v3}, Landroid/util/SparseArray;->size()I

    move-result v3

    if-gtz v3, :cond_1

    .line 706
    :cond_0
    :goto_0
    return v2

    .line 692
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->mPermissionMapArray:Landroid/util/SparseArray;

    invoke-virtual {v3, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 693
    .local v0, "appNameMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Landroid/util/SparseArray<Ljava/util/List<Ljava/lang/String;>;>;>;"
    if-eqz v0, :cond_0

    .line 697
    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/util/SparseArray;

    .line 698
    .local v1, "fingerIndexSparseArray":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Ljava/lang/String;>;>;"
    if-eqz v1, :cond_0

    .line 702
    invoke-virtual {v1, p3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 703
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public isEnrolledPermission(ILjava/lang/String;Ljava/lang/String;)Z
    .locals 9
    .param p1, "userId"    # I
    .param p2, "appName"    # Ljava/lang/String;
    .param p3, "permissionName"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    const/4 v7, 0x1

    .line 605
    iget-object v8, p0, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->mPermissionMapArray:Landroid/util/SparseArray;

    invoke-virtual {v8}, Landroid/util/SparseArray;->size()I

    move-result v8

    if-gtz v8, :cond_1

    .line 664
    :cond_0
    :goto_0
    return v6

    .line 609
    :cond_1
    iget-object v8, p0, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->mPermissionMapArray:Landroid/util/SparseArray;

    invoke-virtual {v8, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 611
    .local v0, "appNameMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Landroid/util/SparseArray<Ljava/util/List<Ljava/lang/String;>;>;>;"
    if-eqz v0, :cond_0

    .line 615
    if-nez p2, :cond_6

    .line 616
    if-nez p3, :cond_2

    move v6, v7

    .line 618
    goto :goto_0

    .line 621
    :cond_2
    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/util/SparseArray;

    .line 622
    .local v1, "fingerIndexSparseArray":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Ljava/lang/String;>;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v8

    if-ge v2, v8, :cond_3

    .line 623
    invoke-virtual {v1, v2}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v4

    .line 624
    .local v4, "key":I
    invoke-virtual {v1, v4}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/List;

    .line 625
    .local v5, "permissionList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-nez v5, :cond_5

    .line 622
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 628
    :cond_5
    invoke-interface {v5, p3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-ne v8, v7, :cond_4

    move v6, v7

    .line 629
    goto :goto_0

    .line 635
    .end local v1    # "fingerIndexSparseArray":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Ljava/lang/String;>;>;"
    .end local v2    # "i":I
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v4    # "key":I
    .end local v5    # "permissionList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_6
    if-nez p3, :cond_7

    .line 637
    invoke-interface {v0, p2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    move v6, v7

    .line 638
    goto :goto_0

    .line 642
    :cond_7
    invoke-interface {v0, p2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 646
    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/util/SparseArray;

    .line 647
    .restart local v1    # "fingerIndexSparseArray":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Ljava/lang/String;>;>;"
    if-eqz v1, :cond_0

    .line 651
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_2
    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v8

    if-ge v2, v8, :cond_0

    .line 652
    invoke-virtual {v1, v2}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v4

    .line 653
    .restart local v4    # "key":I
    invoke-virtual {v1, v4}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/List;

    .line 654
    .restart local v5    # "permissionList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-nez v5, :cond_9

    .line 651
    :cond_8
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 657
    :cond_9
    invoke-interface {v5, p3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-ne v8, v7, :cond_8

    move v6, v7

    .line 658
    goto :goto_0
.end method

.method public isEnrolledUserId(I)Z
    .locals 3
    .param p1, "userId"    # I

    .prologue
    const/4 v1, 0x0

    .line 711
    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->mPermissionMapArray:Landroid/util/SparseArray;

    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    move-result v2

    if-gtz v2, :cond_1

    .line 720
    :cond_0
    :goto_0
    return v1

    .line 715
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->mPermissionMapArray:Landroid/util/SparseArray;

    invoke-virtual {v2, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 716
    .local v0, "appNameMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Landroid/util/SparseArray<Ljava/util/List<Ljava/lang/String;>;>;>;"
    if-eqz v0, :cond_0

    .line 720
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isEnrolledUserId(Ljava/lang/String;)Z
    .locals 4
    .param p1, "stringUserId"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 724
    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->mPermissionMapArray:Landroid/util/SparseArray;

    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    move-result v2

    if-gtz v2, :cond_0

    .line 725
    const-string v2, "FingerprintDBAdapter"

    const-string v3, "isEnrolledUserId: size < 0"

    invoke-static {v2, v3}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 736
    :goto_0
    return v1

    .line 729
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->mPermissionMapArray:Landroid/util/SparseArray;

    invoke-static {p1}, Lcom/samsung/android/fingerprint/service/util/ValueUtils;->getIntUserId(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 731
    .local v0, "appNameMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Landroid/util/SparseArray<Ljava/util/List<Ljava/lang/String;>;>;>;"
    if-eqz v0, :cond_1

    invoke-static {p1}, Lcom/samsung/android/fingerprint/service/util/ValueUtils;->getOwnName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_2

    .line 732
    :cond_1
    const-string v2, "FingerprintDBAdapter"

    const-string v3, "isEnrolledUserId: null"

    invoke-static {v2, v3}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 736
    :cond_2
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public removeAllEnrolledFingers(I)I
    .locals 4
    .param p1, "userId"    # I

    .prologue
    const/4 v3, 0x0

    .line 977
    const-string v1, "FingerprintDBAdapter"

    const-string v2, "removeAllEnrolledFingers i BEGIN"

    invoke-static {v1, v2}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 979
    invoke-virtual {p0, p1}, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->isEnrolledUserId(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 980
    const-string v1, "FingerprintDBAdapter"

    const-string v2, "removeAllEnrolledFingers i END: REMOVE_FAIL_USERID_DATA_EMPTY"

    invoke-static {v1, v2}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 981
    const/4 v1, 0x3

    .line 997
    :goto_0
    return v1

    .line 984
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->mPermissionMapArray:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->remove(I)V

    .line 987
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->mPermissionMapArray:Landroid/util/SparseArray;

    invoke-direct {p0, v1}, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->dumpPermissionMap(Landroid/util/SparseArray;)V

    .line 991
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 992
    .local v0, "msg":Landroid/os/Message;
    const/4 v1, 0x4

    iput v1, v0, Landroid/os/Message;->what:I

    .line 993
    new-instance v1, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter$FingerptintData;

    const/4 v2, -0x1

    invoke-direct {v1, p1, v3, v2, v3}, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter$FingerptintData;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 994
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->mFingerprintDBThread:Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter$FingerprintDBThread;

    invoke-virtual {v1, v0}, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter$FingerprintDBThread;->sendMessage(Landroid/os/Message;)V

    .line 996
    const-string v1, "FingerprintDBAdapter"

    const-string v2, "removeAllEnrolledFingers i END"

    invoke-static {v1, v2}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 997
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public removeAllEnrolledFingers(Ljava/lang/String;)I
    .locals 7
    .param p1, "stringUserId"    # Ljava/lang/String;

    .prologue
    .line 1001
    const-string v4, "FingerprintDBAdapter"

    const-string v5, "removeAllEnrolledFingers s BEGIN"

    invoke-static {v4, v5}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1003
    invoke-static {p1}, Lcom/samsung/android/fingerprint/service/util/ValueUtils;->getIntUserId(Ljava/lang/String;)I

    move-result v3

    .line 1004
    .local v3, "userId":I
    invoke-static {p1}, Lcom/samsung/android/fingerprint/service/util/ValueUtils;->getOwnName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1006
    .local v0, "appName":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1007
    const-string v4, "FingerprintDBAdapter"

    const-string v5, "removeAllEnrolledFingers s END"

    invoke-static {v4, v5}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1008
    invoke-virtual {p0, v3}, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->removeAllEnrolledFingers(I)I

    move-result v4

    .line 1032
    :goto_0
    return v4

    .line 1011
    :cond_0
    invoke-virtual {p0, p1}, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->isEnrolledUserId(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 1012
    const-string v4, "FingerprintDBAdapter"

    const-string v5, "removeAllEnrolledFingers s END: REMOVE_FAIL_USERID_DATA_EMPTY"

    invoke-static {v4, v5}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1013
    const/4 v4, 0x3

    goto :goto_0

    .line 1016
    :cond_1
    iget-object v4, p0, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->mPermissionMapArray:Landroid/util/SparseArray;

    invoke-virtual {v4, v3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map;

    .line 1017
    .local v1, "appNameMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Landroid/util/SparseArray<Ljava/util/List<Ljava/lang/String;>;>;>;"
    if-eqz v1, :cond_2

    .line 1018
    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1022
    :cond_2
    iget-object v4, p0, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->mPermissionMapArray:Landroid/util/SparseArray;

    invoke-direct {p0, v4}, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->dumpPermissionMap(Landroid/util/SparseArray;)V

    .line 1026
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v2

    .line 1027
    .local v2, "msg":Landroid/os/Message;
    const/4 v4, 0x4

    iput v4, v2, Landroid/os/Message;->what:I

    .line 1028
    new-instance v4, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter$FingerptintData;

    const/4 v5, -0x1

    const/4 v6, 0x0

    invoke-direct {v4, v3, v0, v5, v6}, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter$FingerptintData;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    iput-object v4, v2, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 1029
    iget-object v4, p0, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->mFingerprintDBThread:Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter$FingerprintDBThread;

    invoke-virtual {v4, v2}, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter$FingerprintDBThread;->sendMessage(Landroid/os/Message;)V

    .line 1031
    const-string v4, "FingerprintDBAdapter"

    const-string v5, "removeAllEnrolledFingers s END"

    invoke-static {v4, v5}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1032
    const/4 v4, 0x1

    goto :goto_0
.end method

.method public removeEnrolledFinger(Ljava/lang/String;I)I
    .locals 9
    .param p1, "stringUserId"    # Ljava/lang/String;
    .param p2, "fingerIndex"    # I

    .prologue
    const/4 v6, 0x2

    .line 896
    const-string v7, "FingerprintDBAdapter"

    const-string v8, "removeEnrolledFinger BEGIN"

    invoke-static {v7, v8}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 898
    invoke-static {p1}, Lcom/samsung/android/fingerprint/service/util/ValueUtils;->getIntUserId(Ljava/lang/String;)I

    move-result v5

    .line 899
    .local v5, "userId":I
    invoke-static {p1}, Lcom/samsung/android/fingerprint/service/util/ValueUtils;->getOwnName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 901
    .local v0, "appName":Ljava/lang/String;
    iget-object v7, p0, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->mPermissionMapArray:Landroid/util/SparseArray;

    invoke-virtual {v7, v5}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map;

    .line 902
    .local v1, "appNameMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Landroid/util/SparseArray<Ljava/util/List<Ljava/lang/String;>;>;>;"
    if-nez v1, :cond_0

    .line 903
    const-string v6, "FingerprintDBAdapter"

    const-string v7, "removeEnrolledFinger END: appNameMap REMOVE_FAIL_USERID_DATA_EMPTY"

    invoke-static {v6, v7}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 904
    const/4 v6, 0x3

    .line 937
    :goto_0
    return v6

    .line 907
    :cond_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 908
    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/util/SparseArray;

    .line 909
    .local v2, "fingerIndexSparseArray":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Ljava/lang/String;>;>;"
    invoke-virtual {v2, p2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v7

    if-nez v7, :cond_1

    .line 910
    const-string v7, "FingerprintDBAdapter"

    const-string v8, "removeEnrolledFinger END: fingerIndexSparseArray REMOVE_FAIL_NO_SUCH_DATA"

    invoke-static {v7, v8}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 913
    :cond_1
    invoke-virtual {v2, p2}, Landroid/util/SparseArray;->remove(I)V

    goto :goto_1

    .line 917
    .end local v2    # "fingerIndexSparseArray":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Ljava/lang/String;>;>;"
    .end local v3    # "i$":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/util/SparseArray;

    .line 918
    .restart local v2    # "fingerIndexSparseArray":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Ljava/lang/String;>;>;"
    if-eqz v2, :cond_3

    invoke-virtual {v2, p2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v7

    if-nez v7, :cond_4

    .line 919
    :cond_3
    const-string v7, "FingerprintDBAdapter"

    const-string v8, "removeEnrolledFinger END: fingerIndexSparseArray REMOVE_FAIL_NO_SUCH_DATA"

    invoke-static {v7, v8}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 922
    :cond_4
    invoke-virtual {v2, p2}, Landroid/util/SparseArray;->remove(I)V

    .line 927
    .end local v2    # "fingerIndexSparseArray":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Ljava/lang/String;>;>;"
    :cond_5
    iget-object v7, p0, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->mPermissionMapArray:Landroid/util/SparseArray;

    invoke-direct {p0, v7}, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->dumpPermissionMap(Landroid/util/SparseArray;)V

    .line 931
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v4

    .line 932
    .local v4, "msg":Landroid/os/Message;
    iput v6, v4, Landroid/os/Message;->what:I

    .line 933
    new-instance v6, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter$FingerptintData;

    const/4 v7, 0x0

    invoke-direct {v6, v5, v0, p2, v7}, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter$FingerptintData;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    iput-object v6, v4, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 934
    iget-object v6, p0, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->mFingerprintDBThread:Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter$FingerprintDBThread;

    invoke-virtual {v6, v4}, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter$FingerprintDBThread;->sendMessage(Landroid/os/Message;)V

    .line 936
    const-string v6, "FingerprintDBAdapter"

    const-string v7, "removeEnrolledFinger END"

    invoke-static {v6, v7}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 937
    const/4 v6, 0x1

    goto :goto_0
.end method

.method public removeEnrolledPermission(ILjava/lang/String;Ljava/lang/String;I)I
    .locals 8
    .param p1, "userId"    # I
    .param p2, "appName"    # Ljava/lang/String;
    .param p3, "permissionName"    # Ljava/lang/String;
    .param p4, "fingerIndex"    # I

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    .line 817
    const-string v6, "FingerprintDBAdapter"

    const-string v7, "removeEnrolledPermission BEGIN"

    invoke-static {v6, v7}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 819
    if-eqz p2, :cond_0

    if-nez p3, :cond_2

    .line 820
    :cond_0
    const-string v5, "FingerprintDBAdapter"

    const-string v6, "removeEnrolledPermission END: appName, permissionName REMOVE_FAIL_NO_SUCH_DATA"

    invoke-static {v5, v6}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 864
    :cond_1
    :goto_0
    return v4

    .line 824
    :cond_2
    invoke-virtual {p0, p1, p4}, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->isEnrolledFinger(II)Z

    move-result v6

    if-nez v6, :cond_3

    .line 825
    const-string v5, "FingerprintDBAdapter"

    const-string v6, "removeEnrolledPermission END: isEnrolledFinger REMOVE_FAIL_NO_SUCH_DATA"

    invoke-static {v5, v6}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 829
    :cond_3
    invoke-virtual {p0, p1, p2, p3}, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->isEnrolledPermission(ILjava/lang/String;Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_4

    .line 830
    const-string v5, "FingerprintDBAdapter"

    const-string v6, "removeEnrolledPermission END: isEnrolledPermission REMOVE_FAIL_NO_SUCH_DATA"

    invoke-static {v5, v6}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 834
    :cond_4
    iget-object v6, p0, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->mPermissionMapArray:Landroid/util/SparseArray;

    invoke-virtual {v6, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 835
    .local v0, "appNameMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Landroid/util/SparseArray<Ljava/util/List<Ljava/lang/String;>;>;>;"
    if-nez v0, :cond_5

    .line 836
    const-string v4, "FingerprintDBAdapter"

    const-string v6, "removeEnrolledPermission END: appNameMap REMOVE_FAIL_NO_SUCH_DATA"

    invoke-static {v4, v6}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    move v4, v5

    .line 837
    goto :goto_0

    .line 840
    :cond_5
    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/util/SparseArray;

    .line 841
    .local v1, "fingerIndexSparseArray":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Ljava/lang/String;>;>;"
    if-eqz v1, :cond_1

    .line 845
    invoke-virtual {v1, p4}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    .line 846
    .local v3, "permissionList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz v3, :cond_1

    .line 850
    invoke-interface {v3, p3}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 855
    iget-object v4, p0, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->mPermissionMapArray:Landroid/util/SparseArray;

    invoke-direct {p0, v4}, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->dumpPermissionMap(Landroid/util/SparseArray;)V

    .line 859
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v2

    .line 860
    .local v2, "msg":Landroid/os/Message;
    iput v5, v2, Landroid/os/Message;->what:I

    .line 861
    new-instance v4, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter$FingerptintData;

    invoke-direct {v4, p1, p2, p4, p3}, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter$FingerptintData;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    iput-object v4, v2, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 862
    iget-object v4, p0, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->mFingerprintDBThread:Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter$FingerprintDBThread;

    invoke-virtual {v4, v2}, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter$FingerprintDBThread;->sendMessage(Landroid/os/Message;)V

    .line 863
    const-string v4, "FingerprintDBAdapter"

    const-string v5, "removeEnrolledPermission END"

    invoke-static {v4, v5}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 864
    const/4 v4, 0x1

    goto :goto_0
.end method

.method public removeEnrolledPermissions(ILjava/lang/String;Ljava/lang/String;)I
    .locals 12
    .param p1, "userId"    # I
    .param p2, "appName"    # Ljava/lang/String;
    .param p3, "permissionName"    # Ljava/lang/String;

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x2

    .line 740
    const-string v10, "FingerprintDBAdapter"

    const-string v11, "removeEnrolledPermissions BEGIN"

    invoke-static {v10, v11}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 742
    if-eqz p2, :cond_0

    if-eqz p3, :cond_0

    iget-object v10, p0, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->mPermissionMapArray:Landroid/util/SparseArray;

    if-nez v10, :cond_1

    .line 786
    :cond_0
    :goto_0
    return v8

    .line 746
    :cond_1
    invoke-virtual {p0, p1, p2, p3}, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->isEnrolledPermission(ILjava/lang/String;Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_2

    .line 747
    const-string v9, "FingerprintDBAdapter"

    const-string v10, "REMOVE_FAIL_NO_SUCH_DATA"

    invoke-static {v9, v10}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 751
    :cond_2
    iget-object v10, p0, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->mPermissionMapArray:Landroid/util/SparseArray;

    invoke-virtual {v10, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 752
    .local v0, "appNameMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Landroid/util/SparseArray<Ljava/util/List<Ljava/lang/String;>;>;>;"
    if-nez v0, :cond_3

    .line 753
    const/4 v8, 0x3

    goto :goto_0

    .line 756
    :cond_3
    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/util/SparseArray;

    .line 757
    .local v2, "fingerIndexSparseArray":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Ljava/lang/String;>;>;"
    if-eqz v2, :cond_0

    .line 761
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    move-result v8

    if-ge v3, v8, :cond_7

    .line 762
    invoke-virtual {v2, v3}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v1

    .line 763
    .local v1, "fingerIndex":I
    invoke-virtual {v2, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/util/List;

    .line 764
    .local v7, "permissionList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-nez v7, :cond_5

    .line 761
    :cond_4
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 767
    :cond_5
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :cond_6
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 768
    .local v6, "permission":Ljava/lang/String;
    invoke-virtual {v6, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 769
    invoke-interface {v7, p3}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_2

    .line 776
    .end local v1    # "fingerIndex":I
    .end local v4    # "i$":Ljava/util/Iterator;
    .end local v6    # "permission":Ljava/lang/String;
    .end local v7    # "permissionList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_7
    iget-object v8, p0, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->mPermissionMapArray:Landroid/util/SparseArray;

    invoke-direct {p0, v8}, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->dumpPermissionMap(Landroid/util/SparseArray;)V

    .line 780
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v5

    .line 781
    .local v5, "msg":Landroid/os/Message;
    iput v9, v5, Landroid/os/Message;->what:I

    .line 782
    new-instance v8, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter$FingerptintData;

    const/4 v10, -0x1

    invoke-direct {v8, p1, p2, v10, p3}, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter$FingerptintData;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    iput-object v8, v5, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 783
    iget-object v8, p0, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->mFingerprintDBThread:Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter$FingerprintDBThread;

    invoke-virtual {v8, v5}, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter$FingerprintDBThread;->sendMessage(Landroid/os/Message;)V

    .line 785
    const-string v8, "FingerprintDBAdapter"

    const-string v10, "removeEnrolledPermissions END"

    invoke-static {v8, v10}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v8, v9

    .line 786
    goto :goto_0
.end method

.method public start()V
    .locals 3

    .prologue
    .line 159
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->mFingerprintDBThread:Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter$FingerprintDBThread;

    invoke-virtual {v1}, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter$FingerprintDBThread;->start()V

    .line 161
    new-instance v1, Lcom/samsung/android/fingerprint/service/FingerprintDBHelper;

    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/samsung/android/fingerprint/service/FingerprintDBHelper;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->mFingerprintDBHelper:Lcom/samsung/android/fingerprint/service/FingerprintDBHelper;

    .line 162
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->mContext:Landroid/content/Context;

    invoke-direct {p0}, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->getDBName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Context;->getDatabasePath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 163
    .local v0, "dbFileCheck":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    .line 164
    const-string v1, "FingerprintDBAdapter"

    const-string v2, "DataBase is not exist!"

    invoke-static {v1, v2}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->getPermissionMap()Landroid/util/SparseArray;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->mPermissionMapArray:Landroid/util/SparseArray;

    .line 171
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->mPermissionMapArray:Landroid/util/SparseArray;

    invoke-direct {p0, v1}, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->dumpPermissionMap(Landroid/util/SparseArray;)V

    .line 173
    return-void
.end method
