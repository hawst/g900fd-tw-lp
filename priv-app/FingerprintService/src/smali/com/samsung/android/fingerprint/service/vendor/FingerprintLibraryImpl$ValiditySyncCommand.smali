.class public abstract Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$ValiditySyncCommand;
.super Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;
.source "FingerprintLibraryImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401
    name = "ValiditySyncCommand"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;


# direct methods
.method public constructor <init>(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p2, "commandName"    # Ljava/lang/String;
    .param p3, "parseInfo"    # Ljava/lang/String;

    .prologue
    .line 1628
    iput-object p1, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$ValiditySyncCommand;->this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    .line 1629
    invoke-direct {p0, p2, p3}, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1630
    return-void
.end method


# virtual methods
.method public requestCancel()Z
    .locals 2

    .prologue
    .line 1634
    iget-boolean v0, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$ValiditySyncCommand;->mCompleted:Z

    if-nez v0, :cond_0

    .line 1635
    const-string v0, "ValidityFingerprintLibraryImpl"

    const-string v1, "cancel() in ValiditySyncCommand.requestCancel"

    invoke-static {v0, v1}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1636
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$ValiditySyncCommand;->this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    # getter for: Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mFingerprint:Lcom/samsung/android/fingerprint/service/vendor/FingerprintProxy;
    invoke-static {v0}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->access$100(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;)Lcom/samsung/android/fingerprint/service/vendor/FingerprintProxy;

    move-result-object v0

    invoke-interface {v0}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintProxy;->cancel()I

    .line 1638
    :cond_0
    invoke-super {p0}, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;->requestCancel()Z

    move-result v0

    return v0
.end method
