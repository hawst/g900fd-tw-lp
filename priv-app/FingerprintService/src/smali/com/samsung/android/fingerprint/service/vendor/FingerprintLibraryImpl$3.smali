.class Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$3;
.super Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;
.source "FingerprintLibraryImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;


# direct methods
.method constructor <init>(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p2, "x0"    # Ljava/lang/String;
    .param p3, "x1"    # Ljava/lang/String;

    .prologue
    .line 1166
    iput-object p1, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$3;->this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    invoke-direct {p0, p2, p3}, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public doCommand()Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1170
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$3;->this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    # getter for: Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mFingerprint:Lcom/samsung/android/fingerprint/service/vendor/FingerprintProxy;
    invoke-static {v1}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->access$100(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;)Lcom/samsung/android/fingerprint/service/vendor/FingerprintProxy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1171
    const-string v1, "ValidityFingerprintLibraryImpl"

    const-string v2, "cleanUp() in doCommand"

    invoke-static {v1, v2}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1173
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$3;->this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    # getter for: Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mFingerprint:Lcom/samsung/android/fingerprint/service/vendor/FingerprintProxy;
    invoke-static {v1}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->access$100(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;)Lcom/samsung/android/fingerprint/service/vendor/FingerprintProxy;

    move-result-object v1

    invoke-interface {v1}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintProxy;->cleanUp()I

    move-result v0

    .line 1175
    .local v0, "result":I
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$3;->this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    # setter for: Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mFingerprint:Lcom/samsung/android/fingerprint/service/vendor/FingerprintProxy;
    invoke-static {v1, v3}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->access$102(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;Lcom/samsung/android/fingerprint/service/vendor/FingerprintProxy;)Lcom/samsung/android/fingerprint/service/vendor/FingerprintProxy;

    .line 1176
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$3;->this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    # setter for: Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mListener:Lcom/samsung/android/fingerprint/service/FingerprintLibraryListener;
    invoke-static {v1, v3}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->access$1602(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;Lcom/samsung/android/fingerprint/service/FingerprintLibraryListener;)Lcom/samsung/android/fingerprint/service/FingerprintLibraryListener;

    .line 1178
    invoke-static {v0}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintValues$Validity2Generic;->getResult(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 1181
    .end local v0    # "result":I
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto :goto_0
.end method
