.class public final Lcom/samsung/android/fingerprint/service/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/fingerprint/service/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final cancel:I = 0x7f060005

.field public static final dialog_header:I = 0x7f060018

.field public static final done:I = 0x7f060031

.field public static final fingerprint:I = 0x7f060004

.field public static final fp_body_10th_m_order:I = 0x7f060043

.field public static final fp_body_1st_m_order:I = 0x7f06003a

.field public static final fp_body_2nd_m_order:I = 0x7f06003b

.field public static final fp_body_3rd_m_order:I = 0x7f06003c

.field public static final fp_body_4th_m_order:I = 0x7f06003d

.field public static final fp_body_5th_m_order:I = 0x7f06003e

.field public static final fp_body_6th_m_order:I = 0x7f06003f

.field public static final fp_body_7th_m_order:I = 0x7f060040

.field public static final fp_body_8th_m_order:I = 0x7f060041

.field public static final fp_body_9th_m_order:I = 0x7f060042

.field public static final fp_tpop_register_ps_fingerprint_vzw:I = 0x7f060039

.field public static final lockpattern_15_incorrent_attempts:I = 0x7f060017

.field public static final lockpattern_5_incorrent_attempts:I = 0x7f060016

.field public static final lockpattern_too_many_failed_confirmation_attempts_footer:I = 0x7f060013

.field public static final lockpattern_too_many_failed_confirmation_attempts_footer_1_second:I = 0x7f060014

.field public static final lockpattern_too_many_incorrent_attempts:I = 0x7f060015

.field public static final permdesc_fingerprintEnroll:I = 0x7f060001

.field public static final permdesc_fingerprintPayload:I = 0x7f060003

.field public static final permlab_fingerprintEnroll:I = 0x7f060000

.field public static final permlab_fingerprintPayload:I = 0x7f060002

.field public static final recognize_fail:I = 0x7f060022

.field public static final spass_alternative_password_guide:I = 0x7f06000a

.field public static final spass_backup_password:I = 0x7f060019

.field public static final spass_backup_password_guide:I = 0x7f06001a

.field public static final spass_backup_pattern:I = 0x7f06001f

.field public static final spass_backup_pattern_guide:I = 0x7f060020

.field public static final spass_backup_pattern_guide_try_again:I = 0x7f060021

.field public static final spass_backup_pin:I = 0x7f06001c

.field public static final spass_backup_pin_guide:I = 0x7f06001d

.field public static final spass_backup_wrong_password:I = 0x7f06001b

.field public static final spass_backup_wrong_pin:I = 0x7f06001e

.field public static final spass_confirm:I = 0x7f060012

.field public static final spass_image_quality_extraction_failure:I = 0x7f060030

.field public static final spass_image_quality_finger_offset_too_far_left:I = 0x7f060026

.field public static final spass_image_quality_finger_offset_too_far_right:I = 0x7f060027

.field public static final spass_image_quality_finger_too_thin:I = 0x7f060028

.field public static final spass_image_quality_pressure_too_hard:I = 0x7f06002a

.field public static final spass_image_quality_pressure_too_light:I = 0x7f06002b

.field public static final spass_image_quality_same_as_previous:I = 0x7f06002f

.field public static final spass_image_quality_skew_too_large:I = 0x7f06002d

.field public static final spass_image_quality_stiction:I = 0x7f060029

.field public static final spass_image_quality_too_fast:I = 0x7f060025

.field public static final spass_image_quality_too_short:I = 0x7f060023

.field public static final spass_image_quality_too_slow:I = 0x7f060024

.field public static final spass_image_quality_wet_finger:I = 0x7f06002c

.field public static final spass_non_password_guide:I = 0x7f060010

.field public static final spass_password:I = 0x7f06000d

.field public static final spass_password_popup_close:I = 0x7f060011

.field public static final spass_sensor_is_not_found:I = 0x7f06000c

.field public static final spass_sensor_is_not_responding:I = 0x7f06000b

.field public static final spass_something_on_sensor:I = 0x7f06002e

.field public static final spass_status_timeout:I = 0x7f06000e

.field public static final spass_status_too_many_badswipe:I = 0x7f06000f

.field public static final spass_wrong_password:I = 0x7f060009

.field public static final succeed:I = 0x7f060008

.field public static final swipe_type_sensor_warning:I = 0x7f060044

.field public static final swipe_your_finger:I = 0x7f060006

.field public static final touch_image_quality_bad_swipe:I = 0x7f060032

.field public static final touch_image_quality_finger_offset_too_far_left:I = 0x7f060033

.field public static final touch_image_quality_finger_offset_too_far_right:I = 0x7f060034

.field public static final touch_image_quality_finger_too_thin:I = 0x7f060035

.field public static final touch_image_quality_pressure_too_hard:I = 0x7f060036

.field public static final touch_image_quality_pressure_too_light:I = 0x7f060037

.field public static final touch_image_quality_remove_too_fast:I = 0x7f060038

.field public static final touch_type_sensor_warning:I = 0x7f060045

.field public static final touch_your_finger:I = 0x7f060007


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 129
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
