.class Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$8;
.super Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$ValiditySyncCommand;
.source "FingerprintLibraryImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;


# direct methods
.method constructor <init>(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p2, "x0"    # Ljava/lang/String;
    .param p3, "x1"    # Ljava/lang/String;

    .prologue
    .line 1250
    iput-object p1, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$8;->this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$ValiditySyncCommand;-><init>(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public doCommand()Ljava/lang/Object;
    .locals 7

    .prologue
    .line 1253
    const-string v4, "ValidityFingerprintLibraryImpl"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "removeEnrolledFinger: userId="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$8;->stringUserId:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", fingerIndex="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$8;->fingerIndex:I

    invoke-static {v6}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintValues$Generic2Validity;->getFingerIndex(I)I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1256
    const/16 v2, 0x1fe

    .line 1257
    .local v2, "result":I
    iget-object v4, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$8;->this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    # getter for: Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mIsVerifiedInEnrollSession:Z
    invoke-static {v4}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->access$1100(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1258
    iget-object v4, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$8;->this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    # getter for: Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mFingerprint:Lcom/samsung/android/fingerprint/service/vendor/FingerprintProxy;
    invoke-static {v4}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->access$100(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;)Lcom/samsung/android/fingerprint/service/vendor/FingerprintProxy;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$8;->stringUserId:Ljava/lang/String;

    iget v6, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$8;->fingerIndex:I

    invoke-static {v6}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintValues$Generic2Validity;->getFingerIndex(I)I

    move-result v6

    invoke-interface {v4, v5, v6}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintProxy;->removeEnrolledFinger(Ljava/lang/String;I)I

    move-result v2

    .line 1261
    if-nez v2, :cond_0

    .line 1262
    iget-object v4, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$8;->this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    # getter for: Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mDataManager:Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;
    invoke-static {v4}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->access$1800(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;)Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$8;->stringUserId:Ljava/lang/String;

    iget v6, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$8;->fingerIndex:I

    invoke-virtual {v4, v5, v6}, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->removeEnrolledFinger(Ljava/lang/String;I)I

    .line 1265
    :cond_0
    new-instance v3, Lcom/validity/fingerprint/VcsInt;

    invoke-direct {v3}, Lcom/validity/fingerprint/VcsInt;-><init>()V

    .line 1266
    .local v3, "validityFingermask":Lcom/validity/fingerprint/VcsInt;
    iget-object v4, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$8;->this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    # getter for: Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mFingerprint:Lcom/samsung/android/fingerprint/service/vendor/FingerprintProxy;
    invoke-static {v4}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->access$100(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;)Lcom/samsung/android/fingerprint/service/vendor/FingerprintProxy;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$8;->stringUserId:Ljava/lang/String;

    invoke-interface {v4, v5, v3}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintProxy;->getEnrolledFingerList(Ljava/lang/String;Lcom/validity/fingerprint/VcsInt;)I

    move-result v4

    if-eqz v4, :cond_1

    .line 1267
    const-string v4, "ValidityFingerprintLibraryImpl"

    const-string v5, "removeEnrolledFinger fail to check next one."

    invoke-static {v4, v5}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 1268
    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    .line 1288
    .end local v3    # "validityFingermask":Lcom/validity/fingerprint/VcsInt;
    :goto_0
    return-object v4

    .line 1270
    .restart local v3    # "validityFingermask":Lcom/validity/fingerprint/VcsInt;
    :cond_1
    iget v4, v3, Lcom/validity/fingerprint/VcsInt;->num:I

    invoke-static {v4}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintValues$Validity2Generic;->getFingerMask(I)I

    move-result v0

    .line 1271
    .local v0, "fingerMask":I
    iget v4, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$8;->fingerIndex:I

    add-int/lit8 v1, v4, 0xa

    .line 1272
    .local v1, "removeIndex":I
    const/4 v4, 0x1

    shl-int/2addr v4, v1

    and-int/2addr v4, v0

    if-eqz v4, :cond_2

    .line 1273
    const-string v4, "ValidityFingerprintLibraryImpl"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "removeEnrolledFinger: fingerMask="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1274
    iget-object v4, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$8;->this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    # getter for: Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mFingerprint:Lcom/samsung/android/fingerprint/service/vendor/FingerprintProxy;
    invoke-static {v4}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->access$100(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;)Lcom/samsung/android/fingerprint/service/vendor/FingerprintProxy;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$8;->stringUserId:Ljava/lang/String;

    invoke-static {v1}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintValues$Generic2Validity;->getFingerIndex(I)I

    move-result v6

    invoke-interface {v4, v5, v6}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintProxy;->removeEnrolledFinger(Ljava/lang/String;I)I

    move-result v2

    .line 1277
    if-nez v2, :cond_2

    .line 1278
    iget-object v4, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$8;->this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    # getter for: Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mDataManager:Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;
    invoke-static {v4}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->access$1900(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;)Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$8;->stringUserId:Ljava/lang/String;

    invoke-virtual {v4, v5, v1}, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->removeEnrolledFinger(Ljava/lang/String;I)I

    .line 1286
    .end local v0    # "fingerMask":I
    .end local v1    # "removeIndex":I
    .end local v3    # "validityFingermask":Lcom/validity/fingerprint/VcsInt;
    :cond_2
    :goto_1
    invoke-virtual {p0}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$8;->notifyCommandCompleted()V

    .line 1288
    invoke-static {v2}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintValues$Validity2Generic;->getResult(I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    goto :goto_0

    .line 1283
    :cond_3
    const-string v4, "ValidityFingerprintLibraryImpl"

    const-string v5, "Not verified in Enroll Session"

    invoke-static {v4, v5}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method
