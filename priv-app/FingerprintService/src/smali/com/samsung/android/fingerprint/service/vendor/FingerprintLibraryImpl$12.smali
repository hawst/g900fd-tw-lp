.class Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$12;
.super Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$ImmediateCommand;
.source "FingerprintLibraryImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;


# direct methods
.method constructor <init>(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p2, "x0"    # Ljava/lang/String;
    .param p3, "x1"    # Ljava/lang/String;

    .prologue
    .line 1356
    iput-object p1, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$12;->this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$ImmediateCommand;-><init>(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public doCommand()Ljava/lang/Object;
    .locals 12

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 1359
    new-instance v4, Lcom/validity/fingerprint/VcsInt;

    invoke-direct {v4}, Lcom/validity/fingerprint/VcsInt;-><init>()V

    .line 1360
    .local v4, "validityFingermask":Lcom/validity/fingerprint/VcsInt;
    iget v7, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$12;->userId:I

    invoke-static {v7}, Lcom/samsung/android/fingerprint/service/util/ValueUtils;->getStringUserId(I)Ljava/lang/String;

    move-result-object v3

    .line 1361
    .local v3, "sUserId":Ljava/lang/String;
    const-string v7, "ValidityFingerprintLibraryImpl"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "addPermission: userId="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1363
    iget-object v7, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$12;->this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    # getter for: Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mFingerprint:Lcom/samsung/android/fingerprint/service/vendor/FingerprintProxy;
    invoke-static {v7}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->access$100(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;)Lcom/samsung/android/fingerprint/service/vendor/FingerprintProxy;

    move-result-object v7

    invoke-interface {v7, v3, v4}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintProxy;->getEnrolledFingerList(Ljava/lang/String;Lcom/validity/fingerprint/VcsInt;)I

    move-result v0

    .line 1366
    .local v0, "enrolledFingersResult":I
    if-eqz v0, :cond_0

    .line 1367
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    .line 1380
    :goto_0
    return-object v5

    .line 1371
    :cond_0
    iget v7, v4, Lcom/validity/fingerprint/VcsInt;->num:I

    invoke-static {v7}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintValues$Validity2Generic;->getFingerMask(I)I

    move-result v1

    .line 1373
    .local v1, "fingerMask":I
    iget v7, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$12;->fingerIndex:I

    shl-int v7, v5, v7

    and-int/2addr v7, v1

    if-nez v7, :cond_1

    .line 1374
    const-string v5, "ValidityFingerprintLibraryImpl"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "addPermission: No such fingerIndex : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$12;->fingerIndex:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1375
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    goto :goto_0

    .line 1378
    :cond_1
    iget-object v7, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$12;->this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    # getter for: Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mDataManager:Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;
    invoke-static {v7}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->access$2300(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;)Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;

    move-result-object v7

    iget v8, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$12;->userId:I

    iget-object v9, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$12;->appName:Ljava/lang/String;

    iget v10, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$12;->fingerIndex:I

    iget-object v11, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$12;->permissionName:Ljava/lang/String;

    invoke-virtual {v7, v8, v9, v10, v11}, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->insert(ILjava/lang/String;ILjava/lang/String;)I

    move-result v2

    .line 1380
    .local v2, "result":I
    if-ne v2, v5, :cond_2

    :goto_1
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    goto :goto_0

    :cond_2
    move v5, v6

    goto :goto_1
.end method
