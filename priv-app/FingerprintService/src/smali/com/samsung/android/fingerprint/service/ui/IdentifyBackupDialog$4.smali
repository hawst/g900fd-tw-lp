.class Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog$4;
.super Ljava/lang/Object;
.source "IdentifyBackupDialog.java"

# interfaces
.implements Lcom/android/internal/widget/LockPatternView$OnPatternListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;


# direct methods
.method constructor <init>(Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;)V
    .locals 0

    .prologue
    .line 479
    iput-object p1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog$4;->this$0:Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPatternCellAdded(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/android/internal/widget/LockPatternView$Cell;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 493
    .local p1, "pattern":Ljava/util/List;, "Ljava/util/List<Lcom/android/internal/widget/LockPatternView$Cell;>;"
    return-void
.end method

.method public onPatternCleared()V
    .locals 2

    .prologue
    .line 487
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog$4;->this$0:Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;

    iget-object v0, v0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPatternView:Lcom/android/internal/widget/LockPatternView;

    if-eqz v0, :cond_0

    .line 488
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog$4;->this$0:Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;

    iget-object v0, v0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPatternView:Lcom/android/internal/widget/LockPatternView;

    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog$4;->this$0:Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;

    # getter for: Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mClearPatternRunnable:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->access$300(Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/internal/widget/LockPatternView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 490
    :cond_0
    return-void
.end method

.method public onPatternDetected(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/android/internal/widget/LockPatternView$Cell;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 495
    .local p1, "pattern":Ljava/util/List;, "Ljava/util/List<Lcom/android/internal/widget/LockPatternView$Cell;>;"
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog$4;->this$0:Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;

    # invokes: Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->verifyPattern(Ljava/util/List;)V
    invoke-static {v0, p1}, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->access$400(Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;Ljava/util/List;)V

    .line 496
    return-void
.end method

.method public onPatternStart()V
    .locals 2

    .prologue
    .line 482
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog$4;->this$0:Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;

    iget-object v0, v0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPatternView:Lcom/android/internal/widget/LockPatternView;

    if-eqz v0, :cond_0

    .line 483
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog$4;->this$0:Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;

    iget-object v0, v0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPatternView:Lcom/android/internal/widget/LockPatternView;

    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog$4;->this$0:Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;

    # getter for: Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mClearPatternRunnable:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->access$300(Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/internal/widget/LockPatternView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 485
    :cond_0
    return-void
.end method
