.class public Lcom/samsung/android/fingerprint/service/vendor/FingerprintValues$Validity2Generic;
.super Ljava/lang/Object;
.source "FingerprintValues.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/fingerprint/service/vendor/FingerprintValues;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Validity2Generic"
.end annotation


# static fields
.field private static sEventIdNameMap:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static sImageQualityNameMap:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static sNotifyNameMap:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static sSensorStatusNameMap:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 144
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    sput-object v0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintValues$Validity2Generic;->sImageQualityNameMap:Landroid/util/SparseArray;

    .line 145
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    sput-object v0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintValues$Validity2Generic;->sEventIdNameMap:Landroid/util/SparseArray;

    .line 146
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    sput-object v0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintValues$Validity2Generic;->sSensorStatusNameMap:Landroid/util/SparseArray;

    .line 147
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    sput-object v0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintValues$Validity2Generic;->sNotifyNameMap:Landroid/util/SparseArray;

    .line 150
    sget-object v0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintValues$Validity2Generic;->sImageQualityNameMap:Landroid/util/SparseArray;

    const-class v1, Lcom/validity/fingerprint/Fingerprint;

    const-string v2, "VCS_IMAGE_QUALITY_"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintValues$Validity2Generic;->buildNameMap(Landroid/util/SparseArray;Ljava/lang/Class;Ljava/lang/String;)V

    .line 151
    sget-object v0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintValues$Validity2Generic;->sEventIdNameMap:Landroid/util/SparseArray;

    const-class v1, Lcom/validity/fingerprint/Fingerprint;

    const-string v2, "VCS_EVT_"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintValues$Validity2Generic;->buildNameMap(Landroid/util/SparseArray;Ljava/lang/Class;Ljava/lang/String;)V

    .line 152
    sget-object v0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintValues$Validity2Generic;->sSensorStatusNameMap:Landroid/util/SparseArray;

    const-class v1, Lcom/validity/fingerprint/Fingerprint;

    const-string v2, "VCS_SENSOR_STATUS_"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintValues$Validity2Generic;->buildNameMap(Landroid/util/SparseArray;Ljava/lang/Class;Ljava/lang/String;)V

    .line 153
    sget-object v0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintValues$Validity2Generic;->sSensorStatusNameMap:Landroid/util/SparseArray;

    const-class v1, Lcom/validity/fingerprint/Fingerprint;

    const-string v2, "VCS_RESULT_SERVICE_NOT_RUNNING"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintValues$Validity2Generic;->buildNameMap(Landroid/util/SparseArray;Ljava/lang/Class;Ljava/lang/String;)V

    .line 154
    sget-object v0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintValues$Validity2Generic;->sNotifyNameMap:Landroid/util/SparseArray;

    const-class v1, Lcom/validity/fingerprint/Fingerprint;

    const-string v2, "VCS_NOTIFY_"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintValues$Validity2Generic;->buildNameMap(Landroid/util/SparseArray;Ljava/lang/Class;Ljava/lang/String;)V

    .line 155
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 143
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static buildNameMap(Landroid/util/SparseArray;Ljava/lang/Class;Ljava/lang/String;)V
    .locals 9
    .param p2, "prefix"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 158
    .local p0, "map":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/String;>;"
    .local p1, "cls":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    invoke-virtual {p1}, Ljava/lang/Class;->getFields()[Ljava/lang/reflect/Field;

    move-result-object v3

    .line 160
    .local v3, "fields":[Ljava/lang/reflect/Field;
    move-object v0, v3

    .local v0, "arr$":[Ljava/lang/reflect/Field;
    array-length v5, v0

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v5, :cond_1

    aget-object v2, v0, v4

    .line 161
    .local v2, "field":Ljava/lang/reflect/Field;
    invoke-virtual {v2}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 163
    const/4 v7, 0x0

    :try_start_0
    invoke-virtual {v2, v7}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v6

    .line 165
    .local v6, "value":I
    invoke-virtual {v2}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v6, v7}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0

    .line 160
    .end local v6    # "value":I
    :cond_0
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 166
    :catch_0
    move-exception v1

    .line 167
    .local v1, "e":Ljava/lang/IllegalAccessException;
    const-string v7, "FingerprintValues"

    const-string v8, "buildNameMap: failed "

    invoke-static {v7, v8, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 171
    .end local v1    # "e":Ljava/lang/IllegalAccessException;
    .end local v2    # "field":Ljava/lang/reflect/Field;
    :cond_1
    return-void
.end method

.method public static eventIdToString(I)Ljava/lang/String;
    .locals 3
    .param p0, "eventId"    # I

    .prologue
    .line 282
    sget-boolean v1, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->DEBUG:Z

    if-eqz v1, :cond_1

    .line 283
    sget-object v1, Lcom/samsung/android/fingerprint/service/vendor/FingerprintValues$Validity2Generic;->sEventIdNameMap:Landroid/util/SparseArray;

    invoke-virtual {v1, p0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 285
    .local v0, "name":Ljava/lang/String;
    if-nez v0, :cond_0

    invoke-static {p0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    .line 287
    .end local v0    # "name":Ljava/lang/String;
    :goto_0
    return-object v1

    .line 285
    .restart local v0    # "name":Ljava/lang/String;
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 287
    .end local v0    # "name":Ljava/lang/String;
    :cond_1
    invoke-static {p0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public static getFingerIndex(I)I
    .locals 6
    .param p0, "validityFingerIndex"    # I

    .prologue
    const/4 v4, 0x0

    .line 202
    # getter for: Lcom/samsung/android/fingerprint/service/vendor/FingerprintValues;->FINGER_INDEX_MAP:[[I
    invoke-static {}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintValues;->access$000()[[I

    move-result-object v0

    .local v0, "arr$":[[I
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v1, v0, v2

    .line 203
    .local v1, "entry":[I
    const/4 v5, 0x1

    aget v5, v1, v5

    if-ne v5, p0, :cond_1

    .line 204
    aget v4, v1, v4

    .line 208
    .end local v1    # "entry":[I
    :cond_0
    return v4

    .line 202
    .restart local v1    # "entry":[I
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public static getFingerIndexFromAppData(Ljava/lang/String;)I
    .locals 4
    .param p0, "appData"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 212
    if-nez p0, :cond_1

    .line 226
    :cond_0
    :goto_0
    return v1

    .line 216
    :cond_1
    const-string v2, "Finger_"

    invoke-virtual {p0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 221
    :try_start_0
    const-string v2, "Finger_"

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {p0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    .line 222
    :catch_0
    move-exception v0

    .line 223
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "FingerprintValues"

    const-string v3, "getFingerIndexFromAppData: failed "

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public static getFingerMask(I)I
    .locals 7
    .param p0, "validityFingerMask"    # I

    .prologue
    const/4 v6, 0x1

    .line 230
    const/4 v4, 0x0

    .line 232
    .local v4, "result":I
    # getter for: Lcom/samsung/android/fingerprint/service/vendor/FingerprintValues;->FINGER_INDEX_MAP:[[I
    invoke-static {}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintValues;->access$000()[[I

    move-result-object v0

    .local v0, "arr$":[[I
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v1, v0, v2

    .line 233
    .local v1, "entry":[I
    aget v5, v1, v6

    shl-int v5, v6, v5

    and-int/2addr v5, p0

    if-eqz v5, :cond_0

    .line 234
    const/4 v5, 0x0

    aget v5, v1, v5

    shl-int v5, v6, v5

    or-int/2addr v4, v5

    .line 232
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 238
    .end local v1    # "entry":[I
    :cond_1
    return v4
.end method

.method public static getImageQuality(I)I
    .locals 6
    .param p0, "validityImageQuality"    # I

    .prologue
    const/4 v4, 0x0

    .line 252
    # getter for: Lcom/samsung/android/fingerprint/service/vendor/FingerprintValues;->FINGER_IMAGE_QUALITY_MAP:[[I
    invoke-static {}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintValues;->access$200()[[I

    move-result-object v0

    .local v0, "arr$":[[I
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v1, v0, v2

    .line 253
    .local v1, "entry":[I
    const/4 v5, 0x1

    aget v5, v1, v5

    and-int/2addr v5, p0

    if-eqz v5, :cond_1

    .line 254
    aget v4, v1, v4

    .line 258
    .end local v1    # "entry":[I
    :cond_0
    return v4

    .line 252
    .restart local v1    # "entry":[I
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public static getImageQualityName(I)Ljava/lang/String;
    .locals 9
    .param p0, "validityImageQuality"    # I

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 178
    const/4 v3, 0x0

    .line 180
    .local v3, "result":Ljava/lang/String;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    const/16 v7, 0x20

    if-ge v2, v7, :cond_3

    .line 181
    shl-int v0, v5, v2

    .line 182
    .local v0, "bit":I
    and-int v7, p0, v0

    if-eqz v7, :cond_1

    move v4, v5

    .line 184
    .local v4, "set":Z
    :goto_1
    if-eqz v4, :cond_0

    .line 185
    sget-object v7, Lcom/samsung/android/fingerprint/service/vendor/FingerprintValues$Validity2Generic;->sImageQualityNameMap:Landroid/util/SparseArray;

    invoke-virtual {v7, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 187
    .local v1, "entryName":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 188
    const/16 v7, 0x12

    invoke-virtual {v1, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 189
    if-nez v3, :cond_2

    .line 190
    move-object v3, v1

    .line 180
    .end local v1    # "entryName":Ljava/lang/String;
    :cond_0
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .end local v4    # "set":Z
    :cond_1
    move v4, v6

    .line 182
    goto :goto_1

    .line 192
    .restart local v1    # "entryName":Ljava/lang/String;
    .restart local v4    # "set":Z
    :cond_2
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "|"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_2

    .line 198
    .end local v0    # "bit":I
    .end local v1    # "entryName":Ljava/lang/String;
    .end local v4    # "set":Z
    :cond_3
    if-nez v3, :cond_4

    sget-object v5, Lcom/samsung/android/fingerprint/service/vendor/FingerprintValues$Validity2Generic;->sImageQualityNameMap:Landroid/util/SparseArray;

    invoke-virtual {v5, v6}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    :goto_3
    return-object v5

    :cond_4
    move-object v5, v3

    goto :goto_3
.end method

.method public static getNotifyName(I)Ljava/lang/String;
    .locals 1
    .param p0, "notifyCondition"    # I

    .prologue
    .line 365
    sget-object v0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintValues$Validity2Generic;->sNotifyNameMap:Landroid/util/SparseArray;

    invoke-virtual {v0, p0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public static getResult(I)I
    .locals 5
    .param p0, "validityResult"    # I

    .prologue
    .line 262
    # getter for: Lcom/samsung/android/fingerprint/service/vendor/FingerprintValues;->FINGER_RESULT_VALUE_MAP:[[I
    invoke-static {}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintValues;->access$300()[[I

    move-result-object v0

    .local v0, "arr$":[[I
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v1, v0, v2

    .line 263
    .local v1, "entry":[I
    const/4 v4, 0x1

    aget v4, v1, v4

    if-ne p0, v4, :cond_0

    .line 264
    const/4 v4, 0x0

    aget v4, v1, v4

    .line 268
    .end local v1    # "entry":[I
    :goto_1
    return v4

    .line 262
    .restart local v1    # "entry":[I
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 268
    .end local v1    # "entry":[I
    :cond_1
    const/4 v4, -0x1

    goto :goto_1
.end method

.method public static getSensorStatusName(I)Ljava/lang/String;
    .locals 1
    .param p0, "validitySensorStatus"    # I

    .prologue
    .line 174
    sget-object v0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintValues$Validity2Generic;->sSensorStatusNameMap:Landroid/util/SparseArray;

    invoke-virtual {v0, p0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public static getStatus(I)I
    .locals 5
    .param p0, "validityStatus"    # I

    .prologue
    .line 242
    # getter for: Lcom/samsung/android/fingerprint/service/vendor/FingerprintValues;->FINGER_STATUS_MAP:[[I
    invoke-static {}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintValues;->access$100()[[I

    move-result-object v0

    .local v0, "arr$":[[I
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v1, v0, v2

    .line 243
    .local v1, "entry":[I
    const/4 v4, 0x1

    aget v4, v1, v4

    if-ne p0, v4, :cond_0

    .line 244
    const/4 v4, 0x0

    aget v4, v1, v4

    .line 248
    .end local v1    # "entry":[I
    :goto_1
    return v4

    .line 242
    .restart local v1    # "entry":[I
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 248
    .end local v1    # "entry":[I
    :cond_1
    const/16 v4, 0x10

    goto :goto_1
.end method

.method public static getSwipeDirection(I)I
    .locals 5
    .param p0, "validityDirection"    # I

    .prologue
    .line 272
    # getter for: Lcom/samsung/android/fingerprint/service/vendor/FingerprintValues;->FINGER_SWIPE_DIRECTION_MAP:[[I
    invoke-static {}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintValues;->access$400()[[I

    move-result-object v0

    .local v0, "arr$":[[I
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v1, v0, v2

    .line 273
    .local v1, "entry":[I
    const/4 v4, 0x1

    aget v4, v1, v4

    if-ne p0, v4, :cond_0

    .line 274
    const/4 v4, 0x0

    aget v4, v1, v4

    .line 278
    .end local v1    # "entry":[I
    :goto_1
    return v4

    .line 272
    .restart local v1    # "entry":[I
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 278
    .end local v1    # "entry":[I
    :cond_1
    const/4 v4, -0x1

    goto :goto_1
.end method

.method public static objectToString(ILjava/lang/Object;)Ljava/lang/String;
    .locals 9
    .param p0, "eventId"    # I
    .param p1, "data"    # Ljava/lang/Object;

    .prologue
    .line 292
    if-nez p1, :cond_0

    .line 293
    const/4 v5, 0x0

    .line 361
    :goto_0
    return-object v5

    .line 296
    :cond_0
    const/4 v5, 0x0

    .line 298
    .local v5, "result":Ljava/lang/String;
    instance-of v7, p1, Lcom/validity/fingerprint/EnrollCaptureStatus;

    if-eqz v7, :cond_2

    move-object v1, p1

    .line 299
    check-cast v1, Lcom/validity/fingerprint/EnrollCaptureStatus;

    .line 301
    .local v1, "ecs":Lcom/validity/fingerprint/EnrollCaptureStatus;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 302
    .local v0, "builder":Ljava/lang/StringBuilder;
    sget-boolean v7, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->DEBUG:Z

    if-eqz v7, :cond_1

    .line 303
    const-string v7, "EnrollCaptureStatus{ badSwipes="

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 304
    iget v7, v1, Lcom/validity/fingerprint/EnrollCaptureStatus;->badSwipes:I

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 305
    const-string v7, ", imageQuality="

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 306
    iget v7, v1, Lcom/validity/fingerprint/EnrollCaptureStatus;->imageQuality:I

    invoke-static {v7}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintValues$Validity2Generic;->getImageQualityName(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 307
    const-string v7, " }"

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 315
    :goto_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 316
    goto :goto_0

    .line 309
    :cond_1
    const-string v7, "E bs="

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 310
    iget v7, v1, Lcom/validity/fingerprint/EnrollCaptureStatus;->badSwipes:I

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 311
    const-string v7, ", iq="

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 312
    iget v7, v1, Lcom/validity/fingerprint/EnrollCaptureStatus;->imageQuality:I

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 316
    .end local v0    # "builder":Ljava/lang/StringBuilder;
    .end local v1    # "ecs":Lcom/validity/fingerprint/EnrollCaptureStatus;
    :cond_2
    instance-of v7, p1, Lcom/validity/fingerprint/IdentifyResult;

    if-eqz v7, :cond_4

    move-object v4, p1

    .line 317
    check-cast v4, Lcom/validity/fingerprint/IdentifyResult;

    .line 319
    .local v4, "ir":Lcom/validity/fingerprint/IdentifyResult;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 320
    .restart local v0    # "builder":Ljava/lang/StringBuilder;
    sget-boolean v7, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->DEBUG:Z

    if-eqz v7, :cond_3

    .line 321
    const-string v7, "IdentifyResult{ userId="

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 322
    iget-object v7, v4, Lcom/validity/fingerprint/IdentifyResult;->userId:Ljava/lang/String;

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 323
    const-string v7, ", fingerIndex="

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 324
    iget v7, v4, Lcom/validity/fingerprint/IdentifyResult;->fingerIndex:I

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 325
    const-string v7, " }"

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 330
    :goto_2
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 331
    goto :goto_0

    .line 327
    :cond_3
    const-string v7, "i="

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 328
    iget v7, v4, Lcom/validity/fingerprint/IdentifyResult;->fingerIndex:I

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 331
    .end local v0    # "builder":Ljava/lang/StringBuilder;
    .end local v4    # "ir":Lcom/validity/fingerprint/IdentifyResult;
    :cond_4
    instance-of v7, p1, Lcom/validity/fingerprint/VcsInt;

    if-eqz v7, :cond_6

    move-object v6, p1

    .line 332
    check-cast v6, Lcom/validity/fingerprint/VcsInt;

    .line 334
    .local v6, "vi":Lcom/validity/fingerprint/VcsInt;
    sget-boolean v7, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->DEBUG:Z

    if-eqz v7, :cond_5

    .line 335
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "VcsInt{ "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, v6, Lcom/validity/fingerprint/VcsInt;->num:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " }"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_0

    .line 337
    :cond_5
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "v="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, v6, Lcom/validity/fingerprint/VcsInt;->num:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_0

    .line 339
    .end local v6    # "vi":Lcom/validity/fingerprint/VcsInt;
    :cond_6
    instance-of v7, p1, Lcom/validity/fingerprint/FingerprintBitmap;

    if-eqz v7, :cond_8

    move-object v2, p1

    .line 340
    check-cast v2, Lcom/validity/fingerprint/FingerprintBitmap;

    .line 342
    .local v2, "fb":Lcom/validity/fingerprint/FingerprintBitmap;
    sget-boolean v7, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->DEBUG:Z

    if-eqz v7, :cond_7

    .line 343
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "{ quality="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, v2, Lcom/validity/fingerprint/FingerprintBitmap;->quality:I

    invoke-static {v8}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintValues$Validity2Generic;->getImageQualityName(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " }"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_0

    .line 345
    :cond_7
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "q="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, v2, Lcom/validity/fingerprint/FingerprintBitmap;->quality:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_0

    .line 348
    .end local v2    # "fb":Lcom/validity/fingerprint/FingerprintBitmap;
    :cond_8
    const/16 v7, 0x2d

    if-ne p0, v7, :cond_a

    move-object v3, p1

    .line 349
    check-cast v3, Ljava/lang/Integer;

    .line 350
    .local v3, "imgQuality":Ljava/lang/Integer;
    sget-boolean v7, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->DEBUG:Z

    if-eqz v7, :cond_9

    .line 351
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "{ quality="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v8

    invoke-static {v8}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintValues$Validity2Generic;->getImageQualityName(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " }"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_0

    .line 353
    :cond_9
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "q="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_0

    .line 357
    .end local v3    # "imgQuality":Ljava/lang/Integer;
    :cond_a
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_0
.end method
