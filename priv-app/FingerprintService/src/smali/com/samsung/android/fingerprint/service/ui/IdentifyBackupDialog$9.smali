.class Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog$9;
.super Ljava/lang/Object;
.source "IdentifyBackupDialog.java"

# interfaces
.implements Landroid/widget/TextView$OnEditorActionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->setEditBoxListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;


# direct methods
.method constructor <init>(Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;)V
    .locals 0

    .prologue
    .line 663
    iput-object p1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog$9;->this$0:Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "v"    # Landroid/widget/TextView;
    .param p2, "actionId"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v0, 0x1

    .line 666
    const/4 v1, 0x6

    if-eq p2, v1, :cond_0

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    const/16 v2, 0x42

    if-ne v1, v2, :cond_3

    .line 667
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog$9;->this$0:Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;

    iget v1, v1, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mBackupType:I

    if-ne v1, v0, :cond_2

    .line 668
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog$9;->this$0:Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;

    # invokes: Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->verifyPassword()V
    invoke-static {v1}, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->access$700(Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;)V

    .line 674
    :cond_1
    :goto_0
    return v0

    .line 669
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog$9;->this$0:Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;

    iget v1, v1, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mBackupType:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    .line 670
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog$9;->this$0:Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;

    # invokes: Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->verifyPin()V
    invoke-static {v1}, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->access$800(Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;)V

    goto :goto_0

    .line 674
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method
