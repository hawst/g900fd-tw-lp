.class public Lcom/samsung/android/fingerprint/service/FingerprintDBHelper;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "FingerprintDBHelper.java"


# static fields
.field public static final APP_NAME_COLUMN:Ljava/lang/String; = "APP_NAME"

.field private static final DATABASE_CREATE_COMMAND:Ljava/lang/String; = "CREATE TABLE IF NOT EXISTS fingerprint_table(_id INTEGER PRIMARY KEY AUTOINCREMENT, USER_ID INT, APP_NAME TEXT, FINGER_INDEX INT, PERMISSION TEXT);"

.field private static final DATABASE_VERSION:I = 0x1

.field public static final DB_NAME:Ljava/lang/String; = "fingerprint.db"

.field public static final FINGERPRINT_TABLE_NAME:Ljava/lang/String; = "fingerprint_table"

.field public static final FINGER_INDEX_COLUMN:Ljava/lang/String; = "FINGER_INDEX"

.field public static final PERMISSION_COLUMN:Ljava/lang/String; = "PERMISSION"

.field private static final TAG:Ljava/lang/String; = "FingerprintDBHelper"

.field public static final USER_ID_COLUMN:Ljava/lang/String; = "USER_ID"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 53
    const-string v0, "fingerprint.db"

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {p0, p1, v0, v1, v2}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 54
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 58
    const-string v0, "FingerprintDBHelper"

    const-string v1, "FingerPrint DB Create"

    invoke-static {v0, v1}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    const-string v0, "CREATE TABLE IF NOT EXISTS fingerprint_table(_id INTEGER PRIMARY KEY AUTOINCREMENT, USER_ID INT, APP_NAME TEXT, FINGER_INDEX INT, PERMISSION TEXT);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 60
    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 2
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "oldVersion"    # I
    .param p3, "newVersion"    # I

    .prologue
    .line 64
    const-string v0, "FingerprintDBHelper"

    const-string v1, "FingerPrint DB Upgrade"

    invoke-static {v0, v1}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    return-void
.end method
