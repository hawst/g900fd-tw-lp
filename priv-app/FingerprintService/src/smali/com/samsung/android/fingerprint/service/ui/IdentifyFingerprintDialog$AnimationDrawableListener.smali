.class public abstract Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog$AnimationDrawableListener;
.super Landroid/graphics/drawable/AnimationDrawable;
.source "IdentifyFingerprintDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401
    name = "AnimationDrawableListener"
.end annotation


# instance fields
.field anim:Landroid/graphics/drawable/AnimationDrawable;

.field mAnimationHandler:Landroid/os/Handler;

.field mRunnable:Ljava/lang/Runnable;

.field final synthetic this$0:Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;


# direct methods
.method public constructor <init>(Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;Landroid/graphics/drawable/AnimationDrawable;)V
    .locals 0
    .param p2, "aniDrawable"    # Landroid/graphics/drawable/AnimationDrawable;

    .prologue
    .line 299
    iput-object p1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog$AnimationDrawableListener;->this$0:Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;

    invoke-direct {p0}, Landroid/graphics/drawable/AnimationDrawable;-><init>()V

    .line 300
    iput-object p2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog$AnimationDrawableListener;->anim:Landroid/graphics/drawable/AnimationDrawable;

    .line 301
    return-void
.end method


# virtual methods
.method public getTotalDuration()I
    .locals 3

    .prologue
    .line 345
    const/4 v0, 0x0

    .line 346
    .local v0, "duration":I
    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog$AnimationDrawableListener;->anim:Landroid/graphics/drawable/AnimationDrawable;

    if-eqz v2, :cond_0

    .line 347
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog$AnimationDrawableListener;->anim:Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/AnimationDrawable;->getNumberOfFrames()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 348
    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog$AnimationDrawableListener;->anim:Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v2, v1}, Landroid/graphics/drawable/AnimationDrawable;->getDuration(I)I

    move-result v2

    add-int/2addr v0, v2

    .line 347
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 351
    .end local v1    # "i":I
    :cond_0
    return v0
.end method

.method public isRunning()Z
    .locals 1

    .prologue
    .line 336
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog$AnimationDrawableListener;->anim:Landroid/graphics/drawable/AnimationDrawable;

    if-eqz v0, :cond_0

    .line 337
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog$AnimationDrawableListener;->anim:Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/AnimationDrawable;->isRunning()Z

    move-result v0

    .line 339
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method abstract onAnimationEnd()V
.end method

.method public start()V
    .locals 4

    .prologue
    .line 305
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog$AnimationDrawableListener;->anim:Landroid/graphics/drawable/AnimationDrawable;

    if-nez v0, :cond_0

    .line 316
    :goto_0
    return-void

    .line 308
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog$AnimationDrawableListener;->anim:Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/AnimationDrawable;->start()V

    .line 309
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog$AnimationDrawableListener;->mAnimationHandler:Landroid/os/Handler;

    .line 310
    new-instance v0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog$AnimationDrawableListener$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog$AnimationDrawableListener$1;-><init>(Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog$AnimationDrawableListener;)V

    iput-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog$AnimationDrawableListener;->mRunnable:Ljava/lang/Runnable;

    .line 315
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog$AnimationDrawableListener;->mAnimationHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog$AnimationDrawableListener;->mRunnable:Ljava/lang/Runnable;

    invoke-virtual {p0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog$AnimationDrawableListener;->getTotalDuration()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method public stop()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 320
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog$AnimationDrawableListener;->anim:Landroid/graphics/drawable/AnimationDrawable;

    if-eqz v0, :cond_1

    .line 321
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog$AnimationDrawableListener;->anim:Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/AnimationDrawable;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 322
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog$AnimationDrawableListener;->anim:Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/AnimationDrawable;->stop()V

    .line 324
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog$AnimationDrawableListener;->anim:Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/AnimationDrawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 325
    iput-object v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog$AnimationDrawableListener;->anim:Landroid/graphics/drawable/AnimationDrawable;

    .line 327
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog$AnimationDrawableListener;->mAnimationHandler:Landroid/os/Handler;

    if-eqz v0, :cond_2

    .line 328
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog$AnimationDrawableListener;->mAnimationHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog$AnimationDrawableListener;->mRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 329
    iput-object v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog$AnimationDrawableListener;->mAnimationHandler:Landroid/os/Handler;

    .line 331
    :cond_2
    iput-object v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog$AnimationDrawableListener;->mRunnable:Ljava/lang/Runnable;

    .line 332
    return-void
.end method
