.class Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog$13;
.super Ljava/lang/Object;
.source "IdentifyBackupDialog.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->setEditBoxListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;


# direct methods
.method constructor <init>(Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;)V
    .locals 0

    .prologue
    .line 729
    iput-object p1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog$13;->this$0:Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 732
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog$13;->this$0:Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;

    iget v0, v0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mBackupType:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 733
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog$13;->this$0:Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;

    # invokes: Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->verifyPassword()V
    invoke-static {v0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->access$700(Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;)V

    .line 737
    :cond_0
    :goto_0
    return-void

    .line 734
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog$13;->this$0:Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;

    iget v0, v0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mBackupType:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 735
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog$13;->this$0:Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;

    # invokes: Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->verifyPin()V
    invoke-static {v0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->access$800(Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;)V

    goto :goto_0
.end method
