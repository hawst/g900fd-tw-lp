.class public Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter$FingerptintData;
.super Ljava/lang/Object;
.source "FingerprintDBAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "FingerptintData"
.end annotation


# instance fields
.field mAppName:Ljava/lang/String;

.field mFingerIndex:I

.field mPermission:Ljava/lang/String;

.field mUserId:I


# direct methods
.method public constructor <init>(ILjava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p1, "userId"    # I
    .param p2, "appName"    # Ljava/lang/String;
    .param p3, "fingerIndex"    # I
    .param p4, "permisstion"    # Ljava/lang/String;

    .prologue
    .line 143
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 144
    iput p1, p0, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter$FingerptintData;->mUserId:I

    .line 145
    iput-object p2, p0, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter$FingerptintData;->mAppName:Ljava/lang/String;

    .line 146
    iput p3, p0, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter$FingerptintData;->mFingerIndex:I

    .line 147
    iput-object p4, p0, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter$FingerptintData;->mPermission:Ljava/lang/String;

    .line 148
    return-void
.end method
