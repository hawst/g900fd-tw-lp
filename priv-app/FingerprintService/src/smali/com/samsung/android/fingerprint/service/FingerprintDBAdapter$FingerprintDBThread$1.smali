.class Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter$FingerprintDBThread$1;
.super Landroid/os/Handler;
.source "FingerprintDBAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter$FingerprintDBThread;->onLooperPrepared()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter$FingerprintDBThread;


# direct methods
.method constructor <init>(Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter$FingerprintDBThread;)V
    .locals 0

    .prologue
    .line 87
    iput-object p1, p0, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter$FingerprintDBThread$1;->this$1:Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter$FingerprintDBThread;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 7
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 89
    const/4 v0, 0x0

    .line 90
    .local v0, "args":Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter$FingerptintData;
    iget v2, p1, Landroid/os/Message;->what:I

    packed-switch v2, :pswitch_data_0

    .line 121
    :goto_0
    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter$FingerprintDBThread$1;->this$1:Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter$FingerprintDBThread;

    iget-object v2, v2, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter$FingerprintDBThread;->this$0:Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;

    # invokes: Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->getPermissionMap()Landroid/util/SparseArray;
    invoke-static {v2}, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->access$500(Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;)Landroid/util/SparseArray;

    move-result-object v1

    .line 122
    .local v1, "dPermissionMap":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/Map<Ljava/lang/String;Landroid/util/SparseArray<Ljava/util/List<Ljava/lang/String;>;>;>;>;"
    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter$FingerprintDBThread$1;->this$1:Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter$FingerprintDBThread;

    iget-object v2, v2, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter$FingerprintDBThread;->this$0:Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;

    # invokes: Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->dumpPermissionMap(Landroid/util/SparseArray;)V
    invoke-static {v2, v1}, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->access$600(Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;Landroid/util/SparseArray;)V

    .line 124
    return-void

    .line 92
    .end local v1    # "dPermissionMap":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/Map<Ljava/lang/String;Landroid/util/SparseArray<Ljava/util/List<Ljava/lang/String;>;>;>;>;"
    :pswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .end local v0    # "args":Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter$FingerptintData;
    check-cast v0, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter$FingerptintData;

    .line 93
    .restart local v0    # "args":Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter$FingerptintData;
    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter$FingerprintDBThread$1;->this$1:Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter$FingerprintDBThread;

    iget-object v2, v2, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter$FingerprintDBThread;->this$0:Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;

    iget v3, v0, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter$FingerptintData;->mUserId:I

    iget-object v4, v0, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter$FingerptintData;->mAppName:Ljava/lang/String;

    iget v5, v0, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter$FingerptintData;->mFingerIndex:I

    iget-object v6, v0, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter$FingerptintData;->mPermission:Ljava/lang/String;

    # invokes: Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->insertLocked(ILjava/lang/String;ILjava/lang/String;)V
    invoke-static {v2, v3, v4, v5, v6}, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->access$000(Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;ILjava/lang/String;ILjava/lang/String;)V

    goto :goto_0

    .line 97
    :pswitch_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .end local v0    # "args":Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter$FingerptintData;
    check-cast v0, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter$FingerptintData;

    .line 98
    .restart local v0    # "args":Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter$FingerptintData;
    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter$FingerprintDBThread$1;->this$1:Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter$FingerprintDBThread;

    iget-object v2, v2, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter$FingerprintDBThread;->this$0:Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;

    iget v3, v0, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter$FingerptintData;->mUserId:I

    iget-object v4, v0, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter$FingerptintData;->mAppName:Ljava/lang/String;

    iget-object v5, v0, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter$FingerptintData;->mPermission:Ljava/lang/String;

    # invokes: Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->removeEnrolledPermissionsLocked(ILjava/lang/String;Ljava/lang/String;)V
    invoke-static {v2, v3, v4, v5}, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->access$100(Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 102
    :pswitch_2
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .end local v0    # "args":Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter$FingerptintData;
    check-cast v0, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter$FingerptintData;

    .line 103
    .restart local v0    # "args":Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter$FingerptintData;
    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter$FingerprintDBThread$1;->this$1:Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter$FingerprintDBThread;

    iget-object v2, v2, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter$FingerprintDBThread;->this$0:Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;

    iget v3, v0, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter$FingerptintData;->mUserId:I

    iget-object v4, v0, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter$FingerptintData;->mAppName:Ljava/lang/String;

    iget v5, v0, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter$FingerptintData;->mFingerIndex:I

    # invokes: Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->removeEnrolledFingerLocked(ILjava/lang/String;I)V
    invoke-static {v2, v3, v4, v5}, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->access$200(Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;ILjava/lang/String;I)V

    goto :goto_0

    .line 107
    :pswitch_3
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .end local v0    # "args":Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter$FingerptintData;
    check-cast v0, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter$FingerptintData;

    .line 108
    .restart local v0    # "args":Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter$FingerptintData;
    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter$FingerprintDBThread$1;->this$1:Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter$FingerprintDBThread;

    iget-object v2, v2, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter$FingerprintDBThread;->this$0:Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;

    iget v3, v0, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter$FingerptintData;->mUserId:I

    iget-object v4, v0, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter$FingerptintData;->mAppName:Ljava/lang/String;

    iget-object v5, v0, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter$FingerptintData;->mPermission:Ljava/lang/String;

    iget v6, v0, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter$FingerptintData;->mFingerIndex:I

    # invokes: Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->removeEnrolledPermissionLocked(ILjava/lang/String;Ljava/lang/String;I)V
    invoke-static {v2, v3, v4, v5, v6}, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->access$300(Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;ILjava/lang/String;Ljava/lang/String;I)V

    goto :goto_0

    .line 112
    :pswitch_4
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .end local v0    # "args":Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter$FingerptintData;
    check-cast v0, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter$FingerptintData;

    .line 113
    .restart local v0    # "args":Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter$FingerptintData;
    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter$FingerprintDBThread$1;->this$1:Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter$FingerprintDBThread;

    iget-object v2, v2, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter$FingerprintDBThread;->this$0:Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;

    iget v3, v0, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter$FingerptintData;->mUserId:I

    iget-object v4, v0, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter$FingerptintData;->mAppName:Ljava/lang/String;

    # invokes: Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->removeAllEnrolledFingersLocked(ILjava/lang/String;)V
    invoke-static {v2, v3, v4}, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->access$400(Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;ILjava/lang/String;)V

    goto :goto_0

    .line 90
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
