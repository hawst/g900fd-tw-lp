.class Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$16;
.super Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$ValiditySyncCommand;
.source "FingerprintLibraryImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;


# direct methods
.method constructor <init>(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p2, "x0"    # Ljava/lang/String;
    .param p3, "x1"    # Ljava/lang/String;

    .prologue
    .line 1468
    iput-object p1, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$16;->this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$ValiditySyncCommand;-><init>(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public doCommand()Ljava/lang/Object;
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 1471
    const-string v2, "ValidityFingerprintLibraryImpl"

    const-string v3, "verifyPassword: BEGIN"

    invoke-static {v2, v3}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1472
    const/4 v0, -0x1

    .line 1474
    .local v0, "result":I
    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$16;->this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    invoke-virtual {v2}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->isSensorReady()Z

    move-result v2

    if-ne v2, v4, :cond_0

    .line 1475
    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$16;->this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    iget-object v3, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$16;->password:Ljava/lang/String;

    # invokes: Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->generateSha1Hash(Ljava/lang/String;)[B
    invoke-static {v2, v3}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->access$2500(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;Ljava/lang/String;)[B

    move-result-object v1

    .line 1476
    .local v1, "verifyPwdHash":[B
    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$16;->this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    # getter for: Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mFingerprint:Lcom/samsung/android/fingerprint/service/vendor/FingerprintProxy;
    invoke-static {v2}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->access$100(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;)Lcom/samsung/android/fingerprint/service/vendor/FingerprintProxy;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$16;->stringUserId:Ljava/lang/String;

    invoke-interface {v2, v3, v1}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintProxy;->verifyPassword(Ljava/lang/String;[B)I

    move-result v0

    .line 1477
    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$16;->this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    # getter for: Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mIsEnrollSession:Z
    invoke-static {v2}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->access$1000(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;)Z

    move-result v2

    if-eqz v2, :cond_0

    if-nez v0, :cond_0

    .line 1478
    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$16;->this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    # setter for: Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mIsVerifiedInEnrollSession:Z
    invoke-static {v2, v4}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->access$1102(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;Z)Z

    .line 1482
    .end local v1    # "verifyPwdHash":[B
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$16;->notifyCommandCompleted()V

    .line 1484
    const-string v2, "ValidityFingerprintLibraryImpl"

    const-string v3, "verifyPassword: END"

    invoke-static {v2, v3}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1485
    const/16 v2, 0x216

    if-eq v0, v2, :cond_1

    const/16 v2, 0x168

    if-ne v0, v2, :cond_2

    .line 1487
    :cond_1
    const/4 v2, 0x7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 1489
    :goto_0
    return-object v2

    :cond_2
    invoke-static {v0}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintValues$Validity2Generic;->getResult(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    goto :goto_0
.end method
