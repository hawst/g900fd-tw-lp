.class public final Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommandFactory;
.super Ljava/lang/Object;
.source "FingerprintLibraryCommandFactory.java"


# static fields
.field static final TAG:Ljava/lang/String; = "FingerprintLibraryCommandFactory"


# instance fields
.field private mCommandMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommandFactory;->mCommandMap:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method public varargs create(Ljava/lang/String;[Ljava/lang/Object;)Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;
    .locals 2
    .param p1, "commandName"    # Ljava/lang/String;
    .param p2, "args"    # [Ljava/lang/Object;

    .prologue
    .line 50
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommandFactory;->mCommandMap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 51
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommandFactory;->mCommandMap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;

    .line 53
    .local v0, "cmd":Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;
    invoke-virtual {v0, p2}, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;->create([Ljava/lang/Object;)Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;

    move-result-object v1

    .line 56
    .end local v0    # "cmd":Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public register(Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;)Z
    .locals 4
    .param p1, "prototype"    # Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;

    .prologue
    const/4 v0, 0x0

    .line 70
    if-nez p1, :cond_0

    .line 71
    const-string v1, "FingerprintLibraryCommandFactory"

    const-string v2, "register: commandName may not be null."

    invoke-static {v1, v2}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    :goto_0
    return v0

    .line 79
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommandFactory;->mCommandMap:Ljava/util/Map;

    iget-object v2, p1, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;->commandName:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 80
    const-string v1, "FingerprintLibraryCommandFactory"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "register: commandName("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p1, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;->commandName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") already added."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 84
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommandFactory;->mCommandMap:Ljava/util/Map;

    iget-object v1, p1, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;->commandName:Ljava/lang/String;

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 86
    const/4 v0, 0x1

    goto :goto_0
.end method
