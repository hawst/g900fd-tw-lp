.class Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager$H;
.super Landroid/os/Handler;
.source "IdentifyDialogManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "H"
.end annotation


# static fields
.field private static final MSG_SWITCH_DIALOG:I = 0x208


# instance fields
.field final synthetic this$0:Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;


# direct methods
.method private constructor <init>(Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;)V
    .locals 0

    .prologue
    .line 75
    iput-object p1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager$H;->this$0:Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;
    .param p2, "x1"    # Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager$1;

    .prologue
    .line 75
    invoke-direct {p0, p1}, Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager$H;-><init>(Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 80
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 88
    :cond_0
    :goto_0
    return-void

    .line 82
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager$H;->this$0:Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v0, v1}, Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;->createIdentifyDialog(I)V

    .line 83
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager$H;->this$0:Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;

    # getter for: Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;->mIdentifyDialog:Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog;
    invoke-static {v0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;->access$000(Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;)Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 84
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager$H;->this$0:Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;

    # getter for: Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;->mIdentifyDialog:Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog;
    invoke-static {v0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;->access$000(Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;)Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog;->show()V

    goto :goto_0

    .line 80
    nop

    :pswitch_data_0
    .packed-switch 0x208
        :pswitch_0
    .end packed-switch
.end method
