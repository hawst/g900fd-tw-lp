.class Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$18;
.super Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$ValiditySyncCommand;
.source "FingerprintLibraryImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;


# direct methods
.method constructor <init>(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p2, "x0"    # Ljava/lang/String;
    .param p3, "x1"    # Ljava/lang/String;

    .prologue
    .line 1518
    iput-object p1, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$18;->this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$ValiditySyncCommand;-><init>(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public doCommand()Ljava/lang/Object;
    .locals 6

    .prologue
    .line 1521
    const-string v4, "ValidityFingerprintLibraryImpl"

    const-string v5, "protect BEGIN"

    invoke-static {v4, v5}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1522
    const/4 v3, 0x0

    .line 1523
    .local v3, "returnObj":Ljava/lang/Object;
    new-instance v2, Lcom/validity/fingerprint/VcsByteArray;

    invoke-direct {v2}, Lcom/validity/fingerprint/VcsByteArray;-><init>()V

    .line 1524
    .local v2, "resultVcsByteArray":Lcom/validity/fingerprint/VcsByteArray;
    new-instance v0, Lcom/validity/fingerprint/VcsAddInfo;

    const/4 v4, 0x1

    invoke-direct {v0, v4}, Lcom/validity/fingerprint/VcsAddInfo;-><init>(I)V

    .line 1525
    .local v0, "addInfo":Lcom/validity/fingerprint/VcsAddInfo;
    iget-object v4, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$18;->this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    # getter for: Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mFingerprint:Lcom/samsung/android/fingerprint/service/vendor/FingerprintProxy;
    invoke-static {v4}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->access$100(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;)Lcom/samsung/android/fingerprint/service/vendor/FingerprintProxy;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$18;->inputByteData:[B

    invoke-interface {v4, v5, v0, v2}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintProxy;->protect([BLcom/validity/fingerprint/VcsAddInfo;Lcom/validity/fingerprint/VcsByteArray;)I

    move-result v1

    .line 1527
    .local v1, "result":I
    invoke-virtual {p0}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$18;->notifyCommandCompleted()V

    .line 1529
    if-eqz v2, :cond_0

    if-nez v1, :cond_0

    .line 1530
    iget-object v3, v2, Lcom/validity/fingerprint/VcsByteArray;->data:[B

    .line 1533
    .end local v3    # "returnObj":Ljava/lang/Object;
    :cond_0
    const-string v4, "ValidityFingerprintLibraryImpl"

    const-string v5, "protect END"

    invoke-static {v4, v5}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1534
    return-object v3
.end method
