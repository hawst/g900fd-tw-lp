.class public final Lcom/samsung/android/fingerprint/service/R$drawable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/fingerprint/service/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "drawable"
.end annotation


# static fields
.field public static final btn_scroll_down_light:I = 0x7f020000

.field public static final error_icon_default_red:I = 0x7f020001

.field public static final error_icon_default_white:I = 0x7f020002

.field public static final error_icon_diagonal_red:I = 0x7f020003

.field public static final error_icon_diagonal_white:I = 0x7f020004

.field public static final error_icon_homekey_red:I = 0x7f020005

.field public static final error_icon_homekey_white:I = 0x7f020006

.field public static final error_icon_nomatch_red:I = 0x7f020007

.field public static final error_icon_nomatch_white:I = 0x7f020008

.field public static final error_icon_same_blue_1:I = 0x7f020009

.field public static final error_icon_same_blue_2:I = 0x7f02000a

.field public static final error_icon_same_red:I = 0x7f02000b

.field public static final error_icon_same_white:I = 0x7f02000c

.field public static final error_icon_short_red:I = 0x7f02000d

.field public static final error_icon_short_white:I = 0x7f02000e

.field public static final error_icon_side_left_red:I = 0x7f02000f

.field public static final error_icon_side_left_white:I = 0x7f020010

.field public static final error_icon_side_right_red:I = 0x7f020011

.field public static final error_icon_side_right_white:I = 0x7f020012

.field public static final error_icon_speed_red:I = 0x7f020013

.field public static final error_icon_speed_white:I = 0x7f020014

.field public static final error_icon_up_red:I = 0x7f020015

.field public static final error_icon_up_white:I = 0x7f020016

.field public static final error_icon_wet_red:I = 0x7f020017

.field public static final error_icon_wet_white:I = 0x7f020018

.field public static final fingerprint_btn:I = 0x7f020019

.field public static final fingerprint_btn_focus:I = 0x7f02001a

.field public static final fingerprint_btn_press:I = 0x7f02001b

.field public static final fingerprint_lock_line:I = 0x7f02001c

.field public static final fingerprint_unlock_succeed:I = 0x7f02001d

.field public static final ic_webpass:I = 0x7f02001e

.field public static final ic_webpass_ac_password_press:I = 0x7f02001f

.field public static final ic_webpass_alternative:I = 0x7f020020

.field public static final ic_webpass_alternative_press:I = 0x7f020021

.field public static final ic_webpass_back_normal:I = 0x7f020022

.field public static final ic_webpass_focus:I = 0x7f020023

.field public static final ic_webpass_normal:I = 0x7f020024

.field public static final ic_webpass_press:I = 0x7f020025

.field public static final ic_webpass_select:I = 0x7f020026

.field public static final ic_webpass_selected:I = 0x7f020027

.field public static final icon_lock_backup:I = 0x7f020028

.field public static final icon_main_error:I = 0x7f020029

.field public static final spass_close_selector:I = 0x7f02002a

.field public static final spass_password_buttonbar_press:I = 0x7f02002b

.field public static final spass_password_pressed:I = 0x7f02002c

.field public static final spass_textfield_selector:I = 0x7f02002d

.field public static final tw_buttonbarbutton_selector_default_holo_light:I = 0x7f02002e

.field public static final tw_buttonbarbutton_selector_disabled_focused_holo_light:I = 0x7f02002f

.field public static final tw_buttonbarbutton_selector_disabled_holo_light:I = 0x7f020030

.field public static final tw_buttonbarbutton_selector_focused_holo_light:I = 0x7f020031

.field public static final tw_buttonbarbutton_selector_pressed_holo_light:I = 0x7f020032

.field public static final tw_buttonbarbutton_selector_selected_holo_light:I = 0x7f020033

.field public static final tw_dialog_title_holo_dark:I = 0x7f020034

.field public static final tw_textfield_default_holo_light:I = 0x7f020035

.field public static final tw_textfield_focused_holo_light:I = 0x7f020036

.field public static final tw_textfield_pressed_holo_light:I = 0x7f020037

.field public static final weblogin_bg:I = 0x7f020038


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
