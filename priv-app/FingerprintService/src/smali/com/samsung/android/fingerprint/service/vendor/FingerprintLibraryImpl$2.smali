.class Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;
.super Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;
.source "FingerprintLibraryImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# static fields
.field private static final DEFAULT_TIMEOUT_DURATION:I = 0x4e20


# instance fields
.field private allowIndexes:[I

.field private firstTouchTime:J

.field private identifyResult:Lcom/validity/fingerprint/IdentifyResult;

.field private isCancel:Z

.field private isCancelBySensor:Z

.field private isCaptureStarted:Z

.field private isCompleted:Z

.field private isFirstEvent:Z

.field private isResulted:Z

.field private isRunning:Z

.field private isSensorRemoved:Z

.field private isStarted:Z

.field private mAppName:Ljava/lang/String;

.field private mLastSwipeSpeed:I

.field private shouldDropEvents:Z

.field final synthetic this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

.field private timeoutCallback:Ljava/lang/Runnable;

.field private validityImageQuality:I


# direct methods
.method constructor <init>(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 4
    .param p2, "x0"    # Ljava/lang/String;
    .param p3, "x1"    # Ljava/lang/String;
    .param p4, "x2"    # I

    .prologue
    const/4 v3, 0x0

    const/4 v0, 0x1

    const/4 v2, 0x0

    .line 647
    iput-object p1, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    invoke-direct {p0, p2, p3, p4}, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    .line 649
    iput-boolean v2, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->isRunning:Z

    .line 650
    iput-boolean v0, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->isFirstEvent:Z

    .line 651
    iput-boolean v2, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->isCompleted:Z

    .line 652
    iput-boolean v2, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->isResulted:Z

    .line 653
    iput-boolean v2, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->isStarted:Z

    .line 654
    iput-boolean v2, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->isSensorRemoved:Z

    .line 655
    iput-boolean v2, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->isCancel:Z

    .line 656
    iput-boolean v0, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->isCancelBySensor:Z

    .line 657
    iput-boolean v2, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->isCaptureStarted:Z

    .line 659
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->firstTouchTime:J

    .line 661
    iput v2, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->validityImageQuality:I

    .line 663
    iput-boolean v2, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->shouldDropEvents:Z

    .line 664
    iput-object v3, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->mAppName:Ljava/lang/String;

    .line 665
    iput-object v3, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->allowIndexes:[I

    .line 671
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->mLastSwipeSpeed:I

    return-void
.end method

.method static synthetic access$702(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;
    .param p1, "x1"    # Z

    .prologue
    .line 647
    iput-boolean p1, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->shouldDropEvents:Z

    return p1
.end method

.method static synthetic access$802(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;
    .param p1, "x1"    # Z

    .prologue
    .line 647
    iput-boolean p1, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->isResulted:Z

    return p1
.end method

.method static synthetic access$900(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;

    .prologue
    .line 647
    invoke-direct {p0}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->timedOut()V

    return-void
.end method

.method private captureFailed()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x0

    .line 1139
    new-instance v0, Lcom/samsung/android/fingerprint/FingerprintEvent;

    const/16 v2, 0xe

    invoke-direct {v0, v2}, Lcom/samsung/android/fingerprint/FingerprintEvent;-><init>(I)V

    .line 1141
    .local v0, "gev":Lcom/samsung/android/fingerprint/FingerprintEvent;
    iget v2, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->validityImageQuality:I

    invoke-static {v2}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintValues$Validity2Generic;->getImageQuality(I)I

    move-result v1

    .line 1143
    .local v1, "imageQuality":I
    const/16 v2, 0x14

    iput v2, v0, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventStatus:I

    .line 1144
    iget-object v2, v0, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventData:Landroid/os/Bundle;

    const-string v3, "imageQuality"

    invoke-virtual {v2, v3, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1145
    iget-object v2, v0, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventData:Landroid/os/Bundle;

    const-string v3, "imageQualityFeedback"

    iget-object v4, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    # invokes: Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->getImageQualityFeedback(I)Ljava/lang/String;
    invoke-static {v4, v1}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->access$200(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1147
    const/4 v2, 0x4

    if-ne v1, v2, :cond_1

    .line 1148
    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    # operator++ for: Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mCntOfTooShort:I
    invoke-static {v2}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->access$308(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;)I

    .line 1149
    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    # getter for: Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mCntOfTooShort:I
    invoke-static {v2}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->access$300(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;)I

    move-result v2

    const/4 v3, 0x3

    if-lt v2, v3, :cond_0

    .line 1150
    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    # setter for: Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mCntOfTooShort:I
    invoke-static {v2, v5}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->access$302(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;I)I

    .line 1151
    const-string v2, "ValidityFingerprintLibraryImpl"

    const-string v3, "TOO SHORT --> TOO light pressure"

    invoke-static {v2, v3}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1152
    iget-object v2, v0, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventData:Landroid/os/Bundle;

    const-string v3, "imageQuality"

    invoke-virtual {v2, v3, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1154
    iget-object v2, v0, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventData:Landroid/os/Bundle;

    const-string v3, "imageQualityFeedback"

    iget-object v4, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    # invokes: Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->getImageQualityFeedback(I)Ljava/lang/String;
    invoke-static {v4, v6}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->access$200(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1160
    :cond_0
    :goto_0
    invoke-virtual {p0, v0}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->deliverEventToClient(Lcom/samsung/android/fingerprint/FingerprintEvent;)V

    .line 1161
    return-void

    .line 1158
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    # setter for: Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mCntOfTooShort:I
    invoke-static {v2, v5}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->access$302(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;I)I

    goto :goto_0
.end method

.method private isAllowedFinger(I)Z
    .locals 8
    .param p1, "index"    # I

    .prologue
    const/4 v4, 0x1

    .line 1007
    const-string v5, "ValidityFingerprintLibraryImpl"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "isAllowedFinger find:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " in "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->allowIndexes:[I

    invoke-static {v7}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->da(Ljava/lang/String;Ljava/lang/String;)V

    .line 1008
    iget-object v5, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->allowIndexes:[I

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->allowIndexes:[I

    array-length v5, v5

    if-nez v5, :cond_1

    .line 1017
    :cond_0
    :goto_0
    return v4

    .line 1011
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->allowIndexes:[I

    .local v0, "arr$":[I
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_1
    if-ge v2, v3, :cond_2

    aget v1, v0, v2

    .line 1012
    .local v1, "i":I
    if-eq v1, p1, :cond_0

    .line 1011
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1017
    .end local v1    # "i":I
    :cond_2
    const/4 v4, 0x0

    goto :goto_0
.end method

.method private restartTimeoutCallback()V
    .locals 4

    .prologue
    .line 1000
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->timeoutCallback:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 1001
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    # getter for: Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->access$1300(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->timeoutCallback:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1002
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    # getter for: Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->access$1300(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->timeoutCallback:Ljava/lang/Runnable;

    const-wide/16 v2, 0x4e20

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1004
    :cond_0
    return-void
.end method

.method private stopTimeoutCallback()V
    .locals 2

    .prologue
    .line 994
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->timeoutCallback:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 995
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    # getter for: Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->access$1300(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->timeoutCallback:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 997
    :cond_0
    return-void
.end method

.method private timedOut()V
    .locals 2

    .prologue
    .line 1121
    new-instance v0, Lcom/samsung/android/fingerprint/FingerprintEvent;

    const/16 v1, 0xd

    invoke-direct {v0, v1}, Lcom/samsung/android/fingerprint/FingerprintEvent;-><init>(I)V

    .line 1123
    .local v0, "gev":Lcom/samsung/android/fingerprint/FingerprintEvent;
    const/4 v1, -0x1

    iput v1, v0, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventResult:I

    .line 1124
    const/4 v1, 0x4

    iput v1, v0, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventStatus:I

    .line 1126
    invoke-virtual {p0, v0}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->deliverEventToClient(Lcom/samsung/android/fingerprint/FingerprintEvent;)V

    .line 1127
    return-void
.end method

.method private verifyFailed(I)V
    .locals 7
    .param p1, "validityStatus"    # I

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x0

    .line 1083
    new-instance v0, Lcom/samsung/android/fingerprint/FingerprintEvent;

    const/16 v2, 0xd

    invoke-direct {v0, v2}, Lcom/samsung/android/fingerprint/FingerprintEvent;-><init>(I)V

    .line 1085
    .local v0, "gev":Lcom/samsung/android/fingerprint/FingerprintEvent;
    const/4 v2, -0x1

    iput v2, v0, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventResult:I

    .line 1086
    invoke-static {p1}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintValues$Validity2Generic;->getStatus(I)I

    move-result v2

    iput v2, v0, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventStatus:I

    .line 1088
    iget v2, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->validityImageQuality:I

    invoke-static {v2}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintValues$Validity2Generic;->getImageQuality(I)I

    move-result v1

    .line 1090
    .local v1, "imageQuality":I
    iget v2, v0, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventStatus:I

    packed-switch v2, :pswitch_data_0

    .line 1102
    :goto_0
    :pswitch_0
    iget-object v2, v0, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventData:Landroid/os/Bundle;

    const-string v3, "imageQuality"

    invoke-virtual {v2, v3, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1103
    iget-object v2, v0, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventData:Landroid/os/Bundle;

    const-string v3, "imageQualityFeedback"

    iget-object v4, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    # invokes: Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->getImageQualityFeedback(I)Ljava/lang/String;
    invoke-static {v4, v1}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->access$200(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1104
    const/4 v2, 0x4

    if-ne v1, v2, :cond_2

    .line 1105
    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    # operator++ for: Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mCntOfTooShort:I
    invoke-static {v2}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->access$308(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;)I

    .line 1106
    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    # getter for: Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mCntOfTooShort:I
    invoke-static {v2}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->access$300(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;)I

    move-result v2

    const/4 v3, 0x3

    if-lt v2, v3, :cond_0

    .line 1107
    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    # setter for: Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mCntOfTooShort:I
    invoke-static {v2, v5}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->access$302(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;I)I

    .line 1108
    const-string v2, "ValidityFingerprintLibraryImpl"

    const-string v3, "TOO SHORT --> TOO light pressure"

    invoke-static {v2, v3}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1109
    iget-object v2, v0, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventData:Landroid/os/Bundle;

    const-string v3, "imageQuality"

    invoke-virtual {v2, v3, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1111
    iget-object v2, v0, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventData:Landroid/os/Bundle;

    const-string v3, "imageQualityFeedback"

    iget-object v4, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    # invokes: Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->getImageQualityFeedback(I)Ljava/lang/String;
    invoke-static {v4, v6}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->access$200(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1117
    :cond_0
    :goto_1
    invoke-virtual {p0, v0}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->deliverEventToClient(Lcom/samsung/android/fingerprint/FingerprintEvent;)V

    .line 1118
    return-void

    .line 1094
    :pswitch_1
    if-nez v1, :cond_1

    .line 1095
    const/16 v2, 0xb

    iput v2, v0, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventStatus:I

    .line 1096
    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    # setter for: Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mCntOfTooShort:I
    invoke-static {v2, v5}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->access$302(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;I)I

    goto :goto_0

    .line 1098
    :cond_1
    const/16 v2, 0xc

    iput v2, v0, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventStatus:I

    goto :goto_0

    .line 1115
    :cond_2
    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    # setter for: Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mCntOfTooShort:I
    invoke-static {v2, v5}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->access$302(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;I)I

    goto :goto_1

    .line 1090
    nop

    :pswitch_data_0
    .packed-switch 0xb
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private verifySuccess()V
    .locals 13

    .prologue
    const/4 v12, -0x1

    const/4 v11, 0x0

    .line 1021
    new-instance v1, Lcom/samsung/android/fingerprint/FingerprintEvent;

    const/16 v8, 0xd

    invoke-direct {v1, v8}, Lcom/samsung/android/fingerprint/FingerprintEvent;-><init>(I)V

    .line 1023
    .local v1, "gev":Lcom/samsung/android/fingerprint/FingerprintEvent;
    iget-object v8, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->identifyResult:Lcom/validity/fingerprint/IdentifyResult;

    iget-object v8, v8, Lcom/validity/fingerprint/IdentifyResult;->userId:Ljava/lang/String;

    invoke-static {v8}, Lcom/samsung/android/fingerprint/service/util/ValueUtils;->getIntUserId(Ljava/lang/String;)I

    move-result v2

    .line 1024
    .local v2, "identifiedUserId":I
    iget-object v8, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->identifyResult:Lcom/validity/fingerprint/IdentifyResult;

    iget-object v8, v8, Lcom/validity/fingerprint/IdentifyResult;->userId:Ljava/lang/String;

    invoke-static {v8}, Lcom/samsung/android/fingerprint/service/util/ValueUtils;->getOwnName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 1025
    .local v5, "ownName":Ljava/lang/String;
    iget-object v8, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    # setter for: Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mCntOfTooShort:I
    invoke-static {v8, v11}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->access$302(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;I)I

    .line 1026
    iget-object v8, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->identifyResult:Lcom/validity/fingerprint/IdentifyResult;

    iget v8, v8, Lcom/validity/fingerprint/IdentifyResult;->fingerIndex:I

    invoke-static {v8}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintValues$Validity2Generic;->getFingerIndex(I)I

    move-result v0

    .line 1028
    .local v0, "fingerIndex":I
    invoke-direct {p0, v0}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->isAllowedFinger(I)Z

    move-result v8

    if-nez v8, :cond_0

    .line 1029
    const-string v8, "ValidityFingerprintLibraryImpl"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "verify success but,"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "is not allowed"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1030
    const/16 v8, 0x16b

    invoke-direct {p0, v8}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->verifyFailed(I)V

    .line 1080
    :goto_0
    return-void

    .line 1033
    :cond_0
    const-string v8, "ValidityFingerprintLibraryImpl"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "verifySuccess: ownName="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", appName="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->mAppName:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1035
    const/4 v7, 0x0

    .line 1036
    .local v7, "permissions":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v8, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->mAppName:Ljava/lang/String;

    if-eqz v8, :cond_1

    const-string v8, "com.samsung.android.fingerprint.FingerprintIdentifyDialog"

    iget-object v9, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->mAppName:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v8

    if-nez v8, :cond_1

    .line 1037
    iget-object v8, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    # getter for: Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mDataManager:Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;
    invoke-static {v8}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->access$1400(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;)Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;

    move-result-object v8

    iget v9, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->userId:I

    const/4 v10, 0x0

    invoke-virtual {v8, v9, v10, v0}, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->getEnrolledPermissions(ILjava/lang/String;I)Ljava/util/List;

    move-result-object v7

    .line 1041
    :goto_1
    iget v8, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->validityImageQuality:I

    invoke-static {v8}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintValues$Validity2Generic;->getImageQuality(I)I

    move-result v3

    .line 1043
    .local v3, "imageQuality":I
    iput v11, v1, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventResult:I

    .line 1044
    const/16 v8, 0xb

    if-gt v8, v0, :cond_2

    const/16 v8, 0x14

    if-gt v0, v8, :cond_2

    .line 1046
    iget-object v8, v1, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventData:Landroid/os/Bundle;

    const-string v9, "fingerIndex"

    add-int/lit8 v10, v0, -0xa

    invoke-virtual {v8, v9, v10}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1051
    :goto_2
    iget-object v8, v1, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventData:Landroid/os/Bundle;

    const-string v9, "userId"

    invoke-virtual {v8, v9, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1052
    iget-object v8, v1, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventData:Landroid/os/Bundle;

    const-string v9, "imageQuality"

    invoke-virtual {v8, v9, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1053
    iget-object v8, v1, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventData:Landroid/os/Bundle;

    const-string v9, "imageQualityFeedback"

    iget-object v10, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    # invokes: Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->getImageQualityFeedback(I)Ljava/lang/String;
    invoke-static {v10, v3}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->access$200(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1055
    if-eqz v7, :cond_5

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v8

    if-lez v8, :cond_5

    .line 1057
    iget-object v8, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->permissionName:Ljava/lang/String;

    if-nez v8, :cond_3

    .line 1058
    invoke-virtual {p0, v7}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->convertToPermissionList(Ljava/util/List;)Ljava/lang/String;

    move-result-object v6

    .line 1059
    .local v6, "permissionList":Ljava/lang/String;
    iget-object v8, v1, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventData:Landroid/os/Bundle;

    const-string v9, "permission"

    invoke-virtual {v8, v9, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1079
    .end local v6    # "permissionList":Ljava/lang/String;
    :goto_3
    invoke-virtual {p0, v1}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->deliverEventToClient(Lcom/samsung/android/fingerprint/FingerprintEvent;)V

    goto/16 :goto_0

    .line 1039
    .end local v3    # "imageQuality":I
    :cond_1
    iget-object v8, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    # getter for: Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mDataManager:Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;
    invoke-static {v8}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->access$1500(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;)Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;

    move-result-object v8

    iget v9, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->userId:I

    iget-object v10, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->mAppName:Ljava/lang/String;

    invoke-virtual {v8, v9, v10, v0}, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->getEnrolledPermissions(ILjava/lang/String;I)Ljava/util/List;

    move-result-object v7

    goto :goto_1

    .line 1049
    .restart local v3    # "imageQuality":I
    :cond_2
    iget-object v8, v1, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventData:Landroid/os/Bundle;

    const-string v9, "fingerIndex"

    invoke-virtual {v8, v9, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_2

    .line 1061
    :cond_3
    iget-object v8, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->permissionName:Ljava/lang/String;

    invoke-virtual {p0, v7, v8}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->findMatchedPermission(Ljava/util/List;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1063
    .local v4, "matchedPermName":Ljava/lang/String;
    if-nez v4, :cond_4

    .line 1064
    iput v12, v1, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventResult:I

    goto :goto_3

    .line 1066
    :cond_4
    iget-object v8, v1, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventData:Landroid/os/Bundle;

    const-string v9, "permission"

    invoke-virtual {v8, v9, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 1071
    .end local v4    # "matchedPermName":Ljava/lang/String;
    :cond_5
    iget-object v8, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->permissionName:Ljava/lang/String;

    if-nez v8, :cond_6

    .line 1072
    iput v11, v1, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventResult:I

    goto :goto_3

    .line 1074
    :cond_6
    iput v12, v1, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventResult:I

    .line 1075
    const/16 v8, 0x10

    iput v8, v1, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventStatus:I

    goto :goto_3
.end method

.method private verifySuccessWithAlternativePassword()V
    .locals 2

    .prologue
    .line 1130
    new-instance v0, Lcom/samsung/android/fingerprint/FingerprintEvent;

    const/16 v1, 0xd

    invoke-direct {v0, v1}, Lcom/samsung/android/fingerprint/FingerprintEvent;-><init>(I)V

    .line 1132
    .local v0, "gev":Lcom/samsung/android/fingerprint/FingerprintEvent;
    const/4 v1, 0x0

    iput v1, v0, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventResult:I

    .line 1133
    const/16 v1, 0x64

    iput v1, v0, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventStatus:I

    .line 1135
    invoke-virtual {p0, v0}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->deliverEventToClient(Lcom/samsung/android/fingerprint/FingerprintEvent;)V

    .line 1136
    return-void
.end method


# virtual methods
.method public doCommand()Ljava/lang/Object;
    .locals 6

    .prologue
    .line 674
    iget-object v3, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->clientRecord:Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

    if-eqz v3, :cond_0

    .line 675
    iget-object v3, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->clientRecord:Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

    invoke-virtual {v3}, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->getAppName()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->mAppName:Ljava/lang/String;

    .line 676
    iget-object v3, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->clientRecord:Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

    invoke-virtual {v3}, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->getAllowIndexes()[I

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->allowIndexes:[I

    .line 679
    :cond_0
    const-string v3, "ValidityFingerprintLibraryImpl"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "identify: stringUserId="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->stringUserId:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", mAppName="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->mAppName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", allowIndexes="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->allowIndexes:[I

    invoke-static {v5}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 683
    iget-object v3, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    # getter for: Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mFingerprintLog:Lcom/samsung/android/fingerprint/service/util/FingerprintLog;
    invoke-static {v3}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->access$000(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;)Lcom/samsung/android/fingerprint/service/util/FingerprintLog;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 684
    iget-object v3, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    # getter for: Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mFingerprintLog:Lcom/samsung/android/fingerprint/service/util/FingerprintLog;
    invoke-static {v3}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->access$000(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;)Lcom/samsung/android/fingerprint/service/util/FingerprintLog;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->createLogFile()V

    .line 685
    iget-object v3, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    # getter for: Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mFingerprintLog:Lcom/samsung/android/fingerprint/service/util/FingerprintLog;
    invoke-static {v3}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->access$000(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;)Lcom/samsung/android/fingerprint/service/util/FingerprintLog;

    move-result-object v3

    invoke-virtual {p0}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->dumpArgumentValues()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->logToFile(Ljava/lang/String;)V

    .line 688
    :cond_1
    const-string v3, "ValidityFingerprintLibraryImpl"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "doCommand identify getSecurityLevel() ="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->getSecurityLevel()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 690
    invoke-virtual {p0}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->getSecurityLevel()I

    move-result v3

    invoke-static {v3}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintValues$Generic2Validity;->getSecurityLevel(I)I

    move-result v2

    .line 692
    .local v2, "validitySecurityLevel":I
    iget-object v3, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    # getter for: Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mLastValiditySecurityLevel:I
    invoke-static {v3}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->access$600(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;)I

    move-result v3

    if-eq v3, v2, :cond_2

    .line 693
    iget-object v3, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    # getter for: Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mFingerprint:Lcom/samsung/android/fingerprint/service/vendor/FingerprintProxy;
    invoke-static {v3}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->access$100(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;)Lcom/samsung/android/fingerprint/service/vendor/FingerprintProxy;

    move-result-object v3

    invoke-interface {v3, v2}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintProxy;->setSecurityLevel(I)I

    move-result v1

    .line 695
    .local v1, "retval":I
    if-eqz v1, :cond_4

    .line 696
    const-string v3, "ValidityFingerprintLibraryImpl"

    const-string v4, "setSecurityLevel failed!!!"

    invoke-static {v3, v4}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 702
    .end local v1    # "retval":I
    :cond_2
    :goto_0
    iget-object v3, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    # getter for: Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mFingerprint:Lcom/samsung/android/fingerprint/service/vendor/FingerprintProxy;
    invoke-static {v3}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->access$100(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;)Lcom/samsung/android/fingerprint/service/vendor/FingerprintProxy;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->stringUserId:Ljava/lang/String;

    invoke-interface {v3, v4}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintProxy;->identify(Ljava/lang/String;)I

    move-result v0

    .line 703
    .local v0, "result":I
    const-string v3, "ValidityFingerprintLibraryImpl"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "identify: result="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 705
    if-nez v0, :cond_3

    .line 706
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->isRunning:Z

    .line 708
    invoke-virtual {p0}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->doesUseDefaultTimeoutPolicy()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 710
    new-instance v3, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2$1;

    invoke-direct {v3, p0}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2$1;-><init>(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;)V

    iput-object v3, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->timeoutCallback:Ljava/lang/Runnable;

    .line 724
    invoke-direct {p0}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->restartTimeoutCallback()V

    .line 728
    :cond_3
    invoke-static {v0}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintValues$Validity2Generic;->getResult(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    return-object v3

    .line 698
    .end local v0    # "result":I
    .restart local v1    # "retval":I
    :cond_4
    iget-object v3, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    # setter for: Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mLastValiditySecurityLevel:I
    invoke-static {v3, v2}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->access$602(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;I)I

    goto :goto_0
.end method

.method public forceCancel()V
    .locals 1

    .prologue
    .line 732
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    # getter for: Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mFingerprint:Lcom/samsung/android/fingerprint/service/vendor/FingerprintProxy;
    invoke-static {v0}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->access$100(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;)Lcom/samsung/android/fingerprint/service/vendor/FingerprintProxy;

    move-result-object v0

    invoke-interface {v0}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintProxy;->cancel()I

    .line 733
    return-void
.end method

.method public notifyCommandCompleted()V
    .locals 0

    .prologue
    .line 989
    invoke-direct {p0}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->stopTimeoutCallback()V

    .line 990
    invoke-super {p0}, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;->notifyCommandCompleted()V

    .line 991
    return-void
.end method

.method public onEvent(Ljava/lang/Object;)Z
    .locals 22
    .param p1, "evt"    # Ljava/lang/Object;

    .prologue
    .line 753
    move-object/from16 v16, p1

    check-cast v16, Lcom/validity/fingerprint/FingerprintEvent;

    .line 754
    .local v16, "vev":Lcom/validity/fingerprint/FingerprintEvent;
    const/4 v7, 0x0

    .line 756
    .local v7, "gev":Lcom/samsung/android/fingerprint/FingerprintEvent;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    move-object/from16 v17, v0

    # getter for: Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mFingerprintLog:Lcom/samsung/android/fingerprint/service/util/FingerprintLog;
    invoke-static/range {v17 .. v17}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->access$000(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;)Lcom/samsung/android/fingerprint/service/util/FingerprintLog;

    move-result-object v17

    if-eqz v17, :cond_0

    .line 757
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    move-object/from16 v17, v0

    # getter for: Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mFingerprintLog:Lcom/samsung/android/fingerprint/service/util/FingerprintLog;
    invoke-static/range {v17 .. v17}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->access$000(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;)Lcom/samsung/android/fingerprint/service/util/FingerprintLog;

    move-result-object v17

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "Identify.onEvent: "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v16

    iget v0, v0, Lcom/validity/fingerprint/FingerprintEvent;->eventId:I

    move/from16 v19, v0

    invoke-static/range {v19 .. v19}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintValues$Validity2Generic;->eventIdToString(I)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ", "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v16

    iget v0, v0, Lcom/validity/fingerprint/FingerprintEvent;->eventId:I

    move/from16 v19, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/validity/fingerprint/FingerprintEvent;->eventData:Ljava/lang/Object;

    move-object/from16 v20, v0

    invoke-static/range {v19 .. v20}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintValues$Validity2Generic;->objectToString(ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->logToFile(Ljava/lang/String;)V

    .line 760
    :cond_0
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->shouldDropEvents:Z

    move/from16 v17, v0

    if-eqz v17, :cond_1

    .line 761
    const-string v17, "ValidityFingerprintLibraryImpl"

    const-string v18, "Dropping events because shouldDropEvents is true!!!"

    invoke-static/range {v17 .. v18}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 762
    const/16 v17, 0x1

    .line 966
    :goto_0
    return v17

    .line 770
    :cond_1
    const-string v17, "ValidityFingerprintLibraryImpl"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "identify: onEvent="

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v16

    iget v0, v0, Lcom/validity/fingerprint/FingerprintEvent;->eventId:I

    move/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 771
    move-object/from16 v0, v16

    iget v0, v0, Lcom/validity/fingerprint/FingerprintEvent;->eventId:I

    move/from16 v17, v0

    sparse-switch v17, :sswitch_data_0

    .line 961
    :cond_2
    :goto_1
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->isRunning:Z

    move/from16 v17, v0

    if-eqz v17, :cond_3

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->isResulted:Z

    move/from16 v17, v0

    if-eqz v17, :cond_3

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->isCompleted:Z

    move/from16 v17, v0

    if-eqz v17, :cond_3

    .line 962
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->notifyCommandCompleted()V

    .line 963
    const/16 v17, 0x0

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->isRunning:Z

    .line 966
    :cond_3
    const/16 v17, 0x1

    goto :goto_0

    .line 773
    :sswitch_0
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->isStarted:Z

    move/from16 v17, v0

    if-nez v17, :cond_4

    .line 774
    const/16 v17, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->isStarted:Z

    .line 775
    const/16 v17, 0xc

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->deliverEventToClient(I)V

    .line 777
    :cond_4
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->firstTouchTime:J

    move-wide/from16 v18, v0

    const-wide/16 v20, 0x0

    cmp-long v17, v18, v20

    if-nez v17, :cond_5

    .line 778
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v18

    move-wide/from16 v0, v18

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->firstTouchTime:J

    .line 781
    :cond_5
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->restartTimeoutCallback()V

    goto :goto_1

    .line 785
    :sswitch_1
    const/16 v17, 0x3f3

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->deliverExtraEventToClient(I)V

    goto :goto_1

    .line 789
    :sswitch_2
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/validity/fingerprint/FingerprintEvent;->eventData:Ljava/lang/Object;

    move-object/from16 v17, v0

    if-eqz v17, :cond_2

    .line 790
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/validity/fingerprint/FingerprintEvent;->eventData:Ljava/lang/Object;

    move-object/from16 v17, v0

    check-cast v17, Ljava/lang/Integer;

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Integer;->intValue()I

    move-result v14

    .line 792
    .local v14, "validitySpeed":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->mLastSwipeSpeed:I

    move/from16 v17, v0

    move/from16 v0, v17

    if-eq v0, v14, :cond_2

    .line 793
    move-object/from16 v0, p0

    iput v14, v0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->mLastSwipeSpeed:I

    .line 794
    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->deliverSwipeSpeedEventToClient(I)V

    goto/16 :goto_1

    .line 801
    .end local v14    # "validitySpeed":I
    :sswitch_3
    const/16 v17, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->isCompleted:Z

    .line 802
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/validity/fingerprint/FingerprintEvent;->eventData:Ljava/lang/Object;

    move-object/from16 v17, v0

    check-cast v17, Lcom/validity/fingerprint/IdentifyResult;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->identifyResult:Lcom/validity/fingerprint/IdentifyResult;

    goto/16 :goto_1

    .line 806
    :sswitch_4
    const/16 v17, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->isCaptureStarted:Z

    .line 807
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->isFirstEvent:Z

    move/from16 v17, v0

    if-eqz v17, :cond_6

    .line 808
    const/16 v17, 0x0

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->isFirstEvent:Z

    .line 809
    const/16 v17, 0xb

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->deliverEventToClient(I)V

    .line 811
    :cond_6
    const/16 v17, 0x0

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->validityImageQuality:I

    goto/16 :goto_1

    .line 815
    :sswitch_5
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/validity/fingerprint/FingerprintEvent;->eventData:Ljava/lang/Object;

    move-object/from16 v17, v0

    if-eqz v17, :cond_2

    .line 816
    move-object/from16 v0, v16

    iget-object v5, v0, Lcom/validity/fingerprint/FingerprintEvent;->eventData:Ljava/lang/Object;

    check-cast v5, Lcom/validity/fingerprint/FingerprintBitmap;

    .line 818
    .local v5, "fb":Lcom/validity/fingerprint/FingerprintBitmap;
    if-eqz v5, :cond_2

    .line 819
    iget v0, v5, Lcom/validity/fingerprint/FingerprintBitmap;->quality:I

    move/from16 v17, v0

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->validityImageQuality:I

    goto/16 :goto_1

    .line 826
    .end local v5    # "fb":Lcom/validity/fingerprint/FingerprintBitmap;
    :sswitch_6
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/validity/fingerprint/FingerprintEvent;->eventData:Ljava/lang/Object;

    move-object/from16 v17, v0

    if-eqz v17, :cond_7

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/validity/fingerprint/FingerprintEvent;->eventData:Ljava/lang/Object;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    instance-of v0, v0, Ljava/lang/Integer;

    move/from16 v17, v0

    if-eqz v17, :cond_7

    .line 827
    move-object/from16 v0, v16

    iget-object v8, v0, Lcom/validity/fingerprint/FingerprintEvent;->eventData:Ljava/lang/Object;

    check-cast v8, Ljava/lang/Integer;

    .line 828
    .local v8, "imgQuality":Ljava/lang/Integer;
    if-eqz v8, :cond_7

    .line 829
    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v17

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->validityImageQuality:I

    .line 832
    .end local v8    # "imgQuality":Ljava/lang/Integer;
    :cond_7
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->captureFailed()V

    .line 833
    const/16 v17, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->isFirstEvent:Z

    .line 834
    const/16 v17, 0x0

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->isStarted:Z

    .line 835
    const-wide/16 v18, 0x0

    move-wide/from16 v0, v18

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->firstTouchTime:J

    goto/16 :goto_1

    .line 840
    :sswitch_7
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->stopTimeoutCallback()V

    .line 842
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->identifyResult:Lcom/validity/fingerprint/IdentifyResult;

    move-object/from16 v17, v0

    if-eqz v17, :cond_9

    .line 843
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    move-object/from16 v17, v0

    # getter for: Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mIsEnrollSession:Z
    invoke-static/range {v17 .. v17}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->access$1000(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;)Z

    move-result v17

    if-eqz v17, :cond_8

    .line 844
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    move-object/from16 v17, v0

    const/16 v18, 0x1

    # setter for: Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mIsVerifiedInEnrollSession:Z
    invoke-static/range {v17 .. v18}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->access$1102(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;Z)Z

    .line 846
    :cond_8
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->verifySuccess()V

    .line 852
    :goto_2
    const/16 v17, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->isResulted:Z

    goto/16 :goto_1

    .line 848
    :cond_9
    const-string v17, "ValidityFingerprintLibraryImpl"

    const-string v18, "IDENTIFY_SUCCESS but identifyResult is null!"

    invoke-static/range {v17 .. v18}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 849
    const/16 v17, -0x1

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->verifyFailed(I)V

    goto :goto_2

    .line 856
    :sswitch_8
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->stopTimeoutCallback()V

    .line 857
    const/16 v17, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->isResulted:Z

    .line 858
    const/16 v17, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->isCompleted:Z

    .line 860
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->forceCancel()V

    .line 861
    new-instance v7, Lcom/samsung/android/fingerprint/FingerprintEvent;

    .end local v7    # "gev":Lcom/samsung/android/fingerprint/FingerprintEvent;
    const/16 v17, 0xd

    move/from16 v0, v17

    invoke-direct {v7, v0}, Lcom/samsung/android/fingerprint/FingerprintEvent;-><init>(I)V

    .line 862
    .restart local v7    # "gev":Lcom/samsung/android/fingerprint/FingerprintEvent;
    const/16 v17, 0x7

    move/from16 v0, v17

    iput v0, v7, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventStatus:I

    .line 863
    const/16 v17, -0x1

    move/from16 v0, v17

    iput v0, v7, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventResult:I

    .line 864
    move-object/from16 v0, p0

    invoke-virtual {v0, v7}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->deliverEventToClient(Lcom/samsung/android/fingerprint/FingerprintEvent;)V

    goto/16 :goto_1

    .line 869
    :sswitch_9
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->stopTimeoutCallback()V

    .line 870
    const/16 v17, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->isResulted:Z

    .line 871
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/validity/fingerprint/FingerprintEvent;->eventData:Ljava/lang/Object;

    move-object/from16 v17, v0

    if-eqz v17, :cond_2

    .line 872
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/validity/fingerprint/FingerprintEvent;->eventData:Ljava/lang/Object;

    move-object/from16 v17, v0

    check-cast v17, Ljava/lang/Integer;

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Integer;->intValue()I

    move-result v15

    .line 874
    .local v15, "validityStatus":I
    invoke-static {v15}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintValues$Validity2Generic;->getStatus(I)I

    move-result v17

    const/16 v18, 0x7

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_b

    .line 876
    new-instance v7, Lcom/samsung/android/fingerprint/FingerprintEvent;

    .end local v7    # "gev":Lcom/samsung/android/fingerprint/FingerprintEvent;
    const/16 v17, 0xd

    move/from16 v0, v17

    invoke-direct {v7, v0}, Lcom/samsung/android/fingerprint/FingerprintEvent;-><init>(I)V

    .line 877
    .restart local v7    # "gev":Lcom/samsung/android/fingerprint/FingerprintEvent;
    const/16 v17, 0x168

    move/from16 v0, v17

    if-ne v15, v0, :cond_a

    .line 878
    const-string v17, "ValidityFingerprintLibraryImpl"

    const-string v18, "Sensor is not available for a while. This command will not be finished until sensor ready again."

    invoke-static/range {v17 .. v18}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 879
    const/16 v17, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->isSensorRemoved:Z

    .line 880
    const/16 v17, 0x0

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->isResulted:Z

    .line 881
    new-instance v7, Lcom/samsung/android/fingerprint/FingerprintEvent;

    .end local v7    # "gev":Lcom/samsung/android/fingerprint/FingerprintEvent;
    const/16 v17, 0xe

    move/from16 v0, v17

    invoke-direct {v7, v0}, Lcom/samsung/android/fingerprint/FingerprintEvent;-><init>(I)V

    .line 883
    .restart local v7    # "gev":Lcom/samsung/android/fingerprint/FingerprintEvent;
    :cond_a
    const/16 v17, 0x7

    move/from16 v0, v17

    iput v0, v7, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventStatus:I

    .line 884
    const/16 v17, -0x1

    move/from16 v0, v17

    iput v0, v7, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventResult:I

    .line 885
    move-object/from16 v0, p0

    invoke-virtual {v0, v7}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->deliverEventToClient(Lcom/samsung/android/fingerprint/FingerprintEvent;)V

    goto/16 :goto_1

    .line 888
    :cond_b
    const/4 v4, 0x0

    .line 889
    .local v4, "bSkipEvent":Z
    invoke-static {v15}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintValues$Validity2Generic;->getStatus(I)I

    move-result v17

    const/16 v18, 0x8

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_c

    .line 890
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->isCancel:Z

    move/from16 v17, v0

    if-nez v17, :cond_c

    .line 891
    const/16 v17, 0x0

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->isCompleted:Z

    .line 892
    const/16 v17, 0x0

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->isResulted:Z

    .line 893
    const/4 v4, 0x1

    .line 896
    :cond_c
    if-nez v4, :cond_d

    .line 897
    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->verifyFailed(I)V

    goto/16 :goto_1

    .line 899
    :cond_d
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->isCancelBySensor:Z

    move/from16 v17, v0

    if-eqz v17, :cond_2

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->isCaptureStarted:Z

    move/from16 v17, v0

    if-eqz v17, :cond_2

    .line 900
    const/16 v17, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->isCompleted:Z

    .line 901
    const/16 v17, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->isResulted:Z

    .line 902
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->timedOut()V

    goto/16 :goto_1

    .line 911
    .end local v4    # "bSkipEvent":Z
    .end local v15    # "validityStatus":I
    :sswitch_a
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/validity/fingerprint/FingerprintEvent;->eventData:Ljava/lang/Object;

    move-object/from16 v17, v0

    if-eqz v17, :cond_2

    .line 912
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/validity/fingerprint/FingerprintEvent;->eventData:Ljava/lang/Object;

    move-object/from16 v17, v0

    check-cast v17, Ljava/lang/Integer;

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Integer;->intValue()I

    move-result v13

    .line 913
    .local v13, "validityDirection":I
    invoke-static {v13}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintValues$Validity2Generic;->getSwipeDirection(I)I

    move-result v6

    .line 915
    .local v6, "genericDirection":I
    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->deliverNavigationEventToClient(I)V

    goto/16 :goto_1

    .line 920
    .end local v6    # "genericDirection":I
    .end local v13    # "validityDirection":I
    :sswitch_b
    const/4 v12, 0x0

    .line 921
    .local v12, "skip":Z
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->firstTouchTime:J

    move-wide/from16 v18, v0

    const-wide/16 v20, 0x0

    cmp-long v17, v18, v20

    if-eqz v17, :cond_e

    .line 922
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v18

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->firstTouchTime:J

    move-wide/from16 v20, v0

    sub-long v10, v18, v20

    .line 923
    .local v10, "restTime":J
    const-wide/16 v18, 0x190

    cmp-long v17, v10, v18

    if-gez v17, :cond_e

    .line 924
    const/4 v12, 0x1

    .line 928
    .end local v10    # "restTime":J
    :cond_e
    if-nez v12, :cond_f

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->isExtraEventDemanded()Z

    move-result v17

    if-eqz v17, :cond_f

    .line 929
    const/16 v17, 0x3f2

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->deliverExtraEventToClient(I)V

    .line 931
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->notifyCommandCompleted()V

    .line 932
    const/16 v17, 0x0

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->isRunning:Z

    goto/16 :goto_1

    .line 934
    :cond_f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    move-object/from16 v17, v0

    # getter for: Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mFingerprint:Lcom/samsung/android/fingerprint/service/vendor/FingerprintProxy;
    invoke-static/range {v17 .. v17}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->access$100(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;)Lcom/samsung/android/fingerprint/service/vendor/FingerprintProxy;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->stringUserId:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-interface/range {v17 .. v18}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintProxy;->identify(Ljava/lang/String;)I

    move-result v9

    .line 936
    .local v9, "result":I
    if-eqz v9, :cond_2

    .line 937
    const-string v17, "ValidityFingerprintLibraryImpl"

    const-string v18, "Cannot re-identify for VCS_EVT_GESTURE. "

    invoke-static/range {v17 .. v18}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 940
    move-object/from16 v0, v16

    iget v0, v0, Lcom/validity/fingerprint/FingerprintEvent;->eventId:I

    move/from16 v17, v0

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->verifyFailed(I)V

    .line 941
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->notifyCommandCompleted()V

    .line 942
    const/16 v17, 0x0

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->isRunning:Z

    goto/16 :goto_1

    .line 948
    .end local v9    # "result":I
    .end local v12    # "skip":Z
    :sswitch_c
    const-string v17, "ValidityFingerprintLibraryImpl"

    const-string v18, "Sensor became ready again!!"

    invoke-static/range {v17 .. v18}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 949
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->isSensorRemoved:Z

    move/from16 v17, v0

    if-eqz v17, :cond_2

    .line 950
    new-instance v7, Lcom/samsung/android/fingerprint/FingerprintEvent;

    .end local v7    # "gev":Lcom/samsung/android/fingerprint/FingerprintEvent;
    const/16 v17, 0xd

    move/from16 v0, v17

    invoke-direct {v7, v0}, Lcom/samsung/android/fingerprint/FingerprintEvent;-><init>(I)V

    .line 951
    .restart local v7    # "gev":Lcom/samsung/android/fingerprint/FingerprintEvent;
    const/16 v17, -0x1

    move/from16 v0, v17

    iput v0, v7, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventResult:I

    .line 952
    const/16 v17, 0x7

    move/from16 v0, v17

    iput v0, v7, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventStatus:I

    .line 953
    const/16 v17, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->isResulted:Z

    .line 954
    const/16 v17, 0x0

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->isSensorRemoved:Z

    .line 956
    move-object/from16 v0, p0

    invoke-virtual {v0, v7}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->deliverEventToClient(Lcom/samsung/android/fingerprint/FingerprintEvent;)V

    goto/16 :goto_1

    .line 771
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_8
        0x3 -> :sswitch_c
        0x8 -> :sswitch_4
        0xb -> :sswitch_0
        0xe -> :sswitch_3
        0xf -> :sswitch_3
        0x11 -> :sswitch_5
        0x14 -> :sswitch_1
        0x29 -> :sswitch_a
        0x2a -> :sswitch_2
        0x2d -> :sswitch_6
        0x1a6 -> :sswitch_7
        0x1a7 -> :sswitch_7
        0x1a9 -> :sswitch_9
        0x1aa -> :sswitch_9
        0x1ab -> :sswitch_b
    .end sparse-switch
.end method

.method public onInterrupt(IILjava/lang/Object;)Z
    .locals 3
    .param p1, "type"    # I
    .param p2, "param1"    # I
    .param p3, "param2"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    .line 971
    move-object v0, p3

    check-cast v0, Ljava/lang/String;

    .line 973
    .local v0, "inputPassword":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    # getter for: Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;
    invoke-static {v2}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->access$1200(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;)Lcom/android/internal/widget/LockPatternUtils;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/android/internal/widget/LockPatternUtils;->checkFingerprintPassword(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 974
    invoke-direct {p0}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->verifySuccessWithAlternativePassword()V

    .line 976
    iput-boolean v1, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->shouldDropEvents:Z

    .line 977
    iput-boolean v1, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->isResulted:Z

    .line 979
    invoke-virtual {p0}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->requestCancel()Z

    .line 984
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public requestCancel()Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 738
    const-string v2, "ValidityFingerprintLibraryImpl"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "cancel() in identify.requestCancel: isRunning = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->isRunning:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", isCancel = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->isCancel:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->da(Ljava/lang/String;Ljava/lang/String;)V

    .line 739
    iget-boolean v2, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->isRunning:Z

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->isCancel:Z

    if-nez v2, :cond_0

    .line 740
    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    # getter for: Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mFingerprint:Lcom/samsung/android/fingerprint/service/vendor/FingerprintProxy;
    invoke-static {v2}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->access$100(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;)Lcom/samsung/android/fingerprint/service/vendor/FingerprintProxy;

    move-result-object v2

    invoke-interface {v2}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintProxy;->cancel()I

    move-result v2

    if-nez v2, :cond_0

    .line 741
    iput-boolean v0, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->isCancel:Z

    .line 742
    iput-boolean v1, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->isCancelBySensor:Z

    .line 743
    invoke-direct {p0}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;->stopTimeoutCallback()V

    .line 749
    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method
