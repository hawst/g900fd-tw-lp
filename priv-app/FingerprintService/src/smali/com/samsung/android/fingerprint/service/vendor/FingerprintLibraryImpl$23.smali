.class Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$23;
.super Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;
.source "FingerprintLibraryImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;


# direct methods
.method constructor <init>(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 0
    .param p2, "x0"    # Ljava/lang/String;
    .param p3, "x1"    # Ljava/lang/String;
    .param p4, "x2"    # I

    .prologue
    .line 2109
    iput-object p1, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$23;->this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    invoke-direct {p0, p2, p3, p4}, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method


# virtual methods
.method public doCommand()Ljava/lang/Object;
    .locals 4

    .prologue
    .line 2118
    const-string v1, "ValidityFingerprintLibraryImpl"

    const-string v2, "getSensorInfo: BEGIN"

    invoke-static {v1, v2}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2119
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$23;->this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    # getter for: Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mFingerprint:Lcom/samsung/android/fingerprint/service/vendor/FingerprintProxy;
    invoke-static {v1}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->access$100(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;)Lcom/samsung/android/fingerprint/service/vendor/FingerprintProxy;

    move-result-object v1

    const/16 v2, 0xc

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintProxy;->request(ILjava/lang/Object;)I

    move-result v0

    .line 2120
    .local v0, "result":I
    const-string v1, "ValidityFingerprintLibraryImpl"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getSensorInfo result="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2121
    const-string v1, "ValidityFingerprintLibraryImpl"

    const-string v2, "getSensorInfo: END"

    invoke-static {v1, v2}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2122
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    return-object v1
.end method

.method public onEvent(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "evt"    # Ljava/lang/Object;

    .prologue
    .line 2127
    move-object v0, p1

    check-cast v0, Lcom/validity/fingerprint/FingerprintEvent;

    .line 2128
    .local v0, "vev":Lcom/validity/fingerprint/FingerprintEvent;
    iget v1, v0, Lcom/validity/fingerprint/FingerprintEvent;->eventId:I

    const/16 v2, 0x1ac

    if-ne v1, v2, :cond_0

    .line 2129
    const-string v1, "ValidityFingerprintLibraryImpl"

    const-string v2, "getSensorInfo: onEvent"

    invoke-static {v1, v2}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2131
    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$23;->this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    iget-object v1, v0, Lcom/validity/fingerprint/FingerprintEvent;->eventData:Ljava/lang/Object;

    check-cast v1, Lcom/validity/fingerprint/SensorInfo;

    # setter for: Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mDeviceInfo:Lcom/validity/fingerprint/SensorInfo;
    invoke-static {v2, v1}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->access$2602(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;Lcom/validity/fingerprint/SensorInfo;)Lcom/validity/fingerprint/SensorInfo;

    .line 2132
    invoke-virtual {p0}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$23;->notifyCommandCompleted()V

    .line 2134
    :cond_0
    const/4 v1, 0x1

    return v1
.end method

.method public requestCancel()Z
    .locals 2

    .prologue
    .line 2113
    const-string v0, "ValidityFingerprintLibraryImpl"

    const-string v1, "cancel() in getSensorInfo.requestCancel"

    invoke-static {v0, v1}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2114
    const/4 v0, 0x1

    return v0
.end method
