.class public abstract Lcom/samsung/android/fingerprint/service/FingerprintLibraryImplAbstractClass;
.super Ljava/lang/Object;
.source "FingerprintLibraryImplAbstractClass.java"


# instance fields
.field protected mDataManager:Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract checkIntegrity()V
.end method

.method public abstract dump(Ljava/io/PrintWriter;)V
.end method

.method public abstract getImplVersion()Ljava/lang/String;
.end method

.method public abstract getLibraryVersion()Ljava/lang/String;
.end method

.method public abstract isSensorReady()Z
.end method

.method public abstract registerCommands(Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommandFactory;)V
.end method

.method public abstract registerListener(Lcom/samsung/android/fingerprint/service/FingerprintLibraryListener;)Z
.end method
