.class public Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;
.super Lcom/samsung/android/fingerprint/service/FingerprintLibraryImplAbstractClass;
.source "FingerprintLibraryImpl.java"

# interfaces
.implements Lcom/validity/fingerprint/FingerprintCore$EventListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$ImmediateCommand;,
        Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$ValiditySyncCommand;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "ValidityFingerprintLibraryImpl"

.field private static final VALIDITY_FINGERPRINT_LIBRARY_IMPL_VER:Ljava/lang/String; = "4_6_503_1"


# instance fields
.field addPermission:Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;

.field cancel:Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;

.field cleanUp:Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;

.field enroll:Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;

.field getEnrollRepeatCount:Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;

.field getEnrolledFingers:Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;

.field getEnrolledPermissions:Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;

.field getEnrolledUsers:Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;

.field getFingerprintIdByFinger:Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;

.field getFingerprintIds:Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;

.field getSensorInfo:Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;

.field identify:Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;

.field isEnrolledPermission:Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;

.field lock:Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;

.field private mCntOfTooShort:I

.field private mContext:Landroid/content/Context;

.field private mDeviceInfo:Lcom/validity/fingerprint/SensorInfo;

.field private mFingerprint:Lcom/samsung/android/fingerprint/service/vendor/FingerprintProxy;

.field private mFingerprintLog:Lcom/samsung/android/fingerprint/service/util/FingerprintLog;

.field private mHandler:Landroid/os/Handler;

.field private mIsEnrollSession:Z

.field private mIsVerifiedInEnrollSession:Z

.field private mLastValiditySecurityLevel:I

.field private final mLibraryVersion:Ljava/lang/String;

.field private mListener:Lcom/samsung/android/fingerprint/service/FingerprintLibraryListener;

.field private mListenerRegistered:Z

.field private mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

.field notify:Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;

.field processFIDO:Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;

.field protect:Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;

.field removeAllEnrolledFingers:Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;

.field removeEnrolledFinger:Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;

.field removeEnrolledPermission:Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;

.field removeEnrolledPermissions:Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;

.field setPassword:Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;

.field unProtect:Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;

.field verifyPassword:Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommandFactory;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "factory"    # Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommandFactory;

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x0

    const/4 v4, 0x0

    .line 89
    invoke-direct {p0}, Lcom/samsung/android/fingerprint/service/FingerprintLibraryImplAbstractClass;-><init>()V

    .line 71
    iput-boolean v2, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mListenerRegistered:Z

    .line 79
    const/4 v1, -0x1

    iput v1, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mLastValiditySecurityLevel:I

    .line 81
    iput-object v4, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mFingerprintLog:Lcom/samsung/android/fingerprint/service/util/FingerprintLog;

    .line 83
    iput-boolean v2, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mIsVerifiedInEnrollSession:Z

    .line 84
    iput-boolean v2, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mIsEnrollSession:Z

    .line 85
    iput v2, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mCntOfTooShort:I

    .line 87
    iput-object v4, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mDeviceInfo:Lcom/validity/fingerprint/SensorInfo;

    .line 337
    new-instance v1, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$1;

    const-string v2, "enroll"

    const-string v3, "0=stringUserId:1=appName:2=permissionName:3=fingerIndex:4=enrollMode"

    invoke-direct {v1, p0, v2, v3, v5}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$1;-><init>(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;Ljava/lang/String;Ljava/lang/String;I)V

    iput-object v1, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->enroll:Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;

    .line 644
    new-instance v1, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;

    const-string v2, "identify"

    const-string v3, "0=stringUserId:1=clientRecord:2=permissionName"

    invoke-direct {v1, p0, v2, v3, v5}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$2;-><init>(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;Ljava/lang/String;Ljava/lang/String;I)V

    iput-object v1, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->identify:Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;

    .line 1165
    new-instance v1, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$3;

    const-string v2, "cleanUp"

    invoke-direct {v1, p0, v2, v4}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$3;-><init>(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->cleanUp:Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;

    .line 1185
    new-instance v1, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$4;

    const-string v2, "cancel"

    const-string v3, "0=userId"

    invoke-direct {v1, p0, v2, v3}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$4;-><init>(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->cancel:Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;

    .line 1199
    new-instance v1, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$5;

    const-string v2, "getEnrolledFingers"

    const-string v3, "0=stringUserId"

    invoke-direct {v1, p0, v2, v3}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$5;-><init>(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->getEnrolledFingers:Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;

    .line 1221
    new-instance v1, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$6;

    const-string v2, "getEnrolledPermissions"

    const-string v3, "0=userId:1=appName"

    invoke-direct {v1, p0, v2, v3}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$6;-><init>(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->getEnrolledPermissions:Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;

    .line 1232
    new-instance v1, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$7;

    const-string v2, "getEnrolledUsers"

    invoke-direct {v1, p0, v2, v4}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$7;-><init>(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->getEnrolledUsers:Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;

    .line 1248
    new-instance v1, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$8;

    const-string v2, "removeEnrolledFinger"

    const-string v3, "0=stringUserId:1=fingerIndex"

    invoke-direct {v1, p0, v2, v3}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$8;-><init>(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->removeEnrolledFinger:Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;

    .line 1292
    new-instance v1, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$9;

    const-string v2, "removeEnrolledPermission"

    const-string v3, "0=userId:1=appName:2=permissionName:3=fingerIndex"

    invoke-direct {v1, p0, v2, v3}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$9;-><init>(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->removeEnrolledPermission:Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;

    .line 1303
    new-instance v1, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$10;

    const-string v2, "removeAllEnrolledFingers"

    const-string v3, "0=stringUserId"

    invoke-direct {v1, p0, v2, v3}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$10;-><init>(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->removeAllEnrolledFingers:Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;

    .line 1337
    new-instance v1, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$11;

    const-string v2, "removeEnrolledPermissions"

    const-string v3, "0=userId:1=appName:2=permissionName"

    invoke-direct {v1, p0, v2, v3}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$11;-><init>(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->removeEnrolledPermissions:Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;

    .line 1354
    new-instance v1, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$12;

    const-string v2, "addPermission"

    const-string v3, "0=userId:1=appName:2=permissionName:3=fingerIndex"

    invoke-direct {v1, p0, v2, v3}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$12;-><init>(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->addPermission:Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;

    .line 1384
    new-instance v1, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$13;

    const-string v2, "isEnrolledPermission"

    const-string v3, "0=userId:1=appName:2=permissionName"

    invoke-direct {v1, p0, v2, v3}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$13;-><init>(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->isEnrolledPermission:Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;

    .line 1393
    new-instance v1, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$14;

    const-string v2, "lock"

    invoke-direct {v1, p0, v2, v4, v5}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$14;-><init>(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;Ljava/lang/String;Ljava/lang/String;I)V

    iput-object v1, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->lock:Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;

    .line 1424
    new-instance v1, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$15;

    const-string v2, "notify"

    const-string v3, "0=notifyCondition"

    invoke-direct {v1, p0, v2, v3}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$15;-><init>(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->notify:Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;

    .line 1466
    new-instance v1, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$16;

    const-string v2, "verifyPassword"

    const-string v3, "0=stringUserId:1=password"

    invoke-direct {v1, p0, v2, v3}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$16;-><init>(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->verifyPassword:Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;

    .line 1493
    new-instance v1, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$17;

    const-string v2, "setPassword"

    const-string v3, "0=stringUserId:1=newPassword"

    invoke-direct {v1, p0, v2, v3}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$17;-><init>(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->setPassword:Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;

    .line 1516
    new-instance v1, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$18;

    const-string v2, "protect"

    const-string v3, "0=userId:1=appName:2=permissionName:3=inputByteData"

    invoke-direct {v1, p0, v2, v3}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$18;-><init>(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->protect:Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;

    .line 1538
    new-instance v1, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$19;

    const-string v2, "unProtect"

    const-string v3, "0=userId:1=appName:2=permissionName:3=inputByteData"

    invoke-direct {v1, p0, v2, v3}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$19;-><init>(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->unProtect:Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;

    .line 1561
    new-instance v1, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$20;

    const-string v2, "processFIDO"

    const-string v3, "0=userId:1=appName:2=permissionName:3=inputByteData"

    invoke-direct {v1, p0, v2, v3}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$20;-><init>(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->processFIDO:Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;

    .line 2053
    new-instance v1, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$21;

    const-string v2, "getFingerprintIds"

    const-string v3, "0=stringUserId"

    invoke-direct {v1, p0, v2, v3}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$21;-><init>(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->getFingerprintIds:Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;

    .line 2085
    new-instance v1, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$22;

    const-string v2, "getFingerprintIdByFinger"

    const-string v3, "0=stringUserId:1=fingerIndex"

    invoke-direct {v1, p0, v2, v3}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$22;-><init>(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->getFingerprintIdByFinger:Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;

    .line 2106
    new-instance v1, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$23;

    const-string v2, "getSensorInfo"

    invoke-direct {v1, p0, v2, v4, v5}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$23;-><init>(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;Ljava/lang/String;Ljava/lang/String;I)V

    iput-object v1, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->getSensorInfo:Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;

    .line 2142
    new-instance v1, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$24;

    const-string v2, "getEnrollRepeatCount"

    invoke-direct {v1, p0, v2, v4}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$24;-><init>(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->getEnrollRepeatCount:Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;

    .line 90
    iput-object p1, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mContext:Landroid/content/Context;

    .line 91
    new-instance v0, Lcom/validity/fingerprint/Fingerprint;

    invoke-direct {v0, p1}, Lcom/validity/fingerprint/Fingerprint;-><init>(Landroid/content/Context;)V

    .line 92
    .local v0, "validity":Lcom/validity/fingerprint/Fingerprint;
    invoke-static {v0}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintProxyFactory;->create(Ljava/lang/Object;)Lcom/samsung/android/fingerprint/service/vendor/FingerprintProxy;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mFingerprint:Lcom/samsung/android/fingerprint/service/vendor/FingerprintProxy;

    .line 94
    new-instance v1, Landroid/os/Handler;

    invoke-virtual {p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mHandler:Landroid/os/Handler;

    .line 96
    invoke-virtual {p0, p2}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->registerCommands(Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommandFactory;)V

    .line 98
    new-instance v1, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;

    invoke-direct {v1, p1}, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mDataManager:Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;

    .line 99
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mDataManager:Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;

    invoke-virtual {v1}, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->start()V

    .line 101
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mFingerprint:Lcom/samsung/android/fingerprint/service/vendor/FingerprintProxy;

    invoke-interface {v1}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintProxy;->getVersion()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mLibraryVersion:Ljava/lang/String;

    .line 102
    new-instance v1, Lcom/android/internal/widget/LockPatternUtils;

    invoke-direct {v1, p1}, Lcom/android/internal/widget/LockPatternUtils;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    .line 104
    new-instance v1, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;

    invoke-direct {v1, p1}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mFingerprintLog:Lcom/samsung/android/fingerprint/service/util/FingerprintLog;

    .line 105
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;)Lcom/samsung/android/fingerprint/service/util/FingerprintLog;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mFingerprintLog:Lcom/samsung/android/fingerprint/service/util/FingerprintLog;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;)Lcom/samsung/android/fingerprint/service/vendor/FingerprintProxy;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mFingerprint:Lcom/samsung/android/fingerprint/service/vendor/FingerprintProxy;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    .prologue
    .line 64
    iget-boolean v0, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mIsEnrollSession:Z

    return v0
.end method

.method static synthetic access$1002(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;
    .param p1, "x1"    # Z

    .prologue
    .line 64
    iput-boolean p1, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mIsEnrollSession:Z

    return p1
.end method

.method static synthetic access$102(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;Lcom/samsung/android/fingerprint/service/vendor/FingerprintProxy;)Lcom/samsung/android/fingerprint/service/vendor/FingerprintProxy;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;
    .param p1, "x1"    # Lcom/samsung/android/fingerprint/service/vendor/FingerprintProxy;

    .prologue
    .line 64
    iput-object p1, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mFingerprint:Lcom/samsung/android/fingerprint/service/vendor/FingerprintProxy;

    return-object p1
.end method

.method static synthetic access$1100(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    .prologue
    .line 64
    iget-boolean v0, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mIsVerifiedInEnrollSession:Z

    return v0
.end method

.method static synthetic access$1102(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;
    .param p1, "x1"    # Z

    .prologue
    .line 64
    iput-boolean p1, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mIsVerifiedInEnrollSession:Z

    return p1
.end method

.method static synthetic access$1200(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;)Lcom/android/internal/widget/LockPatternUtils;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;)Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mDataManager:Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;)Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mDataManager:Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;

    return-object v0
.end method

.method static synthetic access$1602(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;Lcom/samsung/android/fingerprint/service/FingerprintLibraryListener;)Lcom/samsung/android/fingerprint/service/FingerprintLibraryListener;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;
    .param p1, "x1"    # Lcom/samsung/android/fingerprint/service/FingerprintLibraryListener;

    .prologue
    .line 64
    iput-object p1, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mListener:Lcom/samsung/android/fingerprint/service/FingerprintLibraryListener;

    return-object p1
.end method

.method static synthetic access$1700(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;)Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mDataManager:Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;)Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mDataManager:Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;)Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mDataManager:Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;I)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;
    .param p1, "x1"    # I

    .prologue
    .line 64
    invoke-direct {p0, p1}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->getImageQualityFeedback(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2000(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;)Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mDataManager:Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;

    return-object v0
.end method

.method static synthetic access$2100(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;)Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mDataManager:Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;

    return-object v0
.end method

.method static synthetic access$2200(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;)Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mDataManager:Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;

    return-object v0
.end method

.method static synthetic access$2300(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;)Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mDataManager:Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;

    return-object v0
.end method

.method static synthetic access$2400(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;)Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mDataManager:Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;

    return-object v0
.end method

.method static synthetic access$2500(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;Ljava/lang/String;)[B
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 64
    invoke-direct {p0, p1}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->generateSha1Hash(Ljava/lang/String;)[B

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2602(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;Lcom/validity/fingerprint/SensorInfo;)Lcom/validity/fingerprint/SensorInfo;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;
    .param p1, "x1"    # Lcom/validity/fingerprint/SensorInfo;

    .prologue
    .line 64
    iput-object p1, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mDeviceInfo:Lcom/validity/fingerprint/SensorInfo;

    return-object p1
.end method

.method static synthetic access$300(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    .prologue
    .line 64
    iget v0, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mCntOfTooShort:I

    return v0
.end method

.method static synthetic access$302(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;
    .param p1, "x1"    # I

    .prologue
    .line 64
    iput p1, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mCntOfTooShort:I

    return p1
.end method

.method static synthetic access$308(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;)I
    .locals 2
    .param p0, "x0"    # Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    .prologue
    .line 64
    iget v0, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mCntOfTooShort:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mCntOfTooShort:I

    return v0
.end method

.method static synthetic access$400(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$500(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;)Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mDataManager:Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;

    return-object v0
.end method

.method static synthetic access$600(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    .prologue
    .line 64
    iget v0, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mLastValiditySecurityLevel:I

    return v0
.end method

.method static synthetic access$602(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;
    .param p1, "x1"    # I

    .prologue
    .line 64
    iput p1, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mLastValiditySecurityLevel:I

    return p1
.end method

.method private checkIntegrityChild(Ljava/lang/String;)V
    .locals 26
    .param p1, "stringUserId"    # Ljava/lang/String;

    .prologue
    .line 160
    new-instance v11, Lcom/validity/fingerprint/VcsInt;

    invoke-direct {v11}, Lcom/validity/fingerprint/VcsInt;-><init>()V

    .line 161
    .local v11, "fingermask":Lcom/validity/fingerprint/VcsInt;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mFingerprint:Lcom/samsung/android/fingerprint/service/vendor/FingerprintProxy;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    move-object/from16 v1, p1

    invoke-interface {v0, v1, v11}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintProxy;->getEnrolledFingerList(Ljava/lang/String;Lcom/validity/fingerprint/VcsInt;)I

    move-result v18

    .line 163
    .local v18, "result":I
    if-eqz v18, :cond_1

    .line 164
    const-string v23, "ValidityFingerprintLibraryImpl"

    const-string v24, "checkIntegrityChild: failed because getEnrolledFingerList returned error."

    invoke-static/range {v23 .. v24}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 265
    :cond_0
    :goto_0
    return-void

    .line 168
    :cond_1
    iget v0, v11, Lcom/validity/fingerprint/VcsInt;->num:I

    move/from16 v23, v0

    if-eqz v23, :cond_2

    iget v0, v11, Lcom/validity/fingerprint/VcsInt;->num:I

    move/from16 v23, v0

    const/16 v24, -0x1

    move/from16 v0, v23

    move/from16 v1, v24

    if-ne v0, v1, :cond_3

    .line 169
    :cond_2
    const-string v23, "ValidityFingerprintLibraryImpl"

    const-string v24, "checkIntegrityChild: getEnrolledFingerList is failed"

    invoke-static/range {v23 .. v24}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 173
    :cond_3
    iget v0, v11, Lcom/validity/fingerprint/VcsInt;->num:I

    move/from16 v23, v0

    invoke-static/range {v23 .. v23}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintValues$Validity2Generic;->getFingerMask(I)I

    move-result v21

    .line 174
    .local v21, "validityFingerMask":I
    const/4 v5, 0x0

    .line 176
    .local v5, "dbFingerMask":I
    invoke-static/range {p1 .. p1}, Lcom/samsung/android/fingerprint/service/util/ValueUtils;->getIntUserId(Ljava/lang/String;)I

    move-result v20

    .line 177
    .local v20, "userId":I
    invoke-static/range {p1 .. p1}, Lcom/samsung/android/fingerprint/service/util/ValueUtils;->getOwnName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 178
    .local v4, "appName":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mDataManager:Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    move/from16 v1, v20

    invoke-virtual {v0, v1, v4}, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->getEnrolledPermissions(ILjava/lang/String;)Ljava/util/List;

    move-result-object v17

    .line 180
    .local v17, "permissions":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v16, Landroid/util/SparseArray;

    invoke-direct/range {v16 .. v16}, Landroid/util/SparseArray;-><init>()V

    .line 181
    .local v16, "permList":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Ljava/lang/String;>;>;"
    if-eqz v17, :cond_7

    .line 182
    invoke-interface/range {v17 .. v17}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    .local v13, "i$":Ljava/util/Iterator;
    :cond_4
    :goto_1
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v23

    if-eqz v23, :cond_7

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/String;

    .line 183
    .local v15, "perm":Ljava/lang/String;
    const-string v23, ":|,"

    move-object/from16 v0, v23

    invoke-virtual {v15, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v19

    .line 185
    .local v19, "tokens":[Ljava/lang/String;
    if-eqz v19, :cond_4

    move-object/from16 v0, v19

    array-length v0, v0

    move/from16 v23, v0

    const/16 v24, 0x1

    move/from16 v0, v23

    move/from16 v1, v24

    if-lt v0, v1, :cond_4

    .line 190
    const/16 v23, 0x0

    :try_start_0
    aget-object v23, v19, v23

    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v9

    .line 191
    .local v9, "finger":I
    const/16 v23, 0x1

    shl-int v23, v23, v9

    or-int v5, v5, v23

    .line 193
    const/4 v12, 0x1

    .local v12, "i":I
    :goto_2
    move-object/from16 v0, v19

    array-length v0, v0

    move/from16 v23, v0

    move/from16 v0, v23

    if-ge v12, v0, :cond_4

    .line 194
    move-object/from16 v0, v16

    invoke-virtual {v0, v9}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/util/List;

    .line 196
    .local v14, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-nez v14, :cond_5

    .line 197
    new-instance v14, Ljava/util/ArrayList;

    .end local v14    # "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    .line 198
    .restart local v14    # "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    move-object/from16 v0, v16

    invoke-virtual {v0, v9, v14}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 201
    :cond_5
    aget-object v23, v19, v12

    move-object/from16 v0, v23

    invoke-interface {v14, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v23

    if-nez v23, :cond_6

    .line 202
    aget-object v23, v19, v12

    move-object/from16 v0, v23

    invoke-interface {v14, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 193
    :cond_6
    add-int/lit8 v12, v12, 0x1

    goto :goto_2

    .line 205
    .end local v9    # "finger":I
    .end local v12    # "i":I
    .end local v14    # "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :catch_0
    move-exception v6

    .line 206
    .local v6, "e":Ljava/lang/Exception;
    const-string v23, "checkIntegrityChild"

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-direct {v0, v1, v6}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->logExceptionInDetail(Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_1

    .line 211
    .end local v6    # "e":Ljava/lang/Exception;
    .end local v13    # "i$":Ljava/util/Iterator;
    .end local v15    # "perm":Ljava/lang/String;
    .end local v19    # "tokens":[Ljava/lang/String;
    :cond_7
    const-string v7, ""

    .line 213
    .local v7, "enrolledFingerList":Ljava/lang/String;
    const/4 v12, 0x1

    .restart local v12    # "i":I
    :goto_3
    const/16 v23, 0xa

    move/from16 v0, v23

    if-gt v12, v0, :cond_a

    .line 214
    const/16 v23, 0x1

    shl-int v23, v23, v12

    and-int v23, v23, v21

    if-eqz v23, :cond_8

    .line 215
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v23

    if-nez v23, :cond_9

    .line 216
    invoke-static {v12}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    .line 213
    :cond_8
    :goto_4
    add-int/lit8 v12, v12, 0x1

    goto :goto_3

    .line 218
    :cond_9
    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v23

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, ", "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    goto :goto_4

    .line 223
    :cond_a
    const-string v23, "ValidityFingerprintLibraryImpl"

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "checkIntegrityChild: Enrolled finger list to Validity Library is ["

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, "]"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v23 .. v24}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 225
    const-string v8, ""

    .line 227
    .local v8, "enrolledPermList":Ljava/lang/String;
    const/4 v12, 0x0

    :goto_5
    invoke-virtual/range {v16 .. v16}, Landroid/util/SparseArray;->size()I

    move-result v23

    move/from16 v0, v23

    if-ge v12, v0, :cond_c

    .line 228
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v23

    if-nez v23, :cond_b

    .line 229
    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, ""

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v16

    invoke-virtual {v0, v12}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 227
    :goto_6
    add-int/lit8 v12, v12, 0x1

    goto :goto_5

    .line 231
    :cond_b
    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v23

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, ", "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v16

    invoke-virtual {v0, v12}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    goto :goto_6

    .line 235
    :cond_c
    const-string v23, "ValidityFingerprintLibraryImpl"

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "checkIntegrityChild: Enrolled permission list ["

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, "]"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v23 .. v24}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 238
    xor-int v22, v21, v5

    .line 239
    .local v22, "wrongFingerMask":I
    if-nez v22, :cond_d

    .line 240
    const-string v23, "ValidityFingerprintLibraryImpl"

    const-string v24, "checkIntegrityChild: OK"

    invoke-static/range {v23 .. v24}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 242
    :cond_d
    const-string v23, "ValidityFingerprintLibraryImpl"

    const-string v24, "checkIntegrityChild: Need to match"

    invoke-static/range {v23 .. v24}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 243
    if-nez v21, :cond_e

    .line 244
    const-string v23, "ValidityFingerprintLibraryImpl"

    const-string v24, "checkIntegrityChild: No enrolled fingers on library."

    invoke-static/range {v23 .. v24}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 247
    :cond_e
    const/4 v10, 0x1

    .local v10, "fingerIndex":I
    :goto_7
    const/16 v23, 0xa

    move/from16 v0, v23

    if-gt v10, v0, :cond_0

    .line 248
    const/16 v23, 0x1

    shl-int v23, v23, v10

    and-int v23, v23, v22

    if-nez v23, :cond_f

    .line 247
    :goto_8
    add-int/lit8 v10, v10, 0x1

    goto :goto_7

    .line 251
    :cond_f
    const/16 v23, 0x1

    shl-int v23, v23, v10

    and-int v23, v23, v21

    if-eqz v23, :cond_11

    .line 252
    const-string v23, "ValidityFingerprintLibraryImpl"

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "checkIntegrityChild: FingerIndex:"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, ", id:"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, " is exist. So add this fingerIndex to FingerprintService"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v23 .. v24}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 253
    if-eqz v4, :cond_10

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v23

    if-lez v23, :cond_10

    .line 254
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mDataManager:Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    move/from16 v1, v20

    invoke-virtual {v0, v1, v4, v10, v4}, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->insert(ILjava/lang/String;ILjava/lang/String;)I

    goto :goto_8

    .line 256
    :cond_10
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mDataManager:Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;

    move-object/from16 v23, v0

    const-string v24, "system"

    const-string v25, "com.android.settings.permission.unlock"

    move-object/from16 v0, v23

    move/from16 v1, v20

    move-object/from16 v2, v24

    move-object/from16 v3, v25

    invoke-virtual {v0, v1, v2, v10, v3}, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->insert(ILjava/lang/String;ILjava/lang/String;)I

    goto :goto_8

    .line 260
    :cond_11
    const-string v23, "ValidityFingerprintLibraryImpl"

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "checkIntegrityChild: FingerIndex : "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, " is not exist, So it will be removed."

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v23 .. v24}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 261
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mDataManager:Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v10}, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->removeEnrolledFinger(Ljava/lang/String;I)I

    goto/16 :goto_8
.end method

.method private generateSha1Hash(Ljava/lang/String;)[B
    .locals 5
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 1616
    :try_start_0
    const-string v2, "SHA-1"

    invoke-static {v2}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v1

    .line 1617
    .local v1, "md":Ljava/security/MessageDigest;
    const-string v2, "iso-8859-1"

    invoke-virtual {p1, v2}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {v1, v2, v3, v4}, Ljava/security/MessageDigest;->update([BII)V

    .line 1618
    invoke-virtual {v1}, Ljava/security/MessageDigest;->digest()[B
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v2

    .line 1624
    .end local v1    # "md":Ljava/security/MessageDigest;
    :goto_0
    return-object v2

    .line 1619
    :catch_0
    move-exception v0

    .line 1620
    .local v0, "e":Ljava/security/NoSuchAlgorithmException;
    const-string v2, "generateSha1Hash"

    invoke-direct {p0, v2, v0}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->logExceptionInDetail(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 1624
    .end local v0    # "e":Ljava/security/NoSuchAlgorithmException;
    :goto_1
    const/4 v2, 0x0

    goto :goto_0

    .line 1621
    :catch_1
    move-exception v0

    .line 1622
    .local v0, "e":Ljava/io/UnsupportedEncodingException;
    const-string v2, "generateSha1Hash"

    invoke-direct {p0, v2, v0}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->logExceptionInDetail(Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_1
.end method

.method private getImageQualityFeedback(I)Ljava/lang/String;
    .locals 3
    .param p1, "quality"    # I

    .prologue
    .line 1673
    const/4 v1, 0x0

    .line 1674
    .local v1, "result":I
    sparse-switch p1, :sswitch_data_0

    .line 1721
    const v1, 0x7f060006

    .line 1725
    :goto_0
    if-eqz v1, :cond_0

    .line 1726
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 1731
    :goto_1
    return-object v2

    .line 1676
    :sswitch_0
    const v1, 0x7f060022

    .line 1677
    goto :goto_0

    .line 1681
    :sswitch_1
    const v1, 0x7f060023

    .line 1682
    goto :goto_0

    .line 1684
    :sswitch_2
    const v1, 0x7f060024

    .line 1685
    goto :goto_0

    .line 1687
    :sswitch_3
    const v1, 0x7f060025

    .line 1688
    goto :goto_0

    .line 1690
    :sswitch_4
    const v1, 0x7f060026

    .line 1691
    goto :goto_0

    .line 1693
    :sswitch_5
    const v1, 0x7f060027

    .line 1694
    goto :goto_0

    .line 1696
    :sswitch_6
    const v1, 0x7f060028

    .line 1697
    goto :goto_0

    .line 1699
    :sswitch_7
    const v1, 0x7f060029

    .line 1700
    goto :goto_0

    .line 1702
    :sswitch_8
    const v1, 0x7f06002a

    .line 1703
    goto :goto_0

    .line 1705
    :sswitch_9
    const v1, 0x7f06002b

    .line 1706
    goto :goto_0

    .line 1708
    :sswitch_a
    const v1, 0x7f06002c

    .line 1709
    goto :goto_0

    .line 1712
    :sswitch_b
    const v1, 0x7f06002d

    .line 1713
    goto :goto_0

    .line 1715
    :sswitch_c
    const v1, 0x7f06002e

    .line 1716
    goto :goto_0

    .line 1718
    :sswitch_d
    const v1, 0x7f06002f

    .line 1719
    goto :goto_0

    .line 1728
    :catch_0
    move-exception v0

    .line 1729
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 1731
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    const/4 v2, 0x0

    goto :goto_1

    .line 1674
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_7
        0x2 -> :sswitch_3
        0x3 -> :sswitch_b
        0x4 -> :sswitch_1
        0x10 -> :sswitch_2
        0x200 -> :sswitch_c
        0x1000 -> :sswitch_1
        0x2000 -> :sswitch_1
        0x8000 -> :sswitch_b
        0x10000 -> :sswitch_9
        0x20000 -> :sswitch_4
        0x40000 -> :sswitch_5
        0x80000 -> :sswitch_8
        0x1000000 -> :sswitch_a
        0x2000000 -> :sswitch_6
        0x30000000 -> :sswitch_d
    .end sparse-switch
.end method

.method private logExceptionInDetail(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 3
    .param p1, "methodName"    # Ljava/lang/String;
    .param p2, "e"    # Ljava/lang/Exception;

    .prologue
    .line 1741
    const-string v0, "ValidityFingerprintLibraryImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": failed "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p2}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1742
    return-void
.end method


# virtual methods
.method public checkIntegrity()V
    .locals 11

    .prologue
    .line 130
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v7

    .line 132
    .local v7, "userId":I
    new-instance v8, Lcom/validity/fingerprint/VcsStringArray;

    invoke-direct {v8}, Lcom/validity/fingerprint/VcsStringArray;-><init>()V

    .line 134
    .local v8, "userList":Lcom/validity/fingerprint/VcsStringArray;
    invoke-static {v7}, Lcom/samsung/android/fingerprint/service/util/ValueUtils;->getStringUserId(I)Ljava/lang/String;

    move-result-object v4

    .line 135
    .local v4, "prefix":Ljava/lang/String;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 136
    .local v1, "cUsers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v9, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mFingerprint:Lcom/samsung/android/fingerprint/service/vendor/FingerprintProxy;

    invoke-interface {v9, v8}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintProxy;->getUserList(Lcom/validity/fingerprint/VcsStringArray;)I

    move-result v9

    if-nez v9, :cond_1

    .line 137
    iget-object v9, v8, Lcom/validity/fingerprint/VcsStringArray;->strlist:[Ljava/lang/String;

    if-eqz v9, :cond_3

    iget-object v9, v8, Lcom/validity/fingerprint/VcsStringArray;->strlist:[Ljava/lang/String;

    array-length v9, v9

    if-lez v9, :cond_3

    .line 138
    iget-object v0, v8, Lcom/validity/fingerprint/VcsStringArray;->strlist:[Ljava/lang/String;

    .local v0, "arr$":[Ljava/lang/String;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_3

    aget-object v5, v0, v2

    .line 139
    .local v5, "sUser":Ljava/lang/String;
    invoke-virtual {v5, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 140
    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 138
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 145
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    .end local v5    # "sUser":Ljava/lang/String;
    :cond_1
    const-string v9, "ValidityFingerprintLibraryImpl"

    const-string v10, "checkIntegrity: failed because getUserList returned error."

    invoke-static {v9, v10}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 157
    :cond_2
    :goto_1
    return-void

    .line 149
    :cond_3
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-nez v9, :cond_4

    .line 150
    const-string v9, "ValidityFingerprintLibraryImpl"

    const-string v10, "checkIntegrity: empty user"

    invoke-static {v9, v10}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 154
    :cond_4
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 155
    .local v6, "stringUserId":Ljava/lang/String;
    invoke-direct {p0, v6}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->checkIntegrityChild(Ljava/lang/String;)V

    goto :goto_2
.end method

.method public dump(Ljava/io/PrintWriter;)V
    .locals 2
    .param p1, "pw"    # Ljava/io/PrintWriter;

    .prologue
    .line 1661
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  Sensor Status : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mFingerprint:Lcom/samsung/android/fingerprint/service/vendor/FingerprintProxy;

    invoke-interface {v1}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintProxy;->getSensorStatus()I

    move-result v1

    invoke-static {v1}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintValues$Validity2Generic;->getSensorStatusName(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1662
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  Security level : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mLastValiditySecurityLevel:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1663
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mDeviceInfo:Lcom/validity/fingerprint/SensorInfo;

    if-eqz v0, :cond_0

    .line 1664
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  Sensor fwVersion : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mDeviceInfo:Lcom/validity/fingerprint/SensorInfo;

    iget-object v1, v1, Lcom/validity/fingerprint/SensorInfo;->fwVersion:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1665
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  Sensor flexId : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mDeviceInfo:Lcom/validity/fingerprint/SensorInfo;

    iget v1, v1, Lcom/validity/fingerprint/SensorInfo;->flexId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1667
    :cond_0
    invoke-virtual {p1}, Ljava/io/PrintWriter;->println()V

    .line 1669
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mDataManager:Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;

    invoke-virtual {v0, p1}, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->dump(Ljava/io/PrintWriter;)V

    .line 1670
    return-void
.end method

.method public getImplVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 268
    const-string v0, "4_6_503_1"

    return-object v0
.end method

.method public getLibraryVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 272
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mLibraryVersion:Ljava/lang/String;

    return-object v0
.end method

.method public getSensorInfo()Lcom/validity/fingerprint/SensorInfo;
    .locals 1

    .prologue
    .line 2139
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mDeviceInfo:Lcom/validity/fingerprint/SensorInfo;

    return-object v0
.end method

.method public isSensorReady()Z
    .locals 5

    .prologue
    .line 109
    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mFingerprint:Lcom/samsung/android/fingerprint/service/vendor/FingerprintProxy;

    invoke-interface {v2}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintProxy;->getSensorStatus()I

    move-result v1

    .line 111
    .local v1, "validitySensorStatus":I
    const/4 v0, 0x0

    .line 113
    .local v0, "result":Z
    sparse-switch v1, :sswitch_data_0

    .line 120
    const-string v2, "ValidityFingerprintLibraryImpl"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "isSensorReady: status="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v1}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintValues$Validity2Generic;->getSensorStatusName(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    const/4 v2, 0x0

    invoke-static {v2}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->setSensorReady(Z)V

    .line 126
    :goto_0
    return v0

    .line 116
    :sswitch_0
    const/4 v0, 0x1

    .line 117
    goto :goto_0

    .line 113
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x3ec -> :sswitch_0
    .end sparse-switch
.end method

.method public isTZLib()Z
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v4, 0x0

    .line 1745
    iget-object v5, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mLibraryVersion:Ljava/lang/String;

    if-nez v5, :cond_1

    .line 1763
    :cond_0
    :goto_0
    return v4

    .line 1750
    :cond_1
    :try_start_0
    iget-object v5, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mLibraryVersion:Ljava/lang/String;

    const-string v6, "\\."

    const/4 v7, 0x4

    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v3

    .line 1751
    .local v3, "versionArray":[Ljava/lang/String;
    array-length v5, v3

    if-ne v5, v8, :cond_0

    .line 1752
    const/4 v5, 0x2

    aget-object v5, v3, v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v0

    .line 1753
    .local v0, "intDivision":I
    const/16 v5, 0xc8

    if-ge v5, v0, :cond_0

    .line 1754
    const/4 v4, 0x1

    goto :goto_0

    .line 1757
    .end local v0    # "intDivision":I
    .end local v3    # "versionArray":[Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 1758
    .local v2, "npe":Ljava/lang/NullPointerException;
    const-string v5, "ValidityFingerprintLibraryImpl"

    const-string v6, "isTZLib"

    invoke-static {v5, v6, v2}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1759
    .end local v2    # "npe":Ljava/lang/NullPointerException;
    :catch_1
    move-exception v1

    .line 1760
    .local v1, "nfe":Ljava/lang/NumberFormatException;
    const-string v5, "ValidityFingerprintLibraryImpl"

    const-string v6, "isTZLib"

    invoke-static {v5, v6, v1}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public onEvent(Lcom/validity/fingerprint/FingerprintEvent;)V
    .locals 4
    .param p1, "evt"    # Lcom/validity/fingerprint/FingerprintEvent;

    .prologue
    .line 295
    if-nez p1, :cond_1

    .line 296
    const-string v0, "ValidityFingerprintLibraryImpl"

    const-string v1, "onEvent: FingerprintEvent is null"

    invoke-static {v0, v1}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 314
    :cond_0
    :goto_0
    return-void

    .line 300
    :cond_1
    const-string v0, "ValidityFingerprintLibraryImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onEvent: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Lcom/validity/fingerprint/FingerprintEvent;->eventId:I

    invoke-static {v2}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintValues$Validity2Generic;->eventIdToString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Lcom/validity/fingerprint/FingerprintEvent;->eventId:I

    iget-object v3, p1, Lcom/validity/fingerprint/FingerprintEvent;->eventData:Ljava/lang/Object;

    invoke-static {v2, v3}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintValues$Validity2Generic;->objectToString(ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->da(Ljava/lang/String;Ljava/lang/String;)V

    .line 304
    iget v0, p1, Lcom/validity/fingerprint/FingerprintEvent;->eventId:I

    if-nez v0, :cond_2

    iget-object v0, p1, Lcom/validity/fingerprint/FingerprintEvent;->eventData:Ljava/lang/Object;

    if-nez v0, :cond_2

    .line 305
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mListener:Lcom/samsung/android/fingerprint/service/FingerprintLibraryListener;

    if-eqz v0, :cond_0

    .line 306
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mListener:Lcom/samsung/android/fingerprint/service/FingerprintLibraryListener;

    invoke-interface {v0}, Lcom/samsung/android/fingerprint/service/FingerprintLibraryListener;->onFpDaemonDied()V

    goto :goto_0

    .line 310
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mListener:Lcom/samsung/android/fingerprint/service/FingerprintLibraryListener;

    if-eqz v0, :cond_0

    .line 311
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mListener:Lcom/samsung/android/fingerprint/service/FingerprintLibraryListener;

    invoke-interface {v0, p1}, Lcom/samsung/android/fingerprint/service/FingerprintLibraryListener;->onEvent(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public registerCommands(Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommandFactory;)V
    .locals 9
    .param p1, "factory"    # Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommandFactory;

    .prologue
    .line 322
    const-class v7, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    invoke-virtual {v7}, Ljava/lang/Class;->getDeclaredFields()[Ljava/lang/reflect/Field;

    move-result-object v4

    .line 324
    .local v4, "fields":[Ljava/lang/reflect/Field;
    move-object v0, v4

    .local v0, "arr$":[Ljava/lang/reflect/Field;
    array-length v6, v0

    .local v6, "len$":I
    const/4 v5, 0x0

    .local v5, "i$":I
    :goto_0
    if-ge v5, v6, :cond_1

    aget-object v3, v0, v5

    .line 325
    .local v3, "field":Ljava/lang/reflect/Field;
    invoke-virtual {v3}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    move-result-object v7

    const-class v8, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;

    invoke-virtual {v7, v8}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 327
    :try_start_0
    invoke-virtual {v3, p0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;

    .line 329
    .local v1, "cmd":Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;
    invoke-virtual {p1, v1}, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommandFactory;->register(Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 324
    .end local v1    # "cmd":Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;
    :cond_0
    :goto_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 330
    :catch_0
    move-exception v2

    .line 331
    .local v2, "e":Ljava/lang/Exception;
    const-string v7, "registerCommands"

    invoke-direct {p0, v7, v2}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->logExceptionInDetail(Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_1

    .line 335
    .end local v2    # "e":Ljava/lang/Exception;
    .end local v3    # "field":Ljava/lang/reflect/Field;
    :cond_1
    return-void
.end method

.method public registerListener(Lcom/samsung/android/fingerprint/service/FingerprintLibraryListener;)Z
    .locals 4
    .param p1, "listener"    # Lcom/samsung/android/fingerprint/service/FingerprintLibraryListener;

    .prologue
    const/4 v0, 0x1

    .line 276
    if-nez p1, :cond_1

    .line 277
    const-string v0, "ValidityFingerprintLibraryImpl"

    const-string v1, "registerListener: Invalid callback listener"

    invoke-static {v0, v1}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 278
    const/4 v0, 0x0

    .line 291
    :cond_0
    :goto_0
    return v0

    .line 282
    :cond_1
    iput-object p1, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mListener:Lcom/samsung/android/fingerprint/service/FingerprintLibraryListener;

    .line 284
    iget-boolean v1, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mListenerRegistered:Z

    if-nez v1, :cond_0

    .line 285
    const-string v1, "ValidityFingerprintLibraryImpl"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "registerListener: EventListener="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 287
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mFingerprint:Lcom/samsung/android/fingerprint/service/vendor/FingerprintProxy;

    invoke-interface {v1, p0}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintProxy;->registerListener(Lcom/validity/fingerprint/FingerprintCore$EventListener;)I

    .line 288
    iput-boolean v0, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mListenerRegistered:Z

    goto :goto_0
.end method
