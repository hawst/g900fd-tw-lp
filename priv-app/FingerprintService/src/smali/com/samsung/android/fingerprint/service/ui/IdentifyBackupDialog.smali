.class public Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;
.super Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog;
.source "IdentifyBackupDialog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog$16;,
        Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog$H;
    }
.end annotation


# static fields
.field private static final BUTTON_TEXTCOLOR_DISABLE:I = 0x66f5f5f5

.field private static final BUTTON_TEXTCOLOR_ENABLE:I = -0x1000000

.field private static final CLOSE_ERR_MSG:I = 0x1388

.field private static final MINIMUM_HEIGHT_OF_KEYPAD:I = 0xc8

.field private static final PASSWORD_LENGTH:I = 0x1

.field private static final TAG:Ljava/lang/String; = "IdentifyBackupDialog"

.field private static final WRONG_PATTERN_CLEAR_TIMEOUT_MS:I = 0x7d0


# instance fields
.field protected mAltPassword:Landroid/widget/EditText;

.field protected mAltPasswordBox:Landroid/widget/LinearLayout;

.field protected mAltPasswordGuide:Landroid/widget/TextView;

.field protected mAltPatternBox:Landroid/widget/RelativeLayout;

.field private mAltPatternGuide:Landroid/widget/TextView;

.field protected mAltPatternListener:Lcom/android/internal/widget/LockPatternView$OnPatternListener;

.field protected mAltPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

.field protected mAltPatternView:Lcom/android/internal/widget/LockPatternView;

.field protected mAltPin:Landroid/widget/EditText;

.field protected mAltPinBox:Landroid/widget/LinearLayout;

.field protected mAltPinGuide:Landroid/widget/TextView;

.field protected mBackupGuide:Ljava/lang/String;

.field protected mBackupInput:Ljava/lang/String;

.field protected mBackupType:I

.field protected mCancelButton:Landroid/widget/Button;

.field private mClearPatternRunnable:Ljava/lang/Runnable;

.field protected mDoneButton:Landroid/widget/Button;

.field private mEvent:Lcom/samsung/android/fingerprint/FingerprintEvent;

.field private mHandler:Landroid/os/Handler;

.field private mIsPendingEvent:Z

.field private mIsdismissTouchOutside:Z

.field private mNumWrongAttempts:I

.field private mPasswordTextRunnable:Ljava/lang/Runnable;

.field private mSIPRunnable:Ljava/lang/Runnable;

.field private orientationListener:Landroid/view/OrientationEventListener;

.field private orientationScreen:I

.field private preOrientationScreen:I


# direct methods
.method public constructor <init>(Lcom/samsung/android/fingerprint/service/FingerprintManagerService;Landroid/content/Context;)V
    .locals 3
    .param p1, "service"    # Lcom/samsung/android/fingerprint/service/FingerprintManagerService;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    const/4 v1, -0x1

    .line 138
    invoke-direct {p0, p2, p1}, Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog;-><init>(Landroid/content/Context;Lcom/samsung/android/fingerprint/service/FingerprintManagerService;)V

    .line 70
    iput v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mBackupType:I

    .line 97
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mIsPendingEvent:Z

    .line 98
    iput-boolean v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mIsdismissTouchOutside:Z

    .line 102
    iput v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->preOrientationScreen:I

    .line 103
    iput v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->orientationScreen:I

    .line 479
    new-instance v0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog$4;

    invoke-direct {v0, p0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog$4;-><init>(Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;)V

    iput-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPatternListener:Lcom/android/internal/widget/LockPatternView$OnPatternListener;

    .line 504
    new-instance v0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog$5;

    invoke-direct {v0, p0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog$5;-><init>(Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;)V

    iput-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mClearPatternRunnable:Ljava/lang/Runnable;

    .line 139
    new-instance v0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog$H;

    invoke-virtual {p2}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog$H;-><init>(Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mHandler:Landroid/os/Handler;

    .line 140
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;)Lcom/samsung/android/fingerprint/FingerprintEvent;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mEvent:Lcom/samsung/android/fingerprint/FingerprintEvent;

    return-object v0
.end method

.method static synthetic access$102(Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;
    .param p1, "x1"    # Z

    .prologue
    .line 58
    iput-boolean p1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mIsPendingEvent:Z

    return p1
.end method

.method static synthetic access$200(Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->onConfigurationChanged()V

    return-void
.end method

.method static synthetic access$300(Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mClearPatternRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$400(Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;Ljava/util/List;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 58
    invoke-direct {p0, p1}, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->verifyPattern(Ljava/util/List;)V

    return-void
.end method

.method static synthetic access$500(Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;I)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;
    .param p1, "x1"    # I

    .prologue
    .line 58
    invoke-direct {p0, p1}, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->setLockoutString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$600(Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPatternGuide:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$700(Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->verifyPassword()V

    return-void
.end method

.method static synthetic access$800(Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->verifyPin()V

    return-void
.end method

.method private backupAndRestoreData(Ljava/lang/String;)V
    .locals 2
    .param p1, "type"    # Ljava/lang/String;

    .prologue
    .line 167
    const-string v0, "bBackup"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 168
    iget v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mBackupType:I

    packed-switch v0, :pswitch_data_0

    .line 225
    :cond_0
    :goto_0
    return-void

    .line 170
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPassword:Landroid/widget/EditText;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPassword:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 171
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPassword:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mBackupInput:Ljava/lang/String;

    .line 173
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPasswordGuide:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPasswordGuide:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 174
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPasswordGuide:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mBackupGuide:Ljava/lang/String;

    goto :goto_0

    .line 178
    :pswitch_1
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPin:Landroid/widget/EditText;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPin:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 179
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPin:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mBackupInput:Ljava/lang/String;

    .line 181
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPinGuide:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPinGuide:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 182
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPinGuide:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mBackupGuide:Ljava/lang/String;

    goto :goto_0

    .line 186
    :pswitch_2
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPatternGuide:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPatternGuide:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 187
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPatternGuide:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mBackupGuide:Ljava/lang/String;

    goto :goto_0

    .line 191
    :cond_3
    const-string v0, "bRestore"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 192
    iget v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mBackupType:I

    packed-switch v0, :pswitch_data_1

    goto/16 :goto_0

    .line 194
    :pswitch_3
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPassword:Landroid/widget/EditText;

    if-eqz v0, :cond_4

    .line 195
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPassword:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mBackupInput:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 197
    :cond_4
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPasswordGuide:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 198
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mBackupGuide:Ljava/lang/String;

    if-nez v0, :cond_5

    .line 199
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f06001a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mBackupGuide:Ljava/lang/String;

    .line 201
    :cond_5
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPasswordGuide:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mBackupGuide:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 205
    :pswitch_4
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPin:Landroid/widget/EditText;

    if-eqz v0, :cond_6

    .line 206
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPin:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mBackupInput:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 208
    :cond_6
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPinGuide:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 209
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mBackupGuide:Ljava/lang/String;

    if-nez v0, :cond_7

    .line 210
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f06001d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mBackupGuide:Ljava/lang/String;

    .line 212
    :cond_7
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPinGuide:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mBackupGuide:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 216
    :pswitch_5
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPatternGuide:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 217
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mBackupGuide:Ljava/lang/String;

    if-nez v0, :cond_8

    .line 218
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f060020

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mBackupGuide:Ljava/lang/String;

    .line 220
    :cond_8
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPatternGuide:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mBackupGuide:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 168
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    .line 192
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private checkBackupValue(Ljava/lang/String;)Z
    .locals 4
    .param p1, "backupValue"    # Ljava/lang/String;

    .prologue
    .line 749
    const/4 v0, 0x0

    .line 750
    .local v0, "ret":Z
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mFpmsService:Lcom/samsung/android/fingerprint/service/FingerprintManagerService;

    if-eqz v1, :cond_0

    .line 751
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mFpmsService:Lcom/samsung/android/fingerprint/service/FingerprintManagerService;

    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mToken:Landroid/os/IBinder;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p1, v3}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->verifyPassword(Landroid/os/IBinder;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    .line 753
    :cond_0
    const-string v1, "IdentifyBackupDialog"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "backupValue : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", ret : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 754
    return v0
.end method

.method private declared-synchronized cleanUpLayoutResources()V
    .locals 2

    .prologue
    .line 285
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPasswordBox:Landroid/widget/LinearLayout;

    .line 286
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPatternBox:Landroid/widget/RelativeLayout;

    .line 287
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPassword:Landroid/widget/EditText;

    if-eqz v0, :cond_0

    .line 288
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPassword:Landroid/widget/EditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 289
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPassword:Landroid/widget/EditText;

    .line 291
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPasswordGuide:Landroid/widget/TextView;

    .line 293
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPin:Landroid/widget/EditText;

    if-eqz v0, :cond_1

    .line 294
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPin:Landroid/widget/EditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 295
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPin:Landroid/widget/EditText;

    .line 297
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPinGuide:Landroid/widget/TextView;

    .line 299
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPatternView:Lcom/android/internal/widget/LockPatternView;

    if-eqz v0, :cond_2

    .line 300
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPatternView:Lcom/android/internal/widget/LockPatternView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/internal/widget/LockPatternView;->setOnPatternListener(Lcom/android/internal/widget/LockPatternView$OnPatternListener;)V

    .line 301
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPatternView:Lcom/android/internal/widget/LockPatternView;

    .line 303
    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPatternGuide:Landroid/widget/TextView;

    .line 305
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_3

    .line 306
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 307
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mHandler:Landroid/os/Handler;

    .line 309
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->orientationListener:Landroid/view/OrientationEventListener;

    if-eqz v0, :cond_4

    .line 310
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->orientationListener:Landroid/view/OrientationEventListener;

    invoke-virtual {v0}, Landroid/view/OrientationEventListener;->disable()V

    .line 311
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->orientationListener:Landroid/view/OrientationEventListener;

    .line 313
    :cond_4
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mCountdownTimer:Landroid/os/CountDownTimer;

    if-eqz v0, :cond_5

    .line 314
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mCountdownTimer:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->cancel()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 316
    :cond_5
    monitor-exit p0

    return-void

    .line 285
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private handleAttemptLockout()V
    .locals 6

    .prologue
    .line 512
    sget-object v0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog$Stage;->LockedOut:Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog$Stage;

    invoke-direct {p0, v0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->updateStage(Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog$Stage;)V

    .line 514
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mCountdownTimer:Landroid/os/CountDownTimer;

    if-eqz v0, :cond_0

    .line 515
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mCountdownTimer:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->cancel()V

    .line 516
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mCountdownTimer:Landroid/os/CountDownTimer;

    .line 518
    :cond_0
    const-string v0, "IdentifyBackupDialog"

    const-string v1, "CountdownTimer: start"

    invoke-static {v0, v1}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 519
    new-instance v0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog$6;

    invoke-static {}, Lcom/samsung/android/fingerprint/service/util/Config;->getRemaingTimeToUnlock()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog$6;-><init>(Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;JJ)V

    invoke-virtual {v0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog$6;->start()Landroid/os/CountDownTimer;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mCountdownTimer:Landroid/os/CountDownTimer;

    .line 548
    return-void
.end method

.method public static isTablet(Landroid/content/Context;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 743
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit8 v0, v0, 0xf

    const/4 v1, 0x3

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private onConfigurationChanged()V
    .locals 2

    .prologue
    .line 228
    invoke-virtual {p0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->orientationScreen:I

    .line 229
    iget v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->preOrientationScreen:I

    iget v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->orientationScreen:I

    if-ne v0, v1, :cond_0

    .line 245
    :goto_0
    return-void

    .line 232
    :cond_0
    const-string v0, "bBackup"

    invoke-direct {p0, v0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->backupAndRestoreData(Ljava/lang/String;)V

    .line 233
    iget v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->orientationScreen:I

    iput v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->preOrientationScreen:I

    .line 234
    iget v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->orientationScreen:I

    packed-switch v0, :pswitch_data_0

    .line 244
    :goto_1
    invoke-virtual {p0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->updateLayout()V

    goto :goto_0

    .line 237
    :pswitch_0
    const v0, 0x7f030001

    invoke-virtual {p0, v0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->setContentView(I)V

    goto :goto_1

    .line 241
    :pswitch_1
    const/high16 v0, 0x7f030000

    invoke-virtual {p0, v0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->setContentView(I)V

    goto :goto_1

    .line 234
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private postClearPatternRunnable()V
    .locals 4

    .prologue
    .line 499
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPatternView:Lcom/android/internal/widget/LockPatternView;

    if-eqz v0, :cond_0

    .line 500
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPatternView:Lcom/android/internal/widget/LockPatternView;

    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mClearPatternRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/android/internal/widget/LockPatternView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 501
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPatternView:Lcom/android/internal/widget/LockPatternView;

    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mClearPatternRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/internal/widget/LockPatternView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 503
    :cond_0
    return-void
.end method

.method private setLockoutString(I)Ljava/lang/String;
    .locals 7
    .param p1, "secondsCountdown"    # I

    .prologue
    const/4 v5, 0x1

    .line 551
    const/4 v1, 0x0

    .line 552
    .local v1, "string2":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mContext:Landroid/content/Context;

    const v4, 0x7f060015

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "line.separator"

    invoke-static {v3}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 553
    .local v0, "string1":Ljava/lang/String;
    if-ne p1, v5, :cond_0

    .line 554
    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mContext:Landroid/content/Context;

    const v3, 0x7f060014

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 558
    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 556
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mContext:Landroid/content/Context;

    const v3, 0x7f060013

    new-array v4, v5, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method private updateStage(Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog$Stage;)V
    .locals 8
    .param p1, "stage"    # Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog$Stage;

    .prologue
    const v7, 0x66f5f5f5

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 562
    sget-object v1, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog$16;->$SwitchMap$com$samsung$android$fingerprint$service$ui$IdentifyBaseDialog$Stage:[I

    invoke-virtual {p1}, Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog$Stage;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 625
    :cond_0
    :goto_0
    return-void

    .line 564
    :pswitch_0
    iget v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mBackupType:I

    if-ne v1, v5, :cond_0

    .line 565
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPatternView:Lcom/android/internal/widget/LockPatternView;

    if-eqz v1, :cond_0

    .line 566
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPatternView:Lcom/android/internal/widget/LockPatternView;

    sget-object v2, Lcom/android/internal/widget/LockPatternView$DisplayMode;->Wrong:Lcom/android/internal/widget/LockPatternView$DisplayMode;

    invoke-virtual {v1, v2}, Lcom/android/internal/widget/LockPatternView;->setDisplayMode(Lcom/android/internal/widget/LockPatternView$DisplayMode;)V

    .line 567
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPatternView:Lcom/android/internal/widget/LockPatternView;

    invoke-virtual {v1, v4}, Lcom/android/internal/widget/LockPatternView;->setEnabled(Z)V

    .line 568
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPatternView:Lcom/android/internal/widget/LockPatternView;

    invoke-virtual {v1}, Lcom/android/internal/widget/LockPatternView;->enableInput()V

    goto :goto_0

    .line 573
    :pswitch_1
    const-string v1, "IdentifyBackupDialog"

    const-string v2, "stage is LockedOut"

    invoke-static {v1, v2}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 574
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mContext:Landroid/content/Context;

    const-string v2, "input_method"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 576
    .local v0, "imm":Landroid/view/inputmethod/InputMethodManager;
    iget v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mBackupType:I

    if-ne v1, v4, :cond_4

    .line 577
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPassword:Landroid/widget/EditText;

    if-eqz v1, :cond_2

    .line 578
    if-eqz v0, :cond_1

    .line 579
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPassword:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    invoke-virtual {v0, v1, v3}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 581
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPassword:Landroid/widget/EditText;

    invoke-virtual {v1, v6}, Landroid/widget/EditText;->setVisibility(I)V

    .line 583
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mDoneButton:Landroid/widget/Button;

    if-eqz v1, :cond_3

    .line 584
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mDoneButton:Landroid/widget/Button;

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 585
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mDoneButton:Landroid/widget/Button;

    invoke-virtual {v1, v7}, Landroid/widget/Button;->setTextColor(I)V

    .line 587
    :cond_3
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mCancelButton:Landroid/widget/Button;

    if-eqz v1, :cond_0

    .line 588
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mCancelButton:Landroid/widget/Button;

    new-instance v2, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog$7;

    invoke-direct {v2, p0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog$7;-><init>(Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 596
    :cond_4
    iget v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mBackupType:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_8

    .line 597
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPin:Landroid/widget/EditText;

    if-eqz v1, :cond_6

    .line 598
    if-eqz v0, :cond_5

    .line 599
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPin:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    invoke-virtual {v0, v1, v3}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 601
    :cond_5
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPin:Landroid/widget/EditText;

    invoke-virtual {v1, v6}, Landroid/widget/EditText;->setVisibility(I)V

    .line 603
    :cond_6
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mDoneButton:Landroid/widget/Button;

    if-eqz v1, :cond_7

    .line 604
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mDoneButton:Landroid/widget/Button;

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 605
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mDoneButton:Landroid/widget/Button;

    invoke-virtual {v1, v7}, Landroid/widget/Button;->setTextColor(I)V

    .line 607
    :cond_7
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mCancelButton:Landroid/widget/Button;

    if-eqz v1, :cond_0

    .line 608
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mCancelButton:Landroid/widget/Button;

    new-instance v2, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog$8;

    invoke-direct {v2, p0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog$8;-><init>(Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0

    .line 616
    :cond_8
    iget v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mBackupType:I

    if-ne v1, v5, :cond_0

    .line 617
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPatternView:Lcom/android/internal/widget/LockPatternView;

    if-eqz v1, :cond_0

    .line 618
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPatternView:Lcom/android/internal/widget/LockPatternView;

    invoke-virtual {v1, v3}, Lcom/android/internal/widget/LockPatternView;->setEnabled(Z)V

    goto/16 :goto_0

    .line 562
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private verifyPassword()V
    .locals 6

    .prologue
    const/16 v4, 0x291c

    const/16 v3, 0xd

    .line 758
    const-string v0, ""

    .line 759
    .local v0, "password":Ljava/lang/String;
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPassword:Landroid/widget/EditText;

    if-eqz v1, :cond_0

    .line 760
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPassword:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 762
    :cond_0
    invoke-direct {p0, v0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->checkBackupValue(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 763
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mEvent:Lcom/samsung/android/fingerprint/FingerprintEvent;

    iput v3, v1, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventId:I

    .line 764
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mEvent:Lcom/samsung/android/fingerprint/FingerprintEvent;

    const/4 v2, 0x0

    iput v2, v1, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventResult:I

    .line 765
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mEvent:Lcom/samsung/android/fingerprint/FingerprintEvent;

    const/16 v2, 0x64

    iput v2, v1, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventStatus:I

    .line 766
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 767
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 800
    :goto_0
    return-void

    .line 769
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPassword:Landroid/widget/EditText;

    if-eqz v1, :cond_2

    .line 770
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPassword:Landroid/widget/EditText;

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 772
    :cond_2
    invoke-static {}, Lcom/samsung/android/fingerprint/service/util/Config;->reportFailedFingerprintAttempts()V

    .line 773
    invoke-static {}, Lcom/samsung/android/fingerprint/service/util/Config;->getFailedFingerprintAttempts()I

    move-result v1

    if-nez v1, :cond_4

    .line 774
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mPasswordTextRunnable:Ljava/lang/Runnable;

    if-eqz v1, :cond_3

    .line 775
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mPasswordTextRunnable:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 777
    :cond_3
    invoke-static {}, Lcom/samsung/android/fingerprint/service/util/Config;->setRemaingTimeToUnlock()V

    .line 778
    invoke-direct {p0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->handleAttemptLockout()V

    .line 779
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mEvent:Lcom/samsung/android/fingerprint/FingerprintEvent;

    iput v3, v1, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventId:I

    .line 780
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mEvent:Lcom/samsung/android/fingerprint/FingerprintEvent;

    const/4 v2, -0x1

    iput v2, v1, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventResult:I

    .line 781
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mEvent:Lcom/samsung/android/fingerprint/FingerprintEvent;

    const/16 v2, 0xb

    iput v2, v1, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventStatus:I

    goto :goto_0

    .line 784
    :cond_4
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPasswordGuide:Landroid/widget/TextView;

    if-eqz v1, :cond_5

    .line 785
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPasswordGuide:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f06001b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 787
    :cond_5
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mPasswordTextRunnable:Ljava/lang/Runnable;

    if-eqz v1, :cond_6

    .line 788
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mPasswordTextRunnable:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 790
    :cond_6
    new-instance v1, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog$14;

    invoke-direct {v1, p0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog$14;-><init>(Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;)V

    iput-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mPasswordTextRunnable:Ljava/lang/Runnable;

    .line 797
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mPasswordTextRunnable:Ljava/lang/Runnable;

    const-wide/16 v4, 0x1388

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method private verifyPattern(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/android/internal/widget/LockPatternView$Cell;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 849
    .local p1, "pattern":Ljava/util/List;, "Ljava/util/List<Lcom/android/internal/widget/LockPatternView$Cell;>;"
    invoke-static {p1}, Lcom/android/internal/widget/LockPatternUtils;->patternToString(Ljava/util/List;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->checkBackupValue(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 851
    :try_start_0
    const-string v1, "IdentifyBackupDialog"

    const-string v2, "pattern verification is successful"

    invoke-static {v1, v2}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 852
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mEvent:Lcom/samsung/android/fingerprint/FingerprintEvent;

    const/16 v2, 0xd

    iput v2, v1, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventId:I

    .line 853
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mEvent:Lcom/samsung/android/fingerprint/FingerprintEvent;

    const/4 v2, 0x0

    iput v2, v1, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventResult:I

    .line 854
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mEvent:Lcom/samsung/android/fingerprint/FingerprintEvent;

    const/16 v2, 0x64

    iput v2, v1, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventStatus:I

    .line 855
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mHandler:Landroid/os/Handler;

    const/16 v2, 0x291c

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 856
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mHandler:Landroid/os/Handler;

    const/16 v2, 0x291c

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 879
    :goto_0
    return-void

    .line 858
    :catch_0
    move-exception v0

    .line 859
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "IdentifyBackupDialog"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Exception, mode change is fail"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 863
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    invoke-static {}, Lcom/samsung/android/fingerprint/service/util/Config;->reportFailedFingerprintAttempts()V

    .line 864
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x4

    if-lt v1, v2, :cond_1

    invoke-static {}, Lcom/samsung/android/fingerprint/service/util/Config;->getFailedFingerprintAttempts()I

    move-result v1

    if-nez v1, :cond_1

    .line 866
    const-string v1, "IdentifyBackupDialog"

    const-string v2, "pattern count reach Maximum. Screen is Locked out"

    invoke-static {v1, v2}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 867
    invoke-static {}, Lcom/samsung/android/fingerprint/service/util/Config;->setRemaingTimeToUnlock()V

    .line 868
    invoke-direct {p0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->handleAttemptLockout()V

    goto :goto_0

    .line 871
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPatternGuide:Landroid/widget/TextView;

    if-eqz v1, :cond_2

    .line 872
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPatternGuide:Landroid/widget/TextView;

    const v2, 0x7f060021

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 874
    :cond_2
    const-string v1, "IdentifyBackupDialog"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "pattern is wrong. try count = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mNumWrongAttempts:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 875
    sget-object v1, Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog$Stage;->UnlockWrong:Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog$Stage;

    invoke-direct {p0, v1}, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->updateStage(Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog$Stage;)V

    .line 876
    invoke-direct {p0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->postClearPatternRunnable()V

    goto :goto_0
.end method

.method private verifyPin()V
    .locals 6

    .prologue
    const/16 v5, 0x291c

    const/16 v4, 0xd

    .line 803
    const-string v1, "IdentifyBackupDialog"

    const-string v2, "verifyPin() called"

    invoke-static {v1, v2}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 805
    const-string v0, ""

    .line 806
    .local v0, "pin":Ljava/lang/String;
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPin:Landroid/widget/EditText;

    if-eqz v1, :cond_0

    .line 807
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPin:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 809
    :cond_0
    const-string v1, "IdentifyBackupDialog"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "pin : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 811
    invoke-direct {p0, v0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->checkBackupValue(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 812
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mEvent:Lcom/samsung/android/fingerprint/FingerprintEvent;

    iput v4, v1, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventId:I

    .line 813
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mEvent:Lcom/samsung/android/fingerprint/FingerprintEvent;

    const/4 v2, 0x0

    iput v2, v1, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventResult:I

    .line 814
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mEvent:Lcom/samsung/android/fingerprint/FingerprintEvent;

    const/16 v2, 0x64

    iput v2, v1, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventStatus:I

    .line 815
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v5}, Landroid/os/Handler;->removeMessages(I)V

    .line 816
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v5}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 846
    :goto_0
    return-void

    .line 818
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPin:Landroid/widget/EditText;

    if-eqz v1, :cond_2

    .line 819
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPin:Landroid/widget/EditText;

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 821
    :cond_2
    invoke-static {}, Lcom/samsung/android/fingerprint/service/util/Config;->reportFailedFingerprintAttempts()V

    .line 822
    invoke-static {}, Lcom/samsung/android/fingerprint/service/util/Config;->getFailedFingerprintAttempts()I

    move-result v1

    if-nez v1, :cond_3

    .line 823
    invoke-static {}, Lcom/samsung/android/fingerprint/service/util/Config;->setRemaingTimeToUnlock()V

    .line 824
    invoke-direct {p0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->handleAttemptLockout()V

    .line 825
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mEvent:Lcom/samsung/android/fingerprint/FingerprintEvent;

    iput v4, v1, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventId:I

    .line 826
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mEvent:Lcom/samsung/android/fingerprint/FingerprintEvent;

    const/4 v2, -0x1

    iput v2, v1, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventResult:I

    .line 827
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mEvent:Lcom/samsung/android/fingerprint/FingerprintEvent;

    const/16 v2, 0xb

    iput v2, v1, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventStatus:I

    goto :goto_0

    .line 830
    :cond_3
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPinGuide:Landroid/widget/TextView;

    if-eqz v1, :cond_4

    .line 831
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPinGuide:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f06001e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 833
    :cond_4
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mPasswordTextRunnable:Ljava/lang/Runnable;

    if-eqz v1, :cond_5

    .line 834
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mPasswordTextRunnable:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 836
    :cond_5
    new-instance v1, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog$15;

    invoke-direct {v1, p0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog$15;-><init>(Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;)V

    iput-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mPasswordTextRunnable:Ljava/lang/Runnable;

    .line 843
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mPasswordTextRunnable:Ljava/lang/Runnable;

    const-wide/16 v4, 0x1388

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method


# virtual methods
.method public dismiss()V
    .locals 4

    .prologue
    .line 259
    invoke-direct {p0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->cleanUpLayoutResources()V

    .line 262
    :try_start_0
    iget-boolean v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mIsdismissTouchOutside:Z

    const/4 v3, 0x1

    if-ne v2, v3, :cond_2

    .line 263
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mIsdismissTouchOutside:Z

    .line 264
    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mEvent:Lcom/samsung/android/fingerprint/FingerprintEvent;

    const/16 v3, 0xd

    iput v3, v2, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventId:I

    .line 265
    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mEvent:Lcom/samsung/android/fingerprint/FingerprintEvent;

    const/4 v3, -0x1

    iput v3, v2, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventResult:I

    .line 266
    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mEvent:Lcom/samsung/android/fingerprint/FingerprintEvent;

    const/16 v3, 0xd

    iput v3, v2, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventStatus:I

    .line 267
    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mEvent:Lcom/samsung/android/fingerprint/FingerprintEvent;

    invoke-virtual {p0, v2}, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->deliverEventToClient(Lcom/samsung/android/fingerprint/FingerprintEvent;)V

    .line 275
    :cond_0
    :goto_0
    invoke-super {p0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog;->dismiss()V

    .line 282
    :cond_1
    :goto_1
    return-void

    .line 268
    :cond_2
    iget-boolean v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mIsPendingEvent:Z

    if-eqz v2, :cond_0

    .line 269
    new-instance v1, Lcom/samsung/android/fingerprint/FingerprintEvent;

    invoke-direct {v1}, Lcom/samsung/android/fingerprint/FingerprintEvent;-><init>()V

    .line 270
    .local v1, "event":Lcom/samsung/android/fingerprint/FingerprintEvent;
    const/16 v2, 0xd

    iput v2, v1, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventId:I

    .line 271
    const/4 v2, -0x1

    iput v2, v1, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventResult:I

    .line 272
    const/16 v2, 0x8

    iput v2, v1, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventStatus:I

    .line 273
    invoke-virtual {p0, v1}, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->deliverEventToClient(Lcom/samsung/android/fingerprint/FingerprintEvent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 276
    .end local v1    # "event":Lcom/samsung/android/fingerprint/FingerprintEvent;
    :catch_0
    move-exception v0

    .line 277
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "IdentifyBackupDialog"

    const-string v3, "dismiss() : already dismissed"

    invoke-static {v2, v3}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 278
    if-eqz v0, :cond_1

    .line 279
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method protected enableButtonIfReady()V
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 628
    const/4 v0, 0x0

    .line 629
    .local v0, "isReadyPassword":Z
    iget-object v3, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mDoneButton:Landroid/widget/Button;

    if-eqz v3, :cond_1

    .line 630
    iget v3, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mBackupType:I

    if-ne v3, v1, :cond_3

    .line 631
    iget-object v3, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPassword:Landroid/widget/EditText;

    if-eqz v3, :cond_0

    .line 632
    iget-object v3, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPassword:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-lt v3, v1, :cond_2

    move v0, v1

    .line 639
    :cond_0
    :goto_0
    if-eqz v0, :cond_5

    .line 640
    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mDoneButton:Landroid/widget/Button;

    invoke-virtual {v2, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 641
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mDoneButton:Landroid/widget/Button;

    const/high16 v2, -0x1000000

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setTextColor(I)V

    .line 647
    :cond_1
    :goto_1
    return-void

    :cond_2
    move v0, v2

    .line 632
    goto :goto_0

    .line 634
    :cond_3
    iget v3, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mBackupType:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_0

    .line 635
    iget-object v3, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPin:Landroid/widget/EditText;

    if-eqz v3, :cond_0

    .line 636
    iget-object v3, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPin:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-lt v3, v1, :cond_4

    move v0, v1

    :goto_2
    goto :goto_0

    :cond_4
    move v0, v2

    goto :goto_2

    .line 643
    :cond_5
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mDoneButton:Landroid/widget/Button;

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 644
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mDoneButton:Landroid/widget/Button;

    const v2, 0x66f5f5f5

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setTextColor(I)V

    goto :goto_1
.end method

.method protected initLayout()V
    .locals 2

    .prologue
    .line 329
    const-string v0, "IdentifyBackupDialog"

    const-string v1, "IdentifyBackupDialog initLayout"

    invoke-static {v0, v1}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 331
    const v0, 0x7f070001

    invoke-virtual {p0, v0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPasswordBox:Landroid/widget/LinearLayout;

    .line 332
    const v0, 0x7f070003

    invoke-virtual {p0, v0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPassword:Landroid/widget/EditText;

    .line 333
    const v0, 0x7f070002

    invoke-virtual {p0, v0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPasswordGuide:Landroid/widget/TextView;

    .line 334
    const v0, 0x7f070004

    invoke-virtual {p0, v0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mCancelButton:Landroid/widget/Button;

    .line 335
    const v0, 0x7f070005

    invoke-virtual {p0, v0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mDoneButton:Landroid/widget/Button;

    .line 338
    const v0, 0x7f070006

    invoke-virtual {p0, v0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPinBox:Landroid/widget/LinearLayout;

    .line 339
    const v0, 0x7f070008

    invoke-virtual {p0, v0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPin:Landroid/widget/EditText;

    .line 340
    const v0, 0x7f070007

    invoke-virtual {p0, v0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPinGuide:Landroid/widget/TextView;

    .line 343
    const v0, 0x7f070009

    invoke-virtual {p0, v0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPatternBox:Landroid/widget/RelativeLayout;

    .line 344
    const v0, 0x7f07000b

    invoke-virtual {p0, v0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/internal/widget/LockPatternView;

    iput-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPatternView:Lcom/android/internal/widget/LockPatternView;

    .line 345
    const v0, 0x7f07000a

    invoke-virtual {p0, v0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPatternGuide:Landroid/widget/TextView;

    .line 347
    const-string v0, "bRestore"

    invoke-direct {p0, v0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->backupAndRestoreData(Ljava/lang/String;)V

    .line 348
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v2, 0x1

    .line 145
    new-instance v0, Lcom/samsung/android/fingerprint/FingerprintEvent;

    invoke-direct {v0}, Lcom/samsung/android/fingerprint/FingerprintEvent;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mEvent:Lcom/samsung/android/fingerprint/FingerprintEvent;

    .line 146
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mEvent:Lcom/samsung/android/fingerprint/FingerprintEvent;

    const/16 v1, 0xd

    iput v1, v0, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventId:I

    .line 147
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mEvent:Lcom/samsung/android/fingerprint/FingerprintEvent;

    const/4 v1, -0x1

    iput v1, v0, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventResult:I

    .line 148
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mEvent:Lcom/samsung/android/fingerprint/FingerprintEvent;

    const/16 v1, 0x8

    iput v1, v0, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventStatus:I

    .line 150
    new-instance v0, Lcom/android/internal/widget/LockPatternUtils;

    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/android/internal/widget/LockPatternUtils;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    .line 151
    iput v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mBackupType:I

    .line 152
    iget v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mBackupType:I

    const/4 v1, 0x3

    if-le v0, v1, :cond_0

    iget v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mBackupType:I

    if-ge v0, v2, :cond_0

    .line 153
    iput v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mBackupType:I

    .line 155
    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->onConfigurationChanged()V

    .line 156
    new-instance v0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog$1;

    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mContext:Landroid/content/Context;

    invoke-direct {v0, p0, v1}, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog$1;-><init>(Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->orientationListener:Landroid/view/OrientationEventListener;

    .line 162
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->orientationListener:Landroid/view/OrientationEventListener;

    invoke-virtual {v0}, Landroid/view/OrientationEventListener;->enable()V

    .line 163
    return-void
.end method

.method public onFingerprintEvent(Lcom/samsung/android/fingerprint/FingerprintEvent;)V
    .locals 0
    .param p1, "event"    # Lcom/samsung/android/fingerprint/FingerprintEvent;

    .prologue
    .line 135
    return-void
.end method

.method protected onStart()V
    .locals 0

    .prologue
    .line 249
    invoke-super {p0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog;->onStart()V

    .line 250
    return-void
.end method

.method protected onStop()V
    .locals 0

    .prologue
    .line 254
    invoke-super {p0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog;->onStop()V

    .line 255
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 3
    .param p1, "hasFocus"    # Z

    .prologue
    .line 320
    invoke-super {p0, p1}, Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog;->onWindowFocusChanged(Z)V

    .line 321
    const-string v0, "IdentifyBackupDialog"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Backup onWindowFocusChanged, hasFocus : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 323
    if-nez p1, :cond_0

    .line 324
    invoke-virtual {p0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->dismiss()V

    .line 326
    :cond_0
    return-void
.end method

.method protected setEditBoxListener()V
    .locals 4

    .prologue
    .line 650
    const/4 v0, 0x0

    .line 652
    .local v0, "commonEdit":Landroid/widget/EditText;
    iget v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mBackupType:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_5

    .line 653
    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPassword:Landroid/widget/EditText;

    if-eqz v2, :cond_0

    .line 654
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPassword:Landroid/widget/EditText;

    .line 662
    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    .line 663
    new-instance v2, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog$9;

    invoke-direct {v2, p0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog$9;-><init>(Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;)V

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 677
    new-instance v2, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog$10;

    invoke-direct {v2, p0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog$10;-><init>(Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;)V

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 694
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->enableButtonIfReady()V

    .line 696
    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->isTablet(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 697
    const/high16 v2, 0x7f070000

    invoke-virtual {p0, v2}, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 698
    .local v1, "rootLayout":Landroid/view/View;
    invoke-virtual {v1}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v2

    new-instance v3, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog$11;

    invoke-direct {v3, p0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog$11;-><init>(Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;)V

    invoke-virtual {v2, v3}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 719
    .end local v1    # "rootLayout":Landroid/view/View;
    :cond_2
    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mCancelButton:Landroid/widget/Button;

    if-eqz v2, :cond_3

    .line 720
    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mCancelButton:Landroid/widget/Button;

    new-instance v3, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog$12;

    invoke-direct {v3, p0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog$12;-><init>(Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;)V

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 728
    :cond_3
    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mDoneButton:Landroid/widget/Button;

    if-eqz v2, :cond_4

    .line 729
    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mDoneButton:Landroid/widget/Button;

    new-instance v3, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog$13;

    invoke-direct {v3, p0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog$13;-><init>(Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;)V

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 740
    :cond_4
    return-void

    .line 656
    :cond_5
    iget v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mBackupType:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    .line 657
    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPin:Landroid/widget/EditText;

    if-eqz v2, :cond_0

    .line 658
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPin:Landroid/widget/EditText;

    goto :goto_0
.end method

.method protected setPasswordLayout()V
    .locals 4

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x0

    .line 370
    invoke-virtual {p0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->setEditBoxListener()V

    .line 371
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPassword:Landroid/widget/EditText;

    if-eqz v0, :cond_0

    .line 372
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPassword:Landroid/widget/EditText;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setVisibility(I)V

    .line 373
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPassword:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPassword:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    .line 374
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPassword:Landroid/widget/EditText;

    const/16 v1, 0x81

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setInputType(I)V

    .line 376
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPasswordBox:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_1

    .line 377
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPasswordBox:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 379
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPinBox:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_2

    .line 380
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPinBox:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 382
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPatternBox:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_3

    .line 383
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPatternBox:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 385
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPasswordGuide:Landroid/widget/TextView;

    if-eqz v0, :cond_4

    .line 386
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPasswordGuide:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f06001a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 389
    :cond_4
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mCancelButton:Landroid/widget/Button;

    if-eqz v0, :cond_6

    .line 390
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPasswordBox:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_5

    .line 391
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mCancelButton:Landroid/widget/Button;

    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPasswordBox:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getWidth()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setWidth(I)V

    .line 393
    :cond_5
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mCancelButton:Landroid/widget/Button;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 394
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mCancelButton:Landroid/widget/Button;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(I)V

    .line 397
    :cond_6
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mSIPRunnable:Ljava/lang/Runnable;

    if-eqz v0, :cond_7

    .line 398
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mSIPRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 400
    :cond_7
    new-instance v0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog$2;

    invoke-direct {v0, p0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog$2;-><init>(Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;)V

    iput-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mSIPRunnable:Ljava/lang/Runnable;

    .line 411
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mSIPRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 412
    return-void
.end method

.method protected setPatternLayout()V
    .locals 3

    .prologue
    const/4 v2, 0x4

    .line 459
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPatternView:Lcom/android/internal/widget/LockPatternView;

    if-eqz v0, :cond_0

    .line 460
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPatternView:Lcom/android/internal/widget/LockPatternView;

    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    invoke-virtual {v1}, Lcom/android/internal/widget/LockPatternUtils;->isTactileFeedbackEnabled()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/internal/widget/LockPatternView;->setTactileFeedbackEnabled(Z)V

    .line 461
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPatternView:Lcom/android/internal/widget/LockPatternView;

    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPatternListener:Lcom/android/internal/widget/LockPatternView$OnPatternListener;

    invoke-virtual {v0, v1}, Lcom/android/internal/widget/LockPatternView;->setOnPatternListener(Lcom/android/internal/widget/LockPatternView$OnPatternListener;)V

    .line 463
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPasswordBox:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_1

    .line 464
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPasswordBox:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 466
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPinBox:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_2

    .line 467
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPinBox:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 469
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPatternBox:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_3

    .line 470
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPatternBox:Landroid/widget/RelativeLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 472
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPatternView:Lcom/android/internal/widget/LockPatternView;

    if-eqz v0, :cond_4

    .line 473
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPatternView:Lcom/android/internal/widget/LockPatternView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/internal/widget/LockPatternView;->setEnabled(Z)V

    .line 474
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPatternView:Lcom/android/internal/widget/LockPatternView;

    invoke-virtual {v0}, Lcom/android/internal/widget/LockPatternView;->enableInput()V

    .line 475
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPatternView:Lcom/android/internal/widget/LockPatternView;

    invoke-virtual {v0}, Lcom/android/internal/widget/LockPatternView;->clearPattern()V

    .line 477
    :cond_4
    return-void
.end method

.method protected setPinLayout()V
    .locals 4

    .prologue
    const/4 v2, 0x4

    .line 415
    invoke-virtual {p0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->setEditBoxListener()V

    .line 416
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPin:Landroid/widget/EditText;

    if-eqz v0, :cond_0

    .line 417
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPin:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPin:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    .line 418
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPin:Landroid/widget/EditText;

    const/16 v1, 0x12

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setInputType(I)V

    .line 420
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPinBox:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_1

    .line 421
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPinBox:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 423
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPasswordBox:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_2

    .line 424
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPasswordBox:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 426
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPatternBox:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_3

    .line 427
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPatternBox:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 429
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPinGuide:Landroid/widget/TextView;

    if-eqz v0, :cond_4

    .line 430
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPinGuide:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f06001d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 433
    :cond_4
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mCancelButton:Landroid/widget/Button;

    if-eqz v0, :cond_6

    .line 434
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPasswordBox:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_5

    .line 435
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mCancelButton:Landroid/widget/Button;

    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPasswordBox:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getWidth()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setWidth(I)V

    .line 437
    :cond_5
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mCancelButton:Landroid/widget/Button;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 438
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mCancelButton:Landroid/widget/Button;

    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f060012

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 439
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mCancelButton:Landroid/widget/Button;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(I)V

    .line 441
    :cond_6
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mSIPRunnable:Ljava/lang/Runnable;

    if-eqz v0, :cond_7

    .line 442
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mSIPRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 444
    :cond_7
    new-instance v0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog$3;

    invoke-direct {v0, p0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog$3;-><init>(Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;)V

    iput-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mSIPRunnable:Ljava/lang/Runnable;

    .line 455
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mSIPRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 456
    return-void
.end method

.method protected updateLayout()V
    .locals 4

    .prologue
    .line 351
    invoke-virtual {p0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->initLayout()V

    .line 352
    invoke-static {}, Lcom/samsung/android/fingerprint/service/util/Config;->getRemaingTimeToUnlock()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 353
    invoke-direct {p0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->handleAttemptLockout()V

    .line 367
    :goto_0
    return-void

    .line 355
    :cond_0
    iget v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mBackupType:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 357
    :pswitch_0
    invoke-virtual {p0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->setPasswordLayout()V

    goto :goto_0

    .line 360
    :pswitch_1
    invoke-virtual {p0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->setPinLayout()V

    goto :goto_0

    .line 363
    :pswitch_2
    invoke-virtual {p0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->setPatternLayout()V

    goto :goto_0

    .line 355
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
