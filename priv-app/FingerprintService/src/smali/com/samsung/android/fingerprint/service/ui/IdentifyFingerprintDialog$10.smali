.class Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog$10;
.super Ljava/lang/Object;
.source "IdentifyFingerprintDialog.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->resetErrorMsgAndRequest()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;


# direct methods
.method constructor <init>(Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;)V
    .locals 0

    .prologue
    .line 1058
    iput-object p1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog$10;->this$0:Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 1060
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog$10;->this$0:Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;

    iget-object v0, v0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mInfoImage:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 1061
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog$10;->this$0:Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;

    iget-object v0, v0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mInfoImage:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1062
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog$10;->this$0:Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;

    iget-object v0, v0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mInfoImage:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1064
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog$10;->this$0:Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;

    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog$10;->this$0:Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;

    iget-object v1, v1, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f060006

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    # invokes: Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->setInfoText(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->access$800(Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;Ljava/lang/String;)V

    .line 1065
    return-void
.end method
