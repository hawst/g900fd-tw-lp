.class public abstract Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;
.super Ljava/lang/Object;
.source "FingerprintLibraryCommand.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand$Names;
    }
.end annotation


# static fields
.field private static final ARG_PATTERN:Ljava/util/regex/Pattern;

.field public static final COMMAND_TYPE_ASYNCHRONOUS:I = 0x1

.field public static final COMMAND_TYPE_IMMEDIATE:I = 0x2

.field public static final COMMAND_TYPE_SYNCHRONOUS:I = 0x0

.field private static final PARSEINFO_DELIMETER:Ljava/lang/String; = ":"

.field private static final TAG:Ljava/lang/String; = "FingerprintLibraryCommand"

.field private static sFieldsMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/reflect/Field;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public appName:Ljava/lang/String;

.field public clientRecord:Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

.field public commandName:Ljava/lang/String;

.field public enrollMode:I

.field public fingerIndex:I

.field public inputByteData:[B

.field private mCommandType:I

.field protected mCompleted:Z

.field private mFingerprintEvent:Lcom/samsung/android/fingerprint/FingerprintEvent;

.field private final mParseInfo:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mRecord:Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

.field public newPassword:Ljava/lang/String;

.field public notifyCondition:I

.field public password:Ljava/lang/String;

.field public permissionName:Ljava/lang/String;

.field public stringUserId:Ljava/lang/String;

.field public userId:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    .line 39
    const-string v5, "([0-9]*)=(.*)"

    invoke-static {v5}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v5

    sput-object v5, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;->ARG_PATTERN:Ljava/util/regex/Pattern;

    .line 42
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    sput-object v5, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;->sFieldsMap:Ljava/util/Map;

    .line 93
    const-class v5, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;

    invoke-virtual {v5}, Ljava/lang/Class;->getFields()[Ljava/lang/reflect/Field;

    move-result-object v2

    .line 95
    .local v2, "fields":[Ljava/lang/reflect/Field;
    move-object v0, v2

    .local v0, "arr$":[Ljava/lang/reflect/Field;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_0

    aget-object v1, v0, v3

    .line 96
    .local v1, "field":Ljava/lang/reflect/Field;
    sget-object v5, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;->sFieldsMap:Ljava/util/Map;

    invoke-virtual {v1}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 95
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 98
    .end local v1    # "field":Ljava/lang/reflect/Field;
    :cond_0
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "commandName"    # Ljava/lang/String;
    .param p2, "parseInfo"    # Ljava/lang/String;

    .prologue
    .line 101
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    .line 102
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 12
    .param p1, "commandName"    # Ljava/lang/String;
    .param p2, "parseInfo"    # Ljava/lang/String;
    .param p3, "commandType"    # I

    .prologue
    .line 104
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;->mCompleted:Z

    .line 86
    const/4 v9, 0x0

    iput-object v9, p0, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;->mFingerprintEvent:Lcom/samsung/android/fingerprint/FingerprintEvent;

    .line 105
    iput-object p1, p0, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;->commandName:Ljava/lang/String;

    .line 106
    iput p3, p0, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;->mCommandType:I

    .line 109
    const/4 v3, 0x0

    .line 111
    .local v3, "info":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/String;>;"
    if-eqz p2, :cond_1

    .line 118
    const-string v9, ":"

    invoke-virtual {p2, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    .line 119
    .local v7, "tokens":[Ljava/lang/String;
    new-instance v3, Landroid/util/SparseArray;

    .end local v3    # "info":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/String;>;"
    invoke-direct {v3}, Landroid/util/SparseArray;-><init>()V

    .line 121
    .restart local v3    # "info":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/String;>;"
    move-object v0, v7

    .local v0, "arr$":[Ljava/lang/String;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v4, :cond_1

    aget-object v6, v0, v1

    .line 122
    .local v6, "token":Ljava/lang/String;
    sget-object v9, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;->ARG_PATTERN:Ljava/util/regex/Pattern;

    invoke-virtual {v9, v6}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v5

    .line 124
    .local v5, "match":Ljava/util/regex/Matcher;
    invoke-virtual {v5}, Ljava/util/regex/Matcher;->matches()Z

    move-result v9

    if-eqz v9, :cond_0

    invoke-virtual {v5}, Ljava/util/regex/Matcher;->groupCount()I

    move-result v9

    const/4 v10, 0x2

    if-ne v9, v10, :cond_0

    .line 129
    const/4 v9, 0x1

    invoke-virtual {v5, v9}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 130
    .local v2, "index":I
    const/4 v9, 0x2

    invoke-virtual {v5, v9}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v8

    .line 132
    .local v8, "varName":Ljava/lang/String;
    invoke-virtual {v3, v2, v8}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 121
    .end local v2    # "index":I
    .end local v8    # "varName":Ljava/lang/String;
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 134
    :cond_0
    const-string v9, "FingerprintLibraryCommand"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "parseInfo cannot be parsed: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 139
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v1    # "i$":I
    .end local v4    # "len$":I
    .end local v5    # "match":Ljava/util/regex/Matcher;
    .end local v6    # "token":Ljava/lang/String;
    .end local v7    # "tokens":[Ljava/lang/String;
    :cond_1
    iput-object v3, p0, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;->mParseInfo:Landroid/util/SparseArray;

    .line 140
    return-void
.end method

.method private static getFieldValue(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5
    .param p0, "varName"    # Ljava/lang/String;
    .param p1, "object"    # Ljava/lang/Object;

    .prologue
    .line 376
    :try_start_0
    sget-object v2, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;->sFieldsMap:Ljava/util/Map;

    invoke-interface {v2, p0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 377
    sget-object v2, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;->sFieldsMap:Ljava/util/Map;

    invoke-interface {v2, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/reflect/Field;

    .line 379
    .local v1, "field":Ljava/lang/reflect/Field;
    invoke-virtual {v1, p1}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v2

    .line 387
    .end local v1    # "field":Ljava/lang/reflect/Field;
    :goto_0
    return-object v2

    .line 381
    :catch_0
    move-exception v0

    .line 382
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    const-string v2, "FingerprintLibraryCommand"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getFieldValue: failed for varName="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 387
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :cond_0
    :goto_1
    const/4 v2, 0x0

    goto :goto_0

    .line 383
    :catch_1
    move-exception v0

    .line 384
    .local v0, "e":Ljava/lang/IllegalAccessException;
    const-string v2, "FingerprintLibraryCommand"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getFieldValue: failed for varName="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method private static setFieldValue(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 5
    .param p0, "varName"    # Ljava/lang/String;
    .param p1, "object"    # Ljava/lang/Object;
    .param p2, "value"    # Ljava/lang/Object;

    .prologue
    .line 350
    :try_start_0
    sget-object v2, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;->sFieldsMap:Ljava/util/Map;

    invoke-interface {v2, p0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 351
    sget-object v2, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;->sFieldsMap:Ljava/util/Map;

    invoke-interface {v2, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/reflect/Field;

    .line 353
    .local v1, "field":Ljava/lang/reflect/Field;
    invoke-virtual {v1, p1, p2}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    .line 354
    const/4 v2, 0x1

    .line 362
    .end local v1    # "field":Ljava/lang/reflect/Field;
    :goto_0
    return v2

    .line 356
    :catch_0
    move-exception v0

    .line 357
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    const-string v2, "FingerprintLibraryCommand"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setFieldValue: failed for varName="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 362
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :cond_0
    :goto_1
    const/4 v2, 0x0

    goto :goto_0

    .line 358
    :catch_1
    move-exception v0

    .line 359
    .local v0, "e":Ljava/lang/IllegalAccessException;
    const-string v2, "FingerprintLibraryCommand"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setFieldValue: failed for varName="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method


# virtual methods
.method public convertToPermissionList(Ljava/util/List;)Ljava/lang/String;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 270
    .local p1, "permissions":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-nez p1, :cond_0

    .line 271
    const-string v4, ""

    .line 287
    :goto_0
    return-object v4

    .line 274
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 276
    .local v0, "builder":Ljava/lang/StringBuilder;
    const/4 v2, 0x1

    .line 278
    .local v2, "isFirst":Z
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 279
    .local v3, "permName":Ljava/lang/String;
    if-nez v2, :cond_1

    .line 280
    const/16 v4, 0x2c

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 284
    :goto_2
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 282
    :cond_1
    const/4 v2, 0x0

    goto :goto_2

    .line 287
    .end local v3    # "permName":Ljava/lang/String;
    :cond_2
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_0
.end method

.method public final create([Ljava/lang/Object;)Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;
    .locals 10
    .param p1, "args"    # [Ljava/lang/Object;

    .prologue
    .line 308
    const/4 v5, 0x0

    .line 311
    .local v5, "retObj":Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v7

    move-object v0, v7

    check-cast v0, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;

    move-object v5, v0
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 316
    :goto_0
    if-nez v5, :cond_1

    .line 317
    const/4 v5, 0x0

    .line 336
    .end local v5    # "retObj":Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;
    :cond_0
    return-object v5

    .line 312
    .restart local v5    # "retObj":Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;
    :catch_0
    move-exception v3

    .line 313
    .local v3, "e":Ljava/lang/CloneNotSupportedException;
    const-string v7, "FingerprintLibraryCommand"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "create: cannot clone: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 320
    .end local v3    # "e":Ljava/lang/CloneNotSupportedException;
    :cond_1
    iget-object v7, p0, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;->mParseInfo:Landroid/util/SparseArray;

    if-eqz v7, :cond_0

    .line 321
    iget-object v7, p0, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;->mParseInfo:Landroid/util/SparseArray;

    invoke-virtual {v7}, Landroid/util/SparseArray;->size()I

    move-result v1

    .line 323
    .local v1, "N":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_1
    if-ge v4, v1, :cond_0

    .line 324
    iget-object v7, p0, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;->mParseInfo:Landroid/util/SparseArray;

    invoke-virtual {v7, v4}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v2

    .line 325
    .local v2, "argIndex":I
    iget-object v7, p0, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;->mParseInfo:Landroid/util/SparseArray;

    invoke-virtual {v7, v4}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 330
    .local v6, "varName":Ljava/lang/String;
    aget-object v7, p1, v2

    invoke-static {v6, v5, v7}, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;->setFieldValue(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_2

    .line 331
    const-string v7, "FingerprintLibraryCommand"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "unknown varName was used: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 323
    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_1
.end method

.method public deliverEventToBackgroundClient(Lcom/samsung/android/fingerprint/FingerprintEvent;)V
    .locals 1
    .param p1, "evt"    # Lcom/samsung/android/fingerprint/FingerprintEvent;

    .prologue
    .line 233
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;->mRecord:Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;->mRecord:Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

    invoke-virtual {v0}, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->isBackground()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 234
    invoke-virtual {p0, p1}, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;->deliverEventToClient(Lcom/samsung/android/fingerprint/FingerprintEvent;)V

    .line 235
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;->mRecord:Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

    invoke-virtual {v0, p0, p1}, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->onEvent(Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;Lcom/samsung/android/fingerprint/FingerprintEvent;)V

    .line 237
    :cond_0
    return-void
.end method

.method public deliverEventToClient(I)V
    .locals 1
    .param p1, "eventId"    # I

    .prologue
    .line 151
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;->deliverEventToClient(II)V

    .line 152
    return-void
.end method

.method public deliverEventToClient(II)V
    .locals 1
    .param p1, "eventId"    # I
    .param p2, "eventResult"    # I

    .prologue
    .line 155
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;->deliverEventToClient(III)V

    .line 156
    return-void
.end method

.method public deliverEventToClient(III)V
    .locals 1
    .param p1, "eventId"    # I
    .param p2, "eventResult"    # I
    .param p3, "eventStatus"    # I

    .prologue
    .line 159
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;->deliverEventToClient(IIILandroid/os/Bundle;)V

    .line 160
    return-void
.end method

.method public deliverEventToClient(IIILandroid/os/Bundle;)V
    .locals 2
    .param p1, "eventId"    # I
    .param p2, "eventResult"    # I
    .param p3, "eventStatus"    # I
    .param p4, "eventData"    # Landroid/os/Bundle;

    .prologue
    .line 163
    new-instance v0, Lcom/samsung/android/fingerprint/FingerprintEvent;

    invoke-direct {v0, p1}, Lcom/samsung/android/fingerprint/FingerprintEvent;-><init>(I)V

    .line 165
    .local v0, "evt":Lcom/samsung/android/fingerprint/FingerprintEvent;
    iput p2, v0, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventResult:I

    .line 166
    iput p3, v0, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventStatus:I

    .line 168
    if-eqz p4, :cond_0

    .line 169
    iget-object v1, v0, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventData:Landroid/os/Bundle;

    invoke-virtual {v1, p4}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 172
    :cond_0
    invoke-virtual {p0, v0}, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;->deliverEventToClient(Lcom/samsung/android/fingerprint/FingerprintEvent;)V

    .line 173
    return-void
.end method

.method public deliverEventToClient(Lcom/samsung/android/fingerprint/FingerprintEvent;)V
    .locals 3
    .param p1, "evt"    # Lcom/samsung/android/fingerprint/FingerprintEvent;

    .prologue
    .line 182
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;->mRecord:Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

    if-nez v0, :cond_0

    .line 183
    const-string v0, "FingerprintLibraryCommand"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "deliverEventToClient: drop appName="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;->appName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", eventId="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/samsung/android/fingerprint/FingerprintEvent;->getEventIdName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " eventResult="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/samsung/android/fingerprint/FingerprintEvent;->getEventResultName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " eventStatus="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/samsung/android/fingerprint/FingerprintEvent;->getEventStatusName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    :goto_0
    return-void

    .line 188
    :cond_0
    iput-object p1, p0, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;->mFingerprintEvent:Lcom/samsung/android/fingerprint/FingerprintEvent;

    .line 189
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;->mRecord:Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

    invoke-virtual {v0, p0, p1}, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->onEvent(Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;Lcom/samsung/android/fingerprint/FingerprintEvent;)V

    goto :goto_0
.end method

.method public deliverExtraEventToClient(I)V
    .locals 1
    .param p1, "eventId"    # I

    .prologue
    .line 194
    invoke-virtual {p0}, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;->isExtraEventDemanded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 196
    invoke-virtual {p0, p1}, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;->deliverEventToClient(I)V

    .line 198
    :cond_0
    return-void
.end method

.method public deliverExtraEventToClient(Lcom/samsung/android/fingerprint/FingerprintEvent;)V
    .locals 1
    .param p1, "evt"    # Lcom/samsung/android/fingerprint/FingerprintEvent;

    .prologue
    .line 240
    invoke-virtual {p0}, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;->isExtraEventDemanded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 241
    invoke-virtual {p0, p1}, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;->deliverEventToClient(Lcom/samsung/android/fingerprint/FingerprintEvent;)V

    .line 243
    :cond_0
    return-void
.end method

.method public deliverNavigationEventToClient(I)V
    .locals 3
    .param p1, "navigationDirection"    # I

    .prologue
    const/4 v2, 0x0

    .line 207
    invoke-virtual {p0}, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;->isExtraEventDemanded()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 208
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 209
    .local v0, "data":Landroid/os/Bundle;
    const-string v1, "direction"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 211
    const/16 v1, 0x3e8

    invoke-virtual {p0, v1, v2, v2, v0}, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;->deliverEventToClient(IIILandroid/os/Bundle;)V

    .line 214
    .end local v0    # "data":Landroid/os/Bundle;
    :cond_0
    return-void
.end method

.method public deliverSwipeSpeedEventToClient(I)V
    .locals 3
    .param p1, "speed"    # I

    .prologue
    const/4 v2, 0x0

    .line 217
    invoke-virtual {p0}, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;->isExtraEventDemanded()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 218
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 219
    .local v0, "data":Landroid/os/Bundle;
    const-string v1, "swipeSpeed"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 221
    const/16 v1, 0x3f5

    invoke-virtual {p0, v1, v2, v2, v0}, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;->deliverEventToClient(IIILandroid/os/Bundle;)V

    .line 224
    .end local v0    # "data":Landroid/os/Bundle;
    :cond_0
    return-void
.end method

.method protected abstract doCommand()Ljava/lang/Object;
.end method

.method public doesUseDefaultTimeoutPolicy()Z
    .locals 1

    .prologue
    .line 250
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;->mRecord:Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;->mRecord:Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

    invoke-virtual {v0}, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->doesUseDefaultTimeoutPolicy()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected dumpArgumentValues()Ljava/lang/String;
    .locals 7

    .prologue
    const/16 v6, 0x20

    .line 396
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 398
    .local v1, "builder":Ljava/lang/StringBuilder;
    iget-object v5, p0, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;->commandName:Ljava/lang/String;

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 399
    const/16 v5, 0x3a

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 400
    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 402
    iget-object v5, p0, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;->mParseInfo:Landroid/util/SparseArray;

    if-eqz v5, :cond_1

    .line 403
    iget-object v5, p0, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;->mParseInfo:Landroid/util/SparseArray;

    invoke-virtual {v5}, Landroid/util/SparseArray;->size()I

    move-result v0

    .line 405
    .local v0, "N":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v0, :cond_2

    .line 406
    iget-object v5, p0, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;->mParseInfo:Landroid/util/SparseArray;

    invoke-virtual {v5, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 408
    .local v4, "varName":Ljava/lang/String;
    if-lez v2, :cond_0

    .line 409
    const/16 v5, 0x2c

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 410
    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 413
    :cond_0
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 414
    const/16 v5, 0x3d

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 416
    invoke-static {v4, p0}, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;->getFieldValue(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    .line 417
    .local v3, "value":Ljava/lang/Object;
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 405
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 420
    .end local v0    # "N":I
    .end local v2    # "i":I
    .end local v3    # "value":Ljava/lang/Object;
    .end local v4    # "varName":Ljava/lang/String;
    :cond_1
    const-string v5, "called."

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 423
    :cond_2
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    return-object v5
.end method

.method public execute()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 427
    const-string v0, "FingerprintLibraryCommand"

    invoke-virtual {p0}, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;->dumpArgumentValues()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 429
    invoke-virtual {p0}, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;->doCommand()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public findMatchedPermission(Ljava/util/List;Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p2, "requestedPermission"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 291
    .local p1, "permissions":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-nez p2, :cond_1

    .line 292
    const/4 v1, 0x0

    .line 304
    :cond_0
    :goto_0
    return-object v1

    .line 295
    :cond_1
    const/4 v1, 0x0

    .line 297
    .local v1, "matchedPermName":Ljava/lang/String;
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 298
    .local v2, "permName":Ljava/lang/String;
    iget-object v3, p0, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;->permissionName:Ljava/lang/String;

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 299
    move-object v1, v2

    .line 300
    goto :goto_0
.end method

.method public forceCancel()V
    .locals 0

    .prologue
    .line 438
    return-void
.end method

.method public getCommandType()I
    .locals 1

    .prologue
    .line 143
    iget v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;->mCommandType:I

    return v0
.end method

.method public getFingerprivetEvent()Lcom/samsung/android/fingerprint/FingerprintEvent;
    .locals 1

    .prologue
    .line 457
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;->mFingerprintEvent:Lcom/samsung/android/fingerprint/FingerprintEvent;

    return-object v0
.end method

.method public getSecurityLevel()I
    .locals 1

    .prologue
    .line 254
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;->mRecord:Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;->mRecord:Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

    invoke-virtual {v0}, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->getSecurityLevel()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public interrupt(IILjava/lang/Object;)Z
    .locals 1
    .param p1, "type"    # I
    .param p2, "param1"    # I
    .param p3, "param2"    # Ljava/lang/Object;

    .prologue
    .line 449
    invoke-virtual {p0, p1, p2, p3}, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;->onInterrupt(IILjava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public isExtraEventDemanded()Z
    .locals 1

    .prologue
    .line 246
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;->mRecord:Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;->mRecord:Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

    invoke-virtual {v0}, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->isExtraEventDemanded()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public notifyCommandCompleted()V
    .locals 3

    .prologue
    .line 258
    iget-boolean v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;->mCompleted:Z

    if-eqz v0, :cond_1

    .line 259
    const-string v0, "FingerprintLibraryCommand"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "notifyCommandCompleted: command ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;->commandName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] was already completed."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 267
    :cond_0
    :goto_0
    return-void

    .line 261
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;->mCompleted:Z

    .line 263
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;->mRecord:Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

    if-eqz v0, :cond_0

    .line 264
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;->mRecord:Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

    invoke-virtual {v0, p0}, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->onCommandCompleted(Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;)V

    goto :goto_0
.end method

.method public onEvent(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "evt"    # Ljava/lang/Object;

    .prologue
    .line 471
    const/4 v0, 0x0

    return v0
.end method

.method public onInterrupt(IILjava/lang/Object;)Z
    .locals 1
    .param p1, "type"    # I
    .param p2, "param1"    # I
    .param p3, "param2"    # Ljava/lang/Object;

    .prologue
    .line 453
    const/4 v0, 0x1

    return v0
.end method

.method public requestCancel()Z
    .locals 1

    .prologue
    .line 444
    invoke-virtual {p0}, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;->notifyCommandCompleted()V

    .line 445
    const/4 v0, 0x1

    return v0
.end method

.method public setClientRecord(Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;)V
    .locals 0
    .param p1, "record"    # Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

    .prologue
    .line 147
    iput-object p1, p0, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;->mRecord:Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

    .line 148
    return-void
.end method
