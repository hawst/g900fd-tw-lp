.class Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog$8;
.super Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog$AnimationDrawableListener;
.source "IdentifyFingerprintDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->startFailedAnimation()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;


# direct methods
.method constructor <init>(Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;Landroid/graphics/drawable/AnimationDrawable;)V
    .locals 0
    .param p2, "x0"    # Landroid/graphics/drawable/AnimationDrawable;

    .prologue
    .line 988
    iput-object p1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog$8;->this$0:Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;

    invoke-direct {p0, p1, p2}, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog$AnimationDrawableListener;-><init>(Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;Landroid/graphics/drawable/AnimationDrawable;)V

    return-void
.end method


# virtual methods
.method onAnimationEnd()V
    .locals 4

    .prologue
    .line 991
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog$8;->this$0:Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;

    const/4 v1, 0x0

    # setter for: Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mIsFailState:Z
    invoke-static {v0, v1}, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->access$402(Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;Z)Z

    .line 992
    invoke-static {}, Lcom/samsung/android/fingerprint/service/util/Config;->getRemaingTimeToUnlock()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 993
    invoke-static {}, Lcom/samsung/android/fingerprint/service/util/Config;->setRemaingTimeToUnlock()V

    .line 994
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog$8;->this$0:Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;

    # invokes: Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->showLockoutAttentionDialog()V
    invoke-static {v0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->access$500(Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;)V

    .line 998
    :goto_0
    return-void

    .line 996
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog$8;->this$0:Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;

    invoke-virtual {v0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->startStandbyAnimation()V

    goto :goto_0
.end method
