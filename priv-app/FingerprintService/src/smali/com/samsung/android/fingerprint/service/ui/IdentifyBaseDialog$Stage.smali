.class public final enum Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog$Stage;
.super Ljava/lang/Enum;
.source "IdentifyBaseDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401c
    name = "Stage"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog$Stage;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog$Stage;

.field public static final enum LockedOut:Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog$Stage;

.field public static final enum UnlockFingerprint:Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog$Stage;

.field public static final enum UnlockWrong:Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog$Stage;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 52
    new-instance v0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog$Stage;

    const-string v1, "UnlockFingerprint"

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog$Stage;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog$Stage;->UnlockFingerprint:Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog$Stage;

    new-instance v0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog$Stage;

    const-string v1, "UnlockWrong"

    invoke-direct {v0, v1, v3}, Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog$Stage;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog$Stage;->UnlockWrong:Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog$Stage;

    new-instance v0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog$Stage;

    const-string v1, "LockedOut"

    invoke-direct {v0, v1, v4}, Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog$Stage;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog$Stage;->LockedOut:Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog$Stage;

    .line 51
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog$Stage;

    sget-object v1, Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog$Stage;->UnlockFingerprint:Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog$Stage;

    aput-object v1, v0, v2

    sget-object v1, Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog$Stage;->UnlockWrong:Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog$Stage;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog$Stage;->LockedOut:Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog$Stage;

    aput-object v1, v0, v4

    sput-object v0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog$Stage;->$VALUES:[Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog$Stage;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 51
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog$Stage;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 51
    const-class v0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog$Stage;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog$Stage;

    return-object v0
.end method

.method public static values()[Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog$Stage;
    .locals 1

    .prologue
    .line 51
    sget-object v0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog$Stage;->$VALUES:[Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog$Stage;

    invoke-virtual {v0}, [Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog$Stage;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog$Stage;

    return-object v0
.end method
