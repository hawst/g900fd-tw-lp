.class Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$1;
.super Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;
.source "FingerprintLibraryImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private isCompleted:Z

.field private isFirstEvent:Z

.field private isResulted:Z

.field private isRunning:Z

.field private isSensorRemoved:Z

.field private isStarted:Z

.field private mCntPrevBadSwipe:I

.field private pendingEvent:Ljava/lang/Object;

.field final synthetic this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

.field private validityImageQuality:I


# direct methods
.method constructor <init>(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 2
    .param p2, "x0"    # Ljava/lang/String;
    .param p3, "x1"    # Ljava/lang/String;
    .param p4, "x2"    # I

    .prologue
    const/4 v1, 0x0

    .line 340
    iput-object p1, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$1;->this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    invoke-direct {p0, p2, p3, p4}, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    .line 342
    iput-boolean v1, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$1;->isRunning:Z

    .line 343
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$1;->isFirstEvent:Z

    .line 344
    iput-boolean v1, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$1;->isCompleted:Z

    .line 345
    iput-boolean v1, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$1;->isResulted:Z

    .line 346
    iput-boolean v1, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$1;->isStarted:Z

    .line 347
    iput-boolean v1, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$1;->isSensorRemoved:Z

    .line 348
    iput v1, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$1;->mCntPrevBadSwipe:I

    .line 349
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$1;->pendingEvent:Ljava/lang/Object;

    .line 351
    iput v1, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$1;->validityImageQuality:I

    return-void
.end method


# virtual methods
.method public doCommand()Ljava/lang/Object;
    .locals 7

    .prologue
    .line 354
    iget v4, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$1;->fingerIndex:I

    invoke-static {v4}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintValues$Generic2Validity;->getAppData(I)Ljava/lang/String;

    move-result-object v2

    .line 355
    .local v2, "vAppData":Ljava/lang/String;
    iget v4, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$1;->fingerIndex:I

    invoke-static {v4}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintValues$Generic2Validity;->getFingerIndex(I)I

    move-result v3

    .line 357
    .local v3, "vIndex":I
    new-instance v4, Ljava/lang/StringBuilder;

    const/16 v5, 0x80

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "enroll: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 358
    .local v1, "sb":Ljava/lang/StringBuilder;
    sget-boolean v4, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->DEBUG:Z

    if-eqz v4, :cond_2

    .line 359
    const-string v4, "stringUserId="

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$1;->stringUserId:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", appName="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$1;->appName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", appData="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", fingerIndex="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 367
    :goto_0
    const-string v4, "ValidityFingerprintLibraryImpl"

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->da(Ljava/lang/String;Ljava/lang/String;)V

    .line 369
    iget-object v4, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$1;->this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    # getter for: Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mFingerprintLog:Lcom/samsung/android/fingerprint/service/util/FingerprintLog;
    invoke-static {v4}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->access$000(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;)Lcom/samsung/android/fingerprint/service/util/FingerprintLog;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 370
    iget-object v4, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$1;->this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    # getter for: Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mFingerprintLog:Lcom/samsung/android/fingerprint/service/util/FingerprintLog;
    invoke-static {v4}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->access$000(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;)Lcom/samsung/android/fingerprint/service/util/FingerprintLog;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->createLogFile()V

    .line 371
    iget-object v4, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$1;->this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    # getter for: Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mFingerprintLog:Lcom/samsung/android/fingerprint/service/util/FingerprintLog;
    invoke-static {v4}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->access$000(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;)Lcom/samsung/android/fingerprint/service/util/FingerprintLog;

    move-result-object v4

    invoke-virtual {p0}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$1;->dumpArgumentValues()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->logToFile(Ljava/lang/String;)V

    .line 374
    :cond_0
    iget-object v4, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$1;->this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    # getter for: Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mFingerprint:Lcom/samsung/android/fingerprint/service/vendor/FingerprintProxy;
    invoke-static {v4}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->access$100(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;)Lcom/samsung/android/fingerprint/service/vendor/FingerprintProxy;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$1;->stringUserId:Ljava/lang/String;

    iget v6, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$1;->enrollMode:I

    invoke-static {v6}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintValues$Generic2Validity;->getEnrollMode(I)I

    move-result v6

    invoke-interface {v4, v5, v2, v3, v6}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintProxy;->enroll(Ljava/lang/String;Ljava/lang/String;II)I

    move-result v0

    .line 379
    .local v0, "result":I
    if-nez v0, :cond_1

    .line 380
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$1;->isRunning:Z

    .line 382
    :cond_1
    const-string v4, "ValidityFingerprintLibraryImpl"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "enroll: result="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 384
    invoke-static {v0}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintValues$Validity2Generic;->getResult(I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    return-object v4

    .line 364
    .end local v0    # "result":I
    :cond_2
    const-string v4, "sid="

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$1;->stringUserId:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", i="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method public onEvent(Ljava/lang/Object;)Z
    .locals 14
    .param p1, "evt"    # Ljava/lang/Object;

    .prologue
    .line 406
    move-object v7, p1

    check-cast v7, Lcom/validity/fingerprint/FingerprintEvent;

    .line 407
    .local v7, "vev":Lcom/validity/fingerprint/FingerprintEvent;
    const/4 v2, 0x0

    .line 409
    .local v2, "gev":Lcom/samsung/android/fingerprint/FingerprintEvent;
    iget-object v9, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$1;->this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    # getter for: Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mFingerprintLog:Lcom/samsung/android/fingerprint/service/util/FingerprintLog;
    invoke-static {v9}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->access$000(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;)Lcom/samsung/android/fingerprint/service/util/FingerprintLog;

    move-result-object v9

    if-eqz v9, :cond_0

    .line 410
    iget-object v9, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$1;->this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    # getter for: Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mFingerprintLog:Lcom/samsung/android/fingerprint/service/util/FingerprintLog;
    invoke-static {v9}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->access$000(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;)Lcom/samsung/android/fingerprint/service/util/FingerprintLog;

    move-result-object v9

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Enroll.onEvent: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget v11, v7, Lcom/validity/fingerprint/FingerprintEvent;->eventId:I

    invoke-static {v11}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintValues$Validity2Generic;->eventIdToString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ", "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget v11, v7, Lcom/validity/fingerprint/FingerprintEvent;->eventId:I

    iget-object v12, v7, Lcom/validity/fingerprint/FingerprintEvent;->eventData:Ljava/lang/Object;

    invoke-static {v11, v12}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintValues$Validity2Generic;->objectToString(ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->logToFile(Ljava/lang/String;)V

    .line 439
    :cond_0
    const-string v9, "ValidityFingerprintLibraryImpl"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "enroll: onEvent="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget v11, v7, Lcom/validity/fingerprint/FingerprintEvent;->eventId:I

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 440
    iget v9, v7, Lcom/validity/fingerprint/FingerprintEvent;->eventId:I

    sparse-switch v9, :sswitch_data_0

    .line 628
    :cond_1
    :goto_0
    :sswitch_0
    iget-boolean v9, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$1;->isRunning:Z

    if-eqz v9, :cond_3

    iget-boolean v9, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$1;->isResulted:Z

    if-eqz v9, :cond_3

    iget-boolean v9, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$1;->isCompleted:Z

    if-eqz v9, :cond_3

    .line 629
    iget-object v9, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$1;->pendingEvent:Ljava/lang/Object;

    if-eqz v9, :cond_2

    .line 630
    iget-object v5, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$1;->pendingEvent:Ljava/lang/Object;

    .line 631
    .local v5, "savedEvent":Ljava/lang/Object;
    const/4 v9, 0x0

    iput-object v9, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$1;->pendingEvent:Ljava/lang/Object;

    .line 633
    invoke-virtual {p0, v5}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$1;->onEvent(Ljava/lang/Object;)Z

    .line 636
    .end local v5    # "savedEvent":Ljava/lang/Object;
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$1;->notifyCommandCompleted()V

    .line 637
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$1;->isRunning:Z

    .line 640
    :cond_3
    const/4 v9, 0x1

    return v9

    .line 442
    :sswitch_1
    iget-boolean v9, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$1;->isStarted:Z

    if-nez v9, :cond_1

    .line 443
    const/4 v9, 0x1

    iput-boolean v9, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$1;->isStarted:Z

    .line 445
    iget-boolean v9, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$1;->isResulted:Z

    if-nez v9, :cond_1

    .line 446
    const/4 v9, 0x2

    invoke-virtual {p0, v9}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$1;->deliverEventToClient(I)V

    goto :goto_0

    .line 455
    :sswitch_2
    const/4 v9, 0x1

    iput-boolean v9, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$1;->isCompleted:Z

    .line 457
    iget-object v9, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$1;->pendingEvent:Ljava/lang/Object;

    if-eqz v9, :cond_1

    .line 458
    const/4 v9, 0x1

    iput-boolean v9, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$1;->isResulted:Z

    goto :goto_0

    .line 463
    :sswitch_3
    iget-boolean v9, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$1;->isFirstEvent:Z

    if-eqz v9, :cond_1

    .line 464
    const/4 v9, 0x1

    invoke-virtual {p0, v9}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$1;->deliverEventToClient(I)V

    .line 465
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$1;->isFirstEvent:Z

    goto :goto_0

    .line 471
    :sswitch_4
    const/4 v9, 0x0

    iput v9, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$1;->validityImageQuality:I

    goto :goto_0

    .line 475
    :sswitch_5
    iget-object v9, v7, Lcom/validity/fingerprint/FingerprintEvent;->eventData:Ljava/lang/Object;

    if-eqz v9, :cond_1

    .line 476
    iget-object v1, v7, Lcom/validity/fingerprint/FingerprintEvent;->eventData:Ljava/lang/Object;

    check-cast v1, Lcom/validity/fingerprint/FingerprintBitmap;

    .line 478
    .local v1, "fb":Lcom/validity/fingerprint/FingerprintBitmap;
    if-eqz v1, :cond_1

    .line 479
    iget v9, v1, Lcom/validity/fingerprint/FingerprintBitmap;->quality:I

    iput v9, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$1;->validityImageQuality:I

    goto :goto_0

    .line 486
    .end local v1    # "fb":Lcom/validity/fingerprint/FingerprintBitmap;
    :sswitch_6
    new-instance v2, Lcom/samsung/android/fingerprint/FingerprintEvent;

    .end local v2    # "gev":Lcom/samsung/android/fingerprint/FingerprintEvent;
    const/4 v9, 0x3

    invoke-direct {v2, v9}, Lcom/samsung/android/fingerprint/FingerprintEvent;-><init>(I)V

    .line 487
    .restart local v2    # "gev":Lcom/samsung/android/fingerprint/FingerprintEvent;
    const/16 v9, 0x14

    iput v9, v2, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventStatus:I

    .line 489
    iget-object v9, v7, Lcom/validity/fingerprint/FingerprintEvent;->eventData:Ljava/lang/Object;

    if-eqz v9, :cond_5

    iget-object v9, v7, Lcom/validity/fingerprint/FingerprintEvent;->eventData:Ljava/lang/Object;

    instance-of v9, v9, Ljava/lang/Integer;

    if-eqz v9, :cond_5

    .line 490
    iget-object v4, v7, Lcom/validity/fingerprint/FingerprintEvent;->eventData:Ljava/lang/Object;

    check-cast v4, Ljava/lang/Integer;

    .line 491
    .local v4, "imgQuality":Ljava/lang/Integer;
    if-eqz v4, :cond_4

    .line 492
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v9

    iput v9, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$1;->validityImageQuality:I

    .line 494
    :cond_4
    iget v9, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$1;->validityImageQuality:I

    invoke-static {v9}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintValues$Validity2Generic;->getImageQuality(I)I

    move-result v3

    .line 495
    .local v3, "imageQuality":I
    sget-boolean v9, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->DEBUG:Z

    if-eqz v9, :cond_6

    .line 496
    const-string v9, "ValidityFingerprintLibraryImpl"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "VCS_EVT_EIV_FINGERPRINT_CAPTURED_BAD: imageQuality="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-static {v3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 500
    :goto_1
    iget-object v9, v2, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventData:Landroid/os/Bundle;

    const-string v10, "imageQuality"

    invoke-virtual {v9, v10, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 501
    iget-object v9, v2, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventData:Landroid/os/Bundle;

    const-string v10, "imageQualityFeedback"

    iget-object v11, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$1;->this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    # invokes: Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->getImageQualityFeedback(I)Ljava/lang/String;
    invoke-static {v11, v3}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->access$200(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 503
    const/4 v9, 0x4

    if-ne v3, v9, :cond_7

    .line 504
    iget-object v9, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$1;->this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    # operator++ for: Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mCntOfTooShort:I
    invoke-static {v9}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->access$308(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;)I

    .line 505
    iget-object v9, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$1;->this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    # getter for: Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mCntOfTooShort:I
    invoke-static {v9}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->access$300(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;)I

    move-result v9

    const/4 v10, 0x3

    if-lt v9, v10, :cond_5

    .line 506
    iget-object v9, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$1;->this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    const/4 v10, 0x0

    # setter for: Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mCntOfTooShort:I
    invoke-static {v9, v10}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->access$302(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;I)I

    .line 507
    const-string v9, "ValidityFingerprintLibraryImpl"

    const-string v10, "TOO SHORT --> TOO light pressure"

    invoke-static {v9, v10}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 508
    iget-object v9, v2, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventData:Landroid/os/Bundle;

    const-string v10, "imageQuality"

    const/4 v11, 0x2

    invoke-virtual {v9, v10, v11}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 510
    iget-object v9, v2, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventData:Landroid/os/Bundle;

    const-string v10, "imageQualityFeedback"

    iget-object v11, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$1;->this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    const/4 v12, 0x2

    # invokes: Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->getImageQualityFeedback(I)Ljava/lang/String;
    invoke-static {v11, v12}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->access$200(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 518
    .end local v3    # "imageQuality":I
    .end local v4    # "imgQuality":Ljava/lang/Integer;
    :cond_5
    :goto_2
    invoke-virtual {p0, v2}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$1;->deliverEventToClient(Lcom/samsung/android/fingerprint/FingerprintEvent;)V

    .line 520
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$1;->isStarted:Z

    goto/16 :goto_0

    .line 498
    .restart local v3    # "imageQuality":I
    .restart local v4    # "imgQuality":Ljava/lang/Integer;
    :cond_6
    const-string v9, "ValidityFingerprintLibraryImpl"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "q="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-static {v3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->da(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 514
    :cond_7
    iget-object v9, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$1;->this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    const/4 v10, 0x0

    # setter for: Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mCntOfTooShort:I
    invoke-static {v9, v10}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->access$302(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;I)I

    goto :goto_2

    .line 524
    .end local v3    # "imageQuality":I
    .end local v4    # "imgQuality":Ljava/lang/Integer;
    :sswitch_7
    const-string v9, "ValidityFingerprintLibraryImpl"

    const-string v10, "VCS_EVT_EIV_FINGERPRINT_CAPTURE_REDUNDANT"

    invoke-static {v9, v10}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 525
    const/high16 v4, 0x30000000

    .line 527
    .local v4, "imgQuality":I
    new-instance v2, Lcom/samsung/android/fingerprint/FingerprintEvent;

    .end local v2    # "gev":Lcom/samsung/android/fingerprint/FingerprintEvent;
    const/4 v9, 0x3

    invoke-direct {v2, v9}, Lcom/samsung/android/fingerprint/FingerprintEvent;-><init>(I)V

    .line 528
    .restart local v2    # "gev":Lcom/samsung/android/fingerprint/FingerprintEvent;
    const/16 v9, 0x14

    iput v9, v2, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventStatus:I

    .line 529
    iget-object v9, v2, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventData:Landroid/os/Bundle;

    const-string v10, "imageQuality"

    invoke-virtual {v9, v10, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 530
    iget-object v9, v2, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventData:Landroid/os/Bundle;

    const-string v10, "imageQualityFeedback"

    iget-object v11, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$1;->this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    # invokes: Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->getImageQualityFeedback(I)Ljava/lang/String;
    invoke-static {v11, v4}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->access$200(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 532
    invoke-virtual {p0, v2}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$1;->deliverEventToClient(Lcom/samsung/android/fingerprint/FingerprintEvent;)V

    .line 533
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$1;->isStarted:Z

    goto/16 :goto_0

    .line 538
    .end local v4    # "imgQuality":I
    :sswitch_8
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$1;->isStarted:Z

    .line 540
    iget-object v6, v7, Lcom/validity/fingerprint/FingerprintEvent;->eventData:Ljava/lang/Object;

    check-cast v6, Lcom/validity/fingerprint/EnrollCaptureStatus;

    .line 542
    .local v6, "status":Lcom/validity/fingerprint/EnrollCaptureStatus;
    iget v9, v6, Lcom/validity/fingerprint/EnrollCaptureStatus;->imageQuality:I

    invoke-static {v9}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintValues$Validity2Generic;->getImageQuality(I)I

    move-result v4

    .line 543
    .restart local v4    # "imgQuality":I
    iget v9, v6, Lcom/validity/fingerprint/EnrollCaptureStatus;->badSwipes:I

    iget v10, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$1;->mCntPrevBadSwipe:I

    if-ne v9, v10, :cond_8

    .line 544
    iget-object v9, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$1;->this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    const/4 v10, 0x0

    # setter for: Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mCntOfTooShort:I
    invoke-static {v9, v10}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->access$302(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;I)I

    .line 545
    iget-object v9, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$1;->this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    # getter for: Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mContext:Landroid/content/Context;
    invoke-static {v9}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->access$400(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;)Landroid/content/Context;

    move-result-object v9

    const-string v10, "vibrator"

    invoke-virtual {v9, v10}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/os/Vibrator;

    .line 546
    .local v8, "vib":Landroid/os/Vibrator;
    if-eqz v8, :cond_8

    .line 547
    const-wide/16 v10, 0x32

    invoke-virtual {v8, v10, v11}, Landroid/os/Vibrator;->vibrate(J)V

    .line 550
    .end local v8    # "vib":Landroid/os/Vibrator;
    :cond_8
    new-instance v2, Lcom/samsung/android/fingerprint/FingerprintEvent;

    .end local v2    # "gev":Lcom/samsung/android/fingerprint/FingerprintEvent;
    const/4 v9, 0x3

    invoke-direct {v2, v9}, Lcom/samsung/android/fingerprint/FingerprintEvent;-><init>(I)V

    .line 551
    .restart local v2    # "gev":Lcom/samsung/android/fingerprint/FingerprintEvent;
    iget-object v9, v2, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventData:Landroid/os/Bundle;

    const-string v10, "badSwipes"

    iget v11, v6, Lcom/validity/fingerprint/EnrollCaptureStatus;->badSwipes:I

    invoke-virtual {v9, v10, v11}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 552
    iget v9, v6, Lcom/validity/fingerprint/EnrollCaptureStatus;->badSwipes:I

    iput v9, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$1;->mCntPrevBadSwipe:I

    .line 553
    iget-object v9, v2, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventData:Landroid/os/Bundle;

    const-string v10, "imageQuality"

    invoke-virtual {v9, v10, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 554
    iget-object v9, v2, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventData:Landroid/os/Bundle;

    const-string v10, "imageQualityFeedback"

    iget-object v11, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$1;->this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    # invokes: Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->getImageQualityFeedback(I)Ljava/lang/String;
    invoke-static {v11, v4}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->access$200(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 556
    iget-object v9, v2, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventData:Landroid/os/Bundle;

    const-string v10, "progress"

    iget v11, v6, Lcom/validity/fingerprint/EnrollCaptureStatus;->totalSwipes:I

    iget v12, v6, Lcom/validity/fingerprint/EnrollCaptureStatus;->badSwipes:I

    sub-int/2addr v11, v12

    invoke-virtual {v9, v10, v11}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 557
    iget-object v9, v2, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventData:Landroid/os/Bundle;

    const-string v10, "templateResult"

    iget v11, v6, Lcom/validity/fingerprint/EnrollCaptureStatus;->templateResult:I

    invoke-virtual {v9, v10, v11}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 558
    iget-object v9, v2, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventData:Landroid/os/Bundle;

    const-string v10, "totalSwipes"

    iget v11, v6, Lcom/validity/fingerprint/EnrollCaptureStatus;->totalSwipes:I

    invoke-virtual {v9, v10, v11}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 560
    invoke-virtual {p0, v2}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$1;->deliverEventToClient(Lcom/samsung/android/fingerprint/FingerprintEvent;)V

    goto/16 :goto_0

    .line 565
    .end local v4    # "imgQuality":I
    .end local v6    # "status":Lcom/validity/fingerprint/EnrollCaptureStatus;
    :sswitch_9
    iget-boolean v9, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$1;->isCompleted:Z

    if-nez v9, :cond_9

    .line 566
    iput-object p1, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$1;->pendingEvent:Ljava/lang/Object;

    goto/16 :goto_0

    .line 570
    :cond_9
    const/4 v9, 0x1

    iput-boolean v9, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$1;->isResulted:Z

    .line 572
    new-instance v2, Lcom/samsung/android/fingerprint/FingerprintEvent;

    .end local v2    # "gev":Lcom/samsung/android/fingerprint/FingerprintEvent;
    const/4 v9, 0x4

    invoke-direct {v2, v9}, Lcom/samsung/android/fingerprint/FingerprintEvent;-><init>(I)V

    .line 574
    .restart local v2    # "gev":Lcom/samsung/android/fingerprint/FingerprintEvent;
    const/4 v9, 0x0

    iput v9, v2, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventResult:I

    .line 575
    iget-object v9, v2, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventData:Landroid/os/Bundle;

    const-string v10, "fingerIndex"

    iget v11, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$1;->fingerIndex:I

    invoke-virtual {v9, v10, v11}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 577
    iget-object v9, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$1;->permissionName:Ljava/lang/String;

    if-eqz v9, :cond_a

    iget-object v9, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$1;->permissionName:Ljava/lang/String;

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    if-eqz v9, :cond_a

    iget v9, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$1;->enrollMode:I

    invoke-static {v9}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintValues$Generic2Validity;->getEnrollMode(I)I

    move-result v9

    const/4 v10, 0x1

    if-ne v9, v10, :cond_a

    .line 580
    iget-object v9, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$1;->this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    # getter for: Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mDataManager:Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;
    invoke-static {v9}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->access$500(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;)Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;

    move-result-object v9

    iget-object v10, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$1;->stringUserId:Ljava/lang/String;

    invoke-static {v10}, Lcom/samsung/android/fingerprint/service/util/ValueUtils;->getIntUserId(Ljava/lang/String;)I

    move-result v10

    iget-object v11, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$1;->appName:Ljava/lang/String;

    iget v12, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$1;->fingerIndex:I

    iget-object v13, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$1;->permissionName:Ljava/lang/String;

    invoke-virtual {v9, v10, v11, v12, v13}, Lcom/samsung/android/fingerprint/service/FingerprintDBAdapter;->insert(ILjava/lang/String;ILjava/lang/String;)I

    move-result v0

    .line 582
    .local v0, "dbResult":I
    const/4 v9, 0x1

    if-eq v0, v9, :cond_a

    .line 583
    const-string v9, "ValidityFingerprintLibraryImpl"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Enroll succeed but adding permission failed!! ["

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$1;->permissionName:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "] result="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 586
    .end local v0    # "dbResult":I
    :cond_a
    invoke-virtual {p0, v2}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$1;->deliverEventToClient(Lcom/samsung/android/fingerprint/FingerprintEvent;)V

    goto/16 :goto_0

    .line 592
    :sswitch_a
    const/4 v9, 0x1

    iput-boolean v9, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$1;->isResulted:Z

    .line 594
    new-instance v2, Lcom/samsung/android/fingerprint/FingerprintEvent;

    .end local v2    # "gev":Lcom/samsung/android/fingerprint/FingerprintEvent;
    const/4 v9, 0x4

    invoke-direct {v2, v9}, Lcom/samsung/android/fingerprint/FingerprintEvent;-><init>(I)V

    .line 596
    .restart local v2    # "gev":Lcom/samsung/android/fingerprint/FingerprintEvent;
    const/4 v9, -0x1

    iput v9, v2, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventResult:I

    .line 597
    iget-object v9, v7, Lcom/validity/fingerprint/FingerprintEvent;->eventData:Ljava/lang/Object;

    if-eqz v9, :cond_d

    iget-object v9, v7, Lcom/validity/fingerprint/FingerprintEvent;->eventData:Ljava/lang/Object;

    check-cast v9, Ljava/lang/Integer;

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v9

    invoke-static {v9}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintValues$Validity2Generic;->getStatus(I)I

    move-result v9

    :goto_3
    iput v9, v2, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventStatus:I

    .line 599
    iget v9, v2, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventStatus:I

    const/4 v10, 0x7

    if-ne v9, v10, :cond_e

    .line 600
    iget-object v9, v7, Lcom/validity/fingerprint/FingerprintEvent;->eventData:Ljava/lang/Object;

    check-cast v9, Ljava/lang/Integer;

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v9

    const/16 v10, 0x168

    if-ne v9, v10, :cond_b

    .line 601
    const/4 v9, 0x1

    iput-boolean v9, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$1;->isSensorRemoved:Z

    .line 602
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$1;->isResulted:Z

    .line 603
    new-instance v2, Lcom/samsung/android/fingerprint/FingerprintEvent;

    .end local v2    # "gev":Lcom/samsung/android/fingerprint/FingerprintEvent;
    const/4 v9, 0x3

    invoke-direct {v2, v9}, Lcom/samsung/android/fingerprint/FingerprintEvent;-><init>(I)V

    .line 605
    .restart local v2    # "gev":Lcom/samsung/android/fingerprint/FingerprintEvent;
    :cond_b
    const/4 v9, 0x7

    iput v9, v2, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventStatus:I

    .line 612
    :cond_c
    :goto_4
    invoke-virtual {p0, v2}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$1;->deliverEventToClient(Lcom/samsung/android/fingerprint/FingerprintEvent;)V

    goto/16 :goto_0

    .line 597
    :cond_d
    const/16 v9, 0x10

    goto :goto_3

    .line 607
    :cond_e
    iget v9, v2, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventStatus:I

    const/16 v10, 0xc

    if-ne v9, v10, :cond_c

    .line 608
    const/16 v3, 0x2000

    .line 609
    .restart local v3    # "imageQuality":I
    iget-object v9, v2, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventData:Landroid/os/Bundle;

    const-string v10, "imageQuality"

    invoke-virtual {v9, v10, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_4

    .line 617
    .end local v3    # "imageQuality":I
    :sswitch_b
    iget-boolean v9, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$1;->isSensorRemoved:Z

    if-eqz v9, :cond_1

    .line 618
    new-instance v2, Lcom/samsung/android/fingerprint/FingerprintEvent;

    .end local v2    # "gev":Lcom/samsung/android/fingerprint/FingerprintEvent;
    const/4 v9, 0x4

    invoke-direct {v2, v9}, Lcom/samsung/android/fingerprint/FingerprintEvent;-><init>(I)V

    .line 619
    .restart local v2    # "gev":Lcom/samsung/android/fingerprint/FingerprintEvent;
    const/4 v9, 0x7

    iput v9, v2, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventStatus:I

    .line 620
    const/4 v9, 0x1

    iput-boolean v9, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$1;->isResulted:Z

    .line 621
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$1;->isSensorRemoved:Z

    .line 623
    invoke-virtual {p0, v2}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$1;->deliverEventToClient(Lcom/samsung/android/fingerprint/FingerprintEvent;)V

    goto/16 :goto_0

    .line 440
    :sswitch_data_0
    .sparse-switch
        0x3 -> :sswitch_b
        0x8 -> :sswitch_3
        0xb -> :sswitch_1
        0xd -> :sswitch_2
        0x10 -> :sswitch_4
        0x11 -> :sswitch_5
        0x20 -> :sswitch_8
        0x2a -> :sswitch_0
        0x2d -> :sswitch_6
        0x31 -> :sswitch_7
        0x1a5 -> :sswitch_9
        0x1a8 -> :sswitch_a
    .end sparse-switch
.end method

.method public requestCancel()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 393
    iget-boolean v1, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$1;->isRunning:Z

    if-eqz v1, :cond_0

    .line 394
    const-string v1, "ValidityFingerprintLibraryImpl"

    const-string v2, "cancel() in enroll.requestCancel"

    invoke-static {v1, v2}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 396
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$1;->this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    # getter for: Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mFingerprint:Lcom/samsung/android/fingerprint/service/vendor/FingerprintProxy;
    invoke-static {v1}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->access$100(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;)Lcom/samsung/android/fingerprint/service/vendor/FingerprintProxy;

    move-result-object v1

    invoke-interface {v1}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintProxy;->cancel()I

    move-result v1

    if-nez v1, :cond_0

    .line 397
    invoke-virtual {p0}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$1;->notifyCommandCompleted()V

    .line 398
    iput-boolean v0, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$1;->isRunning:Z

    .line 399
    const/4 v0, 0x1

    .line 402
    :cond_0
    return v0
.end method
