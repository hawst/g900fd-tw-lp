.class public interface abstract Lcom/samsung/android/fingerprint/service/vendor/FingerprintProxy;
.super Ljava/lang/Object;
.source "FingerprintProxy.java"


# virtual methods
.method public abstract cancel()I
.end method

.method public abstract cleanUp()I
.end method

.method public abstract enroll(Ljava/lang/String;Ljava/lang/String;I)I
.end method

.method public abstract enroll(Ljava/lang/String;Ljava/lang/String;II)I
.end method

.method public abstract getEnrolledFingerList(Ljava/lang/String;Lcom/validity/fingerprint/VcsInt;)I
.end method

.method public abstract getEnrolledTemplateIdByFinger(Ljava/lang/String;ILcom/validity/fingerprint/VcsTemplateIds;)I
.end method

.method public abstract getEnrolledTemplateIds(Ljava/lang/String;Lcom/validity/fingerprint/VcsTemplateIds;)I
.end method

.method public abstract getSensorStatus()I
.end method

.method public abstract getUserList(Lcom/validity/fingerprint/VcsStringArray;)I
.end method

.method public abstract getVersion()Ljava/lang/String;
.end method

.method public abstract identify(Ljava/lang/String;)I
.end method

.method public abstract notify(ILjava/lang/Object;)I
.end method

.method public abstract processFIDO([BLcom/validity/fingerprint/VcsByteArray;)I
.end method

.method public abstract protect([BLcom/validity/fingerprint/VcsAddInfo;Lcom/validity/fingerprint/VcsByteArray;)I
.end method

.method public abstract registerListener(Lcom/validity/fingerprint/FingerprintCore$EventListener;)I
.end method

.method public abstract removeEnrolledFinger(Ljava/lang/String;I)I
.end method

.method public abstract request(ILjava/lang/Object;)I
.end method

.method public abstract setDetectFinger(I)I
.end method

.method public abstract setPassword(Ljava/lang/String;[B)I
.end method

.method public abstract setSecurityLevel(I)I
.end method

.method public abstract unProtect([BLcom/validity/fingerprint/VcsAddInfo;Lcom/validity/fingerprint/VcsByteArray;)I
.end method

.method public abstract verifyPassword(Ljava/lang/String;[B)I
.end method
