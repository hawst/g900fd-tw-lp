.class Lcom/samsung/android/fingerprint/service/FingerprintManagerService$4;
.super Ljava/lang/Object;
.source "FingerprintManagerService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->sensorWarningToast(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/fingerprint/service/FingerprintManagerService;

.field final synthetic val$stringId:I


# direct methods
.method constructor <init>(Lcom/samsung/android/fingerprint/service/FingerprintManagerService;I)V
    .locals 0

    .prologue
    .line 1933
    iput-object p1, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService$4;->this$0:Lcom/samsung/android/fingerprint/service/FingerprintManagerService;

    iput p2, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService$4;->val$stringId:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    .line 1937
    :try_start_0
    iget-object v5, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService$4;->this$0:Lcom/samsung/android/fingerprint/service/FingerprintManagerService;

    # getter for: Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->access$1000(Lcom/samsung/android/fingerprint/service/FingerprintManagerService;)Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    iget v6, p0, Lcom/samsung/android/fingerprint/service/FingerprintManagerService$4;->val$stringId:I

    const/4 v7, 0x2

    invoke-static {v5, v6, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    .line 1938
    .local v3, "toast":Landroid/widget/Toast;
    invoke-virtual {v3}, Landroid/widget/Toast;->getView()Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 1939
    .local v1, "layout":Landroid/widget/LinearLayout;
    const/4 v5, -0x1

    invoke-virtual {v1, v5}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    .line 1940
    const/4 v5, 0x0

    invoke-virtual {v1, v5}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 1941
    .local v4, "tv":Landroid/widget/TextView;
    const/high16 v5, -0x10000

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1942
    invoke-virtual {v3, v1}, Landroid/widget/Toast;->setView(Landroid/view/View;)V

    .line 1943
    invoke-virtual {v3}, Landroid/widget/Toast;->show()V
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 1949
    .end local v1    # "layout":Landroid/widget/LinearLayout;
    .end local v3    # "toast":Landroid/widget/Toast;
    .end local v4    # "tv":Landroid/widget/TextView;
    :goto_0
    return-void

    .line 1944
    :catch_0
    move-exception v2

    .line 1945
    .local v2, "nfe":Landroid/content/res/Resources$NotFoundException;
    const-string v5, "FingerprintManagerService"

    const-string v6, "sensorWarningToast: wrong resource"

    invoke-static {v5, v6, v2}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1946
    .end local v2    # "nfe":Landroid/content/res/Resources$NotFoundException;
    :catch_1
    move-exception v0

    .line 1947
    .local v0, "e":Ljava/lang/Exception;
    const-string v5, "FingerprintManagerService"

    const-string v6, "sensorWarningToast: creation failure"

    invoke-static {v5, v6, v0}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
