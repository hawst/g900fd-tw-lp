.class public Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;
.super Ljava/lang/Object;
.source "FingerprintClientRecord.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "FingerprintClientRecord"


# instance fields
.field private mAccuracy:F

.field private mAllowIndexes:[I

.field private mAppName:Ljava/lang/String;

.field private mBackground:Z

.field private mCancelState:Z

.field private mClientSpec:Landroid/os/Bundle;

.field private mClients:Landroid/os/RemoteCallbackList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/RemoteCallbackList",
            "<",
            "Lcom/samsung/android/fingerprint/IFingerprintClient;",
            ">;"
        }
    .end annotation
.end field

.field private mCommand:Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;

.field private mComponentName:Landroid/content/ComponentName;

.field private mExtraEventDemanded:Z

.field private mIsKeyguardClient:Z

.field private mLockState:Z

.field private mOwnName:Ljava/lang/String;

.field private mPackageName:Ljava/lang/String;

.field private mPid:I

.field private mSecurityLevel:I

.field private mService:Lcom/samsung/android/fingerprint/service/FingerprintManagerService;

.field private mToken:Landroid/os/IBinder;

.field private mTokenMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Landroid/os/IBinder;",
            "Lcom/samsung/android/fingerprint/IFingerprintClient;",
            ">;"
        }
    .end annotation
.end field

.field private mUseDefaultTimeoutPolicy:Z


# direct methods
.method constructor <init>(Lcom/samsung/android/fingerprint/service/FingerprintManagerService;Landroid/os/IBinder;Landroid/os/Bundle;I)V
    .locals 2
    .param p1, "service"    # Lcom/samsung/android/fingerprint/service/FingerprintManagerService;
    .param p2, "token"    # Landroid/os/IBinder;
    .param p3, "clientSpec"    # Landroid/os/Bundle;
    .param p4, "pid"    # I

    .prologue
    const/4 v1, 0x0

    .line 93
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-boolean v1, p0, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->mCancelState:Z

    .line 49
    new-instance v0, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord$1;-><init>(Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;)V

    iput-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->mClients:Landroid/os/RemoteCallbackList;

    .line 63
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->mTokenMap:Ljava/util/Map;

    .line 65
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->mComponentName:Landroid/content/ComponentName;

    .line 72
    iput-boolean v1, p0, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->mIsKeyguardClient:Z

    .line 94
    iput-object p2, p0, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->mToken:Landroid/os/IBinder;

    .line 95
    iput-object p1, p0, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->mService:Lcom/samsung/android/fingerprint/service/FingerprintManagerService;

    .line 96
    iput p4, p0, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->mPid:I

    .line 98
    invoke-virtual {p0, p3}, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->setClientSpecFromBundle(Landroid/os/Bundle;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 99
    new-instance v0, Ljava/lang/ExceptionInInitializerError;

    const-string v1, "setClientSpecFromBundle failed."

    invoke-direct {v0, v1}, Ljava/lang/ExceptionInInitializerError;-><init>(Ljava/lang/String;)V

    throw v0

    .line 101
    :cond_0
    return-void
.end method

.method constructor <init>(Lcom/samsung/android/fingerprint/service/FingerprintManagerService;Lcom/samsung/android/fingerprint/IFingerprintClient;Landroid/os/Bundle;I)V
    .locals 1
    .param p1, "service"    # Lcom/samsung/android/fingerprint/service/FingerprintManagerService;
    .param p2, "client"    # Lcom/samsung/android/fingerprint/IFingerprintClient;
    .param p3, "clientSpec"    # Landroid/os/Bundle;
    .param p4, "pid"    # I

    .prologue
    .line 85
    if-eqz p2, :cond_1

    invoke-interface {p2}, Lcom/samsung/android/fingerprint/IFingerprintClient;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    :goto_0
    invoke-direct {p0, p1, v0, p3, p4}, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;-><init>(Lcom/samsung/android/fingerprint/service/FingerprintManagerService;Landroid/os/IBinder;Landroid/os/Bundle;I)V

    .line 87
    if-eqz p2, :cond_0

    .line 88
    invoke-virtual {p0, p2}, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->register(Lcom/samsung/android/fingerprint/IFingerprintClient;)Z

    .line 90
    :cond_0
    return-void

    .line 85
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;)Landroid/os/IBinder;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->mToken:Landroid/os/IBinder;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;)Lcom/samsung/android/fingerprint/service/FingerprintManagerService;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->mService:Lcom/samsung/android/fingerprint/service/FingerprintManagerService;

    return-object v0
.end method

.method private combineAttrInfo(Ljava/lang/String;ZLjava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "prevAttr"    # Ljava/lang/String;
    .param p2, "cond"    # Z
    .param p3, "value"    # Ljava/lang/String;

    .prologue
    .line 367
    if-nez p2, :cond_0

    .line 374
    .end local p1    # "prevAttr":Ljava/lang/String;
    :goto_0
    return-object p1

    .line 371
    .restart local p1    # "prevAttr":Ljava/lang/String;
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    move-object p1, p3

    .line 372
    goto :goto_0

    .line 374
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0
.end method


# virtual methods
.method public cleanUp()V
    .locals 6

    .prologue
    .line 265
    iget-object v4, p0, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->mClients:Landroid/os/RemoteCallbackList;

    monitor-enter v4

    .line 266
    :try_start_0
    iget-object v3, p0, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->mClients:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v0

    .line 268
    .local v0, "N":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v0, :cond_1

    .line 269
    iget-object v3, p0, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->mClients:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3, v2}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/fingerprint/IFingerprintClient;

    .line 271
    .local v1, "client":Lcom/samsung/android/fingerprint/IFingerprintClient;
    iget-object v3, p0, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->mTokenMap:Ljava/util/Map;

    if-eqz v3, :cond_0

    .line 272
    iget-object v3, p0, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->mTokenMap:Ljava/util/Map;

    invoke-interface {v1}, Lcom/samsung/android/fingerprint/IFingerprintClient;->asBinder()Landroid/os/IBinder;

    move-result-object v5

    invoke-interface {v3, v5}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 268
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 275
    .end local v1    # "client":Lcom/samsung/android/fingerprint/IFingerprintClient;
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->mClients:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    .line 281
    iget-object v3, p0, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->mClients:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->kill()V

    .line 282
    monitor-exit v4

    .line 283
    return-void

    .line 282
    .end local v0    # "N":I
    .end local v2    # "i":I
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3
.end method

.method public closeTransaction()V
    .locals 1

    .prologue
    .line 290
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->mLockState:Z

    .line 291
    return-void
.end method

.method public doesUseDefaultTimeoutPolicy()Z
    .locals 1

    .prologue
    .line 334
    iget-boolean v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->mUseDefaultTimeoutPolicy:Z

    return v0
.end method

.method dump(Ljava/io/PrintWriter;)V
    .locals 8
    .param p1, "pw"    # Ljava/io/PrintWriter;

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 379
    const-string v1, ""

    .line 381
    .local v1, "attrInfo":Ljava/lang/String;
    iget-boolean v4, p0, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->mExtraEventDemanded:Z

    const-string v7, "ext"

    invoke-direct {p0, v1, v4, v7}, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->combineAttrInfo(Ljava/lang/String;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 382
    iget-boolean v4, p0, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->mLockState:Z

    const-string v7, "lock"

    invoke-direct {p0, v1, v4, v7}, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->combineAttrInfo(Ljava/lang/String;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 383
    iget-boolean v4, p0, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->mUseDefaultTimeoutPolicy:Z

    const-string v7, "deftime"

    invoke-direct {p0, v1, v4, v7}, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->combineAttrInfo(Ljava/lang/String;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 384
    iget v4, p0, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->mSecurityLevel:I

    const/4 v7, 0x2

    if-ne v4, v7, :cond_0

    move v4, v5

    :goto_0
    const-string v7, "sl:high"

    invoke-direct {p0, v1, v4, v7}, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->combineAttrInfo(Ljava/lang/String;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 385
    iget v4, p0, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->mSecurityLevel:I

    const/4 v7, 0x3

    if-ne v4, v7, :cond_1

    :goto_1
    const-string v4, "sl:veryhigh"

    invoke-direct {p0, v1, v5, v4}, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->combineAttrInfo(Ljava/lang/String;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 387
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Client Record { appName="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->mAppName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", pid="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->mPid:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", hashCode="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", attr="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " }"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 390
    const-string v4, "FingerprintClientRecord"

    const-string v5, "dump: wait to lock."

    invoke-static {v4, v5}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->lock(Ljava/lang/String;Ljava/lang/String;)V

    .line 392
    iget-object v5, p0, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->mClients:Landroid/os/RemoteCallbackList;

    monitor-enter v5

    .line 393
    :try_start_0
    const-string v4, "FingerprintClientRecord"

    const-string v6, "dump: locked."

    invoke-static {v4, v6}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->lock(Ljava/lang/String;Ljava/lang/String;)V

    .line 395
    iget-object v4, p0, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->mClients:Landroid/os/RemoteCallbackList;

    invoke-virtual {v4}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v0

    .line 397
    .local v0, "N":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_2
    if-ge v3, v0, :cond_2

    .line 398
    iget-object v4, p0, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->mClients:Landroid/os/RemoteCallbackList;

    invoke-virtual {v4, v3}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/fingerprint/IFingerprintClient;

    .line 399
    .local v2, "client":Lcom/samsung/android/fingerprint/IFingerprintClient;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "    Callback : "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v2}, Lcom/samsung/android/fingerprint/service/util/ClientInfoHelper;->simplifyObjectInfo(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, "/"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {v2}, Lcom/samsung/android/fingerprint/IFingerprintClient;->asBinder()Landroid/os/IBinder;

    move-result-object v6

    invoke-static {v6}, Lcom/samsung/android/fingerprint/service/util/ClientInfoHelper;->simplifyObjectInfo(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 397
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .end local v0    # "N":I
    .end local v2    # "client":Lcom/samsung/android/fingerprint/IFingerprintClient;
    .end local v3    # "i":I
    :cond_0
    move v4, v6

    .line 384
    goto/16 :goto_0

    :cond_1
    move v5, v6

    .line 385
    goto/16 :goto_1

    .line 402
    .restart local v0    # "N":I
    .restart local v3    # "i":I
    :cond_2
    iget-object v4, p0, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->mClients:Landroid/os/RemoteCallbackList;

    invoke-virtual {v4}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    .line 404
    const-string v4, "FingerprintClientRecord"

    const-string v6, "dump: released."

    invoke-static {v4, v6}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->lock(Ljava/lang/String;Ljava/lang/String;)V

    .line 405
    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 407
    invoke-virtual {p1}, Ljava/io/PrintWriter;->println()V

    .line 408
    return-void

    .line 405
    .end local v0    # "N":I
    .end local v3    # "i":I
    :catchall_0
    move-exception v4

    :try_start_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v4
.end method

.method public getAccuracy()F
    .locals 1

    .prologue
    .line 363
    iget v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->mAccuracy:F

    return v0
.end method

.method public getAllowIndexes()[I
    .locals 1

    .prologue
    .line 359
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->mAllowIndexes:[I

    return-object v0
.end method

.method public getAppName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 310
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->mAppName:Ljava/lang/String;

    return-object v0
.end method

.method public getClientSpec()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->mClientSpec:Landroid/os/Bundle;

    return-object v0
.end method

.method public getCommand()Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->mCommand:Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;

    return-object v0
.end method

.method public getComponentName()Landroid/content/ComponentName;
    .locals 1

    .prologue
    .line 322
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->mComponentName:Landroid/content/ComponentName;

    return-object v0
.end method

.method public getOwnName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 355
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->mOwnName:Ljava/lang/String;

    return-object v0
.end method

.method public getPackageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 314
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->mPackageName:Ljava/lang/String;

    return-object v0
.end method

.method public getSecurityLevel()I
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/high16 v4, 0x42be0000    # 95.0f

    const/high16 v3, 0x42b40000    # 90.0f

    const/high16 v2, 0x42aa0000    # 85.0f

    .line 338
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->mOwnName:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->mOwnName:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 339
    const/high16 v0, 0x42a00000    # 80.0f

    iget v1, p0, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->mAccuracy:F

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_1

    iget v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->mAccuracy:F

    cmpg-float v0, v0, v2

    if-gez v0, :cond_1

    .line 340
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->mSecurityLevel:I

    .line 351
    :cond_0
    :goto_0
    iget v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->mSecurityLevel:I

    return v0

    .line 341
    :cond_1
    iget v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->mAccuracy:F

    cmpg-float v0, v2, v0

    if-gtz v0, :cond_2

    iget v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->mAccuracy:F

    cmpg-float v0, v0, v3

    if-gez v0, :cond_2

    .line 342
    const/4 v0, 0x1

    iput v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->mSecurityLevel:I

    goto :goto_0

    .line 343
    :cond_2
    iget v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->mAccuracy:F

    cmpg-float v0, v3, v0

    if-gtz v0, :cond_3

    iget v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->mAccuracy:F

    cmpg-float v0, v0, v4

    if-gez v0, :cond_3

    .line 344
    iput v5, p0, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->mSecurityLevel:I

    goto :goto_0

    .line 345
    :cond_3
    iget v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->mAccuracy:F

    cmpg-float v0, v4, v0

    if-gtz v0, :cond_4

    iget v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->mAccuracy:F

    const/high16 v1, 0x42c80000    # 100.0f

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_4

    .line 346
    const/4 v0, 0x3

    iput v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->mSecurityLevel:I

    goto :goto_0

    .line 348
    :cond_4
    iput v5, p0, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->mSecurityLevel:I

    goto :goto_0
.end method

.method public isBackground()Z
    .locals 1

    .prologue
    .line 326
    iget-boolean v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->mBackground:Z

    return v0
.end method

.method public isExtraEventDemanded()Z
    .locals 1

    .prologue
    .line 330
    iget-boolean v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->mExtraEventDemanded:Z

    return v0
.end method

.method public isKeyguardClient()Z
    .locals 1

    .prologue
    .line 302
    iget-boolean v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->mIsKeyguardClient:Z

    return v0
.end method

.method public isLocked()Z
    .locals 1

    .prologue
    .line 294
    iget-boolean v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->mLockState:Z

    return v0
.end method

.method public onCommandCompleted(Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;)V
    .locals 3
    .param p1, "command"    # Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;

    .prologue
    .line 154
    const-string v0, "FingerprintClientRecord"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onCommandCompleted: command="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;->commandName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 156
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->mCommand:Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;

    if-eq v0, p1, :cond_0

    .line 157
    const-string v0, "FingerprintClientRecord"

    const-string v1, "onCommandCompleted: command is not currentCommand!!!"

    invoke-static {v0, v1}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 178
    :goto_0
    return-void

    .line 160
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->mCommand:Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;

    .line 161
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->mService:Lcom/samsung/android/fingerprint/service/FingerprintManagerService;

    invoke-virtual {v0}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->updateFocusedClient()V

    goto :goto_0
.end method

.method public onEvent(Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;Lcom/samsung/android/fingerprint/FingerprintEvent;)V
    .locals 9
    .param p1, "command"    # Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;
    .param p2, "evt"    # Lcom/samsung/android/fingerprint/FingerprintEvent;

    .prologue
    .line 189
    if-nez p1, :cond_0

    const/4 v2, 0x0

    .line 191
    .local v2, "commandName":Ljava/lang/String;
    :goto_0
    if-nez p2, :cond_1

    .line 192
    const-string v5, "FingerprintClientRecord"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "onEvent: commandName="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", eventId=null"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 238
    :goto_1
    return-void

    .line 189
    .end local v2    # "commandName":Ljava/lang/String;
    :cond_0
    iget-object v2, p1, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;->commandName:Ljava/lang/String;

    goto :goto_0

    .line 195
    .restart local v2    # "commandName":Ljava/lang/String;
    :cond_1
    sget-boolean v5, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->DEBUG:Z

    if-eqz v5, :cond_3

    .line 196
    const-string v5, "FingerprintClientRecord"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "onEvent: commandName="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", eventId="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p2}, Lcom/samsung/android/fingerprint/FingerprintEvent;->getEventIdName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", eventResult="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p2}, Lcom/samsung/android/fingerprint/FingerprintEvent;->getEventResultName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", eventStatus="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p2}, Lcom/samsung/android/fingerprint/FingerprintEvent;->getEventStatusName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 204
    :goto_2
    const-string v5, "FingerprintClientRecord"

    const-string v6, "onEvent: wait to lock."

    invoke-static {v5, v6}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->lock(Ljava/lang/String;Ljava/lang/String;)V

    .line 206
    iget-object v6, p0, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->mClients:Landroid/os/RemoteCallbackList;

    monitor-enter v6

    .line 207
    :try_start_0
    const-string v5, "FingerprintClientRecord"

    const-string v7, "onEvent: locked."

    invoke-static {v5, v7}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->lock(Ljava/lang/String;Ljava/lang/String;)V

    .line 211
    iget-object v5, p0, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->mClients:Landroid/os/RemoteCallbackList;

    invoke-virtual {v5}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v0

    .line 213
    .local v0, "N":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_3
    if-ge v4, v0, :cond_4

    .line 214
    iget-object v5, p0, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->mClients:Landroid/os/RemoteCallbackList;

    invoke-virtual {v5, v4}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/fingerprint/IFingerprintClient;

    .line 216
    .local v1, "client":Lcom/samsung/android/fingerprint/IFingerprintClient;
    const-string v5, "FingerprintClientRecord"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Calling IFingerprintClient: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {v1}, Lcom/samsung/android/fingerprint/service/util/ClientInfoHelper;->simplifyObjectInfo(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " pid="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p0, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->mPid:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 222
    :try_start_1
    iget-boolean v5, p0, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->mCancelState:Z

    if-nez v5, :cond_2

    .line 223
    invoke-interface {v1, p2}, Lcom/samsung/android/fingerprint/IFingerprintClient;->onFingerprintEvent(Lcom/samsung/android/fingerprint/FingerprintEvent;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 213
    :cond_2
    :goto_4
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    .line 199
    .end local v0    # "N":I
    .end local v1    # "client":Lcom/samsung/android/fingerprint/IFingerprintClient;
    .end local v4    # "i":I
    :cond_3
    const-string v5, "FingerprintClientRecord"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "onEvent: commandName="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p2, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventId:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p2, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventResult:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p2, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventStatus:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->da(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 225
    .restart local v0    # "N":I
    .restart local v1    # "client":Lcom/samsung/android/fingerprint/IFingerprintClient;
    .restart local v4    # "i":I
    :catch_0
    move-exception v3

    .line 226
    .local v3, "e":Landroid/os/RemoteException;
    :try_start_2
    const-string v5, "FingerprintClientRecord"

    const-string v7, "Calling IFingerprintClient failed : "

    invoke-static {v5, v7, v3}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_4

    .line 237
    .end local v0    # "N":I
    .end local v1    # "client":Lcom/samsung/android/fingerprint/IFingerprintClient;
    .end local v3    # "e":Landroid/os/RemoteException;
    .end local v4    # "i":I
    :catchall_0
    move-exception v5

    monitor-exit v6
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v5

    .line 234
    .restart local v0    # "N":I
    .restart local v4    # "i":I
    :cond_4
    :try_start_3
    iget-object v5, p0, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->mClients:Landroid/os/RemoteCallbackList;

    invoke-virtual {v5}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    .line 236
    const-string v5, "FingerprintClientRecord"

    const-string v7, "onEvent: released."

    invoke-static {v5, v7}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->lock(Ljava/lang/String;Ljava/lang/String;)V

    .line 237
    monitor-exit v6
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_1
.end method

.method public openTransaction()V
    .locals 1

    .prologue
    .line 286
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->mLockState:Z

    .line 287
    return-void
.end method

.method public register(Lcom/samsung/android/fingerprint/IFingerprintClient;)Z
    .locals 3
    .param p1, "client"    # Lcom/samsung/android/fingerprint/IFingerprintClient;

    .prologue
    .line 241
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->mClients:Landroid/os/RemoteCallbackList;

    monitor-enter v1

    .line 242
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->mClients:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0, p1}, Landroid/os/RemoteCallbackList;->register(Landroid/os/IInterface;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 243
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->mTokenMap:Ljava/util/Map;

    invoke-interface {p1}, Lcom/samsung/android/fingerprint/IFingerprintClient;->asBinder()Landroid/os/IBinder;

    move-result-object v2

    invoke-interface {v0, v2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 245
    const/4 v0, 0x1

    monitor-exit v1

    .line 248
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    monitor-exit v1

    goto :goto_0

    .line 249
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setCancelState(Z)V
    .locals 0
    .param p1, "flag"    # Z

    .prologue
    .line 104
    iput-boolean p1, p0, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->mCancelState:Z

    .line 105
    return-void
.end method

.method public setClientSpecFromBundle(Landroid/os/Bundle;)Z
    .locals 4
    .param p1, "clientSpec"    # Landroid/os/Bundle;

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 111
    if-nez p1, :cond_0

    .line 142
    :goto_0
    return v2

    .line 115
    :cond_0
    iput-object p1, p0, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->mClientSpec:Landroid/os/Bundle;

    .line 116
    const-string v0, "appName"

    invoke-virtual {p1, v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->mAppName:Ljava/lang/String;

    .line 117
    const-string v0, "packageName"

    invoke-virtual {p1, v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->mPackageName:Ljava/lang/String;

    .line 118
    const-string v0, "background"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->mBackground:Z

    .line 119
    const-string v0, "useManualTimeout"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->mUseDefaultTimeoutPolicy:Z

    .line 120
    const-string v0, "demandExtraEvent"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->mExtraEventDemanded:Z

    .line 121
    const-string v0, "securityLevel"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->mSecurityLevel:I

    .line 122
    const-string v0, "ownName"

    invoke-virtual {p1, v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->mOwnName:Ljava/lang/String;

    .line 123
    const-string v0, "request_template_index_list"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getIntArray(Ljava/lang/String;)[I

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->mAllowIndexes:[I

    .line 124
    const-string v0, "request_accuracy"

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;F)F

    move-result v0

    iput v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->mAccuracy:F

    .line 126
    const-string v0, "FingerprintClientRecord"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setClientSpecFromBundle: mAppName: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->mAppName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", Background="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->mBackground:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", UseDefaultTimeoutPolicy="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->mUseDefaultTimeoutPolicy:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", ExtraEventDemanded="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->mExtraEventDemanded:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", SecurityLevel="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->mSecurityLevel:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    const-string v0, "FingerprintClientRecord"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setClientSpecFromBundle: OwnName="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->mOwnName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", AllowIndexes="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->mAllowIndexes:[I

    invoke-static {v3}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", Accuracy="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->mAccuracy:F

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v2, v1

    .line 142
    goto/16 :goto_0

    :cond_1
    move v0, v2

    .line 119
    goto/16 :goto_1
.end method

.method public setCommand(Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;)V
    .locals 0
    .param p1, "cmd"    # Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;

    .prologue
    .line 150
    iput-object p1, p0, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->mCommand:Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;

    .line 151
    return-void
.end method

.method public setComponentName(Landroid/content/ComponentName;)V
    .locals 0
    .param p1, "componentName"    # Landroid/content/ComponentName;

    .prologue
    .line 318
    iput-object p1, p0, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->mComponentName:Landroid/content/ComponentName;

    .line 319
    return-void
.end method

.method public setKeyguardClient()V
    .locals 1

    .prologue
    .line 298
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->mIsKeyguardClient:Z

    .line 299
    return-void
.end method

.method public toLongString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 417
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ClientRecord { appName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->mAppName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", pid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->mPid:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", bg="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->mBackground:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", extra="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->mExtraEventDemanded:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", lock="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->mLockState:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", defaultTimeoutPolicy="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->mUseDefaultTimeoutPolicy:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " }"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 412
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ClientRecord { appName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->mAppName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", pid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->mPid:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", hashCode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " }"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public unregister(Landroid/os/IBinder;)Z
    .locals 3
    .param p1, "token"    # Landroid/os/IBinder;

    .prologue
    .line 253
    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->mClients:Landroid/os/RemoteCallbackList;

    monitor-enter v2

    .line 254
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->mTokenMap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 255
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->mTokenMap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/fingerprint/IFingerprintClient;

    .line 257
    .local v0, "client":Lcom/samsung/android/fingerprint/IFingerprintClient;
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->mClients:Landroid/os/RemoteCallbackList;

    invoke-virtual {v1, v0}, Landroid/os/RemoteCallbackList;->unregister(Landroid/os/IInterface;)Z

    move-result v1

    monitor-exit v2

    .line 260
    .end local v0    # "client":Lcom/samsung/android/fingerprint/IFingerprintClient;
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    monitor-exit v2

    goto :goto_0

    .line 261
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
