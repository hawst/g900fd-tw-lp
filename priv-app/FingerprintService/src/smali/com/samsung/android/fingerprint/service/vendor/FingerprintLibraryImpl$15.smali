.class Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$15;
.super Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$ImmediateCommand;
.source "FingerprintLibraryImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;


# direct methods
.method constructor <init>(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p2, "x0"    # Ljava/lang/String;
    .param p3, "x1"    # Ljava/lang/String;

    .prologue
    .line 1426
    iput-object p1, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$15;->this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$ImmediateCommand;-><init>(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public doCommand()Ljava/lang/Object;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 1430
    iget v3, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$15;->notifyCondition:I

    invoke-static {v3}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintValues$Generic2Validity;->getNotifyType(I)I

    move-result v0

    .line 1431
    .local v0, "notiType":I
    sget-boolean v3, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->DEBUG:Z

    if-eqz v3, :cond_2

    invoke-static {v0}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintValues$Validity2Generic;->getNotifyName(I)Ljava/lang/String;

    move-result-object v1

    .line 1433
    .local v1, "notifyName":Ljava/lang/String;
    :goto_0
    const-string v3, "ValidityFingerprintLibraryImpl"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "notify(notifyCondition="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->da(Ljava/lang/String;Ljava/lang/String;)V

    .line 1435
    iget-object v3, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$15;->this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    # getter for: Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mFingerprintLog:Lcom/samsung/android/fingerprint/service/util/FingerprintLog;
    invoke-static {v3}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->access$000(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;)Lcom/samsung/android/fingerprint/service/util/FingerprintLog;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 1436
    iget-object v3, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$15;->this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    # getter for: Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mFingerprintLog:Lcom/samsung/android/fingerprint/service/util/FingerprintLog;
    invoke-static {v3}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->access$000(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;)Lcom/samsung/android/fingerprint/service/util/FingerprintLog;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->createLogFile()V

    .line 1437
    iget-object v3, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$15;->this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    # getter for: Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mFingerprintLog:Lcom/samsung/android/fingerprint/service/util/FingerprintLog;
    invoke-static {v3}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->access$000(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;)Lcom/samsung/android/fingerprint/service/util/FingerprintLog;

    move-result-object v3

    invoke-virtual {p0}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$15;->dumpArgumentValues()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->logToFile(Ljava/lang/String;)V

    .line 1441
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$15;->this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    # getter for: Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mFingerprint:Lcom/samsung/android/fingerprint/service/vendor/FingerprintProxy;
    invoke-static {v3}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->access$100(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;)Lcom/samsung/android/fingerprint/service/vendor/FingerprintProxy;

    move-result-object v3

    invoke-interface {v3}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintProxy;->cancel()I

    .line 1443
    iget-object v3, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$15;->this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    # getter for: Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mFingerprint:Lcom/samsung/android/fingerprint/service/vendor/FingerprintProxy;
    invoke-static {v3}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->access$100(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;)Lcom/samsung/android/fingerprint/service/vendor/FingerprintProxy;

    move-result-object v3

    const/4 v4, 0x0

    invoke-interface {v3, v0, v4}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintProxy;->notify(ILjava/lang/Object;)I

    move-result v2

    .line 1445
    .local v2, "result":I
    if-nez v2, :cond_1

    .line 1446
    packed-switch v0, :pswitch_data_0

    .line 1462
    :cond_1
    :goto_1
    invoke-static {v2}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintValues$Validity2Generic;->getResult(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    return-object v3

    .line 1431
    .end local v1    # "notifyName":Ljava/lang/String;
    .end local v2    # "result":I
    :cond_2
    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 1449
    .restart local v1    # "notifyName":Ljava/lang/String;
    .restart local v2    # "result":I
    :pswitch_0
    iget-object v3, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$15;->this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    # setter for: Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mIsVerifiedInEnrollSession:Z
    invoke-static {v3, v6}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->access$1102(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;Z)Z

    .line 1450
    iget-object v3, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$15;->this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    const/4 v4, 0x1

    # setter for: Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mIsEnrollSession:Z
    invoke-static {v3, v4}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->access$1002(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;Z)Z

    goto :goto_1

    .line 1454
    :pswitch_1
    iget-object v3, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$15;->this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    # setter for: Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mIsVerifiedInEnrollSession:Z
    invoke-static {v3, v6}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->access$1102(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;Z)Z

    .line 1455
    iget-object v3, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$15;->this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    # setter for: Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mIsEnrollSession:Z
    invoke-static {v3, v6}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->access$1002(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;Z)Z

    goto :goto_1

    .line 1446
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
