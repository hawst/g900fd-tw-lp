.class Lcom/samsung/android/fingerprint/service/vendor/FingerprintProxyFactory$ProxyInvocationHandler;
.super Ljava/lang/Object;
.source "FingerprintProxyFactory.java"

# interfaces
.implements Ljava/lang/reflect/InvocationHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/fingerprint/service/vendor/FingerprintProxyFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ProxyInvocationHandler"
.end annotation


# instance fields
.field private methodMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/reflect/Method;",
            ">;"
        }
    .end annotation
.end field

.field private targetObject:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Ljava/lang/Object;)V
    .locals 9
    .param p1, "target"    # Ljava/lang/Object;

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    new-instance v8, Ljava/util/HashMap;

    invoke-direct {v8}, Ljava/util/HashMap;-><init>()V

    iput-object v8, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintProxyFactory$ProxyInvocationHandler;->methodMap:Ljava/util/Map;

    .line 51
    iput-object p1, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintProxyFactory$ProxyInvocationHandler;->targetObject:Ljava/lang/Object;

    .line 52
    const-class v8, Lcom/samsung/android/fingerprint/service/vendor/FingerprintProxy;

    invoke-virtual {v8}, Ljava/lang/Class;->getMethods()[Ljava/lang/reflect/Method;

    move-result-object v4

    .line 53
    .local v4, "proxyMethods":[Ljava/lang/reflect/Method;
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Class;->getMethods()[Ljava/lang/reflect/Method;

    move-result-object v7

    .line 55
    .local v7, "targetMethods":[Ljava/lang/reflect/Method;
    move-object v0, v4

    .local v0, "arr$":[Ljava/lang/reflect/Method;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 57
    .local v3, "method":Ljava/lang/reflect/Method;
    invoke-direct {p0, v3}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintProxyFactory$ProxyInvocationHandler;->getMethodSignature(Ljava/lang/reflect/Method;)Ljava/lang/String;

    move-result-object v5

    .line 58
    .local v5, "signature":Ljava/lang/String;
    invoke-direct {p0, v7, v5}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintProxyFactory$ProxyInvocationHandler;->findMatchMethod([Ljava/lang/reflect/Method;Ljava/lang/String;)Ljava/lang/reflect/Method;

    move-result-object v6

    .line 60
    .local v6, "targetMethod":Ljava/lang/reflect/Method;
    if-eqz v6, :cond_0

    .line 61
    iget-object v8, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintProxyFactory$ProxyInvocationHandler;->methodMap:Ljava/util/Map;

    invoke-interface {v8, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 55
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 64
    .end local v3    # "method":Ljava/lang/reflect/Method;
    .end local v5    # "signature":Ljava/lang/String;
    .end local v6    # "targetMethod":Ljava/lang/reflect/Method;
    :cond_1
    return-void
.end method

.method private findMatchMethod([Ljava/lang/reflect/Method;Ljava/lang/String;)Ljava/lang/reflect/Method;
    .locals 6
    .param p1, "targetMethods"    # [Ljava/lang/reflect/Method;
    .param p2, "targetSignature"    # Ljava/lang/String;

    .prologue
    .line 122
    if-eqz p2, :cond_1

    .line 123
    move-object v0, p1

    .local v0, "arr$":[Ljava/lang/reflect/Method;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 124
    .local v3, "method":Ljava/lang/reflect/Method;
    invoke-direct {p0, v3}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintProxyFactory$ProxyInvocationHandler;->getMethodSignature(Ljava/lang/reflect/Method;)Ljava/lang/String;

    move-result-object v4

    .line 126
    .local v4, "signature":Ljava/lang/String;
    invoke-virtual {p2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 132
    .end local v0    # "arr$":[Ljava/lang/reflect/Method;
    .end local v1    # "i$":I
    .end local v2    # "len$":I
    .end local v3    # "method":Ljava/lang/reflect/Method;
    .end local v4    # "signature":Ljava/lang/String;
    :goto_1
    return-object v3

    .line 123
    .restart local v0    # "arr$":[Ljava/lang/reflect/Method;
    .restart local v1    # "i$":I
    .restart local v2    # "len$":I
    .restart local v3    # "method":Ljava/lang/reflect/Method;
    .restart local v4    # "signature":Ljava/lang/String;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 132
    .end local v0    # "arr$":[Ljava/lang/reflect/Method;
    .end local v1    # "i$":I
    .end local v2    # "len$":I
    .end local v3    # "method":Ljava/lang/reflect/Method;
    .end local v4    # "signature":Ljava/lang/String;
    :cond_1
    const/4 v3, 0x0

    goto :goto_1
.end method

.method private getMethodSignature(Ljava/lang/reflect/Method;)Ljava/lang/String;
    .locals 8
    .param p1, "method"    # Ljava/lang/reflect/Method;

    .prologue
    .line 136
    if-eqz p1, :cond_2

    .line 137
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 139
    .local v1, "builder":Ljava/lang/StringBuilder;
    invoke-virtual {p1}, Ljava/lang/reflect/Method;->getReturnType()Ljava/lang/Class;

    move-result-object v2

    .line 141
    .local v2, "cls":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 142
    const/16 v7, 0x20

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 143
    invoke-virtual {p1}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 144
    const/16 v7, 0x28

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 146
    const/4 v3, 0x1

    .line 147
    .local v3, "first":Z
    invoke-virtual {p1}, Ljava/lang/reflect/Method;->getParameterTypes()[Ljava/lang/Class;

    move-result-object v0

    .local v0, "arr$":[Ljava/lang/Class;
    array-length v5, v0

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v5, :cond_1

    aget-object v6, v0, v4

    .line 148
    .local v6, "param":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    if-eqz v3, :cond_0

    .line 149
    const/4 v3, 0x0

    .line 153
    :goto_1
    invoke-virtual {v6}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 147
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 151
    :cond_0
    const/16 v7, 0x2c

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 155
    .end local v6    # "param":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :cond_1
    const/16 v7, 0x29

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 157
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 159
    .end local v0    # "arr$":[Ljava/lang/Class;
    .end local v1    # "builder":Ljava/lang/StringBuilder;
    .end local v2    # "cls":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v3    # "first":Z
    .end local v4    # "i$":I
    .end local v5    # "len$":I
    :goto_2
    return-object v7

    :cond_2
    const/4 v7, 0x0

    goto :goto_2
.end method

.method private logParameters(Ljava/lang/String;Ljava/lang/reflect/Method;[Ljava/lang/Object;)V
    .locals 8
    .param p1, "tag"    # Ljava/lang/String;
    .param p2, "method"    # Ljava/lang/reflect/Method;
    .param p3, "args"    # [Ljava/lang/Object;

    .prologue
    .line 67
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 69
    .local v2, "builder":Ljava/lang/StringBuilder;
    if-eqz p2, :cond_0

    .line 70
    invoke-virtual {p2}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 73
    :cond_0
    if-eqz p1, :cond_1

    .line 74
    const/16 v6, 0x20

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 77
    :cond_1
    if-eqz p3, :cond_4

    array-length v6, p3

    if-eqz v6, :cond_4

    .line 78
    const-string v6, ": ("

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 80
    const/4 v3, 0x1

    .line 82
    .local v3, "first":Z
    move-object v1, p3

    .local v1, "arr$":[Ljava/lang/Object;
    array-length v5, v1

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v5, :cond_3

    aget-object v0, v1, v4

    .line 83
    .local v0, "arg":Ljava/lang/Object;
    if-eqz v3, :cond_2

    .line 84
    const/4 v3, 0x0

    .line 89
    :goto_1
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 82
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 86
    :cond_2
    const-string v6, ", "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 92
    .end local v0    # "arg":Ljava/lang/Object;
    :cond_3
    const/16 v6, 0x29

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 95
    .end local v1    # "arr$":[Ljava/lang/Object;
    .end local v3    # "first":Z
    .end local v4    # "i$":I
    .end local v5    # "len$":I
    :cond_4
    const-string v6, "ValidityFingerprintProxyFactory"

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    return-void
.end method


# virtual methods
.method public invoke(Ljava/lang/Object;Ljava/lang/reflect/Method;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 10
    .param p1, "proxy"    # Ljava/lang/Object;
    .param p2, "method"    # Ljava/lang/reflect/Method;
    .param p3, "args"    # [Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 103
    invoke-direct {p0, p2}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintProxyFactory$ProxyInvocationHandler;->getMethodSignature(Ljava/lang/reflect/Method;)Ljava/lang/String;

    move-result-object v5

    .line 104
    .local v5, "signature":Ljava/lang/String;
    iget-object v7, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintProxyFactory$ProxyInvocationHandler;->methodMap:Ljava/util/Map;

    invoke-interface {v7, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/reflect/Method;

    .line 106
    .local v6, "targetMethod":Ljava/lang/reflect/Method;
    const-string v7, "BEGIN"

    invoke-direct {p0, v7, p2, p3}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintProxyFactory$ProxyInvocationHandler;->logParameters(Ljava/lang/String;Ljava/lang/reflect/Method;[Ljava/lang/Object;)V

    .line 108
    if-eqz v6, :cond_1

    .line 109
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 110
    .local v0, "begin":J
    iget-object v7, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintProxyFactory$ProxyInvocationHandler;->targetObject:Ljava/lang/Object;

    invoke-virtual {v6, v7, p3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    .line 111
    .local v4, "result":Ljava/lang/Object;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    sub-long v2, v8, v0

    .line 113
    .local v2, "period":J
    const-string v8, "ValidityFingerprintProxyFactory"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v7

    :goto_0
    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v9, " FINISH ("

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v9, "ms) RESULT: "

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v8, v7}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    return-object v4

    .line 113
    :cond_0
    const-string v7, ""

    goto :goto_0

    .line 118
    .end local v0    # "begin":J
    .end local v2    # "period":J
    .end local v4    # "result":Ljava/lang/Object;
    :cond_1
    new-instance v7, Ljava/lang/IllegalStateException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Cannot find the method: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v7
.end method
