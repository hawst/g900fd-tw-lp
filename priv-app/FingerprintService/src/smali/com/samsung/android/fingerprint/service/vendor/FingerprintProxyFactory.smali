.class public Lcom/samsung/android/fingerprint/service/vendor/FingerprintProxyFactory;
.super Ljava/lang/Object;
.source "FingerprintProxyFactory.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/fingerprint/service/vendor/FingerprintProxyFactory$ProxyInvocationHandler;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "ValidityFingerprintProxyFactory"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    return-void
.end method

.method public static create(Ljava/lang/Object;)Lcom/samsung/android/fingerprint/service/vendor/FingerprintProxy;
    .locals 6
    .param p0, "target"    # Ljava/lang/Object;

    .prologue
    .line 34
    new-instance v1, Lcom/samsung/android/fingerprint/service/vendor/FingerprintProxyFactory$ProxyInvocationHandler;

    invoke-direct {v1, p0}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintProxyFactory$ProxyInvocationHandler;-><init>(Ljava/lang/Object;)V

    .line 36
    .local v1, "handler":Ljava/lang/reflect/InvocationHandler;
    const-class v3, Lcom/samsung/android/fingerprint/service/vendor/FingerprintProxyFactory;

    invoke-virtual {v3}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    .line 38
    .local v0, "classLoader":Ljava/lang/ClassLoader;
    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Class;

    const/4 v4, 0x0

    const-class v5, Lcom/samsung/android/fingerprint/service/vendor/FingerprintProxy;

    aput-object v5, v3, v4

    invoke-static {v0, v3, v1}, Ljava/lang/reflect/Proxy;->newProxyInstance(Ljava/lang/ClassLoader;[Ljava/lang/Class;Ljava/lang/reflect/InvocationHandler;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/fingerprint/service/vendor/FingerprintProxy;

    .line 41
    .local v2, "proxy":Lcom/samsung/android/fingerprint/service/vendor/FingerprintProxy;
    return-object v2
.end method
