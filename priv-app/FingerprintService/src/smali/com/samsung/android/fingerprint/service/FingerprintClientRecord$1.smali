.class Lcom/samsung/android/fingerprint/service/FingerprintClientRecord$1;
.super Landroid/os/RemoteCallbackList;
.source "FingerprintClientRecord.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/RemoteCallbackList",
        "<",
        "Lcom/samsung/android/fingerprint/IFingerprintClient;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;


# direct methods
.method constructor <init>(Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;)V
    .locals 0

    .prologue
    .line 49
    iput-object p1, p0, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord$1;->this$0:Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

    invoke-direct {p0}, Landroid/os/RemoteCallbackList;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic onCallbackDied(Landroid/os/IInterface;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/IInterface;

    .prologue
    .line 49
    check-cast p1, Lcom/samsung/android/fingerprint/IFingerprintClient;

    .end local p1    # "x0":Landroid/os/IInterface;
    invoke-virtual {p0, p1}, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord$1;->onCallbackDied(Lcom/samsung/android/fingerprint/IFingerprintClient;)V

    return-void
.end method

.method public onCallbackDied(Lcom/samsung/android/fingerprint/IFingerprintClient;)V
    .locals 3
    .param p1, "client"    # Lcom/samsung/android/fingerprint/IFingerprintClient;

    .prologue
    .line 52
    const-string v0, "FingerprintClientRecord"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onCallbackDied: client="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 54
    invoke-virtual {p0, p1}, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord$1;->unregister(Landroid/os/IInterface;)Z

    .line 56
    invoke-virtual {p0}, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord$1;->getRegisteredCallbackCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 58
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord$1;->this$0:Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

    # getter for: Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->mService:Lcom/samsung/android/fingerprint/service/FingerprintManagerService;
    invoke-static {v0}, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->access$100(Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;)Lcom/samsung/android/fingerprint/service/FingerprintManagerService;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord$1;->this$0:Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

    # getter for: Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->mToken:Landroid/os/IBinder;
    invoke-static {v1}, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->access$000(Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;)Landroid/os/IBinder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord$1;->this$0:Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->onClientDied(Landroid/os/IBinder;Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;)V

    .line 60
    :cond_0
    return-void
.end method
