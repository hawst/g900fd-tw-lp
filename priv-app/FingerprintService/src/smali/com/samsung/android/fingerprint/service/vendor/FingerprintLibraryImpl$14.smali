.class Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$14;
.super Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;
.source "FingerprintLibraryImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field isRunning:Z

.field final synthetic this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;


# direct methods
.method constructor <init>(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 1
    .param p2, "x0"    # Ljava/lang/String;
    .param p3, "x1"    # Ljava/lang/String;
    .param p4, "x2"    # I

    .prologue
    .line 1396
    iput-object p1, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$14;->this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    invoke-direct {p0, p2, p3, p4}, Lcom/samsung/android/fingerprint/service/FingerprintLibraryCommand;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1398
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$14;->isRunning:Z

    return-void
.end method


# virtual methods
.method public doCommand()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1401
    const-string v0, "ValidityFingerprintLibraryImpl"

    const-string v1, "lock()"

    invoke-static {v0, v1}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1403
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$14;->isRunning:Z

    .line 1405
    const/4 v0, 0x0

    return-object v0
.end method

.method public requestCancel()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1414
    iget-boolean v1, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$14;->isRunning:Z

    if-eqz v1, :cond_0

    .line 1415
    const-string v1, "ValidityFingerprintLibraryImpl"

    const-string v2, "unlock()"

    invoke-static {v1, v2}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1416
    invoke-virtual {p0}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$14;->notifyCommandCompleted()V

    .line 1417
    iput-boolean v0, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$14;->isRunning:Z

    .line 1418
    const/4 v0, 0x1

    .line 1420
    :cond_0
    return v0
.end method
