.class Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$21;
.super Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$ImmediateCommand;
.source "FingerprintLibraryImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;


# direct methods
.method constructor <init>(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p2, "x0"    # Ljava/lang/String;
    .param p3, "x1"    # Ljava/lang/String;

    .prologue
    .line 2055
    iput-object p1, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$21;->this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$ImmediateCommand;-><init>(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public doCommand()Ljava/lang/Object;
    .locals 11

    .prologue
    .line 2058
    new-instance v7, Lcom/validity/fingerprint/VcsTemplateIds;

    invoke-direct {v7}, Lcom/validity/fingerprint/VcsTemplateIds;-><init>()V

    .line 2060
    .local v7, "vcsTemplateIds":Lcom/validity/fingerprint/VcsTemplateIds;
    const-string v8, "ValidityFingerprintLibraryImpl"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "getFingerprintIds: userId="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$21;->stringUserId:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->da(Ljava/lang/String;Ljava/lang/String;)V

    .line 2062
    iget-object v8, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$21;->this$0:Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;

    # getter for: Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->mFingerprint:Lcom/samsung/android/fingerprint/service/vendor/FingerprintProxy;
    invoke-static {v8}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;->access$100(Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl;)Lcom/samsung/android/fingerprint/service/vendor/FingerprintProxy;

    move-result-object v8

    iget-object v9, p0, Lcom/samsung/android/fingerprint/service/vendor/FingerprintLibraryImpl$21;->stringUserId:Ljava/lang/String;

    invoke-interface {v8, v9, v7}, Lcom/samsung/android/fingerprint/service/vendor/FingerprintProxy;->getEnrolledTemplateIds(Ljava/lang/String;Lcom/validity/fingerprint/VcsTemplateIds;)I

    move-result v6

    .line 2064
    .local v6, "result":I
    if-eqz v6, :cond_1

    .line 2065
    const/4 v2, 0x0

    .line 2081
    :cond_0
    return-object v2

    .line 2068
    :cond_1
    const/16 v8, 0xb

    new-array v2, v8, [Ljava/lang/String;

    .line 2070
    .local v2, "ids":[Ljava/lang/String;
    invoke-virtual {v7}, Lcom/validity/fingerprint/VcsTemplateIds;->getTemplateIds()Ljava/util/Map;

    move-result-object v5

    .line 2072
    .local v5, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;[B>;"
    invoke-interface {v5}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 2073
    .local v3, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/Integer;[B>;>;"
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 2074
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map$Entry;

    .line 2075
    .local v4, "mEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;[B>;"
    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 2076
    .local v0, "fingerIndex":I
    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, [B

    invoke-static {v8}, Lcom/samsung/android/fingerprint/service/util/ValueUtils;->byteArrayToHex([B)Ljava/lang/String;

    move-result-object v1

    .line 2077
    .local v1, "id":Ljava/lang/String;
    aput-object v1, v2, v0

    .line 2078
    const-string v8, "ValidityFingerprintLibraryImpl"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "getFingerprintIds: fingerIndex="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", id="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
