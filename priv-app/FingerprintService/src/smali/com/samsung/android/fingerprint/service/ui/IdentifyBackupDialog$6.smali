.class Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog$6;
.super Landroid/os/CountDownTimer;
.source "IdentifyBackupDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->handleAttemptLockout()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;


# direct methods
.method constructor <init>(Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;JJ)V
    .locals 0
    .param p2, "x0"    # J
    .param p4, "x1"    # J

    .prologue
    .line 519
    iput-object p1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog$6;->this$0:Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;

    invoke-direct {p0, p2, p3, p4, p5}, Landroid/os/CountDownTimer;-><init>(JJ)V

    return-void
.end method


# virtual methods
.method public onFinish()V
    .locals 2

    .prologue
    .line 540
    const-string v0, "IdentifyBackupDialog"

    const-string v1, "CountdownTimer: onFinish"

    invoke-static {v0, v1}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 541
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog$6;->this$0:Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;

    iget-object v0, v0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mCountdownTimer:Landroid/os/CountDownTimer;

    if-eqz v0, :cond_0

    .line 542
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog$6;->this$0:Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;

    iget-object v0, v0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mCountdownTimer:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->cancel()V

    .line 543
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog$6;->this$0:Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mCountdownTimer:Landroid/os/CountDownTimer;

    .line 545
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog$6;->this$0:Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;

    invoke-virtual {v0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->updateLayout()V

    .line 546
    return-void
.end method

.method public onTick(J)V
    .locals 5
    .param p1, "millisUntilFinished"    # J

    .prologue
    .line 522
    const-wide/16 v2, 0x3e8

    div-long v2, p1, v2

    long-to-int v0, v2

    .line 523
    .local v0, "secondsCountdown":I
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog$6;->this$0:Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;

    iget v1, v1, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mBackupType:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 524
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog$6;->this$0:Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;

    iget-object v1, v1, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPasswordGuide:Landroid/widget/TextView;

    if-eqz v1, :cond_0

    .line 525
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog$6;->this$0:Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;

    iget-object v1, v1, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPasswordGuide:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog$6;->this$0:Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;

    # invokes: Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->setLockoutString(I)Ljava/lang/String;
    invoke-static {v2, v0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->access$500(Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 536
    :cond_0
    :goto_0
    return-void

    .line 527
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog$6;->this$0:Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;

    iget v1, v1, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mBackupType:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    .line 528
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog$6;->this$0:Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;

    iget-object v1, v1, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPinGuide:Landroid/widget/TextView;

    if-eqz v1, :cond_0

    .line 529
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog$6;->this$0:Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;

    iget-object v1, v1, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPinGuide:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog$6;->this$0:Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;

    # invokes: Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->setLockoutString(I)Ljava/lang/String;
    invoke-static {v2, v0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->access$500(Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 531
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog$6;->this$0:Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;

    iget v1, v1, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mBackupType:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_0

    .line 532
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog$6;->this$0:Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;

    # getter for: Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPatternGuide:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->access$600(Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;)Landroid/widget/TextView;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 533
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog$6;->this$0:Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;

    # getter for: Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->mAltPatternGuide:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->access$600(Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;)Landroid/widget/TextView;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog$6;->this$0:Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;

    # invokes: Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->setLockoutString(I)Ljava/lang/String;
    invoke-static {v2, v0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;->access$500(Lcom/samsung/android/fingerprint/service/ui/IdentifyBackupDialog;I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
