.class public Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;
.super Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog;
.source "IdentifyFingerprintDialog.java"

# interfaces
.implements Landroid/content/DialogInterface$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog$11;,
        Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog$AnimationDrawableListener;
    }
.end annotation


# static fields
.field private static final CLOSE_DIALOG_TEXT_TIME:I = 0xbb8

.field private static final CLOSE_ERR_MSG:I = 0x1388

.field protected static final DIALOG_BG_TRANSPRENCY:I = 0xc0

.field private static final DIALOG_TITLE_COLOR_DEFAULT:I = -0xa0a0b

.field private static final IDENTIFY_TERM:I = 0x190

.field private static final TAG:Ljava/lang/String; = "IdentifyFingerprintDialog"


# instance fields
.field private aniDrawable:Landroid/graphics/drawable/AnimationDrawable;

.field private aniDrawableListner:Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog$AnimationDrawableListener;

.field protected mAnimationBox:Landroid/widget/RelativeLayout;

.field protected mAnimationImage:Landroid/widget/ImageView;

.field protected mBackupInfoImage:Landroid/graphics/drawable/Drawable;

.field protected mBackupInfoText:Ljava/lang/String;

.field protected mBackupType:I

.field private mCallerComponentName:Landroid/content/ComponentName;

.field protected mCloseButton:Landroid/widget/ImageButton;

.field protected mDialogBgTransparency:I

.field private mDialogIconName:Ljava/lang/String;

.field protected mDialogLogoIconView:Landroid/widget/ImageView;

.field protected mDialogOutsideBox:Landroid/widget/RelativeLayout;

.field private mDialogTitleColor:I

.field private mDialogTitleText:Ljava/lang/String;

.field protected mDialogTitleTextView:Landroid/widget/TextView;

.field protected mDialogTouchOutside:Z

.field private mEnableBackup:Z

.field private mErrClearRunnable:Ljava/lang/Runnable;

.field private mEvent:Lcom/samsung/android/fingerprint/FingerprintEvent;

.field private mHandler:Landroid/os/Handler;

.field private mHasSecondScreen:Z

.field protected mInfoImage:Landroid/widget/ImageView;

.field protected mInfoText:Landroid/widget/TextView;

.field private mIsBackupState:Z

.field private mIsCloseButtonPressed:Z

.field private mIsDetecedFinger:Z

.field private mIsDisalbedHomeKey:Z

.field private mIsDropEvent:Z

.field private mIsFailState:Z

.field private mIsFingerprintLockedOut:Z

.field private mIsFirstReady:Z

.field private mIsHomeKeyPressed:Z

.field private mIsInternalCancel:Z

.field private mIsSensorRemoved:Z

.field private mIsSucceed:Z

.field private mIsdismissTouchOutside:Z

.field private mPermission:Ljava/lang/String;

.field private mQualityMessage:I

.field protected mScrollImage:Landroid/widget/ImageView;

.field private mTweenAni:Landroid/view/animation/Animation;

.field protected mUnlockButton:Landroid/widget/ImageView;

.field protected mUnlockLine:Landroid/widget/ImageView;

.field private orientationListener:Landroid/view/OrientationEventListener;

.field private orientationScreen:I

.field private preOrientationScreen:I


# direct methods
.method public constructor <init>(Lcom/samsung/android/fingerprint/service/FingerprintManagerService;Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 5
    .param p1, "service"    # Lcom/samsung/android/fingerprint/service/FingerprintManagerService;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "permission"    # Ljava/lang/String;
    .param p4, "bundle"    # Landroid/os/Bundle;

    .prologue
    const/4 v4, 0x1

    const/4 v0, -0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 358
    invoke-direct {p0, p2, p1}, Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog;-><init>(Landroid/content/Context;Lcom/samsung/android/fingerprint/service/FingerprintManagerService;)V

    .line 76
    iput-boolean v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mIsDropEvent:Z

    .line 77
    iput-boolean v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mIsHomeKeyPressed:Z

    .line 78
    iput-object v3, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mCallerComponentName:Landroid/content/ComponentName;

    .line 79
    iput-boolean v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mIsDisalbedHomeKey:Z

    .line 80
    iput-boolean v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mIsDetecedFinger:Z

    .line 85
    iput-boolean v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mHasSecondScreen:Z

    .line 88
    iput v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mBackupType:I

    .line 113
    iput-boolean v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mIsBackupState:Z

    .line 114
    iput-boolean v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mIsFailState:Z

    .line 115
    iput-boolean v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mIsSucceed:Z

    .line 116
    iput-boolean v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mIsInternalCancel:Z

    .line 117
    iput-boolean v4, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mIsFirstReady:Z

    .line 118
    iput-boolean v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mIsSensorRemoved:Z

    .line 119
    iput-boolean v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mIsCloseButtonPressed:Z

    .line 120
    iput-boolean v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mIsFingerprintLockedOut:Z

    .line 121
    iput-boolean v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mIsdismissTouchOutside:Z

    .line 125
    iput v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->preOrientationScreen:I

    .line 126
    iput v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->orientationScreen:I

    .line 360
    new-instance v0, Landroid/os/Handler;

    invoke-virtual {p2}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mHandler:Landroid/os/Handler;

    .line 361
    iput-object p3, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mPermission:Ljava/lang/String;

    .line 362
    const-string v0, "password"

    invoke-virtual {p4, v0, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mEnableBackup:Z

    .line 363
    const-string v0, "titletext"

    invoke-virtual {p4, v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mDialogTitleText:Ljava/lang/String;

    .line 364
    const-string v0, "titlecolor"

    const v1, -0xa0a0b

    invoke-virtual {p4, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mDialogTitleColor:I

    .line 365
    const-string v0, "iconname"

    invoke-virtual {p4, v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mDialogIconName:Ljava/lang/String;

    .line 366
    const-string v0, "transparency"

    const/16 v1, 0xc0

    invoke-virtual {p4, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mDialogBgTransparency:I

    .line 367
    const-string v0, "touchoutside"

    invoke-virtual {p4, v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mDialogTouchOutside:Z

    .line 369
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mIdentifyDialogManager:Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;

    if-eqz v0, :cond_0

    .line 370
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mIdentifyDialogManager:Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;

    invoke-virtual {v0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;->getCallerClient()Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->getComponentName()Landroid/content/ComponentName;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mCallerComponentName:Landroid/content/ComponentName;

    .line 372
    :cond_0
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;

    .prologue
    .line 65
    invoke-direct {p0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->onConfigurationChanged()V

    return-void
.end method

.method static synthetic access$100(Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;

    .prologue
    .line 65
    invoke-direct {p0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->showBackupDialog()V

    return-void
.end method

.method static synthetic access$202(Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;
    .param p1, "x1"    # Z

    .prologue
    .line 65
    iput-boolean p1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mIsCloseButtonPressed:Z

    return p1
.end method

.method static synthetic access$300(Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog$Stage;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;
    .param p1, "x1"    # Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog$Stage;

    .prologue
    .line 65
    invoke-direct {p0, p1}, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->updateStage(Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog$Stage;)V

    return-void
.end method

.method static synthetic access$402(Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;
    .param p1, "x1"    # Z

    .prologue
    .line 65
    iput-boolean p1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mIsFailState:Z

    return p1
.end method

.method static synthetic access$500(Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;

    .prologue
    .line 65
    invoke-direct {p0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->showLockoutAttentionDialog()V

    return-void
.end method

.method static synthetic access$600(Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;

    .prologue
    .line 65
    invoke-direct {p0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->identify()Z

    move-result v0

    return v0
.end method

.method static synthetic access$700(Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;)Landroid/view/animation/Animation;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mTweenAni:Landroid/view/animation/Animation;

    return-object v0
.end method

.method static synthetic access$702(Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;Landroid/view/animation/Animation;)Landroid/view/animation/Animation;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;
    .param p1, "x1"    # Landroid/view/animation/Animation;

    .prologue
    .line 65
    iput-object p1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mTweenAni:Landroid/view/animation/Animation;

    return-object p1
.end method

.method static synthetic access$800(Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 65
    invoke-direct {p0, p1}, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->setInfoText(Ljava/lang/String;)V

    return-void
.end method

.method private backupAndRestoreData(Ljava/lang/String;)V
    .locals 2
    .param p1, "type"    # Ljava/lang/String;

    .prologue
    .line 408
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mInfoText:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mInfoImage:Landroid/widget/ImageView;

    if-nez v0, :cond_1

    .line 420
    :cond_0
    :goto_0
    return-void

    .line 411
    :cond_1
    const-string v0, "bBackup"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 412
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mInfoText:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mInfoImage:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 413
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mInfoText:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mBackupInfoText:Ljava/lang/String;

    .line 414
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mInfoImage:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mBackupInfoImage:Landroid/graphics/drawable/Drawable;

    goto :goto_0

    .line 416
    :cond_2
    const-string v0, "bRestore"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 417
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mBackupInfoText:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->setInfoText(Ljava/lang/String;)V

    .line 418
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mInfoImage:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mBackupInfoImage:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method private declared-synchronized cleanUpLayoutResources()V
    .locals 2

    .prologue
    .line 560
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mAnimationBox:Landroid/widget/RelativeLayout;

    .line 561
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mAnimationImage:Landroid/widget/ImageView;

    .line 562
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mScrollImage:Landroid/widget/ImageView;

    .line 563
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mInfoText:Landroid/widget/TextView;

    .line 564
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mInfoImage:Landroid/widget/ImageView;

    .line 565
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mUnlockButton:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 566
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mUnlockButton:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 567
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mUnlockButton:Landroid/widget/ImageView;

    .line 569
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mCloseButton:Landroid/widget/ImageButton;

    if-eqz v0, :cond_1

    .line 570
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mCloseButton:Landroid/widget/ImageButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 571
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mCloseButton:Landroid/widget/ImageButton;

    .line 573
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->aniDrawable:Landroid/graphics/drawable/AnimationDrawable;

    if-eqz v0, :cond_2

    .line 574
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->aniDrawable:Landroid/graphics/drawable/AnimationDrawable;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/AnimationDrawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 575
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->aniDrawable:Landroid/graphics/drawable/AnimationDrawable;

    .line 577
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_3

    .line 578
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 579
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mHandler:Landroid/os/Handler;

    .line 581
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->orientationListener:Landroid/view/OrientationEventListener;

    if-eqz v0, :cond_4

    .line 582
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->orientationListener:Landroid/view/OrientationEventListener;

    invoke-virtual {v0}, Landroid/view/OrientationEventListener;->disable()V

    .line 583
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->orientationListener:Landroid/view/OrientationEventListener;

    .line 586
    :cond_4
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->aniDrawableListner:Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog$AnimationDrawableListener;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->aniDrawableListner:Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog$AnimationDrawableListener;

    invoke-virtual {v0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog$AnimationDrawableListener;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 587
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->aniDrawableListner:Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog$AnimationDrawableListener;

    invoke-virtual {v0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog$AnimationDrawableListener;->stop()V

    .line 588
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->aniDrawableListner:Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog$AnimationDrawableListener;

    .line 590
    :cond_5
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mCountdownTimer:Landroid/os/CountDownTimer;

    if-eqz v0, :cond_6

    .line 591
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mCountdownTimer:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->cancel()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 593
    :cond_6
    monitor-exit p0

    return-void

    .line 560
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private disalbeHomeKey()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1097
    iget-boolean v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mIsDisalbedHomeKey:Z

    if-nez v0, :cond_0

    .line 1098
    const/4 v0, 0x3

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->requestSystemKeyEvent(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1099
    iput-boolean v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mIsDisalbedHomeKey:Z

    .line 1102
    :cond_0
    return-void
.end method

.method private enableHomeKey()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1105
    const/4 v0, 0x3

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->requestSystemKeyEvent(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1106
    iput-boolean v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mIsDisalbedHomeKey:Z

    .line 1108
    :cond_0
    return-void
.end method

.method private getIconResource(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    .locals 9
    .param p1, "string"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 924
    const-string v6, "IdentifyFingerprintDialog"

    const-string v7, "getIconResource called"

    invoke-static {v6, v7}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 925
    iget-object v6, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mIdentifyDialogManager:Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;

    invoke-virtual {v6}, Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;->getCallerClient()Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/android/fingerprint/service/FingerprintClientRecord;->getPackageName()Ljava/lang/String;

    move-result-object v3

    .line 927
    .local v3, "pkgName":Ljava/lang/String;
    iget-object v6, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    .line 928
    .local v4, "pm":Landroid/content/pm/PackageManager;
    const/4 v0, 0x0

    .line 929
    .local v0, "Res":Landroid/content/res/Resources;
    const/4 v5, -0x1

    .line 932
    .local v5, "resourceId":I
    :try_start_0
    invoke-virtual {v4, v3}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Ljava/lang/String;)Landroid/content/res/Resources;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 937
    if-nez v0, :cond_0

    .line 938
    const-string v6, "IdentifyFingerprintDialog"

    const-string v7, "resource is not ready"

    invoke-static {v6, v7}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 948
    :goto_0
    return-object v2

    .line 933
    :catch_0
    move-exception v1

    .line 934
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v1}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 941
    .end local v1    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_0
    const-string v6, "drawable"

    invoke-virtual {v0, p1, v6, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v5

    .line 942
    const-string v6, "IdentifyFingerprintDialog"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "getLogoResource: logoPath = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "resourceId = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 943
    const/4 v6, -0x1

    if-ne v5, v6, :cond_1

    .line 944
    const-string v6, "IdentifyFingerprintDialog"

    const-string v7, "resourceId is invalid"

    invoke-static {v6, v7}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 947
    :cond_1
    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 948
    .local v2, "logoDrawable":Landroid/graphics/drawable/Drawable;
    goto :goto_0
.end method

.method private identify()Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 463
    const/4 v0, -0x1

    .line 464
    .local v0, "result":I
    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mFpmsService:Lcom/samsung/android/fingerprint/service/FingerprintManagerService;

    if-eqz v2, :cond_0

    .line 465
    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mFpmsService:Lcom/samsung/android/fingerprint/service/FingerprintManagerService;

    iget-object v3, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mToken:Landroid/os/IBinder;

    iget-object v4, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mPermission:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->identify(Landroid/os/IBinder;Ljava/lang/String;)I

    move-result v0

    .line 466
    if-nez v0, :cond_0

    .line 467
    const/4 v1, 0x1

    .line 473
    :cond_0
    return v1
.end method

.method private launchHomeScreen()V
    .locals 2

    .prologue
    .line 1090
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.MAIN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1091
    .local v0, "startMain":Landroid/content/Intent;
    const-string v1, "android.intent.category.HOME"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 1092
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1093
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 1094
    return-void
.end method

.method private onConfigurationChanged()V
    .locals 2

    .prologue
    .line 423
    invoke-virtual {p0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->orientationScreen:I

    .line 424
    iget v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->preOrientationScreen:I

    iget v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->orientationScreen:I

    if-ne v0, v1, :cond_0

    .line 460
    :goto_0
    return-void

    .line 427
    :cond_0
    const-string v0, "bBackup"

    invoke-direct {p0, v0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->backupAndRestoreData(Ljava/lang/String;)V

    .line 428
    iget v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->orientationScreen:I

    iput v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->preOrientationScreen:I

    .line 429
    iget v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->orientationScreen:I

    packed-switch v0, :pswitch_data_0

    .line 459
    :goto_1
    invoke-virtual {p0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->updateLayout()V

    goto :goto_0

    .line 431
    :pswitch_0
    iget-boolean v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mEnableBackup:Z

    if-nez v0, :cond_1

    .line 432
    const v0, 0x7f030006

    invoke-virtual {p0, v0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->setContentView(I)V

    goto :goto_1

    .line 434
    :cond_1
    const v0, 0x7f030002

    invoke-virtual {p0, v0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->setContentView(I)V

    goto :goto_1

    .line 438
    :pswitch_1
    iget-boolean v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mEnableBackup:Z

    if-nez v0, :cond_2

    .line 439
    const v0, 0x7f030009

    invoke-virtual {p0, v0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->setContentView(I)V

    goto :goto_1

    .line 441
    :cond_2
    const v0, 0x7f030005

    invoke-virtual {p0, v0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->setContentView(I)V

    goto :goto_1

    .line 445
    :pswitch_2
    iget-boolean v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mEnableBackup:Z

    if-nez v0, :cond_3

    .line 446
    const v0, 0x7f030007

    invoke-virtual {p0, v0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->setContentView(I)V

    goto :goto_1

    .line 448
    :cond_3
    const v0, 0x7f030003

    invoke-virtual {p0, v0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->setContentView(I)V

    goto :goto_1

    .line 452
    :pswitch_3
    iget-boolean v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mEnableBackup:Z

    if-nez v0, :cond_4

    .line 453
    const v0, 0x7f030008

    invoke-virtual {p0, v0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->setContentView(I)V

    goto :goto_1

    .line 455
    :cond_4
    const v0, 0x7f030004

    invoke-virtual {p0, v0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->setContentView(I)V

    goto :goto_1

    .line 429
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private pointInside(IILandroid/view/View;)Z
    .locals 4
    .param p1, "x"    # I
    .param p2, "y"    # I
    .param p3, "v"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x0

    .line 596
    if-eqz p3, :cond_0

    .line 597
    invoke-virtual {p3}, Landroid/view/View;->getLeft()I

    move-result v0

    .line 598
    .local v0, "l":I
    invoke-virtual {p3}, Landroid/view/View;->getRight()I

    move-result v1

    .line 599
    .local v1, "r":I
    invoke-virtual {p3}, Landroid/view/View;->getTop()I

    move-result v2

    .line 601
    .local v2, "t":I
    if-lt p1, v0, :cond_0

    if-ge p1, v1, :cond_0

    if-lt p2, v2, :cond_0

    const/4 v3, 0x1

    .line 603
    .end local v0    # "l":I
    .end local v1    # "r":I
    .end local v2    # "t":I
    :cond_0
    return v3
.end method

.method private requestSystemKeyEvent(IZ)Z
    .locals 6
    .param p1, "keycode"    # I
    .param p2, "request"    # Z

    .prologue
    const/4 v2, 0x0

    .line 1071
    iget-object v3, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mCallerComponentName:Landroid/content/ComponentName;

    if-nez v3, :cond_1

    .line 1072
    const-string v3, "IdentifyFingerprintDialog"

    const-string v4, "requestSystemKeyEvent: ComponentName is null"

    invoke-static {v3, v4}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1086
    :cond_0
    :goto_0
    return v2

    .line 1075
    :cond_1
    const-string v3, "window"

    invoke-static {v3}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v3

    invoke-static {v3}, Landroid/view/IWindowManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/IWindowManager;

    move-result-object v1

    .line 1078
    .local v1, "windowmanager":Landroid/view/IWindowManager;
    if-eqz v1, :cond_0

    .line 1079
    :try_start_0
    iget-object v3, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mCallerComponentName:Landroid/content/ComponentName;

    invoke-interface {v1, p1, v3, p2}, Landroid/view/IWindowManager;->requestSystemKeyEvent(ILandroid/content/ComponentName;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    goto :goto_0

    .line 1083
    :catch_0
    move-exception v0

    .line 1084
    .local v0, "e":Landroid/os/RemoteException;
    const-string v3, "IdentifyFingerprintDialog"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "requestSystemKeyEvent - "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private resetErrorMsgAndRequest()V
    .locals 4

    .prologue
    .line 1041
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mHandler:Landroid/os/Handler;

    if-nez v0, :cond_0

    .line 1068
    :goto_0
    return-void

    .line 1044
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog$9;

    invoke-direct {v1, p0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog$9;-><init>(Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;)V

    const-wide/16 v2, 0x190

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1055
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mErrClearRunnable:Ljava/lang/Runnable;

    if-eqz v0, :cond_1

    .line 1056
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mErrClearRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1058
    :cond_1
    new-instance v0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog$10;

    invoke-direct {v0, p0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog$10;-><init>(Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;)V

    iput-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mErrClearRunnable:Ljava/lang/Runnable;

    .line 1067
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mErrClearRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1388

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method private setInfoText(Ljava/lang/String;)V
    .locals 1
    .param p1, "string"    # Ljava/lang/String;

    .prologue
    .line 952
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mInfoText:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 953
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mInfoText:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 954
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mInfoText:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->announceForAccessibility(Ljava/lang/CharSequence;)V

    .line 956
    :cond_0
    return-void
.end method

.method private setSecondScreenAnimLayout()V
    .locals 7

    .prologue
    const/16 v6, 0xf

    const/16 v2, 0xe

    const/high16 v3, 0x41200000    # 10.0f

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 737
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mScrollImage:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 738
    .local v0, "nParam":Landroid/widget/RelativeLayout$LayoutParams;
    iget v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->orientationScreen:I

    packed-switch v1, :pswitch_data_0

    .line 776
    :goto_0
    return-void

    .line 740
    :pswitch_0
    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout$LayoutParams;->removeRule(I)V

    .line 741
    invoke-virtual {v0, v6}, Landroid/widget/RelativeLayout$LayoutParams;->removeRule(I)V

    .line 742
    const/high16 v1, 0x43380000    # 184.0f

    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    invoke-static {v5, v1, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v1

    float-to-int v1, v1

    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    invoke-static {v5, v3, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {v0, v1, v4, v4, v2}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    goto :goto_0

    .line 749
    :pswitch_1
    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout$LayoutParams;->removeRule(I)V

    .line 750
    invoke-virtual {v0, v6}, Landroid/widget/RelativeLayout$LayoutParams;->removeRule(I)V

    .line 751
    const/high16 v1, 0x43330000    # 179.0f

    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    invoke-static {v5, v1, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v1

    float-to-int v1, v1

    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    invoke-static {v5, v3, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {v0, v4, v1, v2, v4}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    goto :goto_0

    .line 758
    :pswitch_2
    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout$LayoutParams;->removeRule(I)V

    .line 759
    invoke-virtual {v0, v6}, Landroid/widget/RelativeLayout$LayoutParams;->removeRule(I)V

    .line 760
    const/high16 v1, 0x43340000    # 180.0f

    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    invoke-static {v5, v1, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v1

    float-to-int v1, v1

    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    invoke-static {v5, v3, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {v0, v1, v2, v4, v4}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    goto :goto_0

    .line 767
    :pswitch_3
    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout$LayoutParams;->removeRule(I)V

    .line 768
    invoke-virtual {v0, v6}, Landroid/widget/RelativeLayout$LayoutParams;->removeRule(I)V

    .line 769
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    invoke-static {v5, v3, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v1

    float-to-int v1, v1

    const/high16 v2, 0x43370000    # 183.0f

    iget-object v3, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    invoke-static {v5, v2, v3}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {v0, v1, v2, v4, v4}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    goto/16 :goto_0

    .line 738
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private showAlertDialog(Ljava/lang/String;)V
    .locals 5
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 779
    const/4 v1, 0x4

    .line 780
    .local v1, "mDialogTheme":I
    iget v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mDeviceTheme:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    .line 781
    const/4 v1, 0x5

    .line 783
    :cond_0
    new-instance v2, Landroid/app/AlertDialog$Builder;

    iget-object v3, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    iget-object v3, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mContext:Landroid/content/Context;

    const v4, 0x7f060018

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x104000a

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 788
    .local v0, "dialog":Landroid/app/AlertDialog;
    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    const/16 v3, 0x7d8

    invoke-virtual {v2, v3}, Landroid/view/Window;->setType(I)V

    .line 789
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 790
    return-void
.end method

.method private showBackupDialog()V
    .locals 2

    .prologue
    .line 1111
    const-string v0, "IdentifyFingerprintDialog"

    const-string v1, "showBackupDialog: called"

    invoke-static {v0, v1}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1112
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mIsDropEvent:Z

    .line 1113
    invoke-virtual {p0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->detachClient()V

    .line 1114
    invoke-direct {p0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->cleanUpLayoutResources()V

    .line 1115
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mFpmsService:Lcom/samsung/android/fingerprint/service/FingerprintManagerService;

    invoke-virtual {v0}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->getIdentifyDialogManager()Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/samsung/android/fingerprint/service/ui/IdentifyDialogManager;->switchDialog(I)V

    .line 1116
    return-void
.end method

.method private showLockoutAttentionDialog()V
    .locals 9

    .prologue
    const/4 v8, 0x5

    const/4 v3, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 793
    invoke-static {}, Lcom/samsung/android/fingerprint/service/util/Config;->getOverallFailedFingerprintAttempts()I

    move-result v1

    if-ne v1, v5, :cond_0

    .line 794
    invoke-virtual {p0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->dismiss()V

    .line 795
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mContext:Landroid/content/Context;

    const v2, 0x7f060016

    new-array v3, v7, [Ljava/lang/Object;

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    const/16 v4, 0x1e

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 796
    .local v0, "string":Ljava/lang/String;
    invoke-direct {p0, v0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->showAlertDialog(Ljava/lang/String;)V

    .line 804
    .end local v0    # "string":Ljava/lang/String;
    :goto_0
    return-void

    .line 797
    :cond_0
    invoke-static {}, Lcom/samsung/android/fingerprint/service/util/Config;->getOverallFailedFingerprintAttempts()I

    move-result v1

    if-ne v1, v3, :cond_1

    .line 798
    invoke-virtual {p0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->dismiss()V

    .line 799
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mContext:Landroid/content/Context;

    const v2, 0x7f060017

    new-array v3, v3, [Ljava/lang/Object;

    const/16 v4, 0xf

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    const/16 v4, 0x1e

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 800
    .restart local v0    # "string":Ljava/lang/String;
    invoke-direct {p0, v0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->showAlertDialog(Ljava/lang/String;)V

    goto :goto_0

    .line 802
    .end local v0    # "string":Ljava/lang/String;
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->handleLockoutFingerprint()V

    goto :goto_0
.end method

.method private updateStage(Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog$Stage;)V
    .locals 6
    .param p1, "stage"    # Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog$Stage;

    .prologue
    const/4 v5, 0x0

    const/16 v4, 0x8

    const/4 v3, 0x1

    .line 855
    const-string v0, "IdentifyFingerprintDialog"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "stage "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 856
    sget-object v0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog$11;->$SwitchMap$com$samsung$android$fingerprint$service$ui$IdentifyBaseDialog$Stage:[I

    invoke-virtual {p1}, Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog$Stage;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 897
    :cond_0
    :goto_0
    return-void

    .line 858
    :pswitch_0
    iput-boolean v5, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mIsFingerprintLockedOut:Z

    .line 859
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mAnimationBox:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_1

    .line 860
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mAnimationBox:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setEnabled(Z)V

    .line 862
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mInfoImage:Landroid/widget/ImageView;

    if-eqz v0, :cond_2

    .line 863
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mInfoImage:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 865
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f060006

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->setInfoText(Ljava/lang/String;)V

    .line 866
    invoke-virtual {p0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->startStandbyAnimation()V

    .line 867
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mFpmsService:Lcom/samsung/android/fingerprint/service/FingerprintManagerService;

    invoke-virtual {v0}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->hasPendingCommand()Z

    move-result v0

    if-nez v0, :cond_0

    .line 868
    invoke-direct {p0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->identify()Z

    goto :goto_0

    .line 873
    :pswitch_1
    iput-boolean v3, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mIsFingerprintLockedOut:Z

    .line 874
    iput-boolean v3, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mIsInternalCancel:Z

    .line 875
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mFpmsService:Lcom/samsung/android/fingerprint/service/FingerprintManagerService;

    invoke-virtual {v0}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->hasPendingCommand()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 876
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mFpmsService:Lcom/samsung/android/fingerprint/service/FingerprintManagerService;

    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mToken:Landroid/os/IBinder;

    invoke-virtual {v0, v1}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->cancel(Landroid/os/IBinder;)I

    .line 878
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mAnimationBox:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_4

    .line 879
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mAnimationBox:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v5}, Landroid/widget/RelativeLayout;->setEnabled(Z)V

    .line 881
    :cond_4
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mAnimationImage:Landroid/widget/ImageView;

    if-eqz v0, :cond_5

    .line 882
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mAnimationImage:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 884
    :cond_5
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mInfoImage:Landroid/widget/ImageView;

    if-eqz v0, :cond_6

    .line 885
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mInfoImage:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 887
    :cond_6
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->aniDrawableListner:Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog$AnimationDrawableListener;

    if-eqz v0, :cond_7

    .line 888
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->aniDrawableListner:Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog$AnimationDrawableListener;

    invoke-virtual {v0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog$AnimationDrawableListener;->stop()V

    .line 890
    :cond_7
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mErrClearRunnable:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 891
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mErrClearRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 856
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method protected detachClient()V
    .locals 2

    .prologue
    .line 375
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mFpmsService:Lcom/samsung/android/fingerprint/service/FingerprintManagerService;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mToken:Landroid/os/IBinder;

    if-eqz v0, :cond_0

    .line 376
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mFpmsService:Lcom/samsung/android/fingerprint/service/FingerprintManagerService;

    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mToken:Landroid/os/IBinder;

    invoke-virtual {v0, v1}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->cancel(Landroid/os/IBinder;)I

    .line 378
    :cond_0
    return-void
.end method

.method public dismiss()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 513
    iget-boolean v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mIsDropEvent:Z

    if-nez v2, :cond_0

    .line 515
    :try_start_0
    const-string v2, "statusbar"

    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v2

    invoke-static {v2}, Lcom/android/internal/statusbar/IStatusBarService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/statusbar/IStatusBarService;

    move-result-object v1

    .line 517
    .local v1, "mStatusBarService":Lcom/android/internal/statusbar/IStatusBarService;
    const/4 v2, 0x0

    const/4 v3, -0x1

    invoke-interface {v1, v2, v3}, Lcom/android/internal/statusbar/IStatusBarService;->setSystemUiVisibility(II)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1

    .line 521
    .end local v1    # "mStatusBarService":Lcom/android/internal/statusbar/IStatusBarService;
    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->enableHomeKey()V

    .line 522
    invoke-virtual {p0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->detachClient()V

    .line 523
    invoke-direct {p0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->cleanUpLayoutResources()V

    .line 525
    :try_start_1
    invoke-virtual {p0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->isShowing()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 526
    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mEvent:Lcom/samsung/android/fingerprint/FingerprintEvent;

    if-eqz v2, :cond_7

    .line 527
    iget-boolean v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mIsCloseButtonPressed:Z

    if-ne v2, v4, :cond_3

    .line 528
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mIsCloseButtonPressed:Z

    .line 529
    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mEvent:Lcom/samsung/android/fingerprint/FingerprintEvent;

    const/16 v3, 0xd

    iput v3, v2, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventId:I

    .line 530
    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mEvent:Lcom/samsung/android/fingerprint/FingerprintEvent;

    const/4 v3, -0x1

    iput v3, v2, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventResult:I

    .line 531
    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mEvent:Lcom/samsung/android/fingerprint/FingerprintEvent;

    const/16 v3, 0x8

    iput v3, v2, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventStatus:I

    .line 532
    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mEvent:Lcom/samsung/android/fingerprint/FingerprintEvent;

    invoke-virtual {p0, v2}, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->deliverEventToClient(Lcom/samsung/android/fingerprint/FingerprintEvent;)V

    .line 550
    :cond_1
    :goto_1
    invoke-super {p0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog;->dismiss()V

    .line 557
    :cond_2
    :goto_2
    return-void

    .line 533
    :cond_3
    iget-boolean v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mIsdismissTouchOutside:Z

    if-ne v2, v4, :cond_4

    .line 534
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mIsdismissTouchOutside:Z

    .line 535
    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mEvent:Lcom/samsung/android/fingerprint/FingerprintEvent;

    const/16 v3, 0xd

    iput v3, v2, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventStatus:I

    .line 536
    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mEvent:Lcom/samsung/android/fingerprint/FingerprintEvent;

    invoke-virtual {p0, v2}, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->deliverEventToClient(Lcom/samsung/android/fingerprint/FingerprintEvent;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 551
    :catch_0
    move-exception v0

    .line 552
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "IdentifyFingerprintDialog"

    const-string v3, "dismiss() : already dismissed"

    invoke-static {v2, v3}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 553
    if-eqz v0, :cond_2

    .line 554
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2

    .line 537
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_4
    :try_start_2
    iget-boolean v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mIsDropEvent:Z

    if-nez v2, :cond_6

    .line 538
    iget-boolean v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mIsSucceed:Z

    if-nez v2, :cond_5

    .line 539
    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mEvent:Lcom/samsung/android/fingerprint/FingerprintEvent;

    const/4 v3, -0x1

    iput v3, v2, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventResult:I

    .line 540
    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mEvent:Lcom/samsung/android/fingerprint/FingerprintEvent;

    const/16 v3, 0xd

    iput v3, v2, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventId:I

    .line 542
    :cond_5
    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mEvent:Lcom/samsung/android/fingerprint/FingerprintEvent;

    invoke-virtual {p0, v2}, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->deliverEventToClient(Lcom/samsung/android/fingerprint/FingerprintEvent;)V

    goto :goto_1

    .line 544
    :cond_6
    const-string v2, "IdentifyFingerprintDialog"

    const-string v3, "dismiss: Launch backup Password dialog!"

    invoke-static {v2, v3}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 547
    :cond_7
    const-string v2, "IdentifyFingerprintDialog"

    const-string v3, "dismiss: FingerprintEvent is null!! Cannot send event!"

    invoke-static {v2, v3}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_1

    .line 518
    :catch_1
    move-exception v2

    goto :goto_0
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 5
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v1, 0x1

    .line 609
    iget-boolean v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mDialogTouchOutside:Z

    if-ne v2, v1, :cond_1

    .line 610
    const v2, 0x7f07000d

    invoke-virtual {p0, v2}, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 611
    .local v0, "view":Landroid/view/ViewGroup;
    if-eqz v0, :cond_1

    .line 612
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v3, v3

    iget-object v4, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mAnimationBox:Landroid/widget/RelativeLayout;

    invoke-direct {p0, v2, v3, v4}, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->pointInside(IILandroid/view/View;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 613
    iput-boolean v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mIsdismissTouchOutside:Z

    .line 614
    invoke-virtual {p0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->dismiss()V

    .line 622
    .end local v0    # "view":Landroid/view/ViewGroup;
    :cond_0
    :goto_0
    return v1

    .line 619
    :cond_1
    invoke-super {p0, p1}, Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 622
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public handleLockoutFingerprint()V
    .locals 6

    .prologue
    .line 808
    sget-object v0, Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog$Stage;->LockedOut:Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog$Stage;

    invoke-direct {p0, v0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->updateStage(Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog$Stage;)V

    .line 810
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mCountdownTimer:Landroid/os/CountDownTimer;

    if-eqz v0, :cond_0

    .line 811
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mCountdownTimer:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->cancel()V

    .line 812
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mCountdownTimer:Landroid/os/CountDownTimer;

    .line 814
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mScrollImage:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    .line 815
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mScrollImage:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    .line 816
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mScrollImage:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 818
    :cond_1
    const-string v0, "IdentifyFingerprintDialog"

    const-string v1, "CountdownTimer: start"

    invoke-static {v0, v1}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 819
    new-instance v0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog$7;

    invoke-static {}, Lcom/samsung/android/fingerprint/service/util/Config;->getRemaingTimeToUnlock()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog$7;-><init>(Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;JJ)V

    invoke-virtual {v0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog$7;->start()Landroid/os/CountDownTimer;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mCountdownTimer:Landroid/os/CountDownTimer;

    .line 852
    return-void
.end method

.method protected initLayout()V
    .locals 7

    .prologue
    const/16 v3, 0xc0

    const/4 v6, 0x0

    const/16 v5, 0x8

    const/4 v4, 0x1

    .line 647
    const v2, 0x7f07000c

    invoke-virtual {p0, v2}, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    iput-object v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mDialogOutsideBox:Landroid/widget/RelativeLayout;

    .line 648
    const v2, 0x7f07000d

    invoke-virtual {p0, v2}, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    iput-object v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mAnimationBox:Landroid/widget/RelativeLayout;

    .line 649
    const v2, 0x7f070017

    invoke-virtual {p0, v2}, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mAnimationImage:Landroid/widget/ImageView;

    .line 650
    const v2, 0x7f070015

    invoke-virtual {p0, v2}, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mInfoText:Landroid/widget/TextView;

    .line 651
    const v2, 0x7f070014

    invoke-virtual {p0, v2}, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mInfoImage:Landroid/widget/ImageView;

    .line 652
    const v2, 0x7f070012

    invoke-virtual {p0, v2}, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageButton;

    iput-object v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mCloseButton:Landroid/widget/ImageButton;

    .line 653
    const v2, 0x7f070010

    invoke-virtual {p0, v2}, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mUnlockButton:Landroid/widget/ImageView;

    .line 654
    const v2, 0x7f070011

    invoke-virtual {p0, v2}, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mUnlockLine:Landroid/widget/ImageView;

    .line 655
    const v2, 0x7f070016

    invoke-virtual {p0, v2}, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mScrollImage:Landroid/widget/ImageView;

    .line 656
    const v2, 0x7f07000e

    invoke-virtual {p0, v2}, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mDialogTitleTextView:Landroid/widget/TextView;

    .line 657
    const v2, 0x7f070018

    invoke-virtual {p0, v2}, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mDialogLogoIconView:Landroid/widget/ImageView;

    .line 658
    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mDialogOutsideBox:Landroid/widget/RelativeLayout;

    if-eqz v2, :cond_0

    .line 659
    iget v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mDialogBgTransparency:I

    if-eq v2, v3, :cond_7

    .line 660
    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mDialogOutsideBox:Landroid/widget/RelativeLayout;

    invoke-virtual {v2}, Landroid/widget/RelativeLayout;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iget v3, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mDialogBgTransparency:I

    invoke-virtual {v2, v3}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 664
    :goto_0
    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mDialogOutsideBox:Landroid/widget/RelativeLayout;

    invoke-virtual {v2}, Landroid/widget/RelativeLayout;->refreshDrawableState()V

    .line 666
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mDialogTitleTextView:Landroid/widget/TextView;

    if-eqz v2, :cond_1

    .line 667
    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mDialogTitleText:Ljava/lang/String;

    if-eqz v2, :cond_8

    .line 668
    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mDialogTitleTextView:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mDialogTitleText:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 669
    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mDialogTitleTextView:Landroid/widget/TextView;

    iget v3, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mDialogTitleColor:I

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 670
    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mDialogTitleTextView:Landroid/widget/TextView;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setSelected(Z)V

    .line 671
    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mDialogTitleTextView:Landroid/widget/TextView;

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 676
    :cond_1
    :goto_1
    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mDialogLogoIconView:Landroid/widget/ImageView;

    if-eqz v2, :cond_2

    .line 677
    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mDialogIconName:Ljava/lang/String;

    if-eqz v2, :cond_a

    .line 678
    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mDialogIconName:Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->getIconResource(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 679
    .local v1, "iconImage":Landroid/graphics/drawable/Drawable;
    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mDialogLogoIconView:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 680
    .local v0, "dafaultImage":Landroid/graphics/drawable/Drawable;
    if-eqz v1, :cond_9

    if-eq v1, v0, :cond_9

    .line 681
    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mDialogLogoIconView:Landroid/widget/ImageView;

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 682
    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mDialogLogoIconView:Landroid/widget/ImageView;

    invoke-virtual {v2, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 690
    .end local v0    # "dafaultImage":Landroid/graphics/drawable/Drawable;
    .end local v1    # "iconImage":Landroid/graphics/drawable/Drawable;
    :cond_2
    :goto_2
    iget-boolean v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mHasSecondScreen:Z

    if-eqz v2, :cond_3

    .line 691
    invoke-direct {p0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->setSecondScreenAnimLayout()V

    .line 693
    :cond_3
    const-string v2, "bRestore"

    invoke-direct {p0, v2}, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->backupAndRestoreData(Ljava/lang/String;)V

    .line 694
    iput v4, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mBackupType:I

    .line 695
    iget v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mBackupType:I

    const/4 v3, 0x3

    if-le v2, v3, :cond_4

    iget v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mBackupType:I

    if-ge v2, v4, :cond_4

    .line 696
    iput v4, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mBackupType:I

    .line 698
    :cond_4
    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mUnlockButton:Landroid/widget/ImageView;

    if-eqz v2, :cond_5

    .line 699
    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mUnlockButton:Landroid/widget/ImageView;

    new-instance v3, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog$4;

    invoke-direct {v3, p0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog$4;-><init>(Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;)V

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 706
    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mUnlockButton:Landroid/widget/ImageView;

    new-instance v3, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog$5;

    invoke-direct {v3, p0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog$5;-><init>(Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;)V

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 724
    :cond_5
    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mCloseButton:Landroid/widget/ImageButton;

    if-eqz v2, :cond_6

    .line 725
    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mCloseButton:Landroid/widget/ImageButton;

    new-instance v3, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog$6;

    invoke-direct {v3, p0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog$6;-><init>(Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;)V

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 734
    :cond_6
    return-void

    .line 662
    :cond_7
    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mDialogOutsideBox:Landroid/widget/RelativeLayout;

    invoke-virtual {v2}, Landroid/widget/RelativeLayout;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    goto/16 :goto_0

    .line 673
    :cond_8
    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mDialogTitleTextView:Landroid/widget/TextView;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    .line 684
    .restart local v0    # "dafaultImage":Landroid/graphics/drawable/Drawable;
    .restart local v1    # "iconImage":Landroid/graphics/drawable/Drawable;
    :cond_9
    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mDialogLogoIconView:Landroid/widget/ImageView;

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_2

    .line 687
    .end local v0    # "dafaultImage":Landroid/graphics/drawable/Drawable;
    .end local v1    # "iconImage":Landroid/graphics/drawable/Drawable;
    :cond_a
    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mDialogLogoIconView:Landroid/widget/ImageView;

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_2
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 901
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mIsCloseButtonPressed:Z

    .line 902
    invoke-super {p0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog;->onBackPressed()V

    .line 903
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v2, 0x0

    .line 382
    invoke-super {p0, p1}, Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog;->onCreate(Landroid/os/Bundle;)V

    .line 384
    new-instance v0, Lcom/samsung/android/fingerprint/FingerprintEvent;

    invoke-direct {v0}, Lcom/samsung/android/fingerprint/FingerprintEvent;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mEvent:Lcom/samsung/android/fingerprint/FingerprintEvent;

    .line 385
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mEvent:Lcom/samsung/android/fingerprint/FingerprintEvent;

    const/16 v1, 0xd

    iput v1, v0, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventId:I

    .line 386
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mEvent:Lcom/samsung/android/fingerprint/FingerprintEvent;

    const/4 v1, -0x1

    iput v1, v0, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventResult:I

    .line 387
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mEvent:Lcom/samsung/android/fingerprint/FingerprintEvent;

    const/16 v1, 0x8

    iput v1, v0, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventStatus:I

    .line 388
    iput v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mQualityMessage:I

    .line 389
    iput-boolean v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mIsSucceed:Z

    .line 390
    iput-boolean v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mIsInternalCancel:Z

    .line 391
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mIsFirstReady:Z

    .line 392
    iput-boolean v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mIsSensorRemoved:Z

    .line 393
    iput-boolean v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mIsBackupState:Z

    .line 394
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "com.sec.feature.cocktailbar"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mHasSecondScreen:Z

    .line 396
    invoke-direct {p0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->onConfigurationChanged()V

    .line 397
    new-instance v0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog$3;

    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mContext:Landroid/content/Context;

    invoke-direct {v0, p0, v1}, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog$3;-><init>(Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->orientationListener:Landroid/view/OrientationEventListener;

    .line 403
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->orientationListener:Landroid/view/OrientationEventListener;

    invoke-virtual {v0}, Landroid/view/OrientationEventListener;->enable()V

    .line 404
    invoke-virtual {p0, p0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 405
    return-void
.end method

.method public onFingerprintEvent(Lcom/samsung/android/fingerprint/FingerprintEvent;)V
    .locals 8
    .param p1, "evt"    # Lcom/samsung/android/fingerprint/FingerprintEvent;

    .prologue
    const/4 v7, 0x2

    const/4 v2, 0x1

    const/16 v6, 0x8

    const/4 v3, 0x0

    .line 136
    move-object v0, p1

    .line 137
    .local v0, "event":Lcom/samsung/android/fingerprint/FingerprintEvent;
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mHandler:Landroid/os/Handler;

    if-nez v1, :cond_2

    .line 138
    :cond_0
    const-string v1, "IdentifyFingerprintDialog"

    const-string v2, "onFingerprintEvent: Invalid Event"

    invoke-static {v1, v2}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 292
    :cond_1
    :goto_0
    return-void

    .line 141
    :cond_2
    iget v1, v0, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventId:I

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 143
    :pswitch_0
    const-string v1, "IdentifyFingerprintDialog"

    const-string v2, "IDENTIFY_READY"

    invoke-static {v1, v2}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    iput-boolean v3, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mIsDetecedFinger:Z

    .line 145
    iget-boolean v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mIsFirstReady:Z

    if-eqz v1, :cond_1

    .line 146
    iput-boolean v3, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mIsFirstReady:Z

    .line 147
    invoke-virtual {p0, v0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->deliverEventToClient(Lcom/samsung/android/fingerprint/FingerprintEvent;)V

    goto :goto_0

    .line 152
    :pswitch_1
    const-string v1, "IdentifyFingerprintDialog"

    const-string v3, "IDENTIFY_STARTED"

    invoke-static {v1, v3}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 153
    iput-boolean v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mIsDetecedFinger:Z

    .line 154
    invoke-virtual {p0, v0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->deliverEventToClient(Lcom/samsung/android/fingerprint/FingerprintEvent;)V

    .line 155
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mScrollImage:Landroid/widget/ImageView;

    if-eqz v1, :cond_1

    .line 156
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mScrollImage:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->clearAnimation()V

    .line 157
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mContext:Landroid/content/Context;

    const v2, 0x7f04000b

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mTweenAni:Landroid/view/animation/Animation;

    .line 158
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mScrollImage:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mTweenAni:Landroid/view/animation/Animation;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 159
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mTweenAni:Landroid/view/animation/Animation;

    new-instance v2, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog$1;

    invoke-direct {v2, p0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog$1;-><init>(Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;)V

    invoke-virtual {v1, v2}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    goto :goto_0

    .line 180
    :pswitch_2
    const-string v1, "IdentifyFingerprintDialog"

    const-string v4, "IDENTIFY_STATUS"

    invoke-static {v1, v4}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 181
    iget v1, v0, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventStatus:I

    sparse-switch v1, :sswitch_data_0

    goto :goto_0

    .line 196
    :sswitch_0
    const-string v1, "IdentifyFingerprintDialog"

    const-string v4, "SENSOR_ERROR : SENSOR IS REMOVED"

    invoke-static {v1, v4}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 197
    iput-boolean v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mIsSensorRemoved:Z

    .line 198
    iput v3, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mQualityMessage:I

    .line 199
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mToastContext:Landroid/content/Context;

    const v2, 0x7f06000b

    invoke-static {v1, v2, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 200
    iget-boolean v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mEnableBackup:Z

    if-nez v1, :cond_4

    .line 201
    invoke-virtual {p0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->dismiss()V

    goto :goto_0

    .line 183
    :sswitch_1
    iput-boolean v3, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mIsSucceed:Z

    .line 184
    iput-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mEvent:Lcom/samsung/android/fingerprint/FingerprintEvent;

    .line 185
    invoke-virtual {p0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->startFailedAnimation()V

    .line 186
    iget-boolean v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mIsHomeKeyPressed:Z

    if-eqz v1, :cond_3

    .line 187
    iget v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mQualityMessage:I

    const/16 v2, 0x2000

    if-ne v1, v2, :cond_3

    .line 188
    const-string v1, "IdentifyFingerprintDialog"

    const-string v2, "STATUS_QUALITY_FAILED: HOME KEY PRESSED"

    invoke-static {v1, v2}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 189
    invoke-direct {p0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->launchHomeScreen()V

    .line 192
    :cond_3
    iput-boolean v3, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mIsHomeKeyPressed:Z

    goto/16 :goto_0

    .line 205
    :cond_4
    invoke-direct {p0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->showBackupDialog()V

    goto/16 :goto_0

    .line 212
    :pswitch_3
    const-string v4, "IdentifyFingerprintDialog"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "IDENTIFY_FINISHED : "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v1, v0, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventResult:I

    if-nez v1, :cond_5

    const-string v1, "Success"

    :goto_1
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 215
    iget v1, v0, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventStatus:I

    const/4 v4, 0x7

    if-ne v1, v4, :cond_7

    iget-boolean v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mIsSensorRemoved:Z

    if-nez v1, :cond_7

    .line 216
    const-string v1, "IdentifyFingerprintDialog"

    const-string v2, "SENSOR_ERROR : SENSOR NOT FOUND"

    invoke-static {v1, v2}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 217
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mToastContext:Landroid/content/Context;

    const v2, 0x7f06000c

    invoke-static {v1, v2, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 218
    iget-boolean v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mEnableBackup:Z

    if-nez v1, :cond_6

    .line 219
    invoke-virtual {p0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->dismiss()V

    goto/16 :goto_0

    .line 212
    :cond_5
    const-string v1, "Fail"

    goto :goto_1

    .line 223
    :cond_6
    invoke-direct {p0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->showBackupDialog()V

    goto/16 :goto_0

    .line 227
    :cond_7
    iput-boolean v3, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mIsHomeKeyPressed:Z

    .line 228
    iput-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mEvent:Lcom/samsung/android/fingerprint/FingerprintEvent;

    .line 229
    iget v1, v0, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventResult:I

    if-nez v1, :cond_8

    move v1, v2

    :goto_2
    iput-boolean v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mIsSucceed:Z

    .line 231
    iget-boolean v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mIsSucceed:Z

    if-eqz v1, :cond_9

    .line 232
    invoke-virtual {p0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->startSucceedAnimation()V

    goto/16 :goto_0

    :cond_8
    move v1, v3

    .line 229
    goto :goto_2

    .line 234
    :cond_9
    iget v1, v0, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventStatus:I

    packed-switch v1, :pswitch_data_1

    :pswitch_4
    goto/16 :goto_0

    .line 245
    :pswitch_5
    const-string v1, "IdentifyFingerprintDialog"

    const-string v2, "STATUS_TIMEOUT"

    invoke-static {v1, v2}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 246
    iget-boolean v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mIsBackupState:Z

    if-nez v1, :cond_1

    .line 249
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->aniDrawableListner:Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog$AnimationDrawableListener;

    if-eqz v1, :cond_a

    .line 250
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->aniDrawableListner:Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog$AnimationDrawableListener;

    invoke-virtual {v1}, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog$AnimationDrawableListener;->stop()V

    .line 252
    :cond_a
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mUnlockButton:Landroid/widget/ImageView;

    if-eqz v1, :cond_b

    .line 253
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mUnlockButton:Landroid/widget/ImageView;

    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 255
    :cond_b
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mUnlockLine:Landroid/widget/ImageView;

    if-eqz v1, :cond_c

    .line 256
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mUnlockLine:Landroid/widget/ImageView;

    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 258
    :cond_c
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mInfoImage:Landroid/widget/ImageView;

    if-eqz v1, :cond_d

    .line 259
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mInfoImage:Landroid/widget/ImageView;

    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 261
    :cond_d
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mScrollImage:Landroid/widget/ImageView;

    if-eqz v1, :cond_e

    .line 262
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mScrollImage:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->clearAnimation()V

    .line 263
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mScrollImage:Landroid/widget/ImageView;

    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 265
    :cond_e
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mDialogTitleTextView:Landroid/widget/TextView;

    if-eqz v1, :cond_f

    .line 266
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mDialogTitleTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 268
    :cond_f
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mDialogLogoIconView:Landroid/widget/ImageView;

    if-eqz v1, :cond_10

    .line 269
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mDialogLogoIconView:Landroid/widget/ImageView;

    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 271
    :cond_10
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mCloseButton:Landroid/widget/ImageButton;

    if-eqz v1, :cond_11

    .line 272
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mCloseButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v6}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 274
    :cond_11
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f06000e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->setInfoText(Ljava/lang/String;)V

    .line 275
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mHandler:Landroid/os/Handler;

    new-instance v2, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog$2;

    invoke-direct {v2, p0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog$2;-><init>(Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;)V

    const-wide/16 v4, 0xbb8

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    .line 236
    :pswitch_6
    iget-boolean v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mIsInternalCancel:Z

    if-nez v1, :cond_12

    .line 237
    const-string v1, "IdentifyFingerprintDialog"

    const-string v2, "STATUS_USER_CANCELLED"

    invoke-static {v1, v2}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 238
    invoke-virtual {p0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->dismiss()V

    goto/16 :goto_0

    .line 240
    :cond_12
    const-string v1, "IdentifyFingerprintDialog"

    const-string v2, "Internal Cancel. Do not dismiss dialog."

    invoke-static {v1, v2}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 241
    iput-boolean v3, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mIsInternalCancel:Z

    goto/16 :goto_0

    .line 284
    :pswitch_7
    invoke-virtual {p0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->startFailedAnimation()V

    goto/16 :goto_0

    .line 141
    nop

    :pswitch_data_0
    .packed-switch 0xb
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_2
    .end packed-switch

    .line 181
    :sswitch_data_0
    .sparse-switch
        0x7 -> :sswitch_0
        0x14 -> :sswitch_1
    .end sparse-switch

    .line 234
    :pswitch_data_1
    .packed-switch 0x4
        :pswitch_5
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_6
        :pswitch_4
        :pswitch_4
        :pswitch_7
        :pswitch_7
    .end packed-switch
.end method

.method public onKey(Landroid/content/DialogInterface;ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "keyCode"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v1, 0x1

    .line 1120
    const/4 v0, 0x3

    if-ne p2, v0, :cond_0

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-ne v0, v1, :cond_0

    .line 1121
    iget-boolean v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mIsDetecedFinger:Z

    if-eqz v0, :cond_1

    .line 1122
    iput-boolean v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mIsHomeKeyPressed:Z

    .line 1123
    const-string v0, "IdentifyFingerprintDialog"

    const-string v1, "onKey: Finger Detected & Home key pressed"

    invoke-static {v0, v1}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1129
    :cond_0
    :goto_0
    const/4 v0, 0x0

    return v0

    .line 1126
    :cond_1
    invoke-direct {p0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->launchHomeScreen()V

    goto :goto_0
.end method

.method protected onStart()V
    .locals 4

    .prologue
    .line 478
    invoke-super {p0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog;->onStart()V

    .line 479
    const-string v1, "IdentifyFingerprintDialog"

    const-string v2, "onStart"

    invoke-static {v1, v2}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 480
    const/4 v0, 0x1

    .line 482
    .local v0, "isError":Z
    iget-boolean v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mIsFingerprintLockedOut:Z

    if-eqz v1, :cond_1

    .line 483
    const-string v1, "IdentifyFingerprintDialog"

    const-string v2, "Identify request is denied and Screen is Locked out"

    invoke-static {v1, v2}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 504
    :cond_0
    :goto_0
    return-void

    .line 487
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mFpmsService:Lcom/samsung/android/fingerprint/service/FingerprintManagerService;

    invoke-virtual {v1}, Lcom/samsung/android/fingerprint/service/FingerprintManagerService;->isSensorReady()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 488
    invoke-direct {p0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->identify()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 489
    invoke-direct {p0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->disalbeHomeKey()V

    .line 490
    const/4 v0, 0x0

    .line 493
    :cond_2
    if-eqz v0, :cond_0

    .line 494
    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mToastContext:Landroid/content/Context;

    const v2, 0x7f06000c

    const/4 v3, 0x2

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 495
    iget-boolean v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mEnableBackup:Z

    if-nez v1, :cond_3

    .line 496
    invoke-virtual {p0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->dismiss()V

    goto :goto_0

    .line 501
    :cond_3
    invoke-direct {p0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->showBackupDialog()V

    goto :goto_0
.end method

.method protected onStop()V
    .locals 0

    .prologue
    .line 508
    invoke-super {p0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog;->onStop()V

    .line 509
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 2
    .param p1, "hasFocus"    # Z

    .prologue
    .line 627
    invoke-super {p0, p1}, Lcom/samsung/android/fingerprint/service/ui/IdentifyBaseDialog;->onWindowFocusChanged(Z)V

    .line 628
    if-nez p1, :cond_0

    .line 629
    const-string v0, "IdentifyFingerprintDialog"

    const-string v1, "Fingerprint onWindowFocusChanged, dismiss()"

    invoke-static {v0, v1}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 630
    invoke-virtual {p0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->dismiss()V

    .line 632
    :cond_0
    return-void
.end method

.method protected setAnimationLayout()V
    .locals 2

    .prologue
    .line 906
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mAnimationBox:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    .line 907
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mAnimationBox:Landroid/widget/RelativeLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 909
    :cond_0
    new-instance v0, Landroid/graphics/drawable/AnimationDrawable;

    invoke-direct {v0}, Landroid/graphics/drawable/AnimationDrawable;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->aniDrawable:Landroid/graphics/drawable/AnimationDrawable;

    .line 910
    iget-boolean v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mIsFailState:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 911
    invoke-virtual {p0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->startFailedAnimation()V

    .line 921
    :goto_0
    return-void

    .line 912
    :cond_1
    iget-boolean v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mIsSucceed:Z

    if-eqz v0, :cond_2

    .line 913
    invoke-virtual {p0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->startSucceedAnimation()V

    goto :goto_0

    .line 915
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mInfoImage:Landroid/widget/ImageView;

    if-eqz v0, :cond_3

    .line 916
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mInfoImage:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 918
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f060006

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->setInfoText(Ljava/lang/String;)V

    .line 919
    invoke-virtual {p0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->startStandbyAnimation()V

    goto :goto_0
.end method

.method protected declared-synchronized startFailedAnimation()V
    .locals 3

    .prologue
    .line 973
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->aniDrawableListner:Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog$AnimationDrawableListener;

    if-eqz v0, :cond_0

    .line 974
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->aniDrawableListner:Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog$AnimationDrawableListener;

    invoke-virtual {v0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog$AnimationDrawableListener;->stop()V

    .line 976
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mIsFailState:Z

    .line 977
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mEvent:Lcom/samsung/android/fingerprint/FingerprintEvent;

    if-eqz v0, :cond_1

    .line 978
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mEvent:Lcom/samsung/android/fingerprint/FingerprintEvent;

    invoke-virtual {v0}, Lcom/samsung/android/fingerprint/FingerprintEvent;->getImageQuality()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mQualityMessage:I

    .line 979
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mEvent:Lcom/samsung/android/fingerprint/FingerprintEvent;

    invoke-virtual {v0}, Lcom/samsung/android/fingerprint/FingerprintEvent;->getImageQualityFeedback()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->setInfoText(Ljava/lang/String;)V

    .line 981
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mInfoImage:Landroid/widget/ImageView;

    if-eqz v0, :cond_2

    .line 982
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mInfoImage:Landroid/widget/ImageView;

    iget v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mQualityMessage:I

    iget-object v2, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mContext:Landroid/content/Context;

    invoke-static {v1, v2}, Lcom/samsung/android/fingerprint/FingerprintManager;->getImageQualityAnimation(ILandroid/content/Context;)Landroid/graphics/drawable/AnimationDrawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 983
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mInfoImage:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 984
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mInfoImage:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/AnimationDrawable;

    iput-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->aniDrawable:Landroid/graphics/drawable/AnimationDrawable;

    .line 986
    :cond_2
    invoke-direct {p0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->resetErrorMsgAndRequest()V

    .line 987
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->aniDrawable:Landroid/graphics/drawable/AnimationDrawable;

    if-eqz v0, :cond_3

    .line 988
    new-instance v0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog$8;

    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->aniDrawable:Landroid/graphics/drawable/AnimationDrawable;

    invoke-direct {v0, p0, v1}, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog$8;-><init>(Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;Landroid/graphics/drawable/AnimationDrawable;)V

    iput-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->aniDrawableListner:Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog$AnimationDrawableListener;

    .line 1001
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->aniDrawableListner:Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog$AnimationDrawableListener;

    if-eqz v0, :cond_4

    .line 1002
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->aniDrawableListner:Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog$AnimationDrawableListener;

    invoke-virtual {v0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog$AnimationDrawableListener;->start()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1004
    :cond_4
    monitor-exit p0

    return-void

    .line 973
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected declared-synchronized startStandbyAnimation()V
    .locals 2

    .prologue
    .line 959
    monitor-enter p0

    :try_start_0
    const-string v0, "IdentifyFingerprintDialog"

    const-string v1, "startStandbyAnimation called"

    invoke-static {v0, v1}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 961
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mAnimationImage:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 962
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mAnimationImage:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 964
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mScrollImage:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    .line 965
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mScrollImage:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    .line 966
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mScrollImage:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 967
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mContext:Landroid/content/Context;

    const v1, 0x7f04000e

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mTweenAni:Landroid/view/animation/Animation;

    .line 968
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mScrollImage:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mTweenAni:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 970
    :cond_1
    monitor-exit p0

    return-void

    .line 959
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected declared-synchronized startSucceedAnimation()V
    .locals 2

    .prologue
    .line 1007
    monitor-enter p0

    :try_start_0
    const-string v0, "IdentifyFingerprintDialog"

    const-string v1, "startSucceedAnimation called"

    invoke-static {v0, v1}, Lcom/samsung/android/fingerprint/service/util/FingerprintLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1008
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->aniDrawableListner:Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog$AnimationDrawableListener;

    if-eqz v0, :cond_0

    .line 1009
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->aniDrawableListner:Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog$AnimationDrawableListener;

    invoke-virtual {v0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog$AnimationDrawableListener;->stop()V

    .line 1011
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mScrollImage:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    .line 1012
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mScrollImage:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1013
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mScrollImage:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1015
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mInfoImage:Landroid/widget/ImageView;

    if-eqz v0, :cond_2

    .line 1016
    iget-object v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mInfoImage:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1030
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->dismiss()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1038
    monitor-exit p0

    return-void

    .line 1007
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected updateLayout()V
    .locals 4

    .prologue
    .line 635
    invoke-virtual {p0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x400

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 636
    invoke-virtual {p0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->initLayout()V

    .line 638
    invoke-static {}, Lcom/samsung/android/fingerprint/service/util/Config;->getRemaingTimeToUnlock()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 639
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->mIsFingerprintLockedOut:Z

    .line 640
    invoke-virtual {p0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->handleLockoutFingerprint()V

    .line 644
    :goto_0
    return-void

    .line 642
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/fingerprint/service/ui/IdentifyFingerprintDialog;->setAnimationLayout()V

    goto :goto_0
.end method
