.class Lcom/linkedin/android/authsso/LinkedInAuthUtil$1$1;
.super Lcom/linkedin/android/developer/IOAuthServiceResultCallback$Stub;
.source "LinkedInAuthUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/linkedin/android/authsso/LinkedInAuthUtil$1;->onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/linkedin/android/authsso/LinkedInAuthUtil$1;


# direct methods
.method constructor <init>(Lcom/linkedin/android/authsso/LinkedInAuthUtil$1;)V
    .locals 0

    .prologue
    .line 45
    iput-object p1, p0, Lcom/linkedin/android/authsso/LinkedInAuthUtil$1$1;->this$0:Lcom/linkedin/android/authsso/LinkedInAuthUtil$1;

    invoke-direct {p0}, Lcom/linkedin/android/developer/IOAuthServiceResultCallback$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Ljava/lang/String;)V
    .locals 1
    .param p1, "error"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 55
    iget-object v0, p0, Lcom/linkedin/android/authsso/LinkedInAuthUtil$1$1;->this$0:Lcom/linkedin/android/authsso/LinkedInAuthUtil$1;

    iget-object v0, v0, Lcom/linkedin/android/authsso/LinkedInAuthUtil$1;->val$latch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 56
    return-void
.end method

.method public onSuccess(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "token"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 49
    iget-object v0, p0, Lcom/linkedin/android/authsso/LinkedInAuthUtil$1$1;->this$0:Lcom/linkedin/android/authsso/LinkedInAuthUtil$1;

    iget-object v0, v0, Lcom/linkedin/android/authsso/LinkedInAuthUtil$1;->val$tokenResult:Lcom/linkedin/android/authsso/LinkedInAuthUtil$TokenResult;

    iput-object p1, v0, Lcom/linkedin/android/authsso/LinkedInAuthUtil$TokenResult;->token:Landroid/os/Bundle;

    .line 50
    iget-object v0, p0, Lcom/linkedin/android/authsso/LinkedInAuthUtil$1$1;->this$0:Lcom/linkedin/android/authsso/LinkedInAuthUtil$1;

    iget-object v0, v0, Lcom/linkedin/android/authsso/LinkedInAuthUtil$1;->val$latch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 51
    return-void
.end method
