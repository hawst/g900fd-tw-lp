.class public Lcom/linkedin/android/authsso/LinkedInAuthUtil;
.super Ljava/lang/Object;
.source "LinkedInAuthUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/linkedin/android/authsso/LinkedInAuthUtil$TokenResult;
    }
.end annotation


# static fields
.field public static final ACTION_GET_TOKEN:Ljava/lang/String; = "com.linkedin.android.developer.action.GET_TOKEN"

.field public static final CATEGORY_SSO:Ljava/lang/String; = "com.linkedin.android.developer.category.SSO"

.field private static final PKG_NAME:Ljava/lang/String; = "com.linkedin.android"

.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const-class v0, Lcom/linkedin/android/authsso/LinkedInAuthUtil;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/linkedin/android/authsso/LinkedInAuthUtil;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    return-void
.end method

.method static synthetic access$100()Ljava/lang/String;
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/linkedin/android/authsso/LinkedInAuthUtil;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method public static getToken(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Landroid/os/Bundle;
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "appId"    # Ljava/lang/String;
    .param p2, "secret"    # Ljava/lang/String;
    .param p3, "scope"    # Ljava/lang/String;
    .param p4, "consentPage"    # I

    .prologue
    const/4 v8, 0x1

    .line 33
    new-instance v5, Lcom/linkedin/android/authsso/LinkedInAuthUtil$TokenResult;

    const/4 v1, 0x0

    invoke-direct {v5, v1}, Lcom/linkedin/android/authsso/LinkedInAuthUtil$TokenResult;-><init>(Lcom/linkedin/android/authsso/LinkedInAuthUtil$1;)V

    .line 35
    .local v5, "tokenResult":Lcom/linkedin/android/authsso/LinkedInAuthUtil$TokenResult;
    new-instance v6, Ljava/util/concurrent/CountDownLatch;

    invoke-direct {v6, v8}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    .line 37
    .local v6, "latch":Ljava/util/concurrent/CountDownLatch;
    new-instance v0, Lcom/linkedin/android/authsso/LinkedInAuthUtil$1;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v6}, Lcom/linkedin/android/authsso/LinkedInAuthUtil$1;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILcom/linkedin/android/authsso/LinkedInAuthUtil$TokenResult;Ljava/util/concurrent/CountDownLatch;)V

    .line 69
    .local v0, "connection":Landroid/content/ServiceConnection;
    new-instance v7, Landroid/content/Intent;

    invoke-direct {v7}, Landroid/content/Intent;-><init>()V

    .line 70
    .local v7, "intent":Landroid/content/Intent;
    const-string v1, "com.linkedin.android"

    invoke-virtual {v7, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 71
    const-string v1, "com.linkedin.android.developer.action.GET_TOKEN"

    invoke-virtual {v7, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 72
    const-string v1, "com.linkedin.android.developer.category.SSO"

    invoke-virtual {v7, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 73
    invoke-virtual {p0, v7, v0, v8}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 75
    :try_start_0
    invoke-virtual {v6}, Ljava/util/concurrent/CountDownLatch;->await()V

    .line 76
    invoke-virtual {p0, v0}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 80
    :goto_0
    iget-object v1, v5, Lcom/linkedin/android/authsso/LinkedInAuthUtil$TokenResult;->token:Landroid/os/Bundle;

    return-object v1

    .line 77
    :catch_0
    move-exception v1

    goto :goto_0
.end method
