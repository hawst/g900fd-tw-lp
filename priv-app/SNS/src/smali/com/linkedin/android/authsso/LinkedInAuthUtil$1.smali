.class final Lcom/linkedin/android/authsso/LinkedInAuthUtil$1;
.super Ljava/lang/Object;
.source "LinkedInAuthUtil.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/linkedin/android/authsso/LinkedInAuthUtil;->getToken(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Landroid/os/Bundle;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field authService:Lcom/linkedin/android/developer/IOAuthService;

.field final synthetic val$appId:Ljava/lang/String;

.field final synthetic val$consentPage:I

.field final synthetic val$latch:Ljava/util/concurrent/CountDownLatch;

.field final synthetic val$scope:Ljava/lang/String;

.field final synthetic val$secret:Ljava/lang/String;

.field final synthetic val$tokenResult:Lcom/linkedin/android/authsso/LinkedInAuthUtil$TokenResult;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILcom/linkedin/android/authsso/LinkedInAuthUtil$TokenResult;Ljava/util/concurrent/CountDownLatch;)V
    .locals 0

    .prologue
    .line 37
    iput-object p1, p0, Lcom/linkedin/android/authsso/LinkedInAuthUtil$1;->val$appId:Ljava/lang/String;

    iput-object p2, p0, Lcom/linkedin/android/authsso/LinkedInAuthUtil$1;->val$secret:Ljava/lang/String;

    iput-object p3, p0, Lcom/linkedin/android/authsso/LinkedInAuthUtil$1;->val$scope:Ljava/lang/String;

    iput p4, p0, Lcom/linkedin/android/authsso/LinkedInAuthUtil$1;->val$consentPage:I

    iput-object p5, p0, Lcom/linkedin/android/authsso/LinkedInAuthUtil$1;->val$tokenResult:Lcom/linkedin/android/authsso/LinkedInAuthUtil$TokenResult;

    iput-object p6, p0, Lcom/linkedin/android/authsso/LinkedInAuthUtil$1;->val$latch:Ljava/util/concurrent/CountDownLatch;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 7
    .param p1, "name"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 42
    # getter for: Lcom/linkedin/android/authsso/LinkedInAuthUtil;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/linkedin/android/authsso/LinkedInAuthUtil;->access$100()Ljava/lang/String;

    move-result-object v0

    const-string v1, "onServiceConnected"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 44
    :try_start_0
    invoke-static {p2}, Lcom/linkedin/android/developer/IOAuthService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/linkedin/android/developer/IOAuthService;

    move-result-object v0

    iput-object v0, p0, Lcom/linkedin/android/authsso/LinkedInAuthUtil$1;->authService:Lcom/linkedin/android/developer/IOAuthService;

    .line 45
    iget-object v0, p0, Lcom/linkedin/android/authsso/LinkedInAuthUtil$1;->authService:Lcom/linkedin/android/developer/IOAuthService;

    iget-object v1, p0, Lcom/linkedin/android/authsso/LinkedInAuthUtil$1;->val$appId:Ljava/lang/String;

    iget-object v2, p0, Lcom/linkedin/android/authsso/LinkedInAuthUtil$1;->val$secret:Ljava/lang/String;

    iget-object v3, p0, Lcom/linkedin/android/authsso/LinkedInAuthUtil$1;->val$scope:Ljava/lang/String;

    iget v4, p0, Lcom/linkedin/android/authsso/LinkedInAuthUtil$1;->val$consentPage:I

    new-instance v5, Lcom/linkedin/android/authsso/LinkedInAuthUtil$1$1;

    invoke-direct {v5, p0}, Lcom/linkedin/android/authsso/LinkedInAuthUtil$1$1;-><init>(Lcom/linkedin/android/authsso/LinkedInAuthUtil$1;)V

    invoke-interface/range {v0 .. v5}, Lcom/linkedin/android/developer/IOAuthService;->getToken(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILcom/linkedin/android/developer/IOAuthServiceResultCallback;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 61
    :goto_0
    return-void

    .line 58
    :catch_0
    move-exception v6

    .line 59
    .local v6, "e":Landroid/os/RemoteException;
    invoke-virtual {v6}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "name"    # Landroid/content/ComponentName;

    .prologue
    .line 65
    # getter for: Lcom/linkedin/android/authsso/LinkedInAuthUtil;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/linkedin/android/authsso/LinkedInAuthUtil;->access$100()Ljava/lang/String;

    move-result-object v0

    const-string v1, "onServiceDisconnected"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 66
    return-void
.end method
