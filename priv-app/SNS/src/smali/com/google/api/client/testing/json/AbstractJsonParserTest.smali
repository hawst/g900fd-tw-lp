.class public abstract Lcom/google/api/client/testing/json/AbstractJsonParserTest;
.super Ljunit/framework/TestCase;
.source "AbstractJsonParserTest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/api/client/testing/json/AbstractJsonParserTest$TypeVariablesPassedAround;,
        Lcom/google/api/client/testing/json/AbstractJsonParserTest$Z;,
        Lcom/google/api/client/testing/json/AbstractJsonParserTest$Y;,
        Lcom/google/api/client/testing/json/AbstractJsonParserTest$X;,
        Lcom/google/api/client/testing/json/AbstractJsonParserTest$EnumValue;,
        Lcom/google/api/client/testing/json/AbstractJsonParserTest$E;,
        Lcom/google/api/client/testing/json/AbstractJsonParserTest$StringNullValue;,
        Lcom/google/api/client/testing/json/AbstractJsonParserTest$FloatMapTypeVariableType;,
        Lcom/google/api/client/testing/json/AbstractJsonParserTest$DoubleListTypeVariableType;,
        Lcom/google/api/client/testing/json/AbstractJsonParserTest$IntArrayTypeVariableType;,
        Lcom/google/api/client/testing/json/AbstractJsonParserTest$IntegerTypeVariableType;,
        Lcom/google/api/client/testing/json/AbstractJsonParserTest$TypeVariableType;,
        Lcom/google/api/client/testing/json/AbstractJsonParserTest$WildCardTypes;,
        Lcom/google/api/client/testing/json/AbstractJsonParserTest$MapOfMapType;,
        Lcom/google/api/client/testing/json/AbstractJsonParserTest$CollectionOfCollectionType;,
        Lcom/google/api/client/testing/json/AbstractJsonParserTest$ArrayType;,
        Lcom/google/api/client/testing/json/AbstractJsonParserTest$AnyType;,
        Lcom/google/api/client/testing/json/AbstractJsonParserTest$NumberTypesAsString;,
        Lcom/google/api/client/testing/json/AbstractJsonParserTest$NumberTypes;,
        Lcom/google/api/client/testing/json/AbstractJsonParserTest$A;,
        Lcom/google/api/client/testing/json/AbstractJsonParserTest$Feed;,
        Lcom/google/api/client/testing/json/AbstractJsonParserTest$Entry;
    }
.end annotation


# static fields
.field static final ANY_TYPE:Ljava/lang/String; = "{\"arr\":[1],\"bool\":true,\"nul\":null,\"num\":5,\"obj\":{\"key\":\"value\"},\"str\":\"value\"}"

.field static final ARRAY_TYPE:Ljava/lang/String; = "{\"arr\":[4,5],\"arr2\":[[1,2],[3]],\"integerArr\":[6,7]}"

.field private static final BOOL_TOP_VALUE:Ljava/lang/String; = "true"

.field static final COLLECTION_TYPE:Ljava/lang/String; = "{\"arr\":[[\"a\",\"b\"],[\"c\"]]}"

.field static final CONTAINED_MAP:Ljava/lang/String; = "{\"map\":{\"title\":\"foo\"}}"

.field static final DOUBLE_LIST_TYPE_VARIABLE_TYPE:Ljava/lang/String; = "{\"arr\":[null,[null,[1.0]]],\"list\":[null,[null,[1.0]]],\"nullValue\":null,\"value\":[1.0]}"

.field private static final EMPTY:Ljava/lang/String; = ""

.field private static final EMPTY_ARRAY:Ljava/lang/String; = "[]"

.field private static final EMPTY_OBJECT:Ljava/lang/String; = "{}"

.field static final ENUM_VALUE:Ljava/lang/String; = "{\"nullValue\":null,\"otherValue\":\"other\",\"value\":\"VALUE\"}"

.field static final FLOAT_MAP_TYPE_VARIABLE_TYPE:Ljava/lang/String; = "{\"arr\":[null,[null,{\"a\":1.0}]],\"list\":[null,[null,{\"a\":1.0}]],\"nullValue\":null,\"value\":{\"a\":1.0}}"

.field static final INTEGER_TYPE_VARIABLE_TYPE:Ljava/lang/String; = "{\"arr\":[null,[null,1]],\"list\":[null,[null,1]],\"nullValue\":null,\"value\":1}"

.field static final INT_ARRAY:Ljava/lang/String; = "[1,2,3]"

.field static final INT_ARRAY_TYPE_VARIABLE_TYPE:Ljava/lang/String; = "{\"arr\":[null,[null,[1]]],\"list\":[null,[null,[1]]],\"nullValue\":null,\"value\":[1]}"

.field private static final JSON_ENTRY:Ljava/lang/String; = "{\"title\":\"foo\"}"

.field private static final JSON_FEED:Ljava/lang/String; = "{\"entries\":[{\"title\":\"foo\"},{\"title\":\"bar\"}]}"

.field static final MAP_TYPE:Ljava/lang/String; = "{\"value\":[{\"map1\":{\"k1\":1,\"k2\":2},\"map2\":{\"kk1\":3,\"kk2\":4}}]}"

.field private static final NULL_TOP_VALUE:Ljava/lang/String; = "null"

.field static final NULL_VALUE:Ljava/lang/String; = "{\"arr\":[null],\"arr2\":[null,[null]],\"value\":null}"

.field private static final NUMBER_TOP_VALUE:Ljava/lang/String; = "1"

.field static final NUMBER_TYPES:Ljava/lang/String; = "{\"bigDecimalValue\":1.0,\"bigIntegerValue\":1,\"byteObjValue\":1,\"byteValue\":1,\"doubleObjValue\":1.0,\"doubleValue\":1.0,\"floatObjValue\":1.0,\"floatValue\":1.0,\"intObjValue\":1,\"intValue\":1,\"longObjValue\":1,\"longValue\":1,\"shortObjValue\":1,\"shortValue\":1,\"yetAnotherBigDecimalValue\":1}"

.field static final NUMBER_TYPES_AS_STRING:Ljava/lang/String; = "{\"bigDecimalValue\":\"1.0\",\"bigIntegerValue\":\"1\",\"byteObjValue\":\"1\",\"byteValue\":\"1\",\"doubleObjValue\":\"1.0\",\"doubleValue\":\"1.0\",\"floatObjValue\":\"1.0\",\"floatValue\":\"1.0\",\"intObjValue\":\"1\",\"intValue\":\"1\",\"longObjValue\":\"1\",\"longValue\":\"1\",\"shortObjValue\":\"1\",\"shortValue\":\"1\",\"yetAnotherBigDecimalValue\":\"1\"}"

.field static final STRING_ARRAY:Ljava/lang/String; = "[\"a\",\"b\",\"c\"]"

.field private static final STRING_TOP_VALUE:Ljava/lang/String; = "\"a\""

.field static final TYPE_VARS:Ljava/lang/String; = "{\"y\":{\"z\":{\"f\":[\"abc\"]}}}"

.field private static final UTF8_JSON:Ljava/lang/String; = "{\"value\":\"123\u05d9\u05e0\u05d9\u05d1\"}"

.field private static final UTF8_VALUE:Ljava/lang/String; = "123\u05d9\u05e0\u05d9\u05d1"

.field static final WILDCARD_TYPE:Ljava/lang/String; = "{\"lower\":[[1,2,3]],\"map\":{\"v\":1},\"mapInWild\":[{\"v\":1}],\"mapUpper\":{\"v\":1},\"simple\":[[1,2,3]],\"upper\":[[1,2,3]]}"


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 54
    invoke-direct {p0, p1}, Ljunit/framework/TestCase;-><init>(Ljava/lang/String;)V

    .line 55
    return-void
.end method


# virtual methods
.method protected abstract newFactory()Lcom/google/api/client/json/JsonFactory;
.end method

.method public testCurrentToken()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 219
    invoke-virtual {p0}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->newFactory()Lcom/google/api/client/json/JsonFactory;

    move-result-object v1

    const-string v2, "{\"entries\":[{\"title\":\"foo\"},{\"title\":\"bar\"}]}"

    invoke-virtual {v1, v2}, Lcom/google/api/client/json/JsonFactory;->createJsonParser(Ljava/lang/String;)Lcom/google/api/client/json/JsonParser;

    move-result-object v0

    .line 220
    .local v0, "parser":Lcom/google/api/client/json/JsonParser;
    invoke-virtual {v0}, Lcom/google/api/client/json/JsonParser;->getCurrentToken()Lcom/google/api/client/json/JsonToken;

    move-result-object v1

    invoke-static {v1}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertNull(Ljava/lang/Object;)V

    .line 221
    invoke-virtual {v0}, Lcom/google/api/client/json/JsonParser;->nextToken()Lcom/google/api/client/json/JsonToken;

    .line 222
    sget-object v1, Lcom/google/api/client/json/JsonToken;->START_OBJECT:Lcom/google/api/client/json/JsonToken;

    invoke-virtual {v0}, Lcom/google/api/client/json/JsonParser;->getCurrentToken()Lcom/google/api/client/json/JsonToken;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 223
    invoke-virtual {v0}, Lcom/google/api/client/json/JsonParser;->nextToken()Lcom/google/api/client/json/JsonToken;

    .line 224
    sget-object v1, Lcom/google/api/client/json/JsonToken;->FIELD_NAME:Lcom/google/api/client/json/JsonToken;

    invoke-virtual {v0}, Lcom/google/api/client/json/JsonParser;->getCurrentToken()Lcom/google/api/client/json/JsonToken;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 225
    invoke-virtual {v0}, Lcom/google/api/client/json/JsonParser;->nextToken()Lcom/google/api/client/json/JsonToken;

    .line 226
    sget-object v1, Lcom/google/api/client/json/JsonToken;->START_ARRAY:Lcom/google/api/client/json/JsonToken;

    invoke-virtual {v0}, Lcom/google/api/client/json/JsonParser;->getCurrentToken()Lcom/google/api/client/json/JsonToken;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 227
    invoke-virtual {v0}, Lcom/google/api/client/json/JsonParser;->nextToken()Lcom/google/api/client/json/JsonToken;

    .line 228
    sget-object v1, Lcom/google/api/client/json/JsonToken;->START_OBJECT:Lcom/google/api/client/json/JsonToken;

    invoke-virtual {v0}, Lcom/google/api/client/json/JsonParser;->getCurrentToken()Lcom/google/api/client/json/JsonToken;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 229
    invoke-virtual {v0}, Lcom/google/api/client/json/JsonParser;->nextToken()Lcom/google/api/client/json/JsonToken;

    .line 230
    sget-object v1, Lcom/google/api/client/json/JsonToken;->FIELD_NAME:Lcom/google/api/client/json/JsonToken;

    invoke-virtual {v0}, Lcom/google/api/client/json/JsonParser;->getCurrentToken()Lcom/google/api/client/json/JsonToken;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 231
    invoke-virtual {v0}, Lcom/google/api/client/json/JsonParser;->nextToken()Lcom/google/api/client/json/JsonToken;

    .line 232
    sget-object v1, Lcom/google/api/client/json/JsonToken;->VALUE_STRING:Lcom/google/api/client/json/JsonToken;

    invoke-virtual {v0}, Lcom/google/api/client/json/JsonParser;->getCurrentToken()Lcom/google/api/client/json/JsonToken;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 233
    invoke-virtual {v0}, Lcom/google/api/client/json/JsonParser;->nextToken()Lcom/google/api/client/json/JsonToken;

    .line 234
    sget-object v1, Lcom/google/api/client/json/JsonToken;->END_OBJECT:Lcom/google/api/client/json/JsonToken;

    invoke-virtual {v0}, Lcom/google/api/client/json/JsonParser;->getCurrentToken()Lcom/google/api/client/json/JsonToken;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 235
    invoke-virtual {v0}, Lcom/google/api/client/json/JsonParser;->nextToken()Lcom/google/api/client/json/JsonToken;

    .line 236
    sget-object v1, Lcom/google/api/client/json/JsonToken;->START_OBJECT:Lcom/google/api/client/json/JsonToken;

    invoke-virtual {v0}, Lcom/google/api/client/json/JsonParser;->getCurrentToken()Lcom/google/api/client/json/JsonToken;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 237
    invoke-virtual {v0}, Lcom/google/api/client/json/JsonParser;->nextToken()Lcom/google/api/client/json/JsonToken;

    .line 238
    sget-object v1, Lcom/google/api/client/json/JsonToken;->FIELD_NAME:Lcom/google/api/client/json/JsonToken;

    invoke-virtual {v0}, Lcom/google/api/client/json/JsonParser;->getCurrentToken()Lcom/google/api/client/json/JsonToken;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 239
    invoke-virtual {v0}, Lcom/google/api/client/json/JsonParser;->nextToken()Lcom/google/api/client/json/JsonToken;

    .line 240
    sget-object v1, Lcom/google/api/client/json/JsonToken;->VALUE_STRING:Lcom/google/api/client/json/JsonToken;

    invoke-virtual {v0}, Lcom/google/api/client/json/JsonParser;->getCurrentToken()Lcom/google/api/client/json/JsonToken;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 241
    invoke-virtual {v0}, Lcom/google/api/client/json/JsonParser;->nextToken()Lcom/google/api/client/json/JsonToken;

    .line 242
    sget-object v1, Lcom/google/api/client/json/JsonToken;->END_OBJECT:Lcom/google/api/client/json/JsonToken;

    invoke-virtual {v0}, Lcom/google/api/client/json/JsonParser;->getCurrentToken()Lcom/google/api/client/json/JsonToken;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 243
    invoke-virtual {v0}, Lcom/google/api/client/json/JsonParser;->nextToken()Lcom/google/api/client/json/JsonToken;

    .line 244
    sget-object v1, Lcom/google/api/client/json/JsonToken;->END_ARRAY:Lcom/google/api/client/json/JsonToken;

    invoke-virtual {v0}, Lcom/google/api/client/json/JsonParser;->getCurrentToken()Lcom/google/api/client/json/JsonToken;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 245
    invoke-virtual {v0}, Lcom/google/api/client/json/JsonParser;->nextToken()Lcom/google/api/client/json/JsonToken;

    .line 246
    sget-object v1, Lcom/google/api/client/json/JsonToken;->END_OBJECT:Lcom/google/api/client/json/JsonToken;

    invoke-virtual {v0}, Lcom/google/api/client/json/JsonParser;->getCurrentToken()Lcom/google/api/client/json/JsonToken;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 247
    invoke-virtual {v0}, Lcom/google/api/client/json/JsonParser;->nextToken()Lcom/google/api/client/json/JsonToken;

    .line 248
    invoke-virtual {v0}, Lcom/google/api/client/json/JsonParser;->getCurrentToken()Lcom/google/api/client/json/JsonToken;

    move-result-object v1

    invoke-static {v1}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertNull(Ljava/lang/Object;)V

    .line 249
    return-void
.end method

.method public testNextToken()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 201
    invoke-virtual {p0}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->newFactory()Lcom/google/api/client/json/JsonFactory;

    move-result-object v1

    const-string v2, "{\"entries\":[{\"title\":\"foo\"},{\"title\":\"bar\"}]}"

    invoke-virtual {v1, v2}, Lcom/google/api/client/json/JsonFactory;->createJsonParser(Ljava/lang/String;)Lcom/google/api/client/json/JsonParser;

    move-result-object v0

    .line 202
    .local v0, "parser":Lcom/google/api/client/json/JsonParser;
    sget-object v1, Lcom/google/api/client/json/JsonToken;->START_OBJECT:Lcom/google/api/client/json/JsonToken;

    invoke-virtual {v0}, Lcom/google/api/client/json/JsonParser;->nextToken()Lcom/google/api/client/json/JsonToken;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 203
    sget-object v1, Lcom/google/api/client/json/JsonToken;->FIELD_NAME:Lcom/google/api/client/json/JsonToken;

    invoke-virtual {v0}, Lcom/google/api/client/json/JsonParser;->nextToken()Lcom/google/api/client/json/JsonToken;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 204
    sget-object v1, Lcom/google/api/client/json/JsonToken;->START_ARRAY:Lcom/google/api/client/json/JsonToken;

    invoke-virtual {v0}, Lcom/google/api/client/json/JsonParser;->nextToken()Lcom/google/api/client/json/JsonToken;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 205
    sget-object v1, Lcom/google/api/client/json/JsonToken;->START_OBJECT:Lcom/google/api/client/json/JsonToken;

    invoke-virtual {v0}, Lcom/google/api/client/json/JsonParser;->nextToken()Lcom/google/api/client/json/JsonToken;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 206
    sget-object v1, Lcom/google/api/client/json/JsonToken;->FIELD_NAME:Lcom/google/api/client/json/JsonToken;

    invoke-virtual {v0}, Lcom/google/api/client/json/JsonParser;->nextToken()Lcom/google/api/client/json/JsonToken;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 207
    sget-object v1, Lcom/google/api/client/json/JsonToken;->VALUE_STRING:Lcom/google/api/client/json/JsonToken;

    invoke-virtual {v0}, Lcom/google/api/client/json/JsonParser;->nextToken()Lcom/google/api/client/json/JsonToken;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 208
    sget-object v1, Lcom/google/api/client/json/JsonToken;->END_OBJECT:Lcom/google/api/client/json/JsonToken;

    invoke-virtual {v0}, Lcom/google/api/client/json/JsonParser;->nextToken()Lcom/google/api/client/json/JsonToken;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 209
    sget-object v1, Lcom/google/api/client/json/JsonToken;->START_OBJECT:Lcom/google/api/client/json/JsonToken;

    invoke-virtual {v0}, Lcom/google/api/client/json/JsonParser;->nextToken()Lcom/google/api/client/json/JsonToken;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 210
    sget-object v1, Lcom/google/api/client/json/JsonToken;->FIELD_NAME:Lcom/google/api/client/json/JsonToken;

    invoke-virtual {v0}, Lcom/google/api/client/json/JsonParser;->nextToken()Lcom/google/api/client/json/JsonToken;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 211
    sget-object v1, Lcom/google/api/client/json/JsonToken;->VALUE_STRING:Lcom/google/api/client/json/JsonToken;

    invoke-virtual {v0}, Lcom/google/api/client/json/JsonParser;->nextToken()Lcom/google/api/client/json/JsonToken;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 212
    sget-object v1, Lcom/google/api/client/json/JsonToken;->END_OBJECT:Lcom/google/api/client/json/JsonToken;

    invoke-virtual {v0}, Lcom/google/api/client/json/JsonParser;->nextToken()Lcom/google/api/client/json/JsonToken;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 213
    sget-object v1, Lcom/google/api/client/json/JsonToken;->END_ARRAY:Lcom/google/api/client/json/JsonToken;

    invoke-virtual {v0}, Lcom/google/api/client/json/JsonParser;->nextToken()Lcom/google/api/client/json/JsonToken;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 214
    sget-object v1, Lcom/google/api/client/json/JsonToken;->END_OBJECT:Lcom/google/api/client/json/JsonToken;

    invoke-virtual {v0}, Lcom/google/api/client/json/JsonParser;->nextToken()Lcom/google/api/client/json/JsonToken;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 215
    invoke-virtual {v0}, Lcom/google/api/client/json/JsonParser;->nextToken()Lcom/google/api/client/json/JsonToken;

    move-result-object v1

    invoke-static {v1}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertNull(Ljava/lang/Object;)V

    .line 216
    return-void
.end method

.method public testParse()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 269
    invoke-virtual {p0}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->newFactory()Lcom/google/api/client/json/JsonFactory;

    move-result-object v2

    const-string v3, "{\"map\":{\"title\":\"foo\"}}"

    invoke-virtual {v2, v3}, Lcom/google/api/client/json/JsonFactory;->createJsonParser(Ljava/lang/String;)Lcom/google/api/client/json/JsonParser;

    move-result-object v1

    .line 270
    .local v1, "parser":Lcom/google/api/client/json/JsonParser;
    invoke-virtual {v1}, Lcom/google/api/client/json/JsonParser;->nextToken()Lcom/google/api/client/json/JsonToken;

    .line 271
    const-class v2, Lcom/google/api/client/testing/json/AbstractJsonParserTest$A;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/google/api/client/json/JsonParser;->parse(Ljava/lang/Class;Lcom/google/api/client/json/CustomizeJsonParser;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/client/testing/json/AbstractJsonParserTest$A;

    .line 272
    .local v0, "a":Lcom/google/api/client/testing/json/AbstractJsonParserTest$A;
    const-string v2, "title"

    const-string v3, "foo"

    invoke-static {v2, v3}, Lcom/google/common/collect/ImmutableMap;->of(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableMap;

    move-result-object v2

    iget-object v3, v0, Lcom/google/api/client/testing/json/AbstractJsonParserTest$A;->map:Ljava/util/Map;

    invoke-static {v2, v3}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 273
    return-void
.end method

.method public testParseEntry()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 109
    invoke-virtual {p0}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->newFactory()Lcom/google/api/client/json/JsonFactory;

    move-result-object v1

    const-string v2, "{\"title\":\"foo\"}"

    invoke-virtual {v1, v2}, Lcom/google/api/client/json/JsonFactory;->createJsonParser(Ljava/lang/String;)Lcom/google/api/client/json/JsonParser;

    move-result-object v1

    const-class v2, Lcom/google/api/client/testing/json/AbstractJsonParserTest$Entry;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/google/api/client/json/JsonParser;->parseAndClose(Ljava/lang/Class;Lcom/google/api/client/json/CustomizeJsonParser;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/client/testing/json/AbstractJsonParserTest$Entry;

    .line 110
    .local v0, "entry":Lcom/google/api/client/testing/json/AbstractJsonParserTest$Entry;
    const-string v1, "foo"

    iget-object v2, v0, Lcom/google/api/client/testing/json/AbstractJsonParserTest$Entry;->title:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    return-void
.end method

.method public testParseEntryAsMap()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 138
    invoke-virtual {p0}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->newFactory()Lcom/google/api/client/json/JsonFactory;

    move-result-object v2

    const-string v3, "{\"title\":\"foo\"}"

    invoke-virtual {v2, v3}, Lcom/google/api/client/json/JsonFactory;->createJsonParser(Ljava/lang/String;)Lcom/google/api/client/json/JsonParser;

    move-result-object v1

    .line 139
    .local v1, "parser":Lcom/google/api/client/json/JsonParser;
    invoke-virtual {v1}, Lcom/google/api/client/json/JsonParser;->nextToken()Lcom/google/api/client/json/JsonToken;

    .line 140
    const-class v2, Ljava/util/HashMap;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/google/api/client/json/JsonParser;->parseAndClose(Ljava/lang/Class;Lcom/google/api/client/json/CustomizeJsonParser;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    .line 141
    .local v0, "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string v2, "foo"

    const-string v3, "title"

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 142
    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v2

    invoke-static {v2}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertTrue(Z)V

    .line 143
    return-void
.end method

.method public testParseFeed()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 127
    invoke-virtual {p0}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->newFactory()Lcom/google/api/client/json/JsonFactory;

    move-result-object v3

    const-string v4, "{\"entries\":[{\"title\":\"foo\"},{\"title\":\"bar\"}]}"

    invoke-virtual {v3, v4}, Lcom/google/api/client/json/JsonFactory;->createJsonParser(Ljava/lang/String;)Lcom/google/api/client/json/JsonParser;

    move-result-object v2

    .line 128
    .local v2, "parser":Lcom/google/api/client/json/JsonParser;
    invoke-virtual {v2}, Lcom/google/api/client/json/JsonParser;->nextToken()Lcom/google/api/client/json/JsonToken;

    .line 129
    const-class v3, Lcom/google/api/client/testing/json/AbstractJsonParserTest$Feed;

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/google/api/client/json/JsonParser;->parseAndClose(Ljava/lang/Class;Lcom/google/api/client/json/CustomizeJsonParser;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/client/testing/json/AbstractJsonParserTest$Feed;

    .line 130
    .local v0, "feed":Lcom/google/api/client/testing/json/AbstractJsonParserTest$Feed;
    iget-object v3, v0, Lcom/google/api/client/testing/json/AbstractJsonParserTest$Feed;->entries:Ljava/util/Collection;

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 131
    .local v1, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/google/api/client/testing/json/AbstractJsonParserTest$Entry;>;"
    const-string v4, "foo"

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/api/client/testing/json/AbstractJsonParserTest$Entry;

    iget-object v3, v3, Lcom/google/api/client/testing/json/AbstractJsonParserTest$Entry;->title:Ljava/lang/String;

    invoke-static {v4, v3}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    const-string v4, "bar"

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/api/client/testing/json/AbstractJsonParserTest$Entry;

    iget-object v3, v3, Lcom/google/api/client/testing/json/AbstractJsonParserTest$Entry;->title:Ljava/lang/String;

    invoke-static {v4, v3}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(Ljava/lang/String;Ljava/lang/String;)V

    .line 133
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    invoke-static {v3}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertFalse(Z)V

    .line 134
    return-void
.end method

.method public testParse_empty()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 62
    invoke-virtual {p0}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->newFactory()Lcom/google/api/client/json/JsonFactory;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Lcom/google/api/client/json/JsonFactory;->createJsonParser(Ljava/lang/String;)Lcom/google/api/client/json/JsonParser;

    move-result-object v0

    .line 63
    .local v0, "parser":Lcom/google/api/client/json/JsonParser;
    invoke-virtual {v0}, Lcom/google/api/client/json/JsonParser;->nextToken()Lcom/google/api/client/json/JsonToken;

    .line 65
    :try_start_0
    const-class v1, Ljava/util/HashMap;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/api/client/json/JsonParser;->parseAndClose(Ljava/lang/Class;Lcom/google/api/client/json/CustomizeJsonParser;)Ljava/lang/Object;

    .line 66
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "expected "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-class v2, Ljava/lang/IllegalArgumentException;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->fail(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 70
    :goto_0
    return-void

    .line 67
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public testParse_emptyGenericJson()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 83
    invoke-virtual {p0}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->newFactory()Lcom/google/api/client/json/JsonFactory;

    move-result-object v2

    const-string v3, "{}"

    invoke-virtual {v2, v3}, Lcom/google/api/client/json/JsonFactory;->createJsonParser(Ljava/lang/String;)Lcom/google/api/client/json/JsonParser;

    move-result-object v1

    .line 84
    .local v1, "parser":Lcom/google/api/client/json/JsonParser;
    invoke-virtual {v1}, Lcom/google/api/client/json/JsonParser;->nextToken()Lcom/google/api/client/json/JsonToken;

    .line 85
    const-class v2, Lcom/google/api/client/json/GenericJson;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/google/api/client/json/JsonParser;->parseAndClose(Ljava/lang/Class;Lcom/google/api/client/json/CustomizeJsonParser;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/client/json/GenericJson;

    .line 86
    .local v0, "json":Lcom/google/api/client/json/GenericJson;
    invoke-virtual {v0}, Lcom/google/api/client/json/GenericJson;->isEmpty()Z

    move-result v2

    invoke-static {v2}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertTrue(Z)V

    .line 87
    return-void
.end method

.method public testParse_emptyMap()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 75
    invoke-virtual {p0}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->newFactory()Lcom/google/api/client/json/JsonFactory;

    move-result-object v2

    const-string v3, "{}"

    invoke-virtual {v2, v3}, Lcom/google/api/client/json/JsonFactory;->createJsonParser(Ljava/lang/String;)Lcom/google/api/client/json/JsonParser;

    move-result-object v1

    .line 76
    .local v1, "parser":Lcom/google/api/client/json/JsonParser;
    invoke-virtual {v1}, Lcom/google/api/client/json/JsonParser;->nextToken()Lcom/google/api/client/json/JsonToken;

    .line 78
    const-class v2, Ljava/util/HashMap;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/google/api/client/json/JsonParser;->parseAndClose(Ljava/lang/Class;Lcom/google/api/client/json/CustomizeJsonParser;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    .line 79
    .local v0, "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v2

    invoke-static {v2}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertTrue(Z)V

    .line 80
    return-void
.end method

.method public testParser_anyType()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 438
    invoke-virtual {p0}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->newFactory()Lcom/google/api/client/json/JsonFactory;

    move-result-object v0

    .line 440
    .local v0, "factory":Lcom/google/api/client/json/JsonFactory;
    const-string v3, "{\"arr\":[1],\"bool\":true,\"nul\":null,\"num\":5,\"obj\":{\"key\":\"value\"},\"str\":\"value\"}"

    invoke-virtual {v0, v3}, Lcom/google/api/client/json/JsonFactory;->createJsonParser(Ljava/lang/String;)Lcom/google/api/client/json/JsonParser;

    move-result-object v1

    .line 441
    .local v1, "parser":Lcom/google/api/client/json/JsonParser;
    invoke-virtual {v1}, Lcom/google/api/client/json/JsonParser;->nextToken()Lcom/google/api/client/json/JsonToken;

    .line 442
    const-class v3, Lcom/google/api/client/testing/json/AbstractJsonParserTest$AnyType;

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, Lcom/google/api/client/json/JsonParser;->parse(Ljava/lang/Class;Lcom/google/api/client/json/CustomizeJsonParser;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/api/client/testing/json/AbstractJsonParserTest$AnyType;

    .line 443
    .local v2, "result":Lcom/google/api/client/testing/json/AbstractJsonParserTest$AnyType;
    const-string v3, "{\"arr\":[1],\"bool\":true,\"nul\":null,\"num\":5,\"obj\":{\"key\":\"value\"},\"str\":\"value\"}"

    invoke-virtual {v0, v2}, Lcom/google/api/client/json/JsonFactory;->toString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(Ljava/lang/String;Ljava/lang/String;)V

    .line 444
    return-void
.end method

.method public testParser_arrayType()V
    .locals 14
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v13, 0x1

    const/4 v12, 0x0

    const/4 v11, 0x2

    .line 460
    invoke-virtual {p0}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->newFactory()Lcom/google/api/client/json/JsonFactory;

    move-result-object v5

    .line 462
    .local v5, "factory":Lcom/google/api/client/json/JsonFactory;
    const-string v9, "{\"arr\":[4,5],\"arr2\":[[1,2],[3]],\"integerArr\":[6,7]}"

    invoke-virtual {v5, v9}, Lcom/google/api/client/json/JsonFactory;->createJsonParser(Ljava/lang/String;)Lcom/google/api/client/json/JsonParser;

    move-result-object v7

    .line 463
    .local v7, "parser":Lcom/google/api/client/json/JsonParser;
    invoke-virtual {v7}, Lcom/google/api/client/json/JsonParser;->nextToken()Lcom/google/api/client/json/JsonToken;

    .line 464
    const-class v9, Lcom/google/api/client/testing/json/AbstractJsonParserTest$ArrayType;

    const/4 v10, 0x0

    invoke-virtual {v7, v9, v10}, Lcom/google/api/client/json/JsonParser;->parse(Ljava/lang/Class;Lcom/google/api/client/json/CustomizeJsonParser;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/google/api/client/testing/json/AbstractJsonParserTest$ArrayType;

    .line 465
    .local v8, "result":Lcom/google/api/client/testing/json/AbstractJsonParserTest$ArrayType;
    const-string v9, "{\"arr\":[4,5],\"arr2\":[[1,2],[3]],\"integerArr\":[6,7]}"

    invoke-virtual {v5, v8}, Lcom/google/api/client/json/JsonFactory;->toString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(Ljava/lang/String;Ljava/lang/String;)V

    .line 467
    iget-object v0, v8, Lcom/google/api/client/testing/json/AbstractJsonParserTest$ArrayType;->arr:[I

    .line 468
    .local v0, "arr":[I
    new-array v9, v11, [I

    fill-array-data v9, :array_0

    invoke-static {v9, v0}, Ljava/util/Arrays;->equals([I[I)Z

    move-result v9

    invoke-static {v9}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertTrue(Z)V

    .line 469
    iget-object v1, v8, Lcom/google/api/client/testing/json/AbstractJsonParserTest$ArrayType;->arr2:[[I

    .line 470
    .local v1, "arr2":[[I
    array-length v9, v1

    invoke-static {v11, v9}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(II)V

    .line 471
    aget-object v2, v1, v12

    .line 472
    .local v2, "arr20":[I
    array-length v9, v2

    invoke-static {v11, v9}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(II)V

    .line 473
    aget v3, v2, v12

    .line 474
    .local v3, "arr200":I
    invoke-static {v13, v3}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(II)V

    .line 475
    aget v9, v2, v13

    invoke-static {v11, v9}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(II)V

    .line 476
    aget-object v4, v1, v13

    .line 477
    .local v4, "arr21":[I
    array-length v9, v4

    invoke-static {v13, v9}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(II)V

    .line 478
    const/4 v9, 0x3

    aget v10, v4, v12

    invoke-static {v9, v10}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(II)V

    .line 479
    iget-object v6, v8, Lcom/google/api/client/testing/json/AbstractJsonParserTest$ArrayType;->integerArr:[Ljava/lang/Integer;

    .line 480
    .local v6, "integerArr":[Ljava/lang/Integer;
    array-length v9, v6

    invoke-static {v11, v9}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(II)V

    .line 481
    const/4 v9, 0x6

    aget-object v10, v6, v12

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v10

    invoke-static {v9, v10}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(II)V

    .line 482
    return-void

    .line 468
    nop

    :array_0
    .array-data 4
        0x4
        0x5
    .end array-data
.end method

.method public testParser_bool()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1014
    invoke-virtual {p0}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->newFactory()Lcom/google/api/client/json/JsonFactory;

    move-result-object v0

    .line 1015
    .local v0, "factory":Lcom/google/api/client/json/JsonFactory;
    const-string v2, "true"

    invoke-virtual {v0, v2}, Lcom/google/api/client/json/JsonFactory;->createJsonParser(Ljava/lang/String;)Lcom/google/api/client/json/JsonParser;

    move-result-object v2

    sget-object v3, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/google/api/client/json/JsonParser;->parse(Ljava/lang/Class;Lcom/google/api/client/json/CustomizeJsonParser;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 1016
    .local v1, "result":Z
    const-string v2, "true"

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/api/client/json/JsonFactory;->toString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(Ljava/lang/String;Ljava/lang/String;)V

    .line 1018
    invoke-static {v1}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertTrue(Z)V

    .line 1019
    return-void
.end method

.method public testParser_collectionType()V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 492
    invoke-virtual {p0}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->newFactory()Lcom/google/api/client/json/JsonFactory;

    move-result-object v1

    .line 494
    .local v1, "factory":Lcom/google/api/client/json/JsonFactory;
    const-string v5, "{\"arr\":[[\"a\",\"b\"],[\"c\"]]}"

    invoke-virtual {v1, v5}, Lcom/google/api/client/json/JsonFactory;->createJsonParser(Ljava/lang/String;)Lcom/google/api/client/json/JsonParser;

    move-result-object v3

    .line 495
    .local v3, "parser":Lcom/google/api/client/json/JsonParser;
    invoke-virtual {v3}, Lcom/google/api/client/json/JsonParser;->nextToken()Lcom/google/api/client/json/JsonToken;

    .line 496
    const-class v5, Lcom/google/api/client/testing/json/AbstractJsonParserTest$CollectionOfCollectionType;

    const/4 v6, 0x0

    invoke-virtual {v3, v5, v6}, Lcom/google/api/client/json/JsonParser;->parse(Ljava/lang/Class;Lcom/google/api/client/json/CustomizeJsonParser;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/api/client/testing/json/AbstractJsonParserTest$CollectionOfCollectionType;

    .line 497
    .local v4, "result":Lcom/google/api/client/testing/json/AbstractJsonParserTest$CollectionOfCollectionType;
    const-string v5, "{\"arr\":[[\"a\",\"b\"],[\"c\"]]}"

    invoke-virtual {v1, v4}, Lcom/google/api/client/json/JsonFactory;->toString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(Ljava/lang/String;Ljava/lang/String;)V

    .line 499
    iget-object v0, v4, Lcom/google/api/client/testing/json/AbstractJsonParserTest$CollectionOfCollectionType;->arr:Ljava/util/LinkedList;

    .line 501
    .local v0, "arr":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Ljava/util/LinkedList<Ljava/lang/String;>;>;"
    invoke-virtual {v0, v7}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/LinkedList;

    .line 502
    .local v2, "linkedlist":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Ljava/lang/String;>;"
    const-string v6, "a"

    invoke-virtual {v2, v7}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {v6, v5}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(Ljava/lang/String;Ljava/lang/String;)V

    .line 503
    return-void
.end method

.method public testParser_doubleListTypeVariableType()V
    .locals 14
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 722
    invoke-virtual {p0}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->newFactory()Lcom/google/api/client/json/JsonFactory;

    move-result-object v3

    .line 724
    .local v3, "factory":Lcom/google/api/client/json/JsonFactory;
    const-string v11, "{\"arr\":[null,[null,[1.0]]],\"list\":[null,[null,[1.0]]],\"nullValue\":null,\"value\":[1.0]}"

    invoke-virtual {v3, v11}, Lcom/google/api/client/json/JsonFactory;->createJsonParser(Ljava/lang/String;)Lcom/google/api/client/json/JsonParser;

    move-result-object v6

    .line 725
    .local v6, "parser":Lcom/google/api/client/json/JsonParser;
    invoke-virtual {v6}, Lcom/google/api/client/json/JsonParser;->nextToken()Lcom/google/api/client/json/JsonToken;

    .line 726
    const-class v11, Lcom/google/api/client/testing/json/AbstractJsonParserTest$DoubleListTypeVariableType;

    const/4 v12, 0x0

    invoke-virtual {v6, v11, v12}, Lcom/google/api/client/json/JsonParser;->parse(Ljava/lang/Class;Lcom/google/api/client/json/CustomizeJsonParser;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/api/client/testing/json/AbstractJsonParserTest$DoubleListTypeVariableType;

    .line 728
    .local v7, "result":Lcom/google/api/client/testing/json/AbstractJsonParserTest$DoubleListTypeVariableType;
    const-string v11, "{\"arr\":[null,[null,[1.0]]],\"list\":[null,[null,[1.0]]],\"nullValue\":null,\"value\":[1.0]}"

    invoke-virtual {v3, v7}, Lcom/google/api/client/json/JsonFactory;->toString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(Ljava/lang/String;Ljava/lang/String;)V

    .line 731
    iget-object v0, v7, Lcom/google/api/client/testing/json/AbstractJsonParserTest$DoubleListTypeVariableType;->arr:[[Ljava/lang/Object;

    check-cast v0, [[Ljava/util/List;

    .line 732
    .local v0, "arr":[[Ljava/util/List;, "[[Ljava/util/List<Ljava/lang/Double;>;"
    const/4 v11, 0x2

    array-length v12, v0

    invoke-static {v11, v12}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(II)V

    .line 733
    const-class v11, [Ljava/util/List;

    invoke-static {v11}, Lcom/google/api/client/util/Data;->nullOf(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v11

    const/4 v12, 0x0

    aget-object v12, v0, v12

    invoke-static {v11, v12}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 734
    const/4 v11, 0x1

    aget-object v8, v0, v11

    .line 735
    .local v8, "subArr":[Ljava/util/List;, "[Ljava/util/List<Ljava/lang/Double;>;"
    const/4 v11, 0x2

    array-length v12, v8

    invoke-static {v11, v12}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(II)V

    .line 736
    const-class v11, Ljava/util/ArrayList;

    invoke-static {v11}, Lcom/google/api/client/util/Data;->nullOf(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v11

    const/4 v12, 0x0

    aget-object v12, v8, v12

    invoke-static {v11, v12}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 737
    const/4 v11, 0x1

    aget-object v1, v8, v11

    .line 738
    .local v1, "arrValue":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Double;>;"
    const/4 v11, 0x1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v12

    invoke-static {v11, v12}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(II)V

    .line 739
    const/4 v11, 0x0

    invoke-interface {v1, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Double;

    .line 740
    .local v2, "dValue":Ljava/lang/Double;
    const-wide/high16 v12, 0x3ff0000000000000L    # 1.0

    invoke-static {v12, v13}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v11

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 742
    iget-object v4, v7, Lcom/google/api/client/testing/json/AbstractJsonParserTest$DoubleListTypeVariableType;->list:Ljava/util/LinkedList;

    .line 743
    .local v4, "list":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Ljava/util/LinkedList<Ljava/util/List<Ljava/lang/Double;>;>;>;"
    const/4 v11, 0x2

    invoke-virtual {v4}, Ljava/util/LinkedList;->size()I

    move-result v12

    invoke-static {v11, v12}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(II)V

    .line 744
    const-class v11, Ljava/util/LinkedList;

    invoke-static {v11}, Lcom/google/api/client/util/Data;->nullOf(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v11

    const/4 v12, 0x0

    invoke-virtual {v4, v12}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 745
    const/4 v11, 0x1

    invoke-virtual {v4, v11}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/util/LinkedList;

    .line 746
    .local v9, "subList":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Ljava/util/List<Ljava/lang/Double;>;>;"
    const/4 v11, 0x2

    invoke-virtual {v9}, Ljava/util/LinkedList;->size()I

    move-result v12

    invoke-static {v11, v12}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(II)V

    .line 747
    const-class v11, Ljava/util/ArrayList;

    invoke-static {v11}, Lcom/google/api/client/util/Data;->nullOf(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v11

    const/4 v12, 0x0

    invoke-virtual {v9, v12}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 748
    const/4 v11, 0x1

    invoke-virtual {v9, v11}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "arrValue":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Double;>;"
    check-cast v1, Ljava/util/List;

    .line 749
    .restart local v1    # "arrValue":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Double;>;"
    new-instance v11, Ljava/lang/Double;

    const-wide/high16 v12, 0x3ff0000000000000L    # 1.0

    invoke-direct {v11, v12, v13}, Ljava/lang/Double;-><init>(D)V

    invoke-static {v11}, Lcom/google/common/collect/ImmutableList;->of(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableList;

    move-result-object v11

    invoke-static {v11, v1}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 751
    iget-object v5, v7, Lcom/google/api/client/testing/json/AbstractJsonParserTest$DoubleListTypeVariableType;->nullValue:Ljava/lang/Object;

    check-cast v5, Ljava/util/List;

    .line 752
    .local v5, "nullValue":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Double;>;"
    const-class v11, Ljava/util/ArrayList;

    invoke-static {v11}, Lcom/google/api/client/util/Data;->nullOf(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v11

    invoke-static {v11, v5}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 754
    iget-object v10, v7, Lcom/google/api/client/testing/json/AbstractJsonParserTest$DoubleListTypeVariableType;->value:Ljava/lang/Object;

    check-cast v10, Ljava/util/List;

    .line 755
    .local v10, "value":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Double;>;"
    new-instance v11, Ljava/lang/Double;

    const-wide/high16 v12, 0x3ff0000000000000L    # 1.0

    invoke-direct {v11, v12, v13}, Ljava/lang/Double;-><init>(D)V

    invoke-static {v11}, Lcom/google/common/collect/ImmutableList;->of(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableList;

    move-result-object v11

    invoke-static {v11, v10}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 756
    return-void
.end method

.method public testParser_emptyArray()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 961
    invoke-virtual {p0}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->newFactory()Lcom/google/api/client/json/JsonFactory;

    move-result-object v0

    .line 962
    .local v0, "factory":Lcom/google/api/client/json/JsonFactory;
    const-string v2, "[]"

    invoke-virtual {v0, v2}, Lcom/google/api/client/json/JsonFactory;->createJsonParser(Ljava/lang/String;)Lcom/google/api/client/json/JsonParser;

    move-result-object v2

    const-class v3, [Ljava/lang/String;

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/google/api/client/json/JsonParser;->parse(Ljava/lang/Class;Lcom/google/api/client/json/CustomizeJsonParser;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/String;

    .line 963
    .local v1, "result":[Ljava/lang/String;
    const-string v2, "[]"

    invoke-virtual {v0, v1}, Lcom/google/api/client/json/JsonFactory;->toString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(Ljava/lang/String;Ljava/lang/String;)V

    .line 965
    const/4 v2, 0x0

    array-length v3, v1

    invoke-static {v2, v3}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(II)V

    .line 966
    return-void
.end method

.method public testParser_enumValue()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 885
    invoke-virtual {p0}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->newFactory()Lcom/google/api/client/json/JsonFactory;

    move-result-object v0

    .line 887
    .local v0, "factory":Lcom/google/api/client/json/JsonFactory;
    const-string v3, "{\"nullValue\":null,\"otherValue\":\"other\",\"value\":\"VALUE\"}"

    invoke-virtual {v0, v3}, Lcom/google/api/client/json/JsonFactory;->createJsonParser(Ljava/lang/String;)Lcom/google/api/client/json/JsonParser;

    move-result-object v1

    .line 888
    .local v1, "parser":Lcom/google/api/client/json/JsonParser;
    invoke-virtual {v1}, Lcom/google/api/client/json/JsonParser;->nextToken()Lcom/google/api/client/json/JsonToken;

    .line 889
    const-class v3, Lcom/google/api/client/testing/json/AbstractJsonParserTest$EnumValue;

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, Lcom/google/api/client/json/JsonParser;->parse(Ljava/lang/Class;Lcom/google/api/client/json/CustomizeJsonParser;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/api/client/testing/json/AbstractJsonParserTest$EnumValue;

    .line 891
    .local v2, "result":Lcom/google/api/client/testing/json/AbstractJsonParserTest$EnumValue;
    const-string v3, "{\"nullValue\":null,\"otherValue\":\"other\",\"value\":\"VALUE\"}"

    invoke-virtual {v0, v2}, Lcom/google/api/client/json/JsonFactory;->toString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(Ljava/lang/String;Ljava/lang/String;)V

    .line 893
    sget-object v3, Lcom/google/api/client/testing/json/AbstractJsonParserTest$E;->VALUE:Lcom/google/api/client/testing/json/AbstractJsonParserTest$E;

    iget-object v4, v2, Lcom/google/api/client/testing/json/AbstractJsonParserTest$EnumValue;->value:Lcom/google/api/client/testing/json/AbstractJsonParserTest$E;

    invoke-static {v3, v4}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 894
    sget-object v3, Lcom/google/api/client/testing/json/AbstractJsonParserTest$E;->OTHER_VALUE:Lcom/google/api/client/testing/json/AbstractJsonParserTest$E;

    iget-object v4, v2, Lcom/google/api/client/testing/json/AbstractJsonParserTest$EnumValue;->otherValue:Lcom/google/api/client/testing/json/AbstractJsonParserTest$E;

    invoke-static {v3, v4}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 895
    sget-object v3, Lcom/google/api/client/testing/json/AbstractJsonParserTest$E;->NULL:Lcom/google/api/client/testing/json/AbstractJsonParserTest$E;

    iget-object v4, v2, Lcom/google/api/client/testing/json/AbstractJsonParserTest$EnumValue;->nullValue:Lcom/google/api/client/testing/json/AbstractJsonParserTest$E;

    invoke-static {v3, v4}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 896
    return-void
.end method

.method public testParser_floatMapTypeVariableType()V
    .locals 13
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 760
    invoke-virtual {p0}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->newFactory()Lcom/google/api/client/json/JsonFactory;

    move-result-object v3

    .line 762
    .local v3, "factory":Lcom/google/api/client/json/JsonFactory;
    const-string v11, "{\"arr\":[null,[null,{\"a\":1.0}]],\"list\":[null,[null,{\"a\":1.0}]],\"nullValue\":null,\"value\":{\"a\":1.0}}"

    invoke-virtual {v3, v11}, Lcom/google/api/client/json/JsonFactory;->createJsonParser(Ljava/lang/String;)Lcom/google/api/client/json/JsonParser;

    move-result-object v6

    .line 763
    .local v6, "parser":Lcom/google/api/client/json/JsonParser;
    invoke-virtual {v6}, Lcom/google/api/client/json/JsonParser;->nextToken()Lcom/google/api/client/json/JsonToken;

    .line 764
    const-class v11, Lcom/google/api/client/testing/json/AbstractJsonParserTest$FloatMapTypeVariableType;

    const/4 v12, 0x0

    invoke-virtual {v6, v11, v12}, Lcom/google/api/client/json/JsonParser;->parse(Ljava/lang/Class;Lcom/google/api/client/json/CustomizeJsonParser;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/api/client/testing/json/AbstractJsonParserTest$FloatMapTypeVariableType;

    .line 766
    .local v7, "result":Lcom/google/api/client/testing/json/AbstractJsonParserTest$FloatMapTypeVariableType;
    const-string v11, "{\"arr\":[null,[null,{\"a\":1.0}]],\"list\":[null,[null,{\"a\":1.0}]],\"nullValue\":null,\"value\":{\"a\":1.0}}"

    invoke-virtual {v3, v7}, Lcom/google/api/client/json/JsonFactory;->toString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(Ljava/lang/String;Ljava/lang/String;)V

    .line 769
    iget-object v0, v7, Lcom/google/api/client/testing/json/AbstractJsonParserTest$FloatMapTypeVariableType;->arr:[[Ljava/lang/Object;

    check-cast v0, [[Ljava/util/Map;

    .line 770
    .local v0, "arr":[[Ljava/util/Map;, "[[Ljava/util/Map<Ljava/lang/String;Ljava/lang/Float;>;"
    const/4 v11, 0x2

    array-length v12, v0

    invoke-static {v11, v12}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(II)V

    .line 771
    const-class v11, [Ljava/util/Map;

    invoke-static {v11}, Lcom/google/api/client/util/Data;->nullOf(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v11

    const/4 v12, 0x0

    aget-object v12, v0, v12

    invoke-static {v11, v12}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 772
    const/4 v11, 0x1

    aget-object v8, v0, v11

    .line 773
    .local v8, "subArr":[Ljava/util/Map;, "[Ljava/util/Map<Ljava/lang/String;Ljava/lang/Float;>;"
    const/4 v11, 0x2

    array-length v12, v8

    invoke-static {v11, v12}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(II)V

    .line 774
    const-class v11, Ljava/util/HashMap;

    invoke-static {v11}, Lcom/google/api/client/util/Data;->nullOf(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v11

    const/4 v12, 0x0

    aget-object v12, v8, v12

    invoke-static {v11, v12}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 775
    const/4 v11, 0x1

    aget-object v1, v8, v11

    .line 776
    .local v1, "arrValue":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Float;>;"
    const/4 v11, 0x1

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v12

    invoke-static {v11, v12}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(II)V

    .line 777
    const-string v11, "a"

    invoke-interface {v1, v11}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    .line 778
    .local v2, "fValue":Ljava/lang/Float;
    const/high16 v11, 0x3f800000    # 1.0f

    invoke-static {v11}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v11

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v12

    invoke-static {v12}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 780
    iget-object v4, v7, Lcom/google/api/client/testing/json/AbstractJsonParserTest$FloatMapTypeVariableType;->list:Ljava/util/LinkedList;

    .line 781
    .local v4, "list":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Ljava/util/LinkedList<Ljava/util/Map<Ljava/lang/String;Ljava/lang/Float;>;>;>;"
    const/4 v11, 0x2

    invoke-virtual {v4}, Ljava/util/LinkedList;->size()I

    move-result v12

    invoke-static {v11, v12}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(II)V

    .line 782
    const-class v11, Ljava/util/LinkedList;

    invoke-static {v11}, Lcom/google/api/client/util/Data;->nullOf(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v11

    const/4 v12, 0x0

    invoke-virtual {v4, v12}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 783
    const/4 v11, 0x1

    invoke-virtual {v4, v11}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/util/LinkedList;

    .line 784
    .local v9, "subList":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Ljava/util/Map<Ljava/lang/String;Ljava/lang/Float;>;>;"
    const/4 v11, 0x2

    invoke-virtual {v9}, Ljava/util/LinkedList;->size()I

    move-result v12

    invoke-static {v11, v12}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(II)V

    .line 785
    const-class v11, Ljava/util/HashMap;

    invoke-static {v11}, Lcom/google/api/client/util/Data;->nullOf(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v11

    const/4 v12, 0x0

    invoke-virtual {v9, v12}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 786
    const/4 v11, 0x1

    invoke-virtual {v9, v11}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "arrValue":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Float;>;"
    check-cast v1, Ljava/util/Map;

    .line 787
    .restart local v1    # "arrValue":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Float;>;"
    const/4 v11, 0x1

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v12

    invoke-static {v11, v12}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(II)V

    .line 788
    const-string v11, "a"

    invoke-interface {v1, v11}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "fValue":Ljava/lang/Float;
    check-cast v2, Ljava/lang/Float;

    .line 789
    .restart local v2    # "fValue":Ljava/lang/Float;
    const/high16 v11, 0x3f800000    # 1.0f

    invoke-static {v11}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v11

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v12

    invoke-static {v12}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 791
    iget-object v5, v7, Lcom/google/api/client/testing/json/AbstractJsonParserTest$FloatMapTypeVariableType;->nullValue:Ljava/lang/Object;

    check-cast v5, Ljava/util/Map;

    .line 792
    .local v5, "nullValue":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Float;>;"
    const-class v11, Ljava/util/HashMap;

    invoke-static {v11}, Lcom/google/api/client/util/Data;->nullOf(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v11

    invoke-static {v11, v5}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 794
    iget-object v10, v7, Lcom/google/api/client/testing/json/AbstractJsonParserTest$FloatMapTypeVariableType;->value:Ljava/lang/Object;

    check-cast v10, Ljava/util/Map;

    .line 795
    .local v10, "value":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Float;>;"
    const/4 v11, 0x1

    invoke-interface {v10}, Ljava/util/Map;->size()I

    move-result v12

    invoke-static {v11, v12}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(II)V

    .line 796
    const-string v11, "a"

    invoke-interface {v10, v11}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "fValue":Ljava/lang/Float;
    check-cast v2, Ljava/lang/Float;

    .line 797
    .restart local v2    # "fValue":Ljava/lang/Float;
    const/high16 v11, 0x3f800000    # 1.0f

    invoke-static {v11}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v11

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v12

    invoke-static {v12}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 798
    return-void
.end method

.method public testParser_hashmapForMapType()V
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 535
    invoke-virtual {p0}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->newFactory()Lcom/google/api/client/json/JsonFactory;

    move-result-object v0

    .line 537
    .local v0, "factory":Lcom/google/api/client/json/JsonFactory;
    const-string v7, "{\"value\":[{\"map1\":{\"k1\":1,\"k2\":2},\"map2\":{\"kk1\":3,\"kk2\":4}}]}"

    invoke-virtual {v0, v7}, Lcom/google/api/client/json/JsonFactory;->createJsonParser(Ljava/lang/String;)Lcom/google/api/client/json/JsonParser;

    move-result-object v4

    .line 538
    .local v4, "parser":Lcom/google/api/client/json/JsonParser;
    invoke-virtual {v4}, Lcom/google/api/client/json/JsonParser;->nextToken()Lcom/google/api/client/json/JsonToken;

    .line 540
    const-class v7, Ljava/util/HashMap;

    const/4 v8, 0x0

    invoke-virtual {v4, v7, v8}, Lcom/google/api/client/json/JsonParser;->parse(Ljava/lang/Class;Lcom/google/api/client/json/CustomizeJsonParser;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/HashMap;

    .line 543
    .local v5, "result":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/util/ArrayList<Lcom/google/api/client/util/ArrayMap<Ljava/lang/String;Lcom/google/api/client/util/ArrayMap<Ljava/lang/String;Ljava/math/BigDecimal;>;>;>;>;"
    const-string v7, "{\"value\":[{\"map1\":{\"k1\":1,\"k2\":2},\"map2\":{\"kk1\":3,\"kk2\":4}}]}"

    invoke-virtual {v0, v5}, Lcom/google/api/client/json/JsonFactory;->toString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(Ljava/lang/String;Ljava/lang/String;)V

    .line 545
    const-string v7, "value"

    invoke-virtual {v5, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/ArrayList;

    .line 546
    .local v6, "value":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/api/client/util/ArrayMap<Ljava/lang/String;Lcom/google/api/client/util/ArrayMap<Ljava/lang/String;Ljava/math/BigDecimal;>;>;>;"
    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/api/client/util/ArrayMap;

    .line 547
    .local v1, "firstMap":Lcom/google/api/client/util/ArrayMap;, "Lcom/google/api/client/util/ArrayMap<Ljava/lang/String;Lcom/google/api/client/util/ArrayMap<Ljava/lang/String;Ljava/math/BigDecimal;>;>;"
    const-string v7, "map1"

    invoke-virtual {v1, v7}, Lcom/google/api/client/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/api/client/util/ArrayMap;

    .line 548
    .local v3, "map1":Lcom/google/api/client/util/ArrayMap;, "Lcom/google/api/client/util/ArrayMap<Ljava/lang/String;Ljava/math/BigDecimal;>;"
    const-string v7, "k1"

    invoke-virtual {v3, v7}, Lcom/google/api/client/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/math/BigDecimal;

    .line 549
    .local v2, "integer":Ljava/math/BigDecimal;
    const/4 v7, 0x1

    invoke-virtual {v2}, Ljava/math/BigDecimal;->intValue()I

    move-result v8

    invoke-static {v7, v8}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(II)V

    .line 550
    return-void
.end method

.method public testParser_intArray()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 948
    invoke-virtual {p0}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->newFactory()Lcom/google/api/client/json/JsonFactory;

    move-result-object v0

    .line 950
    .local v0, "factory":Lcom/google/api/client/json/JsonFactory;
    const-string v3, "[1,2,3]"

    invoke-virtual {v0, v3}, Lcom/google/api/client/json/JsonFactory;->createJsonParser(Ljava/lang/String;)Lcom/google/api/client/json/JsonParser;

    move-result-object v1

    .line 951
    .local v1, "parser":Lcom/google/api/client/json/JsonParser;
    invoke-virtual {v1}, Lcom/google/api/client/json/JsonParser;->nextToken()Lcom/google/api/client/json/JsonToken;

    .line 952
    const-class v3, [I

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, Lcom/google/api/client/json/JsonParser;->parse(Ljava/lang/Class;Lcom/google/api/client/json/CustomizeJsonParser;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [I

    .line 953
    .local v2, "result":[I
    const-string v3, "[1,2,3]"

    invoke-virtual {v0, v2}, Lcom/google/api/client/json/JsonFactory;->toString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(Ljava/lang/String;Ljava/lang/String;)V

    .line 955
    const/4 v3, 0x3

    new-array v3, v3, [I

    fill-array-data v3, :array_0

    invoke-static {v3, v2}, Ljava/util/Arrays;->equals([I[I)Z

    move-result v3

    invoke-static {v3}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertTrue(Z)V

    .line 956
    return-void

    .line 955
    nop

    :array_0
    .array-data 4
        0x1
        0x2
        0x3
    .end array-data
.end method

.method public testParser_intArrayTypeVariableType()V
    .locals 15
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v14, 0x2

    const/4 v13, 0x0

    const/4 v12, 0x1

    .line 686
    invoke-virtual {p0}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->newFactory()Lcom/google/api/client/json/JsonFactory;

    move-result-object v2

    .line 688
    .local v2, "factory":Lcom/google/api/client/json/JsonFactory;
    const-string v10, "{\"arr\":[null,[null,[1]]],\"list\":[null,[null,[1]]],\"nullValue\":null,\"value\":[1]}"

    invoke-virtual {v2, v10}, Lcom/google/api/client/json/JsonFactory;->createJsonParser(Ljava/lang/String;)Lcom/google/api/client/json/JsonParser;

    move-result-object v5

    .line 689
    .local v5, "parser":Lcom/google/api/client/json/JsonParser;
    invoke-virtual {v5}, Lcom/google/api/client/json/JsonParser;->nextToken()Lcom/google/api/client/json/JsonToken;

    .line 690
    const-class v10, Lcom/google/api/client/testing/json/AbstractJsonParserTest$IntArrayTypeVariableType;

    const/4 v11, 0x0

    invoke-virtual {v5, v10, v11}, Lcom/google/api/client/json/JsonParser;->parse(Ljava/lang/Class;Lcom/google/api/client/json/CustomizeJsonParser;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/api/client/testing/json/AbstractJsonParserTest$IntArrayTypeVariableType;

    .line 692
    .local v6, "result":Lcom/google/api/client/testing/json/AbstractJsonParserTest$IntArrayTypeVariableType;
    const-string v10, "{\"arr\":[null,[null,[1]]],\"list\":[null,[null,[1]]],\"nullValue\":null,\"value\":[1]}"

    invoke-virtual {v2, v6}, Lcom/google/api/client/json/JsonFactory;->toString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(Ljava/lang/String;Ljava/lang/String;)V

    .line 695
    iget-object v0, v6, Lcom/google/api/client/testing/json/AbstractJsonParserTest$IntArrayTypeVariableType;->arr:[[Ljava/lang/Object;

    check-cast v0, [[[I

    .line 696
    .local v0, "arr":[[[I
    array-length v10, v0

    invoke-static {v14, v10}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(II)V

    .line 697
    const-class v10, [[I

    invoke-static {v10}, Lcom/google/api/client/util/Data;->nullOf(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v10

    aget-object v11, v0, v13

    invoke-static {v10, v11}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 698
    aget-object v7, v0, v12

    .line 699
    .local v7, "subArr":[[I
    array-length v10, v7

    invoke-static {v14, v10}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(II)V

    .line 700
    const-class v10, [I

    invoke-static {v10}, Lcom/google/api/client/util/Data;->nullOf(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v10

    aget-object v11, v7, v13

    invoke-static {v10, v11}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 701
    aget-object v1, v7, v12

    .line 702
    .local v1, "arrValue":[I
    new-array v10, v12, [I

    aput v12, v10, v13

    invoke-static {v10, v1}, Ljava/util/Arrays;->equals([I[I)Z

    move-result v10

    invoke-static {v10}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertTrue(Z)V

    .line 704
    iget-object v3, v6, Lcom/google/api/client/testing/json/AbstractJsonParserTest$IntArrayTypeVariableType;->list:Ljava/util/LinkedList;

    .line 705
    .local v3, "list":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Ljava/util/LinkedList<[I>;>;"
    invoke-virtual {v3}, Ljava/util/LinkedList;->size()I

    move-result v10

    invoke-static {v14, v10}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(II)V

    .line 706
    const-class v10, Ljava/util/LinkedList;

    invoke-static {v10}, Lcom/google/api/client/util/Data;->nullOf(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v10

    invoke-virtual {v3, v13}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 707
    invoke-virtual {v3, v12}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/util/LinkedList;

    .line 708
    .local v8, "subList":Ljava/util/LinkedList;, "Ljava/util/LinkedList<[I>;"
    invoke-virtual {v8}, Ljava/util/LinkedList;->size()I

    move-result v10

    invoke-static {v14, v10}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(II)V

    .line 709
    const-class v10, [I

    invoke-static {v10}, Lcom/google/api/client/util/Data;->nullOf(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v10

    invoke-virtual {v8, v13}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 710
    invoke-virtual {v8, v12}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "arrValue":[I
    check-cast v1, [I

    .line 711
    .restart local v1    # "arrValue":[I
    new-array v10, v12, [I

    aput v12, v10, v13

    invoke-static {v10, v1}, Ljava/util/Arrays;->equals([I[I)Z

    move-result v10

    invoke-static {v10}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertTrue(Z)V

    .line 713
    iget-object v4, v6, Lcom/google/api/client/testing/json/AbstractJsonParserTest$IntArrayTypeVariableType;->nullValue:Ljava/lang/Object;

    check-cast v4, [I

    .line 714
    .local v4, "nullValue":[I
    const-class v10, [I

    invoke-static {v10}, Lcom/google/api/client/util/Data;->nullOf(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v10

    invoke-static {v10, v4}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 716
    iget-object v9, v6, Lcom/google/api/client/testing/json/AbstractJsonParserTest$IntArrayTypeVariableType;->value:Ljava/lang/Object;

    check-cast v9, [I

    .line 717
    .local v9, "value":[I
    new-array v10, v12, [I

    aput v12, v10, v13

    invoke-static {v10, v9}, Ljava/util/Arrays;->equals([I[I)Z

    move-result v10

    invoke-static {v10}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertTrue(Z)V

    .line 718
    return-void
.end method

.method public testParser_integerTypeVariableType()V
    .locals 15
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v14, 0x2

    const/4 v13, 0x0

    const/4 v12, 0x1

    .line 650
    invoke-virtual {p0}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->newFactory()Lcom/google/api/client/json/JsonFactory;

    move-result-object v2

    .line 652
    .local v2, "factory":Lcom/google/api/client/json/JsonFactory;
    const-string v10, "{\"arr\":[null,[null,1]],\"list\":[null,[null,1]],\"nullValue\":null,\"value\":1}"

    invoke-virtual {v2, v10}, Lcom/google/api/client/json/JsonFactory;->createJsonParser(Ljava/lang/String;)Lcom/google/api/client/json/JsonParser;

    move-result-object v5

    .line 653
    .local v5, "parser":Lcom/google/api/client/json/JsonParser;
    invoke-virtual {v5}, Lcom/google/api/client/json/JsonParser;->nextToken()Lcom/google/api/client/json/JsonToken;

    .line 654
    const-class v10, Lcom/google/api/client/testing/json/AbstractJsonParserTest$IntegerTypeVariableType;

    const/4 v11, 0x0

    invoke-virtual {v5, v10, v11}, Lcom/google/api/client/json/JsonParser;->parse(Ljava/lang/Class;Lcom/google/api/client/json/CustomizeJsonParser;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/api/client/testing/json/AbstractJsonParserTest$IntegerTypeVariableType;

    .line 656
    .local v6, "result":Lcom/google/api/client/testing/json/AbstractJsonParserTest$IntegerTypeVariableType;
    const-string v10, "{\"arr\":[null,[null,1]],\"list\":[null,[null,1]],\"nullValue\":null,\"value\":1}"

    invoke-virtual {v2, v6}, Lcom/google/api/client/json/JsonFactory;->toString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(Ljava/lang/String;Ljava/lang/String;)V

    .line 659
    iget-object v0, v6, Lcom/google/api/client/testing/json/AbstractJsonParserTest$IntegerTypeVariableType;->arr:[[Ljava/lang/Object;

    check-cast v0, [[Ljava/lang/Integer;

    .line 660
    .local v0, "arr":[[Ljava/lang/Integer;
    array-length v10, v0

    invoke-static {v14, v10}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(II)V

    .line 661
    const-class v10, [Ljava/lang/Integer;

    invoke-static {v10}, Lcom/google/api/client/util/Data;->nullOf(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v10

    aget-object v11, v0, v13

    invoke-static {v10, v11}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 662
    aget-object v7, v0, v12

    .line 663
    .local v7, "subArr":[Ljava/lang/Integer;
    array-length v10, v7

    invoke-static {v14, v10}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(II)V

    .line 664
    sget-object v10, Lcom/google/api/client/util/Data;->NULL_INTEGER:Ljava/lang/Integer;

    aget-object v11, v7, v13

    invoke-static {v10, v11}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 665
    aget-object v1, v7, v12

    .line 666
    .local v1, "arrValue":Ljava/lang/Integer;
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v10

    invoke-static {v12, v10}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(II)V

    .line 668
    iget-object v3, v6, Lcom/google/api/client/testing/json/AbstractJsonParserTest$IntegerTypeVariableType;->list:Ljava/util/LinkedList;

    .line 669
    .local v3, "list":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Ljava/util/LinkedList<Ljava/lang/Integer;>;>;"
    invoke-virtual {v3}, Ljava/util/LinkedList;->size()I

    move-result v10

    invoke-static {v14, v10}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(II)V

    .line 670
    const-class v10, Ljava/util/LinkedList;

    invoke-static {v10}, Lcom/google/api/client/util/Data;->nullOf(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v10

    invoke-virtual {v3, v13}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 671
    invoke-virtual {v3, v12}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/util/LinkedList;

    .line 672
    .local v8, "subList":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Ljava/lang/Integer;>;"
    invoke-virtual {v8}, Ljava/util/LinkedList;->size()I

    move-result v10

    invoke-static {v14, v10}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(II)V

    .line 673
    sget-object v10, Lcom/google/api/client/util/Data;->NULL_INTEGER:Ljava/lang/Integer;

    invoke-virtual {v8, v13}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 674
    invoke-virtual {v8, v12}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "arrValue":Ljava/lang/Integer;
    check-cast v1, Ljava/lang/Integer;

    .line 675
    .restart local v1    # "arrValue":Ljava/lang/Integer;
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v10

    invoke-static {v12, v10}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(II)V

    .line 677
    iget-object v4, v6, Lcom/google/api/client/testing/json/AbstractJsonParserTest$IntegerTypeVariableType;->nullValue:Ljava/lang/Object;

    check-cast v4, Ljava/lang/Integer;

    .line 678
    .local v4, "nullValue":Ljava/lang/Integer;
    sget-object v10, Lcom/google/api/client/util/Data;->NULL_INTEGER:Ljava/lang/Integer;

    invoke-static {v10, v4}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 680
    iget-object v9, v6, Lcom/google/api/client/testing/json/AbstractJsonParserTest$IntegerTypeVariableType;->value:Ljava/lang/Object;

    check-cast v9, Ljava/lang/Integer;

    .line 681
    .local v9, "value":Ljava/lang/Integer;
    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v10

    invoke-static {v12, v10}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(II)V

    .line 682
    return-void
.end method

.method public testParser_mapType()V
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 515
    invoke-virtual {p0}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->newFactory()Lcom/google/api/client/json/JsonFactory;

    move-result-object v0

    .line 517
    .local v0, "factory":Lcom/google/api/client/json/JsonFactory;
    const-string v8, "{\"value\":[{\"map1\":{\"k1\":1,\"k2\":2},\"map2\":{\"kk1\":3,\"kk2\":4}}]}"

    invoke-virtual {v0, v8}, Lcom/google/api/client/json/JsonFactory;->createJsonParser(Ljava/lang/String;)Lcom/google/api/client/json/JsonParser;

    move-result-object v5

    .line 518
    .local v5, "parser":Lcom/google/api/client/json/JsonParser;
    invoke-virtual {v5}, Lcom/google/api/client/json/JsonParser;->nextToken()Lcom/google/api/client/json/JsonToken;

    .line 519
    const-class v8, Lcom/google/api/client/testing/json/AbstractJsonParserTest$MapOfMapType;

    const/4 v9, 0x0

    invoke-virtual {v5, v8, v9}, Lcom/google/api/client/json/JsonParser;->parse(Ljava/lang/Class;Lcom/google/api/client/json/CustomizeJsonParser;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/api/client/testing/json/AbstractJsonParserTest$MapOfMapType;

    .line 521
    .local v6, "result":Lcom/google/api/client/testing/json/AbstractJsonParserTest$MapOfMapType;
    const-string v8, "{\"value\":[{\"map1\":{\"k1\":1,\"k2\":2},\"map2\":{\"kk1\":3,\"kk2\":4}}]}"

    invoke-virtual {v0, v6}, Lcom/google/api/client/json/JsonFactory;->toString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(Ljava/lang/String;Ljava/lang/String;)V

    .line 523
    iget-object v7, v6, Lcom/google/api/client/testing/json/AbstractJsonParserTest$MapOfMapType;->value:[Ljava/util/Map;

    .line 524
    .local v7, "value":[Ljava/util/Map;, "[Ljava/util/Map<Ljava/lang/String;Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;>;"
    const/4 v8, 0x0

    aget-object v1, v7, v8

    .line 525
    .local v1, "firstMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;>;"
    const-string v8, "map1"

    invoke-interface {v1, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map;

    .line 526
    .local v3, "map1":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    const-string v8, "k1"

    invoke-interface {v3, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    .line 527
    .local v2, "integer":Ljava/lang/Integer;
    const/4 v8, 0x1

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v9

    invoke-static {v8, v9}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(II)V

    .line 528
    const-string v8, "map2"

    invoke-interface {v1, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map;

    .line 529
    .local v4, "map2":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    const/4 v9, 0x3

    const-string v8, "kk1"

    invoke-interface {v4, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    invoke-static {v9, v8}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(II)V

    .line 530
    const/4 v9, 0x4

    const-string v8, "kk2"

    invoke-interface {v4, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    invoke-static {v9, v8}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(II)V

    .line 531
    return-void
.end method

.method public testParser_null()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1004
    invoke-virtual {p0}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->newFactory()Lcom/google/api/client/json/JsonFactory;

    move-result-object v0

    .line 1005
    .local v0, "factory":Lcom/google/api/client/json/JsonFactory;
    const-string v2, "null"

    invoke-virtual {v0, v2}, Lcom/google/api/client/json/JsonFactory;->createJsonParser(Ljava/lang/String;)Lcom/google/api/client/json/JsonParser;

    move-result-object v2

    const-class v3, Ljava/lang/String;

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/google/api/client/json/JsonParser;->parse(Ljava/lang/Class;Lcom/google/api/client/json/CustomizeJsonParser;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1006
    .local v1, "result":Ljava/lang/String;
    const-string v2, "null"

    invoke-virtual {v0, v1}, Lcom/google/api/client/json/JsonFactory;->toString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(Ljava/lang/String;Ljava/lang/String;)V

    .line 1008
    invoke-static {v1}, Lcom/google/api/client/util/Data;->isNull(Ljava/lang/Object;)Z

    move-result v2

    invoke-static {v2}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertTrue(Z)V

    .line 1009
    return-void
.end method

.method public testParser_nullValue()V
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 841
    invoke-virtual {p0}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->newFactory()Lcom/google/api/client/json/JsonFactory;

    move-result-object v2

    .line 843
    .local v2, "factory":Lcom/google/api/client/json/JsonFactory;
    const-string v6, "{\"arr\":[null],\"arr2\":[null,[null]],\"value\":null}"

    invoke-virtual {v2, v6}, Lcom/google/api/client/json/JsonFactory;->createJsonParser(Ljava/lang/String;)Lcom/google/api/client/json/JsonParser;

    move-result-object v3

    .line 844
    .local v3, "parser":Lcom/google/api/client/json/JsonParser;
    invoke-virtual {v3}, Lcom/google/api/client/json/JsonParser;->nextToken()Lcom/google/api/client/json/JsonToken;

    .line 845
    const-class v6, Lcom/google/api/client/testing/json/AbstractJsonParserTest$StringNullValue;

    const/4 v7, 0x0

    invoke-virtual {v3, v6, v7}, Lcom/google/api/client/json/JsonParser;->parse(Ljava/lang/Class;Lcom/google/api/client/json/CustomizeJsonParser;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/api/client/testing/json/AbstractJsonParserTest$StringNullValue;

    .line 847
    .local v4, "result":Lcom/google/api/client/testing/json/AbstractJsonParserTest$StringNullValue;
    const-string v6, "{\"arr\":[null],\"arr2\":[null,[null]],\"value\":null}"

    invoke-virtual {v2, v4}, Lcom/google/api/client/json/JsonFactory;->toString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(Ljava/lang/String;Ljava/lang/String;)V

    .line 849
    sget-object v6, Lcom/google/api/client/util/Data;->NULL_STRING:Ljava/lang/String;

    iget-object v7, v4, Lcom/google/api/client/testing/json/AbstractJsonParserTest$StringNullValue;->value:Ljava/lang/String;

    invoke-static {v6, v7}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(Ljava/lang/String;Ljava/lang/String;)V

    .line 850
    iget-object v0, v4, Lcom/google/api/client/testing/json/AbstractJsonParserTest$StringNullValue;->arr:[Ljava/lang/String;

    .line 851
    .local v0, "arr":[Ljava/lang/String;
    array-length v6, v0

    invoke-static {v9, v6}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(II)V

    .line 852
    const-class v6, Ljava/lang/String;

    invoke-static {v6}, Lcom/google/api/client/util/Data;->nullOf(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v6

    aget-object v7, v0, v8

    invoke-static {v6, v7}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 853
    iget-object v1, v4, Lcom/google/api/client/testing/json/AbstractJsonParserTest$StringNullValue;->arr2:[[Ljava/lang/String;

    .line 854
    .local v1, "arr2":[[Ljava/lang/String;
    const/4 v6, 0x2

    array-length v7, v1

    invoke-static {v6, v7}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(II)V

    .line 855
    const-class v6, [Ljava/lang/String;

    invoke-static {v6}, Lcom/google/api/client/util/Data;->nullOf(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v6

    aget-object v7, v1, v8

    invoke-static {v6, v7}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 856
    aget-object v5, v1, v9

    .line 857
    .local v5, "subArr2":[Ljava/lang/String;
    array-length v6, v5

    invoke-static {v9, v6}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(II)V

    .line 858
    sget-object v6, Lcom/google/api/client/util/Data;->NULL_STRING:Ljava/lang/String;

    aget-object v7, v5, v8

    invoke-static {v6, v7}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(Ljava/lang/String;Ljava/lang/String;)V

    .line 859
    return-void
.end method

.method public testParser_num()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 984
    invoke-virtual {p0}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->newFactory()Lcom/google/api/client/json/JsonFactory;

    move-result-object v0

    .line 985
    .local v0, "factory":Lcom/google/api/client/json/JsonFactory;
    const-string v2, "1"

    invoke-virtual {v0, v2}, Lcom/google/api/client/json/JsonFactory;->createJsonParser(Ljava/lang/String;)Lcom/google/api/client/json/JsonParser;

    move-result-object v2

    sget-object v3, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/google/api/client/json/JsonParser;->parse(Ljava/lang/Class;Lcom/google/api/client/json/CustomizeJsonParser;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 986
    .local v1, "result":I
    const-string v2, "1"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/api/client/json/JsonFactory;->toString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(Ljava/lang/String;Ljava/lang/String;)V

    .line 988
    const/4 v2, 0x1

    invoke-static {v2, v1}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(II)V

    .line 989
    return-void
.end method

.method public testParser_numberTypes()V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 371
    invoke-virtual {p0}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->newFactory()Lcom/google/api/client/json/JsonFactory;

    move-result-object v0

    .line 374
    .local v0, "factory":Lcom/google/api/client/json/JsonFactory;
    const-string v4, "{\"bigDecimalValue\":1.0,\"bigIntegerValue\":1,\"byteObjValue\":1,\"byteValue\":1,\"doubleObjValue\":1.0,\"doubleValue\":1.0,\"floatObjValue\":1.0,\"floatValue\":1.0,\"intObjValue\":1,\"intValue\":1,\"longObjValue\":1,\"longValue\":1,\"shortObjValue\":1,\"shortValue\":1,\"yetAnotherBigDecimalValue\":1}"

    invoke-virtual {v0, v4}, Lcom/google/api/client/json/JsonFactory;->createJsonParser(Ljava/lang/String;)Lcom/google/api/client/json/JsonParser;

    move-result-object v1

    .line 375
    .local v1, "parser":Lcom/google/api/client/json/JsonParser;
    invoke-virtual {v1}, Lcom/google/api/client/json/JsonParser;->nextToken()Lcom/google/api/client/json/JsonToken;

    .line 376
    const-class v4, Lcom/google/api/client/testing/json/AbstractJsonParserTest$NumberTypes;

    invoke-virtual {v1, v4, v6}, Lcom/google/api/client/json/JsonParser;->parse(Ljava/lang/Class;Lcom/google/api/client/json/CustomizeJsonParser;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/api/client/testing/json/AbstractJsonParserTest$NumberTypes;

    .line 377
    .local v2, "result":Lcom/google/api/client/testing/json/AbstractJsonParserTest$NumberTypes;
    const-string v4, "{\"bigDecimalValue\":1.0,\"bigIntegerValue\":1,\"byteObjValue\":1,\"byteValue\":1,\"doubleObjValue\":1.0,\"doubleValue\":1.0,\"floatObjValue\":1.0,\"floatValue\":1.0,\"intObjValue\":1,\"intValue\":1,\"longObjValue\":1,\"longValue\":1,\"shortObjValue\":1,\"shortValue\":1,\"yetAnotherBigDecimalValue\":1}"

    invoke-virtual {v0, v2}, Lcom/google/api/client/json/JsonFactory;->toString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(Ljava/lang/String;Ljava/lang/String;)V

    .line 379
    const-string v4, "{\"bigDecimalValue\":\"1.0\",\"bigIntegerValue\":\"1\",\"byteObjValue\":\"1\",\"byteValue\":\"1\",\"doubleObjValue\":\"1.0\",\"doubleValue\":\"1.0\",\"floatObjValue\":\"1.0\",\"floatValue\":\"1.0\",\"intObjValue\":\"1\",\"intValue\":\"1\",\"longObjValue\":\"1\",\"longValue\":\"1\",\"shortObjValue\":\"1\",\"shortValue\":\"1\",\"yetAnotherBigDecimalValue\":\"1\"}"

    invoke-virtual {v0, v4}, Lcom/google/api/client/json/JsonFactory;->createJsonParser(Ljava/lang/String;)Lcom/google/api/client/json/JsonParser;

    move-result-object v1

    .line 380
    invoke-virtual {v1}, Lcom/google/api/client/json/JsonParser;->nextToken()Lcom/google/api/client/json/JsonToken;

    .line 381
    const-class v4, Lcom/google/api/client/testing/json/AbstractJsonParserTest$NumberTypesAsString;

    invoke-virtual {v1, v4, v6}, Lcom/google/api/client/json/JsonParser;->parse(Ljava/lang/Class;Lcom/google/api/client/json/CustomizeJsonParser;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/api/client/testing/json/AbstractJsonParserTest$NumberTypesAsString;

    .line 382
    .local v3, "resultAsString":Lcom/google/api/client/testing/json/AbstractJsonParserTest$NumberTypesAsString;
    const-string v4, "{\"bigDecimalValue\":\"1.0\",\"bigIntegerValue\":\"1\",\"byteObjValue\":\"1\",\"byteValue\":\"1\",\"doubleObjValue\":\"1.0\",\"doubleValue\":\"1.0\",\"floatObjValue\":\"1.0\",\"floatValue\":\"1.0\",\"intObjValue\":\"1\",\"intValue\":\"1\",\"longObjValue\":\"1\",\"longValue\":\"1\",\"shortObjValue\":\"1\",\"shortValue\":\"1\",\"yetAnotherBigDecimalValue\":\"1\"}"

    invoke-virtual {v0, v3}, Lcom/google/api/client/json/JsonFactory;->toString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(Ljava/lang/String;Ljava/lang/String;)V

    .line 385
    :try_start_0
    const-string v4, "{\"bigDecimalValue\":\"1.0\",\"bigIntegerValue\":\"1\",\"byteObjValue\":\"1\",\"byteValue\":\"1\",\"doubleObjValue\":\"1.0\",\"doubleValue\":\"1.0\",\"floatObjValue\":\"1.0\",\"floatValue\":\"1.0\",\"intObjValue\":\"1\",\"intValue\":\"1\",\"longObjValue\":\"1\",\"longValue\":\"1\",\"shortObjValue\":\"1\",\"shortValue\":\"1\",\"yetAnotherBigDecimalValue\":\"1\"}"

    invoke-virtual {v0, v4}, Lcom/google/api/client/json/JsonFactory;->createJsonParser(Ljava/lang/String;)Lcom/google/api/client/json/JsonParser;

    move-result-object v1

    .line 386
    invoke-virtual {v1}, Lcom/google/api/client/json/JsonParser;->nextToken()Lcom/google/api/client/json/JsonToken;

    .line 387
    const-class v4, Lcom/google/api/client/testing/json/AbstractJsonParserTest$NumberTypes;

    const/4 v5, 0x0

    invoke-virtual {v1, v4, v5}, Lcom/google/api/client/json/JsonParser;->parse(Ljava/lang/Class;Lcom/google/api/client/json/CustomizeJsonParser;)Ljava/lang/Object;

    .line 388
    const-string v4, "expected IllegalArgumentException"

    invoke-static {v4}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->fail(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    .line 394
    :goto_0
    :try_start_1
    const-string v4, "{\"bigDecimalValue\":1.0,\"bigIntegerValue\":1,\"byteObjValue\":1,\"byteValue\":1,\"doubleObjValue\":1.0,\"doubleValue\":1.0,\"floatObjValue\":1.0,\"floatValue\":1.0,\"intObjValue\":1,\"intValue\":1,\"longObjValue\":1,\"longValue\":1,\"shortObjValue\":1,\"shortValue\":1,\"yetAnotherBigDecimalValue\":1}"

    invoke-virtual {v0, v4}, Lcom/google/api/client/json/JsonFactory;->createJsonParser(Ljava/lang/String;)Lcom/google/api/client/json/JsonParser;

    move-result-object v1

    .line 395
    invoke-virtual {v1}, Lcom/google/api/client/json/JsonParser;->nextToken()Lcom/google/api/client/json/JsonToken;

    .line 396
    const-class v4, Lcom/google/api/client/testing/json/AbstractJsonParserTest$NumberTypesAsString;

    const/4 v5, 0x0

    invoke-virtual {v1, v4, v5}, Lcom/google/api/client/json/JsonParser;->parse(Ljava/lang/Class;Lcom/google/api/client/json/CustomizeJsonParser;)Ljava/lang/Object;

    .line 397
    const-string v4, "expected IllegalArgumentException"

    invoke-static {v4}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->fail(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0

    .line 401
    :goto_1
    return-void

    .line 398
    :catch_0
    move-exception v4

    goto :goto_1

    .line 389
    :catch_1
    move-exception v4

    goto :goto_0
.end method

.method public testParser_partialEmpty()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 90
    invoke-virtual {p0}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->newFactory()Lcom/google/api/client/json/JsonFactory;

    move-result-object v0

    .line 92
    .local v0, "factory":Lcom/google/api/client/json/JsonFactory;
    const-string v3, "{}"

    invoke-virtual {v0, v3}, Lcom/google/api/client/json/JsonFactory;->createJsonParser(Ljava/lang/String;)Lcom/google/api/client/json/JsonParser;

    move-result-object v1

    .line 93
    .local v1, "parser":Lcom/google/api/client/json/JsonParser;
    invoke-virtual {v1}, Lcom/google/api/client/json/JsonParser;->nextToken()Lcom/google/api/client/json/JsonToken;

    .line 94
    invoke-virtual {v1}, Lcom/google/api/client/json/JsonParser;->nextToken()Lcom/google/api/client/json/JsonToken;

    .line 97
    const-class v3, Ljava/util/HashMap;

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, Lcom/google/api/client/json/JsonParser;->parseAndClose(Ljava/lang/Class;Lcom/google/api/client/json/CustomizeJsonParser;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/HashMap;

    .line 98
    .local v2, "result":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string v3, "{}"

    invoke-virtual {v0, v2}, Lcom/google/api/client/json/JsonFactory;->toString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    invoke-virtual {v2}, Ljava/util/HashMap;->isEmpty()Z

    move-result v3

    invoke-static {v3}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertTrue(Z)V

    .line 101
    return-void
.end method

.method public testParser_partialEmptyArray()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 969
    invoke-virtual {p0}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->newFactory()Lcom/google/api/client/json/JsonFactory;

    move-result-object v0

    .line 971
    .local v0, "factory":Lcom/google/api/client/json/JsonFactory;
    const-string v3, "[]"

    invoke-virtual {v0, v3}, Lcom/google/api/client/json/JsonFactory;->createJsonParser(Ljava/lang/String;)Lcom/google/api/client/json/JsonParser;

    move-result-object v1

    .line 972
    .local v1, "parser":Lcom/google/api/client/json/JsonParser;
    invoke-virtual {v1}, Lcom/google/api/client/json/JsonParser;->nextToken()Lcom/google/api/client/json/JsonToken;

    .line 973
    invoke-virtual {v1}, Lcom/google/api/client/json/JsonParser;->nextToken()Lcom/google/api/client/json/JsonToken;

    .line 975
    const-class v3, [Ljava/lang/String;

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, Lcom/google/api/client/json/JsonParser;->parse(Ljava/lang/Class;Lcom/google/api/client/json/CustomizeJsonParser;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/String;

    .line 976
    .local v2, "result":[Ljava/lang/String;
    const-string v3, "[]"

    invoke-virtual {v0, v2}, Lcom/google/api/client/json/JsonFactory;->toString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(Ljava/lang/String;Ljava/lang/String;)V

    .line 978
    const/4 v3, 0x0

    array-length v4, v2

    invoke-static {v3, v4}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(II)V

    .line 979
    return-void
.end method

.method public testParser_partialEntry()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 114
    invoke-virtual {p0}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->newFactory()Lcom/google/api/client/json/JsonFactory;

    move-result-object v0

    .line 116
    .local v0, "factory":Lcom/google/api/client/json/JsonFactory;
    const-string v3, "{\"title\":\"foo\"}"

    invoke-virtual {v0, v3}, Lcom/google/api/client/json/JsonFactory;->createJsonParser(Ljava/lang/String;)Lcom/google/api/client/json/JsonParser;

    move-result-object v1

    .line 117
    .local v1, "parser":Lcom/google/api/client/json/JsonParser;
    invoke-virtual {v1}, Lcom/google/api/client/json/JsonParser;->nextToken()Lcom/google/api/client/json/JsonToken;

    .line 118
    invoke-virtual {v1}, Lcom/google/api/client/json/JsonParser;->nextToken()Lcom/google/api/client/json/JsonToken;

    .line 120
    const-class v3, Lcom/google/api/client/testing/json/AbstractJsonParserTest$Entry;

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, Lcom/google/api/client/json/JsonParser;->parseAndClose(Ljava/lang/Class;Lcom/google/api/client/json/CustomizeJsonParser;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/api/client/testing/json/AbstractJsonParserTest$Entry;

    .line 121
    .local v2, "result":Lcom/google/api/client/testing/json/AbstractJsonParserTest$Entry;
    const-string v3, "{\"title\":\"foo\"}"

    invoke-virtual {v0, v2}, Lcom/google/api/client/json/JsonFactory;->toString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    const-string v3, "foo"

    iget-object v4, v2, Lcom/google/api/client/testing/json/AbstractJsonParserTest$Entry;->title:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    return-void
.end method

.method public testParser_string()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 994
    invoke-virtual {p0}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->newFactory()Lcom/google/api/client/json/JsonFactory;

    move-result-object v0

    .line 995
    .local v0, "factory":Lcom/google/api/client/json/JsonFactory;
    const-string v2, "\"a\""

    invoke-virtual {v0, v2}, Lcom/google/api/client/json/JsonFactory;->createJsonParser(Ljava/lang/String;)Lcom/google/api/client/json/JsonParser;

    move-result-object v2

    const-class v3, Ljava/lang/String;

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/google/api/client/json/JsonParser;->parse(Ljava/lang/Class;Lcom/google/api/client/json/CustomizeJsonParser;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 996
    .local v1, "result":Ljava/lang/String;
    const-string v2, "\"a\""

    invoke-virtual {v0, v1}, Lcom/google/api/client/json/JsonFactory;->toString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(Ljava/lang/String;Ljava/lang/String;)V

    .line 998
    const-string v2, "a"

    invoke-static {v2, v1}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(Ljava/lang/String;Ljava/lang/String;)V

    .line 999
    return-void
.end method

.method public testParser_stringArray()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 935
    invoke-virtual {p0}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->newFactory()Lcom/google/api/client/json/JsonFactory;

    move-result-object v0

    .line 937
    .local v0, "factory":Lcom/google/api/client/json/JsonFactory;
    const-string v3, "[\"a\",\"b\",\"c\"]"

    invoke-virtual {v0, v3}, Lcom/google/api/client/json/JsonFactory;->createJsonParser(Ljava/lang/String;)Lcom/google/api/client/json/JsonParser;

    move-result-object v1

    .line 938
    .local v1, "parser":Lcom/google/api/client/json/JsonParser;
    invoke-virtual {v1}, Lcom/google/api/client/json/JsonParser;->nextToken()Lcom/google/api/client/json/JsonToken;

    .line 939
    const-class v3, [Ljava/lang/String;

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, Lcom/google/api/client/json/JsonParser;->parse(Ljava/lang/Class;Lcom/google/api/client/json/CustomizeJsonParser;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/String;

    .line 940
    .local v2, "result":[Ljava/lang/String;
    const-string v3, "[\"a\",\"b\",\"c\"]"

    invoke-virtual {v0, v2}, Lcom/google/api/client/json/JsonFactory;->toString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(Ljava/lang/String;Ljava/lang/String;)V

    .line 942
    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "a"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "b"

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const-string v5, "c"

    aput-object v5, v3, v4

    invoke-static {v3, v2}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    invoke-static {v3}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertTrue(Z)V

    .line 943
    return-void
.end method

.method public testParser_treemapForTypeVariableType()V
    .locals 13
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v12, 0x2

    const/4 v11, 0x0

    const/4 v10, 0x1

    .line 803
    invoke-virtual {p0}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->newFactory()Lcom/google/api/client/json/JsonFactory;

    move-result-object v2

    .line 805
    .local v2, "factory":Lcom/google/api/client/json/JsonFactory;
    const-string v8, "{\"arr\":[null,[null,1]],\"list\":[null,[null,1]],\"nullValue\":null,\"value\":1}"

    invoke-virtual {v2, v8}, Lcom/google/api/client/json/JsonFactory;->createJsonParser(Ljava/lang/String;)Lcom/google/api/client/json/JsonParser;

    move-result-object v4

    .line 806
    .local v4, "parser":Lcom/google/api/client/json/JsonParser;
    invoke-virtual {v4}, Lcom/google/api/client/json/JsonParser;->nextToken()Lcom/google/api/client/json/JsonToken;

    .line 807
    const-class v8, Ljava/util/TreeMap;

    const/4 v9, 0x0

    invoke-virtual {v4, v8, v9}, Lcom/google/api/client/json/JsonParser;->parse(Ljava/lang/Class;Lcom/google/api/client/json/CustomizeJsonParser;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/TreeMap;

    .line 809
    .local v5, "result":Ljava/util/TreeMap;, "Ljava/util/TreeMap<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string v8, "{\"arr\":[null,[null,1]],\"list\":[null,[null,1]],\"nullValue\":null,\"value\":1}"

    invoke-virtual {v2, v5}, Lcom/google/api/client/json/JsonFactory;->toString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(Ljava/lang/String;Ljava/lang/String;)V

    .line 812
    const-string v8, "arr"

    invoke-virtual {v5, v8}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 813
    .local v0, "arr":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v8

    invoke-static {v12, v8}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(II)V

    .line 814
    const-class v8, Ljava/lang/Object;

    invoke-static {v8}, Lcom/google/api/client/util/Data;->nullOf(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v8

    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 815
    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/ArrayList;

    .line 816
    .local v6, "subArr":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/math/BigDecimal;>;"
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v8

    invoke-static {v12, v8}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(II)V

    .line 817
    const-class v8, Ljava/lang/Object;

    invoke-static {v8}, Lcom/google/api/client/util/Data;->nullOf(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v8

    invoke-virtual {v6, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 818
    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/math/BigDecimal;

    .line 819
    .local v1, "arrValue":Ljava/math/BigDecimal;
    invoke-virtual {v1}, Ljava/math/BigDecimal;->intValue()I

    move-result v8

    invoke-static {v10, v8}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(II)V

    .line 821
    const-string v8, "nullValue"

    invoke-virtual {v5, v8}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    .line 822
    .local v3, "nullValue":Ljava/lang/Object;
    const-class v8, Ljava/lang/Object;

    invoke-static {v8}, Lcom/google/api/client/util/Data;->nullOf(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v8

    invoke-static {v8, v3}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 824
    const-string v8, "value"

    invoke-virtual {v5, v8}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/math/BigDecimal;

    .line 825
    .local v7, "value":Ljava/math/BigDecimal;
    invoke-virtual {v7}, Ljava/math/BigDecimal;->intValue()I

    move-result v8

    invoke-static {v10, v8}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(II)V

    .line 826
    return-void
.end method

.method public testParser_typeVariablesPassAround()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 920
    invoke-virtual {p0}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->newFactory()Lcom/google/api/client/json/JsonFactory;

    move-result-object v1

    .line 922
    .local v1, "factory":Lcom/google/api/client/json/JsonFactory;
    const-string v4, "{\"y\":{\"z\":{\"f\":[\"abc\"]}}}"

    invoke-virtual {v1, v4}, Lcom/google/api/client/json/JsonFactory;->createJsonParser(Ljava/lang/String;)Lcom/google/api/client/json/JsonParser;

    move-result-object v2

    .line 923
    .local v2, "parser":Lcom/google/api/client/json/JsonParser;
    invoke-virtual {v2}, Lcom/google/api/client/json/JsonParser;->nextToken()Lcom/google/api/client/json/JsonToken;

    .line 924
    const-class v4, Lcom/google/api/client/testing/json/AbstractJsonParserTest$TypeVariablesPassedAround;

    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, Lcom/google/api/client/json/JsonParser;->parse(Ljava/lang/Class;Lcom/google/api/client/json/CustomizeJsonParser;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/api/client/testing/json/AbstractJsonParserTest$TypeVariablesPassedAround;

    .line 926
    .local v3, "result":Lcom/google/api/client/testing/json/AbstractJsonParserTest$TypeVariablesPassedAround;
    const-string v4, "{\"y\":{\"z\":{\"f\":[\"abc\"]}}}"

    invoke-virtual {v1, v3}, Lcom/google/api/client/json/JsonFactory;->toString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(Ljava/lang/String;Ljava/lang/String;)V

    .line 928
    iget-object v4, v3, Lcom/google/api/client/testing/json/AbstractJsonParserTest$TypeVariablesPassedAround;->y:Lcom/google/api/client/testing/json/AbstractJsonParserTest$Y;

    iget-object v4, v4, Lcom/google/api/client/testing/json/AbstractJsonParserTest$Y;->z:Lcom/google/api/client/testing/json/AbstractJsonParserTest$Z;

    iget-object v0, v4, Lcom/google/api/client/testing/json/AbstractJsonParserTest$Z;->f:Ljava/lang/Object;

    check-cast v0, Ljava/util/LinkedList;

    .line 929
    .local v0, "f":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Ljava/lang/String;>;"
    const-string v5, "abc"

    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-static {v5, v4}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(Ljava/lang/String;Ljava/lang/String;)V

    .line 930
    return-void
.end method

.method public testParser_wildCardType()V
    .locals 23
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 574
    invoke-virtual/range {p0 .. p0}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->newFactory()Lcom/google/api/client/json/JsonFactory;

    move-result-object v2

    .line 576
    .local v2, "factory":Lcom/google/api/client/json/JsonFactory;
    const-string v21, "{\"lower\":[[1,2,3]],\"map\":{\"v\":1},\"mapInWild\":[{\"v\":1}],\"mapUpper\":{\"v\":1},\"simple\":[[1,2,3]],\"upper\":[[1,2,3]]}"

    move-object/from16 v0, v21

    invoke-virtual {v2, v0}, Lcom/google/api/client/json/JsonFactory;->createJsonParser(Ljava/lang/String;)Lcom/google/api/client/json/JsonParser;

    move-result-object v11

    .line 577
    .local v11, "parser":Lcom/google/api/client/json/JsonParser;
    invoke-virtual {v11}, Lcom/google/api/client/json/JsonParser;->nextToken()Lcom/google/api/client/json/JsonToken;

    .line 578
    const-class v21, Lcom/google/api/client/testing/json/AbstractJsonParserTest$WildCardTypes;

    const/16 v22, 0x0

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-virtual {v11, v0, v1}, Lcom/google/api/client/json/JsonParser;->parse(Ljava/lang/Class;Lcom/google/api/client/json/CustomizeJsonParser;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/google/api/client/testing/json/AbstractJsonParserTest$WildCardTypes;

    .line 580
    .local v12, "result":Lcom/google/api/client/testing/json/AbstractJsonParserTest$WildCardTypes;
    const-string v21, "{\"lower\":[[1,2,3]],\"map\":{\"v\":1},\"mapInWild\":[{\"v\":1}],\"mapUpper\":{\"v\":1},\"simple\":[[1,2,3]],\"upper\":[[1,2,3]]}"

    invoke-virtual {v2, v12}, Lcom/google/api/client/json/JsonFactory;->toString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(Ljava/lang/String;Ljava/lang/String;)V

    .line 582
    iget-object v13, v12, Lcom/google/api/client/testing/json/AbstractJsonParserTest$WildCardTypes;->simple:[Ljava/util/Collection;

    check-cast v13, [Ljava/util/Collection;

    .line 583
    .local v13, "simple":[Ljava/util/Collection;, "[Ljava/util/Collection<Ljava/math/BigDecimal;>;"
    const/16 v21, 0x0

    aget-object v15, v13, v21

    check-cast v15, Ljava/util/ArrayList;

    .line 584
    .local v15, "wildcard":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/math/BigDecimal;>;"
    const/16 v21, 0x0

    move/from16 v0, v21

    invoke-virtual {v15, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Ljava/math/BigDecimal;

    .line 585
    .local v16, "wildcardFirstValue":Ljava/math/BigDecimal;
    const/16 v21, 0x1

    invoke-virtual/range {v16 .. v16}, Ljava/math/BigDecimal;->intValue()I

    move-result v22

    invoke-static/range {v21 .. v22}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(II)V

    .line 586
    iget-object v14, v12, Lcom/google/api/client/testing/json/AbstractJsonParserTest$WildCardTypes;->upper:[Ljava/util/Collection;

    check-cast v14, [Ljava/util/Collection;

    .line 587
    .local v14, "upper":[Ljava/util/Collection;, "[Ljava/util/Collection<Ljava/lang/Integer;>;"
    const/16 v21, 0x0

    aget-object v20, v14, v21

    check-cast v20, Ljava/util/ArrayList;

    .line 588
    .local v20, "wildcardUpper":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const/16 v21, 0x0

    invoke-virtual/range {v20 .. v21}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/lang/Integer;

    .line 589
    .local v18, "wildcardFirstValueUpper":Ljava/lang/Integer;
    const/16 v21, 0x1

    invoke-virtual/range {v18 .. v18}, Ljava/lang/Integer;->intValue()I

    move-result v22

    invoke-static/range {v21 .. v22}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(II)V

    .line 590
    iget-object v3, v12, Lcom/google/api/client/testing/json/AbstractJsonParserTest$WildCardTypes;->lower:[Ljava/util/Collection;

    check-cast v3, [Ljava/util/Collection;

    .line 591
    .local v3, "lower":[Ljava/util/Collection;, "[Ljava/util/Collection<Ljava/lang/Integer;>;"
    const/16 v21, 0x0

    aget-object v19, v3, v21

    check-cast v19, Ljava/util/ArrayList;

    .line 592
    .local v19, "wildcardLower":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const/16 v21, 0x0

    move-object/from16 v0, v19

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/lang/Integer;

    .line 593
    .local v17, "wildcardFirstValueLower":Ljava/lang/Integer;
    const/16 v21, 0x1

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Integer;->intValue()I

    move-result v22

    invoke-static/range {v21 .. v22}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(II)V

    .line 594
    iget-object v4, v12, Lcom/google/api/client/testing/json/AbstractJsonParserTest$WildCardTypes;->map:Ljava/util/Map;

    .line 595
    .local v4, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/math/BigDecimal;>;"
    const-string v21, "v"

    move-object/from16 v0, v21

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/math/BigDecimal;

    .line 596
    .local v10, "mapValue":Ljava/math/BigDecimal;
    const/16 v21, 0x1

    invoke-virtual {v10}, Ljava/math/BigDecimal;->intValue()I

    move-result v22

    invoke-static/range {v21 .. v22}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(II)V

    .line 597
    iget-object v8, v12, Lcom/google/api/client/testing/json/AbstractJsonParserTest$WildCardTypes;->mapUpper:Ljava/util/Map;

    .line 598
    .local v8, "mapUpper":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    const-string v21, "v"

    move-object/from16 v0, v21

    invoke-interface {v8, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Integer;

    .line 599
    .local v9, "mapUpperValue":Ljava/lang/Integer;
    const/16 v21, 0x1

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v22

    invoke-static/range {v21 .. v22}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(II)V

    .line 600
    iget-object v5, v12, Lcom/google/api/client/testing/json/AbstractJsonParserTest$WildCardTypes;->mapInWild:Ljava/util/Collection;

    check-cast v5, Ljava/util/ArrayList;

    .line 602
    .local v5, "mapInWild":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/util/TreeMap<Ljava/lang/String;+Ljava/lang/Integer;>;>;"
    const/16 v21, 0x0

    move/from16 v0, v21

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/TreeMap;

    .line 603
    .local v6, "mapInWildFirst":Ljava/util/TreeMap;, "Ljava/util/TreeMap<Ljava/lang/String;Ljava/lang/Integer;>;"
    const-string v21, "v"

    move-object/from16 v0, v21

    invoke-virtual {v6, v0}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    .line 604
    .local v7, "mapInWildFirstValue":Ljava/lang/Integer;
    const/16 v21, 0x1

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v22

    invoke-static/range {v21 .. v22}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(II)V

    .line 605
    return-void
.end method

.method public testSkipChildren_array()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 193
    invoke-virtual {p0}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->newFactory()Lcom/google/api/client/json/JsonFactory;

    move-result-object v1

    const-string v2, "{\"entries\":[{\"title\":\"foo\"},{\"title\":\"bar\"}]}"

    invoke-virtual {v1, v2}, Lcom/google/api/client/json/JsonFactory;->createJsonParser(Ljava/lang/String;)Lcom/google/api/client/json/JsonParser;

    move-result-object v0

    .line 194
    .local v0, "parser":Lcom/google/api/client/json/JsonParser;
    invoke-virtual {v0}, Lcom/google/api/client/json/JsonParser;->nextToken()Lcom/google/api/client/json/JsonToken;

    .line 195
    const-string v1, "entries"

    invoke-virtual {v0, v1}, Lcom/google/api/client/json/JsonParser;->skipToKey(Ljava/lang/String;)V

    .line 196
    invoke-virtual {v0}, Lcom/google/api/client/json/JsonParser;->skipChildren()Lcom/google/api/client/json/JsonParser;

    .line 197
    sget-object v1, Lcom/google/api/client/json/JsonToken;->END_ARRAY:Lcom/google/api/client/json/JsonToken;

    invoke-virtual {v0}, Lcom/google/api/client/json/JsonParser;->getCurrentToken()Lcom/google/api/client/json/JsonToken;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 198
    return-void
.end method

.method public testSkipChildren_object()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 186
    invoke-virtual {p0}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->newFactory()Lcom/google/api/client/json/JsonFactory;

    move-result-object v1

    const-string v2, "{\"title\":\"foo\"}"

    invoke-virtual {v1, v2}, Lcom/google/api/client/json/JsonFactory;->createJsonParser(Ljava/lang/String;)Lcom/google/api/client/json/JsonParser;

    move-result-object v0

    .line 187
    .local v0, "parser":Lcom/google/api/client/json/JsonParser;
    invoke-virtual {v0}, Lcom/google/api/client/json/JsonParser;->nextToken()Lcom/google/api/client/json/JsonToken;

    .line 188
    invoke-virtual {v0}, Lcom/google/api/client/json/JsonParser;->skipChildren()Lcom/google/api/client/json/JsonParser;

    .line 189
    sget-object v1, Lcom/google/api/client/json/JsonToken;->END_OBJECT:Lcom/google/api/client/json/JsonToken;

    invoke-virtual {v0}, Lcom/google/api/client/json/JsonParser;->getCurrentToken()Lcom/google/api/client/json/JsonToken;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 190
    return-void
.end method

.method public testSkipChildren_string()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 177
    invoke-virtual {p0}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->newFactory()Lcom/google/api/client/json/JsonFactory;

    move-result-object v1

    const-string v2, "{\"title\":\"foo\"}"

    invoke-virtual {v1, v2}, Lcom/google/api/client/json/JsonFactory;->createJsonParser(Ljava/lang/String;)Lcom/google/api/client/json/JsonParser;

    move-result-object v0

    .line 178
    .local v0, "parser":Lcom/google/api/client/json/JsonParser;
    invoke-virtual {v0}, Lcom/google/api/client/json/JsonParser;->nextToken()Lcom/google/api/client/json/JsonToken;

    .line 179
    const-string v1, "title"

    invoke-virtual {v0, v1}, Lcom/google/api/client/json/JsonParser;->skipToKey(Ljava/lang/String;)V

    .line 180
    invoke-virtual {v0}, Lcom/google/api/client/json/JsonParser;->skipChildren()Lcom/google/api/client/json/JsonParser;

    .line 181
    sget-object v1, Lcom/google/api/client/json/JsonToken;->VALUE_STRING:Lcom/google/api/client/json/JsonToken;

    invoke-virtual {v0}, Lcom/google/api/client/json/JsonParser;->getCurrentToken()Lcom/google/api/client/json/JsonToken;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 182
    const-string v1, "foo"

    invoke-virtual {v0}, Lcom/google/api/client/json/JsonParser;->getText()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(Ljava/lang/String;Ljava/lang/String;)V

    .line 183
    return-void
.end method

.method public testSkipToKey_found()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 160
    invoke-virtual {p0}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->newFactory()Lcom/google/api/client/json/JsonFactory;

    move-result-object v1

    const-string v2, "{\"title\":\"foo\"}"

    invoke-virtual {v1, v2}, Lcom/google/api/client/json/JsonFactory;->createJsonParser(Ljava/lang/String;)Lcom/google/api/client/json/JsonParser;

    move-result-object v0

    .line 161
    .local v0, "parser":Lcom/google/api/client/json/JsonParser;
    invoke-virtual {v0}, Lcom/google/api/client/json/JsonParser;->nextToken()Lcom/google/api/client/json/JsonToken;

    .line 162
    const-string v1, "title"

    invoke-virtual {v0, v1}, Lcom/google/api/client/json/JsonParser;->skipToKey(Ljava/lang/String;)V

    .line 163
    sget-object v1, Lcom/google/api/client/json/JsonToken;->VALUE_STRING:Lcom/google/api/client/json/JsonToken;

    invoke-virtual {v0}, Lcom/google/api/client/json/JsonParser;->getCurrentToken()Lcom/google/api/client/json/JsonToken;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 164
    const-string v1, "foo"

    invoke-virtual {v0}, Lcom/google/api/client/json/JsonParser;->getText()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(Ljava/lang/String;Ljava/lang/String;)V

    .line 165
    return-void
.end method

.method public testSkipToKey_missing()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 153
    invoke-virtual {p0}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->newFactory()Lcom/google/api/client/json/JsonFactory;

    move-result-object v1

    const-string v2, "{\"title\":\"foo\"}"

    invoke-virtual {v1, v2}, Lcom/google/api/client/json/JsonFactory;->createJsonParser(Ljava/lang/String;)Lcom/google/api/client/json/JsonParser;

    move-result-object v0

    .line 154
    .local v0, "parser":Lcom/google/api/client/json/JsonParser;
    invoke-virtual {v0}, Lcom/google/api/client/json/JsonParser;->nextToken()Lcom/google/api/client/json/JsonToken;

    .line 155
    const-string v1, "missing"

    invoke-virtual {v0, v1}, Lcom/google/api/client/json/JsonParser;->skipToKey(Ljava/lang/String;)V

    .line 156
    sget-object v1, Lcom/google/api/client/json/JsonToken;->END_OBJECT:Lcom/google/api/client/json/JsonToken;

    invoke-virtual {v0}, Lcom/google/api/client/json/JsonParser;->getCurrentToken()Lcom/google/api/client/json/JsonToken;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 157
    return-void
.end method

.method public testSkipToKey_missingEmpty()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 146
    invoke-virtual {p0}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->newFactory()Lcom/google/api/client/json/JsonFactory;

    move-result-object v1

    const-string v2, "{}"

    invoke-virtual {v1, v2}, Lcom/google/api/client/json/JsonFactory;->createJsonParser(Ljava/lang/String;)Lcom/google/api/client/json/JsonParser;

    move-result-object v0

    .line 147
    .local v0, "parser":Lcom/google/api/client/json/JsonParser;
    invoke-virtual {v0}, Lcom/google/api/client/json/JsonParser;->nextToken()Lcom/google/api/client/json/JsonToken;

    .line 148
    const-string v1, "missing"

    invoke-virtual {v0, v1}, Lcom/google/api/client/json/JsonParser;->skipToKey(Ljava/lang/String;)V

    .line 149
    sget-object v1, Lcom/google/api/client/json/JsonToken;->END_OBJECT:Lcom/google/api/client/json/JsonToken;

    invoke-virtual {v0}, Lcom/google/api/client/json/JsonParser;->getCurrentToken()Lcom/google/api/client/json/JsonToken;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 150
    return-void
.end method

.method public testSkipToKey_startWithFieldName()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 168
    invoke-virtual {p0}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->newFactory()Lcom/google/api/client/json/JsonFactory;

    move-result-object v1

    const-string v2, "{\"title\":\"foo\"}"

    invoke-virtual {v1, v2}, Lcom/google/api/client/json/JsonFactory;->createJsonParser(Ljava/lang/String;)Lcom/google/api/client/json/JsonParser;

    move-result-object v0

    .line 169
    .local v0, "parser":Lcom/google/api/client/json/JsonParser;
    invoke-virtual {v0}, Lcom/google/api/client/json/JsonParser;->nextToken()Lcom/google/api/client/json/JsonToken;

    .line 170
    invoke-virtual {v0}, Lcom/google/api/client/json/JsonParser;->nextToken()Lcom/google/api/client/json/JsonToken;

    .line 171
    const-string v1, "title"

    invoke-virtual {v0, v1}, Lcom/google/api/client/json/JsonParser;->skipToKey(Ljava/lang/String;)V

    .line 172
    sget-object v1, Lcom/google/api/client/json/JsonToken;->VALUE_STRING:Lcom/google/api/client/json/JsonToken;

    invoke-virtual {v0}, Lcom/google/api/client/json/JsonParser;->getCurrentToken()Lcom/google/api/client/json/JsonToken;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 173
    const-string v1, "foo"

    invoke-virtual {v0}, Lcom/google/api/client/json/JsonParser;->getText()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(Ljava/lang/String;Ljava/lang/String;)V

    .line 174
    return-void
.end method

.method public testToFromString()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 404
    invoke-virtual {p0}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->newFactory()Lcom/google/api/client/json/JsonFactory;

    move-result-object v0

    .line 405
    .local v0, "factory":Lcom/google/api/client/json/JsonFactory;
    const-string v2, "{\"bigDecimalValue\":1.0,\"bigIntegerValue\":1,\"byteObjValue\":1,\"byteValue\":1,\"doubleObjValue\":1.0,\"doubleValue\":1.0,\"floatObjValue\":1.0,\"floatValue\":1.0,\"intObjValue\":1,\"intValue\":1,\"longObjValue\":1,\"longValue\":1,\"shortObjValue\":1,\"shortValue\":1,\"yetAnotherBigDecimalValue\":1}"

    const-class v3, Lcom/google/api/client/testing/json/AbstractJsonParserTest$NumberTypes;

    invoke-virtual {v0, v2, v3}, Lcom/google/api/client/json/JsonFactory;->fromString(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/api/client/testing/json/AbstractJsonParserTest$NumberTypes;

    .line 406
    .local v1, "result":Lcom/google/api/client/testing/json/AbstractJsonParserTest$NumberTypes;
    const-string v2, "{\"bigDecimalValue\":1.0,\"bigIntegerValue\":1,\"byteObjValue\":1,\"byteValue\":1,\"doubleObjValue\":1.0,\"doubleValue\":1.0,\"floatObjValue\":1.0,\"floatValue\":1.0,\"intObjValue\":1,\"intValue\":1,\"longObjValue\":1,\"longValue\":1,\"shortObjValue\":1,\"shortValue\":1,\"yetAnotherBigDecimalValue\":1}"

    invoke-virtual {v0, v1}, Lcom/google/api/client/json/JsonFactory;->toString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(Ljava/lang/String;Ljava/lang/String;)V

    .line 407
    return-void
.end method

.method public testToFromString_UTF8()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 413
    invoke-virtual {p0}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->newFactory()Lcom/google/api/client/json/JsonFactory;

    move-result-object v0

    .line 414
    .local v0, "factory":Lcom/google/api/client/json/JsonFactory;
    const-string v2, "{\"value\":\"123\u05d9\u05e0\u05d9\u05d1\"}"

    const-class v3, Lcom/google/api/client/json/GenericJson;

    invoke-virtual {v0, v2, v3}, Lcom/google/api/client/json/JsonFactory;->fromString(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/api/client/json/GenericJson;

    .line 415
    .local v1, "result":Lcom/google/api/client/json/GenericJson;
    const-string v2, "123\u05d9\u05e0\u05d9\u05d1"

    const-string v3, "value"

    invoke-virtual {v1, v3}, Lcom/google/api/client/json/GenericJson;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 416
    const-string v2, "{\"value\":\"123\u05d9\u05e0\u05d9\u05d1\"}"

    invoke-virtual {v0, v1}, Lcom/google/api/client/json/JsonFactory;->toString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/api/client/testing/json/AbstractJsonParserTest;->assertEquals(Ljava/lang/String;Ljava/lang/String;)V

    .line 417
    return-void
.end method
