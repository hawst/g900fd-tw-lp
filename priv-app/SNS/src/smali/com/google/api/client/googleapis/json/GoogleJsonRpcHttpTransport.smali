.class public final Lcom/google/api/client/googleapis/json/GoogleJsonRpcHttpTransport;
.super Ljava/lang/Object;
.source "GoogleJsonRpcHttpTransport.java"


# instance fields
.field public accept:Ljava/lang/String;

.field public contentType:Ljava/lang/String;

.field public jsonFactory:Lcom/google/api/client/json/JsonFactory;

.field public rpcServerUrl:Lcom/google/api/client/http/GenericUrl;

.field public transport:Lcom/google/api/client/http/HttpTransport;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    const-string v0, "application/json-rpc"

    iput-object v0, p0, Lcom/google/api/client/googleapis/json/GoogleJsonRpcHttpTransport;->contentType:Ljava/lang/String;

    .line 63
    iget-object v0, p0, Lcom/google/api/client/googleapis/json/GoogleJsonRpcHttpTransport;->contentType:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/api/client/googleapis/json/GoogleJsonRpcHttpTransport;->accept:Ljava/lang/String;

    return-void
.end method

.method private internalExecute(Ljava/lang/Object;)Lcom/google/api/client/http/HttpRequest;
    .locals 5
    .param p1, "data"    # Ljava/lang/Object;

    .prologue
    .line 103
    new-instance v0, Lcom/google/api/client/http/json/JsonHttpContent;

    iget-object v3, p0, Lcom/google/api/client/googleapis/json/GoogleJsonRpcHttpTransport;->jsonFactory:Lcom/google/api/client/json/JsonFactory;

    invoke-direct {v0, v3, p1}, Lcom/google/api/client/http/json/JsonHttpContent;-><init>(Lcom/google/api/client/json/JsonFactory;Ljava/lang/Object;)V

    .line 104
    .local v0, "content":Lcom/google/api/client/http/json/JsonHttpContent;
    iget-object v3, p0, Lcom/google/api/client/googleapis/json/GoogleJsonRpcHttpTransport;->contentType:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/google/api/client/http/json/JsonHttpContent;->setType(Ljava/lang/String;)Lcom/google/api/client/http/json/JsonHttpContent;

    .line 107
    :try_start_0
    iget-object v3, p0, Lcom/google/api/client/googleapis/json/GoogleJsonRpcHttpTransport;->transport:Lcom/google/api/client/http/HttpTransport;

    invoke-virtual {v3}, Lcom/google/api/client/http/HttpTransport;->createRequestFactory()Lcom/google/api/client/http/HttpRequestFactory;

    move-result-object v3

    iget-object v4, p0, Lcom/google/api/client/googleapis/json/GoogleJsonRpcHttpTransport;->rpcServerUrl:Lcom/google/api/client/http/GenericUrl;

    invoke-virtual {v3, v4, v0}, Lcom/google/api/client/http/HttpRequestFactory;->buildPostRequest(Lcom/google/api/client/http/GenericUrl;Lcom/google/api/client/http/HttpContent;)Lcom/google/api/client/http/HttpRequest;

    move-result-object v2

    .line 108
    .local v2, "httpRequest":Lcom/google/api/client/http/HttpRequest;
    invoke-virtual {v2}, Lcom/google/api/client/http/HttpRequest;->getHeaders()Lcom/google/api/client/http/HttpHeaders;

    move-result-object v3

    iget-object v4, p0, Lcom/google/api/client/googleapis/json/GoogleJsonRpcHttpTransport;->accept:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/google/api/client/http/HttpHeaders;->setAccept(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 109
    return-object v2

    .line 110
    .end local v2    # "httpRequest":Lcom/google/api/client/http/HttpRequest;
    :catch_0
    move-exception v1

    .line 111
    .local v1, "e":Ljava/io/IOException;
    new-instance v3, Ljava/lang/IllegalStateException;

    invoke-direct {v3, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v3
.end method


# virtual methods
.method public buildPostRequest(Lcom/google/api/client/json/rpc2/JsonRpcRequest;)Lcom/google/api/client/http/HttpRequest;
    .locals 1
    .param p1, "request"    # Lcom/google/api/client/json/rpc2/JsonRpcRequest;

    .prologue
    .line 80
    invoke-direct {p0, p1}, Lcom/google/api/client/googleapis/json/GoogleJsonRpcHttpTransport;->internalExecute(Ljava/lang/Object;)Lcom/google/api/client/http/HttpRequest;

    move-result-object v0

    return-object v0
.end method

.method public buildPostRequest(Ljava/util/List;)Lcom/google/api/client/http/HttpRequest;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/client/json/rpc2/JsonRpcRequest;",
            ">;)",
            "Lcom/google/api/client/http/HttpRequest;"
        }
    .end annotation

    .prologue
    .line 99
    .local p1, "requests":Ljava/util/List;, "Ljava/util/List<Lcom/google/api/client/json/rpc2/JsonRpcRequest;>;"
    invoke-direct {p0, p1}, Lcom/google/api/client/googleapis/json/GoogleJsonRpcHttpTransport;->internalExecute(Ljava/lang/Object;)Lcom/google/api/client/http/HttpRequest;

    move-result-object v0

    return-object v0
.end method
