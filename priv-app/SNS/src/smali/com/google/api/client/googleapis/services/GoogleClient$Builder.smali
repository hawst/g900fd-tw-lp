.class public Lcom/google/api/client/googleapis/services/GoogleClient$Builder;
.super Lcom/google/api/client/http/json/JsonHttpClient$Builder;
.source "GoogleClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/api/client/googleapis/services/GoogleClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# direct methods
.method protected constructor <init>(Lcom/google/api/client/http/HttpTransport;Lcom/google/api/client/json/JsonFactory;Lcom/google/api/client/http/GenericUrl;)V
    .locals 0
    .param p1, "transport"    # Lcom/google/api/client/http/HttpTransport;
    .param p2, "jsonFactory"    # Lcom/google/api/client/json/JsonFactory;
    .param p3, "baseUrl"    # Lcom/google/api/client/http/GenericUrl;

    .prologue
    .line 146
    invoke-direct {p0, p1, p2, p3}, Lcom/google/api/client/http/json/JsonHttpClient$Builder;-><init>(Lcom/google/api/client/http/HttpTransport;Lcom/google/api/client/json/JsonFactory;Lcom/google/api/client/http/GenericUrl;)V

    .line 147
    return-void
.end method


# virtual methods
.method public build()Lcom/google/api/client/googleapis/services/GoogleClient;
    .locals 7

    .prologue
    .line 152
    new-instance v0, Lcom/google/api/client/googleapis/services/GoogleClient;

    invoke-virtual {p0}, Lcom/google/api/client/googleapis/services/GoogleClient$Builder;->getTransport()Lcom/google/api/client/http/HttpTransport;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/api/client/googleapis/services/GoogleClient$Builder;->getJsonHttpRequestInitializer()Lcom/google/api/client/http/json/JsonHttpRequestInitializer;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/api/client/googleapis/services/GoogleClient$Builder;->getHttpRequestInitializer()Lcom/google/api/client/http/HttpRequestInitializer;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/api/client/googleapis/services/GoogleClient$Builder;->getJsonFactory()Lcom/google/api/client/json/JsonFactory;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/api/client/googleapis/services/GoogleClient$Builder;->getBaseUrl()Lcom/google/api/client/http/GenericUrl;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/api/client/http/GenericUrl;->build()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0}, Lcom/google/api/client/googleapis/services/GoogleClient$Builder;->getApplicationName()Ljava/lang/String;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, Lcom/google/api/client/googleapis/services/GoogleClient;-><init>(Lcom/google/api/client/http/HttpTransport;Lcom/google/api/client/http/json/JsonHttpRequestInitializer;Lcom/google/api/client/http/HttpRequestInitializer;Lcom/google/api/client/json/JsonFactory;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/google/api/client/http/json/JsonHttpClient;
    .locals 1

    .prologue
    .line 136
    invoke-virtual {p0}, Lcom/google/api/client/googleapis/services/GoogleClient$Builder;->build()Lcom/google/api/client/googleapis/services/GoogleClient;

    move-result-object v0

    return-object v0
.end method
