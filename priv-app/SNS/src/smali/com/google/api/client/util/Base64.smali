.class public Lcom/google/api/client/util/Base64;
.super Ljava/lang/Object;
.source "Base64.java"


# static fields
.field private static final ALPHABET:[B

.field private static final DECODABET:[B

.field private static final EQUALS_SIGN:B = 0x3dt

.field private static final EQUALS_SIGN_ENC:B = -0x1t

.field private static final WHITE_SPACE_ENC:B = -0x5t


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 155
    const/16 v0, 0x40

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/api/client/util/Base64;->ALPHABET:[B

    .line 225
    const/16 v0, 0x100

    new-array v0, v0, [B

    fill-array-data v0, :array_1

    sput-object v0, Lcom/google/api/client/util/Base64;->DECODABET:[B

    return-void

    .line 155
    nop

    :array_0
    .array-data 1
        0x41t
        0x42t
        0x43t
        0x44t
        0x45t
        0x46t
        0x47t
        0x48t
        0x49t
        0x4at
        0x4bt
        0x4ct
        0x4dt
        0x4et
        0x4ft
        0x50t
        0x51t
        0x52t
        0x53t
        0x54t
        0x55t
        0x56t
        0x57t
        0x58t
        0x59t
        0x5at
        0x61t
        0x62t
        0x63t
        0x64t
        0x65t
        0x66t
        0x67t
        0x68t
        0x69t
        0x6at
        0x6bt
        0x6ct
        0x6dt
        0x6et
        0x6ft
        0x70t
        0x71t
        0x72t
        0x73t
        0x74t
        0x75t
        0x76t
        0x77t
        0x78t
        0x79t
        0x7at
        0x30t
        0x31t
        0x32t
        0x33t
        0x34t
        0x35t
        0x36t
        0x37t
        0x38t
        0x39t
        0x2bt
        0x2ft
    .end array-data

    .line 225
    :array_1
    .array-data 1
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x5t
        -0x5t
        -0x9t
        -0x9t
        -0x5t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x5t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        0x3et
        -0x9t
        -0x9t
        -0x9t
        0x3ft
        0x34t
        0x35t
        0x36t
        0x37t
        0x38t
        0x39t
        0x3at
        0x3bt
        0x3ct
        0x3dt
        -0x9t
        -0x9t
        -0x9t
        -0x1t
        -0x9t
        -0x9t
        -0x9t
        0x0t
        0x1t
        0x2t
        0x3t
        0x4t
        0x5t
        0x6t
        0x7t
        0x8t
        0x9t
        0xat
        0xbt
        0xct
        0xdt
        0xet
        0xft
        0x10t
        0x11t
        0x12t
        0x13t
        0x14t
        0x15t
        0x16t
        0x17t
        0x18t
        0x19t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        0x1at
        0x1bt
        0x1ct
        0x1dt
        0x1et
        0x1ft
        0x20t
        0x21t
        0x22t
        0x23t
        0x24t
        0x25t
        0x26t
        0x27t
        0x28t
        0x29t
        0x2at
        0x2bt
        0x2ct
        0x2dt
        0x2et
        0x2ft
        0x30t
        0x31t
        0x32t
        0x33t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
    .end array-data
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 478
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 479
    return-void
.end method

.method public static decode([B)[B
    .locals 2
    .param p0, "source"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 764
    const/4 v0, 0x0

    array-length v1, p0

    invoke-static {p0, v0, v1}, Lcom/google/api/client/util/Base64;->decode([BII)[B

    move-result-object v0

    return-object v0
.end method

.method public static decode([BII)[B
    .locals 15
    .param p0, "source"    # [B
    .param p1, "off"    # I
    .param p2, "len"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 787
    if-nez p0, :cond_0

    .line 788
    new-instance v10, Ljava/lang/NullPointerException;

    const-string v11, "Cannot decode null source array."

    invoke-direct {v10, v11}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v10

    .line 790
    :cond_0
    if-ltz p1, :cond_1

    add-int v10, p1, p2

    array-length v11, p0

    if-le v10, v11, :cond_2

    .line 791
    :cond_1
    new-instance v10, Ljava/lang/IllegalArgumentException;

    const-string v11, "Source array with length %d cannot have offset of %d and process %d bytes."

    const/4 v12, 0x3

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    array-length v14, p0

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    aput-object v14, v12, v13

    const/4 v13, 0x1

    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    aput-object v14, v12, v13

    const/4 v13, 0x2

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    aput-object v14, v12, v13

    invoke-static {v11, v12}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v10

    .line 796
    :cond_2
    if-nez p2, :cond_3

    .line 797
    const/4 v10, 0x0

    new-array v6, v10, [B

    .line 844
    :goto_0
    return-object v6

    .line 798
    :cond_3
    const/4 v10, 0x4

    move/from16 v0, p2

    if-ge v0, v10, :cond_4

    .line 799
    new-instance v10, Ljava/lang/IllegalArgumentException;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Base64-encoded string must have at least four characters, but length specified was "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move/from16 v0, p2

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v10

    .line 804
    :cond_4
    mul-int/lit8 v10, p2, 0x3

    div-int/lit8 v5, v10, 0x4

    .line 805
    .local v5, "len34":I
    new-array v7, v5, [B

    .line 806
    .local v7, "outBuff":[B
    const/4 v8, 0x0

    .line 808
    .local v8, "outBuffPosn":I
    const/4 v10, 0x4

    new-array v1, v10, [B

    .line 809
    .local v1, "b4":[B
    const/4 v2, 0x0

    .line 810
    .local v2, "b4Posn":I
    const/4 v4, 0x0

    .line 811
    .local v4, "i":I
    const/4 v9, 0x0

    .line 813
    .local v9, "sbiDecode":B
    move/from16 v4, p1

    move v3, v2

    .end local v2    # "b4Posn":I
    .local v3, "b4Posn":I
    :goto_1
    add-int v10, p1, p2

    if-ge v4, v10, :cond_8

    .line 815
    sget-object v10, Lcom/google/api/client/util/Base64;->DECODABET:[B

    aget-byte v11, p0, v4

    and-int/lit16 v11, v11, 0xff

    aget-byte v9, v10, v11

    .line 820
    const/4 v10, -0x5

    if-lt v9, v10, :cond_5

    .line 821
    const/4 v10, -0x1

    if-lt v9, v10, :cond_6

    .line 822
    add-int/lit8 v2, v3, 0x1

    .end local v3    # "b4Posn":I
    .restart local v2    # "b4Posn":I
    aget-byte v10, p0, v4

    aput-byte v10, v1, v3

    .line 823
    const/4 v10, 0x3

    if-le v2, v10, :cond_7

    .line 824
    const/4 v10, 0x0

    invoke-static {v1, v10, v7, v8}, Lcom/google/api/client/util/Base64;->decode4to3([BI[BI)I

    move-result v10

    add-int/2addr v8, v10

    .line 825
    const/4 v2, 0x0

    .line 828
    aget-byte v10, p0, v4

    const/16 v11, 0x3d

    if-ne v10, v11, :cond_7

    .line 842
    :goto_2
    new-array v6, v8, [B

    .line 843
    .local v6, "out":[B
    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-static {v7, v10, v6, v11, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0

    .line 836
    .end local v2    # "b4Posn":I
    .end local v6    # "out":[B
    .restart local v3    # "b4Posn":I
    :cond_5
    new-instance v10, Ljava/io/IOException;

    const-string v11, "Bad Base64 input character decimal %d in array position %d"

    const/4 v12, 0x2

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    aget-byte v14, p0, v4

    and-int/lit16 v14, v14, 0xff

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    aput-object v14, v12, v13

    const/4 v13, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    aput-object v14, v12, v13

    invoke-static {v11, v12}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v10

    :cond_6
    move v2, v3

    .line 813
    .end local v3    # "b4Posn":I
    .restart local v2    # "b4Posn":I
    :cond_7
    add-int/lit8 v4, v4, 0x1

    move v3, v2

    .end local v2    # "b4Posn":I
    .restart local v3    # "b4Posn":I
    goto :goto_1

    :cond_8
    move v2, v3

    .end local v3    # "b4Posn":I
    .restart local v2    # "b4Posn":I
    goto :goto_2
.end method

.method private static decode4to3([BI[BI)I
    .locals 7
    .param p0, "source"    # [B
    .param p1, "srcOffset"    # I
    .param p2, "destination"    # [B
    .param p3, "destOffset"    # I

    .prologue
    const/16 v5, 0x3d

    const/4 v6, 0x0

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 681
    if-nez p0, :cond_0

    .line 682
    new-instance v1, Ljava/lang/NullPointerException;

    const-string v2, "Source array was null."

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 684
    :cond_0
    if-nez p2, :cond_1

    .line 685
    new-instance v1, Ljava/lang/NullPointerException;

    const-string v2, "Destination array was null."

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 687
    :cond_1
    if-ltz p1, :cond_2

    add-int/lit8 v3, p1, 0x3

    array-length v4, p0

    if-lt v3, v4, :cond_3

    .line 688
    :cond_2
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "Source array with length %d cannot have offset of %d and still process four bytes."

    new-array v2, v2, [Ljava/lang/Object;

    array-length v5, p0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v2, v6

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v2, v1

    invoke-static {v4, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v3, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 692
    :cond_3
    if-ltz p3, :cond_4

    add-int/lit8 v3, p3, 0x2

    array-length v4, p2

    if-lt v3, v4, :cond_5

    .line 693
    :cond_4
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "Destination array with length %d cannot have offset of %d and still store three bytes."

    new-array v2, v2, [Ljava/lang/Object;

    array-length v5, p2

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v2, v6

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v2, v1

    invoke-static {v4, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v3, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 701
    :cond_5
    add-int/lit8 v3, p1, 0x2

    aget-byte v3, p0, v3

    if-ne v3, v5, :cond_6

    .line 705
    sget-object v2, Lcom/google/api/client/util/Base64;->DECODABET:[B

    aget-byte v3, p0, p1

    aget-byte v2, v2, v3

    and-int/lit16 v2, v2, 0xff

    shl-int/lit8 v2, v2, 0x12

    sget-object v3, Lcom/google/api/client/util/Base64;->DECODABET:[B

    add-int/lit8 v4, p1, 0x1

    aget-byte v4, p0, v4

    aget-byte v3, v3, v4

    and-int/lit16 v3, v3, 0xff

    shl-int/lit8 v3, v3, 0xc

    or-int v0, v2, v3

    .line 708
    .local v0, "outBuff":I
    ushr-int/lit8 v2, v0, 0x10

    int-to-byte v2, v2

    aput-byte v2, p2, p3

    .line 746
    :goto_0
    return v1

    .line 713
    .end local v0    # "outBuff":I
    :cond_6
    add-int/lit8 v1, p1, 0x3

    aget-byte v1, p0, v1

    if-ne v1, v5, :cond_7

    .line 718
    sget-object v1, Lcom/google/api/client/util/Base64;->DECODABET:[B

    aget-byte v3, p0, p1

    aget-byte v1, v1, v3

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x12

    sget-object v3, Lcom/google/api/client/util/Base64;->DECODABET:[B

    add-int/lit8 v4, p1, 0x1

    aget-byte v4, p0, v4

    aget-byte v3, v3, v4

    and-int/lit16 v3, v3, 0xff

    shl-int/lit8 v3, v3, 0xc

    or-int/2addr v1, v3

    sget-object v3, Lcom/google/api/client/util/Base64;->DECODABET:[B

    add-int/lit8 v4, p1, 0x2

    aget-byte v4, p0, v4

    aget-byte v3, v3, v4

    and-int/lit16 v3, v3, 0xff

    shl-int/lit8 v3, v3, 0x6

    or-int v0, v1, v3

    .line 723
    .restart local v0    # "outBuff":I
    ushr-int/lit8 v1, v0, 0x10

    int-to-byte v1, v1

    aput-byte v1, p2, p3

    .line 724
    add-int/lit8 v1, p3, 0x1

    ushr-int/lit8 v3, v0, 0x8

    int-to-byte v3, v3

    aput-byte v3, p2, v1

    move v1, v2

    .line 725
    goto :goto_0

    .line 735
    .end local v0    # "outBuff":I
    :cond_7
    sget-object v1, Lcom/google/api/client/util/Base64;->DECODABET:[B

    aget-byte v2, p0, p1

    aget-byte v1, v1, v2

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x12

    sget-object v2, Lcom/google/api/client/util/Base64;->DECODABET:[B

    add-int/lit8 v3, p1, 0x1

    aget-byte v3, p0, v3

    aget-byte v2, v2, v3

    and-int/lit16 v2, v2, 0xff

    shl-int/lit8 v2, v2, 0xc

    or-int/2addr v1, v2

    sget-object v2, Lcom/google/api/client/util/Base64;->DECODABET:[B

    add-int/lit8 v3, p1, 0x2

    aget-byte v3, p0, v3

    aget-byte v2, v2, v3

    and-int/lit16 v2, v2, 0xff

    shl-int/lit8 v2, v2, 0x6

    or-int/2addr v1, v2

    sget-object v2, Lcom/google/api/client/util/Base64;->DECODABET:[B

    add-int/lit8 v3, p1, 0x3

    aget-byte v3, p0, v3

    aget-byte v2, v2, v3

    and-int/lit16 v2, v2, 0xff

    or-int v0, v1, v2

    .line 742
    .restart local v0    # "outBuff":I
    shr-int/lit8 v1, v0, 0x10

    int-to-byte v1, v1

    aput-byte v1, p2, p3

    .line 743
    add-int/lit8 v1, p3, 0x1

    shr-int/lit8 v2, v0, 0x8

    int-to-byte v2, v2

    aput-byte v2, p2, v1

    .line 744
    add-int/lit8 v1, p3, 0x2

    int-to-byte v2, v0

    aput-byte v2, p2, v1

    .line 746
    const/4 v1, 0x3

    goto :goto_0
.end method

.method public static encode([B)[B
    .locals 2
    .param p0, "source"    # [B

    .prologue
    .line 565
    const/4 v0, 0x0

    array-length v1, p0

    invoke-static {p0, v0, v1}, Lcom/google/api/client/util/Base64;->encode([BII)[B

    move-result-object v0

    return-object v0
.end method

.method public static encode([BII)[B
    .locals 12
    .param p0, "source"    # [B
    .param p1, "off"    # I
    .param p2, "len"    # I

    .prologue
    const/4 v10, 0x3

    const/4 v8, 0x0

    .line 585
    if-nez p0, :cond_0

    .line 586
    new-instance v7, Ljava/lang/NullPointerException;

    const-string v8, "Cannot serialize a null array."

    invoke-direct {v7, v8}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 589
    :cond_0
    if-gez p1, :cond_1

    .line 590
    new-instance v7, Ljava/lang/IllegalArgumentException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Cannot have negative offset: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 593
    :cond_1
    if-gez p2, :cond_2

    .line 594
    new-instance v7, Ljava/lang/IllegalArgumentException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Cannot have length offset: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 597
    :cond_2
    add-int v7, p1, p2

    array-length v9, p0

    if-le v7, v9, :cond_3

    .line 598
    new-instance v7, Ljava/lang/IllegalArgumentException;

    const-string v9, "Cannot have offset of %d and length of %d with array of length %d"

    new-array v10, v10, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v10, v8

    const/4 v8, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v10, v8

    const/4 v8, 0x2

    array-length v11, p0

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v10, v8

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 612
    :cond_3
    div-int/lit8 v7, p2, 0x3

    mul-int/lit8 v9, v7, 0x4

    rem-int/lit8 v7, p2, 0x3

    if-lez v7, :cond_4

    const/4 v7, 0x4

    :goto_0
    add-int v2, v9, v7

    .line 613
    .local v2, "encLen":I
    new-array v6, v2, [B

    .line 616
    .local v6, "outBuff":[B
    const/4 v0, 0x0

    .line 617
    .local v0, "d":I
    const/4 v1, 0x0

    .line 618
    .local v1, "e":I
    add-int/lit8 v4, p2, -0x2

    .line 619
    .local v4, "len2":I
    const/4 v5, 0x0

    .line 620
    .local v5, "lineLength":I
    :goto_1
    if-ge v0, v4, :cond_5

    .line 621
    add-int v7, v0, p1

    invoke-static {p0, v7, v10, v6, v1}, Lcom/google/api/client/util/Base64;->encode3to4([BII[BI)[B

    .line 623
    add-int/lit8 v5, v5, 0x4

    .line 620
    add-int/lit8 v0, v0, 0x3

    add-int/lit8 v1, v1, 0x4

    goto :goto_1

    .end local v0    # "d":I
    .end local v1    # "e":I
    .end local v2    # "encLen":I
    .end local v4    # "len2":I
    .end local v5    # "lineLength":I
    .end local v6    # "outBuff":[B
    :cond_4
    move v7, v8

    .line 612
    goto :goto_0

    .line 626
    .restart local v0    # "d":I
    .restart local v1    # "e":I
    .restart local v2    # "encLen":I
    .restart local v4    # "len2":I
    .restart local v5    # "lineLength":I
    .restart local v6    # "outBuff":[B
    :cond_5
    if-ge v0, p2, :cond_6

    .line 627
    add-int v7, v0, p1

    sub-int v9, p2, v0

    invoke-static {p0, v7, v9, v6, v1}, Lcom/google/api/client/util/Base64;->encode3to4([BII[BI)[B

    .line 628
    add-int/lit8 v1, v1, 0x4

    .line 633
    :cond_6
    array-length v7, v6

    add-int/lit8 v7, v7, -0x1

    if-gt v1, v7, :cond_7

    .line 638
    new-array v3, v1, [B

    .line 639
    .local v3, "finalOut":[B
    invoke-static {v6, v8, v3, v8, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 644
    .end local v3    # "finalOut":[B
    :goto_2
    return-object v3

    :cond_7
    move-object v3, v6

    goto :goto_2
.end method

.method private static encode3to4([BII[BI)[B
    .locals 5
    .param p0, "source"    # [B
    .param p1, "srcOffset"    # I
    .param p2, "numSigBytes"    # I
    .param p3, "destination"    # [B
    .param p4, "destOffset"    # I

    .prologue
    const/16 v4, 0x3d

    const/4 v1, 0x0

    .line 520
    if-lez p2, :cond_1

    aget-byte v2, p0, p1

    shl-int/lit8 v2, v2, 0x18

    ushr-int/lit8 v2, v2, 0x8

    move v3, v2

    :goto_0
    const/4 v2, 0x1

    if-le p2, v2, :cond_2

    add-int/lit8 v2, p1, 0x1

    aget-byte v2, p0, v2

    shl-int/lit8 v2, v2, 0x18

    ushr-int/lit8 v2, v2, 0x10

    :goto_1
    or-int/2addr v2, v3

    const/4 v3, 0x2

    if-le p2, v3, :cond_0

    add-int/lit8 v1, p1, 0x2

    aget-byte v1, p0, v1

    shl-int/lit8 v1, v1, 0x18

    ushr-int/lit8 v1, v1, 0x18

    :cond_0
    or-int v0, v2, v1

    .line 525
    .local v0, "inBuff":I
    packed-switch p2, :pswitch_data_0

    .line 548
    :goto_2
    return-object p3

    .end local v0    # "inBuff":I
    :cond_1
    move v3, v1

    .line 520
    goto :goto_0

    :cond_2
    move v2, v1

    goto :goto_1

    .line 527
    .restart local v0    # "inBuff":I
    :pswitch_0
    sget-object v1, Lcom/google/api/client/util/Base64;->ALPHABET:[B

    ushr-int/lit8 v2, v0, 0x12

    aget-byte v1, v1, v2

    aput-byte v1, p3, p4

    .line 528
    add-int/lit8 v1, p4, 0x1

    sget-object v2, Lcom/google/api/client/util/Base64;->ALPHABET:[B

    ushr-int/lit8 v3, v0, 0xc

    and-int/lit8 v3, v3, 0x3f

    aget-byte v2, v2, v3

    aput-byte v2, p3, v1

    .line 529
    add-int/lit8 v1, p4, 0x2

    sget-object v2, Lcom/google/api/client/util/Base64;->ALPHABET:[B

    ushr-int/lit8 v3, v0, 0x6

    and-int/lit8 v3, v3, 0x3f

    aget-byte v2, v2, v3

    aput-byte v2, p3, v1

    .line 530
    add-int/lit8 v1, p4, 0x3

    sget-object v2, Lcom/google/api/client/util/Base64;->ALPHABET:[B

    and-int/lit8 v3, v0, 0x3f

    aget-byte v2, v2, v3

    aput-byte v2, p3, v1

    goto :goto_2

    .line 534
    :pswitch_1
    sget-object v1, Lcom/google/api/client/util/Base64;->ALPHABET:[B

    ushr-int/lit8 v2, v0, 0x12

    aget-byte v1, v1, v2

    aput-byte v1, p3, p4

    .line 535
    add-int/lit8 v1, p4, 0x1

    sget-object v2, Lcom/google/api/client/util/Base64;->ALPHABET:[B

    ushr-int/lit8 v3, v0, 0xc

    and-int/lit8 v3, v3, 0x3f

    aget-byte v2, v2, v3

    aput-byte v2, p3, v1

    .line 536
    add-int/lit8 v1, p4, 0x2

    sget-object v2, Lcom/google/api/client/util/Base64;->ALPHABET:[B

    ushr-int/lit8 v3, v0, 0x6

    and-int/lit8 v3, v3, 0x3f

    aget-byte v2, v2, v3

    aput-byte v2, p3, v1

    .line 537
    add-int/lit8 v1, p4, 0x3

    aput-byte v4, p3, v1

    goto :goto_2

    .line 541
    :pswitch_2
    sget-object v1, Lcom/google/api/client/util/Base64;->ALPHABET:[B

    ushr-int/lit8 v2, v0, 0x12

    aget-byte v1, v1, v2

    aput-byte v1, p3, p4

    .line 542
    add-int/lit8 v1, p4, 0x1

    sget-object v2, Lcom/google/api/client/util/Base64;->ALPHABET:[B

    ushr-int/lit8 v3, v0, 0xc

    and-int/lit8 v3, v3, 0x3f

    aget-byte v2, v2, v3

    aput-byte v2, p3, v1

    .line 543
    add-int/lit8 v1, p4, 0x2

    aput-byte v4, p3, v1

    .line 544
    add-int/lit8 v1, p4, 0x3

    aput-byte v4, p3, v1

    goto :goto_2

    .line 525
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
