.class public Lcom/google/api/client/http/protobuf/ProtoHttpContent;
.super Lcom/google/api/client/http/AbstractHttpContent;
.source "ProtoHttpContent.java"


# instance fields
.field private final message:Lcom/google/protobuf/MessageLite;

.field private type:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/protobuf/MessageLite;)V
    .locals 1
    .param p1, "message"    # Lcom/google/protobuf/MessageLite;

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/google/api/client/http/AbstractHttpContent;-><init>()V

    .line 54
    const-string v0, "application/x-protobuf"

    iput-object v0, p0, Lcom/google/api/client/http/protobuf/ProtoHttpContent;->type:Ljava/lang/String;

    .line 60
    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    iput-object v0, p0, Lcom/google/api/client/http/protobuf/ProtoHttpContent;->message:Lcom/google/protobuf/MessageLite;

    .line 61
    return-void
.end method


# virtual methods
.method public getLength()J
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 65
    iget-object v0, p0, Lcom/google/api/client/http/protobuf/ProtoHttpContent;->message:Lcom/google/protobuf/MessageLite;

    invoke-interface {v0}, Lcom/google/protobuf/MessageLite;->getSerializedSize()I

    move-result v0

    int-to-long v0, v0

    return-wide v0
.end method

.method public final getMessage()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/google/api/client/http/protobuf/ProtoHttpContent;->message:Lcom/google/protobuf/MessageLite;

    return-object v0
.end method

.method public getType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/api/client/http/protobuf/ProtoHttpContent;->type:Ljava/lang/String;

    return-object v0
.end method

.method public setType(Ljava/lang/String;)Lcom/google/api/client/http/protobuf/ProtoHttpContent;
    .locals 0
    .param p1, "type"    # Ljava/lang/String;

    .prologue
    .line 84
    iput-object p1, p0, Lcom/google/api/client/http/protobuf/ProtoHttpContent;->type:Ljava/lang/String;

    .line 85
    return-object p0
.end method

.method public writeTo(Ljava/io/OutputStream;)V
    .locals 1
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/api/client/http/protobuf/ProtoHttpContent;->message:Lcom/google/protobuf/MessageLite;

    invoke-interface {v0, p1}, Lcom/google/protobuf/MessageLite;->writeTo(Ljava/io/OutputStream;)V

    .line 74
    return-void
.end method
