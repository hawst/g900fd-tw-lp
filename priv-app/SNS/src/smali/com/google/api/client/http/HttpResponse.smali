.class public final Lcom/google/api/client/http/HttpResponse;
.super Ljava/lang/Object;
.source "HttpResponse.java"


# instance fields
.field private content:Ljava/io/InputStream;

.field private final contentEncoding:Ljava/lang/String;

.field private contentLength:J

.field private final contentType:Ljava/lang/String;

.field private disableContentLogging:Z

.field private final headers:Lcom/google/api/client/http/HttpHeaders;

.field private final isSuccessStatusCode:Z

.field private final request:Lcom/google/api/client/http/HttpRequest;

.field private response:Lcom/google/api/client/http/LowLevelHttpResponse;

.field private final statusCode:I

.field private final statusMessage:Ljava/lang/String;

.field private final transport:Lcom/google/api/client/http/HttpTransport;


# direct methods
.method constructor <init>(Lcom/google/api/client/http/HttpRequest;Lcom/google/api/client/http/LowLevelHttpResponse;)V
    .locals 28
    .param p1, "request"    # Lcom/google/api/client/http/HttpRequest;
    .param p2, "response"    # Lcom/google/api/client/http/LowLevelHttpResponse;

    .prologue
    .line 98
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 99
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/api/client/http/HttpResponse;->request:Lcom/google/api/client/http/HttpRequest;

    .line 100
    invoke-virtual/range {p1 .. p1}, Lcom/google/api/client/http/HttpRequest;->getTransport()Lcom/google/api/client/http/HttpTransport;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/api/client/http/HttpResponse;->transport:Lcom/google/api/client/http/HttpTransport;

    .line 101
    invoke-virtual/range {p1 .. p1}, Lcom/google/api/client/http/HttpRequest;->getResponseHeaders()Lcom/google/api/client/http/HttpHeaders;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/api/client/http/HttpResponse;->headers:Lcom/google/api/client/http/HttpHeaders;

    .line 102
    move-object/from16 v0, p2

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/api/client/http/HttpResponse;->response:Lcom/google/api/client/http/LowLevelHttpResponse;

    .line 103
    invoke-virtual/range {p2 .. p2}, Lcom/google/api/client/http/LowLevelHttpResponse;->getContentLength()J

    move-result-wide v26

    move-wide/from16 v0, v26

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/google/api/client/http/HttpResponse;->contentLength:J

    .line 104
    invoke-virtual/range {p2 .. p2}, Lcom/google/api/client/http/LowLevelHttpResponse;->getContentType()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/api/client/http/HttpResponse;->contentType:Ljava/lang/String;

    .line 105
    invoke-virtual/range {p2 .. p2}, Lcom/google/api/client/http/LowLevelHttpResponse;->getContentEncoding()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/api/client/http/HttpResponse;->contentEncoding:Ljava/lang/String;

    .line 106
    invoke-virtual/range {p2 .. p2}, Lcom/google/api/client/http/LowLevelHttpResponse;->getStatusCode()I

    move-result v6

    .line 107
    .local v6, "code":I
    move-object/from16 v0, p0

    iput v6, v0, Lcom/google/api/client/http/HttpResponse;->statusCode:I

    .line 108
    invoke-static {v6}, Lcom/google/api/client/http/HttpStatusCodes;->isSuccess(I)Z

    move-result v26

    move/from16 v0, v26

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/api/client/http/HttpResponse;->isSuccessStatusCode:Z

    .line 109
    invoke-virtual/range {p2 .. p2}, Lcom/google/api/client/http/LowLevelHttpResponse;->getReasonPhrase()Ljava/lang/String;

    move-result-object v20

    .line 110
    .local v20, "message":Ljava/lang/String;
    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/api/client/http/HttpResponse;->statusMessage:Ljava/lang/String;

    .line 111
    sget-object v19, Lcom/google/api/client/http/HttpTransport;->LOGGER:Ljava/util/logging/Logger;

    .line 112
    .local v19, "logger":Ljava/util/logging/Logger;
    sget-object v26, Ljava/util/logging/Level;->CONFIG:Ljava/util/logging/Level;

    move-object/from16 v0, v19

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v18

    .line 113
    .local v18, "loggable":Z
    const/16 v17, 0x0

    .line 114
    .local v17, "logbuf":Ljava/lang/StringBuilder;
    if-eqz v18, :cond_1

    .line 115
    new-instance v17, Ljava/lang/StringBuilder;

    .end local v17    # "logbuf":Ljava/lang/StringBuilder;
    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    .line 116
    .restart local v17    # "logbuf":Ljava/lang/StringBuilder;
    const-string v26, "-------------- RESPONSE --------------"

    move-object/from16 v0, v17

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    sget-object v27, Lcom/google/api/client/util/Strings;->LINE_SEPARATOR:Ljava/lang/String;

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 117
    invoke-virtual/range {p2 .. p2}, Lcom/google/api/client/http/LowLevelHttpResponse;->getStatusLine()Ljava/lang/String;

    move-result-object v23

    .line 118
    .local v23, "statusLine":Ljava/lang/String;
    if-eqz v23, :cond_4

    .line 119
    move-object/from16 v0, v17

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 126
    :cond_0
    :goto_0
    sget-object v26, Lcom/google/api/client/util/Strings;->LINE_SEPARATOR:Ljava/lang/String;

    move-object/from16 v0, v17

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 129
    .end local v23    # "statusLine":Ljava/lang/String;
    :cond_1
    invoke-virtual/range {p2 .. p2}, Lcom/google/api/client/http/LowLevelHttpResponse;->getHeaderCount()I

    move-result v22

    .line 130
    .local v22, "size":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/api/client/http/HttpResponse;->headers:Lcom/google/api/client/http/HttpHeaders;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v14

    .line 131
    .local v14, "headersClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/google/api/client/http/HttpHeaders;>;"
    const/16 v26, 0x1

    move/from16 v0, v26

    new-array v0, v0, [Ljava/lang/reflect/Type;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    aput-object v14, v26, v27

    invoke-static/range {v26 .. v26}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v8

    .line 132
    .local v8, "context":Ljava/util/List;, "Ljava/util/List<Ljava/lang/reflect/Type;>;"
    invoke-static {v14}, Lcom/google/api/client/util/ClassInfo;->of(Ljava/lang/Class;)Lcom/google/api/client/util/ClassInfo;

    move-result-object v5

    .line 133
    .local v5, "classInfo":Lcom/google/api/client/util/ClassInfo;
    invoke-static {v14}, Lcom/google/api/client/http/HttpHeaders;->getFieldNameMap(Ljava/lang/Class;)Ljava/util/HashMap;

    move-result-object v11

    .line 134
    .local v11, "fieldNameMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v4, Lcom/google/api/client/util/ArrayValueMap;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/api/client/http/HttpResponse;->headers:Lcom/google/api/client/http/HttpHeaders;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    invoke-direct {v4, v0}, Lcom/google/api/client/util/ArrayValueMap;-><init>(Ljava/lang/Object;)V

    .line 135
    .local v4, "arrayValueMap":Lcom/google/api/client/util/ArrayValueMap;
    const/4 v15, 0x0

    .local v15, "i":I
    :goto_1
    move/from16 v0, v22

    if-ge v15, v0, :cond_b

    .line 136
    move-object/from16 v0, p2

    invoke-virtual {v0, v15}, Lcom/google/api/client/http/LowLevelHttpResponse;->getHeaderName(I)Ljava/lang/String;

    move-result-object v12

    .line 137
    .local v12, "headerName":Ljava/lang/String;
    move-object/from16 v0, p2

    invoke-virtual {v0, v15}, Lcom/google/api/client/http/LowLevelHttpResponse;->getHeaderValue(I)Ljava/lang/String;

    move-result-object v13

    .line 138
    .local v13, "headerValue":Ljava/lang/String;
    if-eqz v18, :cond_2

    .line 139
    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v26

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, ": "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v17

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    sget-object v27, Lcom/google/api/client/util/Strings;->LINE_SEPARATOR:Ljava/lang/String;

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 141
    :cond_2
    invoke-virtual {v11, v12}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    .line 142
    .local v10, "fieldName":Ljava/lang/String;
    if-nez v10, :cond_3

    .line 143
    move-object v10, v12

    .line 146
    :cond_3
    invoke-virtual {v5, v10}, Lcom/google/api/client/util/ClassInfo;->getFieldInfo(Ljava/lang/String;)Lcom/google/api/client/util/FieldInfo;

    move-result-object v9

    .line 147
    .local v9, "fieldInfo":Lcom/google/api/client/util/FieldInfo;
    if-eqz v9, :cond_9

    .line 148
    invoke-virtual {v9}, Lcom/google/api/client/util/FieldInfo;->getGenericType()Ljava/lang/reflect/Type;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-static {v8, v0}, Lcom/google/api/client/util/Data;->resolveWildcardTypeOrTypeVariable(Ljava/util/List;Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;

    move-result-object v25

    .line 150
    .local v25, "type":Ljava/lang/reflect/Type;
    invoke-static/range {v25 .. v25}, Lcom/google/api/client/util/Types;->isArray(Ljava/lang/reflect/Type;)Z

    move-result v26

    if-eqz v26, :cond_5

    .line 152
    invoke-static/range {v25 .. v25}, Lcom/google/api/client/util/Types;->getArrayComponentType(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-static {v8, v0}, Lcom/google/api/client/util/Types;->getRawArrayComponentType(Ljava/util/List;Ljava/lang/reflect/Type;)Ljava/lang/Class;

    move-result-object v21

    .line 154
    .local v21, "rawArrayComponentType":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    invoke-virtual {v9}, Lcom/google/api/client/util/FieldInfo;->getField()Ljava/lang/reflect/Field;

    move-result-object v26

    move-object/from16 v0, v21

    invoke-static {v0, v8, v13}, Lcom/google/api/client/http/HttpResponse;->parseValue(Ljava/lang/reflect/Type;Ljava/util/List;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v27

    move-object/from16 v0, v26

    move-object/from16 v1, v21

    move-object/from16 v2, v27

    invoke-virtual {v4, v0, v1, v2}, Lcom/google/api/client/util/ArrayValueMap;->put(Ljava/lang/reflect/Field;Ljava/lang/Class;Ljava/lang/Object;)V

    .line 135
    .end local v21    # "rawArrayComponentType":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v25    # "type":Ljava/lang/reflect/Type;
    :goto_2
    add-int/lit8 v15, v15, 0x1

    goto :goto_1

    .line 121
    .end local v4    # "arrayValueMap":Lcom/google/api/client/util/ArrayValueMap;
    .end local v5    # "classInfo":Lcom/google/api/client/util/ClassInfo;
    .end local v8    # "context":Ljava/util/List;, "Ljava/util/List<Ljava/lang/reflect/Type;>;"
    .end local v9    # "fieldInfo":Lcom/google/api/client/util/FieldInfo;
    .end local v10    # "fieldName":Ljava/lang/String;
    .end local v11    # "fieldNameMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v12    # "headerName":Ljava/lang/String;
    .end local v13    # "headerValue":Ljava/lang/String;
    .end local v14    # "headersClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/google/api/client/http/HttpHeaders;>;"
    .end local v15    # "i":I
    .end local v22    # "size":I
    .restart local v23    # "statusLine":Ljava/lang/String;
    :cond_4
    move-object/from16 v0, v17

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 122
    if-eqz v20, :cond_0

    .line 123
    const/16 v26, 0x20

    move-object/from16 v0, v17

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 156
    .end local v23    # "statusLine":Ljava/lang/String;
    .restart local v4    # "arrayValueMap":Lcom/google/api/client/util/ArrayValueMap;
    .restart local v5    # "classInfo":Lcom/google/api/client/util/ClassInfo;
    .restart local v8    # "context":Ljava/util/List;, "Ljava/util/List<Ljava/lang/reflect/Type;>;"
    .restart local v9    # "fieldInfo":Lcom/google/api/client/util/FieldInfo;
    .restart local v10    # "fieldName":Ljava/lang/String;
    .restart local v11    # "fieldNameMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    .restart local v12    # "headerName":Ljava/lang/String;
    .restart local v13    # "headerValue":Ljava/lang/String;
    .restart local v14    # "headersClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/google/api/client/http/HttpHeaders;>;"
    .restart local v15    # "i":I
    .restart local v22    # "size":I
    .restart local v25    # "type":Ljava/lang/reflect/Type;
    :cond_5
    move-object/from16 v0, v25

    invoke-static {v8, v0}, Lcom/google/api/client/util/Types;->getRawArrayComponentType(Ljava/util/List;Ljava/lang/reflect/Type;)Ljava/lang/Class;

    move-result-object v26

    const-class v27, Ljava/lang/Iterable;

    invoke-static/range {v26 .. v27}, Lcom/google/api/client/util/Types;->isAssignableToOrFrom(Ljava/lang/Class;Ljava/lang/Class;)Z

    move-result v26

    if-eqz v26, :cond_8

    .line 160
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/api/client/http/HttpResponse;->headers:Lcom/google/api/client/http/HttpHeaders;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    invoke-virtual {v9, v0}, Lcom/google/api/client/util/FieldInfo;->getValue(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/util/Collection;

    .line 161
    .local v7, "collection":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Object;>;"
    if-nez v7, :cond_6

    .line 162
    invoke-static/range {v25 .. v25}, Lcom/google/api/client/util/Data;->newCollectionInstance(Ljava/lang/reflect/Type;)Ljava/util/Collection;

    move-result-object v7

    .line 163
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/api/client/http/HttpResponse;->headers:Lcom/google/api/client/http/HttpHeaders;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    invoke-virtual {v9, v0, v7}, Lcom/google/api/client/util/FieldInfo;->setValue(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 165
    :cond_6
    const-class v26, Ljava/lang/Object;

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    if-ne v0, v1, :cond_7

    const/16 v24, 0x0

    .line 166
    .local v24, "subFieldType":Ljava/lang/reflect/Type;
    :goto_3
    move-object/from16 v0, v24

    invoke-static {v0, v8, v13}, Lcom/google/api/client/http/HttpResponse;->parseValue(Ljava/lang/reflect/Type;Ljava/util/List;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-interface {v7, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 165
    .end local v24    # "subFieldType":Ljava/lang/reflect/Type;
    :cond_7
    invoke-static/range {v25 .. v25}, Lcom/google/api/client/util/Types;->getIterableParameter(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;

    move-result-object v24

    goto :goto_3

    .line 169
    .end local v7    # "collection":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Object;>;"
    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/api/client/http/HttpResponse;->headers:Lcom/google/api/client/http/HttpHeaders;

    move-object/from16 v26, v0

    move-object/from16 v0, v25

    invoke-static {v0, v8, v13}, Lcom/google/api/client/http/HttpResponse;->parseValue(Ljava/lang/reflect/Type;Ljava/util/List;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v27

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    invoke-virtual {v9, v0, v1}, Lcom/google/api/client/util/FieldInfo;->setValue(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_2

    .line 174
    .end local v25    # "type":Ljava/lang/reflect/Type;
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/api/client/http/HttpResponse;->headers:Lcom/google/api/client/http/HttpHeaders;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    invoke-virtual {v0, v10}, Lcom/google/api/client/http/HttpHeaders;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Ljava/util/ArrayList;

    .line 175
    .local v16, "listValue":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-nez v16, :cond_a

    .line 176
    new-instance v16, Ljava/util/ArrayList;

    .end local v16    # "listValue":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-direct/range {v16 .. v16}, Ljava/util/ArrayList;-><init>()V

    .line 177
    .restart local v16    # "listValue":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/api/client/http/HttpResponse;->headers:Lcom/google/api/client/http/HttpHeaders;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    move-object/from16 v1, v16

    invoke-virtual {v0, v10, v1}, Lcom/google/api/client/http/HttpHeaders;->set(Ljava/lang/String;Ljava/lang/Object;)V

    .line 179
    :cond_a
    move-object/from16 v0, v16

    invoke-virtual {v0, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 182
    .end local v9    # "fieldInfo":Lcom/google/api/client/util/FieldInfo;
    .end local v10    # "fieldName":Ljava/lang/String;
    .end local v12    # "headerName":Ljava/lang/String;
    .end local v13    # "headerValue":Ljava/lang/String;
    .end local v16    # "listValue":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_b
    invoke-virtual {v4}, Lcom/google/api/client/util/ArrayValueMap;->setValues()V

    .line 184
    if-eqz v18, :cond_c

    .line 185
    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v19

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->config(Ljava/lang/String;)V

    .line 187
    :cond_c
    return-void
.end method

.method public static isSuccessStatusCode(I)Z
    .locals 1
    .param p0, "statusCode"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 433
    invoke-static {p0}, Lcom/google/api/client/http/HttpStatusCodes;->isSuccess(I)Z

    move-result v0

    return v0
.end method

.method private static parseValue(Ljava/lang/reflect/Type;Ljava/util/List;Ljava/lang/String;)Ljava/lang/Object;
    .locals 2
    .param p0, "valueType"    # Ljava/lang/reflect/Type;
    .param p2, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/reflect/Type;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/reflect/Type;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/Object;"
        }
    .end annotation

    .prologue
    .line 289
    .local p1, "context":Ljava/util/List;, "Ljava/util/List<Ljava/lang/reflect/Type;>;"
    invoke-static {p1, p0}, Lcom/google/api/client/util/Data;->resolveWildcardTypeOrTypeVariable(Ljava/util/List;Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;

    move-result-object v0

    .line 290
    .local v0, "resolved":Ljava/lang/reflect/Type;
    invoke-static {v0, p2}, Lcom/google/api/client/util/Data;->parsePrimitiveValue(Ljava/lang/reflect/Type;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    return-object v1
.end method


# virtual methods
.method public disconnect()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 360
    iget-object v0, p0, Lcom/google/api/client/http/HttpResponse;->response:Lcom/google/api/client/http/LowLevelHttpResponse;

    invoke-virtual {v0}, Lcom/google/api/client/http/LowLevelHttpResponse;->disconnect()V

    .line 361
    return-void
.end method

.method public getContent()Ljava/io/InputStream;
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 302
    iget-object v8, p0, Lcom/google/api/client/http/HttpResponse;->response:Lcom/google/api/client/http/LowLevelHttpResponse;

    .line 303
    .local v8, "response":Lcom/google/api/client/http/LowLevelHttpResponse;
    if-nez v8, :cond_1

    .line 304
    iget-object v0, p0, Lcom/google/api/client/http/HttpResponse;->content:Ljava/io/InputStream;

    .line 341
    :cond_0
    :goto_0
    return-object v0

    .line 306
    :cond_1
    iget-object v9, p0, Lcom/google/api/client/http/HttpResponse;->response:Lcom/google/api/client/http/LowLevelHttpResponse;

    invoke-virtual {v9}, Lcom/google/api/client/http/LowLevelHttpResponse;->getContent()Ljava/io/InputStream;

    move-result-object v0

    .line 307
    .local v0, "content":Ljava/io/InputStream;
    const/4 v9, 0x0

    iput-object v9, p0, Lcom/google/api/client/http/HttpResponse;->response:Lcom/google/api/client/http/LowLevelHttpResponse;

    .line 308
    if-eqz v0, :cond_0

    .line 309
    const/4 v4, 0x0

    .line 310
    .local v4, "debugContentByteArray":[B
    sget-object v7, Lcom/google/api/client/http/HttpTransport;->LOGGER:Ljava/util/logging/Logger;

    .line 311
    .local v7, "logger":Ljava/util/logging/Logger;
    iget-boolean v9, p0, Lcom/google/api/client/http/HttpResponse;->disableContentLogging:Z

    if-nez v9, :cond_2

    sget-object v9, Ljava/util/logging/Level;->CONFIG:Ljava/util/logging/Level;

    invoke-virtual {v7, v9}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v9

    if-nez v9, :cond_3

    :cond_2
    sget-object v9, Ljava/util/logging/Level;->ALL:Ljava/util/logging/Level;

    invoke-virtual {v7, v9}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v9

    if-eqz v9, :cond_7

    :cond_3
    const/4 v6, 0x1

    .line 313
    .local v6, "loggable":Z
    :goto_1
    if-eqz v6, :cond_4

    .line 314
    new-instance v5, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v5}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 315
    .local v5, "debugStream":Ljava/io/ByteArrayOutputStream;
    invoke-static {v0, v5}, Lcom/google/api/client/http/AbstractInputStreamContent;->copy(Ljava/io/InputStream;Ljava/io/OutputStream;)V

    .line 316
    invoke-virtual {v5}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v4

    .line 317
    new-instance v0, Ljava/io/ByteArrayInputStream;

    .end local v0    # "content":Ljava/io/InputStream;
    invoke-direct {v0, v4}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 318
    .restart local v0    # "content":Ljava/io/InputStream;
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Response size: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    array-length v10, v4

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " bytes"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v9}, Ljava/util/logging/Logger;->config(Ljava/lang/String;)V

    .line 321
    .end local v5    # "debugStream":Ljava/io/ByteArrayOutputStream;
    :cond_4
    iget-object v2, p0, Lcom/google/api/client/http/HttpResponse;->contentEncoding:Ljava/lang/String;

    .line 322
    .local v2, "contentEncoding":Ljava/lang/String;
    if-eqz v2, :cond_5

    const-string v9, "gzip"

    invoke-virtual {v2, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 323
    new-instance v1, Ljava/util/zip/GZIPInputStream;

    invoke-direct {v1, v0}, Ljava/util/zip/GZIPInputStream;-><init>(Ljava/io/InputStream;)V

    .line 324
    .end local v0    # "content":Ljava/io/InputStream;
    .local v1, "content":Ljava/io/InputStream;
    const-wide/16 v10, -0x1

    iput-wide v10, p0, Lcom/google/api/client/http/HttpResponse;->contentLength:J

    .line 325
    if-eqz v6, :cond_8

    .line 326
    new-instance v5, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v5}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 327
    .restart local v5    # "debugStream":Ljava/io/ByteArrayOutputStream;
    invoke-static {v1, v5}, Lcom/google/api/client/http/AbstractInputStreamContent;->copy(Ljava/io/InputStream;Ljava/io/OutputStream;)V

    .line 328
    invoke-virtual {v5}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v4

    .line 329
    new-instance v0, Ljava/io/ByteArrayInputStream;

    invoke-direct {v0, v4}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 332
    .end local v1    # "content":Ljava/io/InputStream;
    .end local v5    # "debugStream":Ljava/io/ByteArrayOutputStream;
    .restart local v0    # "content":Ljava/io/InputStream;
    :cond_5
    :goto_2
    if-eqz v6, :cond_6

    .line 334
    iget-object v3, p0, Lcom/google/api/client/http/HttpResponse;->contentType:Ljava/lang/String;

    .line 335
    .local v3, "contentType":Ljava/lang/String;
    array-length v9, v4

    if-eqz v9, :cond_6

    invoke-static {v3}, Lcom/google/api/client/http/LogContent;->isTextBasedContentType(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 336
    invoke-static {v4}, Lcom/google/api/client/util/Strings;->fromBytesUtf8([B)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v9}, Ljava/util/logging/Logger;->config(Ljava/lang/String;)V

    .line 339
    .end local v3    # "contentType":Ljava/lang/String;
    :cond_6
    iput-object v0, p0, Lcom/google/api/client/http/HttpResponse;->content:Ljava/io/InputStream;

    goto/16 :goto_0

    .line 311
    .end local v2    # "contentEncoding":Ljava/lang/String;
    .end local v6    # "loggable":Z
    :cond_7
    const/4 v6, 0x0

    goto :goto_1

    .end local v0    # "content":Ljava/io/InputStream;
    .restart local v1    # "content":Ljava/io/InputStream;
    .restart local v2    # "contentEncoding":Ljava/lang/String;
    .restart local v6    # "loggable":Z
    :cond_8
    move-object v0, v1

    .end local v1    # "content":Ljava/io/InputStream;
    .restart local v0    # "content":Ljava/io/InputStream;
    goto :goto_2
.end method

.method public getContentEncoding()Ljava/lang/String;
    .locals 1

    .prologue
    .line 221
    iget-object v0, p0, Lcom/google/api/client/http/HttpResponse;->contentEncoding:Ljava/lang/String;

    return-object v0
.end method

.method public getContentType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 230
    iget-object v0, p0, Lcom/google/api/client/http/HttpResponse;->contentType:Ljava/lang/String;

    return-object v0
.end method

.method public getDisableContentLogging()Z
    .locals 1

    .prologue
    .line 196
    iget-boolean v0, p0, Lcom/google/api/client/http/HttpResponse;->disableContentLogging:Z

    return v0
.end method

.method public getHeaders()Lcom/google/api/client/http/HttpHeaders;
    .locals 1

    .prologue
    .line 239
    iget-object v0, p0, Lcom/google/api/client/http/HttpResponse;->headers:Lcom/google/api/client/http/HttpHeaders;

    return-object v0
.end method

.method public getParser()Lcom/google/api/client/http/HttpParser;
    .locals 2

    .prologue
    .line 368
    iget-object v0, p0, Lcom/google/api/client/http/HttpResponse;->request:Lcom/google/api/client/http/HttpRequest;

    iget-object v1, p0, Lcom/google/api/client/http/HttpResponse;->contentType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/api/client/http/HttpRequest;->getParser(Ljava/lang/String;)Lcom/google/api/client/http/HttpParser;

    move-result-object v0

    return-object v0
.end method

.method public getRequest()Lcom/google/api/client/http/HttpRequest;
    .locals 1

    .prologue
    .line 285
    iget-object v0, p0, Lcom/google/api/client/http/HttpResponse;->request:Lcom/google/api/client/http/HttpRequest;

    return-object v0
.end method

.method public getStatusCode()I
    .locals 1

    .prologue
    .line 258
    iget v0, p0, Lcom/google/api/client/http/HttpResponse;->statusCode:I

    return v0
.end method

.method public getStatusMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 267
    iget-object v0, p0, Lcom/google/api/client/http/HttpResponse;->statusMessage:Ljava/lang/String;

    return-object v0
.end method

.method public getTransport()Lcom/google/api/client/http/HttpTransport;
    .locals 1

    .prologue
    .line 276
    iget-object v0, p0, Lcom/google/api/client/http/HttpResponse;->transport:Lcom/google/api/client/http/HttpTransport;

    return-object v0
.end method

.method public ignore()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 348
    invoke-virtual {p0}, Lcom/google/api/client/http/HttpResponse;->getContent()Ljava/io/InputStream;

    move-result-object v0

    .line 349
    .local v0, "content":Ljava/io/InputStream;
    if-eqz v0, :cond_0

    .line 350
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    .line 352
    :cond_0
    return-void
.end method

.method public isSuccessStatusCode()Z
    .locals 1

    .prologue
    .line 249
    iget-boolean v0, p0, Lcom/google/api/client/http/HttpResponse;->isSuccessStatusCode:Z

    return v0
.end method

.method public parseAs(Ljava/lang/Class;)Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 381
    .local p1, "dataClass":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    invoke-virtual {p0}, Lcom/google/api/client/http/HttpResponse;->getParser()Lcom/google/api/client/http/HttpParser;

    move-result-object v0

    .line 382
    .local v0, "parser":Lcom/google/api/client/http/HttpParser;
    if-nez v0, :cond_1

    .line 383
    iget-object v1, p0, Lcom/google/api/client/http/HttpResponse;->contentType:Ljava/lang/String;

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    const-string v2, "Missing Content-Type header in response"

    invoke-static {v1, v2}, Lcom/google/common/base/Preconditions;->checkArgument(ZLjava/lang/Object;)V

    .line 384
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "No parser defined for Content-Type: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/api/client/http/HttpResponse;->contentType:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 383
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 386
    :cond_1
    invoke-interface {v0, p0, p1}, Lcom/google/api/client/http/HttpParser;->parse(Lcom/google/api/client/http/HttpResponse;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    return-object v1
.end method

.method public parseAsString()Ljava/lang/String;
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v1, 0x1000

    .line 399
    invoke-virtual {p0}, Lcom/google/api/client/http/HttpResponse;->getContent()Ljava/io/InputStream;

    move-result-object v3

    .line 400
    .local v3, "content":Ljava/io/InputStream;
    if-nez v3, :cond_0

    .line 401
    const-string v9, ""

    .line 422
    :goto_0
    return-object v9

    .line 404
    :cond_0
    :try_start_0
    iget-wide v4, p0, Lcom/google/api/client/http/HttpResponse;->contentLength:J

    .line 405
    .local v4, "contentLength":J
    const-wide/16 v10, -0x1

    cmp-long v9, v4, v10

    if-nez v9, :cond_2

    .line 406
    .local v1, "bufferSize":I
    :goto_1
    const/4 v6, 0x0

    .line 407
    .local v6, "length":I
    new-array v0, v1, [B

    .line 408
    .local v0, "buffer":[B
    const/16 v9, 0x1000

    new-array v8, v9, [B

    .line 410
    .local v8, "tmp":[B
    :goto_2
    invoke-virtual {v3, v8}, Ljava/io/InputStream;->read([B)I

    move-result v2

    .local v2, "bytesRead":I
    const/4 v9, -0x1

    if-eq v2, v9, :cond_3

    .line 411
    add-int v9, v6, v2

    if-le v9, v1, :cond_1

    .line 412
    shl-int/lit8 v9, v1, 0x1

    add-int v10, v6, v2

    invoke-static {v9, v10}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 413
    new-array v7, v1, [B

    .line 414
    .local v7, "newbuffer":[B
    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-static {v0, v9, v7, v10, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 415
    move-object v0, v7

    .line 417
    .end local v7    # "newbuffer":[B
    :cond_1
    const/4 v9, 0x0

    invoke-static {v8, v9, v0, v6, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 418
    add-int/2addr v6, v2

    goto :goto_2

    .line 405
    .end local v0    # "buffer":[B
    .end local v1    # "bufferSize":I
    .end local v2    # "bytesRead":I
    .end local v6    # "length":I
    .end local v8    # "tmp":[B
    :cond_2
    long-to-int v1, v4

    goto :goto_1

    .line 420
    .restart local v0    # "buffer":[B
    .restart local v1    # "bufferSize":I
    .restart local v2    # "bytesRead":I
    .restart local v6    # "length":I
    .restart local v8    # "tmp":[B
    :cond_3
    new-instance v9, Ljava/lang/String;

    const/4 v10, 0x0

    const-string v11, "UTF-8"

    invoke-direct {v9, v0, v10, v6, v11}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 422
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V

    goto :goto_0

    .end local v0    # "buffer":[B
    .end local v1    # "bufferSize":I
    .end local v2    # "bytesRead":I
    .end local v4    # "contentLength":J
    .end local v6    # "length":I
    .end local v8    # "tmp":[B
    :catchall_0
    move-exception v9

    invoke-virtual {v3}, Ljava/io/InputStream;->close()V

    throw v9
.end method

.method public setDisableContentLogging(Z)Lcom/google/api/client/http/HttpResponse;
    .locals 0
    .param p1, "disableContentLogging"    # Z

    .prologue
    .line 211
    iput-boolean p1, p0, Lcom/google/api/client/http/HttpResponse;->disableContentLogging:Z

    .line 212
    return-object p0
.end method
