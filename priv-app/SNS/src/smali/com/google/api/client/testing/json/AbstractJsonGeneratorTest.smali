.class public abstract Lcom/google/api/client/testing/json/AbstractJsonGeneratorTest;
.super Ljunit/framework/TestCase;
.source "AbstractJsonGeneratorTest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/api/client/testing/json/AbstractJsonGeneratorTest$Feed;,
        Lcom/google/api/client/testing/json/AbstractJsonGeneratorTest$Entry;
    }
.end annotation


# static fields
.field private static final JSON_ENTRY:Ljava/lang/String; = "{\"title\":\"foo\"}"

.field private static final JSON_FEED:Ljava/lang/String; = "{\"entries\":[{\"title\":\"foo\"},{\"title\":\"bar\"}]}"


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 37
    invoke-direct {p0, p1}, Ljunit/framework/TestCase;-><init>(Ljava/lang/String;)V

    .line 38
    return-void
.end method


# virtual methods
.method protected abstract newFactory()Lcom/google/api/client/json/JsonFactory;
.end method

.method public final testGenerateEntry()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 48
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 49
    .local v1, "out":Ljava/io/ByteArrayOutputStream;
    invoke-virtual {p0}, Lcom/google/api/client/testing/json/AbstractJsonGeneratorTest;->newFactory()Lcom/google/api/client/json/JsonFactory;

    move-result-object v2

    sget-object v3, Lcom/google/api/client/json/JsonEncoding;->UTF8:Lcom/google/api/client/json/JsonEncoding;

    invoke-virtual {v2, v1, v3}, Lcom/google/api/client/json/JsonFactory;->createJsonGenerator(Ljava/io/OutputStream;Lcom/google/api/client/json/JsonEncoding;)Lcom/google/api/client/json/JsonGenerator;

    move-result-object v0

    .line 50
    .local v0, "generator":Lcom/google/api/client/json/JsonGenerator;
    new-instance v2, Lcom/google/api/client/testing/json/AbstractJsonGeneratorTest$Entry;

    const-string v3, "foo"

    invoke-direct {v2, v3}, Lcom/google/api/client/testing/json/AbstractJsonGeneratorTest$Entry;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Lcom/google/api/client/json/JsonGenerator;->serialize(Ljava/lang/Object;)V

    .line 51
    invoke-virtual {v0}, Lcom/google/api/client/json/JsonGenerator;->flush()V

    .line 52
    const-string v2, "{\"title\":\"foo\"}"

    new-instance v3, Ljava/lang/String;

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/String;-><init>([B)V

    invoke-static {v2, v3}, Lcom/google/api/client/testing/json/AbstractJsonGeneratorTest;->assertEquals(Ljava/lang/String;Ljava/lang/String;)V

    .line 53
    return-void
.end method

.method public final testGenerateFeed()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 56
    new-instance v2, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v2}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 57
    .local v2, "out":Ljava/io/ByteArrayOutputStream;
    invoke-virtual {p0}, Lcom/google/api/client/testing/json/AbstractJsonGeneratorTest;->newFactory()Lcom/google/api/client/json/JsonFactory;

    move-result-object v3

    sget-object v4, Lcom/google/api/client/json/JsonEncoding;->UTF8:Lcom/google/api/client/json/JsonEncoding;

    invoke-virtual {v3, v2, v4}, Lcom/google/api/client/json/JsonFactory;->createJsonGenerator(Ljava/io/OutputStream;Lcom/google/api/client/json/JsonEncoding;)Lcom/google/api/client/json/JsonGenerator;

    move-result-object v1

    .line 58
    .local v1, "generator":Lcom/google/api/client/json/JsonGenerator;
    new-instance v0, Lcom/google/api/client/testing/json/AbstractJsonGeneratorTest$Feed;

    invoke-direct {v0}, Lcom/google/api/client/testing/json/AbstractJsonGeneratorTest$Feed;-><init>()V

    .line 59
    .local v0, "feed":Lcom/google/api/client/testing/json/AbstractJsonGeneratorTest$Feed;
    iget-object v3, v0, Lcom/google/api/client/testing/json/AbstractJsonGeneratorTest$Feed;->entries:Ljava/util/Collection;

    new-instance v4, Lcom/google/api/client/testing/json/AbstractJsonGeneratorTest$Entry;

    const-string v5, "foo"

    invoke-direct {v4, v5}, Lcom/google/api/client/testing/json/AbstractJsonGeneratorTest$Entry;-><init>(Ljava/lang/String;)V

    invoke-interface {v3, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 60
    iget-object v3, v0, Lcom/google/api/client/testing/json/AbstractJsonGeneratorTest$Feed;->entries:Ljava/util/Collection;

    new-instance v4, Lcom/google/api/client/testing/json/AbstractJsonGeneratorTest$Entry;

    const-string v5, "bar"

    invoke-direct {v4, v5}, Lcom/google/api/client/testing/json/AbstractJsonGeneratorTest$Entry;-><init>(Ljava/lang/String;)V

    invoke-interface {v3, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 61
    invoke-virtual {v1, v0}, Lcom/google/api/client/json/JsonGenerator;->serialize(Ljava/lang/Object;)V

    .line 62
    invoke-virtual {v1}, Lcom/google/api/client/json/JsonGenerator;->flush()V

    .line 63
    const-string v3, "{\"entries\":[{\"title\":\"foo\"},{\"title\":\"bar\"}]}"

    new-instance v4, Ljava/lang/String;

    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/String;-><init>([B)V

    invoke-static {v3, v4}, Lcom/google/api/client/testing/json/AbstractJsonGeneratorTest;->assertEquals(Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    return-void
.end method
