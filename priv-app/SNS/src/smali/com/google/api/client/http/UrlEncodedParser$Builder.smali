.class public Lcom/google/api/client/http/UrlEncodedParser$Builder;
.super Ljava/lang/Object;
.source "UrlEncodedParser.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/api/client/http/UrlEncodedParser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private contentType:Ljava/lang/String;

.field private disableContentLogging:Z


# direct methods
.method protected constructor <init>()V
    .locals 1

    .prologue
    .line 262
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 260
    const-string v0, "application/x-www-form-urlencoded"

    iput-object v0, p0, Lcom/google/api/client/http/UrlEncodedParser$Builder;->contentType:Ljava/lang/String;

    .line 263
    return-void
.end method


# virtual methods
.method public build()Lcom/google/api/client/http/UrlEncodedParser;
    .locals 3

    .prologue
    .line 267
    new-instance v0, Lcom/google/api/client/http/UrlEncodedParser;

    iget-object v1, p0, Lcom/google/api/client/http/UrlEncodedParser$Builder;->contentType:Ljava/lang/String;

    iget-boolean v2, p0, Lcom/google/api/client/http/UrlEncodedParser$Builder;->disableContentLogging:Z

    invoke-direct {v0, v1, v2}, Lcom/google/api/client/http/UrlEncodedParser;-><init>(Ljava/lang/String;Z)V

    return-object v0
.end method

.method public final getContentType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 272
    iget-object v0, p0, Lcom/google/api/client/http/UrlEncodedParser$Builder;->contentType:Ljava/lang/String;

    return-object v0
.end method

.method public final getDisableContentLogging()Z
    .locals 1

    .prologue
    .line 297
    iget-boolean v0, p0, Lcom/google/api/client/http/UrlEncodedParser$Builder;->disableContentLogging:Z

    return v0
.end method

.method public setContentType(Ljava/lang/String;)Lcom/google/api/client/http/UrlEncodedParser$Builder;
    .locals 1
    .param p1, "contentType"    # Ljava/lang/String;

    .prologue
    .line 283
    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/api/client/http/UrlEncodedParser$Builder;->contentType:Ljava/lang/String;

    .line 284
    return-object p0
.end method

.method public setDisableContentLogging(Z)Lcom/google/api/client/http/UrlEncodedParser$Builder;
    .locals 0
    .param p1, "disableContentLogging"    # Z

    .prologue
    .line 310
    iput-boolean p1, p0, Lcom/google/api/client/http/UrlEncodedParser$Builder;->disableContentLogging:Z

    .line 311
    return-object p0
.end method
