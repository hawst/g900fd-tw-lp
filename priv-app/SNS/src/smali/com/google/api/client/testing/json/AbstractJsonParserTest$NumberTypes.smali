.class public Lcom/google/api/client/testing/json/AbstractJsonParserTest$NumberTypes;
.super Ljava/lang/Object;
.source "AbstractJsonParserTest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/api/client/testing/json/AbstractJsonParserTest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "NumberTypes"
.end annotation


# instance fields
.field anotherBigDecimalValue:Ljava/math/BigDecimal;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "yetAnotherBigDecimalValue"
    .end annotation
.end field

.field bigDecimalValue:Ljava/math/BigDecimal;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field

.field bigIntegerValue:Ljava/math/BigInteger;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field

.field byteObjValue:Ljava/lang/Byte;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field

.field byteValue:B
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field

.field doubleObjValue:Ljava/lang/Double;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field

.field doubleValue:D
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field

.field floatObjValue:Ljava/lang/Float;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field

.field floatValue:F
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field

.field intObjValue:Ljava/lang/Integer;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field

.field intValue:I
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field

.field longObjValue:Ljava/lang/Long;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field

.field longValue:J
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field

.field shortObjValue:Ljava/lang/Short;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field

.field shortValue:S
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 276
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
