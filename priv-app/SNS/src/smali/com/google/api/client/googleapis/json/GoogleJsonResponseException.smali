.class public Lcom/google/api/client/googleapis/json/GoogleJsonResponseException;
.super Lcom/google/api/client/http/HttpResponseException;
.source "GoogleJsonResponseException.java"


# static fields
.field static final serialVersionUID:J = 0x1L


# instance fields
.field private final details:Lcom/google/api/client/googleapis/json/GoogleJsonError;

.field private final jsonFactory:Lcom/google/api/client/json/JsonFactory;


# direct methods
.method private constructor <init>(Lcom/google/api/client/json/JsonFactory;Lcom/google/api/client/http/HttpResponse;Lcom/google/api/client/googleapis/json/GoogleJsonError;Ljava/lang/String;)V
    .locals 0
    .param p1, "jsonFactory"    # Lcom/google/api/client/json/JsonFactory;
    .param p2, "response"    # Lcom/google/api/client/http/HttpResponse;
    .param p3, "details"    # Lcom/google/api/client/googleapis/json/GoogleJsonError;
    .param p4, "message"    # Ljava/lang/String;

    .prologue
    .line 60
    invoke-direct {p0, p2, p4}, Lcom/google/api/client/http/HttpResponseException;-><init>(Lcom/google/api/client/http/HttpResponse;Ljava/lang/String;)V

    .line 61
    iput-object p1, p0, Lcom/google/api/client/googleapis/json/GoogleJsonResponseException;->jsonFactory:Lcom/google/api/client/json/JsonFactory;

    .line 62
    iput-object p3, p0, Lcom/google/api/client/googleapis/json/GoogleJsonResponseException;->details:Lcom/google/api/client/googleapis/json/GoogleJsonError;

    .line 63
    return-void
.end method

.method public static from(Lcom/google/api/client/json/JsonFactory;Lcom/google/api/client/http/HttpResponse;)Lcom/google/api/client/googleapis/json/GoogleJsonResponseException;
    .locals 9
    .param p0, "jsonFactory"    # Lcom/google/api/client/json/JsonFactory;
    .param p1, "response"    # Lcom/google/api/client/http/HttpResponse;

    .prologue
    .line 95
    invoke-static {p0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 96
    const/4 v3, 0x0

    .line 97
    .local v3, "details":Lcom/google/api/client/googleapis/json/GoogleJsonError;
    invoke-virtual {p1}, Lcom/google/api/client/http/HttpResponse;->getContentType()Ljava/lang/String;

    move-result-object v1

    .line 98
    .local v1, "contentType":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/google/api/client/http/HttpResponse;->isSuccessStatusCode()Z

    move-result v7

    if-nez v7, :cond_2

    if-eqz v1, :cond_2

    const-string v7, "application/json"

    invoke-virtual {v1, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 100
    const/4 v6, 0x0

    .line 102
    .local v6, "parser":Lcom/google/api/client/json/JsonParser;
    :try_start_0
    invoke-static {p0, p1}, Lcom/google/api/client/http/json/JsonHttpParser;->parserForResponse(Lcom/google/api/client/json/JsonFactory;Lcom/google/api/client/http/HttpResponse;)Lcom/google/api/client/json/JsonParser;

    move-result-object v6

    .line 103
    invoke-virtual {v6}, Lcom/google/api/client/json/JsonParser;->getCurrentToken()Lcom/google/api/client/json/JsonToken;

    move-result-object v2

    .line 105
    .local v2, "currentToken":Lcom/google/api/client/json/JsonToken;
    if-nez v2, :cond_0

    .line 106
    invoke-virtual {v6}, Lcom/google/api/client/json/JsonParser;->nextToken()Lcom/google/api/client/json/JsonToken;

    move-result-object v2

    .line 109
    :cond_0
    if-eqz v2, :cond_1

    .line 111
    const-string v7, "error"

    invoke-virtual {v6, v7}, Lcom/google/api/client/json/JsonParser;->skipToKey(Ljava/lang/String;)V

    .line 112
    invoke-virtual {v6}, Lcom/google/api/client/json/JsonParser;->getCurrentToken()Lcom/google/api/client/json/JsonToken;

    move-result-object v7

    sget-object v8, Lcom/google/api/client/json/JsonToken;->END_OBJECT:Lcom/google/api/client/json/JsonToken;

    if-eq v7, v8, :cond_1

    .line 113
    const-class v7, Lcom/google/api/client/googleapis/json/GoogleJsonError;

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, Lcom/google/api/client/json/JsonParser;->parseAndClose(Ljava/lang/Class;Lcom/google/api/client/json/CustomizeJsonParser;)Ljava/lang/Object;

    move-result-object v7

    move-object v0, v7

    check-cast v0, Lcom/google/api/client/googleapis/json/GoogleJsonError;

    move-object v3, v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 121
    :cond_1
    if-nez v6, :cond_4

    .line 122
    :try_start_1
    invoke-virtual {p1}, Lcom/google/api/client/http/HttpResponse;->getContent()Ljava/io/InputStream;

    move-result-object v7

    invoke-virtual {v7}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 133
    .end local v2    # "currentToken":Lcom/google/api/client/json/JsonToken;
    .end local v6    # "parser":Lcom/google/api/client/json/JsonParser;
    :cond_2
    :goto_0
    invoke-static {p1}, Lcom/google/api/client/googleapis/json/GoogleJsonResponseException;->computeMessage(Lcom/google/api/client/http/HttpResponse;)Ljava/lang/String;

    move-result-object v5

    .line 134
    .local v5, "message":Ljava/lang/String;
    if-eqz v3, :cond_3

    .line 135
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Lcom/google/api/client/util/Strings;->LINE_SEPARATOR:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v3}, Lcom/google/api/client/googleapis/json/GoogleJsonError;->toPrettyString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 138
    :cond_3
    new-instance v7, Lcom/google/api/client/googleapis/json/GoogleJsonResponseException;

    invoke-direct {v7, p0, p1, v3, v5}, Lcom/google/api/client/googleapis/json/GoogleJsonResponseException;-><init>(Lcom/google/api/client/json/JsonFactory;Lcom/google/api/client/http/HttpResponse;Lcom/google/api/client/googleapis/json/GoogleJsonError;Ljava/lang/String;)V

    return-object v7

    .line 123
    .end local v5    # "message":Ljava/lang/String;
    .restart local v2    # "currentToken":Lcom/google/api/client/json/JsonToken;
    .restart local v6    # "parser":Lcom/google/api/client/json/JsonParser;
    :cond_4
    if-nez v3, :cond_2

    .line 124
    :try_start_2
    invoke-virtual {v6}, Lcom/google/api/client/json/JsonParser;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 126
    :catch_0
    move-exception v4

    .line 128
    .local v4, "exception":Ljava/io/IOException;
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 116
    .end local v2    # "currentToken":Lcom/google/api/client/json/JsonToken;
    .end local v4    # "exception":Ljava/io/IOException;
    :catch_1
    move-exception v4

    .line 118
    .restart local v4    # "exception":Ljava/io/IOException;
    :try_start_3
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 121
    if-nez v6, :cond_5

    .line 122
    :try_start_4
    invoke-virtual {p1}, Lcom/google/api/client/http/HttpResponse;->getContent()Ljava/io/InputStream;

    move-result-object v7

    invoke-virtual {v7}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 126
    :catch_2
    move-exception v4

    .line 128
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 123
    :cond_5
    if-nez v3, :cond_2

    .line 124
    :try_start_5
    invoke-virtual {v6}, Lcom/google/api/client/json/JsonParser;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    goto :goto_0

    .line 120
    .end local v4    # "exception":Ljava/io/IOException;
    :catchall_0
    move-exception v7

    .line 121
    if-nez v6, :cond_7

    .line 122
    :try_start_6
    invoke-virtual {p1}, Lcom/google/api/client/http/HttpResponse;->getContent()Ljava/io/InputStream;

    move-result-object v8

    invoke-virtual {v8}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    .line 129
    :cond_6
    :goto_1
    throw v7

    .line 123
    :cond_7
    if-nez v3, :cond_6

    .line 124
    :try_start_7
    invoke-virtual {v6}, Lcom/google/api/client/json/JsonParser;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_3

    goto :goto_1

    .line 126
    :catch_3
    move-exception v4

    .line 128
    .restart local v4    # "exception":Ljava/io/IOException;
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1
.end method


# virtual methods
.method public final getDetails()Lcom/google/api/client/googleapis/json/GoogleJsonError;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/api/client/googleapis/json/GoogleJsonResponseException;->details:Lcom/google/api/client/googleapis/json/GoogleJsonError;

    return-object v0
.end method

.method public final getJsonFactory()Lcom/google/api/client/json/JsonFactory;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/api/client/googleapis/json/GoogleJsonResponseException;->jsonFactory:Lcom/google/api/client/json/JsonFactory;

    return-object v0
.end method
