.class public Lcom/google/api/client/googleapis/GoogleUrl;
.super Lcom/google/api/client/http/GenericUrl;
.source "GoogleUrl.java"


# instance fields
.field public alt:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field

.field public fields:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field

.field public key:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field

.field public prettyprint:Ljava/lang/Boolean;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field

.field public userip:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 70
    invoke-direct {p0}, Lcom/google/api/client/http/GenericUrl;-><init>()V

    .line 71
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "encodedUrl"    # Ljava/lang/String;

    .prologue
    .line 77
    invoke-direct {p0, p1}, Lcom/google/api/client/http/GenericUrl;-><init>(Ljava/lang/String;)V

    .line 78
    return-void
.end method

.method public static create(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/client/googleapis/GoogleUrl;
    .locals 6
    .param p0, "encodedServerUrl"    # Ljava/lang/String;
    .param p1, "pathTemplate"    # Ljava/lang/String;
    .param p2, "parameters"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 101
    new-instance v3, Lcom/google/api/client/googleapis/GoogleUrl;

    invoke-direct {v3, p0}, Lcom/google/api/client/googleapis/GoogleUrl;-><init>(Ljava/lang/String;)V

    .line 103
    .local v3, "url":Lcom/google/api/client/googleapis/GoogleUrl;
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 104
    .local v2, "requestMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    invoke-static {p2}, Lcom/google/api/client/util/Data;->mapOf(Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 105
    .local v0, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Object;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    .line 106
    .local v4, "value":Ljava/lang/Object;
    if-eqz v4, :cond_0

    .line 107
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v2, v5, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 110
    .end local v0    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Object;>;"
    .end local v4    # "value":Ljava/lang/Object;
    :cond_1
    invoke-static {p1, v2}, Lcom/google/api/client/googleapis/GoogleUrl;->expandUriTemplates(Ljava/lang/String;Ljava/util/HashMap;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/google/api/client/googleapis/GoogleUrl;->appendRawPath(Ljava/lang/String;)V

    .line 112
    invoke-virtual {v3, v2}, Lcom/google/api/client/googleapis/GoogleUrl;->putAll(Ljava/util/Map;)V

    .line 113
    return-object v3
.end method

.method static expandUriTemplates(Ljava/lang/String;Ljava/util/HashMap;)Ljava/lang/String;
    .locals 12
    .param p0, "pathUri"    # Ljava/lang/String;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .local p1, "variableMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 130
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 131
    .local v4, "pathBuf":Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    .line 132
    .local v1, "cur":I
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    .line 133
    .local v2, "length":I
    :goto_0
    if-ge v1, v2, :cond_0

    .line 134
    const/16 v7, 0x7b

    invoke-virtual {p0, v7, v1}, Ljava/lang/String;->indexOf(II)I

    move-result v3

    .line 135
    .local v3, "next":I
    const/4 v7, -0x1

    if-ne v3, v7, :cond_1

    .line 136
    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 149
    .end local v3    # "next":I
    :cond_0
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    return-object v7

    .line 139
    .restart local v3    # "next":I
    :cond_1
    invoke-virtual {p0, v1, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 140
    const/16 v7, 0x7d

    add-int/lit8 v10, v3, 0x2

    invoke-virtual {p0, v7, v10}, Ljava/lang/String;->indexOf(II)I

    move-result v0

    .line 141
    .local v0, "close":I
    add-int/lit8 v7, v3, 0x1

    invoke-virtual {p0, v7, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    .line 142
    .local v6, "varName":Ljava/lang/String;
    add-int/lit8 v1, v0, 0x1

    .line 143
    if-eqz p1, :cond_2

    move v7, v8

    :goto_1
    const-string v10, "no variable map supplied for parameterize path: %s"

    new-array v11, v8, [Ljava/lang/Object;

    aput-object v6, v11, v9

    invoke-static {v7, v10, v11}, Lcom/google/common/base/Preconditions;->checkArgument(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 145
    invoke-virtual {p1, v6}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    .line 146
    .local v5, "value":Ljava/lang/Object;
    if-eqz v5, :cond_3

    move v7, v8

    :goto_2
    const-string v10, "missing required path parameter: %s"

    new-array v11, v8, [Ljava/lang/Object;

    aput-object v6, v11, v9

    invoke-static {v7, v10, v11}, Lcom/google/common/base/Preconditions;->checkArgument(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 147
    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/google/api/client/util/escape/CharEscapers;->escapeUriPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .end local v5    # "value":Ljava/lang/Object;
    :cond_2
    move v7, v9

    .line 143
    goto :goto_1

    .restart local v5    # "value":Ljava/lang/Object;
    :cond_3
    move v7, v9

    .line 146
    goto :goto_2
.end method


# virtual methods
.method public clone()Lcom/google/api/client/googleapis/GoogleUrl;
    .locals 1

    .prologue
    .line 82
    invoke-super {p0}, Lcom/google/api/client/http/GenericUrl;->clone()Lcom/google/api/client/http/GenericUrl;

    move-result-object v0

    check-cast v0, Lcom/google/api/client/googleapis/GoogleUrl;

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/api/client/http/GenericUrl;
    .locals 1

    .prologue
    .line 35
    invoke-virtual {p0}, Lcom/google/api/client/googleapis/GoogleUrl;->clone()Lcom/google/api/client/googleapis/GoogleUrl;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/api/client/util/GenericData;
    .locals 1

    .prologue
    .line 35
    invoke-virtual {p0}, Lcom/google/api/client/googleapis/GoogleUrl;->clone()Lcom/google/api/client/googleapis/GoogleUrl;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 35
    invoke-virtual {p0}, Lcom/google/api/client/googleapis/GoogleUrl;->clone()Lcom/google/api/client/googleapis/GoogleUrl;

    move-result-object v0

    return-object v0
.end method
