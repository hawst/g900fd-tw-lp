.class public Lcom/google/api/client/auth/RsaSha;
.super Ljava/lang/Object;
.source "RsaSha.java"


# static fields
.field private static final BEGIN:Ljava/lang/String; = "-----BEGIN PRIVATE KEY-----"

.field private static final END:Ljava/lang/String; = "-----END PRIVATE KEY-----"


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    return-void
.end method

.method public static getPrivateKeyFromKeystore(Ljava/io/InputStream;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/security/PrivateKey;
    .locals 2
    .param p0, "keyStream"    # Ljava/io/InputStream;
    .param p1, "storePass"    # Ljava/lang/String;
    .param p2, "alias"    # Ljava/lang/String;
    .param p3, "keyPass"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/security/GeneralSecurityException;
        }
    .end annotation

    .prologue
    .line 62
    invoke-static {}, Ljava/security/KeyStore;->getDefaultType()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/security/KeyStore;->getInstance(Ljava/lang/String;)Ljava/security/KeyStore;

    move-result-object v0

    .line 64
    .local v0, "keyStore":Ljava/security/KeyStore;
    :try_start_0
    invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Ljava/security/KeyStore;->load(Ljava/io/InputStream;[C)V

    .line 65
    invoke-virtual {p3}, Ljava/lang/String;->toCharArray()[C

    move-result-object v1

    invoke-virtual {v0, p2, v1}, Ljava/security/KeyStore;->getKey(Ljava/lang/String;[C)Ljava/security/Key;

    move-result-object v1

    check-cast v1, Ljava/security/PrivateKey;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 67
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V

    return-object v1

    :catchall_0
    move-exception v1

    invoke-virtual {p0}, Ljava/io/InputStream;->close()V

    throw v1
.end method

.method public static getPrivateKeyFromPk8(Ljava/io/File;)Ljava/security/PrivateKey;
    .locals 8
    .param p0, "file"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/security/GeneralSecurityException;
        }
    .end annotation

    .prologue
    .line 78
    invoke-virtual {p0}, Ljava/io/File;->length()J

    move-result-wide v6

    long-to-int v5, v6

    new-array v2, v5, [B

    .line 79
    .local v2, "privKeyBytes":[B
    new-instance v1, Ljava/io/DataInputStream;

    new-instance v5, Ljava/io/FileInputStream;

    invoke-direct {v5, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v1, v5}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 81
    .local v1, "inputStream":Ljava/io/DataInputStream;
    :try_start_0
    invoke-virtual {v1, v2}, Ljava/io/DataInputStream;->readFully([B)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 83
    invoke-virtual {v1}, Ljava/io/DataInputStream;->close()V

    .line 85
    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, v2}, Ljava/lang/String;-><init>([B)V

    .line 86
    .local v4, "str":Ljava/lang/String;
    const-string v5, "-----BEGIN PRIVATE KEY-----"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    const-string v5, "-----END PRIVATE KEY-----"

    invoke-virtual {v4, v5}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 87
    const-string v5, "-----BEGIN PRIVATE KEY-----"

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    const-string v6, "-----END PRIVATE KEY-----"

    invoke-virtual {v4, v6}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 89
    :cond_0
    const-string v5, "RSA"

    invoke-static {v5}, Ljava/security/KeyFactory;->getInstance(Ljava/lang/String;)Ljava/security/KeyFactory;

    move-result-object v0

    .line 90
    .local v0, "fac":Ljava/security/KeyFactory;
    new-instance v3, Ljava/security/spec/PKCS8EncodedKeySpec;

    invoke-static {v4}, Lcom/google/api/client/util/Strings;->toBytesUtf8(Ljava/lang/String;)[B

    move-result-object v5

    invoke-static {v5}, Lcom/google/api/client/util/Base64;->decode([B)[B

    move-result-object v5

    invoke-direct {v3, v5}, Ljava/security/spec/PKCS8EncodedKeySpec;-><init>([B)V

    .line 91
    .local v3, "privKeySpec":Ljava/security/spec/EncodedKeySpec;
    invoke-virtual {v0, v3}, Ljava/security/KeyFactory;->generatePrivate(Ljava/security/spec/KeySpec;)Ljava/security/PrivateKey;

    move-result-object v5

    return-object v5

    .line 83
    .end local v0    # "fac":Ljava/security/KeyFactory;
    .end local v3    # "privKeySpec":Ljava/security/spec/EncodedKeySpec;
    .end local v4    # "str":Ljava/lang/String;
    :catchall_0
    move-exception v5

    invoke-virtual {v1}, Ljava/io/DataInputStream;->close()V

    throw v5
.end method

.method public static sign(Ljava/security/PrivateKey;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "privateKey"    # Ljava/security/PrivateKey;
    .param p1, "data"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/GeneralSecurityException;
        }
    .end annotation

    .prologue
    .line 100
    const-string v1, "SHA1withRSA"

    invoke-static {v1}, Ljava/security/Signature;->getInstance(Ljava/lang/String;)Ljava/security/Signature;

    move-result-object v0

    .line 101
    .local v0, "signature":Ljava/security/Signature;
    invoke-virtual {v0, p0}, Ljava/security/Signature;->initSign(Ljava/security/PrivateKey;)V

    .line 102
    invoke-static {p1}, Lcom/google/api/client/util/Strings;->toBytesUtf8(Ljava/lang/String;)[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/security/Signature;->update([B)V

    .line 103
    invoke-virtual {v0}, Ljava/security/Signature;->sign()[B

    move-result-object v1

    invoke-static {v1}, Lcom/google/api/client/util/Base64;->encode([B)[B

    move-result-object v1

    invoke-static {v1}, Lcom/google/api/client/util/Strings;->fromBytesUtf8([B)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
