.class public abstract Lcom/google/api/client/http/AbstractInputStreamContent;
.super Ljava/lang/Object;
.source "AbstractInputStreamContent.java"

# interfaces
.implements Lcom/google/api/client/http/HttpContent;


# static fields
.field private static final BUFFER_SIZE:I = 0x800


# instance fields
.field private encoding:Ljava/lang/String;

.field private type:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "type"    # Ljava/lang/String;

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    invoke-virtual {p0, p1}, Lcom/google/api/client/http/AbstractInputStreamContent;->setType(Ljava/lang/String;)Lcom/google/api/client/http/AbstractInputStreamContent;

    .line 52
    return-void
.end method

.method public static copy(Ljava/io/InputStream;Ljava/io/OutputStream;)V
    .locals 3
    .param p0, "inputStream"    # Ljava/io/InputStream;
    .param p1, "outputStream"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 141
    const/16 v2, 0x800

    :try_start_0
    new-array v1, v2, [B

    .line 143
    .local v1, "tmp":[B
    :goto_0
    invoke-virtual {p0, v1}, Ljava/io/InputStream;->read([B)I

    move-result v0

    .local v0, "bytesRead":I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 144
    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2, v0}, Ljava/io/OutputStream;->write([BII)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 147
    .end local v0    # "bytesRead":I
    .end local v1    # "tmp":[B
    :catchall_0
    move-exception v2

    invoke-virtual {p0}, Ljava/io/InputStream;->close()V

    throw v2

    .restart local v0    # "bytesRead":I
    .restart local v1    # "tmp":[B
    :cond_0
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V

    .line 149
    return-void
.end method


# virtual methods
.method public getEncoding()Ljava/lang/String;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/google/api/client/http/AbstractInputStreamContent;->encoding:Ljava/lang/String;

    return-object v0
.end method

.method protected abstract getInputStream()Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public getType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/google/api/client/http/AbstractInputStreamContent;->type:Ljava/lang/String;

    return-object v0
.end method

.method public setEncoding(Ljava/lang/String;)Lcom/google/api/client/http/AbstractInputStreamContent;
    .locals 0
    .param p1, "encoding"    # Ljava/lang/String;

    .prologue
    .line 100
    iput-object p1, p0, Lcom/google/api/client/http/AbstractInputStreamContent;->encoding:Ljava/lang/String;

    .line 101
    return-object p0
.end method

.method public setType(Ljava/lang/String;)Lcom/google/api/client/http/AbstractInputStreamContent;
    .locals 0
    .param p1, "type"    # Ljava/lang/String;

    .prologue
    .line 110
    iput-object p1, p0, Lcom/google/api/client/http/AbstractInputStreamContent;->type:Ljava/lang/String;

    .line 111
    return-object p0
.end method

.method public writeTo(Ljava/io/OutputStream;)V
    .locals 12
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v10, 0x0

    .line 63
    invoke-virtual {p0}, Lcom/google/api/client/http/AbstractInputStreamContent;->getInputStream()Ljava/io/InputStream;

    move-result-object v1

    .line 64
    .local v1, "inputStream":Ljava/io/InputStream;
    invoke-virtual {p0}, Lcom/google/api/client/http/AbstractInputStreamContent;->getLength()J

    move-result-wide v2

    .line 65
    .local v2, "contentLength":J
    cmp-long v5, v2, v10

    if-gez v5, :cond_0

    .line 66
    invoke-static {v1, p1}, Lcom/google/api/client/http/AbstractInputStreamContent;->copy(Ljava/io/InputStream;Ljava/io/OutputStream;)V

    .line 84
    :goto_0
    return-void

    .line 68
    :cond_0
    const/16 v5, 0x800

    new-array v0, v5, [B

    .line 71
    .local v0, "buffer":[B
    move-wide v6, v2

    .line 72
    .local v6, "remaining":J
    :goto_1
    cmp-long v5, v6, v10

    if-lez v5, :cond_1

    .line 73
    const/4 v5, 0x0

    const-wide/16 v8, 0x800

    :try_start_0
    invoke-static {v8, v9, v6, v7}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v8

    long-to-int v8, v8

    invoke-virtual {v1, v0, v5, v8}, Ljava/io/InputStream;->read([BII)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v4

    .line 74
    .local v4, "read":I
    const/4 v5, -0x1

    if-ne v4, v5, :cond_2

    .line 81
    .end local v4    # "read":I
    :cond_1
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    goto :goto_0

    .line 77
    .restart local v4    # "read":I
    :cond_2
    const/4 v5, 0x0

    :try_start_1
    invoke-virtual {p1, v0, v5, v4}, Ljava/io/OutputStream;->write([BII)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 78
    int-to-long v8, v4

    sub-long/2addr v6, v8

    .line 79
    goto :goto_1

    .line 81
    .end local v4    # "read":I
    :catchall_0
    move-exception v5

    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    throw v5
.end method
