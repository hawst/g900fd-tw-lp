.class public final Lcom/google/api/client/xml/atom/Atom;
.super Ljava/lang/Object;
.source "Atom.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/api/client/xml/atom/Atom$StopAtAtomEntry;
    }
.end annotation


# static fields
.field public static final ATOM_NAMESPACE:Ljava/lang/String; = "http://www.w3.org/2005/Atom"

.field public static final CONTENT_TYPE:Ljava/lang/String; = "application/atom+xml"


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    return-void
.end method

.method public static checkContentType(Ljava/lang/String;)V
    .locals 4
    .param p0, "contentType"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 51
    if-eqz p0, :cond_0

    const-string v0, "application/atom+xml"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Wrong content type: expected <application/atom+xml> but got <%s>"

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p0, v1, v2

    invoke-static {v0, v3, v1}, Lcom/google/common/base/Preconditions;->checkArgument(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 53
    return-void

    :cond_0
    move v0, v2

    .line 51
    goto :goto_0
.end method
