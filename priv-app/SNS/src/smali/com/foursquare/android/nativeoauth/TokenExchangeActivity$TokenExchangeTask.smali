.class Lcom/foursquare/android/nativeoauth/TokenExchangeActivity$TokenExchangeTask;
.super Landroid/os/AsyncTask;
.source "TokenExchangeActivity.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/foursquare/android/nativeoauth/TokenExchangeActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "TokenExchangeTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Lcom/foursquare/android/nativeoauth/model/AccessTokenResponse;",
        ">;",
        "Ljava/io/Serializable;"
    }
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private mActivity:Lcom/foursquare/android/nativeoauth/TokenExchangeActivity;


# direct methods
.method public constructor <init>(Lcom/foursquare/android/nativeoauth/TokenExchangeActivity;)V
    .locals 0
    .param p1, "activity"    # Lcom/foursquare/android/nativeoauth/TokenExchangeActivity;

    .prologue
    .line 155
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 156
    iput-object p1, p0, Lcom/foursquare/android/nativeoauth/TokenExchangeActivity$TokenExchangeTask;->mActivity:Lcom/foursquare/android/nativeoauth/TokenExchangeActivity;

    .line 157
    return-void
.end method

.method private closeQuietly(Ljava/io/Closeable;)V
    .locals 1
    .param p1, "closeable"    # Ljava/io/Closeable;

    .prologue
    .line 212
    if-eqz p1, :cond_0

    .line 213
    :try_start_0
    invoke-interface {p1}, Ljava/io/Closeable;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 218
    :cond_0
    :goto_0
    return-void

    .line 215
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private createErrorResponse(Ljava/lang/Exception;)Lcom/foursquare/android/nativeoauth/model/AccessTokenResponse;
    .locals 2
    .param p1, "e"    # Ljava/lang/Exception;

    .prologue
    .line 235
    new-instance v0, Lcom/foursquare/android/nativeoauth/model/AccessTokenResponse;

    invoke-direct {v0}, Lcom/foursquare/android/nativeoauth/model/AccessTokenResponse;-><init>()V

    .line 236
    .local v0, "response":Lcom/foursquare/android/nativeoauth/model/AccessTokenResponse;
    new-instance v1, Lcom/foursquare/android/nativeoauth/FoursquareInternalErrorException;

    invoke-direct {v1, p1}, Lcom/foursquare/android/nativeoauth/FoursquareInternalErrorException;-><init>(Ljava/lang/Throwable;)V

    invoke-virtual {v0, v1}, Lcom/foursquare/android/nativeoauth/model/AccessTokenResponse;->setException(Ljava/lang/Exception;)V

    .line 237
    return-object v0
.end method

.method private parseAccessToken(Ljava/lang/String;)Lcom/foursquare/android/nativeoauth/model/AccessTokenResponse;
    .locals 4
    .param p1, "json"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 221
    new-instance v2, Lcom/foursquare/android/nativeoauth/model/AccessTokenResponse;

    invoke-direct {v2}, Lcom/foursquare/android/nativeoauth/model/AccessTokenResponse;-><init>()V

    .line 222
    .local v2, "response":Lcom/foursquare/android/nativeoauth/model/AccessTokenResponse;
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 223
    .local v1, "obj":Lorg/json/JSONObject;
    const-string v3, "error"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 225
    .local v0, "errorCode":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 226
    const-string v3, "access_token"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/foursquare/android/nativeoauth/model/AccessTokenResponse;->setAccessToken(Ljava/lang/String;)V

    .line 231
    :goto_0
    return-object v2

    .line 228
    :cond_0
    new-instance v3, Lcom/foursquare/android/nativeoauth/FoursquareOAuthException;

    invoke-direct {v3, v0}, Lcom/foursquare/android/nativeoauth/FoursquareOAuthException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Lcom/foursquare/android/nativeoauth/model/AccessTokenResponse;->setException(Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method private readStream(Ljava/io/InputStream;)Ljava/lang/String;
    .locals 6
    .param p1, "in"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 198
    new-instance v2, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v2}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 200
    .local v2, "out":Ljava/io/ByteArrayOutputStream;
    const/16 v3, 0x400

    :try_start_0
    new-array v0, v3, [B

    .line 201
    .local v0, "buffer":[B
    :goto_0
    invoke-virtual {p1, v0}, Ljava/io/InputStream;->read([B)I

    move-result v1

    .local v1, "count":I
    const/4 v3, -0x1

    if-ne v1, v3, :cond_0

    .line 204
    new-instance v3, Ljava/lang/String;

    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v4

    const-string v5, "UTF-8"

    invoke-direct {v3, v4, v5}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 206
    invoke-direct {p0, v2}, Lcom/foursquare/android/nativeoauth/TokenExchangeActivity$TokenExchangeTask;->closeQuietly(Ljava/io/Closeable;)V

    .line 204
    return-object v3

    .line 202
    :cond_0
    const/4 v3, 0x0

    :try_start_1
    invoke-virtual {v2, v0, v3, v1}, Ljava/io/ByteArrayOutputStream;->write([BII)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 205
    .end local v0    # "buffer":[B
    .end local v1    # "count":I
    :catchall_0
    move-exception v3

    .line 206
    invoke-direct {p0, v2}, Lcom/foursquare/android/nativeoauth/TokenExchangeActivity$TokenExchangeTask;->closeQuietly(Ljava/io/Closeable;)V

    .line 207
    throw v3
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/String;)Lcom/foursquare/android/nativeoauth/model/AccessTokenResponse;
    .locals 14
    .param p1, "params"    # [Ljava/lang/String;

    .prologue
    const/4 v13, 0x2

    const/4 v12, 0x1

    const/4 v11, 0x0

    .line 165
    const-string v8, "https://foursquare.com/oauth2/access_token?client_id=%s&client_secret=%s&grant_type=authorization_code&code=%s"

    const/4 v9, 0x3

    new-array v9, v9, [Ljava/lang/Object;

    aget-object v10, p1, v11

    aput-object v10, v9, v11

    aget-object v10, p1, v12

    aput-object v10, v9, v12

    aget-object v10, p1, v13

    aput-object v10, v9, v13

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 166
    .local v1, "accessTokenUrl":Ljava/lang/String;
    const/4 v6, 0x0

    .line 167
    .local v6, "result":Lcom/foursquare/android/nativeoauth/model/AccessTokenResponse;
    const/4 v2, 0x0

    .line 170
    .local v2, "connection":Ljava/net/HttpURLConnection;
    :try_start_0
    new-instance v7, Ljava/net/URL;

    invoke-direct {v7, v1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 171
    .local v7, "url":Ljava/net/URL;
    invoke-virtual {v7}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v8

    move-object v0, v8

    check-cast v0, Ljava/net/HttpURLConnection;

    move-object v2, v0

    .line 172
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v4

    .line 173
    .local v4, "in":Ljava/io/InputStream;
    invoke-direct {p0, v4}, Lcom/foursquare/android/nativeoauth/TokenExchangeActivity$TokenExchangeTask;->readStream(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v5

    .line 174
    .local v5, "json":Ljava/lang/String;
    invoke-direct {p0, v5}, Lcom/foursquare/android/nativeoauth/TokenExchangeActivity$TokenExchangeTask;->parseAccessToken(Ljava/lang/String;)Lcom/foursquare/android/nativeoauth/model/AccessTokenResponse;
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v6

    .line 185
    if-eqz v2, :cond_0

    .line 186
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 189
    .end local v4    # "in":Ljava/io/InputStream;
    .end local v5    # "json":Ljava/lang/String;
    .end local v7    # "url":Ljava/net/URL;
    :cond_0
    :goto_0
    return-object v6

    .line 176
    :catch_0
    move-exception v3

    .line 177
    .local v3, "e":Ljava/net/MalformedURLException;
    :try_start_1
    invoke-direct {p0, v3}, Lcom/foursquare/android/nativeoauth/TokenExchangeActivity$TokenExchangeTask;->createErrorResponse(Ljava/lang/Exception;)Lcom/foursquare/android/nativeoauth/model/AccessTokenResponse;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v6

    .line 185
    if-eqz v2, :cond_0

    .line 186
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->disconnect()V

    goto :goto_0

    .line 178
    .end local v3    # "e":Ljava/net/MalformedURLException;
    :catch_1
    move-exception v3

    .line 179
    .local v3, "e":Ljava/io/IOException;
    :try_start_2
    invoke-direct {p0, v3}, Lcom/foursquare/android/nativeoauth/TokenExchangeActivity$TokenExchangeTask;->createErrorResponse(Ljava/lang/Exception;)Lcom/foursquare/android/nativeoauth/model/AccessTokenResponse;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v6

    .line 185
    if-eqz v2, :cond_0

    .line 186
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->disconnect()V

    goto :goto_0

    .line 180
    .end local v3    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v3

    .line 181
    .local v3, "e":Lorg/json/JSONException;
    :try_start_3
    invoke-direct {p0, v3}, Lcom/foursquare/android/nativeoauth/TokenExchangeActivity$TokenExchangeTask;->createErrorResponse(Ljava/lang/Exception;)Lcom/foursquare/android/nativeoauth/model/AccessTokenResponse;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v6

    .line 185
    if-eqz v2, :cond_0

    .line 186
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->disconnect()V

    goto :goto_0

    .line 182
    .end local v3    # "e":Lorg/json/JSONException;
    :catch_3
    move-exception v3

    .line 183
    .local v3, "e":Ljava/lang/Exception;
    :try_start_4
    invoke-direct {p0, v3}, Lcom/foursquare/android/nativeoauth/TokenExchangeActivity$TokenExchangeTask;->createErrorResponse(Ljava/lang/Exception;)Lcom/foursquare/android/nativeoauth/model/AccessTokenResponse;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result-object v6

    .line 185
    if-eqz v2, :cond_0

    .line 186
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->disconnect()V

    goto :goto_0

    .line 184
    .end local v3    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v8

    .line 185
    if-eqz v2, :cond_1

    .line 186
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 188
    :cond_1
    throw v8
.end method

.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/foursquare/android/nativeoauth/TokenExchangeActivity$TokenExchangeTask;->doInBackground([Ljava/lang/String;)Lcom/foursquare/android/nativeoauth/model/AccessTokenResponse;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Lcom/foursquare/android/nativeoauth/model/AccessTokenResponse;)V
    .locals 1
    .param p1, "result"    # Lcom/foursquare/android/nativeoauth/model/AccessTokenResponse;

    .prologue
    .line 194
    iget-object v0, p0, Lcom/foursquare/android/nativeoauth/TokenExchangeActivity$TokenExchangeTask;->mActivity:Lcom/foursquare/android/nativeoauth/TokenExchangeActivity;

    # invokes: Lcom/foursquare/android/nativeoauth/TokenExchangeActivity;->onTokenComplete(Lcom/foursquare/android/nativeoauth/model/AccessTokenResponse;)V
    invoke-static {v0, p1}, Lcom/foursquare/android/nativeoauth/TokenExchangeActivity;->access$0(Lcom/foursquare/android/nativeoauth/TokenExchangeActivity;Lcom/foursquare/android/nativeoauth/model/AccessTokenResponse;)V

    .line 195
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1
    check-cast p1, Lcom/foursquare/android/nativeoauth/model/AccessTokenResponse;

    invoke-virtual {p0, p1}, Lcom/foursquare/android/nativeoauth/TokenExchangeActivity$TokenExchangeTask;->onPostExecute(Lcom/foursquare/android/nativeoauth/model/AccessTokenResponse;)V

    return-void
.end method

.method public setActivity(Lcom/foursquare/android/nativeoauth/TokenExchangeActivity;)V
    .locals 0
    .param p1, "activity"    # Lcom/foursquare/android/nativeoauth/TokenExchangeActivity;

    .prologue
    .line 160
    iput-object p1, p0, Lcom/foursquare/android/nativeoauth/TokenExchangeActivity$TokenExchangeTask;->mActivity:Lcom/foursquare/android/nativeoauth/TokenExchangeActivity;

    .line 161
    return-void
.end method
