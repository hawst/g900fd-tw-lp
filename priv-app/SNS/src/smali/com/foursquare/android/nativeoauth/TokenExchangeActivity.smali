.class public final Lcom/foursquare/android/nativeoauth/TokenExchangeActivity;
.super Landroid/app/Activity;
.source "TokenExchangeActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/foursquare/android/nativeoauth/TokenExchangeActivity$TokenExchangeTask;
    }
.end annotation


# static fields
.field private static final ACCESS_TOKEN_URL:Ljava/lang/String; = "https://foursquare.com/oauth2/access_token?client_id=%s&client_secret=%s&grant_type=authorization_code&code=%s"

.field private static final HTTP_BASE:Ljava/lang/String; = "https://foursquare.com/oauth2/access_token?"

.field public static final INTENT_EXTRA_AUTH_CODE:Ljava/lang/String;

.field public static final INTENT_EXTRA_CLIENT_ID:Ljava/lang/String;

.field public static final INTENT_EXTRA_CLIENT_SECRET:Ljava/lang/String;

.field private static final INTENT_EXTRA_TOKEN_EXCHANGE_TASK:Ljava/lang/String;

.field public static final INTENT_RESULT_RESPONSE:Ljava/lang/String;

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mTask:Lcom/foursquare/android/nativeoauth/TokenExchangeActivity$TokenExchangeTask;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 71
    const-class v0, Lcom/foursquare/android/nativeoauth/TokenExchangeActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/foursquare/android/nativeoauth/TokenExchangeActivity;->TAG:Ljava/lang/String;

    .line 73
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/foursquare/android/nativeoauth/TokenExchangeActivity;->TAG:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, ".INTENT_EXTRA_CLIENT_ID"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/foursquare/android/nativeoauth/TokenExchangeActivity;->INTENT_EXTRA_CLIENT_ID:Ljava/lang/String;

    .line 75
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/foursquare/android/nativeoauth/TokenExchangeActivity;->TAG:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, ".INTENT_EXTRA_CLIENT_SECRET"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/foursquare/android/nativeoauth/TokenExchangeActivity;->INTENT_EXTRA_CLIENT_SECRET:Ljava/lang/String;

    .line 77
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/foursquare/android/nativeoauth/TokenExchangeActivity;->TAG:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, ".INTENT_EXTRA_AUTH_CODE"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/foursquare/android/nativeoauth/TokenExchangeActivity;->INTENT_EXTRA_AUTH_CODE:Ljava/lang/String;

    .line 79
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/foursquare/android/nativeoauth/TokenExchangeActivity;->TAG:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, ".INTENT_RESULT_RESPONSE"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/foursquare/android/nativeoauth/TokenExchangeActivity;->INTENT_RESULT_RESPONSE:Ljava/lang/String;

    .line 81
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/foursquare/android/nativeoauth/TokenExchangeActivity;->TAG:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, ".INTENT_EXTRA_TOKEN_EXCHANGE_TASK"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/foursquare/android/nativeoauth/TokenExchangeActivity;->INTENT_EXTRA_TOKEN_EXCHANGE_TASK:Ljava/lang/String;

    .line 86
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 69
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lcom/foursquare/android/nativeoauth/TokenExchangeActivity;Lcom/foursquare/android/nativeoauth/model/AccessTokenResponse;)V
    .locals 0

    .prologue
    .line 130
    invoke-direct {p0, p1}, Lcom/foursquare/android/nativeoauth/TokenExchangeActivity;->onTokenComplete(Lcom/foursquare/android/nativeoauth/model/AccessTokenResponse;)V

    return-void
.end method

.method private getThemeRes()I
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "InlinedApi"
        }
    .end annotation

    .prologue
    .line 140
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-ge v0, v1, :cond_0

    .line 141
    const v0, 0x103000b

    .line 145
    :goto_0
    return v0

    .line 142
    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-ge v0, v1, :cond_1

    .line 143
    const v0, 0x1030076

    goto :goto_0

    .line 145
    :cond_1
    const v0, 0x1030135

    goto :goto_0
.end method

.method private onTokenComplete(Lcom/foursquare/android/nativeoauth/model/AccessTokenResponse;)V
    .locals 2
    .param p1, "response"    # Lcom/foursquare/android/nativeoauth/model/AccessTokenResponse;

    .prologue
    .line 131
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 132
    .local v0, "data":Landroid/content/Intent;
    sget-object v1, Lcom/foursquare/android/nativeoauth/TokenExchangeActivity;->INTENT_RESULT_RESPONSE:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 134
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/foursquare/android/nativeoauth/TokenExchangeActivity;->setResult(ILandroid/content/Intent;)V

    .line 135
    invoke-virtual {p0}, Lcom/foursquare/android/nativeoauth/TokenExchangeActivity;->finish()V

    .line 136
    return-void
.end method


# virtual methods
.method public onBackPressed()V
    .locals 0

    .prologue
    .line 128
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v7, 0x1

    .line 92
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 93
    invoke-virtual {p0, v7}, Lcom/foursquare/android/nativeoauth/TokenExchangeActivity;->requestWindowFeature(I)Z

    .line 94
    invoke-direct {p0}, Lcom/foursquare/android/nativeoauth/TokenExchangeActivity;->getThemeRes()I

    move-result v4

    invoke-virtual {p0, v4}, Lcom/foursquare/android/nativeoauth/TokenExchangeActivity;->setTheme(I)V

    .line 95
    new-instance v3, Landroid/widget/TextView;

    invoke-direct {v3, p0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 96
    .local v3, "tView":Landroid/widget/TextView;
    const-string v4, "Loading..."

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 97
    invoke-virtual {p0, v3}, Lcom/foursquare/android/nativeoauth/TokenExchangeActivity;->setContentView(Landroid/view/View;)V

    .line 99
    invoke-virtual {p0}, Lcom/foursquare/android/nativeoauth/TokenExchangeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    sget-object v5, Lcom/foursquare/android/nativeoauth/TokenExchangeActivity;->INTENT_EXTRA_CLIENT_ID:Ljava/lang/String;

    invoke-virtual {v4, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 100
    .local v1, "clientId":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/foursquare/android/nativeoauth/TokenExchangeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    sget-object v5, Lcom/foursquare/android/nativeoauth/TokenExchangeActivity;->INTENT_EXTRA_CLIENT_SECRET:Ljava/lang/String;

    invoke-virtual {v4, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 101
    .local v2, "clientSecret":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/foursquare/android/nativeoauth/TokenExchangeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    sget-object v5, Lcom/foursquare/android/nativeoauth/TokenExchangeActivity;->INTENT_EXTRA_AUTH_CODE:Ljava/lang/String;

    invoke-virtual {v4, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 103
    .local v0, "authCode":Ljava/lang/String;
    if-nez p1, :cond_0

    .line 104
    new-instance v4, Lcom/foursquare/android/nativeoauth/TokenExchangeActivity$TokenExchangeTask;

    invoke-direct {v4, p0}, Lcom/foursquare/android/nativeoauth/TokenExchangeActivity$TokenExchangeTask;-><init>(Lcom/foursquare/android/nativeoauth/TokenExchangeActivity;)V

    iput-object v4, p0, Lcom/foursquare/android/nativeoauth/TokenExchangeActivity;->mTask:Lcom/foursquare/android/nativeoauth/TokenExchangeActivity$TokenExchangeTask;

    .line 105
    iget-object v4, p0, Lcom/foursquare/android/nativeoauth/TokenExchangeActivity;->mTask:Lcom/foursquare/android/nativeoauth/TokenExchangeActivity$TokenExchangeTask;

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object v1, v5, v6

    aput-object v2, v5, v7

    const/4 v6, 0x2

    aput-object v0, v5, v6

    invoke-virtual {v4, v5}, Lcom/foursquare/android/nativeoauth/TokenExchangeActivity$TokenExchangeTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 111
    :goto_0
    return-void

    .line 108
    :cond_0
    sget-object v4, Lcom/foursquare/android/nativeoauth/TokenExchangeActivity;->INTENT_EXTRA_TOKEN_EXCHANGE_TASK:Ljava/lang/String;

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v4

    check-cast v4, Lcom/foursquare/android/nativeoauth/TokenExchangeActivity$TokenExchangeTask;

    iput-object v4, p0, Lcom/foursquare/android/nativeoauth/TokenExchangeActivity;->mTask:Lcom/foursquare/android/nativeoauth/TokenExchangeActivity$TokenExchangeTask;

    .line 109
    iget-object v4, p0, Lcom/foursquare/android/nativeoauth/TokenExchangeActivity;->mTask:Lcom/foursquare/android/nativeoauth/TokenExchangeActivity$TokenExchangeTask;

    invoke-virtual {v4, p0}, Lcom/foursquare/android/nativeoauth/TokenExchangeActivity$TokenExchangeTask;->setActivity(Lcom/foursquare/android/nativeoauth/TokenExchangeActivity;)V

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 115
    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 116
    sget-object v0, Lcom/foursquare/android/nativeoauth/TokenExchangeActivity;->INTENT_EXTRA_TOKEN_EXCHANGE_TASK:Ljava/lang/String;

    iget-object v1, p0, Lcom/foursquare/android/nativeoauth/TokenExchangeActivity;->mTask:Lcom/foursquare/android/nativeoauth/TokenExchangeActivity$TokenExchangeTask;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 117
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 122
    const/4 v0, 0x1

    return v0
.end method
