.class public Lcom/foursquare/android/nativeoauth/model/AccessTokenResponse;
.super Ljava/lang/Object;
.source "AccessTokenResponse.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private accessToken:Ljava/lang/String;

.field private exception:Ljava/lang/Exception;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getAccessToken()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/foursquare/android/nativeoauth/model/AccessTokenResponse;->accessToken:Ljava/lang/String;

    return-object v0
.end method

.method public getException()Ljava/lang/Exception;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/foursquare/android/nativeoauth/model/AccessTokenResponse;->exception:Ljava/lang/Exception;

    return-object v0
.end method

.method public setAccessToken(Ljava/lang/String;)V
    .locals 0
    .param p1, "accessToken"    # Ljava/lang/String;

    .prologue
    .line 38
    iput-object p1, p0, Lcom/foursquare/android/nativeoauth/model/AccessTokenResponse;->accessToken:Ljava/lang/String;

    .line 39
    return-void
.end method

.method public setException(Ljava/lang/Exception;)V
    .locals 0
    .param p1, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 46
    iput-object p1, p0, Lcom/foursquare/android/nativeoauth/model/AccessTokenResponse;->exception:Ljava/lang/Exception;

    .line 47
    return-void
.end method
