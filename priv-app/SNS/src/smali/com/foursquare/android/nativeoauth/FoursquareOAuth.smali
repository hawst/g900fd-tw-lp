.class public final Lcom/foursquare/android/nativeoauth/FoursquareOAuth;
.super Ljava/lang/Object;
.source "FoursquareOAuth.java"


# static fields
.field private static final ERROR_CODE_INTERNAL_ERROR:Ljava/lang/String; = "internal_error"

.field private static final ERROR_CODE_INVALID_REQUEST:Ljava/lang/String; = "invalid_request"

.field private static final ERROR_CODE_UNSUPPORTED_VERSION:Ljava/lang/String; = "unsupported_version"

.field private static final INTENT_RESULT_CODE:Ljava/lang/String; = "com.joelapenna.foursquared.fragments.OauthWebviewFragment.INTENT_RESULT_CODE"

.field private static final INTENT_RESULT_DENIED:Ljava/lang/String; = "com.joelapenna.foursquared.fragments.OauthWebviewFragment.INTENT_RESULT_DENIED"

.field private static final INTENT_RESULT_ERROR:Ljava/lang/String; = "com.joelapenna.foursquared.fragments.OauthWebviewFragment.INTENT_RESULT_ERROR"

.field private static final INTENT_RESULT_ERROR_MESSAGE:Ljava/lang/String; = "com.joelapenna.foursquared.fragments.OauthWebviewFragment.INTENT_RESULT_ERROR_MESSAGE"

.field private static final LIB_VERSION:I = 0x1332acd

.field private static final MARKET_REFERRER:Ljava/lang/String; = "utm_source=foursquare-android-oauth&utm_term=%s"

.field private static final PACKAGE:Ljava/lang/String; = "com.joelapenna.foursquared"

.field private static final PARAM_CLIENT_ID:Ljava/lang/String; = "client_id"

.field private static final PARAM_SIGNATURE:Ljava/lang/String; = "androidKeyHash"

.field private static final PARAM_VERSION:Ljava/lang/String; = "v"

.field private static final URI_AUTHORITY:Ljava/lang/String; = "authorize"

.field private static final URI_MARKET_PAGE:Ljava/lang/String; = "market://details?id=com.joelapenna.foursquared"

.field private static final URI_SCHEME:Ljava/lang/String; = "foursquareauth"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getAuthCodeFromResult(ILandroid/content/Intent;)Lcom/foursquare/android/nativeoauth/model/AuthCodeResponse;
    .locals 7
    .param p0, "resultCode"    # I
    .param p1, "data"    # Landroid/content/Intent;

    .prologue
    .line 107
    new-instance v4, Lcom/foursquare/android/nativeoauth/model/AuthCodeResponse;

    invoke-direct {v4}, Lcom/foursquare/android/nativeoauth/model/AuthCodeResponse;-><init>()V

    .line 109
    .local v4, "response":Lcom/foursquare/android/nativeoauth/model/AuthCodeResponse;
    packed-switch p0, :pswitch_data_0

    .line 136
    new-instance v5, Lcom/foursquare/android/nativeoauth/FoursquareCancelException;

    invoke-direct {v5}, Lcom/foursquare/android/nativeoauth/FoursquareCancelException;-><init>()V

    invoke-virtual {v4, v5}, Lcom/foursquare/android/nativeoauth/model/AuthCodeResponse;->setException(Ljava/lang/Exception;)V

    .line 137
    :goto_0
    return-object v4

    .line 111
    :pswitch_0
    const-string v5, "com.joelapenna.foursquared.fragments.OauthWebviewFragment.INTENT_RESULT_DENIED"

    const/4 v6, 0x0

    invoke-virtual {p1, v5, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 112
    .local v1, "denied":Z
    const-string v5, "com.joelapenna.foursquared.fragments.OauthWebviewFragment.INTENT_RESULT_CODE"

    invoke-virtual {p1, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 113
    .local v0, "authCode":Ljava/lang/String;
    const-string v5, "com.joelapenna.foursquared.fragments.OauthWebviewFragment.INTENT_RESULT_ERROR"

    invoke-virtual {p1, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 114
    .local v2, "errorCode":Ljava/lang/String;
    const-string v5, "com.joelapenna.foursquared.fragments.OauthWebviewFragment.INTENT_RESULT_ERROR_MESSAGE"

    invoke-virtual {p1, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 116
    .local v3, "errorMessage":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 117
    new-instance v5, Lcom/foursquare/android/nativeoauth/FoursquareDenyException;

    invoke-direct {v5}, Lcom/foursquare/android/nativeoauth/FoursquareDenyException;-><init>()V

    invoke-virtual {v4, v5}, Lcom/foursquare/android/nativeoauth/model/AuthCodeResponse;->setException(Ljava/lang/Exception;)V

    goto :goto_0

    .line 119
    :cond_0
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 120
    invoke-virtual {v4, v0}, Lcom/foursquare/android/nativeoauth/model/AuthCodeResponse;->setCode(Ljava/lang/String;)V

    goto :goto_0

    .line 121
    :cond_1
    const-string v5, "invalid_request"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 122
    new-instance v5, Lcom/foursquare/android/nativeoauth/FoursquareInvalidRequestException;

    invoke-direct {v5, v3}, Lcom/foursquare/android/nativeoauth/FoursquareInvalidRequestException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v5}, Lcom/foursquare/android/nativeoauth/model/AuthCodeResponse;->setException(Ljava/lang/Exception;)V

    goto :goto_0

    .line 123
    :cond_2
    const-string v5, "unsupported_version"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 124
    new-instance v5, Lcom/foursquare/android/nativeoauth/FoursquareUnsupportedVersionException;

    invoke-direct {v5, v3}, Lcom/foursquare/android/nativeoauth/FoursquareUnsupportedVersionException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v5}, Lcom/foursquare/android/nativeoauth/model/AuthCodeResponse;->setException(Ljava/lang/Exception;)V

    goto :goto_0

    .line 125
    :cond_3
    const-string v5, "internal_error"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 126
    new-instance v5, Lcom/foursquare/android/nativeoauth/FoursquareInternalErrorException;

    invoke-direct {v5, v3}, Lcom/foursquare/android/nativeoauth/FoursquareInternalErrorException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v5}, Lcom/foursquare/android/nativeoauth/model/AuthCodeResponse;->setException(Ljava/lang/Exception;)V

    goto :goto_0

    .line 128
    :cond_4
    new-instance v5, Lcom/foursquare/android/nativeoauth/FoursquareOAuthException;

    invoke-direct {v5, v2}, Lcom/foursquare/android/nativeoauth/FoursquareOAuthException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v5}, Lcom/foursquare/android/nativeoauth/model/AuthCodeResponse;->setException(Ljava/lang/Exception;)V

    goto :goto_0

    .line 109
    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_0
    .end packed-switch
.end method

.method public static getConnectIntent(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "clientId"    # Ljava/lang/String;

    .prologue
    .line 83
    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    .line 84
    .local v0, "builder":Landroid/net/Uri$Builder;
    const-string v2, "foursquareauth"

    invoke-virtual {v0, v2}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 85
    const-string v2, "authorize"

    invoke-virtual {v0, v2}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 86
    const-string v2, "client_id"

    invoke-virtual {v0, v2, p1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 87
    const-string v2, "v"

    const v3, 0x1332acd

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 88
    const-string v2, "KEY"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Is: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p0}, Lcom/foursquare/android/nativeoauth/FoursquareOAuth;->getSignatureFingerprint(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 89
    const-string v2, "androidKeyHash"

    invoke-static {p0}, Lcom/foursquare/android/nativeoauth/FoursquareOAuth;->getSignatureFingerprint(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 91
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 92
    .local v1, "intent":Landroid/content/Intent;
    invoke-static {p0, v1}, Lcom/foursquare/android/nativeoauth/FoursquareOAuth;->isIntentAvailable(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 96
    .end local v1    # "intent":Landroid/content/Intent;
    :goto_0
    return-object v1

    .restart local v1    # "intent":Landroid/content/Intent;
    :cond_0
    invoke-static {p1}, Lcom/foursquare/android/nativeoauth/FoursquareOAuth;->getPlayStoreIntent(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    goto :goto_0
.end method

.method private static getPlayStoreIntent(Ljava/lang/String;)Landroid/content/Intent;
    .locals 5
    .param p0, "clientId"    # Ljava/lang/String;

    .prologue
    .line 207
    const-string v1, "utm_source=foursquare-android-oauth&utm_term=%s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 208
    .local v0, "referrer":Ljava/lang/String;
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    .line 209
    const-string v3, "market://details?id=com.joelapenna.foursquared"

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 210
    invoke-virtual {v3}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v3

    .line 211
    const-string v4, "referrer"

    invoke-virtual {v3, v4, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    .line 212
    invoke-virtual {v3}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    .line 208
    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    return-object v1
.end method

.method private static getSignatureFingerprint(Landroid/content/Context;)Ljava/lang/String;
    .locals 15
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v11, 0x0

    .line 230
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v13

    invoke-virtual {v13}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    .line 231
    .local v1, "callingPackage":Ljava/lang/String;
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v10

    .line 232
    .local v10, "pm":Landroid/content/pm/PackageManager;
    const/16 v6, 0x40

    .line 234
    .local v6, "flags":I
    const/4 v2, 0x0

    .line 236
    .local v2, "callingPackageInfo":Landroid/content/pm/PackageInfo;
    :try_start_0
    invoke-virtual {v10, v1, v6}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 242
    if-eqz v2, :cond_0

    .line 243
    iget-object v12, v2, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    .line 244
    .local v12, "signatures":[Landroid/content/pm/Signature;
    if-eqz v12, :cond_0

    array-length v13, v12

    if-lez v13, :cond_0

    .line 245
    const/4 v13, 0x0

    aget-object v13, v12, v13

    invoke-virtual {v13}, Landroid/content/pm/Signature;->toByteArray()[B

    move-result-object v3

    .line 248
    .local v3, "cert":[B
    :try_start_1
    const-string v13, "SHA1"

    invoke-static {v13}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v9

    .line 249
    .local v9, "md":Ljava/security/MessageDigest;
    invoke-virtual {v9, v3}, Ljava/security/MessageDigest;->digest([B)[B

    move-result-object v5

    .line 251
    .local v5, "fingerprint":[B
    new-instance v7, Ljava/lang/StringBuffer;

    invoke-direct {v7}, Ljava/lang/StringBuffer;-><init>()V

    .line 252
    .local v7, "hexString":Ljava/lang/StringBuffer;
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_0
    array-length v13, v5

    if-lt v8, v13, :cond_1

    .line 261
    invoke-virtual {v7}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;
    :try_end_1
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v11

    .line 269
    .end local v3    # "cert":[B
    .end local v5    # "fingerprint":[B
    .end local v7    # "hexString":Ljava/lang/StringBuffer;
    .end local v8    # "i":I
    .end local v9    # "md":Ljava/security/MessageDigest;
    .end local v12    # "signatures":[Landroid/content/pm/Signature;
    :cond_0
    :goto_1
    return-object v11

    .line 237
    :catch_0
    move-exception v4

    .line 238
    .local v4, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v4}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_1

    .line 253
    .end local v4    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v3    # "cert":[B
    .restart local v5    # "fingerprint":[B
    .restart local v7    # "hexString":Ljava/lang/StringBuffer;
    .restart local v8    # "i":I
    .restart local v9    # "md":Ljava/security/MessageDigest;
    .restart local v12    # "signatures":[Landroid/content/pm/Signature;
    :cond_1
    :try_start_2
    aget-byte v13, v5, v8

    and-int/lit16 v13, v13, 0xff

    invoke-static {v13}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    .line 254
    .local v0, "appendString":Ljava/lang/String;
    invoke-virtual {v7}, Ljava/lang/StringBuffer;->length()I

    move-result v13

    if-lez v13, :cond_2

    .line 255
    const-string v13, ":"

    invoke-virtual {v7, v13}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 257
    :cond_2
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v13

    const/4 v14, 0x1

    if-ne v13, v14, :cond_3

    const-string v13, "0"

    invoke-virtual {v7, v13}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 258
    :cond_3
    invoke-virtual {v7, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
    :try_end_2
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_2 .. :try_end_2} :catch_1

    .line 252
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 264
    .end local v0    # "appendString":Ljava/lang/String;
    .end local v5    # "fingerprint":[B
    .end local v7    # "hexString":Ljava/lang/StringBuffer;
    .end local v8    # "i":I
    .end local v9    # "md":Ljava/security/MessageDigest;
    :catch_1
    move-exception v4

    .line 265
    .local v4, "e":Ljava/security/NoSuchAlgorithmException;
    invoke-virtual {v4}, Ljava/security/NoSuchAlgorithmException;->printStackTrace()V

    goto :goto_1
.end method

.method public static getTokenExchangeIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "clientId"    # Ljava/lang/String;
    .param p2, "clientSecret"    # Ljava/lang/String;
    .param p3, "authCode"    # Ljava/lang/String;

    .prologue
    .line 156
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 157
    .local v0, "intent":Landroid/content/Intent;
    const-class v1, Lcom/foursquare/android/nativeoauth/TokenExchangeActivity;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 158
    sget-object v1, Lcom/foursquare/android/nativeoauth/TokenExchangeActivity;->INTENT_EXTRA_CLIENT_ID:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 159
    sget-object v1, Lcom/foursquare/android/nativeoauth/TokenExchangeActivity;->INTENT_EXTRA_CLIENT_SECRET:Ljava/lang/String;

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 160
    sget-object v1, Lcom/foursquare/android/nativeoauth/TokenExchangeActivity;->INTENT_EXTRA_AUTH_CODE:Ljava/lang/String;

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 161
    return-object v0
.end method

.method public static getTokenFromResult(ILandroid/content/Intent;)Lcom/foursquare/android/nativeoauth/model/AccessTokenResponse;
    .locals 1
    .param p0, "resultCode"    # I
    .param p1, "data"    # Landroid/content/Intent;

    .prologue
    .line 174
    const/4 v0, -0x1

    if-ne p0, v0, :cond_0

    .line 175
    sget-object v0, Lcom/foursquare/android/nativeoauth/TokenExchangeActivity;->INTENT_RESULT_RESPONSE:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/foursquare/android/nativeoauth/model/AccessTokenResponse;

    .line 178
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static isIntentAvailable(Landroid/content/Context;Landroid/content/Intent;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 222
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 224
    .local v0, "packageManager":Landroid/content/pm/PackageManager;
    const/high16 v2, 0x10000

    .line 223
    invoke-virtual {v0, p1, v2}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v1

    .line 226
    .local v1, "resolveInfo":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static isPlayStoreIntent(Landroid/content/Intent;)Z
    .locals 4
    .param p0, "intent"    # Landroid/content/Intent;

    .prologue
    .line 189
    const-string v2, "market://details?id=com.joelapenna.foursquared"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 190
    .local v0, "marketUri":Landroid/net/Uri;
    invoke-virtual {p0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    .line 192
    .local v1, "uri":Landroid/net/Uri;
    if-eqz p0, :cond_0

    .line 193
    const-string v2, "android.intent.action.VIEW"

    invoke-virtual {p0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 194
    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 195
    invoke-virtual {v0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 196
    const-string v2, "id"

    invoke-virtual {v0, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "id"

    invoke-virtual {v1, v3}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 192
    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method
