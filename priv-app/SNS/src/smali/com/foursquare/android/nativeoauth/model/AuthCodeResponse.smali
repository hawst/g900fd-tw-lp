.class public Lcom/foursquare/android/nativeoauth/model/AuthCodeResponse;
.super Ljava/lang/Object;
.source "AuthCodeResponse.java"


# instance fields
.field private code:Ljava/lang/String;

.field private exception:Ljava/lang/Exception;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/foursquare/android/nativeoauth/model/AuthCodeResponse;->code:Ljava/lang/String;

    return-object v0
.end method

.method public getException()Ljava/lang/Exception;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/foursquare/android/nativeoauth/model/AuthCodeResponse;->exception:Ljava/lang/Exception;

    return-object v0
.end method

.method public setCode(Ljava/lang/String;)V
    .locals 0
    .param p1, "code"    # Ljava/lang/String;

    .prologue
    .line 33
    iput-object p1, p0, Lcom/foursquare/android/nativeoauth/model/AuthCodeResponse;->code:Ljava/lang/String;

    .line 34
    return-void
.end method

.method public setException(Ljava/lang/Exception;)V
    .locals 0
    .param p1, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 41
    iput-object p1, p0, Lcom/foursquare/android/nativeoauth/model/AuthCodeResponse;->exception:Ljava/lang/Exception;

    .line 42
    return-void
.end method
