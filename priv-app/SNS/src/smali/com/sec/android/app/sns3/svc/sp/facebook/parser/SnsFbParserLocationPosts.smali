.class public Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserLocationPosts;
.super Ljava/lang/Object;
.source "SnsFbParserLocationPosts.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserLocationPosts$FacebookLocationPost;
    }
.end annotation


# instance fields
.field mLocationPosts:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPosts;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserLocationPosts;->mLocationPosts:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPosts;

    .line 35
    return-void
.end method

.method private parseQuery1(Ljava/lang/String;)V
    .locals 12
    .param p1, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 170
    const/4 v6, 0x0

    .line 173
    .local v6, "jsonObject":Lorg/json/JSONObject;
    :try_start_0
    new-instance v4, Lorg/json/JSONArray;

    invoke-direct {v4, p1}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 175
    .local v4, "ja":Lorg/json/JSONArray;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v11

    if-ge v2, v11, :cond_3

    .line 176
    new-instance v8, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPost;

    invoke-direct {v8}, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPost;-><init>()V

    .line 178
    .local v8, "newLocationPost":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPost;
    invoke-virtual {v4, v2}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v6

    .line 180
    const-string v11, "author_uid"

    invoke-virtual {v6, v11}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    iput-object v11, v8, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPost;->mAuthorUID:Ljava/lang/String;

    .line 183
    const-string v11, "id"

    invoke-virtual {v6, v11}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    iput-object v11, v8, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPost;->mPostID:Ljava/lang/String;

    .line 186
    const-string v11, "page_id"

    invoke-virtual {v6, v11}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    iput-object v11, v8, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPost;->mPageID:Ljava/lang/String;

    .line 189
    const-string v11, "timestamp"

    invoke-virtual {v6, v11}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    iput-object v11, v8, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPost;->mTimestamp:Ljava/lang/String;

    .line 192
    const-string v11, "type"

    invoke-virtual {v6, v11}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    iput-object v11, v8, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPost;->mType:Ljava/lang/String;

    .line 195
    const-string v11, "tagged_uids"

    invoke-virtual {v6, v11}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 198
    .local v10, "taggedUids":Ljava/lang/String;
    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v11

    if-lez v11, :cond_2

    const-string v11, "[]"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v11

    if-nez v11, :cond_2

    .line 200
    new-instance v7, Lorg/json/JSONObject;

    invoke-direct {v7, v10}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 202
    .local v7, "jsonTaggedUidObject":Lorg/json/JSONObject;
    invoke-virtual {v7}, Lorg/json/JSONObject;->names()Lorg/json/JSONArray;

    move-result-object v5

    .line 204
    .local v5, "jaTaggedUidName":Lorg/json/JSONArray;
    const/4 v0, 0x0

    .line 206
    .local v0, "curTaggedUser":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTaggedUser;
    const/4 v3, 0x0

    .local v3, "j":I
    :goto_1
    invoke-virtual {v5}, Lorg/json/JSONArray;->length()I

    move-result v11

    if-ge v3, v11, :cond_2

    .line 208
    new-instance v9, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTaggedUser;

    invoke-direct {v9}, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTaggedUser;-><init>()V

    .line 209
    .local v9, "newTaggedUser":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTaggedUser;
    invoke-virtual {v5, v3}, Lorg/json/JSONArray;->optString(I)Ljava/lang/String;

    move-result-object v11

    iput-object v11, v9, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTaggedUser;->mUID:Ljava/lang/String;

    .line 211
    if-nez v0, :cond_1

    .line 212
    move-object v0, v9

    .line 218
    :goto_2
    iget-object v11, v8, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPost;->mTaggedUser:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTaggedUser;

    if-nez v11, :cond_0

    .line 219
    iput-object v0, v8, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPost;->mTaggedUser:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTaggedUser;

    .line 206
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 214
    :cond_1
    iput-object v9, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTaggedUser;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTaggedUser;

    .line 215
    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTaggedUser;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTaggedUser;

    goto :goto_2

    .line 224
    .end local v0    # "curTaggedUser":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTaggedUser;
    .end local v3    # "j":I
    .end local v5    # "jaTaggedUidName":Lorg/json/JSONArray;
    .end local v7    # "jsonTaggedUidObject":Lorg/json/JSONObject;
    .end local v9    # "newTaggedUser":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTaggedUser;
    :cond_2
    iget-object v11, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserLocationPosts;->mLocationPosts:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPosts;

    iget-object v11, v11, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPosts;->mList:Ljava/util/ArrayList;

    invoke-virtual {v11, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 175
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0

    .line 234
    .end local v2    # "i":I
    .end local v4    # "ja":Lorg/json/JSONArray;
    .end local v8    # "newLocationPost":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPost;
    .end local v10    # "taggedUids":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 235
    .local v1, "e":Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    .line 237
    .end local v1    # "e":Lorg/json/JSONException;
    :cond_3
    return-void
.end method

.method private parseQuery2(Ljava/lang/String;)V
    .locals 8
    .param p1, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 242
    :try_start_0
    new-instance v4, Lorg/json/JSONArray;

    invoke-direct {v4, p1}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 244
    .local v4, "ja":Lorg/json/JSONArray;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v6

    if-ge v2, v6, :cond_2

    .line 248
    invoke-virtual {v4, v2}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v5

    .line 250
    .local v5, "jsonObj":Lorg/json/JSONObject;
    const/4 v3, 0x0

    .local v3, "j":I
    :goto_1
    iget-object v6, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserLocationPosts;->mLocationPosts:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPosts;

    iget-object v6, v6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPosts;->mList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-ge v3, v6, :cond_1

    .line 252
    iget-object v6, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserLocationPosts;->mLocationPosts:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPosts;

    iget-object v6, v6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPosts;->mList:Ljava/util/ArrayList;

    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPost;

    .line 254
    .local v0, "curLocationPost":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPost;
    const-string v6, "page_id"

    invoke-virtual {v5, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iget-object v7, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPost;->mPageID:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 257
    const-string v6, "name"

    invoke-virtual {v5, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPost;->mPlaceName:Ljava/lang/String;

    .line 259
    const-string v6, "latitude"

    invoke-virtual {v5, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPost;->mLatitude:Ljava/lang/String;

    .line 261
    const-string v6, "longitude"

    invoke-virtual {v5, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPost;->mLongitude:Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 250
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 244
    .end local v0    # "curLocationPost":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPost;
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 268
    .end local v2    # "i":I
    .end local v3    # "j":I
    .end local v4    # "ja":Lorg/json/JSONArray;
    .end local v5    # "jsonObj":Lorg/json/JSONObject;
    :catch_0
    move-exception v1

    .line 269
    .local v1, "e":Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    .line 271
    .end local v1    # "e":Lorg/json/JSONException;
    :cond_2
    return-void
.end method

.method private parseQuery3(Ljava/lang/String;)V
    .locals 9
    .param p1, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 276
    :try_start_0
    new-instance v5, Lorg/json/JSONArray;

    invoke-direct {v5, p1}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 278
    .local v5, "ja":Lorg/json/JSONArray;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-virtual {v5}, Lorg/json/JSONArray;->length()I

    move-result v7

    if-ge v3, v7, :cond_3

    .line 282
    invoke-virtual {v5, v3}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v6

    .line 284
    .local v6, "jsonObj":Lorg/json/JSONObject;
    const/4 v4, 0x0

    .local v4, "j":I
    :goto_1
    iget-object v7, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserLocationPosts;->mLocationPosts:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPosts;

    iget-object v7, v7, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPosts;->mList:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-ge v4, v7, :cond_2

    .line 286
    iget-object v7, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserLocationPosts;->mLocationPosts:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPosts;

    iget-object v7, v7, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPosts;->mList:Ljava/util/ArrayList;

    invoke-virtual {v7, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPost;

    .line 288
    .local v0, "curLocationPost":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPost;
    iget-object v1, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPost;->mTaggedUser:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTaggedUser;

    .line 290
    .local v1, "curTaggedUser":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTaggedUser;
    :goto_2
    if-eqz v1, :cond_1

    .line 292
    const-string v7, "uid"

    invoke-virtual {v6, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iget-object v8, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTaggedUser;->mUID:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 295
    const-string v7, "name"

    invoke-virtual {v6, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTaggedUser;->mUserName:Ljava/lang/String;

    .line 297
    const-string v7, "pic_small"

    invoke-virtual {v6, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTaggedUser;->mUserPicUrl:Ljava/lang/String;

    .line 301
    :cond_0
    iget-object v1, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTaggedUser;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTaggedUser;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 284
    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 278
    .end local v0    # "curLocationPost":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPost;
    .end local v1    # "curTaggedUser":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTaggedUser;
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 328
    .end local v3    # "i":I
    .end local v4    # "j":I
    .end local v5    # "ja":Lorg/json/JSONArray;
    .end local v6    # "jsonObj":Lorg/json/JSONObject;
    :catch_0
    move-exception v2

    .line 329
    .local v2, "e":Lorg/json/JSONException;
    invoke-virtual {v2}, Lorg/json/JSONException;->printStackTrace()V

    .line 331
    .end local v2    # "e":Lorg/json/JSONException;
    :cond_3
    return-void
.end method

.method private parseQuery4(Ljava/lang/String;)V
    .locals 8
    .param p1, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 336
    :try_start_0
    new-instance v4, Lorg/json/JSONArray;

    invoke-direct {v4, p1}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 338
    .local v4, "ja":Lorg/json/JSONArray;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v6

    if-ge v2, v6, :cond_2

    .line 342
    invoke-virtual {v4, v2}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v5

    .line 344
    .local v5, "jsonObj":Lorg/json/JSONObject;
    const/4 v3, 0x0

    .local v3, "j":I
    :goto_1
    iget-object v6, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserLocationPosts;->mLocationPosts:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPosts;

    iget-object v6, v6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPosts;->mList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-ge v3, v6, :cond_1

    .line 346
    iget-object v6, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserLocationPosts;->mLocationPosts:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPosts;

    iget-object v6, v6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPosts;->mList:Ljava/util/ArrayList;

    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPost;

    .line 348
    .local v0, "curLocationPost":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPost;
    const-string v6, "object_id"

    invoke-virtual {v5, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iget-object v7, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPost;->mPostID:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 351
    const-string v6, "src_big"

    invoke-virtual {v5, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPost;->mImageSrc:Ljava/lang/String;

    .line 353
    const-string v6, "caption"

    invoke-virtual {v5, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPost;->mCaption:Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 344
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 338
    .end local v0    # "curLocationPost":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPost;
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 371
    .end local v2    # "i":I
    .end local v3    # "j":I
    .end local v4    # "ja":Lorg/json/JSONArray;
    .end local v5    # "jsonObj":Lorg/json/JSONObject;
    :catch_0
    move-exception v1

    .line 372
    .local v1, "e":Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    .line 374
    .end local v1    # "e":Lorg/json/JSONException;
    :cond_2
    return-void
.end method

.method private parseQuery5(Ljava/lang/String;)V
    .locals 8
    .param p1, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 379
    :try_start_0
    new-instance v4, Lorg/json/JSONArray;

    invoke-direct {v4, p1}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 381
    .local v4, "ja":Lorg/json/JSONArray;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v6

    if-ge v2, v6, :cond_2

    .line 385
    invoke-virtual {v4, v2}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v5

    .line 388
    .local v5, "jsonObj":Lorg/json/JSONObject;
    const/4 v3, 0x0

    .local v3, "j":I
    :goto_1
    iget-object v6, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserLocationPosts;->mLocationPosts:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPosts;

    iget-object v6, v6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPosts;->mList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-ge v3, v6, :cond_1

    .line 390
    iget-object v6, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserLocationPosts;->mLocationPosts:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPosts;

    iget-object v6, v6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPosts;->mList:Ljava/util/ArrayList;

    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPost;

    .line 392
    .local v0, "curLocationPost":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPost;
    const-string v6, "status_id"

    invoke-virtual {v5, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iget-object v7, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPost;->mPostID:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 395
    const-string v6, "message"

    invoke-virtual {v5, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPost;->mCaption:Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 388
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 381
    .end local v0    # "curLocationPost":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPost;
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 411
    .end local v2    # "i":I
    .end local v3    # "j":I
    .end local v4    # "ja":Lorg/json/JSONArray;
    .end local v5    # "jsonObj":Lorg/json/JSONObject;
    :catch_0
    move-exception v1

    .line 412
    .local v1, "e":Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    .line 414
    .end local v1    # "e":Lorg/json/JSONException;
    :cond_2
    return-void
.end method

.method private parseQuery6(Ljava/lang/String;)V
    .locals 8
    .param p1, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 419
    :try_start_0
    new-instance v4, Lorg/json/JSONArray;

    invoke-direct {v4, p1}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 421
    .local v4, "ja":Lorg/json/JSONArray;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v6

    if-ge v2, v6, :cond_2

    .line 425
    invoke-virtual {v4, v2}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v5

    .line 427
    .local v5, "jsonObj":Lorg/json/JSONObject;
    const/4 v3, 0x0

    .local v3, "j":I
    :goto_1
    iget-object v6, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserLocationPosts;->mLocationPosts:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPosts;

    iget-object v6, v6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPosts;->mList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-ge v3, v6, :cond_1

    .line 429
    iget-object v6, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserLocationPosts;->mLocationPosts:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPosts;

    iget-object v6, v6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPosts;->mList:Ljava/util/ArrayList;

    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPost;

    .line 431
    .local v0, "curLocationPost":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPost;
    const-string v6, "checkin_id"

    invoke-virtual {v5, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iget-object v7, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPost;->mPostID:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 434
    const-string v6, "message"

    invoke-virtual {v5, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPost;->mCaption:Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 427
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 421
    .end local v0    # "curLocationPost":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPost;
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 450
    .end local v2    # "i":I
    .end local v3    # "j":I
    .end local v4    # "ja":Lorg/json/JSONArray;
    .end local v5    # "jsonObj":Lorg/json/JSONObject;
    :catch_0
    move-exception v1

    .line 451
    .local v1, "e":Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    .line 453
    .end local v1    # "e":Lorg/json/JSONException;
    :cond_2
    return-void
.end method

.method private parseQuery7(Ljava/lang/String;)V
    .locals 8
    .param p1, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 458
    :try_start_0
    new-instance v4, Lorg/json/JSONArray;

    invoke-direct {v4, p1}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 460
    .local v4, "ja":Lorg/json/JSONArray;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v6

    if-ge v2, v6, :cond_2

    .line 464
    invoke-virtual {v4, v2}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v5

    .line 466
    .local v5, "jsonObj":Lorg/json/JSONObject;
    const/4 v3, 0x0

    .local v3, "j":I
    :goto_1
    iget-object v6, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserLocationPosts;->mLocationPosts:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPosts;

    iget-object v6, v6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPosts;->mList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-ge v3, v6, :cond_1

    .line 468
    iget-object v6, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserLocationPosts;->mLocationPosts:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPosts;

    iget-object v6, v6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPosts;->mList:Ljava/util/ArrayList;

    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPost;

    .line 470
    .local v0, "curLocationPost":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPost;
    const-string v6, "vid"

    invoke-virtual {v5, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iget-object v7, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPost;->mPostID:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 473
    const-string v6, "description "

    invoke-virtual {v5, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPost;->mCaption:Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 466
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 460
    .end local v0    # "curLocationPost":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPost;
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 489
    .end local v2    # "i":I
    .end local v3    # "j":I
    .end local v4    # "ja":Lorg/json/JSONArray;
    .end local v5    # "jsonObj":Lorg/json/JSONObject;
    :catch_0
    move-exception v1

    .line 490
    .local v1, "e":Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    .line 492
    .end local v1    # "e":Lorg/json/JSONException;
    :cond_2
    return-void
.end method


# virtual methods
.method public parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPosts;
    .locals 7
    .param p1, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 126
    new-instance v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPosts;

    invoke-direct {v5}, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPosts;-><init>()V

    iput-object v5, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserLocationPosts;->mLocationPosts:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPosts;

    .line 130
    :try_start_0
    new-instance v2, Lorg/json/JSONArray;

    invoke-direct {v2, p1}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 132
    .local v2, "ja":Lorg/json/JSONArray;
    if-eqz v2, :cond_2

    .line 134
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v5

    if-ge v1, v5, :cond_2

    .line 136
    invoke-virtual {v2, v1}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v5

    const-string v6, "name"

    invoke-virtual {v5, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 138
    .local v3, "queryName":Ljava/lang/String;
    invoke-virtual {v2, v1}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v5

    const-string v6, "fql_result_set"

    invoke-virtual {v5, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 141
    .local v4, "queryObject":Ljava/lang/String;
    const-string v5, "query1"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 142
    invoke-direct {p0, v4}, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserLocationPosts;->parseQuery1(Ljava/lang/String;)V

    .line 134
    :cond_0
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 143
    :cond_1
    const-string v5, "query2"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 144
    invoke-direct {p0, v4}, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserLocationPosts;->parseQuery2(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 159
    .end local v1    # "i":I
    .end local v2    # "ja":Lorg/json/JSONArray;
    .end local v3    # "queryName":Ljava/lang/String;
    .end local v4    # "queryObject":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 160
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    .line 163
    .end local v0    # "e":Lorg/json/JSONException;
    :cond_2
    iget-object v5, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserLocationPosts;->mLocationPosts:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPosts;

    return-object v5

    .line 145
    .restart local v1    # "i":I
    .restart local v2    # "ja":Lorg/json/JSONArray;
    .restart local v3    # "queryName":Ljava/lang/String;
    .restart local v4    # "queryObject":Ljava/lang/String;
    :cond_3
    :try_start_1
    const-string v5, "query3"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 146
    invoke-direct {p0, v4}, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserLocationPosts;->parseQuery3(Ljava/lang/String;)V

    goto :goto_1

    .line 147
    :cond_4
    const-string v5, "query4"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 148
    invoke-direct {p0, v4}, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserLocationPosts;->parseQuery4(Ljava/lang/String;)V

    goto :goto_1

    .line 149
    :cond_5
    const-string v5, "query5"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 150
    invoke-direct {p0, v4}, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserLocationPosts;->parseQuery5(Ljava/lang/String;)V

    goto :goto_1

    .line 151
    :cond_6
    const-string v5, "query6"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 152
    invoke-direct {p0, v4}, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserLocationPosts;->parseQuery6(Ljava/lang/String;)V

    goto :goto_1

    .line 153
    :cond_7
    const-string v5, "query7"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 154
    invoke-direct {p0, v4}, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserLocationPosts;->parseQuery7(Ljava/lang/String;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method
