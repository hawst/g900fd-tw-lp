.class public interface abstract Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserVideoFormat$FacebookVideoFormat;
.super Ljava/lang/Object;
.source "SnsFbParserVideoFormat.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserVideoFormat;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "FacebookVideoFormat"
.end annotation


# static fields
.field public static final EMBED_HTML:Ljava/lang/String; = "embed_html"

.field public static final FILTER:Ljava/lang/String; = "filter"

.field public static final HEIGHT:Ljava/lang/String; = "height"

.field public static final PICTURE:Ljava/lang/String; = "picture"

.field public static final WIDTH:Ljava/lang/String; = "width"
