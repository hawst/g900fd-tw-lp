.class Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity$2;
.super Ljava/lang/Object;
.source "SnsAccountLiSyncIntervalActivity.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;

.field final synthetic val$appAccount:[Landroid/accounts/Account;


# direct methods
.method constructor <init>(Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;[Landroid/accounts/Account;)V
    .locals 0

    .prologue
    .line 201
    iput-object p1, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity$2;->this$0:Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;

    iput-object p2, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity$2;->val$appAccount:[Landroid/accounts/Account;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 7
    .param p1, "preference"    # Landroid/preference/Preference;
    .param p2, "newValue"    # Ljava/lang/Object;

    .prologue
    const/4 v6, 0x0

    .line 208
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity$2;->this$0:Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;

    # getter for: Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->mLinkedinAppIntervalPreference:Landroid/preference/ListPreference;
    invoke-static {v1}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->access$100(Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;)Landroid/preference/ListPreference;

    move-result-object v2

    move-object v1, p2

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v1}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 209
    iget-object v2, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity$2;->this$0:Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;

    move-object v1, p2

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->writeLinkedinAppInterval(Ljava/lang/Long;)V

    .line 211
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity$2;->this$0:Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;

    # getter for: Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->mLinkedinAppIntervalPreference:Landroid/preference/ListPreference;
    invoke-static {v1}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->access$100(Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;)Landroid/preference/ListPreference;

    move-result-object v1

    invoke-virtual {v1}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 212
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity$2;->this$0:Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;

    # getter for: Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->mLinkedinAppIntervalPreference:Landroid/preference/ListPreference;
    invoke-static {v1}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->access$100(Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;)Landroid/preference/ListPreference;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity$2;->this$0:Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;

    # getter for: Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->mLinkedinAppIntervalPreference:Landroid/preference/ListPreference;
    invoke-static {v2}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->access$100(Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;)Landroid/preference/ListPreference;

    move-result-object v2

    invoke-virtual {v2}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 217
    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 219
    .local v0, "extra":Landroid/os/Bundle;
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity$2;->val$appAccount:[Landroid/accounts/Account;

    array-length v1, v1

    if-lez v1, :cond_1

    .line 220
    const-string v1, "0"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 221
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity$2;->val$appAccount:[Landroid/accounts/Account;

    aget-object v1, v1, v6

    const-string v2, "com.linkedin.android"

    invoke-static {v1, v2, v0}, Landroid/content/ContentResolver;->removePeriodicSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 228
    :goto_0
    const-string v1, "SnsAccountLiAuth"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mLinkedinAppIntervalPreference spType : LinkedIn sec :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 231
    :cond_1
    return v6

    .line 224
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity$2;->val$appAccount:[Landroid/accounts/Account;

    aget-object v2, v1, v6

    const-string v3, "com.linkedin.android"

    move-object v1, p2

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    int-to-long v4, v1

    invoke-static {v2, v3, v0, v4, v5}, Landroid/content/ContentResolver;->addPeriodicSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;J)V

    goto :goto_0
.end method
