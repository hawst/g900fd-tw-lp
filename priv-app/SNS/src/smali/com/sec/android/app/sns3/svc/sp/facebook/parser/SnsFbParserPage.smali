.class public Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserPage;
.super Ljava/lang/Object;
.source "SnsFbParserPage.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserPage$FacebookPage;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    return-void
.end method

.method public static parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePage;
    .locals 4
    .param p0, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 50
    new-instance v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePage;

    invoke-direct {v2}, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePage;-><init>()V

    .line 53
    .local v2, "page":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePage;
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 55
    .local v1, "jsonObj":Lorg/json/JSONObject;
    const-string v3, "id"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePage;->mPageID:Ljava/lang/String;

    .line 56
    const-string v3, "name"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePage;->mName:Ljava/lang/String;

    .line 57
    const-string v3, "link"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePage;->mLink:Ljava/lang/String;

    .line 59
    const-string v3, "category"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePage;->mCategory:Ljava/lang/String;

    .line 60
    const-string v3, "likes"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePage;->mLikeCnt:Ljava/lang/String;

    .line 61
    const-string v3, "location"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePage;->mLocation:Ljava/lang/String;

    .line 62
    const-string v3, "phone"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePage;->mPhone:Ljava/lang/String;

    .line 63
    const-string v3, "checkins"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePage;->mCheckinCnt:Ljava/lang/String;

    .line 64
    const-string v3, "access_token"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePage;->mAccessToken:Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 71
    .end local v1    # "jsonObj":Lorg/json/JSONObject;
    :goto_0
    return-object v2

    .line 66
    :catch_0
    move-exception v0

    .line 68
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method
