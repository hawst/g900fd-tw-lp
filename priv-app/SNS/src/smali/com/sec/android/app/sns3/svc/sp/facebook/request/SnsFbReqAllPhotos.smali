.class public abstract Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqAllPhotos;
.super Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqBase;
.source "SnsFbReqAllPhotos.java"

# interfaces
.implements Lcom/sec/android/app/sns3/svc/sp/facebook/callback/ISnsFbReqCbAllPhotos;


# direct methods
.method protected constructor <init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;I)V
    .locals 1
    .param p1, "svcMgr"    # Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    .param p2, "reqCategory"    # I

    .prologue
    .line 24
    const/16 v0, 0x16

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqBase;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;I)V

    .line 25
    return-void
.end method


# virtual methods
.method protected compose()Lcom/sec/android/app/sns3/svc/http/SnsHttpRequest;
    .locals 7

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqAllPhotos;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/SnsSvcMgr;->getTokenMgr()Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;

    move-result-object v0

    const-string v1, "facebook"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;->getToken(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbToken;

    .line 30
    .local v6, "token":Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbToken;
    const-string v2, "GET"

    .line 31
    .local v2, "method":Ljava/lang/String;
    const/4 v3, 0x0

    .line 32
    .local v3, "uri":Ljava/lang/String;
    const/4 v4, 0x0

    .line 33
    .local v4, "header":Landroid/os/Bundle;
    const/4 v5, 0x0

    .line 35
    .local v5, "body":Landroid/os/Bundle;
    const-string v3, "https://graph.facebook.com//fql?q="

    .line 36
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "SELECT pid, src, src_height, src_width, modified, created FROM photo WHERE owner = me()"

    invoke-static {v1}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 39
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&access_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v6}, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbToken;->getAccessToken()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->encodeUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&expires_in="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v6}, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbToken;->getExpires()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->encodeUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 42
    new-instance v0, Lcom/sec/android/app/sns3/svc/http/SnsHttpRequest;

    iget v1, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqAllPhotos;->mReqID:I

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/sns3/svc/http/SnsHttpRequest;-><init>(ILjava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;)V

    return-object v0
.end method

.method protected parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;
    .locals 1
    .param p1, "content"    # Ljava/lang/String;

    .prologue
    .line 55
    invoke-super {p0, p1}, Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqBase;->parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;

    move-result-object v0

    .line 57
    .local v0, "response":Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;
    if-eqz v0, :cond_0

    .line 60
    .end local v0    # "response":Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;
    :goto_0
    return-object v0

    .restart local v0    # "response":Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;
    :cond_0
    invoke-static {p1}, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserAllPhotos;->parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;

    move-result-object v0

    goto :goto_0
.end method

.method protected respond(Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;)Z
    .locals 7
    .param p1, "result"    # Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;

    .prologue
    .line 47
    iget v1, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqAllPhotos;->mReqID:I

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->isSuccess()Z

    move-result v2

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->getHttpstatus()I

    move-result v3

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->getErrorCode()I

    move-result v4

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->getReason()Landroid/os/Bundle;

    move-result-object v5

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->getResponse()Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAllPhotos;

    move-object v0, p0

    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqAllPhotos;->onReqRespond(IZIILandroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAllPhotos;)Z

    .line 50
    const/4 v0, 0x1

    return v0
.end method
