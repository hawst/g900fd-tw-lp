.class public Lcom/sec/android/app/sns3/agent/sp/twitter/db/SnsTwitterDB;
.super Ljava/lang/Object;
.source "SnsTwitterDB.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/agent/sp/twitter/db/SnsTwitterDB$FriendsProfileInfoColumns;,
        Lcom/sec/android/app/sns3/agent/sp/twitter/db/SnsTwitterDB$FriendsProfileInfo;,
        Lcom/sec/android/app/sns3/agent/sp/twitter/db/SnsTwitterDB$StatusStreamColumns;,
        Lcom/sec/android/app/sns3/agent/sp/twitter/db/SnsTwitterDB$StatusStream;,
        Lcom/sec/android/app/sns3/agent/sp/twitter/db/SnsTwitterDB$DetailViewColumns;,
        Lcom/sec/android/app/sns3/agent/sp/twitter/db/SnsTwitterDB$DetailView;,
        Lcom/sec/android/app/sns3/agent/sp/twitter/db/SnsTwitterDB$UserTimelineColumns;,
        Lcom/sec/android/app/sns3/agent/sp/twitter/db/SnsTwitterDB$UserTimeLine;,
        Lcom/sec/android/app/sns3/agent/sp/twitter/db/SnsTwitterDB$TimelineColumns;,
        Lcom/sec/android/app/sns3/agent/sp/twitter/db/SnsTwitterDB$TimeLine;,
        Lcom/sec/android/app/sns3/agent/sp/twitter/db/SnsTwitterDB$UserTwBasicInfoColumns;,
        Lcom/sec/android/app/sns3/agent/sp/twitter/db/SnsTwitterDB$UserTwBasicInfo;,
        Lcom/sec/android/app/sns3/agent/sp/twitter/db/SnsTwitterDB$WIPE_TWITTER_DATA;
    }
.end annotation


# static fields
.field public static final AUTHORITY:Ljava/lang/String; = "com.sec.android.app.sns3.sp.twitter"

.field public static final CONTENT_ITEM_TYPE_BASE:Ljava/lang/String; = "vnd.android.cursor.item/vnd.com.sec.android.app.sns3.sp.twitter"

.field public static final CONTENT_TYPE_BASE:Ljava/lang/String; = "vnd.android.cursor.dir/vnd.com.sec.android.app.sns3.sp.twitter"

.field public static final DATABASE_NAME:Ljava/lang/String; = "snsTwitterDB.db"

.field public static final DATABASE_VERSION:I = 0xd

.field public static final DETAILVIEW_TABLE_NAME:Ljava/lang/String; = "detailview"

.field public static final FRIENDS_PROFILE_TABLE_NAME:Ljava/lang/String; = "friends_profile_info"

.field public static final HOME_AUTHORITY:Ljava/lang/String; = "com.sec.android.app.sns3.home"

.field public static final LIFE_AUTHORITY:Ljava/lang/String; = "com.sec.android.app.sns3.life"

.field public static final PROFILES_AUTHORITY:Ljava/lang/String; = "com.sec.android.app.sns3.profiles"

.field public static final STATUS_STREAM_TABLE_NAME:Ljava/lang/String; = "status_stream"

.field public static final TIMELINE_TABLE_NAME:Ljava/lang/String; = "timeline"

.field public static final USER_BASIC_INFO_TABLE_NAME:Ljava/lang/String; = "user_basic_info"

.field public static final USER_TIMELINE_TABLE_NAME:Ljava/lang/String; = "usertimeline"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 251
    return-void
.end method
