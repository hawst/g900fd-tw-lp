.class public Lcom/sec/android/app/sns3/svc/sp/instagram/parser/SnsInParserProfile;
.super Ljava/lang/Object;
.source "SnsInParserProfile.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/instagram/parser/SnsInParserProfile$InstagramUser;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    return-void
.end method

.method public static parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseProfile;
    .locals 5
    .param p0, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 47
    new-instance v3, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseProfile;

    invoke-direct {v3}, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseProfile;-><init>()V

    .line 50
    .local v3, "user":Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseProfile;
    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 51
    .local v2, "job":Lorg/json/JSONObject;
    const-string v4, "data"

    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/json/JSONObject;

    .line 53
    .local v0, "datObj":Lorg/json/JSONObject;
    const-string v4, "id"

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseProfile;->mUserID:Ljava/lang/String;

    .line 54
    const-string v4, "username"

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseProfile;->mUserName:Ljava/lang/String;

    .line 55
    const-string v4, "full_name"

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseProfile;->mFullName:Ljava/lang/String;

    .line 56
    const-string v4, "bio"

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseProfile;->mBio:Ljava/lang/String;

    .line 57
    const-string v4, "website"

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseProfile;->mWebsite:Ljava/lang/String;

    .line 58
    const-string v4, "profile_picture"

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseProfile;->mImageUrl:Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 64
    .end local v0    # "datObj":Lorg/json/JSONObject;
    .end local v2    # "job":Lorg/json/JSONObject;
    :goto_0
    return-object v3

    .line 60
    :catch_0
    move-exception v1

    .line 62
    .local v1, "e1":Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method
