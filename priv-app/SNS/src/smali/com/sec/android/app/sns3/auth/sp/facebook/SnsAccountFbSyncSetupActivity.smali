.class public Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;
.super Landroid/preference/PreferenceActivity;
.source "SnsAccountFbSyncSetupActivity.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "SnsAccountFbAuth"


# instance fields
.field private mAppContext:Landroid/content/Context;

.field private mCalendarCheckBox:Landroid/preference/CheckBoxPreference;

.field private mGalleryCheckBox:Landroid/preference/CheckBoxPreference;

.field private mHomeCheckBox:Landroid/preference/CheckBoxPreference;

.field private mIsGallerySync:Ljava/lang/String;

.field private mLifeFeedsCheckBox:Landroid/preference/CheckBoxPreference;

.field private mProfilesCheckBox:Landroid/preference/CheckBoxPreference;

.field private mStreamsCheckBox:Landroid/preference/CheckBoxPreference;

.field private mSyncIntervalSetting:Landroid/preference/PreferenceScreen;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 57
    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    .line 59
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->mAppContext:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method isTable()Z
    .locals 2

    .prologue
    .line 336
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit8 v0, v0, 0x3

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    .line 339
    :cond_0
    const/4 v0, 0x1

    .line 341
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onClickDone()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 268
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->mAppContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    const-string v2, "com.sec.android.app.sns3.facebook"

    invoke-virtual {v1, v2}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 270
    .local v0, "account":[Landroid/accounts/Account;
    array-length v1, v0

    if-lez v1, :cond_5

    .line 271
    const-string v1, "true"

    iget-object v2, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->mIsGallerySync:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 272
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->mGalleryCheckBox:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 273
    aget-object v1, v0, v3

    const-string v2, "com.sec.android.gallery3d.sns.contentprovider"

    invoke-static {v1, v2, v4}, Landroid/content/ContentResolver;->setIsSyncable(Landroid/accounts/Account;Ljava/lang/String;I)V

    .line 274
    aget-object v1, v0, v3

    const-string v2, "com.sec.android.gallery3d.sns.contentprovider"

    invoke-static {v1, v2, v4}, Landroid/content/ContentResolver;->setSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;Z)V

    .line 278
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->mCalendarCheckBox:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 279
    aget-object v1, v0, v3

    const-string v2, "com.android.calendar"

    invoke-static {v1, v2, v4}, Landroid/content/ContentResolver;->setIsSyncable(Landroid/accounts/Account;Ljava/lang/String;I)V

    .line 280
    aget-object v1, v0, v3

    const-string v2, "com.android.calendar"

    invoke-static {v1, v2, v4}, Landroid/content/ContentResolver;->setSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;Z)V

    .line 283
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->mHomeCheckBox:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 284
    aget-object v1, v0, v3

    const-string v2, "com.sec.android.app.sns3.home"

    invoke-static {v1, v2, v4}, Landroid/content/ContentResolver;->setIsSyncable(Landroid/accounts/Account;Ljava/lang/String;I)V

    .line 285
    aget-object v1, v0, v3

    const-string v2, "com.sec.android.app.sns3.home"

    invoke-static {v1, v2, v4}, Landroid/content/ContentResolver;->setSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;Z)V

    .line 289
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->mLifeFeedsCheckBox:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 290
    aget-object v1, v0, v3

    const-string v2, "com.sec.android.app.sns3.life"

    invoke-static {v1, v2, v4}, Landroid/content/ContentResolver;->setIsSyncable(Landroid/accounts/Account;Ljava/lang/String;I)V

    .line 291
    aget-object v1, v0, v3

    const-string v2, "com.sec.android.app.sns3.life"

    invoke-static {v1, v2, v4}, Landroid/content/ContentResolver;->setSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;Z)V

    .line 295
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->mProfilesCheckBox:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 296
    aget-object v1, v0, v3

    const-string v2, "com.sec.android.app.sns3.profiles"

    invoke-static {v1, v2, v4}, Landroid/content/ContentResolver;->setIsSyncable(Landroid/accounts/Account;Ljava/lang/String;I)V

    .line 297
    aget-object v1, v0, v3

    const-string v2, "com.sec.android.app.sns3.profiles"

    invoke-static {v1, v2, v4}, Landroid/content/ContentResolver;->setSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;Z)V

    .line 300
    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->mStreamsCheckBox:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 301
    aget-object v1, v0, v3

    const-string v2, "com.sec.android.app.sns3.streams"

    invoke-static {v1, v2, v4}, Landroid/content/ContentResolver;->setIsSyncable(Landroid/accounts/Account;Ljava/lang/String;I)V

    .line 302
    aget-object v1, v0, v3

    const-string v2, "com.sec.android.app.sns3.streams"

    invoke-static {v1, v2, v4}, Landroid/content/ContentResolver;->setSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;Z)V

    .line 305
    :cond_5
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->finish()V

    .line 306
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 13
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const v12, 0x7f080020

    const v11, 0x7f080058

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 77
    iput-object p0, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->mAppContext:Landroid/content/Context;

    .line 79
    iget-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->mAppContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->isLightTheme(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_18

    const v5, 0x103012b

    :goto_0
    invoke-virtual {p0, v5}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->setTheme(I)V

    .line 81
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 83
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    .line 84
    .local v1, "actionBar":Landroid/app/ActionBar;
    invoke-virtual {v1, v9}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 86
    const/high16 v5, 0x7f030000

    invoke-virtual {p0, v5}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->setContentView(I)V

    .line 87
    const v5, 0x7f040006

    invoke-virtual {p0, v5}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->addPreferencesFromResource(I)V

    .line 89
    const v5, 0x7f080046

    new-array v6, v10, [Ljava/lang/Object;

    invoke-virtual {p0, v12}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-virtual {p0, v5, v6}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 91
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->mAppContext:Landroid/content/Context;

    invoke-virtual {v5, v6}, Landroid/preference/PreferenceManager;->createPreferenceScreen(Landroid/content/Context;)Landroid/preference/PreferenceScreen;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->mSyncIntervalSetting:Landroid/preference/PreferenceScreen;

    .line 92
    iget-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->mSyncIntervalSetting:Landroid/preference/PreferenceScreen;

    const v6, 0x7f080056

    invoke-virtual {v5, v6}, Landroid/preference/PreferenceScreen;->setTitle(I)V

    .line 93
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->mSyncIntervalSetting:Landroid/preference/PreferenceScreen;

    invoke-virtual {v5, v6}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 95
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "content://com.sec.android.gallery3d.sns.contentprovider/support_sns"

    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/content/ContentResolver;->getType(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->mIsGallerySync:Ljava/lang/String;

    .line 98
    iget-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->mAppContext:Landroid/content/Context;

    invoke-static {v5}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v5

    const-string v6, "com.sec.android.app.sns3.facebook"

    invoke-virtual {v5, v6}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 101
    .local v0, "account":[Landroid/accounts/Account;
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/sns3/SnsApplication;->getFeatureMgr()Lcom/sec/android/app/sns3/SnsFeatureManager;

    move-result-object v2

    .line 102
    .local v2, "featureMgr":Lcom/sec/android/app/sns3/SnsFeatureManager;
    const/4 v4, 0x0

    .line 103
    .local v4, "providerLabel":Ljava/lang/CharSequence;
    const/4 v3, 0x0

    .line 106
    .local v3, "providerInfo":Landroid/content/pm/ProviderInfo;
    const/4 v4, 0x0

    .line 107
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    const-string v6, "com.android.calendar"

    invoke-virtual {v5, v6, v9}, Landroid/content/pm/PackageManager;->resolveContentProvider(Ljava/lang/String;I)Landroid/content/pm/ProviderInfo;

    move-result-object v3

    .line 108
    if-eqz v3, :cond_0

    .line 109
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/content/pm/ProviderInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v4

    .line 111
    :cond_0
    if-eqz v4, :cond_1

    invoke-interface {v4}, Ljava/lang/CharSequence;->length()I

    move-result v5

    if-nez v5, :cond_2

    .line 112
    :cond_1
    const-string v5, "SnsAccountFbAuth"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "providerLabel is null - "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 113
    const-string v4, "Calendar"

    .line 115
    :cond_2
    new-instance v5, Landroid/preference/CheckBoxPreference;

    iget-object v6, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->mAppContext:Landroid/content/Context;

    invoke-direct {v5, v6}, Landroid/preference/CheckBoxPreference;-><init>(Landroid/content/Context;)V

    iput-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->mCalendarCheckBox:Landroid/preference/CheckBoxPreference;

    .line 116
    iget-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->mCalendarCheckBox:Landroid/preference/CheckBoxPreference;

    new-array v6, v10, [Ljava/lang/Object;

    aput-object v4, v6, v9

    invoke-virtual {p0, v11, v6}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/preference/CheckBoxPreference;->setTitle(Ljava/lang/CharSequence;)V

    .line 117
    iget-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->mCalendarCheckBox:Landroid/preference/CheckBoxPreference;

    const v6, 0x7f080050

    invoke-virtual {p0, v6}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/preference/CheckBoxPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 118
    iget-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->mCalendarCheckBox:Landroid/preference/CheckBoxPreference;

    const-string v6, "snsaccount_sync_calendar"

    invoke-virtual {v5, v6}, Landroid/preference/CheckBoxPreference;->setKey(Ljava/lang/String;)V

    .line 119
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->mCalendarCheckBox:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v5, v6}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 120
    iget-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->mCalendarCheckBox:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v5, v10}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 123
    const-string v5, "true"

    iget-object v6, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->mIsGallerySync:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_19

    .line 124
    const/4 v4, 0x0

    .line 125
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    const-string v6, "com.sec.android.gallery3d.sns.contentprovider"

    invoke-virtual {v5, v6, v9}, Landroid/content/pm/PackageManager;->resolveContentProvider(Ljava/lang/String;I)Landroid/content/pm/ProviderInfo;

    move-result-object v3

    .line 126
    if-eqz v3, :cond_3

    .line 127
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/content/pm/ProviderInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v4

    .line 129
    :cond_3
    if-eqz v4, :cond_4

    invoke-interface {v4}, Ljava/lang/CharSequence;->length()I

    move-result v5

    if-nez v5, :cond_5

    .line 130
    :cond_4
    const-string v5, "SnsAccountFbAuth"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "providerLabel is null - "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 131
    const-string v4, "Gallery"

    .line 133
    :cond_5
    new-instance v5, Landroid/preference/CheckBoxPreference;

    iget-object v6, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->mAppContext:Landroid/content/Context;

    invoke-direct {v5, v6}, Landroid/preference/CheckBoxPreference;-><init>(Landroid/content/Context;)V

    iput-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->mGalleryCheckBox:Landroid/preference/CheckBoxPreference;

    .line 134
    iget-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->mGalleryCheckBox:Landroid/preference/CheckBoxPreference;

    new-array v6, v10, [Ljava/lang/Object;

    aput-object v4, v6, v9

    invoke-virtual {p0, v11, v6}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/preference/CheckBoxPreference;->setTitle(Ljava/lang/CharSequence;)V

    .line 135
    iget-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->mGalleryCheckBox:Landroid/preference/CheckBoxPreference;

    const v6, 0x7f080053

    invoke-virtual {p0, v6}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/preference/CheckBoxPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 136
    iget-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->mGalleryCheckBox:Landroid/preference/CheckBoxPreference;

    const-string v6, "snsaccount_sync_gallery"

    invoke-virtual {v5, v6}, Landroid/preference/CheckBoxPreference;->setKey(Ljava/lang/String;)V

    .line 137
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->mGalleryCheckBox:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v5, v6}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 138
    iget-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->mGalleryCheckBox:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v5, v10}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 145
    :cond_6
    :goto_1
    const/4 v4, 0x0

    .line 146
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    const-string v6, "com.sec.android.app.sns3.home"

    invoke-virtual {v5, v6, v9}, Landroid/content/pm/PackageManager;->resolveContentProvider(Ljava/lang/String;I)Landroid/content/pm/ProviderInfo;

    move-result-object v3

    .line 147
    if-eqz v3, :cond_7

    .line 148
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/content/pm/ProviderInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v4

    .line 150
    :cond_7
    if-eqz v4, :cond_8

    invoke-interface {v4}, Ljava/lang/CharSequence;->length()I

    move-result v5

    if-nez v5, :cond_9

    .line 151
    :cond_8
    const-string v5, "SnsAccountFbAuth"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "providerLabel is null - "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 152
    const-string v4, "Home"

    .line 154
    :cond_9
    new-instance v5, Landroid/preference/CheckBoxPreference;

    iget-object v6, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->mAppContext:Landroid/content/Context;

    invoke-direct {v5, v6}, Landroid/preference/CheckBoxPreference;-><init>(Landroid/content/Context;)V

    iput-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->mHomeCheckBox:Landroid/preference/CheckBoxPreference;

    .line 155
    iget-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->mHomeCheckBox:Landroid/preference/CheckBoxPreference;

    new-array v6, v10, [Ljava/lang/Object;

    aput-object v4, v6, v9

    invoke-virtual {p0, v11, v6}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/preference/CheckBoxPreference;->setTitle(Ljava/lang/CharSequence;)V

    .line 156
    iget-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->mHomeCheckBox:Landroid/preference/CheckBoxPreference;

    const v6, 0x7f080055

    new-array v7, v10, [Ljava/lang/Object;

    invoke-virtual {p0, v12}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->getString(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v9

    invoke-virtual {p0, v6, v7}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/preference/CheckBoxPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 157
    iget-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->mHomeCheckBox:Landroid/preference/CheckBoxPreference;

    const-string v6, "snsaccount_sync_home"

    invoke-virtual {v5, v6}, Landroid/preference/CheckBoxPreference;->setKey(Ljava/lang/String;)V

    .line 158
    invoke-virtual {v2}, Lcom/sec/android/app/sns3/SnsFeatureManager;->isSyncAdapterForHomeFeedsNeeded()Z

    move-result v5

    if-eqz v5, :cond_a

    .line 159
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->mHomeCheckBox:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v5, v6}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 160
    iget-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->mHomeCheckBox:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v5, v10}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 164
    :cond_a
    const/4 v4, 0x0

    .line 165
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    const-string v6, "com.sec.android.app.sns3.life"

    invoke-virtual {v5, v6, v9}, Landroid/content/pm/PackageManager;->resolveContentProvider(Ljava/lang/String;I)Landroid/content/pm/ProviderInfo;

    move-result-object v3

    .line 166
    if-eqz v3, :cond_b

    .line 167
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/content/pm/ProviderInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v4

    .line 169
    :cond_b
    if-eqz v4, :cond_c

    invoke-interface {v4}, Ljava/lang/CharSequence;->length()I

    move-result v5

    if-nez v5, :cond_d

    .line 170
    :cond_c
    const-string v5, "SnsAccountFbAuth"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "providerLabel is null - "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 171
    const-string v4, "Life Times"

    .line 173
    :cond_d
    new-instance v5, Landroid/preference/CheckBoxPreference;

    iget-object v6, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->mAppContext:Landroid/content/Context;

    invoke-direct {v5, v6}, Landroid/preference/CheckBoxPreference;-><init>(Landroid/content/Context;)V

    iput-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->mLifeFeedsCheckBox:Landroid/preference/CheckBoxPreference;

    .line 174
    iget-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->mLifeFeedsCheckBox:Landroid/preference/CheckBoxPreference;

    new-array v6, v10, [Ljava/lang/Object;

    aput-object v4, v6, v9

    invoke-virtual {p0, v11, v6}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/preference/CheckBoxPreference;->setTitle(Ljava/lang/CharSequence;)V

    .line 175
    iget-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->mLifeFeedsCheckBox:Landroid/preference/CheckBoxPreference;

    const v6, 0x7f08005b

    new-array v7, v10, [Ljava/lang/Object;

    invoke-virtual {p0, v12}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->getString(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v9

    invoke-virtual {p0, v6, v7}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/preference/CheckBoxPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 177
    iget-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->mLifeFeedsCheckBox:Landroid/preference/CheckBoxPreference;

    const-string v6, "snsaccount_sync_life_feeds"

    invoke-virtual {v5, v6}, Landroid/preference/CheckBoxPreference;->setKey(Ljava/lang/String;)V

    .line 178
    invoke-virtual {v2}, Lcom/sec/android/app/sns3/SnsFeatureManager;->isSyncAdapterForProfileFeedsNeeded()Z

    move-result v5

    if-eqz v5, :cond_e

    .line 179
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->mLifeFeedsCheckBox:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v5, v6}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 180
    iget-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->mLifeFeedsCheckBox:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v5, v10}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 184
    :cond_e
    const/4 v4, 0x0

    .line 185
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    const-string v6, "com.sec.android.app.sns3.profiles"

    invoke-virtual {v5, v6, v9}, Landroid/content/pm/PackageManager;->resolveContentProvider(Ljava/lang/String;I)Landroid/content/pm/ProviderInfo;

    move-result-object v3

    .line 186
    if-eqz v3, :cond_f

    .line 187
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/content/pm/ProviderInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v4

    .line 189
    :cond_f
    if-eqz v4, :cond_10

    invoke-interface {v4}, Ljava/lang/CharSequence;->length()I

    move-result v5

    if-nez v5, :cond_11

    .line 190
    :cond_10
    const-string v5, "SnsAccountFbAuth"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "providerLabel is null - "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 191
    const-string v4, "Profiles"

    .line 193
    :cond_11
    new-instance v5, Landroid/preference/CheckBoxPreference;

    iget-object v6, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->mAppContext:Landroid/content/Context;

    invoke-direct {v5, v6}, Landroid/preference/CheckBoxPreference;-><init>(Landroid/content/Context;)V

    iput-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->mProfilesCheckBox:Landroid/preference/CheckBoxPreference;

    .line 194
    iget-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->mProfilesCheckBox:Landroid/preference/CheckBoxPreference;

    new-array v6, v10, [Ljava/lang/Object;

    aput-object v4, v6, v9

    invoke-virtual {p0, v11, v6}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/preference/CheckBoxPreference;->setTitle(Ljava/lang/CharSequence;)V

    .line 195
    iget-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->mProfilesCheckBox:Landroid/preference/CheckBoxPreference;

    const v6, 0x7f080057

    new-array v7, v10, [Ljava/lang/Object;

    invoke-virtual {p0, v12}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->getString(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v9

    invoke-virtual {p0, v6, v7}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/preference/CheckBoxPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 197
    iget-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->mProfilesCheckBox:Landroid/preference/CheckBoxPreference;

    const-string v6, "snsaccount_sync_profiles"

    invoke-virtual {v5, v6}, Landroid/preference/CheckBoxPreference;->setKey(Ljava/lang/String;)V

    .line 198
    invoke-virtual {v2}, Lcom/sec/android/app/sns3/SnsFeatureManager;->isSyncAdapterForProfilesNeeded()Z

    move-result v5

    if-eqz v5, :cond_12

    .line 199
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->mProfilesCheckBox:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v5, v6}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 200
    iget-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->mProfilesCheckBox:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v5, v10}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 204
    :cond_12
    const/4 v4, 0x0

    .line 205
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    const-string v6, "com.sec.android.app.sns3.streams"

    invoke-virtual {v5, v6, v9}, Landroid/content/pm/PackageManager;->resolveContentProvider(Ljava/lang/String;I)Landroid/content/pm/ProviderInfo;

    move-result-object v3

    .line 206
    if-eqz v3, :cond_13

    .line 207
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/content/pm/ProviderInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v4

    .line 209
    :cond_13
    if-eqz v4, :cond_14

    invoke-interface {v4}, Ljava/lang/CharSequence;->length()I

    move-result v5

    if-nez v5, :cond_15

    .line 210
    :cond_14
    const-string v5, "SnsAccountFbAuth"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "providerLabel is null - "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 211
    const-string v4, "Streams"

    .line 213
    :cond_15
    new-instance v5, Landroid/preference/CheckBoxPreference;

    iget-object v6, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->mAppContext:Landroid/content/Context;

    invoke-direct {v5, v6}, Landroid/preference/CheckBoxPreference;-><init>(Landroid/content/Context;)V

    iput-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->mStreamsCheckBox:Landroid/preference/CheckBoxPreference;

    .line 214
    iget-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->mStreamsCheckBox:Landroid/preference/CheckBoxPreference;

    new-array v6, v10, [Ljava/lang/Object;

    aput-object v4, v6, v9

    invoke-virtual {p0, v11, v6}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/preference/CheckBoxPreference;->setTitle(Ljava/lang/CharSequence;)V

    .line 215
    iget-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->mStreamsCheckBox:Landroid/preference/CheckBoxPreference;

    const v6, 0x7f08005a

    new-array v7, v10, [Ljava/lang/Object;

    invoke-virtual {p0, v12}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->getString(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v9

    invoke-virtual {p0, v6, v7}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/preference/CheckBoxPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 217
    iget-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->mStreamsCheckBox:Landroid/preference/CheckBoxPreference;

    const-string v6, "snsaccount_sync_streams"

    invoke-virtual {v5, v6}, Landroid/preference/CheckBoxPreference;->setKey(Ljava/lang/String;)V

    .line 218
    invoke-virtual {v2}, Lcom/sec/android/app/sns3/SnsFeatureManager;->isSyncAdapterForStreamsNeeded()Z

    move-result v5

    if-eqz v5, :cond_16

    .line 219
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->mStreamsCheckBox:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v5, v6}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 220
    iget-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->mStreamsCheckBox:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v5, v10}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 223
    :cond_16
    iget-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->mSyncIntervalSetting:Landroid/preference/PreferenceScreen;

    new-instance v6, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity$1;

    invoke-direct {v6, p0}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity$1;-><init>(Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;)V

    invoke-virtual {v5, v6}, Landroid/preference/PreferenceScreen;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 234
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v5

    invoke-virtual {v5}, Landroid/preference/PreferenceScreen;->getPreferenceCount()I

    move-result v5

    if-gt v5, v10, :cond_17

    .line 235
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->finish()V

    .line 237
    :cond_17
    return-void

    .line 79
    .end local v0    # "account":[Landroid/accounts/Account;
    .end local v1    # "actionBar":Landroid/app/ActionBar;
    .end local v2    # "featureMgr":Lcom/sec/android/app/sns3/SnsFeatureManager;
    .end local v3    # "providerInfo":Landroid/content/pm/ProviderInfo;
    .end local v4    # "providerLabel":Ljava/lang/CharSequence;
    :cond_18
    const v5, 0x1030128

    goto/16 :goto_0

    .line 140
    .restart local v0    # "account":[Landroid/accounts/Account;
    .restart local v1    # "actionBar":Landroid/app/ActionBar;
    .restart local v2    # "featureMgr":Lcom/sec/android/app/sns3/SnsFeatureManager;
    .restart local v3    # "providerInfo":Landroid/content/pm/ProviderInfo;
    .restart local v4    # "providerLabel":Ljava/lang/CharSequence;
    :cond_19
    array-length v5, v0

    if-lez v5, :cond_6

    .line 141
    aget-object v5, v0, v9

    const-string v6, "com.sec.android.gallery3d.sns.contentprovider"

    invoke-static {v5, v6, v9}, Landroid/content/ContentResolver;->setIsSyncable(Landroid/accounts/Account;Ljava/lang/String;I)V

    goto/16 :goto_1
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 310
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    .line 312
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->isActionbarLightTheme(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 313
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0a0001

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 317
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 315
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const/high16 v1, 0x7f0a0000

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 346
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onDestroy()V

    .line 347
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 322
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 330
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    .line 332
    :goto_0
    return v0

    .line 324
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->onClickDone()V

    .line 332
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 327
    :pswitch_1
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->finish()V

    goto :goto_1

    .line 322
    nop

    :pswitch_data_0
    .packed-switch 0x7f0b0005
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 241
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 243
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->mCalendarCheckBox:Landroid/preference/CheckBoxPreference;

    const-string v1, "calendar"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 244
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->mHomeCheckBox:Landroid/preference/CheckBoxPreference;

    const-string v1, "home"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 245
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->mLifeFeedsCheckBox:Landroid/preference/CheckBoxPreference;

    const-string v1, "feeds"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 246
    const-string v0, "true"

    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->mIsGallerySync:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 247
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->mGalleryCheckBox:Landroid/preference/CheckBoxPreference;

    const-string v1, "gallery"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 249
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->mProfilesCheckBox:Landroid/preference/CheckBoxPreference;

    const-string v1, "profiles"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 250
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->mStreamsCheckBox:Landroid/preference/CheckBoxPreference;

    const-string v1, "streams"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 251
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 255
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 257
    const-string v0, "calendar"

    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->mCalendarCheckBox:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 258
    const-string v0, "home"

    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->mHomeCheckBox:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 259
    const-string v0, "feeds"

    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->mLifeFeedsCheckBox:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 260
    const-string v0, "true"

    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->mIsGallerySync:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 261
    const-string v0, "gallery"

    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->mGalleryCheckBox:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 263
    :cond_0
    const-string v0, "profiles"

    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->mProfilesCheckBox:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 264
    const-string v0, "streams"

    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;->mStreamsCheckBox:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 265
    return-void
.end method
