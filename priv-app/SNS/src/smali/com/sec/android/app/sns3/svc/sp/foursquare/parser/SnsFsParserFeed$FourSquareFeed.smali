.class public interface abstract Lcom/sec/android/app/sns3/svc/sp/foursquare/parser/SnsFsParserFeed$FourSquareFeed;
.super Ljava/lang/Object;
.source "SnsFsParserFeed.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/svc/sp/foursquare/parser/SnsFsParserFeed;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "FourSquareFeed"
.end annotation


# static fields
.field public static final ADDRESS:Ljava/lang/String; = "address"

.field public static final CATEGORY:Ljava/lang/String; = "categories"

.field public static final CATEGORY_PRIMARY:Ljava/lang/String; = "primary"

.field public static final CHECKINS:Ljava/lang/String; = "checkins"

.field public static final CITY:Ljava/lang/String; = "city"

.field public static final CODE:Ljava/lang/String; = "code"

.field public static final COUNT:Ljava/lang/String; = "count"

.field public static final COUNTRY:Ljava/lang/String; = "country"

.field public static final CREATED_AT:Ljava/lang/String; = "createdAt"

.field public static final DESCRIPTION:Ljava/lang/String; = "shout"

.field public static final ICON:Ljava/lang/String; = "icon"

.field public static final ID:Ljava/lang/String; = "id"

.field public static final ITEMS:Ljava/lang/String; = "items"

.field public static final LOCATION:Ljava/lang/String; = "location"

.field public static final LOC_LATITUDE:Ljava/lang/String; = "lat"

.field public static final LOC_LONGITUDE:Ljava/lang/String; = "lng"

.field public static final META:Ljava/lang/String; = "meta"

.field public static final NAME:Ljava/lang/String; = "name"

.field public static final PLURAL_NAME:Ljava/lang/String; = "pluralName"

.field public static final PREFIX:Ljava/lang/String; = "prefix"

.field public static final RECENT:Ljava/lang/String; = "recent"

.field public static final RESPONSE:Ljava/lang/String; = "response"

.field public static final STATE:Ljava/lang/String; = "state"

.field public static final SUFFIX:Ljava/lang/String; = "suffix"

.field public static final TIMEZONE_OFFSET:Ljava/lang/String; = "timeZoneOffset"

.field public static final URL:Ljava/lang/String; = "url"

.field public static final VENUE:Ljava/lang/String; = "venue"
