.class public Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDBHelper;
.super Lcom/sec/android/app/sns3/agent/db/SnsDBHelperBase;
.source "SnsLinkedInDBHelper.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 32
    const-string v0, "snsLinkedInDB.db"

    const/4 v1, 0x0

    const/4 v2, 0x3

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/sec/android/app/sns3/agent/db/SnsDBHelperBase;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 33
    return-void
.end method


# virtual methods
.method public createTables(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 51
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 53
    const-string v0, "CREATE TABLE IF NOT EXISTS user_basic_info (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,id VARCHAR(50),first_name TEXT,last_name TEXT,formatted_name TEXT,headline TEXT,location TEXT,location_code TEXT,industry TEXT,num_connections INTEGER,picture_url VARCHAR(1024),public_profile_url VARCHAR(1024),email_address VARCHAR(150),date_of_birth VARCHAR(15), UNIQUE (id));"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 70
    const-string v0, "SnsAgent"

    const-string v1, "DB TABLE(user_basic_info) Has been created."

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 74
    const-string v0, "CREATE TABLE IF NOT EXISTS user_work_info (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,position_id VARCHAR(50),title TEXT,summary TEXT,startMonth INTEGER,startYear INTEGER,endMONTH INTEGER,endtYear INTEGER,isCurrent BOOLEAN,company_id VARCHAR(50),company_name TEXT,company_size VARCHAR(15),company_type VARCHAR(15),industry TEXT,person_id VARCHAR(50), UNIQUE (position_id));"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 92
    const-string v0, "SnsAgent"

    const-string v1, "DB TABLE(user_work_info) Has been created."

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 96
    const-string v0, "CREATE TABLE IF NOT EXISTS user_connections (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,id VARCHAR(50),first_name TEXT,last_name TEXT,headline TEXT,picture_url VARCHAR(1024),profile_url VARCHAR(1024),name TEXT,code INTEGER,industry VARCHAR(100),degree INTEGER DEFAULT 1, UNIQUE (id));"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 110
    const-string v0, "SnsAgent"

    const-string v1, "DB TABLE(user_connections) Has been created."

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 114
    const-string v0, "CREATE TABLE IF NOT EXISTS user_skills (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,skill_id VARCHAR(50),skill_name VARCHAR(50), UNIQUE (skill_id));"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 119
    const-string v0, "SnsAgent"

    const-string v1, "DB TABLE(user_skills) Has been created."

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 123
    const-string v0, "CREATE TABLE IF NOT EXISTS followings (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,following_id VARCHAR(50),following_type VARCHAR(50),following_name VARCHAR(50), UNIQUE (following_id));"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 130
    const-string v0, "SnsAgent"

    const-string v1, "DB TABLE(followings) Has been created."

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 134
    const-string v0, "CREATE TABLE IF NOT EXISTS user_contact (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,contact_type VARCHAR(15),contact_subtype VARCHAR(15),contact_value VARCHAR(150), UNIQUE (contact_value));"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 141
    const-string v0, "SnsAgent"

    const-string v1, "DB TABLE(user_contact) Has been created."

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 145
    const-string v0, "CREATE TABLE IF NOT EXISTS user_groups (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,group_key VARCHAR(15),group_id VARCHAR(15),group_name VARCHAR(50),membership_state VARCHAR(50), UNIQUE (group_id));"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 160
    const-string v0, "SnsAgent"

    const-string v1, "DB TABLE(user_groups) Has been created."

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 163
    const-string v0, "CREATE TABLE IF NOT EXISTS user_updates (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,update_key VARCHAR(15),update_type VARCHAR(15),timestamp TIMESTAMP,person_id VARCHAR(15),is_commentable BOOLEAN,is_likable BOOLEAN,is_liked BOOLEAN,action_code VARCHAR(40),number_of_likes INTEGER,current_status VARCHAR(15),updated_fields VARCHAR(150),share_id VARCHAR(15),description TEXT);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 180
    const-string v0, "SnsAgent"

    const-string v1, "DB TABLE(user_updates) Has been created."

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 184
    const-string v0, "CREATE TABLE IF NOT EXISTS user_share_updates (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,current_share_id VARCHAR(15),share_update_key VARCHAR(15),visibility BOOLEAN,comment VARCHAR(1024),submitted_url VARCHAR(1024),title VARCHAR(50),source_name VARCHAR(50),source_app VARCHAR(50),time_stamp TIMESTAMP,author_person_id VARCHAR(15));"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 199
    const-string v0, "SnsAgent"

    const-string v1, "DB TABLE(user_share_updates) Has been created."

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 203
    const-string v0, "CREATE TABLE IF NOT EXISTS education (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,degree_of_education VARCHAR(40),end_date_year INTEGER,education_id VARCHAR(15),school_name VARCHAR(100),start_date_year INTEGER,field_of_study VARCHAR(15), UNIQUE (education_id));"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 213
    const-string v0, "SnsAgent"

    const-string v1, "DB TABLE(education) Has been created."

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 217
    const-string v0, "CREATE TABLE IF NOT EXISTS user_likes (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,like_update_key VARCHAR(15),person_id VARCHAR(15));"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 223
    const-string v0, "SnsAgent"

    const-string v1, "DB TABLE(user_likes) Has been created."

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 227
    const-string v0, "CREATE TABLE IF NOT EXISTS user_comments (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,comment_update_key VARCHAR(15),comment_id VARCHAR(15),comment_person_id VARCHAR(50),comment_sequence_number VARCHAR(50),comment_stime_stamp TIMESTAMP,comment_text VARCHAR(50));"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 239
    const-string v0, "SnsAgent"

    const-string v1, "DB TABLE(user_comments) Has been created."

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 243
    const-string v0, "CREATE TABLE IF NOT EXISTS group_connection_updates (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,group_update_KEY VARCHAR(15),group_id VARCHAR(15),group_name VARCHAR(50));"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 251
    const-string v0, "SnsAgent"

    const-string v1, "DB TABLE(group_connection_updates) Has been created."

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 254
    const-string v0, "CREATE TABLE IF NOT EXISTS user_connection_updates (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,connection_update_key VARCHAR(15),new_connection_id VARCHAR(15));"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 262
    const-string v0, "SnsAgent"

    const-string v1, "DB TABLE(user_connection_updates) Has been created."

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 265
    const-string v0, "CREATE TABLE IF NOT EXISTS user_apps_updates (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,app_id INTEGER,activity_body VARCHAR(100),apps_update_key VARCHAR(15));"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 271
    const-string v0, "SnsAgent"

    const-string v1, "DB TABLE(user_apps_updates) Has been created."

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 274
    const-string v0, "CREATE TABLE IF NOT EXISTS user_jobs_updates (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,jobs_update_key VARCHAR(15),job_id VARCHAR(15),job_url TEXT,position VARCHAR(15),company VARCHAR(15));"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 282
    const-string v0, "SnsAgent"

    const-string v1, "DB TABLE(user_jobs_updates) Has been created."

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 285
    const-string v0, "CREATE TABLE IF NOT EXISTS user_companies (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,company_id VARCHAR(15),company_name VARCHAR(15),company_size INTEGER,company_type VARCHAR(15),industries VARCHAR(15));"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 295
    const-string v0, "SnsAgent"

    const-string v1, "DB TABLE(user_companies) Has been created."

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 298
    const-string v0, "CREATE TABLE IF NOT EXISTS company_updates (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,update_key VARCHAR(15),company_id VARCHAR(15),company_name VARCHAR(15),job_id VARCHAR(15),share_id VARCHAR(15),person_id VARCHAR(15),update_sub_type VARCHAR(15),old_position_title VARCHAR(15),old_position_company VARCHAR(15),new_position_company VARCHAR(15),new_position_title VARCHAR(15));"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 314
    const-string v0, "SnsAgent"

    const-string v1, "DB TABLE(company_updates) Has been created."

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 317
    const-string v0, "CREATE TABLE IF NOT EXISTS viral_updates (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,update_key VARCHAR(15),original_post_update_key VARCHAR(15));"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 325
    const-string v0, "SnsAgent"

    const-string v1, "DB TABLE(viral_updates) Has been created."

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 329
    const-string v0, "CREATE TABLE IF NOT EXISTS user_recommendation_updates (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,recommendation_id INTEGER,recommendation_update_key VARCHAR(15),recommendation_type TEXT,recommender_id VARCHAR(50),recommendee_id VARCHAR(50),recommendation_url VARCHAR(1024));"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 341
    const-string v0, "SnsAgent"

    const-string v1, "DB TABLE(user_recommendation_updates) Has been created."

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 344
    const-string v0, "CREATE TABLE IF NOT EXISTS feed_list (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,from_id TEXT,from_name TEXT,from_icon_url TEXT,update_key TEXT,update_type TEXT,title TEXT,message TEXT,submitted_url TEXT,thumbnail_url TEXT,comment TEXT,timestamp TEXT);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 358
    const-string v0, "SnsAgent"

    const-string v1, "DB TABLE(feed_list) Has been created."

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 361
    const-string v0, "CREATE TABLE IF NOT EXISTS profile_feed_list (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,from_id TEXT,from_name TEXT,from_icon_url TEXT,update_key TEXT,update_type TEXT,title TEXT,message TEXT,submitted_url TEXT,thumbnail_url TEXT,comment TEXT,timestamp TEXT);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 375
    const-string v0, "SnsAgent"

    const-string v1, "DB TABLE(profile_feed_list) Has been created."

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 378
    const-string v0, "CREATE TABLE IF NOT EXISTS home_feed (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,from_id TEXT,from_name TEXT,from_icon_url TEXT,update_key TEXT,update_type TEXT,title TEXT,message TEXT,submitted_url TEXT,thumbnail_url TEXT,comment TEXT,timestamp TEXT);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 392
    const-string v0, "SnsAgent"

    const-string v1, "DB TABLE(home_feed) Has been created."

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 395
    const-string v0, "CREATE TABLE IF NOT EXISTS status_stream (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,from_id TEXT,from_name TEXT,from_icon_url TEXT,update_key TEXT,update_type TEXT,title TEXT,message TEXT,submitted_url TEXT,thumbnail_url TEXT,comment TEXT,likes_count INTEGER,comments_count INTEGER,timestamp TEXT);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 411
    const-string v0, "SnsAgent"

    const-string v1, "DB TABLE(status_stream) Has been created."

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 414
    const-string v0, "CREATE TABLE IF NOT EXISTS people_lookup (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,member_id VARCHAR(50),first_name TEXT,last_name TEXT,formatted_name TEXT,headline TEXT,email VARCHAR(150),phone_number VARCHAR(40),profile_url TEXT,position_title TEXT,position_company_name TEXT,picture_url TEXT,picture_cache_uri TEXT,large_picture_url TEXT,large_picture_cache_uri TEXT,last_updated_timestamp TEXT);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 432
    const-string v0, "SnsAgent"

    const-string v1, "DB TABLE(people_lookup) Has been created."

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 435
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 436
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 437
    return-void
.end method

.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 0
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 37
    invoke-super {p0, p1}, Lcom/sec/android/app/sns3/agent/db/SnsDBHelperBase;->onCreate(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 39
    invoke-virtual {p0, p1}, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDBHelper;->createTables(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 40
    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 0
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "oldVersion"    # I
    .param p3, "newVersion"    # I

    .prologue
    .line 44
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/app/sns3/agent/db/SnsDBHelperBase;->onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V

    .line 46
    invoke-virtual {p0, p1}, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDBHelper;->upgradeTables(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 48
    return-void
.end method

.method public upgradeTables(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 442
    const-string v0, "DROP TABLE IF EXISTS user_basic_info"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 443
    const-string v0, "DROP TABLE IF EXISTS user_work_info"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 444
    const-string v0, "DROP TABLE IF EXISTS user_connections"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 445
    const-string v0, "DROP TABLE IF EXISTS user_skills"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 446
    const-string v0, "DROP TABLE IF EXISTS followings"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 447
    const-string v0, "DROP TABLE IF EXISTS user_contact"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 448
    const-string v0, "DROP TABLE IF EXISTS user_groups"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 449
    const-string v0, "DROP TABLE IF EXISTS user_updates"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 450
    const-string v0, "DROP TABLE IF EXISTS user_share_updates"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 451
    const-string v0, "DROP TABLE IF EXISTS user_likes"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 452
    const-string v0, "DROP TABLE IF EXISTS user_comments"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 453
    const-string v0, "DROP TABLE IF EXISTS user_connection_updates"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 454
    const-string v0, "DROP TABLE IF EXISTS group_connection_updates"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 455
    const-string v0, "DROP TABLE IF EXISTS education"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 456
    const-string v0, "DROP TABLE IF EXISTS user_apps_updates"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 457
    const-string v0, "DROP TABLE IF EXISTS user_jobs_updates"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 458
    const-string v0, "DROP TABLE IF EXISTS user_companies"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 459
    const-string v0, "DROP TABLE IF EXISTS company_updates"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 460
    const-string v0, "DROP TABLE IF EXISTS viral_updates"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 461
    const-string v0, "DROP TABLE IF EXISTS user_recommendation_updates"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 462
    const-string v0, "DROP TABLE IF EXISTS feed_list"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 463
    const-string v0, "DROP TABLE IF EXISTS profile_feed_list"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 464
    const-string v0, "DROP TABLE IF EXISTS home_feed"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 465
    const-string v0, "DROP TABLE IF EXISTS status_stream"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 466
    const-string v0, "DROP TABLE IF EXISTS people_lookup"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 467
    invoke-virtual {p0, p1}, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDBHelper;->createTables(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 468
    return-void
.end method

.method public wipeData(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 4
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 471
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 473
    :try_start_0
    const-string v1, "DELETE FROM user_basic_info"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 474
    const-string v1, "DELETE FROM user_work_info"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 475
    const-string v1, "DELETE FROM user_connections"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 476
    const-string v1, "DELETE FROM user_skills"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 477
    const-string v1, "DELETE FROM followings"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 478
    const-string v1, "DELETE FROM user_contact"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 479
    const-string v1, "DELETE FROM user_groups"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 480
    const-string v1, "DELETE FROM user_updates"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 481
    const-string v1, "DELETE FROM user_share_updates"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 482
    const-string v1, "DELETE FROM user_likes"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 483
    const-string v1, "DELETE FROM user_comments"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 484
    const-string v1, "DELETE FROM user_connection_updates"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 485
    const-string v1, "DELETE FROM group_connection_updates"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 486
    const-string v1, "DELETE FROM education"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 487
    const-string v1, "DELETE FROM user_apps_updates"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 488
    const-string v1, "DELETE FROM user_jobs_updates"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 489
    const-string v1, "DELETE FROM user_companies"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 490
    const-string v1, "DELETE FROM company_updates"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 491
    const-string v1, "DELETE FROM viral_updates"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 492
    const-string v1, "DELETE FROM user_recommendation_updates"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 493
    const-string v1, "DELETE FROM feed_list"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 494
    const-string v1, "DELETE FROM profile_feed_list"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 495
    const-string v1, "DELETE FROM home_feed"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 496
    const-string v1, "DELETE FROM status_stream"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 497
    const-string v1, "DELETE FROM people_lookup"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 498
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 504
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 506
    :goto_0
    return-void

    .line 499
    :catch_0
    move-exception v0

    .line 500
    .local v0, "e":Landroid/database/sqlite/SQLiteException;
    :try_start_1
    const-string v1, "SnsAgent"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error in wiping db. "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 501
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 504
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    goto :goto_0

    .end local v0    # "e":Landroid/database/sqlite/SQLiteException;
    :catchall_0
    move-exception v1

    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v1
.end method
