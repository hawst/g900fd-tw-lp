.class public interface abstract Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserAlbum$FacebookAlbum;
.super Ljava/lang/Object;
.source "SnsFbParserAlbum.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserAlbum;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "FacebookAlbum"
.end annotation


# static fields
.field public static final ALBUM_NAME:Ljava/lang/String; = "name"

.field public static final COUNT:Ljava/lang/String; = "count"

.field public static final COVER_PHOTO:Ljava/lang/String; = "cover_photo"

.field public static final CREATED_TIME:Ljava/lang/String; = "created_time"

.field public static final DESCRIPTION:Ljava/lang/String; = "description"

.field public static final FROM:Ljava/lang/String; = "from"

.field public static final ID:Ljava/lang/String; = "id"

.field public static final LINK:Ljava/lang/String; = "link"

.field public static final LOCATION:Ljava/lang/String; = "location"

.field public static final PRIVACY:Ljava/lang/String; = "privacy"

.field public static final TYPE:Ljava/lang/String; = "type"

.field public static final UPDATED_TIME:Ljava/lang/String; = "updated_time"
