.class public Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthenticatorService$FourSquareAccountAuthenticator;
.super Landroid/accounts/AbstractAccountAuthenticator;
.source "SnsAccountFsAuthenticatorService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthenticatorService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "FourSquareAccountAuthenticator"
.end annotation


# instance fields
.field private mAppContext:Landroid/content/Context;

.field final synthetic this$0:Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthenticatorService;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthenticatorService;Landroid/content/Context;)V
    .locals 0
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 79
    iput-object p1, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthenticatorService$FourSquareAccountAuthenticator;->this$0:Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthenticatorService;

    .line 80
    invoke-direct {p0, p2}, Landroid/accounts/AbstractAccountAuthenticator;-><init>(Landroid/content/Context;)V

    .line 81
    iput-object p2, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthenticatorService$FourSquareAccountAuthenticator;->mAppContext:Landroid/content/Context;

    .line 82
    return-void
.end method


# virtual methods
.method public addAccount(Landroid/accounts/AccountAuthenticatorResponse;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 8
    .param p1, "response"    # Landroid/accounts/AccountAuthenticatorResponse;
    .param p2, "accountType"    # Ljava/lang/String;
    .param p3, "authTokenType"    # Ljava/lang/String;
    .param p4, "requiredFeatures"    # [Ljava/lang/String;
    .param p5, "options"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/accounts/NetworkErrorException;
        }
    .end annotation

    .prologue
    .line 88
    invoke-static {}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->isLoggable()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 89
    # getter for: Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthenticatorService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthenticatorService;->access$000()Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "FourSquareAuthenticatorService : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " - "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 93
    :cond_0
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 95
    .local v4, "result":Landroid/os/Bundle;
    if-eqz p5, :cond_3

    # getter for: Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthenticatorService;->OPTIONS_USERNAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthenticatorService;->access$100()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p5, v5}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 96
    new-instance v0, Landroid/accounts/Account;

    # getter for: Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthenticatorService;->OPTIONS_USERNAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthenticatorService;->access$100()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p5, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "com.sec.android.app.sns3.foursquare"

    invoke-direct {v0, v5, v6}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    .local v0, "account":Landroid/accounts/Account;
    iget-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthenticatorService$FourSquareAccountAuthenticator;->mAppContext:Landroid/content/Context;

    invoke-static {v5}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    .line 100
    .local v1, "am":Landroid/accounts/AccountManager;
    # getter for: Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthenticatorService;->OPTIONS_PASSWORD:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthenticatorService;->access$200()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p5, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v1, v0, v5, v6}, Landroid/accounts/AccountManager;->addAccountExplicitly(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 101
    const-string v5, "authAccount"

    # getter for: Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthenticatorService;->OPTIONS_USERNAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthenticatorService;->access$100()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p5, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    const-string v5, "accountType"

    const-string v6, "com.sec.android.app.sns3.foursquare"

    invoke-virtual {v4, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    new-instance v3, Landroid/content/Intent;

    const-string v5, "com.sec.android.app.sns.ACTION_SNS_FOURSQUARE_LOGGED_IN"

    invoke-direct {v3, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 107
    .local v3, "intent":Landroid/content/Intent;
    iget-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthenticatorService$FourSquareAccountAuthenticator;->this$0:Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthenticatorService;

    const-string v6, "com.sec.android.app.sns3.permission.RECEIVE_SNS_BROADCAST"

    invoke-virtual {v5, v3, v6}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthenticatorService;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    .line 130
    .end local v0    # "account":Landroid/accounts/Account;
    .end local v1    # "am":Landroid/accounts/AccountManager;
    .end local v3    # "intent":Landroid/content/Intent;
    :cond_1
    :goto_0
    return-object v4

    .line 109
    .restart local v0    # "account":Landroid/accounts/Account;
    .restart local v1    # "am":Landroid/accounts/AccountManager;
    :cond_2
    const-string v5, "com.sec.android.app.sns3.foursquare"

    invoke-virtual {v1, v5}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v2

    .line 110
    .local v2, "fsAccounts":[Landroid/accounts/Account;
    array-length v5, v2

    if-lez v5, :cond_1

    .line 112
    new-instance v3, Landroid/content/Intent;

    const-string v5, "com.sec.android.app.sns.ACTION_SNS_FOURSQUARE_TOKEN_UPDATED"

    invoke-direct {v3, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 114
    .restart local v3    # "intent":Landroid/content/Intent;
    iget-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthenticatorService$FourSquareAccountAuthenticator;->this$0:Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthenticatorService;

    const-string v6, "com.sec.android.app.sns3.permission.RECEIVE_SNS_BROADCAST"

    invoke-virtual {v5, v3, v6}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthenticatorService;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    goto :goto_0

    .line 118
    .end local v0    # "account":Landroid/accounts/Account;
    .end local v1    # "am":Landroid/accounts/AccountManager;
    .end local v2    # "fsAccounts":[Landroid/accounts/Account;
    .end local v3    # "intent":Landroid/content/Intent;
    :cond_3
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 120
    .restart local v3    # "intent":Landroid/content/Intent;
    iget-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthenticatorService$FourSquareAccountAuthenticator;->mAppContext:Landroid/content/Context;

    const-class v6, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthSSOActivity;

    invoke-virtual {v3, v5, v6}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 124
    const-string v5, "accountAuthenticatorResponse"

    invoke-virtual {v3, v5, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 125
    const-string v5, "manageAccount"

    invoke-virtual {v3, v5, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 127
    const-string v5, "intent"

    invoke-virtual {v4, v5, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    goto :goto_0
.end method

.method public confirmCredentials(Landroid/accounts/AccountAuthenticatorResponse;Landroid/accounts/Account;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 2
    .param p1, "response"    # Landroid/accounts/AccountAuthenticatorResponse;
    .param p2, "account"    # Landroid/accounts/Account;
    .param p3, "options"    # Landroid/os/Bundle;

    .prologue
    .line 136
    # getter for: Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthenticatorService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthenticatorService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "FourSquareAuthenticatorService : confirmCredentials"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 137
    const/4 v0, 0x0

    return-object v0
.end method

.method public editProperties(Landroid/accounts/AccountAuthenticatorResponse;Ljava/lang/String;)Landroid/os/Bundle;
    .locals 2
    .param p1, "response"    # Landroid/accounts/AccountAuthenticatorResponse;
    .param p2, "accountType"    # Ljava/lang/String;

    .prologue
    .line 142
    # getter for: Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthenticatorService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthenticatorService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "FourSquareAuthenticatorService : editProperties"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 143
    const/4 v0, 0x0

    return-object v0
.end method

.method public getAccountRemovalAllowed(Landroid/accounts/AccountAuthenticatorResponse;Landroid/accounts/Account;)Landroid/os/Bundle;
    .locals 12
    .param p1, "response"    # Landroid/accounts/AccountAuthenticatorResponse;
    .param p2, "account"    # Landroid/accounts/Account;

    .prologue
    const-wide/16 v10, 0x0

    const/4 v9, 0x0

    .line 148
    # getter for: Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthenticatorService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthenticatorService;->access$000()Ljava/lang/String;

    move-result-object v7

    const-string v8, "FourSquareAuthenticatorService : getAccountRemovalAllowed"

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 150
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v0

    .line 151
    .local v0, "app":Lcom/sec/android/app/sns3/SnsApplication;
    new-instance v1, Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdAuthLogout;

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/SnsApplication;->getSvcMgr()Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v7

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/SnsApplication;->getAgentMgr()Lcom/sec/android/app/sns3/agent/SnsAgentMgr;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/app/sns3/agent/SnsAgentMgr;->getCommandMgr()Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;->getCommandMgrHandle()Landroid/os/Handler;

    move-result-object v8

    invoke-direct {v1, v7, v8}, Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdAuthLogout;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Handler;)V

    .line 153
    .local v1, "cmd":Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;
    new-instance v7, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthenticatorService$FourSquareAccountAuthenticator$1;

    invoke-direct {v7, p0}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthenticatorService$FourSquareAccountAuthenticator$1;-><init>(Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthenticatorService$FourSquareAccountAuthenticator;)V

    invoke-virtual {v1, v7}, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->setCommandCallback(Lcom/sec/android/app/sns3/agent/command/ISnsCommandCallback;)Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;

    .line 163
    invoke-virtual {v1}, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->send()I

    .line 166
    iget-object v7, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthenticatorService$FourSquareAccountAuthenticator;->this$0:Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthenticatorService;

    const-string v8, "FS_profile_feed_sync_interval"

    invoke-virtual {v7, v8, v9}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthenticatorService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v7

    invoke-interface {v7}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    .line 168
    .local v4, "profileFeedEditor":Landroid/content/SharedPreferences$Editor;
    const-string v7, "FS_profile_feed_sync_interval"

    invoke-interface {v4, v7, v10, v11}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 169
    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 171
    iget-object v7, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthenticatorService$FourSquareAccountAuthenticator;->this$0:Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthenticatorService;

    const-string v8, "FS_profile_sync_interval"

    invoke-virtual {v7, v8, v9}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthenticatorService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v7

    invoke-interface {v7}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    .line 173
    .local v5, "profilesEditor":Landroid/content/SharedPreferences$Editor;
    const-string v7, "FS_profile_sync_interval"

    invoke-interface {v5, v7, v10, v11}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 174
    invoke-interface {v5}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 177
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v7

    const-string v8, "notification"

    invoke-virtual {v7, v8}, Lcom/sec/android/app/sns3/SnsApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/app/NotificationManager;

    move-object v3, v7

    check-cast v3, Landroid/app/NotificationManager;

    .line 179
    .local v3, "notiMgr":Landroid/app/NotificationManager;
    const/16 v7, 0x10cc

    invoke-virtual {v3, v7}, Landroid/app/NotificationManager;->cancel(I)V

    .line 182
    new-instance v2, Landroid/content/Intent;

    const-string v7, "com.sec.android.app.sns.ACTION_SNS_FOURSQUARE_LOGGED_OUT"

    invoke-direct {v2, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 183
    .local v2, "intent":Landroid/content/Intent;
    iget-object v7, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthenticatorService$FourSquareAccountAuthenticator;->this$0:Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthenticatorService;

    const-string v8, "com.sec.android.app.sns3.permission.RECEIVE_SNS_BROADCAST"

    invoke-virtual {v7, v2, v8}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthenticatorService;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    .line 185
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 186
    .local v6, "result":Landroid/os/Bundle;
    const-string v7, "booleanResult"

    const/4 v8, 0x1

    invoke-virtual {v6, v7, v8}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 187
    return-object v6
.end method

.method public getAuthToken(Landroid/accounts/AccountAuthenticatorResponse;Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 2
    .param p1, "response"    # Landroid/accounts/AccountAuthenticatorResponse;
    .param p2, "account"    # Landroid/accounts/Account;
    .param p3, "authTokenType"    # Ljava/lang/String;
    .param p4, "loginOptions"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/accounts/NetworkErrorException;
        }
    .end annotation

    .prologue
    .line 193
    # getter for: Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthenticatorService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthenticatorService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "FourSquareAuthenticatorService : getAuthToken"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 194
    const/4 v0, 0x0

    return-object v0
.end method

.method public getAuthTokenLabel(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "authTokenType"    # Ljava/lang/String;

    .prologue
    .line 199
    # getter for: Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthenticatorService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthenticatorService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "FourSquareAuthenticatorService : getAuthTokenLabel"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 200
    const/4 v0, 0x0

    return-object v0
.end method

.method public hasFeatures(Landroid/accounts/AccountAuthenticatorResponse;Landroid/accounts/Account;[Ljava/lang/String;)Landroid/os/Bundle;
    .locals 2
    .param p1, "response"    # Landroid/accounts/AccountAuthenticatorResponse;
    .param p2, "account"    # Landroid/accounts/Account;
    .param p3, "features"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/accounts/NetworkErrorException;
        }
    .end annotation

    .prologue
    .line 206
    # getter for: Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthenticatorService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthenticatorService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "FourSquareAuthenticatorService : hasFeatures"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 207
    const/4 v0, 0x0

    return-object v0
.end method

.method public updateCredentials(Landroid/accounts/AccountAuthenticatorResponse;Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 2
    .param p1, "response"    # Landroid/accounts/AccountAuthenticatorResponse;
    .param p2, "account"    # Landroid/accounts/Account;
    .param p3, "authTokenType"    # Ljava/lang/String;
    .param p4, "loginOptions"    # Landroid/os/Bundle;

    .prologue
    .line 213
    # getter for: Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthenticatorService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthenticatorService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "FourSquareAuthenticatorService : updateCredentials"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 214
    const/4 v0, 0x0

    return-object v0
.end method
