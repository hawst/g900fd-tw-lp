.class public interface abstract Lcom/sec/android/app/sns3/agent/sp/googleplus/db/SnsGooglePlusDB$UserProfileInfoColumns;
.super Ljava/lang/Object;
.source "SnsGooglePlusDB.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/agent/sp/googleplus/db/SnsGooglePlusDB;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "UserProfileInfoColumns"
.end annotation


# static fields
.field public static final PROFILE_BIRTHDAY:Ljava/lang/String; = "birthday"

.field public static final PROFILE_FIRST_NAME:Ljava/lang/String; = "first_name"

.field public static final PROFILE_ID:Ljava/lang/String; = "profile_id"

.field public static final PROFILE_IMAGE_URL:Ljava/lang/String; = "profile_image"

.field public static final PROFILE_LAST_NAME:Ljava/lang/String; = "last_name"

.field public static final PROFILE_NAME:Ljava/lang/String; = "profile_name"

.field public static final PROFILE_RELATION_STATUS:Ljava/lang/String; = "status"

.field public static final PROFILE_TAGLINE:Ljava/lang/String; = "tagline"

.field public static final PROFILE_URL:Ljava/lang/String; = "profile_url"
