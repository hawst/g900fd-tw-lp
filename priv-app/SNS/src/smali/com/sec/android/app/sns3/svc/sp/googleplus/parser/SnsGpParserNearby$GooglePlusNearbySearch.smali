.class public interface abstract Lcom/sec/android/app/sns3/svc/sp/googleplus/parser/SnsGpParserNearby$GooglePlusNearbySearch;
.super Ljava/lang/Object;
.source "SnsGpParserNearby.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/svc/sp/googleplus/parser/SnsGpParserNearby;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "GooglePlusNearbySearch"
.end annotation


# static fields
.field public static final ATTRIBUTIONS:Ljava/lang/String; = "html_attributions"

.field public static final FORMATTED_ADDRESS:Ljava/lang/String; = "formatted_address"

.field public static final GEOMETRY:Ljava/lang/String; = "geometry"

.field public static final ICON:Ljava/lang/String; = "icon"

.field public static final ID:Ljava/lang/String; = "id"

.field public static final LATITUDE:Ljava/lang/String; = "lat"

.field public static final LOCATION:Ljava/lang/String; = "location"

.field public static final LONGITUDE:Ljava/lang/String; = "lng"

.field public static final NAME:Ljava/lang/String; = "name"

.field public static final NEXT_PAGE_TOKEN:Ljava/lang/String; = "next_page_token"

.field public static final OPENING_HOURS:Ljava/lang/String; = "opening_hours"

.field public static final OPEN_NOW:Ljava/lang/String; = "open_now"

.field public static final PHOTOS:Ljava/lang/String; = "photos"

.field public static final PHOTO_HEIGHT:Ljava/lang/String; = "height"

.field public static final PHOTO_REFERENCE:Ljava/lang/String; = "photo_reference"

.field public static final PHOTO_WIDTH:Ljava/lang/String; = "width"

.field public static final RATING:Ljava/lang/String; = "rating"

.field public static final REFERENCE:Ljava/lang/String; = "reference"

.field public static final RESULT:Ljava/lang/String; = "result"

.field public static final RESULTS:Ljava/lang/String; = "results"

.field public static final STATUS:Ljava/lang/String; = "status"

.field public static final STATUS_OK:Ljava/lang/String; = "OK"

.field public static final TYPES:Ljava/lang/String; = "types"

.field public static final VICINITY:Ljava/lang/String; = "vicinity"
