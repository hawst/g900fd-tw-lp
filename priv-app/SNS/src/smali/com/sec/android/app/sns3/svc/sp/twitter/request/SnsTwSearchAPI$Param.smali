.class public final enum Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI$Param;
.super Ljava/lang/Enum;
.source "SnsTwSearchAPI.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Param"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI$Param;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI$Param;

.field public static final enum CALLBACK:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI$Param;

.field public static final enum GEOCODE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI$Param;

.field public static final enum LANG:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI$Param;

.field public static final enum LOCALE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI$Param;

.field public static final enum PAGE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI$Param;

.field public static final enum Q:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI$Param;

.field public static final enum RESULT_TYPE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI$Param;

.field public static final enum RPP:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI$Param;

.field public static final enum SHOW_USER:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI$Param;

.field public static final enum SINCE_ID:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI$Param;

.field public static final enum UNTIL:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI$Param;


# instance fields
.field private mParam:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 30
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI$Param;

    const-string v1, "Q"

    const-string v2, "q"

    invoke-direct {v0, v1, v4, v2}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI$Param;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI$Param;->Q:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI$Param;

    .line 31
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI$Param;

    const-string v1, "CALLBACK"

    const-string v2, "callback"

    invoke-direct {v0, v1, v5, v2}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI$Param;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI$Param;->CALLBACK:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI$Param;

    .line 32
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI$Param;

    const-string v1, "GEOCODE"

    const-string v2, "geocode"

    invoke-direct {v0, v1, v6, v2}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI$Param;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI$Param;->GEOCODE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI$Param;

    .line 33
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI$Param;

    const-string v1, "LANG"

    const-string v2, "lang"

    invoke-direct {v0, v1, v7, v2}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI$Param;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI$Param;->LANG:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI$Param;

    .line 34
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI$Param;

    const-string v1, "LOCALE"

    const-string v2, "locale"

    invoke-direct {v0, v1, v8, v2}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI$Param;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI$Param;->LOCALE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI$Param;

    .line 35
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI$Param;

    const-string v1, "PAGE"

    const/4 v2, 0x5

    const-string v3, "page"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI$Param;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI$Param;->PAGE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI$Param;

    .line 36
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI$Param;

    const-string v1, "RESULT_TYPE"

    const/4 v2, 0x6

    const-string v3, "result_type"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI$Param;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI$Param;->RESULT_TYPE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI$Param;

    .line 37
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI$Param;

    const-string v1, "RPP"

    const/4 v2, 0x7

    const-string v3, "rpp"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI$Param;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI$Param;->RPP:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI$Param;

    .line 38
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI$Param;

    const-string v1, "SHOW_USER"

    const/16 v2, 0x8

    const-string v3, "show_user"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI$Param;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI$Param;->SHOW_USER:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI$Param;

    .line 39
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI$Param;

    const-string v1, "UNTIL"

    const/16 v2, 0x9

    const-string v3, "until"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI$Param;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI$Param;->UNTIL:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI$Param;

    .line 40
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI$Param;

    const-string v1, "SINCE_ID"

    const/16 v2, 0xa

    const-string v3, "since_id"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI$Param;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI$Param;->SINCE_ID:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI$Param;

    .line 29
    const/16 v0, 0xb

    new-array v0, v0, [Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI$Param;

    sget-object v1, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI$Param;->Q:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI$Param;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI$Param;->CALLBACK:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI$Param;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI$Param;->GEOCODE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI$Param;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI$Param;->LANG:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI$Param;

    aput-object v1, v0, v7

    sget-object v1, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI$Param;->LOCALE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI$Param;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI$Param;->PAGE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI$Param;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI$Param;->RESULT_TYPE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI$Param;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI$Param;->RPP:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI$Param;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI$Param;->SHOW_USER:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI$Param;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI$Param;->UNTIL:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI$Param;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI$Param;->SINCE_ID:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI$Param;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI$Param;->$VALUES:[Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI$Param;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "param"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 44
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 45
    iput-object p3, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI$Param;->mParam:Ljava/lang/String;

    .line 46
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI$Param;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 29
    const-class v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI$Param;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI$Param;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI$Param;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI$Param;->$VALUES:[Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI$Param;

    invoke-virtual {v0}, [Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI$Param;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI$Param;

    return-object v0
.end method


# virtual methods
.method public getParam()Ljava/lang/String;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI$Param;->mParam:Ljava/lang/String;

    return-object v0
.end method
