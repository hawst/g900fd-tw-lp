.class Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterCallService$SyncAdapterImpl;
.super Landroid/content/AbstractThreadedSyncAdapter;
.source "SnsGpSyncAdapterCallService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterCallService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SyncAdapterImpl"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterCallService;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterCallService;Landroid/content/Context;)V
    .locals 1
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 48
    iput-object p1, p0, Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterCallService$SyncAdapterImpl;->this$0:Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterCallService;

    .line 49
    const/4 v0, 0x1

    invoke-direct {p0, p2, v0}, Landroid/content/AbstractThreadedSyncAdapter;-><init>(Landroid/content/Context;Z)V

    .line 50
    return-void
.end method


# virtual methods
.method public onPerformSync(Landroid/accounts/Account;Landroid/os/Bundle;Ljava/lang/String;Landroid/content/ContentProviderClient;Landroid/content/SyncResult;)V
    .locals 3
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "extras"    # Landroid/os/Bundle;
    .param p3, "authority"    # Ljava/lang/String;
    .param p4, "provider"    # Landroid/content/ContentProviderClient;
    .param p5, "syncResult"    # Landroid/content/SyncResult;

    .prologue
    .line 56
    const-string v1, "SnsGpSync"

    const-string v2, "***************** SnsGpSyncAdapterCallService : onPerformSync!!! *****************"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 59
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterCallService$SyncAdapterImpl;->this$0:Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterCallService;

    # setter for: Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterCallService;->mAccount:Landroid/accounts/Account;
    invoke-static {v1, p1}, Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterCallService;->access$002(Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterCallService;Landroid/accounts/Account;)Landroid/accounts/Account;

    .line 60
    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterCallService$SyncAdapterImpl;->this$0:Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterCallService;

    # setter for: Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterCallService;->mAuthority:Ljava/lang/String;
    invoke-static {v1, p3}, Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterCallService;->access$102(Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterCallService;Ljava/lang/String;)Ljava/lang/String;

    .line 62
    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterCallService$SyncAdapterImpl;->this$0:Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterCallService;

    # invokes: Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterCallService;->performSync()V
    invoke-static {v1}, Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterCallService;->access$200(Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterCallService;)V
    :try_end_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 72
    :goto_0
    return-void

    .line 64
    :catch_0
    move-exception v0

    .line 65
    .local v0, "e":Landroid/accounts/OperationCanceledException;
    const-string v1, "SnsGpSync"

    const-string v2, "SnsGpSyncAdapterCallService : onPerformSync is CANCELED!!!"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 66
    invoke-virtual {v0}, Landroid/accounts/OperationCanceledException;->printStackTrace()V

    goto :goto_0

    .line 67
    .end local v0    # "e":Landroid/accounts/OperationCanceledException;
    :catch_1
    move-exception v0

    .line 68
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "SnsGpSync"

    const-string v2, "SnsGpSyncAdapterCallService : Abnormal Syncing!!!"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 69
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public onSyncCanceled()V
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterCallService$SyncAdapterImpl;->this$0:Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterCallService;

    # invokes: Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterCallService;->onSyncCanceled()V
    invoke-static {v0}, Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterCallService;->access$300(Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterCallService;)V

    .line 77
    return-void
.end method
