.class public interface abstract Lcom/sec/android/app/sns3/agent/sp/twitter/db/SnsTwitterDB$StatusStreamColumns;
.super Ljava/lang/Object;
.source "SnsTwitterDB.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/agent/sp/twitter/db/SnsTwitterDB;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "StatusStreamColumns"
.end annotation


# static fields
.field public static final CREATE_AT:Ljava/lang/String; = "create_at"

.field public static final FAVOURITES_COUNT:Ljava/lang/String; = "favourites_count"

.field public static final LINK:Ljava/lang/String; = "link"

.field public static final MEDIA_URL:Ljava/lang/String; = "media_url"

.field public static final MESSAGE:Ljava/lang/String; = "message"

.field public static final PICTURE:Ljava/lang/String; = "picture"

.field public static final RETWEET_COUNT:Ljava/lang/String; = "retweet_count"

.field public static final TWEET_ID:Ljava/lang/String; = "tweet_id"

.field public static final USER_ID:Ljava/lang/String; = "user_id"

.field public static final USER_NAME:Ljava/lang/String; = "user_name"

.field public static final USER_SCREEN_NAME:Ljava/lang/String; = "user_screen_name"

.field public static final _ID:Ljava/lang/String; = "_id"
