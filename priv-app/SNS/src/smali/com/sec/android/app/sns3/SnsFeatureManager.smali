.class public Lcom/sec/android/app/sns3/SnsFeatureManager;
.super Ljava/lang/Object;
.source "SnsFeatureManager.java"


# static fields
.field public static final ANGLE_INDICATION_SUPPORTED:Ljava/lang/Boolean;

.field private static final CALENDAR_CLASS_ATTENDEES_VIEW:Ljava/lang/String; = "com.android.calendar.detail.MeetingInvitationAttendeesView"

.field private static final CALENDAR_PACKAGE_NAME:Ljava/lang/String; = "com.android.calendar"

.field private static final CONTACTS_CLASS_LINKEDIN_CONTACT:Ljava/lang/String; = "com.samsung.contacts.detail.LinkedContactActivity"

.field private static final CONTACTS_CLASS_SNS_USER_INFO:Ljava/lang/String; = "com.samsung.contacts.detail.SnsUserInfo$LINKEDIN_USER_INFO"

.field private static final CONTACTS_PACKAGE_NAME:Ljava/lang/String; = "com.android.contacts"

.field private static final COUNTRYISO_CHINA:Ljava/lang/String; = "CN"

.field private static final CSC_COUNTRYISO_CODE:Ljava/lang/String; = "ro.csc.countryiso_code"

.field private static final CSC_SALES_CODE:Ljava/lang/String; = "ro.csc.sales_code"

.field private static final INCALLUI_CLASS_CALLER_INFO_CARD:Ljava/lang/String; = "com.android.incallui.callerinfocard.CallerInfoCardUtils"

.field private static final INCALLUI_PACKAGE_NAME:Ljava/lang/String; = "com.android.incallui"

.field private static final MY_PLACES_CLASS_NEARBY_INFO:Ljava/lang/String; = "com.sec.android.widgetapp.locationwidget.nearbyplaces.FoursquareApi"

.field private static final MY_PLACES_PACKAGE_NAME:Ljava/lang/String; = "com.sec.android.widgetapp.locationwidget"

.field private static final PROPERTY_ALL_ACCOUNT:Ljava/lang/String; = "debug.sns.allaccount"

.field private static final SALES_TARGET_CHINA_OPEN:Ljava/lang/String; = "CHC"

.field private static final SALES_TARGET_VERIZON:Ljava/lang/String; = "VZW"

.field private static final SNS_IMAGE_CACHE_PACKAGE_NAME:Ljava/lang/String; = "com.sec.android.app.snsimagecache"

.field private static final TAG:Ljava/lang/String; = "SnsFeatureManager"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mPackageManager:Landroid/content/pm/PackageManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 83
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/sns3/SnsFeatureManager;->ANGLE_INDICATION_SUPPORTED:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 88
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 85
    iput-object v0, p0, Lcom/sec/android/app/sns3/SnsFeatureManager;->mContext:Landroid/content/Context;

    .line 86
    iput-object v0, p0, Lcom/sec/android/app/sns3/SnsFeatureManager;->mPackageManager:Landroid/content/pm/PackageManager;

    .line 89
    iput-object p1, p0, Lcom/sec/android/app/sns3/SnsFeatureManager;->mContext:Landroid/content/Context;

    .line 90
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/SnsFeatureManager;->mPackageManager:Landroid/content/pm/PackageManager;

    .line 91
    return-void
.end method

.method private isAvaiableCalendarProfile()Z
    .locals 7

    .prologue
    .line 189
    const/4 v3, 0x0

    .line 192
    .local v3, "supportLinkedinProfile":Z
    :try_start_0
    iget-object v4, p0, Lcom/sec/android/app/sns3/SnsFeatureManager;->mContext:Landroid/content/Context;

    const-string v5, "com.android.calendar"

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/content/Context;->createPackageContext(Ljava/lang/String;I)Landroid/content/Context;

    move-result-object v0

    .line 193
    .local v0, "calendarApp":Landroid/content/Context;
    if-eqz v0, :cond_0

    .line 194
    new-instance v2, Ldalvik/system/PathClassLoader;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageCodePath()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/sns3/SnsFeatureManager;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v5

    invoke-direct {v2, v4, v5}, Ldalvik/system/PathClassLoader;-><init>(Ljava/lang/String;Ljava/lang/ClassLoader;)V

    .line 197
    .local v2, "pcl":Ldalvik/system/PathClassLoader;
    const-string v4, "com.android.calendar.detail.MeetingInvitationAttendeesView"

    invoke-direct {p0, v2, v4}, Lcom/sec/android/app/sns3/SnsFeatureManager;->isAvailableLoadClass(Ldalvik/system/PathClassLoader;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 198
    const-string v4, "SnsFeatureManager"

    const-string v5, "Calendar profile is supported"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 199
    const/4 v3, 0x1

    .line 206
    .end local v0    # "calendarApp":Landroid/content/Context;
    .end local v2    # "pcl":Ldalvik/system/PathClassLoader;
    :cond_0
    :goto_0
    return v3

    .line 202
    :catch_0
    move-exception v1

    .line 203
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v4, "SnsFeatureManager"

    const-string v5, "can\'t find Calendar package"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private isAvaiableMyPlaces()Z
    .locals 7

    .prologue
    .line 231
    const/4 v3, 0x0

    .line 233
    .local v3, "supportMyPlacesNearby":Z
    :try_start_0
    iget-object v4, p0, Lcom/sec/android/app/sns3/SnsFeatureManager;->mContext:Landroid/content/Context;

    const-string v5, "com.sec.android.widgetapp.locationwidget"

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/content/Context;->createPackageContext(Ljava/lang/String;I)Landroid/content/Context;

    move-result-object v1

    .line 234
    .local v1, "myPlacesApp":Landroid/content/Context;
    if-eqz v1, :cond_0

    .line 235
    new-instance v2, Ldalvik/system/PathClassLoader;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageCodePath()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/sns3/SnsFeatureManager;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v5

    invoke-direct {v2, v4, v5}, Ldalvik/system/PathClassLoader;-><init>(Ljava/lang/String;Ljava/lang/ClassLoader;)V

    .line 238
    .local v2, "pcl":Ldalvik/system/PathClassLoader;
    const-string v4, "com.sec.android.widgetapp.locationwidget.nearbyplaces.FoursquareApi"

    invoke-direct {p0, v2, v4}, Lcom/sec/android/app/sns3/SnsFeatureManager;->isAvailableLoadClass(Ldalvik/system/PathClassLoader;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 239
    const-string v4, "SnsFeatureManager"

    const-string v5, "MP nearby is supported"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 240
    const/4 v3, 0x1

    .line 247
    .end local v1    # "myPlacesApp":Landroid/content/Context;
    .end local v2    # "pcl":Ldalvik/system/PathClassLoader;
    :cond_0
    :goto_0
    return v3

    .line 243
    :catch_0
    move-exception v0

    .line 244
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v4, "SnsFeatureManager"

    const-string v5, "can\'t find My Places package"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private isAvaiableQuickContactProfile()Z
    .locals 9

    .prologue
    .line 160
    const/4 v5, 0x0

    .line 161
    .local v5, "supportLinkedinProfile":Z
    const-string v1, "com.android.contacts"

    .line 162
    .local v1, "contactsPkg":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v6

    const-string v7, "CscFeature_Contact_ReplacePackageAs"

    invoke-virtual {v6, v7}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 164
    .local v4, "replaceContactsPkg":Ljava/lang/String;
    if-eqz v4, :cond_0

    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_0

    .line 165
    move-object v1, v4

    .line 166
    const-string v6, "SnsFeatureManager"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "replace Contacts = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 170
    :cond_0
    :try_start_0
    iget-object v6, p0, Lcom/sec/android/app/sns3/SnsFeatureManager;->mContext:Landroid/content/Context;

    const/4 v7, 0x0

    invoke-virtual {v6, v1, v7}, Landroid/content/Context;->createPackageContext(Ljava/lang/String;I)Landroid/content/Context;

    move-result-object v0

    .line 171
    .local v0, "contactsApp":Landroid/content/Context;
    if-eqz v0, :cond_2

    .line 172
    new-instance v3, Ldalvik/system/PathClassLoader;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageCodePath()Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/sns3/SnsFeatureManager;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v7

    invoke-direct {v3, v6, v7}, Ldalvik/system/PathClassLoader;-><init>(Ljava/lang/String;Ljava/lang/ClassLoader;)V

    .line 175
    .local v3, "pcl":Ldalvik/system/PathClassLoader;
    const-string v6, "com.samsung.contacts.detail.SnsUserInfo$LINKEDIN_USER_INFO"

    invoke-direct {p0, v3, v6}, Lcom/sec/android/app/sns3/SnsFeatureManager;->isAvailableLoadClass(Ldalvik/system/PathClassLoader;Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_1

    const-string v6, "com.samsung.contacts.detail.LinkedContactActivity"

    invoke-direct {p0, v3, v6}, Lcom/sec/android/app/sns3/SnsFeatureManager;->isAvailableLoadClass(Ldalvik/system/PathClassLoader;Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 177
    :cond_1
    const-string v6, "SnsFeatureManager"

    const-string v7, "Contacts profile is supported"

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 178
    const/4 v5, 0x1

    .line 185
    .end local v0    # "contactsApp":Landroid/content/Context;
    .end local v3    # "pcl":Ldalvik/system/PathClassLoader;
    :cond_2
    :goto_0
    return v5

    .line 181
    :catch_0
    move-exception v2

    .line 182
    .local v2, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v6, "SnsFeatureManager"

    const-string v7, "can\'t find Contacts package"

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private isAvailableCallerInfo()Z
    .locals 7

    .prologue
    .line 210
    const/4 v3, 0x0

    .line 213
    .local v3, "supportCallerInfo":Z
    :try_start_0
    iget-object v4, p0, Lcom/sec/android/app/sns3/SnsFeatureManager;->mContext:Landroid/content/Context;

    const-string v5, "com.android.incallui"

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/content/Context;->createPackageContext(Ljava/lang/String;I)Landroid/content/Context;

    move-result-object v1

    .line 214
    .local v1, "incalluiApp":Landroid/content/Context;
    if-eqz v1, :cond_0

    .line 215
    new-instance v2, Ldalvik/system/PathClassLoader;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageCodePath()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/sns3/SnsFeatureManager;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v5

    invoke-direct {v2, v4, v5}, Ldalvik/system/PathClassLoader;-><init>(Ljava/lang/String;Ljava/lang/ClassLoader;)V

    .line 218
    .local v2, "pcl":Ldalvik/system/PathClassLoader;
    const-string v4, "com.android.incallui.callerinfocard.CallerInfoCardUtils"

    invoke-direct {p0, v2, v4}, Lcom/sec/android/app/sns3/SnsFeatureManager;->isAvailableLoadClass(Ldalvik/system/PathClassLoader;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 219
    const-string v4, "SnsFeatureManager"

    const-string v5, "Caller card is supported"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 220
    const/4 v3, 0x1

    .line 227
    .end local v1    # "incalluiApp":Landroid/content/Context;
    .end local v2    # "pcl":Ldalvik/system/PathClassLoader;
    :cond_0
    :goto_0
    return v3

    .line 223
    :catch_0
    move-exception v0

    .line 224
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v4, "SnsFeatureManager"

    const-string v5, "can\'t find InCallUI package"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private isAvailableLoadClass(Ldalvik/system/PathClassLoader;Ljava/lang/String;)Z
    .locals 2
    .param p1, "pcl"    # Ldalvik/system/PathClassLoader;
    .param p2, "className"    # Ljava/lang/String;

    .prologue
    .line 351
    :try_start_0
    invoke-virtual {p1, p2}, Ldalvik/system/PathClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 356
    const/4 v1, 0x1

    :goto_0
    return v1

    .line 352
    :catch_0
    move-exception v0

    .line 353
    .local v0, "e":Ljava/lang/ClassNotFoundException;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isChineseModel()Z
    .locals 2

    .prologue
    .line 156
    const-string v0, "CN"

    const-string v1, "ro.csc.countryiso_code"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private isPackageInstalled(Ljava/lang/String;)Z
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 342
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/sns3/SnsFeatureManager;->mPackageManager:Landroid/content/pm/PackageManager;

    const/16 v2, 0x80

    invoke-virtual {v1, p1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 346
    const/4 v1, 0x1

    :goto_0
    return v1

    .line 343
    :catch_0
    move-exception v0

    .line 344
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isVerizonDevice()Z
    .locals 2

    .prologue
    .line 152
    const-string v0, "VZW"

    const-string v1, "ro.csc.sales_code"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private setComponentEnabled(Landroid/content/ComponentName;Z)V
    .locals 5
    .param p1, "component"    # Landroid/content/ComponentName;
    .param p2, "enable"    # Z

    .prologue
    .line 326
    if-eqz p2, :cond_0

    .line 327
    const/4 v1, 0x1

    .line 333
    .local v1, "newState":I
    :goto_0
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/sns3/SnsFeatureManager;->mPackageManager:Landroid/content/pm/PackageManager;

    const/4 v3, 0x1

    invoke-virtual {v2, p1, v1, v3}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 338
    :goto_1
    return-void

    .line 329
    .end local v1    # "newState":I
    :cond_0
    const/4 v1, 0x2

    .restart local v1    # "newState":I
    goto :goto_0

    .line 335
    :catch_0
    move-exception v0

    .line 336
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    const-string v2, "SnsFeatureManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "IllegalArgumentException : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private setEnableFacebook(Z)V
    .locals 3
    .param p1, "enable"    # Z

    .prologue
    .line 252
    new-instance v0, Landroid/content/ComponentName;

    iget-object v1, p0, Lcom/sec/android/app/sns3/SnsFeatureManager;->mContext:Landroid/content/Context;

    const-class v2, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthenticatorService;

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 253
    .local v0, "component":Landroid/content/ComponentName;
    invoke-direct {p0, v0, p1}, Lcom/sec/android/app/sns3/SnsFeatureManager;->setComponentEnabled(Landroid/content/ComponentName;Z)V

    .line 254
    new-instance v0, Landroid/content/ComponentName;

    .end local v0    # "component":Landroid/content/ComponentName;
    iget-object v1, p0, Lcom/sec/android/app/sns3/SnsFeatureManager;->mContext:Landroid/content/Context;

    const-class v2, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOReceiver;

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 255
    .restart local v0    # "component":Landroid/content/ComponentName;
    invoke-direct {p0, v0, p1}, Lcom/sec/android/app/sns3/SnsFeatureManager;->setComponentEnabled(Landroid/content/ComponentName;Z)V

    .line 256
    new-instance v0, Landroid/content/ComponentName;

    .end local v0    # "component":Landroid/content/ComponentName;
    iget-object v1, p0, Lcom/sec/android/app/sns3/SnsFeatureManager;->mContext:Landroid/content/Context;

    const-class v2, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 257
    .restart local v0    # "component":Landroid/content/ComponentName;
    invoke-direct {p0, v0, p1}, Lcom/sec/android/app/sns3/SnsFeatureManager;->setComponentEnabled(Landroid/content/ComponentName;Z)V

    .line 258
    new-instance v0, Landroid/content/ComponentName;

    .end local v0    # "component":Landroid/content/ComponentName;
    iget-object v1, p0, Lcom/sec/android/app/sns3/SnsFeatureManager;->mContext:Landroid/content/Context;

    const-class v2, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbServiceForAuthToken;

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 259
    .restart local v0    # "component":Landroid/content/ComponentName;
    invoke-direct {p0, v0, p1}, Lcom/sec/android/app/sns3/SnsFeatureManager;->setComponentEnabled(Landroid/content/ComponentName;Z)V

    .line 260
    return-void
.end method

.method private setEnableFoursquare(Z)V
    .locals 3
    .param p1, "enable"    # Z

    .prologue
    .line 296
    new-instance v0, Landroid/content/ComponentName;

    iget-object v1, p0, Lcom/sec/android/app/sns3/SnsFeatureManager;->mContext:Landroid/content/Context;

    const-class v2, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthenticatorService;

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 297
    .local v0, "component":Landroid/content/ComponentName;
    invoke-direct {p0, v0, p1}, Lcom/sec/android/app/sns3/SnsFeatureManager;->setComponentEnabled(Landroid/content/ComponentName;Z)V

    .line 298
    new-instance v0, Landroid/content/ComponentName;

    .end local v0    # "component":Landroid/content/ComponentName;
    iget-object v1, p0, Lcom/sec/android/app/sns3/SnsFeatureManager;->mContext:Landroid/content/Context;

    const-class v2, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthSSOReceiver;

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 299
    .restart local v0    # "component":Landroid/content/ComponentName;
    invoke-direct {p0, v0, p1}, Lcom/sec/android/app/sns3/SnsFeatureManager;->setComponentEnabled(Landroid/content/ComponentName;Z)V

    .line 300
    new-instance v0, Landroid/content/ComponentName;

    .end local v0    # "component":Landroid/content/ComponentName;
    iget-object v1, p0, Lcom/sec/android/app/sns3/SnsFeatureManager;->mContext:Landroid/content/Context;

    const-class v2, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService;

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 301
    .restart local v0    # "component":Landroid/content/ComponentName;
    invoke-direct {p0, v0, p1}, Lcom/sec/android/app/sns3/SnsFeatureManager;->setComponentEnabled(Landroid/content/ComponentName;Z)V

    .line 302
    new-instance v0, Landroid/content/ComponentName;

    .end local v0    # "component":Landroid/content/ComponentName;
    iget-object v1, p0, Lcom/sec/android/app/sns3/SnsFeatureManager;->mContext:Landroid/content/Context;

    const-class v2, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsServiceForAuthToken;

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 303
    .restart local v0    # "component":Landroid/content/ComponentName;
    invoke-direct {p0, v0, p1}, Lcom/sec/android/app/sns3/SnsFeatureManager;->setComponentEnabled(Landroid/content/ComponentName;Z)V

    .line 304
    return-void
.end method

.method private setEnableGooglePlus(Z)V
    .locals 3
    .param p1, "enable"    # Z

    .prologue
    .line 284
    new-instance v0, Landroid/content/ComponentName;

    iget-object v1, p0, Lcom/sec/android/app/sns3/SnsFeatureManager;->mContext:Landroid/content/Context;

    const-class v2, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthenticatorService;

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 285
    .local v0, "component":Landroid/content/ComponentName;
    invoke-direct {p0, v0, p1}, Lcom/sec/android/app/sns3/SnsFeatureManager;->setComponentEnabled(Landroid/content/ComponentName;Z)V

    .line 286
    new-instance v0, Landroid/content/ComponentName;

    .end local v0    # "component":Landroid/content/ComponentName;
    iget-object v1, p0, Lcom/sec/android/app/sns3/SnsFeatureManager;->mContext:Landroid/content/Context;

    const-class v2, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOReceiver;

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 287
    .restart local v0    # "component":Landroid/content/ComponentName;
    invoke-direct {p0, v0, p1}, Lcom/sec/android/app/sns3/SnsFeatureManager;->setComponentEnabled(Landroid/content/ComponentName;Z)V

    .line 288
    new-instance v0, Landroid/content/ComponentName;

    .end local v0    # "component":Landroid/content/ComponentName;
    iget-object v1, p0, Lcom/sec/android/app/sns3/SnsFeatureManager;->mContext:Landroid/content/Context;

    const-class v2, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpService;

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 289
    .restart local v0    # "component":Landroid/content/ComponentName;
    invoke-direct {p0, v0, p1}, Lcom/sec/android/app/sns3/SnsFeatureManager;->setComponentEnabled(Landroid/content/ComponentName;Z)V

    .line 290
    new-instance v0, Landroid/content/ComponentName;

    .end local v0    # "component":Landroid/content/ComponentName;
    iget-object v1, p0, Lcom/sec/android/app/sns3/SnsFeatureManager;->mContext:Landroid/content/Context;

    const-class v2, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpServiceForAuthToken;

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 291
    .restart local v0    # "component":Landroid/content/ComponentName;
    invoke-direct {p0, v0, p1}, Lcom/sec/android/app/sns3/SnsFeatureManager;->setComponentEnabled(Landroid/content/ComponentName;Z)V

    .line 292
    return-void
.end method

.method private setEnableInstagram(Z)V
    .locals 3
    .param p1, "enable"    # Z

    .prologue
    .line 308
    new-instance v0, Landroid/content/ComponentName;

    iget-object v1, p0, Lcom/sec/android/app/sns3/SnsFeatureManager;->mContext:Landroid/content/Context;

    const-class v2, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInAuthenticatorService;

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 309
    .local v0, "component":Landroid/content/ComponentName;
    invoke-direct {p0, v0, p1}, Lcom/sec/android/app/sns3/SnsFeatureManager;->setComponentEnabled(Landroid/content/ComponentName;Z)V

    .line 310
    return-void
.end method

.method private setEnableLinkedin(Z)V
    .locals 3
    .param p1, "enable"    # Z

    .prologue
    .line 276
    new-instance v0, Landroid/content/ComponentName;

    iget-object v1, p0, Lcom/sec/android/app/sns3/SnsFeatureManager;->mContext:Landroid/content/Context;

    const-class v2, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthenticatorService;

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 277
    .local v0, "component":Landroid/content/ComponentName;
    invoke-direct {p0, v0, p1}, Lcom/sec/android/app/sns3/SnsFeatureManager;->setComponentEnabled(Landroid/content/ComponentName;Z)V

    .line 278
    new-instance v0, Landroid/content/ComponentName;

    .end local v0    # "component":Landroid/content/ComponentName;
    iget-object v1, p0, Lcom/sec/android/app/sns3/SnsFeatureManager;->mContext:Landroid/content/Context;

    const-class v2, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOReceiver;

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 279
    .restart local v0    # "component":Landroid/content/ComponentName;
    invoke-direct {p0, v0, p1}, Lcom/sec/android/app/sns3/SnsFeatureManager;->setComponentEnabled(Landroid/content/ComponentName;Z)V

    .line 280
    return-void
.end method

.method private setEnableQzone(Z)V
    .locals 3
    .param p1, "enable"    # Z

    .prologue
    .line 314
    new-instance v0, Landroid/content/ComponentName;

    iget-object v1, p0, Lcom/sec/android/app/sns3/SnsFeatureManager;->mContext:Landroid/content/Context;

    const-class v2, Lcom/sec/android/app/sns3/auth/sp/qzone/SnsAccountQzAuthenticatorService;

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 315
    .local v0, "component":Landroid/content/ComponentName;
    invoke-direct {p0, v0, p1}, Lcom/sec/android/app/sns3/SnsFeatureManager;->setComponentEnabled(Landroid/content/ComponentName;Z)V

    .line 316
    return-void
.end method

.method private setEnableSinaweibo(Z)V
    .locals 3
    .param p1, "enable"    # Z

    .prologue
    .line 320
    new-instance v0, Landroid/content/ComponentName;

    iget-object v1, p0, Lcom/sec/android/app/sns3/SnsFeatureManager;->mContext:Landroid/content/Context;

    const-class v2, Lcom/sec/android/app/sns3/auth/sp/sinaweibo/SnsAccountSwAuthenticatorService;

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 321
    .local v0, "component":Landroid/content/ComponentName;
    invoke-direct {p0, v0, p1}, Lcom/sec/android/app/sns3/SnsFeatureManager;->setComponentEnabled(Landroid/content/ComponentName;Z)V

    .line 322
    return-void
.end method

.method private setEnableTwitter(Z)V
    .locals 3
    .param p1, "enable"    # Z

    .prologue
    .line 264
    new-instance v0, Landroid/content/ComponentName;

    iget-object v1, p0, Lcom/sec/android/app/sns3/SnsFeatureManager;->mContext:Landroid/content/Context;

    const-class v2, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthenticatorService;

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 265
    .local v0, "component":Landroid/content/ComponentName;
    invoke-direct {p0, v0, p1}, Lcom/sec/android/app/sns3/SnsFeatureManager;->setComponentEnabled(Landroid/content/ComponentName;Z)V

    .line 266
    new-instance v0, Landroid/content/ComponentName;

    .end local v0    # "component":Landroid/content/ComponentName;
    iget-object v1, p0, Lcom/sec/android/app/sns3/SnsFeatureManager;->mContext:Landroid/content/Context;

    const-class v2, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthSSOReceiver;

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 267
    .restart local v0    # "component":Landroid/content/ComponentName;
    invoke-direct {p0, v0, p1}, Lcom/sec/android/app/sns3/SnsFeatureManager;->setComponentEnabled(Landroid/content/ComponentName;Z)V

    .line 268
    new-instance v0, Landroid/content/ComponentName;

    .end local v0    # "component":Landroid/content/ComponentName;
    iget-object v1, p0, Lcom/sec/android/app/sns3/SnsFeatureManager;->mContext:Landroid/content/Context;

    const-class v2, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService;

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 269
    .restart local v0    # "component":Landroid/content/ComponentName;
    invoke-direct {p0, v0, p1}, Lcom/sec/android/app/sns3/SnsFeatureManager;->setComponentEnabled(Landroid/content/ComponentName;Z)V

    .line 270
    new-instance v0, Landroid/content/ComponentName;

    .end local v0    # "component":Landroid/content/ComponentName;
    iget-object v1, p0, Lcom/sec/android/app/sns3/SnsFeatureManager;->mContext:Landroid/content/Context;

    const-class v2, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwServiceForAuthToken;

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 271
    .restart local v0    # "component":Landroid/content/ComponentName;
    invoke-direct {p0, v0, p1}, Lcom/sec/android/app/sns3/SnsFeatureManager;->setComponentEnabled(Landroid/content/ComponentName;Z)V

    .line 272
    return-void
.end method


# virtual methods
.method public checkAccounts()V
    .locals 12

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 94
    const-string v8, "true"

    const-string v11, "debug.sns.allaccount"

    invoke-static {v11}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v8, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    .line 96
    .local v2, "supportAllAccount":Z
    invoke-direct {p0}, Lcom/sec/android/app/sns3/SnsFeatureManager;->isAvaiableQuickContactProfile()Z

    move-result v7

    .line 97
    .local v7, "supportQuickContact":Z
    invoke-direct {p0}, Lcom/sec/android/app/sns3/SnsFeatureManager;->isAvaiableCalendarProfile()Z

    move-result v3

    .line 98
    .local v3, "supportCalendar":Z
    invoke-direct {p0}, Lcom/sec/android/app/sns3/SnsFeatureManager;->isAvailableCallerInfo()Z

    move-result v4

    .line 99
    .local v4, "supportCallerInfo":Z
    invoke-direct {p0}, Lcom/sec/android/app/sns3/SnsFeatureManager;->isAvaiableMyPlaces()Z

    move-result v6

    .line 101
    .local v6, "supportMyPlaces":Z
    invoke-static {}, Lcom/sec/android/app/sns3/SnsFeatureManager;->isChineseModel()Z

    move-result v8

    if-eqz v8, :cond_6

    .line 102
    const/4 v5, 0x0

    .line 103
    .local v5, "supportLinkedinForChina":Z
    if-nez v7, :cond_0

    if-eqz v3, :cond_1

    :cond_0
    sget v8, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v11, 0x13

    if-le v8, v11, :cond_1

    .line 106
    const-string v8, "ro.csc.sales_code"

    invoke-static {v8}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 107
    .local v1, "salesCode":Ljava/lang/String;
    const-string v8, "ro.product.name"

    invoke-static {v8}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 108
    .local v0, "productName":Ljava/lang/String;
    const-string v8, "CHC"

    invoke-virtual {v8, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    if-eqz v0, :cond_1

    const-string v8, "zero"

    invoke-virtual {v0, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 110
    const/4 v5, 0x1

    .line 114
    .end local v0    # "productName":Ljava/lang/String;
    .end local v1    # "salesCode":Ljava/lang/String;
    :cond_1
    if-nez v2, :cond_4

    move v8, v9

    :goto_0
    invoke-direct {p0, v8}, Lcom/sec/android/app/sns3/SnsFeatureManager;->setEnableFacebook(Z)V

    .line 115
    if-nez v2, :cond_5

    move v8, v9

    :goto_1
    invoke-direct {p0, v8}, Lcom/sec/android/app/sns3/SnsFeatureManager;->setEnableTwitter(Z)V

    .line 116
    if-nez v2, :cond_2

    if-eqz v5, :cond_3

    :cond_2
    move v9, v10

    :cond_3
    invoke-direct {p0, v9}, Lcom/sec/android/app/sns3/SnsFeatureManager;->setEnableLinkedin(Z)V

    .line 117
    invoke-direct {p0, v2}, Lcom/sec/android/app/sns3/SnsFeatureManager;->setEnableQzone(Z)V

    .line 118
    invoke-direct {p0, v2}, Lcom/sec/android/app/sns3/SnsFeatureManager;->setEnableSinaweibo(Z)V

    .line 125
    .end local v5    # "supportLinkedinForChina":Z
    :goto_2
    return-void

    .restart local v5    # "supportLinkedinForChina":Z
    :cond_4
    move v8, v10

    .line 114
    goto :goto_0

    :cond_5
    move v8, v10

    .line 115
    goto :goto_1

    .line 120
    .end local v5    # "supportLinkedinForChina":Z
    :cond_6
    if-nez v2, :cond_7

    if-nez v7, :cond_7

    if-eqz v3, :cond_b

    :cond_7
    move v8, v10

    :goto_3
    invoke-direct {p0, v8}, Lcom/sec/android/app/sns3/SnsFeatureManager;->setEnableLinkedin(Z)V

    .line 121
    if-nez v2, :cond_8

    if-eqz v4, :cond_c

    :cond_8
    move v8, v10

    :goto_4
    invoke-direct {p0, v8}, Lcom/sec/android/app/sns3/SnsFeatureManager;->setEnableGooglePlus(Z)V

    .line 122
    if-nez v2, :cond_9

    if-eqz v6, :cond_a

    :cond_9
    move v9, v10

    :cond_a
    invoke-direct {p0, v9}, Lcom/sec/android/app/sns3/SnsFeatureManager;->setEnableFoursquare(Z)V

    .line 123
    invoke-direct {p0, v2}, Lcom/sec/android/app/sns3/SnsFeatureManager;->setEnableInstagram(Z)V

    goto :goto_2

    :cond_b
    move v8, v9

    .line 120
    goto :goto_3

    :cond_c
    move v8, v9

    .line 121
    goto :goto_4
.end method

.method public isSnsImageCacheAvailable()Z
    .locals 1

    .prologue
    .line 148
    const-string v0, "com.sec.android.app.snsimagecache"

    invoke-direct {p0, v0}, Lcom/sec/android/app/sns3/SnsFeatureManager;->isPackageInstalled(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public isSyncAdapterForCallNeeded()Z
    .locals 1

    .prologue
    .line 144
    const/4 v0, 0x1

    return v0
.end method

.method public isSyncAdapterForHomeFeedsNeeded()Z
    .locals 1

    .prologue
    .line 132
    const/4 v0, 0x0

    return v0
.end method

.method public isSyncAdapterForProfileFeedsNeeded()Z
    .locals 1

    .prologue
    .line 128
    const/4 v0, 0x0

    return v0
.end method

.method public isSyncAdapterForProfilesNeeded()Z
    .locals 1

    .prologue
    .line 136
    const/4 v0, 0x0

    return v0
.end method

.method public isSyncAdapterForStreamsNeeded()Z
    .locals 1

    .prologue
    .line 140
    const/4 v0, 0x0

    return v0
.end method
