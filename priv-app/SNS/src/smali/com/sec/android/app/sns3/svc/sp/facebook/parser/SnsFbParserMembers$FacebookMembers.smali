.class public interface abstract Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserMembers$FacebookMembers;
.super Ljava/lang/Object;
.source "SnsFbParserMembers.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserMembers;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "FacebookMembers"
.end annotation


# static fields
.field public static final DATA:Ljava/lang/String; = "data"

.field public static final GROUP_ID:Ljava/lang/String; = "id"

.field public static final GROUP_NAME:Ljava/lang/String; = "name"

.field public static final GROUP_TYPE:Ljava/lang/String; = "list_type"

.field public static final ID:Ljava/lang/String; = "id"

.field public static final MEMBERS:Ljava/lang/String; = "members"

.field public static final NAME:Ljava/lang/String; = "name"
