.class Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity$2;
.super Ljava/lang/Object;
.source "SnsAccountLiAuthSSOActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;)V
    .locals 0

    .prologue
    .line 172
    iput-object p1, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity$2;->this$0:Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 175
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity$2;->this$0:Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 176
    const-string v0, "SnsAccountLiAuthSSOActivity"

    const-string v1, "Watchdog : finish"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 177
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity$2;->this$0:Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;

    # invokes: Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->isLoggedInLinkedin()Z
    invoke-static {v0}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->access$200(Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 178
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity$2;->this$0:Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.sec.android.app.sns3.auth.sp.linkedin.SNS_LOGIN_LINKEDIN_NOTIFICATION"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 181
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity$2;->this$0:Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->finish()V

    .line 185
    :goto_0
    return-void

    .line 183
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity$2;->this$0:Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;

    # getter for: Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->access$400(Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity$2;->this$0:Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;

    # getter for: Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->mWatchdogRunnable:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->access$300(Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;)Ljava/lang/Runnable;

    move-result-object v1

    const-wide/16 v2, 0x2710

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method
