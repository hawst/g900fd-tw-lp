.class public Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;
.super Landroid/accounts/AccountAuthenticatorActivity;
.source "SnsAccountGpAuthActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity$getAccessTokenTask;,
        Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity$UIDialogThreadRunnable;
    }
.end annotation


# static fields
.field private static final CONNECTION_TIMEOUT:I = 0x2

.field private static final LOGIN_FAIL_BY_ANOTHER:I = 0x0

.field private static final NETWORK_UNAVAILABLE:I = 0x1

.field private static final REQUEST_SYNC_INTERVAL:I = 0x3e8

.field private static TAG:Ljava/lang/String;


# instance fields
.field private mAccessToken:Ljava/lang/String;

.field mAccountManagerCallback:Landroid/accounts/AccountManagerCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/accounts/AccountManagerCallback",
            "<",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation
.end field

.field mAccountManagerRemoveCallback:Landroid/accounts/AccountManagerCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/accounts/AccountManagerCallback",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private mAppContext:Landroid/content/Context;

.field private mCode:Ljava/lang/String;

.field private mHandler:Landroid/os/Handler;

.field private mRefreshToken:Ljava/lang/String;

.field private mRetryLogin:Z

.field private mSkipSyncSetup:Z

.field private mWebView:Landroid/webkit/WebView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 73
    const-string v0, "SnsAccountGpAuthActivity"

    sput-object v0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 71
    invoke-direct {p0}, Landroid/accounts/AccountAuthenticatorActivity;-><init>()V

    .line 83
    iput-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->mAppContext:Landroid/content/Context;

    .line 87
    iput-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->mHandler:Landroid/os/Handler;

    .line 89
    iput-boolean v1, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->mRetryLogin:Z

    .line 91
    iput-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->mAccessToken:Ljava/lang/String;

    .line 93
    iput-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->mRefreshToken:Ljava/lang/String;

    .line 95
    iput-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->mCode:Ljava/lang/String;

    .line 97
    iput-boolean v1, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->mSkipSyncSetup:Z

    .line 99
    new-instance v0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity$1;-><init>(Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->mAccountManagerCallback:Landroid/accounts/AccountManagerCallback;

    .line 158
    new-instance v0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity$2;-><init>(Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->mAccountManagerRemoveCallback:Landroid/accounts/AccountManagerCallback;

    .line 569
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;

    .prologue
    .line 71
    iget-boolean v0, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->mSkipSyncSetup:Z

    return v0
.end method

.method static synthetic access$100()Ljava/lang/String;
    .locals 1

    .prologue
    .line 71
    sget-object v0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;

    .prologue
    .line 71
    iget-boolean v0, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->mRetryLogin:Z

    return v0
.end method

.method static synthetic access$202(Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 71
    iput-boolean p1, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->mRetryLogin:Z

    return p1
.end method

.method static synthetic access$300(Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->mAccessToken:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$302(Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 71
    iput-object p1, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->mAccessToken:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$400(Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->mRefreshToken:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$402(Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 71
    iput-object p1, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->mRefreshToken:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$500(Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->mAppContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->mCode:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$602(Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 71
    iput-object p1, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->mCode:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$800(Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;Landroid/content/Context;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;
    .param p1, "x1"    # Landroid/content/Context;
    .param p2, "x2"    # I

    .prologue
    .line 71
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->handleError(Landroid/content/Context;I)V

    return-void
.end method

.method private checkNetwork(Landroid/content/Context;)Z
    .locals 5
    .param p1, "mAppContext"    # Landroid/content/Context;

    .prologue
    .line 534
    const-string v3, "connectivity"

    invoke-virtual {p1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/ConnectivityManager;

    .line 536
    .local v1, "cm":Landroid/net/ConnectivityManager;
    const/4 v0, 0x0

    .line 538
    .local v0, "bNetworkStatus":Z
    const/4 v2, 0x0

    .line 540
    .local v2, "netInfo":Landroid/net/NetworkInfo;
    invoke-virtual {v1}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 541
    invoke-virtual {v2}, Landroid/net/NetworkInfo;->isAvailable()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 543
    sget-object v3, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->TAG:Ljava/lang/String;

    const-string v4, "Checked that network is available!"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 545
    invoke-virtual {v2}, Landroid/net/NetworkInfo;->isConnectedOrConnecting()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 546
    sget-object v3, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->TAG:Ljava/lang/String;

    const-string v4, "Checked that network is now connected or connecting!"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 547
    const/4 v0, 0x1

    .line 552
    :cond_0
    return v0
.end method

.method private handleError(Landroid/content/Context;I)V
    .locals 4
    .param p1, "mContext"    # Landroid/content/Context;
    .param p2, "errorCode"    # I

    .prologue
    .line 648
    sget-object v1, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "webview errorcode="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 649
    const/4 v1, -0x8

    if-ne p2, v1, :cond_0

    .line 650
    new-instance v0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity$UIDialogThreadRunnable;

    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->mAppContext:Landroid/content/Context;

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity$UIDialogThreadRunnable;-><init>(Landroid/content/Context;I)V

    .line 652
    .local v0, "connectionTimeout":Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity$UIDialogThreadRunnable;
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 654
    .end local v0    # "connectionTimeout":Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity$UIDialogThreadRunnable;
    :cond_0
    return-void
.end method


# virtual methods
.method public getUniqueString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 566
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public loginFail(IILjava/lang/String;)V
    .locals 4
    .param p1, "errCode"    # I
    .param p2, "subErrCode"    # I
    .param p3, "reason"    # Ljava/lang/String;

    .prologue
    .line 479
    iget-object v2, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->mAppContext:Landroid/content/Context;

    if-eqz v2, :cond_0

    .line 484
    const-string v2, "Network unreachable"

    invoke-virtual {v2, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 485
    new-instance v1, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity$UIDialogThreadRunnable;

    iget-object v2, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->mAppContext:Landroid/content/Context;

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity$UIDialogThreadRunnable;-><init>(Landroid/content/Context;I)V

    .line 487
    .local v1, "networkFail":Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity$UIDialogThreadRunnable;
    iget-object v2, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 496
    .end local v1    # "networkFail":Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity$UIDialogThreadRunnable;
    :cond_0
    :goto_0
    return-void

    .line 489
    :cond_1
    new-instance v0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity$UIDialogThreadRunnable;

    iget-object v2, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->mAppContext:Landroid/content/Context;

    const/4 v3, 0x0

    invoke-direct {v0, v2, v3}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity$UIDialogThreadRunnable;-><init>(Landroid/content/Context;I)V

    .line 491
    .local v0, "another":Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity$UIDialogThreadRunnable;
    iget-object v2, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public loginSuccess(Ljava/lang/String;Ljava/lang/String;)V
    .locals 10
    .param p1, "userName"    # Ljava/lang/String;
    .param p2, "id"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 452
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 453
    .local v4, "options":Landroid/os/Bundle;
    invoke-static {}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->isLoggable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 454
    sget-object v0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SnsAccountGpAuthActivity : addAccount() :  userName = "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 457
    :cond_0
    const-string v0, "username"

    invoke-virtual {v4, v0, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 459
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->mAppContext:Landroid/content/Context;

    if-nez v0, :cond_1

    .line 460
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    const-string v1, "com.sec.android.app.sns3.googleplus"

    move-object v3, v2

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Landroid/accounts/AccountManager;->addAccount(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Landroid/os/Bundle;Landroid/app/Activity;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    .line 468
    :goto_0
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/SnsApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    .line 469
    .local v8, "cr":Landroid/content/ContentResolver;
    new-instance v9, Landroid/content/ContentValues;

    invoke-direct {v9}, Landroid/content/ContentValues;-><init>()V

    .line 471
    .local v9, "values":Landroid/content/ContentValues;
    invoke-virtual {v9}, Landroid/content/ContentValues;->clear()V

    .line 473
    const-string v0, "user_id"

    invoke-virtual {v9, v0, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 474
    const-string v0, "user_name"

    invoke-virtual {v9, v0, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 475
    sget-object v0, Lcom/sec/android/app/sns3/agent/sp/googleplus/db/SnsGooglePlusDB$UserGpBasicInfo;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v8, v0, v9}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 476
    return-void

    .line 463
    .end local v8    # "cr":Landroid/content/ContentResolver;
    .end local v9    # "values":Landroid/content/ContentValues;
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->mAppContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    const-string v1, "com.sec.android.app.sns3.googleplus"

    iget-object v6, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->mAccountManagerCallback:Landroid/accounts/AccountManagerCallback;

    move-object v3, v2

    move-object v5, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Landroid/accounts/AccountManager;->addAccount(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Landroid/os/Bundle;Landroid/app/Activity;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    goto :goto_0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "result"    # Landroid/content/Intent;

    .prologue
    .line 323
    const/16 v0, 0x3e8

    if-ne p1, v0, :cond_0

    .line 324
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->finish()V

    .line 326
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 12
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const v11, 0x7f080023

    const/4 v8, 0x2

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 201
    invoke-static {p0}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->isLightTheme(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_0

    const v5, 0x103012b

    :goto_0
    invoke-virtual {p0, v5}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->setTheme(I)V

    .line 204
    invoke-super {p0, p1}, Landroid/accounts/AccountAuthenticatorActivity;->onCreate(Landroid/os/Bundle;)V

    .line 206
    iput-object p0, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->mAppContext:Landroid/content/Context;

    .line 207
    new-instance v5, Landroid/os/Handler;

    invoke-direct {v5}, Landroid/os/Handler;-><init>()V

    iput-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->mHandler:Landroid/os/Handler;

    .line 209
    invoke-virtual {p0, v8}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->requestWindowFeature(I)Z

    .line 211
    const v5, 0x7f080046

    new-array v6, v10, [Ljava/lang/Object;

    invoke-virtual {p0, v11}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-virtual {p0, v5, v6}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 213
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    const-string v6, "RetryLogin"

    invoke-virtual {v5, v6, v9}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    iput-boolean v5, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->mRetryLogin:Z

    .line 215
    iput-boolean v10, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->mSkipSyncSetup:Z

    .line 217
    iget-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->mAppContext:Landroid/content/Context;

    invoke-static {v5}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v5

    const-string v6, "com.sec.android.app.sns3.googleplus"

    invoke-virtual {v5, v6}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 220
    .local v0, "accounts":[Landroid/accounts/Account;
    array-length v5, v0

    if-lez v5, :cond_1

    iget-boolean v5, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->mRetryLogin:Z

    if-nez v5, :cond_1

    .line 221
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    const/high16 v6, 0x7f080000

    new-array v7, v10, [Ljava/lang/Object;

    invoke-virtual {p0, v11}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->getString(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v9

    invoke-virtual {p0, v6, v7}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6, v10}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    .line 224
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->finish()V

    .line 320
    :goto_1
    return-void

    .line 201
    .end local v0    # "accounts":[Landroid/accounts/Account;
    :cond_0
    const v5, 0x1030128

    goto :goto_0

    .line 228
    .restart local v0    # "accounts":[Landroid/accounts/Account;
    :cond_1
    iget-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->mAppContext:Landroid/content/Context;

    invoke-direct {p0, v5}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->checkNetwork(Landroid/content/Context;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 229
    new-instance v2, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity$UIDialogThreadRunnable;

    iget-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->mAppContext:Landroid/content/Context;

    invoke-direct {v2, v5, v10}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity$UIDialogThreadRunnable;-><init>(Landroid/content/Context;I)V

    .line 231
    .local v2, "networkUnavailable":Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity$UIDialogThreadRunnable;
    iget-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->mAppContext:Landroid/content/Context;

    check-cast v5, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;

    invoke-virtual {v5, v2}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_1

    .line 235
    .end local v2    # "networkUnavailable":Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity$UIDialogThreadRunnable;
    :cond_2
    const-string v5, "notification"

    invoke-virtual {p0, v5}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/NotificationManager;

    .line 236
    .local v3, "notiMgr":Landroid/app/NotificationManager;
    const/16 v5, 0x1450

    invoke-virtual {v3, v5}, Landroid/app/NotificationManager;->cancel(I)V

    .line 237
    const/16 v5, 0x14b4

    invoke-virtual {v3, v5}, Landroid/app/NotificationManager;->cancel(I)V

    .line 239
    new-instance v5, Landroid/webkit/WebView;

    iget-object v6, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->mAppContext:Landroid/content/Context;

    invoke-direct {v5, v6}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;)V

    iput-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->mWebView:Landroid/webkit/WebView;

    .line 241
    iget-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v5}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v5

    invoke-virtual {v5, v9}, Landroid/webkit/WebSettings;->setAllowFileAccess(Z)V

    .line 242
    iget-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v5}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v5

    invoke-virtual {v5, v9}, Landroid/webkit/WebSettings;->setAppCacheEnabled(Z)V

    .line 243
    iget-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v5}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v5

    invoke-virtual {v5, v8}, Landroid/webkit/WebSettings;->setCacheMode(I)V

    .line 244
    iget-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v5}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v5

    invoke-virtual {v5, v9}, Landroid/webkit/WebSettings;->setDatabaseEnabled(Z)V

    .line 245
    iget-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v5}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v5

    invoke-virtual {v5, v9}, Landroid/webkit/WebSettings;->setSaveFormData(Z)V

    .line 246
    iget-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v5}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v5

    invoke-virtual {v5, v10}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 250
    iget-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->mAppContext:Landroid/content/Context;

    invoke-static {v5}, Landroid/webkit/CookieSyncManager;->createInstance(Landroid/content/Context;)Landroid/webkit/CookieSyncManager;

    .line 251
    invoke-static {}, Landroid/webkit/CookieManager;->getInstance()Landroid/webkit/CookieManager;

    move-result-object v1

    .line 252
    .local v1, "cookieManager":Landroid/webkit/CookieManager;
    invoke-virtual {v1}, Landroid/webkit/CookieManager;->removeAllCookie()V

    .line 254
    iget-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->mWebView:Landroid/webkit/WebView;

    new-instance v6, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity$3;

    invoke-direct {v6, p0}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity$3;-><init>(Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;)V

    invoke-virtual {v5, v6}, Landroid/webkit/WebView;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    .line 260
    iget-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->mWebView:Landroid/webkit/WebView;

    new-instance v6, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity$4;

    invoke-direct {v6, p0}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity$4;-><init>(Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;)V

    invoke-virtual {v5, v6}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 311
    iget-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {p0, v5}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->setContentView(Landroid/view/View;)V

    .line 313
    new-instance v5, Lcom/google/api/client/googleapis/auth/oauth2/draft10/GoogleAuthorizationRequestUrl;

    sget-object v6, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGooglePlus;->CLIENT_ID:Ljava/lang/String;

    const-string v7, "http://localhost"

    const-string v8, "https://www.googleapis.com/auth/plus.login"

    invoke-direct {v5, v6, v7, v8}, Lcom/google/api/client/googleapis/auth/oauth2/draft10/GoogleAuthorizationRequestUrl;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5}, Lcom/google/api/client/googleapis/auth/oauth2/draft10/GoogleAuthorizationRequestUrl;->build()Ljava/lang/String;

    move-result-object v4

    .line 316
    .local v4, "url":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->isLoggable()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 317
    sget-object v5, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "loadUrl : url = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 319
    :cond_3
    iget-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v5, v4}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    goto/16 :goto_1
.end method

.method public onCreateDialog(I)Landroid/app/Dialog;
    .locals 3
    .param p1, "id"    # I

    .prologue
    const v2, 0x7f080034

    const v1, 0x7f080031

    .line 359
    packed-switch p1, :pswitch_data_0

    .line 442
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 361
    :pswitch_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f080032

    invoke-virtual {p0, v1}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity$6;

    invoke-direct {v1, p0}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity$6;-><init>(Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity$5;

    invoke-direct {v1, p0}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity$5;-><init>(Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;)V

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    .line 388
    :pswitch_1
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f080033

    invoke-virtual {p0, v1}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity$8;

    invoke-direct {v1, p0}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity$8;-><init>(Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity$7;

    invoke-direct {v1, p0}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity$7;-><init>(Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;)V

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    .line 415
    :pswitch_2
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f08001f

    invoke-virtual {p0, v1}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity$10;

    invoke-direct {v1, p0}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity$10;-><init>(Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity$9;

    invoke-direct {v1, p0}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity$9;-><init>(Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;)V

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto/16 :goto_0

    .line 359
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 447
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->mAppContext:Landroid/content/Context;

    .line 448
    invoke-super {p0}, Landroid/accounts/AccountAuthenticatorActivity;->onDestroy()V

    .line 449
    return-void
.end method

.method public onPrepareDialog(ILandroid/app/Dialog;)V
    .locals 4
    .param p1, "id"    # I
    .param p2, "dialog"    # Landroid/app/Dialog;

    .prologue
    .line 330
    packed-switch p1, :pswitch_data_0

    .line 355
    :cond_0
    :goto_0
    return-void

    .line 332
    :pswitch_0
    instance-of v3, p2, Landroid/app/AlertDialog;

    if-eqz v3, :cond_0

    move-object v0, p2

    .line 333
    check-cast v0, Landroid/app/AlertDialog;

    .line 334
    .local v0, "alert":Landroid/app/AlertDialog;
    const v3, 0x7f080032

    invoke-virtual {p0, v3}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 339
    .end local v0    # "alert":Landroid/app/AlertDialog;
    :pswitch_1
    instance-of v3, p2, Landroid/app/AlertDialog;

    if-eqz v3, :cond_0

    move-object v1, p2

    .line 340
    check-cast v1, Landroid/app/AlertDialog;

    .line 341
    .local v1, "alert1":Landroid/app/AlertDialog;
    const v3, 0x7f080033

    invoke-virtual {p0, v3}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 346
    .end local v1    # "alert1":Landroid/app/AlertDialog;
    :pswitch_2
    instance-of v3, p2, Landroid/app/AlertDialog;

    if-eqz v3, :cond_0

    move-object v2, p2

    .line 347
    check-cast v2, Landroid/app/AlertDialog;

    .line 348
    .local v2, "timeout":Landroid/app/AlertDialog;
    const v3, 0x7f08001f

    invoke-virtual {p0, v3}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 330
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public setAuthTokenNExpires(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3
    .param p1, "accessToken"    # Ljava/lang/String;
    .param p2, "refreshToken"    # Ljava/lang/String;

    .prologue
    .line 556
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 557
    :cond_0
    const/4 v1, 0x0

    .line 562
    :goto_0
    return v1

    .line 559
    :cond_1
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/sns3/SnsApplication;->getSvcMgr()Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/sns3/svc/SnsSvcMgr;->getTokenMgr()Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;

    move-result-object v1

    const-string v2, "googleplus"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;->getToken(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpToken;

    .line 561
    .local v0, "GpToken":Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpToken;
    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpToken;->setAccessTokenNExpires(Ljava/lang/String;Ljava/lang/String;)V

    .line 562
    const/4 v1, 0x1

    goto :goto_0
.end method
