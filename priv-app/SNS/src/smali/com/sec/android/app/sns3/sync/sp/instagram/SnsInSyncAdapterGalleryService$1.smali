.class Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService$1;
.super Lcom/sec/android/app/sns3/svc/sp/instagram/request/SnsInReqGetFeed;
.source "SnsInSyncAdapterGalleryService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService;->performSync()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService;


# direct methods
.method constructor <init>(Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 0
    .param p2, "x0"    # Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    .param p3, "x1"    # Ljava/lang/String;
    .param p4, "x2"    # Landroid/os/Bundle;

    .prologue
    .line 171
    iput-object p1, p0, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService$1;->this$0:Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService;

    invoke-direct {p0, p2, p3, p4}, Lcom/sec/android/app/sns3/svc/sp/instagram/request/SnsInReqGetFeed;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;Landroid/os/Bundle;)V

    return-void
.end method


# virtual methods
.method public onReqRespond(IZIILandroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseFeedList;)Z
    .locals 12
    .param p1, "reqID"    # I
    .param p2, "bSuccess"    # Z
    .param p3, "httpStatus"    # I
    .param p4, "errorCode"    # I
    .param p5, "reason"    # Landroid/os/Bundle;
    .param p6, "feed"    # Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseFeedList;

    .prologue
    .line 175
    new-instance v11, Landroid/content/ContentValues;

    invoke-direct {v11}, Landroid/content/ContentValues;-><init>()V

    .line 176
    .local v11, "values":Landroid/content/ContentValues;
    new-instance v7, Landroid/content/ContentValues;

    invoke-direct {v7}, Landroid/content/ContentValues;-><init>()V

    .line 177
    .local v7, "albumValues":Landroid/content/ContentValues;
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/sns3/SnsApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 179
    .local v1, "cr":Landroid/content/ContentResolver;
    if-eqz p2, :cond_4

    .line 180
    iget-object v2, p0, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService$1;->this$0:Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService;

    const/4 v3, 0x2

    # setter for: Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService;->mSyncState:I
    invoke-static {v2, v3}, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService;->access$602(Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService;I)I

    .line 182
    sget-object v2, Lcom/sec/android/app/sns3/agent/sp/instagram/db/SnsInstagramDB$SyncDBPhoto;->CONTENT_URI:Landroid/net/Uri;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 183
    invoke-virtual/range {p6 .. p6}, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseFeedList;->getFeeds()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    .local v10, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseFeed;

    .line 185
    .local v8, "curFeed":Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseFeed;
    invoke-virtual {v11}, Landroid/content/ContentValues;->clear()V

    .line 186
    const-string v2, "image"

    iget-object v3, v8, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseFeed;->mType:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 187
    const-string v2, "id"

    iget-object v3, v8, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseFeed;->mPostId:Ljava/lang/String;

    invoke-virtual {v11, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 188
    const-string v2, "target_id"

    iget-object v3, p0, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService$1;->this$0:Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService;

    # getter for: Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService;->ALBUM_ID_VALUE:I
    invoke-static {v3}, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService;->access$700(Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v11, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 189
    const-string v2, "name"

    iget-object v3, v8, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseFeed;->mCaption:Ljava/lang/String;

    invoke-virtual {v11, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 190
    const-string v2, "created_time"

    iget-object v3, v8, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseFeed;->mCreatedTime:Ljava/lang/String;

    invoke-virtual {v11, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    const-string v2, "from_name"

    iget-object v3, v8, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseFeed;->mUserName:Ljava/lang/String;

    invoke-virtual {v11, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 192
    const-string v2, "from_id"

    iget-object v3, v8, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseFeed;->mUserId:Ljava/lang/String;

    invoke-virtual {v11, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 193
    const-string v2, "width"

    iget-object v3, v8, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseFeed;->mWidth:Ljava/lang/String;

    invoke-virtual {v11, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 194
    const-string v2, "height"

    iget-object v3, v8, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseFeed;->mHeight:Ljava/lang/String;

    invoke-virtual {v11, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 195
    const-string v2, "updated_time"

    iget-object v3, v8, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseFeed;->mCreatedTime:Ljava/lang/String;

    invoke-virtual {v11, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 196
    const-string v2, "link"

    iget-object v3, v8, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseFeed;->mLink:Ljava/lang/String;

    invoke-virtual {v11, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 197
    const-string v2, "source"

    iget-object v3, v8, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseFeed;->mUrl:Ljava/lang/String;

    invoke-virtual {v11, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 198
    const-string v2, "location_name"

    iget-object v3, v8, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseFeed;->mLocName:Ljava/lang/String;

    invoke-virtual {v11, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 199
    const-string v2, "latitude"

    iget-object v3, v8, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseFeed;->mLatitude:Ljava/lang/String;

    invoke-virtual {v11, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 200
    const-string v2, "longitude"

    iget-object v3, v8, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseFeed;->mLongitude:Ljava/lang/String;

    invoke-virtual {v11, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 201
    const-string v2, "picture"

    iget-object v3, v8, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseFeed;->mThumbnailUrl:Ljava/lang/String;

    invoke-virtual {v11, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 203
    const-string v2, "url"

    iget-object v3, v8, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseFeed;->mUrl:Ljava/lang/String;

    invoke-virtual {v11, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 204
    const-string v2, "image_height"

    iget-object v3, v8, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseFeed;->mHeight:Ljava/lang/String;

    invoke-virtual {v11, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 205
    const-string v2, "image_width"

    iget-object v3, v8, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseFeed;->mWidth:Ljava/lang/String;

    invoke-virtual {v11, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 207
    sget-object v2, Lcom/sec/android/app/sns3/agent/sp/instagram/db/SnsInstagramDB$SyncDBPhoto;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1, v2, v11}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    goto/16 :goto_0

    .line 210
    .end local v8    # "curFeed":Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseFeed;
    :cond_1
    sget-object v2, Lcom/sec/android/app/sns3/agent/sp/instagram/db/SnsInstagramDB$SyncDBPhoto;->CONTENT_URI:Landroid/net/Uri;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 213
    .local v9, "cursor":Landroid/database/Cursor;
    if-eqz v9, :cond_3

    .line 215
    :try_start_0
    sget-object v2, Lcom/sec/android/app/sns3/agent/sp/instagram/db/SnsInstagramDB$SyncDBAlbum;->CONTENT_URI:Landroid/net/Uri;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 216
    const-string v2, "count"

    invoke-interface {v9}, Landroid/database/Cursor;->getCount()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v7, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 217
    const-string v2, "privacy"

    iget-object v3, p0, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService$1;->this$0:Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService;

    # getter for: Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService;->ALBUM_PRIVACY_VALUE:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService;->access$800(Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v7, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 219
    const-string v2, "id"

    iget-object v3, p0, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService$1;->this$0:Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService;

    # getter for: Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService;->ALBUM_ID_VALUE:I
    invoke-static {v3}, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService;->access$700(Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v7, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 220
    const-string v2, "name"

    iget-object v3, p0, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService$1;->this$0:Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService;

    # getter for: Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService;->ALBUM_NAME_VALUE:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService;->access$900(Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v7, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 221
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 222
    const-string v2, "cover_photo"

    const-string v3, "id"

    invoke-interface {v9, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v9, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v7, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 224
    const-string v2, "updated_time"

    const-string v3, "updated_time"

    invoke-interface {v9, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v9, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v7, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 227
    const-string v2, "from_name"

    const-string v3, "from_name"

    invoke-interface {v9, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v9, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v7, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 229
    const-string v2, "from_id"

    const-string v3, "from_id"

    invoke-interface {v9, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v9, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v7, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 232
    invoke-interface {v9}, Landroid/database/Cursor;->moveToLast()Z

    .line 233
    const-string v2, "created_time"

    const-string v3, "created_time"

    invoke-interface {v9, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v9, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v7, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 238
    :cond_2
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 241
    :cond_3
    sget-object v2, Lcom/sec/android/app/sns3/agent/sp/instagram/db/SnsInstagramDB$SyncDBAlbum;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1, v2, v7}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 248
    .end local v9    # "cursor":Landroid/database/Cursor;
    .end local v10    # "i$":Ljava/util/Iterator;
    :goto_1
    iget-object v2, p0, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService$1;->this$0:Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService;

    invoke-virtual {v2}, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService;->resumeSync()V

    .line 249
    const/4 v2, 0x1

    return v2

    .line 238
    .restart local v9    # "cursor":Landroid/database/Cursor;
    .restart local v10    # "i$":Ljava/util/Iterator;
    :catchall_0
    move-exception v2

    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    throw v2

    .line 243
    .end local v9    # "cursor":Landroid/database/Cursor;
    .end local v10    # "i$":Ljava/util/Iterator;
    :cond_4
    const-string v2, "SnsInSync"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SnsInSyncAdapterGalleryService errorCode : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, p4

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", reason : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p5

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 246
    iget-object v2, p0, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService$1;->this$0:Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService;

    const/4 v3, -0x1

    # setter for: Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService;->mSyncState:I
    invoke-static {v2, v3}, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService;->access$602(Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService;I)I

    goto :goto_1
.end method
