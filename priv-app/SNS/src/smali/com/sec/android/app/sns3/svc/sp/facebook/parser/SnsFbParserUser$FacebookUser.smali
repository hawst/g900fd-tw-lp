.class public interface abstract Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserUser$FacebookUser;
.super Ljava/lang/Object;
.source "SnsFbParserUser.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserUser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "FacebookUser"
.end annotation


# static fields
.field public static final BIO:Ljava/lang/String; = "bio"

.field public static final BIRTHDAY:Ljava/lang/String; = "birthday"

.field public static final EDUCATION:Ljava/lang/String; = "education"

.field public static final EMAIL:Ljava/lang/String; = "email"

.field public static final FAVORITE_ATHLETES:Ljava/lang/String; = "favorite_athletes"

.field public static final FAVORITE_TEAMS:Ljava/lang/String; = "favorite_teams"

.field public static final FIRST_NAME:Ljava/lang/String; = "first_name"

.field public static final GENDER:Ljava/lang/String; = "gender"

.field public static final HOMETOWN:Ljava/lang/String; = "hometown"

.field public static final INTERESTED_IN:Ljava/lang/String; = "interested_in"

.field public static final LANGUAGE:Ljava/lang/String; = "language"

.field public static final LAST_NAME:Ljava/lang/String; = "last_name"

.field public static final LINK:Ljava/lang/String; = "link"

.field public static final LOCALE:Ljava/lang/String; = "locale"

.field public static final LOCATION:Ljava/lang/String; = "location"

.field public static final MIDDLE_NAME:Ljava/lang/String; = "middle_name"

.field public static final NAME:Ljava/lang/String; = "name"

.field public static final POLITICAL:Ljava/lang/String; = "political"

.field public static final QUOTES:Ljava/lang/String; = "quotes"

.field public static final RELATIONSHIP_STATUS:Ljava/lang/String; = "relationship_status"

.field public static final RELIGION:Ljava/lang/String; = "religion"

.field public static final SIGNIFICANT_OTHER:Ljava/lang/String; = "significant_other"

.field public static final THIRD_PARTY_ID:Ljava/lang/String; = "third_party_id"

.field public static final TIMEZONE:Ljava/lang/String; = "timezone"

.field public static final UPDATED_TIME:Ljava/lang/String; = "updated_time"

.field public static final USER_ID:Ljava/lang/String; = "id"

.field public static final USER_NAME:Ljava/lang/String; = "username"

.field public static final VERIFIED:Ljava/lang/String; = "verified"

.field public static final VIDEO_UPLOAD_LIMITS:Ljava/lang/String; = "video_upload_limits"

.field public static final WEBSITE:Ljava/lang/String; = "website"

.field public static final WORK:Ljava/lang/String; = "work"
