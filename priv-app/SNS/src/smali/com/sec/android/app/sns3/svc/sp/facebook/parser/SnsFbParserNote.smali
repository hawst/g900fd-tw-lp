.class public Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserNote;
.super Ljava/lang/Object;
.source "SnsFbParserNote.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserNote$FacebookNote;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    return-void
.end method

.method public static parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNote;
    .locals 6
    .param p0, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 62
    new-instance v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNote;

    invoke-direct {v3}, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNote;-><init>()V

    .line 65
    .local v3, "note":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNote;
    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 67
    .local v2, "jsonObject":Lorg/json/JSONObject;
    const-string v4, "id"

    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNote;->mNoteID:Ljava/lang/String;

    .line 69
    const-string v4, "from"

    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 70
    const-string v4, "from"

    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    invoke-virtual {v4}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserFrom;->parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFrom;

    move-result-object v4

    iput-object v4, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNote;->mFrom:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFrom;

    .line 75
    :cond_0
    const-string v4, "subject"

    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNote;->mSubject:Ljava/lang/String;

    .line 76
    const-string v4, "message"

    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNote;->mMessage:Ljava/lang/String;

    .line 77
    const-string v4, "created_time"

    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNote;->mCreatedTime:Ljava/lang/String;

    .line 78
    const-string v4, "updated_time"

    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNote;->mUpdatedTime:Ljava/lang/String;

    .line 79
    const-string v4, "icon"

    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNote;->mIcon:Ljava/lang/String;

    .line 80
    const-string v4, "revision"

    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNote;->mRevision:Ljava/lang/String;

    .line 81
    const-string v4, "can_edit"

    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNote;->mCanEdit:Ljava/lang/String;

    .line 82
    const-string v4, "can_delete"

    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNote;->mCanDelete:Ljava/lang/String;

    .line 85
    const-string v4, "comments"

    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 86
    const-string v4, "comments"

    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserComment;->parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseComment;

    move-result-object v4

    iput-object v4, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNote;->mCommentList:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseComment;

    .line 89
    const-string v4, "comments"

    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "count"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 90
    const-string v4, "comments"

    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "count"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNote;->mCommentCount:Ljava/lang/String;

    .line 106
    .end local v2    # "jsonObject":Lorg/json/JSONObject;
    :cond_1
    :goto_0
    return-object v3

    .line 93
    .restart local v2    # "jsonObject":Lorg/json/JSONObject;
    :cond_2
    const-string v4, "comments"

    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "data"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 94
    const-string v4, "comments"

    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "data"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v4

    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 96
    .local v0, "commentCount":Ljava/lang/Integer;
    invoke-virtual {v0}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNote;->mCommentCount:Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 101
    .end local v0    # "commentCount":Ljava/lang/Integer;
    .end local v2    # "jsonObject":Lorg/json/JSONObject;
    :catch_0
    move-exception v1

    .line 103
    .local v1, "e":Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method
