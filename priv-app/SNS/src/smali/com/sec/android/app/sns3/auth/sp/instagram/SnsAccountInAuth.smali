.class public Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInAuth;
.super Ljava/lang/Object;
.source "SnsAccountInAuth.java"


# static fields
.field public static final ACTION_SNS_INSTAGRAM_LOGGED_IN:Ljava/lang/String; = "com.sec.android.app.sns.ACTION_SNS_INSTAGRAM_LOGGED_IN"

.field public static final ACTION_SNS_INSTAGRAM_LOGGED_OUT:Ljava/lang/String; = "com.sec.android.app.sns.ACTION_SNS_INSTAGRAM_LOGGED_OUT"

.field public static final GALLERY_PREFERENCE_KEY:Ljava/lang/String; = "IN_gallery_sync_interval"

.field public static final INSTAGRAM_SSO_NOTIFICATION_ID:I = 0x17d4

.field public static final LOGIN_INSTAGRAM_NOTIFICATION_ID:I = 0x1838

.field public static final LOGIN_REQUEST_CODE:I = 0x1770

.field public static final MARKET_APP_SSO:Z = false

.field public static final PROFILE_FEED_PREFERENCE_KEY:Ljava/lang/String; = "IN_profile_feed_sync_interval"

.field public static final RETRY_LOGIN_NOTIFICATION_ID:I = 0x189c

.field public static final SNS_BROADCAST_PERMISSION:Ljava/lang/String; = "com.sec.android.app.sns3.permission.RECEIVE_SNS_BROADCAST"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
