.class public interface abstract Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserLatestPhoto$FacebookLatestPhotoPost$SnsFbStreamQuery;
.super Ljava/lang/Object;
.source "SnsFbParserLatestPhoto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserLatestPhoto$FacebookLatestPhotoPost;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "SnsFbStreamQuery"
.end annotation


# static fields
.field public static final ATTACHMENT:Ljava/lang/String; = "attachment"

.field public static final CREATED_TIME:Ljava/lang/String; = "created_time"

.field public static final MEDIA:Ljava/lang/String; = "media"

.field public static final MESSAGE:Ljava/lang/String; = "message"

.field public static final PHOTO:Ljava/lang/String; = "photo"

.field public static final PID:Ljava/lang/String; = "pid"

.field public static final POST_ID:Ljava/lang/String; = "post_id"

.field public static final UPDATED_TIME:Ljava/lang/String; = "updated_time"
