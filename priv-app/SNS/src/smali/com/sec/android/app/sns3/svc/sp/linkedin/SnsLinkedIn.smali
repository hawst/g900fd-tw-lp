.class public Lcom/sec/android/app/sns3/svc/sp/linkedin/SnsLinkedIn;
.super Ljava/lang/Object;
.source "SnsLinkedIn.java"


# static fields
.field public static final ACCOUNT_TYPE:Ljava/lang/String; = "com.sec.android.app.sns3.linkedin"

.field public static final BUNDLE_KEY_AFTER:Ljava/lang/String; = "after"

.field public static final BUNDLE_KEY_LIMIT:Ljava/lang/String; = "count"

.field public static final BUNDLE_KEY_START:Ljava/lang/String; = "start"

.field public static final BUNDLE_KEY_WHERE:Ljava/lang/String; = "where"

.field public static final CANCEL_URI:Ljava/lang/String; = "http://localhost/cancel"

.field public static final DIALOG_BASE_URL:Ljava/lang/String; = "https://www.linkedin.com/uas/"

.field public static final LINKEDIN_ACCOUNT_TYPE:Ljava/lang/String; = "com.linkedin.android"

.field public static final LINKEDIN_AUTHORITY:Ljava/lang/String; = "com.linkedin.android"

.field public static final MARKET_URI:Ljava/lang/String; = "market://details"

.field public static final REDIRECT_URI:Ljava/lang/String; = "http://localhost/success"

.field public static final REST_URL:Ljava/lang/String; = "https://api.linkedin.com/v1/"

.field public static final REST_URL_PEOPLE:Ljava/lang/String; = "https://api.linkedin.com/v1/people/~"

.field public static final SP:Ljava/lang/String; = "linkedin"

.field public static final USER_DENIED_URI:Ljava/lang/String; = "http://localhost/success/?error=access_denied"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
