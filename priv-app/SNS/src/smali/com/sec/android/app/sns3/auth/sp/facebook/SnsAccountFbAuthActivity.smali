.class public Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;
.super Landroid/accounts/AccountAuthenticatorActivity;
.source "SnsAccountFbAuthActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity$UIDialogThreadRunnable;
    }
.end annotation


# static fields
.field private static final CONNECTION_TIMEOUT:I = 0x2

.field private static final LOGIN_FAIL_BY_ANOTHER:I = 0x0

.field private static final NETWORK_UNAVAILABLE:I = 0x1

.field private static final REQUEST_SYNC_INTERVAL:I = 0x3e8

.field private static TAG:Ljava/lang/String;


# instance fields
.field private mAccessToken:Ljava/lang/String;

.field mAccountManagerCallback:Landroid/accounts/AccountManagerCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/accounts/AccountManagerCallback",
            "<",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation
.end field

.field mAccountManagerRemoveCallback:Landroid/accounts/AccountManagerCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/accounts/AccountManagerCallback",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private mAppContext:Landroid/content/Context;

.field private mCode:Ljava/lang/String;

.field private mExpires:Ljava/lang/String;

.field private mHandler:Landroid/os/Handler;

.field private mRetryLogin:Z

.field private mState:Ljava/lang/String;

.field private mWebView:Landroid/webkit/WebView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 71
    const-string v0, "SnsAccountFbAuthActivity"

    sput-object v0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 69
    invoke-direct {p0}, Landroid/accounts/AccountAuthenticatorActivity;-><init>()V

    .line 81
    iput-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;->mAppContext:Landroid/content/Context;

    .line 85
    iput-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;->mHandler:Landroid/os/Handler;

    .line 87
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;->mRetryLogin:Z

    .line 89
    iput-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;->mAccessToken:Ljava/lang/String;

    .line 91
    iput-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;->mExpires:Ljava/lang/String;

    .line 93
    iput-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;->mState:Ljava/lang/String;

    .line 95
    iput-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;->mCode:Ljava/lang/String;

    .line 97
    new-instance v0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity$1;-><init>(Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;->mAccountManagerCallback:Landroid/accounts/AccountManagerCallback;

    .line 170
    new-instance v0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity$2;-><init>(Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;->mAccountManagerRemoveCallback:Landroid/accounts/AccountManagerCallback;

    .line 644
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 69
    sget-object v0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;

    .prologue
    .line 69
    iget-boolean v0, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;->mRetryLogin:Z

    return v0
.end method

.method static synthetic access$102(Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 69
    iput-boolean p1, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;->mRetryLogin:Z

    return p1
.end method

.method static synthetic access$200(Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;->mAccessToken:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$202(Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 69
    iput-object p1, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;->mAccessToken:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$300(Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;->mExpires:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$302(Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 69
    iput-object p1, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;->mExpires:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$400(Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;->mCode:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$402(Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 69
    iput-object p1, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;->mCode:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$500(Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;->mState:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;->mAppContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;Landroid/content/Context;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;
    .param p1, "x1"    # Landroid/content/Context;
    .param p2, "x2"    # I

    .prologue
    .line 69
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;->handleError(Landroid/content/Context;I)V

    return-void
.end method

.method private checkNetwork(Landroid/content/Context;)Z
    .locals 5
    .param p1, "mAppContext"    # Landroid/content/Context;

    .prologue
    .line 680
    const-string v3, "connectivity"

    invoke-virtual {p1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/ConnectivityManager;

    .line 682
    .local v1, "cm":Landroid/net/ConnectivityManager;
    const/4 v0, 0x0

    .line 684
    .local v0, "bNetworkStatus":Z
    const/4 v2, 0x0

    .line 686
    .local v2, "netInfo":Landroid/net/NetworkInfo;
    invoke-virtual {v1}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 687
    invoke-virtual {v2}, Landroid/net/NetworkInfo;->isAvailable()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 689
    sget-object v3, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;->TAG:Ljava/lang/String;

    const-string v4, "Checked that network is available!"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 691
    invoke-virtual {v2}, Landroid/net/NetworkInfo;->isConnectedOrConnecting()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 692
    sget-object v3, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;->TAG:Ljava/lang/String;

    const-string v4, "Checked that network is now connected or connecting!"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 693
    const/4 v0, 0x1

    .line 698
    :cond_0
    return v0
.end method

.method private handleError(Landroid/content/Context;I)V
    .locals 4
    .param p1, "mContext"    # Landroid/content/Context;
    .param p2, "errorCode"    # I

    .prologue
    .line 716
    sget-object v1, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "webview errorcode="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 717
    const/4 v1, -0x8

    if-ne p2, v1, :cond_0

    .line 718
    new-instance v0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity$UIDialogThreadRunnable;

    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;->mAppContext:Landroid/content/Context;

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity$UIDialogThreadRunnable;-><init>(Landroid/content/Context;I)V

    .line 720
    .local v0, "connectionTimeout":Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity$UIDialogThreadRunnable;
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 722
    .end local v0    # "connectionTimeout":Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity$UIDialogThreadRunnable;
    :cond_0
    return-void
.end method


# virtual methods
.method public getUniqueString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 712
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public loginFail(IILjava/lang/String;)V
    .locals 4
    .param p1, "errCode"    # I
    .param p2, "subErrCode"    # I
    .param p3, "reason"    # Ljava/lang/String;

    .prologue
    .line 625
    iget-object v2, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;->mAppContext:Landroid/content/Context;

    if-eqz v2, :cond_0

    .line 630
    const-string v2, "Network unreachable"

    invoke-virtual {v2, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 631
    new-instance v1, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity$UIDialogThreadRunnable;

    iget-object v2, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;->mAppContext:Landroid/content/Context;

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity$UIDialogThreadRunnable;-><init>(Landroid/content/Context;I)V

    .line 633
    .local v1, "networkFail":Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity$UIDialogThreadRunnable;
    iget-object v2, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 642
    .end local v1    # "networkFail":Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity$UIDialogThreadRunnable;
    :cond_0
    :goto_0
    return-void

    .line 635
    :cond_1
    new-instance v0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity$UIDialogThreadRunnable;

    iget-object v2, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;->mAppContext:Landroid/content/Context;

    const/4 v3, 0x0

    invoke-direct {v0, v2, v3}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity$UIDialogThreadRunnable;-><init>(Landroid/content/Context;I)V

    .line 637
    .local v0, "another":Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity$UIDialogThreadRunnable;
    iget-object v2, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public loginSuccess(Ljava/lang/String;Ljava/lang/String;)V
    .locals 10
    .param p1, "userName"    # Ljava/lang/String;
    .param p2, "id"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 597
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 598
    .local v4, "options":Landroid/os/Bundle;
    invoke-static {}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->isLoggable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 599
    sget-object v0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SnsAccountFbAuthActivity : addAccount() :  userName = "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 602
    :cond_0
    const-string v0, "username"

    invoke-virtual {v4, v0, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 604
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;->mAppContext:Landroid/content/Context;

    if-nez v0, :cond_1

    .line 605
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    const-string v1, "com.sec.android.app.sns3.facebook"

    move-object v3, v2

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Landroid/accounts/AccountManager;->addAccount(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Landroid/os/Bundle;Landroid/app/Activity;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    .line 613
    :goto_0
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/SnsApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    .line 614
    .local v8, "cr":Landroid/content/ContentResolver;
    new-instance v9, Landroid/content/ContentValues;

    invoke-direct {v9}, Landroid/content/ContentValues;-><init>()V

    .line 615
    .local v9, "values":Landroid/content/ContentValues;
    sget-object v0, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$User;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v8, v0, v2, v2}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 617
    invoke-virtual {v9}, Landroid/content/ContentValues;->clear()V

    .line 619
    const-string v0, "user_id"

    invoke-virtual {v9, v0, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 620
    const-string v0, "username"

    invoke-virtual {v9, v0, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 621
    sget-object v0, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$User;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v8, v0, v9}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 622
    return-void

    .line 608
    .end local v8    # "cr":Landroid/content/ContentResolver;
    .end local v9    # "values":Landroid/content/ContentValues;
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;->mAppContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    const-string v1, "com.sec.android.app.sns3.facebook"

    iget-object v6, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;->mAccountManagerCallback:Landroid/accounts/AccountManagerCallback;

    move-object v3, v2

    move-object v5, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Landroid/accounts/AccountManager;->addAccount(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Landroid/os/Bundle;Landroid/app/Activity;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    goto :goto_0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "result"    # Landroid/content/Intent;

    .prologue
    .line 468
    const/16 v0, 0x3e8

    if-ne p1, v0, :cond_0

    .line 469
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;->finish()V

    .line 471
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 14
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const v13, 0x7f080020

    const/4 v10, 0x2

    const/4 v12, 0x1

    const/4 v11, 0x0

    .line 211
    invoke-static {p0}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->isLightTheme(Landroid/content/Context;)Z

    move-result v7

    if-eqz v7, :cond_0

    const v7, 0x103012b

    :goto_0
    invoke-virtual {p0, v7}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;->setTheme(I)V

    .line 214
    invoke-super {p0, p1}, Landroid/accounts/AccountAuthenticatorActivity;->onCreate(Landroid/os/Bundle;)V

    .line 216
    iput-object p0, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;->mAppContext:Landroid/content/Context;

    .line 217
    new-instance v7, Landroid/os/Handler;

    invoke-direct {v7}, Landroid/os/Handler;-><init>()V

    iput-object v7, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;->mHandler:Landroid/os/Handler;

    .line 219
    invoke-virtual {p0, v10}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;->requestWindowFeature(I)Z

    .line 221
    const v7, 0x7f080046

    new-array v8, v12, [Ljava/lang/Object;

    invoke-virtual {p0, v13}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;->getString(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v11

    invoke-virtual {p0, v7, v8}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 223
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;->getIntent()Landroid/content/Intent;

    move-result-object v7

    const-string v8, "RetryLogin"

    invoke-virtual {v7, v8, v11}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v7

    iput-boolean v7, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;->mRetryLogin:Z

    .line 225
    iget-object v7, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;->mAppContext:Landroid/content/Context;

    invoke-static {v7}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v7

    const-string v8, "com.sec.android.app.sns3.facebook"

    invoke-virtual {v7, v8}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 227
    .local v0, "accounts":[Landroid/accounts/Account;
    array-length v7, v0

    if-lez v7, :cond_1

    iget-boolean v7, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;->mRetryLogin:Z

    if-nez v7, :cond_1

    .line 228
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    const/high16 v8, 0x7f080000

    new-array v9, v12, [Ljava/lang/Object;

    invoke-virtual {p0, v13}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;->getString(I)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v9, v11

    invoke-virtual {p0, v8, v9}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8, v12}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/Toast;->show()V

    .line 231
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;->finish()V

    .line 465
    :goto_1
    return-void

    .line 211
    .end local v0    # "accounts":[Landroid/accounts/Account;
    :cond_0
    const v7, 0x1030128

    goto :goto_0

    .line 235
    .restart local v0    # "accounts":[Landroid/accounts/Account;
    :cond_1
    iget-object v7, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;->mAppContext:Landroid/content/Context;

    invoke-direct {p0, v7}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;->checkNetwork(Landroid/content/Context;)Z

    move-result v7

    if-nez v7, :cond_2

    .line 236
    new-instance v3, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity$UIDialogThreadRunnable;

    iget-object v7, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;->mAppContext:Landroid/content/Context;

    invoke-direct {v3, v7, v12}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity$UIDialogThreadRunnable;-><init>(Landroid/content/Context;I)V

    .line 238
    .local v3, "networkUnavailable":Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity$UIDialogThreadRunnable;
    iget-object v7, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;->mAppContext:Landroid/content/Context;

    check-cast v7, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;

    invoke-virtual {v7, v3}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_1

    .line 242
    .end local v3    # "networkUnavailable":Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity$UIDialogThreadRunnable;
    :cond_2
    const-string v7, "notification"

    invoke-virtual {p0, v7}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/app/NotificationManager;

    .line 243
    .local v4, "notiMgr":Landroid/app/NotificationManager;
    const/16 v7, 0x4b0

    invoke-virtual {v4, v7}, Landroid/app/NotificationManager;->cancel(I)V

    .line 244
    const/16 v7, 0x514

    invoke-virtual {v4, v7}, Landroid/app/NotificationManager;->cancel(I)V

    .line 246
    new-instance v7, Landroid/webkit/WebView;

    iget-object v8, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;->mAppContext:Landroid/content/Context;

    invoke-direct {v7, v8}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;)V

    iput-object v7, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;->mWebView:Landroid/webkit/WebView;

    .line 248
    iget-object v7, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v7}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v7

    invoke-virtual {v7, v11}, Landroid/webkit/WebSettings;->setAllowFileAccess(Z)V

    .line 249
    iget-object v7, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v7}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v7

    invoke-virtual {v7, v11}, Landroid/webkit/WebSettings;->setAppCacheEnabled(Z)V

    .line 250
    iget-object v7, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v7}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v7

    invoke-virtual {v7, v10}, Landroid/webkit/WebSettings;->setCacheMode(I)V

    .line 251
    iget-object v7, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v7}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v7

    invoke-virtual {v7, v11}, Landroid/webkit/WebSettings;->setDatabaseEnabled(Z)V

    .line 252
    iget-object v7, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v7}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v7

    invoke-virtual {v7, v11}, Landroid/webkit/WebSettings;->setSaveFormData(Z)V

    .line 253
    iget-object v7, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v7}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v7

    invoke-virtual {v7, v12}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 257
    iget-object v7, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;->mAppContext:Landroid/content/Context;

    invoke-static {v7}, Landroid/webkit/CookieSyncManager;->createInstance(Landroid/content/Context;)Landroid/webkit/CookieSyncManager;

    .line 258
    invoke-static {}, Landroid/webkit/CookieManager;->getInstance()Landroid/webkit/CookieManager;

    move-result-object v1

    .line 259
    .local v1, "cookieManager":Landroid/webkit/CookieManager;
    invoke-virtual {v1}, Landroid/webkit/CookieManager;->removeAllCookie()V

    .line 261
    iget-object v7, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;->mWebView:Landroid/webkit/WebView;

    new-instance v8, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity$3;

    invoke-direct {v8, p0}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity$3;-><init>(Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;)V

    invoke-virtual {v7, v8}, Landroid/webkit/WebView;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    .line 267
    iget-object v7, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;->mWebView:Landroid/webkit/WebView;

    new-instance v8, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity$4;

    invoke-direct {v8, p0}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity$4;-><init>(Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;)V

    invoke-virtual {v7, v8}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 445
    iget-object v7, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {p0, v7}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;->setContentView(Landroid/view/View;)V

    .line 447
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;->getUniqueString()Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;->mState:Ljava/lang/String;

    .line 449
    sget-object v7, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuth;->PERMISSION:[Ljava/lang/String;

    aget-object v5, v7, v11

    .line 450
    .local v5, "permissions":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/sns3/SnsApplication;->getFeatureMgr()Lcom/sec/android/app/sns3/SnsFeatureManager;

    move-result-object v2

    .line 451
    .local v2, "featureMgr":Lcom/sec/android/app/sns3/SnsFeatureManager;
    invoke-virtual {v2}, Lcom/sec/android/app/sns3/SnsFeatureManager;->isSyncAdapterForProfilesNeeded()Z

    move-result v7

    if-nez v7, :cond_3

    invoke-virtual {v2}, Lcom/sec/android/app/sns3/SnsFeatureManager;->isSyncAdapterForStreamsNeeded()Z

    move-result v7

    if-eqz v7, :cond_4

    .line 453
    :cond_3
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuth;->PERMISSION_EXTRA:[Ljava/lang/String;

    aget-object v8, v8, v11

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 456
    :cond_4
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "https://m.facebook.com/dialog/oauth?client_id="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {}, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbAppIdManager;->getInstance()Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbAppIdManager;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbAppIdManager;->getAppId()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "&redirect_uri="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "fbconnect://success"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "&scope="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "&state="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;->mState:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 461
    .local v6, "url":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->isLoggable()Z

    move-result v7

    if-eqz v7, :cond_5

    .line 462
    sget-object v7, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "loadUrl : url = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 464
    :cond_5
    iget-object v7, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v7, v6}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    goto/16 :goto_1
.end method

.method public onCreateDialog(I)Landroid/app/Dialog;
    .locals 3
    .param p1, "id"    # I

    .prologue
    const v2, 0x7f080034

    const v1, 0x7f080031

    .line 504
    packed-switch p1, :pswitch_data_0

    .line 587
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 506
    :pswitch_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f080032

    invoke-virtual {p0, v1}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity$6;

    invoke-direct {v1, p0}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity$6;-><init>(Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity$5;

    invoke-direct {v1, p0}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity$5;-><init>(Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;)V

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    .line 533
    :pswitch_1
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f080033

    invoke-virtual {p0, v1}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity$8;

    invoke-direct {v1, p0}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity$8;-><init>(Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity$7;

    invoke-direct {v1, p0}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity$7;-><init>(Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;)V

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    .line 560
    :pswitch_2
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f08001f

    invoke-virtual {p0, v1}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity$10;

    invoke-direct {v1, p0}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity$10;-><init>(Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity$9;

    invoke-direct {v1, p0}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity$9;-><init>(Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;)V

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto/16 :goto_0

    .line 504
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 592
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;->mAppContext:Landroid/content/Context;

    .line 593
    invoke-super {p0}, Landroid/accounts/AccountAuthenticatorActivity;->onDestroy()V

    .line 594
    return-void
.end method

.method public onPrepareDialog(ILandroid/app/Dialog;)V
    .locals 4
    .param p1, "id"    # I
    .param p2, "dialog"    # Landroid/app/Dialog;

    .prologue
    .line 475
    packed-switch p1, :pswitch_data_0

    .line 500
    :cond_0
    :goto_0
    return-void

    .line 477
    :pswitch_0
    instance-of v3, p2, Landroid/app/AlertDialog;

    if-eqz v3, :cond_0

    move-object v0, p2

    .line 478
    check-cast v0, Landroid/app/AlertDialog;

    .line 479
    .local v0, "alert":Landroid/app/AlertDialog;
    const v3, 0x7f080032

    invoke-virtual {p0, v3}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 484
    .end local v0    # "alert":Landroid/app/AlertDialog;
    :pswitch_1
    instance-of v3, p2, Landroid/app/AlertDialog;

    if-eqz v3, :cond_0

    move-object v1, p2

    .line 485
    check-cast v1, Landroid/app/AlertDialog;

    .line 486
    .local v1, "alert1":Landroid/app/AlertDialog;
    const v3, 0x7f080033

    invoke-virtual {p0, v3}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 491
    .end local v1    # "alert1":Landroid/app/AlertDialog;
    :pswitch_2
    instance-of v3, p2, Landroid/app/AlertDialog;

    if-eqz v3, :cond_0

    move-object v2, p2

    .line 492
    check-cast v2, Landroid/app/AlertDialog;

    .line 493
    .local v2, "timeout":Landroid/app/AlertDialog;
    const v3, 0x7f08001f

    invoke-virtual {p0, v3}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 475
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public setAuthTokenNExpires(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3
    .param p1, "accessToken"    # Ljava/lang/String;
    .param p2, "expires"    # Ljava/lang/String;

    .prologue
    .line 702
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 703
    :cond_0
    const/4 v1, 0x0

    .line 708
    :goto_0
    return v1

    .line 705
    :cond_1
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/sns3/SnsApplication;->getSvcMgr()Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/sns3/svc/SnsSvcMgr;->getTokenMgr()Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;

    move-result-object v1

    const-string v2, "facebook"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;->getToken(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbToken;

    .line 707
    .local v0, "fbToken":Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbToken;
    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbToken;->setAccessTokenNExpires(Ljava/lang/String;Ljava/lang/String;)V

    .line 708
    const/4 v1, 0x1

    goto :goto_0
.end method
