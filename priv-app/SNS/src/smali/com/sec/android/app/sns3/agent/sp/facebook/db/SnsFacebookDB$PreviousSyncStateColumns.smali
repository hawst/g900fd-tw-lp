.class public interface abstract Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$PreviousSyncStateColumns;
.super Ljava/lang/Object;
.source "SnsFacebookDB.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "PreviousSyncStateColumns"
.end annotation


# static fields
.field public static final PAGING_NEXT:Ljava/lang/String; = "paging_next"

.field public static final PAGING_PREVIOUS:Ljava/lang/String; = "paging_previous"

.field public static final PREVIOUS_STATUS:Ljava/lang/String; = "previous_state"

.field public static final SYNC_TYPE:Ljava/lang/String; = "sync_type"
