.class public Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;
.super Ljava/lang/Thread;
.source "SnsRequestMgr.java"


# static fields
.field private static final REQUEST_MAX_COUNT:I = 0x32

.field public static final STATE_FREE:I = 0x0

.field public static final STATE_LOCK:I = 0x1


# instance fields
.field private mAuthRequestMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;",
            ">;"
        }
    .end annotation
.end field

.field private mHandler:Landroid/os/Handler;

.field private mPollingRequestMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;",
            ">;"
        }
    .end annotation
.end field

.field private mRequestMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;",
            ">;"
        }
    .end annotation
.end field

.field private mSentReqCnt:I

.field private mState:I

.field private mTokenMgr:Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;)Ljava/util/Map;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;->mAuthRequestMap:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;)Ljava/util/Map;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;->mPollingRequestMap:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;)Ljava/util/Map;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;->mRequestMap:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;->getCurrentRequestCount()I

    move-result v0

    return v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;IZIILandroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;
    .param p1, "x1"    # I
    .param p2, "x2"    # Z
    .param p3, "x3"    # I
    .param p4, "x4"    # I
    .param p5, "x5"    # Landroid/os/Bundle;
    .param p6, "x6"    # Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;

    .prologue
    .line 38
    invoke-direct/range {p0 .. p6}, Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;->processError(IZIILandroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;)V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;I)Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;
    .param p1, "x1"    # I

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;->getRequest(I)Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$608(Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;)I
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;

    .prologue
    .line 38
    iget v0, p0, Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;->mSentReqCnt:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;->mSentReqCnt:I

    return v0
.end method

.method static synthetic access$610(Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;)I
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;

    .prologue
    .line 38
    iget v0, p0, Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;->mSentReqCnt:I

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;->mSentReqCnt:I

    return v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;
    .param p1, "x1"    # Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;->processResponse(Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;)V

    return-void
.end method

.method static synthetic access$800(Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;

    .prologue
    .line 38
    iget v0, p0, Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;->mState:I

    return v0
.end method

.method static synthetic access$802(Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;
    .param p1, "x1"    # I

    .prologue
    .line 38
    iput p1, p0, Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;->mState:I

    return p1
.end method

.method static synthetic access$900(Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;->getCurrentHttpClientState()I

    move-result v0

    return v0
.end method

.method private getCurrentHttpClientState()I
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 196
    const/4 v0, 0x0

    .line 198
    .local v0, "curReq":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    iget-object v4, p0, Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;->mAuthRequestMap:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->size()I

    move-result v4

    if-lez v4, :cond_1

    .line 215
    :cond_0
    :goto_0
    return v2

    .line 201
    :cond_1
    iget v4, p0, Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;->mSentReqCnt:I

    const/16 v5, 0x1e

    if-lt v4, v5, :cond_2

    move v2, v3

    .line 202
    goto :goto_0

    .line 204
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;->getReservedRequest()Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;

    move-result-object v0

    .line 205
    if-eqz v0, :cond_0

    .line 206
    iget-object v4, p0, Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;->mTokenMgr:Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->getSpType()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;->getToken(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;

    move-result-object v1

    .line 207
    .local v1, "token":Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->getCategory()I

    move-result v4

    const/16 v5, 0x14

    if-eq v4, v5, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->getCategory()I

    move-result v4

    const/16 v5, 0x18

    if-eq v4, v5, :cond_0

    invoke-virtual {v1}, Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;->getTokenState()I

    move-result v4

    if-ne v4, v3, :cond_0

    .line 210
    const-string v2, "SNS"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->getSpType()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " token expired"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    move v2, v3

    .line 211
    goto :goto_0
.end method

.method private getCurrentRequestCount()I
    .locals 2

    .prologue
    .line 221
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;->mAuthRequestMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;->mRequestMap:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;->mPollingRequestMap:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method private getRequest(I)Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    .locals 3
    .param p1, "reqID"    # I

    .prologue
    .line 294
    const/4 v0, 0x0

    .line 296
    .local v0, "req":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;->mAuthRequestMap:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "req":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    check-cast v0, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;

    .line 298
    .restart local v0    # "req":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    if-nez v0, :cond_0

    .line 299
    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;->mRequestMap:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "req":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    check-cast v0, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;

    .line 301
    .restart local v0    # "req":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    :cond_0
    if-nez v0, :cond_1

    .line 302
    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;->mPollingRequestMap:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "req":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    check-cast v0, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;

    .line 304
    .restart local v0    # "req":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    :cond_1
    return-object v0
.end method

.method private processError(IZIILandroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;)V
    .locals 9
    .param p1, "reqID"    # I
    .param p2, "bSuccess"    # Z
    .param p3, "httpStatus"    # I
    .param p4, "errorCode"    # I
    .param p5, "reason"    # Landroid/os/Bundle;
    .param p6, "response"    # Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;

    .prologue
    .line 329
    invoke-direct {p0, p1}, Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;->getRequest(I)Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;

    move-result-object v7

    .line 330
    .local v7, "req":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    if-nez v7, :cond_0

    .line 394
    :goto_0
    return-void

    .line 332
    :cond_0
    const/4 v0, 0x0

    .line 333
    .local v0, "result":Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;
    new-instance v0, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;

    .end local v0    # "result":Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;
    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;-><init>(IZIILandroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;)V

    .line 335
    .restart local v0    # "result":Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;
    const-string v1, "SNS"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "processError : errorCode[reqID("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "):"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 337
    sparse-switch p4, :sswitch_data_0

    .line 391
    invoke-direct {p0, v0}, Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;->processResponse(Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;)V

    goto :goto_0

    .line 342
    :sswitch_0
    invoke-virtual {v7}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->isMaxRetry()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 343
    invoke-virtual {v7}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->abort()Z

    .line 344
    const/16 v1, 0x7d6

    invoke-virtual {v0, v1}, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->setErrorCode(I)V

    .line 345
    invoke-direct {p0, v0}, Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;->processResponse(Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;)V

    goto :goto_0

    .line 347
    :cond_1
    invoke-virtual {v7}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->increaseRetryCnt()V

    .line 348
    invoke-virtual {v7}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->retryRequest()V

    goto :goto_0

    .line 354
    :sswitch_1
    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;->mTokenMgr:Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;

    invoke-virtual {v7}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->getSpType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;->getToken(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;

    move-result-object v8

    .line 355
    .local v8, "token":Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;
    const/4 v1, 0x1

    invoke-virtual {v8, v1}, Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;->setTokenState(I)V

    .line 356
    invoke-virtual {v7}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->restoreToken()V

    .line 357
    invoke-virtual {v7}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->abort()Z

    .line 358
    invoke-direct {p0, v0}, Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;->processResponse(Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;)V

    goto :goto_0

    .line 362
    .end local v8    # "token":Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;
    :sswitch_2
    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;->mTokenMgr:Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;

    invoke-virtual {v7}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->getSpType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;->getToken(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;

    move-result-object v8

    .line 363
    .restart local v8    # "token":Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;
    const/4 v1, 0x3

    invoke-virtual {v8, v1}, Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;->setTokenState(I)V

    .line 364
    const/4 v1, 0x1

    invoke-virtual {v7, v1}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->setState(I)V

    .line 366
    invoke-virtual {v7}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->isMaxRetry()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 367
    invoke-virtual {v7}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->abort()Z

    .line 368
    const/16 v1, 0x7d6

    invoke-virtual {v0, v1}, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->setErrorCode(I)V

    .line 369
    invoke-direct {p0, v0}, Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;->processResponse(Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;)V

    goto/16 :goto_0

    .line 371
    :cond_2
    invoke-virtual {v7}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->increaseRetryCnt()V

    .line 372
    invoke-virtual {v7}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->restoreToken()V

    goto/16 :goto_0

    .line 378
    .end local v8    # "token":Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;
    :sswitch_3
    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;->mTokenMgr:Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;

    invoke-virtual {v7}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->getSpType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;->getToken(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;

    move-result-object v8

    .line 379
    .restart local v8    # "token":Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;
    const/4 v1, 0x2

    invoke-virtual {v8, v1}, Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;->setTokenState(I)V

    .line 384
    .end local v8    # "token":Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;
    :sswitch_4
    invoke-virtual {v7}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->abort()Z

    .line 385
    invoke-direct {p0, v0}, Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;->processResponse(Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;)V

    goto/16 :goto_0

    .line 337
    :sswitch_data_0
    .sparse-switch
        0x7d1 -> :sswitch_1
        0x7d2 -> :sswitch_0
        0x7d3 -> :sswitch_4
        0x7d4 -> :sswitch_4
        0x7d7 -> :sswitch_3
        0xbb9 -> :sswitch_0
        0xbbd -> :sswitch_0
        0xfa1 -> :sswitch_2
    .end sparse-switch
.end method

.method private processResponse(Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;)V
    .locals 3
    .param p1, "result"    # Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;

    .prologue
    .line 320
    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->getReqID()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;->getRequest(I)Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;

    move-result-object v0

    .line 322
    .local v0, "req":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->setState(I)V

    .line 323
    invoke-virtual {v0, p1}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->respond(Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;)Z

    .line 324
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->getReqID()I

    move-result v1

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->getCategory()I

    move-result v2

    invoke-direct {p0, v1, v2}, Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;->removeRequest(II)V

    .line 325
    return-void
.end method

.method private removeRequest(II)V
    .locals 3
    .param p1, "reqID"    # I
    .param p2, "category"    # I

    .prologue
    .line 308
    const/16 v0, 0x14

    if-ne p2, v0, :cond_0

    .line 309
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;->mAuthRequestMap:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 315
    :goto_0
    const-string v0, "SNS"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "removeRequest : mRequestMap["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " count is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;->mRequestMap:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 317
    return-void

    .line 310
    :cond_0
    const/16 v0, 0x15

    if-ne p2, v0, :cond_1

    .line 311
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;->mPollingRequestMap:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 313
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;->mRequestMap:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method


# virtual methods
.method public clearReservedRequestsBySp(Ljava/lang/String;)Z
    .locals 7
    .param p1, "spType"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    .line 258
    if-nez p1, :cond_1

    .line 259
    const/4 v4, 0x0

    .line 290
    :cond_0
    return v4

    .line 261
    :cond_1
    iget-object v5, p0, Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;->mAuthRequestMap:Ljava/util/Map;

    invoke-interface {v5}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 262
    .local v0, "authIter":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    iget-object v5, p0, Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;->mRequestMap:Ljava/util/Map;

    invoke-interface {v5}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 263
    .local v1, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    iget-object v5, p0, Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;->mPollingRequestMap:Ljava/util/Map;

    invoke-interface {v5}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 264
    .local v2, "pollingIter":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    const/4 v3, 0x0

    .line 266
    .local v3, "req":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    :cond_2
    :goto_0
    if-eqz v0, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 267
    iget-object v5, p0, Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;->mAuthRequestMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "req":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    check-cast v3, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;

    .line 268
    .restart local v3    # "req":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    if-eqz v3, :cond_2

    invoke-virtual {v3}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->getState()I

    move-result v5

    if-ne v5, v4, :cond_2

    invoke-virtual {v3}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->getSpType()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_2

    invoke-virtual {v3}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->getSpType()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 270
    invoke-virtual {v3}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->abort()Z

    goto :goto_0

    .line 274
    :cond_3
    :goto_1
    if-eqz v1, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 275
    iget-object v5, p0, Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;->mRequestMap:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "req":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    check-cast v3, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;

    .line 276
    .restart local v3    # "req":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    if-eqz v3, :cond_3

    invoke-virtual {v3}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->getState()I

    move-result v5

    if-ne v5, v4, :cond_3

    invoke-virtual {v3}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->getSpType()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_3

    invoke-virtual {v3}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->getSpType()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 278
    invoke-virtual {v3}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->abort()Z

    goto :goto_1

    .line 282
    :cond_4
    :goto_2
    if-eqz v2, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 283
    iget-object v5, p0, Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;->mPollingRequestMap:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "req":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    check-cast v3, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;

    .line 284
    .restart local v3    # "req":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    if-eqz v3, :cond_4

    invoke-virtual {v3}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->getState()I

    move-result v5

    if-ne v5, v4, :cond_4

    invoke-virtual {v3}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->getSpType()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_4

    invoke-virtual {v3}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->getSpType()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 286
    invoke-virtual {v3}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->abort()Z

    goto :goto_2
.end method

.method public getRequestMgrHandle()Landroid/os/Handler;
    .locals 5

    .prologue
    .line 175
    const/4 v0, 0x0

    .line 176
    .local v0, "count":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;->mHandler:Landroid/os/Handler;

    if-nez v2, :cond_0

    .line 178
    const-wide/16 v2, 0x64

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 183
    :goto_1
    add-int/lit8 v0, v0, 0x1

    .line 184
    const/16 v2, 0x64

    if-le v0, v2, :cond_1

    .line 185
    const-string v2, "SNS"

    const-string v3, "waiting timeout!"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 192
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;->mHandler:Landroid/os/Handler;

    return-object v2

    .line 179
    :catch_0
    move-exception v1

    .line 180
    .local v1, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v1}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_1

    .line 188
    .end local v1    # "e":Ljava/lang/InterruptedException;
    :cond_1
    const-string v2, "SNS"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "wait thread running... "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getReservedRequest()Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 225
    iget-object v4, p0, Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;->mAuthRequestMap:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 226
    .local v0, "authIter":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    iget-object v4, p0, Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;->mRequestMap:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 227
    .local v1, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    iget-object v4, p0, Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;->mPollingRequestMap:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 228
    .local v2, "pollingIter":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    const/4 v3, 0x0

    .line 230
    .local v3, "req":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    :cond_0
    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 231
    iget-object v4, p0, Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;->mAuthRequestMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "req":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    check-cast v3, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;

    .line 232
    .restart local v3    # "req":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    invoke-virtual {v3}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->getState()I

    move-result v4

    if-ne v4, v6, :cond_0

    move-object v4, v3

    .line 254
    :goto_0
    return-object v4

    .line 237
    :cond_1
    if-nez v3, :cond_3

    .line 238
    :cond_2
    if-eqz v1, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 239
    iget-object v4, p0, Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;->mRequestMap:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "req":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    check-cast v3, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;

    .line 240
    .restart local v3    # "req":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    invoke-virtual {v3}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->getState()I

    move-result v4

    if-ne v4, v6, :cond_2

    move-object v4, v3

    .line 241
    goto :goto_0

    .line 246
    :cond_3
    if-nez v3, :cond_5

    .line 247
    :cond_4
    if-eqz v2, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 248
    iget-object v4, p0, Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;->mPollingRequestMap:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "req":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    check-cast v3, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;

    .line 249
    .restart local v3    # "req":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    invoke-virtual {v3}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->getState()I

    move-result v4

    if-ne v4, v6, :cond_4

    move-object v4, v3

    .line 250
    goto :goto_0

    .line 254
    :cond_5
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public initialize(Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;)V
    .locals 1
    .param p1, "tokenMgr"    # Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;

    .prologue
    .line 62
    iput-object p1, p0, Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;->mTokenMgr:Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;

    .line 64
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;->mRequestMap:Ljava/util/Map;

    .line 65
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;->mAuthRequestMap:Ljava/util/Map;

    .line 66
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;->mPollingRequestMap:Ljava/util/Map;

    .line 68
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;->start()V

    .line 69
    return-void
.end method

.method public run()V
    .locals 1

    .prologue
    .line 73
    invoke-static {}, Landroid/os/Looper;->prepare()V

    .line 75
    new-instance v0, Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr$1;-><init>(Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;)V

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;->mHandler:Landroid/os/Handler;

    .line 171
    invoke-static {}, Landroid/os/Looper;->loop()V

    .line 172
    return-void
.end method
