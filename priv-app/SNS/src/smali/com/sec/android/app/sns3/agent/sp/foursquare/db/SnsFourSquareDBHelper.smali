.class public Lcom/sec/android/app/sns3/agent/sp/foursquare/db/SnsFourSquareDBHelper;
.super Lcom/sec/android/app/sns3/agent/db/SnsDBHelperBase;
.source "SnsFourSquareDBHelper.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 32
    const-string v0, "snsFourSquareDB.db"

    const/4 v1, 0x0

    const/4 v2, 0x2

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/sec/android/app/sns3/agent/db/SnsDBHelperBase;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 33
    return-void
.end method


# virtual methods
.method public createTables(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 51
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 53
    const-string v0, "CREATE TABLE IF NOT EXISTS user_basic_info (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,user_id VARCHAR(50),formatted_name TEXT, UNIQUE (user_id));"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 59
    const-string v0, "SnsAgent"

    const-string v1, "DB TABLE(user_basic_info) Has been created."

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 62
    const-string v0, "CREATE TABLE IF NOT EXISTS user_profile_info (user_id VARCHAR(50),first_name TEXT,last_name TEXT,type TEXT,about_me TEXT,image_url TEXT,formatted_name TEXT, UNIQUE (user_id));"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 72
    const-string v0, "SnsAgent"

    const-string v1, "DB TABLE(user_profile_info) Has been created."

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 75
    const-string v0, "CREATE TABLE IF NOT EXISTS user_feed_info (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,post_id VARCHAR(50),message TEXT,timestamp_utc TIMESTAMP,object_type TEXT,location_name TEXT,latitude DOUBLE,longitude DOUBLE,category_id VARCHAR(50),category_name TEXT,category_icon TEXT, UNIQUE (post_id));"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 89
    const-string v0, "SnsAgent"

    const-string v1, "DB TABLE(user_feed_info) Has been created."

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 92
    const-string v0, "CREATE TABLE IF NOT EXISTS friends_profile_info (user_id VARCHAR(50),first_name TEXT,last_name TEXT,type TEXT,about_me TEXT,image_url TEXT,formatted_name TEXT,relationship TEXT,friends TEXT,homeCity TEXT,gender TEXT,phone TEXT,email TEXT,twitter TEXT,facebook TEXT, UNIQUE (user_id));"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 110
    const-string v0, "SnsAgent"

    const-string v1, "DB TABLE(friends_profile_info) Has been created."

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 113
    const-string v0, "CREATE TABLE IF NOT EXISTS venues_categories (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,category_id VARCHAR(50),category_name TEXT,category_plural_name TEXT,category_icon VARCHAR(1024), UNIQUE (category_id));"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 121
    const-string v0, "SnsAgent"

    const-string v1, "DB TABLE(venues_categories) Has been created."

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 124
    const-string v0, "CREATE TABLE IF NOT EXISTS user_info (user_id VARCHAR(50),formatted_name TEXT,first_name TEXT,last_name TEXT,image_url TEXT,type TEXT,about_me TEXT,twitter TEXT,facebook TEXT, UNIQUE (user_id));"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 136
    const-string v0, "SnsAgent"

    const-string v1, "DB TABLE(user_info) Has been created."

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 139
    const-string v0, "CREATE TABLE IF NOT EXISTS status_stream (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,user_id VARCHAR(50),user_name VARCHAR(50),checkin_id VARCHAR(50),object_type TEXT,message TEXT,timestamp_utc TIMESTAMP,time_zone TEXT,location_id VARCHAR(50),location_name TEXT,latitude DOUBLE,longitude DOUBLE,category_id VARCHAR(50),category_name TEXT,category_icon TEXT,likes_count VARCHAR(50),comments_count VARCHAR(50), UNIQUE (checkin_id));"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 159
    const-string v0, "SnsAgent"

    const-string v1, "DB TABLE(status_stream) Has been created."

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 162
    const-string v0, "CREATE TABLE IF NOT EXISTS timeline (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,user_id VARCHAR(50),formatted_name TEXT,relationship TEXT,image_url VARCHAR(1024),checkin_id VARCHAR(50),message TEXT,timestamp_utc TIMESTAMP,time_zone TEXT,object_type TEXT,location_name TEXT,latitude DOUBLE,longitude DOUBLE,category_id VARCHAR(50),category_name TEXT,category_icon TEXT, UNIQUE (checkin_id));"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 181
    const-string v0, "SnsAgent"

    const-string v1, "DB TABLE(timeline) Has been created."

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 184
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 185
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 186
    return-void
.end method

.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 0
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 37
    invoke-super {p0, p1}, Lcom/sec/android/app/sns3/agent/db/SnsDBHelperBase;->onCreate(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 39
    invoke-virtual {p0, p1}, Lcom/sec/android/app/sns3/agent/sp/foursquare/db/SnsFourSquareDBHelper;->createTables(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 40
    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 0
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "oldVersion"    # I
    .param p3, "newVersion"    # I

    .prologue
    .line 44
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/app/sns3/agent/db/SnsDBHelperBase;->onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V

    .line 46
    invoke-virtual {p0, p1}, Lcom/sec/android/app/sns3/agent/sp/foursquare/db/SnsFourSquareDBHelper;->upgradeTables(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 48
    return-void
.end method

.method public upgradeTables(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 191
    const-string v0, "DROP TABLE IF EXISTS user_basic_info"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 192
    const-string v0, "DROP TABLE IF EXISTS user_profile_info"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 193
    const-string v0, "DROP TABLE IF EXISTS user_feed_info"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 194
    const-string v0, "DROP TABLE IF EXISTS friends_profile_info"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 195
    const-string v0, "DROP TABLE IF EXISTS venues_categories"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 196
    const-string v0, "DROP TABLE IF EXISTS user_info"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 197
    const-string v0, "DROP TABLE IF EXISTS status_stream"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 199
    invoke-virtual {p0, p1}, Lcom/sec/android/app/sns3/agent/sp/foursquare/db/SnsFourSquareDBHelper;->createTables(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 200
    return-void
.end method

.method public wipeData(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 4
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 203
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 205
    :try_start_0
    const-string v1, "DELETE FROM user_basic_info"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 206
    const-string v1, "DELETE FROM user_profile_info"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 207
    const-string v1, "DELETE FROM user_feed_info"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 208
    const-string v1, "DELETE FROM friends_profile_info"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 209
    const-string v1, "DELETE FROM venues_categories"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 210
    const-string v1, "DELETE FROM user_info"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 211
    const-string v1, "DELETE FROM status_stream"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 213
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 219
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 221
    :goto_0
    return-void

    .line 214
    :catch_0
    move-exception v0

    .line 215
    .local v0, "e":Landroid/database/sqlite/SQLiteException;
    :try_start_1
    const-string v1, "SnsAgent"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error in wiping db. "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 216
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 219
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    goto :goto_0

    .end local v0    # "e":Landroid/database/sqlite/SQLiteException;
    :catchall_0
    move-exception v1

    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v1
.end method
