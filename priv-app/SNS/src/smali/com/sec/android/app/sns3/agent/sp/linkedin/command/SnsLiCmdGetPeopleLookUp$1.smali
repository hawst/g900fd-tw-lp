.class Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetPeopleLookUp$1;
.super Lcom/sec/android/app/sns3/svc/sp/linkedin/request/SnsLiReqGetPeopleLookUp;
.source "SnsLiCmdGetPeopleLookUp.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetPeopleLookUp;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Handler;Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetPeopleLookUp;


# direct methods
.method constructor <init>(Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetPeopleLookUp;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Bundle;)V
    .locals 0
    .param p2, "x0"    # Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    .param p3, "x1"    # Landroid/os/Bundle;

    .prologue
    .line 55
    iput-object p1, p0, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetPeopleLookUp$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetPeopleLookUp;

    invoke-direct {p0, p2, p3}, Lcom/sec/android/app/sns3/svc/sp/linkedin/request/SnsLiReqGetPeopleLookUp;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Bundle;)V

    return-void
.end method


# virtual methods
.method public onReqRespond(IZIILandroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp;)Z
    .locals 16
    .param p1, "reqID"    # I
    .param p2, "bSuccess"    # Z
    .param p3, "httpStatus"    # I
    .param p4, "errorCode"    # I
    .param p5, "reason"    # Landroid/os/Bundle;
    .param p6, "people"    # Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp;

    .prologue
    .line 60
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/sns3/SnsApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    .line 62
    .local v4, "cr":Landroid/content/ContentResolver;
    if-eqz p2, :cond_9

    .line 63
    if-eqz p6, :cond_8

    .line 65
    move-object/from16 v12, p6

    .line 67
    .local v12, "curPeople":Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp;
    :goto_0
    if-eqz v12, :cond_8

    .line 68
    new-instance v11, Landroid/content/ContentValues;

    invoke-direct {v11}, Landroid/content/ContentValues;-><init>()V

    .line 69
    .local v11, "contentValues":Landroid/content/ContentValues;
    invoke-virtual {v11}, Landroid/content/ContentValues;->clear()V

    .line 70
    const-string v5, "member_id"

    invoke-virtual {v12}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp;->getMemberId()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v11, v5, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    const-string v5, "first_name"

    invoke-virtual {v12}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp;->getFirstName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v11, v5, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    const-string v5, "last_name"

    invoke-virtual {v12}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp;->getLastName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v11, v5, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    const-string v5, "formatted_name"

    invoke-virtual {v12}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp;->getFormattedName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v11, v5, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    const-string v5, "headline"

    invoke-virtual {v12}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp;->getHeadline()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v11, v5, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    invoke-virtual {v12}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp;->getMemberId()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_1

    .line 77
    invoke-virtual {v12}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp;->getEmail()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_0

    invoke-virtual {v12}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp;->getEmail()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_7

    .line 78
    :cond_0
    const-string v8, "email"

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetPeopleLookUp$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetPeopleLookUp;

    iget-object v5, v5, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetPeopleLookUp;->mEmailListToQuery:Ljava/util/ArrayList;

    const/4 v9, 0x0

    invoke-virtual {v5, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v11, v8, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    :cond_1
    :goto_1
    const-string v5, "profile_url"

    invoke-virtual {v12}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp;->getProfileUrl()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v11, v5, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    const-string v5, "position_title"

    invoke-virtual {v12}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp;->getPositionTitle()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v11, v5, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    const-string v5, "position_company_name"

    invoke-virtual {v12}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp;->getCompanyName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v11, v5, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    const-string v5, "picture_url"

    invoke-virtual {v12}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp;->getPictureUrl()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v11, v5, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    const-string v5, "large_picture_url"

    invoke-virtual {v12}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp;->getLargePictureUrl()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v11, v5, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 91
    invoke-virtual {v12}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp;->getMemberId()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_2

    .line 92
    const-string v5, "last_updated_timestamp"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v11, v5, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 96
    :cond_2
    const/4 v14, 0x0

    .line 97
    .local v14, "oldPictureUrl":Ljava/lang/String;
    const/4 v13, 0x0

    .line 98
    .local v13, "oldLargePictureUrl":Ljava/lang/String;
    const/4 v5, 0x2

    new-array v6, v5, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v8, "picture_url"

    aput-object v8, v6, v5

    const/4 v5, 0x1

    const-string v8, "large_picture_url"

    aput-object v8, v6, v5

    .line 102
    .local v6, "projection":[Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "member_id = "

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v12}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp;->getMemberId()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 104
    .local v7, "selection":Ljava/lang/String;
    sget-object v5, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$PeopleLookUp;->CONTENT_URI:Landroid/net/Uri;

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 106
    .local v10, "c":Landroid/database/Cursor;
    if-eqz v10, :cond_5

    .line 107
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 108
    const-string v5, "picture_url"

    invoke-interface {v10, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v10, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    .line 110
    if-eqz v14, :cond_3

    invoke-virtual {v12}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp;->getPictureUrl()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v14, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 112
    const-string v5, "picture_cache_uri"

    invoke-virtual {v11, v5}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 115
    :cond_3
    const-string v5, "large_picture_url"

    invoke-interface {v10, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v10, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 117
    if-eqz v13, :cond_4

    invoke-virtual {v12}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp;->getLargePictureUrl()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v13, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_4

    .line 120
    const-string v5, "large_picture_cache_uri"

    invoke-virtual {v11, v5}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 124
    :cond_4
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 127
    :cond_5
    sget-object v5, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$PeopleLookUp;->CONTENT_URI:Landroid/net/Uri;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "email = \'"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v12}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp;->getEmail()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "\'"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v4, v5, v11, v8, v9}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v15

    .line 129
    .local v15, "update":I
    if-nez v15, :cond_6

    .line 130
    sget-object v5, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$PeopleLookUp;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v4, v5, v11}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 133
    :cond_6
    iget-object v12, v12, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp;->mNext:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp;

    .line 134
    goto/16 :goto_0

    .line 80
    .end local v6    # "projection":[Ljava/lang/String;
    .end local v7    # "selection":Ljava/lang/String;
    .end local v10    # "c":Landroid/database/Cursor;
    .end local v13    # "oldLargePictureUrl":Ljava/lang/String;
    .end local v14    # "oldPictureUrl":Ljava/lang/String;
    .end local v15    # "update":I
    :cond_7
    const-string v5, "email"

    invoke-virtual {v12}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp;->getEmail()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v11, v5, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 137
    .end local v11    # "contentValues":Landroid/content/ContentValues;
    .end local v12    # "curPeople":Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp;
    :cond_8
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetPeopleLookUp$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetPeopleLookUp;

    sget-object v8, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$PeopleLookUp;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v8}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetPeopleLookUp;->setUri(Ljava/lang/String;)V

    .line 144
    :goto_2
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetPeopleLookUp$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetPeopleLookUp;

    move/from16 v0, p2

    invoke-virtual {v5, v0}, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetPeopleLookUp;->setSuccess(Z)V

    .line 145
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetPeopleLookUp$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetPeopleLookUp;

    move-object/from16 v0, p0

    invoke-virtual {v5, v0}, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetPeopleLookUp;->receive(Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;)V

    .line 146
    const/4 v5, 0x1

    return v5

    .line 139
    :cond_9
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetPeopleLookUp$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetPeopleLookUp;

    new-instance v8, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;

    const-string v9, "linkedin"

    move/from16 v0, p3

    move/from16 v1, p4

    move-object/from16 v2, p5

    invoke-direct {v8, v9, v0, v1, v2}, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;-><init>(Ljava/lang/String;IILandroid/os/Bundle;)V

    invoke-virtual {v5, v8}, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetPeopleLookUp;->setResponseList(Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;)Z

    .line 141
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetPeopleLookUp$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetPeopleLookUp;

    const/4 v8, 0x0

    invoke-virtual {v5, v8}, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetPeopleLookUp;->setUri(Ljava/lang/String;)V

    goto :goto_2
.end method
