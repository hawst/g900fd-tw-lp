.class public Lcom/sec/android/app/sns3/svc/sp/twitter/request/composer/SnsTwComposerUrls;
.super Ljava/lang/Object;
.source "SnsTwComposerUrls.java"


# static fields
.field public static final ACCOUNT_END_SESSION:Ljava/lang/String; = "/account/end_session.json"

.field public static final ACCOUNT_RATE_LIMIT_STATUS:Ljava/lang/String; = "/account/rate_limit_status.json"

.field public static final ACCOUNT_SETTINGS:Ljava/lang/String; = "/account/settings.json"

.field public static final ACCOUNT_TOTALS:Ljava/lang/String; = "/account/totals.json"

.field public static final ACCOUNT_UPDATE_DELIVERY_DEVICE:Ljava/lang/String; = "/account/update_delivery_device.json"

.field public static final ACCOUNT_UPDATE_PROFILE:Ljava/lang/String; = "/account/update_profile.json"

.field public static final ACCOUNT_UPDATE_PROFILE_BACKGROUND_IMAGE:Ljava/lang/String; = "/account/update_profile_background_image.json"

.field public static final ACCOUNT_UPDATE_PROFILE_COLORS:Ljava/lang/String; = "/account/update_profile_colors.json"

.field public static final ACCOUNT_UPDATE_PROFILE_IMAGE:Ljava/lang/String; = "/account/update_profile_image.json"

.field public static final ACCOUNT_VERIFY_CREDENTIALS:Ljava/lang/String; = "/account/verify_credentials.json"

.field public static final BLOCKS_BLOCKING:Ljava/lang/String; = "/blocks/blocking.json"

.field public static final BLOCKS_BLOCKING_IDS:Ljava/lang/String; = "/blocks/blocking/ids.json"

.field public static final BLOCKS_CREATE:Ljava/lang/String; = "/blocks/create.json"

.field public static final BLOCKS_DESTROY:Ljava/lang/String; = "/blocks/destroy.json"

.field public static final BLOCKS_EXISTS:Ljava/lang/String; = "/blocks/exists.json"

.field public static final DIRECTMESSAGES:Ljava/lang/String; = "/direct_messages.json"

.field public static final DIRECTMESSAGES_DESTROY:Ljava/lang/String; = "/direct_messages/destroy/"

.field public static final DIRECTMESSAGES_ID:Ljava/lang/String; = "/direct_messages/show/"

.field public static final DIRECTMESSAGES_NEW:Ljava/lang/String; = "/direct_messages/new.json"

.field public static final DIRECTMESSAGES_SENT:Ljava/lang/String; = "/direct_messages/sent.json"

.field public static final FAVORITES:Ljava/lang/String; = "/favorites.json"

.field public static final FAVORITES_CREATE:Ljava/lang/String; = "/favorites/create/"

.field public static final FAVORITES_DESTROY:Ljava/lang/String; = "/favorites/destroy/"

.field public static final FOLLOWERS_IDS:Ljava/lang/String; = "/followers/ids.json"

.field public static final FREINDSHIPS_CREATE:Ljava/lang/String; = "/friendships/create.json"

.field public static final FREINDSHIPS_DESTROY:Ljava/lang/String; = "/friendships/destroy.json"

.field public static final FREINDSHIPS_EXISTS:Ljava/lang/String; = "/friendships/exists.json"

.field public static final FREINDSHIPS_INCOMING:Ljava/lang/String; = "/friendships/incoming.json"

.field public static final FREINDSHIPS_LOOKUP:Ljava/lang/String; = "/friendships/lookup.json"

.field public static final FREINDSHIPS_NORETWEETIDS:Ljava/lang/String; = "/friendships/no_retweet_ids.json"

.field public static final FREINDSHIPS_OUTGOING:Ljava/lang/String; = "/friendships/outgoing.json"

.field public static final FREINDSHIPS_SHOW:Ljava/lang/String; = "/friendships/show.json"

.field public static final FREINDSHIPS_UPDATE:Ljava/lang/String; = "/friendships/update.json"

.field public static final FRIENDS_IDS:Ljava/lang/String; = "/friends/ids.json"

.field public static final GEO_ID:Ljava/lang/String; = "/geo/id/"

.field public static final GEO_PLACE:Ljava/lang/String; = "/geo/place.json"

.field public static final GEO_REVERSE_GEOCODE:Ljava/lang/String; = "/geo/reverse_geocode.json"

.field public static final GEO_SEARCH:Ljava/lang/String; = "/geo/search.json"

.field public static final GEO_SIMILAR_PLACES:Ljava/lang/String; = "/geo/similar_places.json"

.field public static final HELP_CONFIGURATION:Ljava/lang/String; = "/help/configuration.json"

.field public static final HELP_LANGUAGES:Ljava/lang/String; = "/help/languages.json"

.field public static final HELP_TEST:Ljava/lang/String; = "/help/test.json"

.field public static final INVALID:Ljava/lang/String;

.field public static final LEGAL_PRIVACY:Ljava/lang/String; = "/legal/privacy.json"

.field public static final LEGAL_TOS:Ljava/lang/String; = "/legal/tos.json"

.field public static final LISTS:Ljava/lang/String; = "/lists.json"

.field public static final LISTS_ALL:Ljava/lang/String; = "/lists/all.json"

.field public static final LISTS_CREATE:Ljava/lang/String; = "/lists/create.json"

.field public static final LISTS_DESTROY:Ljava/lang/String; = "/lists/destroy.json"

.field public static final LISTS_MEMBERS:Ljava/lang/String; = "/lists/members.json"

.field public static final LISTS_MEMBERSHIPS:Ljava/lang/String; = "/lists/memberships.json"

.field public static final LISTS_MEMBERS_CREATE:Ljava/lang/String; = "/lists/members/create.json"

.field public static final LISTS_MEMBERS_CREATEALL:Ljava/lang/String; = "/lists/members/create_all.json"

.field public static final LISTS_MEMBERS_DESTROY:Ljava/lang/String; = "/lists/members/destroy.json"

.field public static final LISTS_MEMBERS_SHOW:Ljava/lang/String; = "/lists/members/show.json"

.field public static final LISTS_SHOW:Ljava/lang/String; = "/lists/show.json"

.field public static final LISTS_STATUSES:Ljava/lang/String; = "/lists/statuses.json"

.field public static final LISTS_SUBSCRIBERS:Ljava/lang/String; = "/lists/subscribers.json"

.field public static final LISTS_SUBSCRIBERS_CREATE:Ljava/lang/String; = "/lists/subscribers/create.json"

.field public static final LISTS_SUBSCRIBERS_DESTROY:Ljava/lang/String; = "/lists/subscribers/destroy.json"

.field public static final LISTS_SUBSCRIBERS_SHOW:Ljava/lang/String; = "/lists/subscribers/show.json"

.field public static final LISTS_UPDATE:Ljava/lang/String; = "/lists/update.json"

.field public static final NOTIFICATIONS_FOLLOW:Ljava/lang/String; = "/notifications/follow.json"

.field public static final NOTIFICATIONS_LEAVE:Ljava/lang/String; = "/notifications/leave.json"

.field public static final OAUTH_ACCESSTOKEN:Ljava/lang/String; = "oauth/access_token"

.field public static final OAUTH_REQUESTTOKEN:Ljava/lang/String; = "oauth/request_token"

.field public static final REPORT_SPAM:Ljava/lang/String; = "/report_spam.json"

.field public static final SAVED_SEARCHES:Ljava/lang/String; = "/saved_searches.json"

.field public static final SAVED_SEARCHES_CREATE:Ljava/lang/String; = "/saved_searches/create.json"

.field public static final SAVED_SEARCHES_DESTROY:Ljava/lang/String; = "/saved_searches/destroy/"

.field public static final SAVED_SEARCHES_SHOW:Ljava/lang/String; = "/saved_searches/show/"

.field public static final SEARCH:Ljava/lang/String; = "search.json"

.field public static final STATUSES_DESTROY:Ljava/lang/String; = "/statuses/destroy/"

.field public static final STATUSES_HOMETIMELINE:Ljava/lang/String; = "/statuses/home_timeline.json"

.field public static final STATUSES_MENTIONS:Ljava/lang/String; = "/statuses/mentions.json"

.field public static final STATUSES_PUBLICTIMELINE:Ljava/lang/String; = "/statuses/public_timeline.json"

.field public static final STATUSES_RETWEET:Ljava/lang/String; = "/statuses/retweet/"

.field public static final STATUSES_RETWEETEDBY:Ljava/lang/String; = "/retweeted_by.json"

.field public static final STATUSES_RETWEETEDBYIDS:Ljava/lang/String; = "/retweeted_by/ids.json"

.field public static final STATUSES_RETWEETEDBYME:Ljava/lang/String; = "/statuses/retweeted_by_me.json"

.field public static final STATUSES_RETWEETEDBYUSER:Ljava/lang/String; = "/statuses/retweeted_by_user.json"

.field public static final STATUSES_RETWEETEDTOME:Ljava/lang/String; = "/statuses/retweeted_to_me.json"

.field public static final STATUSES_RETWEETEDTOUSER:Ljava/lang/String; = "/statuses/retweeted_to_user.json"

.field public static final STATUSES_RETWEETS:Ljava/lang/String; = "/statuses/retweets/"

.field public static final STATUSES_RETWEETSOFME:Ljava/lang/String; = "/statuses/retweets_of_me.json"

.field public static final STATUSES_SHOW:Ljava/lang/String; = "/statuses/show/"

.field public static final STATUSES_UPDATE:Ljava/lang/String; = "/statuses/update.json"

.field public static final STATUSES_UPDATEWITHMEDIA:Ljava/lang/String; = "/statuses/update_with_media.json"

.field public static final STATUSES_USERTIMELINE:Ljava/lang/String; = "/statuses/user_timeline.json"

.field public static final TRENDS:Ljava/lang/String; = "/trends.json"

.field public static final TRENDS_AVAILABLE:Ljava/lang/String; = "/trends/available.json"

.field public static final TRENDS_CURRENT:Ljava/lang/String; = "/trends/current.json"

.field public static final TRENDS_DAILY:Ljava/lang/String; = "/trends/daily.json"

.field public static final TRENDS_WEEKLY:Ljava/lang/String; = "/trends/weekly.json"

.field public static final TRENDS_WOEID:Ljava/lang/String; = "/trends/"

.field public static final USERS_CONTRIBUTEES:Ljava/lang/String; = "/users/contributees.json"

.field public static final USERS_CONTRIBUTORS:Ljava/lang/String; = "/users/contributors.json"

.field public static final USERS_LOOKUP:Ljava/lang/String; = "/users/lookup.json"

.field public static final USERS_PROFILEIMAGE:Ljava/lang/String; = "/users/profile_image/"

.field public static final USERS_SEARCH:Ljava/lang/String; = "/users/search.json"

.field public static final USERS_SHOW:Ljava/lang/String; = "/users/show.json"

.field public static final USERS_SUGGESTIONS:Ljava/lang/String; = "/users/suggestions.json"

.field public static final USERS_SUGGESTIONS_SLUG:Ljava/lang/String; = "/users/suggestions/"

.field public static final USERS_SUGGESTIONS_SLUG_MEMBERS:Ljava/lang/String; = "/users/suggestions/"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/composer/SnsTwComposerUrls;->INVALID:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
