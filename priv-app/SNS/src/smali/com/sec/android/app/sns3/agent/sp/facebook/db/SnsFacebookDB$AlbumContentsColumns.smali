.class public interface abstract Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$AlbumContentsColumns;
.super Ljava/lang/Object;
.source "SnsFacebookDB.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "AlbumContentsColumns"
.end annotation


# static fields
.field public static final CREATE_TIME:Ljava/lang/String; = "created_time"

.field public static final HEIGHT:Ljava/lang/String; = "height"

.field public static final PHOTO_ID:Ljava/lang/String; = "photo_id"

.field public static final PICTURE:Ljava/lang/String; = "picture"

.field public static final POST_ID:Ljava/lang/String; = "feed_id"

.field public static final UPDATE_TIME:Ljava/lang/String; = "updated_time"

.field public static final WIDTH:Ljava/lang/String; = "width"
