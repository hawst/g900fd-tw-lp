.class public interface abstract Lcom/sec/android/app/sns3/svc/sp/foursquare/parser/SnsFsParserCategories$FourSquareCategories;
.super Ljava/lang/Object;
.source "SnsFsParserCategories.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/svc/sp/foursquare/parser/SnsFsParserCategories;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "FourSquareCategories"
.end annotation


# static fields
.field public static final CATEGORIES:Ljava/lang/String; = "categories"

.field public static final CODE:Ljava/lang/String; = "code"

.field public static final ICON:Ljava/lang/String; = "icon"

.field public static final ICON_PREFIX:Ljava/lang/String; = "prefix"

.field public static final ICON_SUFFIX:Ljava/lang/String; = "suffix"

.field public static final ID:Ljava/lang/String; = "id"

.field public static final META:Ljava/lang/String; = "meta"

.field public static final NAME:Ljava/lang/String; = "name"

.field public static final PLURAL_NAME:Ljava/lang/String; = "pluralName"

.field public static final RESPONSE:Ljava/lang/String; = "response"
