.class interface abstract Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseContactInfo$LinkedInContactInfo;
.super Ljava/lang/Object;
.source "SnsLiResponseContactInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseContactInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x608
    name = "LinkedInContactInfo"
.end annotation


# static fields
.field public static final ARRAY_VALUES:Ljava/lang/String; = "values"

.field public static final IM_ACCOUNT:Ljava/lang/String; = "imAccount"

.field public static final IM_ACCOUNTS:Ljava/lang/String; = "imAccounts"

.field public static final IM_ACCOUNT_NAME:Ljava/lang/String; = "imAccountName"

.field public static final IM_ACCOUNT_TYPE:Ljava/lang/String; = "imAccountType"

.field public static final PHONE_NUMBER:Ljava/lang/String; = "phoneNumber"

.field public static final PHONE_NUMBERS:Ljava/lang/String; = "phoneNumbers"

.field public static final PHONE_NUMBER_ITEM_TYPE:Ljava/lang/String; = "phoneType"

.field public static final TOTAL:Ljava/lang/String; = "_total"

.field public static final TWITTER_ACCOUNT:Ljava/lang/String; = "twitterAccount"

.field public static final TWITTER_ACCOUNTS:Ljava/lang/String; = "twitterAccounts"

.field public static final TWITTER_ACCOUNT_ID:Ljava/lang/String; = "providerAccountId"

.field public static final TWITTER_ACCOUNT_NAME:Ljava/lang/String; = "providerAccountName"
