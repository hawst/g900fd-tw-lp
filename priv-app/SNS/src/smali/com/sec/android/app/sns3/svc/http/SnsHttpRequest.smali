.class public Lcom/sec/android/app/sns3/svc/http/SnsHttpRequest;
.super Lorg/apache/http/client/methods/HttpEntityEnclosingRequestBase;
.source "SnsHttpRequest.java"


# static fields
.field private static final CONTENT_ENCODING_UTF_8:Ljava/lang/String; = "UTF-8"

.field private static final _BODY:Ljava/lang/String; = "body"

.field public static final _CONTENT_TYPE:Ljava/lang/String; = "Content-Type"

.field private static final _FILENAME:Ljava/lang/String; = "filename"

.field public static final _MULTIPART_FORM_DATA:Ljava/lang/String; = "multipart/form-data"


# instance fields
.field mFwkReqID:I

.field mHttpMethod:Ljava/lang/String;


# direct methods
.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "reqID"    # I
    .param p2, "httpMethod"    # Ljava/lang/String;
    .param p3, "reqUri"    # Ljava/lang/String;

    .prologue
    .line 70
    invoke-direct {p0}, Lorg/apache/http/client/methods/HttpEntityEnclosingRequestBase;-><init>()V

    .line 72
    iput p1, p0, Lcom/sec/android/app/sns3/svc/http/SnsHttpRequest;->mFwkReqID:I

    .line 73
    iput-object p2, p0, Lcom/sec/android/app/sns3/svc/http/SnsHttpRequest;->mHttpMethod:Ljava/lang/String;

    .line 76
    :try_start_0
    new-instance v1, Ljava/net/URI;

    invoke-direct {v1, p3}, Ljava/net/URI;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v1}, Lcom/sec/android/app/sns3/svc/http/SnsHttpRequest;->setURI(Ljava/net/URI;)V
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    .line 80
    :goto_0
    return-void

    .line 77
    :catch_0
    move-exception v0

    .line 78
    .local v0, "e":Ljava/net/URISyntaxException;
    invoke-virtual {v0}, Ljava/net/URISyntaxException;->printStackTrace()V

    goto :goto_0
.end method

.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;)V
    .locals 7
    .param p1, "reqID"    # I
    .param p2, "httpMethod"    # Ljava/lang/String;
    .param p3, "reqUri"    # Ljava/lang/String;
    .param p4, "header"    # Landroid/os/Bundle;
    .param p5, "body"    # Landroid/os/Bundle;

    .prologue
    .line 83
    const/4 v6, 0x0

    move-object v0, p0

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/sns3/svc/http/SnsHttpRequest;-><init>(ILjava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;Lorg/apache/http/entity/mime/MultipartEntity;)V

    .line 84
    return-void
.end method

.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;Lorg/apache/http/entity/mime/MultipartEntity;)V
    .locals 16
    .param p1, "reqID"    # I
    .param p2, "httpMethod"    # Ljava/lang/String;
    .param p3, "reqUri"    # Ljava/lang/String;
    .param p4, "header"    # Landroid/os/Bundle;
    .param p5, "body"    # Landroid/os/Bundle;
    .param p6, "reqEntity"    # Lorg/apache/http/entity/mime/MultipartEntity;

    .prologue
    .line 87
    invoke-direct/range {p0 .. p3}, Lcom/sec/android/app/sns3/svc/http/SnsHttpRequest;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    .line 90
    if-eqz p4, :cond_2

    .line 91
    :try_start_0
    invoke-virtual/range {p4 .. p4}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v12

    .line 92
    .local v12, "keySet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {v12}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v10

    .local v10, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_2

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    .line 93
    .local v8, "headerKey":Ljava/lang/String;
    move-object/from16 v0, p4

    invoke-virtual {v0, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 94
    .local v9, "headerValue":Ljava/lang/String;
    if-eqz v9, :cond_0

    .line 95
    move-object/from16 v0, p0

    invoke-virtual {v0, v8, v9}, Lcom/sec/android/app/sns3/svc/http/SnsHttpRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 152
    .end local v8    # "headerKey":Ljava/lang/String;
    .end local v9    # "headerValue":Ljava/lang/String;
    .end local v10    # "i$":Ljava/util/Iterator;
    .end local v12    # "keySet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :catch_0
    move-exception v6

    .line 153
    .local v6, "e":Ljava/io/UnsupportedEncodingException;
    invoke-virtual {v6}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    .line 154
    const-string v14, "SNS"

    const-string v15, "SnsHttpRequest : CONSTRUCTOR : UnsupportedEncodingException occur"

    invoke-static {v14, v15, v6}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 159
    .end local v6    # "e":Ljava/io/UnsupportedEncodingException;
    :cond_1
    :goto_1
    return-void

    .line 99
    :cond_2
    if-eqz p5, :cond_1

    :try_start_1
    invoke-virtual/range {p5 .. p5}, Landroid/os/Bundle;->isEmpty()Z

    move-result v14

    if-nez v14, :cond_1

    .line 101
    const-string v14, "Content-Type"

    move-object/from16 v0, p5

    invoke-virtual {v0, v14}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 103
    .local v5, "contentType":Ljava/lang/String;
    if-eqz v5, :cond_6

    const-string v14, "multipart/form-data"

    invoke-virtual {v5, v14}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_6

    .line 108
    if-nez p6, :cond_3

    .line 109
    new-instance v13, Lorg/apache/http/entity/mime/MultipartEntity;

    sget-object v14, Lorg/apache/http/entity/mime/HttpMultipartMode;->BROWSER_COMPATIBLE:Lorg/apache/http/entity/mime/HttpMultipartMode;

    invoke-direct {v13, v14}, Lorg/apache/http/entity/mime/MultipartEntity;-><init>(Lorg/apache/http/entity/mime/HttpMultipartMode;)V

    .end local p6    # "reqEntity":Lorg/apache/http/entity/mime/MultipartEntity;
    .local v13, "reqEntity":Lorg/apache/http/entity/mime/MultipartEntity;
    move-object/from16 p6, v13

    .line 111
    .end local v13    # "reqEntity":Lorg/apache/http/entity/mime/MultipartEntity;
    .restart local p6    # "reqEntity":Lorg/apache/http/entity/mime/MultipartEntity;
    :cond_3
    invoke-virtual/range {p5 .. p5}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v3

    .line 112
    .local v3, "bodyKeySet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v10

    .restart local v10    # "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_5

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 114
    .local v4, "bodykey":Ljava/lang/String;
    move-object/from16 v0, p5

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v7

    check-cast v7, Landroid/os/ParcelFileDescriptor;

    .line 116
    .local v7, "fileDesc":Landroid/os/ParcelFileDescriptor;
    if-eqz v7, :cond_4

    move-object/from16 v0, p5

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v14

    instance-of v14, v14, Landroid/os/ParcelFileDescriptor;

    if-eqz v14, :cond_4

    .line 119
    invoke-virtual {v7}, Landroid/os/ParcelFileDescriptor;->getStatSize()J

    move-result-wide v14

    sput-wide v14, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;->mContentSize:J

    .line 121
    new-instance v11, Lorg/apache/http/entity/mime/content/InputStreamBody;

    new-instance v14, Landroid/os/ParcelFileDescriptor$AutoCloseInputStream;

    invoke-direct {v14, v7}, Landroid/os/ParcelFileDescriptor$AutoCloseInputStream;-><init>(Landroid/os/ParcelFileDescriptor;)V

    const-string v15, "filename"

    move-object/from16 v0, p5

    invoke-virtual {v0, v15}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    invoke-direct {v11, v14, v15}, Lorg/apache/http/entity/mime/content/InputStreamBody;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    .line 123
    .local v11, "isb":Lorg/apache/http/entity/mime/content/InputStreamBody;
    move-object/from16 v0, p6

    invoke-virtual {v0, v4, v11}, Lorg/apache/http/entity/mime/MultipartEntity;->addPart(Ljava/lang/String;Lorg/apache/http/entity/mime/content/ContentBody;)V
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_2

    .line 156
    .end local v3    # "bodyKeySet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v4    # "bodykey":Ljava/lang/String;
    .end local v5    # "contentType":Ljava/lang/String;
    .end local v7    # "fileDesc":Landroid/os/ParcelFileDescriptor;
    .end local v10    # "i$":Ljava/util/Iterator;
    .end local v11    # "isb":Lorg/apache/http/entity/mime/content/InputStreamBody;
    :catch_1
    move-exception v6

    .line 157
    .local v6, "e":Ljava/lang/Exception;
    const-string v14, "SNS"

    invoke-virtual {v6}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 125
    .end local v6    # "e":Ljava/lang/Exception;
    .restart local v3    # "bodyKeySet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .restart local v4    # "bodykey":Ljava/lang/String;
    .restart local v5    # "contentType":Ljava/lang/String;
    .restart local v7    # "fileDesc":Landroid/os/ParcelFileDescriptor;
    .restart local v10    # "i$":Ljava/util/Iterator;
    :cond_4
    :try_start_2
    new-instance v14, Lorg/apache/http/entity/mime/content/StringBody;

    move-object/from16 v0, p5

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    invoke-direct {v14, v15}, Lorg/apache/http/entity/mime/content/StringBody;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p6

    invoke-virtual {v0, v4, v14}, Lorg/apache/http/entity/mime/MultipartEntity;->addPart(Ljava/lang/String;Lorg/apache/http/entity/mime/content/ContentBody;)V

    goto :goto_2

    .line 129
    .end local v4    # "bodykey":Ljava/lang/String;
    .end local v7    # "fileDesc":Landroid/os/ParcelFileDescriptor;
    :cond_5
    move-object/from16 v0, p0

    move-object/from16 v1, p6

    invoke-virtual {v0, v1}, Lcom/sec/android/app/sns3/svc/http/SnsHttpRequest;->setEntity(Lorg/apache/http/HttpEntity;)V

    goto/16 :goto_1

    .line 133
    .end local v3    # "bodyKeySet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v10    # "i$":Ljava/util/Iterator;
    :cond_6
    const-string v14, "body"

    move-object/from16 v0, p5

    invoke-virtual {v0, v14}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    if-nez v14, :cond_7

    .line 134
    new-instance v14, Ljava/lang/Exception;

    const-string v15, "SnsHttpRequest : omitted Body error"

    invoke-direct {v14, v15}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v14

    .line 136
    :cond_7
    new-instance v2, Lorg/apache/http/entity/ByteArrayEntity;

    const-string v14, "body"

    move-object/from16 v0, p5

    invoke-virtual {v0, v14}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    const-string v15, "UTF-8"

    invoke-virtual {v14, v15}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v14

    invoke-direct {v2, v14}, Lorg/apache/http/entity/ByteArrayEntity;-><init>([B)V

    .line 139
    .local v2, "be":Lorg/apache/http/entity/ByteArrayEntity;
    if-eqz v5, :cond_8

    .line 140
    invoke-virtual {v2, v5}, Lorg/apache/http/entity/ByteArrayEntity;->setContentType(Ljava/lang/String;)V

    .line 146
    :goto_3
    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/sns3/svc/http/SnsHttpRequest;->setEntity(Lorg/apache/http/HttpEntity;)V

    goto/16 :goto_1

    .line 142
    :cond_8
    const-string v14, "text/xml"

    invoke-virtual {v2, v14}, Lorg/apache/http/entity/ByteArrayEntity;->setContentType(Ljava/lang/String;)V

    .line 143
    const-string v14, "UTF-8"

    invoke-virtual {v2, v14}, Lorg/apache/http/entity/ByteArrayEntity;->setContentEncoding(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_3
.end method


# virtual methods
.method public getFwkReqID()I
    .locals 1

    .prologue
    .line 167
    iget v0, p0, Lcom/sec/android/app/sns3/svc/http/SnsHttpRequest;->mFwkReqID:I

    return v0
.end method

.method public getMethod()Ljava/lang/String;
    .locals 1

    .prologue
    .line 172
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/http/SnsHttpRequest;->mHttpMethod:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 7

    .prologue
    .line 177
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 179
    .local v4, "sb":Ljava/lang/StringBuilder;
    const-string v5, "##########################################################################################\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 180
    const-string v5, "<< REQUEST >> \n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 181
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "reqID = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/sec/android/app/sns3/svc/http/SnsHttpRequest;->mFwkReqID:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 182
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "method = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/sns3/svc/http/SnsHttpRequest;->mHttpMethod:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 183
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "url = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/svc/http/SnsHttpRequest;->getURI()Ljava/net/URI;

    move-result-object v6

    invoke-virtual {v6}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 185
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/svc/http/SnsHttpRequest;->getAllHeaders()[Lorg/apache/http/Header;

    move-result-object v5

    array-length v5, v5

    if-lez v5, :cond_0

    .line 186
    const-string v5, "header = \n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 188
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/svc/http/SnsHttpRequest;->getAllHeaders()[Lorg/apache/http/Header;

    move-result-object v0

    .local v0, "arr$":[Lorg/apache/http/Header;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v1, v0, v2

    .line 189
    .local v1, "header":Lorg/apache/http/Header;
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 190
    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 188
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 213
    .end local v0    # "arr$":[Lorg/apache/http/Header;
    .end local v1    # "header":Lorg/apache/http/Header;
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    :cond_0
    const-string v5, "##########################################################################################\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 214
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    return-object v5
.end method
