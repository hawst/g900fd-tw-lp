.class public Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserUser;
.super Ljava/lang/Object;
.source "SnsFbParserUser.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserUser$FacebookUser;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    return-void
.end method

.method public static parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseUser;
    .locals 4
    .param p0, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 94
    new-instance v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseUser;

    invoke-direct {v2}, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseUser;-><init>()V

    .line 97
    .local v2, "user":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseUser;
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 99
    .local v1, "jsonObject":Lorg/json/JSONObject;
    const-string v3, "id"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseUser;->mUserID:Ljava/lang/String;

    .line 100
    const-string v3, "name"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseUser;->mName:Ljava/lang/String;

    .line 101
    const-string v3, "first_name"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseUser;->mFirstName:Ljava/lang/String;

    .line 102
    const-string v3, "middle_name"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseUser;->mMiddleName:Ljava/lang/String;

    .line 103
    const-string v3, "last_name"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseUser;->mLastName:Ljava/lang/String;

    .line 105
    const-string v3, "gender"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseUser;->mGender:Ljava/lang/String;

    .line 106
    const-string v3, "locale"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseUser;->mLocale:Ljava/lang/String;

    .line 107
    const-string v3, "language"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseUser;->mLanguage:Ljava/lang/String;

    .line 108
    const-string v3, "link"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseUser;->mLink:Ljava/lang/String;

    .line 109
    const-string v3, "username"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseUser;->mUserName:Ljava/lang/String;

    .line 110
    const-string v3, "third_party_id"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseUser;->mThirdPartyID:Ljava/lang/String;

    .line 111
    const-string v3, "timezone"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseUser;->mTimeZone:Ljava/lang/String;

    .line 113
    const-string v3, "updated_time"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseUser;->mUpdatedTime:Ljava/lang/String;

    .line 114
    const-string v3, "verified"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseUser;->mVerified:Ljava/lang/String;

    .line 115
    const-string v3, "bio"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseUser;->mBio:Ljava/lang/String;

    .line 116
    const-string v3, "birthday"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseUser;->mBirthday:Ljava/lang/String;

    .line 117
    const-string v3, "education"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseUser;->mEducation:Ljava/lang/String;

    .line 118
    const-string v3, "email"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseUser;->mEmail:Ljava/lang/String;

    .line 120
    const-string v3, "hometown"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseUser;->mHomeTown:Ljava/lang/String;

    .line 121
    const-string v3, "interested_in"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseUser;->mInterstedIn:Ljava/lang/String;

    .line 122
    const-string v3, "location"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseUser;->mLocation:Ljava/lang/String;

    .line 123
    const-string v3, "political"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseUser;->mPolitical:Ljava/lang/String;

    .line 124
    const-string v3, "favorite_athletes"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseUser;->mFavoriteAthletes:Ljava/lang/String;

    .line 125
    const-string v3, "favorite_teams"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseUser;->mFavoriteTeams:Ljava/lang/String;

    .line 126
    const-string v3, "quotes"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseUser;->mQuotes:Ljava/lang/String;

    .line 128
    const-string v3, "relationship_status"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseUser;->mRelationshipStatus:Ljava/lang/String;

    .line 129
    const-string v3, "religion"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseUser;->mReligion:Ljava/lang/String;

    .line 130
    const-string v3, "significant_other"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseUser;->mSignificantOther:Ljava/lang/String;

    .line 131
    const-string v3, "video_upload_limits"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseUser;->mViedoUploadLimits:Ljava/lang/String;

    .line 132
    const-string v3, "website"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseUser;->mWebSite:Ljava/lang/String;

    .line 133
    const-string v3, "work"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseUser;->mWork:Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 141
    .end local v1    # "jsonObject":Lorg/json/JSONObject;
    :goto_0
    return-object v2

    .line 136
    :catch_0
    move-exception v0

    .line 138
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method
