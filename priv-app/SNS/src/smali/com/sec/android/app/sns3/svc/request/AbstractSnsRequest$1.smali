.class Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest$1;
.super Ljava/util/TimerTask;
.source "AbstractSnsRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->request()Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;


# direct methods
.method constructor <init>(Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;)V
    .locals 0

    .prologue
    .line 105
    iput-object p1, p0, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest$1;->this$0:Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;

    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 108
    const-string v2, "SNS"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "TimerTask : run() : mReqID = ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest$1;->this$0:Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;

    iget v4, v4, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->mReqID:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] TIMEOUT!!"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 110
    iget-object v2, p0, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest$1;->this$0:Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;

    iget-object v2, v2, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    invoke-virtual {v2}, Lcom/sec/android/app/sns3/svc/SnsSvcMgr;->getRequestMgrHandler()Landroid/os/Handler;

    move-result-object v1

    .line 111
    .local v1, "reqMgrHandler":Landroid/os/Handler;
    const/16 v2, 0x16

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 113
    .local v0, "msg":Landroid/os/Message;
    iget-object v2, p0, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest$1;->this$0:Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;

    iget v2, v2, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->mReqID:I

    iput v2, v0, Landroid/os/Message;->arg1:I

    .line 114
    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 115
    return-void
.end method
