.class public Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseMedia;
.super Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;
.source "SnsTwResponseMedia.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseMedia;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public mDisplayUrl:Ljava/lang/String;

.field public mExpandedUrl:Ljava/lang/String;

.field public mId:Ljava/lang/String;

.field public mIndices:Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseIntArray;

.field public mMediaUrl:Ljava/lang/String;

.field public mMediaUrlHttps:Ljava/lang/String;

.field public mNext:Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseMedia;

.field public mSizes:Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseSizes;

.field public mType:Ljava/lang/String;

.field public mUrl:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 56
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseMedia$1;

    invoke-direct {v0}, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseMedia$1;-><init>()V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseMedia;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;-><init>()V

    .line 50
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 52
    invoke-direct {p0}, Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;-><init>()V

    .line 53
    invoke-virtual {p0, p1}, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseMedia;->readFromParcel(Landroid/os/Parcel;)V

    .line 54
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 82
    const/4 v0, 0x0

    return v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 68
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseMedia;->mId:Ljava/lang/String;

    .line 69
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseMedia;->mMediaUrl:Ljava/lang/String;

    .line 70
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseMedia;->mMediaUrlHttps:Ljava/lang/String;

    .line 71
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseMedia;->mUrl:Ljava/lang/String;

    .line 72
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseMedia;->mDisplayUrl:Ljava/lang/String;

    .line 73
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseMedia;->mExpandedUrl:Ljava/lang/String;

    .line 74
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseMedia;->mType:Ljava/lang/String;

    .line 76
    const-class v0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseIntArray;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseIntArray;

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseMedia;->mIndices:Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseIntArray;

    .line 77
    const-class v0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseMedia;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseMedia;

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseMedia;->mNext:Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseMedia;

    .line 78
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 88
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseMedia;->mId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 89
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseMedia;->mMediaUrl:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 90
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseMedia;->mMediaUrlHttps:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 91
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseMedia;->mUrl:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 92
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseMedia;->mDisplayUrl:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 93
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseMedia;->mExpandedUrl:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 94
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseMedia;->mType:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 96
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseMedia;->mIndices:Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseIntArray;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 97
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseMedia;->mNext:Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseMedia;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 98
    return-void
.end method
