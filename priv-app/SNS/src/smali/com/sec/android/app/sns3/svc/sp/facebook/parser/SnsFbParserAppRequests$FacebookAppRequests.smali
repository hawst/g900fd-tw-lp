.class public interface abstract Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserAppRequests$FacebookAppRequests;
.super Ljava/lang/Object;
.source "SnsFbParserAppRequests.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserAppRequests;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "FacebookAppRequests"
.end annotation


# static fields
.field public static final APP_ID:Ljava/lang/String; = "app_id"

.field public static final CREATED_TIME:Ljava/lang/String; = "created_time"

.field public static final DATA:Ljava/lang/String; = "data"

.field public static final MESSAGE:Ljava/lang/String; = "message"

.field public static final RECIPIENT_UID:Ljava/lang/String; = "recipient_uid"

.field public static final REQUEST_ID:Ljava/lang/String; = "request_id"

.field public static final SENDER_UID:Ljava/lang/String; = "sender_uid"
