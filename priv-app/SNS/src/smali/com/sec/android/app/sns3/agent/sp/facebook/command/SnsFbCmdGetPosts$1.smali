.class Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPosts$1;
.super Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqGetPosts;
.source "SnsFbCmdGetPosts.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPosts;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Handler;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPosts;


# direct methods
.method constructor <init>(Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPosts;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;)V
    .locals 0
    .param p2, "x0"    # Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    .param p3, "x1"    # Ljava/lang/String;

    .prologue
    .line 53
    iput-object p1, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPosts$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPosts;

    invoke-direct {p0, p2, p3}, Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqGetPosts;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public onReqRespond(IZIILandroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePosts;)Z
    .locals 9
    .param p1, "reqID"    # I
    .param p2, "bSuccess"    # Z
    .param p3, "httpStatus"    # I
    .param p4, "errorCode"    # I
    .param p5, "reason"    # Landroid/os/Bundle;
    .param p6, "posts"    # Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePosts;

    .prologue
    .line 58
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/sns3/SnsApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 60
    .local v1, "cr":Landroid/content/ContentResolver;
    if-eqz p2, :cond_5

    .line 61
    if-eqz p6, :cond_4

    .line 63
    iget-object v2, p6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePosts;->mPosts:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;

    .line 64
    .local v2, "tempPost":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;
    if-eqz v2, :cond_3

    iget-object v4, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mFrom:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFrom;

    if-eqz v4, :cond_3

    .line 65
    sget-object v4, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$StatusStream;->CONTENT_URI:Landroid/net/Uri;

    const-string v5, "from_id=?"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    iget-object v8, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mFrom:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFrom;

    iget-object v8, v8, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFrom;->mFromID:Ljava/lang/String;

    aput-object v8, v6, v7

    invoke-virtual {v1, v4, v5, v6}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 70
    :goto_0
    if-eqz v2, :cond_4

    .line 72
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 73
    .local v0, "contentValues":Landroid/content/ContentValues;
    invoke-virtual {v0}, Landroid/content/ContentValues;->clear()V

    .line 74
    const-string v4, "post_id"

    iget-object v5, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mPostID:Ljava/lang/String;

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    const-string v4, "from_id"

    iget-object v5, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mFrom:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFrom;

    iget-object v5, v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFrom;->mFromID:Ljava/lang/String;

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    const-string v4, "from_name"

    iget-object v5, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mFrom:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFrom;

    iget-object v5, v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFrom;->mFromName:Ljava/lang/String;

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    const-string v4, "to_id"

    iget-object v5, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mToID:Ljava/lang/String;

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    const-string v4, "to_name"

    iget-object v5, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mToName:Ljava/lang/String;

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    const-string v4, "message"

    iget-object v5, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mMessage:Ljava/lang/String;

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    const-string v4, "picture"

    iget-object v5, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mPicture:Ljava/lang/String;

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    const-string v4, "link"

    iget-object v5, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mLink:Ljava/lang/String;

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    const-string v4, "name"

    iget-object v5, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mName:Ljava/lang/String;

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    const-string v4, "caption"

    iget-object v5, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mCaption:Ljava/lang/String;

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    const-string v4, "description"

    iget-object v5, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mDescription:Ljava/lang/String;

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    const-string v4, "type"

    iget-object v5, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mType:Ljava/lang/String;

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    const-string v4, "likes_count"

    iget-object v5, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mLikeCount:Ljava/lang/String;

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    const-string v4, "comments_count"

    iget-object v5, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mCommentCount:Ljava/lang/String;

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    const-string v4, "create_time"

    iget-object v5, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mCreatedTime:Ljava/lang/String;

    invoke-static {v5}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->toTimestamp(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 91
    const-string v4, "updated_time"

    iget-object v5, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mUpdatedTime:Ljava/lang/String;

    invoke-static {v5}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->toTimestamp(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 93
    iget-object v4, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mPlace:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePlace;

    if-eqz v4, :cond_0

    .line 94
    const-string v4, "place_name"

    iget-object v5, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mPlace:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePlace;

    iget-object v5, v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePlace;->mPlaceName:Ljava/lang/String;

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    const-string v4, "street"

    iget-object v5, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mPlace:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePlace;

    iget-object v5, v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePlace;->mStreet:Ljava/lang/String;

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    const-string v4, "city"

    iget-object v5, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mPlace:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePlace;

    iget-object v5, v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePlace;->mCity:Ljava/lang/String;

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    const-string v4, "state"

    iget-object v5, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mPlace:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePlace;

    iget-object v5, v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePlace;->mState:Ljava/lang/String;

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 99
    const-string v4, "country"

    iget-object v5, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mPlace:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePlace;

    iget-object v5, v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePlace;->mCountry:Ljava/lang/String;

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    const-string v4, "zip"

    iget-object v5, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mPlace:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePlace;

    iget-object v5, v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePlace;->mZip:Ljava/lang/String;

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    const-string v4, "latitude"

    iget-object v5, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mPlace:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePlace;

    iget-object v5, v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePlace;->mLatitude:Ljava/lang/String;

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    const-string v4, "longitude"

    iget-object v5, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mPlace:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePlace;

    iget-object v5, v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePlace;->mLongitude:Ljava/lang/String;

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    :cond_0
    sget-object v4, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$StatusStream;->CONTENT_URI:Landroid/net/Uri;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "post_id=\'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mPostID:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v1, v4, v0, v5, v6}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v3

    .line 110
    .local v3, "update":I
    if-nez v3, :cond_2

    .line 111
    iget-object v4, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mType:Ljava/lang/String;

    const-string v5, "status"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v4, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mStory:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_2

    .line 113
    :cond_1
    sget-object v4, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$StatusStream;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1, v4, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 117
    :cond_2
    iget-object v2, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;

    .line 118
    goto/16 :goto_0

    .line 121
    .end local v0    # "contentValues":Landroid/content/ContentValues;
    .end local v3    # "update":I
    :cond_3
    const-string v4, "SnsStautsStream"

    const-string v5, "can\'t find user id"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 124
    .end local v2    # "tempPost":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;
    :cond_4
    iget-object v4, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPosts$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPosts;

    sget-object v5, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$StatusStream;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v5}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPosts;->setUri(Ljava/lang/String;)V

    .line 131
    :goto_1
    iget-object v4, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPosts$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPosts;

    invoke-virtual {v4, p2}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPosts;->setSuccess(Z)V

    .line 132
    iget-object v4, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPosts$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPosts;

    invoke-virtual {v4, p0}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPosts;->receive(Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;)V

    .line 134
    const/4 v4, 0x1

    return v4

    .line 126
    :cond_5
    iget-object v4, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPosts$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPosts;

    new-instance v5, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;

    const-string v6, "facebook"

    invoke-direct {v5, v6, p3, p4, p5}, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;-><init>(Ljava/lang/String;IILandroid/os/Bundle;)V

    invoke-virtual {v4, v5}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPosts;->setResponseList(Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;)Z

    .line 128
    iget-object v4, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPosts$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPosts;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPosts;->setUri(Ljava/lang/String;)V

    goto :goto_1
.end method
