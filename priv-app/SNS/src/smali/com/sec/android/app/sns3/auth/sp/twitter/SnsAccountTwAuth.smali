.class public Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuth;
.super Ljava/lang/Object;
.source "SnsAccountTwAuth.java"


# static fields
.field public static final ACTION_SNS_TWITTER_LOGGED_IN:Ljava/lang/String; = "com.sec.android.app.sns.ACTION_SNS_TWITTER_LOGGED_IN"

.field public static final ACTION_SNS_TWITTER_LOGGED_OUT:Ljava/lang/String; = "com.sec.android.app.sns.ACTION_SNS_TWITTER_LOGGED_OUT"

.field public static final EXTRA_SKIP_SSO_NOTI:Ljava/lang/String; = "skip_sso_noti"

.field public static final EXTRA_SKIP_SYNC_ADAPTER_OF_MARKET_APP:Ljava/lang/String; = "skip_sync_marketapp"

.field public static final EXTRA_SKIP_SYNC_SETUP:Ljava/lang/String; = "skip_sync_setup"

.field public static final HOME_FEED_PREFERENCE_KEY:Ljava/lang/String; = "TW_home_feed_sync_interval"

.field public static final LOGIN_REQUEST_CODE:I = 0x7d0

.field public static final LOGIN_TWITTER_NOTIFICATION_ID:I = 0x898

.field public static final MARKET_APP_SSO:Z = true

.field public static final PROFILE_FEED_PREFERENCE_KEY:Ljava/lang/String; = "TW_profile_feed_sync_interval"

.field public static final RETRY_LOGIN_NOTIFICATION_ID:I = 0x8fc

.field public static final SNS_BROADCAST_PERMISSION:Ljava/lang/String; = "com.sec.android.app.sns3.permission.RECEIVE_SNS_BROADCAST"

.field public static final TWITTER_APP_PREFERENCE_KEY:Ljava/lang/String; = "TW_twitter_app_sync_interval"

.field public static final TWITTER_SSO_NOTIFICATION_ID:I = 0x834


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
