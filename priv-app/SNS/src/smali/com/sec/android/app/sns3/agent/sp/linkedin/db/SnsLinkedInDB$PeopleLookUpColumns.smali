.class public interface abstract Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$PeopleLookUpColumns;
.super Ljava/lang/Object;
.source "SnsLinkedInDB.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "PeopleLookUpColumns"
.end annotation


# static fields
.field public static final EMAIL:Ljava/lang/String; = "email"

.field public static final FIRST_NAME:Ljava/lang/String; = "first_name"

.field public static final FORMATTED_NAME:Ljava/lang/String; = "formatted_name"

.field public static final HEADLINE:Ljava/lang/String; = "headline"

.field public static final LARGE_PICTURE_CACHE_URI:Ljava/lang/String; = "large_picture_cache_uri"

.field public static final LARGE_PICTURE_URL:Ljava/lang/String; = "large_picture_url"

.field public static final LAST_NAME:Ljava/lang/String; = "last_name"

.field public static final LAST_UPDATED_TIMESTAMP:Ljava/lang/String; = "last_updated_timestamp"

.field public static final MEMBER_ID:Ljava/lang/String; = "member_id"

.field public static final PHONE_NUMBER:Ljava/lang/String; = "phone_number"

.field public static final PICTURE_CACHE_URI:Ljava/lang/String; = "picture_cache_uri"

.field public static final PICTURE_URL:Ljava/lang/String; = "picture_url"

.field public static final POSITION_COMPANY_NAME:Ljava/lang/String; = "position_company_name"

.field public static final POSITION_TITLE:Ljava/lang/String; = "position_title"

.field public static final PROFILE_URL:Ljava/lang/String; = "profile_url"
