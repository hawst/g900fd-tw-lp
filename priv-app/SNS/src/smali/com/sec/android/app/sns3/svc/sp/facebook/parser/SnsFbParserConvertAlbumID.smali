.class public Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserConvertAlbumID;
.super Ljava/lang/Object;
.source "SnsFbParserConvertAlbumID.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserConvertAlbumID$FacebookConvertAlbumID;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    return-void
.end method

.method public static parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseConvertID;
    .locals 9
    .param p0, "contentObj"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    .line 40
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseConvertID;

    invoke-direct {v0}, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseConvertID;-><init>()V

    .line 43
    .local v0, "albumID":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseConvertID;
    :try_start_0
    new-instance v2, Lorg/json/JSONArray;

    invoke-direct {v2, p0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 45
    .local v2, "jArr":Lorg/json/JSONArray;
    const/4 v6, 0x0

    invoke-virtual {v2, v6}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    .line 46
    .local v4, "jsonObj":Lorg/json/JSONObject;
    const-string v6, "aid"

    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseConvertID;->mOldID:Ljava/lang/String;

    .line 47
    const-string v6, "object_id"

    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseConvertID;->mObjectID:Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 56
    .end local v0    # "albumID":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseConvertID;
    .end local v2    # "jArr":Lorg/json/JSONArray;
    .end local v4    # "jsonObj":Lorg/json/JSONObject;
    :goto_0
    return-object v0

    .line 48
    .restart local v0    # "albumID":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseConvertID;
    :catch_0
    move-exception v3

    .line 49
    .local v3, "je":Lorg/json/JSONException;
    const-string v6, "SNS"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "parser : JSONException error :"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v3}, Lorg/json/JSONException;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v5

    .line 50
    goto :goto_0

    .line 51
    .end local v3    # "je":Lorg/json/JSONException;
    :catch_1
    move-exception v1

    .line 52
    .local v1, "e":Ljava/lang/Exception;
    const-string v6, "SNS"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "parser : Exception error : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v5

    .line 53
    goto :goto_0
.end method
