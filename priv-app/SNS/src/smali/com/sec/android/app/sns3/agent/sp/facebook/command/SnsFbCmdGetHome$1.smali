.class Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetHome$1;
.super Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqGetHome;
.source "SnsFbCmdGetHome.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetHome;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Handler;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetHome;


# direct methods
.method constructor <init>(Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetHome;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;)V
    .locals 0
    .param p2, "x0"    # Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    .param p3, "x1"    # Ljava/lang/String;

    .prologue
    .line 54
    iput-object p1, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetHome$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetHome;

    invoke-direct {p0, p2, p3}, Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqGetHome;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public onReqRespond(IZIILandroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePosts;)Z
    .locals 7
    .param p1, "reqID"    # I
    .param p2, "bSuccess"    # Z
    .param p3, "httpStatus"    # I
    .param p4, "errorCode"    # I
    .param p5, "reason"    # Landroid/os/Bundle;
    .param p6, "posts"    # Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePosts;

    .prologue
    const/4 v6, 0x0

    .line 60
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 61
    .local v2, "values":Landroid/content/ContentValues;
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/sns3/SnsApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 63
    .local v0, "cr":Landroid/content/ContentResolver;
    if-eqz p2, :cond_6

    .line 64
    if-eqz p6, :cond_5

    .line 66
    sget-object v3, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$Post;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v3, v6, v6}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 68
    iget-object v1, p6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePosts;->mPosts:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;

    .line 69
    .local v1, "curPost":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;
    :goto_0
    if-eqz v1, :cond_5

    .line 71
    invoke-virtual {v2}, Landroid/content/ContentValues;->clear()V

    .line 73
    const-string v3, "post_id"

    iget-object v4, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mPostID:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    const-string v3, "from_name"

    iget-object v4, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mFrom:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFrom;

    iget-object v4, v4, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFrom;->mFromName:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    const-string v3, "from_id"

    iget-object v4, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mFrom:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFrom;

    iget-object v4, v4, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFrom;->mFromID:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    const-string v3, "create_time"

    iget-object v4, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mCreatedTime:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    const-string v3, "updated_time"

    iget-object v4, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetHome$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetHome;

    # invokes: Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetHome;->getCurrentTime()Ljava/lang/Long;
    invoke-static {v4}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetHome;->access$000(Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetHome;)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 78
    const-string v3, "picture"

    iget-object v4, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mPicture:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    const-string v3, "_actor_photo_url"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "https://graph.facebook.com/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mFrom:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFrom;

    iget-object v5, v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFrom;->mFromID:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/picture?type=normal"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    iget-object v3, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mMessage:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_2

    .line 83
    const-string v3, "message"

    iget-object v4, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mMessage:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    :cond_0
    :goto_1
    const-string v3, "message"

    invoke-virtual {v2, v3}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 94
    sget-object v3, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$Post;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v3, v2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 97
    :cond_1
    iget-object v1, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;

    goto :goto_0

    .line 86
    :cond_2
    iget-object v3, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mDescription:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_3

    .line 87
    const-string v3, "message"

    iget-object v4, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mDescription:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 88
    :cond_3
    iget-object v3, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mCaption:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_4

    .line 89
    const-string v3, "message"

    iget-object v4, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mCaption:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 90
    :cond_4
    iget-object v3, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mName:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 91
    const-string v3, "message"

    iget-object v4, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mName:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 100
    .end local v1    # "curPost":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;
    :cond_5
    iget-object v3, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetHome$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetHome;

    sget-object v4, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$Post;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetHome;->setUri(Ljava/lang/String;)V

    .line 107
    :goto_2
    iget-object v3, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetHome$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetHome;

    invoke-virtual {v3, p2}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetHome;->setSuccess(Z)V

    .line 108
    iget-object v3, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetHome$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetHome;

    invoke-virtual {v3, p0}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetHome;->receive(Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;)V

    .line 110
    const/4 v3, 0x1

    return v3

    .line 102
    :cond_6
    iget-object v3, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetHome$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetHome;

    new-instance v4, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;

    const-string v5, "facebook"

    invoke-direct {v4, v5, p3, p4, p5}, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;-><init>(Ljava/lang/String;IILandroid/os/Bundle;)V

    invoke-virtual {v3, v4}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetHome;->setResponseList(Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;)Z

    .line 104
    iget-object v3, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetHome$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetHome;

    invoke-virtual {v3, v6}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetHome;->setUri(Ljava/lang/String;)V

    goto :goto_2
.end method
