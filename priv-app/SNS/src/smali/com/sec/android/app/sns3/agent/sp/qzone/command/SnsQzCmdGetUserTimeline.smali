.class public Lcom/sec/android/app/sns3/agent/sp/qzone/command/SnsQzCmdGetUserTimeline;
.super Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;
.source "SnsQzCmdGetUserTimeline.java"


# instance fields
.field private mFeedCount:I

.field private mUserID:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Handler;Ljava/lang/String;I)V
    .locals 5
    .param p1, "svcMgr"    # Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    .param p2, "cmdHandler"    # Landroid/os/Handler;
    .param p3, "userID"    # Ljava/lang/String;
    .param p4, "feedCount"    # I

    .prologue
    .line 52
    invoke-direct {p0, p2}, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;-><init>(Landroid/os/Handler;)V

    .line 54
    new-instance v2, Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;

    invoke-direct {v2}, Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;-><init>()V

    .line 56
    .local v2, "unit":Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;
    if-nez p3, :cond_0

    .line 57
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/sns3/SnsApplication;->getSvcMgr()Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/sns3/svc/SnsSvcMgr;->getTokenMgr()Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;

    move-result-object v3

    const-string v4, "qzone"

    invoke-virtual {v3, v4}, Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;->getToken(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzToken;

    .line 59
    .local v0, "QzToken":Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzToken;
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzToken;->getUserID()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->encodeUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/sns3/agent/sp/qzone/command/SnsQzCmdGetUserTimeline;->mUserID:Ljava/lang/String;

    .line 64
    .end local v0    # "QzToken":Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzToken;
    :goto_0
    iput p4, p0, Lcom/sec/android/app/sns3/agent/sp/qzone/command/SnsQzCmdGetUserTimeline;->mFeedCount:I

    .line 66
    new-instance v1, Lcom/sec/android/app/sns3/agent/sp/qzone/command/SnsQzCmdGetUserTimeline$1;

    iget-object v3, p0, Lcom/sec/android/app/sns3/agent/sp/qzone/command/SnsQzCmdGetUserTimeline;->mUserID:Ljava/lang/String;

    iget v4, p0, Lcom/sec/android/app/sns3/agent/sp/qzone/command/SnsQzCmdGetUserTimeline;->mFeedCount:I

    invoke-direct {v1, p0, p1, v3, v4}, Lcom/sec/android/app/sns3/agent/sp/qzone/command/SnsQzCmdGetUserTimeline$1;-><init>(Lcom/sec/android/app/sns3/agent/sp/qzone/command/SnsQzCmdGetUserTimeline;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;I)V

    .line 121
    .local v1, "req":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    invoke-virtual {v2, v1}, Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;->addRequest(Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;)V

    .line 123
    invoke-virtual {p0, v2}, Lcom/sec/android/app/sns3/agent/sp/qzone/command/SnsQzCmdGetUserTimeline;->addCommandUnit(Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;)V

    .line 124
    return-void

    .line 61
    .end local v1    # "req":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    :cond_0
    iput-object p3, p0, Lcom/sec/android/app/sns3/agent/sp/qzone/command/SnsQzCmdGetUserTimeline;->mUserID:Ljava/lang/String;

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/sec/android/app/sns3/agent/sp/qzone/command/SnsQzCmdGetUserTimeline;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sns3/agent/sp/qzone/command/SnsQzCmdGetUserTimeline;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/app/sns3/agent/sp/qzone/command/SnsQzCmdGetUserTimeline;->mUserID:Ljava/lang/String;

    return-object v0
.end method

.method private getCurrentTime()Ljava/lang/Long;
    .locals 4

    .prologue
    .line 132
    new-instance v0, Ljava/util/GregorianCalendar;

    invoke-direct {v0}, Ljava/util/GregorianCalendar;-><init>()V

    .line 133
    .local v0, "calendar":Ljava/util/GregorianCalendar;
    invoke-virtual {v0}, Ljava/util/GregorianCalendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    return-object v1
.end method


# virtual methods
.method public getUserID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/sec/android/app/sns3/agent/sp/qzone/command/SnsQzCmdGetUserTimeline;->mUserID:Ljava/lang/String;

    return-object v0
.end method

.method protected respond()Z
    .locals 5

    .prologue
    .line 138
    const-string v0, "SnsAgent"

    const-string v1, "<SnsQzCmdGetUserTimeline> respond()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 140
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/qzone/command/SnsQzCmdGetUserTimeline;->getCommandCallback()Lcom/sec/android/app/sns3/agent/command/ISnsCommandCallback;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/qzone/command/SnsQzCmdGetUserTimeline;->getCommandID()I

    move-result v1

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/qzone/command/SnsQzCmdGetUserTimeline;->isSuccess()Z

    move-result v2

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/qzone/command/SnsQzCmdGetUserTimeline;->getUri()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/qzone/command/SnsQzCmdGetUserTimeline;->getResponseList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/sec/android/app/sns3/agent/command/ISnsCommandCallback;->onCmdRespond(IZLjava/lang/String;Ljava/util/List;)V

    .line 142
    const/4 v0, 0x1

    return v0
.end method
