.class public final Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$PreviousSyncState;
.super Ljava/lang/Object;
.source "SnsFacebookDB.java"

# interfaces
.implements Landroid/provider/BaseColumns;
.implements Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$PreviousSyncStateColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PreviousSyncState"
.end annotation


# static fields
.field public static final CONTENT_ITEM_TYPE:Ljava/lang/String; = "vnd.android.cursor.item/vnd.com.sec.android.app.sns3.sp.facebook.previoussyncstate"

.field public static final CONTENT_TYPE:Ljava/lang/String; = "vnd.android.cursor.dir/vnd.com.sec.android.app.sns3.sp.facebook.previoussyncstate"

.field public static final CONTENT_URI:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 909
    const-string v0, "content://com.sec.android.app.sns3.sp.facebook/previous_sync_state"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$PreviousSyncState;->CONTENT_URI:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 907
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
