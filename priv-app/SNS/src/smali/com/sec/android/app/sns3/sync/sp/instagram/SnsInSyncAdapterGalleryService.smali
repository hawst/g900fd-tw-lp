.class public Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService;
.super Landroid/app/Service;
.source "SnsInSyncAdapterGalleryService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService$SyncAdapterImpl;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "SnsInSync"


# instance fields
.field private ALBUM_ID_VALUE:I

.field private ALBUM_NAME_VALUE:Ljava/lang/String;

.field private ALBUM_PRIVACY_VALUE:Ljava/lang/String;

.field private mAccount:Landroid/accounts/Account;

.field private mAuthority:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field private mReq:Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;

.field private mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

.field private mSyncAdapter:Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService$SyncAdapterImpl;

.field private mSyncResult:Landroid/content/SyncResult;

.field private mSyncState:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 55
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 59
    iput-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService;->mSyncAdapter:Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService$SyncAdapterImpl;

    .line 61
    iput v1, p0, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService;->mSyncState:I

    .line 63
    iput-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService;->mSyncResult:Landroid/content/SyncResult;

    .line 65
    iput-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService;->mReq:Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;

    .line 67
    iput-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService;->mAccount:Landroid/accounts/Account;

    .line 69
    iput-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService;->mAuthority:Ljava/lang/String;

    .line 75
    const-string v0, "everyone"

    iput-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService;->ALBUM_PRIVACY_VALUE:Ljava/lang/String;

    .line 77
    iput v1, p0, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService;->ALBUM_ID_VALUE:I

    .line 79
    const-string v0, "Instagram"

    iput-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService;->ALBUM_NAME_VALUE:Ljava/lang/String;

    .line 81
    return-void
.end method

.method static synthetic access$002(Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService;Landroid/content/Context;)Landroid/content/Context;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService;
    .param p1, "x1"    # Landroid/content/Context;

    .prologue
    .line 55
    iput-object p1, p0, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService;->mContext:Landroid/content/Context;

    return-object p1
.end method

.method static synthetic access$102(Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService;Landroid/accounts/Account;)Landroid/accounts/Account;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService;
    .param p1, "x1"    # Landroid/accounts/Account;

    .prologue
    .line 55
    iput-object p1, p0, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService;->mAccount:Landroid/accounts/Account;

    return-object p1
.end method

.method static synthetic access$202(Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 55
    iput-object p1, p0, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService;->mAuthority:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$302(Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService;Landroid/content/SyncResult;)Landroid/content/SyncResult;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService;
    .param p1, "x1"    # Landroid/content/SyncResult;

    .prologue
    .line 55
    iput-object p1, p0, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService;->mSyncResult:Landroid/content/SyncResult;

    return-object p1
.end method

.method static synthetic access$400(Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/accounts/OperationCanceledException;
        }
    .end annotation

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService;->performSync()V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService;

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService;->onSyncCanceled()V

    return-void
.end method

.method static synthetic access$602(Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService;
    .param p1, "x1"    # I

    .prologue
    .line 55
    iput p1, p0, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService;->mSyncState:I

    return p1
.end method

.method static synthetic access$700(Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService;

    .prologue
    .line 55
    iget v0, p0, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService;->ALBUM_ID_VALUE:I

    return v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService;

    .prologue
    .line 55
    iget-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService;->ALBUM_PRIVACY_VALUE:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService;

    .prologue
    .line 55
    iget-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService;->ALBUM_NAME_VALUE:Ljava/lang/String;

    return-object v0
.end method

.method private handleSessionExpired()I
    .locals 11

    .prologue
    const/4 v9, 0x1

    const/4 v10, 0x0

    .line 273
    iget-object v7, p0, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService;->mAccount:Landroid/accounts/Account;

    const-string v8, "com.sec.android.gallery3d.instagram.contentprovider"

    invoke-static {v7, v8}, Landroid/content/ContentResolver;->cancelSync(Landroid/accounts/Account;Ljava/lang/String;)V

    .line 275
    iget-object v7, p0, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService;->mContext:Landroid/content/Context;

    const-string v8, "notification"

    invoke-virtual {v7, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/app/NotificationManager;

    .line 278
    .local v4, "notiMgr":Landroid/app/NotificationManager;
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 279
    .local v3, "notiIntent":Landroid/content/Intent;
    const-string v7, "com.sec.android.app.sns3.RETRY_LOGIN_INSTAGRAM"

    invoke-virtual {v3, v7}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 280
    const/high16 v7, 0x800000

    invoke-virtual {v3, v7}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 281
    const-string v7, "RetryLogin"

    invoke-virtual {v3, v7, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 282
    iget-object v7, p0, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService;->mContext:Landroid/content/Context;

    invoke-static {v7, v10, v3, v10}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 284
    .local v1, "launchIntent":Landroid/app/PendingIntent;
    const v6, 0x7f08003f

    .line 285
    .local v6, "titleID":I
    const v5, 0x7f080025

    .line 286
    .local v5, "spID":I
    const v0, 0x7f02001d

    .line 288
    .local v0, "iconID":I
    new-instance v2, Landroid/app/Notification$Builder;

    iget-object v7, p0, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService;->mContext:Landroid/content/Context;

    invoke-direct {v2, v7}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    .line 289
    .local v2, "notiBuilder":Landroid/app/Notification$Builder;
    iget-object v7, p0, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService;->mContext:Landroid/content/Context;

    new-array v8, v9, [Ljava/lang/Object;

    iget-object v9, p0, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService;->mContext:Landroid/content/Context;

    invoke-virtual {v9, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v10

    invoke-virtual {v7, v6, v8}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v7

    invoke-virtual {v7, v0}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService;->mContext:Landroid/content/Context;

    const v9, 0x7f08003e

    invoke-virtual {v8, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v7

    invoke-virtual {v7, v1}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v7

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Landroid/app/Notification$Builder;->setWhen(J)Landroid/app/Notification$Builder;

    .line 294
    const/16 v7, 0x189c

    invoke-virtual {v2}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v8

    invoke-virtual {v4, v7, v8}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 296
    const/4 v7, -0x1

    return v7
.end method

.method private invokeBroadcast()V
    .locals 7

    .prologue
    .line 326
    const/4 v2, 0x0

    .line 328
    .local v2, "intent":Landroid/content/Intent;
    sget-object v3, Lcom/sec/android/app/sns3/agent/sp/instagram/db/SnsInstagramDB$SyncDBPhoto;->CONTENT_URI:Landroid/net/Uri;

    .line 329
    .local v3, "uri":Landroid/net/Uri;
    const-string v0, "com.sec.android.app.sns3.action.SYNC_IN_GALLERY"

    .line 330
    .local v0, "action":Ljava/lang/String;
    iget v4, p0, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService;->mSyncState:I

    const/4 v5, 0x2

    if-ne v4, v5, :cond_1

    const/4 v1, 0x1

    .line 332
    .local v1, "bResult":Z
    :goto_0
    new-instance v2, Landroid/content/Intent;

    .end local v2    # "intent":Landroid/content/Intent;
    invoke-direct {v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 333
    .restart local v2    # "intent":Landroid/content/Intent;
    const-string v4, "SNS3_CONTENT_URI_PHOTO"

    invoke-virtual {v2, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 334
    const-string v4, "SNS_RESULT"

    invoke-virtual {v2, v4, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 336
    const-string v4, "com.sec.android.app.sns3.permission.RECEIVE_SNS_BROADCAST"

    invoke-virtual {p0, v2, v4}, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    .line 337
    invoke-static {}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->isLoggable()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 338
    const-string v4, "SnsInSync"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "SnsInSyncAdapterGalleryService - invokeBroadcast() : action = ["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "], uri = ["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "], result = ["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 341
    :cond_0
    return-void

    .line 330
    .end local v1    # "bResult":Z
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private onSyncCanceled()V
    .locals 2

    .prologue
    .line 313
    const-string v0, "SnsInSync"

    const-string v1, "***************** SnsInSyncAdapterGalleryService : onSyncCanceled !!! *****************"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 316
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService;->mSyncState:I

    .line 318
    iget-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService;->mReq:Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;

    if-eqz v0, :cond_0

    .line 319
    iget-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService;->mReq:Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->abort()Z

    .line 322
    :cond_0
    return-void
.end method

.method private performSync()V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/accounts/OperationCanceledException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v6, -0x1

    .line 162
    iput v4, p0, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService;->mSyncState:I

    .line 163
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/sns3/SnsApplication;->getSvcMgr()Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/sns3/svc/SnsSvcMgr;->getTokenMgr()Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;

    move-result-object v2

    const-string v3, "instagram"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;->getToken(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/sns3/svc/sp/instagram/SnsInToken;

    .line 166
    .local v1, "mInToken":Lcom/sec/android/app/sns3/svc/sp/instagram/SnsInToken;
    :try_start_0
    invoke-virtual {v1}, Lcom/sec/android/app/sns3/svc/sp/instagram/SnsInToken;->getTokenState()I

    move-result v2

    if-eq v2, v4, :cond_0

    invoke-virtual {v1}, Lcom/sec/android/app/sns3/svc/sp/instagram/SnsInToken;->getTokenState()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    .line 168
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService;->handleSessionExpired()I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService;->mSyncState:I

    .line 169
    new-instance v2, Ljava/lang/Exception;

    const-string v3, "Session expired or invalid!!!"

    invoke-direct {v2, v3}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 261
    :catch_0
    move-exception v0

    .line 262
    .local v0, "e":Ljava/lang/Exception;
    :try_start_1
    const-string v2, "SnsInSync"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SnsInSyncAdapterGalleryService : EXCEPTION !!! "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 263
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 265
    iget-object v2, p0, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService;->mSyncResult:Landroid/content/SyncResult;

    iget-object v2, v2, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v4, v2, Landroid/content/SyncStats;->numConflictDetectedExceptions:J

    const-wide/16 v6, 0x1

    add-long/2addr v4, v6

    iput-wide v4, v2, Landroid/content/SyncStats;->numConflictDetectedExceptions:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 267
    invoke-direct {p0}, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService;->invokeBroadcast()V

    .line 270
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void

    .line 171
    :cond_1
    :try_start_2
    new-instance v2, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService$1;

    iget-object v3, p0, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    const-string v4, "self"

    const/4 v5, 0x0

    invoke-direct {v2, p0, v3, v4, v5}, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService$1;-><init>(Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;Landroid/os/Bundle;)V

    iput-object v2, p0, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService;->mReq:Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;

    .line 253
    iget-object v2, p0, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService;->mReq:Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;

    invoke-virtual {v2}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->request()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 254
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService;->suspendSync()V

    .line 259
    :goto_1
    iget v2, p0, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService;->mSyncState:I

    if-ne v2, v6, :cond_3

    .line 260
    new-instance v2, Ljava/lang/Exception;

    const-string v3, "SnsInSyncAdapterGalleryService is failed!!!"

    invoke-direct {v2, v3}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 267
    :catchall_0
    move-exception v2

    invoke-direct {p0}, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService;->invokeBroadcast()V

    throw v2

    .line 256
    :cond_2
    const/4 v2, -0x1

    :try_start_3
    iput v2, p0, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService;->mSyncState:I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 267
    :cond_3
    invoke-direct {p0}, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService;->invokeBroadcast()V

    goto :goto_0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 133
    const-string v0, "SnsInSync"

    const-string v1, "***************** SnsInSyncAdapterGalleryService : onBind !!! *****************"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 136
    iget-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService;->mSyncAdapter:Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService$SyncAdapterImpl;

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService$SyncAdapterImpl;->getSyncAdapterBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 120
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 122
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/SnsApplication;->getSvcMgr()Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    .line 124
    iget-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService;->mSyncAdapter:Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService$SyncAdapterImpl;

    if-nez v0, :cond_0

    .line 125
    new-instance v0, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService$SyncAdapterImpl;

    invoke-direct {v0, p0, p0}, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService$SyncAdapterImpl;-><init>(Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService;->mSyncAdapter:Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService$SyncAdapterImpl;

    .line 128
    :cond_0
    return-void
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 142
    const/4 v0, 0x0

    .line 143
    .local v0, "bActive":Z
    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService;->mAccount:Landroid/accounts/Account;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService;->mAuthority:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 144
    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService;->mAccount:Landroid/accounts/Account;

    iget-object v2, p0, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService;->mAuthority:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/content/ContentResolver;->isSyncActive(Landroid/accounts/Account;Ljava/lang/String;)Z

    move-result v0

    .line 147
    :cond_0
    const-string v1, "SnsInSync"

    const-string v2, "***************** SnsInSyncAdapterGalleryService : onUnbind !!! *****************"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 150
    if-eqz v0, :cond_2

    iget v1, p0, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService;->mSyncState:I

    const/4 v2, 0x1

    if-eq v1, v2, :cond_1

    iget v1, p0, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterGalleryService;->mSyncState:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_2

    .line 152
    :cond_1
    const-string v1, "SnsInSync"

    const-string v2, "SnsInSyncAdapterGalleryService : onUnbind : SYNC ERROR : performSync was stopped by forced abort or pending problem!!!"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 158
    :goto_0
    invoke-super {p0, p1}, Landroid/app/Service;->onUnbind(Landroid/content/Intent;)Z

    move-result v1

    return v1

    .line 156
    :cond_2
    const-string v1, "SnsInSync"

    const-string v2, "SnsInSyncAdapterGalleryService : onUnbind : COMPLETE STATE!!!"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public declared-synchronized resumeSync()V
    .locals 1

    .prologue
    .line 308
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->notify()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 309
    monitor-exit p0

    return-void

    .line 308
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized suspendSync()V
    .locals 2

    .prologue
    .line 301
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 305
    :goto_0
    monitor-exit p0

    return-void

    .line 302
    :catch_0
    move-exception v0

    .line 303
    .local v0, "e":Ljava/lang/InterruptedException;
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 301
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method
