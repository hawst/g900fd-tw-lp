.class public Lcom/sec/android/app/sns3/app/ticker/sp/facebook/SnsFbTickerIntervalSet;
.super Ljava/lang/Object;
.source "SnsFbTickerIntervalSet.java"


# static fields
.field private static final CHANGED_TICKER_INFO_FOR_PENDINGINTENT:Ljava/lang/String; = "android.sns.CHANGED_TICKER_INFO_FOR_PENDINGINTENT"

.field public static final INTERVAL_SET_1_HOUR:I = 0x2

.field public static final INTERVAL_SET_2_HOUR:I = 0x3

.field public static final INTERVAL_SET_30_MINUTE:I = 0x1

.field public static final INTERVAL_SET_4_HOUR:I = 0x4

.field public static final INTERVAL_SET_NONE:I = 0x0

.field private static final PERMISSION_TICKER:Ljava/lang/String; = "read_stream"

.field private static final TAG:Ljava/lang/String; = "Sns_ticker_interval_set"

.field private static final TIME_1_HOUR:I = 0x36ee80

.field private static final TIME_2_HOURS:I = 0x6ddd00

.field private static final TIME_30_MINUTE:I = 0x1b7740

.field private static final TIME_4_HOURS:I = 0xdbba00

.field private static final TIME_NONE:I

.field private static mAutoRefreshKey:I

.field private static mCmdHandler:Landroid/os/Handler;

.field static mContext:Landroid/content/Context;

.field private static mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

.field private static mUserID:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000()I
    .locals 1

    .prologue
    .line 19
    sget v0, Lcom/sec/android/app/sns3/app/ticker/sp/facebook/SnsFbTickerIntervalSet;->mAutoRefreshKey:I

    return v0
.end method

.method public static setAutoRefresh(Landroid/content/Context;I)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "autoRefreshKey"    # I

    .prologue
    .line 47
    invoke-static {p0}, Lcom/sec/android/app/sns3/app/ticker/sp/facebook/SnsFbTickerIntervalSet;->stopAutoRefresh(Landroid/content/Context;)V

    .line 49
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/sns3/SnsApplication;->getSvcMgr()Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v1

    sput-object v1, Lcom/sec/android/app/sns3/app/ticker/sp/facebook/SnsFbTickerIntervalSet;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    .line 50
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/sns3/SnsApplication;->getAgentMgr()Lcom/sec/android/app/sns3/agent/SnsAgentMgr;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/sns3/agent/SnsAgentMgr;->getCommandMgr()Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;->getCommandMgrHandle()Landroid/os/Handler;

    move-result-object v1

    sput-object v1, Lcom/sec/android/app/sns3/app/ticker/sp/facebook/SnsFbTickerIntervalSet;->mCmdHandler:Landroid/os/Handler;

    .line 51
    const-string v1, "me"

    sput-object v1, Lcom/sec/android/app/sns3/app/ticker/sp/facebook/SnsFbTickerIntervalSet;->mUserID:Ljava/lang/String;

    .line 53
    sput-object p0, Lcom/sec/android/app/sns3/app/ticker/sp/facebook/SnsFbTickerIntervalSet;->mContext:Landroid/content/Context;

    .line 54
    sput p1, Lcom/sec/android/app/sns3/app/ticker/sp/facebook/SnsFbTickerIntervalSet;->mAutoRefreshKey:I

    .line 56
    sget-object v1, Lcom/sec/android/app/sns3/app/ticker/sp/facebook/SnsFbTickerIntervalSet;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    if-nez v1, :cond_0

    .line 57
    sget-object v1, Lcom/sec/android/app/sns3/app/ticker/sp/facebook/SnsFbTickerResource;->TAG:Ljava/lang/String;

    const-string v2, "[SNS] mSvcMgr is null !!"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 136
    :goto_0
    return-void

    .line 60
    :cond_0
    sget-object v1, Lcom/sec/android/app/sns3/app/ticker/sp/facebook/SnsFbTickerIntervalSet;->mCmdHandler:Landroid/os/Handler;

    if-nez v1, :cond_1

    .line 61
    sget-object v1, Lcom/sec/android/app/sns3/app/ticker/sp/facebook/SnsFbTickerResource;->TAG:Ljava/lang/String;

    const-string v2, "[SNS] mCmdHandler is null !!"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 65
    :cond_1
    new-instance v0, Lcom/sec/android/app/sns3/app/ticker/sp/facebook/SnsFbTickerIntervalSet$1;

    sget-object v1, Lcom/sec/android/app/sns3/app/ticker/sp/facebook/SnsFbTickerIntervalSet;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    sget-object v2, Lcom/sec/android/app/sns3/app/ticker/sp/facebook/SnsFbTickerIntervalSet;->mUserID:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/sns3/app/ticker/sp/facebook/SnsFbTickerIntervalSet$1;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;)V

    .line 135
    .local v0, "req":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->request()Z

    goto :goto_0
.end method

.method public static stopAutoRefresh(Landroid/content/Context;)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v5, 0x0

    .line 140
    const-string v3, "Sns_ticker_interval_set"

    const-string v4, "stopAutoRefresh start"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 142
    if-eqz p0, :cond_1

    .line 143
    new-instance v0, Landroid/content/Intent;

    const-string v3, "android.sns.CHANGED_TICKER_INFO_FOR_PENDINGINTENT"

    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 144
    .local v0, "intent":Landroid/content/Intent;
    invoke-static {p0, v5, v0, v5}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 145
    .local v1, "pendingIntent":Landroid/app/PendingIntent;
    const-string v3, "alarm"

    invoke-virtual {p0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/AlarmManager;

    .line 147
    .local v2, "tickerAlarmManager":Landroid/app/AlarmManager;
    if-eqz v2, :cond_0

    .line 148
    invoke-virtual {v2, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 153
    .end local v0    # "intent":Landroid/content/Intent;
    .end local v1    # "pendingIntent":Landroid/app/PendingIntent;
    .end local v2    # "tickerAlarmManager":Landroid/app/AlarmManager;
    :cond_0
    :goto_0
    return-void

    .line 151
    :cond_1
    const-string v3, "Sns_ticker_interval_set"

    const-string v4, "stopAutoRefresh context is null"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
