.class public interface abstract Lcom/sec/android/app/sns3/agent/sp/twitter/db/SnsTwitterDB$FriendsProfileInfoColumns;
.super Ljava/lang/Object;
.source "SnsTwitterDB.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/agent/sp/twitter/db/SnsTwitterDB;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "FriendsProfileInfoColumns"
.end annotation


# static fields
.field public static final CREATE_AT:Ljava/lang/String; = "create_at"

.field public static final FRIEND_LINK:Ljava/lang/String; = "link"

.field public static final FRIEND_MEDIA_URL:Ljava/lang/String; = "media_url"

.field public static final FRIEND_MESSAGE:Ljava/lang/String; = "message"

.field public static final FRIEND_PICTURE:Ljava/lang/String; = "picture"

.field public static final FRIEND_PROFILE_ID:Ljava/lang/String; = "profile_id"

.field public static final FRIEND_PROFILE_NAME:Ljava/lang/String; = "profile_name"

.field public static final FRIEND_SCREEN_NAME:Ljava/lang/String; = "screen_name"
