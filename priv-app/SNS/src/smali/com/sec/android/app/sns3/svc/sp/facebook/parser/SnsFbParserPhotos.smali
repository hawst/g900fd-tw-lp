.class public Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserPhotos;
.super Ljava/lang/Object;
.source "SnsFbParserPhotos.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserPhotos$FacebookPhotos;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    return-void
.end method

.method public static parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhotos;
    .locals 9
    .param p0, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 38
    new-instance v7, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhotos;

    invoke-direct {v7}, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhotos;-><init>()V

    .line 40
    .local v7, "photosObj":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhotos;
    const/4 v6, 0x0

    .line 41
    .local v6, "photos":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;
    const/4 v0, 0x0

    .line 44
    .local v0, "curPhotos":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;
    :try_start_0
    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 46
    .local v4, "jsonObject":Lorg/json/JSONObject;
    new-instance v3, Lorg/json/JSONArray;

    const-string v8, "data"

    invoke-virtual {v4, v8}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v3, v8}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 48
    .local v3, "ja":Lorg/json/JSONArray;
    if-eqz v3, :cond_1

    .line 50
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v8

    if-ge v2, v8, :cond_1

    .line 51
    invoke-virtual {v3, v2}, Lorg/json/JSONArray;->optString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserPhoto;->parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;

    move-result-object v5

    .line 54
    .local v5, "newPhoto":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;
    if-nez v6, :cond_0

    .line 55
    move-object v6, v5

    .line 56
    move-object v0, v6

    .line 50
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 58
    :cond_0
    iput-object v5, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;

    .line 59
    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;

    goto :goto_1

    .line 64
    .end local v2    # "i":I
    .end local v5    # "newPhoto":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;
    :cond_1
    iput-object v6, v7, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhotos;->mPhoto:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;

    .line 66
    const-string v8, "paging"

    invoke-virtual {v4, v8}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 68
    const-string v8, "paging"

    invoke-virtual {v4, v8}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserPaging;->parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePaging;

    move-result-object v8

    iput-object v8, v7, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhotos;->mPaging:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePaging;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 78
    .end local v3    # "ja":Lorg/json/JSONArray;
    .end local v4    # "jsonObject":Lorg/json/JSONObject;
    :cond_2
    :goto_2
    return-object v7

    .line 73
    :catch_0
    move-exception v1

    .line 75
    .local v1, "e":Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_2
.end method
