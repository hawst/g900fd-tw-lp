.class public Lcom/sec/android/app/sns3/svc/sp/twitter/parser/SnsTwParserAccountSettings;
.super Ljava/lang/Object;
.source "SnsTwParserAccountSettings.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseAccountSettings;
    .locals 4
    .param p0, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 30
    const/4 v2, 0x0

    .line 32
    .local v2, "response":Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseAccountSettings;
    invoke-static {p0}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->isJsonEmpty(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 33
    const/4 v3, 0x0

    .line 53
    :goto_0
    return-object v3

    .line 36
    :cond_0
    new-instance v2, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseAccountSettings;

    .end local v2    # "response":Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseAccountSettings;
    invoke-direct {v2}, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseAccountSettings;-><init>()V

    .line 38
    .restart local v2    # "response":Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseAccountSettings;
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 40
    .local v1, "jsonObj":Lorg/json/JSONObject;
    const-string v3, "language"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseAccountSettings;->mLanguage:Ljava/lang/String;

    .line 41
    const-string v3, "always_use_https"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v3

    iput-boolean v3, v2, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseAccountSettings;->mAlwaysUseHttps:Z

    .line 42
    const-string v3, "discoverable_by_email"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v3

    iput-boolean v3, v2, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseAccountSettings;->mDiscoverableByEmail:Z

    .line 43
    const-string v3, "geo_enabled"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v3

    iput-boolean v3, v2, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseAccountSettings;->mGeoEnabled:Z

    .line 45
    const-string v3, "sleep_time"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/parser/SnsTwParserSleepTime;->parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseSleepTime;

    move-result-object v3

    iput-object v3, v2, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseAccountSettings;->mSleepTime:Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseSleepTime;

    .line 46
    const-string v3, "trend_location"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/parser/SnsTwParserTrendLocation;->parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseTrendLocation;

    move-result-object v3

    iput-object v3, v2, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseAccountSettings;->mTrendLocation:Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseTrendLocation;

    .line 47
    const-string v3, "time_zone"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/parser/SnsTwParserTimeZone;->parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseTimeZone;

    move-result-object v3

    iput-object v3, v2, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseAccountSettings;->mTimeZone:Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseTimeZone;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .end local v1    # "jsonObj":Lorg/json/JSONObject;
    :goto_1
    move-object v3, v2

    .line 53
    goto :goto_0

    .line 49
    :catch_0
    move-exception v0

    .line 50
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_1
.end method
