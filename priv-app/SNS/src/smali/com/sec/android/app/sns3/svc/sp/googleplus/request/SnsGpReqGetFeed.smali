.class public abstract Lcom/sec/android/app/sns3/svc/sp/googleplus/request/SnsGpReqGetFeed;
.super Lcom/sec/android/app/sns3/svc/sp/googleplus/request/SnsGpReqBase;
.source "SnsGpReqGetFeed.java"

# interfaces
.implements Lcom/sec/android/app/sns3/svc/sp/googleplus/callback/ISnsGpReqCbFeed;


# static fields
.field public static final PARAM_MAX_RESULTS:Ljava/lang/String; = "maxResults"

.field public static final PARAM_ORDER_BY:Ljava/lang/String; = "orderBy"

.field public static final PARAM_VALUE_ORDER_BY_RECENT:Ljava/lang/String; = "recent"

.field private static final REQUEST_URL_PATTERN:Ljava/lang/String; = "https://www.googleapis.com/plus/v1/people/%1$s/activities/public?key=AIzaSyB0d_BAwH-HHwqdBAtgs8wKlFxd7OxhPzQ&access_token=%2$s%3$s"


# instance fields
.field private mAdditionalPrams:Ljava/lang/String;

.field private mUserId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 2
    .param p1, "svcMgr"    # Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    .param p2, "userID"    # Ljava/lang/String;
    .param p3, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 55
    const/16 v0, 0x16

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/sns3/svc/sp/googleplus/request/SnsGpReqBase;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;I)V

    .line 40
    const-string v0, "me"

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/request/SnsGpReqGetFeed;->mUserId:Ljava/lang/String;

    .line 42
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/request/SnsGpReqGetFeed;->mAdditionalPrams:Ljava/lang/String;

    .line 56
    if-eqz p2, :cond_0

    .line 57
    iput-object p2, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/request/SnsGpReqGetFeed;->mUserId:Ljava/lang/String;

    .line 59
    :cond_0
    if-eqz p3, :cond_1

    .line 60
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "&"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p3}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->bundle2QueryString(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/request/SnsGpReqGetFeed;->mAdditionalPrams:Ljava/lang/String;

    .line 62
    :cond_1
    return-void
.end method


# virtual methods
.method protected compose()Lcom/sec/android/app/sns3/svc/http/SnsHttpRequest;
    .locals 9

    .prologue
    .line 67
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/request/SnsGpReqGetFeed;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/SnsSvcMgr;->getTokenMgr()Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;

    move-result-object v0

    const-string v1, "googleplus"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;->getToken(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpToken;

    .line 69
    .local v6, "token":Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpToken;
    const-string v2, "GET"

    .line 70
    .local v2, "method":Ljava/lang/String;
    const/4 v3, 0x0

    .line 71
    .local v3, "uri":Ljava/lang/String;
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 72
    .local v4, "header":Landroid/os/Bundle;
    const/4 v5, 0x0

    .line 74
    .local v5, "body":Landroid/os/Bundle;
    const-string v0, "https://www.googleapis.com/plus/v1/people/%1$s/activities/public?key=AIzaSyB0d_BAwH-HHwqdBAtgs8wKlFxd7OxhPzQ&access_token=%2$s%3$s"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget-object v8, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/request/SnsGpReqGetFeed;->mUserId:Ljava/lang/String;

    aput-object v8, v1, v7

    const/4 v7, 0x1

    invoke-virtual {v6}, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpToken;->getAccessToken()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->encodeUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v1, v7

    const/4 v7, 0x2

    iget-object v8, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/request/SnsGpReqGetFeed;->mAdditionalPrams:Ljava/lang/String;

    aput-object v8, v1, v7

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 76
    const-string v0, "x-li-format"

    const-string v1, "json"

    invoke-virtual {v4, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    new-instance v0, Lcom/sec/android/app/sns3/svc/http/SnsHttpRequest;

    iget v1, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/request/SnsGpReqGetFeed;->mReqID:I

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/sns3/svc/http/SnsHttpRequest;-><init>(ILjava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;)V

    return-object v0
.end method

.method protected parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;
    .locals 3
    .param p1, "content"    # Ljava/lang/String;

    .prologue
    .line 82
    const-string v1, "SNS"

    const-string v2, "GooglePlus  SnsGpReqGetFeed response SUCCESS!!"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 83
    invoke-super {p0, p1}, Lcom/sec/android/app/sns3/svc/sp/googleplus/request/SnsGpReqBase;->parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;

    move-result-object v0

    .line 85
    .local v0, "response":Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;
    if-eqz v0, :cond_0

    .line 88
    .end local v0    # "response":Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;
    :goto_0
    return-object v0

    .restart local v0    # "response":Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;
    :cond_0
    invoke-static {p1}, Lcom/sec/android/app/sns3/svc/sp/googleplus/parser/SnsGpParserFeed;->parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;

    move-result-object v0

    goto :goto_0
.end method

.method protected respond(Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;)Z
    .locals 7
    .param p1, "result"    # Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;

    .prologue
    .line 93
    iget v1, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/request/SnsGpReqGetFeed;->mReqID:I

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->isSuccess()Z

    move-result v2

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->getHttpstatus()I

    move-result v3

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->getErrorCode()I

    move-result v4

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->getReason()Landroid/os/Bundle;

    move-result-object v5

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->getResponse()Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;

    move-object v0, p0

    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/app/sns3/svc/sp/googleplus/request/SnsGpReqGetFeed;->onReqRespond(IZIILandroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;)Z

    .line 96
    const/4 v0, 0x1

    return v0
.end method
