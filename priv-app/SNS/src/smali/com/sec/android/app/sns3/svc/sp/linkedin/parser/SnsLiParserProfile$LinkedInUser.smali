.class public interface abstract Lcom/sec/android/app/sns3/svc/sp/linkedin/parser/SnsLiParserProfile$LinkedInUser;
.super Ljava/lang/Object;
.source "SnsLiParserProfile.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/svc/sp/linkedin/parser/SnsLiParserProfile;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "LinkedInUser"
.end annotation


# static fields
.field public static final BIRTHDAY:Ljava/lang/String; = "dateOfBirth"

.field public static final CONNECTIONS:Ljava/lang/String; = "connections"

.field public static final DAY:Ljava/lang/String; = "day"

.field public static final EDUCATION:Ljava/lang/String; = "educations"

.field public static final EMAIL:Ljava/lang/String; = "emailAddress"

.field public static final FIRST_NAME:Ljava/lang/String; = "firstName"

.field public static final FOLLOWING:Ljava/lang/String; = "following"

.field public static final FORMATTED_NAME:Ljava/lang/String; = "formattedName"

.field public static final GROUPS:Ljava/lang/String; = "groupMemberships"

.field public static final HEADLINE:Ljava/lang/String; = "headline"

.field public static final INDUSTRY:Ljava/lang/String; = "industry"

.field public static final LAST_NAME:Ljava/lang/String; = "lastName"

.field public static final LOCATION:Ljava/lang/String; = "location"

.field public static final LOCATION_COUNTRY:Ljava/lang/String; = "country"

.field public static final LOCATION_COUNTRY_CODE:Ljava/lang/String; = "code"

.field public static final LOCATION_NAME:Ljava/lang/String; = "name"

.field public static final MONTH:Ljava/lang/String; = "month"

.field public static final NUMBER_OF_CONNECTIONS:Ljava/lang/String; = "numConnections"

.field public static final PICTURE_URL:Ljava/lang/String; = "pictureUrl"

.field public static final POSITIONS:Ljava/lang/String; = "positions"

.field public static final PROFILE_URL:Ljava/lang/String; = "publicProfileUrl"

.field public static final SKILLS:Ljava/lang/String; = "skills"

.field public static final USER_ID:Ljava/lang/String; = "id"

.field public static final YEAR:Ljava/lang/String; = "year"
