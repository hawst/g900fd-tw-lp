.class public final Lcom/sec/android/app/sns3/svc/sp/twitter/request/composer/SnsTwComposerParams;
.super Ljava/lang/Object;
.source "SnsTwComposerParams.java"


# static fields
.field public static final ACCURACY:Ljava/lang/String; = "accuracy"

.field public static final ATTRIBUTE:Ljava/lang/String; = "attribute:street_address"

.field public static final CALLBACK:Ljava/lang/String; = "callback"

.field public static final CONTAINED_WITHIN:Ljava/lang/String; = "contained_within"

.field public static final CONTRIBUTOR_DETAILS:Ljava/lang/String; = "contributor_details"

.field public static final COUNT:Ljava/lang/String; = "count"

.field public static final CURSOR:Ljava/lang/String; = "cursor"

.field public static final DATE:Ljava/lang/String; = "date"

.field public static final DESCRIPTION:Ljava/lang/String; = "description"

.field public static final DEVICE:Ljava/lang/String; = "device"

.field public static final DISPLAY_COORDINATES:Ljava/lang/String; = "display_coordinates"

.field public static final END_SLEEP_TIME:Ljava/lang/String; = "end_sleep_time"

.field public static final EXCLUDE:Ljava/lang/String; = "exclude"

.field public static final EXCLUDE_REPLIES:Ljava/lang/String; = "exclude_replies"

.field public static final FILENAME:Ljava/lang/String; = "filename"

.field public static final FILTER_TO_OWNED_LISTS:Ljava/lang/String; = "filter_to_owned_lists"

.field public static final FOLLOW:Ljava/lang/String; = "follow"

.field public static final GEOCODE:Ljava/lang/String; = "geocode"

.field public static final GRANULARITY:Ljava/lang/String; = "granularity"

.field public static final ID:Ljava/lang/String; = "id"

.field public static final IMAGE:Ljava/lang/String; = "image"

.field public static final INCLUDE_ENTITIES:Ljava/lang/String; = "include_entities"

.field public static final INCLUDE_RTS:Ljava/lang/String; = "include_rts"

.field public static final IN_REPLY_TO_STATUS_ID:Ljava/lang/String; = "in_reply_to_status_id"

.field public static final IP:Ljava/lang/String; = "ip"

.field public static final LANG:Ljava/lang/String; = "lang"

.field public static final LAT:Ljava/lang/String; = "lat"

.field public static final LIST_ID:Ljava/lang/String; = "list_id"

.field public static final LOCALE:Ljava/lang/String; = "locale"

.field public static final LOCATION:Ljava/lang/String; = "location"

.field public static final LONG:Ljava/lang/String; = "long"

.field public static final MAX_ID:Ljava/lang/String; = "max_id"

.field public static final MAX_RESULTS:Ljava/lang/String; = "max_results"

.field public static final MEDIA:Ljava/lang/String; = "media[]"

.field public static final MODE:Ljava/lang/String; = "mode"

.field public static final NAME:Ljava/lang/String; = "name"

.field public static final OAUTH_CALLBACK:Ljava/lang/String; = "oauth_callback"

.field public static final OAUTH_CONSUMER_KEY:Ljava/lang/String; = "oauth_consumer_key"

.field public static final OAUTH_NONCE:Ljava/lang/String; = "oauth_nonce"

.field public static final OAUTH_SIGNATURE:Ljava/lang/String; = "oauth_signature"

.field public static final OAUTH_SIGNATURE_METHOD:Ljava/lang/String; = "oauth_signature_method"

.field public static final OAUTH_TIMESTAMP:Ljava/lang/String; = "oauth_timestamp"

.field public static final OAUTH_TOKEN:Ljava/lang/String; = "oauth_token"

.field public static final OAUTH_VERIFIER:Ljava/lang/String; = "oauth_verifier"

.field public static final OAUTH_VERSION:Ljava/lang/String; = "oauth_version"

.field public static final OWNER_ID:Ljava/lang/String; = "owner_id"

.field public static final OWNER_SCREEN_NAME:Ljava/lang/String; = "owner_screen_name"

.field public static final PAGE:Ljava/lang/String; = "page"

.field public static final PER_PAGE:Ljava/lang/String; = "per_page"

.field public static final PLACE_ID:Ljava/lang/String; = "place_id"

.field public static final POSSIBLY_SENSITIVE:Ljava/lang/String; = "possibly_sensitive"

.field public static final PROFILE_BACKGROUND_COLOR:Ljava/lang/String; = "profile_background_color"

.field public static final PROFILE_LINK_COLOR:Ljava/lang/String; = "profile_link_color"

.field public static final PROFILE_SIDEBAR_BORDER_COLOR:Ljava/lang/String; = "profile_sidebar_border_color"

.field public static final PROFILE_SIDEBAR_FILL_COLOR:Ljava/lang/String; = "profile_sidebar_fill_color"

.field public static final PROFILE_TEXT_COLOR:Ljava/lang/String; = "profile_text_color"

.field public static final Q:Ljava/lang/String; = "q"

.field public static final QUERY:Ljava/lang/String; = "query"

.field public static final RESULT_TYPE:Ljava/lang/String; = "result_type"

.field public static final RETWEETS:Ljava/lang/String; = "retweets"

.field public static final RPP:Ljava/lang/String; = "rpp"

.field public static final SCREEN_NAME:Ljava/lang/String; = "screen_name"

.field public static final SCREEN_NAME_A:Ljava/lang/String; = "screen_name_a"

.field public static final SCREEN_NAME_B:Ljava/lang/String; = "screen_name_b"

.field public static final SHOW_USER:Ljava/lang/String; = "show_user"

.field public static final SINCE_ID:Ljava/lang/String; = "since_id"

.field public static final SIZE:Ljava/lang/String; = "size"

.field public static final SKIP_STATUS:Ljava/lang/String; = "skip_status"

.field public static final SLEEP_TIME_ENABLED:Ljava/lang/String; = "sleep_time_enabled"

.field public static final SLUG:Ljava/lang/String; = "slug"

.field public static final SOURCE_ID:Ljava/lang/String; = "source_id"

.field public static final SOURCE_SCREEN_NAME:Ljava/lang/String; = "source_screen_name"

.field public static final START_SLEEP_TIME:Ljava/lang/String; = "start_sleep_time"

.field public static final STATUS:Ljava/lang/String; = "status"

.field public static final STRINGIFY_IDS:Ljava/lang/String; = "stringify_ids"

.field public static final TARGET_ID:Ljava/lang/String; = "target_id"

.field public static final TARGET_SCREEN_NAME:Ljava/lang/String; = "target_screen_name"

.field public static final TEXT:Ljava/lang/String; = "text"

.field public static final TILE:Ljava/lang/String; = "tile"

.field public static final TIME_ZONE:Ljava/lang/String; = "time_zone"

.field public static final TOKEN:Ljava/lang/String; = "token"

.field public static final TREND_LOCATION_WOEID:Ljava/lang/String; = "trend_location_woeid"

.field public static final TRIM_USER:Ljava/lang/String; = "trim_user"

.field public static final UNTIL:Ljava/lang/String; = "until"

.field public static final URL:Ljava/lang/String; = "url"

.field public static final USE:Ljava/lang/String; = "use"

.field public static final USER_ID:Ljava/lang/String; = "user_id"

.field public static final USER_ID_A:Ljava/lang/String; = "user_id_a"

.field public static final USER_ID_B:Ljava/lang/String; = "user_id_b"

.field public static final WOEID:Ljava/lang/String; = "woeid"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
