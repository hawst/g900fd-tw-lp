.class public interface abstract Lcom/sec/android/app/sns3/svc/sp/instagram/parser/SnsInParserProfile$InstagramUser;
.super Ljava/lang/Object;
.source "SnsInParserProfile.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/svc/sp/instagram/parser/SnsInParserProfile;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "InstagramUser"
.end annotation


# static fields
.field public static final DATA:Ljava/lang/String; = "data"

.field public static final PROFILE_BIO:Ljava/lang/String; = "bio"

.field public static final PROFILE_FULL_NAME:Ljava/lang/String; = "full_name"

.field public static final PROFILE_ID:Ljava/lang/String; = "id"

.field public static final PROFILE_IMAGE_URL:Ljava/lang/String; = "profile_picture"

.field public static final PROFILE_NAME:Ljava/lang/String; = "username"

.field public static final PROFILE_WEBSITE:Ljava/lang/String; = "website"
