.class public Lcom/sec/android/app/sns3/svc/util/SnsErrorCode;
.super Ljava/lang/Object;
.source "SnsErrorCode.java"


# static fields
.field public static final EC_FB_SESSION_EXPIRED:I = 0x2711

.field public static final EC_FS_RATE_LIMIT_REACHED:I = 0x193

.field public static final EC_HTTP_ABORTED:I = 0xbb9

.field public static final EC_HTTP_ERROR:I = 0xbb8

.field public static final EC_HTTP_ILLEGAL_STATE_EXCEPTION:I = 0xbba

.field public static final EC_HTTP_INTERRUPTED_IO:I = 0xbbc

.field public static final EC_HTTP_IO_EXCEPTION:I = 0xbbb

.field public static final EC_HTTP_NETWORK_UNREACHABLE:I = 0xbbd

.field public static final EC_HTTP_UNKNOWN_HOST:I = 0xbbe

.field public static final EC_IN_RATE_LIMIT_REACHED:I = 0x1f7

.field public static final EC_LI_THROTTLE_LIMIT_REACHED:I = 0x193

.field public static final EC_OK:I = -0x1

.field public static final EC_QZ_SESSION_EXPIRED:I = 0x32c9

.field public static final EC_REQ_ACCOUNT_ERROR:I = 0x7d7

.field public static final EC_REQ_CANCEL:I = 0x7d5

.field public static final EC_REQ_ERROR:I = 0x7d0

.field public static final EC_REQ_FULL_ERROR:I = 0x7d3

.field public static final EC_REQ_OVER_RETRY:I = 0x7d6

.field public static final EC_REQ_RESPONSE_ERROR:I = 0x7d8

.field public static final EC_REQ_SESSION_ERROR:I = 0x7d1

.field public static final EC_REQ_SESSION_ERROR_RECOVERABLE:I = 0xfa1

.field public static final EC_REQ_SESSION_INVALID:I = 0x7d9

.field public static final EC_REQ_TEMPORARY_ERROR:I = 0x7d2

.field public static final EC_REQ_TIMEOUT:I = 0x7d4

.field public static final EC_RESP_ERROR:I = 0x3e8

.field public static final EC_SESSION_EXPIRED:I = 0x191

.field public static final EC_SW_SESSION_EXPIRED:I = 0x2ee1

.field public static final EC_TW_RATE_LIMIT_REACHED:I = 0x1ad


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
