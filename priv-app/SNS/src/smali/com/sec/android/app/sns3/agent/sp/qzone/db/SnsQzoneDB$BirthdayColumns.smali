.class public interface abstract Lcom/sec/android/app/sns3/agent/sp/qzone/db/SnsQzoneDB$BirthdayColumns;
.super Ljava/lang/Object;
.source "SnsQzoneDB.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/agent/sp/qzone/db/SnsQzoneDB;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "BirthdayColumns"
.end annotation


# static fields
.field public static final BIRTHDAY_DAY:Ljava/lang/String; = "day"

.field public static final BIRTHDAY_MONTH:Ljava/lang/String; = "month"

.field public static final FRIEND_OPENID:Ljava/lang/String; = "friend_openid"

.field public static final GENDER:Ljava/lang/String; = "gender"

.field public static final HOST_OPENID:Ljava/lang/String; = "host_openid"

.field public static final LUNAR_FLAG:Ljava/lang/String; = "lunar_flag"

.field public static final NAME:Ljava/lang/String; = "name"

.field public static final PIC_URL:Ljava/lang/String; = "pic_url"
