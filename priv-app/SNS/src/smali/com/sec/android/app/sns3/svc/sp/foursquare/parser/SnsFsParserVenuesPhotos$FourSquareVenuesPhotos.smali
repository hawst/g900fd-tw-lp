.class public interface abstract Lcom/sec/android/app/sns3/svc/sp/foursquare/parser/SnsFsParserVenuesPhotos$FourSquareVenuesPhotos;
.super Ljava/lang/Object;
.source "SnsFsParserVenuesPhotos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/svc/sp/foursquare/parser/SnsFsParserVenuesPhotos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "FourSquareVenuesPhotos"
.end annotation


# static fields
.field public static final CODE:Ljava/lang/String; = "code"

.field public static final COUNT:Ljava/lang/String; = "count"

.field public static final CREATED_AT:Ljava/lang/String; = "createdAt"

.field public static final ID:Ljava/lang/String; = "id"

.field public static final ITEMS:Ljava/lang/String; = "items"

.field public static final META:Ljava/lang/String; = "meta"

.field public static final PHOTOS:Ljava/lang/String; = "photos"

.field public static final PHOTO_HEIGHT:Ljava/lang/String; = "height"

.field public static final PHOTO_WIDTH:Ljava/lang/String; = "width"

.field public static final PREFIX:Ljava/lang/String; = "prefix"

.field public static final RESPONSE:Ljava/lang/String; = "response"

.field public static final SOURCE:Ljava/lang/String; = "source"

.field public static final SOURCE_NAME:Ljava/lang/String; = "name"

.field public static final SOURCE_URL:Ljava/lang/String; = "url"

.field public static final SUFFIX:Ljava/lang/String; = "suffix"

.field public static final USER:Ljava/lang/String; = "user"

.field public static final USER_FIRST_NAME:Ljava/lang/String; = "firstName"

.field public static final USER_GENDER:Ljava/lang/String; = "gender"

.field public static final USER_LAST_NAME:Ljava/lang/String; = "lastName"

.field public static final USER_PHOTO:Ljava/lang/String; = "photo"

.field public static final VISIBILITY:Ljava/lang/String; = "visibility"
