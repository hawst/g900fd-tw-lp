.class public Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserLikes;
.super Ljava/lang/Object;
.source "SnsFbParserLikes.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserLikes$FacebookLikes;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    return-void
.end method

.method public static parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLikes;
    .locals 10
    .param p0, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 42
    new-instance v7, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLikes;

    invoke-direct {v7}, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLikes;-><init>()V

    .line 44
    .local v7, "likesObj":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLikes;
    const/4 v6, 0x0

    .line 45
    .local v6, "likes":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLike;
    const/4 v0, 0x0

    .line 48
    .local v0, "curLikes":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLike;
    :try_start_0
    new-instance v5, Lorg/json/JSONObject;

    invoke-direct {v5, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 50
    .local v5, "jsonObject":Lorg/json/JSONObject;
    new-instance v3, Lorg/json/JSONArray;

    const-string v9, "data"

    invoke-virtual {v5, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v3, v9}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 52
    .local v3, "ja":Lorg/json/JSONArray;
    if-eqz v3, :cond_1

    .line 54
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v9

    if-ge v2, v9, :cond_1

    .line 55
    invoke-virtual {v3, v2}, Lorg/json/JSONArray;->optString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserLike;->parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLike;

    move-result-object v8

    .line 57
    .local v8, "newLike":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLike;
    if-nez v6, :cond_0

    .line 58
    move-object v6, v8

    .line 59
    move-object v0, v6

    .line 54
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 61
    :cond_0
    iput-object v8, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLike;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLike;

    .line 62
    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLike;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLike;

    goto :goto_1

    .line 67
    .end local v2    # "i":I
    .end local v8    # "newLike":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLike;
    :cond_1
    iput-object v6, v7, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLikes;->mLike:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLike;

    .line 69
    const-string v9, "paging"

    invoke-virtual {v5, v9}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 71
    const-string v9, "paging"

    invoke-virtual {v5, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserPaging;->parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePaging;

    move-result-object v9

    iput-object v9, v7, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLikes;->mPaging:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePaging;

    .line 75
    :cond_2
    const-string v9, "summary"

    invoke-virtual {v5, v9}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v9

    if-eqz v9, :cond_3

    .line 78
    :try_start_1
    new-instance v4, Lorg/json/JSONObject;

    const-string v9, "summary"

    invoke-virtual {v5, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v4, v9}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 79
    .local v4, "js":Lorg/json/JSONObject;
    const-string v9, "total_count"

    invoke-virtual {v4, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v7, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLikes;->mLikeCount:Ljava/lang/String;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    .line 90
    .end local v3    # "ja":Lorg/json/JSONArray;
    .end local v4    # "js":Lorg/json/JSONObject;
    .end local v5    # "jsonObject":Lorg/json/JSONObject;
    :cond_3
    :goto_2
    return-object v7

    .line 80
    .restart local v3    # "ja":Lorg/json/JSONArray;
    .restart local v5    # "jsonObject":Lorg/json/JSONObject;
    :catch_0
    move-exception v1

    .line 81
    .local v1, "e":Lorg/json/JSONException;
    :try_start_2
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_2

    .line 85
    .end local v1    # "e":Lorg/json/JSONException;
    .end local v3    # "ja":Lorg/json/JSONArray;
    .end local v5    # "jsonObject":Lorg/json/JSONObject;
    :catch_1
    move-exception v1

    .line 87
    .restart local v1    # "e":Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_2
.end method
