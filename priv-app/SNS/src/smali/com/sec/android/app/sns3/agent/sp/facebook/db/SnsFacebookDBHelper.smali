.class public Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDBHelper;
.super Lcom/sec/android/app/sns3/agent/db/SnsDBHelperBase;
.source "SnsFacebookDBHelper.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 31
    const-string v0, "snsFacebookDB.db"

    const/4 v1, 0x0

    const/16 v2, 0x19

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/sec/android/app/sns3/agent/db/SnsDBHelperBase;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 32
    return-void
.end method


# virtual methods
.method public createTables(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 50
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 53
    const-string v0, "CREATE TABLE IF NOT EXISTS user (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,user_id VARCHAR(50),username TEXT, UNIQUE (user_id));"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 59
    const-string v0, "SnsAgent"

    const-string v1, "DB TABLE(user) Has been created."

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 62
    const-string v0, "CREATE TABLE IF NOT EXISTS feed_list (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,feed_id VARCHAR(50),type TEXT,status_type TEXT,icon TEXT,caption TEXT,message TEXT,description TEXT,picture TEXT,story TEXT,link TEXT,name TEXT,source TEXT,created_time TIMESTAMP,updated_time TIMESTAMP,location_name TEXT,location_latitude DOUBLE,location_longitude DOUBLE,from_id VARCHAR(50),photo_id TEXT,from_name TEXT, UNIQUE (feed_id));"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 86
    const-string v0, "SnsAgent"

    const-string v1, "DB TABLE(feed_list) Has been created."

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 90
    const-string v0, "CREATE TABLE IF NOT EXISTS album_contents (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,feed_id VARCHAR(50),photo_id VARCHAR(50),picture TEXT,created_time TIMESTAMP,updated_time TIMESTAMP,width INTEGER,height INTEGER, UNIQUE (photo_id));"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 101
    const-string v0, "SnsAgent"

    const-string v1, "DB TABLE(album_contents) Has been created."

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 105
    const-string v0, "CREATE TABLE IF NOT EXISTS post (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,post_id VARCHAR(50),from_id VARCHAR(50),from_name VARCHAR(100),to_id VARCHAR(50),to_name VARCHAR(100),message TEXT,picture TEXT,link TEXT,name TEXT,caption TEXT,description TEXT,source TEXT,properties TEXT,icon TEXT,actions TEXT,privacy TEXT,type TEXT,likes_name TEXT,likes_count INTEGER,comments_count INTEGER,object_id TEXT,application_name TEXT,application_id VARCHAR(50),create_time TIMESTAMP,updated_time TIMESTAMP,targeting TEXT,place_id VARCHAR(50),place_name TEXT,street TEXT,city TEXT,state TEXT,country TEXT,like_done TEXT,shares INTEGER,zip VARCHAR(50),latitude VARCHAR(50),longitude VARCHAR(50),_actor_photo_url VARCHAR(1024), UNIQUE (post_id));"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 145
    const-string v0, "SnsAgent"

    const-string v1, "DB TABLE(post) Has been created."

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 148
    const-string v0, "CREATE TABLE IF NOT EXISTS comments (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,comments_id VARCHAR(50),target_id VARCHAR(50),target_type VARCHAR(100),from_id TEXT,from_name TEXT,message TEXT,created_time TIMESTAMP,can_remove BOOLEAN,likes TEXT,user_likes TEXT,_actor_photo_url VARCHAR(1024), UNIQUE (comments_id));"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 163
    const-string v0, "SnsAgent"

    const-string v1, "DB TABLE(comments) Has been created."

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 167
    const-string v0, "CREATE TABLE IF NOT EXISTS likes (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,likes_id VARCHAR(50),target_id VARCHAR(50),name TEXT,category TEXT,create_time TIMESTAMP,profile_id TEXT,photo_url VARCHAR(1024), UNIQUE (target_id,likes_id));"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 178
    const-string v0, "SnsAgent"

    const-string v1, "DB TABLE(likes) Has been created."

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 181
    const-string v0, "CREATE TABLE IF NOT EXISTS birthday (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,uid VARCHAR(50),first_name VARCHAR(100),last_name VARCHAR(100),name VARCHAR(100),birthday_date TIMESTAMP,pic_url VARCHAR(512), UNIQUE (uid));"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 191
    const-string v0, "SnsAgent"

    const-string v1, "DB TABLE(birthday) Has been created."

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 195
    const-string v0, "CREATE TABLE IF NOT EXISTS event (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,event_id VARCHAR(50),owner_id VARCHAR(50),owner_name TEXT,name TEXT,description TEXT,start_time TIMESTAMP,end_time TIMESTAMP,location TEXT,venue_id VARCHAR(50),street TEXT,city TEXT,state TEXT,zip TEXT,country TEXT,latitude TEXT,longitude TEXT,privacy TEXT,updated_time TIMESTAMP,_actor_photo_url VARCHAR(1024),type TEXT, UNIQUE (event_id));"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 219
    const-string v0, "SnsAgent"

    const-string v1, "DB TABLE(event) Has been created."

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 222
    const-string v0, "CREATE TABLE IF NOT EXISTS album (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,id VARCHAR(50),from_name VARCHAR(100),from_id VARCHAR(50),name TEXT,description TEXT,location TEXT,link TEXT,cover_photo VARCHAR(50),privacy TEXT,count INTEGER,created_time TIMESTAMP,updated_time TIMESTAMP,type TEXT,like_done TEXT, UNIQUE (id));"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 240
    const-string v0, "SnsAgent"

    const-string v1, "DB TABLE(album) Has been created."

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 243
    const-string v0, "CREATE TABLE IF NOT EXISTS photo (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,id VARCHAR(50),from_id VARCHAR(50),from_name VARCHAR(100),target_id VARCHAR(50),name TEXT,picture TEXT,source TEXT,height INTEGER,width INTEGER,link TEXT,icon TEXT,position INTEGER,created_time TIMESTAMP,updated_time TIMESTAMP,like_done TEXT,latitude TEXT,longitude TEXT,location_name TEXT, UNIQUE (id));"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 265
    const-string v0, "SnsAgent"

    const-string v1, "DB TABLE(photo) Has been created."

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 268
    const-string v0, "CREATE TABLE IF NOT EXISTS tags (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,target_id VARCHAR(50),target_type VARCHAR(100),profile_id VARCHAR(50),tagged_name VARCHAR(100),x TEXT,y TEXT,created_time TIMESTAMP, UNIQUE (_id));"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 279
    const-string v0, "SnsAgent"

    const-string v1, "DB TABLE(tags) Has been created."

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 282
    const-string v0, "CREATE TABLE IF NOT EXISTS photo_stream (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,post_id VARCHAR(50),actor_id VARCHAR(50),user_name TEXT,profile_url TEXT,message TEXT,updated_time TIMESTAMP,photo_url TEXT,created_time TIMESTAMP,likes_count INTEGER,comments_count INTEGER, UNIQUE (post_id));"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 296
    const-string v0, "SnsAgent"

    const-string v1, "DB TABLE(photo_stream) Has been created."

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 300
    const-string v0, "CREATE TABLE IF NOT EXISTS groups (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,id VARCHAR(50),versioin VARCHAR(50),icon TEXT,owner_id VARCHAR(50),owner_name TEXT,name TEXT,description TEXT,administrator TEXT,link TEXT,privacy TEXT,updated_time TIMESTAMP,bookmark_order TEXT, UNIQUE (id));"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 316
    const-string v0, "SnsAgent"

    const-string v1, "DB TABLE(groups) Has been created."

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 320
    const-string v0, "CREATE TABLE IF NOT EXISTS note (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,note_id VARCHAR(50),from_id VARCHAR(50),from_name VARCHAR(100),subject TEXT,message TEXT,create_time TIMESTAMP,updated_time TIMESTAMP,comments_count INTEGER,icon TEXT,_actor_photo_url VARCHAR(1024),like_done TEXT, UNIQUE (note_id));"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 334
    const-string v0, "SnsAgent"

    const-string v1, "DB TABLE(note) Has been created."

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 337
    const-string v0, "CREATE TABLE IF NOT EXISTS sync_event (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,event_id VARCHAR(50),owner_id VARCHAR(50),owner_name TEXT,name TEXT,description TEXT,start_time TIMESTAMP,end_time TIMESTAMP,location TEXT,privacy TEXT,updated_time TIMESTAMP,rsvp_status TEXT,street TEXT,city TEXT,state TEXT,zip TEXT,country TEXT,latitude TEXT,longitude TEXT,vanue_id TEXT,event_pic VARCHAR(1024),paging_previous VARCHAR(1024),paging_next VARCHAR(1024),_is_valid BOOLEAN, UNIQUE (event_id,_is_valid));"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 364
    const-string v0, "SnsAgent"

    const-string v1, "DB TABLE(sync_event) Has been created."

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 368
    const-string v0, "CREATE TABLE IF NOT EXISTS sync_album (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,id VARCHAR(50),from_name VARCHAR(100),from_id VARCHAR(50),name TEXT,description TEXT,location TEXT,link TEXT,cover_photo VARCHAR(50),privacy TEXT,count INTEGER,created_time TIMESTAMP,updated_time TIMESTAMP,type TEXT,paging_previous VARCHAR(1024),paging_next VARCHAR(1024),_is_valid BOOLEAN, UNIQUE (id,_is_valid));"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 388
    const-string v0, "SnsAgent"

    const-string v1, "DB TABLE(sync_album) Has been created."

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 392
    const-string v0, "CREATE TABLE IF NOT EXISTS sync_photo (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,id VARCHAR(50),target_id VARCHAR(50),from_id VARCHAR(50),from_name VARCHAR(100),name TEXT,picture TEXT,source TEXT,height INTEGER,width INTEGER,link TEXT,icon TEXT,position INTEGER,created_time TIMESTAMP,updated_time TIMESTAMP,paging_previous VARCHAR(1024),latitude TEXT,longitude TEXT,location_name TEXT,likes_count INTEGER,comments_count INTEGER,paging_next VARCHAR(1024),_is_valid BOOLEAN, UNIQUE (id,_is_valid));"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 418
    const-string v0, "SnsAgent"

    const-string v1, "DB TABLE(sync_photo) Has been created."

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 422
    const-string v0, "CREATE TABLE IF NOT EXISTS sync_tags (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,target_id VARCHAR(50),target_type VARCHAR(100),profile_id VARCHAR(50),tagged_name VARCHAR(100),x TEXT,y TEXT,created_time TIMESTAMP, UNIQUE (_id));"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 433
    const-string v0, "SnsAgent"

    const-string v1, "DB TABLE(sync_tags) Has been created."

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 437
    const-string v0, "CREATE TABLE IF NOT EXISTS photos_of_user (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,id VARCHAR(50),target_id VARCHAR(50),from_id VARCHAR(50),from_name VARCHAR(100),name TEXT,picture TEXT,source TEXT,height INTEGER,width INTEGER,link TEXT,icon TEXT,position INTEGER,created_time TIMESTAMP,updated_time TIMESTAMP,paging_previous VARCHAR(1024),latitude TEXT,longitude TEXT,location_name TEXT,paging_next VARCHAR(1024),_is_valid BOOLEAN, UNIQUE (id,_is_valid));"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 461
    const-string v0, "SnsAgent"

    const-string v1, "DB TABLE(photos_of_user) Has been created."

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 465
    const-string v0, "CREATE TABLE IF NOT EXISTS sync_photo_images (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,target_id VARCHAR(50),width INTEGER,height INTEGER,url VARCHAR(1024),_is_valid BOOLEAN, UNIQUE (_id));"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 474
    const-string v0, "SnsAgent"

    const-string v1, "DB TABLE(sync_photo_images) Has been created."

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 478
    const-string v0, "CREATE TABLE IF NOT EXISTS previous_sync_state (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,sync_type TEXT,previous_state TEXT,paging_previous VARCHAR(1024),paging_next VARCHAR(1024), UNIQUE (sync_type));"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 486
    const-string v0, "SnsAgent"

    const-string v1, "DB TABLE(previous_sync_state) Has been created."

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 490
    const-string v0, "CREATE TABLE IF NOT EXISTS thread (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,message_id VARCHAR(50),thread_id VARCHAR(50),author_id VARCHAR(50),author_name TEXT,author_pic TEXT,body TEXT,created_time TIMESTAMP,viewer_id VARCHAR(50), UNIQUE (message_id));"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 502
    const-string v0, "SnsAgent"

    const-string v1, "DB TABLE(thread) Has been created."

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 506
    const-string v0, "CREATE TABLE IF NOT EXISTS notification_query (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,notification_id VARCHAR(50),sender_id VARCHAR(50),recipient_id VARCHAR(50),object_id VARCHAR(50),object_type TEXT,created_time TIMESTAMP,updated_time TIMESTAMP,title_html VARCHAR(1024),title_text VARCHAR(100),body_html TEXT,body_text TEXT,href VARCHAR(1024),app_id VARCHAR(50),is_unread BOOLEAN,is_hidden BOOLEAN,icon_url VARCHAR(1024), UNIQUE (notification_id));"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 526
    const-string v0, "SnsAgent"

    const-string v1, "DB TABLE(notification_query) Has been created."

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 529
    const-string v0, "CREATE TABLE IF NOT EXISTS message_query (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,message_id VARCHAR(50),thread_id VARCHAR(50),author_id VARCHAR(50),body TEXT,created_time TIMESTAMP,viewer_id VARCHAR(50), UNIQUE (message_id));"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 539
    const-string v0, "SnsAgent"

    const-string v1, "DB TABLE(message_query) Has been created."

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 543
    const-string v0, "CREATE TABLE IF NOT EXISTS friend_request_query (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,user_id_from VARCHAR(50),user_id_to VARCHAR(50),message VARCHAR(100),time TIMESTAMP,unread TEXT);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 551
    const-string v0, "SnsAgent"

    const-string v1, "DB TABLE(friend_request_query) Has been created."

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 555
    const-string v0, "CREATE TABLE IF NOT EXISTS friends (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,friend_id VARCHAR(50),friend_name TEXT,friend_email TEXT,friend_pic TEXT,friend_birthdate TIMESTAMP,friend_availability TEXT,friend_relation_status TEXT,friend_current_status TEXT,friend_current_status_time TIMESTAMP,friend_current_status_comment_count INTEGER,gender TEXT,current_location TEXT,interests TEXT,update_time TIMESTAMP,hometown_location TEXT,locale TEXT,friend_relationship TEXT, UNIQUE (friend_id));"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 576
    const-string v0, "SnsAgent"

    const-string v1, "DB TABLE(friends) Has been created."

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 579
    const-string v0, "CREATE TABLE IF NOT EXISTS work (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,profile_id VARCHAR(50),name TEXT,employer_id TIMESTAMP,employer_name TEXT,position_id TEXT,position_name TEXT,from_id TEXT,from_name TEXT,location_id TEXT,location_name TEXT,start_date TEXT,end_date TEXT, UNIQUE (_id));"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 595
    const-string v0, "SnsAgent"

    const-string v1, "DB TABLE(work) Has been created."

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 598
    const-string v0, "CREATE TABLE IF NOT EXISTS education (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,profile_id VARCHAR(50),name TEXT,school_id TEXT,school_name TEXT,year_id TIMESTAMP,year_name TEXT,degree_id TEXT,degree_name TEXT,type TEXT, UNIQUE (_id));"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 611
    const-string v0, "SnsAgent"

    const-string v1, "DB TABLE(education) Has been created."

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 614
    const-string v0, "CREATE TABLE IF NOT EXISTS friendlist_group_members (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,group_id VARCHAR(50),group_name TEXT,group_type TEXT,mem_id VARCHAR(50),mem_name TEXT, UNIQUE (_id));"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 623
    const-string v0, "SnsAgent"

    const-string v1, "DB TABLE(friendlist_group_members) Has been created."

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 628
    const-string v0, "CREATE TABLE IF NOT EXISTS status_stream (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,post_id VARCHAR(50),from_id VARCHAR(50),from_name VARCHAR(100),to_id VARCHAR(50),to_name VARCHAR(100),message TEXT,picture TEXT,link TEXT,name TEXT,caption TEXT,description TEXT,source TEXT,properties TEXT,icon TEXT,actions TEXT,privacy TEXT,type TEXT,likes_name TEXT,likes_count INTEGER,comments_count INTEGER,object_id TEXT,application_name TEXT,application_id VARCHAR(50),create_time TIMESTAMP,updated_time TIMESTAMP,targeting TEXT,place_id VARCHAR(50),place_name TEXT,street TEXT,city TEXT,state TEXT,country TEXT,like_done TEXT,shares INTEGER,zip VARCHAR(50),latitude VARCHAR(50),longitude VARCHAR(50),_actor_photo_url VARCHAR(1024), UNIQUE (post_id));"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 670
    const-string v0, "SnsAgent"

    const-string v1, "DB TABLE(status_stream) Has been created."

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 673
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 674
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 675
    return-void
.end method

.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 0
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 36
    invoke-super {p0, p1}, Lcom/sec/android/app/sns3/agent/db/SnsDBHelperBase;->onCreate(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 38
    invoke-virtual {p0, p1}, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDBHelper;->createTables(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 39
    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 0
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "oldVersion"    # I
    .param p3, "newVersion"    # I

    .prologue
    .line 43
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/app/sns3/agent/db/SnsDBHelperBase;->onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V

    .line 45
    invoke-virtual {p0, p1}, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDBHelper;->upgradeTables(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 47
    return-void
.end method

.method public upgradeTables(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 680
    const-string v0, "DROP TABLE IF EXISTS user"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 681
    const-string v0, "DROP TABLE IF EXISTS feed_list"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 682
    const-string v0, "DROP TABLE IF EXISTS album_contents"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 683
    const-string v0, "DROP TABLE IF EXISTS birthday"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 684
    const-string v0, "DROP TABLE IF EXISTS post"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 685
    const-string v0, "DROP TABLE IF EXISTS comments"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 686
    const-string v0, "DROP TABLE IF EXISTS likes"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 687
    const-string v0, "DROP TABLE IF EXISTS event"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 688
    const-string v0, "DROP TABLE IF EXISTS album"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 689
    const-string v0, "DROP TABLE IF EXISTS photo"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 690
    const-string v0, "DROP TABLE IF EXISTS tags"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 691
    const-string v0, "DROP TABLE IF EXISTS photo_stream"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 692
    const-string v0, "DROP TABLE IF EXISTS groups"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 693
    const-string v0, "DROP TABLE IF EXISTS sync_event"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 694
    const-string v0, "DROP TABLE IF EXISTS sync_album"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 695
    const-string v0, "DROP TABLE IF EXISTS sync_photo"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 696
    const-string v0, "DROP TABLE IF EXISTS sync_tags"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 697
    const-string v0, "DROP TABLE IF EXISTS sync_photo_images"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 698
    const-string v0, "DROP TABLE IF EXISTS previous_sync_state"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 699
    const-string v0, "DROP TABLE IF EXISTS thread"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 700
    const-string v0, "DROP TABLE IF EXISTS notification_query"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 701
    const-string v0, "DROP TABLE IF EXISTS message_query"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 702
    const-string v0, "DROP TABLE IF EXISTS friend_request_query"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 703
    const-string v0, "DROP TABLE IF EXISTS status_stream"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 704
    invoke-virtual {p0, p1}, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDBHelper;->createTables(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 705
    return-void
.end method

.method public wipeData(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 709
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 711
    :try_start_0
    const-string v0, "DELETE FROM user"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 712
    const-string v0, "DELETE FROM feed_list"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 713
    const-string v0, "DELETE FROM album_contents"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 714
    const-string v0, "DELETE FROM birthday"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 715
    const-string v0, "DELETE FROM post"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 716
    const-string v0, "DELETE FROM comments"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 717
    const-string v0, "DELETE FROM likes"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 718
    const-string v0, "DELETE FROM event"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 719
    const-string v0, "DELETE FROM album"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 720
    const-string v0, "DELETE FROM photo"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 721
    const-string v0, "DELETE FROM tags"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 722
    const-string v0, "DELETE FROM photo_stream"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 723
    const-string v0, "DELETE FROM groups"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 724
    const-string v0, "DELETE FROM sync_event"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 725
    const-string v0, "DELETE FROM sync_album"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 726
    const-string v0, "DELETE FROM sync_photo"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 727
    const-string v0, "DELETE FROM sync_tags"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 728
    const-string v0, "DELETE FROM sync_photo_images"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 729
    const-string v0, "DELETE FROM previous_sync_state"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 730
    const-string v0, "DELETE FROM thread"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 731
    const-string v0, "DELETE FROM notification_query"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 732
    const-string v0, "DELETE FROM message_query"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 733
    const-string v0, "DELETE FROM friend_request_query"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 734
    const-string v0, "DELETE FROM friends"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 735
    const-string v0, "DELETE FROM education"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 736
    const-string v0, "DELETE FROM work"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 737
    const-string v0, "DELETE FROM friendlist_group_members"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 738
    const-string v0, "DELETE FROM status_stream"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 739
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 741
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 743
    return-void

    .line 741
    :catchall_0
    move-exception v0

    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0
.end method
