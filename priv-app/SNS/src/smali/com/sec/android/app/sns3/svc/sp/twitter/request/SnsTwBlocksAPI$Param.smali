.class public final enum Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI$Param;
.super Ljava/lang/Enum;
.source "SnsTwBlocksAPI.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Param"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI$Param;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI$Param;

.field public static final enum INCLUDE_ENTITIES:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI$Param;

.field public static final enum PAGE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI$Param;

.field public static final enum PER_PAGE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI$Param;

.field public static final enum SCREEN_NAME:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI$Param;

.field public static final enum SKIP_STATUS:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI$Param;

.field public static final enum STRINGIFY_IDS:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI$Param;

.field public static final enum USER_ID:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI$Param;


# instance fields
.field private mParam:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 37
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI$Param;

    const-string v1, "USER_ID"

    const-string v2, "user_id"

    invoke-direct {v0, v1, v4, v2}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI$Param;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI$Param;->USER_ID:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI$Param;

    .line 38
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI$Param;

    const-string v1, "SCREEN_NAME"

    const-string v2, "screen_name"

    invoke-direct {v0, v1, v5, v2}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI$Param;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI$Param;->SCREEN_NAME:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI$Param;

    .line 39
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI$Param;

    const-string v1, "PAGE"

    const-string v2, "page"

    invoke-direct {v0, v1, v6, v2}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI$Param;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI$Param;->PAGE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI$Param;

    .line 40
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI$Param;

    const-string v1, "PER_PAGE"

    const-string v2, "per_page"

    invoke-direct {v0, v1, v7, v2}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI$Param;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI$Param;->PER_PAGE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI$Param;

    .line 41
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI$Param;

    const-string v1, "INCLUDE_ENTITIES"

    const-string v2, "include_entities"

    invoke-direct {v0, v1, v8, v2}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI$Param;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI$Param;->INCLUDE_ENTITIES:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI$Param;

    .line 42
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI$Param;

    const-string v1, "SKIP_STATUS"

    const/4 v2, 0x5

    const-string v3, "skip_status"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI$Param;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI$Param;->SKIP_STATUS:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI$Param;

    .line 43
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI$Param;

    const-string v1, "STRINGIFY_IDS"

    const/4 v2, 0x6

    const-string v3, "stringify_ids"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI$Param;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI$Param;->STRINGIFY_IDS:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI$Param;

    .line 36
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI$Param;

    sget-object v1, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI$Param;->USER_ID:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI$Param;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI$Param;->SCREEN_NAME:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI$Param;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI$Param;->PAGE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI$Param;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI$Param;->PER_PAGE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI$Param;

    aput-object v1, v0, v7

    sget-object v1, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI$Param;->INCLUDE_ENTITIES:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI$Param;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI$Param;->SKIP_STATUS:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI$Param;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI$Param;->STRINGIFY_IDS:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI$Param;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI$Param;->$VALUES:[Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI$Param;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "param"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 47
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 48
    iput-object p3, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI$Param;->mParam:Ljava/lang/String;

    .line 49
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI$Param;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 36
    const-class v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI$Param;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI$Param;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI$Param;
    .locals 1

    .prologue
    .line 36
    sget-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI$Param;->$VALUES:[Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI$Param;

    invoke-virtual {v0}, [Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI$Param;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI$Param;

    return-object v0
.end method


# virtual methods
.method public getParam()Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI$Param;->mParam:Ljava/lang/String;

    return-object v0
.end method
