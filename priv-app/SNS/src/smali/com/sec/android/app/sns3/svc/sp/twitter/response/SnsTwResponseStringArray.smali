.class public Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStringArray;
.super Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;
.source "SnsTwResponseStringArray.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStringArray;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public mStringArray:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStringArray$1;

    invoke-direct {v0}, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStringArray$1;-><init>()V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStringArray;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;-><init>()V

    .line 32
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;-><init>()V

    .line 35
    invoke-virtual {p0, p1}, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStringArray;->readFromParcel(Landroid/os/Parcel;)V

    .line 36
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 56
    const/4 v0, 0x0

    return v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 49
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 50
    .local v0, "size":I
    new-array v1, v0, [Ljava/lang/String;

    iput-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStringArray;->mStringArray:[Ljava/lang/String;

    .line 51
    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStringArray;->mStringArray:[Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readStringArray([Ljava/lang/String;)V

    .line 52
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStringArray;->mStringArray:[Ljava/lang/String;

    array-length v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 62
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStringArray;->mStringArray:[Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    .line 63
    return-void
.end method
