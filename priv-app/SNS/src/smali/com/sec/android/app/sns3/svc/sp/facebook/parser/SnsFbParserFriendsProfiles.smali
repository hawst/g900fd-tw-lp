.class public Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserFriendsProfiles;
.super Ljava/lang/Object;
.source "SnsFbParserFriendsProfiles.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserFriendsProfiles$FacebookFriendsProfiles;
    }
.end annotation


# static fields
.field static final FQL_RESULT_SET:Ljava/lang/String; = "fql_result_set"

.field static final QUERY_NAME:Ljava/lang/String; = "name"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    return-void
.end method

.method public static parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsProfiles;
    .locals 14
    .param p0, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 73
    const/4 v2, 0x0

    .line 74
    .local v2, "friendlists":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsProfiles;
    const/4 v0, 0x0

    .line 77
    .local v0, "curFriendlists":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsProfiles;
    :try_start_0
    new-instance v5, Lorg/json/JSONArray;

    invoke-direct {v5, p0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 78
    .local v5, "jArr":Lorg/json/JSONArray;
    const/4 v4, 0x0

    .local v4, "j":I
    :goto_0
    invoke-virtual {v5}, Lorg/json/JSONArray;->length()I

    move-result v12

    if-ge v4, v12, :cond_5

    .line 80
    invoke-virtual {v5, v4}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v12

    const-string v13, "name"

    invoke-virtual {v12, v13}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 81
    .local v10, "queryName":Ljava/lang/String;
    invoke-virtual {v5, v4}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v12

    const-string v13, "fql_result_set"

    invoke-virtual {v12, v13}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 84
    .local v11, "queryObject":Ljava/lang/String;
    const-string v12, "user_query"

    invoke-virtual {v12, v10}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_4

    .line 86
    new-instance v6, Lorg/json/JSONArray;

    invoke-direct {v6, v11}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 87
    .local v6, "ja":Lorg/json/JSONArray;
    if-eqz v6, :cond_4

    .line 89
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    invoke-virtual {v6}, Lorg/json/JSONArray;->length()I

    move-result v12

    if-ge v3, v12, :cond_4

    .line 90
    new-instance v9, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsProfiles;

    invoke-direct {v9}, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsProfiles;-><init>()V

    .line 92
    .local v9, "newFriendlists":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsProfiles;
    invoke-virtual {v6, v3}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v7

    .line 94
    .local v7, "jsonObj":Lorg/json/JSONObject;
    const-string v12, "uid"

    invoke-virtual {v7, v12}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    iput-object v12, v9, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsProfiles;->mProfileID:Ljava/lang/String;

    .line 96
    const-string v12, "name"

    invoke-virtual {v7, v12}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    iput-object v12, v9, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsProfiles;->mName:Ljava/lang/String;

    .line 98
    const-string v12, "pic"

    invoke-virtual {v7, v12}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    iput-object v12, v9, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsProfiles;->mPic:Ljava/lang/String;

    .line 100
    const-string v12, "email"

    invoke-virtual {v7, v12}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    iput-object v12, v9, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsProfiles;->mEmail:Ljava/lang/String;

    .line 102
    const-string v12, "birthday_date"

    invoke-virtual {v7, v12}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    iput-object v12, v9, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsProfiles;->mBirthdayDate:Ljava/lang/String;

    .line 104
    const-string v12, "relationship_status"

    invoke-virtual {v7, v12}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    iput-object v12, v9, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsProfiles;->mRelationShipStatus:Ljava/lang/String;

    .line 106
    const-string v12, "online_presence"

    invoke-virtual {v7, v12}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    iput-object v12, v9, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsProfiles;->mOnlinePresence:Ljava/lang/String;

    .line 108
    const-string v12, "interests"

    invoke-virtual {v7, v12}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    iput-object v12, v9, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsProfiles;->mInterests:Ljava/lang/String;

    .line 110
    const-string v12, "profile_update_time"

    invoke-virtual {v7, v12}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    iput-object v12, v9, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsProfiles;->mUpdateTime:Ljava/lang/Long;

    .line 112
    const-string v12, "sex"

    invoke-virtual {v7, v12}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    iput-object v12, v9, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsProfiles;->mGender:Ljava/lang/String;

    .line 114
    const-string v12, "locale"

    invoke-virtual {v7, v12}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    iput-object v12, v9, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsProfiles;->mLocale:Ljava/lang/String;

    .line 116
    const-string v12, "current_location"

    invoke-virtual {v7, v12}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_0

    const-string v12, "current_location"

    invoke-virtual {v7, v12}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    const-string v13, "null"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_0

    .line 122
    new-instance v8, Lorg/json/JSONObject;

    const-string v12, "current_location"

    invoke-virtual {v7, v12}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    invoke-direct {v8, v12}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 124
    .local v8, "jsonStatObj":Lorg/json/JSONObject;
    const-string v12, "name"

    invoke-virtual {v8, v12}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    iput-object v12, v9, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsProfiles;->mCurrentLocation:Ljava/lang/String;

    .line 128
    .end local v8    # "jsonStatObj":Lorg/json/JSONObject;
    :cond_0
    const-string v12, "hometown_location"

    invoke-virtual {v7, v12}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_1

    const-string v12, "hometown_location"

    invoke-virtual {v7, v12}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    const-string v13, "null"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_1

    .line 134
    new-instance v8, Lorg/json/JSONObject;

    const-string v12, "hometown_location"

    invoke-virtual {v7, v12}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    invoke-direct {v8, v12}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 136
    .restart local v8    # "jsonStatObj":Lorg/json/JSONObject;
    const-string v12, "name"

    invoke-virtual {v8, v12}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    iput-object v12, v9, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsProfiles;->mHomeTownLocation:Ljava/lang/String;

    .line 139
    .end local v8    # "jsonStatObj":Lorg/json/JSONObject;
    :cond_1
    const-string v12, "status"

    invoke-virtual {v7, v12}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_2

    const-string v12, "status"

    invoke-virtual {v7, v12}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    const-string v13, "null"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_2

    .line 143
    new-instance v8, Lorg/json/JSONObject;

    const-string v12, "status"

    invoke-virtual {v7, v12}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    invoke-direct {v8, v12}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 145
    .restart local v8    # "jsonStatObj":Lorg/json/JSONObject;
    const-string v12, "message"

    invoke-virtual {v8, v12}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    iput-object v12, v9, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsProfiles;->mCurrentStatus:Ljava/lang/String;

    .line 147
    const-string v12, "time"

    invoke-virtual {v8, v12}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    iput-object v12, v9, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsProfiles;->mCurrentStatusTime:Ljava/lang/String;

    .line 149
    const-string v12, "comment_count"

    invoke-virtual {v8, v12}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    iput-object v12, v9, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsProfiles;->mCurrentStatusCommentCount:Ljava/lang/String;

    .line 153
    .end local v8    # "jsonStatObj":Lorg/json/JSONObject;
    :cond_2
    if-nez v2, :cond_3

    .line 154
    move-object v2, v9

    .line 155
    move-object v0, v2

    .line 89
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    .line 157
    :cond_3
    iput-object v9, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsProfiles;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsProfiles;

    .line 158
    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsProfiles;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsProfiles;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 78
    .end local v3    # "i":I
    .end local v6    # "ja":Lorg/json/JSONArray;
    .end local v7    # "jsonObj":Lorg/json/JSONObject;
    .end local v9    # "newFriendlists":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsProfiles;
    :cond_4
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_0

    .line 165
    .end local v4    # "j":I
    .end local v5    # "jArr":Lorg/json/JSONArray;
    .end local v10    # "queryName":Ljava/lang/String;
    .end local v11    # "queryObject":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 166
    .local v1, "e":Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    .line 169
    .end local v1    # "e":Lorg/json/JSONException;
    :cond_5
    return-object v2
.end method
