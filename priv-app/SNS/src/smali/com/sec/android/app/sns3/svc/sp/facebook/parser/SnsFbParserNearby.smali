.class public Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserNearby;
.super Ljava/lang/Object;
.source "SnsFbParserNearby.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserNearby$FacebookNearby;
    }
.end annotation


# instance fields
.field mNearby:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNearby;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserNearby;->mNearby:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNearby;

    .line 31
    return-void
.end method

.method private parseQuery1(Ljava/lang/String;)V
    .locals 7
    .param p1, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 117
    const/4 v0, 0x0

    .line 119
    .local v0, "curNearby":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNearby;
    const/4 v4, 0x0

    .line 122
    .local v4, "jsonObject":Lorg/json/JSONObject;
    :try_start_0
    new-instance v3, Lorg/json/JSONArray;

    invoke-direct {v3, p1}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 124
    .local v3, "ja":Lorg/json/JSONArray;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v6

    if-ge v2, v6, :cond_3

    .line 125
    new-instance v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNearby;

    invoke-direct {v5}, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNearby;-><init>()V

    .line 127
    .local v5, "newNearby":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNearby;
    invoke-virtual {v3, v2}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    .line 129
    const-string v6, "author_uid"

    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNearby;->mAuthorUID:Ljava/lang/String;

    .line 132
    const-string v6, "id"

    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNearby;->mID:Ljava/lang/String;

    .line 134
    const-string v6, "page_id"

    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNearby;->mPageID:Ljava/lang/String;

    .line 136
    const-string v6, "message"

    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNearby;->mMessage:Ljava/lang/String;

    .line 138
    const-string v6, "post_id"

    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 139
    const-string v6, "post_id"

    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNearby;->mPostId:Ljava/lang/String;

    .line 142
    :cond_0
    const-string v6, "timestamp"

    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNearby;->mTimestamp:Ljava/lang/String;

    .line 145
    const-string v6, "type"

    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNearby;->mType:Ljava/lang/String;

    .line 148
    if-nez v0, :cond_2

    .line 149
    move-object v0, v5

    .line 155
    :goto_1
    iget-object v6, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserNearby;->mNearby:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNearby;

    if-nez v6, :cond_1

    .line 156
    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserNearby;->mNearby:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNearby;

    .line 124
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 151
    :cond_2
    iput-object v5, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNearby;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNearby;

    .line 152
    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNearby;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNearby;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 159
    .end local v2    # "i":I
    .end local v3    # "ja":Lorg/json/JSONArray;
    .end local v5    # "newNearby":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNearby;
    :catch_0
    move-exception v1

    .line 160
    .local v1, "e":Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    .line 162
    .end local v1    # "e":Lorg/json/JSONException;
    :cond_3
    return-void
.end method

.method private parseQuery2(Ljava/lang/String;)V
    .locals 7
    .param p1, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 167
    :try_start_0
    new-instance v3, Lorg/json/JSONArray;

    invoke-direct {v3, p1}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 169
    .local v3, "ja":Lorg/json/JSONArray;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v5

    if-ge v2, v5, :cond_2

    .line 171
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserNearby;->mNearby:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNearby;

    .line 173
    .local v0, "curNearby":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNearby;
    invoke-virtual {v3, v2}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    .line 175
    .local v4, "jsonObj":Lorg/json/JSONObject;
    :goto_1
    if-eqz v0, :cond_1

    .line 176
    const-string v5, "page_id"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iget-object v6, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNearby;->mPageID:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 179
    const-string v5, "name"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNearby;->mPlaceName:Ljava/lang/String;

    .line 181
    const-string v5, "latitude"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNearby;->mLatitude:Ljava/lang/String;

    .line 183
    const-string v5, "longitude"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNearby;->mLongitude:Ljava/lang/String;

    .line 188
    :cond_0
    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNearby;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNearby;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 169
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 191
    .end local v0    # "curNearby":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNearby;
    .end local v2    # "i":I
    .end local v3    # "ja":Lorg/json/JSONArray;
    .end local v4    # "jsonObj":Lorg/json/JSONObject;
    :catch_0
    move-exception v1

    .line 192
    .local v1, "e":Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    .line 194
    .end local v1    # "e":Lorg/json/JSONException;
    :cond_2
    return-void
.end method

.method private parseQuery3(Ljava/lang/String;)V
    .locals 7
    .param p1, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 199
    :try_start_0
    new-instance v3, Lorg/json/JSONArray;

    invoke-direct {v3, p1}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 201
    .local v3, "ja":Lorg/json/JSONArray;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v5

    if-ge v2, v5, :cond_2

    .line 203
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserNearby;->mNearby:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNearby;

    .line 205
    .local v0, "curNearby":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNearby;
    invoke-virtual {v3, v2}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    .line 207
    .local v4, "jsonObj":Lorg/json/JSONObject;
    :goto_1
    if-eqz v0, :cond_1

    .line 209
    const-string v5, "uid"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iget-object v6, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNearby;->mAuthorUID:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 212
    const-string v5, "name"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNearby;->mUserName:Ljava/lang/String;

    .line 214
    const-string v5, "pic_big"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNearby;->mUserPicUrl:Ljava/lang/String;

    .line 216
    const-string v5, "sex"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNearby;->mSex:Ljava/lang/String;

    .line 219
    :cond_0
    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNearby;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNearby;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 201
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 222
    .end local v0    # "curNearby":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNearby;
    .end local v2    # "i":I
    .end local v3    # "ja":Lorg/json/JSONArray;
    .end local v4    # "jsonObj":Lorg/json/JSONObject;
    :catch_0
    move-exception v1

    .line 223
    .local v1, "e":Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    .line 225
    .end local v1    # "e":Lorg/json/JSONException;
    :cond_2
    return-void
.end method


# virtual methods
.method public parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNearby;
    .locals 7
    .param p1, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 88
    :try_start_0
    new-instance v2, Lorg/json/JSONArray;

    invoke-direct {v2, p1}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 90
    .local v2, "ja":Lorg/json/JSONArray;
    if-eqz v2, :cond_2

    .line 92
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v5

    if-ge v1, v5, :cond_2

    .line 94
    invoke-virtual {v2, v1}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v5

    const-string v6, "name"

    invoke-virtual {v5, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 95
    .local v3, "queryName":Ljava/lang/String;
    invoke-virtual {v2, v1}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v5

    const-string v6, "fql_result_set"

    invoke-virtual {v5, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 98
    .local v4, "queryObject":Ljava/lang/String;
    const-string v5, "query1"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 99
    invoke-direct {p0, v4}, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserNearby;->parseQuery1(Ljava/lang/String;)V

    .line 92
    :cond_0
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 100
    :cond_1
    const-string v5, "query2"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 101
    invoke-direct {p0, v4}, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserNearby;->parseQuery2(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 108
    .end local v1    # "i":I
    .end local v2    # "ja":Lorg/json/JSONArray;
    .end local v3    # "queryName":Ljava/lang/String;
    .end local v4    # "queryObject":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 109
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    .line 112
    .end local v0    # "e":Lorg/json/JSONException;
    :cond_2
    iget-object v5, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserNearby;->mNearby:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNearby;

    return-object v5

    .line 102
    .restart local v1    # "i":I
    .restart local v2    # "ja":Lorg/json/JSONArray;
    .restart local v3    # "queryName":Ljava/lang/String;
    .restart local v4    # "queryObject":Ljava/lang/String;
    :cond_3
    :try_start_1
    const-string v5, "query3"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 103
    invoke-direct {p0, v4}, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserNearby;->parseQuery3(Ljava/lang/String;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method
