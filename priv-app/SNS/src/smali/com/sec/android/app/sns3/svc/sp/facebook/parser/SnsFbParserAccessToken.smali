.class public Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserAccessToken;
.super Ljava/lang/Object;
.source "SnsFbParserAccessToken.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserAccessToken$FacebookAccessToken;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    return-void
.end method

.method public static parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAccessToken;
    .locals 11
    .param p0, "contentObj"    # Ljava/lang/String;

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 36
    new-instance v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAccessToken;

    invoke-direct {v5}, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAccessToken;-><init>()V

    .line 38
    .local v5, "user":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAccessToken;
    const-string v7, "&"

    invoke-virtual {p0, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 40
    .local v4, "splitUrl":[Ljava/lang/String;
    move-object v0, v4

    .local v0, "arr$":[Ljava/lang/String;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_2

    aget-object v3, v0, v1

    .line 41
    .local v3, "parameter":Ljava/lang/String;
    const-string v7, "="

    invoke-virtual {v3, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    .line 42
    .local v6, "v":[Ljava/lang/String;
    aget-object v7, v6, v9

    invoke-static {v7}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "access_token"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 43
    aget-object v7, v6, v10

    invoke-static {v7}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAccessToken;->mAccessToken:Ljava/lang/String;

    .line 40
    :cond_0
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 45
    :cond_1
    aget-object v7, v6, v9

    invoke-static {v7}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "expires"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 46
    aget-object v7, v6, v10

    invoke-static {v7}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAccessToken;->mExpires:Ljava/lang/String;

    goto :goto_1

    .line 49
    .end local v3    # "parameter":Ljava/lang/String;
    .end local v6    # "v":[Ljava/lang/String;
    :cond_2
    return-object v5
.end method
