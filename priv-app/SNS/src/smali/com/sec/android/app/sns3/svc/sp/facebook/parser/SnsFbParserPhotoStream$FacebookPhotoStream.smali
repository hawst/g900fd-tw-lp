.class public interface abstract Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserPhotoStream$FacebookPhotoStream;
.super Ljava/lang/Object;
.source "SnsFbParserPhotoStream.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserPhotoStream;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "FacebookPhotoStream"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserPhotoStream$FacebookPhotoStream$SnsFbPageQuery;,
        Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserPhotoStream$FacebookPhotoStream$SnsFbUserQuery;,
        Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserPhotoStream$FacebookPhotoStream$SnsFbPhotoQuery;,
        Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserPhotoStream$FacebookPhotoStream$SnsFbStreamQuery;
    }
.end annotation


# static fields
.field public static final FQL_RESULT_SET:Ljava/lang/String; = "fql_result_set"

.field public static final PAGE_QUERY_NAME:Ljava/lang/String; = "page_query"

.field public static final PHTO_QUERY_NAME:Ljava/lang/String; = "photo_query"

.field public static final QUERY_NAME:Ljava/lang/String; = "name"

.field public static final STREAM_QUERY_NAME:Ljava/lang/String; = "stream_query"

.field public static final USER_QUERY_NAME:Ljava/lang/String; = "user_query"
