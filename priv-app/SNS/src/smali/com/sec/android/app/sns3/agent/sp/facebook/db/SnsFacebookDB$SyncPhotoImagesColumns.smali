.class public interface abstract Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$SyncPhotoImagesColumns;
.super Ljava/lang/Object;
.source "SnsFacebookDB.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "SyncPhotoImagesColumns"
.end annotation


# static fields
.field public static final HEIGHT:Ljava/lang/String; = "height"

.field public static final TARGET_ID:Ljava/lang/String; = "target_id"

.field public static final URL:Ljava/lang/String; = "url"

.field public static final WIDTH:Ljava/lang/String; = "width"
