.class Lcom/sec/android/app/sns3/agent/sp/instagram/command/SnsInCmdGetHome$1;
.super Lcom/sec/android/app/sns3/svc/sp/instagram/request/SnsInReqGetHome;
.source "SnsInCmdGetHome.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/sns3/agent/sp/instagram/command/SnsInCmdGetHome;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Handler;Ljava/lang/String;Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/sns3/agent/sp/instagram/command/SnsInCmdGetHome;


# direct methods
.method constructor <init>(Lcom/sec/android/app/sns3/agent/sp/instagram/command/SnsInCmdGetHome;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Bundle;)V
    .locals 0
    .param p2, "x0"    # Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    .param p3, "x1"    # Landroid/os/Bundle;

    .prologue
    .line 47
    iput-object p1, p0, Lcom/sec/android/app/sns3/agent/sp/instagram/command/SnsInCmdGetHome$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/instagram/command/SnsInCmdGetHome;

    invoke-direct {p0, p2, p3}, Lcom/sec/android/app/sns3/svc/sp/instagram/request/SnsInReqGetHome;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Bundle;)V

    return-void
.end method


# virtual methods
.method public onReqRespond(IZIILandroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseFeedList;)Z
    .locals 8
    .param p1, "reqID"    # I
    .param p2, "bSuccess"    # Z
    .param p3, "httpStatus"    # I
    .param p4, "errorCode"    # I
    .param p5, "reason"    # Landroid/os/Bundle;
    .param p6, "feed"    # Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseFeedList;

    .prologue
    const/4 v7, 0x0

    .line 52
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 53
    .local v3, "values":Landroid/content/ContentValues;
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/sns3/SnsApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 55
    .local v0, "cr":Landroid/content/ContentResolver;
    if-eqz p2, :cond_1

    .line 56
    sget-object v4, Lcom/sec/android/app/sns3/agent/sp/instagram/db/SnsInstagramDB$UserInFeedInfo;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v4, v7, v7}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 57
    invoke-virtual {p6}, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseFeedList;->getFeeds()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseFeed;

    .line 59
    .local v1, "curFeed":Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseFeed;
    invoke-virtual {v3}, Landroid/content/ContentValues;->clear()V

    .line 61
    const-string v4, "post_id"

    iget-object v5, v1, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseFeed;->mPostId:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    const-string v4, "message"

    iget-object v5, v1, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseFeed;->mCaption:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    const-string v4, "timestamp_utc"

    iget-object v5, v1, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseFeed;->mCreatedTime:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    const-string v4, "object_type"

    iget-object v5, v1, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseFeed;->mType:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    const-string v4, "media_url"

    iget-object v5, v1, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseFeed;->mUrl:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    const-string v4, "link"

    iget-object v5, v1, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseFeed;->mLink:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    const-string v4, "width"

    iget-object v5, v1, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseFeed;->mWidth:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    const-string v4, "height"

    iget-object v5, v1, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseFeed;->mHeight:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    const-string v4, "location_name"

    iget-object v5, v1, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseFeed;->mLocName:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    const-string v4, "latitude"

    iget-object v5, v1, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseFeed;->mLatitude:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    const-string v4, "longitude"

    iget-object v5, v1, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseFeed;->mLongitude:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    sget-object v4, Lcom/sec/android/app/sns3/agent/sp/instagram/db/SnsInstagramDB$UserInFeedInfo;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v4, v3}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    goto :goto_0

    .line 77
    .end local v1    # "curFeed":Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseFeed;
    :cond_0
    iget-object v4, p0, Lcom/sec/android/app/sns3/agent/sp/instagram/command/SnsInCmdGetHome$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/instagram/command/SnsInCmdGetHome;

    sget-object v5, Lcom/sec/android/app/sns3/agent/sp/instagram/db/SnsInstagramDB$UserInFeedInfo;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v5}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/sns3/agent/sp/instagram/command/SnsInCmdGetHome;->setUri(Ljava/lang/String;)V

    .line 84
    .end local v2    # "i$":Ljava/util/Iterator;
    :goto_1
    iget-object v4, p0, Lcom/sec/android/app/sns3/agent/sp/instagram/command/SnsInCmdGetHome$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/instagram/command/SnsInCmdGetHome;

    invoke-virtual {v4, p2}, Lcom/sec/android/app/sns3/agent/sp/instagram/command/SnsInCmdGetHome;->setSuccess(Z)V

    .line 85
    iget-object v4, p0, Lcom/sec/android/app/sns3/agent/sp/instagram/command/SnsInCmdGetHome$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/instagram/command/SnsInCmdGetHome;

    invoke-virtual {v4, p0}, Lcom/sec/android/app/sns3/agent/sp/instagram/command/SnsInCmdGetHome;->receive(Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;)V

    .line 87
    const/4 v4, 0x1

    return v4

    .line 79
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/sns3/agent/sp/instagram/command/SnsInCmdGetHome$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/instagram/command/SnsInCmdGetHome;

    new-instance v5, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;

    const-string v6, "instagram"

    invoke-direct {v5, v6, p3, p4, p5}, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;-><init>(Ljava/lang/String;IILandroid/os/Bundle;)V

    invoke-virtual {v4, v5}, Lcom/sec/android/app/sns3/agent/sp/instagram/command/SnsInCmdGetHome;->setResponseList(Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;)Z

    .line 81
    iget-object v4, p0, Lcom/sec/android/app/sns3/agent/sp/instagram/command/SnsInCmdGetHome$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/instagram/command/SnsInCmdGetHome;

    invoke-virtual {v4, v7}, Lcom/sec/android/app/sns3/agent/sp/instagram/command/SnsInCmdGetHome;->setUri(Ljava/lang/String;)V

    goto :goto_1
.end method
