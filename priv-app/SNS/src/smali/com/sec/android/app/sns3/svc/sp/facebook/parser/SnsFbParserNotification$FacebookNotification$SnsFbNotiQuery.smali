.class public interface abstract Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserNotification$FacebookNotification$SnsFbNotiQuery;
.super Ljava/lang/Object;
.source "SnsFbParserNotification.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserNotification$FacebookNotification;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "SnsFbNotiQuery"
.end annotation


# static fields
.field public static final APP_ID:Ljava/lang/String; = "app_id"

.field public static final BODY_HTML:Ljava/lang/String; = "body_html"

.field public static final BODY_TEXT:Ljava/lang/String; = "body_text"

.field public static final CREATED_TIME:Ljava/lang/String; = "created_time"

.field public static final HREF:Ljava/lang/String; = "href"

.field public static final ICON_URL:Ljava/lang/String; = "icon_url"

.field public static final IS_HIDDEN:Ljava/lang/String; = "is_hidden"

.field public static final IS_UNREAD:Ljava/lang/String; = "is_unread"

.field public static final NOTIFICATION_ID:Ljava/lang/String; = "notification_id"

.field public static final OBJECT_ID:Ljava/lang/String; = "object_id"

.field public static final OBJECT_TYPE:Ljava/lang/String; = "object_type"

.field public static final RECIPIENT_ID:Ljava/lang/String; = "recipient_id"

.field public static final SENDER_ID:Ljava/lang/String; = "sender_id"

.field public static final TITLE_HTML:Ljava/lang/String; = "title_html"

.field public static final TITLE_TEXT:Ljava/lang/String; = "title_text"

.field public static final UPDATED_TIME:Ljava/lang/String; = "updated_time"
