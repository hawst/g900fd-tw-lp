.class public Lcom/sec/android/app/sns3/app/ticker/sp/qzone/SnsQzTickerIntervalSet;
.super Ljava/lang/Object;
.source "SnsQzTickerIntervalSet.java"


# static fields
.field private static final CHANGED_TICKER_INFO_FOR_PENDINGINTENT:Ljava/lang/String; = "android.sns.CHANGED_TICKER_INFO_FOR_PENDINGINTENT"

.field public static final INTERVAL_SET_1_HOUR:I = 0x2

.field public static final INTERVAL_SET_2_HOUR:I = 0x3

.field public static final INTERVAL_SET_30_MINUTE:I = 0x1

.field public static final INTERVAL_SET_4_HOUR:I = 0x4

.field public static final INTERVAL_SET_NONE:I = 0x0

.field private static final TAG:Ljava/lang/String; = "Sns_ticker_interval_set"

.field private static final TIME_1_HOUR:I = 0x36ee80

.field private static final TIME_2_HOURS:I = 0x6ddd00

.field private static final TIME_30_MINUTE:I = 0x1b7740

.field private static final TIME_4_HOURS:I = 0xdbba00

.field private static final TIME_NONE:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static setAutoRefresh(Landroid/content/Context;I)V
    .locals 12
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "autoRefreshKey"    # I

    .prologue
    const/4 v11, 0x0

    .line 30
    invoke-static {p0}, Lcom/sec/android/app/sns3/app/ticker/sp/qzone/SnsQzTickerIntervalSet;->stopAutoRefresh(Landroid/content/Context;)V

    .line 32
    const/4 v10, 0x0

    .line 34
    .local v10, "interval":I
    const-string v1, "Sns_ticker_interval_set"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "autoRefreshKey is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 36
    packed-switch p1, :pswitch_data_0

    .line 52
    const-string v1, "Sns_ticker_interval_set"

    const-string v4, "setAutoRefresh() : autoRefreshKey is wrong"

    invoke-static {v1, v4}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 56
    :goto_0
    const-string v1, "alarm"

    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 58
    .local v0, "tickerAlarmManager":Landroid/app/AlarmManager;
    if-eqz v0, :cond_0

    .line 60
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    .line 61
    .local v8, "currnetTime":J
    int-to-long v4, v10

    add-long v2, v8, v4

    .line 63
    .local v2, "triggerAtTime":J
    new-instance v7, Landroid/content/Intent;

    const-string v1, "android.sns.CHANGED_TICKER_INFO_FOR_PENDINGINTENT"

    invoke-direct {v7, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 64
    .local v7, "intent":Landroid/content/Intent;
    invoke-static {p0, v11, v7, v11}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v6

    .line 66
    .local v6, "pendingIntent":Landroid/app/PendingIntent;
    const/4 v1, 0x1

    int-to-long v4, v10

    invoke-virtual/range {v0 .. v6}, Landroid/app/AlarmManager;->setRepeating(IJJLandroid/app/PendingIntent;)V

    .line 68
    const-string v1, "Sns_ticker_interval_set"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "setAutoRefresh() triggerAtTime : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", interval : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", operation"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 73
    .end local v0    # "tickerAlarmManager":Landroid/app/AlarmManager;
    .end local v2    # "triggerAtTime":J
    .end local v6    # "pendingIntent":Landroid/app/PendingIntent;
    .end local v7    # "intent":Landroid/content/Intent;
    .end local v8    # "currnetTime":J
    :goto_1
    :pswitch_0
    return-void

    .line 40
    :pswitch_1
    const v10, 0x1b7740

    .line 41
    goto :goto_0

    .line 43
    :pswitch_2
    const v10, 0x36ee80

    .line 44
    goto :goto_0

    .line 46
    :pswitch_3
    const v10, 0x6ddd00

    .line 47
    goto :goto_0

    .line 49
    :pswitch_4
    const v10, 0xdbba00

    .line 50
    goto :goto_0

    .line 71
    .restart local v0    # "tickerAlarmManager":Landroid/app/AlarmManager;
    :cond_0
    const-string v1, "Sns_ticker_interval_set"

    const-string v4, "setAutoRefresh() : tickerAlarmManager is null"

    invoke-static {v1, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 36
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static stopAutoRefresh(Landroid/content/Context;)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v5, 0x0

    .line 77
    const-string v3, "Sns_ticker_interval_set"

    const-string v4, "stopAutoRefresh start"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 79
    if-eqz p0, :cond_1

    .line 80
    new-instance v0, Landroid/content/Intent;

    const-string v3, "android.sns.CHANGED_TICKER_INFO_FOR_PENDINGINTENT"

    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 81
    .local v0, "intent":Landroid/content/Intent;
    invoke-static {p0, v5, v0, v5}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 82
    .local v1, "pendingIntent":Landroid/app/PendingIntent;
    const-string v3, "alarm"

    invoke-virtual {p0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/AlarmManager;

    .line 84
    .local v2, "tickerAlarmManager":Landroid/app/AlarmManager;
    if-eqz v2, :cond_0

    .line 85
    invoke-virtual {v2, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 90
    .end local v0    # "intent":Landroid/content/Intent;
    .end local v1    # "pendingIntent":Landroid/app/PendingIntent;
    .end local v2    # "tickerAlarmManager":Landroid/app/AlarmManager;
    :cond_0
    :goto_0
    return-void

    .line 88
    :cond_1
    const-string v3, "Sns_ticker_interval_set"

    const-string v4, "stopAutoRefresh context is null"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
