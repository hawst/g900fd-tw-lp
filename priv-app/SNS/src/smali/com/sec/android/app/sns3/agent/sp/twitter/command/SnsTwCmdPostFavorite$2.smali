.class Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdPostFavorite$2;
.super Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestFavorites;
.source "SnsTwCmdPostFavorite.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdPostFavorite;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Handler;Ljava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdPostFavorite;


# direct methods
.method constructor <init>(Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdPostFavorite;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwFavoritesAPI;Landroid/os/Bundle;)V
    .locals 0
    .param p2, "x0"    # Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    .param p3, "x1"    # Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwFavoritesAPI;
    .param p4, "x2"    # Landroid/os/Bundle;

    .prologue
    .line 72
    iput-object p1, p0, Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdPostFavorite$2;->this$0:Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdPostFavorite;

    invoke-direct {p0, p2, p3, p4}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestFavorites;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwFavoritesAPI;Landroid/os/Bundle;)V

    return-void
.end method


# virtual methods
.method public onReqRespond(IZIILandroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;)Z
    .locals 3
    .param p1, "reqID"    # I
    .param p2, "bSuccess"    # Z
    .param p3, "httpStatus"    # I
    .param p4, "errorCode"    # I
    .param p5, "reason"    # Landroid/os/Bundle;
    .param p6, "resp"    # Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;

    .prologue
    .line 78
    if-nez p2, :cond_0

    .line 79
    iget-object v0, p0, Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdPostFavorite$2;->this$0:Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdPostFavorite;

    new-instance v1, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;

    const-string v2, "twitter"

    invoke-direct {v1, v2, p3, p4, p5}, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;-><init>(Ljava/lang/String;IILandroid/os/Bundle;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdPostFavorite;->setResponseList(Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;)Z

    .line 81
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdPostFavorite$2;->this$0:Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdPostFavorite;

    invoke-virtual {v0, p2}, Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdPostFavorite;->setSuccess(Z)V

    .line 82
    iget-object v0, p0, Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdPostFavorite$2;->this$0:Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdPostFavorite;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdPostFavorite;->setUri(Ljava/lang/String;)V

    .line 83
    iget-object v0, p0, Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdPostFavorite$2;->this$0:Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdPostFavorite;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdPostFavorite;->receive(Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;)V

    .line 85
    const/4 v0, 0x0

    return v0
.end method
