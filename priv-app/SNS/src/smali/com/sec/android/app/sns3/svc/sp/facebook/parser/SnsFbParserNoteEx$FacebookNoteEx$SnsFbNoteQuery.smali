.class public interface abstract Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserNoteEx$FacebookNoteEx$SnsFbNoteQuery;
.super Ljava/lang/Object;
.source "SnsFbParserNoteEx.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserNoteEx$FacebookNoteEx;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "SnsFbNoteQuery"
.end annotation


# static fields
.field public static final CONTENT:Ljava/lang/String; = "content"

.field public static final CONTENT_HTML:Ljava/lang/String; = "content_html"

.field public static final CREATED_TIME:Ljava/lang/String; = "created_time"

.field public static final NOTE_ID:Ljava/lang/String; = "note_id"

.field public static final TITLE:Ljava/lang/String; = "title"

.field public static final UPDATED_TIME:Ljava/lang/String; = "updated_time"

.field public static final USER_ID:Ljava/lang/String; = "uid"
