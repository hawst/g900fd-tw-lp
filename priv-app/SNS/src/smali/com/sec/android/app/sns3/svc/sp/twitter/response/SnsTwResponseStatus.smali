.class public Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;
.super Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;
.source "SnsTwResponseStatus.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public mContributors:Ljava/lang/String;

.field public mCoordinates:Ljava/lang/String;

.field public mCreatedAt:Ljava/lang/String;

.field public mEntities:Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseEntities;

.field public mFavorited:Z

.field public mFavouritesCount:I

.field public mGeo:Ljava/lang/String;

.field public mId:Ljava/lang/String;

.field public mInReplyToScreenName:Ljava/lang/String;

.field public mInReplyToStatusId:Ljava/lang/String;

.field public mInReplyToUserId:Ljava/lang/String;

.field public mNext:Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;

.field public mPlace:Ljava/lang/String;

.field public mPossiblySensitive:Z

.field public mRetweetCount:I

.field public mRetweeted:Z

.field public mRetweetedStatus:Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;

.field public mSource:Ljava/lang/String;

.field public mText:Ljava/lang/String;

.field public mTruncated:Z

.field public mUser:Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseUser;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 78
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus$1;

    invoke-direct {v0}, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus$1;-><init>()V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 71
    invoke-direct {p0}, Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;-><init>()V

    .line 72
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 74
    invoke-direct {p0}, Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;-><init>()V

    .line 75
    invoke-direct {p0, p1}, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;->readFromParcel(Landroid/os/Parcel;)V

    .line 76
    return-void
.end method

.method private readFromParcel(Landroid/os/Parcel;)V
    .locals 3
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 90
    const-class v0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseUser;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseUser;

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;->mUser:Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseUser;

    .line 91
    const-class v0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;->mRetweetedStatus:Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;

    .line 92
    const-class v0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseEntities;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseEntities;

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;->mEntities:Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseEntities;

    .line 94
    const-class v0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;->mNext:Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;

    .line 96
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;->mId:Ljava/lang/String;

    .line 97
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;->mRetweeted:Z

    .line 98
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-ne v0, v1, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;->mPossiblySensitive:Z

    .line 99
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;->mInReplyToUserId:Ljava/lang/String;

    .line 100
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;->mContributors:Ljava/lang/String;

    .line 101
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;->mFavorited:Z

    .line 102
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-ne v0, v1, :cond_3

    :goto_3
    iput-boolean v1, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;->mTruncated:Z

    .line 103
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;->mSource:Ljava/lang/String;

    .line 104
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;->mPlace:Ljava/lang/String;

    .line 105
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;->mGeo:Ljava/lang/String;

    .line 106
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;->mCoordinates:Ljava/lang/String;

    .line 107
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;->mText:Ljava/lang/String;

    .line 108
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;->mCreatedAt:Ljava/lang/String;

    .line 109
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;->mRetweetCount:I

    .line 110
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;->mInReplyToScreenName:Ljava/lang/String;

    .line 111
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;->mInReplyToStatusId:Ljava/lang/String;

    .line 112
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;->mFavouritesCount:I

    .line 113
    return-void

    :cond_0
    move v0, v2

    .line 97
    goto :goto_0

    :cond_1
    move v0, v2

    .line 98
    goto :goto_1

    :cond_2
    move v0, v2

    .line 101
    goto :goto_2

    :cond_3
    move v1, v2

    .line 102
    goto :goto_3
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 117
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 123
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;->mUser:Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseUser;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 124
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;->mRetweetedStatus:Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 125
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;->mEntities:Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseEntities;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 127
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;->mNext:Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 129
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;->mId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 130
    iget-boolean v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;->mRetweeted:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 131
    iget-boolean v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;->mPossiblySensitive:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 132
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;->mInReplyToUserId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 133
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;->mContributors:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 134
    iget-boolean v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;->mFavorited:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 135
    iget-boolean v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;->mTruncated:Z

    if-eqz v0, :cond_3

    :goto_3
    int-to-byte v0, v1

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 136
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;->mSource:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 137
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;->mPlace:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 138
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;->mGeo:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 139
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;->mCoordinates:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 140
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;->mText:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 141
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;->mCreatedAt:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 142
    iget v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;->mRetweetCount:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 143
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;->mInReplyToScreenName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 144
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;->mInReplyToStatusId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 145
    iget v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;->mFavouritesCount:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 146
    return-void

    :cond_0
    move v0, v2

    .line 130
    goto :goto_0

    :cond_1
    move v0, v2

    .line 131
    goto :goto_1

    :cond_2
    move v0, v2

    .line 134
    goto :goto_2

    :cond_3
    move v1, v2

    .line 135
    goto :goto_3
.end method
