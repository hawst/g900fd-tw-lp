.class Lcom/sec/android/app/sns3/agent/sp/sinaweibo/command/SnsSwCmdGetUserTimeline$1;
.super Lcom/sec/android/app/sns3/svc/sp/sinaweibo/request/SnsSwReqGetStatuses;
.source "SnsSwCmdGetUserTimeline.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/sns3/agent/sp/sinaweibo/command/SnsSwCmdGetUserTimeline;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Handler;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/sns3/agent/sp/sinaweibo/command/SnsSwCmdGetUserTimeline;


# direct methods
.method constructor <init>(Lcom/sec/android/app/sns3/agent/sp/sinaweibo/command/SnsSwCmdGetUserTimeline;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Lcom/sec/android/app/sns3/svc/sp/sinaweibo/request/SnsSwStatusesAPI;Ljava/lang/String;)V
    .locals 0
    .param p2, "x0"    # Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    .param p3, "x1"    # Lcom/sec/android/app/sns3/svc/sp/sinaweibo/request/SnsSwStatusesAPI;
    .param p4, "x2"    # Ljava/lang/String;

    .prologue
    .line 51
    iput-object p1, p0, Lcom/sec/android/app/sns3/agent/sp/sinaweibo/command/SnsSwCmdGetUserTimeline$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/sinaweibo/command/SnsSwCmdGetUserTimeline;

    invoke-direct {p0, p2, p3, p4}, Lcom/sec/android/app/sns3/svc/sp/sinaweibo/request/SnsSwReqGetStatuses;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Lcom/sec/android/app/sns3/svc/sp/sinaweibo/request/SnsSwStatusesAPI;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public onReqRespond(IZIILandroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/sinaweibo/response/SnsSwResponseStatus;)Z
    .locals 7
    .param p1, "reqID"    # I
    .param p2, "bSuccess"    # Z
    .param p3, "httpStatus"    # I
    .param p4, "errorCode"    # I
    .param p5, "reason"    # Landroid/os/Bundle;
    .param p6, "feed"    # Lcom/sec/android/app/sns3/svc/sp/sinaweibo/response/SnsSwResponseStatus;

    .prologue
    const/4 v6, 0x0

    .line 57
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 58
    .local v2, "values":Landroid/content/ContentValues;
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/sns3/SnsApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 60
    .local v0, "cr":Landroid/content/ContentResolver;
    if-eqz p2, :cond_1

    .line 61
    if-eqz p6, :cond_0

    .line 63
    sget-object v3, Lcom/sec/android/app/sns3/agent/sp/sinaweibo/db/SnsSinaweiboDB$UserTimeLine;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v3, v6, v6}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 65
    move-object v1, p6

    .line 66
    .local v1, "curFeed":Lcom/sec/android/app/sns3/svc/sp/sinaweibo/response/SnsSwResponseStatus;
    :goto_0
    if-eqz v1, :cond_0

    .line 68
    invoke-virtual {v2}, Landroid/content/ContentValues;->clear()V

    .line 70
    const-string v3, "post_id"

    iget-object v4, v1, Lcom/sec/android/app/sns3/svc/sp/sinaweibo/response/SnsSwResponseStatus;->mFeedID:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    const-string v3, "message"

    iget-object v4, v1, Lcom/sec/android/app/sns3/svc/sp/sinaweibo/response/SnsSwResponseStatus;->mMessage:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    const-string v3, "picture"

    iget-object v4, v1, Lcom/sec/android/app/sns3/svc/sp/sinaweibo/response/SnsSwResponseStatus;->mPicture:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    const-string v3, "from_name"

    iget-object v4, v1, Lcom/sec/android/app/sns3/svc/sp/sinaweibo/response/SnsSwResponseStatus;->mName:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    const-string v3, "create_time"

    iget-object v4, v1, Lcom/sec/android/app/sns3/svc/sp/sinaweibo/response/SnsSwResponseStatus;->mCreatedTime:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    const-string v3, "updated_time"

    iget-object v4, p0, Lcom/sec/android/app/sns3/agent/sp/sinaweibo/command/SnsSwCmdGetUserTimeline$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/sinaweibo/command/SnsSwCmdGetUserTimeline;

    # invokes: Lcom/sec/android/app/sns3/agent/sp/sinaweibo/command/SnsSwCmdGetUserTimeline;->getCurrentTime()Ljava/lang/Long;
    invoke-static {v4}, Lcom/sec/android/app/sns3/agent/sp/sinaweibo/command/SnsSwCmdGetUserTimeline;->access$000(Lcom/sec/android/app/sns3/agent/sp/sinaweibo/command/SnsSwCmdGetUserTimeline;)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 82
    sget-object v3, Lcom/sec/android/app/sns3/agent/sp/sinaweibo/db/SnsSinaweiboDB$UserTimeLine;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v3, v2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 84
    iget-object v1, v1, Lcom/sec/android/app/sns3/svc/sp/sinaweibo/response/SnsSwResponseStatus;->mNext:Lcom/sec/android/app/sns3/svc/sp/sinaweibo/response/SnsSwResponseStatus;

    goto :goto_0

    .line 87
    .end local v1    # "curFeed":Lcom/sec/android/app/sns3/svc/sp/sinaweibo/response/SnsSwResponseStatus;
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/sns3/agent/sp/sinaweibo/command/SnsSwCmdGetUserTimeline$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/sinaweibo/command/SnsSwCmdGetUserTimeline;

    sget-object v4, Lcom/sec/android/app/sns3/agent/sp/sinaweibo/db/SnsSinaweiboDB$UserTimeLine;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/sns3/agent/sp/sinaweibo/command/SnsSwCmdGetUserTimeline;->setUri(Ljava/lang/String;)V

    .line 93
    :goto_1
    iget-object v3, p0, Lcom/sec/android/app/sns3/agent/sp/sinaweibo/command/SnsSwCmdGetUserTimeline$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/sinaweibo/command/SnsSwCmdGetUserTimeline;

    invoke-virtual {v3, p2}, Lcom/sec/android/app/sns3/agent/sp/sinaweibo/command/SnsSwCmdGetUserTimeline;->setSuccess(Z)V

    .line 94
    iget-object v3, p0, Lcom/sec/android/app/sns3/agent/sp/sinaweibo/command/SnsSwCmdGetUserTimeline$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/sinaweibo/command/SnsSwCmdGetUserTimeline;

    invoke-virtual {v3, p0}, Lcom/sec/android/app/sns3/agent/sp/sinaweibo/command/SnsSwCmdGetUserTimeline;->receive(Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;)V

    .line 96
    const/4 v3, 0x1

    return v3

    .line 89
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/sns3/agent/sp/sinaweibo/command/SnsSwCmdGetUserTimeline$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/sinaweibo/command/SnsSwCmdGetUserTimeline;

    new-instance v4, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;

    const-string v5, "sinaweibo"

    invoke-direct {v4, v5, p3, p4, p5}, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;-><init>(Ljava/lang/String;IILandroid/os/Bundle;)V

    invoke-virtual {v3, v4}, Lcom/sec/android/app/sns3/agent/sp/sinaweibo/command/SnsSwCmdGetUserTimeline;->setResponseList(Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;)Z

    .line 90
    iget-object v3, p0, Lcom/sec/android/app/sns3/agent/sp/sinaweibo/command/SnsSwCmdGetUserTimeline$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/sinaweibo/command/SnsSwCmdGetUserTimeline;

    invoke-virtual {v3, v6}, Lcom/sec/android/app/sns3/agent/sp/sinaweibo/command/SnsSwCmdGetUserTimeline;->setUri(Ljava/lang/String;)V

    goto :goto_1
.end method
