.class public interface abstract Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserReviews$FacebookReviews;
.super Ljava/lang/Object;
.source "SnsFbParserReviews.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserReviews;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "FacebookReviews"
.end annotation


# static fields
.field public static final CREATED_TIME:Ljava/lang/String; = "created_time"

.field public static final DATA:Ljava/lang/String; = "data"

.field public static final FROM:Ljava/lang/String; = "from"

.field public static final ID:Ljava/lang/String; = "id"

.field public static final MESSAGE:Ljava/lang/String; = "message"

.field public static final NAME:Ljava/lang/String; = "name"

.field public static final RATING:Ljava/lang/String; = "rating"

.field public static final TO:Ljava/lang/String; = "to"
