.class public interface abstract Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$FriendsEducationColumns;
.super Ljava/lang/Object;
.source "SnsFacebookDB.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "FriendsEducationColumns"
.end annotation


# static fields
.field public static final DEGREE_ID:Ljava/lang/String; = "degree_id"

.field public static final DEGREE_NAME:Ljava/lang/String; = "degree_name"

.field public static final NAME:Ljava/lang/String; = "name"

.field public static final PROFILE_ID:Ljava/lang/String; = "profile_id"

.field public static final SCHOOL_ID:Ljava/lang/String; = "school_id"

.field public static final SCHOOL_NAME:Ljava/lang/String; = "school_name"

.field public static final TYPE:Ljava/lang/String; = "type"

.field public static final YEAR_ID:Ljava/lang/String; = "year_id"

.field public static final YEAR_NAME:Ljava/lang/String; = "year_name"
