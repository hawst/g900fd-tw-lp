.class Lcom/sec/android/app/sns3/agent/sp/qzone/command/SnsQzCmdGetUserTimeline$1;
.super Lcom/sec/android/app/sns3/svc/sp/qzone/request/SnsQzReqGetUserTimeline;
.source "SnsQzCmdGetUserTimeline.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/sns3/agent/sp/qzone/command/SnsQzCmdGetUserTimeline;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Handler;Ljava/lang/String;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/sns3/agent/sp/qzone/command/SnsQzCmdGetUserTimeline;


# direct methods
.method constructor <init>(Lcom/sec/android/app/sns3/agent/sp/qzone/command/SnsQzCmdGetUserTimeline;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;I)V
    .locals 0
    .param p2, "x0"    # Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    .param p3, "x1"    # Ljava/lang/String;
    .param p4, "x2"    # I

    .prologue
    .line 66
    iput-object p1, p0, Lcom/sec/android/app/sns3/agent/sp/qzone/command/SnsQzCmdGetUserTimeline$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/qzone/command/SnsQzCmdGetUserTimeline;

    invoke-direct {p0, p2, p3, p4}, Lcom/sec/android/app/sns3/svc/sp/qzone/request/SnsQzReqGetUserTimeline;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;I)V

    return-void
.end method


# virtual methods
.method public onReqRespond(IZIILandroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseUserTimeline;)Z
    .locals 7
    .param p1, "reqID"    # I
    .param p2, "bSuccess"    # Z
    .param p3, "httpStatus"    # I
    .param p4, "errorCode"    # I
    .param p5, "reason"    # Landroid/os/Bundle;
    .param p6, "feed"    # Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseUserTimeline;

    .prologue
    const/4 v6, 0x0

    .line 72
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 73
    .local v2, "values":Landroid/content/ContentValues;
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/sns3/SnsApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 77
    .local v0, "cr":Landroid/content/ContentResolver;
    if-eqz p2, :cond_2

    .line 78
    if-eqz p6, :cond_1

    .line 80
    sget-object v3, Lcom/sec/android/app/sns3/agent/sp/qzone/db/SnsQzoneDB$UserTimeLine;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v3, v6, v6}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 82
    move-object v1, p6

    .line 83
    .local v1, "curFeed":Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseUserTimeline;
    :goto_0
    if-eqz v1, :cond_1

    .line 85
    iget-object v3, p0, Lcom/sec/android/app/sns3/agent/sp/qzone/command/SnsQzCmdGetUserTimeline$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/qzone/command/SnsQzCmdGetUserTimeline;

    # getter for: Lcom/sec/android/app/sns3/agent/sp/qzone/command/SnsQzCmdGetUserTimeline;->mUserID:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/sns3/agent/sp/qzone/command/SnsQzCmdGetUserTimeline;->access$000(Lcom/sec/android/app/sns3/agent/sp/qzone/command/SnsQzCmdGetUserTimeline;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, v1, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseUserTimeline;->mOpenID:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 86
    invoke-virtual {v2}, Landroid/content/ContentValues;->clear()V

    .line 88
    const-string v3, "feed_id"

    iget-object v4, v1, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseUserTimeline;->mFeedID:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    const-string v3, "message"

    iget-object v4, v1, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseUserTimeline;->mMessage:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    const-string v3, "picture"

    iget-object v4, v1, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseUserTimeline;->mPicture:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 91
    const-string v3, "author_name"

    iget-object v4, v1, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseUserTimeline;->mAuthorName:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    const-string v3, "author_openid"

    iget-object v4, v1, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseUserTimeline;->mOpenID:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    const-string v3, "link_uri"

    iget-object v4, v1, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseUserTimeline;->mLinkUri:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    const-string v3, "create_at"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, v1, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseUserTimeline;->mCreatedTime:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "000"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    sget-object v3, Lcom/sec/android/app/sns3/agent/sp/qzone/db/SnsQzoneDB$UserTimeLine;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v3, v2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 106
    :cond_0
    iget-object v1, v1, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseUserTimeline;->mNext:Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseUserTimeline;

    goto :goto_0

    .line 109
    .end local v1    # "curFeed":Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseUserTimeline;
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/sns3/agent/sp/qzone/command/SnsQzCmdGetUserTimeline$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/qzone/command/SnsQzCmdGetUserTimeline;

    sget-object v4, Lcom/sec/android/app/sns3/agent/sp/qzone/db/SnsQzoneDB$UserTimeLine;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/sns3/agent/sp/qzone/command/SnsQzCmdGetUserTimeline;->setUri(Ljava/lang/String;)V

    .line 115
    :goto_1
    iget-object v3, p0, Lcom/sec/android/app/sns3/agent/sp/qzone/command/SnsQzCmdGetUserTimeline$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/qzone/command/SnsQzCmdGetUserTimeline;

    invoke-virtual {v3, p2}, Lcom/sec/android/app/sns3/agent/sp/qzone/command/SnsQzCmdGetUserTimeline;->setSuccess(Z)V

    .line 116
    iget-object v3, p0, Lcom/sec/android/app/sns3/agent/sp/qzone/command/SnsQzCmdGetUserTimeline$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/qzone/command/SnsQzCmdGetUserTimeline;

    invoke-virtual {v3, p0}, Lcom/sec/android/app/sns3/agent/sp/qzone/command/SnsQzCmdGetUserTimeline;->receive(Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;)V

    .line 118
    const/4 v3, 0x1

    return v3

    .line 111
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/sns3/agent/sp/qzone/command/SnsQzCmdGetUserTimeline$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/qzone/command/SnsQzCmdGetUserTimeline;

    new-instance v4, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;

    const-string v5, "qzone"

    invoke-direct {v4, v5, p3, p4, p5}, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;-><init>(Ljava/lang/String;IILandroid/os/Bundle;)V

    invoke-virtual {v3, v4}, Lcom/sec/android/app/sns3/agent/sp/qzone/command/SnsQzCmdGetUserTimeline;->setResponseList(Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;)Z

    .line 112
    iget-object v3, p0, Lcom/sec/android/app/sns3/agent/sp/qzone/command/SnsQzCmdGetUserTimeline$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/qzone/command/SnsQzCmdGetUserTimeline;

    invoke-virtual {v3, v6}, Lcom/sec/android/app/sns3/agent/sp/qzone/command/SnsQzCmdGetUserTimeline;->setUri(Ljava/lang/String;)V

    goto :goto_1
.end method
