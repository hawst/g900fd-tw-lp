.class public Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuth;
.super Ljava/lang/Object;
.source "SnsAccountFbAuth.java"


# static fields
.field public static final ACTION_SNS_FACEBOOK_LOGGED_IN:Ljava/lang/String; = "com.sec.android.app.sns.ACTION_SNS_FACEBOOK_LOGGED_IN"

.field public static final ACTION_SNS_FACEBOOK_LOGGED_OUT:Ljava/lang/String; = "com.sec.android.app.sns.ACTION_SNS_FACEBOOK_LOGGED_OUT"

.field public static final CALENDAR_PREFERENCE_KEY:Ljava/lang/String; = "FB_calendar_sync_interval"

.field public static final CONTACT_PREFERENCE_KEY:Ljava/lang/String; = "FB_contact_sync_interval"

.field public static final EXTRA_SKIP_SSO_NOTI:Ljava/lang/String; = "skip_sso_noti"

.field public static final EXTRA_SKIP_SYNC_ADAPTER_OF_MARKET_APP:Ljava/lang/String; = "skip_sync_marketapp"

.field public static final FACEBOOK_SSO_NOTIFICATION_ID:I = 0x44c

.field public static final GALLERY_PREFERENCE_KEY:Ljava/lang/String; = "FB_gallery_sync_interval"

.field public static final HOME_FEED_PREFERENCE_KEY:Ljava/lang/String; = "FB_home_feed_sync_interval"

.field public static final LOGIN_FACEBOOK_NOTIFICATION_ID:I = 0x4b0

.field public static final LOGIN_REQUEST_CODE:I = 0x3e8

.field public static final MARKET_APP_SSO:Z = true

.field public static final PERMISSION:[Ljava/lang/String;

.field public static final PERMISSION_EXTRA:[Ljava/lang/String;

.field public static final PROFILES_PREFERENCE_KEY:Ljava/lang/String; = "FB_profiles_sync_interval"

.field public static final PROFILE_FEED_PREFERENCE_KEY:Ljava/lang/String; = "FB_profile_feed_sync_interval"

.field public static final RETRY_LOGIN_NOTIFICATION_ID:I = 0x514

.field public static final SNS_BROADCAST_PERMISSION:Ljava/lang/String; = "com.sec.android.app.sns3.permission.RECEIVE_SNS_BROADCAST"

.field public static final STREAMS_PREFERENCE_KEY:Ljava/lang/String; = "FB_streams_sync_interval"


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 27
    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "user_birthday,user_events,friends_birthday,friends_events,friends_photos,friends_status,friends_videos,user_photos,user_status,user_videos,read_stream,publish_stream,email,read_friendlists,user_about_me,user_notes,read_mailbox,publish_checkins,xmpp_login,publish_actions"

    aput-object v1, v0, v2

    sput-object v0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuth;->PERMISSION:[Ljava/lang/String;

    .line 56
    new-array v0, v3, [Ljava/lang/String;

    const-string v1, ",user_activities,user_checkins,user_education_history,user_groups,user_hometown,user_interests,user_likes,user_location,user_relationships,user_relationship_details,user_website,user_work_history,user_online_presence,manage_friendlists,read_insights,friends_online_presence,friends_about_me,friends_activities,friends_checkins,friends_education_history,friends_groups,friends_hometown,friends_interests,friends_likes,friends_location,friends_relationships,friends_relationship_details,friends_website,friends_work_history"

    aput-object v1, v0, v2

    sput-object v0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuth;->PERMISSION_EXTRA:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
