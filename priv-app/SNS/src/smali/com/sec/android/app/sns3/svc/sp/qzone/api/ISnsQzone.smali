.class public interface abstract Lcom/sec/android/app/sns3/svc/sp/qzone/api/ISnsQzone;
.super Ljava/lang/Object;
.source "ISnsQzone.java"

# interfaces
.implements Landroid/os/IInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/qzone/api/ISnsQzone$Stub;
    }
.end annotation


# virtual methods
.method public abstract getApplicationInfo()Ljava/util/Map;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract getBirthday(Ljava/lang/String;Lcom/sec/android/app/sns3/svc/sp/qzone/api/ISnsQzoneCallbackBirthday;)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract getFeed(Ljava/lang/String;Lcom/sec/android/app/sns3/svc/sp/qzone/api/ISnsQzoneCallbackFeed;)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract getLocationPost(Landroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/qzone/api/ISnsQzoneCallbackLocationPost;)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract getNearby(Landroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/qzone/api/ISnsQzoneCallbackNearby;)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract getStatus(Ljava/lang/String;Lcom/sec/android/app/sns3/svc/sp/qzone/api/ISnsQzoneCallbackStatus;)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method
