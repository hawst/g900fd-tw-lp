.class public Lcom/sec/android/app/sns3/svc/sp/googleplus/parser/SnsGpParserFriends;
.super Ljava/lang/Object;
.source "SnsGpParserFriends.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/googleplus/parser/SnsGpParserFriends$GooglePlusFriend;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    return-void
.end method

.method public static parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFriends;
    .locals 9
    .param p0, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 40
    new-instance v3, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFriends;

    invoke-direct {v3}, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFriends;-><init>()V

    .line 42
    .local v3, "friendsObj":Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFriends;
    const/4 v2, 0x0

    .line 43
    .local v2, "friends":Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseProfile;
    const/4 v0, 0x0

    .line 46
    .local v0, "curFriends":Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseProfile;
    :try_start_0
    new-instance v5, Lorg/json/JSONObject;

    invoke-direct {v5, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 48
    .local v5, "jsonObject":Lorg/json/JSONObject;
    const-string v8, "totalItems"

    invoke-virtual {v5, v8}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v8

    if-lez v8, :cond_1

    .line 49
    const-string v8, "items"

    invoke-virtual {v5, v8}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v7

    .line 50
    .local v7, "userItems":Lorg/json/JSONArray;
    if-eqz v7, :cond_1

    .line 51
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    invoke-virtual {v7}, Lorg/json/JSONArray;->length()I

    move-result v8

    if-ge v4, v8, :cond_1

    .line 52
    invoke-virtual {v7, v4}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/sec/android/app/sns3/svc/sp/googleplus/parser/SnsGpParserFriends;->parseFriends(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseProfile;

    move-result-object v6

    .line 55
    .local v6, "newFriend":Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseProfile;
    if-nez v2, :cond_0

    .line 56
    move-object v2, v6

    .line 57
    move-object v0, v2

    .line 51
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 59
    :cond_0
    iput-object v6, v0, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseProfile;->mNext:Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseProfile;

    .line 60
    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseProfile;->mNext:Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseProfile;

    goto :goto_1

    .line 65
    .end local v4    # "i":I
    .end local v6    # "newFriend":Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseProfile;
    .end local v7    # "userItems":Lorg/json/JSONArray;
    :cond_1
    iput-object v2, v3, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFriends;->mFriends:Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseProfile;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 71
    .end local v5    # "jsonObject":Lorg/json/JSONObject;
    :goto_2
    return-object v3

    .line 67
    :catch_0
    move-exception v1

    .line 68
    .local v1, "e":Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_2
.end method

.method private static parseFriends(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseProfile;
    .locals 5
    .param p0, "response"    # Ljava/lang/String;

    .prologue
    .line 75
    new-instance v2, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseProfile;

    invoke-direct {v2}, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseProfile;-><init>()V

    .line 77
    .local v2, "profile":Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseProfile;
    :try_start_0
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 78
    .local v3, "profileObj":Lorg/json/JSONObject;
    const-string v4, "id"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v2, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseProfile;->mUserID:Ljava/lang/String;

    .line 79
    const-string v4, "displayName"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v2, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseProfile;->mUserName:Ljava/lang/String;

    .line 80
    const-string v4, "url"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v2, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseProfile;->mProfileUrl:Ljava/lang/String;

    .line 81
    const-string v4, "objectType"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v2, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseProfile;->mType:Ljava/lang/String;

    .line 83
    const-string v4, "image"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    .line 84
    .local v1, "image":Lorg/json/JSONObject;
    const-string v4, "url"

    invoke-virtual {v1, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v2, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseProfile;->mImageUrl:Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 89
    .end local v1    # "image":Lorg/json/JSONObject;
    .end local v3    # "profileObj":Lorg/json/JSONObject;
    :goto_0
    return-object v2

    .line 86
    :catch_0
    move-exception v0

    .line 87
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method
