.class public Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;
.super Landroid/accounts/AccountAuthenticatorActivity;
.source "SnsAccountFsAuthActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity$UIDialogThreadRunnable;
    }
.end annotation


# static fields
.field private static final CONNECTION_TIMEOUT:I = 0x2

.field private static final LOGIN_FAIL_BY_ANOTHER:I = 0x0

.field private static final NETWORK_UNAVAILABLE:I = 0x1

.field private static final REQUEST_SYNC_INTERVAL:I = 0x3e8

.field private static TAG:Ljava/lang/String;


# instance fields
.field private mAccessToken:Ljava/lang/String;

.field mAccountManagerCallback:Landroid/accounts/AccountManagerCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/accounts/AccountManagerCallback",
            "<",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation
.end field

.field mAccountManagerRemoveCallback:Landroid/accounts/AccountManagerCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/accounts/AccountManagerCallback",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private mAppContext:Landroid/content/Context;

.field private mCode:Ljava/lang/String;

.field private mHandler:Landroid/os/Handler;

.field private mRetryLogin:Z

.field private mWebView:Landroid/webkit/WebView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 68
    const-string v0, "SnsAccountFsAuthActivity"

    sput-object v0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 66
    invoke-direct {p0}, Landroid/accounts/AccountAuthenticatorActivity;-><init>()V

    .line 78
    iput-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;->mAppContext:Landroid/content/Context;

    .line 82
    iput-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;->mHandler:Landroid/os/Handler;

    .line 84
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;->mRetryLogin:Z

    .line 86
    iput-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;->mAccessToken:Ljava/lang/String;

    .line 88
    iput-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;->mCode:Ljava/lang/String;

    .line 90
    new-instance v0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity$1;-><init>(Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;->mAccountManagerCallback:Landroid/accounts/AccountManagerCallback;

    .line 139
    new-instance v0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity$2;-><init>(Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;->mAccountManagerRemoveCallback:Landroid/accounts/AccountManagerCallback;

    .line 578
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 66
    sget-object v0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;

    .prologue
    .line 66
    iget-boolean v0, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;->mRetryLogin:Z

    return v0
.end method

.method static synthetic access$102(Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 66
    iput-boolean p1, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;->mRetryLogin:Z

    return p1
.end method

.method static synthetic access$200(Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;->mAccessToken:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$202(Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 66
    iput-object p1, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;->mAccessToken:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$300(Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;->mAppContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;->mCode:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$402(Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 66
    iput-object p1, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;->mCode:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$500(Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;Landroid/content/Context;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;
    .param p1, "x1"    # Landroid/content/Context;
    .param p2, "x2"    # I

    .prologue
    .line 66
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;->handleError(Landroid/content/Context;I)V

    return-void
.end method

.method private checkNetwork(Landroid/content/Context;)Z
    .locals 5
    .param p1, "mAppContext"    # Landroid/content/Context;

    .prologue
    .line 614
    const-string v3, "connectivity"

    invoke-virtual {p1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/ConnectivityManager;

    .line 616
    .local v1, "cm":Landroid/net/ConnectivityManager;
    const/4 v0, 0x0

    .line 618
    .local v0, "bNetworkStatus":Z
    const/4 v2, 0x0

    .line 620
    .local v2, "netInfo":Landroid/net/NetworkInfo;
    invoke-virtual {v1}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 621
    invoke-virtual {v2}, Landroid/net/NetworkInfo;->isAvailable()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 623
    sget-object v3, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;->TAG:Ljava/lang/String;

    const-string v4, "Checked that network is available!"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 625
    invoke-virtual {v2}, Landroid/net/NetworkInfo;->isConnectedOrConnecting()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 626
    sget-object v3, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;->TAG:Ljava/lang/String;

    const-string v4, "Checked that network is now connected or connecting!"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 627
    const/4 v0, 0x1

    .line 632
    :cond_0
    return v0
.end method

.method private handleError(Landroid/content/Context;I)V
    .locals 4
    .param p1, "mContext"    # Landroid/content/Context;
    .param p2, "errorCode"    # I

    .prologue
    .line 650
    sget-object v1, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "webview errorcode="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 651
    const/4 v1, -0x8

    if-ne p2, v1, :cond_0

    .line 652
    new-instance v0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity$UIDialogThreadRunnable;

    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;->mAppContext:Landroid/content/Context;

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity$UIDialogThreadRunnable;-><init>(Landroid/content/Context;I)V

    .line 654
    .local v0, "connectionTimeout":Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity$UIDialogThreadRunnable;
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 656
    .end local v0    # "connectionTimeout":Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity$UIDialogThreadRunnable;
    :cond_0
    return-void
.end method


# virtual methods
.method public getUniqueString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 646
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public loginFail(IILjava/lang/String;)V
    .locals 4
    .param p1, "errCode"    # I
    .param p2, "subErrCode"    # I
    .param p3, "reason"    # Ljava/lang/String;

    .prologue
    .line 559
    iget-object v2, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;->mAppContext:Landroid/content/Context;

    if-eqz v2, :cond_0

    .line 564
    const-string v2, "Network unreachable"

    invoke-virtual {v2, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 565
    new-instance v1, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity$UIDialogThreadRunnable;

    iget-object v2, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;->mAppContext:Landroid/content/Context;

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity$UIDialogThreadRunnable;-><init>(Landroid/content/Context;I)V

    .line 567
    .local v1, "networkFail":Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity$UIDialogThreadRunnable;
    iget-object v2, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 576
    .end local v1    # "networkFail":Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity$UIDialogThreadRunnable;
    :cond_0
    :goto_0
    return-void

    .line 569
    :cond_1
    new-instance v0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity$UIDialogThreadRunnable;

    iget-object v2, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;->mAppContext:Landroid/content/Context;

    const/4 v3, 0x0

    invoke-direct {v0, v2, v3}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity$UIDialogThreadRunnable;-><init>(Landroid/content/Context;I)V

    .line 571
    .local v0, "another":Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity$UIDialogThreadRunnable;
    iget-object v2, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public loginSuccess(Ljava/lang/String;Ljava/lang/String;)V
    .locals 10
    .param p1, "userName"    # Ljava/lang/String;
    .param p2, "id"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 532
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 533
    .local v4, "options":Landroid/os/Bundle;
    invoke-static {}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->isLoggable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 534
    sget-object v0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SnsAccountFsAuthActivity : addAccount() :  userName = "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 537
    :cond_0
    const-string v0, "username"

    invoke-virtual {v4, v0, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 539
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;->mAppContext:Landroid/content/Context;

    if-nez v0, :cond_1

    .line 540
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    const-string v1, "com.sec.android.app.sns3.foursquare"

    move-object v3, v2

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Landroid/accounts/AccountManager;->addAccount(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Landroid/os/Bundle;Landroid/app/Activity;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    .line 548
    :goto_0
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/SnsApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    .line 549
    .local v8, "cr":Landroid/content/ContentResolver;
    new-instance v9, Landroid/content/ContentValues;

    invoke-direct {v9}, Landroid/content/ContentValues;-><init>()V

    .line 550
    .local v9, "values":Landroid/content/ContentValues;
    sget-object v0, Lcom/sec/android/app/sns3/agent/sp/foursquare/db/SnsFourSquareDB$UserFsBasicInfo;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v8, v0, v2, v2}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 551
    invoke-virtual {v9}, Landroid/content/ContentValues;->clear()V

    .line 553
    const-string v0, "user_id"

    invoke-virtual {v9, v0, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 554
    const-string v0, "formatted_name"

    invoke-virtual {v9, v0, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 555
    sget-object v0, Lcom/sec/android/app/sns3/agent/sp/foursquare/db/SnsFourSquareDB$UserFsBasicInfo;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v8, v0, v9}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 556
    return-void

    .line 543
    .end local v8    # "cr":Landroid/content/ContentResolver;
    .end local v9    # "values":Landroid/content/ContentValues;
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;->mAppContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    const-string v1, "com.sec.android.app.sns3.foursquare"

    iget-object v6, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;->mAccountManagerCallback:Landroid/accounts/AccountManagerCallback;

    move-object v3, v2

    move-object v5, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Landroid/accounts/AccountManager;->addAccount(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Landroid/os/Bundle;Landroid/app/Activity;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    goto :goto_0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "result"    # Landroid/content/Intent;

    .prologue
    .line 403
    const/16 v0, 0x3e8

    if-ne p1, v0, :cond_0

    .line 404
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;->finish()V

    .line 406
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 12
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const v11, 0x7f080021

    const/4 v8, 0x2

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 180
    invoke-static {p0}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->isLightTheme(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_0

    const v5, 0x103012b

    :goto_0
    invoke-virtual {p0, v5}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;->setTheme(I)V

    .line 183
    invoke-super {p0, p1}, Landroid/accounts/AccountAuthenticatorActivity;->onCreate(Landroid/os/Bundle;)V

    .line 185
    iput-object p0, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;->mAppContext:Landroid/content/Context;

    .line 186
    new-instance v5, Landroid/os/Handler;

    invoke-direct {v5}, Landroid/os/Handler;-><init>()V

    iput-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;->mHandler:Landroid/os/Handler;

    .line 188
    invoke-virtual {p0, v8}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;->requestWindowFeature(I)Z

    .line 190
    const v5, 0x7f080046

    new-array v6, v10, [Ljava/lang/Object;

    invoke-virtual {p0, v11}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-virtual {p0, v5, v6}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 192
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    const-string v6, "RetryLogin"

    invoke-virtual {v5, v6, v9}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    iput-boolean v5, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;->mRetryLogin:Z

    .line 194
    iget-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;->mAppContext:Landroid/content/Context;

    invoke-static {v5}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v5

    const-string v6, "com.sec.android.app.sns3.foursquare"

    invoke-virtual {v5, v6}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 197
    .local v0, "accounts":[Landroid/accounts/Account;
    array-length v5, v0

    if-lez v5, :cond_1

    iget-boolean v5, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;->mRetryLogin:Z

    if-nez v5, :cond_1

    .line 198
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    const/high16 v6, 0x7f080000

    new-array v7, v10, [Ljava/lang/Object;

    invoke-virtual {p0, v11}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;->getString(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v9

    invoke-virtual {p0, v6, v7}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6, v10}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    .line 201
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;->finish()V

    .line 400
    :goto_1
    return-void

    .line 180
    .end local v0    # "accounts":[Landroid/accounts/Account;
    :cond_0
    const v5, 0x1030128

    goto :goto_0

    .line 205
    .restart local v0    # "accounts":[Landroid/accounts/Account;
    :cond_1
    iget-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;->mAppContext:Landroid/content/Context;

    invoke-direct {p0, v5}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;->checkNetwork(Landroid/content/Context;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 206
    new-instance v2, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity$UIDialogThreadRunnable;

    iget-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;->mAppContext:Landroid/content/Context;

    invoke-direct {v2, v5, v10}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity$UIDialogThreadRunnable;-><init>(Landroid/content/Context;I)V

    .line 208
    .local v2, "networkUnavailable":Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity$UIDialogThreadRunnable;
    iget-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;->mAppContext:Landroid/content/Context;

    check-cast v5, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;

    invoke-virtual {v5, v2}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_1

    .line 212
    .end local v2    # "networkUnavailable":Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity$UIDialogThreadRunnable;
    :cond_2
    const-string v5, "notification"

    invoke-virtual {p0, v5}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/NotificationManager;

    .line 213
    .local v3, "notiMgr":Landroid/app/NotificationManager;
    const/16 v5, 0x1068

    invoke-virtual {v3, v5}, Landroid/app/NotificationManager;->cancel(I)V

    .line 214
    const/16 v5, 0x10cc

    invoke-virtual {v3, v5}, Landroid/app/NotificationManager;->cancel(I)V

    .line 216
    new-instance v5, Landroid/webkit/WebView;

    iget-object v6, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;->mAppContext:Landroid/content/Context;

    invoke-direct {v5, v6}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;)V

    iput-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;->mWebView:Landroid/webkit/WebView;

    .line 218
    iget-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v5}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v5

    invoke-virtual {v5, v9}, Landroid/webkit/WebSettings;->setAllowFileAccess(Z)V

    .line 219
    iget-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v5}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v5

    invoke-virtual {v5, v9}, Landroid/webkit/WebSettings;->setAppCacheEnabled(Z)V

    .line 220
    iget-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v5}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v5

    invoke-virtual {v5, v8}, Landroid/webkit/WebSettings;->setCacheMode(I)V

    .line 221
    iget-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v5}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v5

    invoke-virtual {v5, v9}, Landroid/webkit/WebSettings;->setDatabaseEnabled(Z)V

    .line 222
    iget-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v5}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v5

    invoke-virtual {v5, v9}, Landroid/webkit/WebSettings;->setSaveFormData(Z)V

    .line 223
    iget-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v5}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v5

    invoke-virtual {v5, v10}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 227
    iget-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;->mAppContext:Landroid/content/Context;

    invoke-static {v5}, Landroid/webkit/CookieSyncManager;->createInstance(Landroid/content/Context;)Landroid/webkit/CookieSyncManager;

    .line 228
    invoke-static {}, Landroid/webkit/CookieManager;->getInstance()Landroid/webkit/CookieManager;

    move-result-object v1

    .line 229
    .local v1, "cookieManager":Landroid/webkit/CookieManager;
    invoke-virtual {v1}, Landroid/webkit/CookieManager;->removeAllCookie()V

    .line 231
    iget-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;->mWebView:Landroid/webkit/WebView;

    new-instance v6, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity$3;

    invoke-direct {v6, p0}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity$3;-><init>(Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;)V

    invoke-virtual {v5, v6}, Landroid/webkit/WebView;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    .line 237
    iget-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;->mWebView:Landroid/webkit/WebView;

    new-instance v6, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity$4;

    invoke-direct {v6, p0}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity$4;-><init>(Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;)V

    invoke-virtual {v5, v6}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 390
    iget-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {p0, v5}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;->setContentView(Landroid/view/View;)V

    .line 392
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "https://foursquare.com/oauth2/authenticate?client_id="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFourSquare;->CLIENT_ID:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "&response_type=code"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "&redirect_uri="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "x-oauthflow-foursquare://callback"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 396
    .local v4, "url":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->isLoggable()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 397
    sget-object v5, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "loadUrl : url = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 399
    :cond_3
    iget-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v5, v4}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    goto/16 :goto_1
.end method

.method public onCreateDialog(I)Landroid/app/Dialog;
    .locals 3
    .param p1, "id"    # I

    .prologue
    const v2, 0x7f080034

    const v1, 0x7f080031

    .line 439
    packed-switch p1, :pswitch_data_0

    .line 522
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 441
    :pswitch_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f080032

    invoke-virtual {p0, v1}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity$6;

    invoke-direct {v1, p0}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity$6;-><init>(Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity$5;

    invoke-direct {v1, p0}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity$5;-><init>(Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;)V

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    .line 468
    :pswitch_1
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f080033

    invoke-virtual {p0, v1}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity$8;

    invoke-direct {v1, p0}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity$8;-><init>(Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity$7;

    invoke-direct {v1, p0}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity$7;-><init>(Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;)V

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    .line 495
    :pswitch_2
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f08001f

    invoke-virtual {p0, v1}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity$10;

    invoke-direct {v1, p0}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity$10;-><init>(Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity$9;

    invoke-direct {v1, p0}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity$9;-><init>(Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;)V

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto/16 :goto_0

    .line 439
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 527
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;->mAppContext:Landroid/content/Context;

    .line 528
    invoke-super {p0}, Landroid/accounts/AccountAuthenticatorActivity;->onDestroy()V

    .line 529
    return-void
.end method

.method public onPrepareDialog(ILandroid/app/Dialog;)V
    .locals 4
    .param p1, "id"    # I
    .param p2, "dialog"    # Landroid/app/Dialog;

    .prologue
    .line 410
    packed-switch p1, :pswitch_data_0

    .line 435
    :cond_0
    :goto_0
    return-void

    .line 412
    :pswitch_0
    instance-of v3, p2, Landroid/app/AlertDialog;

    if-eqz v3, :cond_0

    move-object v0, p2

    .line 413
    check-cast v0, Landroid/app/AlertDialog;

    .line 414
    .local v0, "alert":Landroid/app/AlertDialog;
    const v3, 0x7f080032

    invoke-virtual {p0, v3}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 419
    .end local v0    # "alert":Landroid/app/AlertDialog;
    :pswitch_1
    instance-of v3, p2, Landroid/app/AlertDialog;

    if-eqz v3, :cond_0

    move-object v1, p2

    .line 420
    check-cast v1, Landroid/app/AlertDialog;

    .line 421
    .local v1, "alert1":Landroid/app/AlertDialog;
    const v3, 0x7f080033

    invoke-virtual {p0, v3}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 426
    .end local v1    # "alert1":Landroid/app/AlertDialog;
    :pswitch_2
    instance-of v3, p2, Landroid/app/AlertDialog;

    if-eqz v3, :cond_0

    move-object v2, p2

    .line 427
    check-cast v2, Landroid/app/AlertDialog;

    .line 428
    .local v2, "timeout":Landroid/app/AlertDialog;
    const v3, 0x7f08001f

    invoke-virtual {p0, v3}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 410
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public setAuthTokenNExpires(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3
    .param p1, "accessToken"    # Ljava/lang/String;
    .param p2, "expires"    # Ljava/lang/String;

    .prologue
    .line 636
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 637
    :cond_0
    const/4 v1, 0x0

    .line 642
    :goto_0
    return v1

    .line 639
    :cond_1
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/sns3/SnsApplication;->getSvcMgr()Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/sns3/svc/SnsSvcMgr;->getTokenMgr()Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;

    move-result-object v1

    const-string v2, "foursquare"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;->getToken(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsToken;

    .line 641
    .local v0, "fsToken":Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsToken;
    invoke-virtual {v0, p1}, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsToken;->setAccessToken(Ljava/lang/String;)V

    .line 642
    const/4 v1, 0x1

    goto :goto_0
.end method
