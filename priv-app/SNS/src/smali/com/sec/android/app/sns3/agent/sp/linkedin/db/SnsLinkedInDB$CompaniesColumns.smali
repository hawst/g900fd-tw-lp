.class public interface abstract Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$CompaniesColumns;
.super Ljava/lang/Object;
.source "SnsLinkedInDB.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "CompaniesColumns"
.end annotation


# static fields
.field public static final COMPANY_ID:Ljava/lang/String; = "company_id"

.field public static final COMPANY_NAME:Ljava/lang/String; = "company_name"

.field public static final COMPANY_SIZE:Ljava/lang/String; = "company_size"

.field public static final COMPANY_TYPE:Ljava/lang/String; = "company_type"

.field public static final INDUSTRIES:Ljava/lang/String; = "industries"
