.class public Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB;
.super Ljava/lang/Object;
.source "SnsFacebookDB.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$StatusStreamColumns;,
        Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$StatusStream;,
        Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$FrndListMembersColumns;,
        Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$FrndListMembers;,
        Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$FriendsEducationColumns;,
        Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$FriendsEducation;,
        Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$FriendsWorkColumns;,
        Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$FriendsWork;,
        Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$FriendsColumns;,
        Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$Friends;,
        Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$FriendRequestNotificationsColumns;,
        Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$FriendRequestNotifications;,
        Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$MessageNotificationColumns;,
        Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$MessageNotifications;,
        Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$NotificationColumns;,
        Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$Notifications;,
        Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$ThreadColumns;,
        Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$Thread;,
        Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$PreviousSyncStateColumns;,
        Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$PreviousSyncState;,
        Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$PhotosOfUser;,
        Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$SyncColumns;,
        Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$SyncPhotoImagesColumns;,
        Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$SyncPhotoImages;,
        Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$SyncTagsColumns;,
        Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$SyncTags;,
        Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$SyncPhotoColumns;,
        Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$SyncPhoto;,
        Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$SyncAlbumColumns;,
        Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$SyncAlbum;,
        Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$SyncEventColumns;,
        Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$SyncEvent;,
        Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$NoteColumns;,
        Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$Note;,
        Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$GroupColumns;,
        Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$Group;,
        Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$TagColumns;,
        Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$Tags;,
        Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$PhotoColumns;,
        Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$Photo;,
        Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$PhotoStreamColumns;,
        Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$PhotoStream;,
        Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$AlbumColumns;,
        Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$Album;,
        Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$EventColumns;,
        Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$Event;,
        Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$LikesColumns;,
        Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$Likes;,
        Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$CommentsColumns;,
        Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$Comments;,
        Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$PostColumns;,
        Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$Post;,
        Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$BirthdayColumns;,
        Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$Birthday;,
        Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$AlbumContentsColumns;,
        Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$AlbumContents;,
        Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$FeedListColumns;,
        Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$FeedList;,
        Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$UserColumns;,
        Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$User;,
        Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$WIPE_FACEBOOK_DATA;
    }
.end annotation


# static fields
.field public static final ALBUM_CONTENTS_TABLE_NAME:Ljava/lang/String; = "album_contents"

.field public static final ALBUM_TABLE_NAME:Ljava/lang/String; = "album"

.field public static final AUTHORITY:Ljava/lang/String; = "com.sec.android.app.sns3.sp.facebook"

.field public static final BIRTHDAY_TABLE_NAME:Ljava/lang/String; = "birthday"

.field public static final COMMENTS_TABLE_NAME:Ljava/lang/String; = "comments"

.field public static final DATABASE_NAME:Ljava/lang/String; = "snsFacebookDB.db"

.field public static final DATABASE_VERSION:I = 0x19

.field public static final EDUCATION_TABLE_NAME:Ljava/lang/String; = "education"

.field public static final EVENT_TABLE_NAME:Ljava/lang/String; = "event"

.field public static final FEED_LIST_TABLE_NAME:Ljava/lang/String; = "feed_list"

.field public static final FRIENDLIST_GROUP_MEMBERS_TABLE_NAME:Ljava/lang/String; = "friendlist_group_members"

.field public static final FRIENDS_TABLE_NAME:Ljava/lang/String; = "friends"

.field public static final FRIEND_REQUEST_NOTIFICATION_TABLE_NAME:Ljava/lang/String; = "friend_request_query"

.field public static final GROUPS_TABLE_NAME:Ljava/lang/String; = "groups"

.field public static final HOME_AUTHORITY:Ljava/lang/String; = "com.sec.android.app.sns3.home"

.field public static final LIFE_AUTHORITY:Ljava/lang/String; = "com.sec.android.app.sns3.life"

.field public static final LIKES_TABLE_NAME:Ljava/lang/String; = "likes"

.field public static final MESSAGE_NOTIFICATION_TABLE_NAME:Ljava/lang/String; = "message_query"

.field public static final NOTE_TABLE_NAME:Ljava/lang/String; = "note"

.field public static final NOTIFICATION_TABLE_NAME:Ljava/lang/String; = "notification_query"

.field public static final PHOTOS_OF_USER_TABLE_NAME:Ljava/lang/String; = "photos_of_user"

.field public static final PHOTO_STREAM_TABLE_NAME:Ljava/lang/String; = "photo_stream"

.field public static final PHOTO_TABLE_NAME:Ljava/lang/String; = "photo"

.field public static final POST_TABLE_NAME:Ljava/lang/String; = "post"

.field public static final PREVIOUS_SYNC_STATE_TABLE_NAME:Ljava/lang/String; = "previous_sync_state"

.field public static final PROFILES_AUTHORITY:Ljava/lang/String; = "com.sec.android.app.sns3.profiles"

.field public static final STATUS_STREAM_TABLE_NAME:Ljava/lang/String; = "status_stream"

.field public static final STREAMS_AUTHORITY:Ljava/lang/String; = "com.sec.android.app.sns3.streams"

.field public static final SYNC_ALBUM_TABLE_NAME:Ljava/lang/String; = "sync_album"

.field public static final SYNC_EVENT_TABLE_NAME:Ljava/lang/String; = "sync_event"

.field public static final SYNC_PHOTO_IMAGES_TABLE_NAME:Ljava/lang/String; = "sync_photo_images"

.field public static final SYNC_PHOTO_TABLE_NAME:Ljava/lang/String; = "sync_photo"

.field public static final SYNC_TAGS_TABLE_NAME:Ljava/lang/String; = "sync_tags"

.field public static final TAGS_TABLE_NAME:Ljava/lang/String; = "tags"

.field public static final THREAD_TABLE_NAME:Ljava/lang/String; = "thread"

.field public static final USER_TABLE_NAME:Ljava/lang/String; = "user"

.field public static final WORK_TABLE_NAME:Ljava/lang/String; = "work"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1221
    return-void
.end method
