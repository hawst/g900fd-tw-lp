.class public Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterHomeFeedService;
.super Landroid/app/Service;
.source "SnsFbSyncAdapterHomeFeedService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterHomeFeedService$SyncAdapterImpl;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "SnsFbSync"

.field private static mUserID:Ljava/lang/String;


# instance fields
.field private mAccount:Landroid/accounts/Account;

.field private mAuthority:Ljava/lang/String;

.field private mCmdHandler:Landroid/os/Handler;

.field private mContext:Landroid/content/Context;

.field private mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

.field private mSyncAdapter:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterHomeFeedService$SyncAdapterImpl;

.field private mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;

.field private mSyncResult:Landroid/content/SyncResult;

.field private mSyncState:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 72
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterHomeFeedService;->mUserID:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 56
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 60
    iput-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterHomeFeedService;->mSyncAdapter:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterHomeFeedService$SyncAdapterImpl;

    .line 62
    iput-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterHomeFeedService;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;

    .line 64
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterHomeFeedService;->mSyncState:I

    .line 66
    iput-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterHomeFeedService;->mSyncResult:Landroid/content/SyncResult;

    .line 68
    iput-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterHomeFeedService;->mAccount:Landroid/accounts/Account;

    .line 70
    iput-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterHomeFeedService;->mAuthority:Ljava/lang/String;

    .line 78
    return-void
.end method

.method static synthetic access$002(Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterHomeFeedService;Landroid/content/Context;)Landroid/content/Context;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterHomeFeedService;
    .param p1, "x1"    # Landroid/content/Context;

    .prologue
    .line 56
    iput-object p1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterHomeFeedService;->mContext:Landroid/content/Context;

    return-object p1
.end method

.method static synthetic access$102(Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterHomeFeedService;Landroid/accounts/Account;)Landroid/accounts/Account;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterHomeFeedService;
    .param p1, "x1"    # Landroid/accounts/Account;

    .prologue
    .line 56
    iput-object p1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterHomeFeedService;->mAccount:Landroid/accounts/Account;

    return-object p1
.end method

.method static synthetic access$202(Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterHomeFeedService;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterHomeFeedService;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 56
    iput-object p1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterHomeFeedService;->mAuthority:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$302(Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterHomeFeedService;Landroid/content/SyncResult;)Landroid/content/SyncResult;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterHomeFeedService;
    .param p1, "x1"    # Landroid/content/SyncResult;

    .prologue
    .line 56
    iput-object p1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterHomeFeedService;->mSyncResult:Landroid/content/SyncResult;

    return-object p1
.end method

.method static synthetic access$400(Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterHomeFeedService;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterHomeFeedService;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/accounts/OperationCanceledException;
        }
    .end annotation

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterHomeFeedService;->performSync()V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterHomeFeedService;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterHomeFeedService;

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterHomeFeedService;->onSyncCanceled()V

    return-void
.end method

.method static synthetic access$602(Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterHomeFeedService;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterHomeFeedService;
    .param p1, "x1"    # I

    .prologue
    .line 56
    iput p1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterHomeFeedService;->mSyncState:I

    return p1
.end method

.method static synthetic access$700(Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterHomeFeedService;)Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterHomeFeedService;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterHomeFeedService;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;

    return-object v0
.end method

.method private handleSessionExpired()I
    .locals 11

    .prologue
    const/4 v9, 0x1

    const/4 v10, 0x0

    .line 218
    iget-object v7, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterHomeFeedService;->mAccount:Landroid/accounts/Account;

    const-string v8, "com.sec.android.app.sns3.home"

    invoke-static {v7, v8}, Landroid/content/ContentResolver;->cancelSync(Landroid/accounts/Account;Ljava/lang/String;)V

    .line 220
    iget-object v7, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterHomeFeedService;->mContext:Landroid/content/Context;

    const-string v8, "notification"

    invoke-virtual {v7, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/app/NotificationManager;

    move-object v4, v7

    check-cast v4, Landroid/app/NotificationManager;

    .line 223
    .local v4, "notiMgr":Landroid/app/NotificationManager;
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 225
    .local v3, "notiIntent":Landroid/content/Intent;
    const-string v7, "com.sec.android.app.sns3.RETRY_SSO_FACEBOOK"

    invoke-virtual {v3, v7}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 229
    const/high16 v7, 0x800000

    invoke-virtual {v3, v7}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 230
    const-string v7, "RetryLogin"

    invoke-virtual {v3, v7, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 231
    iget-object v7, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterHomeFeedService;->mContext:Landroid/content/Context;

    invoke-static {v7, v10, v3, v10}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 233
    .local v1, "launchIntent":Landroid/app/PendingIntent;
    const v6, 0x7f08003f

    .line 234
    .local v6, "titleID":I
    const v5, 0x7f080020

    .line 235
    .local v5, "spID":I
    const v0, 0x7f02001a

    .line 237
    .local v0, "iconID":I
    new-instance v2, Landroid/app/Notification$Builder;

    iget-object v7, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterHomeFeedService;->mContext:Landroid/content/Context;

    invoke-direct {v2, v7}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    .line 238
    .local v2, "notiBuilder":Landroid/app/Notification$Builder;
    iget-object v7, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterHomeFeedService;->mContext:Landroid/content/Context;

    new-array v8, v9, [Ljava/lang/Object;

    iget-object v9, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterHomeFeedService;->mContext:Landroid/content/Context;

    invoke-virtual {v9, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v10

    invoke-virtual {v7, v6, v8}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v7

    invoke-virtual {v7, v0}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterHomeFeedService;->mContext:Landroid/content/Context;

    const v9, 0x7f08003e

    invoke-virtual {v8, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v7

    invoke-virtual {v7, v1}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v7

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Landroid/app/Notification$Builder;->setWhen(J)Landroid/app/Notification$Builder;

    .line 242
    const/16 v7, 0x514

    invoke-virtual {v2}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v8

    invoke-virtual {v4, v7, v8}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 244
    const/4 v7, -0x1

    return v7
.end method

.method private invokeBroadcast()V
    .locals 7

    .prologue
    .line 258
    const/4 v2, 0x0

    .line 260
    .local v2, "intent":Landroid/content/Intent;
    sget-object v3, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$Post;->CONTENT_URI:Landroid/net/Uri;

    .line 261
    .local v3, "uri":Landroid/net/Uri;
    const-string v0, "com.sec.android.app.sns3.action.SYNC_FB_HOME_FEEDS"

    .line 262
    .local v0, "action":Ljava/lang/String;
    iget v4, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterHomeFeedService;->mSyncState:I

    const/4 v5, 0x2

    if-ne v4, v5, :cond_1

    const/4 v1, 0x1

    .line 264
    .local v1, "bResult":Z
    :goto_0
    new-instance v2, Landroid/content/Intent;

    .end local v2    # "intent":Landroid/content/Intent;
    invoke-direct {v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 265
    .restart local v2    # "intent":Landroid/content/Intent;
    const-string v4, "SNS3_CONTENT_URI_HOME"

    invoke-virtual {v2, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 266
    const-string v4, "SNS_RESULT"

    invoke-virtual {v2, v4, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 268
    const-string v4, "com.sec.android.app.sns3.permission.RECEIVE_SNS_BROADCAST"

    invoke-virtual {p0, v2, v4}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterHomeFeedService;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    .line 269
    invoke-static {}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->isLoggable()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 270
    const-string v4, "SnsFbSync"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "SnsFbSyncAdapterHomeFeedService - invokeBroadcast() : action = ["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "], uri = ["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "], result = ["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 273
    :cond_0
    return-void

    .line 262
    .end local v1    # "bResult":Z
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private onSyncCanceled()V
    .locals 2

    .prologue
    .line 249
    const-string v0, "SnsFbSync"

    const-string v1, "***************** SnsFbSyncAdapterHomeFeedService : onSyncCanceled !!! *****************"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 252
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterHomeFeedService;->mSyncState:I

    .line 254
    return-void
.end method

.method private performSync()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/accounts/OperationCanceledException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    .line 163
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/sns3/SnsApplication;->getAgentMgr()Lcom/sec/android/app/sns3/agent/SnsAgentMgr;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/sns3/agent/SnsAgentMgr;->getCommandMgr()Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;->getCommandMgrHandle()Landroid/os/Handler;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterHomeFeedService;->mCmdHandler:Landroid/os/Handler;

    .line 165
    const-string v3, "me"

    sput-object v3, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterHomeFeedService;->mUserID:Ljava/lang/String;

    .line 166
    iput v5, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterHomeFeedService;->mSyncState:I

    .line 167
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/sns3/SnsApplication;->getSvcMgr()Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/sns3/svc/SnsSvcMgr;->getTokenMgr()Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;

    move-result-object v3

    const-string v4, "facebook"

    invoke-virtual {v3, v4}, Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;->getToken(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbToken;

    .line 171
    .local v2, "mFbToken":Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbToken;
    :try_start_0
    invoke-virtual {v2}, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbToken;->getTokenState()I

    move-result v3

    if-eq v3, v5, :cond_0

    invoke-virtual {v2}, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbToken;->getTokenState()I

    move-result v3

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    .line 173
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterHomeFeedService;->handleSessionExpired()I

    move-result v3

    iput v3, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterHomeFeedService;->mSyncState:I

    .line 174
    new-instance v3, Ljava/lang/Exception;

    const-string v4, "Session expired or invalid!!!"

    invoke-direct {v3, v4}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 201
    :catch_0
    move-exception v1

    .line 202
    .local v1, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 203
    const-string v3, "SnsFbSync"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "SnsFbSyncAdapterHomeFeedService : EXCEPTION !!! "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 205
    iget-object v3, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterHomeFeedService;->mSyncResult:Landroid/content/SyncResult;

    iget-object v3, v3, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    const-wide/16 v4, 0x1

    iput-wide v4, v3, Landroid/content/SyncStats;->numConflictDetectedExceptions:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 209
    invoke-direct {p0}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterHomeFeedService;->invokeBroadcast()V

    .line 211
    const-string v3, "SnsFbSync"

    const-string v4, "***************** SnsFbSyncAdapterHomeFeedService : performSync - FINISHED !!! *****************"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 215
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_0
    return-void

    .line 176
    :cond_1
    :try_start_2
    new-instance v0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetHome;

    iget-object v3, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterHomeFeedService;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    iget-object v4, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterHomeFeedService;->mCmdHandler:Landroid/os/Handler;

    sget-object v5, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterHomeFeedService;->mUserID:Ljava/lang/String;

    invoke-direct {v0, v3, v4, v5}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetHome;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Handler;Ljava/lang/String;)V

    .line 177
    .local v0, "cmd":Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;
    new-instance v3, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterHomeFeedService$1;

    invoke-direct {v3, p0}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterHomeFeedService$1;-><init>(Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterHomeFeedService;)V

    invoke-virtual {v0, v3}, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->setCommandCallback(Lcom/sec/android/app/sns3/agent/command/ISnsCommandCallback;)Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;

    .line 193
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->send()I

    .line 195
    iget-object v3, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterHomeFeedService;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;

    invoke-virtual {v3}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->suspendSync()V

    .line 198
    iget v3, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterHomeFeedService;->mSyncState:I

    const/4 v4, -0x1

    if-ne v3, v4, :cond_2

    .line 199
    new-instance v3, Ljava/lang/Exception;

    const-string v4, "[SnsFbSyncAdapterHomeFeedService] updates sync is failed!!!"

    invoke-direct {v3, v4}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 209
    .end local v0    # "cmd":Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;
    :catchall_0
    move-exception v3

    invoke-direct {p0}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterHomeFeedService;->invokeBroadcast()V

    .line 211
    const-string v4, "SnsFbSync"

    const-string v5, "***************** SnsFbSyncAdapterHomeFeedService : performSync - FINISHED !!! *****************"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    throw v3

    .line 209
    .restart local v0    # "cmd":Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterHomeFeedService;->invokeBroadcast()V

    .line 211
    const-string v3, "SnsFbSync"

    const-string v4, "***************** SnsFbSyncAdapterHomeFeedService : performSync - FINISHED !!! *****************"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 132
    const-string v0, "SnsFbSync"

    const-string v1, "***************** SnsFbSyncAdapterHomeFeedService : onBind !!! *****************"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 135
    iget-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterHomeFeedService;->mSyncAdapter:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterHomeFeedService$SyncAdapterImpl;

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterHomeFeedService$SyncAdapterImpl;->getSyncAdapterBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 115
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 117
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/SnsApplication;->getSvcMgr()Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterHomeFeedService;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    .line 119
    iget-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterHomeFeedService;->mSyncAdapter:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterHomeFeedService$SyncAdapterImpl;

    if-nez v0, :cond_0

    .line 120
    new-instance v0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterHomeFeedService$SyncAdapterImpl;

    invoke-direct {v0, p0, p0}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterHomeFeedService$SyncAdapterImpl;-><init>(Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterHomeFeedService;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterHomeFeedService;->mSyncAdapter:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterHomeFeedService$SyncAdapterImpl;

    .line 123
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterHomeFeedService;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;

    if-nez v0, :cond_1

    .line 124
    new-instance v0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;

    invoke-direct {v0}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterHomeFeedService;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;

    .line 127
    :cond_1
    return-void
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 141
    const/4 v0, 0x0

    .line 142
    .local v0, "bActive":Z
    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterHomeFeedService;->mAccount:Landroid/accounts/Account;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterHomeFeedService;->mAuthority:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 143
    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterHomeFeedService;->mAccount:Landroid/accounts/Account;

    iget-object v2, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterHomeFeedService;->mAuthority:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/content/ContentResolver;->isSyncActive(Landroid/accounts/Account;Ljava/lang/String;)Z

    move-result v0

    .line 146
    :cond_0
    const-string v1, "SnsFbSync"

    const-string v2, "***************** SnsFbSyncAdapterHomeFeedService : onUnbind !!! *****************"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 149
    if-eqz v0, :cond_2

    iget v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterHomeFeedService;->mSyncState:I

    const/4 v2, 0x1

    if-eq v1, v2, :cond_1

    iget v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterHomeFeedService;->mSyncState:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_2

    .line 151
    :cond_1
    const-string v1, "SnsFbSync"

    const-string v2, "SnsFbSyncAdapterHomeFeedService : onUnbind : SYNC ERROR : performSync was stopped by forced abort or pending problem!!!"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 157
    :goto_0
    invoke-super {p0, p1}, Landroid/app/Service;->onUnbind(Landroid/content/Intent;)Z

    move-result v1

    return v1

    .line 155
    :cond_2
    const-string v1, "SnsFbSync"

    const-string v2, "SnsFbSyncAdapterHomeFeedService : onUnbind : COMPLETE STATE!!!"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
