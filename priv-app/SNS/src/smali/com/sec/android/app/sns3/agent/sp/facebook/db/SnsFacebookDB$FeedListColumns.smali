.class public interface abstract Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$FeedListColumns;
.super Ljava/lang/Object;
.source "SnsFacebookDB.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "FeedListColumns"
.end annotation


# static fields
.field public static final CAPTION:Ljava/lang/String; = "caption"

.field public static final CREATE_TIME:Ljava/lang/String; = "created_time"

.field public static final DESCRIPTION:Ljava/lang/String; = "description"

.field public static final FEED_ID:Ljava/lang/String; = "feed_id"

.field public static final FROM_ID:Ljava/lang/String; = "from_id"

.field public static final FROM_NAME:Ljava/lang/String; = "from_name"

.field public static final ICON:Ljava/lang/String; = "icon"

.field public static final LINK:Ljava/lang/String; = "link"

.field public static final LOCATION_LATITUDE:Ljava/lang/String; = "location_latitude"

.field public static final LOCATION_LONGITUDE:Ljava/lang/String; = "location_longitude"

.field public static final LOCATION_NAME:Ljava/lang/String; = "location_name"

.field public static final MESSAGE:Ljava/lang/String; = "message"

.field public static final NAME:Ljava/lang/String; = "name"

.field public static final PHOTO_ID:Ljava/lang/String; = "photo_id"

.field public static final PICTURE:Ljava/lang/String; = "picture"

.field public static final SOURCE:Ljava/lang/String; = "source"

.field public static final STATUS_TYPE:Ljava/lang/String; = "status_type"

.field public static final STORY:Ljava/lang/String; = "story"

.field public static final TYPE:Ljava/lang/String; = "type"

.field public static final UPDATE_TIME:Ljava/lang/String; = "updated_time"
