.class Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdGetHomeTimeline$1;
.super Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestStatuses;
.source "SnsTwCmdGetHomeTimeline.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdGetHomeTimeline;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Handler;Ljava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdGetHomeTimeline;


# direct methods
.method constructor <init>(Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdGetHomeTimeline;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;Landroid/os/Bundle;)V
    .locals 0
    .param p2, "x0"    # Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    .param p3, "x1"    # Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;
    .param p4, "x2"    # Landroid/os/Bundle;

    .prologue
    .line 52
    iput-object p1, p0, Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdGetHomeTimeline$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdGetHomeTimeline;

    invoke-direct {p0, p2, p3, p4}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestStatuses;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;Landroid/os/Bundle;)V

    return-void
.end method


# virtual methods
.method public onReqRespond(IZIILandroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;)Z
    .locals 5
    .param p1, "reqID"    # I
    .param p2, "bSuccess"    # Z
    .param p3, "httpStatus"    # I
    .param p4, "errorCode"    # I
    .param p5, "reason"    # Landroid/os/Bundle;
    .param p6, "status"    # Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;

    .prologue
    const/4 v3, 0x0

    .line 58
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 59
    .local v1, "values":Landroid/content/ContentValues;
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/sns3/SnsApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 61
    .local v0, "cr":Landroid/content/ContentResolver;
    if-eqz p2, :cond_2

    .line 62
    sget-object v2, Lcom/sec/android/app/sns3/agent/sp/twitter/db/SnsTwitterDB$TimeLine;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v2, v3, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 64
    :goto_0
    if-eqz p6, :cond_1

    .line 65
    invoke-virtual {v1}, Landroid/content/ContentValues;->clear()V

    .line 67
    const-string v2, "tweet_id"

    iget-object v3, p6, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;->mId:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    const-string v2, "user_name"

    iget-object v3, p6, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;->mUser:Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseUser;

    iget-object v3, v3, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseUser;->mName:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    const-string v2, "user_screen_name"

    iget-object v3, p6, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;->mUser:Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseUser;

    iget-object v3, v3, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseUser;->mScreenName:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    const-string v2, "message"

    iget-object v3, p6, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;->mText:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    const-string v2, "picture"

    iget-object v3, p6, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;->mUser:Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseUser;

    iget-object v3, v3, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseUser;->mProfileImageUrl:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    iget-object v2, p6, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;->mEntities:Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseEntities;

    iget-object v2, v2, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseEntities;->mMedia:Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseMedia;

    if-eqz v2, :cond_0

    .line 73
    const-string v2, "media_url"

    iget-object v3, p6, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;->mEntities:Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseEntities;

    iget-object v3, v3, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseEntities;->mMedia:Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseMedia;

    iget-object v3, v3, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseMedia;->mMediaUrl:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    :cond_0
    const-string v2, "create_at"

    iget-object v3, p6, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;->mCreatedAt:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    sget-object v2, Lcom/sec/android/app/sns3/agent/sp/twitter/db/SnsTwitterDB$TimeLine;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 79
    iget-object p6, p6, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;->mNext:Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;

    goto :goto_0

    .line 81
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdGetHomeTimeline$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdGetHomeTimeline;

    sget-object v3, Lcom/sec/android/app/sns3/agent/sp/twitter/db/SnsTwitterDB$TimeLine;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdGetHomeTimeline;->setUri(Ljava/lang/String;)V

    .line 89
    :goto_1
    iget-object v2, p0, Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdGetHomeTimeline$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdGetHomeTimeline;

    invoke-virtual {v2, p2}, Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdGetHomeTimeline;->setSuccess(Z)V

    .line 90
    iget-object v2, p0, Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdGetHomeTimeline$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdGetHomeTimeline;

    invoke-virtual {v2, p0}, Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdGetHomeTimeline;->receive(Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;)V

    .line 92
    const/4 v2, 0x0

    return v2

    .line 84
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdGetHomeTimeline$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdGetHomeTimeline;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdGetHomeTimeline;->setUri(Ljava/lang/String;)V

    .line 85
    iget-object v2, p0, Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdGetHomeTimeline$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdGetHomeTimeline;

    new-instance v3, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;

    const-string v4, "twitter"

    invoke-direct {v3, v4, p3, p4, p5}, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;-><init>(Ljava/lang/String;IILandroid/os/Bundle;)V

    invoke-virtual {v2, v3}, Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdGetHomeTimeline;->setResponseList(Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;)Z

    goto :goto_1
.end method
