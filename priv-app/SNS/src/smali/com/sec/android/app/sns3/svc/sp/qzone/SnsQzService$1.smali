.class Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzService$1;
.super Lcom/sec/android/app/sns3/svc/sp/qzone/api/ISnsQzone$Stub;
.source "SnsQzService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzService;


# direct methods
.method constructor <init>(Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzService;)V
    .locals 0

    .prologue
    .line 92
    iput-object p1, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzService$1;->this$0:Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzService;

    invoke-direct {p0}, Lcom/sec/android/app/sns3/svc/sp/qzone/api/ISnsQzone$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public getApplicationInfo()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 96
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 97
    .local v0, "appInfo":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v1, "app_id"

    const-string v2, "100350160"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 101
    return-object v0
.end method

.method public getBirthday(Ljava/lang/String;Lcom/sec/android/app/sns3/svc/sp/qzone/api/ISnsQzoneCallbackBirthday;)I
    .locals 3
    .param p1, "userID"    # Ljava/lang/String;
    .param p2, "cb"    # Lcom/sec/android/app/sns3/svc/sp/qzone/api/ISnsQzoneCallbackBirthday;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 156
    const-string v1, "SNS"

    const-string v2, "getBirthday CALLED!!!"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 158
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzService$1$3;

    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzService$1;->this$0:Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzService;

    # getter for: Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzService;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    invoke-static {v1}, Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzService;->access$000(Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzService;)Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v1

    invoke-direct {v0, p0, v1, p1, p2}, Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzService$1$3;-><init>(Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzService$1;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;Lcom/sec/android/app/sns3/svc/sp/qzone/api/ISnsQzoneCallbackBirthday;)V

    .line 172
    .local v0, "req":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->request()Z

    .line 174
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->getReqID()I

    move-result v1

    return v1
.end method

.method public getFeed(Ljava/lang/String;Lcom/sec/android/app/sns3/svc/sp/qzone/api/ISnsQzoneCallbackFeed;)I
    .locals 3
    .param p1, "userID"    # Ljava/lang/String;
    .param p2, "cb"    # Lcom/sec/android/app/sns3/svc/sp/qzone/api/ISnsQzoneCallbackFeed;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 106
    const-string v1, "SNS"

    const-string v2, "getFeed CALLED!!!"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 108
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzService$1$1;

    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzService$1;->this$0:Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzService;

    # getter for: Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzService;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    invoke-static {v1}, Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzService;->access$000(Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzService;)Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v1

    invoke-direct {v0, p0, v1, p1, p2}, Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzService$1$1;-><init>(Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzService$1;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;Lcom/sec/android/app/sns3/svc/sp/qzone/api/ISnsQzoneCallbackFeed;)V

    .line 125
    .local v0, "req":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->request()Z

    .line 127
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->getReqID()I

    move-result v1

    return v1
.end method

.method public getLocationPost(Landroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/qzone/api/ISnsQzoneCallbackLocationPost;)I
    .locals 3
    .param p1, "bundle"    # Landroid/os/Bundle;
    .param p2, "cb"    # Lcom/sec/android/app/sns3/svc/sp/qzone/api/ISnsQzoneCallbackLocationPost;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 181
    const-string v1, "SNS"

    const-string v2, "getLocationPost CALLED!!!"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 183
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzService$1$4;

    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzService$1;->this$0:Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzService;

    # getter for: Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzService;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    invoke-static {v1}, Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzService;->access$000(Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzService;)Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v1

    invoke-direct {v0, p0, v1, p1, p2}, Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzService$1$4;-><init>(Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzService$1;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/qzone/api/ISnsQzoneCallbackLocationPost;)V

    .line 199
    .local v0, "req":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->request()Z

    .line 201
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->getReqID()I

    move-result v1

    return v1
.end method

.method public getNearby(Landroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/qzone/api/ISnsQzoneCallbackNearby;)I
    .locals 3
    .param p1, "bundle"    # Landroid/os/Bundle;
    .param p2, "cb"    # Lcom/sec/android/app/sns3/svc/sp/qzone/api/ISnsQzoneCallbackNearby;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 208
    const-string v1, "SNS"

    const-string v2, "getLocationPost CALLED!!!"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 210
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzService$1$5;

    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzService$1;->this$0:Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzService;

    # getter for: Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzService;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    invoke-static {v1}, Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzService;->access$000(Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzService;)Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v1

    invoke-direct {v0, p0, v1, p1, p2}, Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzService$1$5;-><init>(Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzService$1;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/qzone/api/ISnsQzoneCallbackNearby;)V

    .line 226
    .local v0, "req":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->request()Z

    .line 228
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->getReqID()I

    move-result v1

    return v1
.end method

.method public getStatus(Ljava/lang/String;Lcom/sec/android/app/sns3/svc/sp/qzone/api/ISnsQzoneCallbackStatus;)I
    .locals 3
    .param p1, "userID"    # Ljava/lang/String;
    .param p2, "cb"    # Lcom/sec/android/app/sns3/svc/sp/qzone/api/ISnsQzoneCallbackStatus;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 132
    const-string v1, "SNS"

    const-string v2, "getStatus CALLED!!!"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 134
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzService$1$2;

    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzService$1;->this$0:Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzService;

    # getter for: Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzService;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    invoke-static {v1}, Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzService;->access$000(Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzService;)Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v1

    invoke-direct {v0, p0, v1, p1, p2}, Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzService$1$2;-><init>(Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzService$1;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;Lcom/sec/android/app/sns3/svc/sp/qzone/api/ISnsQzoneCallbackStatus;)V

    .line 149
    .local v0, "req":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->request()Z

    .line 151
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->getReqID()I

    move-result v1

    return v1
.end method
