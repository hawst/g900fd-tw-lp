.class public interface abstract Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserPayments$FacebookPayments;
.super Ljava/lang/Object;
.source "SnsFbParserPayments.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserPayments;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "FacebookPayments"
.end annotation


# static fields
.field public static final AMOUNT:Ljava/lang/String; = "amount"

.field public static final APPLICATION:Ljava/lang/String; = "application"

.field public static final CREATED_TIME:Ljava/lang/String; = "created_time"

.field public static final DATA:Ljava/lang/String; = "data"

.field public static final FROM:Ljava/lang/String; = "from"

.field public static final ID:Ljava/lang/String; = "id"

.field public static final NAME:Ljava/lang/String; = "name"

.field public static final STATUS:Ljava/lang/String; = "status"

.field public static final TO:Ljava/lang/String; = "to"

.field public static final UPDATED_TIME:Ljava/lang/String; = "updated_time"
