.class public interface abstract Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$ContactColumns;
.super Ljava/lang/Object;
.source "SnsLinkedInDB.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ContactColumns"
.end annotation


# static fields
.field public static final CONTACT_SUBTYPE:Ljava/lang/String; = "contact_subtype"

.field public static final CONTACT_TYPE:Ljava/lang/String; = "contact_type"

.field public static final CONTACT_VALUE:Ljava/lang/String; = "contact_value"
