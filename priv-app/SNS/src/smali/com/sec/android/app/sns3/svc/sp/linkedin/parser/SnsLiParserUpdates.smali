.class public Lcom/sec/android/app/sns3/svc/sp/linkedin/parser/SnsLiParserUpdates;
.super Ljava/lang/Object;
.source "SnsLiParserUpdates.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/linkedin/parser/SnsLiParserUpdates$1;,
        Lcom/sec/android/app/sns3/svc/sp/linkedin/parser/SnsLiParserUpdates$LinkedInUpdates;
    }
.end annotation


# static fields
.field private static TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 231
    const-string v0, "SnsLinkedIn"

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/parser/SnsLiParserUpdates;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    return-void
.end method

.method public static parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdates;
    .locals 9
    .param p0, "response"    # Ljava/lang/String;

    .prologue
    .line 235
    new-instance v1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdates;

    invoke-direct {v1}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdates;-><init>()V

    .line 236
    .local v1, "feeds":Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdates;
    if-eqz p0, :cond_3

    const-string v7, "null"

    invoke-virtual {p0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_3

    .line 238
    :try_start_0
    new-instance v7, Ljava/util/HashMap;

    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    iput-object v7, v1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdates;->mUpdates:Ljava/util/Map;

    .line 239
    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 240
    .local v4, "responseJson":Lorg/json/JSONObject;
    const-string v7, "_total"

    invoke-virtual {v4, v7}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v7

    iput v7, v1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdates;->mTotal:I

    .line 241
    iget v7, v1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdates;->mTotal:I

    if-lez v7, :cond_2

    .line 242
    const-string v7, "_count"

    invoke-virtual {v4, v7}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v7

    iput v7, v1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdates;->mCount:I

    .line 243
    iget v7, v1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdates;->mCount:I

    if-nez v7, :cond_0

    .line 244
    iget v7, v1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdates;->mTotal:I

    iput v7, v1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdates;->mCount:I

    .line 246
    :cond_0
    const-string v7, "_start"

    invoke-virtual {v4, v7}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v7

    iput v7, v1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdates;->mStart:I

    .line 247
    const-string v7, "values"

    invoke-virtual {v4, v7}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v3

    .line 249
    .local v3, "ja":Lorg/json/JSONArray;
    if-eqz v3, :cond_2

    .line 251
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v7

    if-ge v2, v7, :cond_2

    .line 252
    invoke-virtual {v3, v2}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v6

    .line 253
    .local v6, "updateItem":Lorg/json/JSONObject;
    iget-object v7, v1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdates;->mUpdates:Ljava/util/Map;

    invoke-static {v6, v7}, Lcom/sec/android/app/sns3/svc/sp/linkedin/parser/SnsLiParserUpdates;->parseUpdate(Lorg/json/JSONObject;Ljava/util/Map;)Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;

    move-result-object v5

    .line 254
    .local v5, "update":Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;
    if-eqz v5, :cond_1

    .line 255
    iget-object v7, v1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdates;->mUpdates:Ljava/util/Map;

    iget-object v8, v5, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mUpdateKey:Ljava/lang/String;

    invoke-interface {v7, v8, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 251
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 260
    .end local v2    # "i":I
    .end local v3    # "ja":Lorg/json/JSONArray;
    .end local v4    # "responseJson":Lorg/json/JSONObject;
    .end local v5    # "update":Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;
    .end local v6    # "updateItem":Lorg/json/JSONObject;
    :catch_0
    move-exception v0

    .line 261
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    .line 265
    .end local v0    # "e":Lorg/json/JSONException;
    .end local v1    # "feeds":Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdates;
    :cond_2
    :goto_1
    return-object v1

    .restart local v1    # "feeds":Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdates;
    :cond_3
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public static parseAppUpdates(Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;Lorg/json/JSONObject;)Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$AppUpdates;
    .locals 6
    .param p0, "updates"    # Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;
    .param p1, "personActivity"    # Lorg/json/JSONObject;

    .prologue
    .line 842
    new-instance v1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$AppUpdates;

    invoke-direct {v1}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$AppUpdates;-><init>()V

    .line 844
    .local v1, "appUpdates":Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$AppUpdates;
    if-eqz p1, :cond_0

    .line 847
    :try_start_0
    const-string v5, "values"

    invoke-virtual {p1, v5}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v4

    .line 849
    .local v4, "personActivityArray":Lorg/json/JSONArray;
    if-eqz v4, :cond_0

    .line 851
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v5

    if-ge v3, v5, :cond_0

    .line 853
    invoke-virtual {v4, v3}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v0

    .line 854
    .local v0, "actvityItem":Lorg/json/JSONObject;
    const-string v5, "body"

    invoke-virtual {v0, v5}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$AppUpdates;->mActivityBody:Ljava/lang/String;

    .line 856
    const-string v5, "appId"

    invoke-virtual {v0, v5}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v5

    iput v5, v1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$AppUpdates;->mAppId:I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 851
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 860
    .end local v0    # "actvityItem":Lorg/json/JSONObject;
    .end local v3    # "i":I
    .end local v4    # "personActivityArray":Lorg/json/JSONArray;
    :catch_0
    move-exception v2

    .line 861
    .local v2, "e":Lorg/json/JSONException;
    invoke-virtual {v2}, Lorg/json/JSONException;->printStackTrace()V

    .line 864
    .end local v2    # "e":Lorg/json/JSONException;
    :cond_0
    return-object v1
.end method

.method private static parseComments(Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;Lorg/json/JSONObject;)Ljava/util/List;
    .locals 10
    .param p0, "update"    # Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;
    .param p1, "commentsObject"    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;",
            "Lorg/json/JSONObject;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Comment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 714
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 716
    .local v0, "CommentsList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Comment;>;"
    if-eqz p1, :cond_0

    const-string v7, "_total"

    invoke-virtual {p1, v7}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v7

    if-lez v7, :cond_0

    .line 718
    :try_start_0
    const-string v7, "_total"

    invoke-virtual {p1, v7}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v7

    iput v7, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mNumComments:I

    .line 719
    const-string v7, "values"

    invoke-virtual {p1, v7}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v5

    .line 721
    .local v5, "ja":Lorg/json/JSONArray;
    if-eqz v5, :cond_0

    .line 723
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    invoke-virtual {v5}, Lorg/json/JSONArray;->length()I

    move-result v7

    if-ge v4, v7, :cond_0

    .line 724
    invoke-virtual {v5, v4}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v2

    .line 725
    .local v2, "commentItem":Lorg/json/JSONObject;
    new-instance v1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Comment;

    invoke-direct {v1}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Comment;-><init>()V

    .line 726
    .local v1, "comment":Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Comment;
    const-string v7, "id"

    invoke-virtual {v2, v7}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v8

    iput-wide v8, v1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Comment;->mId:J

    .line 727
    const-string v7, "comment"

    invoke-virtual {v2, v7}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Comment;->mCommentText:Ljava/lang/String;

    .line 728
    const-string v7, "sequenceNumber"

    invoke-virtual {v2, v7}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v7

    iput v7, v1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Comment;->mSequenceNumber:I

    .line 730
    const-string v7, "timestamp"

    invoke-virtual {v2, v7}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v8

    iput-wide v8, v1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Comment;->mTimeStamp:J

    .line 731
    const-string v7, "person"

    invoke-virtual {v2, v7}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v7

    invoke-static {p0, v7}, Lcom/sec/android/app/sns3/svc/sp/linkedin/parser/SnsLiParserUpdates;->parsePerson(Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;Lorg/json/JSONObject;)Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;

    move-result-object v6

    .line 733
    .local v6, "person":Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;
    iput-object v6, v1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Comment;->mCommentor:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;

    .line 735
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 723
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 740
    .end local v1    # "comment":Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Comment;
    .end local v2    # "commentItem":Lorg/json/JSONObject;
    .end local v4    # "i":I
    .end local v5    # "ja":Lorg/json/JSONArray;
    .end local v6    # "person":Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;
    :catch_0
    move-exception v3

    .line 741
    .local v3, "e":Lorg/json/JSONException;
    invoke-virtual {v3}, Lorg/json/JSONException;->printStackTrace()V

    .line 744
    .end local v3    # "e":Lorg/json/JSONException;
    :cond_0
    return-object v0
.end method

.method public static parseContactDetails(Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;Lorg/json/JSONObject;)Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$ContactDetails;
    .locals 18
    .param p0, "updates"    # Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;
    .param p1, "personObject"    # Lorg/json/JSONObject;

    .prologue
    .line 749
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    .line 750
    .local v13, "sb":Ljava/lang/StringBuilder;
    if-nez p1, :cond_0

    .line 751
    const/4 v2, 0x0

    .line 835
    :goto_0
    return-object v2

    .line 752
    :cond_0
    new-instance v2, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$ContactDetails;

    invoke-direct {v2}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$ContactDetails;-><init>()V

    .line 754
    .local v2, "contact":Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$ContactDetails;
    :try_start_0
    const-string v17, "phoneNumbers"

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v10

    .line 755
    .local v10, "phoneNumber":Lorg/json/JSONObject;
    if-eqz v10, :cond_2

    .line 756
    const-string v17, "values"

    move-object/from16 v0, v17

    invoke-virtual {v10, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v12

    .line 757
    .local v12, "phoneinfoList":Lorg/json/JSONArray;
    const-string v17, "PhoneNumber"

    move-object/from16 v0, v17

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 758
    const/4 v8, 0x0

    .local v8, "k":I
    :goto_1
    invoke-virtual {v12}, Lorg/json/JSONArray;->length()I

    move-result v17

    move/from16 v0, v17

    if-ge v8, v0, :cond_2

    .line 759
    invoke-virtual {v12, v8}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v11

    .line 760
    .local v11, "phoneinfo":Lorg/json/JSONObject;
    if-eqz v11, :cond_1

    .line 761
    const-string v17, "phoneNumber"

    move-object/from16 v0, v17

    invoke-virtual {v11, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    iput-object v0, v2, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$ContactDetails;->mPhoneNumber:Ljava/lang/String;

    .line 762
    const-string v17, "phoneType"

    move-object/from16 v0, v17

    invoke-virtual {v11, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    iput-object v0, v2, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$ContactDetails;->mPhoneNumberType:Ljava/lang/String;

    .line 758
    :cond_1
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    .line 770
    .end local v8    # "k":I
    .end local v11    # "phoneinfo":Lorg/json/JSONObject;
    .end local v12    # "phoneinfoList":Lorg/json/JSONArray;
    :cond_2
    const-string v17, "imAccounts"

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v5

    .line 771
    .local v5, "imAccount":Lorg/json/JSONObject;
    if-eqz v5, :cond_4

    .line 772
    const-string v17, "values"

    move-object/from16 v0, v17

    invoke-virtual {v5, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v7

    .line 773
    .local v7, "imAccountinfoList":Lorg/json/JSONArray;
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->length()I

    move-result v17

    if-lez v17, :cond_3

    .line 774
    const-string v17, ","

    move-object/from16 v0, v17

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 775
    const-string v17, "IMAccount"

    move-object/from16 v0, v17

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 778
    :cond_3
    const/4 v8, 0x0

    .restart local v8    # "k":I
    :goto_2
    invoke-virtual {v7}, Lorg/json/JSONArray;->length()I

    move-result v17

    move/from16 v0, v17

    if-ge v8, v0, :cond_4

    .line 779
    invoke-virtual {v7, v8}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v6

    .line 780
    .local v6, "imAccountinfo":Lorg/json/JSONObject;
    const-string v17, "imAccountName"

    move-object/from16 v0, v17

    invoke-virtual {v6, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    iput-object v0, v2, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$ContactDetails;->mImAccountName:Ljava/lang/String;

    .line 782
    const-string v17, "imAccountType"

    move-object/from16 v0, v17

    invoke-virtual {v6, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    iput-object v0, v2, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$ContactDetails;->mImAccountType:Ljava/lang/String;

    .line 778
    add-int/lit8 v8, v8, 0x1

    goto :goto_2

    .line 788
    .end local v6    # "imAccountinfo":Lorg/json/JSONObject;
    .end local v7    # "imAccountinfoList":Lorg/json/JSONArray;
    .end local v8    # "k":I
    :cond_4
    const-string v17, "twitterAccounts"

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v14

    .line 790
    .local v14, "twitterAccount":Lorg/json/JSONObject;
    if-eqz v14, :cond_6

    .line 791
    const-string v17, "values"

    move-object/from16 v0, v17

    invoke-virtual {v14, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v16

    .line 793
    .local v16, "twitterAccountinfoList":Lorg/json/JSONArray;
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->length()I

    move-result v17

    if-lez v17, :cond_5

    .line 794
    const-string v17, ","

    move-object/from16 v0, v17

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 795
    const-string v17, "TwitterAccount"

    move-object/from16 v0, v17

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 798
    :cond_5
    const/4 v8, 0x0

    .restart local v8    # "k":I
    :goto_3
    invoke-virtual/range {v16 .. v16}, Lorg/json/JSONArray;->length()I

    move-result v17

    move/from16 v0, v17

    if-ge v8, v0, :cond_6

    .line 799
    move-object/from16 v0, v16

    invoke-virtual {v0, v8}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v15

    .line 800
    .local v15, "twitterAccountinfo":Lorg/json/JSONObject;
    const-string v17, "providerAccountId"

    move-object/from16 v0, v17

    invoke-virtual {v15, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    iput-object v0, v2, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$ContactDetails;->mProviderAccountId:Ljava/lang/String;

    .line 802
    const-string v17, "providerAccountName"

    move-object/from16 v0, v17

    invoke-virtual {v15, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    iput-object v0, v2, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$ContactDetails;->mProviderAccountName:Ljava/lang/String;

    .line 798
    add-int/lit8 v8, v8, 0x1

    goto :goto_3

    .line 808
    .end local v8    # "k":I
    .end local v15    # "twitterAccountinfo":Lorg/json/JSONObject;
    .end local v16    # "twitterAccountinfoList":Lorg/json/JSONArray;
    :cond_6
    const-string v17, "mainAddress"

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v9

    .line 809
    .local v9, "mainAddress":Lorg/json/JSONObject;
    if-eqz v9, :cond_7

    .line 810
    const-string v17, "mainAddress"

    move-object/from16 v0, v17

    invoke-virtual {v9, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    iput-object v0, v2, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$ContactDetails;->mMainAddress:Ljava/lang/String;

    .line 811
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->length()I

    move-result v17

    if-lez v17, :cond_7

    .line 812
    const-string v17, ","

    move-object/from16 v0, v17

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 813
    const-string v17, "MainAddress"

    move-object/from16 v0, v17

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 817
    :cond_7
    const-string v17, "dateOfBirth"

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v3

    .line 818
    .local v3, "dateofbirth":Lorg/json/JSONObject;
    if-eqz v3, :cond_8

    .line 819
    const-string v17, "day"

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    iput-object v0, v2, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$ContactDetails;->mDateOfBirthDay:Ljava/lang/String;

    .line 820
    const-string v17, "month"

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    iput-object v0, v2, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$ContactDetails;->mDateOfBirthMonth:Ljava/lang/String;

    .line 822
    const-string v17, "year"

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    iput-object v0, v2, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$ContactDetails;->mDateOfBirthYear:Ljava/lang/String;

    .line 824
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->length()I

    move-result v17

    if-lez v17, :cond_8

    .line 825
    const-string v17, ","

    move-object/from16 v0, v17

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 826
    const-string v17, "DateOfBirth"

    move-object/from16 v0, v17

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 830
    :cond_8
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mUpdatedFields:Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 831
    .end local v3    # "dateofbirth":Lorg/json/JSONObject;
    .end local v5    # "imAccount":Lorg/json/JSONObject;
    .end local v9    # "mainAddress":Lorg/json/JSONObject;
    .end local v10    # "phoneNumber":Lorg/json/JSONObject;
    .end local v14    # "twitterAccount":Lorg/json/JSONObject;
    :catch_0
    move-exception v4

    .line 832
    .local v4, "e":Lorg/json/JSONException;
    invoke-virtual {v4}, Lorg/json/JSONException;->printStackTrace()V

    goto/16 :goto_0
.end method

.method private static parseJob(Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;Lorg/json/JSONObject;)Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$JobPost;
    .locals 5
    .param p0, "update"    # Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;
    .param p1, "jobObject"    # Lorg/json/JSONObject;

    .prologue
    .line 600
    new-instance v1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$JobPost;

    invoke-direct {v1}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$JobPost;-><init>()V

    .line 602
    .local v1, "job":Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$JobPost;
    :try_start_0
    const-string v3, "id"

    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$JobPost;->mJobId:Ljava/lang/String;

    .line 604
    const-string v3, "position"

    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v3

    const-string v4, "title"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$JobPost;->mPosition:Ljava/lang/String;

    .line 606
    const-string v3, "company"

    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v3

    const-string v4, "name"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$JobPost;->mCompanyName:Ljava/lang/String;

    .line 608
    const-string v3, "jobPoster"

    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v3

    invoke-static {p0, v3}, Lcom/sec/android/app/sns3/svc/sp/linkedin/parser/SnsLiParserUpdates;->parsePerson(Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;Lorg/json/JSONObject;)Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;

    move-result-object v2

    .line 610
    .local v2, "jobPoster":Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;
    if-eqz v2, :cond_0

    .line 611
    iput-object v2, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mConnection:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;

    .line 612
    :cond_0
    const-string v3, "siteJobRequest"

    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v3

    const-string v4, "url"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$JobPost;->mJobUrl:Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 617
    .end local v2    # "jobPoster":Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;
    :goto_0
    return-object v1

    .line 614
    :catch_0
    move-exception v0

    .line 615
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public static parseLikes(Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;Lorg/json/JSONObject;)Ljava/util/List;
    .locals 7
    .param p0, "updates"    # Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;
    .param p1, "likesObject"    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;",
            "Lorg/json/JSONObject;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;",
            ">;"
        }
    .end annotation

    .prologue
    .line 687
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 688
    .local v0, "LikesList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;>;"
    if-eqz p1, :cond_0

    const-string v5, "_total"

    invoke-virtual {p1, v5}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v5

    if-lez v5, :cond_0

    .line 691
    :try_start_0
    const-string v5, "values"

    invoke-virtual {p1, v5}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v3

    .line 693
    .local v3, "ja":Lorg/json/JSONArray;
    if-eqz v3, :cond_0

    .line 695
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v5

    if-ge v2, v5, :cond_0

    .line 697
    invoke-virtual {v3, v2}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v5

    const-string v6, "person"

    invoke-virtual {v5, v6}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v5

    invoke-static {p0, v5}, Lcom/sec/android/app/sns3/svc/sp/linkedin/parser/SnsLiParserUpdates;->parsePerson(Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;Lorg/json/JSONObject;)Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;

    move-result-object v4

    .line 699
    .local v4, "person":Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;
    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 695
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 704
    .end local v2    # "i":I
    .end local v3    # "ja":Lorg/json/JSONArray;
    .end local v4    # "person":Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;
    :catch_0
    move-exception v1

    .line 705
    .local v1, "e":Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    .line 708
    .end local v1    # "e":Lorg/json/JSONException;
    :cond_0
    return-object v0
.end method

.method public static parsePerson(Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;Lorg/json/JSONObject;)Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;
    .locals 5
    .param p0, "update"    # Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;
    .param p1, "personObject"    # Lorg/json/JSONObject;

    .prologue
    .line 660
    if-nez p1, :cond_1

    .line 661
    const/4 v1, 0x0

    .line 681
    :cond_0
    :goto_0
    return-object v1

    .line 662
    :cond_1
    new-instance v1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;

    invoke-direct {v1}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;-><init>()V

    .line 665
    .local v1, "person":Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;
    :try_start_0
    const-string v3, "id"

    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;->mId:Ljava/lang/String;

    .line 666
    const-string v3, "private"

    iget-object v4, v1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;->mId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 667
    new-instance v3, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$PrivateProfileException;

    const-string v4, "Person\'s details are not accessible"

    invoke-direct {v3, v4}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$PrivateProfileException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 677
    :catch_0
    move-exception v0

    .line 678
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0

    .line 668
    .end local v0    # "e":Lorg/json/JSONException;
    :cond_2
    :try_start_1
    const-string v3, "firstName"

    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;->mFirstName:Ljava/lang/String;

    .line 669
    const-string v3, "lastName"

    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;->mLastName:Ljava/lang/String;

    .line 670
    const-string v3, "headline"

    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;->mHeadline:Ljava/lang/String;

    .line 671
    const-string v3, "pictureUrl"

    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;->mPictureUrl:Ljava/lang/String;

    .line 672
    const-string v3, "siteStandardProfileRequest"

    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    .line 674
    .local v2, "profileUrlObj":Lorg/json/JSONObject;
    if-eqz v2, :cond_0

    .line 675
    const-string v3, "url"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;->mPublicProfileUrl:Ljava/lang/String;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method private static parseShare(Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;Lorg/json/JSONObject;)Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Share;
    .locals 8
    .param p0, "update"    # Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;
    .param p1, "shareObject"    # Lorg/json/JSONObject;

    .prologue
    .line 621
    if-nez p1, :cond_0

    .line 622
    const/4 v2, 0x0

    .line 655
    :goto_0
    return-object v2

    .line 623
    :cond_0
    new-instance v2, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Share;

    invoke-direct {v2}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Share;-><init>()V

    .line 625
    .local v2, "currentShare":Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Share;
    :try_start_0
    const-string v5, "id"

    invoke-virtual {p1, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v2, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Share;->mId:Ljava/lang/String;

    .line 626
    const-string v5, "timestamp"

    invoke-virtual {p1, v5}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v6

    iput-wide v6, v2, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Share;->mTimeStamp:J

    .line 628
    const-string v5, "visibility"

    invoke-virtual {p1, v5}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v5

    const-string v6, "code"

    invoke-virtual {v5, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v2, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Share;->mVisiblity:Ljava/lang/String;

    .line 630
    const-string v5, "comment"

    invoke-virtual {p1, v5}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v2, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Share;->mComment:Ljava/lang/String;

    .line 632
    const-string v5, "content"

    invoke-virtual {p1, v5}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    .line 633
    .local v1, "contentObject":Lorg/json/JSONObject;
    if-nez v1, :cond_1

    .line 634
    const-string v5, "updateContent"

    invoke-virtual {p1, v5}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    .line 636
    :cond_1
    if-eqz v1, :cond_2

    .line 637
    const-string v5, "submittedUrl"

    invoke-virtual {v1, v5}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v2, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Share;->mSubmittedUrl:Ljava/lang/String;

    .line 638
    const-string v5, "thumbnailUrl"

    invoke-virtual {v1, v5}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v2, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Share;->mThumbnailUrl:Ljava/lang/String;

    .line 639
    const-string v5, "title"

    invoke-virtual {v1, v5}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v2, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Share;->mTitle:Ljava/lang/String;

    .line 641
    :cond_2
    const-string v5, "source"

    invoke-virtual {p1, v5}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    .line 642
    .local v4, "sourceObject":Lorg/json/JSONObject;
    const-string v5, "serviceProvider"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v5

    const-string v6, "name"

    invoke-virtual {v5, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v2, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Share;->mSourceSPName:Ljava/lang/String;

    .line 644
    const-string v5, "application"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 645
    .local v0, "appObj":Lorg/json/JSONObject;
    if-eqz v0, :cond_3

    .line 646
    const-string v5, "name"

    invoke-virtual {v0, v5}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v2, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Share;->mSourceApplication:Ljava/lang/String;

    .line 648
    :cond_3
    const-string v5, "author"

    invoke-virtual {p1, v5}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v5

    invoke-static {p0, v5}, Lcom/sec/android/app/sns3/svc/sp/linkedin/parser/SnsLiParserUpdates;->parsePerson(Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;Lorg/json/JSONObject;)Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;

    move-result-object v5

    iput-object v5, v2, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Share;->mAuthor:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 651
    .end local v0    # "appObj":Lorg/json/JSONObject;
    .end local v1    # "contentObject":Lorg/json/JSONObject;
    .end local v4    # "sourceObject":Lorg/json/JSONObject;
    :catch_0
    move-exception v3

    .line 652
    .local v3, "e":Lorg/json/JSONException;
    invoke-virtual {v3}, Lorg/json/JSONException;->printStackTrace()V

    goto/16 :goto_0
.end method

.method private static parseUpdate(Lorg/json/JSONObject;Ljava/util/Map;)Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;
    .locals 53
    .param p0, "updateItem"    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/json/JSONObject;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;",
            ">;)",
            "Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;"
        }
    .end annotation

    .prologue
    .line 270
    .local p1, "mUpdates":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;>;"
    const/16 v41, 0x0

    .line 273
    .local v41, "update":Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;
    :try_start_0
    const-string v49, "updateKey"

    move-object/from16 v0, p0

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v45

    .line 274
    .local v45, "updateKey":Ljava/lang/String;
    move-object/from16 v0, p1

    move-object/from16 v1, v45

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v49

    move-object/from16 v0, v49

    check-cast v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;

    move-object/from16 v41, v0

    .line 275
    if-eqz v41, :cond_0

    .line 276
    move-object/from16 v0, v41

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mUpdateType:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$UpdateType;

    move-object/from16 v49, v0

    sget-object v50, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$UpdateType;->CONN:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$UpdateType;

    move-object/from16 v0, v49

    move-object/from16 v1, v50

    if-eq v0, v1, :cond_1

    .line 277
    const/16 v49, 0x0

    .line 595
    .end local v45    # "updateKey":Ljava/lang/String;
    :goto_0
    return-object v49

    .line 279
    .restart local v45    # "updateKey":Ljava/lang/String;
    :cond_0
    new-instance v42, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;

    invoke-direct/range {v42 .. v42}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;-><init>()V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$PrivateProfileException; {:try_start_0 .. :try_end_0} :catch_1

    .line 280
    .end local v41    # "update":Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;
    .local v42, "update":Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;
    :try_start_1
    new-instance v49, Ljava/util/ArrayList;

    invoke-direct/range {v49 .. v49}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, v49

    move-object/from16 v1, v42

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mNewConnections:Ljava/util/List;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$PrivateProfileException; {:try_start_1 .. :try_end_1} :catch_4

    move-object/from16 v41, v42

    .line 282
    .end local v42    # "update":Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;
    .restart local v41    # "update":Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;
    :cond_1
    :try_start_2
    const-string v49, "isCommentable"

    move-object/from16 v0, p0

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v49

    move/from16 v0, v49

    move-object/from16 v1, v41

    iput-boolean v0, v1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->isCommentable:Z

    .line 283
    const-string v49, "isLikable"

    move-object/from16 v0, p0

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v49

    move/from16 v0, v49

    move-object/from16 v1, v41

    iput-boolean v0, v1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->isLikable:Z

    .line 284
    move-object/from16 v0, v41

    iget-boolean v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->isLikable:Z

    move/from16 v49, v0

    if-eqz v49, :cond_5

    const-string v49, "isLiked"

    move-object/from16 v0, p0

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v49

    :goto_1
    move/from16 v0, v49

    move-object/from16 v1, v41

    iput-boolean v0, v1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->isLiked:Z

    .line 286
    move-object/from16 v0, v41

    iget-boolean v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->isLikable:Z

    move/from16 v49, v0

    if-eqz v49, :cond_6

    const-string v49, "numLikes"

    move-object/from16 v0, p0

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v49

    :goto_2
    move/from16 v0, v49

    move-object/from16 v1, v41

    iput v0, v1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mNumLikes:I

    .line 288
    const-string v49, "timestamp"

    move-object/from16 v0, p0

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v50

    move-wide/from16 v0, v50

    move-object/from16 v2, v41

    iput-wide v0, v2, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mTimeStamp:J

    .line 290
    move-object/from16 v0, v45

    move-object/from16 v1, v41

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mUpdateKey:Ljava/lang/String;

    .line 291
    const-string v49, "updateType"

    move-object/from16 v0, p0

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v49

    invoke-static/range {v49 .. v49}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$UpdateType;->parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$UpdateType;

    move-result-object v49

    move-object/from16 v0, v49

    move-object/from16 v1, v41

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mUpdateType:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$UpdateType;

    .line 294
    const-string v49, "updateContent"

    move-object/from16 v0, p0

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v44

    .line 295
    .local v44, "updateContent":Lorg/json/JSONObject;
    const-string v49, "person"

    move-object/from16 v0, v44

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v26

    .line 296
    .local v26, "personObject":Lorg/json/JSONObject;
    move-object/from16 v0, v41

    move-object/from16 v1, v26

    invoke-static {v0, v1}, Lcom/sec/android/app/sns3/svc/sp/linkedin/parser/SnsLiParserUpdates;->parsePerson(Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;Lorg/json/JSONObject;)Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;

    move-result-object v49

    move-object/from16 v0, v49

    move-object/from16 v1, v41

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mConnection:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;

    .line 298
    sget-object v49, Lcom/sec/android/app/sns3/svc/sp/linkedin/parser/SnsLiParserUpdates$1;->$SwitchMap$com$sec$android$app$sns3$svc$sp$linkedin$response$SnsLiResponseUpdate$UpdateType:[I

    move-object/from16 v0, v41

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mUpdateType:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$UpdateType;

    move-object/from16 v50, v0

    invoke-virtual/range {v50 .. v50}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$UpdateType;->ordinal()I

    move-result v50

    aget v49, v49, v50

    packed-switch v49, :pswitch_data_0

    .line 571
    sget-object v49, Lcom/sec/android/app/sns3/svc/sp/linkedin/parser/SnsLiParserUpdates;->TAG:Ljava/lang/String;

    new-instance v50, Ljava/lang/StringBuilder;

    invoke-direct/range {v50 .. v50}, Ljava/lang/StringBuilder;-><init>()V

    const-string v51, "[SnsLiParserUpdates] parseUpdate Invalid update type: "

    invoke-virtual/range {v50 .. v51}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v50

    move-object/from16 v0, v41

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mUpdateType:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$UpdateType;

    move-object/from16 v51, v0

    invoke-virtual/range {v50 .. v51}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v50

    invoke-virtual/range {v50 .. v50}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v50

    invoke-static/range {v49 .. v50}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 577
    :cond_2
    :goto_3
    move-object/from16 v0, v41

    iget-boolean v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->isCommentable:Z

    move/from16 v49, v0

    if-eqz v49, :cond_3

    .line 578
    const-string v49, "updateComments"

    move-object/from16 v0, p0

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v49

    move-object/from16 v0, v41

    move-object/from16 v1, v49

    invoke-static {v0, v1}, Lcom/sec/android/app/sns3/svc/sp/linkedin/parser/SnsLiParserUpdates;->parseComments(Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;Lorg/json/JSONObject;)Ljava/util/List;

    move-result-object v49

    move-object/from16 v0, v49

    move-object/from16 v1, v41

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mComments:Ljava/util/List;

    .line 582
    :cond_3
    move-object/from16 v0, v41

    iget-boolean v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->isLikable:Z

    move/from16 v49, v0

    if-eqz v49, :cond_4

    move-object/from16 v0, v41

    iget v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mNumLikes:I

    move/from16 v49, v0

    if-lez v49, :cond_4

    .line 584
    const-string v49, "likes"

    move-object/from16 v0, p0

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v49

    move-object/from16 v0, v41

    move-object/from16 v1, v49

    invoke-static {v0, v1}, Lcom/sec/android/app/sns3/svc/sp/linkedin/parser/SnsLiParserUpdates;->parseLikes(Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;Lorg/json/JSONObject;)Ljava/util/List;

    move-result-object v49

    move-object/from16 v0, v49

    move-object/from16 v1, v41

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mLikes:Ljava/util/List;

    .end local v26    # "personObject":Lorg/json/JSONObject;
    .end local v44    # "updateContent":Lorg/json/JSONObject;
    .end local v45    # "updateKey":Ljava/lang/String;
    :cond_4
    :goto_4
    move-object/from16 v49, v41

    .line 595
    goto/16 :goto_0

    .line 284
    .restart local v45    # "updateKey":Ljava/lang/String;
    :cond_5
    const/16 v49, 0x0

    goto/16 :goto_1

    .line 286
    :cond_6
    const/16 v49, -0x1

    goto/16 :goto_2

    .line 300
    .restart local v26    # "personObject":Lorg/json/JSONObject;
    .restart local v44    # "updateContent":Lorg/json/JSONObject;
    :pswitch_0
    const-string v49, "connections"

    move-object/from16 v0, v26

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v23

    .line 302
    .local v23, "newConnections":Lorg/json/JSONObject;
    const-string v49, "values"

    move-object/from16 v0, v23

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v11

    .line 304
    .local v11, "connectionsList":Lorg/json/JSONArray;
    const/16 v21, 0x0

    .local v21, "j":I
    :goto_5
    invoke-virtual {v11}, Lorg/json/JSONArray;->length()I

    move-result v49

    move/from16 v0, v21

    move/from16 v1, v49

    if-ge v0, v1, :cond_2

    .line 305
    move-object/from16 v0, v41

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mNewConnections:Ljava/util/List;

    move-object/from16 v49, v0

    move/from16 v0, v21

    invoke-virtual {v11, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v50

    move-object/from16 v0, v41

    move-object/from16 v1, v50

    invoke-static {v0, v1}, Lcom/sec/android/app/sns3/svc/sp/linkedin/parser/SnsLiParserUpdates;->parsePerson(Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;Lorg/json/JSONObject;)Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;

    move-result-object v50

    invoke-interface/range {v49 .. v50}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 304
    add-int/lit8 v21, v21, 0x1

    goto :goto_5

    .line 310
    .end local v11    # "connectionsList":Lorg/json/JSONArray;
    .end local v21    # "j":I
    .end local v23    # "newConnections":Lorg/json/JSONObject;
    :pswitch_1
    const-string v49, "currentStatus"

    move-object/from16 v0, v26

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v49

    move-object/from16 v0, v49

    move-object/from16 v1, v41

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mCurrentStatus:Ljava/lang/String;
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$PrivateProfileException; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_3

    .line 588
    .end local v26    # "personObject":Lorg/json/JSONObject;
    .end local v44    # "updateContent":Lorg/json/JSONObject;
    .end local v45    # "updateKey":Ljava/lang/String;
    :catch_0
    move-exception v12

    .line 589
    .local v12, "e":Lorg/json/JSONException;
    :goto_6
    invoke-virtual {v12}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_4

    .line 314
    .end local v12    # "e":Lorg/json/JSONException;
    .restart local v26    # "personObject":Lorg/json/JSONObject;
    .restart local v44    # "updateContent":Lorg/json/JSONObject;
    .restart local v45    # "updateKey":Ljava/lang/String;
    :pswitch_2
    :try_start_3
    const-string v49, "updatedFields"

    move-object/from16 v0, p0

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v47

    .line 316
    .local v47, "updatedFieldObject":Lorg/json/JSONObject;
    if-eqz v47, :cond_2

    .line 317
    const-string v49, "values"

    move-object/from16 v0, v47

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v48

    .line 319
    .local v48, "updatedFields":Lorg/json/JSONArray;
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    .line 320
    .local v18, "fields":Ljava/lang/StringBuilder;
    const/16 v17, 0x0

    .local v17, "fieldIndex":I
    :goto_7
    invoke-virtual/range {v48 .. v48}, Lorg/json/JSONArray;->length()I

    move-result v49

    move/from16 v0, v17

    move/from16 v1, v49

    if-ge v0, v1, :cond_8

    .line 321
    move-object/from16 v0, v48

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v49

    const-string v50, "name"

    invoke-virtual/range {v49 .. v50}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 323
    .local v16, "field":Ljava/lang/String;
    if-eqz v16, :cond_7

    const-string v49, "/"

    move-object/from16 v0, v16

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v49

    if-eqz v49, :cond_7

    .line 324
    const-string v49, "/"

    move-object/from16 v0, v16

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v49

    add-int/lit8 v49, v49, 0x1

    move-object/from16 v0, v16

    move/from16 v1, v49

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v16

    .line 325
    :cond_7
    move-object/from16 v0, v18

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    const-string v50, ","

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 320
    add-int/lit8 v17, v17, 0x1

    goto :goto_7

    .line 327
    .end local v16    # "field":Ljava/lang/String;
    :cond_8
    const-string v49, ","

    move-object/from16 v0, v18

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->lastIndexOf(Ljava/lang/String;)I

    move-result v49

    move-object/from16 v0, v18

    move/from16 v1, v49

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    move-result-object v49

    invoke-virtual/range {v49 .. v49}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v49

    move-object/from16 v0, v49

    move-object/from16 v1, v41

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mUpdatedFields:Ljava/lang/String;
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$PrivateProfileException; {:try_start_3 .. :try_end_3} :catch_1

    goto/16 :goto_3

    .line 590
    .end local v17    # "fieldIndex":I
    .end local v18    # "fields":Ljava/lang/StringBuilder;
    .end local v26    # "personObject":Lorg/json/JSONObject;
    .end local v44    # "updateContent":Lorg/json/JSONObject;
    .end local v45    # "updateKey":Ljava/lang/String;
    .end local v47    # "updatedFieldObject":Lorg/json/JSONObject;
    .end local v48    # "updatedFields":Lorg/json/JSONArray;
    :catch_1
    move-exception v15

    .line 591
    .local v15, "ex":Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$PrivateProfileException;
    :goto_8
    sget-object v49, Lcom/sec/android/app/sns3/svc/sp/linkedin/parser/SnsLiParserUpdates;->TAG:Ljava/lang/String;

    new-instance v50, Ljava/lang/StringBuilder;

    invoke-direct/range {v50 .. v50}, Ljava/lang/StringBuilder;-><init>()V

    const-string v51, "Update involving a private profile skipped. "

    invoke-virtual/range {v50 .. v51}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v50

    move-object/from16 v0, v50

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v50

    invoke-virtual/range {v50 .. v50}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v50

    invoke-static/range {v49 .. v50}, Landroid/util/secutil/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 592
    const/16 v49, 0x0

    goto/16 :goto_0

    .line 333
    .end local v15    # "ex":Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$PrivateProfileException;
    .restart local v26    # "personObject":Lorg/json/JSONObject;
    .restart local v44    # "updateContent":Lorg/json/JSONObject;
    .restart local v45    # "updateKey":Ljava/lang/String;
    :pswitch_3
    :try_start_4
    const-string v49, "company"

    move-object/from16 v0, v44

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v6

    .line 334
    .local v6, "company":Lorg/json/JSONObject;
    new-instance v49, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Company;

    const-string v50, "id"

    move-object/from16 v0, v50

    invoke-virtual {v6, v0}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v50

    const-string v52, "name"

    move-object/from16 v0, v52

    invoke-virtual {v6, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v52

    invoke-direct/range {v49 .. v52}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Company;-><init>(JLjava/lang/String;)V

    move-object/from16 v0, v49

    move-object/from16 v1, v41

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mCompany:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Company;

    .line 337
    const-string v49, "companyPersonUpdate"

    move-object/from16 v0, v44

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v27

    .line 339
    .local v27, "personUpdate":Lorg/json/JSONObject;
    const-string v49, "person"

    move-object/from16 v0, v27

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v49

    move-object/from16 v0, v41

    move-object/from16 v1, v49

    invoke-static {v0, v1}, Lcom/sec/android/app/sns3/svc/sp/linkedin/parser/SnsLiParserUpdates;->parsePerson(Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;Lorg/json/JSONObject;)Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;

    move-result-object v49

    move-object/from16 v0, v49

    move-object/from16 v1, v41

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mConnection:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;

    .line 341
    const-string v49, "action"

    move-object/from16 v0, v27

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v49

    const-string v50, "code"

    invoke-virtual/range {v49 .. v50}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v49

    move-object/from16 v0, v49

    move-object/from16 v1, v41

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mActionCode:Ljava/lang/String;

    goto/16 :goto_3

    .line 347
    .end local v6    # "company":Lorg/json/JSONObject;
    .end local v27    # "personUpdate":Lorg/json/JSONObject;
    :pswitch_4
    const-string v49, "currentShare"

    move-object/from16 v0, v26

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v39

    .line 349
    .local v39, "shareObject":Lorg/json/JSONObject;
    move-object/from16 v0, v41

    move-object/from16 v1, v39

    invoke-static {v0, v1}, Lcom/sec/android/app/sns3/svc/sp/linkedin/parser/SnsLiParserUpdates;->parseShare(Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;Lorg/json/JSONObject;)Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Share;

    move-result-object v49

    move-object/from16 v0, v49

    move-object/from16 v1, v41

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mCurrentShare:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Share;

    goto/16 :goto_3

    .line 353
    .end local v39    # "shareObject":Lorg/json/JSONObject;
    :pswitch_5
    const-string v49, "memberGroups"

    move-object/from16 v0, v26

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v49

    const-string v50, "values"

    invoke-virtual/range {v49 .. v50}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v49

    const/16 v50, 0x0

    invoke-virtual/range {v49 .. v50}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v20

    .line 356
    .local v20, "groupObject":Lorg/json/JSONObject;
    new-instance v49, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseGroups;

    invoke-direct/range {v49 .. v49}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseGroups;-><init>()V

    move-object/from16 v0, v49

    move-object/from16 v1, v41

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mGroup:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseGroups;

    .line 357
    move-object/from16 v0, v41

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mGroup:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseGroups;

    move-object/from16 v49, v0

    const-string v50, "id"

    move-object/from16 v0, v20

    move-object/from16 v1, v50

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v50

    invoke-virtual/range {v49 .. v50}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseGroups;->setGroupId(Ljava/lang/String;)V

    .line 358
    move-object/from16 v0, v41

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mGroup:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseGroups;

    move-object/from16 v49, v0

    const-string v50, "name"

    move-object/from16 v0, v20

    move-object/from16 v1, v50

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v50

    invoke-virtual/range {v49 .. v50}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseGroups;->setGroupName(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 363
    .end local v20    # "groupObject":Lorg/json/JSONObject;
    :pswitch_6
    const-string v49, "job"

    move-object/from16 v0, v44

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v22

    .line 364
    .local v22, "jobObject":Lorg/json/JSONObject;
    move-object/from16 v0, v41

    move-object/from16 v1, v22

    invoke-static {v0, v1}, Lcom/sec/android/app/sns3/svc/sp/linkedin/parser/SnsLiParserUpdates;->parseJob(Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;Lorg/json/JSONObject;)Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$JobPost;

    move-result-object v49

    move-object/from16 v0, v49

    move-object/from16 v1, v41

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mJobPost:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$JobPost;

    goto/16 :goto_3

    .line 369
    .end local v22    # "jobObject":Lorg/json/JSONObject;
    :pswitch_7
    const-string v49, "company"

    move-object/from16 v0, v44

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v43

    .line 370
    .local v43, "updateCompany":Lorg/json/JSONObject;
    new-instance v49, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Company;

    const-string v50, "id"

    move-object/from16 v0, v43

    move-object/from16 v1, v50

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v50

    const-string v52, "name"

    move-object/from16 v0, v43

    move-object/from16 v1, v52

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v52

    invoke-direct/range {v49 .. v52}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Company;-><init>(JLjava/lang/String;)V

    move-object/from16 v0, v49

    move-object/from16 v1, v41

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mCompany:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Company;

    .line 372
    const-string v49, "companyStatusUpdate"

    move-object/from16 v0, v44

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v40

    .line 374
    .local v40, "statusObject":Lorg/json/JSONObject;
    if-eqz v40, :cond_9

    .line 375
    const-string v49, "share"

    move-object/from16 v0, v40

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v49

    move-object/from16 v0, v41

    move-object/from16 v1, v49

    invoke-static {v0, v1}, Lcom/sec/android/app/sns3/svc/sp/linkedin/parser/SnsLiParserUpdates;->parseShare(Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;Lorg/json/JSONObject;)Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Share;

    move-result-object v49

    move-object/from16 v0, v49

    move-object/from16 v1, v41

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mCurrentShare:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Share;

    .line 377
    sget-object v49, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$CompanyUpdateSubType;->COMPANY_STATUS_UPDATE:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$CompanyUpdateSubType;

    move-object/from16 v0, v49

    move-object/from16 v1, v41

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mCompanyUpdateSubType:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$CompanyUpdateSubType;

    goto/16 :goto_3

    .line 380
    :cond_9
    const-string v49, "companyJobUpdate"

    move-object/from16 v0, v44

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v7

    .line 382
    .local v7, "companyJobObject":Lorg/json/JSONObject;
    if-eqz v7, :cond_a

    .line 383
    const-string v49, "action"

    move-object/from16 v0, v49

    invoke-virtual {v7, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v49

    const-string v50, "code"

    invoke-virtual/range {v49 .. v50}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v49

    move-object/from16 v0, v49

    move-object/from16 v1, v41

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mActionCode:Ljava/lang/String;

    .line 385
    const-string v49, "job"

    move-object/from16 v0, v49

    invoke-virtual {v7, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v49

    move-object/from16 v0, v41

    move-object/from16 v1, v49

    invoke-static {v0, v1}, Lcom/sec/android/app/sns3/svc/sp/linkedin/parser/SnsLiParserUpdates;->parseJob(Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;Lorg/json/JSONObject;)Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$JobPost;

    move-result-object v49

    move-object/from16 v0, v49

    move-object/from16 v1, v41

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mJobPost:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$JobPost;

    .line 387
    sget-object v49, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$CompanyUpdateSubType;->COMPANY_JOB_UPDATE:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$CompanyUpdateSubType;

    move-object/from16 v0, v49

    move-object/from16 v1, v41

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mCompanyUpdateSubType:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$CompanyUpdateSubType;

    goto/16 :goto_3

    .line 390
    :cond_a
    const-string v49, "companyProfileUpdate"

    move-object/from16 v0, v44

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v10

    .line 392
    .local v10, "companyProfObject":Lorg/json/JSONObject;
    if-eqz v10, :cond_d

    .line 393
    const-string v49, "action"

    move-object/from16 v0, v49

    invoke-virtual {v10, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v49

    const-string v50, "code"

    invoke-virtual/range {v49 .. v50}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v49

    move-object/from16 v0, v49

    move-object/from16 v1, v41

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mActionCode:Ljava/lang/String;

    .line 396
    const-string v49, "editor"

    move-object/from16 v0, v49

    invoke-virtual {v10, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v13

    .line 398
    .local v13, "editorObject":Lorg/json/JSONObject;
    move-object/from16 v0, v41

    invoke-static {v0, v13}, Lcom/sec/android/app/sns3/svc/sp/linkedin/parser/SnsLiParserUpdates;->parsePerson(Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;Lorg/json/JSONObject;)Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;

    move-result-object v49

    move-object/from16 v0, v49

    move-object/from16 v1, v41

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mConnection:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;

    .line 399
    const-string v49, "updatedFields"

    move-object/from16 v0, v49

    invoke-virtual {v10, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v49

    const-string v50, "values"

    invoke-virtual/range {v49 .. v50}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v46

    .line 402
    .local v46, "updatedCompanyFields":Lorg/json/JSONArray;
    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    .line 403
    .local v19, "fieldsList":Ljava/lang/StringBuilder;
    const/16 v17, 0x0

    .restart local v17    # "fieldIndex":I
    :goto_9
    invoke-virtual/range {v46 .. v46}, Lorg/json/JSONArray;->length()I

    move-result v49

    move/from16 v0, v17

    move/from16 v1, v49

    if-ge v0, v1, :cond_c

    .line 404
    move-object/from16 v0, v46

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v49

    const-string v50, "name"

    invoke-virtual/range {v49 .. v50}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 406
    .restart local v16    # "field":Ljava/lang/String;
    if-eqz v16, :cond_b

    const-string v49, "/"

    move-object/from16 v0, v16

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v49

    if-eqz v49, :cond_b

    .line 407
    const-string v49, "/"

    move-object/from16 v0, v16

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v49

    add-int/lit8 v49, v49, 0x1

    move-object/from16 v0, v16

    move/from16 v1, v49

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v16

    .line 408
    :cond_b
    move-object/from16 v0, v19

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    const-string v50, ","

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 403
    add-int/lit8 v17, v17, 0x1

    goto :goto_9

    .line 410
    .end local v16    # "field":Ljava/lang/String;
    :cond_c
    const-string v49, ","

    move-object/from16 v0, v19

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->lastIndexOf(Ljava/lang/String;)I

    move-result v49

    move-object/from16 v0, v19

    move/from16 v1, v49

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    move-result-object v49

    invoke-virtual/range {v49 .. v49}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v49

    move-object/from16 v0, v49

    move-object/from16 v1, v41

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mUpdatedFields:Ljava/lang/String;

    .line 412
    sget-object v49, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$CompanyUpdateSubType;->COMPANY_PROFILE_UPDATE:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$CompanyUpdateSubType;

    move-object/from16 v0, v49

    move-object/from16 v1, v41

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mCompanyUpdateSubType:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$CompanyUpdateSubType;

    goto/16 :goto_3

    .line 416
    .end local v13    # "editorObject":Lorg/json/JSONObject;
    .end local v17    # "fieldIndex":I
    .end local v19    # "fieldsList":Ljava/lang/StringBuilder;
    .end local v46    # "updatedCompanyFields":Lorg/json/JSONArray;
    :cond_d
    const-string v49, "companyPersonUpdate"

    move-object/from16 v0, v44

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v8

    .line 418
    .local v8, "companyPersonObject":Lorg/json/JSONObject;
    if-eqz v8, :cond_10

    .line 419
    const-string v49, "person"

    move-object/from16 v0, v49

    invoke-virtual {v8, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v13

    .line 421
    .restart local v13    # "editorObject":Lorg/json/JSONObject;
    move-object/from16 v0, v41

    invoke-static {v0, v13}, Lcom/sec/android/app/sns3/svc/sp/linkedin/parser/SnsLiParserUpdates;->parsePerson(Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;Lorg/json/JSONObject;)Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;

    move-result-object v49

    move-object/from16 v0, v49

    move-object/from16 v1, v41

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mConnection:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;

    .line 422
    const-string v49, "action"

    move-object/from16 v0, v49

    invoke-virtual {v8, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v49

    const-string v50, "code"

    invoke-virtual/range {v49 .. v50}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v49

    move-object/from16 v0, v49

    move-object/from16 v1, v41

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mActionCode:Ljava/lang/String;

    .line 424
    const-string v49, "oldPosition"

    move-object/from16 v0, v49

    invoke-virtual {v8, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v28

    .line 426
    .local v28, "positionObject":Lorg/json/JSONObject;
    if-eqz v28, :cond_e

    .line 427
    new-instance v49, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Position;

    invoke-direct/range {v49 .. v49}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Position;-><init>()V

    move-object/from16 v0, v49

    move-object/from16 v1, v41

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mOldPosition:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Position;

    .line 428
    move-object/from16 v0, v41

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mOldPosition:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Position;

    move-object/from16 v49, v0

    const-string v50, "title"

    move-object/from16 v0, v28

    move-object/from16 v1, v50

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v50

    move-object/from16 v0, v50

    move-object/from16 v1, v49

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Position;->mTitle:Ljava/lang/String;

    .line 430
    move-object/from16 v0, v41

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mOldPosition:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Position;

    move-object/from16 v49, v0

    const-string v50, "company"

    move-object/from16 v0, v28

    move-object/from16 v1, v50

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v50

    const-string v51, "name"

    invoke-virtual/range {v50 .. v51}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v50

    move-object/from16 v0, v50

    move-object/from16 v1, v49

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Position;->mCompany:Ljava/lang/String;

    .line 433
    :cond_e
    const-string v49, "newPosition"

    move-object/from16 v0, v49

    invoke-virtual {v8, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v28

    .line 435
    if-eqz v28, :cond_f

    .line 436
    new-instance v49, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Position;

    invoke-direct/range {v49 .. v49}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Position;-><init>()V

    move-object/from16 v0, v49

    move-object/from16 v1, v41

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mNewPosition:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Position;

    .line 437
    move-object/from16 v0, v41

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mNewPosition:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Position;

    move-object/from16 v49, v0

    const-string v50, "title"

    move-object/from16 v0, v28

    move-object/from16 v1, v50

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v50

    move-object/from16 v0, v50

    move-object/from16 v1, v49

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Position;->mTitle:Ljava/lang/String;

    .line 439
    move-object/from16 v0, v41

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mNewPosition:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Position;

    move-object/from16 v49, v0

    const-string v50, "company"

    move-object/from16 v0, v28

    move-object/from16 v1, v50

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v50

    const-string v51, "name"

    invoke-virtual/range {v50 .. v51}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v50

    move-object/from16 v0, v50

    move-object/from16 v1, v49

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Position;->mCompany:Ljava/lang/String;

    .line 442
    :cond_f
    sget-object v49, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$CompanyUpdateSubType;->COMPANY_PERSON_UPDATE:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$CompanyUpdateSubType;

    move-object/from16 v0, v49

    move-object/from16 v1, v41

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mCompanyUpdateSubType:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$CompanyUpdateSubType;

    goto/16 :goto_3

    .line 445
    .end local v13    # "editorObject":Lorg/json/JSONObject;
    .end local v28    # "positionObject":Lorg/json/JSONObject;
    :cond_10
    const-string v49, "companyProductUpdate"

    move-object/from16 v0, v44

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v9

    .line 447
    .local v9, "companyProductObject":Lorg/json/JSONObject;
    if-eqz v9, :cond_14

    .line 448
    const-string v49, "action"

    move-object/from16 v0, v49

    invoke-virtual {v9, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    .line 450
    .local v4, "action":Lorg/json/JSONObject;
    if-eqz v4, :cond_11

    .line 451
    const-string v49, "code"

    move-object/from16 v0, v49

    invoke-virtual {v4, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v49

    move-object/from16 v0, v49

    move-object/from16 v1, v41

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mActionCode:Ljava/lang/String;

    .line 453
    :cond_11
    const-string v49, "eventType"

    move-object/from16 v0, v49

    invoke-virtual {v9, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v14

    .line 455
    .local v14, "event":Lorg/json/JSONObject;
    if-eqz v14, :cond_12

    .line 456
    const-string v49, "code"

    move-object/from16 v0, v49

    invoke-virtual {v14, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v49

    move-object/from16 v0, v49

    move-object/from16 v1, v41

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mEventType:Ljava/lang/String;

    .line 458
    :cond_12
    const-string v49, "product"

    move-object/from16 v0, v49

    invoke-virtual {v9, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v29

    .line 460
    .local v29, "product":Lorg/json/JSONObject;
    if-eqz v29, :cond_13

    .line 461
    const-string v49, "name"

    move-object/from16 v0, v29

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v49

    move-object/from16 v0, v49

    move-object/from16 v1, v41

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mProductName:Ljava/lang/String;

    .line 463
    :cond_13
    sget-object v49, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$CompanyUpdateSubType;->COMPANY_PRODUCT_UPDATE:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$CompanyUpdateSubType;

    move-object/from16 v0, v49

    move-object/from16 v1, v41

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mCompanyUpdateSubType:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$CompanyUpdateSubType;

    goto/16 :goto_3

    .line 466
    .end local v4    # "action":Lorg/json/JSONObject;
    .end local v14    # "event":Lorg/json/JSONObject;
    .end local v29    # "product":Lorg/json/JSONObject;
    :cond_14
    sget-object v49, Lcom/sec/android/app/sns3/svc/sp/linkedin/parser/SnsLiParserUpdates;->TAG:Ljava/lang/String;

    new-instance v50, Ljava/lang/StringBuilder;

    invoke-direct/range {v50 .. v50}, Ljava/lang/StringBuilder;-><init>()V

    const-string v51, "unexpected update: "

    invoke-virtual/range {v50 .. v51}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v50

    move-object/from16 v0, v50

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v50

    invoke-virtual/range {v50 .. v50}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v50

    invoke-static/range {v49 .. v50}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    .line 469
    .end local v7    # "companyJobObject":Lorg/json/JSONObject;
    .end local v8    # "companyPersonObject":Lorg/json/JSONObject;
    .end local v9    # "companyProductObject":Lorg/json/JSONObject;
    .end local v10    # "companyProfObject":Lorg/json/JSONObject;
    .end local v40    # "statusObject":Lorg/json/JSONObject;
    .end local v43    # "updateCompany":Lorg/json/JSONObject;
    :pswitch_8
    const-string v49, "updateAction"

    move-object/from16 v0, v44

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v5

    .line 471
    .local v5, "actionObject":Lorg/json/JSONObject;
    const-string v49, "action"

    move-object/from16 v0, v49

    invoke-virtual {v5, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v49

    const-string v50, "code"

    invoke-virtual/range {v49 .. v50}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v49

    move-object/from16 v0, v49

    move-object/from16 v1, v41

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mActionCode:Ljava/lang/String;

    .line 473
    const-string v49, "originalUpdate"

    move-object/from16 v0, v49

    invoke-virtual {v5, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v24

    .line 475
    .local v24, "origUpdateObject":Lorg/json/JSONObject;
    move-object/from16 v0, v24

    move-object/from16 v1, p1

    invoke-static {v0, v1}, Lcom/sec/android/app/sns3/svc/sp/linkedin/parser/SnsLiParserUpdates;->parseUpdate(Lorg/json/JSONObject;Ljava/util/Map;)Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;

    move-result-object v49

    move-object/from16 v0, v49

    move-object/from16 v1, v41

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mVirlUpdate:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;

    .line 476
    move-object/from16 v0, v41

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mVirlUpdate:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;

    move-object/from16 v49, v0

    if-nez v49, :cond_2

    .line 477
    new-instance v49, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$PrivateProfileException;

    const-string v50, "Original post\'s author details are not accessible"

    invoke-direct/range {v49 .. v50}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$PrivateProfileException;-><init>(Ljava/lang/String;)V

    throw v49

    .line 483
    .end local v5    # "actionObject":Lorg/json/JSONObject;
    .end local v24    # "origUpdateObject":Lorg/json/JSONObject;
    :pswitch_9
    const-string v49, "personActivities"

    move-object/from16 v0, v26

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v25

    .line 485
    .local v25, "personActivity":Lorg/json/JSONObject;
    move-object/from16 v0, v41

    move-object/from16 v1, v25

    invoke-static {v0, v1}, Lcom/sec/android/app/sns3/svc/sp/linkedin/parser/SnsLiParserUpdates;->parseAppUpdates(Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;Lorg/json/JSONObject;)Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$AppUpdates;

    move-result-object v49

    move-object/from16 v0, v49

    move-object/from16 v1, v41

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mAppUpdate:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$AppUpdates;

    goto/16 :goto_3

    .line 489
    .end local v25    # "personActivity":Lorg/json/JSONObject;
    :pswitch_a
    move-object/from16 v0, v41

    move-object/from16 v1, v26

    invoke-static {v0, v1}, Lcom/sec/android/app/sns3/svc/sp/linkedin/parser/SnsLiParserUpdates;->parseContactDetails(Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;Lorg/json/JSONObject;)Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$ContactDetails;

    move-result-object v49

    move-object/from16 v0, v49

    move-object/from16 v1, v41

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mContactDetails:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$ContactDetails;

    goto/16 :goto_3

    .line 494
    :pswitch_b
    const-string v49, "recommendationsReceived"

    move-object/from16 v0, v26

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v30

    .line 496
    .local v30, "recommendation":Lorg/json/JSONObject;
    new-instance v49, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$RecommendationUpdates;

    invoke-direct/range {v49 .. v49}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$RecommendationUpdates;-><init>()V

    move-object/from16 v0, v49

    move-object/from16 v1, v41

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mRecommendationUpdate:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$RecommendationUpdates;

    .line 497
    if-eqz v30, :cond_16

    const-string v49, "_total"

    move-object/from16 v0, v30

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I
    :try_end_4
    .catch Lorg/json/JSONException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$PrivateProfileException; {:try_start_4 .. :try_end_4} :catch_1

    move-result v49

    if-lez v49, :cond_16

    .line 501
    :try_start_5
    const-string v49, "values"

    move-object/from16 v0, v30

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v31

    .line 504
    .local v31, "recommendationArray":Lorg/json/JSONArray;
    if-eqz v31, :cond_16

    .line 506
    const/16 v21, 0x0

    .restart local v21    # "j":I
    :goto_a
    invoke-virtual/range {v31 .. v31}, Lorg/json/JSONArray;->length()I

    move-result v49

    move/from16 v0, v21

    move/from16 v1, v49

    if-ge v0, v1, :cond_16

    .line 508
    move-object/from16 v0, v31

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v35

    .line 510
    .local v35, "recommendationItem":Lorg/json/JSONObject;
    move-object/from16 v0, v41

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mRecommendationUpdate:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$RecommendationUpdates;

    move-object/from16 v49, v0

    const-string v50, "id"

    move-object/from16 v0, v35

    move-object/from16 v1, v50

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v50

    move-wide/from16 v0, v50

    move-object/from16 v2, v49

    iput-wide v0, v2, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$RecommendationUpdates;->mRecommendationId:J

    .line 512
    const-string v49, "recommendationType"

    move-object/from16 v0, v35

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v36

    .line 514
    .local v36, "recommendationType":Lorg/json/JSONObject;
    move-object/from16 v0, v41

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mRecommendationUpdate:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$RecommendationUpdates;

    move-object/from16 v49, v0

    const-string v50, "code"

    move-object/from16 v0, v36

    move-object/from16 v1, v50

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v50

    move-object/from16 v0, v50

    move-object/from16 v1, v49

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$RecommendationUpdates;->mRecommendationType:Ljava/lang/String;

    .line 516
    const-string v49, "recommender"

    move-object/from16 v0, v35

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v38

    .line 519
    .local v38, "recommender":Lorg/json/JSONObject;
    if-eqz v38, :cond_15

    .line 520
    move-object/from16 v0, v41

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mRecommendationUpdate:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$RecommendationUpdates;

    move-object/from16 v49, v0

    move-object/from16 v0, v41

    move-object/from16 v1, v38

    invoke-static {v0, v1}, Lcom/sec/android/app/sns3/svc/sp/linkedin/parser/SnsLiParserUpdates;->parsePerson(Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;Lorg/json/JSONObject;)Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;

    move-result-object v50

    move-object/from16 v0, v50

    move-object/from16 v1, v49

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$RecommendationUpdates;->mRecommender:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;

    .line 524
    :cond_15
    move-object/from16 v0, v41

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mRecommendationUpdate:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$RecommendationUpdates;

    move-object/from16 v49, v0

    const-string v50, "webUrl"

    move-object/from16 v0, v35

    move-object/from16 v1, v50

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v50

    move-object/from16 v0, v50

    move-object/from16 v1, v49

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$RecommendationUpdates;->mRecommondedUrl:Ljava/lang/String;
    :try_end_5
    .catch Lorg/json/JSONException; {:try_start_5 .. :try_end_5} :catch_2
    .catch Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$PrivateProfileException; {:try_start_5 .. :try_end_5} :catch_1

    .line 506
    add-int/lit8 v21, v21, 0x1

    goto :goto_a

    .line 530
    .end local v21    # "j":I
    .end local v31    # "recommendationArray":Lorg/json/JSONArray;
    .end local v35    # "recommendationItem":Lorg/json/JSONObject;
    .end local v36    # "recommendationType":Lorg/json/JSONObject;
    .end local v38    # "recommender":Lorg/json/JSONObject;
    :catch_2
    move-exception v12

    .line 531
    .restart local v12    # "e":Lorg/json/JSONException;
    :try_start_6
    invoke-virtual {v12}, Lorg/json/JSONException;->printStackTrace()V

    .line 535
    .end local v12    # "e":Lorg/json/JSONException;
    :cond_16
    const-string v49, "recommendationsGiven"

    move-object/from16 v0, v26

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v32

    .line 537
    .local v32, "recommendationGiven":Lorg/json/JSONObject;
    if-eqz v32, :cond_2

    const-string v49, "_total"

    move-object/from16 v0, v32

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I
    :try_end_6
    .catch Lorg/json/JSONException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$PrivateProfileException; {:try_start_6 .. :try_end_6} :catch_1

    move-result v49

    if-lez v49, :cond_2

    .line 540
    :try_start_7
    const-string v49, "values"

    move-object/from16 v0, v32

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v33

    .line 543
    .local v33, "recommendationGivenArray":Lorg/json/JSONArray;
    if-eqz v33, :cond_2

    .line 544
    const/16 v21, 0x0

    .restart local v21    # "j":I
    :goto_b
    invoke-virtual/range {v33 .. v33}, Lorg/json/JSONArray;->length()I

    move-result v49

    move/from16 v0, v21

    move/from16 v1, v49

    if-ge v0, v1, :cond_2

    .line 545
    move-object/from16 v0, v33

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v34

    .line 547
    .local v34, "recommendationGivenItem":Lorg/json/JSONObject;
    move-object/from16 v0, v41

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mRecommendationUpdate:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$RecommendationUpdates;

    move-object/from16 v49, v0

    const-string v50, "id"

    move-object/from16 v0, v34

    move-object/from16 v1, v50

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v50

    move-wide/from16 v0, v50

    move-object/from16 v2, v49

    iput-wide v0, v2, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$RecommendationUpdates;->mRecommendationId:J

    .line 549
    const-string v49, "recommendationType"

    move-object/from16 v0, v34

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v36

    .line 551
    .restart local v36    # "recommendationType":Lorg/json/JSONObject;
    move-object/from16 v0, v41

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mRecommendationUpdate:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$RecommendationUpdates;

    move-object/from16 v49, v0

    const-string v50, "code"

    move-object/from16 v0, v36

    move-object/from16 v1, v50

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v50

    move-object/from16 v0, v50

    move-object/from16 v1, v49

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$RecommendationUpdates;->mRecommendationType:Ljava/lang/String;

    .line 553
    const-string v49, "recommendee"

    move-object/from16 v0, v34

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v37

    .line 555
    .local v37, "recommendee":Lorg/json/JSONObject;
    if-eqz v37, :cond_17

    .line 556
    move-object/from16 v0, v41

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mRecommendationUpdate:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$RecommendationUpdates;

    move-object/from16 v49, v0

    move-object/from16 v0, v41

    move-object/from16 v1, v37

    invoke-static {v0, v1}, Lcom/sec/android/app/sns3/svc/sp/linkedin/parser/SnsLiParserUpdates;->parsePerson(Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;Lorg/json/JSONObject;)Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;

    move-result-object v50

    move-object/from16 v0, v50

    move-object/from16 v1, v49

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$RecommendationUpdates;->mRecommedee:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;

    .line 559
    :cond_17
    move-object/from16 v0, v41

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mRecommendationUpdate:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$RecommendationUpdates;

    move-object/from16 v49, v0

    const-string v50, "webUrl"

    move-object/from16 v0, v34

    move-object/from16 v1, v50

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v50

    move-object/from16 v0, v50

    move-object/from16 v1, v49

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$RecommendationUpdates;->mRecommondedUrl:Ljava/lang/String;
    :try_end_7
    .catch Lorg/json/JSONException; {:try_start_7 .. :try_end_7} :catch_3
    .catch Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$PrivateProfileException; {:try_start_7 .. :try_end_7} :catch_1

    .line 544
    add-int/lit8 v21, v21, 0x1

    goto :goto_b

    .line 563
    .end local v21    # "j":I
    .end local v33    # "recommendationGivenArray":Lorg/json/JSONArray;
    .end local v34    # "recommendationGivenItem":Lorg/json/JSONObject;
    .end local v36    # "recommendationType":Lorg/json/JSONObject;
    .end local v37    # "recommendee":Lorg/json/JSONObject;
    :catch_3
    move-exception v12

    .line 564
    .restart local v12    # "e":Lorg/json/JSONException;
    :try_start_8
    invoke-virtual {v12}, Lorg/json/JSONException;->printStackTrace()V
    :try_end_8
    .catch Lorg/json/JSONException; {:try_start_8 .. :try_end_8} :catch_0
    .catch Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$PrivateProfileException; {:try_start_8 .. :try_end_8} :catch_1

    goto/16 :goto_3

    .line 590
    .end local v12    # "e":Lorg/json/JSONException;
    .end local v26    # "personObject":Lorg/json/JSONObject;
    .end local v30    # "recommendation":Lorg/json/JSONObject;
    .end local v32    # "recommendationGiven":Lorg/json/JSONObject;
    .end local v41    # "update":Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;
    .end local v44    # "updateContent":Lorg/json/JSONObject;
    .restart local v42    # "update":Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;
    :catch_4
    move-exception v15

    move-object/from16 v41, v42

    .end local v42    # "update":Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;
    .restart local v41    # "update":Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;
    goto/16 :goto_8

    .line 588
    .end local v41    # "update":Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;
    .restart local v42    # "update":Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;
    :catch_5
    move-exception v12

    move-object/from16 v41, v42

    .end local v42    # "update":Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;
    .restart local v41    # "update":Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;
    goto/16 :goto_6

    .line 298
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method
