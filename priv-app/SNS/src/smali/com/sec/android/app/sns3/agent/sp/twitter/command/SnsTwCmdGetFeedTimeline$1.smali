.class Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdGetFeedTimeline$1;
.super Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestStatuses;
.source "SnsTwCmdGetFeedTimeline.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdGetFeedTimeline;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Handler;Ljava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdGetFeedTimeline;


# direct methods
.method constructor <init>(Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdGetFeedTimeline;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;Landroid/os/Bundle;)V
    .locals 0
    .param p2, "x0"    # Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    .param p3, "x1"    # Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;
    .param p4, "x2"    # Landroid/os/Bundle;

    .prologue
    .line 51
    iput-object p1, p0, Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdGetFeedTimeline$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdGetFeedTimeline;

    invoke-direct {p0, p2, p3, p4}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestStatuses;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;Landroid/os/Bundle;)V

    return-void
.end method


# virtual methods
.method public onReqRespond(IZIILandroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;)Z
    .locals 8
    .param p1, "reqID"    # I
    .param p2, "bSuccess"    # Z
    .param p3, "httpStatus"    # I
    .param p4, "errorCode"    # I
    .param p5, "reason"    # Landroid/os/Bundle;
    .param p6, "status"    # Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;

    .prologue
    const/4 v7, 0x0

    .line 57
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 58
    .local v2, "values":Landroid/content/ContentValues;
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/sns3/SnsApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 60
    .local v0, "cr":Landroid/content/ContentResolver;
    if-eqz p2, :cond_2

    .line 61
    if-eqz p6, :cond_1

    .line 62
    move-object v1, p6

    .line 63
    .local v1, "curStatus":Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;
    sget-object v3, Lcom/sec/android/app/sns3/agent/sp/twitter/db/SnsTwitterDB$StatusStream;->CONTENT_URI:Landroid/net/Uri;

    const-string v4, "user_id=?"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    iget-object v6, v1, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;->mUser:Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseUser;

    iget-object v6, v6, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseUser;->mId:Ljava/lang/String;

    aput-object v6, v5, v7

    invoke-virtual {v0, v3, v4, v5}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 68
    :goto_0
    if-eqz v1, :cond_1

    .line 69
    invoke-virtual {v2}, Landroid/content/ContentValues;->clear()V

    .line 71
    const-string v3, "tweet_id"

    iget-object v4, v1, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;->mId:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    const-string v3, "user_id"

    iget-object v4, v1, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;->mUser:Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseUser;

    iget-object v4, v4, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseUser;->mId:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    const-string v3, "user_name"

    iget-object v4, v1, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;->mUser:Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseUser;

    iget-object v4, v4, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseUser;->mName:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    const-string v3, "user_screen_name"

    iget-object v4, v1, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;->mUser:Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseUser;

    iget-object v4, v4, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseUser;->mScreenName:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    const-string v3, "retweet_count"

    iget v4, v1, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;->mRetweetCount:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 76
    const-string v3, "favourites_count"

    iget v4, v1, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;->mFavouritesCount:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 77
    const-string v3, "message"

    iget-object v4, v1, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;->mText:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    const-string v3, "picture"

    iget-object v4, v1, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;->mUser:Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseUser;

    iget-object v4, v4, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseUser;->mProfileImageUrl:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    iget-object v3, v1, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;->mEntities:Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseEntities;

    iget-object v3, v3, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseEntities;->mMedia:Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseMedia;

    if-eqz v3, :cond_0

    .line 80
    const-string v3, "media_url"

    iget-object v4, v1, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;->mEntities:Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseEntities;

    iget-object v4, v4, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseEntities;->mMedia:Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseMedia;

    iget-object v4, v4, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseMedia;->mMediaUrl:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    const-string v3, "link"

    iget-object v4, v1, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;->mEntities:Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseEntities;

    iget-object v4, v4, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseEntities;->mMedia:Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseMedia;

    iget-object v4, v4, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseMedia;->mExpandedUrl:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    :cond_0
    const-string v3, "create_at"

    iget-object v4, v1, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;->mCreatedAt:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    sget-object v3, Lcom/sec/android/app/sns3/agent/sp/twitter/db/SnsTwitterDB$StatusStream;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v3, v2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 89
    iget-object v1, v1, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;->mNext:Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;

    goto :goto_0

    .line 92
    .end local v1    # "curStatus":Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdGetFeedTimeline$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdGetFeedTimeline;

    sget-object v4, Lcom/sec/android/app/sns3/agent/sp/twitter/db/SnsTwitterDB$StatusStream;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdGetFeedTimeline;->setUri(Ljava/lang/String;)V

    .line 100
    :goto_1
    iget-object v3, p0, Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdGetFeedTimeline$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdGetFeedTimeline;

    invoke-virtual {v3, p2}, Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdGetFeedTimeline;->setSuccess(Z)V

    .line 101
    iget-object v3, p0, Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdGetFeedTimeline$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdGetFeedTimeline;

    invoke-virtual {v3, p0}, Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdGetFeedTimeline;->receive(Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;)V

    .line 103
    return v7

    .line 95
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdGetFeedTimeline$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdGetFeedTimeline;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdGetFeedTimeline;->setUri(Ljava/lang/String;)V

    .line 96
    iget-object v3, p0, Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdGetFeedTimeline$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdGetFeedTimeline;

    new-instance v4, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;

    const-string v5, "twitter"

    invoke-direct {v4, v5, p3, p4, p5}, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;-><init>(Ljava/lang/String;IILandroid/os/Bundle;)V

    invoke-virtual {v3, v4}, Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdGetFeedTimeline;->setResponseList(Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;)Z

    goto :goto_1
.end method
