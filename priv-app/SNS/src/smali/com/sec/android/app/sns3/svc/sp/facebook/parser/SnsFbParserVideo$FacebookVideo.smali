.class public interface abstract Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserVideo$FacebookVideo;
.super Ljava/lang/Object;
.source "SnsFbParserVideo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserVideo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "FacebookVideo"
.end annotation


# static fields
.field public static final COMMENT:Ljava/lang/String; = "comments"

.field public static final CREATED_TIME:Ljava/lang/String; = "created_time"

.field public static final DESCRIPTION:Ljava/lang/String; = "description"

.field public static final EMBED_HTML:Ljava/lang/String; = "embed_html"

.field public static final FORMAT:Ljava/lang/String; = "format"

.field public static final FROM:Ljava/lang/String; = "from"

.field public static final FROM_ID:Ljava/lang/String; = "id"

.field public static final FROM_NAME:Ljava/lang/String; = "name"

.field public static final ICON:Ljava/lang/String; = "icon"

.field public static final ID:Ljava/lang/String; = "id"

.field public static final NAME:Ljava/lang/String; = "name"

.field public static final PICTURE:Ljava/lang/String; = "picture"

.field public static final SOURCE:Ljava/lang/String; = "source"

.field public static final TAGS:Ljava/lang/String; = "tags"

.field public static final UPDATED_TIME:Ljava/lang/String; = "updated_time"
