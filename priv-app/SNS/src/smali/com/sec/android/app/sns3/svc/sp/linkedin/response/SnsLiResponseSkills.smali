.class public final Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseSkills;
.super Ljava/lang/Object;
.source "SnsLiResponseSkills.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseSkills$LinkedInSkills;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseSkills;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mSkillID:I

.field private mSkillName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 71
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseSkills$1;

    invoke-direct {v0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseSkills$1;-><init>()V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseSkills;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 82
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 83
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 85
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 86
    invoke-virtual {p0, p1}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseSkills;->readFromParcel(Landroid/os/Parcel;)V

    .line 87
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseSkills$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseSkills$1;

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseSkills;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public static parse(Lorg/json/JSONObject;)Ljava/util/List;
    .locals 8
    .param p0, "contentObj"    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/json/JSONObject;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseSkills;",
            ">;"
        }
    .end annotation

    .prologue
    .line 116
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 119
    .local v6, "skillsList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseSkills;>;"
    if-eqz p0, :cond_0

    .line 123
    :try_start_0
    const-string v7, "values"

    invoke-virtual {p0, v7}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    .line 125
    .local v2, "ja":Lorg/json/JSONArray;
    if-eqz v2, :cond_0

    .line 127
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v7

    if-ge v1, v7, :cond_0

    .line 129
    new-instance v4, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseSkills;

    invoke-direct {v4}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseSkills;-><init>()V

    .line 131
    .local v4, "newSkills":Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseSkills;
    invoke-virtual {v2, v1}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v3

    .line 132
    .local v3, "jsonObj":Lorg/json/JSONObject;
    const-string v7, "id"

    invoke-virtual {v3, v7}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v7

    invoke-virtual {v4, v7}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseSkills;->setSkillID(I)V

    .line 133
    const-string v7, "skill"

    invoke-virtual {v3, v7}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v5

    .line 134
    .local v5, "skillName":Lorg/json/JSONObject;
    const-string v7, "name"

    invoke-virtual {v5, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseSkills;->setSkillName(Ljava/lang/String;)V

    .line 136
    invoke-interface {v6, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 127
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 150
    .end local v1    # "i":I
    .end local v2    # "ja":Lorg/json/JSONArray;
    .end local v3    # "jsonObj":Lorg/json/JSONObject;
    .end local v4    # "newSkills":Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseSkills;
    .end local v5    # "skillName":Lorg/json/JSONObject;
    :catch_0
    move-exception v0

    .line 152
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    .line 154
    .end local v0    # "e":Lorg/json/JSONException;
    :cond_0
    return-object v6
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 105
    const/4 v0, 0x0

    return v0
.end method

.method public getSkillID()I
    .locals 1

    .prologue
    .line 55
    iget v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseSkills;->mSkillID:I

    return v0
.end method

.method public getSkillName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseSkills;->mSkillName:Ljava/lang/String;

    return-object v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 97
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseSkills;->mSkillID:I

    .line 98
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseSkills;->mSkillName:Ljava/lang/String;

    .line 100
    return-void
.end method

.method public setSkillID(I)V
    .locals 0
    .param p1, "mSkillID"    # I

    .prologue
    .line 59
    iput p1, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseSkills;->mSkillID:I

    .line 60
    return-void
.end method

.method public setSkillName(Ljava/lang/String;)V
    .locals 0
    .param p1, "mSkillName"    # Ljava/lang/String;

    .prologue
    .line 67
    iput-object p1, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseSkills;->mSkillName:Ljava/lang/String;

    .line 68
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 159
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SnsLiResponseSkills [mSkillID="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseSkills;->mSkillID:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mSkillName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseSkills;->mSkillName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flag"    # I

    .prologue
    .line 91
    iget v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseSkills;->mSkillID:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 92
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseSkills;->mSkillName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 94
    return-void
.end method
