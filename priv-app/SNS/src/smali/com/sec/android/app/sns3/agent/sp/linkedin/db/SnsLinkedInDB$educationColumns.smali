.class public interface abstract Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$educationColumns;
.super Ljava/lang/Object;
.source "SnsLinkedInDB.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "educationColumns"
.end annotation


# static fields
.field public static final DEGREE_OF_EDUCATION:Ljava/lang/String; = "degree_of_education"

.field public static final EDUCATION_ID:Ljava/lang/String; = "education_id"

.field public static final END_DATE_YEAR:Ljava/lang/String; = "end_date_year"

.field public static final FIELD_OF_STUDY:Ljava/lang/String; = "field_of_study"

.field public static final SCHOOL_NAME:Ljava/lang/String; = "school_name"

.field public static final START_DATE_YEAR:Ljava/lang/String; = "start_date_year"
