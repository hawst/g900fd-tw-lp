.class public Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseContactInfo;
.super Ljava/lang/Object;
.source "SnsLiResponseContactInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseContactInfo$LinkedInContactInfo;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseContactInfo;",
            ">;"
        }
    .end annotation
.end field

.field public static final INFO_TYPE_EMAIL:Ljava/lang/String; = "EMAIL"

.field public static final INFO_TYPE_IM:Ljava/lang/String; = "IM"

.field public static final INFO_TYPE_PHONE:Ljava/lang/String; = "PHONE"

.field public static final INFO_TYPE_TWITTER:Ljava/lang/String; = "TWITTER"


# instance fields
.field private mInfoType:Ljava/lang/String;

.field private mSubType:Ljava/lang/String;

.field private mValue:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 58
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseContactInfo$1;

    invoke-direct {v0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseContactInfo$1;-><init>()V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseContactInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 112
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 114
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 108
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 109
    invoke-virtual {p0, p1}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseContactInfo;->readFromParcel(Landroid/os/Parcel;)V

    .line 110
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "mInfoType"    # Ljava/lang/String;
    .param p2, "mValue"    # Ljava/lang/String;

    .prologue
    .line 117
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseContactInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "mInfoType"    # Ljava/lang/String;
    .param p2, "mSubType"    # Ljava/lang/String;
    .param p3, "mValue"    # Ljava/lang/String;

    .prologue
    .line 121
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 122
    iput-object p1, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseContactInfo;->mInfoType:Ljava/lang/String;

    .line 123
    iput-object p2, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseContactInfo;->mSubType:Ljava/lang/String;

    .line 124
    iput-object p3, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseContactInfo;->mValue:Ljava/lang/String;

    .line 125
    return-void
.end method

.method public static parse(Ljava/lang/String;)Ljava/util/List;
    .locals 8
    .param p0, "data"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseContactInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 128
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 131
    .local v0, "contactInfoList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseContactInfo;>;"
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 133
    .local v1, "details":Lorg/json/JSONObject;
    const-string v7, "phoneNumbers"

    invoke-virtual {p0, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 134
    const-string v7, "phoneNumbers"

    invoke-virtual {v1, v7}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    .line 135
    .local v4, "phone":Lorg/json/JSONObject;
    const-string v7, "_total"

    invoke-virtual {v4, v7}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v5

    .line 136
    .local v5, "total":I
    if-lez v5, :cond_0

    .line 137
    invoke-static {v4}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseContactInfo;->parsePhoneNumbers(Lorg/json/JSONObject;)Ljava/util/List;

    move-result-object v7

    invoke-interface {v0, v7}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 141
    .end local v4    # "phone":Lorg/json/JSONObject;
    .end local v5    # "total":I
    :cond_0
    const-string v7, "imAccounts"

    invoke-virtual {p0, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 142
    const-string v7, "imAccounts"

    invoke-virtual {v1, v7}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v3

    .line 143
    .local v3, "im":Lorg/json/JSONObject;
    const-string v7, "_total"

    invoke-virtual {v3, v7}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v5

    .line 144
    .restart local v5    # "total":I
    if-lez v5, :cond_1

    .line 145
    invoke-static {v3}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseContactInfo;->parseImAccounts(Lorg/json/JSONObject;)Ljava/util/List;

    move-result-object v7

    invoke-interface {v0, v7}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 149
    .end local v3    # "im":Lorg/json/JSONObject;
    .end local v5    # "total":I
    :cond_1
    const-string v7, "twitterAccounts"

    invoke-virtual {p0, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 150
    const-string v7, "twitterAccounts"

    invoke-virtual {v1, v7}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v6

    .line 151
    .local v6, "twitter":Lorg/json/JSONObject;
    const-string v7, "_total"

    invoke-virtual {v6, v7}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v5

    .line 152
    .restart local v5    # "total":I
    if-lez v5, :cond_2

    .line 153
    invoke-static {v6}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseContactInfo;->parseTwitterAccounts(Lorg/json/JSONObject;)Ljava/util/List;

    move-result-object v7

    invoke-interface {v0, v7}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 161
    .end local v1    # "details":Lorg/json/JSONObject;
    .end local v5    # "total":I
    .end local v6    # "twitter":Lorg/json/JSONObject;
    :cond_2
    :goto_0
    return-object v0

    .line 157
    :catch_0
    move-exception v2

    .line 158
    .local v2, "e":Lorg/json/JSONException;
    invoke-virtual {v2}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public static parseImAccounts(Lorg/json/JSONObject;)Ljava/util/List;
    .locals 9
    .param p0, "data"    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/json/JSONObject;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseContactInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 189
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 192
    .local v1, "contactInfoList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseContactInfo;>;"
    :try_start_0
    const-string v6, "values"

    invoke-virtual {p0, v6}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v5

    .line 193
    .local v5, "infoList":Lorg/json/JSONArray;
    if-eqz v5, :cond_0

    .line 194
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-virtual {v5}, Lorg/json/JSONArray;->length()I

    move-result v6

    if-ge v3, v6, :cond_0

    .line 195
    invoke-virtual {v5, v3}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    .line 197
    .local v4, "info":Lorg/json/JSONObject;
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseContactInfo;

    const-string v6, "IM"

    const-string v7, "imAccountType"

    invoke-virtual {v4, v7}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "imAccountName"

    invoke-virtual {v4, v8}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v0, v6, v7, v8}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseContactInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 200
    .local v0, "contactInfo":Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseContactInfo;
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 194
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 203
    .end local v0    # "contactInfo":Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseContactInfo;
    .end local v3    # "i":I
    .end local v4    # "info":Lorg/json/JSONObject;
    .end local v5    # "infoList":Lorg/json/JSONArray;
    :catch_0
    move-exception v2

    .line 204
    .local v2, "e":Lorg/json/JSONException;
    invoke-virtual {v2}, Lorg/json/JSONException;->printStackTrace()V

    .line 207
    .end local v2    # "e":Lorg/json/JSONException;
    :cond_0
    return-object v1
.end method

.method public static parsePhoneNumbers(Lorg/json/JSONObject;)Ljava/util/List;
    .locals 9
    .param p0, "data"    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/json/JSONObject;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseContactInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 165
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 168
    .local v1, "contactInfoList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseContactInfo;>;"
    :try_start_0
    const-string v6, "values"

    invoke-virtual {p0, v6}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v5

    .line 169
    .local v5, "infoList":Lorg/json/JSONArray;
    if-eqz v5, :cond_0

    .line 170
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-virtual {v5}, Lorg/json/JSONArray;->length()I

    move-result v6

    if-ge v3, v6, :cond_0

    .line 171
    invoke-virtual {v5, v3}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    .line 173
    .local v4, "info":Lorg/json/JSONObject;
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseContactInfo;

    const-string v6, "PHONE"

    const-string v7, "phoneType"

    invoke-virtual {v4, v7}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "phoneNumber"

    invoke-virtual {v4, v8}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v0, v6, v7, v8}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseContactInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 177
    .local v0, "contactInfo":Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseContactInfo;
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 170
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 180
    .end local v0    # "contactInfo":Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseContactInfo;
    .end local v3    # "i":I
    .end local v4    # "info":Lorg/json/JSONObject;
    .end local v5    # "infoList":Lorg/json/JSONArray;
    :catch_0
    move-exception v2

    .line 181
    .local v2, "e":Lorg/json/JSONException;
    invoke-virtual {v2}, Lorg/json/JSONException;->printStackTrace()V

    .line 184
    .end local v2    # "e":Lorg/json/JSONException;
    :cond_0
    return-object v1
.end method

.method public static parseTwitterAccounts(Lorg/json/JSONObject;)Ljava/util/List;
    .locals 9
    .param p0, "data"    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/json/JSONObject;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseContactInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 212
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 215
    .local v1, "contactInfoList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseContactInfo;>;"
    :try_start_0
    const-string v6, "values"

    invoke-virtual {p0, v6}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v5

    .line 216
    .local v5, "infoList":Lorg/json/JSONArray;
    if-eqz v5, :cond_0

    .line 217
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-virtual {v5}, Lorg/json/JSONArray;->length()I

    move-result v6

    if-ge v3, v6, :cond_0

    .line 218
    invoke-virtual {v5, v3}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    .line 220
    .local v4, "info":Lorg/json/JSONObject;
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseContactInfo;

    const-string v6, "TWITTER"

    const-string v7, "providerAccountId"

    invoke-virtual {v4, v7}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "providerAccountName"

    invoke-virtual {v4, v8}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v0, v6, v7, v8}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseContactInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 224
    .local v0, "contactInfo":Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseContactInfo;
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 217
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 227
    .end local v0    # "contactInfo":Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseContactInfo;
    .end local v3    # "i":I
    .end local v4    # "info":Lorg/json/JSONObject;
    .end local v5    # "infoList":Lorg/json/JSONArray;
    :catch_0
    move-exception v2

    .line 228
    .local v2, "e":Lorg/json/JSONException;
    invoke-virtual {v2}, Lorg/json/JSONException;->printStackTrace()V

    .line 231
    .end local v2    # "e":Lorg/json/JSONException;
    :cond_0
    return-object v1
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 94
    const/4 v0, 0x0

    return v0
.end method

.method public getInfoType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseContactInfo;->mInfoType:Ljava/lang/String;

    return-object v0
.end method

.method public getSubType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseContactInfo;->mSubType:Ljava/lang/String;

    return-object v0
.end method

.method public getValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseContactInfo;->mValue:Ljava/lang/String;

    return-object v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 104
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseContactInfo;->mInfoType:Ljava/lang/String;

    .line 105
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseContactInfo;->mValue:Ljava/lang/String;

    .line 106
    return-void
.end method

.method public setInfoType(Ljava/lang/String;)V
    .locals 0
    .param p1, "mInfoType"    # Ljava/lang/String;

    .prologue
    .line 73
    iput-object p1, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseContactInfo;->mInfoType:Ljava/lang/String;

    .line 74
    return-void
.end method

.method public setSubType(Ljava/lang/String;)V
    .locals 0
    .param p1, "mSubType"    # Ljava/lang/String;

    .prologue
    .line 89
    iput-object p1, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseContactInfo;->mSubType:Ljava/lang/String;

    .line 90
    return-void
.end method

.method public setValue(Ljava/lang/String;)V
    .locals 0
    .param p1, "mValue"    # Ljava/lang/String;

    .prologue
    .line 81
    iput-object p1, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseContactInfo;->mValue:Ljava/lang/String;

    .line 82
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 99
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseContactInfo;->mInfoType:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 100
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseContactInfo;->mValue:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 101
    return-void
.end method
