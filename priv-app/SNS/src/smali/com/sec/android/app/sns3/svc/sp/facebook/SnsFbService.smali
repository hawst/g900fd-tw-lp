.class public Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;
.super Landroid/app/Service;
.source "SnsFbService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$SnsFbServiceBinder;
    }
.end annotation


# static fields
.field public static final SNS_FB_NOTIFICATION:Ljava/lang/String; = "com.sec.android.app.sns3.svc.sp.facebook.SNS_FB_NOTIFICATION"

.field public static final SNS_FB_NOTIFICATION_FAILED_COMPLETE:I = 0x3

.field public static final SNS_FB_NOTIFICATION_PROGRESS:I = 0x1

.field public static final SNS_FB_NOTIFICATION_START:I = 0x0

.field public static final SNS_FB_NOTIFICATION_SUCCESS_COMPLETE:I = 0x2

.field public static final SNS_FB_PROGRESS:Ljava/lang/String; = "SNS_FB_NOTIFICATION.progress"

.field public static final SNS_FB_PROGRESS_STATE:Ljava/lang/String; = "SNS_FB_NOTIFICATION.state"

.field public static mContentSize:J


# instance fields
.field private final mBinder:Landroid/os/IBinder;

.field private mProgressNum:J

.field private mReqEntity:Lcom/sec/android/app/sns3/svc/http/SnsCustomMultiPartEntity;

.field private final mSnsSvcFacebookBinder:Lcom/sec/android/app/sns3/svc/sp/facebook/api/ISnsFacebook$Stub;

.field private mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 118
    const-wide/16 v0, 0x0

    sput-wide v0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;->mContentSize:J

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 110
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 112
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$SnsFbServiceBinder;

    invoke-direct {v0, p0}, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$SnsFbServiceBinder;-><init>(Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;)V

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;->mBinder:Landroid/os/IBinder;

    .line 116
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;->mProgressNum:J

    .line 165
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1;-><init>(Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;)V

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;->mSnsSvcFacebookBinder:Lcom/sec/android/app/sns3/svc/sp/facebook/api/ISnsFacebook$Stub;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;)Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;

    .prologue
    .line 110
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;

    .prologue
    .line 110
    iget-wide v0, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;->mProgressNum:J

    return-wide v0
.end method

.method static synthetic access$102(Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;J)J
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;
    .param p1, "x1"    # J

    .prologue
    .line 110
    iput-wide p1, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;->mProgressNum:J

    return-wide p1
.end method

.method static synthetic access$200(Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;)Lcom/sec/android/app/sns3/svc/http/SnsCustomMultiPartEntity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;

    .prologue
    .line 110
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;->mReqEntity:Lcom/sec/android/app/sns3/svc/http/SnsCustomMultiPartEntity;

    return-object v0
.end method

.method static synthetic access$202(Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;Lcom/sec/android/app/sns3/svc/http/SnsCustomMultiPartEntity;)Lcom/sec/android/app/sns3/svc/http/SnsCustomMultiPartEntity;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;
    .param p1, "x1"    # Lcom/sec/android/app/sns3/svc/http/SnsCustomMultiPartEntity;

    .prologue
    .line 110
    iput-object p1, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;->mReqEntity:Lcom/sec/android/app/sns3/svc/http/SnsCustomMultiPartEntity;

    return-object p1
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 149
    const-string v0, "SNS"

    const-string v1, "SnsFbService : onBind()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 151
    const-class v0, Lcom/sec/android/app/sns3/svc/sp/facebook/api/ISnsFacebook;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 152
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;->mSnsSvcFacebookBinder:Lcom/sec/android/app/sns3/svc/sp/facebook/api/ISnsFacebook$Stub;

    .line 155
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;->mBinder:Landroid/os/IBinder;

    goto :goto_0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 141
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 142
    const-string v0, "SNS"

    const-string v1, "SnsFbService : onCreate()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 144
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/SnsApplication;->getSvcMgr()Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    .line 145
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 160
    const-string v0, "SNS"

    const-string v1, "SnsFbService : onStartCommand()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 162
    const/4 v0, 0x1

    return v0
.end method
