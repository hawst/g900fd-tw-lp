.class public final enum Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$CompanyUpdateSubType;
.super Ljava/lang/Enum;
.source "SnsLiResponseUpdate.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "CompanyUpdateSubType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$CompanyUpdateSubType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$CompanyUpdateSubType;

.field public static final enum COMPANY_JOB_UPDATE:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$CompanyUpdateSubType;

.field public static final enum COMPANY_PERSON_UPDATE:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$CompanyUpdateSubType;

.field public static final enum COMPANY_PRODUCT_UPDATE:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$CompanyUpdateSubType;

.field public static final enum COMPANY_PROFILE_UPDATE:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$CompanyUpdateSubType;

.field public static final enum COMPANY_STATUS_UPDATE:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$CompanyUpdateSubType;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 160
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$CompanyUpdateSubType;

    const-string v1, "COMPANY_STATUS_UPDATE"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$CompanyUpdateSubType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$CompanyUpdateSubType;->COMPANY_STATUS_UPDATE:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$CompanyUpdateSubType;

    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$CompanyUpdateSubType;

    const-string v1, "COMPANY_PROFILE_UPDATE"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$CompanyUpdateSubType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$CompanyUpdateSubType;->COMPANY_PROFILE_UPDATE:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$CompanyUpdateSubType;

    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$CompanyUpdateSubType;

    const-string v1, "COMPANY_PERSON_UPDATE"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$CompanyUpdateSubType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$CompanyUpdateSubType;->COMPANY_PERSON_UPDATE:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$CompanyUpdateSubType;

    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$CompanyUpdateSubType;

    const-string v1, "COMPANY_JOB_UPDATE"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$CompanyUpdateSubType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$CompanyUpdateSubType;->COMPANY_JOB_UPDATE:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$CompanyUpdateSubType;

    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$CompanyUpdateSubType;

    const-string v1, "COMPANY_PRODUCT_UPDATE"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$CompanyUpdateSubType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$CompanyUpdateSubType;->COMPANY_PRODUCT_UPDATE:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$CompanyUpdateSubType;

    .line 159
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$CompanyUpdateSubType;

    sget-object v1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$CompanyUpdateSubType;->COMPANY_STATUS_UPDATE:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$CompanyUpdateSubType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$CompanyUpdateSubType;->COMPANY_PROFILE_UPDATE:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$CompanyUpdateSubType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$CompanyUpdateSubType;->COMPANY_PERSON_UPDATE:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$CompanyUpdateSubType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$CompanyUpdateSubType;->COMPANY_JOB_UPDATE:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$CompanyUpdateSubType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$CompanyUpdateSubType;->COMPANY_PRODUCT_UPDATE:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$CompanyUpdateSubType;

    aput-object v1, v0, v6

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$CompanyUpdateSubType;->$VALUES:[Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$CompanyUpdateSubType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 159
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$CompanyUpdateSubType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 159
    const-class v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$CompanyUpdateSubType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$CompanyUpdateSubType;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$CompanyUpdateSubType;
    .locals 1

    .prologue
    .line 159
    sget-object v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$CompanyUpdateSubType;->$VALUES:[Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$CompanyUpdateSubType;

    invoke-virtual {v0}, [Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$CompanyUpdateSubType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$CompanyUpdateSubType;

    return-object v0
.end method
