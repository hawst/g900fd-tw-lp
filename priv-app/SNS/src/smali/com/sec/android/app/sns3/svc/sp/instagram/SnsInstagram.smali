.class public Lcom/sec/android/app/sns3/svc/sp/instagram/SnsInstagram;
.super Ljava/lang/Object;
.source "SnsInstagram.java"


# static fields
.field public static final ACCOUNT_TYPE:Ljava/lang/String; = "com.sec.android.app.sns3.instagram"

.field public static final BUNDLE_KEY_AFTER:Ljava/lang/String; = "after"

.field public static final BUNDLE_KEY_LIMIT:Ljava/lang/String; = "count"

.field public static final BUNDLE_KEY_START:Ljava/lang/String; = "start"

.field public static final BUNDLE_KEY_WHERE:Ljava/lang/String; = "where"

.field public static final CANCEL_URI:Ljava/lang/String; = "http://localhost/cancel"

.field public static final CLIENT_ID:Ljava/lang/String; = "128daeba356a4f29bec3a8649c236be2"

.field public static final CLIENT_SECRET:Ljava/lang/String; = "f157e433c597496ebe9fefb46f605af9"

.field public static final GALLERY_AUTHORITY:Ljava/lang/String; = "com.sec.android.gallery3d.instagram.contentprovider"

.field public static final GALLERY_SUPPORT_PATH:Ljava/lang/String; = "/support_instagram"

.field public static final REDIRECT_URI:Ljava/lang/String; = "http://localhost/success"

.field public static final REST_URL:Ljava/lang/String; = "https://instagram.com/oauth/authorize/"

.field public static final REST_URL_FEED:Ljava/lang/String; = "https://api.instagram.com/v1/"

.field public static final REST_URL_PEOPLE:Ljava/lang/String; = "https://api.instagram.com/v1/users/"

.field public static final SP:Ljava/lang/String; = "instagram"

.field public static final USER_DENIED_URI:Ljava/lang/String; = "http://localhost/success/?error=access_denied"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
