.class public Lcom/sec/android/app/sns3/svc/sp/instagram/parser/SnsInParserFeed;
.super Ljava/lang/Object;
.source "SnsInParserFeed.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/instagram/parser/SnsInParserFeed$InstagramFeed;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    return-void
.end method

.method public static parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseFeedList;
    .locals 17
    .param p0, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 82
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 84
    .local v6, "feedlist":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseFeed;>;"
    :try_start_0
    new-instance v9, Lorg/json/JSONObject;

    move-object/from16 v0, p0

    invoke-direct {v9, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 85
    .local v9, "job":Lorg/json/JSONObject;
    const-string v16, "data"

    move-object/from16 v0, v16

    invoke-virtual {v9, v0}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/json/JSONArray;

    .line 87
    .local v3, "datObj":Lorg/json/JSONArray;
    if-eqz v3, :cond_4

    .line 88
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_0
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v16

    move/from16 v0, v16

    if-ge v8, v0, :cond_4

    .line 90
    new-instance v7, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseFeed;

    invoke-direct {v7}, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseFeed;-><init>()V

    .line 91
    .local v7, "feedobj":Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseFeed;
    invoke-virtual {v3, v8}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v5

    .line 93
    .local v5, "feed":Lorg/json/JSONObject;
    const-string v16, "id"

    move-object/from16 v0, v16

    invoke-virtual {v5, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    iput-object v0, v7, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseFeed;->mPostId:Ljava/lang/String;

    .line 94
    const-string v16, "created_time"

    move-object/from16 v0, v16

    invoke-virtual {v5, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    iput-object v0, v7, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseFeed;->mCreatedTime:Ljava/lang/String;

    .line 95
    const-string v16, "type"

    move-object/from16 v0, v16

    invoke-virtual {v5, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    iput-object v0, v7, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseFeed;->mType:Ljava/lang/String;

    .line 96
    const-string v16, "link"

    move-object/from16 v0, v16

    invoke-virtual {v5, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    iput-object v0, v7, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseFeed;->mLink:Ljava/lang/String;

    .line 98
    const-string v16, "images"

    move-object/from16 v0, v16

    invoke-virtual {v5, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v12

    .line 100
    .local v12, "object":Lorg/json/JSONObject;
    const-string v16, "standard_resolution"

    move-object/from16 v0, v16

    invoke-virtual {v12, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v13

    .line 101
    .local v13, "resolution":Lorg/json/JSONObject;
    const-string v16, "url"

    move-object/from16 v0, v16

    invoke-virtual {v13, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    iput-object v0, v7, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseFeed;->mUrl:Ljava/lang/String;

    .line 102
    const-string v16, "width"

    move-object/from16 v0, v16

    invoke-virtual {v13, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    iput-object v0, v7, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseFeed;->mWidth:Ljava/lang/String;

    .line 103
    const-string v16, "height"

    move-object/from16 v0, v16

    invoke-virtual {v13, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    iput-object v0, v7, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseFeed;->mHeight:Ljava/lang/String;

    .line 105
    const-string v16, "thumbnail"

    move-object/from16 v0, v16

    invoke-virtual {v12, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v14

    .line 106
    .local v14, "thumbnail":Lorg/json/JSONObject;
    const-string v16, "url"

    move-object/from16 v0, v16

    invoke-virtual {v14, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    iput-object v0, v7, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseFeed;->mThumbnailUrl:Ljava/lang/String;

    .line 108
    const-string v16, "user"

    move-object/from16 v0, v16

    invoke-virtual {v5, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v15

    .line 109
    .local v15, "user":Lorg/json/JSONObject;
    const-string v16, "username"

    invoke-virtual/range {v15 .. v16}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    iput-object v0, v7, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseFeed;->mUserName:Ljava/lang/String;

    .line 110
    const-string v16, "id"

    invoke-virtual/range {v15 .. v16}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    iput-object v0, v7, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseFeed;->mUserId:Ljava/lang/String;

    .line 112
    const-string v16, "caption"

    move-object/from16 v0, v16

    invoke-virtual {v5, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    .line 113
    .local v1, "caption":Lorg/json/JSONObject;
    if-eqz v1, :cond_0

    .line 115
    const-string v16, "text"

    move-object/from16 v0, v16

    invoke-virtual {v1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    iput-object v0, v7, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseFeed;->mCaption:Ljava/lang/String;

    .line 118
    :cond_0
    const-string v16, "location"

    move-object/from16 v0, v16

    invoke-virtual {v5, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v11

    .line 119
    .local v11, "location":Lorg/json/JSONObject;
    if-eqz v11, :cond_1

    .line 120
    const-string v16, "name"

    move-object/from16 v0, v16

    invoke-virtual {v11, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    iput-object v0, v7, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseFeed;->mLocName:Ljava/lang/String;

    .line 121
    const-string v16, "latitude"

    move-object/from16 v0, v16

    invoke-virtual {v11, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    iput-object v0, v7, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseFeed;->mLatitude:Ljava/lang/String;

    .line 122
    const-string v16, "longitude"

    move-object/from16 v0, v16

    invoke-virtual {v11, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    iput-object v0, v7, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseFeed;->mLongitude:Ljava/lang/String;

    .line 125
    :cond_1
    const-string v16, "likes"

    move-object/from16 v0, v16

    invoke-virtual {v5, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v10

    .line 126
    .local v10, "likes":Lorg/json/JSONObject;
    if-eqz v10, :cond_2

    .line 127
    const-string v16, "count"

    move-object/from16 v0, v16

    invoke-virtual {v10, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v16

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    move-object/from16 v0, v16

    iput-object v0, v7, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseFeed;->mLikesCount:Ljava/lang/Integer;

    .line 130
    :cond_2
    const-string v16, "comments"

    move-object/from16 v0, v16

    invoke-virtual {v5, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    .line 131
    .local v2, "comments":Lorg/json/JSONObject;
    if-eqz v2, :cond_3

    .line 132
    const-string v16, "count"

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v16

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    move-object/from16 v0, v16

    iput-object v0, v7, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseFeed;->mCommentsCount:Ljava/lang/Integer;

    .line 135
    :cond_3
    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 88
    add-int/lit8 v8, v8, 0x1

    goto/16 :goto_0

    .line 140
    .end local v1    # "caption":Lorg/json/JSONObject;
    .end local v2    # "comments":Lorg/json/JSONObject;
    .end local v3    # "datObj":Lorg/json/JSONArray;
    .end local v5    # "feed":Lorg/json/JSONObject;
    .end local v7    # "feedobj":Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseFeed;
    .end local v8    # "i":I
    .end local v9    # "job":Lorg/json/JSONObject;
    .end local v10    # "likes":Lorg/json/JSONObject;
    .end local v11    # "location":Lorg/json/JSONObject;
    .end local v12    # "object":Lorg/json/JSONObject;
    .end local v13    # "resolution":Lorg/json/JSONObject;
    .end local v14    # "thumbnail":Lorg/json/JSONObject;
    .end local v15    # "user":Lorg/json/JSONObject;
    :catch_0
    move-exception v4

    .line 141
    .local v4, "e1":Lorg/json/JSONException;
    invoke-virtual {v4}, Lorg/json/JSONException;->printStackTrace()V

    .line 143
    .end local v4    # "e1":Lorg/json/JSONException;
    :cond_4
    new-instance v16, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseFeedList;

    move-object/from16 v0, v16

    invoke-direct {v0, v6}, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseFeedList;-><init>(Ljava/util/List;)V

    return-object v16
.end method
