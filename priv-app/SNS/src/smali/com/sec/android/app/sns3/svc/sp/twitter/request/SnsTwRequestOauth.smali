.class public abstract Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestOauth;
.super Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestBase;
.source "SnsTwRequestOauth.java"

# interfaces
.implements Lcom/sec/android/app/sns3/svc/sp/twitter/callback/ISnsTwReqCb;


# instance fields
.field private mOAuth:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwOauthAPI;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwOauthAPI;)V
    .locals 1
    .param p1, "svcMgr"    # Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    .param p2, "oAuth"    # Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwOauthAPI;

    .prologue
    .line 40
    const/16 v0, 0x14

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestBase;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;I)V

    .line 41
    invoke-virtual {p2}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwOauthAPI;->getUrl()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestOauth;->mUrl:Ljava/lang/String;

    .line 42
    invoke-virtual {p2}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwOauthAPI;->getHttpMethod()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestOauth;->mHttpMethod:Ljava/lang/String;

    .line 43
    iput-object p2, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestOauth;->mOAuth:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwOauthAPI;

    .line 44
    return-void
.end method


# virtual methods
.method protected composeMore()V
    .locals 6

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestOauth;->mOAuth:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwOauthAPI;

    sget-object v1, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwOauthAPI;->OAUTH_REQUEST_TOKEN:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwOauthAPI;

    if-ne v0, v1, :cond_1

    .line 50
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestOauth;->mParams:Landroid/os/Bundle;

    const-string v1, "oauth_callback"

    const-string v2, "http://www.facebook.com/SamsungMobile"

    invoke-static {v2}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->encodeUrlEx(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 52
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestOauth;->mHttpMethod:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestOauth;->mReqUri:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestOauth;->mParams:Landroid/os/Bundle;

    iget-object v3, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestOauth;->mSubParams:Landroid/os/Bundle;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwitter;->CONSUMER_SECRET:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "&"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v1, v2, v3, v4}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/composer/SnsTwComposerUtils;->generateSignature(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestOauth;->mSig:Ljava/lang/String;

    .line 65
    :cond_0
    :goto_0
    return-void

    .line 55
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestOauth;->mOAuth:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwOauthAPI;

    sget-object v1, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwOauthAPI;->OAUTH_ACCESS_TOKEN:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwOauthAPI;

    if-ne v0, v1, :cond_0

    .line 57
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestOauth;->mParams:Landroid/os/Bundle;

    const-string v1, "oauth_token"

    iget-object v2, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestOauth;->mToken:Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;

    invoke-virtual {v2}, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;->getUserAuthToken()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->encodeUrlEx(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestOauth;->mParams:Landroid/os/Bundle;

    const-string v1, "oauth_verifier"

    iget-object v2, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestOauth;->mToken:Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;

    invoke-virtual {v2}, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;->getUserAuthVerifier()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->encodeUrlEx(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 61
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestOauth;->mHttpMethod:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestOauth;->mReqUri:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestOauth;->mParams:Landroid/os/Bundle;

    iget-object v3, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestOauth;->mSubParams:Landroid/os/Bundle;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwitter;->CONSUMER_SECRET:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "&"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestOauth;->mToken:Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;

    invoke-virtual {v5}, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;->getUnauthTokenSecret()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v1, v2, v3, v4}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/composer/SnsTwComposerUtils;->generateSignature(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestOauth;->mSig:Ljava/lang/String;

    goto :goto_0
.end method

.method protected parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;
    .locals 6
    .param p1, "content"    # Ljava/lang/String;

    .prologue
    .line 69
    const-string v2, "SNS"

    const-string v3, "<SnsTwRequestOauth> parse()"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 71
    iget-object v2, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestOauth;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    invoke-virtual {v2}, Lcom/sec/android/app/sns3/svc/SnsSvcMgr;->getTokenMgr()Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;

    move-result-object v2

    const-string v3, "twitter"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;->getToken(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;

    .line 72
    .local v1, "token":Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;
    invoke-static {p1}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->decodeUrl2Bundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 74
    .local v0, "TokenBundle":Landroid/os/Bundle;
    iget-object v2, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestOauth;->mOAuth:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwOauthAPI;

    sget-object v3, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwOauthAPI;->OAUTH_REQUEST_TOKEN:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwOauthAPI;

    if-ne v2, v3, :cond_1

    .line 76
    const-string v2, "oauth_token"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;->setUnauthToken(Ljava/lang/String;)V

    .line 77
    const-string v2, "oauth_token_secret"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;->setUnauthTokenSecret(Ljava/lang/String;)V

    .line 86
    :cond_0
    :goto_0
    const/4 v2, 0x0

    return-object v2

    .line 79
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestOauth;->mOAuth:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwOauthAPI;

    sget-object v3, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwOauthAPI;->OAUTH_ACCESS_TOKEN:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwOauthAPI;

    if-ne v2, v3, :cond_0

    .line 81
    const-string v2, "oauth_token"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "oauth_token_secret"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "user_id"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "screen_name"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;->setTokenInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected respond(Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;)Z
    .locals 7
    .param p1, "result"    # Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;

    .prologue
    .line 91
    const-string v0, "SNS"

    const-string v1, "<SnsTwRequestOauth> respond()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 93
    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->getReqID()I

    move-result v1

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->isSuccess()Z

    move-result v2

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->getHttpstatus()I

    move-result v3

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->getErrorCode()I

    move-result v4

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->getReason()Landroid/os/Bundle;

    move-result-object v5

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->getResponse()Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;

    move-result-object v6

    move-object v0, p0

    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestOauth;->onReqRespond(IZIILandroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;)Z

    .line 96
    const/4 v0, 0x1

    return v0
.end method

.method protected setUrl()Ljava/lang/String;
    .locals 2

    .prologue
    .line 101
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 103
    .local v0, "sb":Ljava/lang/StringBuilder;
    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestOauth;->mUrl:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 105
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
