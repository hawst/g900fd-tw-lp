.class Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAttachment$1;
.super Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqGetAttachment;
.source "SnsFbCmdGetAttachment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAttachment;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Handler;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAttachment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAttachment;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;)V
    .locals 0
    .param p2, "x0"    # Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    .param p3, "x1"    # Ljava/lang/String;

    .prologue
    .line 47
    iput-object p1, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAttachment$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAttachment;

    invoke-direct {p0, p2, p3}, Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqGetAttachment;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public onReqRespond(IZIILandroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAttachment;)Z
    .locals 4
    .param p1, "reqID"    # I
    .param p2, "bSuccess"    # Z
    .param p3, "httpStatus"    # I
    .param p4, "errorCode"    # I
    .param p5, "reason"    # Landroid/os/Bundle;
    .param p6, "attachment"    # Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAttachment;

    .prologue
    .line 53
    if-eqz p2, :cond_1

    .line 54
    if-eqz p6, :cond_0

    .line 56
    move-object v0, p6

    .line 57
    .local v0, "curAttachment":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAttachment;
    :goto_0
    if-eqz v0, :cond_0

    .line 58
    new-instance p5, Landroid/os/Bundle;

    .end local p5    # "reason":Landroid/os/Bundle;
    invoke-direct {p5}, Landroid/os/Bundle;-><init>()V

    .line 60
    .restart local p5    # "reason":Landroid/os/Bundle;
    const-string v1, "photo_id"

    iget-object v2, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAttachment;->mPhotoID:Ljava/lang/String;

    invoke-virtual {p5, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 61
    const-string v1, "post_id"

    iget-object v2, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAttachment$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAttachment;

    # getter for: Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAttachment;->mPostID:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAttachment;->access$000(Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAttachment;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p5, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    iget-object v1, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAttachment$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAttachment;

    new-instance v2, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;

    const-string v3, "facebook"

    invoke-direct {v2, v3, p3, p4, p5}, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;-><init>(Ljava/lang/String;IILandroid/os/Bundle;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAttachment;->setResponseList(Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;)Z

    .line 66
    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAttachment;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAttachment;

    goto :goto_0

    .line 69
    .end local v0    # "curAttachment":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAttachment;
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAttachment$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAttachment;

    sget-object v2, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$AlbumContents;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAttachment;->setUri(Ljava/lang/String;)V

    .line 76
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAttachment$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAttachment;

    invoke-virtual {v1, p2}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAttachment;->setSuccess(Z)V

    .line 77
    iget-object v1, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAttachment$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAttachment;

    invoke-virtual {v1, p0}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAttachment;->receive(Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;)V

    .line 79
    const/4 v1, 0x1

    return v1

    .line 71
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAttachment$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAttachment;

    new-instance v2, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;

    const-string v3, "facebook"

    invoke-direct {v2, v3, p3, p4, p5}, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;-><init>(Ljava/lang/String;IILandroid/os/Bundle;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAttachment;->setResponseList(Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;)Z

    .line 73
    iget-object v1, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAttachment$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAttachment;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAttachment;->setUri(Ljava/lang/String;)V

    goto :goto_1
.end method
