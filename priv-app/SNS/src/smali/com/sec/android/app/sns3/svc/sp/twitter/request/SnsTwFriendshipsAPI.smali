.class public final enum Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwFriendshipsAPI;
.super Ljava/lang/Enum;
.source "SnsTwFriendshipsAPI.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwFriendshipsAPI$Param;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwFriendshipsAPI;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwFriendshipsAPI;

.field public static final enum FRIENDSHIPS_CREATE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwFriendshipsAPI;

.field public static final enum FRIENDSHIPS_DESTROY:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwFriendshipsAPI;


# instance fields
.field private mHttpMethod:Ljava/lang/String;

.field private mUrl:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 27
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwFriendshipsAPI;

    const-string v1, "FRIENDSHIPS_CREATE"

    const-string v2, "/friendships/create.json"

    const-string v3, "POST"

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwFriendshipsAPI;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwFriendshipsAPI;->FRIENDSHIPS_CREATE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwFriendshipsAPI;

    .line 28
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwFriendshipsAPI;

    const-string v1, "FRIENDSHIPS_DESTROY"

    const-string v2, "/friendships/destroy.json"

    const-string v3, "POST"

    invoke-direct {v0, v1, v5, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwFriendshipsAPI;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwFriendshipsAPI;->FRIENDSHIPS_DESTROY:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwFriendshipsAPI;

    .line 25
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwFriendshipsAPI;

    sget-object v1, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwFriendshipsAPI;->FRIENDSHIPS_CREATE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwFriendshipsAPI;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwFriendshipsAPI;->FRIENDSHIPS_DESTROY:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwFriendshipsAPI;

    aput-object v1, v0, v5

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwFriendshipsAPI;->$VALUES:[Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwFriendshipsAPI;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p3, "url"    # Ljava/lang/String;
    .param p4, "httpMethod"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 63
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 64
    iput-object p3, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwFriendshipsAPI;->mUrl:Ljava/lang/String;

    .line 65
    iput-object p4, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwFriendshipsAPI;->mHttpMethod:Ljava/lang/String;

    .line 66
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwFriendshipsAPI;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 25
    const-class v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwFriendshipsAPI;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwFriendshipsAPI;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwFriendshipsAPI;
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwFriendshipsAPI;->$VALUES:[Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwFriendshipsAPI;

    invoke-virtual {v0}, [Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwFriendshipsAPI;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwFriendshipsAPI;

    return-object v0
.end method


# virtual methods
.method public getHttpMethod()Ljava/lang/String;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwFriendshipsAPI;->mHttpMethod:Ljava/lang/String;

    return-object v0
.end method

.method public getUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwFriendshipsAPI;->mUrl:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwFriendshipsAPI;->mUrl:Ljava/lang/String;

    return-object v0
.end method
