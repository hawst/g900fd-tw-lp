.class Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService$SyncAdapterImpl;
.super Landroid/content/AbstractThreadedSyncAdapter;
.source "SnsFbSyncAdapterStreamsService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SyncAdapterImpl"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;Landroid/content/Context;)V
    .locals 1
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 112
    iput-object p1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService$SyncAdapterImpl;->this$0:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;

    .line 113
    const/4 v0, 0x1

    invoke-direct {p0, p2, v0}, Landroid/content/AbstractThreadedSyncAdapter;-><init>(Landroid/content/Context;Z)V

    .line 114
    # setter for: Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mContext:Landroid/content/Context;
    invoke-static {p1, p2}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->access$002(Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;Landroid/content/Context;)Landroid/content/Context;

    .line 115
    return-void
.end method


# virtual methods
.method public onPerformSync(Landroid/accounts/Account;Landroid/os/Bundle;Ljava/lang/String;Landroid/content/ContentProviderClient;Landroid/content/SyncResult;)V
    .locals 4
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "extras"    # Landroid/os/Bundle;
    .param p3, "authority"    # Ljava/lang/String;
    .param p4, "provider"    # Landroid/content/ContentProviderClient;
    .param p5, "syncResult"    # Landroid/content/SyncResult;

    .prologue
    .line 121
    # getter for: Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->access$100()Ljava/lang/String;

    move-result-object v1

    const-string v2, "***************** SnsFbSyncAdapterStreamsService : onPerformSync!!! *****************"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 123
    iget-object v2, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService$SyncAdapterImpl;->this$0:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;

    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/sns3/SnsApplication;->getSvcMgr()Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/sns3/svc/SnsSvcMgr;->getTokenMgr()Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;

    move-result-object v1

    const-string v3, "facebook"

    invoke-virtual {v1, v3}, Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;->getToken(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbToken;

    # setter for: Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mFbToken:Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbToken;
    invoke-static {v2, v1}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->access$202(Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbToken;)Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbToken;

    .line 126
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService$SyncAdapterImpl;->this$0:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;

    # setter for: Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mAccount:Landroid/accounts/Account;
    invoke-static {v1, p1}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->access$302(Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;Landroid/accounts/Account;)Landroid/accounts/Account;

    .line 127
    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService$SyncAdapterImpl;->this$0:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;

    # setter for: Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mAuthority:Ljava/lang/String;
    invoke-static {v1, p3}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->access$402(Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;Ljava/lang/String;)Ljava/lang/String;

    .line 128
    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService$SyncAdapterImpl;->this$0:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;

    # setter for: Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mSyncResult:Landroid/content/SyncResult;
    invoke-static {v1, p5}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->access$502(Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;Landroid/content/SyncResult;)Landroid/content/SyncResult;

    .line 130
    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService$SyncAdapterImpl;->this$0:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;

    # invokes: Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->performSync()V
    invoke-static {v1}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->access$600(Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;)V
    :try_end_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 138
    :goto_0
    return-void

    .line 132
    :catch_0
    move-exception v0

    .line 133
    .local v0, "e":Landroid/accounts/OperationCanceledException;
    # getter for: Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->access$100()Ljava/lang/String;

    move-result-object v1

    const-string v2, "SnsFbSyncAdapterStreamsService : onPerformSync is CANCELED!!!"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 134
    .end local v0    # "e":Landroid/accounts/OperationCanceledException;
    :catch_1
    move-exception v0

    .line 135
    .local v0, "e":Ljava/lang/Exception;
    # getter for: Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->access$100()Ljava/lang/String;

    move-result-object v1

    const-string v2, "SnsFbSyncAdapterStreamsService : Abnormal Syncing!!!"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onSyncCanceled()V
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService$SyncAdapterImpl;->this$0:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;

    # invokes: Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->onSyncCanceled()V
    invoke-static {v0}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->access$700(Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;)V

    .line 143
    return-void
.end method
