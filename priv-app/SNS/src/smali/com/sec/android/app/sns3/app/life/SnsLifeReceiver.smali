.class public Lcom/sec/android/app/sns3/app/life/SnsLifeReceiver;
.super Landroid/content/BroadcastReceiver;
.source "SnsLifeReceiver.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 31
    sget-object v0, Lcom/sec/android/app/sns3/app/life/SnsLifeResource;->TAG:Ljava/lang/String;

    const-string v1, "SnsLifeReceiver - onReceive called"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 33
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.sec.android.app.sns.life.ACTION_LIFE_PROFILE_FEED_REQUESTED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 34
    invoke-static {p1}, Lcom/sec/android/app/sns3/app/life/SnsLifeGetFeed;->loadSnsLifeGetFeed(Landroid/content/Context;)V

    .line 36
    :cond_0
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.sec.android.app.sns.life.ACTION_LIFE_LOGGEDIN_USER_REQUESTED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 37
    invoke-static {p1}, Lcom/sec/android/app/sns3/app/life/SnsLifeGetUserID;->loadSnsLifeCmdGetUserID(Landroid/content/Context;)V

    .line 39
    :cond_1
    return-void
.end method
