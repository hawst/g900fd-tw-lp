.class public interface abstract Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserEvent$FacebookEvent;
.super Ljava/lang/Object;
.source "SnsFbParserEvent.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "FacebookEvent"
.end annotation


# static fields
.field public static final CITY:Ljava/lang/String; = "city"

.field public static final COUNTRY:Ljava/lang/String; = "country"

.field public static final DESCRIPTION:Ljava/lang/String; = "description"

.field public static final END_TIME:Ljava/lang/String; = "end_time"

.field public static final EVENT_NAME:Ljava/lang/String; = "name"

.field public static final ID:Ljava/lang/String; = "id"

.field public static final LATITUDE:Ljava/lang/String; = "latitude"

.field public static final LOCATION:Ljava/lang/String; = "location"

.field public static final LONGITUDE:Ljava/lang/String; = "longitude"

.field public static final OWNER:Ljava/lang/String; = "owner"

.field public static final PRIVACY:Ljava/lang/String; = "privacy"

.field public static final RSVP_STATUS:Ljava/lang/String; = "rsvp_status"

.field public static final START_TIME:Ljava/lang/String; = "start_time"

.field public static final STATE:Ljava/lang/String; = "state"

.field public static final STREET:Ljava/lang/String; = "street"

.field public static final TYPE:Ljava/lang/String; = "type"

.field public static final UPDATED_TIME:Ljava/lang/String; = "updated_time"

.field public static final VENUE:Ljava/lang/String; = "venue"

.field public static final ZIP:Ljava/lang/String; = "zip"
