.class public interface abstract Lcom/sec/android/app/sns3/svc/sp/sinaweibo/parser/SnsSwParserUserInfo$User;
.super Ljava/lang/Object;
.source "SnsSwParserUserInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/svc/sp/sinaweibo/parser/SnsSwParserUserInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "User"
.end annotation


# static fields
.field public static final GENDER:Ljava/lang/String; = "gender"

.field public static final NAME:Ljava/lang/String; = "name"

.field public static final PROFILE_URL:Ljava/lang/String; = "profile_image_url"

.field public static final SCREEN_NAME:Ljava/lang/String; = "screen_name"
