.class Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService$1;
.super Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqGetPosts;
.source "SnsFbSyncAdapterStreamsService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->performSync()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;


# direct methods
.method constructor <init>(Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;)V
    .locals 0
    .param p2, "x0"    # Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    .param p3, "x1"    # Ljava/lang/String;

    .prologue
    .line 207
    iput-object p1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService$1;->this$0:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;

    invoke-direct {p0, p2, p3}, Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqGetPosts;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public onReqRespond(IZIILandroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePosts;)Z
    .locals 6
    .param p1, "reqID"    # I
    .param p2, "bSuccess"    # Z
    .param p3, "httpStatus"    # I
    .param p4, "errorCode"    # I
    .param p5, "reason"    # Landroid/os/Bundle;
    .param p6, "posts"    # Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePosts;

    .prologue
    const/4 v5, -0x1

    .line 212
    if-eqz p2, :cond_0

    .line 213
    new-instance v1, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;

    invoke-direct {v1}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;-><init>()V

    .line 215
    .local v1, "fbSyncDataMgr":Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService$1;->this$0:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;

    iget-object v3, p6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePosts;->mPosts:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;

    # setter for: Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->fbResponsePost:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;
    invoke-static {v2, v3}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->access$802(Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;

    .line 216
    invoke-virtual {v1, p6}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->insertPosts(Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePosts;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 230
    .end local v1    # "fbSyncDataMgr":Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService$1;->this$0:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;

    # getter for: Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;
    invoke-static {v2}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->access$1000(Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;)Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->resumeSync()V

    .line 231
    const/4 v2, 0x1

    return v2

    .line 218
    .restart local v1    # "fbSyncDataMgr":Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;
    :catch_0
    move-exception v0

    .line 219
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0

    .line 220
    .end local v0    # "e":Ljava/lang/NullPointerException;
    :catch_1
    move-exception v0

    .line 221
    .local v0, "e":Ljava/lang/Exception;
    iget-object v2, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService$1;->this$0:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;

    # setter for: Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mSyncState:I
    invoke-static {v2, v5}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->access$902(Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;I)I

    .line 222
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 225
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v1    # "fbSyncDataMgr":Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;
    :cond_0
    # getter for: Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->access$100()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SnsFbSyncAdapterStreamsService errorCode : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", reason : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 228
    iget-object v2, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService$1;->this$0:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;

    # setter for: Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mSyncState:I
    invoke-static {v2, v5}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->access$902(Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;I)I

    goto :goto_0
.end method
