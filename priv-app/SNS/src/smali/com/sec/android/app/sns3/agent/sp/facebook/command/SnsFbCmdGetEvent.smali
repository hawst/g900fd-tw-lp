.class public Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetEvent;
.super Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;
.source "SnsFbCmdGetEvent.java"


# instance fields
.field private mEventID:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Handler;Ljava/lang/String;)V
    .locals 3
    .param p1, "svcMgr"    # Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    .param p2, "cmdHandler"    # Landroid/os/Handler;
    .param p3, "eventID"    # Ljava/lang/String;

    .prologue
    .line 46
    invoke-direct {p0, p2}, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;-><init>(Landroid/os/Handler;)V

    .line 48
    new-instance v1, Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;

    invoke-direct {v1}, Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;-><init>()V

    .line 50
    .local v1, "unit":Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;
    iput-object p3, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetEvent;->mEventID:Ljava/lang/String;

    .line 52
    new-instance v0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetEvent$1;

    iget-object v2, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetEvent;->mEventID:Ljava/lang/String;

    invoke-direct {v0, p0, p1, v2}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetEvent$1;-><init>(Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetEvent;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;)V

    .line 122
    .local v0, "req":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    invoke-virtual {v1, v0}, Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;->addRequest(Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;)V

    .line 124
    invoke-virtual {p0, v1}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetEvent;->addCommandUnit(Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;)V

    .line 125
    return-void
.end method


# virtual methods
.method public getEventID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetEvent;->mEventID:Ljava/lang/String;

    return-object v0
.end method

.method protected respond()Z
    .locals 5

    .prologue
    .line 133
    const-string v0, "SnsAgent"

    const-string v1, "<SnsFbCmdGetEvent> respond()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 135
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetEvent;->getCommandCallback()Lcom/sec/android/app/sns3/agent/command/ISnsCommandCallback;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetEvent;->getCommandID()I

    move-result v1

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetEvent;->isSuccess()Z

    move-result v2

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetEvent;->getUri()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetEvent;->getResponseList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/sec/android/app/sns3/agent/command/ISnsCommandCallback;->onCmdRespond(IZLjava/lang/String;Ljava/util/List;)V

    .line 137
    const/4 v0, 0x1

    return v0
.end method
