.class public Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserMyProfile;
.super Ljava/lang/Object;
.source "SnsFbParserMyProfile.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserMyProfile$FacebookMyProfile;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    return-void
.end method


# virtual methods
.method public parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMyProfile;
    .locals 12
    .param p1, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 74
    new-instance v8, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMyProfile;

    invoke-direct {v8}, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMyProfile;-><init>()V

    .line 77
    .local v8, "myProfile":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMyProfile;
    :try_start_0
    new-instance v3, Lorg/json/JSONArray;

    invoke-direct {v3, p1}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 78
    .local v3, "ja":Lorg/json/JSONArray;
    const/4 v9, 0x0

    invoke-virtual {v3, v9}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    .line 80
    .local v4, "jsonObj":Lorg/json/JSONObject;
    const-string v9, "uid"

    invoke-virtual {v4, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v8, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMyProfile;->mId:Ljava/lang/String;

    .line 82
    const-string v9, "relationship_status"

    invoke-virtual {v4, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v8, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMyProfile;->mRelationShipStatus:Ljava/lang/String;

    .line 84
    const-string v9, "online_presence"

    invoke-virtual {v4, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v8, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMyProfile;->mOnlinePresence:Ljava/lang/String;

    .line 86
    const-string v9, "interests"

    invoke-virtual {v4, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v8, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMyProfile;->mInterests:Ljava/lang/String;

    .line 88
    const-string v9, "profile_update_time"

    invoke-virtual {v4, v9}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    iput-object v9, v8, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMyProfile;->mUpdateTime:Ljava/lang/Long;

    .line 90
    const-string v9, "sex"

    invoke-virtual {v4, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v8, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMyProfile;->mGender:Ljava/lang/String;

    .line 91
    const-string v9, "locale"

    invoke-virtual {v4, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v8, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMyProfile;->mLocale:Ljava/lang/String;

    .line 93
    const-string v9, "status"

    invoke-virtual {v4, v9}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_0

    const-string v9, "status"

    invoke-virtual {v4, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    const-string v10, "null"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_0

    .line 96
    new-instance v7, Lorg/json/JSONObject;

    const-string v9, "status"

    invoke-virtual {v4, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v7, v9}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 98
    .local v7, "jsonStatObj":Lorg/json/JSONObject;
    const-string v9, "message"

    invoke-virtual {v7, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v8, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMyProfile;->mCurrentStatus:Ljava/lang/String;

    .line 100
    const-string v9, "time"

    invoke-virtual {v7, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v8, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMyProfile;->mCurrentStatusTime:Ljava/lang/String;

    .line 102
    const-string v9, "comment_count"

    invoke-virtual {v7, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v8, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMyProfile;->mCurrentStatusCommentCount:Ljava/lang/String;

    .line 106
    .end local v7    # "jsonStatObj":Lorg/json/JSONObject;
    :cond_0
    const-string v9, "name"

    invoke-virtual {v4, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v8, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMyProfile;->mName:Ljava/lang/String;

    .line 107
    const-string v9, "email"

    invoke-virtual {v4, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v8, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMyProfile;->mEmail:Ljava/lang/String;

    .line 108
    const-string v9, "pic"

    invoke-virtual {v4, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v8, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMyProfile;->mPic:Ljava/lang/String;

    .line 109
    const-string v9, "birthday_date"

    invoke-virtual {v4, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v8, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMyProfile;->mBirthdayDate:Ljava/lang/String;

    .line 111
    const-string v9, "current_location"

    invoke-virtual {v4, v9}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_1

    const-string v9, "current_location"

    invoke-virtual {v4, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    const-string v10, "null"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_1

    .line 113
    new-instance v6, Lorg/json/JSONObject;

    const-string v9, "current_location"

    invoke-virtual {v4, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v6, v9}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 115
    .local v6, "jsonObject":Lorg/json/JSONObject;
    const-string v9, "name"

    invoke-virtual {v6, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v8, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMyProfile;->mCurrentLocation:Ljava/lang/String;

    .line 118
    .end local v6    # "jsonObject":Lorg/json/JSONObject;
    :cond_1
    iget-object v9, v8, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMyProfile;->mCurrentLocation:Ljava/lang/String;

    if-nez v9, :cond_2

    .line 119
    const-string v9, "hometown_location"

    invoke-virtual {v4, v9}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_2

    const-string v9, "hometown_location"

    invoke-virtual {v4, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    const-string v10, "null"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_2

    .line 121
    new-instance v6, Lorg/json/JSONObject;

    const-string v9, "hometown_location"

    invoke-virtual {v4, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v6, v9}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 123
    .restart local v6    # "jsonObject":Lorg/json/JSONObject;
    const-string v9, "name"

    invoke-virtual {v6, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v8, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMyProfile;->mCurrentLocation:Ljava/lang/String;

    .line 127
    .end local v6    # "jsonObject":Lorg/json/JSONObject;
    :cond_2
    const-string v9, "work"

    invoke-virtual {v4, v9}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 128
    new-instance v2, Lorg/json/JSONArray;

    const-string v9, "work"

    invoke-virtual {v4, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v2, v9}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 130
    .local v2, "jArrWork":Lorg/json/JSONArray;
    if-eqz v2, :cond_3

    .line 131
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v9

    if-ge v1, v9, :cond_3

    .line 133
    invoke-virtual {v2, v1}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v5

    .line 135
    .local v5, "jsonObj_work":Lorg/json/JSONObject;
    const-string v9, "employer"

    invoke-virtual {v5, v9}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 136
    new-instance v6, Lorg/json/JSONObject;

    const-string v9, "employer"

    invoke-virtual {v5, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v6, v9}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 139
    .restart local v6    # "jsonObject":Lorg/json/JSONObject;
    const-string v9, "name"

    invoke-virtual {v6, v9}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_4

    const-string v9, "name"

    invoke-virtual {v6, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    const-string v10, "null"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_4

    .line 141
    const-string v9, "name"

    invoke-virtual {v6, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v8, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMyProfile;->mWork:Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 155
    .end local v1    # "i":I
    .end local v2    # "jArrWork":Lorg/json/JSONArray;
    .end local v3    # "ja":Lorg/json/JSONArray;
    .end local v4    # "jsonObj":Lorg/json/JSONObject;
    .end local v5    # "jsonObj_work":Lorg/json/JSONObject;
    .end local v6    # "jsonObject":Lorg/json/JSONObject;
    :cond_3
    :goto_1
    return-object v8

    .line 131
    .restart local v1    # "i":I
    .restart local v2    # "jArrWork":Lorg/json/JSONArray;
    .restart local v3    # "ja":Lorg/json/JSONArray;
    .restart local v4    # "jsonObj":Lorg/json/JSONObject;
    .restart local v5    # "jsonObj_work":Lorg/json/JSONObject;
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 151
    .end local v1    # "i":I
    .end local v2    # "jArrWork":Lorg/json/JSONArray;
    .end local v3    # "ja":Lorg/json/JSONArray;
    .end local v4    # "jsonObj":Lorg/json/JSONObject;
    .end local v5    # "jsonObj_work":Lorg/json/JSONObject;
    :catch_0
    move-exception v0

    .line 152
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_1
.end method
