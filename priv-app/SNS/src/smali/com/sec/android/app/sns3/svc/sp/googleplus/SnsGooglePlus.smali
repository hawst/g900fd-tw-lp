.class public Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGooglePlus;
.super Ljava/lang/Object;
.source "SnsGooglePlus.java"


# static fields
.field public static final ACCOUNT_TYPE:Ljava/lang/String; = "com.sec.android.app.sns3.googleplus"

.field public static final API_KEY:Ljava/lang/String; = "AIzaSyB0d_BAwH-HHwqdBAtgs8wKlFxd7OxhPzQ"

.field public static final BUNDLE_KEY_AFTER:Ljava/lang/String; = "after"

.field public static final BUNDLE_KEY_LIMIT:Ljava/lang/String; = "count"

.field public static final BUNDLE_KEY_START:Ljava/lang/String; = "start"

.field public static final BUNDLE_KEY_WHERE:Ljava/lang/String; = "where"

.field public static final CLIENT_ID:Ljava/lang/String;

.field public static final CLIENT_SECRET:Ljava/lang/String;

.field public static final GOOGLE_ACCOUNT_TYPE:Ljava/lang/String; = "com.google"

.field public static final REDIRECT_URI:Ljava/lang/String; = "http://localhost"

.field public static final REST_URL:Ljava/lang/String; = "https://www.googleapis.com/plus/v1/"

.field public static final REST_URL_FEED:Ljava/lang/String; = "https://www.googleapis.com/plus/v1/people/me/activities/public?key="

.field public static final REST_URL_PEOPLE:Ljava/lang/String; = "https://www.googleapis.com/plus/v1/people/me?access_token="

.field public static final REST_URL_PROFILE:Ljava/lang/String; = "https://www.googleapis.com/plus/v1/people/me?fields=displayName,birthday,image,placesLived,emails,organizations&access_token="

.field public static final SCOPE:Ljava/lang/String; = "https://www.googleapis.com/auth/plus.login"

.field public static final SP:Ljava/lang/String; = "googleplus"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    invoke-static {}, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpAppIdManager;->getInstance()Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpAppIdManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpAppIdManager;->getClientId()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGooglePlus;->CLIENT_ID:Ljava/lang/String;

    .line 29
    invoke-static {}, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpAppIdManager;->getInstance()Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpAppIdManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpAppIdManager;->getClientSecret()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGooglePlus;->CLIENT_SECRET:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
