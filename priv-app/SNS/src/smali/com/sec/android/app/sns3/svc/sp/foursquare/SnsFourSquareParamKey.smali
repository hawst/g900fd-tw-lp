.class public Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFourSquareParamKey;
.super Ljava/lang/Object;
.source "SnsFourSquareParamKey.java"


# static fields
.field public static final LATITUDE:Ljava/lang/String; = "latitude"

.field public static final LIMIT:Ljava/lang/String; = "limit"

.field public static final LONGITUDE:Ljava/lang/String; = "longitude"

.field public static final NEAR:Ljava/lang/String; = "near"

.field public static final QUERY:Ljava/lang/String; = "query"

.field public static final RADIUS:Ljava/lang/String; = "radius"

.field public static final VENUE_ID:Ljava/lang/String; = "venueId"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
