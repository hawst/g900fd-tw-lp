.class public Lcom/sec/android/app/sns3/agent/sp/qzone/db/SnsQzoneDBHelper;
.super Lcom/sec/android/app/sns3/agent/db/SnsDBHelperBase;
.source "SnsQzoneDBHelper.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 30
    const-string v0, "SnsQzoneDB.db"

    const/4 v1, 0x0

    const/16 v2, 0x14

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/sec/android/app/sns3/agent/db/SnsDBHelperBase;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 31
    return-void
.end method


# virtual methods
.method public createTables(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 49
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 52
    const-string v0, "CREATE TABLE IF NOT EXISTS user (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,user_id VARCHAR(50),username TEXT, UNIQUE (user_id));"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 57
    const-string v0, "SnsAgent"

    const-string v1, "DB TABLE(user) Has been created."

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 60
    const-string v0, "CREATE TABLE IF NOT EXISTS feed_list (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,feed_id VARCHAR(50),type TEXT,profile_url TEXT,title TEXT,message TEXT,author_name TEXT,author_phonenumber TEXT,host_openid TEXT,author_openid TEXT,like_count INTEGER,comment_count INTEGER,create_time TIMESTAMP,picture TEXT,picture_count INTEGER,link_uri TEXT,original_title TEXT,original_message TEXT,original_author_name TEXT,original_picture TEXT,original_picture_count INTEGER, UNIQUE (feed_id));"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 78
    const-string v0, "SnsAgent"

    const-string v1, "DB TABLE(feed_list) Has been created."

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 81
    const-string v0, "CREATE TABLE IF NOT EXISTS birthday (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,friend_openid VARCHAR(50),host_openid VARCHAR(50),name VARCHAR(100),month TIMESTAMP,day TIMESTAMP,pic_url VARCHAR(512),gender TEXT,lunar_flag TEXT, UNIQUE (friend_openid));"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 90
    const-string v0, "SnsAgent"

    const-string v1, "DB TABLE(birthday) Has been created."

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 93
    const-string v0, "CREATE TABLE IF NOT EXISTS usertimeline (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,feed_id VARCHAR(50),author_name TEXT,author_openid TEXT,message TEXT,picture TEXT,create_at TIMESTAMP,link_uri TEXT, UNIQUE (_id));"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 102
    const-string v0, "SnsAgent"

    const-string v1, "DB TABLE(usertimeline) Has been created."

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 104
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 105
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 106
    return-void
.end method

.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 0
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 35
    invoke-super {p0, p1}, Lcom/sec/android/app/sns3/agent/db/SnsDBHelperBase;->onCreate(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 37
    invoke-virtual {p0, p1}, Lcom/sec/android/app/sns3/agent/sp/qzone/db/SnsQzoneDBHelper;->createTables(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 38
    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 0
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "oldVersion"    # I
    .param p3, "newVersion"    # I

    .prologue
    .line 42
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/app/sns3/agent/db/SnsDBHelperBase;->onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V

    .line 44
    invoke-virtual {p0, p1}, Lcom/sec/android/app/sns3/agent/sp/qzone/db/SnsQzoneDBHelper;->upgradeTables(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 46
    return-void
.end method

.method public upgradeTables(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 111
    const-string v0, "DROP TABLE IF EXISTS user"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 112
    const-string v0, "DROP TABLE IF EXISTS feed_list"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 113
    const-string v0, "DROP TABLE IF EXISTS birthday"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 114
    const-string v0, "DROP TABLE IF EXISTS usertimeline"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 116
    invoke-virtual {p0, p1}, Lcom/sec/android/app/sns3/agent/sp/qzone/db/SnsQzoneDBHelper;->createTables(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 117
    return-void
.end method

.method public wipeData(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 121
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 123
    :try_start_0
    const-string v0, "DELETE FROM user"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 124
    const-string v0, "DELETE FROM feed_list"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 125
    const-string v0, "DELETE FROM birthday"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 126
    const-string v0, "DELETE FROM usertimeline"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 128
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 130
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 132
    return-void

    .line 130
    :catchall_0
    move-exception v0

    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0
.end method
