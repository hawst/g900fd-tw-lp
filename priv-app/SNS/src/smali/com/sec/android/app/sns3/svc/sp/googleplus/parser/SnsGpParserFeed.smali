.class public Lcom/sec/android/app/sns3/svc/sp/googleplus/parser/SnsGpParserFeed;
.super Ljava/lang/Object;
.source "SnsGpParserFeed.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/googleplus/parser/SnsGpParserFeed$GooglePlusFeed;
    }
.end annotation


# static fields
.field protected static final MAX_NUMBER_OF_THUMBNAIL:I = 0x5


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    return-void
.end method

.method public static parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;
    .locals 30
    .param p0, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 83
    const/4 v8, 0x0

    .line 84
    .local v8, "feed":Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;
    const/4 v6, 0x0

    .line 87
    .local v6, "curFeed":Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;
    :try_start_0
    new-instance v14, Lorg/json/JSONObject;

    move-object/from16 v0, p0

    invoke-direct {v14, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 89
    .local v14, "jsonObject":Lorg/json/JSONObject;
    const-string v28, "items"

    move-object/from16 v0, v28

    invoke-virtual {v14, v0}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v27

    .line 90
    .local v27, "userItems":Lorg/json/JSONArray;
    if-eqz v27, :cond_4

    .line 91
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_0
    invoke-virtual/range {v27 .. v27}, Lorg/json/JSONArray;->length()I

    move-result v28

    move/from16 v0, v28

    if-ge v10, v0, :cond_4

    .line 92
    move-object/from16 v0, v27

    invoke-virtual {v0, v10}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v26

    .line 93
    .local v26, "userItem":Lorg/json/JSONObject;
    new-instance v25, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;

    invoke-direct/range {v25 .. v25}, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;-><init>()V

    .line 95
    .local v25, "user":Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;
    const-string v28, "id"

    move-object/from16 v0, v26

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, v28

    move-object/from16 v1, v25

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;->mPostId:Ljava/lang/String;

    .line 96
    const-string v28, "title"

    move-object/from16 v0, v26

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, v28

    move-object/from16 v1, v25

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;->mTitle:Ljava/lang/String;

    .line 97
    const-string v28, "updated"

    move-object/from16 v0, v26

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, v28

    move-object/from16 v1, v25

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;->mUpdateTime:Ljava/lang/String;

    .line 99
    const-string v28, "actor"

    move-object/from16 v0, v26

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    .line 100
    .local v2, "actor":Lorg/json/JSONObject;
    const-string v28, "id"

    move-object/from16 v0, v28

    invoke-virtual {v2, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, v28

    move-object/from16 v1, v25

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;->mFromId:Ljava/lang/String;

    .line 101
    const-string v28, "displayName"

    move-object/from16 v0, v28

    invoke-virtual {v2, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, v28

    move-object/from16 v1, v25

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;->mFromName:Ljava/lang/String;

    .line 102
    const-string v28, "url"

    move-object/from16 v0, v28

    invoke-virtual {v2, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, v28

    move-object/from16 v1, v25

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;->mFromUrl:Ljava/lang/String;

    .line 103
    const-string v28, "image"

    move-object/from16 v0, v28

    invoke-virtual {v2, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v21

    .line 104
    .local v21, "profileImage":Lorg/json/JSONObject;
    const-string v28, "url"

    move-object/from16 v0, v21

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, v28

    move-object/from16 v1, v25

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;->mFromProfilePic:Ljava/lang/String;

    .line 106
    const-string v28, "object"

    move-object/from16 v0, v26

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v19

    .line 107
    .local v19, "object":Lorg/json/JSONObject;
    const-string v28, "objectType"

    move-object/from16 v0, v19

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, v28

    move-object/from16 v1, v25

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;->mObjectType:Ljava/lang/String;

    .line 108
    const-string v28, "url"

    move-object/from16 v0, v19

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, v28

    move-object/from16 v1, v25

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;->mContentUrl:Ljava/lang/String;

    .line 110
    const-string v28, "replies"

    move-object/from16 v0, v19

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v5

    .line 111
    .local v5, "comments":Lorg/json/JSONObject;
    const-string v28, "totalItems"

    move-object/from16 v0, v28

    invoke-virtual {v5, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, v28

    move-object/from16 v1, v25

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;->mCommentsCount:Ljava/lang/String;

    .line 112
    const-string v28, "selfLink"

    move-object/from16 v0, v28

    invoke-virtual {v5, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, v28

    move-object/from16 v1, v25

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;->mCommentsUrl:Ljava/lang/String;

    .line 114
    const-string v28, "plusoners"

    move-object/from16 v0, v19

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v16

    .line 115
    .local v16, "likes":Lorg/json/JSONObject;
    const-string v28, "totalItems"

    move-object/from16 v0, v16

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, v28

    move-object/from16 v1, v25

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;->mLikesCount:Ljava/lang/String;

    .line 116
    const-string v28, "selfLink"

    move-object/from16 v0, v16

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, v28

    move-object/from16 v1, v25

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;->mLikesUrl:Ljava/lang/String;

    .line 118
    const-string v28, "resharers"

    move-object/from16 v0, v19

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v22

    .line 119
    .local v22, "reshares":Lorg/json/JSONObject;
    const-string v28, "totalItems"

    move-object/from16 v0, v22

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, v28

    move-object/from16 v1, v25

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;->mResharesCount:Ljava/lang/String;

    .line 120
    const-string v28, "selfLink"

    move-object/from16 v0, v22

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, v28

    move-object/from16 v1, v25

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;->mResharesUrl:Ljava/lang/String;

    .line 122
    const-string v28, "attachments"

    move-object/from16 v0, v19

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v3

    .line 123
    .local v3, "attachements":Lorg/json/JSONArray;
    if-eqz v3, :cond_9

    .line 124
    const/4 v13, 0x0

    .local v13, "j":I
    :goto_1
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v28

    move/from16 v0, v28

    if-ge v13, v0, :cond_9

    .line 125
    invoke-virtual {v3, v13}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    .line 126
    .local v4, "attachmentItem":Lorg/json/JSONObject;
    const-string v28, "objectType"

    move-object/from16 v0, v28

    invoke-virtual {v4, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, v28

    move-object/from16 v1, v25

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;->mObjectType:Ljava/lang/String;

    .line 127
    const-string v28, "article"

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;->mObjectType:Ljava/lang/String;

    move-object/from16 v29, v0

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-nez v28, :cond_0

    .line 128
    const-string v28, "url"

    move-object/from16 v0, v28

    invoke-virtual {v4, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, v28

    move-object/from16 v1, v25

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;->mContentUrl:Ljava/lang/String;

    .line 130
    :cond_0
    const-string v28, "event"

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;->mObjectType:Ljava/lang/String;

    move-object/from16 v29, v0

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_1

    .line 131
    const-string v28, "displayName"

    move-object/from16 v0, v28

    invoke-virtual {v4, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, v28

    move-object/from16 v1, v25

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;->mTitle:Ljava/lang/String;

    .line 134
    :cond_1
    const-string v28, "album"

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;->mObjectType:Ljava/lang/String;

    move-object/from16 v29, v0

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-nez v28, :cond_6

    .line 135
    const-string v28, "image"

    move-object/from16 v0, v28

    invoke-virtual {v4, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v11

    .line 137
    .local v11, "image":Lorg/json/JSONObject;
    const-string v28, "fullImage"

    move-object/from16 v0, v28

    invoke-virtual {v4, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v9

    .line 139
    .local v9, "fullImage":Lorg/json/JSONObject;
    const-string v28, "photo"

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;->mObjectType:Ljava/lang/String;

    move-object/from16 v29, v0

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_3

    .line 140
    const-string v28, "url"

    move-object/from16 v0, v28

    invoke-virtual {v9, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, v28

    move-object/from16 v1, v25

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;->mMediaUrl:Ljava/lang/String;

    .line 141
    const-string v28, "width"

    move-object/from16 v0, v28

    invoke-virtual {v9, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, v28

    move-object/from16 v1, v25

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;->mWidth:Ljava/lang/String;

    .line 142
    const-string v28, "height"

    move-object/from16 v0, v28

    invoke-virtual {v9, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, v28

    move-object/from16 v1, v25

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;->mHeight:Ljava/lang/String;

    .line 124
    .end local v9    # "fullImage":Lorg/json/JSONObject;
    .end local v11    # "image":Lorg/json/JSONObject;
    :cond_2
    :goto_2
    add-int/lit8 v13, v13, 0x1

    goto/16 :goto_1

    .line 143
    .restart local v9    # "fullImage":Lorg/json/JSONObject;
    .restart local v11    # "image":Lorg/json/JSONObject;
    :cond_3
    if-eqz v11, :cond_5

    .line 144
    const-string v28, "url"

    move-object/from16 v0, v28

    invoke-virtual {v11, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, v28

    move-object/from16 v1, v25

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;->mMediaUrl:Ljava/lang/String;

    .line 145
    const-string v28, "width"

    move-object/from16 v0, v28

    invoke-virtual {v11, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, v28

    move-object/from16 v1, v25

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;->mWidth:Ljava/lang/String;

    .line 146
    const-string v28, "height"

    move-object/from16 v0, v28

    invoke-virtual {v11, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, v28

    move-object/from16 v1, v25

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;->mHeight:Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 192
    .end local v2    # "actor":Lorg/json/JSONObject;
    .end local v3    # "attachements":Lorg/json/JSONArray;
    .end local v4    # "attachmentItem":Lorg/json/JSONObject;
    .end local v5    # "comments":Lorg/json/JSONObject;
    .end local v9    # "fullImage":Lorg/json/JSONObject;
    .end local v10    # "i":I
    .end local v11    # "image":Lorg/json/JSONObject;
    .end local v13    # "j":I
    .end local v14    # "jsonObject":Lorg/json/JSONObject;
    .end local v16    # "likes":Lorg/json/JSONObject;
    .end local v19    # "object":Lorg/json/JSONObject;
    .end local v21    # "profileImage":Lorg/json/JSONObject;
    .end local v22    # "reshares":Lorg/json/JSONObject;
    .end local v25    # "user":Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;
    .end local v26    # "userItem":Lorg/json/JSONObject;
    .end local v27    # "userItems":Lorg/json/JSONArray;
    :catch_0
    move-exception v7

    .line 193
    .local v7, "e":Lorg/json/JSONException;
    invoke-virtual {v7}, Lorg/json/JSONException;->printStackTrace()V

    .line 196
    .end local v7    # "e":Lorg/json/JSONException;
    :cond_4
    return-object v8

    .line 147
    .restart local v2    # "actor":Lorg/json/JSONObject;
    .restart local v3    # "attachements":Lorg/json/JSONArray;
    .restart local v4    # "attachmentItem":Lorg/json/JSONObject;
    .restart local v5    # "comments":Lorg/json/JSONObject;
    .restart local v9    # "fullImage":Lorg/json/JSONObject;
    .restart local v10    # "i":I
    .restart local v11    # "image":Lorg/json/JSONObject;
    .restart local v13    # "j":I
    .restart local v14    # "jsonObject":Lorg/json/JSONObject;
    .restart local v16    # "likes":Lorg/json/JSONObject;
    .restart local v19    # "object":Lorg/json/JSONObject;
    .restart local v21    # "profileImage":Lorg/json/JSONObject;
    .restart local v22    # "reshares":Lorg/json/JSONObject;
    .restart local v25    # "user":Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;
    .restart local v26    # "userItem":Lorg/json/JSONObject;
    .restart local v27    # "userItems":Lorg/json/JSONArray;
    :cond_5
    :try_start_1
    const-string v28, "event"

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;->mObjectType:Ljava/lang/String;

    move-object/from16 v29, v0

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-nez v28, :cond_2

    .line 148
    const-string v28, "url"

    move-object/from16 v0, v28

    invoke-virtual {v4, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, v28

    move-object/from16 v1, v25

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;->mMediaUrl:Ljava/lang/String;

    goto :goto_2

    .line 152
    .end local v9    # "fullImage":Lorg/json/JSONObject;
    .end local v11    # "image":Lorg/json/JSONObject;
    :cond_6
    const-string v28, "thumbnails"

    move-object/from16 v0, v28

    invoke-virtual {v4, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v23

    .line 154
    .local v23, "thumbnails":Lorg/json/JSONArray;
    invoke-virtual/range {v23 .. v23}, Lorg/json/JSONArray;->length()I

    move-result v28

    const/16 v29, 0x5

    move/from16 v0, v28

    move/from16 v1, v29

    if-le v0, v1, :cond_7

    const/16 v18, 0x5

    .line 156
    .local v18, "mediaLength":I
    :goto_3
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    .line 157
    .local v12, "images":Ljava/lang/StringBuilder;
    const/4 v15, 0x0

    .local v15, "k":I
    :goto_4
    move/from16 v0, v18

    if-ge v15, v0, :cond_8

    .line 158
    move-object/from16 v0, v23

    invoke-virtual {v0, v15}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v24

    .line 159
    .local v24, "thumbnailsItem":Lorg/json/JSONObject;
    const-string v28, "image"

    move-object/from16 v0, v24

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v11

    .line 162
    .restart local v11    # "image":Lorg/json/JSONObject;
    const-string v28, "url"

    move-object/from16 v0, v28

    invoke-virtual {v11, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, v28

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    const-string v29, ","

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 164
    const-string v28, "width"

    move-object/from16 v0, v28

    invoke-virtual {v11, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, v28

    move-object/from16 v1, v25

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;->mWidth:Ljava/lang/String;

    .line 165
    const-string v28, "height"

    move-object/from16 v0, v28

    invoke-virtual {v11, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, v28

    move-object/from16 v1, v25

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;->mHeight:Ljava/lang/String;

    .line 157
    add-int/lit8 v15, v15, 0x1

    goto :goto_4

    .line 154
    .end local v11    # "image":Lorg/json/JSONObject;
    .end local v12    # "images":Ljava/lang/StringBuilder;
    .end local v15    # "k":I
    .end local v18    # "mediaLength":I
    .end local v24    # "thumbnailsItem":Lorg/json/JSONObject;
    :cond_7
    invoke-virtual/range {v23 .. v23}, Lorg/json/JSONArray;->length()I

    move-result v18

    goto :goto_3

    .line 167
    .restart local v12    # "images":Ljava/lang/StringBuilder;
    .restart local v15    # "k":I
    .restart local v18    # "mediaLength":I
    :cond_8
    const-string v28, ","

    move-object/from16 v0, v28

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->lastIndexOf(Ljava/lang/String;)I

    move-result v28

    move/from16 v0, v28

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, v28

    move-object/from16 v1, v25

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;->mMediaUrl:Ljava/lang/String;

    goto/16 :goto_2

    .line 173
    .end local v4    # "attachmentItem":Lorg/json/JSONObject;
    .end local v12    # "images":Ljava/lang/StringBuilder;
    .end local v13    # "j":I
    .end local v15    # "k":I
    .end local v18    # "mediaLength":I
    .end local v23    # "thumbnails":Lorg/json/JSONArray;
    :cond_9
    const-string v28, "verb"

    move-object/from16 v0, v26

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v28

    const-string v29, "checkin"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v28

    if-eqz v28, :cond_a

    .line 174
    const-string v28, "verb"

    move-object/from16 v0, v26

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, v28

    move-object/from16 v1, v25

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;->mObjectType:Ljava/lang/String;

    .line 175
    const-string v28, "location"

    move-object/from16 v0, v26

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v17

    .line 177
    .local v17, "location":Lorg/json/JSONObject;
    const-string v28, "displayName"

    move-object/from16 v0, v17

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, v28

    move-object/from16 v1, v25

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;->mLocationName:Ljava/lang/String;

    .line 178
    const-string v28, "position"

    move-object/from16 v0, v17

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v20

    .line 179
    .local v20, "position":Lorg/json/JSONObject;
    const-string v28, "latitude"

    move-object/from16 v0, v20

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v28

    invoke-static/range {v28 .. v29}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v28

    move-object/from16 v0, v28

    move-object/from16 v1, v25

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;->mLatitude:Ljava/lang/Double;

    .line 180
    const-string v28, "longitude"

    move-object/from16 v0, v20

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v28

    invoke-static/range {v28 .. v29}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v28

    move-object/from16 v0, v28

    move-object/from16 v1, v25

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;->mLongitude:Ljava/lang/Double;

    .line 183
    .end local v17    # "location":Lorg/json/JSONObject;
    .end local v20    # "position":Lorg/json/JSONObject;
    :cond_a
    if-nez v8, :cond_b

    .line 184
    move-object/from16 v8, v25

    .line 185
    move-object v6, v8

    .line 91
    :goto_5
    add-int/lit8 v10, v10, 0x1

    goto/16 :goto_0

    .line 187
    :cond_b
    move-object/from16 v0, v25

    iput-object v0, v6, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;->mNext:Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;

    .line 188
    iget-object v6, v6, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;->mNext:Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_5
.end method
