.class public Lcom/sec/android/app/sns3/agent/sp/sinaweibo/db/SnsSinaweiboDBHelper;
.super Lcom/sec/android/app/sns3/agent/db/SnsDBHelperBase;
.source "SnsSinaweiboDBHelper.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 30
    const-string v0, "SnsSinaweiboDB.db"

    const/4 v1, 0x0

    const/16 v2, 0x14

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/sec/android/app/sns3/agent/db/SnsDBHelperBase;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 31
    return-void
.end method


# virtual methods
.method public createTables(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 49
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 52
    const-string v0, "CREATE TABLE IF NOT EXISTS user (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,user_id VARCHAR(50),username TEXT, UNIQUE (user_id));"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 57
    const-string v0, "SnsAgent"

    const-string v1, "DB TABLE(user) Has been created."

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 60
    const-string v0, "CREATE TABLE IF NOT EXISTS friendtimeline (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,post_id VARCHAR(50),type TEXT,icon TEXT,caption TEXT,message TEXT,description TEXT,from_name TEXT,create_time TIMESTAMP,updated_time TIMESTAMP,picture TEXT, UNIQUE (post_id));"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 74
    const-string v0, "SnsAgent"

    const-string v1, "DB TABLE(friendtimeline) Has been created."

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 77
    const-string v0, "CREATE TABLE IF NOT EXISTS usertimeline (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,post_id VARCHAR(50),type TEXT,icon TEXT,caption TEXT,message TEXT,description TEXT,from_name TEXT,create_time TIMESTAMP,updated_time TIMESTAMP,picture TEXT, UNIQUE (post_id));"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 90
    const-string v0, "SnsAgent"

    const-string v1, "DB TABLE(usertimeline) Has been created."

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 92
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 93
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 94
    return-void
.end method

.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 0
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 35
    invoke-super {p0, p1}, Lcom/sec/android/app/sns3/agent/db/SnsDBHelperBase;->onCreate(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 37
    invoke-virtual {p0, p1}, Lcom/sec/android/app/sns3/agent/sp/sinaweibo/db/SnsSinaweiboDBHelper;->createTables(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 38
    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 0
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "oldVersion"    # I
    .param p3, "newVersion"    # I

    .prologue
    .line 42
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/app/sns3/agent/db/SnsDBHelperBase;->onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V

    .line 44
    invoke-virtual {p0, p1}, Lcom/sec/android/app/sns3/agent/sp/sinaweibo/db/SnsSinaweiboDBHelper;->upgradeTables(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 46
    return-void
.end method

.method public upgradeTables(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 99
    const-string v0, "DROP TABLE IF EXISTS user"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 100
    const-string v0, "DROP TABLE IF EXISTS friendtimeline"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 101
    const-string v0, "DROP TABLE IF EXISTS usertimeline"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 103
    invoke-virtual {p0, p1}, Lcom/sec/android/app/sns3/agent/sp/sinaweibo/db/SnsSinaweiboDBHelper;->createTables(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 104
    return-void
.end method

.method public wipeData(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 108
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 110
    :try_start_0
    const-string v0, "DELETE FROM user"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 111
    const-string v0, "DELETE FROM friendtimeline"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 112
    const-string v0, "DELETE FROM usertimeline"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 114
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 116
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 118
    return-void

    .line 116
    :catchall_0
    move-exception v0

    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0
.end method
