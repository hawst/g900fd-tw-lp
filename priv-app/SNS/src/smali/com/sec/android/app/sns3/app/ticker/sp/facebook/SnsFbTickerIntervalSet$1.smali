.class final Lcom/sec/android/app/sns3/app/ticker/sp/facebook/SnsFbTickerIntervalSet$1;
.super Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqGetPermissions;
.source "SnsFbTickerIntervalSet.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/sns3/app/ticker/sp/facebook/SnsFbTickerIntervalSet;->setAutoRefresh(Landroid/content/Context;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    .param p2, "x1"    # Ljava/lang/String;

    .prologue
    .line 65
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqGetPermissions;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public onReqRespond(IZIILandroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePermissions;)Z
    .locals 14
    .param p1, "reqID"    # I
    .param p2, "bSuccess"    # Z
    .param p3, "httpStatus"    # I
    .param p4, "errorCode"    # I
    .param p5, "reason"    # Landroid/os/Bundle;
    .param p6, "permissions"    # Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePermissions;

    .prologue
    .line 71
    if-eqz p2, :cond_3

    .line 72
    if-eqz p6, :cond_0

    .line 73
    move-object/from16 v9, p6

    .line 75
    .local v9, "curPermissions":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePermissions;
    :goto_0
    iget-object v3, v9, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePermissions;->mPermissions:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePermission;

    if-eqz v3, :cond_0

    .line 77
    const-string v3, "read_stream"

    iget-object v6, v9, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePermissions;->mPermissions:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePermission;

    iget-object v6, v6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePermission;->mPermission:Ljava/lang/String;

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, v9, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePermissions;->mPermissions:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePermission;

    iget v3, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePermission;->mValue:I

    const/4 v6, 0x1

    if-ne v3, v6, :cond_2

    .line 80
    const/4 v13, 0x0

    .line 82
    .local v13, "interval":I
    const-string v3, "Sns_ticker_interval_set"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "autoRefreshKey is "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    # getter for: Lcom/sec/android/app/sns3/app/ticker/sp/facebook/SnsFbTickerIntervalSet;->mAutoRefreshKey:I
    invoke-static {}, Lcom/sec/android/app/sns3/app/ticker/sp/facebook/SnsFbTickerIntervalSet;->access$000()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 84
    # getter for: Lcom/sec/android/app/sns3/app/ticker/sp/facebook/SnsFbTickerIntervalSet;->mAutoRefreshKey:I
    invoke-static {}, Lcom/sec/android/app/sns3/app/ticker/sp/facebook/SnsFbTickerIntervalSet;->access$000()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 100
    const-string v3, "Sns_ticker_interval_set"

    const-string v6, "setAutoRefresh() : autoRefreshKey is wrong"

    invoke-static {v3, v6}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 104
    :goto_1
    sget-object v3, Lcom/sec/android/app/sns3/app/ticker/sp/facebook/SnsFbTickerIntervalSet;->mContext:Landroid/content/Context;

    const-string v6, "alarm"

    invoke-virtual {v3, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/AlarmManager;

    .line 106
    .local v2, "tickerAlarmManager":Landroid/app/AlarmManager;
    if-eqz v2, :cond_1

    .line 108
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    .line 109
    .local v10, "currnetTime":J
    int-to-long v6, v13

    add-long v4, v10, v6

    .line 111
    .local v4, "triggerAtTime":J
    new-instance v12, Landroid/content/Intent;

    const-string v3, "android.sns.CHANGED_TICKER_INFO_FOR_PENDINGINTENT"

    invoke-direct {v12, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 112
    .local v12, "intent":Landroid/content/Intent;
    sget-object v3, Lcom/sec/android/app/sns3/app/ticker/sp/facebook/SnsFbTickerIntervalSet;->mContext:Landroid/content/Context;

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static {v3, v6, v12, v7}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v8

    .line 114
    .local v8, "pendingIntent":Landroid/app/PendingIntent;
    const/4 v3, 0x1

    int-to-long v6, v13

    invoke-virtual/range {v2 .. v8}, Landroid/app/AlarmManager;->setRepeating(IJJLandroid/app/PendingIntent;)V

    .line 116
    const-string v3, "Sns_ticker_interval_set"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "setAutoRefresh() triggerAtTime : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", interval : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", operation"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 131
    .end local v2    # "tickerAlarmManager":Landroid/app/AlarmManager;
    .end local v4    # "triggerAtTime":J
    .end local v8    # "pendingIntent":Landroid/app/PendingIntent;
    .end local v9    # "curPermissions":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePermissions;
    .end local v10    # "currnetTime":J
    .end local v12    # "intent":Landroid/content/Intent;
    .end local v13    # "interval":I
    :cond_0
    :goto_2
    const/4 v3, 0x0

    return v3

    .line 88
    .restart local v9    # "curPermissions":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePermissions;
    .restart local v13    # "interval":I
    :pswitch_0
    const v13, 0x1b7740

    .line 89
    goto :goto_1

    .line 91
    :pswitch_1
    const v13, 0x36ee80

    .line 92
    goto :goto_1

    .line 94
    :pswitch_2
    const v13, 0x6ddd00

    .line 95
    goto :goto_1

    .line 97
    :pswitch_3
    const v13, 0xdbba00

    .line 98
    goto :goto_1

    .line 119
    .restart local v2    # "tickerAlarmManager":Landroid/app/AlarmManager;
    :cond_1
    const-string v3, "Sns_ticker_interval_set"

    const-string v6, "setAutoRefresh() : tickerAlarmManager is null"

    invoke-static {v3, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 125
    .end local v2    # "tickerAlarmManager":Landroid/app/AlarmManager;
    .end local v13    # "interval":I
    :cond_2
    iget-object v3, v9, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePermissions;->mPermissions:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePermission;

    iget-object v3, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePermission;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePermission;

    iput-object v3, v9, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePermissions;->mPermissions:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePermission;

    goto/16 :goto_0

    .line 129
    .end local v9    # "curPermissions":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePermissions;
    :cond_3
    const-string v3, "Sns_ticker_interval_set"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "SnsFbSyncAdapterEventService errorCode : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, p4

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", reason : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p5

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 84
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
