.class public interface abstract Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserNotification$FacebookNotification$SnsFbFreqQuery;
.super Ljava/lang/Object;
.source "SnsFbParserNotification.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserNotification$FacebookNotification;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "SnsFbFreqQuery"
.end annotation


# static fields
.field public static final MESSAGE:Ljava/lang/String; = "message"

.field public static final TIME:Ljava/lang/String; = "time"

.field public static final UNREAD:Ljava/lang/String; = "unread"

.field public static final USER_ID_FROM:Ljava/lang/String; = "uid_from"

.field public static final USER_ID_TO:Ljava/lang/String; = "uid_to"
