.class Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdGetFollowers$1;
.super Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwReqGetFollowersIds;
.source "SnsTwCmdGetFollowers.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdGetFollowers;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Handler;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdGetFollowers;


# direct methods
.method constructor <init>(Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdGetFollowers;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Bundle;)V
    .locals 0
    .param p2, "x0"    # Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    .param p3, "x1"    # Landroid/os/Bundle;

    .prologue
    .line 51
    iput-object p1, p0, Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdGetFollowers$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdGetFollowers;

    invoke-direct {p0, p2, p3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwReqGetFollowersIds;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Bundle;)V

    return-void
.end method


# virtual methods
.method public onReqRespond(IZIILandroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStringArray;)Z
    .locals 8
    .param p1, "reqID"    # I
    .param p2, "bSuccess"    # Z
    .param p3, "httpStatus"    # I
    .param p4, "errorCode"    # I
    .param p5, "reason"    # Landroid/os/Bundle;
    .param p6, "stringArray"    # Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStringArray;

    .prologue
    const/4 v6, 0x0

    .line 59
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 60
    .local v4, "values":Landroid/content/ContentValues;
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/sns3/SnsApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 62
    .local v0, "cr":Landroid/content/ContentResolver;
    if-eqz p2, :cond_1

    .line 63
    sget-object v5, Lcom/sec/android/app/sns3/agent/sp/twitter/db/SnsTwitterDB$FriendsProfileInfo;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v5, v6, v6}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 65
    if-eqz p6, :cond_0

    iget-object v5, p6, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStringArray;->mStringArray:[Ljava/lang/String;

    if-eqz v5, :cond_0

    .line 67
    iget-object v5, p6, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStringArray;->mStringArray:[Ljava/lang/String;

    array-length v3, v5

    .line 68
    .local v3, "size":I
    new-array v2, v3, [Ljava/lang/String;

    .line 69
    .local v2, "mStringArray":[Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v3, :cond_0

    .line 70
    iget-object v5, p6, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStringArray;->mStringArray:[Ljava/lang/String;

    aget-object v5, v5, v1

    aput-object v5, v2, v1

    .line 71
    const-string v5, "profile_id"

    aget-object v6, v2, v1

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    sget-object v5, Lcom/sec/android/app/sns3/agent/sp/twitter/db/SnsTwitterDB$FriendsProfileInfo;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v5, v4}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 69
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 75
    .end local v1    # "i":I
    .end local v2    # "mStringArray":[Ljava/lang/String;
    .end local v3    # "size":I
    :cond_0
    iget-object v5, p0, Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdGetFollowers$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdGetFollowers;

    sget-object v6, Lcom/sec/android/app/sns3/agent/sp/twitter/db/SnsTwitterDB$FriendsProfileInfo;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v6}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdGetFollowers;->setUri(Ljava/lang/String;)V

    .line 83
    :goto_1
    iget-object v5, p0, Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdGetFollowers$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdGetFollowers;

    invoke-virtual {v5, p2}, Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdGetFollowers;->setSuccess(Z)V

    .line 84
    iget-object v5, p0, Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdGetFollowers$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdGetFollowers;

    invoke-virtual {v5, p0}, Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdGetFollowers;->receive(Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;)V

    .line 86
    const/4 v5, 0x0

    return v5

    .line 78
    :cond_1
    iget-object v5, p0, Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdGetFollowers$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdGetFollowers;

    invoke-virtual {v5, v6}, Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdGetFollowers;->setUri(Ljava/lang/String;)V

    .line 79
    iget-object v5, p0, Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdGetFollowers$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdGetFollowers;

    new-instance v6, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;

    const-string v7, "twitter"

    invoke-direct {v6, v7, p3, p4, p5}, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;-><init>(Ljava/lang/String;IILandroid/os/Bundle;)V

    invoke-virtual {v5, v6}, Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdGetFollowers;->setResponseList(Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;)Z

    goto :goto_1
.end method
