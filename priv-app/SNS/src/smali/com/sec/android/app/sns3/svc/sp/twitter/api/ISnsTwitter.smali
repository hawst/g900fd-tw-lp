.class public interface abstract Lcom/sec/android/app/sns3/svc/sp/twitter/api/ISnsTwitter;
.super Ljava/lang/Object;
.source "ISnsTwitter.java"

# interfaces
.implements Landroid/os/IInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/twitter/api/ISnsTwitter$Stub;
    }
.end annotation


# virtual methods
.method public abstract getApplicationInfo()Ljava/util/Map;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract getHomeTimeline(Landroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/twitter/api/ISnsTwitterCallbackStatus;)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract getUsersLookup(Landroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/twitter/api/ISnsTwitterCallbackUser;)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract postStatus(Landroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/twitter/api/ISnsTwitterCallbackStatus;)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract postStatusWithMedia(Landroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/twitter/api/ISnsTwitterCallbackStatus;)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method
