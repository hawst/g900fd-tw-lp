.class Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetProfileFeeds$1;
.super Lcom/sec/android/app/sns3/svc/sp/linkedin/request/SnsLiReqGetUpdates;
.source "SnsLiCmdGetProfileFeeds.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetProfileFeeds;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Handler;Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetProfileFeeds;


# direct methods
.method constructor <init>(Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetProfileFeeds;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;Landroid/os/Bundle;[Ljava/lang/String;)V
    .locals 0
    .param p2, "x0"    # Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    .param p3, "x1"    # Ljava/lang/String;
    .param p4, "x2"    # Landroid/os/Bundle;
    .param p5, "x3"    # [Ljava/lang/String;

    .prologue
    .line 58
    iput-object p1, p0, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetProfileFeeds$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetProfileFeeds;

    invoke-direct {p0, p2, p3, p4, p5}, Lcom/sec/android/app/sns3/svc/sp/linkedin/request/SnsLiReqGetUpdates;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;Landroid/os/Bundle;[Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public onReqRespond(IZIILandroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdates;)Z
    .locals 5
    .param p1, "reqID"    # I
    .param p2, "bSuccess"    # Z
    .param p3, "httpStatus"    # I
    .param p4, "errorCode"    # I
    .param p5, "reason"    # Landroid/os/Bundle;
    .param p6, "updates"    # Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdates;

    .prologue
    const/4 v3, 0x0

    .line 63
    if-eqz p2, :cond_1

    .line 64
    iget-object v0, p0, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetProfileFeeds$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetProfileFeeds;

    # getter for: Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetProfileFeeds;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;
    invoke-static {v0}, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetProfileFeeds;->access$000(Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetProfileFeeds;)Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;

    move-result-object v0

    invoke-virtual {v0, p6}, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;->insertProfileFeeds(Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdates;)V

    .line 65
    iget v0, p6, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdates;->mStart:I

    iget v1, p6, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdates;->mCount:I

    add-int/2addr v0, v1

    iget v1, p6, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdates;->mTotal:I

    if-ge v0, v1, :cond_0

    .line 66
    iget-object v0, p0, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetProfileFeeds$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetProfileFeeds;

    # getter for: Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetProfileFeeds;->mBundleUpdatesQuery:Landroid/os/Bundle;
    invoke-static {v0}, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetProfileFeeds;->access$100(Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetProfileFeeds;)Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "start"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p6, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdates;->mStart:I

    iget v4, p6, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdates;->mCount:I

    add-int/2addr v3, v4

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetProfileFeeds$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetProfileFeeds;

    invoke-virtual {v0, p2}, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetProfileFeeds;->setSuccess(Z)V

    .line 80
    iget-object v0, p0, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetProfileFeeds$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetProfileFeeds;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetProfileFeeds;->receive(Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;)V

    .line 81
    const/4 v0, 0x1

    return v0

    .line 69
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetProfileFeeds$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetProfileFeeds;

    # setter for: Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetProfileFeeds;->mBundleUpdatesQuery:Landroid/os/Bundle;
    invoke-static {v0, v3}, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetProfileFeeds;->access$102(Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetProfileFeeds;Landroid/os/Bundle;)Landroid/os/Bundle;

    .line 70
    iget-object v0, p0, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetProfileFeeds$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetProfileFeeds;

    sget-object v1, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$ProfileFeedList;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetProfileFeeds;->setUri(Ljava/lang/String;)V

    goto :goto_0

    .line 75
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetProfileFeeds$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetProfileFeeds;

    new-instance v1, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;

    const-string v2, "linkedin"

    invoke-direct {v1, v2, p3, p4, p5}, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;-><init>(Ljava/lang/String;IILandroid/os/Bundle;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetProfileFeeds;->setResponseList(Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;)Z

    .line 77
    iget-object v0, p0, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetProfileFeeds$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetProfileFeeds;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetProfileFeeds;->setUri(Ljava/lang/String;)V

    goto :goto_0
.end method
