.class public Lcom/sec/android/app/sns3/svc/http/SnsCustomMultiPartEntity;
.super Lorg/apache/http/entity/mime/MultipartEntity;
.source "SnsCustomMultiPartEntity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/http/SnsCustomMultiPartEntity$CountingOutputStream;,
        Lcom/sec/android/app/sns3/svc/http/SnsCustomMultiPartEntity$ProgressListener;
    }
.end annotation


# instance fields
.field private final listener:Lcom/sec/android/app/sns3/svc/http/SnsCustomMultiPartEntity$ProgressListener;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/sns3/svc/http/SnsCustomMultiPartEntity$ProgressListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/app/sns3/svc/http/SnsCustomMultiPartEntity$ProgressListener;

    .prologue
    .line 17
    invoke-direct {p0}, Lorg/apache/http/entity/mime/MultipartEntity;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/sec/android/app/sns3/svc/http/SnsCustomMultiPartEntity;->listener:Lcom/sec/android/app/sns3/svc/http/SnsCustomMultiPartEntity$ProgressListener;

    .line 19
    return-void
.end method

.method public constructor <init>(Lorg/apache/http/entity/mime/HttpMultipartMode;Lcom/sec/android/app/sns3/svc/http/SnsCustomMultiPartEntity$ProgressListener;)V
    .locals 0
    .param p1, "mode"    # Lorg/apache/http/entity/mime/HttpMultipartMode;
    .param p2, "listener"    # Lcom/sec/android/app/sns3/svc/http/SnsCustomMultiPartEntity$ProgressListener;

    .prologue
    .line 23
    invoke-direct {p0, p1}, Lorg/apache/http/entity/mime/MultipartEntity;-><init>(Lorg/apache/http/entity/mime/HttpMultipartMode;)V

    .line 24
    iput-object p2, p0, Lcom/sec/android/app/sns3/svc/http/SnsCustomMultiPartEntity;->listener:Lcom/sec/android/app/sns3/svc/http/SnsCustomMultiPartEntity$ProgressListener;

    .line 25
    return-void
.end method

.method public constructor <init>(Lorg/apache/http/entity/mime/HttpMultipartMode;Ljava/lang/String;Ljava/nio/charset/Charset;Lcom/sec/android/app/sns3/svc/http/SnsCustomMultiPartEntity$ProgressListener;)V
    .locals 0
    .param p1, "mode"    # Lorg/apache/http/entity/mime/HttpMultipartMode;
    .param p2, "boundary"    # Ljava/lang/String;
    .param p3, "charset"    # Ljava/nio/charset/Charset;
    .param p4, "listener"    # Lcom/sec/android/app/sns3/svc/http/SnsCustomMultiPartEntity$ProgressListener;

    .prologue
    .line 29
    invoke-direct {p0, p1, p2, p3}, Lorg/apache/http/entity/mime/MultipartEntity;-><init>(Lorg/apache/http/entity/mime/HttpMultipartMode;Ljava/lang/String;Ljava/nio/charset/Charset;)V

    .line 30
    iput-object p4, p0, Lcom/sec/android/app/sns3/svc/http/SnsCustomMultiPartEntity;->listener:Lcom/sec/android/app/sns3/svc/http/SnsCustomMultiPartEntity$ProgressListener;

    .line 31
    return-void
.end method


# virtual methods
.method public writeTo(Ljava/io/OutputStream;)V
    .locals 2
    .param p1, "outstream"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 36
    new-instance v0, Lcom/sec/android/app/sns3/svc/http/SnsCustomMultiPartEntity$CountingOutputStream;

    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/http/SnsCustomMultiPartEntity;->listener:Lcom/sec/android/app/sns3/svc/http/SnsCustomMultiPartEntity$ProgressListener;

    invoke-direct {v0, p1, v1}, Lcom/sec/android/app/sns3/svc/http/SnsCustomMultiPartEntity$CountingOutputStream;-><init>(Ljava/io/OutputStream;Lcom/sec/android/app/sns3/svc/http/SnsCustomMultiPartEntity$ProgressListener;)V

    invoke-super {p0, v0}, Lorg/apache/http/entity/mime/MultipartEntity;->writeTo(Ljava/io/OutputStream;)V

    .line 37
    return-void
.end method
