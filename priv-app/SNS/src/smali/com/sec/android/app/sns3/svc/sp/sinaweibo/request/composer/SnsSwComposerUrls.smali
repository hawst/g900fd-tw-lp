.class public Lcom/sec/android/app/sns3/svc/sp/sinaweibo/request/composer/SnsSwComposerUrls;
.super Ljava/lang/Object;
.source "SnsSwComposerUrls.java"


# static fields
.field public static final INVALID:Ljava/lang/String;

.field public static final STATUSES_FRIENDS_TIMELINE:Ljava/lang/String; = "statuses/friends_timeline.json"

.field public static final STATUSES_HOME_TIMELINE:Ljava/lang/String; = "statuses/home_timeline.json"

.field public static final STATUSES_MENTIONS:Ljava/lang/String; = "statuses/mentions.json"

.field public static final STATUSES_PUBLIC_TIMELINE:Ljava/lang/String; = "statuses/public_timeline.json"

.field public static final STATUSES_REPOST_TIMELINE:Ljava/lang/String; = "statuses/repost_timeline.json"

.field public static final STATUSES_TIMELINE_BATCH:Ljava/lang/String; = "statuses/timeline_batch.json"

.field public static final STATUSES_USER_TIMELINE:Ljava/lang/String; = "statuses/user_timeline.json"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/sinaweibo/request/composer/SnsSwComposerUrls;->INVALID:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
