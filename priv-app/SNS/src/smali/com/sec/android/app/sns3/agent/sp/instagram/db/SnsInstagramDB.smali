.class public Lcom/sec/android/app/sns3/agent/sp/instagram/db/SnsInstagramDB;
.super Ljava/lang/Object;
.source "SnsInstagramDB.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/agent/sp/instagram/db/SnsInstagramDB$FriendsProfileInfoColumns;,
        Lcom/sec/android/app/sns3/agent/sp/instagram/db/SnsInstagramDB$FriendsProfileInfo;,
        Lcom/sec/android/app/sns3/agent/sp/instagram/db/SnsInstagramDB$StatusStreamColumns;,
        Lcom/sec/android/app/sns3/agent/sp/instagram/db/SnsInstagramDB$StatusStream;,
        Lcom/sec/android/app/sns3/agent/sp/instagram/db/SnsInstagramDB$UserInfoColumns;,
        Lcom/sec/android/app/sns3/agent/sp/instagram/db/SnsInstagramDB$UserInfo;,
        Lcom/sec/android/app/sns3/agent/sp/instagram/db/SnsInstagramDB$SyncDBPhotoColumns;,
        Lcom/sec/android/app/sns3/agent/sp/instagram/db/SnsInstagramDB$SyncDBPhoto;,
        Lcom/sec/android/app/sns3/agent/sp/instagram/db/SnsInstagramDB$SyncDBAlbumColumns;,
        Lcom/sec/android/app/sns3/agent/sp/instagram/db/SnsInstagramDB$SyncDBAlbum;,
        Lcom/sec/android/app/sns3/agent/sp/instagram/db/SnsInstagramDB$UserInFeedInfoColumns;,
        Lcom/sec/android/app/sns3/agent/sp/instagram/db/SnsInstagramDB$UserInFeedInfo;,
        Lcom/sec/android/app/sns3/agent/sp/instagram/db/SnsInstagramDB$UserProfileInfoColumns;,
        Lcom/sec/android/app/sns3/agent/sp/instagram/db/SnsInstagramDB$UserProfileInfo;,
        Lcom/sec/android/app/sns3/agent/sp/instagram/db/SnsInstagramDB$UserInBasicInfoColumns;,
        Lcom/sec/android/app/sns3/agent/sp/instagram/db/SnsInstagramDB$UserInBasicInfo;,
        Lcom/sec/android/app/sns3/agent/sp/instagram/db/SnsInstagramDB$WIPE_INSTAGRAM_DATA;
    }
.end annotation


# static fields
.field public static final AUTHORITY:Ljava/lang/String; = "com.sec.android.app.sns3.sp.instagram"

.field public static final DATABASE_NAME:Ljava/lang/String; = "snsInstagramDB.db"

.field public static final DATABASE_VERSION:I = 0x2

.field public static final FRIENDS_PROFILE_TABLE_NAME:Ljava/lang/String; = "friends_profile_info"

.field public static final LIFE_AUTHORITY:Ljava/lang/String; = "com.sec.android.app.sns3.life"

.field public static final STATUS_STREAM_TABLE_NAME:Ljava/lang/String; = "status_stream"

.field public static final SYNC_ALBUM_TABLE_NAME:Ljava/lang/String; = "sync_album"

.field public static final SYNC_PHOTO_TABLE_NAME:Ljava/lang/String; = "sync_photo"

.field public static final USER_BASIC_INFO_TABLE_NAME:Ljava/lang/String; = "user_basic_info"

.field public static final USER_FEED_TABLE_NAME:Ljava/lang/String; = "user_feed_info"

.field public static final USER_PROFILE_TABLE_NAME:Ljava/lang/String; = "user_profile_info"

.field public static final USER_TABLE_NAME:Ljava/lang/String; = "user_info"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 339
    return-void
.end method
