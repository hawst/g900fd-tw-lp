.class public interface abstract Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$UserWorkInfoColumns;
.super Ljava/lang/Object;
.source "SnsLinkedInDB.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "UserWorkInfoColumns"
.end annotation


# static fields
.field public static final COMAPNY_NAME:Ljava/lang/String; = "company_name"

.field public static final COMPANY_ID:Ljava/lang/String; = "company_id"

.field public static final COMPANY_SIZE:Ljava/lang/String; = "company_size"

.field public static final COMPANY_TYPE:Ljava/lang/String; = "company_type"

.field public static final END_MONTH:Ljava/lang/String; = "endMONTH"

.field public static final END_YEAR:Ljava/lang/String; = "endtYear"

.field public static final INDUSTRY:Ljava/lang/String; = "industry"

.field public static final IS_CURRENT:Ljava/lang/String; = "isCurrent"

.field public static final PERSON_ID:Ljava/lang/String; = "person_id"

.field public static final POSITION_ID:Ljava/lang/String; = "position_id"

.field public static final START_MONTH:Ljava/lang/String; = "startMonth"

.field public static final START_YEAR:Ljava/lang/String; = "startYear"

.field public static final SUMMARY:Ljava/lang/String; = "summary"

.field public static final TITLE:Ljava/lang/String; = "title"
