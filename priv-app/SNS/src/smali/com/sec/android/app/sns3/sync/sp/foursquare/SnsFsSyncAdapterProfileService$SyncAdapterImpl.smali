.class Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService$SyncAdapterImpl;
.super Landroid/content/AbstractThreadedSyncAdapter;
.source "SnsFsSyncAdapterProfileService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SyncAdapterImpl"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService;Landroid/content/Context;)V
    .locals 1
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 81
    iput-object p1, p0, Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService$SyncAdapterImpl;->this$0:Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService;

    .line 82
    const/4 v0, 0x1

    invoke-direct {p0, p2, v0}, Landroid/content/AbstractThreadedSyncAdapter;-><init>(Landroid/content/Context;Z)V

    .line 83
    # setter for: Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService;->mContext:Landroid/content/Context;
    invoke-static {p1, p2}, Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService;->access$002(Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService;Landroid/content/Context;)Landroid/content/Context;

    .line 84
    return-void
.end method


# virtual methods
.method public onPerformSync(Landroid/accounts/Account;Landroid/os/Bundle;Ljava/lang/String;Landroid/content/ContentProviderClient;Landroid/content/SyncResult;)V
    .locals 4
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "extras"    # Landroid/os/Bundle;
    .param p3, "authority"    # Ljava/lang/String;
    .param p4, "provider"    # Landroid/content/ContentProviderClient;
    .param p5, "syncResult"    # Landroid/content/SyncResult;

    .prologue
    .line 90
    const-string v1, "SnsFsSync"

    const-string v2, "***************** SnsFsSyncAdapterProfileService : onPerformSync!!! *****************"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 93
    iget-object v2, p0, Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService$SyncAdapterImpl;->this$0:Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService;

    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/sns3/SnsApplication;->getSvcMgr()Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/sns3/svc/SnsSvcMgr;->getTokenMgr()Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;

    move-result-object v1

    const-string v3, "foursquare"

    invoke-virtual {v1, v3}, Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;->getToken(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsToken;

    # setter for: Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService;->mFsToken:Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;
    invoke-static {v2, v1}, Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService;->access$102(Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService;Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;)Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;

    .line 97
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService$SyncAdapterImpl;->this$0:Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService;

    # setter for: Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService;->mAccount:Landroid/accounts/Account;
    invoke-static {v1, p1}, Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService;->access$202(Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService;Landroid/accounts/Account;)Landroid/accounts/Account;

    .line 98
    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService$SyncAdapterImpl;->this$0:Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService;

    # setter for: Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService;->mAuthority:Ljava/lang/String;
    invoke-static {v1, p3}, Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService;->access$302(Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService;Ljava/lang/String;)Ljava/lang/String;

    .line 99
    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService$SyncAdapterImpl;->this$0:Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService;

    # setter for: Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService;->mSyncResult:Landroid/content/SyncResult;
    invoke-static {v1, p5}, Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService;->access$402(Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService;Landroid/content/SyncResult;)Landroid/content/SyncResult;

    .line 101
    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService$SyncAdapterImpl;->this$0:Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService;

    # invokes: Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService;->performSync()V
    invoke-static {v1}, Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService;->access$500(Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService;)V
    :try_end_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 111
    :goto_0
    return-void

    .line 103
    :catch_0
    move-exception v0

    .line 104
    .local v0, "e":Landroid/accounts/OperationCanceledException;
    const-string v1, "SnsFsSync"

    const-string v2, "SnsFsSyncAdapterProfileService : onPerformSync is CANCELED!!!"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 105
    invoke-virtual {v0}, Landroid/accounts/OperationCanceledException;->printStackTrace()V

    goto :goto_0

    .line 106
    .end local v0    # "e":Landroid/accounts/OperationCanceledException;
    :catch_1
    move-exception v0

    .line 107
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "SnsFsSync"

    const-string v2, "SnsFsSyncAdapterProfileService : Abnormal Syncing!!!"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 108
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public onSyncCanceled()V
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService$SyncAdapterImpl;->this$0:Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService;

    # invokes: Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService;->onSyncCanceled()V
    invoke-static {v0}, Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService;->access$600(Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService;)V

    .line 116
    return-void
.end method
