.class public abstract Lcom/sec/android/app/sns3/svc/sp/qzone/request/SnsQzReqGetUserInfo;
.super Lcom/sec/android/app/sns3/svc/sp/qzone/request/SnsQzReqBase;
.source "SnsQzReqGetUserInfo.java"

# interfaces
.implements Lcom/sec/android/app/sns3/svc/sp/qzone/callback/ISnsQzReqCbUserInfo;


# instance fields
.field private final mAccessToken:Ljava/lang/String;

.field private final mUserID:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;)V
    .locals 3
    .param p1, "svcMgr"    # Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    .param p2, "userID"    # Ljava/lang/String;

    .prologue
    .line 42
    const/16 v1, 0x16

    invoke-direct {p0, p1, v1}, Lcom/sec/android/app/sns3/svc/sp/qzone/request/SnsQzReqBase;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;I)V

    .line 43
    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/request/SnsQzReqGetUserInfo;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    invoke-virtual {v1}, Lcom/sec/android/app/sns3/svc/SnsSvcMgr;->getTokenMgr()Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;

    move-result-object v1

    const-string v2, "qzone"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;->getToken(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzToken;

    .line 45
    .local v0, "token":Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzToken;
    if-nez p2, :cond_0

    .line 46
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzToken;->getUserID()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->encodeUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/request/SnsQzReqGetUserInfo;->mUserID:Ljava/lang/String;

    .line 50
    :goto_0
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzToken;->getAccessToken()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->encodeUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/request/SnsQzReqGetUserInfo;->mAccessToken:Ljava/lang/String;

    .line 51
    return-void

    .line 48
    :cond_0
    iput-object p2, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/request/SnsQzReqGetUserInfo;->mUserID:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method protected compose()Lcom/sec/android/app/sns3/svc/http/SnsHttpRequest;
    .locals 6

    .prologue
    .line 56
    const-string v2, "GET"

    .line 57
    .local v2, "method":Ljava/lang/String;
    const/4 v3, 0x0

    .line 58
    .local v3, "uri":Ljava/lang/String;
    const/4 v4, 0x0

    .line 59
    .local v4, "header":Landroid/os/Bundle;
    const/4 v5, 0x0

    .line 61
    .local v5, "body":Landroid/os/Bundle;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "https://graph.qq.com/user/get_user_info?oauth_consumer_key=100350160&access_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/request/SnsQzReqGetUserInfo;->mAccessToken:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&openid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/request/SnsQzReqGetUserInfo;->mUserID:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 64
    new-instance v0, Lcom/sec/android/app/sns3/svc/http/SnsHttpRequest;

    iget v1, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/request/SnsQzReqGetUserInfo;->mReqID:I

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/sns3/svc/http/SnsHttpRequest;-><init>(ILjava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;)V

    return-object v0
.end method

.method protected parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;
    .locals 3
    .param p1, "content"    # Ljava/lang/String;

    .prologue
    .line 69
    const-string v1, "SNS"

    const-string v2, "Qzone  SnsQzReqGetUserInfo response SUCCESS!!"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 70
    invoke-super {p0, p1}, Lcom/sec/android/app/sns3/svc/sp/qzone/request/SnsQzReqBase;->parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;

    move-result-object v0

    .line 72
    .local v0, "response":Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;
    if-eqz v0, :cond_0

    .line 75
    .end local v0    # "response":Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;
    :goto_0
    return-object v0

    .restart local v0    # "response":Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;
    :cond_0
    invoke-static {p1}, Lcom/sec/android/app/sns3/svc/sp/qzone/parser/SnsQzParserUserInfo;->parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseUserInfo;

    move-result-object v0

    goto :goto_0
.end method

.method protected respond(Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;)Z
    .locals 7
    .param p1, "result"    # Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;

    .prologue
    .line 81
    const-string v0, "SNS"

    const-string v1, "<SnsQzReqGetUserInfo> respond()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 83
    iget v1, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/request/SnsQzReqGetUserInfo;->mReqID:I

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->isSuccess()Z

    move-result v2

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->getHttpstatus()I

    move-result v3

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->getErrorCode()I

    move-result v4

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->getReason()Landroid/os/Bundle;

    move-result-object v5

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->getResponse()Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseUserInfo;

    move-object v0, p0

    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/app/sns3/svc/sp/qzone/request/SnsQzReqGetUserInfo;->onReqRespond(IZIILandroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseUserInfo;)Z

    .line 86
    const/4 v0, 0x1

    return v0
.end method
