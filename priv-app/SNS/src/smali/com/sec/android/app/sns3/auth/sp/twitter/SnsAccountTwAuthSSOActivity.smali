.class public Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthSSOActivity;
.super Landroid/accounts/AccountAuthenticatorActivity;
.source "SnsAccountTwAuthSSOActivity.java"


# static fields
.field private static final GOOGLE_ACCOUNT_TYPE:Ljava/lang/String; = "com.google"

.field private static final REQUEST_CODE_GET_TOKEN:I = 0x3e9

.field private static final REQUEST_SYNC_INTERVAL:I = 0x3e8

.field private static final TAG:Ljava/lang/String; = "SnsAccountTwAuthSSOActivity"

.field private static final TWITTER_MARKET_URI:Ljava/lang/String; = "market://details?id=com.twitter.android"

.field private static final TWITTER_PACKAGE_NAME:Ljava/lang/String; = "com.twitter.android"

.field private static final TWITTER_SAMSUNGAPPS_URL:Ljava/lang/String; = "samsungapps://ProductDetail/com.twitter.android"

.field private static mInstanceCount:I


# instance fields
.field private mAccountManagerCallback:Landroid/accounts/AccountManagerCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/accounts/AccountManagerCallback",
            "<",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation
.end field

.field private mAppContext:Landroid/content/Context;

.field private mRetryLogin:Z

.field private mSkipSSONoti:Z

.field private mSkipSyncSetup:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 70
    const/4 v0, 0x0

    sput v0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthSSOActivity;->mInstanceCount:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 53
    invoke-direct {p0}, Landroid/accounts/AccountAuthenticatorActivity;-><init>()V

    .line 72
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthSSOActivity;->mAppContext:Landroid/content/Context;

    .line 74
    iput-boolean v1, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthSSOActivity;->mRetryLogin:Z

    .line 76
    iput-boolean v1, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthSSOActivity;->mSkipSSONoti:Z

    .line 78
    iput-boolean v1, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthSSOActivity;->mSkipSyncSetup:Z

    .line 80
    new-instance v0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthSSOActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthSSOActivity$1;-><init>(Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthSSOActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthSSOActivity;->mAccountManagerCallback:Landroid/accounts/AccountManagerCallback;

    return-void
.end method

.method private TwitterSSOAuth()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 262
    new-instance v0, Lcom/twitter/android/sdk/Twitter;

    sget-object v1, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwitter;->CONSUMER_KEY:Ljava/lang/String;

    sget-object v2, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwitter;->CONSUMER_SECRET:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/twitter/android/sdk/Twitter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 264
    .local v0, "twitterSdk":Lcom/twitter/android/sdk/Twitter;
    const/16 v1, 0x3e9

    invoke-virtual {v0, p0, v1}, Lcom/twitter/android/sdk/Twitter;->startAuthActivityForResult(Landroid/app/Activity;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 265
    invoke-direct {p0}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthSSOActivity;->isTwitterAppInstalled()Z

    move-result v1

    if-nez v1, :cond_3

    .line 266
    const-string v1, "SnsAccountTwAuthSSOActivity"

    const-string v2, "Twitter app is not installed"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 267
    invoke-direct {p0}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthSSOActivity;->isTablet()Z

    move-result v1

    if-ne v1, v3, :cond_1

    invoke-static {}, Lcom/sec/android/app/sns3/SnsFeatureManager;->isVerizonDevice()Z

    move-result v1

    if-nez v1, :cond_1

    .line 268
    invoke-direct {p0}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthSSOActivity;->gotoSamsungAppsForTwitter()V

    .line 280
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthSSOActivity;->finish()V

    .line 282
    :cond_0
    return-void

    .line 270
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthSSOActivity;->isLoggedInGoogle()Z

    move-result v1

    if-ne v1, v3, :cond_2

    .line 271
    invoke-direct {p0}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthSSOActivity;->gotoMarketForTwitter()V

    goto :goto_0

    .line 273
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthSSOActivity;->showAlertDialog()V

    goto :goto_0

    .line 277
    :cond_3
    const-string v1, "SnsAccountTwAuthSSOActivity"

    const-string v2, "TwitterSSOAuth() : Unknown error"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthSSOActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthSSOActivity;

    .prologue
    .line 53
    iget-boolean v0, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthSSOActivity;->mSkipSyncSetup:Z

    return v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthSSOActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthSSOActivity;

    .prologue
    .line 53
    iget-boolean v0, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthSSOActivity;->mRetryLogin:Z

    return v0
.end method

.method private gotoMarketForTwitter()V
    .locals 5

    .prologue
    .line 329
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 330
    .local v1, "marketIntent":Landroid/content/Intent;
    const-string v2, "market://details?id=com.twitter.android"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 331
    const/high16 v2, 0x10000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 333
    :try_start_0
    invoke-virtual {p0, v1}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthSSOActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 338
    :goto_0
    return-void

    .line 334
    :catch_0
    move-exception v0

    .line 335
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthSSOActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f080035

    const/4 v4, 0x1

    invoke-static {v2, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method private gotoSamsungAppsForTwitter()V
    .locals 4

    .prologue
    .line 317
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 318
    .local v1, "marketIntent":Landroid/content/Intent;
    const-string v2, "samsungapps://ProductDetail/com.twitter.android"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 319
    const/high16 v2, 0x10000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 321
    :try_start_0
    invoke-virtual {p0, v1}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthSSOActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 326
    :goto_0
    return-void

    .line 322
    :catch_0
    move-exception v0

    .line 323
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    const-string v2, "SnsAccountTwAuthSSOActivity"

    const-string v3, "SamsungApps is not existed"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 324
    invoke-direct {p0}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthSSOActivity;->gotoMarketForTwitter()V

    goto :goto_0
.end method

.method private isLoggedInGoogle()Z
    .locals 3

    .prologue
    .line 308
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    const-string v2, "com.google"

    invoke-virtual {v1, v2}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 309
    .local v0, "account":[Landroid/accounts/Account;
    if-eqz v0, :cond_0

    array-length v1, v0

    if-lez v1, :cond_0

    .line 310
    const/4 v1, 0x1

    .line 312
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private isLoggedInTwitter()Z
    .locals 3

    .prologue
    .line 298
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    const-string v2, "com.twitter.android.auth.login"

    invoke-virtual {v1, v2}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 300
    .local v0, "account":[Landroid/accounts/Account;
    if-eqz v0, :cond_0

    array-length v1, v0

    if-lez v1, :cond_0

    .line 301
    const/4 v1, 0x1

    .line 303
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isRunning()Z
    .locals 1

    .prologue
    .line 232
    sget v0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthSSOActivity;->mInstanceCount:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isTablet()Z
    .locals 2

    .prologue
    .line 354
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthSSOActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit8 v0, v0, 0x3

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthSSOActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    .line 356
    :cond_0
    const/4 v0, 0x1

    .line 358
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isTwitterAppInstalled()Z
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 286
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthSSOActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    const-string v4, "com.twitter.android"

    const/16 v5, 0x80

    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 288
    .local v1, "twitterPkg":Landroid/content/pm/PackageInfo;
    if-eqz v1, :cond_0

    iget v3, v1, Landroid/content/pm/PackageInfo;->versionCode:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v3, :cond_0

    .line 294
    .end local v1    # "twitterPkg":Landroid/content/pm/PackageInfo;
    :goto_0
    return v2

    .line 291
    :catch_0
    move-exception v0

    .line 292
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    goto :goto_0

    .line 294
    .end local v0    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v1    # "twitterPkg":Landroid/content/pm/PackageInfo;
    :cond_0
    const/4 v2, 0x1

    goto :goto_0
.end method

.method private showAlertDialog()V
    .locals 8

    .prologue
    const v7, 0x7f08006d

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 341
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/sns3/auth/SnsAccountAlertDialogActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 342
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "icon_id"

    const v2, 0x7f020018

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 343
    const-string v1, "title"

    const v2, 0x7f080046

    new-array v3, v6, [Ljava/lang/Object;

    invoke-virtual {p0, v7}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthSSOActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {p0, v2, v3}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthSSOActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 345
    const-string v1, "dlg_title"

    const v2, 0x7f08000d

    invoke-virtual {p0, v2}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthSSOActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 346
    const-string v1, "dlg_message"

    const v2, 0x7f08003c

    new-array v3, v6, [Ljava/lang/Object;

    invoke-virtual {p0, v7}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthSSOActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {p0, v2, v3}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthSSOActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 348
    const-string v1, "url"

    const-string v2, "market://details?id=com.twitter.android"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 349
    const/high16 v1, 0x800000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 350
    invoke-virtual {p0, v0}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthSSOActivity;->startActivity(Landroid/content/Intent;)V

    .line 351
    return-void
.end method


# virtual methods
.method public loginSuccess(Ljava/lang/String;Ljava/lang/String;)V
    .locals 10
    .param p1, "userName"    # Ljava/lang/String;
    .param p2, "userId"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 236
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 237
    .local v4, "options":Landroid/os/Bundle;
    invoke-static {}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->isLoggable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 238
    const-string v0, "SnsAccountTwAuthSSOActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SnsAccountTwAuthSSOActivity : addAccount() :  userName = "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 241
    :cond_0
    const-string v0, "username"

    invoke-virtual {v4, v0, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 243
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthSSOActivity;->mAppContext:Landroid/content/Context;

    if-nez v0, :cond_1

    .line 244
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthSSOActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    const-string v1, "com.sec.android.app.sns3.twitter"

    move-object v3, v2

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Landroid/accounts/AccountManager;->addAccount(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Landroid/os/Bundle;Landroid/app/Activity;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    .line 250
    :goto_0
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/SnsApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    .line 251
    .local v8, "cr":Landroid/content/ContentResolver;
    new-instance v9, Landroid/content/ContentValues;

    invoke-direct {v9}, Landroid/content/ContentValues;-><init>()V

    .line 252
    .local v9, "values":Landroid/content/ContentValues;
    sget-object v0, Lcom/sec/android/app/sns3/agent/sp/twitter/db/SnsTwitterDB$UserTwBasicInfo;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v8, v0, v2, v2}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 254
    invoke-virtual {v9}, Landroid/content/ContentValues;->clear()V

    .line 256
    const-string v0, "user_id"

    invoke-virtual {v9, v0, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 257
    const-string v0, "user_name"

    invoke-virtual {v9, v0, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 258
    sget-object v0, Lcom/sec/android/app/sns3/agent/sp/twitter/db/SnsTwitterDB$UserTwBasicInfo;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v8, v0, v9}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 259
    return-void

    .line 247
    .end local v8    # "cr":Landroid/content/ContentResolver;
    .end local v9    # "values":Landroid/content/ContentValues;
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthSSOActivity;->mAppContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    const-string v1, "com.sec.android.app.sns3.twitter"

    iget-object v6, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthSSOActivity;->mAccountManagerCallback:Landroid/accounts/AccountManagerCallback;

    move-object v3, v2

    move-object v5, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Landroid/accounts/AccountManager;->addAccount(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Landroid/os/Bundle;Landroid/app/Activity;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    goto :goto_0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 10
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 199
    const/16 v6, 0x3e8

    if-ne p1, v6, :cond_1

    .line 200
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthSSOActivity;->finish()V

    .line 229
    :cond_0
    :goto_0
    return-void

    .line 201
    :cond_1
    const/16 v6, 0x3e9

    if-ne p1, v6, :cond_0

    .line 202
    const/4 v6, -0x1

    if-ne p2, v6, :cond_3

    .line 203
    const-string v6, "tk"

    invoke-virtual {p3, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 204
    .local v2, "token":Ljava/lang/String;
    const-string v6, "ts"

    invoke-virtual {p3, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 205
    .local v3, "token_secret":Ljava/lang/String;
    const-string v6, "screen_name"

    invoke-virtual {p3, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 206
    .local v0, "screen_name":Ljava/lang/String;
    const-string v6, "user_id"

    const-wide/16 v8, 0x0

    invoke-virtual {p3, v6, v8, v9}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    .line 208
    .local v5, "user_id":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->isLoggable()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 209
    const-string v6, "SnsAccountTwAuthSSOActivity"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "token : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 210
    const-string v6, "SnsAccountTwAuthSSOActivity"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "token_secret : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 211
    const-string v6, "SnsAccountTwAuthSSOActivity"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "screen_name : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 212
    const-string v6, "SnsAccountTwAuthSSOActivity"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "user_id : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 215
    :cond_2
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/sns3/SnsApplication;->getSvcMgr()Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v1

    .line 216
    .local v1, "svcMgr":Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    invoke-virtual {v1}, Lcom/sec/android/app/sns3/svc/SnsSvcMgr;->getTokenMgr()Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;

    move-result-object v6

    const-string v7, "twitter"

    invoke-virtual {v6, v7}, Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;->getToken(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;

    .line 217
    .local v4, "twToken":Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;
    invoke-virtual {v4, v2, v3, v5, v0}, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;->setTokenInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 219
    invoke-virtual {p0, v0, v5}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthSSOActivity;->loginSuccess(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 221
    .end local v0    # "screen_name":Ljava/lang/String;
    .end local v1    # "svcMgr":Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    .end local v2    # "token":Ljava/lang/String;
    .end local v3    # "token_secret":Ljava/lang/String;
    .end local v4    # "twToken":Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;
    .end local v5    # "user_id":Ljava/lang/String;
    :cond_3
    iget-boolean v6, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthSSOActivity;->mSkipSSONoti:Z

    if-nez v6, :cond_4

    .line 222
    new-instance v6, Landroid/content/Intent;

    const-string v7, "com.sec.android.app.sns3.auth.sp.twitter.SNS_LOGIN_TWITTER_NOTIFICATION"

    invoke-direct {v6, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v6}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthSSOActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 225
    :cond_4
    const-string v6, "SnsAccountTwAuthSSOActivity"

    const-string v7, "SSO failed"

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 226
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthSSOActivity;->finish()V

    goto/16 :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 11
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 148
    invoke-static {p0}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->isLightTheme(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x1030132

    :goto_0
    invoke-virtual {p0, v0}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthSSOActivity;->setTheme(I)V

    .line 151
    invoke-super {p0, p1}, Landroid/accounts/AccountAuthenticatorActivity;->onCreate(Landroid/os/Bundle;)V

    .line 153
    sget v0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthSSOActivity;->mInstanceCount:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthSSOActivity;->mInstanceCount:I

    .line 154
    sget v0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthSSOActivity;->mInstanceCount:I

    if-le v0, v5, :cond_0

    .line 155
    const-string v0, "SnsAccountTwAuthSSOActivity"

    const-string v1, "multi instance"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 158
    :cond_0
    iput-object p0, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthSSOActivity;->mAppContext:Landroid/content/Context;

    .line 160
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthSSOActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "RetryLogin"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthSSOActivity;->mRetryLogin:Z

    .line 161
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthSSOActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "skip_sso_noti"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthSSOActivity;->mSkipSSONoti:Z

    .line 162
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthSSOActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "skip_sync_setup"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthSSOActivity;->mSkipSyncSetup:Z

    .line 164
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    const-string v1, "com.sec.android.app.sns3.twitter"

    invoke-virtual {v0, v1}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v8

    .line 165
    .local v8, "accounts":[Landroid/accounts/Account;
    array-length v0, v8

    if-lez v0, :cond_3

    iget-boolean v0, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthSSOActivity;->mRetryLogin:Z

    if-nez v0, :cond_3

    .line 166
    invoke-direct {p0}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthSSOActivity;->isLoggedInTwitter()Z

    move-result v0

    if-nez v0, :cond_2

    .line 167
    const-string v0, "SnsAccountTwAuthSSOActivity"

    const-string v1, "login to twitter app only"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 169
    :try_start_0
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    const-string v1, "com.twitter.android.auth.login"

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v5, p0

    invoke-virtual/range {v0 .. v7}, Landroid/accounts/AccountManager;->addAccount(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Landroid/os/Bundle;Landroid/app/Activity;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 180
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthSSOActivity;->finish()V

    .line 189
    :goto_2
    return-void

    .line 148
    .end local v8    # "accounts":[Landroid/accounts/Account;
    :cond_1
    const v0, 0x103012e

    goto :goto_0

    .line 171
    .restart local v8    # "accounts":[Landroid/accounts/Account;
    :catch_0
    move-exception v9

    .line 172
    .local v9, "e":Ljava/lang/Exception;
    invoke-virtual {v9}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 175
    .end local v9    # "e":Ljava/lang/Exception;
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthSSOActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const/high16 v1, 0x7f080000

    new-array v2, v5, [Ljava/lang/Object;

    const v3, 0x7f08006d

    invoke-virtual {p0, v3}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthSSOActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthSSOActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_1

    .line 184
    :cond_3
    const-string v0, "notification"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthSSOActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/app/NotificationManager;

    .line 185
    .local v10, "notiMgr":Landroid/app/NotificationManager;
    const/16 v0, 0x834

    invoke-virtual {v10, v0}, Landroid/app/NotificationManager;->cancel(I)V

    .line 186
    const/16 v0, 0x8fc

    invoke-virtual {v10, v0}, Landroid/app/NotificationManager;->cancel(I)V

    .line 188
    invoke-direct {p0}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthSSOActivity;->TwitterSSOAuth()V

    goto :goto_2
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 193
    sget v0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthSSOActivity;->mInstanceCount:I

    add-int/lit8 v0, v0, -0x1

    sput v0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthSSOActivity;->mInstanceCount:I

    .line 194
    invoke-super {p0}, Landroid/accounts/AccountAuthenticatorActivity;->onDestroy()V

    .line 195
    return-void
.end method
