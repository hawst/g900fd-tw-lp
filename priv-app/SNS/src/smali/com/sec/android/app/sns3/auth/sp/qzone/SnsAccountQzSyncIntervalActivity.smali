.class public Lcom/sec/android/app/sns3/auth/sp/qzone/SnsAccountQzSyncIntervalActivity;
.super Landroid/preference/PreferenceActivity;
.source "SnsAccountQzSyncIntervalActivity.java"


# static fields
.field private static final DEFAULT_SYNC_INTERVAL:J = 0x0L

.field private static final SYNC_INTERVAL_KEY:Ljava/lang/String; = "sync_interval"

.field private static final TAG:Ljava/lang/String; = "SnsAccountQzAuth"


# instance fields
.field private mAppContext:Landroid/content/Context;

.field private mProfileFeedIntervalPreference:Landroid/preference/ListPreference;

.field private mProfileFeedSharedPref:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    .line 55
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/qzone/SnsAccountQzSyncIntervalActivity;->mProfileFeedIntervalPreference:Landroid/preference/ListPreference;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/sns3/auth/sp/qzone/SnsAccountQzSyncIntervalActivity;)Landroid/preference/ListPreference;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sns3/auth/sp/qzone/SnsAccountQzSyncIntervalActivity;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/qzone/SnsAccountQzSyncIntervalActivity;->mProfileFeedIntervalPreference:Landroid/preference/ListPreference;

    return-object v0
.end method


# virtual methods
.method isTable()Z
    .locals 2

    .prologue
    .line 187
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/qzone/SnsAccountQzSyncIntervalActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit8 v0, v0, 0x3

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/qzone/SnsAccountQzSyncIntervalActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    .line 190
    :cond_0
    const/4 v0, 0x1

    .line 192
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 12
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const v11, 0x7f080009

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 62
    iput-object p0, p0, Lcom/sec/android/app/sns3/auth/sp/qzone/SnsAccountQzSyncIntervalActivity;->mAppContext:Landroid/content/Context;

    .line 64
    iget-object v6, p0, Lcom/sec/android/app/sns3/auth/sp/qzone/SnsAccountQzSyncIntervalActivity;->mAppContext:Landroid/content/Context;

    invoke-static {v6}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->isLightTheme(Landroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_5

    const v6, 0x103012b

    :goto_0
    invoke-virtual {p0, v6}, Lcom/sec/android/app/sns3/auth/sp/qzone/SnsAccountQzSyncIntervalActivity;->setTheme(I)V

    .line 67
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 69
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/qzone/SnsAccountQzSyncIntervalActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 70
    .local v0, "actionBar":Landroid/app/ActionBar;
    invoke-virtual {v0, v10}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 72
    const v6, 0x7f030005

    invoke-virtual {p0, v6}, Lcom/sec/android/app/sns3/auth/sp/qzone/SnsAccountQzSyncIntervalActivity;->setContentView(I)V

    .line 73
    const v6, 0x7f080046

    new-array v7, v10, [Ljava/lang/Object;

    const v8, 0x7f08003b

    invoke-virtual {p0, v8}, Lcom/sec/android/app/sns3/auth/sp/qzone/SnsAccountQzSyncIntervalActivity;->getString(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v9

    invoke-virtual {p0, v6, v7}, Lcom/sec/android/app/sns3/auth/sp/qzone/SnsAccountQzSyncIntervalActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/sec/android/app/sns3/auth/sp/qzone/SnsAccountQzSyncIntervalActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 74
    const v6, 0x7f04000d

    invoke-virtual {p0, v6}, Lcom/sec/android/app/sns3/auth/sp/qzone/SnsAccountQzSyncIntervalActivity;->addPreferencesFromResource(I)V

    .line 76
    const-string v6, "sync_interval"

    invoke-virtual {p0, v6}, Lcom/sec/android/app/sns3/auth/sp/qzone/SnsAccountQzSyncIntervalActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    check-cast v2, Landroid/preference/PreferenceScreen;

    .line 78
    .local v2, "preferenceScreen":Landroid/preference/PreferenceScreen;
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v6

    const-string v7, "com.sec.android.app.sns3.qzone"

    invoke-virtual {v6, v7}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v5

    .line 81
    .local v5, "sns3Account":[Landroid/accounts/Account;
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/sns3/SnsApplication;->getFeatureMgr()Lcom/sec/android/app/sns3/SnsFeatureManager;

    move-result-object v1

    .line 82
    .local v1, "featureMgr":Lcom/sec/android/app/sns3/SnsFeatureManager;
    const/4 v4, 0x0

    .line 83
    .local v4, "providerLabel":Ljava/lang/CharSequence;
    const/4 v3, 0x0

    .line 86
    .local v3, "providerInfo":Landroid/content/pm/ProviderInfo;
    invoke-virtual {v1}, Lcom/sec/android/app/sns3/SnsFeatureManager;->isSyncAdapterForProfileFeedsNeeded()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 87
    const/4 v4, 0x0

    .line 88
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/qzone/SnsAccountQzSyncIntervalActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    const-string v7, "com.sec.android.app.sns3.life"

    invoke-virtual {v6, v7, v9}, Landroid/content/pm/PackageManager;->resolveContentProvider(Ljava/lang/String;I)Landroid/content/pm/ProviderInfo;

    move-result-object v3

    .line 89
    if-eqz v3, :cond_0

    .line 90
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/qzone/SnsAccountQzSyncIntervalActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    invoke-virtual {v3, v6}, Landroid/content/pm/ProviderInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v4

    .line 92
    :cond_0
    if-eqz v4, :cond_1

    invoke-interface {v4}, Ljava/lang/CharSequence;->length()I

    move-result v6

    if-nez v6, :cond_2

    .line 93
    :cond_1
    const-string v6, "SnsAccountQzAuth"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "providerLabel is null - "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 94
    const-string v4, "Life Times"

    .line 97
    :cond_2
    new-instance v6, Landroid/preference/ListPreference;

    invoke-direct {v6, p0}, Landroid/preference/ListPreference;-><init>(Landroid/content/Context;)V

    iput-object v6, p0, Lcom/sec/android/app/sns3/auth/sp/qzone/SnsAccountQzSyncIntervalActivity;->mProfileFeedIntervalPreference:Landroid/preference/ListPreference;

    .line 98
    iget-object v6, p0, Lcom/sec/android/app/sns3/auth/sp/qzone/SnsAccountQzSyncIntervalActivity;->mProfileFeedIntervalPreference:Landroid/preference/ListPreference;

    new-array v7, v10, [Ljava/lang/Object;

    aput-object v4, v7, v9

    invoke-virtual {p0, v11, v7}, Lcom/sec/android/app/sns3/auth/sp/qzone/SnsAccountQzSyncIntervalActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/preference/ListPreference;->setTitle(Ljava/lang/CharSequence;)V

    .line 99
    iget-object v6, p0, Lcom/sec/android/app/sns3/auth/sp/qzone/SnsAccountQzSyncIntervalActivity;->mProfileFeedIntervalPreference:Landroid/preference/ListPreference;

    invoke-virtual {v6, v9}, Landroid/preference/ListPreference;->setPersistent(Z)V

    .line 100
    iget-object v6, p0, Lcom/sec/android/app/sns3/auth/sp/qzone/SnsAccountQzSyncIntervalActivity;->mProfileFeedIntervalPreference:Landroid/preference/ListPreference;

    const/high16 v7, 0x7f050000

    invoke-virtual {v6, v7}, Landroid/preference/ListPreference;->setEntries(I)V

    .line 101
    iget-object v6, p0, Lcom/sec/android/app/sns3/auth/sp/qzone/SnsAccountQzSyncIntervalActivity;->mProfileFeedIntervalPreference:Landroid/preference/ListPreference;

    const v7, 0x7f050001

    invoke-virtual {v6, v7}, Landroid/preference/ListPreference;->setEntryValues(I)V

    .line 102
    sget-object v6, Lcom/sec/android/app/sns3/SnsFeatureManager;->ANGLE_INDICATION_SUPPORTED:Ljava/lang/Boolean;

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 103
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/qzone/SnsAccountQzSyncIntervalActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->isLightTheme(Landroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 104
    iget-object v6, p0, Lcom/sec/android/app/sns3/auth/sp/qzone/SnsAccountQzSyncIntervalActivity;->mProfileFeedIntervalPreference:Landroid/preference/ListPreference;

    const v7, 0x7f030003

    invoke-virtual {v6, v7}, Landroid/preference/ListPreference;->setWidgetLayoutResource(I)V

    .line 109
    :cond_3
    :goto_1
    iget-object v6, p0, Lcom/sec/android/app/sns3/auth/sp/qzone/SnsAccountQzSyncIntervalActivity;->mProfileFeedIntervalPreference:Landroid/preference/ListPreference;

    new-array v7, v10, [Ljava/lang/Object;

    aput-object v4, v7, v9

    invoke-virtual {p0, v11, v7}, Lcom/sec/android/app/sns3/auth/sp/qzone/SnsAccountQzSyncIntervalActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/preference/ListPreference;->setDialogTitle(Ljava/lang/CharSequence;)V

    .line 111
    array-length v6, v5

    if-lez v6, :cond_7

    .line 112
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/qzone/SnsAccountQzSyncIntervalActivity;->readProfileFeedSyncInterval()V

    .line 114
    iget-object v6, p0, Lcom/sec/android/app/sns3/auth/sp/qzone/SnsAccountQzSyncIntervalActivity;->mProfileFeedIntervalPreference:Landroid/preference/ListPreference;

    new-instance v7, Lcom/sec/android/app/sns3/auth/sp/qzone/SnsAccountQzSyncIntervalActivity$1;

    invoke-direct {v7, p0, v5}, Lcom/sec/android/app/sns3/auth/sp/qzone/SnsAccountQzSyncIntervalActivity$1;-><init>(Lcom/sec/android/app/sns3/auth/sp/qzone/SnsAccountQzSyncIntervalActivity;[Landroid/accounts/Account;)V

    invoke-virtual {v6, v7}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 146
    :goto_2
    iget-object v6, p0, Lcom/sec/android/app/sns3/auth/sp/qzone/SnsAccountQzSyncIntervalActivity;->mProfileFeedIntervalPreference:Landroid/preference/ListPreference;

    invoke-virtual {v2, v6}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 148
    :cond_4
    return-void

    .line 64
    .end local v0    # "actionBar":Landroid/app/ActionBar;
    .end local v1    # "featureMgr":Lcom/sec/android/app/sns3/SnsFeatureManager;
    .end local v2    # "preferenceScreen":Landroid/preference/PreferenceScreen;
    .end local v3    # "providerInfo":Landroid/content/pm/ProviderInfo;
    .end local v4    # "providerLabel":Ljava/lang/CharSequence;
    .end local v5    # "sns3Account":[Landroid/accounts/Account;
    :cond_5
    const v6, 0x1030128

    goto/16 :goto_0

    .line 106
    .restart local v0    # "actionBar":Landroid/app/ActionBar;
    .restart local v1    # "featureMgr":Lcom/sec/android/app/sns3/SnsFeatureManager;
    .restart local v2    # "preferenceScreen":Landroid/preference/PreferenceScreen;
    .restart local v3    # "providerInfo":Landroid/content/pm/ProviderInfo;
    .restart local v4    # "providerLabel":Ljava/lang/CharSequence;
    .restart local v5    # "sns3Account":[Landroid/accounts/Account;
    :cond_6
    iget-object v6, p0, Lcom/sec/android/app/sns3/auth/sp/qzone/SnsAccountQzSyncIntervalActivity;->mProfileFeedIntervalPreference:Landroid/preference/ListPreference;

    const v7, 0x7f030002

    invoke-virtual {v6, v7}, Landroid/preference/ListPreference;->setWidgetLayoutResource(I)V

    goto :goto_1

    .line 143
    :cond_7
    iget-object v6, p0, Lcom/sec/android/app/sns3/auth/sp/qzone/SnsAccountQzSyncIntervalActivity;->mProfileFeedIntervalPreference:Landroid/preference/ListPreference;

    invoke-virtual {v6, v9}, Landroid/preference/ListPreference;->setEnabled(Z)V

    goto :goto_2
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 153
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 158
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 155
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/qzone/SnsAccountQzSyncIntervalActivity;->finish()V

    .line 156
    const/4 v0, 0x1

    goto :goto_0

    .line 153
    nop

    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 163
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onResume()V

    .line 164
    return-void
.end method

.method public readProfileFeedSyncInterval()V
    .locals 6

    .prologue
    .line 167
    const-string v1, "QZ_profile_feed_sync_interval"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/sns3/auth/sp/qzone/SnsAccountQzSyncIntervalActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/qzone/SnsAccountQzSyncIntervalActivity;->mProfileFeedSharedPref:Landroid/content/SharedPreferences;

    .line 170
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/qzone/SnsAccountQzSyncIntervalActivity;->mProfileFeedSharedPref:Landroid/content/SharedPreferences;

    const-string v2, "QZ_profile_feed_sync_interval"

    const-wide/16 v4, 0x0

    invoke-interface {v1, v2, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    .line 173
    .local v0, "savedProfileFeedInterval":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/qzone/SnsAccountQzSyncIntervalActivity;->mProfileFeedIntervalPreference:Landroid/preference/ListPreference;

    invoke-virtual {v1, v0}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 175
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/qzone/SnsAccountQzSyncIntervalActivity;->mProfileFeedIntervalPreference:Landroid/preference/ListPreference;

    invoke-virtual {v1}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 176
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/qzone/SnsAccountQzSyncIntervalActivity;->mProfileFeedIntervalPreference:Landroid/preference/ListPreference;

    iget-object v2, p0, Lcom/sec/android/app/sns3/auth/sp/qzone/SnsAccountQzSyncIntervalActivity;->mProfileFeedIntervalPreference:Landroid/preference/ListPreference;

    invoke-virtual {v2}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 178
    :cond_0
    return-void
.end method

.method public writeProfileFeedInterval(Ljava/lang/Long;)V
    .locals 4
    .param p1, "value"    # Ljava/lang/Long;

    .prologue
    .line 181
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/qzone/SnsAccountQzSyncIntervalActivity;->mProfileFeedSharedPref:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 182
    .local v0, "profileFeedEditor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "QZ_profile_feed_sync_interval"

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 183
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 184
    return-void
.end method
