.class Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService$1;
.super Lcom/sec/android/app/sns3/svc/sp/twitter/api/ISnsTwitter$Stub;
.source "SnsTwService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private mStringArray:[Ljava/lang/String;

.field final synthetic this$0:Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService;


# direct methods
.method constructor <init>(Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService;)V
    .locals 0

    .prologue
    .line 92
    iput-object p1, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService$1;->this$0:Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService;

    invoke-direct {p0}, Lcom/sec/android/app/sns3/svc/sp/twitter/api/ISnsTwitter$Stub;-><init>()V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService$1;)[Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService$1;

    .prologue
    .line 92
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService$1;->mStringArray:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$102(Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService$1;[Ljava/lang/String;)[Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService$1;
    .param p1, "x1"    # [Ljava/lang/String;

    .prologue
    .line 92
    iput-object p1, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService$1;->mStringArray:[Ljava/lang/String;

    return-object p1
.end method


# virtual methods
.method public getApplicationInfo()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 105
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 106
    .local v0, "appInfo":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v1, "authorize_url"

    const-string v2, "https://api.twitter.com/oauth/authorize"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 107
    const-string v1, "oauth_callback"

    const-string v2, "http://www.facebook.com/SamsungMobile"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 108
    return-object v0
.end method

.method public getHomeTimeline(Landroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/twitter/api/ISnsTwitterCallbackStatus;)I
    .locals 6
    .param p1, "param"    # Landroid/os/Bundle;
    .param p2, "cb"    # Lcom/sec/android/app/sns3/svc/sp/twitter/api/ISnsTwitterCallbackStatus;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 326
    const-string v1, "SNS"

    const-string v2, "AIDL<Service>: getHomeTimeline API"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 334
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService$1$5;

    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService$1;->this$0:Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService;

    # getter for: Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    invoke-static {v1}, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService;->access$000(Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService;)Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;->STATUESE_HOME_TIMELINE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService$1$5;-><init>(Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService$1;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;Landroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/twitter/api/ISnsTwitterCallbackStatus;)V

    .line 349
    .local v0, "request":Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestStatuses;
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestStatuses;->request()Z

    .line 351
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestStatuses;->getReqID()I

    move-result v1

    return v1
.end method

.method public getUsersLookup(Landroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/twitter/api/ISnsTwitterCallbackUser;)I
    .locals 8
    .param p1, "param"    # Landroid/os/Bundle;
    .param p2, "cb"    # Lcom/sec/android/app/sns3/svc/sp/twitter/api/ISnsTwitterCallbackUser;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 247
    const-string v1, "SNS"

    const-string v2, "AIDL<Service>: getUsersLookup API"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 249
    new-instance v7, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService$1$3;

    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService$1;->this$0:Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService;

    # getter for: Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    invoke-static {v1}, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService;->access$000(Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService;)Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v1

    invoke-direct {v7, p0, v1, p1}, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService$1$3;-><init>(Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService$1;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Bundle;)V

    .line 277
    .local v7, "request":Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwReqGetFriendsIds;
    invoke-virtual {v7}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwReqGetFriendsIds;->request()Z

    .line 279
    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService$1;->this$0:Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService;

    # getter for: Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService;->mLock:Ljava/lang/Object;
    invoke-static {v1}, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService;->access$200(Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService;)Ljava/lang/Object;

    move-result-object v2

    monitor-enter v2

    .line 281
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService$1;->this$0:Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService;

    # getter for: Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService;->mLock:Ljava/lang/Object;
    invoke-static {v1}, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService;->access$200(Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->wait()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 285
    :goto_0
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 287
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 288
    .local v4, "param2":Landroid/os/Bundle;
    sget-object v1, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwUsersAPI$Param;->USER_ID:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwUsersAPI$Param;

    invoke-virtual {v1}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwUsersAPI$Param;->getParam()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService$1;->mStringArray:[Ljava/lang/String;

    const-string v3, ","

    invoke-static {v2, v3}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->arrayToString([Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 291
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService$1$4;

    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService$1;->this$0:Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService;

    # getter for: Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    invoke-static {v1}, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService;->access$000(Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService;)Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwUsersAPI;->USERS_LOOKUP:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwUsersAPI;

    move-object v1, p0

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService$1$4;-><init>(Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService$1;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwUsersAPI;Landroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/twitter/api/ISnsTwitterCallbackUser;)V

    .line 310
    .local v0, "request2":Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestUsers;
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestUsers;->request()Z

    .line 312
    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService$1;->this$0:Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService;

    # getter for: Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService;->mLock:Ljava/lang/Object;
    invoke-static {v1}, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService;->access$200(Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService;)Ljava/lang/Object;

    move-result-object v2

    monitor-enter v2

    .line 314
    :try_start_2
    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService$1;->this$0:Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService;

    # getter for: Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService;->mLock:Ljava/lang/Object;
    invoke-static {v1}, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService;->access$200(Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->wait()V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 318
    :goto_1
    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 320
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestUsers;->getReqID()I

    move-result v1

    return v1

    .line 282
    .end local v0    # "request2":Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestUsers;
    .end local v4    # "param2":Landroid/os/Bundle;
    :catch_0
    move-exception v6

    .line 283
    .local v6, "e":Ljava/lang/InterruptedException;
    :try_start_4
    invoke-virtual {v6}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0

    .line 285
    .end local v6    # "e":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v1

    .line 315
    .restart local v0    # "request2":Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestUsers;
    .restart local v4    # "param2":Landroid/os/Bundle;
    :catch_1
    move-exception v6

    .line 316
    .restart local v6    # "e":Ljava/lang/InterruptedException;
    :try_start_5
    invoke-virtual {v6}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_1

    .line 318
    .end local v6    # "e":Ljava/lang/InterruptedException;
    :catchall_1
    move-exception v1

    monitor-exit v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    throw v1
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 2
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 97
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->hasAccessToRestrictedData(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 98
    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "Access denied"

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 100
    :cond_0
    invoke-super {p0, p1, p2, p3, p4}, Lcom/sec/android/app/sns3/svc/sp/twitter/api/ISnsTwitter$Stub;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v0

    return v0
.end method

.method public postStatus(Landroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/twitter/api/ISnsTwitterCallbackStatus;)I
    .locals 6
    .param p1, "param"    # Landroid/os/Bundle;
    .param p2, "cb"    # Lcom/sec/android/app/sns3/svc/sp/twitter/api/ISnsTwitterCallbackStatus;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 195
    const-string v1, "SNS"

    const-string v2, "AIDL<Service>: postStatus API"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 197
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService$1$1;

    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService$1;->this$0:Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService;

    # getter for: Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    invoke-static {v1}, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService;->access$000(Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService;)Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;->STATUSES_UPDATE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService$1$1;-><init>(Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService$1;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;Landroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/twitter/api/ISnsTwitterCallbackStatus;)V

    .line 212
    .local v0, "request":Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestStatuses;
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestStatuses;->request()Z

    .line 214
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestStatuses;->getReqID()I

    move-result v1

    return v1
.end method

.method public postStatusWithMedia(Landroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/twitter/api/ISnsTwitterCallbackStatus;)I
    .locals 6
    .param p1, "param"    # Landroid/os/Bundle;
    .param p2, "cb"    # Lcom/sec/android/app/sns3/svc/sp/twitter/api/ISnsTwitterCallbackStatus;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 220
    const-string v1, "SNS"

    const-string v2, "AIDL<Service>: postStatusWithMedia API"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 222
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService$1$2;

    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService$1;->this$0:Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService;

    # getter for: Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    invoke-static {v1}, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService;->access$000(Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService;)Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;->STATUSES_UPDATE_WITH_MEDIA:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService$1$2;-><init>(Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService$1;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;Landroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/twitter/api/ISnsTwitterCallbackStatus;)V

    .line 237
    .local v0, "request":Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestStatuses;
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestStatuses;->request()Z

    .line 239
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestStatuses;->getReqID()I

    move-result v1

    return v1
.end method
