.class public Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwServiceForAuthToken;
.super Landroid/app/Service;
.source "SnsTwServiceForAuthToken.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwServiceForAuthToken$SnsTwServiceBinder;
    }
.end annotation


# instance fields
.field private final mBinder:Landroid/os/IBinder;

.field private mHandler:Landroid/os/Handler;

.field private final mSnsSvcTwitterForAuthTokenBinder:Lcom/sec/android/app/sns3/svc/sp/twitter/auth/api/ISnsTwitterForAuthToken$Stub;

.field private mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 46
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwServiceForAuthToken$SnsTwServiceBinder;

    invoke-direct {v0, p0}, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwServiceForAuthToken$SnsTwServiceBinder;-><init>(Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwServiceForAuthToken;)V

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwServiceForAuthToken;->mBinder:Landroid/os/IBinder;

    .line 56
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwServiceForAuthToken;->mHandler:Landroid/os/Handler;

    .line 84
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwServiceForAuthToken$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwServiceForAuthToken$1;-><init>(Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwServiceForAuthToken;)V

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwServiceForAuthToken;->mSnsSvcTwitterForAuthTokenBinder:Lcom/sec/android/app/sns3/svc/sp/twitter/auth/api/ISnsTwitterForAuthToken$Stub;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwServiceForAuthToken;)Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwServiceForAuthToken;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwServiceForAuthToken;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwServiceForAuthToken;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwServiceForAuthToken;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwServiceForAuthToken;->mHandler:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 68
    const-string v0, "SNS"

    const-string v1, "SnsTwServiceForAuthToken : onBind()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 70
    const-class v0, Lcom/sec/android/app/sns3/svc/sp/twitter/auth/api/ISnsTwitterForAuthToken;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 71
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwServiceForAuthToken;->mSnsSvcTwitterForAuthTokenBinder:Lcom/sec/android/app/sns3/svc/sp/twitter/auth/api/ISnsTwitterForAuthToken$Stub;

    .line 74
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwServiceForAuthToken;->mBinder:Landroid/os/IBinder;

    goto :goto_0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 60
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 61
    const-string v0, "SNS"

    const-string v1, "SnsTwServiceForAuthToken : onCreate()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 63
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/SnsApplication;->getSvcMgr()Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwServiceForAuthToken;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    .line 64
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 79
    const-string v0, "SNS"

    const-string v1, "SnsTwServiceForAuthToken : onStartCommand()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 81
    const/4 v0, 0x1

    return v0
.end method
