.class public interface abstract Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserBirthday$FacebookBirthday;
.super Ljava/lang/Object;
.source "SnsFbParserBirthday.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserBirthday;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "FacebookBirthday"
.end annotation


# static fields
.field public static final BIRTHDAY_DATE:Ljava/lang/String; = "birthday_date"

.field public static final FIRST_NAME:Ljava/lang/String; = "first_name"

.field public static final LAST_NAME:Ljava/lang/String; = "last_name"

.field public static final NAME:Ljava/lang/String; = "name"

.field public static final PIC_BIG:Ljava/lang/String; = "pic_big"

.field public static final SEX:Ljava/lang/String; = "sex"

.field public static final TITLE_TEXT:Ljava/lang/String; = "title_text"

.field public static final UID:Ljava/lang/String; = "uid"
