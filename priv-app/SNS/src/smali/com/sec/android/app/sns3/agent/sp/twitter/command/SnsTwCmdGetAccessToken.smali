.class public Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdGetAccessToken;
.super Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;
.source "SnsTwCmdGetAccessToken.java"


# direct methods
.method public constructor <init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Handler;)V
    .locals 3
    .param p1, "svcMgr"    # Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    .param p2, "cmdHandler"    # Landroid/os/Handler;

    .prologue
    .line 38
    invoke-direct {p0, p2}, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;-><init>(Landroid/os/Handler;)V

    .line 39
    new-instance v1, Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;

    invoke-direct {v1}, Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;-><init>()V

    .line 41
    .local v1, "unit":Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;
    new-instance v0, Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdGetAccessToken$1;

    sget-object v2, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwOauthAPI;->OAUTH_ACCESS_TOKEN:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwOauthAPI;

    invoke-direct {v0, p0, p1, v2}, Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdGetAccessToken$1;-><init>(Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdGetAccessToken;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwOauthAPI;)V

    .line 59
    .local v0, "req":Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestOauth;
    invoke-virtual {v1, v0}, Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;->addRequest(Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;)V

    .line 61
    invoke-virtual {p0, v1}, Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdGetAccessToken;->addCommandUnit(Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;)V

    .line 62
    return-void
.end method


# virtual methods
.method protected respond()Z
    .locals 5

    .prologue
    .line 66
    const-string v0, "SnsAgent"

    const-string v1, "<SnsTwCmdGetAccessToken> respond()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 68
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdGetAccessToken;->getCommandCallback()Lcom/sec/android/app/sns3/agent/command/ISnsCommandCallback;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdGetAccessToken;->getCommandID()I

    move-result v1

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdGetAccessToken;->isSuccess()Z

    move-result v2

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdGetAccessToken;->getUri()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdGetAccessToken;->getResponseList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/sec/android/app/sns3/agent/command/ISnsCommandCallback;->onCmdRespond(IZLjava/lang/String;Ljava/util/List;)V

    .line 70
    const/4 v0, 0x1

    return v0
.end method
