.class public Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwitter;
.super Ljava/lang/Object;
.source "SnsTwitter.java"


# static fields
.field public static final ACCOUNT_TYPE:Ljava/lang/String; = "com.sec.android.app.sns3.twitter"

.field public static final AUTHORIZE_URL:Ljava/lang/String; = "https://api.twitter.com/oauth/authorize"

.field public static final CONSUMER_KEY:Ljava/lang/String;

.field public static final CONSUMER_SECRET:Ljava/lang/String;

.field public static final HTTPS_BASE_URL:Ljava/lang/String; = "https://api.twitter.com/"

.field public static final HTTPS_SEARCH_URL:Ljava/lang/String; = "https://search.twitter.com/"

.field public static final HTTPS_UPLOAD_URL:Ljava/lang/String; = "https://upload.twitter.com/"

.field public static final HTTP_BASE_URL:Ljava/lang/String; = "http://api.twitter.com/"

.field public static final LOGOUT_URL:Ljava/lang/String; = "http://api.twitter.com/logout"

.field public static final OAUTH_CALLBACK:Ljava/lang/String; = "http://www.facebook.com/SamsungMobile"

.field public static final OAUTH_SIGNATURE_METHOD:Ljava/lang/String; = "HMAC-SHA1"

.field public static final OAUTH_VERSION:Ljava/lang/String; = "1.0"

.field public static final SP:Ljava/lang/String; = "twitter"

.field public static final TWITTER_ACCOUNT_TYPE:Ljava/lang/String; = "com.twitter.android.auth.login"

.field public static final TWITTER_AUTHORITY:Ljava/lang/String; = "com.twitter.android.provider.TwitterProvider"

.field public static final VERSION:Ljava/lang/String; = "1.1"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    invoke-static {}, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwAppIdManager;->getInstance()Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwAppIdManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwAppIdManager;->getConsumerKey()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwitter;->CONSUMER_KEY:Ljava/lang/String;

    .line 35
    invoke-static {}, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwAppIdManager;->getInstance()Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwAppIdManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwAppIdManager;->getConsumerSecret()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwitter;->CONSUMER_SECRET:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
