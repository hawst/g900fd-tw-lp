.class public Lcom/sec/android/app/sns3/svc/sp/instagram/parser/SnsInParserUser;
.super Ljava/lang/Object;
.source "SnsInParserUser.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/instagram/parser/SnsInParserUser$InstagramUser;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    return-void
.end method

.method public static parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseUser;
    .locals 5
    .param p0, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 39
    new-instance v3, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseUser;

    invoke-direct {v3}, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseUser;-><init>()V

    .line 42
    .local v3, "user":Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseUser;
    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 43
    .local v2, "job":Lorg/json/JSONObject;
    const-string v4, "data"

    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/json/JSONObject;

    .line 45
    .local v0, "datObj":Lorg/json/JSONObject;
    const-string v4, "id"

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseUser;->mUserID:Ljava/lang/String;

    .line 46
    const-string v4, "username"

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseUser;->mUserName:Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 53
    .end local v0    # "datObj":Lorg/json/JSONObject;
    .end local v2    # "job":Lorg/json/JSONObject;
    :goto_0
    return-object v3

    .line 48
    :catch_0
    move-exception v1

    .line 50
    .local v1, "e1":Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method
