.class public interface abstract Lcom/sec/android/app/sns3/svc/sp/linkedin/parser/SnsLiParserUpdates$LinkedInUpdates;
.super Ljava/lang/Object;
.source "SnsLiParserUpdates.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/svc/sp/linkedin/parser/SnsLiParserUpdates;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "LinkedInUpdates"
.end annotation


# static fields
.field public static final ACTION:Ljava/lang/String; = "action"

.field public static final ACTIVITY_APP_ID:Ljava/lang/String; = "appId"

.field public static final ACTIVITY_BODY:Ljava/lang/String; = "body"

.field public static final APPLICATION:Ljava/lang/String; = "application"

.field public static final AUTHOR:Ljava/lang/String; = "author"

.field public static final CODE:Ljava/lang/String; = "code"

.field public static final COMMENT:Ljava/lang/String; = "comment"

.field public static final COMMENTS:Ljava/lang/String; = "updateComments"

.field public static final COMMENT_ID:Ljava/lang/String; = "id"

.field public static final COMMENT_TEXT:Ljava/lang/String; = "comment"

.field public static final COMPANY:Ljava/lang/String; = "company"

.field public static final COMPANY_JOB_UPDATE:Ljava/lang/String; = "companyJobUpdate"

.field public static final COMPANY_PERSON_UPDATE:Ljava/lang/String; = "companyPersonUpdate"

.field public static final COMPANY_PRODUCT_UPDATE:Ljava/lang/String; = "companyProductUpdate"

.field public static final COMPANY_PROF_EDITOR:Ljava/lang/String; = "editor"

.field public static final COMPANY_PROF_FIELD:Ljava/lang/String; = "profileField"

.field public static final COMPANY_PROF_UPDATE:Ljava/lang/String; = "companyProfileUpdate"

.field public static final COMPANY_STATUS_UPDATE:Ljava/lang/String; = "companyStatusUpdate"

.field public static final CONNECTIONS:Ljava/lang/String; = "connections"

.field public static final CONTENT:Ljava/lang/String; = "content"

.field public static final COUNT:Ljava/lang/String; = "_count"

.field public static final CURRENT_SHARE:Ljava/lang/String; = "currentShare"

.field public static final CURRENT_STATUS:Ljava/lang/String; = "currentStatus"

.field public static final DATE_OF_BIRTH:Ljava/lang/String; = "dateOfBirth"

.field public static final DATE_OF_BIRTH_DAY:Ljava/lang/String; = "day"

.field public static final DATE_OF_BIRTH_MONTH:Ljava/lang/String; = "month"

.field public static final DATE_OF_BIRTH_YEAR:Ljava/lang/String; = "year"

.field public static final EVENT_TYPE:Ljava/lang/String; = "eventType"

.field public static final ID:Ljava/lang/String; = "id"

.field public static final IM_ACCOUNTS:Ljava/lang/String; = "imAccounts"

.field public static final IM_ACCOUNT_NAME:Ljava/lang/String; = "imAccountName"

.field public static final IM_ACCOUNT_TYPE:Ljava/lang/String; = "imAccountType"

.field public static final IS_COMMENTABLE:Ljava/lang/String; = "isCommentable"

.field public static final IS_LIKABLE:Ljava/lang/String; = "isLikable"

.field public static final IS_LIKED:Ljava/lang/String; = "isLiked"

.field public static final JOB:Ljava/lang/String; = "job"

.field public static final JOB_POSTER:Ljava/lang/String; = "jobPoster"

.field public static final JOB_URL:Ljava/lang/String; = "siteJobRequest"

.field public static final LIKES:Ljava/lang/String; = "likes"

.field public static final MAIN_ADDRESS:Ljava/lang/String; = "mainAddress"

.field public static final MEMBER_GROUP:Ljava/lang/String; = "memberGroup"

.field public static final MEMBER_GROUPS:Ljava/lang/String; = "memberGroups"

.field public static final NAME:Ljava/lang/String; = "name"

.field public static final NEW_POSITION:Ljava/lang/String; = "newPosition"

.field public static final NUM_LIKES:Ljava/lang/String; = "numLikes"

.field public static final OLD_POSITION:Ljava/lang/String; = "oldPosition"

.field public static final ORIGINAL_UPDATE:Ljava/lang/String; = "originalUpdate"

.field public static final PERSON:Ljava/lang/String; = "person"

.field public static final PERSON_ACTIVITIES:Ljava/lang/String; = "personActivities"

.field public static final PERSON_FIRST_NAME:Ljava/lang/String; = "firstName"

.field public static final PERSON_HEADLINE:Ljava/lang/String; = "headline"

.field public static final PERSON_ID:Ljava/lang/String; = "id"

.field public static final PERSON_LAST_NAME:Ljava/lang/String; = "lastName"

.field public static final PERSON_PICTURE_URL:Ljava/lang/String; = "pictureUrl"

.field public static final PERSON_PUBLIC_PROFILE_URL:Ljava/lang/String; = "siteStandardProfileRequest"

.field public static final PHONE_NUMBER:Ljava/lang/String; = "phoneNumber"

.field public static final PHONE_NUMBERS:Ljava/lang/String; = "phoneNumbers"

.field public static final PHONE_NUMBER_ITEM_TYPE:Ljava/lang/String; = "phoneType"

.field public static final POSITION:Ljava/lang/String; = "position"

.field public static final PRODUCT:Ljava/lang/String; = "product"

.field public static final RECOMMENDATIONS_GIVEN:Ljava/lang/String; = "recommendationsGiven"

.field public static final RECOMMENDATIONS_RECEIVED:Ljava/lang/String; = "recommendationsReceived"

.field public static final RECOMMENDATIONS_TYPE:Ljava/lang/String; = "recommendationType"

.field public static final RECOMMENDED_URL:Ljava/lang/String; = "webUrl"

.field public static final RECOMMENDEE:Ljava/lang/String; = "recommendee"

.field public static final RECOMMENDER:Ljava/lang/String; = "recommender"

.field public static final SEQUENCE_NUMBER:Ljava/lang/String; = "sequenceNumber"

.field public static final SERVICE_PROVIDER:Ljava/lang/String; = "serviceProvider"

.field public static final SHARE:Ljava/lang/String; = "share"

.field public static final SOURCE:Ljava/lang/String; = "source"

.field public static final START:Ljava/lang/String; = "_start"

.field public static final SUBMITTED_URL:Ljava/lang/String; = "submittedUrl"

.field public static final THUMBNAIL_URL:Ljava/lang/String; = "thumbnailUrl"

.field public static final TIMESTAMP:Ljava/lang/String; = "timestamp"

.field public static final TITLE:Ljava/lang/String; = "title"

.field public static final TOTAL:Ljava/lang/String; = "_total"

.field public static final TWITTER_ACCOUNTS:Ljava/lang/String; = "twitterAccounts"

.field public static final TWITTER_ACCOUNT_ID:Ljava/lang/String; = "providerAccountId"

.field public static final TWITTER_ACCOUNT_NAME:Ljava/lang/String; = "providerAccountName"

.field public static final UPDATED_FIELD:Ljava/lang/String; = "updatedField"

.field public static final UPDATED_FIELDS:Ljava/lang/String; = "updatedFields"

.field public static final UPDATE_ACTION:Ljava/lang/String; = "updateAction"

.field public static final UPDATE_COMMENTS:Ljava/lang/String; = "updateComments"

.field public static final UPDATE_CONTENT:Ljava/lang/String; = "updateContent"

.field public static final UPDATE_KEY:Ljava/lang/String; = "updateKey"

.field public static final UPDATE_TYPE:Ljava/lang/String; = "updateType"

.field public static final URL:Ljava/lang/String; = "url"

.field public static final VALUES:Ljava/lang/String; = "values"

.field public static final VISIBILITY:Ljava/lang/String; = "visibility"
