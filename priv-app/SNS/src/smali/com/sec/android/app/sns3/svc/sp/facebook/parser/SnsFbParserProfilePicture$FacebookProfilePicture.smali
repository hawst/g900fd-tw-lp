.class public interface abstract Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserProfilePicture$FacebookProfilePicture;
.super Ljava/lang/Object;
.source "SnsFbParserProfilePicture.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserProfilePicture;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "FacebookProfilePicture"
.end annotation


# static fields
.field public static final ID:Ljava/lang/String; = "id"

.field public static final IS_SILHOUETTE:Ljava/lang/String; = "is_silhouette"

.field public static final REAL_HEIGHT:Ljava/lang/String; = "real_height"

.field public static final REAL_WIDTH:Ljava/lang/String; = "real_width"

.field public static final URL:Ljava/lang/String; = "url"
