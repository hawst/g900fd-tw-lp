.class public Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseContact;
.super Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;
.source "SnsFsResponseContact.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseContact;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public mEmail:Ljava/lang/String;

.field public mFacebook:Ljava/lang/String;

.field public mPhone:Ljava/lang/String;

.field public mTwitter:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseContact$1;

    invoke-direct {v0}, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseContact$1;-><init>()V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseContact;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;-><init>()V

    .line 30
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;-><init>()V

    .line 33
    invoke-virtual {p0, p1}, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseContact;->readFromParcel(Landroid/os/Parcel;)V

    .line 34
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseContact$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseContact$1;

    .prologue
    .line 8
    invoke-direct {p0, p1}, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseContact;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 48
    const/4 v0, 0x0

    return v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 38
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseContact;->mPhone:Ljava/lang/String;

    .line 39
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseContact;->mEmail:Ljava/lang/String;

    .line 40
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseContact;->mTwitter:Ljava/lang/String;

    .line 41
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseContact;->mFacebook:Ljava/lang/String;

    .line 43
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "arg1"    # I

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseContact;->mPhone:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 54
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseContact;->mEmail:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 55
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseContact;->mTwitter:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 56
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseContact;->mFacebook:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 57
    return-void
.end method
