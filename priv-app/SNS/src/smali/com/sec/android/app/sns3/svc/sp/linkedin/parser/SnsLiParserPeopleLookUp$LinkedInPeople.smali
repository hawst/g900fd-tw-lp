.class public interface abstract Lcom/sec/android/app/sns3/svc/sp/linkedin/parser/SnsLiParserPeopleLookUp$LinkedInPeople;
.super Ljava/lang/Object;
.source "SnsLiParserPeopleLookUp.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/svc/sp/linkedin/parser/SnsLiParserPeopleLookUp;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "LinkedInPeople"
.end annotation


# static fields
.field public static final COMPANY:Ljava/lang/String; = "company"

.field public static final COMPANY_NAME:Ljava/lang/String; = "name"

.field public static final FIRST_NAME:Ljava/lang/String; = "firstName"

.field public static final FORMATTED_NAME:Ljava/lang/String; = "formattedName"

.field public static final HEADLINE:Ljava/lang/String; = "headline"

.field public static final IS_CURRENT:Ljava/lang/String; = "isCurrent"

.field public static final KEY:Ljava/lang/String; = "_key"

.field public static final LAST_NAME:Ljava/lang/String; = "lastName"

.field public static final MEMBER_ID:Ljava/lang/String; = "id"

.field public static final PICTURE_100x100:Ljava/lang/String; = "100x100"

.field public static final PICTURE_ORIGINAL:Ljava/lang/String; = "original"

.field public static final PICTURE_URLS:Ljava/lang/String; = "pictureUrls"

.field public static final POSITIONS:Ljava/lang/String; = "positions"

.field public static final POSITION_TITLE:Ljava/lang/String; = "title"

.field public static final PROFILE_URL:Ljava/lang/String; = "publicProfileUrl"

.field public static final TOTAL:Ljava/lang/String; = "_total"

.field public static final VALUES:Ljava/lang/String; = "values"
