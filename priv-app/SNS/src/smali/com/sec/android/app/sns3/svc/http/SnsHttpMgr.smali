.class public Lcom/sec/android/app/sns3/svc/http/SnsHttpMgr;
.super Ljava/lang/Object;
.source "SnsHttpMgr.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/http/SnsHttpMgr$SnsHttpClientUnit;
    }
.end annotation


# static fields
.field public static final KEEPALIVE_DURATION:J = 0x186a0L


# instance fields
.field private mHttpClientMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/android/app/sns3/svc/http/SnsHttpMgr$SnsHttpClientUnit;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/http/SnsHttpMgr;->mHttpClientMap:Ljava/util/Map;

    .line 40
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/sns3/svc/http/SnsHttpMgr;)Ljava/util/Map;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sns3/svc/http/SnsHttpMgr;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/http/SnsHttpMgr;->mHttpClientMap:Ljava/util/Map;

    return-object v0
.end method


# virtual methods
.method public getHttpClient(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/http/SnsHttpClient;
    .locals 6
    .param p1, "hostUri"    # Ljava/lang/String;

    .prologue
    .line 44
    new-instance v1, Ljava/util/Timer;

    invoke-direct {v1}, Ljava/util/Timer;-><init>()V

    .line 45
    .local v1, "timer":Ljava/util/Timer;
    new-instance v3, Lcom/sec/android/app/sns3/svc/http/SnsHttpMgr$1;

    invoke-direct {v3, p0, p1}, Lcom/sec/android/app/sns3/svc/http/SnsHttpMgr$1;-><init>(Lcom/sec/android/app/sns3/svc/http/SnsHttpMgr;Ljava/lang/String;)V

    const-wide/32 v4, 0x186a0

    invoke-virtual {v1, v3, v4, v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 61
    iget-object v3, p0, Lcom/sec/android/app/sns3/svc/http/SnsHttpMgr;->mHttpClientMap:Ljava/util/Map;

    invoke-interface {v3, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 62
    invoke-static {}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->isLoggable()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 63
    const-string v3, "SNS"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "SnsHttpMgr : getHttpClient() - contain client hostUri = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 65
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/sns3/svc/http/SnsHttpMgr;->mHttpClientMap:Ljava/util/Map;

    invoke-interface {v3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/sns3/svc/http/SnsHttpMgr$SnsHttpClientUnit;

    .line 66
    .local v2, "unit":Lcom/sec/android/app/sns3/svc/http/SnsHttpMgr$SnsHttpClientUnit;
    iget-object v3, v2, Lcom/sec/android/app/sns3/svc/http/SnsHttpMgr$SnsHttpClientUnit;->mKeepAliveTimer:Ljava/util/Timer;

    invoke-virtual {v3}, Ljava/util/Timer;->cancel()V

    .line 68
    iget-object v3, v2, Lcom/sec/android/app/sns3/svc/http/SnsHttpMgr$SnsHttpClientUnit;->mHttpClient:Lcom/sec/android/app/sns3/svc/http/SnsHttpClient;

    invoke-virtual {v3}, Lcom/sec/android/app/sns3/svc/http/SnsHttpClient;->setKeepAlive()V

    .line 69
    iget-object v3, p0, Lcom/sec/android/app/sns3/svc/http/SnsHttpMgr;->mHttpClientMap:Ljava/util/Map;

    new-instance v4, Lcom/sec/android/app/sns3/svc/http/SnsHttpMgr$SnsHttpClientUnit;

    iget-object v5, v2, Lcom/sec/android/app/sns3/svc/http/SnsHttpMgr$SnsHttpClientUnit;->mHttpClient:Lcom/sec/android/app/sns3/svc/http/SnsHttpClient;

    invoke-direct {v4, v5, v1}, Lcom/sec/android/app/sns3/svc/http/SnsHttpMgr$SnsHttpClientUnit;-><init>(Lcom/sec/android/app/sns3/svc/http/SnsHttpClient;Ljava/util/Timer;)V

    invoke-interface {v3, p1, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 70
    iget-object v0, v2, Lcom/sec/android/app/sns3/svc/http/SnsHttpMgr$SnsHttpClientUnit;->mHttpClient:Lcom/sec/android/app/sns3/svc/http/SnsHttpClient;

    .line 79
    .end local v2    # "unit":Lcom/sec/android/app/sns3/svc/http/SnsHttpMgr$SnsHttpClientUnit;
    :goto_0
    return-object v0

    .line 72
    :cond_1
    invoke-static {}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->isLoggable()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 73
    const-string v3, "SNS"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "SnsHttpMgr : getHttpClient() - create client hostUri = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 75
    :cond_2
    new-instance v0, Lcom/sec/android/app/sns3/svc/http/SnsHttpClient;

    invoke-direct {v0}, Lcom/sec/android/app/sns3/svc/http/SnsHttpClient;-><init>()V

    .line 77
    .local v0, "httpClient":Lcom/sec/android/app/sns3/svc/http/SnsHttpClient;
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/http/SnsHttpClient;->setKeepAlive()V

    .line 78
    iget-object v3, p0, Lcom/sec/android/app/sns3/svc/http/SnsHttpMgr;->mHttpClientMap:Ljava/util/Map;

    new-instance v4, Lcom/sec/android/app/sns3/svc/http/SnsHttpMgr$SnsHttpClientUnit;

    invoke-direct {v4, v0, v1}, Lcom/sec/android/app/sns3/svc/http/SnsHttpMgr$SnsHttpClientUnit;-><init>(Lcom/sec/android/app/sns3/svc/http/SnsHttpClient;Ljava/util/Timer;)V

    invoke-interface {v3, p1, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method
