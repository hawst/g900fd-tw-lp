.class public interface abstract Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$UpdateColumns;
.super Ljava/lang/Object;
.source "SnsLinkedInDB.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "UpdateColumns"
.end annotation


# static fields
.field public static final ACTION_CODE:Ljava/lang/String; = "action_code"

.field public static final CURRENT_STATUS:Ljava/lang/String; = "current_status"

.field public static final DESCRIPTION:Ljava/lang/String; = "description"

.field public static final IS_COMMENTABLE:Ljava/lang/String; = "is_commentable"

.field public static final IS_LIKABLE:Ljava/lang/String; = "is_likable"

.field public static final IS_LIKED:Ljava/lang/String; = "is_liked"

.field public static final NUMBER_OF_LIKES:Ljava/lang/String; = "number_of_likes"

.field public static final PERSON_ID:Ljava/lang/String; = "person_id"

.field public static final SHARE_ID:Ljava/lang/String; = "share_id"

.field public static final TIME_STAMP:Ljava/lang/String; = "timestamp"

.field public static final UPDATED_FIELDS:Ljava/lang/String; = "updated_fields"

.field public static final UPDATE_KEY:Ljava/lang/String; = "update_key"

.field public static final UPDATE_TYPE:Ljava/lang/String; = "update_type"
