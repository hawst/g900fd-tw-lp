.class public Lcom/sec/android/app/sns3/svc/http/SnsHttpThread;
.super Ljava/lang/Thread;
.source "SnsHttpThread.java"


# instance fields
.field mCallback:Lcom/sec/android/app/sns3/svc/http/ISnsHttpCallback;

.field mHttpClient:Lcom/sec/android/app/sns3/svc/http/SnsHttpClient;

.field mHttpRequest:Lcom/sec/android/app/sns3/svc/http/SnsHttpRequest;

.field mResponse:Lorg/apache/http/HttpResponse;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/sns3/svc/http/SnsHttpClient;Lcom/sec/android/app/sns3/svc/http/SnsHttpRequest;Lcom/sec/android/app/sns3/svc/http/ISnsHttpCallback;)V
    .locals 2
    .param p1, "httpClient"    # Lcom/sec/android/app/sns3/svc/http/SnsHttpClient;
    .param p2, "httpRequest"    # Lcom/sec/android/app/sns3/svc/http/SnsHttpRequest;
    .param p3, "callback"    # Lcom/sec/android/app/sns3/svc/http/ISnsHttpCallback;

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 51
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    if-nez p3, :cond_1

    .line 52
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "parameters MUST not be null."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 54
    :cond_1
    iput-object p2, p0, Lcom/sec/android/app/sns3/svc/http/SnsHttpThread;->mHttpRequest:Lcom/sec/android/app/sns3/svc/http/SnsHttpRequest;

    .line 55
    iput-object p3, p0, Lcom/sec/android/app/sns3/svc/http/SnsHttpThread;->mCallback:Lcom/sec/android/app/sns3/svc/http/ISnsHttpCallback;

    .line 56
    iput-object p1, p0, Lcom/sec/android/app/sns3/svc/http/SnsHttpThread;->mHttpClient:Lcom/sec/android/app/sns3/svc/http/SnsHttpClient;

    .line 57
    return-void
.end method


# virtual methods
.method public abort()V
    .locals 1

    .prologue
    .line 189
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/http/SnsHttpThread;->mHttpRequest:Lcom/sec/android/app/sns3/svc/http/SnsHttpRequest;

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/http/SnsHttpRequest;->abort()V

    .line 190
    return-void
.end method

.method public run()V
    .locals 10

    .prologue
    .line 60
    const/4 v2, -0x1

    .line 61
    .local v2, "httpStatusCode":I
    const/4 v1, -0x1

    .line 63
    .local v1, "errorCode":I
    const/4 v5, 0x0

    .line 64
    .local v5, "responseContent":Ljava/lang/String;
    const/4 v6, 0x0

    .line 67
    .local v6, "wl":Landroid/os/PowerManager$WakeLock;
    const-string v7, "SNS"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "SnsHttpThread : run() : START~ reqID = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/app/sns3/svc/http/SnsHttpThread;->mHttpRequest:Lcom/sec/android/app/sns3/svc/http/SnsHttpRequest;

    iget v9, v9, Lcom/sec/android/app/sns3/svc/http/SnsHttpRequest;->mFwkReqID:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 75
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/sns3/SnsApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    const-string v8, "power"

    invoke-virtual {v7, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/os/PowerManager;

    .line 78
    .local v3, "pm":Landroid/os/PowerManager;
    const/4 v7, 0x1

    const-string v8, "SNS"

    invoke-virtual {v3, v7, v8}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v6

    .line 80
    invoke-virtual {v6}, Landroid/os/PowerManager$WakeLock;->acquire()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 83
    :try_start_1
    iget-object v7, p0, Lcom/sec/android/app/sns3/svc/http/SnsHttpThread;->mHttpClient:Lcom/sec/android/app/sns3/svc/http/SnsHttpClient;

    iget-object v8, p0, Lcom/sec/android/app/sns3/svc/http/SnsHttpThread;->mHttpRequest:Lcom/sec/android/app/sns3/svc/http/SnsHttpRequest;

    invoke-virtual {v7, v8}, Lcom/sec/android/app/sns3/svc/http/SnsHttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/android/app/sns3/svc/http/SnsHttpThread;->mResponse:Lorg/apache/http/HttpResponse;

    .line 84
    const-string v7, "SNS"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "SnsHttpThread : run() : httpClient.execute(req) - CALLED~ reqID = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/app/sns3/svc/http/SnsHttpThread;->mHttpRequest:Lcom/sec/android/app/sns3/svc/http/SnsHttpRequest;

    iget v9, v9, Lcom/sec/android/app/sns3/svc/http/SnsHttpRequest;->mFwkReqID:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/InterruptedIOException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/net/SocketException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/net/UnknownHostException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 150
    :goto_0
    :try_start_2
    iget-object v7, p0, Lcom/sec/android/app/sns3/svc/http/SnsHttpThread;->mHttpRequest:Lcom/sec/android/app/sns3/svc/http/SnsHttpRequest;

    invoke-virtual {v7}, Lcom/sec/android/app/sns3/svc/http/SnsHttpRequest;->isAborted()Z

    move-result v7

    if-nez v7, :cond_3

    .line 152
    iget-object v7, p0, Lcom/sec/android/app/sns3/svc/http/SnsHttpThread;->mResponse:Lorg/apache/http/HttpResponse;

    if-nez v7, :cond_2

    .line 153
    const-string v7, "SNS"

    const-string v8, "SnsHttpThread : run() - mResponse is NULL !!!!!!! "

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 155
    new-instance v7, Ljava/lang/Exception;

    const-string v8, "Response is Null."

    invoke-direct {v7, v8}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v7
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 172
    .end local v3    # "pm":Landroid/os/PowerManager;
    :catch_0
    move-exception v0

    .line 173
    .local v0, "e":Ljava/lang/Exception;
    :try_start_3
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 174
    const-string v7, "SNS"

    const-string v8, "SnsHttpThread : run() : EXCEPTION OCCUR !!!"

    invoke-static {v7, v8, v0}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 177
    const/16 v1, 0xbb9

    .line 180
    iget-object v7, p0, Lcom/sec/android/app/sns3/svc/http/SnsHttpThread;->mCallback:Lcom/sec/android/app/sns3/svc/http/ISnsHttpCallback;

    invoke-virtual {v7, v2, v1, v5}, Lcom/sec/android/app/sns3/svc/http/ISnsHttpCallback;->onResponse(IILjava/lang/String;)V

    .line 182
    if-eqz v6, :cond_0

    .line 183
    invoke-virtual {v6}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 186
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    :goto_1
    return-void

    .line 87
    .restart local v3    # "pm":Landroid/os/PowerManager;
    :catch_1
    move-exception v0

    .line 88
    .local v0, "e":Lorg/apache/http/client/ClientProtocolException;
    :try_start_4
    invoke-virtual {v0}, Lorg/apache/http/client/ClientProtocolException;->printStackTrace()V

    .line 89
    const-string v7, "SNS"

    const-string v8, "SnsHttpThread : run() : ClientProtocolException"

    invoke-static {v7, v8, v0}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 92
    const-string v7, "SNS"

    const-string v8, "SnsHttpThread : run() : Error in the HTTP protocol!!"

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 94
    iget-object v7, p0, Lcom/sec/android/app/sns3/svc/http/SnsHttpThread;->mHttpRequest:Lcom/sec/android/app/sns3/svc/http/SnsHttpRequest;

    invoke-virtual {v7}, Lcom/sec/android/app/sns3/svc/http/SnsHttpRequest;->abort()V

    .line 96
    const/16 v1, 0xbb9

    .line 148
    goto :goto_0

    .line 98
    .end local v0    # "e":Lorg/apache/http/client/ClientProtocolException;
    :catch_2
    move-exception v0

    .line 99
    .local v0, "e":Ljava/io/InterruptedIOException;
    const-string v7, "SNS"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "SnsHttpThread : run() : InterruptedIOException : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v0}, Ljava/io/InterruptedIOException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 102
    const-string v7, "SNS"

    const-string v8, "SnsHttpThread : run() : The process reading/writing to a stream has been interrupted OR Socket timeout occurs before the request has completed!!"

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 104
    iget-object v7, p0, Lcom/sec/android/app/sns3/svc/http/SnsHttpThread;->mHttpRequest:Lcom/sec/android/app/sns3/svc/http/SnsHttpRequest;

    invoke-virtual {v7}, Lcom/sec/android/app/sns3/svc/http/SnsHttpRequest;->abort()V

    .line 106
    const/16 v1, 0xbbc

    .line 148
    goto :goto_0

    .line 108
    .end local v0    # "e":Ljava/io/InterruptedIOException;
    :catch_3
    move-exception v0

    .line 109
    .local v0, "e":Ljava/net/SocketException;
    const-string v7, "SNS"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "SnsHttpThread : run() : SocketException : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v0}, Ljava/net/SocketException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 111
    const-string v7, "SNS"

    const-string v8, "SnsHttpThread : run() : Error during socket creation or setting options!!"

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 114
    iget-object v7, p0, Lcom/sec/android/app/sns3/svc/http/SnsHttpThread;->mHttpRequest:Lcom/sec/android/app/sns3/svc/http/SnsHttpRequest;

    invoke-virtual {v7}, Lcom/sec/android/app/sns3/svc/http/SnsHttpRequest;->abort()V

    .line 116
    const-string v7, "Network unreachable"

    invoke-virtual {v0}, Ljava/net/SocketException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 117
    const/16 v1, 0xbbd

    goto/16 :goto_0

    .line 119
    :cond_1
    const/16 v1, 0xbb9

    goto/16 :goto_0

    .line 120
    .end local v0    # "e":Ljava/net/SocketException;
    :catch_4
    move-exception v0

    .line 121
    .local v0, "e":Ljava/net/UnknownHostException;
    invoke-virtual {v0}, Ljava/net/UnknownHostException;->printStackTrace()V

    .line 122
    const-string v7, "SNS"

    const-string v8, "SnsHttpThread : run() : UnknownHostException"

    invoke-static {v7, v8, v0}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 126
    iget-object v7, p0, Lcom/sec/android/app/sns3/svc/http/SnsHttpThread;->mHttpRequest:Lcom/sec/android/app/sns3/svc/http/SnsHttpRequest;

    invoke-virtual {v7}, Lcom/sec/android/app/sns3/svc/http/SnsHttpRequest;->abort()V

    .line 128
    const/16 v1, 0xbbe

    .line 148
    goto/16 :goto_0

    .line 129
    .end local v0    # "e":Ljava/net/UnknownHostException;
    :catch_5
    move-exception v0

    .line 130
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    .line 131
    const-string v7, "SNS"

    const-string v8, "SnsHttpThread : run() : IllegalArgumentException"

    invoke-static {v7, v8, v0}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 134
    const-string v7, "SNS"

    const-string v8, "SnsHttpThread : run() : The method is invoked with an argument which it can not reasonably deal with!!"

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 137
    iget-object v7, p0, Lcom/sec/android/app/sns3/svc/http/SnsHttpThread;->mHttpRequest:Lcom/sec/android/app/sns3/svc/http/SnsHttpRequest;

    invoke-virtual {v7}, Lcom/sec/android/app/sns3/svc/http/SnsHttpRequest;->abort()V

    .line 139
    const/16 v1, 0xbb9

    .line 148
    goto/16 :goto_0

    .line 140
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_6
    move-exception v0

    .line 141
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 142
    const-string v7, "SNS"

    const-string v8, "SnsHttpThread : run() : Exception"

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 145
    iget-object v7, p0, Lcom/sec/android/app/sns3/svc/http/SnsHttpThread;->mHttpRequest:Lcom/sec/android/app/sns3/svc/http/SnsHttpRequest;

    invoke-virtual {v7}, Lcom/sec/android/app/sns3/svc/http/SnsHttpRequest;->abort()V

    .line 147
    const/16 v1, 0xbb9

    goto/16 :goto_0

    .line 158
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_2
    iget-object v7, p0, Lcom/sec/android/app/sns3/svc/http/SnsHttpThread;->mResponse:Lorg/apache/http/HttpResponse;

    invoke-interface {v7}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v4

    .line 159
    .local v4, "response":Lorg/apache/http/HttpEntity;
    iget-object v7, p0, Lcom/sec/android/app/sns3/svc/http/SnsHttpThread;->mResponse:Lorg/apache/http/HttpResponse;

    invoke-interface {v7}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v7

    invoke-interface {v7}, Lorg/apache/http/StatusLine;->getStatusCode()I
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result v2

    .line 163
    :try_start_5
    invoke-static {v4}, Lorg/apache/http/util/EntityUtils;->toString(Lorg/apache/http/HttpEntity;)Ljava/lang/String;
    :try_end_5
    .catch Ljava/lang/IllegalStateException; {:try_start_5 .. :try_end_5} :catch_7
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_8
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-result-object v5

    .line 180
    .end local v4    # "response":Lorg/apache/http/HttpEntity;
    :cond_3
    :goto_2
    iget-object v7, p0, Lcom/sec/android/app/sns3/svc/http/SnsHttpThread;->mCallback:Lcom/sec/android/app/sns3/svc/http/ISnsHttpCallback;

    invoke-virtual {v7, v2, v1, v5}, Lcom/sec/android/app/sns3/svc/http/ISnsHttpCallback;->onResponse(IILjava/lang/String;)V

    .line 182
    if-eqz v6, :cond_0

    .line 183
    invoke-virtual {v6}, Landroid/os/PowerManager$WakeLock;->release()V

    goto/16 :goto_1

    .line 164
    .restart local v4    # "response":Lorg/apache/http/HttpEntity;
    :catch_7
    move-exception v0

    .line 165
    .local v0, "e":Ljava/lang/IllegalStateException;
    :try_start_6
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    .line 166
    const/16 v1, 0xbba

    .line 170
    goto :goto_2

    .line 167
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :catch_8
    move-exception v0

    .line 168
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 169
    const/16 v1, 0xbbb

    goto :goto_2

    .line 180
    .end local v0    # "e":Ljava/io/IOException;
    .end local v3    # "pm":Landroid/os/PowerManager;
    .end local v4    # "response":Lorg/apache/http/HttpEntity;
    :catchall_0
    move-exception v7

    iget-object v8, p0, Lcom/sec/android/app/sns3/svc/http/SnsHttpThread;->mCallback:Lcom/sec/android/app/sns3/svc/http/ISnsHttpCallback;

    invoke-virtual {v8, v2, v1, v5}, Lcom/sec/android/app/sns3/svc/http/ISnsHttpCallback;->onResponse(IILjava/lang/String;)V

    .line 182
    if-eqz v6, :cond_4

    .line 183
    invoke-virtual {v6}, Landroid/os/PowerManager$WakeLock;->release()V

    :cond_4
    throw v7
.end method
