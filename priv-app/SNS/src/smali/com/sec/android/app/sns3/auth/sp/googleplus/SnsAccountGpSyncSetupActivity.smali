.class public Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpSyncSetupActivity;
.super Landroid/preference/PreferenceActivity;
.source "SnsAccountGpSyncSetupActivity.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "SnsAccountGpAuth"


# instance fields
.field private mAppContext:Landroid/content/Context;

.field private mCallCheckBox:Landroid/preference/CheckBoxPreference;

.field private mDescription:Landroid/widget/TextView;

.field private mFeedsCheckBox:Landroid/preference/CheckBoxPreference;

.field private mSyncIntervalSetting:Landroid/preference/PreferenceScreen;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 46
    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    .line 48
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpSyncSetupActivity;->mAppContext:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method isTable()Z
    .locals 2

    .prologue
    .line 214
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpSyncSetupActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit8 v0, v0, 0x3

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpSyncSetupActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    .line 217
    :cond_0
    const/4 v0, 0x1

    .line 219
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onClickDone()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 167
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpSyncSetupActivity;->mAppContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    const-string v2, "com.sec.android.app.sns3.googleplus"

    invoke-virtual {v1, v2}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 170
    .local v0, "account":[Landroid/accounts/Account;
    array-length v1, v0

    if-lez v1, :cond_1

    .line 172
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpSyncSetupActivity;->mFeedsCheckBox:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 173
    aget-object v1, v0, v3

    const-string v2, "com.sec.android.app.sns3.life"

    invoke-static {v1, v2, v4}, Landroid/content/ContentResolver;->setIsSyncable(Landroid/accounts/Account;Ljava/lang/String;I)V

    .line 174
    aget-object v1, v0, v3

    const-string v2, "com.sec.android.app.sns3.life"

    invoke-static {v1, v2, v4}, Landroid/content/ContentResolver;->setSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;Z)V

    .line 177
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpSyncSetupActivity;->mCallCheckBox:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 178
    aget-object v1, v0, v3

    const-string v2, "com.sec.android.app.sns3.call"

    invoke-static {v1, v2, v4}, Landroid/content/ContentResolver;->setIsSyncable(Landroid/accounts/Account;Ljava/lang/String;I)V

    .line 179
    aget-object v1, v0, v3

    const-string v2, "com.sec.android.app.sns3.call"

    invoke-static {v1, v2, v4}, Landroid/content/ContentResolver;->setSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;Z)V

    .line 183
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpSyncSetupActivity;->finish()V

    .line 184
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 11
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const v10, 0x7f080058

    const v7, 0x7f080023

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 63
    iput-object p0, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpSyncSetupActivity;->mAppContext:Landroid/content/Context;

    .line 65
    iget-object v4, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpSyncSetupActivity;->mAppContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->isLightTheme(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_a

    const v4, 0x103012b

    :goto_0
    invoke-virtual {p0, v4}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpSyncSetupActivity;->setTheme(I)V

    .line 68
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 70
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpSyncSetupActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 71
    .local v0, "actionBar":Landroid/app/ActionBar;
    invoke-virtual {v0, v8}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 73
    const v4, 0x7f030004

    invoke-virtual {p0, v4}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpSyncSetupActivity;->setContentView(I)V

    .line 74
    const v4, 0x7f040006

    invoke-virtual {p0, v4}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpSyncSetupActivity;->addPreferencesFromResource(I)V

    .line 76
    const v4, 0x7f080046

    new-array v5, v9, [Ljava/lang/Object;

    invoke-virtual {p0, v7}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpSyncSetupActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-virtual {p0, v4, v5}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpSyncSetupActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpSyncSetupActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 78
    const v4, 0x7f0b0004

    invoke-virtual {p0, v4}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpSyncSetupActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpSyncSetupActivity;->mDescription:Landroid/widget/TextView;

    .line 79
    iget-object v4, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpSyncSetupActivity;->mDescription:Landroid/widget/TextView;

    if-eqz v4, :cond_0

    .line 80
    iget-object v4, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpSyncSetupActivity;->mDescription:Landroid/widget/TextView;

    const v5, 0x7f08006f

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 83
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpSyncSetupActivity;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpSyncSetupActivity;->mAppContext:Landroid/content/Context;

    invoke-virtual {v4, v5}, Landroid/preference/PreferenceManager;->createPreferenceScreen(Landroid/content/Context;)Landroid/preference/PreferenceScreen;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpSyncSetupActivity;->mSyncIntervalSetting:Landroid/preference/PreferenceScreen;

    .line 84
    iget-object v4, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpSyncSetupActivity;->mSyncIntervalSetting:Landroid/preference/PreferenceScreen;

    const v5, 0x7f080056

    invoke-virtual {v4, v5}, Landroid/preference/PreferenceScreen;->setTitle(I)V

    .line 85
    iget-object v4, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpSyncSetupActivity;->mSyncIntervalSetting:Landroid/preference/PreferenceScreen;

    new-instance v5, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpSyncSetupActivity$1;

    invoke-direct {v5, p0}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpSyncSetupActivity$1;-><init>(Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpSyncSetupActivity;)V

    invoke-virtual {v4, v5}, Landroid/preference/PreferenceScreen;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 95
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpSyncSetupActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpSyncSetupActivity;->mSyncIntervalSetting:Landroid/preference/PreferenceScreen;

    invoke-virtual {v4, v5}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 97
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/sns3/SnsApplication;->getFeatureMgr()Lcom/sec/android/app/sns3/SnsFeatureManager;

    move-result-object v1

    .line 98
    .local v1, "featureMgr":Lcom/sec/android/app/sns3/SnsFeatureManager;
    const/4 v3, 0x0

    .line 99
    .local v3, "providerLabel":Ljava/lang/CharSequence;
    const/4 v2, 0x0

    .line 102
    .local v2, "providerInfo":Landroid/content/pm/ProviderInfo;
    const/4 v3, 0x0

    .line 103
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpSyncSetupActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    const-string v5, "com.sec.android.app.sns3.life"

    invoke-virtual {v4, v5, v8}, Landroid/content/pm/PackageManager;->resolveContentProvider(Ljava/lang/String;I)Landroid/content/pm/ProviderInfo;

    move-result-object v2

    .line 104
    if-eqz v2, :cond_1

    .line 105
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpSyncSetupActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/content/pm/ProviderInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v3

    .line 107
    :cond_1
    if-eqz v3, :cond_2

    invoke-interface {v3}, Ljava/lang/CharSequence;->length()I

    move-result v4

    if-nez v4, :cond_3

    .line 108
    :cond_2
    const-string v4, "SnsAccountGpAuth"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "providerLabel is null - "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 109
    const-string v3, "Life Times"

    .line 111
    :cond_3
    new-instance v4, Landroid/preference/CheckBoxPreference;

    iget-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpSyncSetupActivity;->mAppContext:Landroid/content/Context;

    invoke-direct {v4, v5}, Landroid/preference/CheckBoxPreference;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpSyncSetupActivity;->mFeedsCheckBox:Landroid/preference/CheckBoxPreference;

    .line 112
    iget-object v4, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpSyncSetupActivity;->mFeedsCheckBox:Landroid/preference/CheckBoxPreference;

    new-array v5, v9, [Ljava/lang/Object;

    aput-object v3, v5, v8

    invoke-virtual {p0, v10, v5}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpSyncSetupActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/preference/CheckBoxPreference;->setTitle(Ljava/lang/CharSequence;)V

    .line 113
    iget-object v4, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpSyncSetupActivity;->mFeedsCheckBox:Landroid/preference/CheckBoxPreference;

    const v5, 0x7f08005b

    new-array v6, v9, [Ljava/lang/Object;

    invoke-virtual {p0, v7}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpSyncSetupActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-virtual {p0, v5, v6}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpSyncSetupActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/preference/CheckBoxPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 115
    iget-object v4, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpSyncSetupActivity;->mFeedsCheckBox:Landroid/preference/CheckBoxPreference;

    const-string v5, "snsaccount_sync_feeds"

    invoke-virtual {v4, v5}, Landroid/preference/CheckBoxPreference;->setKey(Ljava/lang/String;)V

    .line 116
    invoke-virtual {v1}, Lcom/sec/android/app/sns3/SnsFeatureManager;->isSyncAdapterForProfileFeedsNeeded()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 117
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpSyncSetupActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpSyncSetupActivity;->mFeedsCheckBox:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v4, v5}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 118
    iget-object v4, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpSyncSetupActivity;->mFeedsCheckBox:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v4, v9}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 122
    :cond_4
    const/4 v3, 0x0

    .line 123
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpSyncSetupActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    const-string v5, "com.sec.android.app.sns3.call"

    invoke-virtual {v4, v5, v8}, Landroid/content/pm/PackageManager;->resolveContentProvider(Ljava/lang/String;I)Landroid/content/pm/ProviderInfo;

    move-result-object v2

    .line 125
    if-eqz v2, :cond_5

    .line 126
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpSyncSetupActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/content/pm/ProviderInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v3

    .line 128
    :cond_5
    if-eqz v3, :cond_6

    invoke-interface {v3}, Ljava/lang/CharSequence;->length()I

    move-result v4

    if-nez v4, :cond_7

    .line 129
    :cond_6
    const-string v4, "SnsAccountGpAuth"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "providerLabel is null - "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 130
    const-string v3, "Call"

    .line 132
    :cond_7
    new-instance v4, Landroid/preference/CheckBoxPreference;

    iget-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpSyncSetupActivity;->mAppContext:Landroid/content/Context;

    invoke-direct {v4, v5}, Landroid/preference/CheckBoxPreference;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpSyncSetupActivity;->mCallCheckBox:Landroid/preference/CheckBoxPreference;

    .line 133
    iget-object v4, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpSyncSetupActivity;->mCallCheckBox:Landroid/preference/CheckBoxPreference;

    new-array v5, v9, [Ljava/lang/Object;

    aput-object v3, v5, v8

    invoke-virtual {p0, v10, v5}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpSyncSetupActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/preference/CheckBoxPreference;->setTitle(Ljava/lang/CharSequence;)V

    .line 135
    iget-object v4, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpSyncSetupActivity;->mCallCheckBox:Landroid/preference/CheckBoxPreference;

    const-string v5, "snsaccount_sync_call"

    invoke-virtual {v4, v5}, Landroid/preference/CheckBoxPreference;->setKey(Ljava/lang/String;)V

    .line 136
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpSyncSetupActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpSyncSetupActivity;->mCallCheckBox:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v4, v5}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 137
    iget-object v4, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpSyncSetupActivity;->mCallCheckBox:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v4, v9}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 139
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpSyncSetupActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v4

    invoke-virtual {v4}, Landroid/preference/PreferenceScreen;->getPreferenceCount()I

    move-result v4

    if-gt v4, v9, :cond_8

    .line 140
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpSyncSetupActivity;->finish()V

    .line 143
    :cond_8
    invoke-virtual {v1}, Lcom/sec/android/app/sns3/SnsFeatureManager;->isSyncAdapterForProfileFeedsNeeded()Z

    move-result v4

    if-nez v4, :cond_9

    .line 144
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpSyncSetupActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpSyncSetupActivity;->mSyncIntervalSetting:Landroid/preference/PreferenceScreen;

    invoke-virtual {v4, v5}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 146
    :cond_9
    return-void

    .line 65
    .end local v0    # "actionBar":Landroid/app/ActionBar;
    .end local v1    # "featureMgr":Lcom/sec/android/app/sns3/SnsFeatureManager;
    .end local v2    # "providerInfo":Landroid/content/pm/ProviderInfo;
    .end local v3    # "providerLabel":Ljava/lang/CharSequence;
    :cond_a
    const v4, 0x1030128

    goto/16 :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 188
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    .line 190
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpSyncSetupActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->isActionbarLightTheme(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 191
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpSyncSetupActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0a0001

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 195
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 193
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpSyncSetupActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const/high16 v1, 0x7f0a0000

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 224
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onDestroy()V

    .line 225
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 200
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 208
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    .line 210
    :goto_0
    return v0

    .line 202
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpSyncSetupActivity;->onClickDone()V

    .line 210
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 205
    :pswitch_1
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpSyncSetupActivity;->finish()V

    goto :goto_1

    .line 200
    nop

    :pswitch_data_0
    .packed-switch 0x7f0b0005
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 150
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 152
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpSyncSetupActivity;->mFeedsCheckBox:Landroid/preference/CheckBoxPreference;

    const-string v1, "feeds"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 153
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpSyncSetupActivity;->mCallCheckBox:Landroid/preference/CheckBoxPreference;

    const-string v1, "call"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 155
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 159
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 161
    const-string v0, "feeds"

    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpSyncSetupActivity;->mFeedsCheckBox:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 162
    const-string v0, "call"

    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpSyncSetupActivity;->mCallCheckBox:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 164
    return-void
.end method
