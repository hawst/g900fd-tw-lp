.class public Lcom/sec/android/app/sns3/auth/sp/sinaweibo/SnsAccountSwAuthSSOReceiver;
.super Landroid/content/BroadcastReceiver;
.source "SnsAccountSwAuthSSOReceiver.java"


# static fields
.field public static final SNS_LOGIN_SINAWEIBO_NOTIFICATION:Ljava/lang/String; = "com.sec.android.app.sns3.auth.sp.sinaweibo.SNS_LOGIN_SINAWEIBO_NOTIFICATION"

.field public static final SSO_APPLICATION_REMOVE:Ljava/lang/String; = "android.intent.action.PACKAGE_REMOVED"

.field public static final SSO_SINA_APPLICATION_LOGIN:Ljava/lang/String; = "sina.weibo.action.SWITCHUSERDONE"

.field public static final SSO_SINA_APPLICATION_LOGOUT:Ljava/lang/String; = "sina.weibo.action.NOUSER"

.field public static final SSO_SINA_APPLICATION_REMOVE_DATA:Ljava/lang/String; = "package:com.sina.weibo"

.field private static final TAG:Ljava/lang/String; = "SwSSORECEIVER"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method private registerLoginNotification(Landroid/content/Context;)V
    .locals 11
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 81
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 82
    .local v0, "i":Landroid/content/Intent;
    const-class v6, Lcom/sec/android/app/sns3/auth/sp/sinaweibo/SnsAccountSwAuthSSOActivity;

    invoke-virtual {v0, p1, v6}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 83
    const/high16 v6, 0x10000000

    invoke-virtual {v0, v6}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 84
    const/high16 v6, 0x800000

    invoke-virtual {v0, v6}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 85
    const/16 v6, 0x2ee0

    invoke-static {p1, v6, v0, v9}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 87
    .local v1, "intent":Landroid/app/PendingIntent;
    const v6, 0x7f080030

    new-array v7, v10, [Ljava/lang/Object;

    const v8, 0x7f080047

    invoke-virtual {p1, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v9

    invoke-virtual {p1, v6, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 88
    .local v4, "ticker":Ljava/lang/String;
    const v6, 0x7f08000a

    invoke-virtual {p1, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 90
    .local v5, "title":Ljava/lang/String;
    new-instance v2, Landroid/app/Notification$Builder;

    invoke-direct {v2, p1}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    .line 91
    .local v2, "noti":Landroid/app/Notification$Builder;
    invoke-virtual {v2, v5}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    .line 92
    invoke-virtual {v2, v4}, Landroid/app/Notification$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    .line 93
    invoke-virtual {v2, v4}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    .line 94
    invoke-virtual {v2, v1}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    .line 95
    invoke-virtual {v2, v9}, Landroid/app/Notification$Builder;->setAutoCancel(Z)Landroid/app/Notification$Builder;

    .line 96
    new-instance v6, Landroid/app/Notification$BigTextStyle;

    invoke-direct {v6}, Landroid/app/Notification$BigTextStyle;-><init>()V

    invoke-virtual {v6, v4}, Landroid/app/Notification$BigTextStyle;->bigText(Ljava/lang/CharSequence;)Landroid/app/Notification$BigTextStyle;

    move-result-object v6

    invoke-virtual {v2, v6}, Landroid/app/Notification$Builder;->setStyle(Landroid/app/Notification$Style;)Landroid/app/Notification$Builder;

    .line 97
    invoke-virtual {v2, v10}, Landroid/app/Notification$Builder;->setDefaults(I)Landroid/app/Notification$Builder;

    .line 98
    const v6, 0x7f020020

    invoke-virtual {v2, v6}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    .line 100
    const-string v6, "notification"

    invoke-virtual {p1, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/NotificationManager;

    .line 102
    .local v3, "notiMgr":Landroid/app/NotificationManager;
    const/16 v6, 0x2f44

    invoke-virtual {v2}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v7

    invoke-virtual {v3, v6, v7}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 103
    return-void
.end method

.method private removeSinaweiboSSOAccount(Landroid/content/Context;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v6, 0x0

    .line 62
    const-string v4, "SwSSORECEIVER"

    const-string v5, "Sinaweibo SSO Receiver : removeSinaweiboSSOAccount"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 64
    const-string v4, "notification"

    invoke-virtual {p1, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/NotificationManager;

    .line 65
    .local v1, "nm":Landroid/app/NotificationManager;
    const/16 v4, 0x2f44

    invoke-virtual {v1, v4}, Landroid/app/NotificationManager;->cancel(I)V

    .line 67
    invoke-static {p1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v4

    const-string v5, "com.sec.android.app.sns3.sinaweibo"

    invoke-virtual {v4, v5}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 68
    .local v0, "accounts":[Landroid/accounts/Account;
    if-eqz v0, :cond_0

    array-length v4, v0

    if-lez v4, :cond_0

    .line 69
    invoke-static {p1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v4

    const/4 v5, 0x0

    aget-object v5, v0, v5

    invoke-virtual {v4, v5, v6, v6}, Landroid/accounts/AccountManager;->removeAccount(Landroid/accounts/Account;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    .line 72
    :cond_0
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/sns3/SnsApplication;->getSvcMgr()Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v2

    .line 73
    .local v2, "svcMgr":Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    invoke-virtual {v2}, Lcom/sec/android/app/sns3/svc/SnsSvcMgr;->getRequestMgr()Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;

    move-result-object v4

    const-string v5, "sinaweibo"

    invoke-virtual {v4, v5}, Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;->clearReservedRequestsBySp(Ljava/lang/String;)Z

    .line 75
    invoke-virtual {v2}, Lcom/sec/android/app/sns3/svc/SnsSvcMgr;->getTokenMgr()Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;

    move-result-object v3

    .line 76
    .local v3, "tokenMgr":Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;
    const-string v4, "sinaweibo"

    invoke-virtual {v3, v4}, Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;->getToken(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/sns3/svc/sp/sinaweibo/SnsSwToken;

    invoke-virtual {v4}, Lcom/sec/android/app/sns3/svc/sp/sinaweibo/SnsSwToken;->removeAll()V

    .line 78
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v5, 0x0

    .line 37
    const-string v3, "SwSSORECEIVER"

    const-string v4, "Sinaweibo SSO Receiver "

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 38
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    .line 39
    .local v1, "action":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object v2

    .line 41
    .local v2, "data":Ljava/lang/String;
    const-string v3, "com.sec.android.app.sns3.auth.sp.sinaweibo.SNS_LOGIN_SINAWEIBO_NOTIFICATION"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 42
    invoke-static {p1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v3

    const-string v4, "com.sec.android.app.sns3.sinaweibo"

    invoke-virtual {v3, v4}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 43
    .local v0, "accounts":[Landroid/accounts/Account;
    if-eqz v0, :cond_0

    array-length v3, v0

    if-gtz v3, :cond_1

    .line 44
    :cond_0
    invoke-direct {p0, p1}, Lcom/sec/android/app/sns3/auth/sp/sinaweibo/SnsAccountSwAuthSSOReceiver;->registerLoginNotification(Landroid/content/Context;)V

    .line 59
    .end local v0    # "accounts":[Landroid/accounts/Account;
    :cond_1
    :goto_0
    return-void

    .line 46
    :cond_2
    const-string v3, "sina.weibo.action.SWITCHUSERDONE"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 47
    invoke-static {p1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v3

    const-string v4, "com.sec.android.app.sns3.sinaweibo"

    invoke-virtual {v3, v4}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 49
    .restart local v0    # "accounts":[Landroid/accounts/Account;
    if-eqz v0, :cond_3

    array-length v3, v0

    if-lez v3, :cond_3

    .line 50
    invoke-static {p1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v3

    const/4 v4, 0x0

    aget-object v4, v0, v4

    invoke-virtual {v3, v4, v5, v5}, Landroid/accounts/AccountManager;->removeAccount(Landroid/accounts/Account;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    .line 52
    :cond_3
    invoke-direct {p0, p1}, Lcom/sec/android/app/sns3/auth/sp/sinaweibo/SnsAccountSwAuthSSOReceiver;->registerLoginNotification(Landroid/content/Context;)V

    goto :goto_0

    .line 53
    .end local v0    # "accounts":[Landroid/accounts/Account;
    :cond_4
    const-string v3, "sina.weibo.action.NOUSER"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    const-string v3, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    const-string v3, "package:com.sina.weibo"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 57
    :cond_5
    invoke-direct {p0, p1}, Lcom/sec/android/app/sns3/auth/sp/sinaweibo/SnsAccountSwAuthSSOReceiver;->removeSinaweiboSSOAccount(Landroid/content/Context;)V

    goto :goto_0
.end method
