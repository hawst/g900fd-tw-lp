.class public Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;
.super Ljava/lang/Object;
.source "SnsLiSyncDataMgr.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr$1;
    }
.end annotation


# static fields
.field public static final FEEDS_LIMIT:Ljava/lang/String; = "50"

.field private static final TAG:Ljava/lang/String; = "SNSLinkedIn"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mResources:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 103
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 104
    iput-object p1, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;->mContext:Landroid/content/Context;

    .line 105
    iget-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;->mResources:Landroid/content/res/Resources;

    .line 106
    return-void
.end method

.method private clearTable(Landroid/net/Uri;)V
    .locals 3
    .param p1, "tableUri"    # Landroid/net/Uri;

    .prologue
    const/4 v2, 0x0

    .line 335
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/sns3/SnsApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 337
    .local v0, "cr":Landroid/content/ContentResolver;
    invoke-virtual {v0, p1, v2, v2}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 339
    return-void
.end method

.method private insertApps(Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$AppUpdates;Ljava/lang/String;)V
    .locals 4
    .param p1, "mAppUpdates"    # Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$AppUpdates;
    .param p2, "mUpdateKey"    # Ljava/lang/String;

    .prologue
    .line 878
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/sns3/SnsApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 879
    .local v1, "cr":Landroid/content/ContentResolver;
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 880
    .local v0, "appUpdateValues":Landroid/content/ContentValues;
    const-string v2, "apps_update_key"

    invoke-virtual {v0, v2, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 881
    const-string v2, "app_id"

    iget v3, p1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$AppUpdates;->mAppId:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 882
    const-string v2, "activity_body"

    iget-object v3, p1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$AppUpdates;->mActivityBody:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 883
    sget-object v2, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$AppInfo;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 884
    return-void
.end method

.method private insertComments(Ljava/util/List;Ljava/lang/String;)V
    .locals 8
    .param p2, "mUpdateKey"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Comment;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 845
    .local p1, "mComments":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Comment;>;"
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/sns3/SnsApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 846
    .local v2, "cr":Landroid/content/ContentResolver;
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Comment;

    .line 847
    .local v0, "comment":Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Comment;
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 848
    .local v1, "commentValues":Landroid/content/ContentValues;
    const-string v4, "comment_update_key"

    invoke-virtual {v1, v4, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 849
    const-string v4, "comment_id"

    iget-wide v6, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Comment;->mId:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 850
    const-string v4, "comment_person_id"

    iget-object v5, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Comment;->mCommentor:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;

    iget-object v5, v5, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;->mId:Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 851
    const-string v4, "comment_sequence_number"

    iget v5, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Comment;->mSequenceNumber:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 852
    const-string v4, "comment_text"

    iget-object v5, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Comment;->mCommentText:Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 853
    const-string v4, "comment_stime_stamp"

    iget-wide v6, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Comment;->mTimeStamp:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 854
    sget-object v4, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$Comments;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2, v4, v1}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    goto :goto_0

    .line 856
    .end local v0    # "comment":Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Comment;
    .end local v1    # "commentValues":Landroid/content/ContentValues;
    :cond_0
    return-void
.end method

.method private insertConnection(Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;)V
    .locals 1
    .param p1, "connection"    # Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;

    .prologue
    .line 782
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;->insertConnection(Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;I)V

    .line 783
    return-void
.end method

.method private insertConnection(Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;I)V
    .locals 9
    .param p1, "person"    # Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;
    .param p2, "degree"    # I

    .prologue
    const/4 v4, 0x1

    const/4 v8, 0x0

    .line 786
    if-nez p1, :cond_1

    .line 812
    :cond_0
    :goto_0
    return-void

    .line 788
    :cond_1
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/sns3/SnsApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 789
    .local v0, "cr":Landroid/content/ContentResolver;
    sget-object v1, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$Connections;->CONTENT_URI:Landroid/net/Uri;

    new-array v2, v4, [Ljava/lang/String;

    const-string v3, "id"

    aput-object v3, v2, v8

    const-string v3, "id=?"

    new-array v4, v4, [Ljava/lang/String;

    iget-object v5, p1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;->mId:Ljava/lang/String;

    aput-object v5, v4, v8

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 795
    .local v7, "result":Landroid/database/Cursor;
    if-eqz v7, :cond_2

    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-nez v1, :cond_3

    .line 797
    :cond_2
    new-instance v6, Landroid/content/ContentValues;

    invoke-direct {v6}, Landroid/content/ContentValues;-><init>()V

    .line 798
    .local v6, "connectionValues":Landroid/content/ContentValues;
    const-string v1, "degree"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v6, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 799
    const-string v1, "first_name"

    iget-object v2, p1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;->mFirstName:Ljava/lang/String;

    invoke-virtual {v6, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 800
    const-string v1, "last_name"

    iget-object v2, p1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;->mLastName:Ljava/lang/String;

    invoke-virtual {v6, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 801
    const-string v1, "headline"

    iget-object v2, p1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;->mHeadline:Ljava/lang/String;

    invoke-virtual {v6, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 802
    const-string v1, "id"

    iget-object v2, p1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;->mId:Ljava/lang/String;

    invoke-virtual {v6, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 803
    const-string v1, "picture_url"

    iget-object v2, p1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;->mPictureUrl:Ljava/lang/String;

    invoke-virtual {v6, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 804
    const-string v1, "profile_url"

    iget-object v2, p1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;->mPublicProfileUrl:Ljava/lang/String;

    invoke-virtual {v6, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 805
    sget-object v1, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$Connections;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1, v6}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 808
    .end local v6    # "connectionValues":Landroid/content/ContentValues;
    :cond_3
    if-eqz v7, :cond_0

    .line 809
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method

.method private insertFeed(Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;Landroid/net/Uri;)V
    .locals 7
    .param p1, "feed"    # Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;
    .param p2, "tableUri"    # Landroid/net/Uri;

    .prologue
    .line 743
    if-nez p1, :cond_1

    .line 768
    :cond_0
    :goto_0
    return-void

    .line 746
    :cond_1
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 748
    .local v1, "feedValues":Landroid/content/ContentValues;
    const-string v3, "comment"

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->getComment()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 749
    const-string v3, "from_id"

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->getFromId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 750
    const-string v3, "from_name"

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->getFromName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 751
    const-string v3, "from_icon_url"

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->getFromIconUrl()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 752
    const-string v3, "message"

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 753
    const-string v3, "thumbnail_url"

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->getThumbnailUrl()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 754
    const-string v3, "submitted_url"

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->getSubmittedUrl()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 755
    const-string v3, "title"

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->getTitle()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 756
    const-string v3, "timestamp"

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->getTimestamp()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 757
    const-string v3, "update_type"

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->getUpdateType()Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$UpdateType;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$UpdateType;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 758
    const-string v3, "update_key"

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->getUpdateKey()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 760
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/sns3/SnsApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 761
    .local v0, "cr":Landroid/content/ContentResolver;
    const-string v3, "update_key=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->getUpdateKey()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v0, p2, v1, v3, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    .line 765
    .local v2, "updateResult":I
    if-nez v2, :cond_0

    .line 766
    invoke-virtual {v0, p2, v1}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    goto/16 :goto_0
.end method

.method private insertFeed(Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;Landroid/net/Uri;)V
    .locals 24
    .param p1, "update"    # Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;
    .param p2, "tableUri"    # Landroid/net/Uri;

    .prologue
    .line 402
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mUpdateType:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$UpdateType;

    move-object/from16 v18, v0

    sget-object v19, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$UpdateType;->CONN:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$UpdateType;

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    if-eq v0, v1, :cond_0

    invoke-direct/range {p0 .. p2}, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;->isDuplicate(Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;Landroid/net/Uri;)Z

    move-result v18

    if-eqz v18, :cond_0

    .line 721
    :goto_0
    return-void

    .line 405
    :cond_0
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/sns3/SnsApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v10

    .line 406
    .local v10, "cr":Landroid/content/ContentResolver;
    new-instance v16, Landroid/content/ContentValues;

    invoke-direct/range {v16 .. v16}, Landroid/content/ContentValues;-><init>()V

    .line 407
    .local v16, "updateValues":Landroid/content/ContentValues;
    const-string v18, "action_code"

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mActionCode:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 408
    const-string v18, "current_status"

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mCurrentStatus:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 410
    const-string v18, "is_commentable"

    move-object/from16 v0, p1

    iget-boolean v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->isCommentable:Z

    move/from16 v19, v0

    invoke-static/range {v19 .. v19}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v19

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 411
    const-string v18, "is_likable"

    move-object/from16 v0, p1

    iget-boolean v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->isLikable:Z

    move/from16 v19, v0

    invoke-static/range {v19 .. v19}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v19

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 412
    const-string v18, "is_liked"

    move-object/from16 v0, p1

    iget-boolean v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->isLiked:Z

    move/from16 v19, v0

    invoke-static/range {v19 .. v19}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v19

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 413
    const-string v18, "number_of_likes"

    move-object/from16 v0, p1

    iget v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mNumLikes:I

    move/from16 v19, v0

    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v19

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 414
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mConnection:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;

    move-object/from16 v18, v0

    if-eqz v18, :cond_1

    .line 415
    const-string v18, "person_id"

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mConnection:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;->mId:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 416
    :cond_1
    const-string v18, "timestamp"

    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mTimeStamp:J

    move-wide/from16 v20, v0

    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v19

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 417
    const-string v18, "update_key"

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mUpdateKey:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 418
    const-string v18, "updated_fields"

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mUpdatedFields:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 419
    const-string v18, "update_type"

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mUpdateType:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$UpdateType;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$UpdateType;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 421
    sget-object v18, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$Updates;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, v18

    move-object/from16 v1, v16

    invoke-virtual {v10, v0, v1}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 423
    new-instance v11, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;

    invoke-direct {v11}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;-><init>()V

    .line 424
    .local v11, "feed":Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mUpdateKey:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->setUpdateKey(Ljava/lang/String;)V

    .line 425
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mUpdateType:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$UpdateType;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->setUpdateType(Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$UpdateType;)V

    .line 426
    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mTimeStamp:J

    move-wide/from16 v18, v0

    move-wide/from16 v0, v18

    invoke-virtual {v11, v0, v1}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->setTimestamp(J)V

    .line 427
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mConnection:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;

    move-object/from16 v18, v0

    if-eqz v18, :cond_2

    .line 428
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mConnection:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;->mId:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->setFromId(Ljava/lang/String;)V

    .line 429
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mConnection:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;->getFormattedName()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->setFromName(Ljava/lang/String;)V

    .line 430
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mConnection:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;->mPictureUrl:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->setFromIconUrl(Ljava/lang/String;)V

    .line 432
    :cond_2
    sget-object v18, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr$1;->$SwitchMap$com$sec$android$app$sns3$svc$sp$linkedin$response$SnsLiResponseUpdate$UpdateType:[I

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mUpdateType:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$UpdateType;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$UpdateType;->ordinal()I

    move-result v19

    aget v18, v18, v19

    packed-switch v18, :pswitch_data_0

    .line 708
    const-string v18, "SNSLinkedIn"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "[SnsLiSyncDataMgr] insertFeed Invalid update type: "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mUpdateType:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$UpdateType;

    move-object/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 712
    :cond_3
    :goto_1
    move-object/from16 v0, p1

    iget-boolean v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->isCommentable:Z

    move/from16 v18, v0

    if-eqz v18, :cond_4

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mComments:Ljava/util/List;

    move-object/from16 v18, v0

    if-eqz v18, :cond_4

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mComments:Ljava/util/List;

    move-object/from16 v18, v0

    invoke-interface/range {v18 .. v18}, Ljava/util/List;->size()I

    move-result v18

    if-lez v18, :cond_4

    .line 713
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mComments:Ljava/util/List;

    move-object/from16 v18, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mUpdateKey:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;->insertComments(Ljava/util/List;Ljava/lang/String;)V

    .line 715
    :cond_4
    move-object/from16 v0, p1

    iget-boolean v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->isLikable:Z

    move/from16 v18, v0

    if-eqz v18, :cond_5

    move-object/from16 v0, p1

    iget v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mNumLikes:I

    move/from16 v18, v0

    if-lez v18, :cond_5

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mLikes:Ljava/util/List;

    move-object/from16 v18, v0

    if-eqz v18, :cond_5

    .line 716
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mLikes:Ljava/util/List;

    move-object/from16 v18, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mUpdateKey:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;->insertLikes(Ljava/util/List;Ljava/lang/String;)V

    .line 719
    :cond_5
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v11, v1}, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;->insertFeed(Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 434
    :pswitch_0
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mNewConnections:Ljava/util/List;

    move-object/from16 v18, v0

    invoke-interface/range {v18 .. v18}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v14

    .local v14, "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v18

    if-eqz v18, :cond_6

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;

    .line 435
    .local v8, "connection":Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;
    move-object/from16 v0, p0

    invoke-direct {v0, v8}, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;->insertConnection(Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;)V

    .line 436
    new-instance v9, Landroid/content/ContentValues;

    invoke-direct {v9}, Landroid/content/ContentValues;-><init>()V

    .line 437
    .local v9, "connectionUpdateValues":Landroid/content/ContentValues;
    const-string v18, "connection_update_key"

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mUpdateKey:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v9, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 439
    const-string v18, "new_connection_id"

    iget-object v0, v8, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;->mId:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v9, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 441
    sget-object v18, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$ConnectionUpdates;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, v18

    invoke-virtual {v10, v0, v9}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    goto :goto_2

    .line 444
    .end local v8    # "connection":Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;
    .end local v9    # "connectionUpdateValues":Landroid/content/ContentValues;
    :cond_6
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mNewConnections:Ljava/util/List;

    move-object/from16 v18, v0

    invoke-interface/range {v18 .. v18}, Ljava/util/List;->size()I

    move-result v18

    const/16 v19, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-le v0, v1, :cond_a

    .line 445
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;->mResources:Landroid/content/res/Resources;

    move-object/from16 v18, v0

    const v19, 0x7f080062

    const/16 v20, 0x2

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    invoke-virtual {v11}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->getFromName()Ljava/lang/String;

    move-result-object v22

    aput-object v22, v20, v21

    const/16 v21, 0x1

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mNewConnections:Ljava/util/List;

    move-object/from16 v22, v0

    invoke-interface/range {v22 .. v22}, Ljava/util/List;->size()I

    move-result v22

    invoke-static/range {v22 .. v22}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v22

    aput-object v22, v20, v21

    invoke-virtual/range {v18 .. v20}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->setTitle(Ljava/lang/String;)V

    .line 448
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 449
    .local v7, "connNames":Ljava/lang/StringBuilder;
    const/4 v13, 0x0

    .line 450
    .local v13, "i":I
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mNewConnections:Ljava/util/List;

    move-object/from16 v18, v0

    invoke-interface/range {v18 .. v18}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v14

    :goto_3
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v18

    if-eqz v18, :cond_9

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;

    .line 451
    .local v6, "conn":Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;
    invoke-virtual {v6}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;->getFormattedName()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 452
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mNewConnections:Ljava/util/List;

    move-object/from16 v18, v0

    invoke-interface/range {v18 .. v18}, Ljava/util/List;->size()I

    move-result v18

    add-int/lit8 v18, v18, -0x2

    move/from16 v0, v18

    if-ge v13, v0, :cond_8

    .line 453
    const-string v18, ","

    move-object/from16 v0, v18

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 458
    :cond_7
    :goto_4
    add-int/lit8 v13, v13, 0x1

    .line 459
    goto :goto_3

    .line 454
    :cond_8
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mNewConnections:Ljava/util/List;

    move-object/from16 v18, v0

    invoke-interface/range {v18 .. v18}, Ljava/util/List;->size()I

    move-result v18

    add-int/lit8 v18, v18, -0x2

    move/from16 v0, v18

    if-ne v13, v0, :cond_7

    .line 455
    const-string v18, " "

    move-object/from16 v0, v18

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;->mResources:Landroid/content/res/Resources;

    move-object/from16 v19, v0

    const v20, 0x7f08000b

    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4

    .line 460
    .end local v6    # "conn":Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;
    :cond_9
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->setMessage(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 463
    .end local v7    # "connNames":Ljava/lang/StringBuilder;
    .end local v13    # "i":I
    :cond_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;->mResources:Landroid/content/res/Resources;

    move-object/from16 v19, v0

    const v20, 0x7f080061

    const/16 v18, 0x2

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v21, v0

    const/16 v18, 0x0

    invoke-virtual {v11}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->getFromName()Ljava/lang/String;

    move-result-object v22

    aput-object v22, v21, v18

    const/16 v22, 0x1

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mNewConnections:Ljava/util/List;

    move-object/from16 v18, v0

    const/16 v23, 0x0

    move-object/from16 v0, v18

    move/from16 v1, v23

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;->getFormattedName()Ljava/lang/String;

    move-result-object v18

    aput-object v18, v21, v22

    invoke-virtual/range {v19 .. v21}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->setTitle(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 469
    .end local v14    # "i$":Ljava/util/Iterator;
    :pswitch_1
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mConnection:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;

    move-object/from16 v18, v0

    const/16 v19, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move/from16 v2, v19

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;->insertConnection(Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;I)V

    .line 470
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mConnection:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;

    move-object/from16 v18, v0

    if-eqz v18, :cond_3

    .line 471
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;->mResources:Landroid/content/res/Resources;

    move-object/from16 v18, v0

    const v19, 0x7f080066

    const/16 v20, 0x1

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mConnection:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;->getFormattedName()Ljava/lang/String;

    move-result-object v22

    aput-object v22, v20, v21

    invoke-virtual/range {v18 .. v20}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->setTitle(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 477
    :pswitch_2
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mGroup:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseGroups;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;->insertGroup(Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseGroups;)Landroid/net/Uri;

    .line 478
    new-instance v12, Landroid/content/ContentValues;

    invoke-direct {v12}, Landroid/content/ContentValues;-><init>()V

    .line 479
    .local v12, "groupUpdateValues":Landroid/content/ContentValues;
    const-string v18, "group_id"

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mGroup:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseGroups;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseGroups;->getGroupId()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v12, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 480
    const-string v18, "group_name"

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mGroup:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseGroups;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseGroups;->getGroupName()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v12, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 481
    const-string v18, "group_update_KEY"

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mUpdateKey:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v12, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 482
    sget-object v18, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$GroupUpdates;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, v18

    invoke-virtual {v10, v0, v12}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 483
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;->mResources:Landroid/content/res/Resources;

    move-object/from16 v18, v0

    const v19, 0x7f080064

    const/16 v20, 0x2

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    invoke-virtual {v11}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->getFromName()Ljava/lang/String;

    move-result-object v22

    aput-object v22, v20, v21

    const/16 v21, 0x1

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mGroup:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseGroups;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseGroups;->getGroupName()Ljava/lang/String;

    move-result-object v22

    aput-object v22, v20, v21

    invoke-virtual/range {v18 .. v20}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->setTitle(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 488
    .end local v12    # "groupUpdateValues":Landroid/content/ContentValues;
    :pswitch_3
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mCurrentShare:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Share;

    move-object/from16 v18, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mUpdateKey:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;->insertShare(Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Share;Ljava/lang/String;)V

    .line 490
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mConnection:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;

    move-object/from16 v18, v0

    if-eqz v18, :cond_b

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mConnection:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;->mId:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mCurrentShare:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Share;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Share;->mAuthor:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;->mId:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_b

    .line 492
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;->mResources:Landroid/content/res/Resources;

    move-object/from16 v18, v0

    const v19, 0x7f08006a

    const/16 v20, 0x1

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    invoke-virtual {v11}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->getFromName()Ljava/lang/String;

    move-result-object v22

    aput-object v22, v20, v21

    invoke-virtual/range {v18 .. v20}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->setTitle(Ljava/lang/String;)V

    .line 497
    :goto_5
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mCurrentShare:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Share;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Share;->mTitle:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->setMessage(Ljava/lang/String;)V

    .line 498
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mCurrentShare:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Share;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Share;->mSubmittedUrl:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->setSubmittedUrl(Ljava/lang/String;)V

    .line 499
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mCurrentShare:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Share;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Share;->mThumbnailUrl:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->setThumbnailUrl(Ljava/lang/String;)V

    .line 500
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mCurrentShare:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Share;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Share;->mComment:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->setComment(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 494
    :cond_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;->mResources:Landroid/content/res/Resources;

    move-object/from16 v18, v0

    const v19, 0x7f08006b

    const/16 v20, 0x2

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    invoke-virtual {v11}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->getFromName()Ljava/lang/String;

    move-result-object v22

    aput-object v22, v20, v21

    const/16 v21, 0x1

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mCurrentShare:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Share;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Share;->mAuthor:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;->getFormattedName()Ljava/lang/String;

    move-result-object v22

    aput-object v22, v20, v21

    invoke-virtual/range {v18 .. v20}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->setTitle(Ljava/lang/String;)V

    goto :goto_5

    .line 504
    :pswitch_4
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    .line 505
    .local v5, "cmpyValues":Landroid/content/ContentValues;
    const-string v18, "update_key"

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mUpdateKey:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v5, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 506
    const-string v18, "company_id"

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mCompany:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Company;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-wide v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Company;->mCompanyId:J

    move-wide/from16 v20, v0

    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v5, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 507
    const-string v18, "company_name"

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mCompany:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Company;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Company;->mCompanyName:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v5, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 508
    const-string v18, "update_sub_type"

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mCompanyUpdateSubType:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$CompanyUpdateSubType;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$CompanyUpdateSubType;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v5, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 511
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mCompany:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Company;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-wide v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Company;->mCompanyId:J

    move-wide/from16 v20, v0

    move-object/from16 v0, v18

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ""

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->setFromId(Ljava/lang/String;)V

    .line 512
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mCompany:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Company;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Company;->mCompanyName:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->setFromName(Ljava/lang/String;)V

    .line 513
    sget-object v18, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr$1;->$SwitchMap$com$sec$android$app$sns3$svc$sp$linkedin$response$SnsLiResponseUpdate$CompanyUpdateSubType:[I

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mCompanyUpdateSubType:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$CompanyUpdateSubType;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$CompanyUpdateSubType;->ordinal()I

    move-result v19

    aget v18, v18, v19

    packed-switch v18, :pswitch_data_1

    .line 567
    :goto_6
    sget-object v18, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$CompanyUpdates;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, v18

    invoke-virtual {v10, v0, v5}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    goto/16 :goto_1

    .line 515
    :pswitch_5
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mCurrentShare:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Share;

    move-object/from16 v18, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mUpdateKey:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;->insertShare(Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Share;Ljava/lang/String;)V

    .line 516
    const-string v18, "share_id"

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mCurrentShare:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Share;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Share;->mId:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v5, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 517
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;->mResources:Landroid/content/res/Resources;

    move-object/from16 v18, v0

    const v19, 0x7f08006a

    const/16 v20, 0x1

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    invoke-virtual {v11}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->getFromName()Ljava/lang/String;

    move-result-object v22

    aput-object v22, v20, v21

    invoke-virtual/range {v18 .. v20}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->setTitle(Ljava/lang/String;)V

    .line 518
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mCurrentShare:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Share;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Share;->mTitle:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->setMessage(Ljava/lang/String;)V

    .line 519
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mCurrentShare:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Share;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Share;->mSubmittedUrl:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->setSubmittedUrl(Ljava/lang/String;)V

    .line 520
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mCurrentShare:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Share;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Share;->mComment:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->setComment(Ljava/lang/String;)V

    goto/16 :goto_6

    .line 524
    :pswitch_6
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mJobPost:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$JobPost;

    move-object/from16 v18, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mUpdateKey:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;->insertJob(Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$JobPost;Ljava/lang/String;)V

    .line 525
    const-string v18, "job_id"

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mJobPost:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$JobPost;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$JobPost;->mJobId:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v5, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 527
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;->mResources:Landroid/content/res/Resources;

    move-object/from16 v18, v0

    const v19, 0x7f080065

    const/16 v20, 0x1

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    invoke-virtual {v11}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->getFromName()Ljava/lang/String;

    move-result-object v22

    aput-object v22, v20, v21

    invoke-virtual/range {v18 .. v20}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->setTitle(Ljava/lang/String;)V

    .line 528
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;->mResources:Landroid/content/res/Resources;

    move-object/from16 v18, v0

    const v19, 0x7f08002b

    const/16 v20, 0x2

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mJobPost:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$JobPost;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$JobPost;->mPosition:Ljava/lang/String;

    move-object/from16 v22, v0

    aput-object v22, v20, v21

    const/16 v21, 0x1

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mJobPost:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$JobPost;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$JobPost;->mCompanyName:Ljava/lang/String;

    move-object/from16 v22, v0

    aput-object v22, v20, v21

    invoke-virtual/range {v18 .. v20}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->setMessage(Ljava/lang/String;)V

    .line 530
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mJobPost:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$JobPost;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$JobPost;->mJobUrl:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->setSubmittedUrl(Ljava/lang/String;)V

    goto/16 :goto_6

    .line 534
    :pswitch_7
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mConnection:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;

    move-object/from16 v18, v0

    if-eqz v18, :cond_c

    .line 535
    const-string v18, "person_id"

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mConnection:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;->mId:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v5, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 536
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;->mResources:Landroid/content/res/Resources;

    move-object/from16 v18, v0

    const v19, 0x7f080063

    const/16 v20, 0x2

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    invoke-virtual {v11}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->getFromName()Ljava/lang/String;

    move-result-object v22

    aput-object v22, v20, v21

    const/16 v21, 0x1

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mConnection:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;->getFormattedName()Ljava/lang/String;

    move-result-object v22

    aput-object v22, v20, v21

    invoke-virtual/range {v18 .. v20}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->setTitle(Ljava/lang/String;)V

    .line 539
    :cond_c
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "updated fields "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mUpdatedFields:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->setMessage(Ljava/lang/String;)V

    goto/16 :goto_6

    .line 543
    :pswitch_8
    const-string v18, "old_position_title"

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mOldPosition:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Position;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Position;->mTitle:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v5, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 545
    const-string v18, "old_position_company"

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mOldPosition:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Position;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Position;->mCompany:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v5, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 547
    const-string v18, "new_position_title"

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mNewPosition:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Position;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Position;->mTitle:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v5, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 549
    const-string v18, "new_position_company"

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mNewPosition:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Position;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Position;->mCompany:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v5, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 551
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;->mResources:Landroid/content/res/Resources;

    move-object/from16 v18, v0

    const v19, 0x7f08005f

    const/16 v20, 0x1

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    invoke-virtual {v11}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->getFromName()Ljava/lang/String;

    move-result-object v22

    aput-object v22, v20, v21

    invoke-virtual/range {v18 .. v20}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->setTitle(Ljava/lang/String;)V

    .line 553
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;->mResources:Landroid/content/res/Resources;

    move-object/from16 v18, v0

    const v19, 0x7f08002b

    const/16 v20, 0x2

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mNewPosition:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Position;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Position;->mTitle:Ljava/lang/String;

    move-object/from16 v22, v0

    aput-object v22, v20, v21

    const/16 v21, 0x1

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mNewPosition:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Position;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Position;->mCompany:Ljava/lang/String;

    move-object/from16 v22, v0

    aput-object v22, v20, v21

    invoke-virtual/range {v18 .. v20}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->setMessage(Ljava/lang/String;)V

    goto/16 :goto_6

    .line 558
    :pswitch_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;->mResources:Landroid/content/res/Resources;

    move-object/from16 v18, v0

    const v19, 0x7f080060

    const/16 v20, 0x1

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    invoke-virtual {v11}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->getFromName()Ljava/lang/String;

    move-result-object v22

    aput-object v22, v20, v21

    invoke-virtual/range {v18 .. v20}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->setTitle(Ljava/lang/String;)V

    .line 560
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mProductName:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->setMessage(Ljava/lang/String;)V

    goto/16 :goto_6

    .line 571
    .end local v5    # "cmpyValues":Landroid/content/ContentValues;
    :pswitch_a
    new-instance v17, Landroid/content/ContentValues;

    invoke-direct/range {v17 .. v17}, Landroid/content/ContentValues;-><init>()V

    .line 572
    .local v17, "viralValues":Landroid/content/ContentValues;
    const-string v18, "update_key"

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mUpdateKey:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-virtual/range {v17 .. v19}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 573
    const-string v18, "original_post_update_key"

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mVirlUpdate:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mUpdateKey:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-virtual/range {v17 .. v19}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 575
    sget-object v18, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$ViralUpdates;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-virtual {v10, v0, v1}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 576
    const/4 v15, 0x0

    .line 577
    .local v15, "updateString":Ljava/lang/String;
    const v4, 0x7f08002d

    .line 579
    .local v4, "actionCodeResourceId":I
    const-string v18, "like"

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mActionCode:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_d

    .line 580
    const v4, 0x7f08002d

    .line 584
    :goto_7
    sget-object v18, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr$1;->$SwitchMap$com$sec$android$app$sns3$svc$sp$linkedin$response$SnsLiResponseUpdate$UpdateType:[I

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mVirlUpdate:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mUpdateType:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$UpdateType;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$UpdateType;->ordinal()I

    move-result v19

    aget v18, v18, v19

    packed-switch v18, :pswitch_data_2

    .line 642
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;->mResources:Landroid/content/res/Resources;

    move-object/from16 v18, v0

    const v19, 0x7f080036

    invoke-virtual/range {v18 .. v19}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 644
    :goto_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;->mResources:Landroid/content/res/Resources;

    move-object/from16 v18, v0

    const/16 v19, 0x2

    move/from16 v0, v19

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    invoke-virtual {v11}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->getFromName()Ljava/lang/String;

    move-result-object v21

    aput-object v21, v19, v20

    const/16 v20, 0x1

    aput-object v15, v19, v20

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v0, v4, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->setTitle(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 582
    :cond_d
    const v4, 0x7f08001e

    goto :goto_7

    .line 586
    :pswitch_b
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mVirlUpdate:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mConnection:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;

    move-object/from16 v18, v0

    if-eqz v18, :cond_e

    .line 587
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;->mResources:Landroid/content/res/Resources;

    move-object/from16 v18, v0

    const v19, 0x7f080037

    const/16 v20, 0x1

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mVirlUpdate:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mConnection:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;->getFormattedName()Ljava/lang/String;

    move-result-object v22

    aput-object v22, v20, v21

    invoke-virtual/range {v18 .. v20}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v15

    .line 589
    :cond_e
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mVirlUpdate:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mCurrentShare:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Share;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Share;->mTitle:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->setMessage(Ljava/lang/String;)V

    .line 590
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mVirlUpdate:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mCurrentShare:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Share;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Share;->mSubmittedUrl:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->setSubmittedUrl(Ljava/lang/String;)V

    .line 591
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mVirlUpdate:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mCurrentShare:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Share;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Share;->mComment:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->setComment(Ljava/lang/String;)V

    goto/16 :goto_8

    .line 594
    :pswitch_c
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mVirlUpdate:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mConnection:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;

    move-object/from16 v18, v0

    if-eqz v18, :cond_f

    .line 595
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;->mResources:Landroid/content/res/Resources;

    move-object/from16 v18, v0

    const v19, 0x7f08004a

    const/16 v20, 0x1

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mVirlUpdate:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mConnection:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;->getFormattedName()Ljava/lang/String;

    move-result-object v22

    aput-object v22, v20, v21

    invoke-virtual/range {v18 .. v20}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v15

    .line 597
    :cond_f
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mVirlUpdate:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mCurrentStatus:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->setMessage(Ljava/lang/String;)V

    goto/16 :goto_8

    .line 601
    :pswitch_d
    sget-object v18, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr$1;->$SwitchMap$com$sec$android$app$sns3$svc$sp$linkedin$response$SnsLiResponseUpdate$CompanyUpdateSubType:[I

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mVirlUpdate:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mCompanyUpdateSubType:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$CompanyUpdateSubType;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$CompanyUpdateSubType;->ordinal()I

    move-result v19

    aget v18, v18, v19

    packed-switch v18, :pswitch_data_3

    goto/16 :goto_8

    .line 603
    :pswitch_e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;->mResources:Landroid/content/res/Resources;

    move-object/from16 v18, v0

    const v19, 0x7f08004a

    const/16 v20, 0x1

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mVirlUpdate:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mCompany:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Company;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Company;->mCompanyName:Ljava/lang/String;

    move-object/from16 v22, v0

    aput-object v22, v20, v21

    invoke-virtual/range {v18 .. v20}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v15

    .line 605
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mVirlUpdate:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mCurrentShare:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Share;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Share;->mTitle:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->setMessage(Ljava/lang/String;)V

    .line 606
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mVirlUpdate:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mCurrentShare:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Share;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Share;->mSubmittedUrl:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->setSubmittedUrl(Ljava/lang/String;)V

    .line 607
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mVirlUpdate:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mCurrentShare:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Share;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Share;->mComment:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->setComment(Ljava/lang/String;)V

    goto/16 :goto_8

    .line 611
    :pswitch_f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;->mResources:Landroid/content/res/Resources;

    move-object/from16 v18, v0

    const v19, 0x7f08002c

    const/16 v20, 0x1

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mVirlUpdate:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mCompany:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Company;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Company;->mCompanyName:Ljava/lang/String;

    move-object/from16 v22, v0

    aput-object v22, v20, v21

    invoke-virtual/range {v18 .. v20}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v15

    .line 613
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;->mResources:Landroid/content/res/Resources;

    move-object/from16 v18, v0

    const v19, 0x7f08002b

    const/16 v20, 0x2

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mVirlUpdate:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mJobPost:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$JobPost;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$JobPost;->mPosition:Ljava/lang/String;

    move-object/from16 v22, v0

    aput-object v22, v20, v21

    const/16 v21, 0x1

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mVirlUpdate:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mJobPost:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$JobPost;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$JobPost;->mCompanyName:Ljava/lang/String;

    move-object/from16 v22, v0

    aput-object v22, v20, v21

    invoke-virtual/range {v18 .. v20}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->setMessage(Ljava/lang/String;)V

    .line 616
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mVirlUpdate:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mJobPost:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$JobPost;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$JobPost;->mJobUrl:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->setSubmittedUrl(Ljava/lang/String;)V

    goto/16 :goto_8

    .line 620
    :pswitch_10
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;->mResources:Landroid/content/res/Resources;

    move-object/from16 v18, v0

    const v19, 0x7f080038

    const/16 v20, 0x1

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mVirlUpdate:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mCompany:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Company;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Company;->mCompanyName:Ljava/lang/String;

    move-object/from16 v22, v0

    aput-object v22, v20, v21

    invoke-virtual/range {v18 .. v20}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v15

    .line 622
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "updated fields "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mVirlUpdate:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mUpdatedFields:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->setMessage(Ljava/lang/String;)V

    goto/16 :goto_8

    .line 627
    :pswitch_11
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;->mResources:Landroid/content/res/Resources;

    move-object/from16 v18, v0

    const v19, 0x7f08005e

    const/16 v20, 0x1

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mVirlUpdate:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mConnection:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;->getFormattedName()Ljava/lang/String;

    move-result-object v22

    aput-object v22, v20, v21

    invoke-virtual/range {v18 .. v20}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v15

    .line 630
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;->mResources:Landroid/content/res/Resources;

    move-object/from16 v18, v0

    const v19, 0x7f08002b

    const/16 v20, 0x2

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mVirlUpdate:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mNewPosition:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Position;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Position;->mTitle:Ljava/lang/String;

    move-object/from16 v22, v0

    aput-object v22, v20, v21

    const/16 v21, 0x1

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mVirlUpdate:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mNewPosition:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Position;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Position;->mCompany:Ljava/lang/String;

    move-object/from16 v22, v0

    aput-object v22, v20, v21

    invoke-virtual/range {v18 .. v20}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->setMessage(Ljava/lang/String;)V

    goto/16 :goto_8

    .line 649
    .end local v4    # "actionCodeResourceId":I
    .end local v15    # "updateString":Ljava/lang/String;
    .end local v17    # "viralValues":Landroid/content/ContentValues;
    :pswitch_12
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;->mResources:Landroid/content/res/Resources;

    move-object/from16 v18, v0

    const v19, 0x7f080069

    const/16 v20, 0x1

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    invoke-virtual {v11}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->getFromName()Ljava/lang/String;

    move-result-object v22

    aput-object v22, v20, v21

    invoke-virtual/range {v18 .. v20}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->setTitle(Ljava/lang/String;)V

    .line 650
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mUpdatedFields:Ljava/lang/String;

    move-object/from16 v18, v0

    if-eqz v18, :cond_3

    .line 651
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mUpdatedFields:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->setMessage(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 656
    :pswitch_13
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mAppUpdate:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$AppUpdates;

    move-object/from16 v18, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mUpdateKey:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;->insertApps(Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$AppUpdates;Ljava/lang/String;)V

    .line 657
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mAppUpdate:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$AppUpdates;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$AppUpdates;->mActivityBody:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->setTitle(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 661
    :pswitch_14
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mRecommendationUpdate:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$RecommendationUpdates;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$RecommendationUpdates;->mRecommender:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;

    move-object/from16 v18, v0

    if-eqz v18, :cond_10

    .line 662
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mRecommendationUpdate:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$RecommendationUpdates;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$RecommendationUpdates;->mRecommender:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;

    move-object/from16 v18, v0

    const/16 v19, 0x2

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move/from16 v2, v19

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;->insertConnection(Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;I)V

    .line 663
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;->mResources:Landroid/content/res/Resources;

    move-object/from16 v18, v0

    const v19, 0x7f080068

    const/16 v20, 0x2

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    invoke-virtual {v11}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->getFromName()Ljava/lang/String;

    move-result-object v22

    aput-object v22, v20, v21

    const/16 v21, 0x1

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mRecommendationUpdate:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$RecommendationUpdates;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$RecommendationUpdates;->mRecommender:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;

    move-object/from16 v22, v0

    aput-object v22, v20, v21

    invoke-virtual/range {v18 .. v20}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->setTitle(Ljava/lang/String;)V

    .line 670
    :goto_9
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mRecommendationUpdate:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$RecommendationUpdates;

    move-object/from16 v18, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mUpdateKey:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;->insertRecommendation(Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$RecommendationUpdates;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 666
    :cond_10
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mRecommendationUpdate:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$RecommendationUpdates;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$RecommendationUpdates;->mRecommedee:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;

    move-object/from16 v18, v0

    const/16 v19, 0x2

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move/from16 v2, v19

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;->insertConnection(Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;I)V

    .line 667
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;->mResources:Landroid/content/res/Resources;

    move-object/from16 v18, v0

    const v19, 0x7f080067

    const/16 v20, 0x2

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    invoke-virtual {v11}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->getFromName()Ljava/lang/String;

    move-result-object v22

    aput-object v22, v20, v21

    const/16 v21, 0x1

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mRecommendationUpdate:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$RecommendationUpdates;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$RecommendationUpdates;->mRecommender:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;

    move-object/from16 v22, v0

    aput-object v22, v20, v21

    invoke-virtual/range {v18 .. v20}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->setTitle(Ljava/lang/String;)V

    goto :goto_9

    .line 674
    :pswitch_15
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;->mResources:Landroid/content/res/Resources;

    move-object/from16 v18, v0

    const v19, 0x7f080069

    const/16 v20, 0x1

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    invoke-virtual {v11}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->getFromName()Ljava/lang/String;

    move-result-object v22

    aput-object v22, v20, v21

    invoke-virtual/range {v18 .. v20}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->setTitle(Ljava/lang/String;)V

    .line 675
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mUpdatedFields:Ljava/lang/String;

    move-object/from16 v18, v0

    if-eqz v18, :cond_3

    .line 676
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mUpdatedFields:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->setMessage(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 681
    :pswitch_16
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;->mResources:Landroid/content/res/Resources;

    move-object/from16 v18, v0

    const v19, 0x7f080039

    const/16 v20, 0x1

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    invoke-virtual {v11}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->getFromName()Ljava/lang/String;

    move-result-object v22

    aput-object v22, v20, v21

    invoke-virtual/range {v18 .. v20}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->setTitle(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 685
    :pswitch_17
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mConnection:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;

    move-object/from16 v18, v0

    if-eqz v18, :cond_3

    .line 686
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;->mResources:Landroid/content/res/Resources;

    move-object/from16 v18, v0

    const v19, 0x7f080066

    const/16 v20, 0x1

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mConnection:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;->getFormattedName()Ljava/lang/String;

    move-result-object v22

    aput-object v22, v20, v21

    invoke-virtual/range {v18 .. v20}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->setTitle(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 692
    :pswitch_18
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;->mResources:Landroid/content/res/Resources;

    move-object/from16 v18, v0

    const v19, 0x7f080065

    const/16 v20, 0x1

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    invoke-virtual {v11}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->getFromName()Ljava/lang/String;

    move-result-object v22

    aput-object v22, v20, v21

    invoke-virtual/range {v18 .. v20}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->setTitle(Ljava/lang/String;)V

    .line 693
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;->mResources:Landroid/content/res/Resources;

    move-object/from16 v18, v0

    const v19, 0x7f08002b

    const/16 v20, 0x2

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mJobPost:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$JobPost;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$JobPost;->mPosition:Ljava/lang/String;

    move-object/from16 v22, v0

    aput-object v22, v20, v21

    const/16 v21, 0x1

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mJobPost:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$JobPost;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$JobPost;->mCompanyName:Ljava/lang/String;

    move-object/from16 v22, v0

    aput-object v22, v20, v21

    invoke-virtual/range {v18 .. v20}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->setMessage(Ljava/lang/String;)V

    .line 695
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mJobPost:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$JobPost;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$JobPost;->mJobUrl:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->setSubmittedUrl(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 699
    :pswitch_19
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mActionCode:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mCompany:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Company;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Company;->mCompanyName:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->setTitle(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 703
    :pswitch_1a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;->mResources:Landroid/content/res/Resources;

    move-object/from16 v18, v0

    const v19, 0x7f08006c

    invoke-virtual/range {v18 .. v19}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->setTitle(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 432
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_1a
        :pswitch_4
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_a
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
    .end packed-switch

    .line 513
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch

    .line 584
    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_b
        :pswitch_c
        :pswitch_d
    .end packed-switch

    .line 601
    :pswitch_data_3
    .packed-switch 0x1
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
    .end packed-switch
.end method

.method private insertJob(Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$JobPost;Ljava/lang/String;)V
    .locals 4
    .param p1, "jobPost"    # Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$JobPost;
    .param p2, "updateKey"    # Ljava/lang/String;

    .prologue
    .line 771
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 772
    .local v1, "jobValues":Landroid/content/ContentValues;
    const-string v2, "job_id"

    iget-object v3, p1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$JobPost;->mJobId:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 773
    const-string v2, "company"

    iget-object v3, p1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$JobPost;->mCompanyName:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 774
    const-string v2, "position"

    iget-object v3, p1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$JobPost;->mPosition:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 775
    const-string v2, "job_url"

    iget-object v3, p1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$JobPost;->mJobUrl:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 776
    const-string v2, "jobs_update_key"

    invoke-virtual {v1, v2, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 777
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/sns3/SnsApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 778
    .local v0, "cr":Landroid/content/ContentResolver;
    sget-object v2, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$JobInfo;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 779
    return-void
.end method

.method private insertLikes(Ljava/util/List;Ljava/lang/String;)V
    .locals 6
    .param p2, "mUpdateKey"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 835
    .local p1, "likes":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;>;"
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/sns3/SnsApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 836
    .local v0, "cr":Landroid/content/ContentResolver;
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;

    .line 837
    .local v2, "like":Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 838
    .local v3, "likesValues":Landroid/content/ContentValues;
    const-string v4, "like_update_key"

    invoke-virtual {v3, v4, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 839
    const-string v4, "person_id"

    iget-object v5, v2, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;->mId:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 840
    sget-object v4, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$Likes;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v4, v3}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    goto :goto_0

    .line 842
    .end local v2    # "like":Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;
    .end local v3    # "likesValues":Landroid/content/ContentValues;
    :cond_0
    return-void
.end method

.method private insertRecommendation(Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$RecommendationUpdates;Ljava/lang/String;)V
    .locals 6
    .param p1, "mRecommendationupdates"    # Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$RecommendationUpdates;
    .param p2, "mUpdateKey"    # Ljava/lang/String;

    .prologue
    .line 888
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/sns3/SnsApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 889
    .local v0, "cr":Landroid/content/ContentResolver;
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 890
    .local v1, "recommendationUpdateValues":Landroid/content/ContentValues;
    const-string v2, "recommendation_update_key"

    invoke-virtual {v1, v2, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 892
    const-string v2, "recommendation_id"

    iget-wide v4, p1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$RecommendationUpdates;->mRecommendationId:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 894
    const-string v2, "recommendation_type"

    iget-object v3, p1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$RecommendationUpdates;->mRecommendationType:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 896
    const-string v2, "recommendation_url"

    iget-object v3, p1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$RecommendationUpdates;->mRecommondedUrl:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 898
    iget-object v2, p1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$RecommendationUpdates;->mRecommender:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;

    if-eqz v2, :cond_0

    .line 899
    const-string v2, "recommender_id"

    iget-object v3, p1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$RecommendationUpdates;->mRecommender:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;

    iget-object v3, v3, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;->mId:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 906
    :goto_0
    sget-object v2, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$RecommendationInfo;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 908
    return-void

    .line 902
    :cond_0
    const-string v2, "recommendee_id"

    iget-object v3, p1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$RecommendationUpdates;->mRecommedee:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;

    iget-object v3, v3, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;->mId:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private insertShare(Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Share;Ljava/lang/String;)V
    .locals 6
    .param p1, "mCurrentShare"    # Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Share;
    .param p2, "mUpdateKey"    # Ljava/lang/String;

    .prologue
    .line 815
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/sns3/SnsApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 816
    .local v0, "cr":Landroid/content/ContentResolver;
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 817
    .local v1, "shareValues":Landroid/content/ContentValues;
    iget-object v2, p1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Share;->mAuthor:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;

    if-eqz v2, :cond_0

    .line 818
    iget-object v2, p1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Share;->mAuthor:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;

    const/4 v3, 0x2

    invoke-direct {p0, v2, v3}, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;->insertConnection(Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;I)V

    .line 820
    const-string v2, "author_person_id"

    iget-object v3, p1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Share;->mAuthor:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;

    iget-object v3, v3, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;->mId:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 822
    :cond_0
    const-string v2, "comment"

    iget-object v3, p1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Share;->mComment:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 823
    const-string v2, "current_share_id"

    iget-object v3, p1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Share;->mId:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 824
    const-string v2, "share_update_key"

    invoke-virtual {v1, v2, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 825
    const-string v2, "source_app"

    iget-object v3, p1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Share;->mSourceApplication:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 826
    const-string v2, "source_name"

    iget-object v3, p1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Share;->mSourceSPName:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 827
    const-string v2, "submitted_url"

    iget-object v3, p1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Share;->mSubmittedUrl:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 828
    const-string v2, "title"

    iget-object v3, p1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Share;->mTitle:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 829
    const-string v2, "visibility"

    iget-object v3, p1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Share;->mVisiblity:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 830
    const-string v2, "time_stamp"

    iget-wide v4, p1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Share;->mTimeStamp:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 831
    sget-object v2, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$ShareUpdates;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 832
    return-void
.end method

.method private isDuplicate(Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;Landroid/net/Uri;)Z
    .locals 9
    .param p1, "update"    # Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;
    .param p2, "tableUri"    # Landroid/net/Uri;

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 725
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/sns3/SnsApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 726
    .local v0, "cr":Landroid/content/ContentResolver;
    const-string v3, "update_key = ?"

    .line 727
    .local v3, "selection":Ljava/lang/String;
    new-array v4, v7, [Ljava/lang/String;

    iget-object v1, p1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mUpdateKey:Ljava/lang/String;

    aput-object v1, v4, v8

    .line 730
    .local v4, "selectionArgs":[Ljava/lang/String;
    const/4 v6, 0x0

    .line 732
    .local v6, "result":Landroid/database/Cursor;
    const/4 v2, 0x0

    const/4 v5, 0x0

    move-object v1, p2

    :try_start_0
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 733
    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-eqz v1, :cond_1

    .line 736
    if-eqz v6, :cond_0

    .line 737
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_0
    move v1, v7

    .line 739
    :goto_0
    return v1

    .line 736
    :cond_1
    if-eqz v6, :cond_2

    .line 737
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    move v1, v8

    .line 739
    goto :goto_0

    .line 736
    :catchall_0
    move-exception v1

    if-eqz v6, :cond_3

    .line 737
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v1
.end method


# virtual methods
.method public getLastUpdateTimeStamp(Landroid/net/Uri;)J
    .locals 12
    .param p1, "tableUri"    # Landroid/net/Uri;

    .prologue
    const/4 v7, 0x0

    const/4 v3, 0x0

    .line 355
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    const-wide/32 v10, 0x240c8400

    sub-long v8, v4, v10

    .line 357
    .local v8, "timestamp":J
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/sns3/SnsApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 358
    .local v0, "cr":Landroid/content/ContentResolver;
    const/4 v1, 0x1

    new-array v2, v1, [Ljava/lang/String;

    const-string v1, "max(timestamp)"

    aput-object v1, v2, v7

    move-object v1, p1

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 361
    .local v6, "result":Landroid/database/Cursor;
    if-eqz v6, :cond_1

    .line 362
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 363
    invoke-interface {v6, v7}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    .line 365
    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 367
    :cond_1
    return-wide v8
.end method

.method public insertFeeds(Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdates;)V
    .locals 3
    .param p1, "updates"    # Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdates;

    .prologue
    .line 371
    if-nez p1, :cond_1

    .line 378
    :cond_0
    return-void

    .line 374
    :cond_1
    iget-object v2, p1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdates;->mUpdates:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;

    .line 375
    .local v1, "update":Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;
    sget-object v2, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$FeedList;->CONTENT_URI:Landroid/net/Uri;

    invoke-direct {p0, v1, v2}, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;->insertFeed(Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;Landroid/net/Uri;)V

    goto :goto_0
.end method

.method public insertGroup(Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseGroups;)Landroid/net/Uri;
    .locals 11
    .param p1, "group"    # Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseGroups;

    .prologue
    const/4 v4, 0x1

    const/4 v10, 0x0

    const/4 v5, 0x0

    .line 232
    if-eqz p1, :cond_1

    .line 233
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/sns3/SnsApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 234
    .local v0, "cr":Landroid/content/ContentResolver;
    sget-object v1, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$GroupInfo;->CONTENT_URI:Landroid/net/Uri;

    new-array v2, v4, [Ljava/lang/String;

    const-string v3, "group_id"

    aput-object v3, v2, v10

    const-string v3, "group_id=?"

    new-array v4, v4, [Ljava/lang/String;

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseGroups;->getGroupId()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v4, v10

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 240
    .local v7, "result":Landroid/database/Cursor;
    if-eqz v7, :cond_0

    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-nez v1, :cond_3

    .line 241
    :cond_0
    new-instance v8, Landroid/content/ContentValues;

    invoke-direct {v8}, Landroid/content/ContentValues;-><init>()V

    .line 242
    .local v8, "values":Landroid/content/ContentValues;
    const-string v1, "group_key"

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseGroups;->getGroupKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 245
    const-string v1, "group_id"

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseGroups;->getGroupId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 246
    const-string v1, "group_name"

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseGroups;->getGroupName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 247
    const-string v1, "membership_state"

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseGroups;->getMembershipCode()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 249
    :try_start_0
    sget-object v1, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$GroupInfo;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1, v8}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v5

    .line 254
    if-eqz v7, :cond_1

    .line 255
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 263
    .end local v0    # "cr":Landroid/content/ContentResolver;
    .end local v7    # "result":Landroid/database/Cursor;
    .end local v8    # "values":Landroid/content/ContentValues;
    :cond_1
    :goto_0
    return-object v5

    .line 250
    .restart local v0    # "cr":Landroid/content/ContentResolver;
    .restart local v7    # "result":Landroid/database/Cursor;
    .restart local v8    # "values":Landroid/content/ContentValues;
    :catch_0
    move-exception v6

    .line 251
    .local v6, "e":Landroid/database/sqlite/SQLiteException;
    :try_start_1
    const-string v1, "SNSLinkedIn"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error inserting "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 254
    if-eqz v7, :cond_1

    .line 255
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 254
    .end local v6    # "e":Landroid/database/sqlite/SQLiteException;
    :catchall_0
    move-exception v1

    if-eqz v7, :cond_2

    .line 255
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v1

    .line 259
    .end local v8    # "values":Landroid/content/ContentValues;
    :cond_3
    if-eqz v7, :cond_1

    .line 260
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method

.method public insertHomeFeeds(Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdates;)V
    .locals 3
    .param p1, "updates"    # Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdates;

    .prologue
    .line 391
    if-nez p1, :cond_1

    .line 398
    :cond_0
    return-void

    .line 394
    :cond_1
    iget-object v2, p1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdates;->mUpdates:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;

    .line 395
    .local v1, "update":Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;
    sget-object v2, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$HomeFeedList;->CONTENT_URI:Landroid/net/Uri;

    invoke-direct {p0, v1, v2}, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;->insertFeed(Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;Landroid/net/Uri;)V

    goto :goto_0
.end method

.method public insertProfileFeeds(Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdates;)V
    .locals 3
    .param p1, "updates"    # Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdates;

    .prologue
    .line 381
    if-nez p1, :cond_1

    .line 388
    :cond_0
    return-void

    .line 384
    :cond_1
    iget-object v2, p1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdates;->mUpdates:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;

    .line 385
    .local v1, "update":Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;
    sget-object v2, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$ProfileFeedList;->CONTENT_URI:Landroid/net/Uri;

    invoke-direct {p0, v1, v2}, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;->insertFeed(Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;Landroid/net/Uri;)V

    goto :goto_0
.end method

.method public declared-synchronized resumeSync()V
    .locals 1

    .prologue
    .line 350
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->notify()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 351
    monitor-exit p0

    return-void

    .line 350
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized suspendSync()V
    .locals 2

    .prologue
    .line 343
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 347
    :goto_0
    monitor-exit p0

    return-void

    .line 344
    :catch_0
    move-exception v0

    .line 345
    .local v0, "e":Ljava/lang/InterruptedException;
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 343
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public updateConnections(Ljava/util/List;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation

    .prologue
    .line 145
    .local p1, "connectionsList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;>;"
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 146
    .local v3, "resultBundle":Ljava/util/List;, "Ljava/util/List<Landroid/net/Uri;>;"
    sget-object v5, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$Connections;->CONTENT_URI:Landroid/net/Uri;

    invoke-direct {p0, v5}, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;->clearTable(Landroid/net/Uri;)V

    .line 147
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/sns3/SnsApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 148
    .local v1, "cr":Landroid/content/ContentResolver;
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;

    .line 149
    .local v0, "connection":Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 151
    .local v4, "values":Landroid/content/ContentValues;
    const-string v5, "id"

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;->getID()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 152
    const-string v5, "first_name"

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;->getFirstName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 153
    const-string v5, "last_name"

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;->getLastName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    const-string v5, "headline"

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;->getHeadline()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    const-string v5, "picture_url"

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;->getPictureUrl()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 156
    const-string v5, "industry"

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;->getIndustry()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 157
    const-string v5, "profile_url"

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;->getProfileUrl()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 158
    const-string v5, "name"

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;->getLocation()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 159
    const-string v5, "code"

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;->getLocationCountryCode()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 161
    sget-object v5, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$Connections;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1, v5, v4}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v5

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 163
    .end local v0    # "connection":Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;
    .end local v4    # "values":Landroid/content/ContentValues;
    :cond_0
    return-object v3
.end method

.method public updateContactInfo(Ljava/util/List;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseContactInfo;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation

    .prologue
    .line 305
    .local p1, "contactInfoList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseContactInfo;>;"
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 306
    .local v3, "resultBundle":Ljava/util/List;, "Ljava/util/List<Landroid/net/Uri;>;"
    sget-object v5, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$ContactInfo;->CONTENT_URI:Landroid/net/Uri;

    invoke-direct {p0, v5}, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;->clearTable(Landroid/net/Uri;)V

    .line 307
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/sns3/SnsApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 308
    .local v1, "cr":Landroid/content/ContentResolver;
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 309
    .local v4, "values":Landroid/content/ContentValues;
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseContactInfo;

    .line 310
    .local v0, "contactInfo":Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseContactInfo;
    const-string v5, "contact_type"

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseContactInfo;->getInfoType()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 311
    const-string v5, "contact_subtype"

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseContactInfo;->getSubType()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 312
    const-string v5, "contact_value"

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseContactInfo;->getValue()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 313
    sget-object v5, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$ContactInfo;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1, v5, v4}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v5

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 315
    .end local v0    # "contactInfo":Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseContactInfo;
    :cond_0
    return-object v3
.end method

.method public updateEducation(Ljava/util/List;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseEducation;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation

    .prologue
    .line 268
    .local p1, "educationList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseEducation;>;"
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 269
    .local v3, "resultBundle":Ljava/util/List;, "Ljava/util/List<Landroid/net/Uri;>;"
    if-nez p1, :cond_1

    .line 285
    :cond_0
    return-object v3

    .line 271
    :cond_1
    sget-object v5, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$Education;->CONTENT_URI:Landroid/net/Uri;

    invoke-direct {p0, v5}, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;->clearTable(Landroid/net/Uri;)V

    .line 273
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/sns3/SnsApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 274
    .local v0, "cr":Landroid/content/ContentResolver;
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 275
    .local v4, "values":Landroid/content/ContentValues;
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseEducation;

    .line 276
    .local v1, "education":Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseEducation;
    const-string v5, "degree_of_education"

    invoke-virtual {v1}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseEducation;->getEducationDegree()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 277
    const-string v5, "end_date_year"

    invoke-virtual {v1}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseEducation;->getEducationEnd_Date_Year()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 278
    const-string v5, "education_id"

    invoke-virtual {v1}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseEducation;->getEducationId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 279
    const-string v5, "school_name"

    invoke-virtual {v1}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseEducation;->getEducationSchool_Name()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 280
    const-string v5, "start_date_year"

    invoke-virtual {v1}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseEducation;->getEducationStart_Date_Year()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 281
    const-string v5, "field_of_study"

    invoke-virtual {v1}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseEducation;->getField_Of_Study()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 283
    sget-object v5, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$Education;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v5, v4}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v5

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public updateFollowing(Ljava/util/List;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseFollowingInfo;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation

    .prologue
    .line 320
    .local p1, "followingList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseFollowingInfo;>;"
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 321
    .local v3, "resultBundle":Ljava/util/List;, "Ljava/util/List<Landroid/net/Uri;>;"
    sget-object v5, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$FollowingList;->CONTENT_URI:Landroid/net/Uri;

    invoke-direct {p0, v5}, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;->clearTable(Landroid/net/Uri;)V

    .line 322
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/sns3/SnsApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 323
    .local v0, "cr":Landroid/content/ContentResolver;
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 324
    .local v4, "values":Landroid/content/ContentValues;
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseFollowingInfo;

    .line 325
    .local v1, "following":Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseFollowingInfo;
    const-string v5, "following_id"

    invoke-virtual {v1}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseFollowingInfo;->getId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 326
    const-string v5, "following_name"

    invoke-virtual {v1}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseFollowingInfo;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 327
    const-string v5, "following_type"

    invoke-virtual {v1}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseFollowingInfo;->getFollowingType()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 329
    sget-object v5, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$FollowingList;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v5, v4}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v5

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 331
    .end local v1    # "following":Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseFollowingInfo;
    :cond_0
    return-object v3
.end method

.method public updateGroups(Ljava/util/List;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseGroups;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation

    .prologue
    .line 219
    .local p1, "groupsList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseGroups;>;"
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 220
    .local v2, "resultBundle":Ljava/util/List;, "Ljava/util/List<Landroid/net/Uri;>;"
    if-nez p1, :cond_1

    .line 228
    :cond_0
    return-object v2

    .line 222
    :cond_1
    sget-object v3, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$GroupInfo;->CONTENT_URI:Landroid/net/Uri;

    invoke-direct {p0, v3}, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;->clearTable(Landroid/net/Uri;)V

    .line 224
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseGroups;

    .line 226
    .local v0, "group":Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseGroups;
    invoke-virtual {p0, v0}, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;->insertGroup(Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseGroups;)Landroid/net/Uri;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public updatePositions(Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseProfile;Ljava/util/List;Ljava/util/List;)Ljava/util/List;
    .locals 9
    .param p1, "profile"    # Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseProfile;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseProfile;",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation

    .prologue
    .line 169
    .local p2, "positionsList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;>;"
    .local p3, "subPositionList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;>;"
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 170
    .local v4, "resultBundle":Ljava/util/List;, "Ljava/util/List<Landroid/net/Uri;>;"
    sget-object v7, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$USerWorkInfo;->CONTENT_URI:Landroid/net/Uri;

    invoke-direct {p0, v7}, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;->clearTable(Landroid/net/Uri;)V

    .line 171
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/sns3/SnsApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 172
    .local v0, "cr":Landroid/content/ContentResolver;
    new-instance v6, Landroid/content/ContentValues;

    invoke-direct {v6}, Landroid/content/ContentValues;-><init>()V

    .line 174
    .local v6, "values":Landroid/content/ContentValues;
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;

    .line 175
    .local v3, "position":Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;
    const-string v7, "position_id"

    invoke-virtual {v3}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;->getPositionId()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 176
    const-string v7, "company_id"

    invoke-virtual {v3}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;->getCompanyID()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 177
    const-string v7, "title"

    invoke-virtual {v3}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;->getTitle()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 178
    const-string v7, "summary"

    invoke-virtual {v3}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;->getSummary()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    const-string v7, "startMonth"

    invoke-virtual {v3}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;->getStartMonth()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 180
    const-string v7, "startYear"

    invoke-virtual {v3}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;->getStartYear()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 181
    const-string v7, "endMONTH"

    invoke-virtual {v3}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;->getEndMonth()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 182
    const-string v7, "endtYear"

    invoke-virtual {v3}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;->getEndYear()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 183
    const-string v7, "isCurrent"

    invoke-virtual {v3}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;->getIsCurrent()Ljava/lang/Boolean;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 184
    const-string v7, "company_name"

    invoke-virtual {v3}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;->getCompanyName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 185
    const-string v7, "company_type"

    invoke-virtual {v3}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;->getCompanyType()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 186
    const-string v7, "company_size"

    invoke-virtual {v3}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;->getCompanySize()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 187
    const-string v7, "industry"

    invoke-virtual {v3}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;->getIndustry()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 188
    const-string v7, "person_id"

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseProfile;->getUserID()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 190
    sget-object v7, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$USerWorkInfo;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v7, v6}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v7

    invoke-interface {v4, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 192
    .end local v3    # "position":Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;
    :cond_0
    invoke-virtual {v6}, Landroid/content/ContentValues;->clear()V

    .line 194
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .end local v1    # "i$":Ljava/util/Iterator;
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;

    .line 195
    .local v5, "subposition":Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;
    invoke-virtual {v5}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;->getPositions()Ljava/util/List;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;

    .line 196
    .restart local v3    # "position":Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;
    invoke-virtual {v3}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;->getTitle()Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_2

    invoke-virtual {v3}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;->getTitle()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_2

    .line 197
    const-string v7, "position_id"

    invoke-virtual {v3}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;->getPositionId()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 198
    const-string v7, "company_id"

    invoke-virtual {v3}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;->getCompanyID()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 199
    const-string v7, "title"

    invoke-virtual {v3}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;->getTitle()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 200
    const-string v7, "summary"

    invoke-virtual {v3}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;->getSummary()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 201
    const-string v7, "startMonth"

    invoke-virtual {v3}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;->getStartMonth()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 202
    const-string v7, "startYear"

    invoke-virtual {v3}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;->getStartYear()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 203
    const-string v7, "isCurrent"

    invoke-virtual {v3}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;->getIsCurrent()Ljava/lang/Boolean;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 204
    const-string v7, "company_size"

    invoke-virtual {v3}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;->getCompanySize()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 205
    const-string v7, "company_type"

    invoke-virtual {v3}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;->getCompanyType()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 206
    const-string v7, "company_name"

    invoke-virtual {v3}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;->getCompanyName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 207
    const-string v7, "industry"

    invoke-virtual {v3}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;->getIndustry()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 208
    const-string v7, "person_id"

    invoke-virtual {v5}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;->getID()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 210
    sget-object v7, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$USerWorkInfo;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v7, v6}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v7

    invoke-interface {v4, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 214
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "position":Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;
    .end local v5    # "subposition":Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;
    :cond_3
    return-object v4
.end method

.method public updateProfile(Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseProfile;)Ljava/util/List;
    .locals 5
    .param p1, "profile"    # Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseProfile;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseProfile;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation

    .prologue
    .line 110
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 111
    .local v1, "resultUriList":Ljava/util/List;, "Ljava/util/List<Landroid/net/Uri;>;"
    sget-object v3, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$UserBasicInfo;->CONTENT_URI:Landroid/net/Uri;

    invoke-direct {p0, v3}, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;->clearTable(Landroid/net/Uri;)V

    .line 112
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/sns3/SnsApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 113
    .local v0, "cr":Landroid/content/ContentResolver;
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 115
    .local v2, "values":Landroid/content/ContentValues;
    const-string v3, "id"

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseProfile;->getUserID()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 116
    const-string v3, "date_of_birth"

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseProfile;->getDateOfBirth()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    const-string v3, "email_address"

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseProfile;->getEmail()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    const-string v3, "first_name"

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseProfile;->getFirstName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    const-string v3, "headline"

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseProfile;->getHeadline()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    const-string v3, "industry"

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseProfile;->getIndustry()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    const-string v3, "last_name"

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseProfile;->getLastName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    const-string v3, "location"

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseProfile;->getLocation()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    const-string v3, "location_code"

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseProfile;->getLocationCountryCode()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    const-string v3, "num_connections"

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseProfile;->getNumberOfConnections()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 125
    const-string v3, "picture_url"

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseProfile;->getPictureUrl()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 126
    const-string v3, "public_profile_url"

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseProfile;->getPublicProfileUrl()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    const-string v3, "formatted_name"

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseProfile;->getFormattedName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 129
    sget-object v3, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$UserBasicInfo;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v3, v2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 130
    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseProfile;->getConnections()Ljava/util/List;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;->updateConnections(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 131
    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseProfile;->getPositions()Ljava/util/List;

    move-result-object v3

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseProfile;->getConnections()Ljava/util/List;

    move-result-object v4

    invoke-virtual {p0, p1, v3, v4}, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;->updatePositions(Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseProfile;Ljava/util/List;Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 134
    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseProfile;->getFollowing()Ljava/util/List;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;->updateFollowing(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 135
    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseProfile;->getSkills()Ljava/util/List;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;->updateSkills(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 136
    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseProfile;->getContactInfo()Ljava/util/List;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;->updateContactInfo(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 137
    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseProfile;->getGroups()Ljava/util/List;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;->updateGroups(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 138
    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseProfile;->getEducation()Ljava/util/List;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;->updateEducation(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 140
    return-object v1
.end method

.method public updateProfile(Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdates;)V
    .locals 6
    .param p1, "updates"    # Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdates;

    .prologue
    .line 859
    iget-object v4, p1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdates;->mUpdates:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;

    .line 861
    .local v3, "update":Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;
    sget-object v4, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr$1;->$SwitchMap$com$sec$android$app$sns3$svc$sp$linkedin$response$SnsLiResponseUpdate$UpdateType:[I

    iget-object v5, v3, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mUpdateType:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$UpdateType;

    invoke-virtual {v5}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$UpdateType;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 863
    :pswitch_1
    iget-object v4, v3, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mNewConnections:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;

    .line 864
    .local v2, "person":Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;
    invoke-direct {p0, v2}, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;->insertConnection(Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;)V

    goto :goto_1

    .line 867
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "person":Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;
    :pswitch_2
    iget-object v4, v3, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mGroup:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseGroups;

    invoke-virtual {p0, v4}, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;->insertGroup(Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseGroups;)Landroid/net/Uri;

    goto :goto_0

    .line 875
    .end local v3    # "update":Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;
    :cond_1
    return-void

    .line 861
    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public updateSkills(Ljava/util/List;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseSkills;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation

    .prologue
    .line 290
    .local p1, "skillsList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseSkills;>;"
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 291
    .local v2, "resultBundle":Ljava/util/List;, "Ljava/util/List<Landroid/net/Uri;>;"
    sget-object v5, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$SkillsInfo;->CONTENT_URI:Landroid/net/Uri;

    invoke-direct {p0, v5}, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;->clearTable(Landroid/net/Uri;)V

    .line 292
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/sns3/SnsApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 293
    .local v0, "cr":Landroid/content/ContentResolver;
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 294
    .local v4, "values":Landroid/content/ContentValues;
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseSkills;

    .line 295
    .local v3, "skill":Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseSkills;
    const-string v5, "skill_id"

    invoke-virtual {v3}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseSkills;->getSkillID()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 296
    const-string v5, "skill_name"

    invoke-virtual {v3}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseSkills;->getSkillName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 298
    sget-object v5, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$SkillsInfo;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v5, v4}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v5

    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 300
    .end local v3    # "skill":Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseSkills;
    :cond_0
    return-object v2
.end method
