.class public Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserAlbum;
.super Ljava/lang/Object;
.source "SnsFbParserAlbum.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserAlbum$FacebookAlbum;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    return-void
.end method

.method public static parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAlbum;
    .locals 4
    .param p0, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 56
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAlbum;

    invoke-direct {v0}, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAlbum;-><init>()V

    .line 60
    .local v0, "album":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAlbum;
    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 62
    .local v2, "jsonObject":Lorg/json/JSONObject;
    const-string v3, "id"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAlbum;->mAlbumID:Ljava/lang/String;

    .line 64
    const-string v3, "from"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 65
    const-string v3, "from"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserFrom;->parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFrom;

    move-result-object v3

    iput-object v3, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAlbum;->mFrom:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFrom;

    .line 69
    :cond_0
    const-string v3, "name"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAlbum;->mAlbumName:Ljava/lang/String;

    .line 70
    const-string v3, "link"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAlbum;->mLink:Ljava/lang/String;

    .line 71
    const-string v3, "cover_photo"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAlbum;->mCoverPhoto:Ljava/lang/String;

    .line 72
    const-string v3, "privacy"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAlbum;->mPrivacy:Ljava/lang/String;

    .line 73
    const-string v3, "created_time"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAlbum;->mCreatedTime:Ljava/lang/String;

    .line 74
    const-string v3, "updated_time"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAlbum;->mUpdatedTime:Ljava/lang/String;

    .line 75
    const-string v3, "type"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAlbum;->mType:Ljava/lang/String;

    .line 76
    const-string v3, "count"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAlbum;->mCount:Ljava/lang/String;

    .line 78
    const-string v3, "description"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 79
    const-string v3, "description"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAlbum;->mDescription:Ljava/lang/String;

    .line 80
    :cond_1
    const-string v3, "location"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 81
    const-string v3, "location"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAlbum;->mLocation:Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 88
    .end local v2    # "jsonObject":Lorg/json/JSONObject;
    :cond_2
    :goto_0
    return-object v0

    .line 83
    :catch_0
    move-exception v1

    .line 85
    .local v1, "e":Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method
