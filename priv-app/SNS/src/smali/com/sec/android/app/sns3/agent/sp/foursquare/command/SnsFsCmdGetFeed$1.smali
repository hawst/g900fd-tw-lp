.class Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdGetFeed$1;
.super Lcom/sec/android/app/sns3/svc/sp/foursquare/request/SnsFsReqGetUser;
.source "SnsFsCmdGetFeed.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdGetFeed;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Handler;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdGetFeed;


# direct methods
.method constructor <init>(Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdGetFeed;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;)V
    .locals 0
    .param p2, "x0"    # Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    .param p3, "x1"    # Ljava/lang/String;

    .prologue
    .line 50
    iput-object p1, p0, Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdGetFeed$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdGetFeed;

    invoke-direct {p0, p2, p3}, Lcom/sec/android/app/sns3/svc/sp/foursquare/request/SnsFsReqGetUser;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public onReqRespond(IZIILandroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;)Z
    .locals 9
    .param p1, "reqID"    # I
    .param p2, "bSuccess"    # Z
    .param p3, "httpStatus"    # I
    .param p4, "errorCode"    # I
    .param p5, "reason"    # Landroid/os/Bundle;
    .param p6, "user"    # Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;

    .prologue
    const/4 v8, 0x1

    .line 55
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 56
    .local v2, "values":Landroid/content/ContentValues;
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/sns3/SnsApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 58
    .local v0, "cr":Landroid/content/ContentResolver;
    if-eqz p2, :cond_1

    .line 59
    if-eqz p6, :cond_0

    .line 60
    move-object v1, p6

    .line 61
    .local v1, "curUser":Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;
    sget-object v3, Lcom/sec/android/app/sns3/agent/sp/foursquare/db/SnsFourSquareDB$StatusStream;->CONTENT_URI:Landroid/net/Uri;

    const-string v4, "user_id=?"

    new-array v5, v8, [Ljava/lang/String;

    const/4 v6, 0x0

    iget-object v7, v1, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mUserID:Ljava/lang/String;

    aput-object v7, v5, v6

    invoke-virtual {v0, v3, v4, v5}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 66
    invoke-virtual {v2}, Landroid/content/ContentValues;->clear()V

    .line 67
    const-string v3, "user_id"

    iget-object v4, v1, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mUserID:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    const-string v3, "user_name"

    iget-object v4, v1, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mFormattedName:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    const-string v3, "checkin_id"

    iget-object v4, v1, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mCheckIn_Id:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    const-string v3, "object_type"

    iget-object v4, v1, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mCheckinType:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    const-string v3, "timestamp_utc"

    iget-object v4, v1, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mCreatedAt:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    const-string v3, "time_zone"

    iget-object v4, v1, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mTimeZoneOffset:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    const-string v3, "location_id"

    iget-object v4, v1, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mLocId:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    const-string v3, "location_name"

    iget-object v4, v1, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mLocName:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    const-string v3, "latitude"

    iget-object v4, v1, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mLocLatitude:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    const-string v3, "longitude"

    iget-object v4, v1, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mLocLongitude:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    const-string v3, "category_id"

    iget-object v4, v1, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mCategoryId:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    const-string v3, "category_name"

    iget-object v4, v1, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mCategoryName:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    const-string v3, "category_icon"

    iget-object v4, v1, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mCategoryIcon:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    const-string v3, "likes_count"

    iget v4, v1, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mLikesCount:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 81
    const-string v3, "comments_count"

    iget v4, v1, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mCommentsCount:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 83
    sget-object v3, Lcom/sec/android/app/sns3/agent/sp/foursquare/db/SnsFourSquareDB$StatusStream;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v3, v2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 86
    .end local v1    # "curUser":Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdGetFeed$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdGetFeed;

    sget-object v4, Lcom/sec/android/app/sns3/agent/sp/foursquare/db/SnsFourSquareDB$StatusStream;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdGetFeed;->setUri(Ljava/lang/String;)V

    .line 93
    :goto_0
    iget-object v3, p0, Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdGetFeed$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdGetFeed;

    invoke-virtual {v3, p2}, Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdGetFeed;->setSuccess(Z)V

    .line 94
    iget-object v3, p0, Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdGetFeed$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdGetFeed;

    invoke-virtual {v3, p0}, Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdGetFeed;->receive(Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;)V

    .line 96
    return v8

    .line 88
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdGetFeed$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdGetFeed;

    new-instance v4, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;

    const-string v5, "foursquare"

    invoke-direct {v4, v5, p3, p4, p5}, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;-><init>(Ljava/lang/String;IILandroid/os/Bundle;)V

    invoke-virtual {v3, v4}, Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdGetFeed;->setResponseList(Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;)Z

    .line 90
    iget-object v3, p0, Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdGetFeed$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdGetFeed;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdGetFeed;->setUri(Ljava/lang/String;)V

    goto :goto_0
.end method
