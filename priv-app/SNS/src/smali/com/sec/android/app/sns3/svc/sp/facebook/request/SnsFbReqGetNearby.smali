.class public abstract Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqGetNearby;
.super Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqBase;
.source "SnsFbReqGetNearby.java"

# interfaces
.implements Lcom/sec/android/app/sns3/svc/sp/facebook/callback/ISnsFbReqCbNearby;


# static fields
.field private static DEFAULT_DISTANCE:Ljava/lang/String;


# instance fields
.field private mDistance:Ljava/lang/String;

.field private mEndTime:Ljava/lang/String;

.field private mLatitude:Ljava/lang/String;

.field private mLongitude:Ljava/lang/String;

.field public mMultiQueryString:Ljava/lang/String;

.field private mStartTime:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    const-string v0, "1000"

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqGetNearby;->DEFAULT_DISTANCE:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Bundle;)V
    .locals 3
    .param p1, "svcMgr"    # Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    .param p2, "bundle"    # Landroid/os/Bundle;

    .prologue
    const/4 v2, 0x0

    .line 58
    const/16 v1, 0x16

    invoke-direct {p0, p1, v1}, Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqBase;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;I)V

    .line 45
    iput-object v2, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqGetNearby;->mDistance:Ljava/lang/String;

    .line 47
    iput-object v2, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqGetNearby;->mLatitude:Ljava/lang/String;

    .line 49
    iput-object v2, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqGetNearby;->mLongitude:Ljava/lang/String;

    .line 51
    iput-object v2, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqGetNearby;->mStartTime:Ljava/lang/String;

    .line 53
    iput-object v2, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqGetNearby;->mEndTime:Ljava/lang/String;

    .line 55
    iput-object v2, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqGetNearby;->mMultiQueryString:Ljava/lang/String;

    .line 60
    if-eqz p2, :cond_0

    .line 61
    const-string v1, "distance"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqGetNearby;->mDistance:Ljava/lang/String;

    .line 63
    const-string v1, "latitude"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqGetNearby;->mLatitude:Ljava/lang/String;

    .line 64
    const-string v1, "longitude"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqGetNearby;->mLongitude:Ljava/lang/String;

    .line 66
    const-string v1, "start_time"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqGetNearby;->mStartTime:Ljava/lang/String;

    .line 67
    const-string v1, "end_time"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqGetNearby;->mEndTime:Ljava/lang/String;

    .line 70
    :cond_0
    const-string v0, ""

    .line 72
    .local v0, "distance_condition":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqGetNearby;->mLatitude:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqGetNearby;->mLongitude:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 74
    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqGetNearby;->mDistance:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 75
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "and distance(latitude, longitude, \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqGetNearby;->mLatitude:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\', \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqGetNearby;->mLongitude:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\') < "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqGetNearby;->mDistance:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 81
    :cond_1
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "{\"query1\":\"SELECT author_uid, id, page_id, post_id, message, timestamp, type FROM location_post WHERE author_uid in (SELECT uid2 FROM friend WHERE uid1=me()) "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "and timestamp > "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqGetNearby;->mStartTime:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " and timestamp < "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqGetNearby;->mEndTime:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\"query2\":\"select name, page_id, latitude, longitude from place where page_id in (SELECT page_id FROM #query1)\", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\"query3\":\"select uid, name, pic_big, sex from user where uid in (select author_uid from #query1)\" }"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqGetNearby;->mMultiQueryString:Ljava/lang/String;

    .line 88
    return-void

    .line 77
    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "and distance(latitude, longitude, \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqGetNearby;->mLatitude:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\', \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqGetNearby;->mLongitude:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\') < "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqGetNearby;->DEFAULT_DISTANCE:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method protected compose()Lcom/sec/android/app/sns3/svc/http/SnsHttpRequest;
    .locals 7

    .prologue
    .line 92
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqGetNearby;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/SnsSvcMgr;->getTokenMgr()Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;

    move-result-object v0

    const-string v1, "facebook"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;->getToken(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbToken;

    .line 94
    .local v6, "token":Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbToken;
    const-string v2, "GET"

    .line 95
    .local v2, "method":Ljava/lang/String;
    const/4 v3, 0x0

    .line 96
    .local v3, "uri":Ljava/lang/String;
    const/4 v4, 0x0

    .line 97
    .local v4, "header":Landroid/os/Bundle;
    const/4 v5, 0x0

    .line 99
    .local v5, "body":Landroid/os/Bundle;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "https://api.facebook.com/method/fql.multiquery?queries="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqGetNearby;->mMultiQueryString:Ljava/lang/String;

    invoke-static {v1}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&access_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v6}, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbToken;->getAccessToken()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->encodeUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&expires_in="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v6}, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbToken;->getExpires()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->encodeUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&format=json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 103
    new-instance v0, Lcom/sec/android/app/sns3/svc/http/SnsHttpRequest;

    iget v1, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqGetNearby;->mReqID:I

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/sns3/svc/http/SnsHttpRequest;-><init>(ILjava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;)V

    return-object v0
.end method

.method protected parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;
    .locals 3
    .param p1, "content"    # Ljava/lang/String;

    .prologue
    .line 108
    const-string v1, "SNS"

    const-string v2, "Facebook  SnsFbReqGetPhotoStream response SUCCESS!!"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 109
    invoke-super {p0, p1}, Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqBase;->parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;

    move-result-object v0

    .line 111
    .local v0, "response":Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;
    if-eqz v0, :cond_0

    .line 114
    .end local v0    # "response":Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;
    :goto_0
    return-object v0

    .restart local v0    # "response":Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;
    :cond_0
    new-instance v1, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserNearby;

    invoke-direct {v1}, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserNearby;-><init>()V

    invoke-virtual {v1, p1}, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserNearby;->parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNearby;

    move-result-object v0

    goto :goto_0
.end method

.method protected respond(Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;)Z
    .locals 7
    .param p1, "result"    # Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;

    .prologue
    .line 119
    iget v1, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqGetNearby;->mReqID:I

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->isSuccess()Z

    move-result v2

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->getHttpstatus()I

    move-result v3

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->getErrorCode()I

    move-result v4

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->getReason()Landroid/os/Bundle;

    move-result-object v5

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->getResponse()Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNearby;

    move-object v0, p0

    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqGetNearby;->onReqRespond(IZIILandroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNearby;)Z

    .line 121
    const/4 v0, 0x1

    return v0
.end method
