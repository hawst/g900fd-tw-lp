.class Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity$5;
.super Ljava/lang/Object;
.source "SnsAccountGpAuthSSOActivity.java"

# interfaces
.implements Lcom/sec/android/app/sns3/agent/command/ISnsCommandCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->getUserInfo()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;)V
    .locals 0

    .prologue
    .line 437
    iput-object p1, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity$5;->this$0:Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCmdRespond(IZLjava/lang/String;Ljava/util/List;)V
    .locals 7
    .param p1, "cmdID"    # I
    .param p2, "bSuccess"    # Z
    .param p3, "uri"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IZ",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p4, "responseList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;>;"
    const/4 v6, 0x0

    .line 442
    iget-object v3, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity$5;->this$0:Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;

    invoke-virtual {v3}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->isResumed()Z

    move-result v3

    if-nez v3, :cond_0

    .line 470
    :goto_0
    return-void

    .line 446
    :cond_0
    if-eqz p2, :cond_3

    .line 447
    const/4 v0, 0x0

    .line 448
    .local v0, "reason":Landroid/os/Bundle;
    iget-object v3, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity$5;->this$0:Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;

    # getter for: Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->mSelectedAccount:Landroid/accounts/Account;
    invoke-static {v3}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->access$600(Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;)Landroid/accounts/Account;

    move-result-object v3

    iget-object v2, v3, Landroid/accounts/Account;->name:Ljava/lang/String;

    .line 449
    .local v2, "userName":Ljava/lang/String;
    const/4 v1, 0x0

    .line 451
    .local v1, "userId":Ljava/lang/String;
    if-eqz p4, :cond_2

    .line 452
    invoke-interface {p4}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_2

    .line 453
    invoke-interface {p4, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;

    invoke-virtual {v3}, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;->getReason()Landroid/os/Bundle;

    move-result-object v0

    .line 455
    if-eqz v0, :cond_2

    .line 456
    if-nez v2, :cond_1

    .line 457
    const-string v3, "userName"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 459
    :cond_1
    const-string v3, "userID"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 464
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity$5;->this$0:Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;

    # invokes: Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->loginSuccess(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v3, v2, v1}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->access$1100(Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 466
    .end local v0    # "reason":Landroid/os/Bundle;
    .end local v1    # "userId":Ljava/lang/String;
    .end local v2    # "userName":Ljava/lang/String;
    :cond_3
    const-string v4, "SnsAccountGpAuthSSOActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Unable to fetch user details. Error = "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {p4, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;

    invoke-virtual {v3}, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;->getErrorCode()I

    move-result v3

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 468
    iget-object v3, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity$5;->this$0:Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;

    # invokes: Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->loginFail()V
    invoke-static {v3}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->access$1000(Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;)V

    goto :goto_0
.end method
