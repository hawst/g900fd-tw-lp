.class public Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetUpdates;
.super Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;
.source "SnsLiCmdGetUpdates.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetUpdates$2;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "SnsLiCmdGetUpdates"


# instance fields
.field private mUserID:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Handler;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 7
    .param p1, "mServiceMgr"    # Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    .param p2, "mCmdHandler"    # Landroid/os/Handler;
    .param p3, "userId"    # Ljava/lang/String;
    .param p4, "params"    # Landroid/os/Bundle;

    .prologue
    .line 51
    invoke-direct {p0, p2}, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;-><init>(Landroid/os/Handler;)V

    .line 53
    new-instance v6, Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;

    invoke-direct {v6}, Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;-><init>()V

    .line 54
    .local v6, "unit":Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;
    iput-object p3, p0, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetUpdates;->mUserID:Ljava/lang/String;

    .line 56
    new-instance v0, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetUpdates$1;

    iget-object v3, p0, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetUpdates;->mUserID:Ljava/lang/String;

    sget-object v5, Lcom/sec/android/app/sns3/svc/sp/linkedin/request/SnsLiReqGetUpdates;->UPDATE_TYPES_STREAM:[Ljava/lang/String;

    move-object v1, p0

    move-object v2, p1

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetUpdates$1;-><init>(Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetUpdates;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;Landroid/os/Bundle;[Ljava/lang/String;)V

    .line 143
    .local v0, "req":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    invoke-virtual {v6, v0}, Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;->addRequest(Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;)V

    .line 145
    invoke-virtual {p0, v6}, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetUpdates;->addCommandUnit(Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;)V

    .line 146
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetUpdates;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetUpdates;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetUpdates;->mUserID:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;

    .prologue
    .line 43
    invoke-static {p0}, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetUpdates;->insertFeed(Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;)V

    return-void
.end method

.method private static insertFeed(Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;)V
    .locals 6
    .param p0, "feed"    # Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;

    .prologue
    .line 162
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 164
    .local v1, "feedValues":Landroid/content/ContentValues;
    const-string v3, "comment"

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->getComment()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 165
    const-string v3, "from_id"

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->getFromId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 166
    const-string v3, "from_name"

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->getFromName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 167
    const-string v3, "from_icon_url"

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->getFromIconUrl()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    const-string v3, "message"

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 169
    const-string v3, "submitted_url"

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->getSubmittedUrl()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 170
    const-string v3, "thumbnail_url"

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->getThumbnailUrl()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 171
    const-string v3, "title"

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->getTitle()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 172
    const-string v3, "timestamp"

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->getTimestamp()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 173
    const-string v3, "update_type"

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->getUpdateType()Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$UpdateType;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$UpdateType;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 174
    const-string v3, "update_key"

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->getUpdateKey()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 175
    const-string v3, "likes_count"

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->getLikesCount()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 176
    const-string v3, "comments_count"

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->getCommentsCount()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 178
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/sns3/SnsApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 180
    .local v0, "cr":Landroid/content/ContentResolver;
    sget-object v3, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$StatusStream;->CONTENT_URI:Landroid/net/Uri;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "update_key = \'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->getUpdateKey()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v0, v3, v1, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    .line 182
    .local v2, "update":I
    if-nez v2, :cond_0

    .line 183
    sget-object v3, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$StatusStream;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v3, v1}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 186
    :cond_0
    return-void
.end method


# virtual methods
.method public getUserID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetUpdates;->mUserID:Ljava/lang/String;

    return-object v0
.end method

.method protected respond()Z
    .locals 5

    .prologue
    .line 155
    const-string v0, "SnsAgent"

    const-string v1, "<SnsLiCmdGetUpdates> respond()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 156
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetUpdates;->getCommandCallback()Lcom/sec/android/app/sns3/agent/command/ISnsCommandCallback;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetUpdates;->getCommandID()I

    move-result v1

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetUpdates;->isSuccess()Z

    move-result v2

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetUpdates;->getUri()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetUpdates;->getResponseList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/sec/android/app/sns3/agent/command/ISnsCommandCallback;->onCmdRespond(IZLjava/lang/String;Ljava/util/List;)V

    .line 158
    const/4 v0, 0x1

    return v0
.end method
