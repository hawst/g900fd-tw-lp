.class public Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdGetFeedTimeline;
.super Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;
.source "SnsTwCmdGetFeedTimeline.java"


# direct methods
.method public constructor <init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Handler;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "svcMgr"    # Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    .param p2, "cmdHandler"    # Landroid/os/Handler;
    .param p3, "userID"    # Ljava/lang/String;
    .param p4, "count"    # Ljava/lang/String;

    .prologue
    .line 42
    invoke-direct {p0, p2}, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;-><init>(Landroid/os/Handler;)V

    .line 44
    new-instance v2, Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;

    invoke-direct {v2}, Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;-><init>()V

    .line 45
    .local v2, "unit":Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 47
    .local v0, "bundleParam":Landroid/os/Bundle;
    sget-object v3, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;->USER_ID:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;

    invoke-virtual {v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;->getParam()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 48
    sget-object v3, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;->COUNT:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;

    invoke-virtual {v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;->getParam()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3, p4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    new-instance v1, Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdGetFeedTimeline$1;

    sget-object v3, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;->STATUSES_USER_TIMELINE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;

    invoke-direct {v1, p0, p1, v3, v0}, Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdGetFeedTimeline$1;-><init>(Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdGetFeedTimeline;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;Landroid/os/Bundle;)V

    .line 108
    .local v1, "req":Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestStatuses;
    invoke-virtual {v2, v1}, Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;->addRequest(Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;)V

    .line 110
    invoke-virtual {p0, v2}, Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdGetFeedTimeline;->addCommandUnit(Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;)V

    .line 111
    return-void
.end method


# virtual methods
.method protected respond()Z
    .locals 5

    .prologue
    .line 115
    const-string v0, "SnsAgent"

    const-string v1, "<SnsTwCmdGetFeedTimeline> respond()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 117
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdGetFeedTimeline;->getCommandCallback()Lcom/sec/android/app/sns3/agent/command/ISnsCommandCallback;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdGetFeedTimeline;->getCommandID()I

    move-result v1

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdGetFeedTimeline;->isSuccess()Z

    move-result v2

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdGetFeedTimeline;->getUri()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdGetFeedTimeline;->getResponseList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/sec/android/app/sns3/agent/command/ISnsCommandCallback;->onCmdRespond(IZLjava/lang/String;Ljava/util/List;)V

    .line 119
    const/4 v0, 0x1

    return v0
.end method
