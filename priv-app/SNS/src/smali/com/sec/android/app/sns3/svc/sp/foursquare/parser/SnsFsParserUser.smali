.class public Lcom/sec/android/app/sns3/svc/sp/foursquare/parser/SnsFsParserUser;
.super Ljava/lang/Object;
.source "SnsFsParserUser.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/foursquare/parser/SnsFsParserUser$FourSquareUser;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    return-void
.end method

.method public static parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;
    .locals 24
    .param p0, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 74
    new-instance v19, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;

    invoke-direct/range {v19 .. v19}, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;-><init>()V

    .line 78
    .local v19, "user":Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;
    :try_start_0
    new-instance v22, Lorg/json/JSONTokener;

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lorg/json/JSONTokener;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v22 .. v22}, Lorg/json/JSONTokener;->nextValue()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lorg/json/JSONObject;

    .line 79
    .local v13, "jsonObj":Lorg/json/JSONObject;
    const-string v22, "meta"

    move-object/from16 v0, v22

    invoke-virtual {v13, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v16

    .line 81
    .local v16, "meta":Lorg/json/JSONObject;
    const-string v22, "code"

    move-object/from16 v0, v16

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v22

    const/16 v23, 0xc8

    move/from16 v0, v22

    move/from16 v1, v23

    if-ne v0, v1, :cond_4

    .line 82
    const-string v22, "response"

    move-object/from16 v0, v22

    invoke-virtual {v13, v0}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lorg/json/JSONObject;

    .line 83
    .local v18, "response":Lorg/json/JSONObject;
    const-string v22, "user"

    move-object/from16 v0, v18

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->opt(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lorg/json/JSONObject;

    .line 85
    .local v20, "userResponse":Lorg/json/JSONObject;
    const-string v22, "id"

    move-object/from16 v0, v20

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, v19

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mUserID:Ljava/lang/String;

    .line 86
    const-string v22, "firstName"

    move-object/from16 v0, v20

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, v19

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mUserFirstName:Ljava/lang/String;

    .line 87
    const-string v22, "lastName"

    move-object/from16 v0, v20

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, v19

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mUserLastName:Ljava/lang/String;

    .line 88
    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mUserFirstName:Ljava/lang/String;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mUserLastName:Ljava/lang/String;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, v19

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mFormattedName:Ljava/lang/String;

    .line 89
    const-string v22, "bio"

    move-object/from16 v0, v20

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, v19

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mAboutMe:Ljava/lang/String;

    .line 90
    const-string v22, "type"

    move-object/from16 v0, v20

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, v19

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mType:Ljava/lang/String;

    .line 91
    const-string v22, "contact"

    move-object/from16 v0, v20

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_0

    .line 92
    const-string v22, "contact"

    move-object/from16 v0, v20

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v22

    invoke-static/range {v22 .. v22}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v22 .. v22}, Lcom/sec/android/app/sns3/svc/sp/foursquare/parser/SnsFsParserContact;->parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseContact;

    move-result-object v7

    .line 94
    .local v7, "contact":Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseContact;
    move-object/from16 v0, v19

    iput-object v7, v0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mContact:Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseContact;

    .line 97
    .end local v7    # "contact":Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseContact;
    :cond_0
    const-string v22, "photo"

    move-object/from16 v0, v20

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lorg/json/JSONObject;

    .line 98
    .local v17, "photo":Lorg/json/JSONObject;
    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "prefix"

    move-object/from16 v0, v17

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, "100x100"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, "suffix"

    move-object/from16 v0, v17

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, v19

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mPhotoUrl:Ljava/lang/String;

    .line 102
    const-string v22, "checkins"

    move-object/from16 v0, v20

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v5

    .line 103
    .local v5, "checkins":Lorg/json/JSONObject;
    const-string v22, "count"

    move-object/from16 v0, v22

    invoke-virtual {v5, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v22

    if-lez v22, :cond_4

    .line 104
    const-string v22, "items"

    move-object/from16 v0, v22

    invoke-virtual {v5, v0}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v11

    .line 105
    .local v11, "itemsArray":Lorg/json/JSONArray;
    invoke-virtual {v11}, Lorg/json/JSONArray;->length()I

    move-result v22

    if-lez v22, :cond_4

    .line 106
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_0
    invoke-virtual {v11}, Lorg/json/JSONArray;->length()I

    move-result v22

    move/from16 v0, v22

    if-ge v9, v0, :cond_4

    .line 107
    invoke-virtual {v11, v9}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    .line 108
    .local v4, "checkinItems":Lorg/json/JSONObject;
    const-string v22, "id"

    move-object/from16 v0, v22

    invoke-virtual {v4, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, v19

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mCheckIn_Id:Ljava/lang/String;

    .line 109
    const-string v22, "createdAt"

    move-object/from16 v0, v22

    invoke-virtual {v4, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, v19

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mCreatedAt:Ljava/lang/String;

    .line 110
    const-string v22, "type"

    move-object/from16 v0, v22

    invoke-virtual {v4, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, v19

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mCheckinType:Ljava/lang/String;

    .line 111
    const-string v22, "timeZoneOffset"

    move-object/from16 v0, v22

    invoke-virtual {v4, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, v19

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mTimeZoneOffset:Ljava/lang/String;

    .line 113
    const-string v22, "venue"

    move-object/from16 v0, v22

    invoke-virtual {v4, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v21

    .line 114
    .local v21, "venue":Lorg/json/JSONObject;
    if-eqz v21, :cond_2

    .line 115
    const-string v22, "name"

    invoke-virtual/range {v21 .. v22}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, v19

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mLocName:Ljava/lang/String;

    .line 116
    const-string v22, "id"

    invoke-virtual/range {v21 .. v22}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, v19

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mLocId:Ljava/lang/String;

    .line 117
    const-string v22, "location"

    invoke-virtual/range {v21 .. v22}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v15

    .line 118
    .local v15, "location":Lorg/json/JSONObject;
    if-eqz v15, :cond_1

    .line 119
    const-string v22, "lat"

    move-object/from16 v0, v22

    invoke-virtual {v15, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, v19

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mLocLatitude:Ljava/lang/String;

    .line 121
    const-string v22, "lng"

    move-object/from16 v0, v22

    invoke-virtual {v15, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, v19

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mLocLongitude:Ljava/lang/String;

    .line 125
    :cond_1
    const-string v22, "categories"

    invoke-virtual/range {v21 .. v22}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    .line 126
    .local v2, "categories":Lorg/json/JSONArray;
    if-eqz v2, :cond_2

    .line 127
    const/4 v12, 0x0

    .local v12, "j":I
    :goto_1
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v22

    move/from16 v0, v22

    if-ge v12, v0, :cond_2

    .line 128
    invoke-virtual {v2, v12}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v3

    .line 129
    .local v3, "categoryItem":Lorg/json/JSONObject;
    const-string v22, "primary"

    move-object/from16 v0, v22

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_3

    .line 130
    const-string v22, "id"

    move-object/from16 v0, v22

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, v19

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mCategoryId:Ljava/lang/String;

    .line 132
    const-string v22, "name"

    move-object/from16 v0, v22

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, v19

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mCategoryName:Ljava/lang/String;

    .line 134
    const-string v22, "icon"

    move-object/from16 v0, v22

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v10

    .line 136
    .local v10, "icon":Lorg/json/JSONObject;
    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "prefix"

    move-object/from16 v0, v23

    invoke-virtual {v10, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, "bg_88"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, "suffix"

    move-object/from16 v0, v23

    invoke-virtual {v10, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, v19

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mCategoryIcon:Ljava/lang/String;

    .line 146
    .end local v2    # "categories":Lorg/json/JSONArray;
    .end local v3    # "categoryItem":Lorg/json/JSONObject;
    .end local v10    # "icon":Lorg/json/JSONObject;
    .end local v12    # "j":I
    .end local v15    # "location":Lorg/json/JSONObject;
    :cond_2
    const-string v22, "likes"

    move-object/from16 v0, v22

    invoke-virtual {v4, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v14

    .line 147
    .local v14, "likes":Lorg/json/JSONObject;
    const-string v22, "count"

    move-object/from16 v0, v22

    invoke-virtual {v14, v0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v22

    move/from16 v0, v22

    move-object/from16 v1, v19

    iput v0, v1, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mLikesCount:I

    .line 149
    const-string v22, "comments"

    move-object/from16 v0, v22

    invoke-virtual {v4, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v6

    .line 151
    .local v6, "comments":Lorg/json/JSONObject;
    const-string v22, "count"

    move-object/from16 v0, v22

    invoke-virtual {v6, v0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v22

    move/from16 v0, v22

    move-object/from16 v1, v19

    iput v0, v1, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mCommentsCount:I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 106
    add-int/lit8 v9, v9, 0x1

    goto/16 :goto_0

    .line 127
    .end local v6    # "comments":Lorg/json/JSONObject;
    .end local v14    # "likes":Lorg/json/JSONObject;
    .restart local v2    # "categories":Lorg/json/JSONArray;
    .restart local v3    # "categoryItem":Lorg/json/JSONObject;
    .restart local v12    # "j":I
    .restart local v15    # "location":Lorg/json/JSONObject;
    :cond_3
    add-int/lit8 v12, v12, 0x1

    goto/16 :goto_1

    .line 157
    .end local v2    # "categories":Lorg/json/JSONArray;
    .end local v3    # "categoryItem":Lorg/json/JSONObject;
    .end local v4    # "checkinItems":Lorg/json/JSONObject;
    .end local v5    # "checkins":Lorg/json/JSONObject;
    .end local v9    # "i":I
    .end local v11    # "itemsArray":Lorg/json/JSONArray;
    .end local v12    # "j":I
    .end local v13    # "jsonObj":Lorg/json/JSONObject;
    .end local v15    # "location":Lorg/json/JSONObject;
    .end local v16    # "meta":Lorg/json/JSONObject;
    .end local v17    # "photo":Lorg/json/JSONObject;
    .end local v18    # "response":Lorg/json/JSONObject;
    .end local v20    # "userResponse":Lorg/json/JSONObject;
    .end local v21    # "venue":Lorg/json/JSONObject;
    :catch_0
    move-exception v8

    .line 158
    .local v8, "e":Lorg/json/JSONException;
    invoke-virtual {v8}, Lorg/json/JSONException;->printStackTrace()V

    .line 161
    .end local v8    # "e":Lorg/json/JSONException;
    :cond_4
    return-object v19
.end method
