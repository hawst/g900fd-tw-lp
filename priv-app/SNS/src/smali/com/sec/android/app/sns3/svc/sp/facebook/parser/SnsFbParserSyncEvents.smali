.class public Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserSyncEvents;
.super Ljava/lang/Object;
.source "SnsFbParserSyncEvents.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserSyncEvents$FacebookSyncEvent;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    return-void
.end method

.method public static parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvents;
    .locals 19
    .param p0, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 82
    new-instance v4, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvents;

    invoke-direct {v4}, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvents;-><init>()V

    .line 84
    .local v4, "eventList":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvents;
    const/4 v7, 0x0

    .line 85
    .local v7, "events":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;
    const/4 v2, 0x0

    .line 88
    .local v2, "curEvents":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;
    :try_start_0
    new-instance v10, Lorg/json/JSONArray;

    move-object/from16 v0, p0

    invoke-direct {v10, v0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 90
    .local v10, "ja":Lorg/json/JSONArray;
    if-eqz v10, :cond_6

    .line 92
    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-virtual {v10, v0}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v14

    .line 93
    .local v14, "memberQuery":Lorg/json/JSONObject;
    const/16 v17, 0x1

    move/from16 v0, v17

    invoke-virtual {v10, v0}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v5

    .line 95
    .local v5, "eventQuery":Lorg/json/JSONObject;
    const-string v17, "fql_result_set"

    move-object/from16 v0, v17

    invoke-virtual {v14, v0}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v15

    .line 97
    .local v15, "memberQueryList":Lorg/json/JSONArray;
    const-string v17, "fql_result_set"

    move-object/from16 v0, v17

    invoke-virtual {v5, v0}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v6

    .line 100
    .local v6, "eventQueryList":Lorg/json/JSONArray;
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_0
    invoke-virtual {v6}, Lorg/json/JSONArray;->length()I

    move-result v17

    move/from16 v0, v17

    if-ge v8, v0, :cond_6

    .line 102
    new-instance v16, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;

    invoke-direct/range {v16 .. v16}, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;-><init>()V

    .line 104
    .local v16, "newEvent":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;
    invoke-virtual {v6, v8}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v12

    .line 106
    .local v12, "jsonObjEvent":Lorg/json/JSONObject;
    const-string v17, "eid"

    move-object/from16 v0, v17

    invoke-virtual {v12, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;->mID:Ljava/lang/String;

    .line 107
    const-string v17, "name"

    move-object/from16 v0, v17

    invoke-virtual {v12, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;->mEventName:Ljava/lang/String;

    .line 109
    new-instance v17, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFrom;

    invoke-direct/range {v17 .. v17}, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFrom;-><init>()V

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;->mOwner:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFrom;

    .line 110
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;->mOwner:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFrom;

    move-object/from16 v17, v0

    const-string v18, "creator"

    move-object/from16 v0, v18

    invoke-virtual {v12, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFrom;->mFromID:Ljava/lang/String;

    .line 112
    const-string v17, "host"

    move-object/from16 v0, v17

    invoke-virtual {v12, v0}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v17

    if-nez v17, :cond_0

    .line 113
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;->mOwner:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFrom;

    move-object/from16 v17, v0

    const-string v18, "host"

    move-object/from16 v0, v18

    invoke-virtual {v12, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFrom;->mFromName:Ljava/lang/String;

    .line 115
    :cond_0
    const-string v17, "start_time"

    move-object/from16 v0, v17

    invoke-virtual {v12, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;->mStartTime:Ljava/lang/String;

    .line 117
    const-string v17, "location"

    move-object/from16 v0, v17

    invoke-virtual {v12, v0}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v17

    if-nez v17, :cond_1

    .line 118
    const-string v17, "location"

    move-object/from16 v0, v17

    invoke-virtual {v12, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;->mLocation:Ljava/lang/String;

    .line 120
    :cond_1
    const-string v17, "event_type"

    move-object/from16 v0, v17

    invoke-virtual {v12, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;->mEventType:Ljava/lang/String;

    .line 121
    const-string v17, "end_time"

    move-object/from16 v0, v17

    invoke-virtual {v12, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;->mEndTime:Ljava/lang/String;

    .line 122
    const-string v17, "update_time"

    move-object/from16 v0, v17

    invoke-virtual {v12, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;->mUpdatedTime:Ljava/lang/String;

    .line 123
    const-string v17, "description"

    move-object/from16 v0, v17

    invoke-virtual {v12, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;->mDescription:Ljava/lang/String;

    .line 124
    const-string v17, "privacy"

    move-object/from16 v0, v17

    invoke-virtual {v12, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;->mPrivacy:Ljava/lang/String;

    .line 125
    const-string v17, "pic"

    move-object/from16 v0, v17

    invoke-virtual {v12, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;->mEventPicture:Ljava/lang/String;

    .line 127
    const-string v17, "venue"

    move-object/from16 v0, v17

    invoke-virtual {v12, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_2

    .line 128
    const-string v17, "street"

    move-object/from16 v0, v17

    invoke-virtual {v12, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;->mStreet:Ljava/lang/String;

    .line 129
    const-string v17, "city"

    move-object/from16 v0, v17

    invoke-virtual {v12, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;->mCity:Ljava/lang/String;

    .line 130
    const-string v17, "state"

    move-object/from16 v0, v17

    invoke-virtual {v12, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;->mState:Ljava/lang/String;

    .line 131
    const-string v17, "zip"

    move-object/from16 v0, v17

    invoke-virtual {v12, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;->mZip:Ljava/lang/String;

    .line 132
    const-string v17, "country"

    move-object/from16 v0, v17

    invoke-virtual {v12, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;->mCountry:Ljava/lang/String;

    .line 133
    const-string v17, "latitude"

    move-object/from16 v0, v17

    invoke-virtual {v12, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;->mLatitude:Ljava/lang/String;

    .line 134
    const-string v17, "longitude"

    move-object/from16 v0, v17

    invoke-virtual {v12, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;->mLongitude:Ljava/lang/String;

    .line 137
    :cond_2
    const/4 v9, 0x0

    .local v9, "j":I
    :goto_1
    invoke-virtual {v15}, Lorg/json/JSONArray;->length()I

    move-result v17

    move/from16 v0, v17

    if-ge v9, v0, :cond_4

    .line 139
    invoke-virtual {v15, v9}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v13

    .line 140
    .local v13, "jsonObjMember":Lorg/json/JSONObject;
    const-string v17, "eid"

    move-object/from16 v0, v17

    invoke-virtual {v13, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;->mID:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_3

    .line 141
    const-string v17, "rsvp_status"

    move-object/from16 v0, v17

    invoke-virtual {v13, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;->mRsvpStatus:Ljava/lang/String;

    .line 137
    :cond_3
    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    .line 146
    .end local v13    # "jsonObjMember":Lorg/json/JSONObject;
    :cond_4
    if-nez v7, :cond_5

    .line 147
    move-object/from16 v7, v16

    .line 148
    move-object v2, v7

    .line 100
    :goto_2
    add-int/lit8 v8, v8, 0x1

    goto/16 :goto_0

    .line 150
    :cond_5
    move-object/from16 v0, v16

    iput-object v0, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;

    .line 151
    iget-object v2, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;

    goto :goto_2

    .line 156
    .end local v5    # "eventQuery":Lorg/json/JSONObject;
    .end local v6    # "eventQueryList":Lorg/json/JSONArray;
    .end local v8    # "i":I
    .end local v9    # "j":I
    .end local v12    # "jsonObjEvent":Lorg/json/JSONObject;
    .end local v14    # "memberQuery":Lorg/json/JSONObject;
    .end local v15    # "memberQueryList":Lorg/json/JSONArray;
    .end local v16    # "newEvent":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;
    :cond_6
    iput-object v7, v4, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvents;->mEvents:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 164
    .end local v10    # "ja":Lorg/json/JSONArray;
    :goto_3
    return-object v4

    .line 158
    :catch_0
    move-exception v11

    .line 159
    .local v11, "je":Lorg/json/JSONException;
    invoke-virtual {v11}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_3

    .line 160
    .end local v11    # "je":Lorg/json/JSONException;
    :catch_1
    move-exception v3

    .line 161
    .local v3, "e":Ljava/lang/Exception;
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_3
.end method
