.class public interface abstract Lcom/sec/android/app/sns3/agent/sp/foursquare/db/SnsFourSquareDB$StatusStreamColumns;
.super Ljava/lang/Object;
.source "SnsFourSquareDB.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/agent/sp/foursquare/db/SnsFourSquareDB;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "StatusStreamColumns"
.end annotation


# static fields
.field public static final CATEGORY_ICON:Ljava/lang/String; = "category_icon"

.field public static final CATEGORY_ID:Ljava/lang/String; = "category_id"

.field public static final CATEGORY_NAME:Ljava/lang/String; = "category_name"

.field public static final CHECKIN_ID:Ljava/lang/String; = "checkin_id"

.field public static final COMMENTS_COUNT:Ljava/lang/String; = "comments_count"

.field public static final LATITUDE:Ljava/lang/String; = "latitude"

.field public static final LIKES_COUNT:Ljava/lang/String; = "likes_count"

.field public static final LOCATION_ID:Ljava/lang/String; = "location_id"

.field public static final LOCATION_NAME:Ljava/lang/String; = "location_name"

.field public static final LONGITUDE:Ljava/lang/String; = "longitude"

.field public static final MESSAGE:Ljava/lang/String; = "message"

.field public static final OBJECT_TYPE:Ljava/lang/String; = "object_type"

.field public static final TIMESTAMP_UTC:Ljava/lang/String; = "timestamp_utc"

.field public static final TIME_ZONE:Ljava/lang/String; = "time_zone"

.field public static final USER_ID:Ljava/lang/String; = "user_id"

.field public static final USER_NAME:Ljava/lang/String; = "user_name"
