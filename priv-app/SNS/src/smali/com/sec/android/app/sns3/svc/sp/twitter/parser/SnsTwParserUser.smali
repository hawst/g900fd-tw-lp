.class public Lcom/sec/android/app/sns3/svc/sp/twitter/parser/SnsTwParserUser;
.super Ljava/lang/Object;
.source "SnsTwParserUser.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/twitter/parser/SnsTwParserUser$TwitterUser;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    return-void
.end method

.method public static parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseUser;
    .locals 9
    .param p0, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 76
    const/4 v6, 0x0

    .line 79
    .local v6, "response":Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseUser;
    invoke-static {p0}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->isJsonEmpty(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 80
    const/4 v7, 0x0

    .line 110
    :goto_0
    return-object v7

    .line 84
    :cond_0
    :try_start_0
    invoke-static {p0}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->isJsonArray(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 85
    new-instance v3, Lorg/json/JSONArray;

    invoke-direct {v3, p0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 87
    .local v3, "jsonArray":Lorg/json/JSONArray;
    const/4 v0, 0x0

    .line 88
    .local v0, "curNode":Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseUser;
    const/4 v5, 0x0

    .line 90
    .local v5, "newNode":Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseUser;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v7

    if-ge v2, v7, :cond_3

    .line 91
    invoke-virtual {v3, v2}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/app/sns3/svc/sp/twitter/parser/SnsTwParserUser;->parseResponseUser(Lorg/json/JSONObject;)Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseUser;

    move-result-object v5

    .line 93
    if-nez v6, :cond_1

    .line 94
    move-object v6, v5

    .line 95
    move-object v0, v6

    .line 90
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 97
    :cond_1
    iput-object v5, v0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseUser;->mNext:Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseUser;

    .line 98
    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseUser;->mNext:Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseUser;

    goto :goto_2

    .line 102
    .end local v0    # "curNode":Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseUser;
    .end local v2    # "i":I
    .end local v3    # "jsonArray":Lorg/json/JSONArray;
    .end local v5    # "newNode":Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseUser;
    :cond_2
    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 103
    .local v4, "jsonObj":Lorg/json/JSONObject;
    invoke-static {v4}, Lcom/sec/android/app/sns3/svc/sp/twitter/parser/SnsTwParserUser;->parseResponseUser(Lorg/json/JSONObject;)Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseUser;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .end local v4    # "jsonObj":Lorg/json/JSONObject;
    :cond_3
    :goto_3
    move-object v7, v6

    .line 110
    goto :goto_0

    .line 105
    :catch_0
    move-exception v1

    .line 106
    .local v1, "e":Lorg/json/JSONException;
    const-string v7, "SNS"

    invoke-virtual {v1}, Lorg/json/JSONException;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 107
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_3
.end method

.method private static parseResponseUser(Lorg/json/JSONObject;)Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseUser;
    .locals 2
    .param p0, "jsonObj"    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 114
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseUser;

    invoke-direct {v0}, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseUser;-><init>()V

    .line 116
    .local v0, "user":Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseUser;
    const-string v1, "id"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseUser;->mId:Ljava/lang/String;

    .line 117
    const-string v1, "screen_name"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseUser;->mScreenName:Ljava/lang/String;

    .line 118
    const-string v1, "location"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseUser;->mLocation:Ljava/lang/String;

    .line 119
    const-string v1, "description"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseUser;->mDescription:Ljava/lang/String;

    .line 120
    const-string v1, "profile_image_url"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseUser;->mProfileImageUrl:Ljava/lang/String;

    .line 121
    const-string v1, "url"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseUser;->mUrl:Ljava/lang/String;

    .line 122
    const-string v1, "protected"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, v0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseUser;->mProtected:Z

    .line 123
    const-string v1, "followers_count"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v1

    iput v1, v0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseUser;->mFollowersCount:I

    .line 124
    const-string v1, "created_at"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseUser;->mCreatedAt:Ljava/lang/String;

    .line 125
    const-string v1, "friends_count"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v1

    iput v1, v0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseUser;->mFriendsCount:I

    .line 126
    const-string v1, "favourites_count"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v1

    iput v1, v0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseUser;->mFavouritesCount:I

    .line 127
    const-string v1, "statuses_count"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v1

    iput v1, v0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseUser;->mStatusesCount:I

    .line 128
    const-string v1, "utc_offset"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseUser;->mUtcOffset:Ljava/lang/String;

    .line 129
    const-string v1, "time_zone"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseUser;->mTimeZone:Ljava/lang/String;

    .line 130
    const-string v1, "lang"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseUser;->mLang:Ljava/lang/String;

    .line 131
    const-string v1, "geo_enabled"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, v0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseUser;->mGeoEnabled:Z

    .line 132
    const-string v1, "verified"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, v0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseUser;->mVerified:Z

    .line 133
    const-string v1, "notifications"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, v0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseUser;->mNotifications:Z

    .line 134
    const-string v1, "following"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, v0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseUser;->mFollowing:Z

    .line 135
    const-string v1, "contributors_enabled"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, v0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseUser;->mContributorsEnabled:Z

    .line 136
    const-string v1, "profile_background_color"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseUser;->mProfileBackgroundColor:Ljava/lang/String;

    .line 137
    const-string v1, "profile_text_color"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseUser;->mProfileTextColor:Ljava/lang/String;

    .line 138
    const-string v1, "profile_link_color"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseUser;->mProfileLinkColor:Ljava/lang/String;

    .line 139
    const-string v1, "profile_sidebar_border_color"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseUser;->mProfileSidebarBorderColor:Ljava/lang/String;

    .line 140
    const-string v1, "profile_sidebar_fill_color"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseUser;->mProfileSidebarFillColor:Ljava/lang/String;

    .line 141
    const-string v1, "profile_background_image_url"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseUser;->mProfileBackgroundImageUrl:Ljava/lang/String;

    .line 142
    const-string v1, "profile_background_tile"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseUser;->mProfileBackgroundTile:Ljava/lang/String;

    .line 143
    const-string v1, "default_profile"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseUser;->mDefaultProfile:Ljava/lang/String;

    .line 144
    const-string v1, "profile_background_image_url_https"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseUser;->mProfileBackgroundImageUrlHttps:Ljava/lang/String;

    .line 145
    const-string v1, "profile_image_url_https"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseUser;->mProfileImageUrlHttps:Ljava/lang/String;

    .line 146
    const-string v1, "default_profile_image"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, v0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseUser;->mDefaultProfileImage:Z

    .line 147
    const-string v1, "is_translator"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, v0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseUser;->mIsTranslator:Z

    .line 148
    const-string v1, "follow_request_sent"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, v0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseUser;->mFollowRequestSent:Z

    .line 149
    const-string v1, "show_all_inline_media"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, v0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseUser;->mShowAllInlineMedia:Z

    .line 150
    const-string v1, "listed_count"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v1

    iput v1, v0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseUser;->mListedCount:I

    .line 152
    const-string v1, "size"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v1

    iput v1, v0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseUser;->mSize:I

    .line 153
    const-string v1, "name"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseUser;->mName:Ljava/lang/String;

    .line 154
    const-string v1, "slug"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseUser;->mSlug:Ljava/lang/String;

    .line 156
    const-string v1, "status"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/sns3/svc/sp/twitter/parser/SnsTwParserStatus;->parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseUser;->mStatus:Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;

    .line 158
    return-object v0
.end method
