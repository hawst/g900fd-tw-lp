.class public Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseSource;
.super Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;
.source "SnsTwResponseSource.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseSource;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public mAllReplies:Z

.field public mBlocking:Ljava/lang/String;

.field public mCanDm:Z

.field public mFollowedBy:Z

.field public mFollowing:Z

.field public mId:Ljava/lang/String;

.field public mMarkedSpam:Z

.field public mNext:Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseSource;

.field public mNotificationsEnabled:Z

.field public mScreenName:Ljava/lang/String;

.field public mWantRetweets:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 58
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseSource$1;

    invoke-direct {v0}, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseSource$1;-><init>()V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseSource;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;-><init>()V

    .line 52
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;-><init>()V

    .line 55
    invoke-virtual {p0, p1}, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseSource;->readFromParcel(Landroid/os/Parcel;)V

    .line 56
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 86
    const/4 v0, 0x0

    return v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 3
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 70
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseSource;->mId:Ljava/lang/String;

    .line 71
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseSource;->mNotificationsEnabled:Z

    .line 72
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-ne v0, v1, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseSource;->mCanDm:Z

    .line 73
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseSource;->mFollowing:Z

    .line 74
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseSource;->mWantRetweets:Ljava/lang/String;

    .line 75
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseSource;->mScreenName:Ljava/lang/String;

    .line 76
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-ne v0, v1, :cond_3

    move v0, v1

    :goto_3
    iput-boolean v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseSource;->mMarkedSpam:Z

    .line 77
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-ne v0, v1, :cond_4

    move v0, v1

    :goto_4
    iput-boolean v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseSource;->mAllReplies:Z

    .line 78
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseSource;->mBlocking:Ljava/lang/String;

    .line 79
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-ne v0, v1, :cond_5

    :goto_5
    iput-boolean v1, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseSource;->mFollowedBy:Z

    .line 81
    const-class v0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseSource;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseSource;

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseSource;->mNext:Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseSource;

    .line 82
    return-void

    :cond_0
    move v0, v2

    .line 71
    goto :goto_0

    :cond_1
    move v0, v2

    .line 72
    goto :goto_1

    :cond_2
    move v0, v2

    .line 73
    goto :goto_2

    :cond_3
    move v0, v2

    .line 76
    goto :goto_3

    :cond_4
    move v0, v2

    .line 77
    goto :goto_4

    :cond_5
    move v1, v2

    .line 79
    goto :goto_5
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 92
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseSource;->mId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 93
    iget-boolean v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseSource;->mNotificationsEnabled:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 94
    iget-boolean v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseSource;->mCanDm:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 95
    iget-boolean v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseSource;->mFollowing:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 96
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseSource;->mWantRetweets:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 97
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseSource;->mScreenName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 98
    iget-boolean v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseSource;->mMarkedSpam:Z

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 99
    iget-boolean v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseSource;->mAllReplies:Z

    if-eqz v0, :cond_4

    move v0, v1

    :goto_4
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 100
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseSource;->mBlocking:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 101
    iget-boolean v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseSource;->mFollowedBy:Z

    if-eqz v0, :cond_5

    :goto_5
    int-to-byte v0, v1

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 103
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseSource;->mNext:Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseSource;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 104
    return-void

    :cond_0
    move v0, v2

    .line 93
    goto :goto_0

    :cond_1
    move v0, v2

    .line 94
    goto :goto_1

    :cond_2
    move v0, v2

    .line 95
    goto :goto_2

    :cond_3
    move v0, v2

    .line 98
    goto :goto_3

    :cond_4
    move v0, v2

    .line 99
    goto :goto_4

    :cond_5
    move v1, v2

    .line 101
    goto :goto_5
.end method
