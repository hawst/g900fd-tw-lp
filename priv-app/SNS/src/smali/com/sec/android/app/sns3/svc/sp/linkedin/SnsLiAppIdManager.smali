.class public Lcom/sec/android/app/sns3/svc/sp/linkedin/SnsLiAppIdManager;
.super Ljava/lang/Object;
.source "SnsLiAppIdManager.java"


# static fields
.field private static final DEFAULT_APP_KEY:Ljava/lang/String; = "h5983lhodkq3"

.field private static final DEFAULT_SECRET_KEY:Ljava/lang/String; = "OamiJgDSRcVntXys"

.field private static final PROPERTIES_FILE:Ljava/lang/String; = "/etc/snsli.conf"

.field private static final TAG:Ljava/lang/String; = "SnsLiAppIdManager"

.field private static mInstance:Lcom/sec/android/app/sns3/svc/sp/linkedin/SnsLiAppIdManager;


# instance fields
.field private mAppKey:Ljava/lang/String;

.field private mSecretKey:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/SnsLiAppIdManager;->mInstance:Lcom/sec/android/app/sns3/svc/sp/linkedin/SnsLiAppIdManager;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/SnsLiAppIdManager;->mAppKey:Ljava/lang/String;

    .line 42
    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/SnsLiAppIdManager;->mSecretKey:Ljava/lang/String;

    .line 45
    invoke-direct {p0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/SnsLiAppIdManager;->loadLiAppIdAndKey()V

    .line 46
    return-void
.end method

.method public static getInstance()Lcom/sec/android/app/sns3/svc/sp/linkedin/SnsLiAppIdManager;
    .locals 1

    .prologue
    .line 84
    sget-object v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/SnsLiAppIdManager;->mInstance:Lcom/sec/android/app/sns3/svc/sp/linkedin/SnsLiAppIdManager;

    if-nez v0, :cond_0

    .line 85
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/SnsLiAppIdManager;

    invoke-direct {v0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/SnsLiAppIdManager;-><init>()V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/SnsLiAppIdManager;->mInstance:Lcom/sec/android/app/sns3/svc/sp/linkedin/SnsLiAppIdManager;

    .line 87
    :cond_0
    sget-object v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/SnsLiAppIdManager;->mInstance:Lcom/sec/android/app/sns3/svc/sp/linkedin/SnsLiAppIdManager;

    return-object v0
.end method

.method private loadLiAppIdAndKey()V
    .locals 10

    .prologue
    .line 49
    new-instance v3, Ljava/util/Properties;

    invoke-direct {v3}, Ljava/util/Properties;-><init>()V

    .line 50
    .local v3, "properties":Ljava/util/Properties;
    new-instance v2, Ljava/io/File;

    const-string v7, "/etc/snsli.conf"

    invoke-direct {v2, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 51
    .local v2, "file":Ljava/io/File;
    const/4 v5, 0x0

    .line 54
    .local v5, "stream":Ljava/io/FileInputStream;
    :try_start_0
    new-instance v6, Ljava/io/FileInputStream;

    invoke-direct {v6, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 55
    .end local v5    # "stream":Ljava/io/FileInputStream;
    .local v6, "stream":Ljava/io/FileInputStream;
    :try_start_1
    invoke-virtual {v3, v6}, Ljava/util/Properties;->load(Ljava/io/InputStream;)V

    .line 57
    const-string v7, "APP"

    invoke-virtual {v3, v7}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 58
    .local v0, "appKey":Ljava/lang/String;
    const-string v7, "SECRET"

    invoke-virtual {v3, v7}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 60
    .local v4, "secretKey":Ljava/lang/String;
    const-string v7, "SnsLiAppIdManager"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "LI : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 62
    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/SnsLiAppIdManager;->mAppKey:Ljava/lang/String;

    .line 63
    iput-object v4, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/SnsLiAppIdManager;->mSecretKey:Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 67
    if-eqz v6, :cond_4

    .line 69
    :try_start_2
    invoke-virtual {v6}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-object v5, v6

    .line 76
    .end local v0    # "appKey":Ljava/lang/String;
    .end local v4    # "secretKey":Ljava/lang/String;
    .end local v6    # "stream":Ljava/io/FileInputStream;
    .restart local v5    # "stream":Ljava/io/FileInputStream;
    :cond_0
    :goto_0
    iget-object v7, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/SnsLiAppIdManager;->mAppKey:Ljava/lang/String;

    if-eqz v7, :cond_1

    iget-object v7, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/SnsLiAppIdManager;->mSecretKey:Ljava/lang/String;

    if-nez v7, :cond_2

    .line 78
    :cond_1
    const-string v7, "h5983lhodkq3"

    iput-object v7, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/SnsLiAppIdManager;->mAppKey:Ljava/lang/String;

    .line 79
    const-string v7, "OamiJgDSRcVntXys"

    iput-object v7, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/SnsLiAppIdManager;->mSecretKey:Ljava/lang/String;

    .line 81
    :cond_2
    return-void

    .line 70
    .end local v5    # "stream":Ljava/io/FileInputStream;
    .restart local v0    # "appKey":Ljava/lang/String;
    .restart local v4    # "secretKey":Ljava/lang/String;
    .restart local v6    # "stream":Ljava/io/FileInputStream;
    :catch_0
    move-exception v1

    .line 71
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    move-object v5, v6

    .line 72
    .end local v6    # "stream":Ljava/io/FileInputStream;
    .restart local v5    # "stream":Ljava/io/FileInputStream;
    goto :goto_0

    .line 64
    .end local v0    # "appKey":Ljava/lang/String;
    .end local v1    # "e":Ljava/io/IOException;
    .end local v4    # "secretKey":Ljava/lang/String;
    :catch_1
    move-exception v1

    .line 65
    .restart local v1    # "e":Ljava/io/IOException;
    :goto_1
    :try_start_3
    const-string v7, "SnsLiAppIdManager"

    const-string v8, "LI configuration file not existed"

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 67
    if-eqz v5, :cond_0

    .line 69
    :try_start_4
    invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 70
    :catch_2
    move-exception v1

    .line 71
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 67
    .end local v1    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v7

    :goto_2
    if-eqz v5, :cond_3

    .line 69
    :try_start_5
    invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 72
    :cond_3
    :goto_3
    throw v7

    .line 70
    :catch_3
    move-exception v1

    .line 71
    .restart local v1    # "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 67
    .end local v1    # "e":Ljava/io/IOException;
    .end local v5    # "stream":Ljava/io/FileInputStream;
    .restart local v6    # "stream":Ljava/io/FileInputStream;
    :catchall_1
    move-exception v7

    move-object v5, v6

    .end local v6    # "stream":Ljava/io/FileInputStream;
    .restart local v5    # "stream":Ljava/io/FileInputStream;
    goto :goto_2

    .line 64
    .end local v5    # "stream":Ljava/io/FileInputStream;
    .restart local v6    # "stream":Ljava/io/FileInputStream;
    :catch_4
    move-exception v1

    move-object v5, v6

    .end local v6    # "stream":Ljava/io/FileInputStream;
    .restart local v5    # "stream":Ljava/io/FileInputStream;
    goto :goto_1

    .end local v5    # "stream":Ljava/io/FileInputStream;
    .restart local v0    # "appKey":Ljava/lang/String;
    .restart local v4    # "secretKey":Ljava/lang/String;
    .restart local v6    # "stream":Ljava/io/FileInputStream;
    :cond_4
    move-object v5, v6

    .end local v6    # "stream":Ljava/io/FileInputStream;
    .restart local v5    # "stream":Ljava/io/FileInputStream;
    goto :goto_0
.end method


# virtual methods
.method public getAppKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/SnsLiAppIdManager;->mAppKey:Ljava/lang/String;

    return-object v0
.end method

.method public getSecretKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/SnsLiAppIdManager;->mSecretKey:Ljava/lang/String;

    return-object v0
.end method
