.class public Lcom/sec/android/app/sns3/svc/sp/foursquare/parser/SnsFsParserVenues;
.super Ljava/lang/Object;
.source "SnsFsParserVenues.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/foursquare/parser/SnsFsParserVenues$FourSquareNearby;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    return-void
.end method

.method public static parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseVenues;
    .locals 10
    .param p0, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 93
    new-instance v4, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseVenues;

    invoke-direct {v4}, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseVenues;-><init>()V

    .line 95
    .local v4, "nearObj":Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseVenues;
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 96
    .local v0, "con":Lorg/json/JSONObject;
    const-string v8, "meta"

    invoke-virtual {v0, v8}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v3

    .line 98
    .local v3, "meta":Lorg/json/JSONObject;
    const-string v8, "code"

    invoke-virtual {v3, v8}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v8

    const/16 v9, 0xc8

    if-ne v8, v9, :cond_0

    .line 100
    const-string v8, "response"

    invoke-virtual {v0, v8}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v5

    .line 102
    .local v5, "response":Lorg/json/JSONObject;
    const-string v8, "venues"

    invoke-virtual {v5, v8}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v7

    .line 103
    .local v7, "venues":Lorg/json/JSONArray;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {v7}, Lorg/json/JSONArray;->length()I

    move-result v8

    if-ge v2, v8, :cond_0

    .line 104
    invoke-virtual {v7, v2}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v6

    .line 106
    .local v6, "venueItem":Lorg/json/JSONObject;
    iget-object v8, v4, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseVenues;->mVenues:Ljava/util/List;

    invoke-static {v6}, Lcom/sec/android/app/sns3/svc/sp/foursquare/parser/SnsFsParserVenues;->parseVenue(Lorg/json/JSONObject;)Lcom/sec/android/app/sns3/svc/sp/foursquare/response/Venue;

    move-result-object v9

    invoke-interface {v8, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 103
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 111
    .end local v0    # "con":Lorg/json/JSONObject;
    .end local v2    # "i":I
    .end local v3    # "meta":Lorg/json/JSONObject;
    .end local v5    # "response":Lorg/json/JSONObject;
    .end local v6    # "venueItem":Lorg/json/JSONObject;
    .end local v7    # "venues":Lorg/json/JSONArray;
    :catch_0
    move-exception v1

    .line 112
    .local v1, "e1":Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    .line 114
    .end local v1    # "e1":Lorg/json/JSONException;
    :cond_0
    return-object v4
.end method

.method public static parseSuggested(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseVenues;
    .locals 10
    .param p0, "content"    # Ljava/lang/String;

    .prologue
    .line 171
    new-instance v4, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseVenues;

    invoke-direct {v4}, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseVenues;-><init>()V

    .line 173
    .local v4, "nearObj":Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseVenues;
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 174
    .local v0, "con":Lorg/json/JSONObject;
    const-string v8, "meta"

    invoke-virtual {v0, v8}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v3

    .line 176
    .local v3, "meta":Lorg/json/JSONObject;
    const-string v8, "code"

    invoke-virtual {v3, v8}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v8

    const/16 v9, 0xc8

    if-ne v8, v9, :cond_0

    .line 178
    const-string v8, "response"

    invoke-virtual {v0, v8}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v5

    .line 180
    .local v5, "response":Lorg/json/JSONObject;
    const-string v8, "minivenues"

    invoke-virtual {v5, v8}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v7

    .line 181
    .local v7, "venues":Lorg/json/JSONArray;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {v7}, Lorg/json/JSONArray;->length()I

    move-result v8

    if-ge v2, v8, :cond_0

    .line 182
    invoke-virtual {v7, v2}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v6

    .line 184
    .local v6, "venueItem":Lorg/json/JSONObject;
    iget-object v8, v4, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseVenues;->mVenues:Ljava/util/List;

    invoke-static {v6}, Lcom/sec/android/app/sns3/svc/sp/foursquare/parser/SnsFsParserVenues;->parseVenue(Lorg/json/JSONObject;)Lcom/sec/android/app/sns3/svc/sp/foursquare/response/Venue;

    move-result-object v9

    invoke-interface {v8, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 181
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 189
    .end local v0    # "con":Lorg/json/JSONObject;
    .end local v2    # "i":I
    .end local v3    # "meta":Lorg/json/JSONObject;
    .end local v5    # "response":Lorg/json/JSONObject;
    .end local v6    # "venueItem":Lorg/json/JSONObject;
    .end local v7    # "venues":Lorg/json/JSONArray;
    :catch_0
    move-exception v1

    .line 190
    .local v1, "e1":Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    .line 192
    .end local v1    # "e1":Lorg/json/JSONException;
    :cond_0
    return-object v4
.end method

.method public static parseVenue(Lorg/json/JSONObject;)Lcom/sec/android/app/sns3/svc/sp/foursquare/response/Venue;
    .locals 12
    .param p0, "venueItem"    # Lorg/json/JSONObject;

    .prologue
    .line 118
    new-instance v8, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/Venue;

    invoke-direct {v8}, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/Venue;-><init>()V

    .line 120
    .local v8, "venue":Lcom/sec/android/app/sns3/svc/sp/foursquare/response/Venue;
    :try_start_0
    const-string v9, "id"

    invoke-virtual {p0, v9}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v8, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/Venue;->mId:Ljava/lang/String;

    .line 121
    const-string v9, "name"

    invoke-virtual {p0, v9}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v8, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/Venue;->mName:Ljava/lang/String;

    .line 122
    const-string v9, "rating"

    invoke-virtual {p0, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v8, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/Venue;->mRating:Ljava/lang/String;

    .line 124
    const-string v9, "location"

    invoke-virtual {p0, v9}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v6

    .line 125
    .local v6, "location":Lorg/json/JSONObject;
    const-string v9, "address"

    invoke-virtual {v6, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v8, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/Venue;->mAddress:Ljava/lang/String;

    .line 126
    const-string v9, "crossStreet"

    invoke-virtual {v6, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v8, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/Venue;->mStreet:Ljava/lang/String;

    .line 127
    const-string v9, "lat"

    invoke-virtual {v6, v9}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;)D

    move-result-wide v10

    iput-wide v10, v8, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/Venue;->mLatitude:D

    .line 128
    const-string v9, "lng"

    invoke-virtual {v6, v9}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;)D

    move-result-wide v10

    iput-wide v10, v8, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/Venue;->mLongitude:D

    .line 129
    const-string v9, "distance"

    invoke-virtual {v6, v9}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v9

    iput v9, v8, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/Venue;->mDistance:I

    .line 130
    const-string v9, "city"

    invoke-virtual {v6, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v8, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/Venue;->mCity:Ljava/lang/String;

    .line 131
    const-string v9, "state"

    invoke-virtual {v6, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v8, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/Venue;->mState:Ljava/lang/String;

    .line 132
    const-string v9, "country"

    invoke-virtual {v6, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v8, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/Venue;->mCountry:Ljava/lang/String;

    .line 133
    const-string v9, "cc"

    invoke-virtual {v6, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v8, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/Venue;->mCountryCode:Ljava/lang/String;

    .line 134
    const-string v9, "postalCode"

    invoke-virtual {v6, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v8, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/Venue;->mPostalCode:Ljava/lang/String;

    .line 136
    const-string v9, "categories"

    invoke-virtual {p0, v9}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    .line 138
    .local v0, "categories":Lorg/json/JSONArray;
    const/4 v4, 0x0

    .local v4, "j":I
    :goto_0
    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v9

    if-ge v4, v9, :cond_2

    .line 140
    invoke-virtual {v0, v4}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v1

    .line 141
    .local v1, "categoryItem":Lorg/json/JSONObject;
    const-string v9, "name"

    invoke-virtual {v1, v9}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v8, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/Venue;->mCategory:Ljava/lang/String;

    .line 143
    const-string v9, "icon"

    invoke-virtual {v1, v9}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v3

    .line 145
    .local v3, "icon":Lorg/json/JSONObject;
    const-string v9, "prefix"

    invoke-virtual {v3, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v8, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/Venue;->mIconUrlPrefix:Ljava/lang/String;

    .line 147
    const-string v9, "sizes"

    invoke-virtual {v3, v9}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v7

    .line 148
    .local v7, "sizes":Lorg/json/JSONArray;
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    iput-object v9, v8, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/Venue;->mAvailableIconSizes:Ljava/util/List;

    .line 149
    if-eqz v7, :cond_0

    .line 150
    const/4 v5, 0x0

    .local v5, "k":I
    :goto_1
    invoke-virtual {v7}, Lorg/json/JSONArray;->length()I

    move-result v9

    if-ge v5, v9, :cond_1

    .line 151
    iget-object v9, v8, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/Venue;->mAvailableIconSizes:Ljava/util/List;

    invoke-virtual {v7, v5}, Lorg/json/JSONArray;->getInt(I)I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-interface {v9, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 150
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 154
    .end local v5    # "k":I
    :cond_0
    iget-object v9, v8, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/Venue;->mAvailableIconSizes:Ljava/util/List;

    sget-object v10, Lcom/sec/android/app/sns3/svc/sp/foursquare/parser/SnsFsParserVenues$FourSquareNearby;->DEFAULT_SIZES:[Ljava/lang/Integer;

    invoke-static {v10}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v10

    invoke-interface {v9, v10}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 157
    :cond_1
    const-string v9, "suffix"

    invoke-virtual {v3, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v8, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/Venue;->mIconUrlSuffix:Ljava/lang/String;

    .line 159
    const-string v9, "primary"

    invoke-virtual {v1, v9}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v9

    if-eqz v9, :cond_3

    .line 166
    .end local v0    # "categories":Lorg/json/JSONArray;
    .end local v1    # "categoryItem":Lorg/json/JSONObject;
    .end local v3    # "icon":Lorg/json/JSONObject;
    .end local v4    # "j":I
    .end local v6    # "location":Lorg/json/JSONObject;
    .end local v7    # "sizes":Lorg/json/JSONArray;
    :cond_2
    :goto_2
    return-object v8

    .line 138
    .restart local v0    # "categories":Lorg/json/JSONArray;
    .restart local v1    # "categoryItem":Lorg/json/JSONObject;
    .restart local v3    # "icon":Lorg/json/JSONObject;
    .restart local v4    # "j":I
    .restart local v6    # "location":Lorg/json/JSONObject;
    .restart local v7    # "sizes":Lorg/json/JSONArray;
    :cond_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 163
    .end local v0    # "categories":Lorg/json/JSONArray;
    .end local v1    # "categoryItem":Lorg/json/JSONObject;
    .end local v3    # "icon":Lorg/json/JSONObject;
    .end local v4    # "j":I
    .end local v6    # "location":Lorg/json/JSONObject;
    .end local v7    # "sizes":Lorg/json/JSONArray;
    :catch_0
    move-exception v2

    .line 164
    .local v2, "ex":Lorg/json/JSONException;
    invoke-virtual {v2}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_2
.end method
