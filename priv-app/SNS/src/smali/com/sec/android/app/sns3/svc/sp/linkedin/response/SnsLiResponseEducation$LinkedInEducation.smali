.class interface abstract Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseEducation$LinkedInEducation;
.super Ljava/lang/Object;
.source "SnsLiResponseEducation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseEducation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x608
    name = "LinkedInEducation"
.end annotation


# static fields
.field public static final EDUCATION_DATE_YEAR:Ljava/lang/String; = "year"

.field public static final EDUCATION_DEGREE:Ljava/lang/String; = "degree"

.field public static final EDUCATION_END_DATE:Ljava/lang/String; = "endDate"

.field public static final EDUCATION_ID:Ljava/lang/String; = "id"

.field public static final EDUCATION_START_DATE:Ljava/lang/String; = "startDate"

.field public static final FIELD_OF_STUDY:Ljava/lang/String; = "fieldOfStudy"

.field public static final SCHOOL_NAME:Ljava/lang/String; = "schoolName"

.field public static final VALUES:Ljava/lang/String; = "values"
