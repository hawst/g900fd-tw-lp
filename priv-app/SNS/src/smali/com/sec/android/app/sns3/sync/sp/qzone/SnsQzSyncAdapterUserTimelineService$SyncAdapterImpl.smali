.class Lcom/sec/android/app/sns3/sync/sp/qzone/SnsQzSyncAdapterUserTimelineService$SyncAdapterImpl;
.super Landroid/content/AbstractThreadedSyncAdapter;
.source "SnsQzSyncAdapterUserTimelineService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/sync/sp/qzone/SnsQzSyncAdapterUserTimelineService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SyncAdapterImpl"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/sns3/sync/sp/qzone/SnsQzSyncAdapterUserTimelineService;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/sns3/sync/sp/qzone/SnsQzSyncAdapterUserTimelineService;Landroid/content/Context;)V
    .locals 1
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 69
    iput-object p1, p0, Lcom/sec/android/app/sns3/sync/sp/qzone/SnsQzSyncAdapterUserTimelineService$SyncAdapterImpl;->this$0:Lcom/sec/android/app/sns3/sync/sp/qzone/SnsQzSyncAdapterUserTimelineService;

    .line 70
    const/4 v0, 0x1

    invoke-direct {p0, p2, v0}, Landroid/content/AbstractThreadedSyncAdapter;-><init>(Landroid/content/Context;Z)V

    .line 71
    return-void
.end method


# virtual methods
.method public onPerformSync(Landroid/accounts/Account;Landroid/os/Bundle;Ljava/lang/String;Landroid/content/ContentProviderClient;Landroid/content/SyncResult;)V
    .locals 3
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "extras"    # Landroid/os/Bundle;
    .param p3, "authority"    # Ljava/lang/String;
    .param p4, "provider"    # Landroid/content/ContentProviderClient;
    .param p5, "syncResult"    # Landroid/content/SyncResult;

    .prologue
    .line 77
    const-string v1, "SnsQzSync"

    const-string v2, "***************** SnsQzSyncAdapterTimelineService : onPerformSync!!! *****************"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 80
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/qzone/SnsQzSyncAdapterUserTimelineService$SyncAdapterImpl;->this$0:Lcom/sec/android/app/sns3/sync/sp/qzone/SnsQzSyncAdapterUserTimelineService;

    # setter for: Lcom/sec/android/app/sns3/sync/sp/qzone/SnsQzSyncAdapterUserTimelineService;->mAccount:Landroid/accounts/Account;
    invoke-static {v1, p1}, Lcom/sec/android/app/sns3/sync/sp/qzone/SnsQzSyncAdapterUserTimelineService;->access$002(Lcom/sec/android/app/sns3/sync/sp/qzone/SnsQzSyncAdapterUserTimelineService;Landroid/accounts/Account;)Landroid/accounts/Account;

    .line 81
    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/qzone/SnsQzSyncAdapterUserTimelineService$SyncAdapterImpl;->this$0:Lcom/sec/android/app/sns3/sync/sp/qzone/SnsQzSyncAdapterUserTimelineService;

    # setter for: Lcom/sec/android/app/sns3/sync/sp/qzone/SnsQzSyncAdapterUserTimelineService;->mAuthority:Ljava/lang/String;
    invoke-static {v1, p3}, Lcom/sec/android/app/sns3/sync/sp/qzone/SnsQzSyncAdapterUserTimelineService;->access$102(Lcom/sec/android/app/sns3/sync/sp/qzone/SnsQzSyncAdapterUserTimelineService;Ljava/lang/String;)Ljava/lang/String;

    .line 82
    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/qzone/SnsQzSyncAdapterUserTimelineService$SyncAdapterImpl;->this$0:Lcom/sec/android/app/sns3/sync/sp/qzone/SnsQzSyncAdapterUserTimelineService;

    # setter for: Lcom/sec/android/app/sns3/sync/sp/qzone/SnsQzSyncAdapterUserTimelineService;->mSyncResult:Landroid/content/SyncResult;
    invoke-static {v1, p5}, Lcom/sec/android/app/sns3/sync/sp/qzone/SnsQzSyncAdapterUserTimelineService;->access$202(Lcom/sec/android/app/sns3/sync/sp/qzone/SnsQzSyncAdapterUserTimelineService;Landroid/content/SyncResult;)Landroid/content/SyncResult;

    .line 84
    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/qzone/SnsQzSyncAdapterUserTimelineService$SyncAdapterImpl;->this$0:Lcom/sec/android/app/sns3/sync/sp/qzone/SnsQzSyncAdapterUserTimelineService;

    # invokes: Lcom/sec/android/app/sns3/sync/sp/qzone/SnsQzSyncAdapterUserTimelineService;->performSync()V
    invoke-static {v1}, Lcom/sec/android/app/sns3/sync/sp/qzone/SnsQzSyncAdapterUserTimelineService;->access$300(Lcom/sec/android/app/sns3/sync/sp/qzone/SnsQzSyncAdapterUserTimelineService;)V
    :try_end_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 92
    :goto_0
    return-void

    .line 86
    :catch_0
    move-exception v0

    .line 87
    .local v0, "e":Landroid/accounts/OperationCanceledException;
    const-string v1, "SnsQzSync"

    const-string v2, "SnsQzSyncAdapterTimelineService : onPerformSync is CANCELED!!!"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 88
    .end local v0    # "e":Landroid/accounts/OperationCanceledException;
    :catch_1
    move-exception v0

    .line 89
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "SnsQzSync"

    const-string v2, "SnsQzSyncAdapterTimelineService : Abnormal Syncing!!!"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onSyncCanceled()V
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/qzone/SnsQzSyncAdapterUserTimelineService$SyncAdapterImpl;->this$0:Lcom/sec/android/app/sns3/sync/sp/qzone/SnsQzSyncAdapterUserTimelineService;

    # invokes: Lcom/sec/android/app/sns3/sync/sp/qzone/SnsQzSyncAdapterUserTimelineService;->onSyncCanceled()V
    invoke-static {v0}, Lcom/sec/android/app/sns3/sync/sp/qzone/SnsQzSyncAdapterUserTimelineService;->access$400(Lcom/sec/android/app/sns3/sync/sp/qzone/SnsQzSyncAdapterUserTimelineService;)V

    .line 97
    return-void
.end method
