.class public final Lcom/sec/android/app/sns3/Manifest$permission;
.super Ljava/lang/Object;
.source "Manifest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/Manifest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "permission"
.end annotation


# static fields
.field public static final READ_SNS_DB:Ljava/lang/String; = "com.sec.android.app.sns3.permission.READ_SNS_DB"

.field public static final RECEIVE_FOURSQUARE_BROADCAST:Ljava/lang/String; = "com.sec.android.app.sns3.permission.RECEIVE_FOURSQUARE_BROADCAST"

.field public static final RECEIVE_LINKEDIN_BROADCAST:Ljava/lang/String; = "com.sec.android.app.sns3.permission.RECEIVE_LINKEDIN_BROADCAST"

.field public static final RECEIVE_SNS_BROADCAST:Ljava/lang/String; = "com.sec.android.app.sns3.permission.RECEIVE_SNS_BROADCAST"

.field public static final REQUEST_PEOPLE_LOOKUP:Ljava/lang/String; = "com.sec.android.app.sns3.permission.REQUEST_PEOPLE_LOOKUP"

.field public static final SNS_FB_ACCESS_TOKEN:Ljava/lang/String; = "com.sec.android.app.sns3.permission.SNS_FB_ACCESS_TOKEN"

.field public static final SNS_FS_ACCESS_API:Ljava/lang/String; = "com.sec.android.app.sns3.permission.SNS_FS_ACCESS_API"

.field public static final SNS_FS_ACCESS_TOKEN:Ljava/lang/String; = "com.sec.android.app.sns3.permission.SNS_FS_ACCESS_TOKEN"

.field public static final SNS_GP_ACCESS_API:Ljava/lang/String; = "com.sec.android.app.sns3.permission.SNS_GP_ACCESS_API"

.field public static final SNS_GP_ACCESS_TOKEN:Ljava/lang/String; = "com.sec.android.app.sns3.permission.SNS_GP_ACCESS_TOKEN"

.field public static final SNS_TW_ACCESS_TOKEN:Ljava/lang/String; = "com.sec.android.app.sns3.permission.SNS_TW_ACCESS_TOKEN"

.field public static final WRITE_SNS_DB:Ljava/lang/String; = "com.sec.android.app.sns3.permission.WRITE_SNS_DB"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
