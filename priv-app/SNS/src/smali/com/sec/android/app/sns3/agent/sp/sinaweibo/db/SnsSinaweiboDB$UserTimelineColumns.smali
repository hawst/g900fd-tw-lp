.class public interface abstract Lcom/sec/android/app/sns3/agent/sp/sinaweibo/db/SnsSinaweiboDB$UserTimelineColumns;
.super Ljava/lang/Object;
.source "SnsSinaweiboDB.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/agent/sp/sinaweibo/db/SnsSinaweiboDB;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "UserTimelineColumns"
.end annotation


# static fields
.field public static final CAPTION:Ljava/lang/String; = "caption"

.field public static final CREATE_TIME:Ljava/lang/String; = "create_time"

.field public static final DESCRIPTION:Ljava/lang/String; = "description"

.field public static final FEED_ID:Ljava/lang/String; = "post_id"

.field public static final FROM_NAME:Ljava/lang/String; = "from_name"

.field public static final ICON:Ljava/lang/String; = "icon"

.field public static final MESSAGE:Ljava/lang/String; = "message"

.field public static final PICTURE:Ljava/lang/String; = "picture"

.field public static final TYPE:Ljava/lang/String; = "type"

.field public static final UPDATED_TIME:Ljava/lang/String; = "updated_time"
