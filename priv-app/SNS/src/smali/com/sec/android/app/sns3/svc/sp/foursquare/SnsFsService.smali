.class public Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService;
.super Landroid/app/Service;
.source "SnsFsService.java"


# static fields
.field protected static final TAG:Ljava/lang/String; = "SnsFsService"


# instance fields
.field private final mSnsSvcFourSquareBinder:Lcom/sec/android/app/sns3/svc/sp/foursquare/api/ISnsFourSquare$Stub;

.field private mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 65
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 93
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService$1;-><init>(Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService;)V

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService;->mSnsSvcFourSquareBinder:Lcom/sec/android/app/sns3/svc/sp/foursquare/api/ISnsFourSquare$Stub;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService;)Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 65
    invoke-direct {p0}, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService;->ensureAccountAdded()V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService;Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/foursquare/response/Category;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 65
    invoke-direct {p0, p1}, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService;->findCategoryById(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/foursquare/response/Category;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService;Lcom/sec/android/app/sns3/svc/sp/foursquare/api/ISnsFourSquareCallbackCategory;Ljava/lang/String;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService;
    .param p1, "x1"    # Lcom/sec/android/app/sns3/svc/sp/foursquare/api/ISnsFourSquareCallbackCategory;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 65
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService;->getCategories(Lcom/sec/android/app/sns3/svc/sp/foursquare/api/ISnsFourSquareCallbackCategory;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method private ensureAccountAdded()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 429
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v1

    invoke-static {v1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    const-string v2, "com.sec.android.app.sns3.foursquare"

    invoke-virtual {v1, v2}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 431
    .local v0, "fsAccounts":[Landroid/accounts/Account;
    array-length v1, v0

    if-gtz v1, :cond_0

    .line 432
    new-instance v1, Landroid/os/RemoteException;

    const-string v2, "Foursquare Account not added in the device. Please add to use this service"

    invoke-direct {v1, v2}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 434
    :cond_0
    return-void
.end method

.method private findCategoryById(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/foursquare/response/Category;
    .locals 8
    .param p1, "categoryId"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 406
    new-instance v6, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/Category;

    invoke-direct {v6}, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/Category;-><init>()V

    .line 407
    .local v6, "category":Lcom/sec/android/app/sns3/svc/sp/foursquare/response/Category;
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 408
    .local v0, "cr":Landroid/content/ContentResolver;
    sget-object v1, Lcom/sec/android/app/sns3/agent/sp/foursquare/db/SnsFourSquareDB$VenuesCategories;->CONTENT_URI:Landroid/net/Uri;

    const-string v3, "category_id =?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 412
    .local v7, "cursor":Landroid/database/Cursor;
    if-eqz v7, :cond_0

    .line 413
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 414
    const-string v1, "category_id"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v6, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/Category;->mId:Ljava/lang/String;

    .line 415
    const-string v1, "category_name"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v6, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/Category;->mName:Ljava/lang/String;

    .line 416
    const-string v1, "category_plural_name"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v6, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/Category;->mPluralName:Ljava/lang/String;

    .line 418
    const-string v1, "category_icon"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v6, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/Category;->mIconUrl:Ljava/lang/String;

    .line 419
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_0
    move-object v2, v6

    .line 425
    :goto_0
    return-object v2

    .line 421
    :cond_1
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method

.method private getCategories(Lcom/sec/android/app/sns3/svc/sp/foursquare/api/ISnsFourSquareCallbackCategory;Ljava/lang/String;)I
    .locals 7
    .param p1, "cb"    # Lcom/sec/android/app/sns3/svc/sp/foursquare/api/ISnsFourSquareCallbackCategory;
    .param p2, "categoryId"    # Ljava/lang/String;

    .prologue
    .line 346
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    .line 348
    .local v6, "cr":Landroid/content/ContentResolver;
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService$2;

    iget-object v2, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    const/4 v3, 0x0

    move-object v1, p0

    move-object v4, p2

    move-object v5, p1

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService$2;-><init>(Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Bundle;Ljava/lang/String;Lcom/sec/android/app/sns3/svc/sp/foursquare/api/ISnsFourSquareCallbackCategory;Landroid/content/ContentResolver;)V

    .line 400
    .local v0, "req":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->request()Z

    .line 401
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->getReqID()I

    move-result v1

    return v1
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 81
    const-string v0, "SNS"

    const-string v1, "SnsFsService : onBind()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 83
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService;->mSnsSvcFourSquareBinder:Lcom/sec/android/app/sns3/svc/sp/foursquare/api/ISnsFourSquare$Stub;

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 73
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 74
    const-string v0, "SNS"

    const-string v1, "SnsFsService : onCreate()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 76
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/SnsApplication;->getSvcMgr()Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    .line 77
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 88
    const-string v0, "SNS"

    const-string v1, "SnsFbService : onStartCommand()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 90
    const/4 v0, 0x1

    return v0
.end method
