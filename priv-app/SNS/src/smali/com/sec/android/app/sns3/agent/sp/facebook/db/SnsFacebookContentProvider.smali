.class public Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookContentProvider;
.super Landroid/content/ContentProvider;
.source "SnsFacebookContentProvider.java"


# static fields
.field private static final ALBUM:I = 0x320

.field private static final ALBUM_CONTENTS:I = 0xa28

.field private static final BIRTHDAY:I = 0x3e8

.field private static final COMMENT:I = 0x64

.field private static final DEBUG:I = 0xbb8

.field private static final DEBUG_OFF:I = 0xc80

.field private static final DEBUG_ON:I = 0xc1c

.field private static final DEBUG_URI:Ljava/lang/String; = "debug"

.field private static final EVENT:I = 0x2bc

.field private static final FEED_LIST:I = 0xc8

.field private static final FRIENDLIST_GROUP_MEMBER:I = 0x10cc

.field private static final FRIENDS:I = 0xfa0

.field private static final FRIENDS_EDU:I = 0x1068

.field private static final FRIENDS_WORK:I = 0x1004

.field private static final FRIEND_REQUEST_NOTIFICATION:I = 0x708

.field private static final GROUPS:I = 0x578

.field private static final LIKES:I = 0x12c

.field private static final MESSAGE_NOTIFICATION:I = 0x6a4

.field private static final NOTE:I = 0x5dc

.field private static final NOTIFICATION:I = 0x640

.field private static final PHOTO:I = 0x44c

.field private static final PHOTOS_OF_USER:I = 0x11f8

.field private static final PHOTO_STREAM:I = 0x514

.field private static final POST:I = 0x1f4

.field private static final PREVIOUS_SYNC_STATE:I = 0x8fc

.field private static final SSO_ACCOUNT:I = 0x2328

.field private static final STATUS_STREAM:I = 0x1194

.field private static final SYNC_ALBUM:I = 0x834

.field private static final SYNC_EVENT:I = 0x7d0

.field private static final SYNC_EVENT_OWNER_FROM_TO:I = 0x7da

.field private static final SYNC_GALLERY:I = 0x8ac

.field private static final SYNC_GALLERY_ALBUM_ID:I = 0x8b6

.field public static final SYNC_GALLERY_URI_NAME:Ljava/lang/String; = "sync_gallery"

.field private static final SYNC_PHOTO:I = 0x898

.field private static final SYNC_PHOTO_IMAGES:I = 0x8a2

.field private static final SYNC_TAGS:I = 0x8c0

.field private static TAG:Ljava/lang/String; = null

.field private static final TAGS:I = 0x4b0

.field private static final THREAD:I = 0x9c4

.field private static final URI_SSO_ACCOUNT:Ljava/lang/String; = "sso_account"

.field private static final USER:I = 0x258

.field private static final WIPE_FB_DATA:I = 0x960

.field public static final sImageTable:Ljava/lang/String; = "sync_photo LEFT OUTER JOIN sync_photo_images ON (sync_photo.id = sync_photo_images.target_id)"

.field private static sSyncImageProjectionMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final uriMatcher:Landroid/content/UriMatcher;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mOpenHelper:Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDBHelper;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    .line 73
    const-string v1, "SnsFacebookContentProvider"

    sput-object v1, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookContentProvider;->TAG:Ljava/lang/String;

    .line 1117
    new-instance v1, Landroid/content/UriMatcher;

    const/4 v2, -0x1

    invoke-direct {v1, v2}, Landroid/content/UriMatcher;-><init>(I)V

    sput-object v1, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    .line 1118
    sget-object v1, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v2, "com.sec.android.app.sns3.sp.facebook"

    const-string v3, "comments"

    const/16 v4, 0x64

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1119
    sget-object v1, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v2, "com.sec.android.app.sns3.sp.facebook"

    const-string v3, "feed_list"

    const/16 v4, 0xc8

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1120
    sget-object v1, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v2, "com.sec.android.app.sns3.sp.facebook"

    const-string v3, "likes"

    const/16 v4, 0x12c

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1121
    sget-object v1, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v2, "com.sec.android.app.sns3.sp.facebook"

    const-string v3, "post"

    const/16 v4, 0x1f4

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1122
    sget-object v1, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v2, "com.sec.android.app.sns3.sp.facebook"

    const-string v3, "event"

    const/16 v4, 0x2bc

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1123
    sget-object v1, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v2, "com.sec.android.app.sns3.sp.facebook"

    const-string v3, "user"

    const/16 v4, 0x258

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1124
    sget-object v1, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v2, "com.sec.android.app.sns3.sp.facebook"

    const-string v3, "friends"

    const/16 v4, 0xfa0

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1125
    sget-object v1, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v2, "com.sec.android.app.sns3.sp.facebook"

    const-string v3, "work"

    const/16 v4, 0x1004

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1126
    sget-object v1, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v2, "com.sec.android.app.sns3.sp.facebook"

    const-string v3, "education"

    const/16 v4, 0x1068

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1127
    sget-object v1, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v2, "com.sec.android.app.sns3.sp.facebook"

    const-string v3, "friendlist_group_members"

    const/16 v4, 0x10cc

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1128
    sget-object v1, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v2, "com.sec.android.app.sns3.sp.facebook"

    const-string v3, "birthday"

    const/16 v4, 0x3e8

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1129
    sget-object v1, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v2, "com.sec.android.app.sns3.sp.facebook"

    const-string v3, "album"

    const/16 v4, 0x320

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1130
    sget-object v1, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v2, "com.sec.android.app.sns3.sp.facebook"

    const-string v3, "photo"

    const/16 v4, 0x44c

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1131
    sget-object v1, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v2, "com.sec.android.app.sns3.sp.facebook"

    const-string v3, "notification_query"

    const/16 v4, 0x640

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1133
    sget-object v1, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v2, "com.sec.android.app.sns3.sp.facebook"

    const-string v3, "message_query"

    const/16 v4, 0x6a4

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1135
    sget-object v1, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v2, "com.sec.android.app.sns3.sp.facebook"

    const-string v3, "friend_request_query"

    const/16 v4, 0x708

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1137
    sget-object v1, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v2, "com.sec.android.app.sns3.sp.facebook"

    const-string v3, "tags"

    const/16 v4, 0x4b0

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1138
    sget-object v1, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v2, "com.sec.android.app.sns3.sp.facebook"

    const-string v3, "photo_stream"

    const/16 v4, 0x514

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1140
    sget-object v1, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v2, "com.sec.android.app.sns3.sp.facebook"

    const-string v3, "groups"

    const/16 v4, 0x578

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1141
    sget-object v1, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v2, "com.sec.android.app.sns3.sp.facebook"

    const-string v3, "note"

    const/16 v4, 0x5dc

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1142
    sget-object v1, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v2, "com.sec.android.app.sns3.sp.facebook"

    const-string v3, "sync_event"

    const/16 v4, 0x7d0

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1144
    sget-object v1, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v2, "com.sec.android.app.sns3.sp.facebook"

    const-string v3, "sync_album"

    const/16 v4, 0x834

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1145
    sget-object v1, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v2, "com.sec.android.app.sns3.sp.facebook"

    const-string v3, "sync_photo"

    const/16 v4, 0x898

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1146
    sget-object v1, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v2, "com.sec.android.app.sns3.sp.facebook"

    const-string v3, "photos_of_user"

    const/16 v4, 0x11f8

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1147
    sget-object v1, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v2, "com.sec.android.app.sns3.sp.facebook"

    const-string v3, "sync_photo_images"

    const/16 v4, 0x8a2

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1149
    sget-object v1, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v2, "com.sec.android.app.sns3.sp.facebook"

    const-string v3, "sync_gallery"

    const/16 v4, 0x8ac

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1150
    sget-object v1, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v2, "com.sec.android.app.sns3.sp.facebook"

    const-string v3, "sync_gallery/album_id/#"

    const/16 v4, 0x8b6

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1152
    sget-object v1, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v2, "com.sec.android.app.sns3.sp.facebook"

    const-string v3, "sync_tags"

    const/16 v4, 0x8c0

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1153
    sget-object v1, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v2, "com.sec.android.app.sns3.sp.facebook"

    const-string v3, "previous_sync_state"

    const/16 v4, 0x8fc

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1155
    sget-object v1, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v2, "com.sec.android.app.sns3.sp.facebook"

    const-string v3, "sync_event/from/#/to/#"

    const/16 v4, 0x7da

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1157
    sget-object v1, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v2, "com.sec.android.app.sns3.sp.facebook"

    const-string v3, "wipe_fb_data"

    const/16 v4, 0x960

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1159
    sget-object v1, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v2, "com.sec.android.app.sns3.sp.facebook"

    const-string v3, "thread"

    const/16 v4, 0x9c4

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1160
    sget-object v1, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v2, "com.sec.android.app.sns3.sp.facebook"

    const-string v3, "album_contents"

    const/16 v4, 0xa28

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1162
    sget-object v1, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v2, "com.sec.android.app.sns3.sp.facebook"

    const-string v3, "debug"

    const/16 v4, 0xbb8

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1163
    sget-object v1, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v2, "com.sec.android.app.sns3.sp.facebook"

    const-string v3, "debug/on"

    const/16 v4, 0xc1c

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1164
    sget-object v1, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v2, "com.sec.android.app.sns3.sp.facebook"

    const-string v3, "debug/off"

    const/16 v4, 0xc80

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1165
    sget-object v1, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v2, "com.sec.android.app.sns3.sp.facebook"

    const-string v3, "status_stream"

    const/16 v4, 0x1194

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1166
    sget-object v1, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v2, "com.sec.android.app.sns3.sp.facebook"

    const-string v3, "sso_account"

    const/16 v4, 0x2328

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1169
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 1170
    .local v0, "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v1, "id"

    const-string v2, "sync_photo.id as id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1172
    const-string v1, "target_id"

    const-string v2, "sync_photo.target_id as target_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1174
    const-string v1, "from_id"

    const-string v2, "sync_photo.from_id as from_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1176
    const-string v1, "from_name"

    const-string v2, "sync_photo.from_name as from_name"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1178
    const-string v1, "name"

    const-string v2, "sync_photo.name"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1180
    const-string v1, "picture"

    const-string v2, "sync_photo.picture"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1182
    const-string v1, "source"

    const-string v2, "sync_photo.source"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1184
    const-string v1, "height"

    const-string v2, "sync_photo.height"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1186
    const-string v1, "width"

    const-string v2, "sync_photo.width"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1188
    const-string v1, "link"

    const-string v2, "sync_photo.link"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1190
    const-string v1, "icon"

    const-string v2, "sync_photo.icon"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1192
    const-string v1, "position"

    const-string v2, "sync_photo.position"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1194
    const-string v1, "created_time"

    const-string v2, "sync_photo.created_time"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1196
    const-string v1, "updated_time"

    const-string v2, "sync_photo.updated_time"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1198
    const-string v1, "location_name"

    const-string v2, "sync_photo.location_name"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1200
    const-string v1, "latitude"

    const-string v2, "sync_photo.latitude"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1202
    const-string v1, "longitude"

    const-string v2, "sync_photo.longitude"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1204
    const-string v1, "likes_count"

    const-string v2, "sync_photo.likes_count"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1206
    const-string v1, "comments_count"

    const-string v2, "sync_photo.comments_count"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1208
    const-string v1, "_is_valid"

    const-string v2, "sync_photo._is_valid as _is_valid"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1210
    const-string v1, "target_photo_id"

    const-string v2, "sync_photo_images.target_id as target_photo_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1212
    const-string v1, "image_width"

    const-string v2, "sync_photo_images.width as image_width"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1214
    const-string v1, "image_height"

    const-string v2, "sync_photo_images.height as image_height"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1216
    const-string v1, "url"

    const-string v2, "sync_photo_images.url as url"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1218
    sput-object v0, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookContentProvider;->sSyncImageProjectionMap:Ljava/util/HashMap;

    .line 1219
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 71
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    .line 75
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookContentProvider;->mOpenHelper:Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDBHelper;

    return-void
.end method


# virtual methods
.method public bulkInsert(Landroid/net/Uri;[Landroid/content/ContentValues;)I
    .locals 4
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # [Landroid/content/ContentValues;

    .prologue
    .line 722
    const/4 v0, 0x0

    .line 723
    .local v0, "cnt":I
    array-length v2, p2

    .line 725
    .local v2, "numValues":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_1

    .line 726
    aget-object v3, p2, v1

    invoke-virtual {p0, p1, v3}, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookContentProvider;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v3

    if-eqz v3, :cond_0

    add-int/lit8 v0, v0, 0x1

    .line 725
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 728
    :cond_1
    return v0
.end method

.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 6
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "selection"    # Ljava/lang/String;
    .param p3, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 734
    const/4 v1, 0x0

    .line 735
    .local v1, "db":Landroid/database/sqlite/SQLiteDatabase;
    sget-object v3, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v3, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v2

    .line 736
    .local v2, "match":I
    const/4 v0, -0x1

    .line 738
    .local v0, "count":I
    iget-object v3, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookContentProvider;->mOpenHelper:Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDBHelper;

    invoke-virtual {v3}, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDBHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 740
    sparse-switch v2, :sswitch_data_0

    .line 922
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Unknown URI : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 742
    :sswitch_0
    const-string v3, "comments"

    invoke-static {v1, v3, p2, p3}, Lcom/sec/android/app/sns3/agent/db/SnsDBWrapper;->onDelete(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 924
    :goto_0
    iget-object v3, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookContentProvider;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, p1, v4}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 925
    return v0

    .line 748
    :sswitch_1
    const-string v3, "feed_list"

    invoke-static {v1, v3, p2, p3}, Lcom/sec/android/app/sns3/agent/db/SnsDBWrapper;->onDelete(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 751
    goto :goto_0

    .line 753
    :sswitch_2
    const-string v3, "album_contents"

    invoke-static {v1, v3, p2, p3}, Lcom/sec/android/app/sns3/agent/db/SnsDBWrapper;->onDelete(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 756
    goto :goto_0

    .line 759
    :sswitch_3
    const-string v3, "likes"

    invoke-static {v1, v3, p2, p3}, Lcom/sec/android/app/sns3/agent/db/SnsDBWrapper;->onDelete(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 762
    goto :goto_0

    .line 765
    :sswitch_4
    const-string v3, "post"

    invoke-static {v1, v3, p2, p3}, Lcom/sec/android/app/sns3/agent/db/SnsDBWrapper;->onDelete(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 768
    goto :goto_0

    .line 771
    :sswitch_5
    const-string v3, "event"

    invoke-static {v1, v3, p2, p3}, Lcom/sec/android/app/sns3/agent/db/SnsDBWrapper;->onDelete(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 774
    goto :goto_0

    .line 777
    :sswitch_6
    const-string v3, "user"

    invoke-static {v1, v3, p2, p3}, Lcom/sec/android/app/sns3/agent/db/SnsDBWrapper;->onDelete(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 780
    goto :goto_0

    .line 783
    :sswitch_7
    const-string v3, "friends"

    invoke-static {v1, v3, p2, p3}, Lcom/sec/android/app/sns3/agent/db/SnsDBWrapper;->onDelete(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 786
    goto :goto_0

    .line 788
    :sswitch_8
    const-string v3, "work"

    invoke-static {v1, v3, p2, p3}, Lcom/sec/android/app/sns3/agent/db/SnsDBWrapper;->onDelete(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 791
    goto :goto_0

    .line 793
    :sswitch_9
    const-string v3, "education"

    invoke-static {v1, v3, p2, p3}, Lcom/sec/android/app/sns3/agent/db/SnsDBWrapper;->onDelete(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 796
    goto :goto_0

    .line 798
    :sswitch_a
    const-string v3, "friendlist_group_members"

    invoke-static {v1, v3, p2, p3}, Lcom/sec/android/app/sns3/agent/db/SnsDBWrapper;->onDelete(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 801
    goto :goto_0

    .line 804
    :sswitch_b
    const-string v3, "birthday"

    invoke-static {v1, v3, p2, p3}, Lcom/sec/android/app/sns3/agent/db/SnsDBWrapper;->onDelete(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 807
    goto :goto_0

    .line 810
    :sswitch_c
    const-string v3, "notification_query"

    invoke-static {v1, v3, p2, p3}, Lcom/sec/android/app/sns3/agent/db/SnsDBWrapper;->onDelete(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 813
    goto :goto_0

    .line 816
    :sswitch_d
    const-string v3, "message_query"

    invoke-static {v1, v3, p2, p3}, Lcom/sec/android/app/sns3/agent/db/SnsDBWrapper;->onDelete(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 819
    goto :goto_0

    .line 822
    :sswitch_e
    const-string v3, "friend_request_query"

    invoke-static {v1, v3, p2, p3}, Lcom/sec/android/app/sns3/agent/db/SnsDBWrapper;->onDelete(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 826
    goto :goto_0

    .line 829
    :sswitch_f
    const-string v3, "album"

    invoke-static {v1, v3, p2, p3}, Lcom/sec/android/app/sns3/agent/db/SnsDBWrapper;->onDelete(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 832
    goto :goto_0

    .line 835
    :sswitch_10
    const-string v3, "photo"

    invoke-static {v1, v3, p2, p3}, Lcom/sec/android/app/sns3/agent/db/SnsDBWrapper;->onDelete(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 838
    goto :goto_0

    .line 841
    :sswitch_11
    const-string v3, "tags"

    invoke-static {v1, v3, p2, p3}, Lcom/sec/android/app/sns3/agent/db/SnsDBWrapper;->onDelete(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 844
    goto/16 :goto_0

    .line 847
    :sswitch_12
    const-string v3, "photo_stream"

    invoke-static {v1, v3, p2, p3}, Lcom/sec/android/app/sns3/agent/db/SnsDBWrapper;->onDelete(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 850
    goto/16 :goto_0

    .line 853
    :sswitch_13
    const-string v3, "groups"

    invoke-static {v1, v3, p2, p3}, Lcom/sec/android/app/sns3/agent/db/SnsDBWrapper;->onDelete(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 856
    goto/16 :goto_0

    .line 859
    :sswitch_14
    const-string v3, "note"

    invoke-static {v1, v3, p2, p3}, Lcom/sec/android/app/sns3/agent/db/SnsDBWrapper;->onDelete(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 862
    goto/16 :goto_0

    .line 865
    :sswitch_15
    const-string v3, "sync_event"

    invoke-static {v1, v3, p2, p3}, Lcom/sec/android/app/sns3/agent/db/SnsDBWrapper;->onDelete(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 868
    goto/16 :goto_0

    .line 871
    :sswitch_16
    const-string v3, "sync_album"

    invoke-static {v1, v3, p2, p3}, Lcom/sec/android/app/sns3/agent/db/SnsDBWrapper;->onDelete(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 874
    goto/16 :goto_0

    .line 877
    :sswitch_17
    const-string v3, "sync_photo"

    invoke-static {v1, v3, p2, p3}, Lcom/sec/android/app/sns3/agent/db/SnsDBWrapper;->onDelete(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 880
    goto/16 :goto_0

    .line 883
    :sswitch_18
    const-string v3, "photos_of_user"

    invoke-static {v1, v3, p2, p3}, Lcom/sec/android/app/sns3/agent/db/SnsDBWrapper;->onDelete(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 886
    goto/16 :goto_0

    .line 889
    :sswitch_19
    const-string v3, "sync_photo_images"

    invoke-static {v1, v3, p2, p3}, Lcom/sec/android/app/sns3/agent/db/SnsDBWrapper;->onDelete(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 892
    goto/16 :goto_0

    .line 895
    :sswitch_1a
    const-string v3, "sync_tags"

    invoke-static {v1, v3, p2, p3}, Lcom/sec/android/app/sns3/agent/db/SnsDBWrapper;->onDelete(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 898
    goto/16 :goto_0

    .line 901
    :sswitch_1b
    const-string v3, "previous_sync_state"

    invoke-static {v1, v3, p2, p3}, Lcom/sec/android/app/sns3/agent/db/SnsDBWrapper;->onDelete(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 904
    goto/16 :goto_0

    .line 907
    :sswitch_1c
    const-string v3, "thread"

    invoke-static {v1, v3, p2, p3}, Lcom/sec/android/app/sns3/agent/db/SnsDBWrapper;->onDelete(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 910
    goto/16 :goto_0

    .line 913
    :sswitch_1d
    iget-object v3, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookContentProvider;->mOpenHelper:Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDBHelper;

    invoke-virtual {v3, v1}, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDBHelper;->wipeData(Landroid/database/sqlite/SQLiteDatabase;)V

    goto/16 :goto_0

    .line 917
    :sswitch_1e
    const-string v3, "status_stream"

    invoke-static {v1, v3, p2, p3}, Lcom/sec/android/app/sns3/agent/db/SnsDBWrapper;->onDelete(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 920
    goto/16 :goto_0

    .line 740
    :sswitch_data_0
    .sparse-switch
        0x64 -> :sswitch_0
        0xc8 -> :sswitch_1
        0x12c -> :sswitch_3
        0x1f4 -> :sswitch_4
        0x258 -> :sswitch_6
        0x2bc -> :sswitch_5
        0x320 -> :sswitch_f
        0x3e8 -> :sswitch_b
        0x44c -> :sswitch_10
        0x4b0 -> :sswitch_11
        0x514 -> :sswitch_12
        0x578 -> :sswitch_13
        0x5dc -> :sswitch_14
        0x640 -> :sswitch_c
        0x6a4 -> :sswitch_d
        0x708 -> :sswitch_e
        0x7d0 -> :sswitch_15
        0x834 -> :sswitch_16
        0x898 -> :sswitch_17
        0x8a2 -> :sswitch_19
        0x8c0 -> :sswitch_1a
        0x8fc -> :sswitch_1b
        0x960 -> :sswitch_1d
        0x9c4 -> :sswitch_1c
        0xa28 -> :sswitch_2
        0xfa0 -> :sswitch_7
        0x1004 -> :sswitch_8
        0x1068 -> :sswitch_9
        0x10cc -> :sswitch_a
        0x1194 -> :sswitch_1e
        0x11f8 -> :sswitch_18
    .end sparse-switch
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 4
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 416
    sget-object v1, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v1, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 547
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown URI : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 418
    :sswitch_0
    const-string v0, "vnd.android.cursor.dir/vnd.com.sec.android.app.sns3.sp.facebook.comments"

    .line 542
    :cond_0
    :goto_0
    return-object v0

    .line 422
    :sswitch_1
    const-string v0, "vnd.android.cursor.dir/vnd.com.sec.android.app.sns3.sp.facebook.feedlist"

    goto :goto_0

    .line 425
    :sswitch_2
    const-string v0, "vnd.android.cursor.dir/vnd.com.sec.android.app.sns3.sp.facebook.albumcontents"

    goto :goto_0

    .line 429
    :sswitch_3
    const-string v0, "vnd.android.cursor.dir/vnd.com.sec.android.app.sns3.sp.facebook.likes"

    goto :goto_0

    .line 433
    :sswitch_4
    const-string v0, "vnd.android.cursor.dir/vnd.com.sec.android.app.sns3.sp.facebook.post"

    goto :goto_0

    .line 437
    :sswitch_5
    const-string v0, "vnd.android.cursor.dir/vnd.com.sec.android.app.sns3.sp.facebook.event"

    goto :goto_0

    .line 441
    :sswitch_6
    const-string v0, "vnd.android.cursor.dir/vnd.com.sec.android.app.sns3.sp.facebook.user"

    goto :goto_0

    .line 445
    :sswitch_7
    const-string v0, "vnd.android.cursor.dir/vnd.com.sec.android.app.sns3.sp.facebook.friends"

    goto :goto_0

    .line 449
    :sswitch_8
    const-string v0, "vnd.android.cursor.dir/vnd.com.sec.android.app.sns3.sp.facebook.work"

    goto :goto_0

    .line 453
    :sswitch_9
    const-string v0, "vnd.android.cursor.dir/vnd.com.sec.android.app.sns3.sp.facebook.education"

    goto :goto_0

    .line 457
    :sswitch_a
    const-string v0, "vnd.android.cursor.dir/vnd.com.sec.android.app.sns3.sp.facebook.frndlistmembers"

    goto :goto_0

    .line 462
    :sswitch_b
    const-string v0, "vnd.android.cursor.dir/vnd.com.sec.android.app.sns3.sp.facebook.birthday"

    goto :goto_0

    .line 466
    :sswitch_c
    const-string v0, "vnd.android.cursor.dir/vnd.com.sec.android.app.sns3.sp.facebook.photo"

    goto :goto_0

    .line 470
    :sswitch_d
    const-string v0, "vnd.android.cursor.dir/vnd.com.sec.android.app.sns3.sp.facebook.notifications"

    goto :goto_0

    .line 474
    :sswitch_e
    const-string v0, "vnd.android.cursor.dir/vnd.com.sec.android.app.sns3.sp.facebook.messagenotifications"

    goto :goto_0

    .line 478
    :sswitch_f
    const-string v0, "vnd.android.cursor.dir/vnd.com.sec.android.app.sns3.sp.facebook.friendrequestnotifications"

    goto :goto_0

    .line 482
    :sswitch_10
    const-string v0, "vnd.android.cursor.dir/vnd.com.sec.android.app.sns3.sp.facebook.tags"

    goto :goto_0

    .line 486
    :sswitch_11
    const-string v0, "vnd.android.cursor.dir/vnd.com.sec.android.app.sns3.sp.facebook.photostream"

    goto :goto_0

    .line 490
    :sswitch_12
    const-string v0, "vnd.android.cursor.dir/vnd.com.sec.android.app.sns3.sp.facebook.group"

    goto :goto_0

    .line 494
    :sswitch_13
    const-string v0, "vnd.android.cursor.dir/vnd.com.sec.android.app.sns3.sp.facebook.syncevent"

    goto :goto_0

    .line 498
    :sswitch_14
    const-string v0, "vnd.android.cursor.dir/vnd.com.sec.android.app.sns3.sp.facebook.syncalbum"

    goto :goto_0

    .line 502
    :sswitch_15
    const-string v0, "vnd.android.cursor.dir/vnd.com.sec.android.app.sns3.sp.facebook.syncphoto"

    goto :goto_0

    .line 506
    :sswitch_16
    const-string v0, "vnd.android.cursor.dir/vnd.com.sec.android.app.sns3.sp.facebook.photosofuser"

    goto :goto_0

    .line 510
    :sswitch_17
    const-string v0, "vnd.android.cursor.dir/vnd.com.sec.android.app.sns3.sp.facebook.syncphotoimages"

    goto :goto_0

    .line 514
    :sswitch_18
    const-string v0, "vnd.android.cursor.dir/vnd.com.sec.android.app.sns3.sp.facebook.synctags"

    goto :goto_0

    .line 518
    :sswitch_19
    const-string v0, "vnd.android.cursor.dir/vnd.com.sec.android.app.sns3.sp.facebook.previoussyncstate"

    goto :goto_0

    .line 522
    :sswitch_1a
    const-string v0, "vnd.android.cursor.dir/vnd.com.sec.android.app.sns3.sp.facebook.thread"

    goto :goto_0

    .line 526
    :sswitch_1b
    const-string v0, "vnd.android.cursor.dir/vnd.com.sec.android.app.sns3.sp.facebook.status_stream"

    goto :goto_0

    .line 530
    :sswitch_1c
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookContentProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->initDebugKey(Landroid/content/Context;)V

    .line 532
    const-string v0, "DEBUG_OFF"

    .line 534
    .local v0, "debug_str":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->isLoggable()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 535
    const-string v0, "DEBUG_ON"

    goto :goto_0

    .line 542
    .end local v0    # "debug_str":Ljava/lang/String;
    :sswitch_1d
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "com.facebook.auth.login/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookContentProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f080020

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 416
    :sswitch_data_0
    .sparse-switch
        0x64 -> :sswitch_0
        0xc8 -> :sswitch_1
        0x12c -> :sswitch_3
        0x1f4 -> :sswitch_4
        0x258 -> :sswitch_6
        0x2bc -> :sswitch_5
        0x3e8 -> :sswitch_b
        0x44c -> :sswitch_c
        0x4b0 -> :sswitch_10
        0x514 -> :sswitch_11
        0x578 -> :sswitch_12
        0x640 -> :sswitch_d
        0x6a4 -> :sswitch_e
        0x708 -> :sswitch_f
        0x7d0 -> :sswitch_13
        0x834 -> :sswitch_14
        0x898 -> :sswitch_15
        0x8a2 -> :sswitch_17
        0x8c0 -> :sswitch_18
        0x8fc -> :sswitch_19
        0x9c4 -> :sswitch_1a
        0xa28 -> :sswitch_2
        0xbb8 -> :sswitch_1c
        0xfa0 -> :sswitch_7
        0x1004 -> :sswitch_8
        0x1068 -> :sswitch_9
        0x10cc -> :sswitch_a
        0x1194 -> :sswitch_1b
        0x11f8 -> :sswitch_16
        0x2328 -> :sswitch_1d
    .end sparse-switch
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 9
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;

    .prologue
    const/4 v3, 0x0

    .line 554
    const/4 v0, 0x0

    .line 555
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    sget-object v6, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v6, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v2

    .line 556
    .local v2, "match":I
    const-wide/16 v4, -0x1

    .line 559
    .local v4, "rowId":J
    :try_start_0
    iget-object v6, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookContentProvider;->mOpenHelper:Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDBHelper;

    invoke-virtual {v6}, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDBHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 561
    sparse-switch v2, :sswitch_data_0

    .line 709
    new-instance v6, Ljava/lang/IllegalArgumentException;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Unknown URI : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v6
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 712
    :catch_0
    move-exception v1

    .line 713
    .local v1, "e":Landroid/database/sqlite/SQLiteException;
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    .line 716
    .end local v1    # "e":Landroid/database/sqlite/SQLiteException;
    :goto_0
    const-wide/16 v6, -0x1

    cmp-long v6, v4, v6

    if-nez v6, :cond_0

    move-object p1, v3

    .end local p1    # "uri":Landroid/net/Uri;
    :cond_0
    return-object p1

    .line 563
    .restart local p1    # "uri":Landroid/net/Uri;
    :sswitch_0
    :try_start_1
    const-string v6, "comments"

    const/4 v7, 0x0

    invoke-virtual {v0, v6, v7, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v4

    .line 711
    :goto_1
    iget-object v6, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookContentProvider;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v6, p1, v7}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    goto :goto_0

    .line 568
    :sswitch_1
    const-string v6, "feed_list"

    const/4 v7, 0x0

    invoke-virtual {v0, v6, v7, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v4

    .line 570
    goto :goto_1

    .line 572
    :sswitch_2
    const-string v6, "album_contents"

    const/4 v7, 0x0

    invoke-virtual {v0, v6, v7, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v4

    .line 574
    goto :goto_1

    .line 577
    :sswitch_3
    const-string v6, "likes"

    const/4 v7, 0x0

    invoke-virtual {v0, v6, v7, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v4

    .line 579
    goto :goto_1

    .line 582
    :sswitch_4
    const-string v6, "post"

    const/4 v7, 0x0

    invoke-virtual {v0, v6, v7, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v4

    .line 584
    goto :goto_1

    .line 587
    :sswitch_5
    const-string v6, "event"

    const/4 v7, 0x0

    invoke-virtual {v0, v6, v7, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v4

    .line 589
    goto :goto_1

    .line 592
    :sswitch_6
    const-string v6, "user"

    const/4 v7, 0x0

    invoke-virtual {v0, v6, v7, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v4

    .line 594
    goto :goto_1

    .line 597
    :sswitch_7
    const-string v6, "friends"

    const/4 v7, 0x0

    invoke-virtual {v0, v6, v7, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v4

    .line 599
    goto :goto_1

    .line 601
    :sswitch_8
    const-string v6, "work"

    const/4 v7, 0x0

    invoke-virtual {v0, v6, v7, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v4

    .line 603
    goto :goto_1

    .line 605
    :sswitch_9
    const-string v6, "education"

    const/4 v7, 0x0

    invoke-virtual {v0, v6, v7, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v4

    .line 607
    goto :goto_1

    .line 609
    :sswitch_a
    const-string v6, "friendlist_group_members"

    const/4 v7, 0x0

    invoke-virtual {v0, v6, v7, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v4

    .line 611
    goto :goto_1

    .line 614
    :sswitch_b
    const-string v6, "birthday"

    const/4 v7, 0x0

    invoke-virtual {v0, v6, v7, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v4

    .line 616
    goto :goto_1

    .line 619
    :sswitch_c
    const-string v6, "notification_query"

    const/4 v7, 0x0

    invoke-virtual {v0, v6, v7, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v4

    .line 621
    goto :goto_1

    .line 624
    :sswitch_d
    const-string v6, "message_query"

    const/4 v7, 0x0

    invoke-virtual {v0, v6, v7, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v4

    .line 626
    goto :goto_1

    .line 629
    :sswitch_e
    const-string v6, "friend_request_query"

    const/4 v7, 0x0

    invoke-virtual {v0, v6, v7, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v4

    .line 632
    goto :goto_1

    .line 635
    :sswitch_f
    const-string v6, "album"

    const/4 v7, 0x0

    invoke-virtual {v0, v6, v7, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v4

    .line 637
    goto/16 :goto_1

    .line 640
    :sswitch_10
    const-string v6, "photo"

    const/4 v7, 0x0

    invoke-virtual {v0, v6, v7, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v4

    .line 642
    goto/16 :goto_1

    .line 645
    :sswitch_11
    const-string v6, "tags"

    const/4 v7, 0x0

    invoke-virtual {v0, v6, v7, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v4

    .line 647
    goto/16 :goto_1

    .line 650
    :sswitch_12
    const-string v6, "photo_stream"

    const/4 v7, 0x0

    invoke-virtual {v0, v6, v7, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v4

    .line 652
    goto/16 :goto_1

    .line 655
    :sswitch_13
    const-string v6, "groups"

    const/4 v7, 0x0

    invoke-virtual {v0, v6, v7, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v4

    .line 657
    goto/16 :goto_1

    .line 660
    :sswitch_14
    const-string v6, "note"

    const/4 v7, 0x0

    invoke-virtual {v0, v6, v7, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v4

    .line 662
    goto/16 :goto_1

    .line 665
    :sswitch_15
    const-string v6, "sync_event"

    const/4 v7, 0x0

    invoke-virtual {v0, v6, v7, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v4

    .line 667
    goto/16 :goto_1

    .line 670
    :sswitch_16
    const-string v6, "sync_album"

    const/4 v7, 0x0

    invoke-virtual {v0, v6, v7, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v4

    .line 672
    goto/16 :goto_1

    .line 675
    :sswitch_17
    const-string v6, "sync_photo"

    const/4 v7, 0x0

    invoke-virtual {v0, v6, v7, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v4

    .line 677
    goto/16 :goto_1

    .line 680
    :sswitch_18
    const-string v6, "photos_of_user"

    const/4 v7, 0x0

    invoke-virtual {v0, v6, v7, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v4

    .line 682
    goto/16 :goto_1

    .line 685
    :sswitch_19
    const-string v6, "sync_photo_images"

    const/4 v7, 0x0

    invoke-virtual {v0, v6, v7, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v4

    .line 687
    goto/16 :goto_1

    .line 690
    :sswitch_1a
    const-string v6, "sync_tags"

    const/4 v7, 0x0

    invoke-virtual {v0, v6, v7, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v4

    .line 692
    goto/16 :goto_1

    .line 695
    :sswitch_1b
    const-string v6, "previous_sync_state"

    const/4 v7, 0x0

    invoke-virtual {v0, v6, v7, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v4

    .line 697
    goto/16 :goto_1

    .line 700
    :sswitch_1c
    const-string v6, "thread"

    const/4 v7, 0x0

    invoke-virtual {v0, v6, v7, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v4

    .line 702
    goto/16 :goto_1

    .line 704
    :sswitch_1d
    const-string v6, "status_stream"

    const/4 v7, 0x0

    invoke-virtual {v0, v6, v7, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-wide v4

    .line 706
    goto/16 :goto_1

    .line 561
    nop

    :sswitch_data_0
    .sparse-switch
        0x64 -> :sswitch_0
        0xc8 -> :sswitch_1
        0x12c -> :sswitch_3
        0x1f4 -> :sswitch_4
        0x258 -> :sswitch_6
        0x2bc -> :sswitch_5
        0x320 -> :sswitch_f
        0x3e8 -> :sswitch_b
        0x44c -> :sswitch_10
        0x4b0 -> :sswitch_11
        0x514 -> :sswitch_12
        0x578 -> :sswitch_13
        0x5dc -> :sswitch_14
        0x640 -> :sswitch_c
        0x6a4 -> :sswitch_d
        0x708 -> :sswitch_e
        0x7d0 -> :sswitch_15
        0x834 -> :sswitch_16
        0x898 -> :sswitch_17
        0x8a2 -> :sswitch_19
        0x8c0 -> :sswitch_1a
        0x8fc -> :sswitch_1b
        0x9c4 -> :sswitch_1c
        0xa28 -> :sswitch_2
        0xfa0 -> :sswitch_7
        0x1004 -> :sswitch_8
        0x1068 -> :sswitch_9
        0x10cc -> :sswitch_a
        0x1194 -> :sswitch_1d
        0x11f8 -> :sswitch_18
    .end sparse-switch
.end method

.method public onCreate()Z
    .locals 2

    .prologue
    .line 172
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookContentProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookContentProvider;->mContext:Landroid/content/Context;

    .line 173
    new-instance v0, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDBHelper;

    iget-object v1, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookContentProvider;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDBHelper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookContentProvider;->mOpenHelper:Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDBHelper;

    .line 174
    const/4 v0, 0x0

    return v0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 20
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "sortOrder"    # Ljava/lang/String;

    .prologue
    .line 180
    const/4 v3, 0x0

    .line 181
    .local v3, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v10, 0x0

    .line 183
    .local v10, "c":Landroid/database/Cursor;
    sget-object v4, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v14

    .line 185
    .local v14, "match":I
    const/4 v12, 0x0

    .line 186
    .local v12, "from":Ljava/lang/String;
    const/16 v17, 0x0

    .line 187
    .local v17, "to":Ljava/lang/String;
    const/4 v7, 0x0

    .line 189
    .local v7, "groupby":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookContentProvider;->mOpenHelper:Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDBHelper;

    invoke-virtual {v4}, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDBHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    .line 190
    new-instance v2, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v2}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 192
    .local v2, "queryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v16

    .line 194
    .local v16, "pathSegmentList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    sparse-switch v14, :sswitch_data_0

    .line 390
    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Unknown URI : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 196
    :sswitch_0
    const-string v4, "comments"

    invoke-virtual {v2, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 393
    :cond_0
    :goto_0
    if-eqz v12, :cond_1

    if-eqz v17, :cond_1

    .line 394
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    .line 395
    .local v15, "orderOffset":Ljava/lang/StringBuilder;
    move-object/from16 v0, p5

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 396
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " limit "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v15, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 397
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " offset "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v15, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 398
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p5

    .line 403
    .end local v15    # "orderOffset":Ljava/lang/StringBuilder;
    :cond_1
    const/4 v8, 0x0

    move-object/from16 v4, p2

    move-object/from16 v5, p3

    move-object/from16 v6, p4

    move-object/from16 v9, p5

    :try_start_0
    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v10

    .line 410
    :goto_1
    return-object v10

    .line 201
    :sswitch_1
    const-string v4, "feed_list"

    invoke-virtual {v2, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto :goto_0

    .line 205
    :sswitch_2
    const-string v4, "album_contents"

    invoke-virtual {v2, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto :goto_0

    .line 210
    :sswitch_3
    const-string v4, "likes"

    invoke-virtual {v2, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto :goto_0

    .line 215
    :sswitch_4
    const-string v4, "post"

    invoke-virtual {v2, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto :goto_0

    .line 220
    :sswitch_5
    const-string v4, "event"

    invoke-virtual {v2, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto :goto_0

    .line 225
    :sswitch_6
    const-string v4, "user"

    invoke-virtual {v2, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto :goto_0

    .line 229
    :sswitch_7
    const-string v4, "friends"

    invoke-virtual {v2, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto :goto_0

    .line 233
    :sswitch_8
    const-string v4, "work"

    invoke-virtual {v2, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto :goto_0

    .line 237
    :sswitch_9
    const-string v4, "education"

    invoke-virtual {v2, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 241
    :sswitch_a
    const-string v4, "friendlist_group_members"

    invoke-virtual {v2, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 245
    :sswitch_b
    const-string v4, "birthday"

    invoke-virtual {v2, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 246
    const-string p5, "birthday_date ASC"

    .line 248
    goto/16 :goto_0

    .line 251
    :sswitch_c
    const-string v4, "album"

    invoke-virtual {v2, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 256
    :sswitch_d
    const-string v4, "photo"

    invoke-virtual {v2, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 261
    :sswitch_e
    const-string v4, "tags"

    invoke-virtual {v2, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 266
    :sswitch_f
    const-string v4, "notification_query"

    invoke-virtual {v2, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 271
    :sswitch_10
    const-string v4, "message_query"

    invoke-virtual {v2, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 276
    :sswitch_11
    const-string v4, "friend_request_query"

    invoke-virtual {v2, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 281
    :sswitch_12
    const-string v4, "photo_stream"

    invoke-virtual {v2, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 286
    :sswitch_13
    const-string v4, "groups"

    invoke-virtual {v2, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 291
    :sswitch_14
    const-string v4, "note"

    invoke-virtual {v2, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 296
    :sswitch_15
    const-string v4, "sync_event"

    invoke-virtual {v2, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 301
    :sswitch_16
    const-string v4, "sync_event"

    invoke-virtual {v2, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 302
    const/4 v4, 0x2

    move-object/from16 v0, v16

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v12

    .end local v12    # "from":Ljava/lang/String;
    check-cast v12, Ljava/lang/String;

    .line 303
    .restart local v12    # "from":Ljava/lang/String;
    const/4 v4, 0x4

    move-object/from16 v0, v16

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v17

    .end local v17    # "to":Ljava/lang/String;
    check-cast v17, Ljava/lang/String;

    .line 305
    .restart local v17    # "to":Ljava/lang/String;
    goto/16 :goto_0

    .line 308
    :sswitch_17
    const-string v4, "sync_album"

    invoke-virtual {v2, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 313
    :sswitch_18
    const-string v4, "sync_photo"

    invoke-virtual {v2, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 318
    :sswitch_19
    const-string v4, "photos_of_user"

    invoke-virtual {v2, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 323
    :sswitch_1a
    const-string v4, "sync_photo_images"

    invoke-virtual {v2, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 328
    :sswitch_1b
    const-string v4, "sync_photo LEFT OUTER JOIN sync_photo_images ON (sync_photo.id = sync_photo_images.target_id)"

    invoke-virtual {v2, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 329
    sget-object v4, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookContentProvider;->sSyncImageProjectionMap:Ljava/util/HashMap;

    invoke-virtual {v2, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    .line 331
    const-string v7, "sync_photo_images.url"

    .line 334
    const-string p5, "sync_photo.id, sync_photo_images.width DESC"

    .line 338
    goto/16 :goto_0

    .line 341
    :sswitch_1c
    const-string v4, "sync_photo LEFT OUTER JOIN sync_photo_images ON (sync_photo.id = sync_photo_images.target_id)"

    invoke-virtual {v2, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 342
    sget-object v4, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookContentProvider;->sSyncImageProjectionMap:Ljava/util/HashMap;

    invoke-virtual {v2, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    .line 343
    const-string v4, "sync_photo.target_id = "

    invoke-virtual {v2, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 345
    const/4 v4, 0x2

    move-object/from16 v0, v16

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/CharSequence;

    invoke-virtual {v2, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 347
    const-string v7, "sync_photo_images.url"

    .line 350
    const-string p5, "sync_photo.id, sync_photo_images.width DESC"

    .line 354
    goto/16 :goto_0

    .line 357
    :sswitch_1d
    const-string v4, "sync_tags"

    invoke-virtual {v2, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 362
    :sswitch_1e
    const-string v4, "previous_sync_state"

    invoke-virtual {v2, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 367
    :sswitch_1f
    const-string v4, "thread"

    invoke-virtual {v2, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 372
    :sswitch_20
    const-string v4, "status_stream"

    invoke-virtual {v2, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 374
    const-string v4, "update"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    .line 375
    .local v18, "updateParameter":Ljava/lang/String;
    const-string v4, "true"

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 376
    const-string v4, "from_id"

    move-object/from16 v0, p3

    move-object/from16 v1, p4

    invoke-static {v0, v1, v4}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->parseQueryParam(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    .line 378
    .local v19, "userId":Ljava/lang/String;
    if-eqz v19, :cond_0

    .line 379
    new-instance v13, Landroid/content/Intent;

    const-string v4, "com.sec.android.app.sns.profile.ACTION_FACEBOOK_REQUESTED"

    invoke-direct {v13, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 381
    .local v13, "intent":Landroid/content/Intent;
    const-string v4, "id"

    move-object/from16 v0, v19

    invoke-virtual {v13, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 382
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookContentProvider;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v13}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 406
    .end local v13    # "intent":Landroid/content/Intent;
    .end local v18    # "updateParameter":Ljava/lang/String;
    .end local v19    # "userId":Ljava/lang/String;
    :catch_0
    move-exception v11

    .line 407
    .local v11, "e":Landroid/database/sqlite/SQLiteException;
    invoke-virtual {v11}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    goto/16 :goto_1

    .line 194
    :sswitch_data_0
    .sparse-switch
        0x64 -> :sswitch_0
        0xc8 -> :sswitch_1
        0x12c -> :sswitch_3
        0x1f4 -> :sswitch_4
        0x258 -> :sswitch_6
        0x2bc -> :sswitch_5
        0x320 -> :sswitch_c
        0x3e8 -> :sswitch_b
        0x44c -> :sswitch_d
        0x4b0 -> :sswitch_e
        0x514 -> :sswitch_12
        0x578 -> :sswitch_13
        0x5dc -> :sswitch_14
        0x640 -> :sswitch_f
        0x6a4 -> :sswitch_10
        0x708 -> :sswitch_11
        0x7d0 -> :sswitch_15
        0x7da -> :sswitch_16
        0x834 -> :sswitch_17
        0x898 -> :sswitch_18
        0x8a2 -> :sswitch_1a
        0x8ac -> :sswitch_1b
        0x8b6 -> :sswitch_1c
        0x8c0 -> :sswitch_1d
        0x8fc -> :sswitch_1e
        0x9c4 -> :sswitch_1f
        0xa28 -> :sswitch_2
        0xfa0 -> :sswitch_7
        0x1004 -> :sswitch_8
        0x1068 -> :sswitch_9
        0x10cc -> :sswitch_a
        0x1194 -> :sswitch_20
        0x11f8 -> :sswitch_19
    .end sparse-switch
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 8
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 930
    const-string v0, "002c45129f2b78a6a509a7b1514d103d6fe3006b"

    .line 932
    .local v0, "MASTER_KEY":Ljava/lang/String;
    const/4 v1, 0x0

    .line 933
    .local v1, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v4, 0x0

    .line 934
    .local v4, "table":Ljava/lang/String;
    sget-object v6, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v6, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v2

    .line 936
    .local v2, "match":I
    iget-object v6, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookContentProvider;->mOpenHelper:Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDBHelper;

    invoke-virtual {v6}, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDBHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 938
    sparse-switch v2, :sswitch_data_0

    .line 1105
    new-instance v5, Ljava/lang/IllegalArgumentException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Unknown URI : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 940
    :sswitch_0
    const-string v4, "comments"

    .line 1108
    :goto_0
    invoke-static {v1, v4, p2, p3, p4}, Lcom/sec/android/app/sns3/agent/db/SnsDBWrapper;->onUpdate(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v3

    .line 1109
    .local v3, "result":I
    iget-object v5, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookContentProvider;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, p1, v6}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 1110
    .end local v3    # "result":I
    :goto_1
    return v3

    .line 945
    :sswitch_1
    const-string v4, "feed_list"

    .line 947
    goto :goto_0

    .line 949
    :sswitch_2
    const-string v4, "album_contents"

    .line 951
    goto :goto_0

    .line 954
    :sswitch_3
    const-string v4, "likes"

    .line 956
    goto :goto_0

    .line 959
    :sswitch_4
    const-string v4, "post"

    .line 961
    goto :goto_0

    .line 964
    :sswitch_5
    const-string v4, "event"

    .line 966
    goto :goto_0

    .line 969
    :sswitch_6
    const-string v4, "user"

    .line 971
    goto :goto_0

    .line 974
    :sswitch_7
    const-string v4, "friends"

    .line 976
    goto :goto_0

    .line 978
    :sswitch_8
    const-string v4, "work"

    .line 980
    goto :goto_0

    .line 982
    :sswitch_9
    const-string v4, "education"

    .line 984
    goto :goto_0

    .line 986
    :sswitch_a
    const-string v4, "friendlist_group_members"

    .line 988
    goto :goto_0

    .line 991
    :sswitch_b
    const-string v4, "notification_query"

    .line 993
    goto :goto_0

    .line 996
    :sswitch_c
    const-string v4, "message_query"

    .line 998
    goto :goto_0

    .line 1001
    :sswitch_d
    const-string v4, "friend_request_query"

    .line 1003
    goto :goto_0

    .line 1006
    :sswitch_e
    const-string v4, "birthday"

    .line 1008
    goto :goto_0

    .line 1011
    :sswitch_f
    const-string v4, "album"

    .line 1013
    goto :goto_0

    .line 1016
    :sswitch_10
    const-string v4, "photo"

    .line 1018
    goto :goto_0

    .line 1021
    :sswitch_11
    const-string v4, "tags"

    .line 1023
    goto :goto_0

    .line 1026
    :sswitch_12
    const-string v4, "photo_stream"

    .line 1028
    goto :goto_0

    .line 1031
    :sswitch_13
    const-string v4, "groups"

    .line 1033
    goto :goto_0

    .line 1036
    :sswitch_14
    const-string v4, "note"

    .line 1038
    goto :goto_0

    .line 1041
    :sswitch_15
    const-string v4, "sync_event"

    .line 1043
    goto :goto_0

    .line 1046
    :sswitch_16
    const-string v4, "sync_album"

    .line 1048
    goto :goto_0

    .line 1051
    :sswitch_17
    const-string v4, "sync_photo"

    .line 1053
    goto :goto_0

    .line 1056
    :sswitch_18
    const-string v4, "photos_of_user"

    .line 1058
    goto :goto_0

    .line 1061
    :sswitch_19
    const-string v4, "sync_photo_images"

    .line 1063
    goto :goto_0

    .line 1066
    :sswitch_1a
    const-string v4, "sync_tags"

    .line 1068
    goto :goto_0

    .line 1071
    :sswitch_1b
    const-string v4, "previous_sync_state"

    .line 1073
    goto :goto_0

    .line 1076
    :sswitch_1c
    const-string v4, "thread"

    .line 1078
    goto :goto_0

    .line 1081
    :sswitch_1d
    const-string v6, "002c45129f2b78a6a509a7b1514d103d6fe3006b"

    invoke-virtual {p3, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 1082
    sget-object v5, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookContentProvider;->TAG:Ljava/lang/String;

    const-string v6, "checkDebugkey() : SnsProvider DEBUG MODE ON!!"

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 1083
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookContentProvider;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5, v3}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->setDebugKey(Landroid/content/Context;Z)V

    goto :goto_1

    :cond_0
    move v3, v5

    .line 1086
    goto :goto_1

    .line 1091
    :sswitch_1e
    const-string v6, "002c45129f2b78a6a509a7b1514d103d6fe3006b"

    invoke-virtual {p3, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 1092
    sget-object v6, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookContentProvider;->TAG:Ljava/lang/String;

    const-string v7, "checkDebugkey() : SnsProvider DEBUG MODE OFF!!"

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 1093
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookContentProvider;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6, v5}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->setDebugKey(Landroid/content/Context;Z)V

    goto/16 :goto_1

    :cond_1
    move v3, v5

    .line 1096
    goto/16 :goto_1

    .line 1100
    :sswitch_1f
    const-string v4, "status_stream"

    .line 1102
    goto/16 :goto_0

    .line 938
    nop

    :sswitch_data_0
    .sparse-switch
        0x64 -> :sswitch_0
        0xc8 -> :sswitch_1
        0x12c -> :sswitch_3
        0x1f4 -> :sswitch_4
        0x258 -> :sswitch_6
        0x2bc -> :sswitch_5
        0x320 -> :sswitch_f
        0x3e8 -> :sswitch_e
        0x44c -> :sswitch_10
        0x4b0 -> :sswitch_11
        0x514 -> :sswitch_12
        0x578 -> :sswitch_13
        0x5dc -> :sswitch_14
        0x640 -> :sswitch_b
        0x6a4 -> :sswitch_c
        0x708 -> :sswitch_d
        0x7d0 -> :sswitch_15
        0x834 -> :sswitch_16
        0x898 -> :sswitch_17
        0x8a2 -> :sswitch_19
        0x8c0 -> :sswitch_1a
        0x8fc -> :sswitch_1b
        0x9c4 -> :sswitch_1c
        0xa28 -> :sswitch_2
        0xc1c -> :sswitch_1d
        0xc80 -> :sswitch_1e
        0xfa0 -> :sswitch_7
        0x1004 -> :sswitch_8
        0x1068 -> :sswitch_9
        0x10cc -> :sswitch_a
        0x1194 -> :sswitch_1f
        0x11f8 -> :sswitch_18
    .end sparse-switch
.end method
