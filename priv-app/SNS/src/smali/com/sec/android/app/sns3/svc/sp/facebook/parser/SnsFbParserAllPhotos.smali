.class public Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserAllPhotos;
.super Ljava/lang/Object;
.source "SnsFbParserAllPhotos.java"


# static fields
.field private static final CREATED:Ljava/lang/String; = "created"

.field private static final DATA:Ljava/lang/String; = "data"

.field private static final HEIGHT:Ljava/lang/String; = "src_height"

.field private static final MODIFIED:Ljava/lang/String; = "modified"

.field private static final PID:Ljava/lang/String; = "pid"

.field private static final SRC:Ljava/lang/String; = "src"

.field private static final WIDTH:Ljava/lang/String; = "src_width"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;
    .locals 10
    .param p0, "content"    # Ljava/lang/String;

    .prologue
    .line 22
    const/4 v7, 0x0

    .line 24
    .local v7, "response":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAllPhotos;
    const/4 v4, 0x0

    .line 25
    .local v4, "newResponse":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAllPhotos;
    const/4 v1, 0x0

    .line 29
    .local v1, "curResponse":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAllPhotos;
    :try_start_0
    new-instance v6, Lorg/json/JSONObject;

    invoke-direct {v6, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 31
    .local v6, "object":Lorg/json/JSONObject;
    const-string v8, "data"

    invoke-virtual {v6, v8}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 34
    .local v0, "array":Lorg/json/JSONArray;
    const/4 v3, 0x0

    .local v3, "i":I
    move-object v5, v4

    .end local v4    # "newResponse":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAllPhotos;
    .local v5, "newResponse":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAllPhotos;
    :goto_0
    :try_start_1
    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v8

    if-ge v3, v8, :cond_1

    .line 36
    new-instance v4, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAllPhotos;

    invoke-direct {v4}, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAllPhotos;-><init>()V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    .line 37
    .end local v5    # "newResponse":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAllPhotos;
    .restart local v4    # "newResponse":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAllPhotos;
    :try_start_2
    invoke-virtual {v0, v3}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lorg/json/JSONObject;

    const-string v9, "pid"

    invoke-virtual {v8, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, v4, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAllPhotos;->mPid:Ljava/lang/String;

    .line 38
    invoke-virtual {v0, v3}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lorg/json/JSONObject;

    const-string v9, "src"

    invoke-virtual {v8, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, v4, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAllPhotos;->mSrc:Ljava/lang/String;

    .line 39
    invoke-virtual {v0, v3}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lorg/json/JSONObject;

    const-string v9, "src_height"

    invoke-virtual {v8, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, v4, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAllPhotos;->mHeight:Ljava/lang/String;

    .line 40
    invoke-virtual {v0, v3}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lorg/json/JSONObject;

    const-string v9, "src_width"

    invoke-virtual {v8, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, v4, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAllPhotos;->mWidth:Ljava/lang/String;

    .line 41
    invoke-virtual {v0, v3}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lorg/json/JSONObject;

    const-string v9, "modified"

    invoke-virtual {v8, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, v4, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAllPhotos;->mModified:Ljava/lang/String;

    .line 42
    invoke-virtual {v0, v3}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lorg/json/JSONObject;

    const-string v9, "created"

    invoke-virtual {v8, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, v4, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAllPhotos;->mCreated:Ljava/lang/String;

    .line 44
    if-nez v7, :cond_0

    .line 46
    move-object v7, v4

    .line 47
    move-object v1, v7

    .line 34
    :goto_1
    add-int/lit8 v3, v3, 0x1

    move-object v5, v4

    .end local v4    # "newResponse":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAllPhotos;
    .restart local v5    # "newResponse":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAllPhotos;
    goto :goto_0

    .line 51
    .end local v5    # "newResponse":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAllPhotos;
    .restart local v4    # "newResponse":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAllPhotos;
    :cond_0
    iput-object v4, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAllPhotos;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAllPhotos;

    .line 52
    iget-object v1, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAllPhotos;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAllPhotos;
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_1

    .end local v4    # "newResponse":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAllPhotos;
    .restart local v5    # "newResponse":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAllPhotos;
    :cond_1
    move-object v4, v5

    .line 61
    .end local v0    # "array":Lorg/json/JSONArray;
    .end local v3    # "i":I
    .end local v5    # "newResponse":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAllPhotos;
    .end local v6    # "object":Lorg/json/JSONObject;
    .restart local v4    # "newResponse":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAllPhotos;
    :goto_2
    return-object v7

    .line 56
    :catch_0
    move-exception v2

    .line 58
    .local v2, "e":Lorg/json/JSONException;
    :goto_3
    invoke-virtual {v2}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_2

    .line 56
    .end local v2    # "e":Lorg/json/JSONException;
    .end local v4    # "newResponse":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAllPhotos;
    .restart local v0    # "array":Lorg/json/JSONArray;
    .restart local v3    # "i":I
    .restart local v5    # "newResponse":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAllPhotos;
    .restart local v6    # "object":Lorg/json/JSONObject;
    :catch_1
    move-exception v2

    move-object v4, v5

    .end local v5    # "newResponse":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAllPhotos;
    .restart local v4    # "newResponse":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAllPhotos;
    goto :goto_3
.end method
