.class public Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserFamily;
.super Ljava/lang/Object;
.source "SnsFbParserFamily.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserFamily$FacebookFamily;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    return-void
.end method

.method public static parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFamily;
    .locals 12
    .param p0, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 46
    const/4 v2, 0x0

    .line 47
    .local v2, "family":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFamily;
    const/4 v0, 0x0

    .line 50
    .local v0, "curFamily":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFamily;
    :try_start_0
    new-instance v7, Lorg/json/JSONObject;

    invoke-direct {v7, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 52
    .local v7, "jsonObject":Lorg/json/JSONObject;
    new-instance v4, Lorg/json/JSONArray;

    const-string v9, "data"

    invoke-virtual {v7, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v4, v9}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 54
    .local v4, "ja":Lorg/json/JSONArray;
    if-eqz v4, :cond_1

    .line 55
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v9

    if-ge v3, v9, :cond_1

    .line 57
    new-instance v8, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFamily;

    invoke-direct {v8}, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFamily;-><init>()V

    .line 59
    .local v8, "newFamily":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFamily;
    invoke-virtual {v4, v3}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v6

    .line 61
    .local v6, "jsonObj":Lorg/json/JSONObject;
    const-string v9, "id"

    invoke-virtual {v6, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v8, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFamily;->mID:Ljava/lang/String;

    .line 62
    const-string v9, "name"

    invoke-virtual {v6, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v8, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFamily;->mName:Ljava/lang/String;

    .line 63
    const-string v9, "relationship"

    invoke-virtual {v6, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v8, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFamily;->mRelationship:Ljava/lang/String;

    .line 65
    if-nez v2, :cond_0

    .line 66
    move-object v2, v8

    .line 67
    move-object v0, v2

    .line 55
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 70
    :cond_0
    iput-object v8, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFamily;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFamily;

    .line 71
    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFamily;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFamily;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_1

    .line 76
    .end local v3    # "i":I
    .end local v4    # "ja":Lorg/json/JSONArray;
    .end local v6    # "jsonObj":Lorg/json/JSONObject;
    .end local v7    # "jsonObject":Lorg/json/JSONObject;
    .end local v8    # "newFamily":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFamily;
    :catch_0
    move-exception v5

    .line 77
    .local v5, "je":Lorg/json/JSONException;
    const-string v9, "SNS"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "parser : JSONException error :"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v5}, Lorg/json/JSONException;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 82
    .end local v5    # "je":Lorg/json/JSONException;
    :cond_1
    :goto_2
    return-object v2

    .line 78
    :catch_1
    move-exception v1

    .line 79
    .local v1, "e":Ljava/lang/Exception;
    const-string v9, "SNS"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "parser : Exception error : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method
