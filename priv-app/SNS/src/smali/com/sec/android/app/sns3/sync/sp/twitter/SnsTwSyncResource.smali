.class public Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncResource;
.super Ljava/lang/Object;
.source "SnsTwSyncResource.java"


# static fields
.field public static final CONTENT_URI:Ljava/lang/String; = "SNS3_CONTENT_URI"

.field public static final CONTENT_URI_FRIENDS:Ljava/lang/String; = "SNS3_CONTENT_URI_FRIENDS"

.field public static final CONTENT_URI_TIMELINE:Ljava/lang/String; = "SNS3_CONTENT_URI_TIMELINE"

.field public static final RESULT:Ljava/lang/String; = "SNS_RESULT"

.field public static final RETRY_LOGIN:Ljava/lang/String; = "RetryLogin"

.field public static final RETRY_LOGIN_ACTION:Ljava/lang/String; = "com.sec.android.app.sns3.RETRY_LOGIN_TWITTER"

.field public static final RETRY_SSO_ACTION:Ljava/lang/String; = "com.sec.android.app.sns3.RETRY_SSO_TWITTER"

.field public static final SNS_BROADCAST_PERMISSION:Ljava/lang/String; = "com.sec.android.app.sns3.permission.RECEIVE_SNS_BROADCAST"

.field public static final UPDATE_HOME_TIMELINE:Ljava/lang/String; = "com.sec.android.app.sns3.action.SYNC_TW_HOME_TIMELINE"

.field public static final UPDATE_PROFILES:Ljava/lang/String; = "com.sec.android.app.sns3.action.SYNC_TW_PROFILES"

.field public static final UPDATE_USER_TIMELINE:Ljava/lang/String; = "com.sec.android.app.sns3.action.SYNC_TW_USER_TIMELINE"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
