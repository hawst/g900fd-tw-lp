.class public interface abstract Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$ThreadColumns;
.super Ljava/lang/Object;
.source "SnsFacebookDB.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ThreadColumns"
.end annotation


# static fields
.field public static final AUTHOR_ID:Ljava/lang/String; = "author_id"

.field public static final AUTHOR_NAME:Ljava/lang/String; = "author_name"

.field public static final AUTHOR_PIC:Ljava/lang/String; = "author_pic"

.field public static final BODY:Ljava/lang/String; = "body"

.field public static final CREATED_TIME:Ljava/lang/String; = "created_time"

.field public static final MESSAGE_ID:Ljava/lang/String; = "message_id"

.field public static final THREAD_ID:Ljava/lang/String; = "thread_id"

.field public static final VIEWER_ID:Ljava/lang/String; = "viewer_id"
