.class public Lcom/sec/android/app/sns3/auth/SnsAccountAlertDialogActivity;
.super Landroid/app/Activity;
.source "SnsAccountAlertDialogActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/auth/SnsAccountAlertDialogActivity$AlertDialogFragment;
    }
.end annotation


# static fields
.field public static final EXTRA_DIALOG_MESSAGE:Ljava/lang/String; = "dlg_message"

.field public static final EXTRA_DIALOG_TITLE:Ljava/lang/String; = "dlg_title"

.field public static final EXTRA_ICON_ID:Ljava/lang/String; = "icon_id"

.field public static final EXTRA_TITLE:Ljava/lang/String; = "title"

.field public static final EXTRA_URL:Ljava/lang/String; = "url"


# instance fields
.field private mDlgMessage:Ljava/lang/String;

.field private mDlgTitle:Ljava/lang/String;

.field private mIconId:I

.field private mPositiveActionUrl:Ljava/lang/String;

.field private mTitle:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 35
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 43
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/sns3/auth/SnsAccountAlertDialogActivity;->mIconId:I

    .line 44
    iput-object v1, p0, Lcom/sec/android/app/sns3/auth/SnsAccountAlertDialogActivity;->mTitle:Ljava/lang/String;

    .line 45
    iput-object v1, p0, Lcom/sec/android/app/sns3/auth/SnsAccountAlertDialogActivity;->mDlgTitle:Ljava/lang/String;

    .line 46
    iput-object v1, p0, Lcom/sec/android/app/sns3/auth/SnsAccountAlertDialogActivity;->mDlgMessage:Ljava/lang/String;

    .line 47
    iput-object v1, p0, Lcom/sec/android/app/sns3/auth/SnsAccountAlertDialogActivity;->mPositiveActionUrl:Ljava/lang/String;

    .line 94
    return-void
.end method


# virtual methods
.method public doNegativeClick()V
    .locals 0

    .prologue
    .line 91
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/SnsAccountAlertDialogActivity;->finish()V

    .line 92
    return-void
.end method

.method public doPositiveClick()V
    .locals 5

    .prologue
    .line 75
    iget-object v2, p0, Lcom/sec/android/app/sns3/auth/SnsAccountAlertDialogActivity;->mPositiveActionUrl:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 76
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 77
    .local v1, "intent":Landroid/content/Intent;
    iget-object v2, p0, Lcom/sec/android/app/sns3/auth/SnsAccountAlertDialogActivity;->mPositiveActionUrl:Ljava/lang/String;

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 78
    const/high16 v2, 0x10000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 80
    :try_start_0
    invoke-virtual {p0, v1}, Lcom/sec/android/app/sns3/auth/SnsAccountAlertDialogActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 87
    .end local v1    # "intent":Landroid/content/Intent;
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/SnsAccountAlertDialogActivity;->finish()V

    .line 88
    return-void

    .line 81
    .restart local v1    # "intent":Landroid/content/Intent;
    :catch_0
    move-exception v0

    .line 82
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/SnsAccountAlertDialogActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f080035

    const/4 v4, 0x1

    invoke-static {v2, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 51
    invoke-static {p0}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->isLightTheme(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_2

    const v2, 0x103012b

    :goto_0
    invoke-virtual {p0, v2}, Lcom/sec/android/app/sns3/auth/SnsAccountAlertDialogActivity;->setTheme(I)V

    .line 54
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 56
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/SnsAccountAlertDialogActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 57
    .local v0, "intent":Landroid/content/Intent;
    if-eqz v0, :cond_0

    .line 58
    const-string v2, "icon_id"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/sns3/auth/SnsAccountAlertDialogActivity;->mIconId:I

    .line 59
    const-string v2, "title"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/sns3/auth/SnsAccountAlertDialogActivity;->mTitle:Ljava/lang/String;

    .line 60
    const-string v2, "dlg_title"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/sns3/auth/SnsAccountAlertDialogActivity;->mDlgTitle:Ljava/lang/String;

    .line 61
    const-string v2, "dlg_message"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/sns3/auth/SnsAccountAlertDialogActivity;->mDlgMessage:Ljava/lang/String;

    .line 62
    const-string v2, "url"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/sns3/auth/SnsAccountAlertDialogActivity;->mPositiveActionUrl:Ljava/lang/String;

    .line 65
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/sns3/auth/SnsAccountAlertDialogActivity;->mTitle:Ljava/lang/String;

    invoke-virtual {p0, v2}, Lcom/sec/android/app/sns3/auth/SnsAccountAlertDialogActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 66
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/SnsAccountAlertDialogActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 67
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/SnsAccountAlertDialogActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/sns3/auth/SnsAccountAlertDialogActivity;->mIconId:I

    invoke-virtual {v2, v3}, Landroid/app/ActionBar;->setIcon(I)V

    .line 70
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/sns3/auth/SnsAccountAlertDialogActivity;->mDlgTitle:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/android/app/sns3/auth/SnsAccountAlertDialogActivity;->mDlgMessage:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/android/app/sns3/auth/SnsAccountAlertDialogActivity$AlertDialogFragment;->newInstance(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/android/app/sns3/auth/SnsAccountAlertDialogActivity$AlertDialogFragment;

    move-result-object v1

    .line 71
    .local v1, "newFragment":Landroid/app/DialogFragment;
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/SnsAccountAlertDialogActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-string v3, "dialog"

    invoke-virtual {v1, v2, v3}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 72
    return-void

    .line 51
    .end local v0    # "intent":Landroid/content/Intent;
    .end local v1    # "newFragment":Landroid/app/DialogFragment;
    :cond_2
    const v2, 0x1030128

    goto :goto_0
.end method
