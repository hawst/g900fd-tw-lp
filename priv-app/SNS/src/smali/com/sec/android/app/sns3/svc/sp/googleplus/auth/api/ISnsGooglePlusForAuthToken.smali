.class public interface abstract Lcom/sec/android/app/sns3/svc/sp/googleplus/auth/api/ISnsGooglePlusForAuthToken;
.super Ljava/lang/Object;
.source "ISnsGooglePlusForAuthToken.java"

# interfaces
.implements Landroid/os/IInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/googleplus/auth/api/ISnsGooglePlusForAuthToken$Stub;
    }
.end annotation


# virtual methods
.method public abstract getAuthTokenNExpires()Ljava/util/Map;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract getMyAccountInfo(Lcom/sec/android/app/sns3/svc/sp/googleplus/auth/api/ISnsGooglePlusCallbackMyAccountInfo;)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method
