.class public Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserLikeDone;
.super Ljava/lang/Object;
.source "SnsFbParserLikeDone.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserLikeDone$FacebookLikeDone;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    return-void
.end method

.method public static parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseBoolean;
    .locals 4
    .param p0, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 43
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseBoolean;

    invoke-direct {v0}, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseBoolean;-><init>()V

    .line 46
    .local v0, "bool":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseBoolean;
    :try_start_0
    new-instance v2, Lorg/json/JSONArray;

    invoke-direct {v2, p0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 47
    .local v2, "ja":Lorg/json/JSONArray;
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-lez v3, :cond_0

    .line 48
    const-string v3, "true"

    iput-object v3, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseBoolean;->mBeSuccess:Ljava/lang/String;

    .line 57
    .end local v2    # "ja":Lorg/json/JSONArray;
    :goto_0
    return-object v0

    .line 50
    .restart local v2    # "ja":Lorg/json/JSONArray;
    :cond_0
    const-string v3, "false"

    iput-object v3, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseBoolean;->mBeSuccess:Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 52
    .end local v2    # "ja":Lorg/json/JSONArray;
    :catch_0
    move-exception v1

    .line 54
    .local v1, "e":Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method
