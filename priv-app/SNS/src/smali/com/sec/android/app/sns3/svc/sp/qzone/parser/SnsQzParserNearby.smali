.class public Lcom/sec/android/app/sns3/svc/sp/qzone/parser/SnsQzParserNearby;
.super Ljava/lang/Object;
.source "SnsQzParserNearby.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/qzone/parser/SnsQzParserNearby$QzoneNearby;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    return-void
.end method


# virtual methods
.method public parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseNearby;
    .locals 12
    .param p1, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 57
    const/4 v8, 0x0

    .line 58
    .local v8, "nearby":Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseNearby;
    const/4 v1, 0x0

    .line 60
    .local v1, "curNearby":Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseNearby;
    const/4 v6, 0x0

    .line 63
    .local v6, "jsonObject":Lorg/json/JSONObject;
    :try_start_0
    new-instance v7, Lorg/json/JSONObject;

    invoke-direct {v7, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 65
    .end local v6    # "jsonObject":Lorg/json/JSONObject;
    .local v7, "jsonObject":Lorg/json/JSONObject;
    :try_start_1
    new-instance v5, Lorg/json/JSONObject;

    const-string v11, "data"

    invoke-virtual {v7, v11}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v5, v11}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 67
    .local v5, "jdata":Lorg/json/JSONObject;
    const-string v11, "hostopenid"

    invoke-virtual {v5, v11}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 69
    .local v0, "HostID":Ljava/lang/String;
    new-instance v4, Lorg/json/JSONArray;

    const-string v11, "checkins"

    invoke-virtual {v5, v11}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v4, v11}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 71
    .local v4, "janearby":Lorg/json/JSONArray;
    if-eqz v4, :cond_1

    .line 73
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v11

    if-ge v3, v11, :cond_1

    .line 74
    new-instance v10, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseNearby;

    invoke-direct {v10}, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseNearby;-><init>()V

    .line 76
    .local v10, "newNearby":Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseNearby;
    invoke-virtual {v4, v3}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v9

    .line 78
    .local v9, "nearbyJsonObj":Lorg/json/JSONObject;
    const-string v11, "cellid"

    invoke-virtual {v9, v11}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    iput-object v11, v10, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseNearby;->mPostId:Ljava/lang/String;

    .line 79
    const-string v11, "latitude"

    invoke-virtual {v9, v11}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    iput-object v11, v10, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseNearby;->mLatitude:Ljava/lang/String;

    .line 80
    const-string v11, "longitude"

    invoke-virtual {v9, v11}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    iput-object v11, v10, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseNearby;->mLongitude:Ljava/lang/String;

    .line 81
    const-string v11, "positionname"

    invoke-virtual {v9, v11}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    iput-object v11, v10, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseNearby;->mPlaceName:Ljava/lang/String;

    .line 82
    const-string v11, "nickname"

    invoke-virtual {v9, v11}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    iput-object v11, v10, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseNearby;->mUserName:Ljava/lang/String;

    .line 83
    iput-object v0, v10, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseNearby;->mHostOpenID:Ljava/lang/String;

    .line 84
    const-string v11, "openid"

    invoke-virtual {v9, v11}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    iput-object v11, v10, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseNearby;->mAuthorUID:Ljava/lang/String;

    .line 85
    const-string v11, "portraiturl"

    invoke-virtual {v9, v11}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    iput-object v11, v10, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseNearby;->mUserPicUrl:Ljava/lang/String;

    .line 86
    const-string v11, "detailparam"

    invoke-virtual {v9, v11}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    iput-object v11, v10, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseNearby;->mDetailParam:Ljava/lang/String;

    .line 87
    const-string v11, "time"

    invoke-virtual {v9, v11}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    iput-object v11, v10, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseNearby;->mTimestamp:Ljava/lang/String;

    .line 89
    if-nez v8, :cond_0

    .line 90
    move-object v8, v10

    .line 91
    move-object v1, v8

    .line 73
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 93
    :cond_0
    iput-object v10, v1, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseNearby;->mNext:Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseNearby;

    .line 94
    iget-object v1, v1, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseNearby;->mNext:Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseNearby;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .end local v3    # "i":I
    .end local v9    # "nearbyJsonObj":Lorg/json/JSONObject;
    .end local v10    # "newNearby":Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseNearby;
    :cond_1
    move-object v6, v7

    .line 104
    .end local v0    # "HostID":Ljava/lang/String;
    .end local v4    # "janearby":Lorg/json/JSONArray;
    .end local v5    # "jdata":Lorg/json/JSONObject;
    .end local v7    # "jsonObject":Lorg/json/JSONObject;
    .restart local v6    # "jsonObject":Lorg/json/JSONObject;
    :goto_2
    return-object v8

    .line 99
    :catch_0
    move-exception v2

    .line 101
    .local v2, "e":Lorg/json/JSONException;
    :goto_3
    invoke-virtual {v2}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_2

    .line 99
    .end local v2    # "e":Lorg/json/JSONException;
    .end local v6    # "jsonObject":Lorg/json/JSONObject;
    .restart local v7    # "jsonObject":Lorg/json/JSONObject;
    :catch_1
    move-exception v2

    move-object v6, v7

    .end local v7    # "jsonObject":Lorg/json/JSONObject;
    .restart local v6    # "jsonObject":Lorg/json/JSONObject;
    goto :goto_3
.end method
