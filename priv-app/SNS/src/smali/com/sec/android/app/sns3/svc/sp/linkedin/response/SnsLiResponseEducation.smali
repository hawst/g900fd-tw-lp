.class public final Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseEducation;
.super Ljava/lang/Object;
.source "SnsLiResponseEducation.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseEducation$LinkedInEducation;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseEducation;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mEducationDegree:Ljava/lang/String;

.field private mEducationEnd_Date_Year:Ljava/lang/String;

.field private mEducationId:Ljava/lang/String;

.field private mEducationSchool_Name:Ljava/lang/String;

.field private mEducationStart_Date_Year:Ljava/lang/String;

.field private mField_Of_Study:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 121
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseEducation$1;

    invoke-direct {v0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseEducation$1;-><init>()V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseEducation;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 114
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 115
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 117
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 118
    invoke-virtual {p0, p1}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseEducation;->readFromParcel(Landroid/os/Parcel;)V

    .line 119
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseEducation$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseEducation$1;

    .prologue
    .line 31
    invoke-direct {p0, p1}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseEducation;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public static parse(Lorg/json/JSONObject;)Ljava/util/List;
    .locals 9
    .param p0, "contentObj"    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/json/JSONObject;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseEducation;",
            ">;"
        }
    .end annotation

    .prologue
    .line 158
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 160
    .local v2, "educationList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseEducation;>;"
    if-eqz p0, :cond_2

    .line 163
    :try_start_0
    const-string v8, "values"

    invoke-virtual {p0, v8}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v1

    .line 165
    .local v1, "education":Lorg/json/JSONArray;
    if-eqz v1, :cond_2

    .line 167
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v8

    if-ge v6, v8, :cond_2

    .line 169
    new-instance v4, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseEducation;

    invoke-direct {v4}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseEducation;-><init>()V

    .line 171
    .local v4, "educationdetails":Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseEducation;
    invoke-virtual {v1, v6}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v3

    .line 172
    .local v3, "educationValue":Lorg/json/JSONObject;
    const-string v8, "degree"

    invoke-virtual {v3, v8}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v8}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseEducation;->setEducationDegree(Ljava/lang/String;)V

    .line 174
    const-string v8, "endDate"

    invoke-virtual {v3, v8}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v5

    .line 176
    .local v5, "end_date":Lorg/json/JSONObject;
    if-eqz v5, :cond_0

    .line 177
    const-string v8, "year"

    invoke-virtual {v5, v8}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v8}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseEducation;->setEducationEnd_Date_Year(Ljava/lang/String;)V

    .line 179
    :cond_0
    const-string v8, "id"

    invoke-virtual {v3, v8}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v8}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseEducation;->setEducationId(Ljava/lang/String;)V

    .line 181
    const-string v8, "schoolName"

    invoke-virtual {v3, v8}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v8}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseEducation;->setEducationSchool_Name(Ljava/lang/String;)V

    .line 183
    const-string v8, "startDate"

    invoke-virtual {v3, v8}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v7

    .line 185
    .local v7, "start_date":Lorg/json/JSONObject;
    if-eqz v7, :cond_1

    .line 186
    const-string v8, "year"

    invoke-virtual {v7, v8}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v8}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseEducation;->setEducationStart_Date_Year(Ljava/lang/String;)V

    .line 188
    :cond_1
    const-string v8, "fieldOfStudy"

    invoke-virtual {v3, v8}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v8}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseEducation;->setField_Of_Study(Ljava/lang/String;)V

    .line 191
    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 167
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 196
    .end local v1    # "education":Lorg/json/JSONArray;
    .end local v3    # "educationValue":Lorg/json/JSONObject;
    .end local v4    # "educationdetails":Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseEducation;
    .end local v5    # "end_date":Lorg/json/JSONObject;
    .end local v6    # "i":I
    .end local v7    # "start_date":Lorg/json/JSONObject;
    :catch_0
    move-exception v0

    .line 197
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    .line 200
    .end local v0    # "e":Lorg/json/JSONException;
    :cond_2
    return-object v2
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 154
    const/4 v0, 0x0

    return v0
.end method

.method public getEducationDegree()Ljava/lang/String;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseEducation;->mEducationDegree:Ljava/lang/String;

    return-object v0
.end method

.method public getEducationEnd_Date_Year()Ljava/lang/String;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseEducation;->mEducationEnd_Date_Year:Ljava/lang/String;

    return-object v0
.end method

.method public getEducationId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseEducation;->mEducationId:Ljava/lang/String;

    return-object v0
.end method

.method public getEducationSchool_Name()Ljava/lang/String;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseEducation;->mEducationSchool_Name:Ljava/lang/String;

    return-object v0
.end method

.method public getEducationStart_Date_Year()Ljava/lang/String;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseEducation;->mEducationStart_Date_Year:Ljava/lang/String;

    return-object v0
.end method

.method public getField_Of_Study()Ljava/lang/String;
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseEducation;->mField_Of_Study:Ljava/lang/String;

    return-object v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "parcel"    # Landroid/os/Parcel;

    .prologue
    .line 143
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseEducation;->mEducationId:Ljava/lang/String;

    .line 144
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseEducation;->mEducationSchool_Name:Ljava/lang/String;

    .line 145
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseEducation;->mEducationDegree:Ljava/lang/String;

    .line 146
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseEducation;->mEducationStart_Date_Year:Ljava/lang/String;

    .line 147
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseEducation;->mEducationEnd_Date_Year:Ljava/lang/String;

    .line 148
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseEducation;->mField_Of_Study:Ljava/lang/String;

    .line 150
    return-void
.end method

.method public setEducationDegree(Ljava/lang/String;)V
    .locals 0
    .param p1, "mEducationDegree"    # Ljava/lang/String;

    .prologue
    .line 86
    iput-object p1, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseEducation;->mEducationDegree:Ljava/lang/String;

    .line 87
    return-void
.end method

.method public setEducationEnd_Date_Year(Ljava/lang/String;)V
    .locals 0
    .param p1, "mEducationEnd_Date_Year"    # Ljava/lang/String;

    .prologue
    .line 102
    iput-object p1, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseEducation;->mEducationEnd_Date_Year:Ljava/lang/String;

    .line 103
    return-void
.end method

.method public setEducationId(Ljava/lang/String;)V
    .locals 0
    .param p1, "mEducationId"    # Ljava/lang/String;

    .prologue
    .line 70
    iput-object p1, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseEducation;->mEducationId:Ljava/lang/String;

    .line 71
    return-void
.end method

.method public setEducationSchool_Name(Ljava/lang/String;)V
    .locals 0
    .param p1, "mEducationSchool_Name"    # Ljava/lang/String;

    .prologue
    .line 78
    iput-object p1, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseEducation;->mEducationSchool_Name:Ljava/lang/String;

    .line 79
    return-void
.end method

.method public setEducationStart_Date_Year(Ljava/lang/String;)V
    .locals 0
    .param p1, "mEducationStart_Date_Year"    # Ljava/lang/String;

    .prologue
    .line 94
    iput-object p1, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseEducation;->mEducationStart_Date_Year:Ljava/lang/String;

    .line 95
    return-void
.end method

.method public setField_Of_Study(Ljava/lang/String;)V
    .locals 0
    .param p1, "mField_Of_Study"    # Ljava/lang/String;

    .prologue
    .line 110
    iput-object p1, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseEducation;->mField_Of_Study:Ljava/lang/String;

    .line 111
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "parcel"    # Landroid/os/Parcel;
    .param p2, "flag"    # I

    .prologue
    .line 133
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseEducation;->mEducationId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 134
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseEducation;->mEducationSchool_Name:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 135
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseEducation;->mEducationDegree:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 136
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseEducation;->mEducationStart_Date_Year:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 137
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseEducation;->mEducationEnd_Date_Year:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 138
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseEducation;->mField_Of_Study:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 140
    return-void
.end method
