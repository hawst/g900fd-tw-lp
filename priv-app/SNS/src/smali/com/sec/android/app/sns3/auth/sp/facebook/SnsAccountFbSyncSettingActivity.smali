.class public Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSettingActivity;
.super Landroid/preference/PreferenceActivity;
.source "SnsAccountFbSyncSettingActivity.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "SnsAccountFbSyncSettingActivity"


# instance fields
.field private mAppContext:Landroid/content/Context;

.field private mCheckBoxPref:Landroid/preference/CheckBoxPreference;

.field private mDescription:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 40
    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    .line 44
    iput-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSettingActivity;->mAppContext:Landroid/content/Context;

    .line 45
    iput-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSettingActivity;->mDescription:Landroid/widget/TextView;

    .line 46
    iput-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSettingActivity;->mCheckBoxPref:Landroid/preference/CheckBoxPreference;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSettingActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSettingActivity;

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSettingActivity;->isLoggedInSnsFacebook()Z

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSettingActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSettingActivity;

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSettingActivity;->tryFacebookSSO()V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSettingActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSettingActivity;

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSettingActivity;->removeSnsFacebookAccout()V

    return-void
.end method

.method private gotoAccountSyncSettings()V
    .locals 4

    .prologue
    .line 154
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v2

    const-string v3, "com.sec.android.app.sns3.facebook"

    invoke-virtual {v2, v3}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 155
    .local v0, "accounts":[Landroid/accounts/Account;
    array-length v2, v0

    if-lez v2, :cond_0

    .line 156
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 157
    .local v1, "intent":Landroid/content/Intent;
    const-string v2, "android.settings.ACCOUNT_SYNC_SETTINGS"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 158
    const-string v2, "account"

    const/4 v3, 0x0

    aget-object v3, v0, v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 159
    invoke-virtual {p0, v1}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSettingActivity;->startActivity(Landroid/content/Intent;)V

    .line 161
    .end local v1    # "intent":Landroid/content/Intent;
    :cond_0
    return-void
.end method

.method private isLoggedInSnsFacebook()Z
    .locals 3

    .prologue
    .line 130
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    const-string v2, "com.sec.android.app.sns3.facebook"

    invoke-virtual {v1, v2}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 131
    .local v0, "accounts":[Landroid/accounts/Account;
    if-eqz v0, :cond_0

    array-length v1, v0

    if-lez v1, :cond_0

    .line 132
    const/4 v1, 0x1

    .line 134
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private removeSnsFacebookAccout()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 147
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    const-string v2, "com.sec.android.app.sns3.facebook"

    invoke-virtual {v1, v2}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 148
    .local v0, "accounts":[Landroid/accounts/Account;
    array-length v1, v0

    if-lez v1, :cond_0

    .line 149
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSettingActivity;->mAppContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    const/4 v2, 0x0

    aget-object v2, v0, v2

    invoke-virtual {v1, v2, v3, v3}, Landroid/accounts/AccountManager;->removeAccount(Landroid/accounts/Account;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    .line 151
    :cond_0
    return-void
.end method

.method private tryFacebookSSO()V
    .locals 3

    .prologue
    .line 139
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 140
    .local v0, "intent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSettingActivity;->mAppContext:Landroid/content/Context;

    const-class v2, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 141
    const-string v1, "skip_sso_noti"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 142
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 143
    invoke-virtual {p0, v0}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSettingActivity;->startActivity(Landroid/content/Intent;)V

    .line 144
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 50
    invoke-static {p0}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->isLightTheme(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_2

    const v1, 0x103012b

    :goto_0
    invoke-virtual {p0, v1}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSettingActivity;->setTheme(I)V

    .line 53
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 61
    iput-object p0, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSettingActivity;->mAppContext:Landroid/content/Context;

    .line 63
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSettingActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 64
    .local v0, "actionBar":Landroid/app/ActionBar;
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 65
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 67
    const v1, 0x7f030004

    invoke-virtual {p0, v1}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSettingActivity;->setContentView(I)V

    .line 68
    const v1, 0x7f04000e

    invoke-virtual {p0, v1}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSettingActivity;->addPreferencesFromResource(I)V

    .line 70
    const v1, 0x7f0b0004

    invoke-virtual {p0, v1}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSettingActivity;->mDescription:Landroid/widget/TextView;

    .line 71
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSettingActivity;->mDescription:Landroid/widget/TextView;

    if-eqz v1, :cond_0

    .line 72
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSettingActivity;->mDescription:Landroid/widget/TextView;

    const v2, 0x7f080052

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 75
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSettingActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    const-string v2, "sync_checkbox"

    invoke-virtual {v1, v2}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/CheckBoxPreference;

    iput-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSettingActivity;->mCheckBoxPref:Landroid/preference/CheckBoxPreference;

    .line 76
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSettingActivity;->mCheckBoxPref:Landroid/preference/CheckBoxPreference;

    if-eqz v1, :cond_1

    .line 77
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSettingActivity;->mCheckBoxPref:Landroid/preference/CheckBoxPreference;

    new-instance v2, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSettingActivity$1;

    invoke-direct {v2, p0}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSettingActivity$1;-><init>(Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSettingActivity;)V

    invoke-virtual {v1, v2}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 108
    :cond_1
    return-void

    .line 50
    .end local v0    # "actionBar":Landroid/app/ActionBar;
    :cond_2
    const v1, 0x1030128

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 121
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 126
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 123
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSettingActivity;->finish()V

    .line 124
    const/4 v0, 0x1

    goto :goto_0

    .line 121
    nop

    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 112
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onResume()V

    .line 114
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSettingActivity;->mCheckBoxPref:Landroid/preference/CheckBoxPreference;

    if-eqz v0, :cond_0

    .line 115
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSettingActivity;->mCheckBoxPref:Landroid/preference/CheckBoxPreference;

    invoke-direct {p0}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSettingActivity;->isLoggedInSnsFacebook()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 117
    :cond_0
    return-void
.end method
