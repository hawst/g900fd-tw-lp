.class public interface abstract Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserNoteEx$FacebookNoteEx;
.super Ljava/lang/Object;
.source "SnsFbParserNoteEx.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserNoteEx;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "FacebookNoteEx"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserNoteEx$FacebookNoteEx$SnsFbLikeQuery;,
        Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserNoteEx$FacebookNoteEx$SnsFbCommentQuery;,
        Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserNoteEx$FacebookNoteEx$SnsFbNoteQuery;
    }
.end annotation


# static fields
.field public static final COMMENT_QUERY_NAME:Ljava/lang/String; = "comment_query"

.field public static final FQL_RESULT_SET:Ljava/lang/String; = "fql_result_set"

.field public static final LIKE_QUERY_NAME:Ljava/lang/String; = "like_query"

.field public static final NOTE_QUERY_NAME:Ljava/lang/String; = "note_query"

.field public static final QUERY_NAME:Ljava/lang/String; = "name"
