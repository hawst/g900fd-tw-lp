.class public Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$RecommendationUpdates;
.super Ljava/lang/Object;
.source "SnsLiResponseUpdate.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "RecommendationUpdates"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$RecommendationUpdates;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public mRecommedee:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;

.field public mRecommendationId:J

.field public mRecommendationType:Ljava/lang/String;

.field public mRecommender:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;

.field public mRecommondedUrl:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 523
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$RecommendationUpdates$1;

    invoke-direct {v0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$RecommendationUpdates$1;-><init>()V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$RecommendationUpdates;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 537
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 538
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 541
    invoke-direct {p0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$RecommendationUpdates;-><init>()V

    .line 542
    invoke-virtual {p0, p1}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$RecommendationUpdates;->readFromParcel(Landroid/os/Parcel;)V

    .line 543
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$1;

    .prologue
    .line 497
    invoke-direct {p0, p1}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$RecommendationUpdates;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 510
    const/4 v0, 0x0

    return v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 546
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$RecommendationUpdates;->mRecommendationId:J

    .line 547
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$RecommendationUpdates;->mRecommendationType:Ljava/lang/String;

    .line 548
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$RecommendationUpdates;->mRecommondedUrl:Ljava/lang/String;

    .line 549
    const-class v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$RecommendationUpdates;->mRecommender:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;

    .line 550
    const-class v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$RecommendationUpdates;->mRecommedee:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;

    .line 552
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flag"    # I

    .prologue
    .line 515
    iget-wide v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$RecommendationUpdates;->mRecommendationId:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 516
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$RecommendationUpdates;->mRecommendationType:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 517
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$RecommendationUpdates;->mRecommondedUrl:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 518
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$RecommendationUpdates;->mRecommender:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 519
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$RecommendationUpdates;->mRecommedee:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 521
    return-void
.end method
