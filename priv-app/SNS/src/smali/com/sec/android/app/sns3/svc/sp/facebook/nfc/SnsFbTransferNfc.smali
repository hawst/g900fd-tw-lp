.class public Lcom/sec/android/app/sns3/svc/sp/facebook/nfc/SnsFbTransferNfc;
.super Landroid/app/Activity;
.source "SnsFbTransferNfc.java"


# static fields
.field private static final FACEBOOK_PACKAGE_NAME:Ljava/lang/String; = "com.facebook.katana"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method private checkLogin()Z
    .locals 3

    .prologue
    .line 90
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/svc/sp/facebook/nfc/SnsFbTransferNfc;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    const-string v2, "com.facebook.auth.login"

    invoke-virtual {v1, v2}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 93
    .local v0, "accounts":[Landroid/accounts/Account;
    array-length v1, v0

    if-lez v1, :cond_0

    .line 94
    const/4 v1, 0x1

    .line 96
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private checkinstalledApp()Z
    .locals 4

    .prologue
    .line 76
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/svc/sp/facebook/nfc/SnsFbTransferNfc;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 78
    .local v1, "pm":Landroid/content/pm/PackageManager;
    :try_start_0
    const-string v2, "com.facebook.katana"

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x80

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    .line 79
    const-string v2, "TAG"

    const-string v3, "Facebook SDK installed."

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 80
    const/4 v2, 0x1

    .line 85
    :goto_0
    return v2

    .line 82
    :catch_0
    move-exception v0

    .line 84
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v2, "TAG"

    const-string v3, "Facebook SDK Don\'t install."

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 85
    const/4 v2, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 10
    .param p1, "arg0"    # Landroid/os/Bundle;

    .prologue
    const/4 v9, 0x0

    .line 25
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 26
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/svc/sp/facebook/nfc/SnsFbTransferNfc;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 27
    .local v2, "intent":Landroid/content/Intent;
    if-eqz v2, :cond_1

    .line 29
    invoke-virtual {v2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 30
    .local v0, "action":Ljava/lang/String;
    const-string v7, "android.nfc.action.NDEF_DISCOVERED"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 33
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/svc/sp/facebook/nfc/SnsFbTransferNfc;->getIntent()Landroid/content/Intent;

    move-result-object v7

    const-string v8, "android.nfc.extra.NDEF_MESSAGES"

    invoke-virtual {v7, v8}, Landroid/content/Intent;->getParcelableArrayExtra(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v4

    .line 34
    .local v4, "rawMsgs":[Landroid/os/Parcelable;
    const/4 v3, 0x0

    .line 35
    .local v3, "msgs":[Landroid/nfc/NdefMessage;
    if-eqz v4, :cond_0

    array-length v7, v4

    if-lez v7, :cond_0

    .line 36
    array-length v7, v4

    new-array v3, v7, [Landroid/nfc/NdefMessage;

    .line 37
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v7, v4

    if-ge v1, v7, :cond_0

    .line 38
    aget-object v7, v4, v1

    check-cast v7, Landroid/nfc/NdefMessage;

    aput-object v7, v3, v1

    .line 37
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 41
    .end local v1    # "i":I
    :cond_0
    if-eqz v3, :cond_1

    array-length v7, v3

    if-lez v7, :cond_1

    .line 42
    aget-object v7, v3, v9

    invoke-virtual {v7}, Landroid/nfc/NdefMessage;->getRecords()[Landroid/nfc/NdefRecord;

    move-result-object v5

    .line 43
    .local v5, "records":[Landroid/nfc/NdefRecord;
    new-instance v6, Ljava/lang/String;

    aget-object v7, v5, v9

    invoke-virtual {v7}, Landroid/nfc/NdefRecord;->getPayload()[B

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/String;-><init>([B)V

    .line 44
    .local v6, "userId":Ljava/lang/String;
    if-eqz v6, :cond_1

    .line 46
    invoke-direct {p0}, Lcom/sec/android/app/sns3/svc/sp/facebook/nfc/SnsFbTransferNfc;->checkinstalledApp()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-direct {p0}, Lcom/sec/android/app/sns3/svc/sp/facebook/nfc/SnsFbTransferNfc;->checkLogin()Z

    move-result v7

    if-eqz v7, :cond_3

    .line 47
    const-string v7, "SnsTemplate"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "SnsTemplateFacebookActivity : mButtonWall : ownerId = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 50
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 51
    .local v1, "i":Landroid/content/Intent;
    const-string v7, "android.intent.action.VIEW"

    invoke-virtual {v1, v7}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 53
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v7

    if-lez v7, :cond_2

    .line 54
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "fb://profile/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    invoke-virtual {v1, v7}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 58
    :goto_1
    invoke-virtual {p0, v1}, Lcom/sec/android/app/sns3/svc/sp/facebook/nfc/SnsFbTransferNfc;->startActivity(Landroid/content/Intent;)V

    .line 71
    .end local v0    # "action":Ljava/lang/String;
    .end local v1    # "i":Landroid/content/Intent;
    .end local v3    # "msgs":[Landroid/nfc/NdefMessage;
    .end local v4    # "rawMsgs":[Landroid/os/Parcelable;
    .end local v5    # "records":[Landroid/nfc/NdefRecord;
    .end local v6    # "userId":Ljava/lang/String;
    :cond_1
    :goto_2
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/svc/sp/facebook/nfc/SnsFbTransferNfc;->finish()V

    .line 73
    return-void

    .line 56
    .restart local v0    # "action":Ljava/lang/String;
    .restart local v1    # "i":Landroid/content/Intent;
    .restart local v3    # "msgs":[Landroid/nfc/NdefMessage;
    .restart local v4    # "rawMsgs":[Landroid/os/Parcelable;
    .restart local v5    # "records":[Landroid/nfc/NdefRecord;
    .restart local v6    # "userId":Ljava/lang/String;
    :cond_2
    const-string v7, "fb://profile"

    invoke-static {v7}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    invoke-virtual {v1, v7}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    goto :goto_1

    .line 59
    .end local v1    # "i":Landroid/content/Intent;
    :cond_3
    invoke-direct {p0}, Lcom/sec/android/app/sns3/svc/sp/facebook/nfc/SnsFbTransferNfc;->checkinstalledApp()Z

    move-result v7

    if-nez v7, :cond_1

    .line 61
    new-instance v1, Landroid/content/Intent;

    const-string v7, "android.intent.action.VIEW"

    invoke-direct {v1, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 62
    .restart local v1    # "i":Landroid/content/Intent;
    const-string v7, "market://details?id=com.facebook.katana"

    invoke-static {v7}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    invoke-virtual {v1, v7}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 63
    invoke-virtual {p0, v1}, Lcom/sec/android/app/sns3/svc/sp/facebook/nfc/SnsFbTransferNfc;->startActivity(Landroid/content/Intent;)V

    goto :goto_2
.end method
