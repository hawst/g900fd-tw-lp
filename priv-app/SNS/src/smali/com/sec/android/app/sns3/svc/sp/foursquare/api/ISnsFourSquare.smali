.class public interface abstract Lcom/sec/android/app/sns3/svc/sp/foursquare/api/ISnsFourSquare;
.super Ljava/lang/Object;
.source "ISnsFourSquare.java"

# interfaces
.implements Landroid/os/IInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/foursquare/api/ISnsFourSquare$Stub;
    }
.end annotation


# virtual methods
.method public abstract getApplicationInfo()Ljava/util/Map;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract getCategory(Ljava/lang/String;Lcom/sec/android/app/sns3/svc/sp/foursquare/api/ISnsFourSquareCallbackCategory;)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract getFeed(Lcom/sec/android/app/sns3/svc/sp/foursquare/api/ISnsFourSquareCallbackFeeds;)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract getProfile(Lcom/sec/android/app/sns3/svc/sp/foursquare/api/ISnsFourSquareCallbackUser;)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract getRecommendedVenues(Landroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/foursquare/api/ISnsFourSquareCallbackRecommendedVenues;)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract getSuggestCompletion(Landroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/foursquare/api/ISnsFourSquareCallbackSuggestCompletion;)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract getTrendingVenues(Landroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/foursquare/api/ISnsFourSquareCallbackVenues;)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract getUser(Lcom/sec/android/app/sns3/svc/sp/foursquare/api/ISnsFourSquareCallbackUser;)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract getVenues(Landroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/foursquare/api/ISnsFourSquareCallbackVenues;)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract getVenuesPhotos(Landroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/foursquare/api/ISnsFourSquareCallbackVenuesPhotos;)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract setAuthTokenNExpires(Ljava/lang/String;Ljava/lang/String;)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method
