.class public Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserPosts;
.super Ljava/lang/Object;
.source "SnsFbParserPosts.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserPosts$FacebookPosts;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    return-void
.end method

.method public static parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePosts;
    .locals 9
    .param p0, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 38
    new-instance v7, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePosts;

    invoke-direct {v7}, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePosts;-><init>()V

    .line 40
    .local v7, "postsObj":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePosts;
    const/4 v6, 0x0

    .line 41
    .local v6, "posts":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;
    const/4 v0, 0x0

    .line 44
    .local v0, "curPosts":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;
    :try_start_0
    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 45
    .local v4, "jsonObject":Lorg/json/JSONObject;
    new-instance v3, Lorg/json/JSONArray;

    const-string v8, "data"

    invoke-virtual {v4, v8}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v3, v8}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 47
    .local v3, "ja":Lorg/json/JSONArray;
    if-eqz v3, :cond_1

    .line 48
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v8

    if-ge v2, v8, :cond_1

    .line 49
    invoke-virtual {v3, v2}, Lorg/json/JSONArray;->optString(I)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserPost;->parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;

    move-result-object v5

    .line 51
    .local v5, "newPost":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;
    if-nez v6, :cond_0

    .line 52
    move-object v6, v5

    .line 53
    move-object v0, v6

    .line 48
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 55
    :cond_0
    iput-object v5, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;

    .line 56
    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;

    goto :goto_1

    .line 61
    .end local v2    # "i":I
    .end local v5    # "newPost":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;
    :cond_1
    iput-object v6, v7, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePosts;->mPosts:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;

    .line 63
    const-string v8, "paging"

    invoke-virtual {v4, v8}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 64
    const-string v8, "paging"

    invoke-virtual {v4, v8}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserPaging;->parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePaging;

    move-result-object v8

    iput-object v8, v7, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePosts;->mPaging:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePaging;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 73
    .end local v3    # "ja":Lorg/json/JSONArray;
    .end local v4    # "jsonObject":Lorg/json/JSONObject;
    :cond_2
    :goto_2
    return-object v7

    .line 68
    :catch_0
    move-exception v1

    .line 70
    .local v1, "e":Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_2
.end method
