.class public Lcom/sec/android/app/sns3/svc/sp/twitter/parser/SnsTwParserRelationship;
.super Ljava/lang/Object;
.source "SnsTwParserRelationship.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseRelationship;
    .locals 5
    .param p0, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 30
    new-instance v2, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseRelationship;

    invoke-direct {v2}, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseRelationship;-><init>()V

    .line 33
    .local v2, "response":Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseRelationship;
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    const/4 v4, 0x2

    if-gt v3, v4, :cond_0

    .line 34
    const/4 v2, 0x0

    .line 48
    .end local v2    # "response":Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseRelationship;
    :goto_0
    return-object v2

    .line 38
    .restart local v2    # "response":Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseRelationship;
    :cond_0
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 40
    .local v1, "jsonObj":Lorg/json/JSONObject;
    const-string v3, "relationship"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    .line 42
    const-string v3, "target"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/parser/SnsTwParserTarget;->parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseTarget;

    move-result-object v3

    iput-object v3, v2, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseRelationship;->mTarget:Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseTarget;

    .line 43
    const-string v3, "source"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/parser/SnsTwParserSource;->parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseSource;

    move-result-object v3

    iput-object v3, v2, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseRelationship;->mSource:Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseSource;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 44
    .end local v1    # "jsonObj":Lorg/json/JSONObject;
    :catch_0
    move-exception v0

    .line 45
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method
