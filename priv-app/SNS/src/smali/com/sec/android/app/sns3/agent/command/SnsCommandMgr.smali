.class public Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;
.super Ljava/lang/Thread;
.source "SnsCommandMgr.java"


# static fields
.field public static final MESSAGE_RECEIVE:I = 0xb

.field public static final MESSAGE_SEND:I = 0xa


# instance fields
.field private mCommandMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;",
            ">;"
        }
    .end annotation
.end field

.field private mHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;)Ljava/util/Map;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;->mCommandMap:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;)Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;
    .param p1, "x1"    # Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;->getCurrentCommand(Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;)Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;
    .param p1, "x1"    # Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;
    .param p2, "x2"    # Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;

    .prologue
    .line 33
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;->sendCommnadUnit(Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;)V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;)Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;->getReservedCommand()Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;

    move-result-object v0

    return-object v0
.end method

.method private getCurrentCommand(Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;)Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;
    .locals 5
    .param p1, "req"    # Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;

    .prologue
    .line 210
    iget-object v3, p0, Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;->mCommandMap:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 211
    .local v1, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    const/4 v0, 0x0

    .line 213
    .local v0, "cmd":Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;
    :cond_0
    if-eqz v1, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 214
    iget-object v3, p0, Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;->mCommandMap:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "cmd":Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;
    check-cast v0, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;

    .line 215
    .restart local v0    # "cmd":Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->getCurrentCommandUnit()Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;

    move-result-object v2

    .line 217
    .local v2, "unit":Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;
    invoke-virtual {v2}, Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;->getRequestMap()Ljava/util/Map;

    move-result-object v3

    invoke-interface {v3, p1}, Ljava/util/Map;->containsValue(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    move-object v3, v0

    .line 221
    .end local v2    # "unit":Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;
    :goto_0
    return-object v3

    :cond_1
    const/4 v3, 0x0

    goto :goto_0
.end method

.method private getReservedCommand()Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;
    .locals 4

    .prologue
    .line 197
    iget-object v2, p0, Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;->mCommandMap:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 198
    .local v1, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    const/4 v0, 0x0

    .line 200
    .local v0, "command":Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;
    :cond_0
    if-eqz v1, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 201
    iget-object v2, p0, Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;->mCommandMap:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "command":Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;
    check-cast v0, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;

    .line 202
    .restart local v0    # "command":Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->getCurrentCommandUnit()Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;->getState()I

    move-result v2

    if-nez v2, :cond_0

    move-object v2, v0

    .line 206
    :goto_0
    return-object v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private sendCommnadUnit(Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;)V
    .locals 4
    .param p1, "curCmd"    # Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;
    .param p2, "cmdUnit"    # Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;

    .prologue
    .line 183
    invoke-virtual {p2}, Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;->getRequestMap()Ljava/util/Map;

    move-result-object v2

    .line 184
    .local v2, "requestMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;>;"
    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 185
    .local v0, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    const/4 v1, 0x0

    .line 187
    .local v1, "req":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    invoke-virtual {p1, p2}, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->setCurrentCommandUnit(Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;)V

    .line 188
    const/4 v3, 0x1

    invoke-virtual {p2, v3}, Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;->setState(I)V

    .line 190
    :goto_0
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 191
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "req":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    check-cast v1, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;

    .line 192
    .restart local v1    # "req":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    invoke-virtual {v1}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->request()Z

    goto :goto_0

    .line 194
    :cond_0
    return-void
.end method


# virtual methods
.method public getCommandMgrHandle()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 179
    iget-object v0, p0, Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method public initialize()V
    .locals 1

    .prologue
    .line 45
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;->mCommandMap:Ljava/util/Map;

    .line 47
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;->start()V

    .line 48
    return-void
.end method

.method public run()V
    .locals 1

    .prologue
    .line 52
    invoke-static {}, Landroid/os/Looper;->prepare()V

    .line 54
    new-instance v0, Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr$1;-><init>(Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;)V

    iput-object v0, p0, Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;->mHandler:Landroid/os/Handler;

    .line 175
    invoke-static {}, Landroid/os/Looper;->loop()V

    .line 176
    return-void
.end method
