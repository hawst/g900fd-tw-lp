.class Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPost$1;
.super Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqGetPost;
.source "SnsFbCmdGetPost.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPost;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Handler;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPost;


# direct methods
.method constructor <init>(Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPost;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;)V
    .locals 0
    .param p2, "x0"    # Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    .param p3, "x1"    # Ljava/lang/String;

    .prologue
    .line 59
    iput-object p1, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPost$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPost;

    invoke-direct {p0, p2, p3}, Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqGetPost;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public onReqRespond(IZIILandroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;)Z
    .locals 20
    .param p1, "reqID"    # I
    .param p2, "bSuccess"    # Z
    .param p3, "httpStatus"    # I
    .param p4, "errorCode"    # I
    .param p5, "reason"    # Landroid/os/Bundle;
    .param p6, "post"    # Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;

    .prologue
    .line 65
    new-instance v14, Landroid/content/ContentValues;

    invoke-direct {v14}, Landroid/content/ContentValues;-><init>()V

    .line 66
    .local v14, "values":Landroid/content/ContentValues;
    new-instance v15, Landroid/content/ContentValues;

    invoke-direct {v15}, Landroid/content/ContentValues;-><init>()V

    .line 67
    .local v15, "valuesComment":Landroid/content/ContentValues;
    new-instance v16, Landroid/content/ContentValues;

    invoke-direct/range {v16 .. v16}, Landroid/content/ContentValues;-><init>()V

    .line 68
    .local v16, "valuesTags":Landroid/content/ContentValues;
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/sns3/SnsApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    .line 70
    .local v3, "cr":Landroid/content/ContentResolver;
    if-eqz p2, :cond_8

    .line 71
    if-eqz p6, :cond_7

    .line 74
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    .line 75
    .local v17, "where":Ljava/lang/StringBuilder;
    const-string v4, "post_id"

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 76
    const-string v4, " = \'"

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 77
    move-object/from16 v0, p6

    iget-object v4, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mPostID:Ljava/lang/String;

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 78
    const-string v4, "\'"

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 80
    sget-object v4, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$Post;->CONTENT_URI:Landroid/net/Uri;

    const/4 v5, 0x0

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v3 .. v8}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 82
    .local v9, "c":Landroid/database/Cursor;
    if-eqz v9, :cond_0

    invoke-interface {v9}, Landroid/database/Cursor;->getCount()I

    move-result v4

    if-lez v4, :cond_0

    .line 83
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    .line 84
    .local v18, "whereComm":Ljava/lang/StringBuilder;
    const-string v4, "target_id"

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 85
    const-string v4, " = \'"

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 86
    move-object/from16 v0, p6

    iget-object v4, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mPostID:Ljava/lang/String;

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 87
    const-string v4, "\'"

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 89
    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    .line 90
    .local v19, "whereTags":Ljava/lang/StringBuilder;
    const-string v4, "target_id"

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 91
    const-string v4, " = \'"

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 92
    move-object/from16 v0, p6

    iget-object v4, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mPostID:Ljava/lang/String;

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 93
    const-string v4, "\'"

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 95
    sget-object v4, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$Post;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v3, v4, v5, v6}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 96
    sget-object v4, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$Tags;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v3, v4, v5, v6}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 97
    sget-object v4, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$Comments;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v3, v4, v5, v6}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 100
    .end local v18    # "whereComm":Ljava/lang/StringBuilder;
    .end local v19    # "whereTags":Ljava/lang/StringBuilder;
    :cond_0
    if-eqz v9, :cond_1

    .line 101
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 104
    :cond_1
    invoke-virtual {v14}, Landroid/content/ContentValues;->clear()V

    .line 106
    const-string v4, "post_id"

    move-object/from16 v0, p6

    iget-object v5, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mPostID:Ljava/lang/String;

    invoke-virtual {v14, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 107
    const-string v4, "from_id"

    move-object/from16 v0, p6

    iget-object v5, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mFrom:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFrom;

    iget-object v5, v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFrom;->mFromID:Ljava/lang/String;

    invoke-virtual {v14, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    const-string v4, "from_name"

    move-object/from16 v0, p6

    iget-object v5, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mFrom:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFrom;

    iget-object v5, v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFrom;->mFromName:Ljava/lang/String;

    invoke-virtual {v14, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    const-string v4, "to_id"

    move-object/from16 v0, p6

    iget-object v5, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mToID:Ljava/lang/String;

    invoke-virtual {v14, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    const-string v4, "to_name"

    move-object/from16 v0, p6

    iget-object v5, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mToName:Ljava/lang/String;

    invoke-virtual {v14, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    const-string v4, "message"

    move-object/from16 v0, p6

    iget-object v5, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mMessage:Ljava/lang/String;

    invoke-virtual {v14, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    const-string v4, "picture"

    move-object/from16 v0, p6

    iget-object v5, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mPicture:Ljava/lang/String;

    invoke-virtual {v14, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    const-string v4, "link"

    move-object/from16 v0, p6

    iget-object v5, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mLink:Ljava/lang/String;

    invoke-virtual {v14, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    const-string v4, "name"

    move-object/from16 v0, p6

    iget-object v5, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mName:Ljava/lang/String;

    invoke-virtual {v14, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    const-string v4, "caption"

    move-object/from16 v0, p6

    iget-object v5, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mCaption:Ljava/lang/String;

    invoke-virtual {v14, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 116
    const-string v4, "source"

    move-object/from16 v0, p6

    iget-object v5, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mSource:Ljava/lang/String;

    invoke-virtual {v14, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    const-string v4, "properties"

    move-object/from16 v0, p6

    iget-object v5, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mProperties:Ljava/lang/String;

    invoke-virtual {v14, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    const-string v4, "icon"

    move-object/from16 v0, p6

    iget-object v5, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mIcon:Ljava/lang/String;

    invoke-virtual {v14, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    const-string v4, "privacy"

    move-object/from16 v0, p6

    iget-object v5, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mPrivacy:Ljava/lang/String;

    invoke-virtual {v14, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    const-string v4, "type"

    move-object/from16 v0, p6

    iget-object v5, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mType:Ljava/lang/String;

    invoke-virtual {v14, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    const-string v4, "actions"

    move-object/from16 v0, p6

    iget-object v5, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mActions:Ljava/lang/String;

    invoke-virtual {v14, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 126
    move-object/from16 v0, p6

    iget-object v4, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mLikeList:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLike;

    if-eqz v4, :cond_2

    .line 127
    move-object/from16 v0, p6

    iget-object v11, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mLikeList:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLike;

    .line 129
    .local v11, "curLike":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLike;
    if-eqz v11, :cond_2

    .line 130
    const-string v4, "likes_name"

    iget-object v5, v11, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLike;->mName:Ljava/lang/String;

    invoke-virtual {v14, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 134
    .end local v11    # "curLike":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLike;
    :cond_2
    move-object/from16 v0, p6

    iget-object v4, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mCommentList:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseComment;

    if-eqz v4, :cond_3

    .line 135
    move-object/from16 v0, p6

    iget-object v10, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mCommentList:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseComment;

    .line 137
    .local v10, "curComment":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseComment;
    :goto_0
    if-eqz v10, :cond_3

    .line 138
    invoke-virtual {v15}, Landroid/content/ContentValues;->clear()V

    .line 140
    const-string v4, "target_id"

    move-object/from16 v0, p6

    iget-object v5, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mPostID:Ljava/lang/String;

    invoke-virtual {v15, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    const-string v4, "comments_id"

    iget-object v5, v10, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseComment;->mCommentID:Ljava/lang/String;

    invoke-virtual {v15, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    const-string v4, "from_id"

    iget-object v5, v10, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseComment;->mFrom:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFrom;

    iget-object v5, v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFrom;->mFromID:Ljava/lang/String;

    invoke-virtual {v15, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    const-string v4, "from_name"

    iget-object v5, v10, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseComment;->mFrom:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFrom;

    iget-object v5, v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFrom;->mFromName:Ljava/lang/String;

    invoke-virtual {v15, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    const-string v4, "message"

    iget-object v5, v10, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseComment;->mMessage:Ljava/lang/String;

    invoke-virtual {v15, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 145
    const-string v4, "created_time"

    iget-object v5, v10, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseComment;->mCreatedTime:Ljava/lang/String;

    invoke-static {v5}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->toTimestamp(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v15, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 147
    const-string v4, "can_remove"

    iget-object v5, v10, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseComment;->mCanRemove:Ljava/lang/String;

    invoke-virtual {v15, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    const-string v4, "likes"

    iget-object v5, v10, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseComment;->mLikes:Ljava/lang/String;

    invoke-virtual {v15, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 149
    const-string v4, "user_likes"

    iget-object v5, v10, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseComment;->mUserLikes:Ljava/lang/String;

    invoke-virtual {v15, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 150
    const-string v4, "_actor_photo_url"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "https://graph.facebook.com/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v10, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseComment;->mFrom:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFrom;

    iget-object v6, v6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFrom;->mFromID:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/picture?type=normal"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v15, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 153
    sget-object v4, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$Comments;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v3, v4, v15}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 155
    iget-object v10, v10, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseComment;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseComment;

    goto :goto_0

    .line 159
    .end local v10    # "curComment":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseComment;
    :cond_3
    move-object/from16 v0, p6

    iget-object v4, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mPlace:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePlace;

    if-eqz v4, :cond_4

    .line 160
    move-object/from16 v0, p6

    iget-object v13, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mPlace:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePlace;

    .line 162
    .local v13, "place":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePlace;
    const-string v4, "place_id"

    iget-object v5, v13, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePlace;->mPlaceID:Ljava/lang/String;

    invoke-virtual {v14, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    const-string v4, "place_name"

    iget-object v5, v13, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePlace;->mPlaceName:Ljava/lang/String;

    invoke-virtual {v14, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 164
    const-string v4, "street"

    iget-object v5, v13, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePlace;->mStreet:Ljava/lang/String;

    invoke-virtual {v14, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 165
    const-string v4, "city"

    iget-object v5, v13, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePlace;->mCity:Ljava/lang/String;

    invoke-virtual {v14, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 166
    const-string v4, "state"

    iget-object v5, v13, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePlace;->mState:Ljava/lang/String;

    invoke-virtual {v14, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 167
    const-string v4, "country"

    iget-object v5, v13, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePlace;->mCountry:Ljava/lang/String;

    invoke-virtual {v14, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    const-string v4, "zip"

    iget-object v5, v13, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePlace;->mZip:Ljava/lang/String;

    invoke-virtual {v14, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 169
    const-string v4, "latitude"

    iget-object v5, v13, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePlace;->mLatitude:Ljava/lang/String;

    invoke-virtual {v14, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 170
    const-string v4, "longitude"

    iget-object v5, v13, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePlace;->mLongitude:Ljava/lang/String;

    invoke-virtual {v14, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    .end local v13    # "place":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePlace;
    :cond_4
    move-object/from16 v0, p6

    iget-object v4, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mTags:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTags;

    if-eqz v4, :cond_5

    .line 175
    move-object/from16 v0, p6

    iget-object v12, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mTags:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTags;

    .line 177
    .local v12, "curTag":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTags;
    :goto_1
    if-eqz v12, :cond_5

    .line 178
    invoke-virtual/range {v16 .. v16}, Landroid/content/ContentValues;->clear()V

    .line 180
    const-string v4, "profile_id"

    iget-object v5, v12, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTags;->mTagID:Ljava/lang/String;

    move-object/from16 v0, v16

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 181
    const-string v4, "tagged_name"

    iget-object v5, v12, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTags;->mTagName:Ljava/lang/String;

    move-object/from16 v0, v16

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 182
    const-string v4, "created_time"

    iget-object v5, v12, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTags;->mCreatedTime:Ljava/lang/String;

    invoke-static {v5}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->toTimestamp(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v5

    move-object/from16 v0, v16

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 183
    const-string v4, "target_id"

    move-object/from16 v0, p6

    iget-object v5, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mPostID:Ljava/lang/String;

    move-object/from16 v0, v16

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 184
    const-string v4, "target_type"

    const-string v5, "post"

    move-object/from16 v0, v16

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 186
    sget-object v4, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$Tags;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, v16

    invoke-virtual {v3, v4, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 188
    iget-object v12, v12, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTags;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTags;

    goto :goto_1

    .line 193
    .end local v12    # "curTag":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTags;
    :cond_5
    const-string v4, "likes_count"

    move-object/from16 v0, p6

    iget-object v5, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mLikeCount:Ljava/lang/String;

    invoke-virtual {v14, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 194
    const-string v4, "comments_count"

    move-object/from16 v0, p6

    iget-object v5, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mCommentCount:Ljava/lang/String;

    invoke-virtual {v14, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 195
    const-string v4, "object_id"

    move-object/from16 v0, p6

    iget-object v5, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mObjectID:Ljava/lang/String;

    invoke-virtual {v14, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 197
    move-object/from16 v0, p6

    iget-object v4, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mApplication:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseApplication;

    if-eqz v4, :cond_6

    .line 198
    const-string v4, "application_id"

    move-object/from16 v0, p6

    iget-object v5, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mApplication:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseApplication;

    iget-object v5, v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseApplication;->mID:Ljava/lang/String;

    invoke-virtual {v14, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 199
    const-string v4, "application_name"

    move-object/from16 v0, p6

    iget-object v5, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mApplication:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseApplication;

    iget-object v5, v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseApplication;->mName:Ljava/lang/String;

    invoke-virtual {v14, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 202
    :cond_6
    const-string v4, "likes_count"

    move-object/from16 v0, p6

    iget-object v5, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mLikeCount:Ljava/lang/String;

    invoke-virtual {v14, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 203
    const-string v4, "targeting"

    move-object/from16 v0, p6

    iget-object v5, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mTargeting:Ljava/lang/String;

    invoke-virtual {v14, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 204
    const-string v4, "_actor_photo_url"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "https://graph.facebook.com/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p6

    iget-object v6, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mFrom:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFrom;

    iget-object v6, v6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFrom;->mFromID:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/picture?type=normal"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v14, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 207
    const-string v4, "create_time"

    move-object/from16 v0, p6

    iget-object v5, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mCreatedTime:Ljava/lang/String;

    invoke-static {v5}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->toTimestamp(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v14, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 208
    const-string v4, "updated_time"

    move-object/from16 v0, p6

    iget-object v5, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mUpdatedTime:Ljava/lang/String;

    invoke-static {v5}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->toTimestamp(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v14, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 210
    sget-object v4, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$Post;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v3, v4, v14}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 212
    .end local v9    # "c":Landroid/database/Cursor;
    .end local v17    # "where":Ljava/lang/StringBuilder;
    :cond_7
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPost$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPost;

    sget-object v5, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$Post;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v5}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPost;->setUri(Ljava/lang/String;)V

    .line 219
    :goto_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPost$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPost;

    move/from16 v0, p2

    invoke-virtual {v4, v0}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPost;->setSuccess(Z)V

    .line 220
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPost$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPost;

    move-object/from16 v0, p0

    invoke-virtual {v4, v0}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPost;->receive(Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;)V

    .line 222
    const/4 v4, 0x1

    return v4

    .line 214
    :cond_8
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPost$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPost;

    new-instance v5, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;

    const-string v6, "facebook"

    move/from16 v0, p3

    move/from16 v1, p4

    move-object/from16 v2, p5

    invoke-direct {v5, v6, v0, v1, v2}, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;-><init>(Ljava/lang/String;IILandroid/os/Bundle;)V

    invoke-virtual {v4, v5}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPost;->setResponseList(Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;)Z

    .line 216
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPost$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPost;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPost;->setUri(Ljava/lang/String;)V

    goto :goto_2
.end method
