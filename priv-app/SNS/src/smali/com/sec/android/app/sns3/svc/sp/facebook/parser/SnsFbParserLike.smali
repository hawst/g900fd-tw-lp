.class public Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserLike;
.super Ljava/lang/Object;
.source "SnsFbParserLike.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserLike$FacebookLike;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    return-void
.end method

.method public static parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLike;
    .locals 4
    .param p0, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 42
    new-instance v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLike;

    invoke-direct {v2}, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLike;-><init>()V

    .line 45
    .local v2, "like":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLike;
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 47
    .local v1, "jsonObject":Lorg/json/JSONObject;
    const-string v3, "id"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLike;->mLikeID:Ljava/lang/String;

    .line 48
    const-string v3, "name"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLike;->mName:Ljava/lang/String;

    .line 49
    const-string v3, "category"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLike;->mCategory:Ljava/lang/String;

    .line 50
    const-string v3, "create_time"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLike;->mCreatedTime:Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 57
    .end local v1    # "jsonObject":Lorg/json/JSONObject;
    :goto_0
    return-object v2

    .line 52
    :catch_0
    move-exception v0

    .line 54
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method
