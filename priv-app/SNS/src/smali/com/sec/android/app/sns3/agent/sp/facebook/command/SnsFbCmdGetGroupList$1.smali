.class Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetGroupList$1;
.super Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqGetGroupList;
.source "SnsFbCmdGetGroupList.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetGroupList;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Handler;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetGroupList;


# direct methods
.method constructor <init>(Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetGroupList;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;)V
    .locals 0
    .param p2, "x0"    # Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    .param p3, "x1"    # Ljava/lang/String;

    .prologue
    .line 50
    iput-object p1, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetGroupList$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetGroupList;

    invoke-direct {p0, p2, p3}, Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqGetGroupList;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public onReqRespond(IZIILandroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseGroupList;)Z
    .locals 7
    .param p1, "reqID"    # I
    .param p2, "bSuccess"    # Z
    .param p3, "httpStatus"    # I
    .param p4, "errorCode"    # I
    .param p5, "reason"    # Landroid/os/Bundle;
    .param p6, "grouplist"    # Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseGroupList;

    .prologue
    const/4 v6, 0x0

    .line 55
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 56
    .local v2, "values":Landroid/content/ContentValues;
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/sns3/SnsApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 58
    .local v0, "cr":Landroid/content/ContentResolver;
    sget-object v3, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$Group;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v3, v6, v6}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 60
    if-eqz p2, :cond_1

    .line 61
    if-eqz p6, :cond_0

    .line 62
    iget-object v1, p6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseGroupList;->mGroups:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseGroup;

    .line 64
    .local v1, "curGroup":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseGroup;
    :goto_0
    if-eqz v1, :cond_0

    .line 66
    invoke-virtual {v2}, Landroid/content/ContentValues;->clear()V

    .line 67
    const-string v3, "versioin"

    iget-object v4, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseGroup;->mVersion:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    const-string v3, "name"

    iget-object v4, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseGroup;->mGroupName:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    const-string v3, "id"

    iget-object v4, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseGroup;->mGroupName:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    const-string v3, "administrator"

    iget-object v4, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseGroup;->mAdministrator:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    const-string v3, "bookmark_order"

    iget-object v4, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseGroup;->mBookmarkOrder:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    sget-object v3, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$Group;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v3, v2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 75
    iget-object v1, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseGroup;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseGroup;

    goto :goto_0

    .line 78
    .end local v1    # "curGroup":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseGroup;
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetGroupList$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetGroupList;

    sget-object v4, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$Group;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetGroupList;->setUri(Ljava/lang/String;)V

    .line 85
    :goto_1
    iget-object v3, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetGroupList$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetGroupList;

    invoke-virtual {v3, p2}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetGroupList;->setSuccess(Z)V

    .line 86
    iget-object v3, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetGroupList$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetGroupList;

    invoke-virtual {v3, p0}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetGroupList;->receive(Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;)V

    .line 88
    const/4 v3, 0x1

    return v3

    .line 80
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetGroupList$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetGroupList;

    new-instance v4, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;

    const-string v5, "facebook"

    invoke-direct {v4, v5, p3, p4, p5}, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;-><init>(Ljava/lang/String;IILandroid/os/Bundle;)V

    invoke-virtual {v3, v4}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetGroupList;->setResponseList(Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;)Z

    .line 82
    iget-object v3, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetGroupList$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetGroupList;

    invoke-virtual {v3, v6}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetGroupList;->setUri(Ljava/lang/String;)V

    goto :goto_1
.end method
