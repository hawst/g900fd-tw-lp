.class public Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdAuthLogin;
.super Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;
.source "SnsFsCmdAuthLogin.java"


# direct methods
.method public constructor <init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Handler;Landroid/os/Bundle;)V
    .locals 3
    .param p1, "svcMgr"    # Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    .param p2, "cmdHandler"    # Landroid/os/Handler;
    .param p3, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 39
    invoke-direct {p0, p2}, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;-><init>(Landroid/os/Handler;)V

    .line 41
    new-instance v1, Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;

    invoke-direct {v1}, Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;-><init>()V

    .line 43
    .local v1, "unit":Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;
    new-instance v0, Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdAuthLogin$1;

    const-string v2, "self"

    invoke-direct {v0, p0, p1, v2}, Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdAuthLogin$1;-><init>(Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdAuthLogin;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;)V

    .line 83
    .local v0, "req":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    invoke-virtual {v1, v0}, Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;->addRequest(Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;)V

    .line 85
    invoke-virtual {p0, v1}, Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdAuthLogin;->addCommandUnit(Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;)V

    .line 86
    return-void
.end method


# virtual methods
.method protected respond()Z
    .locals 5

    .prologue
    .line 90
    const-string v0, "SnsAgent"

    const-string v1, "<SnsFsCmdAuthLogin> respond()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 92
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdAuthLogin;->getCommandCallback()Lcom/sec/android/app/sns3/agent/command/ISnsCommandCallback;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdAuthLogin;->getCommandID()I

    move-result v1

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdAuthLogin;->isSuccess()Z

    move-result v2

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdAuthLogin;->getUri()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdAuthLogin;->getResponseList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/sec/android/app/sns3/agent/command/ISnsCommandCallback;->onCmdRespond(IZLjava/lang/String;Ljava/util/List;)V

    .line 94
    const/4 v0, 0x1

    return v0
.end method
