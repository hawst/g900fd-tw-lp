.class public Lcom/sec/android/app/sns3/svc/sp/sinaweibo/parser/SnsSwParserFrom;
.super Ljava/lang/Object;
.source "SnsSwParserFrom.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/sinaweibo/parser/SnsSwParserFrom$SinaweiboFrom;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    return-void
.end method

.method public static parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/sinaweibo/response/SnsSwResponseFrom;
    .locals 4
    .param p0, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 36
    new-instance v1, Lcom/sec/android/app/sns3/svc/sp/sinaweibo/response/SnsSwResponseFrom;

    invoke-direct {v1}, Lcom/sec/android/app/sns3/svc/sp/sinaweibo/response/SnsSwResponseFrom;-><init>()V

    .line 39
    .local v1, "from":Lcom/sec/android/app/sns3/svc/sp/sinaweibo/response/SnsSwResponseFrom;
    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 41
    .local v2, "jsonObject":Lorg/json/JSONObject;
    const-string v3, "id"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/sec/android/app/sns3/svc/sp/sinaweibo/response/SnsSwResponseFrom;->mFromID:Ljava/lang/String;

    .line 42
    const-string v3, "name"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/sec/android/app/sns3/svc/sp/sinaweibo/response/SnsSwResponseFrom;->mFromName:Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 48
    .end local v2    # "jsonObject":Lorg/json/JSONObject;
    :goto_0
    return-object v1

    .line 43
    :catch_0
    move-exception v0

    .line 45
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method
