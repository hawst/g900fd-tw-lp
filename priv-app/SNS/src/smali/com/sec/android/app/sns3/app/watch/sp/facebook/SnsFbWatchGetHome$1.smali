.class final Lcom/sec/android/app/sns3/app/watch/sp/facebook/SnsFbWatchGetHome$1;
.super Ljava/lang/Object;
.source "SnsFbWatchGetHome.java"

# interfaces
.implements Lcom/sec/android/app/sns3/agent/command/ISnsCommandCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/sns3/app/watch/sp/facebook/SnsFbWatchGetHome;->loadSnsFbCmdGetHome(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 58
    iput-object p1, p0, Lcom/sec/android/app/sns3/app/watch/sp/facebook/SnsFbWatchGetHome$1;->val$context:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCmdRespond(IZLjava/lang/String;Ljava/util/List;)V
    .locals 3
    .param p1, "cmdID"    # I
    .param p2, "bSuccess"    # Z
    .param p3, "uri"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IZ",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 64
    .local p4, "responseList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;>;"
    sget-object v0, Lcom/sec/android/app/sns3/app/watch/sp/facebook/SnsFbWatchResource;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SnsFbWatchGetHome : loadSnsFbCmdGetHome - onCmdRespond() return "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 66
    iget-object v0, p0, Lcom/sec/android/app/sns3/app/watch/sp/facebook/SnsFbWatchGetHome$1;->val$context:Landroid/content/Context;

    # invokes: Lcom/sec/android/app/sns3/app/watch/sp/facebook/SnsFbWatchGetHome;->invokeBroadcast(Landroid/content/Context;ZLjava/lang/String;)V
    invoke-static {v0, p2, p3}, Lcom/sec/android/app/sns3/app/watch/sp/facebook/SnsFbWatchGetHome;->access$000(Landroid/content/Context;ZLjava/lang/String;)V

    .line 67
    return-void
.end method
