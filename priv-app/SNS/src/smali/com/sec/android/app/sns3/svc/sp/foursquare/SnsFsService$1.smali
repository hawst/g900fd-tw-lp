.class Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService$1;
.super Lcom/sec/android/app/sns3/svc/sp/foursquare/api/ISnsFourSquare$Stub;
.source "SnsFsService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService;


# direct methods
.method constructor <init>(Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService;)V
    .locals 0

    .prologue
    .line 93
    iput-object p1, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService$1;->this$0:Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService;

    invoke-direct {p0}, Lcom/sec/android/app/sns3/svc/sp/foursquare/api/ISnsFourSquare$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public getApplicationInfo()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 206
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 207
    .local v0, "appInfo":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v1, "client_id"

    sget-object v2, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFourSquare;->CLIENT_ID:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208
    const-string v1, "redirect_uri"

    const-string v2, "x-oauthflow-foursquare://callback"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 211
    return-object v0
.end method

.method public getCategory(Ljava/lang/String;Lcom/sec/android/app/sns3/svc/sp/foursquare/api/ISnsFourSquareCallbackCategory;)I
    .locals 9
    .param p1, "categoryId"    # Ljava/lang/String;
    .param p2, "cb"    # Lcom/sec/android/app/sns3/svc/sp/foursquare/api/ISnsFourSquareCallbackCategory;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    .line 326
    const-string v0, "SNS"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getCategories CALLED!!! cb: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 328
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService$1;->this$0:Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService;

    # invokes: Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService;->findCategoryById(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/foursquare/response/Category;
    invoke-static {v0, p1}, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService;->access$200(Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService;Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/foursquare/response/Category;

    move-result-object v6

    .line 329
    .local v6, "category":Lcom/sec/android/app/sns3/svc/sp/foursquare/response/Category;
    if-nez v6, :cond_0

    .line 331
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService$1;->this$0:Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService;

    # invokes: Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService;->getCategories(Lcom/sec/android/app/sns3/svc/sp/foursquare/api/ISnsFourSquareCallbackCategory;Ljava/lang/String;)I
    invoke-static {v0, p2, p1}, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService;->access$300(Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService;Lcom/sec/android/app/sns3/svc/sp/foursquare/api/ISnsFourSquareCallbackCategory;Ljava/lang/String;)I

    move-result v0

    .line 340
    :goto_0
    return v0

    .line 333
    :cond_0
    if-eqz p2, :cond_1

    .line 334
    const/4 v1, 0x0

    const/4 v2, 0x1

    const/16 v3, 0xc8

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p2

    invoke-interface/range {v0 .. v6}, Lcom/sec/android/app/sns3/svc/sp/foursquare/api/ISnsFourSquareCallbackCategory;->onResponse(IZIILandroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/foursquare/response/Category;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    move v0, v8

    .line 340
    goto :goto_0

    .line 337
    .end local v6    # "category":Lcom/sec/android/app/sns3/svc/sp/foursquare/response/Category;
    :catch_0
    move-exception v7

    .line 338
    .local v7, "ex":Ljava/lang/IllegalArgumentException;
    new-instance v0, Landroid/os/RemoteException;

    invoke-virtual {v7}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getFeed(Lcom/sec/android/app/sns3/svc/sp/foursquare/api/ISnsFourSquareCallbackFeeds;)I
    .locals 6
    .param p1, "cb"    # Lcom/sec/android/app/sns3/svc/sp/foursquare/api/ISnsFourSquareCallbackFeeds;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 183
    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService$1;->this$0:Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService;

    # invokes: Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService;->ensureAccountAdded()V
    invoke-static {v1}, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService;->access$100(Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService;)V

    .line 184
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService$1$4;

    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService$1;->this$0:Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService;

    # getter for: Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    invoke-static {v1}, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService;->access$000(Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService;)Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v2

    const-string v3, "self"

    const/4 v4, 0x0

    move-object v1, p0

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService$1$4;-><init>(Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService$1;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;Landroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/foursquare/api/ISnsFourSquareCallbackFeeds;)V

    .line 199
    .local v0, "req":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->request()Z

    .line 201
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->getReqID()I

    move-result v1

    return v1
.end method

.method public getProfile(Lcom/sec/android/app/sns3/svc/sp/foursquare/api/ISnsFourSquareCallbackUser;)I
    .locals 6
    .param p1, "cb"    # Lcom/sec/android/app/sns3/svc/sp/foursquare/api/ISnsFourSquareCallbackUser;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 132
    const-string v1, "SNS"

    const-string v2, "getProfile CALLED!!!"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 133
    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService$1;->this$0:Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService;

    # invokes: Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService;->ensureAccountAdded()V
    invoke-static {v1}, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService;->access$100(Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService;)V

    .line 134
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService$1$2;

    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService$1;->this$0:Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService;

    # getter for: Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    invoke-static {v1}, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService;->access$000(Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService;)Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v2

    const-string v3, "self"

    const/4 v4, 0x0

    move-object v1, p0

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService$1$2;-><init>(Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService$1;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;Landroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/foursquare/api/ISnsFourSquareCallbackUser;)V

    .line 148
    .local v0, "req":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->request()Z

    .line 150
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->getReqID()I

    move-result v1

    return v1
.end method

.method public getRecommendedVenues(Landroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/foursquare/api/ISnsFourSquareCallbackRecommendedVenues;)I
    .locals 5
    .param p1, "bundle"    # Landroid/os/Bundle;
    .param p2, "cb"    # Lcom/sec/android/app/sns3/svc/sp/foursquare/api/ISnsFourSquareCallbackRecommendedVenues;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 217
    const-string v2, "SNS"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getRecommendedVenues CALLED!!! cb: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " bundle: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 219
    :try_start_0
    new-instance v1, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService$1$5;

    iget-object v2, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService$1;->this$0:Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService;

    # getter for: Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    invoke-static {v2}, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService;->access$000(Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService;)Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v2

    invoke-direct {v1, p0, v2, p1, p2}, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService$1$5;-><init>(Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService$1;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/foursquare/api/ISnsFourSquareCallbackRecommendedVenues;)V

    .line 233
    .local v1, "req":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    invoke-virtual {v1}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->request()Z

    .line 235
    invoke-virtual {v1}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->getReqID()I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    return v2

    .line 236
    .end local v1    # "req":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    :catch_0
    move-exception v0

    .line 237
    .local v0, "ex":Ljava/lang/IllegalArgumentException;
    new-instance v2, Landroid/os/RemoteException;

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public getSuggestCompletion(Landroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/foursquare/api/ISnsFourSquareCallbackSuggestCompletion;)I
    .locals 5
    .param p1, "bundle"    # Landroid/os/Bundle;
    .param p2, "cb"    # Lcom/sec/android/app/sns3/svc/sp/foursquare/api/ISnsFourSquareCallbackSuggestCompletion;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 271
    const-string v2, "SNS"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getSuggestCompletion CALLED!!! cb: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " bundle: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 273
    :try_start_0
    new-instance v1, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService$1$7;

    iget-object v2, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService$1;->this$0:Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService;

    # getter for: Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    invoke-static {v2}, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService;->access$000(Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService;)Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v2

    invoke-direct {v1, p0, v2, p1, p2}, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService$1$7;-><init>(Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService$1;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/foursquare/api/ISnsFourSquareCallbackSuggestCompletion;)V

    .line 287
    .local v1, "req":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    invoke-virtual {v1}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->request()Z

    .line 289
    invoke-virtual {v1}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->getReqID()I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    return v2

    .line 290
    .end local v1    # "req":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    :catch_0
    move-exception v0

    .line 291
    .local v0, "ex":Ljava/lang/IllegalArgumentException;
    new-instance v2, Landroid/os/RemoteException;

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public getTrendingVenues(Landroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/foursquare/api/ISnsFourSquareCallbackVenues;)I
    .locals 5
    .param p1, "bundle"    # Landroid/os/Bundle;
    .param p2, "cb"    # Lcom/sec/android/app/sns3/svc/sp/foursquare/api/ISnsFourSquareCallbackVenues;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 244
    const-string v2, "SNS"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getTrendingVenues CALLED!!! cb: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " bundle: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 246
    :try_start_0
    new-instance v1, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService$1$6;

    iget-object v2, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService$1;->this$0:Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService;

    # getter for: Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    invoke-static {v2}, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService;->access$000(Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService;)Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v2

    invoke-direct {v1, p0, v2, p1, p2}, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService$1$6;-><init>(Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService$1;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/foursquare/api/ISnsFourSquareCallbackVenues;)V

    .line 260
    .local v1, "req":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    invoke-virtual {v1}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->request()Z

    .line 262
    invoke-virtual {v1}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->getReqID()I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    return v2

    .line 263
    .end local v1    # "req":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    :catch_0
    move-exception v0

    .line 264
    .local v0, "ex":Ljava/lang/IllegalArgumentException;
    new-instance v2, Landroid/os/RemoteException;

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public getUser(Lcom/sec/android/app/sns3/svc/sp/foursquare/api/ISnsFourSquareCallbackUser;)I
    .locals 3
    .param p1, "cb"    # Lcom/sec/android/app/sns3/svc/sp/foursquare/api/ISnsFourSquareCallbackUser;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 108
    const-string v1, "SNS"

    const-string v2, "getUser CALLED!!!"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 109
    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService$1;->this$0:Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService;

    # invokes: Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService;->ensureAccountAdded()V
    invoke-static {v1}, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService;->access$100(Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService;)V

    .line 111
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService$1$1;

    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService$1;->this$0:Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService;

    # getter for: Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    invoke-static {v1}, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService;->access$000(Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService;)Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v1

    const-string v2, "self"

    invoke-direct {v0, p0, v1, v2, p1}, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService$1$1;-><init>(Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService$1;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;Lcom/sec/android/app/sns3/svc/sp/foursquare/api/ISnsFourSquareCallbackUser;)V

    .line 125
    .local v0, "req":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->request()Z

    .line 127
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->getReqID()I

    move-result v1

    return v1
.end method

.method public getVenues(Landroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/foursquare/api/ISnsFourSquareCallbackVenues;)I
    .locals 5
    .param p1, "bundle"    # Landroid/os/Bundle;
    .param p2, "cb"    # Lcom/sec/android/app/sns3/svc/sp/foursquare/api/ISnsFourSquareCallbackVenues;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 157
    const-string v2, "SNS"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getVenues CALLED!!! cb: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " bundle: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 159
    :try_start_0
    new-instance v1, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService$1$3;

    iget-object v2, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService$1;->this$0:Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService;

    # getter for: Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    invoke-static {v2}, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService;->access$000(Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService;)Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v2

    invoke-direct {v1, p0, v2, p1, p2}, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService$1$3;-><init>(Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService$1;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/foursquare/api/ISnsFourSquareCallbackVenues;)V

    .line 173
    .local v1, "req":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    invoke-virtual {v1}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->request()Z

    .line 175
    invoke-virtual {v1}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->getReqID()I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    return v2

    .line 176
    .end local v1    # "req":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    :catch_0
    move-exception v0

    .line 177
    .local v0, "ex":Ljava/lang/IllegalArgumentException;
    new-instance v2, Landroid/os/RemoteException;

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public getVenuesPhotos(Landroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/foursquare/api/ISnsFourSquareCallbackVenuesPhotos;)I
    .locals 5
    .param p1, "bundle"    # Landroid/os/Bundle;
    .param p2, "cb"    # Lcom/sec/android/app/sns3/svc/sp/foursquare/api/ISnsFourSquareCallbackVenuesPhotos;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 298
    const-string v2, "SNS"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getVenuesPhotos CALLED!!! cb: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " bundle: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 301
    :try_start_0
    new-instance v1, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService$1$8;

    iget-object v2, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService$1;->this$0:Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService;

    # getter for: Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    invoke-static {v2}, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService;->access$000(Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService;)Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v2

    invoke-direct {v1, p0, v2, p1, p2}, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService$1$8;-><init>(Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService$1;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/foursquare/api/ISnsFourSquareCallbackVenuesPhotos;)V

    .line 315
    .local v1, "req":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    invoke-virtual {v1}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->request()Z

    .line 317
    invoke-virtual {v1}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->getReqID()I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    return v2

    .line 318
    .end local v1    # "req":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    :catch_0
    move-exception v0

    .line 319
    .local v0, "ex":Ljava/lang/IllegalArgumentException;
    new-instance v2, Landroid/os/RemoteException;

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public setAuthTokenNExpires(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3
    .param p1, "accessToken"    # Ljava/lang/String;
    .param p2, "expires"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 98
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 99
    :cond_0
    const/4 v1, 0x0

    .line 103
    :goto_0
    return v1

    .line 101
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService$1;->this$0:Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService;

    # getter for: Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    invoke-static {v1}, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService;->access$000(Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService;)Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/sns3/svc/SnsSvcMgr;->getTokenMgr()Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;

    move-result-object v1

    const-string v2, "foursquare"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;->getToken(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsToken;

    .line 102
    .local v0, "fsToken":Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsToken;
    invoke-virtual {v0, p1}, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsToken;->setAccessToken(Ljava/lang/String;)V

    .line 103
    const/4 v1, 0x1

    goto :goto_0
.end method
