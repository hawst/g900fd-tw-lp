.class public interface abstract Lcom/sec/android/app/sns3/agent/sp/qzone/db/SnsQzoneDB$UserTimelineColumns;
.super Ljava/lang/Object;
.source "SnsQzoneDB.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/agent/sp/qzone/db/SnsQzoneDB;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "UserTimelineColumns"
.end annotation


# static fields
.field public static final AUTHOR_NAME:Ljava/lang/String; = "author_name"

.field public static final AUTHOR_OPENID:Ljava/lang/String; = "author_openid"

.field public static final CREATE_AT:Ljava/lang/String; = "create_at"

.field public static final FEED_ID:Ljava/lang/String; = "feed_id"

.field public static final LINK_URI:Ljava/lang/String; = "link_uri"

.field public static final MESSAGE:Ljava/lang/String; = "message"

.field public static final PICTURE:Ljava/lang/String; = "picture"
