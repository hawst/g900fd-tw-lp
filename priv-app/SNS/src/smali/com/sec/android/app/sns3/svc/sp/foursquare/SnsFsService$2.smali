.class Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService$2;
.super Lcom/sec/android/app/sns3/svc/sp/foursquare/request/SnsFsReqGetCategories;
.source "SnsFsService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService;->getCategories(Lcom/sec/android/app/sns3/svc/sp/foursquare/api/ISnsFourSquareCallbackCategory;Ljava/lang/String;)I
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private categoryItemFound:Z

.field final synthetic this$0:Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService;

.field final synthetic val$categoryId:Ljava/lang/String;

.field final synthetic val$cb:Lcom/sec/android/app/sns3/svc/sp/foursquare/api/ISnsFourSquareCallbackCategory;

.field final synthetic val$cr:Landroid/content/ContentResolver;


# direct methods
.method constructor <init>(Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Bundle;Ljava/lang/String;Lcom/sec/android/app/sns3/svc/sp/foursquare/api/ISnsFourSquareCallbackCategory;Landroid/content/ContentResolver;)V
    .locals 1
    .param p2, "x0"    # Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    .param p3, "x1"    # Landroid/os/Bundle;

    .prologue
    .line 348
    iput-object p1, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService$2;->this$0:Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService;

    iput-object p4, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService$2;->val$categoryId:Ljava/lang/String;

    iput-object p5, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService$2;->val$cb:Lcom/sec/android/app/sns3/svc/sp/foursquare/api/ISnsFourSquareCallbackCategory;

    iput-object p6, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService$2;->val$cr:Landroid/content/ContentResolver;

    invoke-direct {p0, p2, p3}, Lcom/sec/android/app/sns3/svc/sp/foursquare/request/SnsFsReqGetCategories;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Bundle;)V

    .line 349
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService$2;->categoryItemFound:Z

    return-void
.end method


# virtual methods
.method public onReqRespond(IZIILandroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseCategories;)Z
    .locals 25
    .param p1, "reqID"    # I
    .param p2, "bSuccess"    # Z
    .param p3, "httpStatus"    # I
    .param p4, "errorCode"    # I
    .param p5, "reason"    # Landroid/os/Bundle;
    .param p6, "categories"    # Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseCategories;

    .prologue
    .line 355
    if-eqz p2, :cond_5

    .line 356
    :try_start_0
    new-instance v24, Landroid/content/ContentValues;

    invoke-direct/range {v24 .. v24}, Landroid/content/ContentValues;-><init>()V

    .line 357
    .local v24, "values":Landroid/content/ContentValues;
    move-object/from16 v0, p6

    iget-object v1, v0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseCategories;->mCategories:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v22

    .local v22, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/Category;

    .line 358
    .local v7, "categoryItem":Lcom/sec/android/app/sns3/svc/sp/foursquare/response/Category;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService$2;->val$categoryId:Ljava/lang/String;

    iget-object v2, v7, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/Category;->mId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 359
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService$2;->val$cb:Lcom/sec/android/app/sns3/svc/sp/foursquare/api/ISnsFourSquareCallbackCategory;

    if-eqz v1, :cond_1

    .line 360
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService$2;->val$cb:Lcom/sec/android/app/sns3/svc/sp/foursquare/api/ISnsFourSquareCallbackCategory;

    move/from16 v2, p1

    move/from16 v3, p2

    move/from16 v4, p3

    move/from16 v5, p4

    move-object/from16 v6, p5

    invoke-interface/range {v1 .. v7}, Lcom/sec/android/app/sns3/svc/sp/foursquare/api/ISnsFourSquareCallbackCategory;->onResponse(IZIILandroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/foursquare/response/Category;)V

    .line 363
    :cond_1
    const/4 v1, 0x1

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService$2;->categoryItemFound:Z

    .line 365
    :cond_2
    const-string v1, "category_id"

    iget-object v2, v7, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/Category;->mId:Ljava/lang/String;

    move-object/from16 v0, v24

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 366
    const-string v1, "category_name"

    iget-object v2, v7, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/Category;->mName:Ljava/lang/String;

    move-object/from16 v0, v24

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 367
    const-string v1, "category_plural_name"

    iget-object v2, v7, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/Category;->mPluralName:Ljava/lang/String;

    move-object/from16 v0, v24

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 369
    const-string v1, "category_icon"

    iget-object v2, v7, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/Category;->mIconUrl:Ljava/lang/String;

    move-object/from16 v0, v24

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 370
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService$2;->val$cr:Landroid/content/ContentResolver;

    sget-object v2, Lcom/sec/android/app/sns3/agent/sp/foursquare/db/SnsFourSquareDB$VenuesCategories;->CONTENT_URI:Landroid/net/Uri;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "category_id=\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v7, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/Category;->mId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    move-object/from16 v0, v24

    invoke-virtual {v1, v2, v0, v3, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v23

    .line 373
    .local v23, "update":I
    if-nez v23, :cond_0

    .line 374
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService$2;->val$cr:Landroid/content/ContentResolver;

    sget-object v2, Lcom/sec/android/app/sns3/agent/sp/foursquare/db/SnsFourSquareDB$VenuesCategories;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, v24

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 393
    .end local v7    # "categoryItem":Lcom/sec/android/app/sns3/svc/sp/foursquare/response/Category;
    .end local v22    # "i$":Ljava/util/Iterator;
    .end local v23    # "update":I
    .end local v24    # "values":Landroid/content/ContentValues;
    :catch_0
    move-exception v21

    .line 394
    .local v21, "e":Landroid/os/RemoteException;
    invoke-virtual/range {v21 .. v21}, Landroid/os/RemoteException;->printStackTrace()V

    .line 396
    .end local v21    # "e":Landroid/os/RemoteException;
    :cond_3
    :goto_1
    const/4 v1, 0x0

    return v1

    .line 378
    .restart local v22    # "i$":Ljava/util/Iterator;
    .restart local v24    # "values":Landroid/content/ContentValues;
    :cond_4
    :try_start_1
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService$2;->categoryItemFound:Z

    if-nez v1, :cond_3

    .line 379
    const-string v1, "SnsFsService"

    const-string v2, "Category Information not retreived from server"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 380
    new-instance v13, Landroid/os/Bundle;

    invoke-direct {v13}, Landroid/os/Bundle;-><init>()V

    .line 381
    .local v13, "reason1":Landroid/os/Bundle;
    const-string v1, "MESSAGE"

    const-string v2, "Category Information not retreived from server"

    invoke-virtual {v13, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 383
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService$2;->val$cb:Lcom/sec/android/app/sns3/svc/sp/foursquare/api/ISnsFourSquareCallbackCategory;

    const/4 v10, 0x0

    const/4 v12, 0x0

    const/4 v14, 0x0

    move/from16 v9, p1

    move/from16 v11, p3

    invoke-interface/range {v8 .. v14}, Lcom/sec/android/app/sns3/svc/sp/foursquare/api/ISnsFourSquareCallbackCategory;->onResponse(IZIILandroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/foursquare/response/Category;)V

    goto :goto_1

    .line 387
    .end local v13    # "reason1":Landroid/os/Bundle;
    .end local v22    # "i$":Ljava/util/Iterator;
    .end local v24    # "values":Landroid/content/ContentValues;
    :cond_5
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService$2;->val$cb:Lcom/sec/android/app/sns3/svc/sp/foursquare/api/ISnsFourSquareCallbackCategory;

    if-eqz v1, :cond_3

    .line 388
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsService$2;->val$cb:Lcom/sec/android/app/sns3/svc/sp/foursquare/api/ISnsFourSquareCallbackCategory;

    const/16 v16, 0x0

    const/16 v20, 0x0

    move/from16 v15, p1

    move/from16 v17, p3

    move/from16 v18, p4

    move-object/from16 v19, p5

    invoke-interface/range {v14 .. v20}, Lcom/sec/android/app/sns3/svc/sp/foursquare/api/ISnsFourSquareCallbackCategory;->onResponse(IZIILandroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/foursquare/response/Category;)V

    .line 389
    const-string v1, "SnsFsService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SnsFsReqGetCategories Failed:  Error code: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, p4

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " bsucess : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, p2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "Status:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, p3

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method
