.class public interface abstract Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$GroupUpdateColumns;
.super Ljava/lang/Object;
.source "SnsLinkedInDB.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "GroupUpdateColumns"
.end annotation


# static fields
.field public static final GROUP_ID:Ljava/lang/String; = "group_id"

.field public static final GROUP_NAME:Ljava/lang/String; = "group_name"

.field public static final GROUP_UPDATE_KEY:Ljava/lang/String; = "group_update_KEY"
