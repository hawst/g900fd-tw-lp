.class public Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetProfileFeeds;
.super Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;
.source "SnsLiCmdGetProfileFeeds.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "SnsLiCmdGetUpdates"


# instance fields
.field private mAuthority:Ljava/lang/String;

.field private mBundleUpdatesQuery:Landroid/os/Bundle;

.field private mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Handler;Landroid/content/Context;)V
    .locals 9
    .param p1, "svcMgr"    # Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    .param p2, "cmdHandler"    # Landroid/os/Handler;
    .param p3, "context"    # Landroid/content/Context;

    .prologue
    .line 49
    invoke-direct {p0, p2}, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;-><init>(Landroid/os/Handler;)V

    .line 42
    const-string v1, "~"

    iput-object v1, p0, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetProfileFeeds;->mAuthority:Ljava/lang/String;

    .line 44
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetProfileFeeds;->mBundleUpdatesQuery:Landroid/os/Bundle;

    .line 50
    new-instance v1, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;

    invoke-direct {v1, p3}, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetProfileFeeds;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;

    .line 51
    new-instance v8, Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;

    invoke-direct {v8}, Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;-><init>()V

    .line 52
    .local v8, "unit":Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;
    iget-object v1, p0, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetProfileFeeds;->mBundleUpdatesQuery:Landroid/os/Bundle;

    const-string v2, "scope"

    const-string v3, "self"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 53
    iget-object v1, p0, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetProfileFeeds;->mBundleUpdatesQuery:Landroid/os/Bundle;

    const-string v2, "after"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetProfileFeeds;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;

    sget-object v5, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$ProfileFeedList;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v4, v5}, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;->getLastUpdateTimeStamp(Landroid/net/Uri;)J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    const/4 v7, 0x0

    .line 57
    .local v7, "requestUpdates":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    :try_start_0
    new-instance v0, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetProfileFeeds$1;

    iget-object v3, p0, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetProfileFeeds;->mAuthority:Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetProfileFeeds;->mBundleUpdatesQuery:Landroid/os/Bundle;

    sget-object v5, Lcom/sec/android/app/sns3/svc/sp/linkedin/request/SnsLiReqGetUpdates;->UPDATE_TYPES_SELF:[Ljava/lang/String;

    move-object v1, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetProfileFeeds$1;-><init>(Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetProfileFeeds;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;Landroid/os/Bundle;[Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 85
    .end local v7    # "requestUpdates":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    .local v0, "requestUpdates":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    :try_start_1
    invoke-virtual {v8, v0}, Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;->addRequest(Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;)V

    .line 86
    invoke-virtual {p0, v8}, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetProfileFeeds;->addCommandUnit(Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 94
    :goto_0
    return-void

    .line 87
    .end local v0    # "requestUpdates":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    .restart local v7    # "requestUpdates":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    :catch_0
    move-exception v6

    move-object v0, v7

    .line 88
    .end local v7    # "requestUpdates":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    .restart local v0    # "requestUpdates":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    .local v6, "e":Ljava/lang/Exception;
    :goto_1
    :try_start_2
    const-string v1, "SnsLiCmdGetUpdates"

    invoke-virtual {v6}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 89
    invoke-virtual {v6}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 90
    .end local v6    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    :goto_2
    throw v1

    .end local v0    # "requestUpdates":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    .restart local v7    # "requestUpdates":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    :catchall_1
    move-exception v1

    move-object v0, v7

    .end local v7    # "requestUpdates":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    .restart local v0    # "requestUpdates":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    goto :goto_2

    .line 87
    :catch_1
    move-exception v6

    goto :goto_1
.end method

.method static synthetic access$000(Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetProfileFeeds;)Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetProfileFeeds;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetProfileFeeds;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetProfileFeeds;)Landroid/os/Bundle;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetProfileFeeds;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetProfileFeeds;->mBundleUpdatesQuery:Landroid/os/Bundle;

    return-object v0
.end method

.method static synthetic access$102(Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetProfileFeeds;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetProfileFeeds;
    .param p1, "x1"    # Landroid/os/Bundle;

    .prologue
    .line 38
    iput-object p1, p0, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetProfileFeeds;->mBundleUpdatesQuery:Landroid/os/Bundle;

    return-object p1
.end method


# virtual methods
.method protected respond()Z
    .locals 5

    .prologue
    .line 99
    const-string v0, "SnsAgent"

    const-string v1, "<SnsLiCmdGetProfileFeeds> respond()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 100
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetProfileFeeds;->getCommandCallback()Lcom/sec/android/app/sns3/agent/command/ISnsCommandCallback;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetProfileFeeds;->getCommandID()I

    move-result v1

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetProfileFeeds;->isSuccess()Z

    move-result v2

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetProfileFeeds;->getUri()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetProfileFeeds;->getResponseList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/sec/android/app/sns3/agent/command/ISnsCommandCallback;->onCmdRespond(IZLjava/lang/String;Ljava/util/List;)V

    .line 102
    const/4 v0, 0x1

    return v0
.end method
