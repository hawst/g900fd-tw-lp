.class Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetNoteEx$1;
.super Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqGetNoteEx;
.source "SnsFbCmdGetNoteEx.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetNoteEx;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Handler;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetNoteEx;


# direct methods
.method constructor <init>(Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetNoteEx;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;)V
    .locals 0
    .param p2, "x0"    # Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    .param p3, "x1"    # Ljava/lang/String;

    .prologue
    .line 53
    iput-object p1, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetNoteEx$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetNoteEx;

    invoke-direct {p0, p2, p3}, Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqGetNoteEx;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public onReqRespond(IZIILandroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNoteEx;)Z
    .locals 15
    .param p1, "reqID"    # I
    .param p2, "bSuccess"    # Z
    .param p3, "httpStatus"    # I
    .param p4, "errorCode"    # I
    .param p5, "reason"    # Landroid/os/Bundle;
    .param p6, "note"    # Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNoteEx;

    .prologue
    .line 59
    new-instance v12, Landroid/content/ContentValues;

    invoke-direct {v12}, Landroid/content/ContentValues;-><init>()V

    .line 60
    .local v12, "values":Landroid/content/ContentValues;
    new-instance v13, Landroid/content/ContentValues;

    invoke-direct {v13}, Landroid/content/ContentValues;-><init>()V

    .line 61
    .local v13, "valuesComment":Landroid/content/ContentValues;
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/sns3/SnsApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    .line 64
    .local v4, "cr":Landroid/content/ContentResolver;
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    .line 65
    .local v14, "where":Ljava/lang/StringBuilder;
    if-eqz p6, :cond_0

    .line 66
    const-string v5, "note_id"

    invoke-virtual {v14, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 67
    const-string v5, " = "

    invoke-virtual {v14, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 68
    move-object/from16 v0, p6

    iget-object v5, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNoteEx;->mNoteID:Ljava/lang/String;

    invoke-virtual {v14, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 70
    sget-object v5, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$Note;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v4, v5, v6, v7}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 72
    const/4 v5, 0x0

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->length()I

    move-result v6

    invoke-virtual {v14, v5, v6}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 73
    const-string v5, "target_id"

    invoke-virtual {v14, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 74
    const-string v5, " = "

    invoke-virtual {v14, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 75
    move-object/from16 v0, p6

    iget-object v5, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNoteEx;->mNoteID:Ljava/lang/String;

    invoke-virtual {v14, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 77
    sget-object v5, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$Comments;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v4, v5, v6, v7}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 80
    :cond_0
    if-eqz p2, :cond_5

    .line 81
    if-eqz p6, :cond_3

    .line 82
    invoke-virtual {v12}, Landroid/content/ContentValues;->clear()V

    .line 84
    const-string v5, "from_id"

    move-object/from16 v0, p6

    iget-object v6, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNoteEx;->mUId:Ljava/lang/String;

    invoke-virtual {v12, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    const-string v5, "note_id"

    move-object/from16 v0, p6

    iget-object v6, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNoteEx;->mNoteID:Ljava/lang/String;

    invoke-virtual {v12, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    const-string v5, "create_time"

    move-object/from16 v0, p6

    iget-object v6, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNoteEx;->mCreatedTime:Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    const-wide/16 v8, 0x3e8

    mul-long/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v12, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 87
    const-string v5, "updated_time"

    move-object/from16 v0, p6

    iget-object v6, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNoteEx;->mUpdatedTime:Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    const-wide/16 v8, 0x3e8

    mul-long/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v12, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 88
    const-string v5, "comments_count"

    move-object/from16 v0, p6

    iget-object v6, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNoteEx;->mCommentsCount:Ljava/lang/Integer;

    invoke-virtual {v12, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 89
    const-string v5, "message"

    move-object/from16 v0, p6

    iget-object v6, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNoteEx;->mContentHTML:Ljava/lang/String;

    invoke-virtual {v12, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    const-string v5, "subject"

    move-object/from16 v0, p6

    iget-object v6, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNoteEx;->mTitle:Ljava/lang/String;

    invoke-virtual {v12, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    move-object/from16 v0, p6

    iget-object v5, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNoteEx;->mLikeDone:Ljava/lang/String;

    move-object/from16 v0, p6

    iget-object v6, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNoteEx;->mNoteID:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 93
    const-string v5, "like_done"

    const-string v6, "true"

    invoke-virtual {v12, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    :cond_1
    move-object/from16 v0, p6

    iget-object v5, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNoteEx;->mComment:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseCommentQueryResult;

    if-eqz v5, :cond_2

    .line 97
    move-object/from16 v0, p6

    iget-object v11, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNoteEx;->mComment:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseCommentQueryResult;

    .line 99
    .local v11, "curComment":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseCommentQueryResult;
    :goto_0
    if-eqz v11, :cond_2

    .line 100
    invoke-virtual {v13}, Landroid/content/ContentValues;->clear()V

    .line 102
    const-string v5, "target_id"

    iget-object v6, v11, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseCommentQueryResult;->mTargetID:Ljava/lang/String;

    invoke-virtual {v13, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    const-string v5, "comments_id"

    iget-object v6, v11, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseCommentQueryResult;->mCommentID:Ljava/lang/String;

    invoke-virtual {v13, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    const-string v5, "from_id"

    iget-object v6, v11, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseCommentQueryResult;->mFromID:Ljava/lang/String;

    invoke-virtual {v13, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    const-string v5, "from_name"

    iget-object v6, v11, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseCommentQueryResult;->mFromName:Ljava/lang/String;

    invoke-virtual {v13, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    const-string v5, "message"

    iget-object v6, v11, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseCommentQueryResult;->mMessage:Ljava/lang/String;

    invoke-virtual {v13, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 107
    const-string v5, "created_time"

    iget-object v6, v11, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseCommentQueryResult;->mCreatedTime:Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    const-wide/16 v8, 0x3e8

    mul-long/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v13, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 109
    const-string v5, "likes"

    iget-object v6, v11, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseCommentQueryResult;->mLikes:Ljava/lang/String;

    invoke-virtual {v13, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    const-string v5, "user_likes"

    iget-object v6, v11, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseCommentQueryResult;->mUserLikes:Ljava/lang/String;

    invoke-virtual {v13, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    sget-object v5, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$Comments;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v4, v5, v13}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 114
    iget-object v11, v11, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseCommentQueryResult;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseCommentQueryResult;

    goto :goto_0

    .line 118
    .end local v11    # "curComment":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseCommentQueryResult;
    :cond_2
    const/4 v5, 0x0

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->length()I

    move-result v6

    invoke-virtual {v14, v5, v6}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 119
    const-string v5, "note_id"

    invoke-virtual {v14, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 120
    const-string v5, " = \'"

    invoke-virtual {v14, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 121
    move-object/from16 v0, p6

    iget-object v5, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNoteEx;->mNoteID:Ljava/lang/String;

    invoke-virtual {v14, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 122
    const-string v5, "\'"

    invoke-virtual {v14, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 124
    sget-object v5, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$Note;->CONTENT_URI:Landroid/net/Uri;

    const/4 v6, 0x0

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 126
    .local v10, "c":Landroid/database/Cursor;
    if-eqz v10, :cond_4

    invoke-interface {v10}, Landroid/database/Cursor;->getCount()I

    move-result v5

    if-lez v5, :cond_4

    .line 127
    sget-object v5, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$Note;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v4, v5, v12, v6, v7}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 131
    :goto_1
    if-eqz v10, :cond_3

    .line 132
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 135
    .end local v10    # "c":Landroid/database/Cursor;
    :cond_3
    iget-object v5, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetNoteEx$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetNoteEx;

    sget-object v6, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$Note;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v6}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetNoteEx;->setUri(Ljava/lang/String;)V

    .line 142
    :goto_2
    iget-object v5, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetNoteEx$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetNoteEx;

    move/from16 v0, p2

    invoke-virtual {v5, v0}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetNoteEx;->setSuccess(Z)V

    .line 143
    iget-object v5, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetNoteEx$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetNoteEx;

    invoke-virtual {v5, p0}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetNoteEx;->receive(Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;)V

    .line 145
    const/4 v5, 0x1

    return v5

    .line 129
    .restart local v10    # "c":Landroid/database/Cursor;
    :cond_4
    sget-object v5, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$Note;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v4, v5, v12}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    goto :goto_1

    .line 137
    .end local v10    # "c":Landroid/database/Cursor;
    :cond_5
    iget-object v5, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetNoteEx$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetNoteEx;

    new-instance v6, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;

    const-string v7, "facebook"

    move/from16 v0, p3

    move/from16 v1, p4

    move-object/from16 v2, p5

    invoke-direct {v6, v7, v0, v1, v2}, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;-><init>(Ljava/lang/String;IILandroid/os/Bundle;)V

    invoke-virtual {v5, v6}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetNoteEx;->setResponseList(Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;)Z

    .line 139
    iget-object v5, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetNoteEx$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetNoteEx;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetNoteEx;->setUri(Ljava/lang/String;)V

    goto :goto_2
.end method
