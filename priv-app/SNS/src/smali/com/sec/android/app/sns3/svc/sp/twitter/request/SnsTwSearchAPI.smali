.class public final enum Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI;
.super Ljava/lang/Enum;
.source "SnsTwSearchAPI.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI$Param;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI;

.field public static final enum SEARCH:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI;


# instance fields
.field private mHttpMethod:Ljava/lang/String;

.field private mUrl:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 27
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI;

    const-string v1, "SEARCH"

    const-string v2, "search.json"

    const-string v3, "GET"

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI;->SEARCH:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI;

    .line 25
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI;

    sget-object v1, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI;->SEARCH:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI;

    aput-object v1, v0, v4

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI;->$VALUES:[Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p3, "url"    # Ljava/lang/String;
    .param p4, "httpMethod"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 56
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 57
    iput-object p3, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI;->mUrl:Ljava/lang/String;

    .line 58
    iput-object p4, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI;->mHttpMethod:Ljava/lang/String;

    .line 59
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 25
    const-class v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI;
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI;->$VALUES:[Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI;

    invoke-virtual {v0}, [Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI;

    return-object v0
.end method


# virtual methods
.method public getHttpMethod()Ljava/lang/String;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI;->mHttpMethod:Ljava/lang/String;

    return-object v0
.end method

.method public getUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI;->mUrl:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSearchAPI;->mUrl:Ljava/lang/String;

    return-object v0
.end method
