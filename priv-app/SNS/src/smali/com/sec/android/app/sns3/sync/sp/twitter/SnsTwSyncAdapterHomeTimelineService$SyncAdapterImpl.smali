.class Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterHomeTimelineService$SyncAdapterImpl;
.super Landroid/content/AbstractThreadedSyncAdapter;
.source "SnsTwSyncAdapterHomeTimelineService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterHomeTimelineService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SyncAdapterImpl"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterHomeTimelineService;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterHomeTimelineService;Landroid/content/Context;)V
    .locals 1
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 76
    iput-object p1, p0, Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterHomeTimelineService$SyncAdapterImpl;->this$0:Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterHomeTimelineService;

    .line 77
    const/4 v0, 0x1

    invoke-direct {p0, p2, v0}, Landroid/content/AbstractThreadedSyncAdapter;-><init>(Landroid/content/Context;Z)V

    .line 78
    # setter for: Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterHomeTimelineService;->mContext:Landroid/content/Context;
    invoke-static {p1, p2}, Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterHomeTimelineService;->access$002(Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterHomeTimelineService;Landroid/content/Context;)Landroid/content/Context;

    .line 79
    return-void
.end method


# virtual methods
.method public onPerformSync(Landroid/accounts/Account;Landroid/os/Bundle;Ljava/lang/String;Landroid/content/ContentProviderClient;Landroid/content/SyncResult;)V
    .locals 3
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "extras"    # Landroid/os/Bundle;
    .param p3, "authority"    # Ljava/lang/String;
    .param p4, "provider"    # Landroid/content/ContentProviderClient;
    .param p5, "syncResult"    # Landroid/content/SyncResult;

    .prologue
    .line 85
    const-string v1, "SnsTwSync"

    const-string v2, "***************** SnsTwSyncAdapterTimelineService : onPerformSync!!! *****************"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 89
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterHomeTimelineService$SyncAdapterImpl;->this$0:Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterHomeTimelineService;

    # setter for: Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterHomeTimelineService;->mAccount:Landroid/accounts/Account;
    invoke-static {v1, p1}, Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterHomeTimelineService;->access$102(Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterHomeTimelineService;Landroid/accounts/Account;)Landroid/accounts/Account;

    .line 90
    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterHomeTimelineService$SyncAdapterImpl;->this$0:Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterHomeTimelineService;

    # setter for: Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterHomeTimelineService;->mAuthority:Ljava/lang/String;
    invoke-static {v1, p3}, Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterHomeTimelineService;->access$202(Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterHomeTimelineService;Ljava/lang/String;)Ljava/lang/String;

    .line 91
    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterHomeTimelineService$SyncAdapterImpl;->this$0:Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterHomeTimelineService;

    # setter for: Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterHomeTimelineService;->mSyncResult:Landroid/content/SyncResult;
    invoke-static {v1, p5}, Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterHomeTimelineService;->access$302(Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterHomeTimelineService;Landroid/content/SyncResult;)Landroid/content/SyncResult;

    .line 93
    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterHomeTimelineService$SyncAdapterImpl;->this$0:Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterHomeTimelineService;

    # invokes: Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterHomeTimelineService;->performSync()V
    invoke-static {v1}, Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterHomeTimelineService;->access$400(Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterHomeTimelineService;)V
    :try_end_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 101
    :goto_0
    return-void

    .line 95
    :catch_0
    move-exception v0

    .line 96
    .local v0, "e":Landroid/accounts/OperationCanceledException;
    const-string v1, "SnsTwSync"

    const-string v2, "SnsTwSyncAdapterTimelineService : onPerformSync is CANCELED!!!"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 97
    .end local v0    # "e":Landroid/accounts/OperationCanceledException;
    :catch_1
    move-exception v0

    .line 98
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "SnsTwSync"

    const-string v2, "SnsTwSyncAdapterTimelineService : Abnormal Syncing!!!"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onSyncCanceled()V
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterHomeTimelineService$SyncAdapterImpl;->this$0:Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterHomeTimelineService;

    # invokes: Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterHomeTimelineService;->onSyncCanceled()V
    invoke-static {v0}, Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterHomeTimelineService;->access$500(Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterHomeTimelineService;)V

    .line 106
    return-void
.end method
