.class public Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterCallService;
.super Landroid/app/Service;
.source "SnsGpSyncAdapterCallService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterCallService$SyncAdapterImpl;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "SnsGpSync"


# instance fields
.field private mAccount:Landroid/accounts/Account;

.field private mAuthority:Ljava/lang/String;

.field private mSyncAdapter:Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterCallService$SyncAdapterImpl;

.field private mSyncState:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 34
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 38
    iput-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterCallService;->mSyncAdapter:Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterCallService$SyncAdapterImpl;

    .line 40
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterCallService;->mSyncState:I

    .line 42
    iput-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterCallService;->mAccount:Landroid/accounts/Account;

    .line 44
    iput-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterCallService;->mAuthority:Ljava/lang/String;

    .line 46
    return-void
.end method

.method static synthetic access$002(Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterCallService;Landroid/accounts/Account;)Landroid/accounts/Account;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterCallService;
    .param p1, "x1"    # Landroid/accounts/Account;

    .prologue
    .line 34
    iput-object p1, p0, Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterCallService;->mAccount:Landroid/accounts/Account;

    return-object p1
.end method

.method static synthetic access$102(Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterCallService;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterCallService;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 34
    iput-object p1, p0, Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterCallService;->mAuthority:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$200(Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterCallService;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterCallService;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/accounts/OperationCanceledException;
        }
    .end annotation

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterCallService;->performSync()V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterCallService;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterCallService;

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterCallService;->onSyncCanceled()V

    return-void
.end method

.method static synthetic access$402(Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterCallService;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterCallService;
    .param p1, "x1"    # I

    .prologue
    .line 34
    iput p1, p0, Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterCallService;->mSyncState:I

    return p1
.end method

.method private onSyncCanceled()V
    .locals 2

    .prologue
    .line 153
    const-string v0, "SnsGpSync"

    const-string v1, "***************** SnsGpSyncAdapterProfileFeedService : onSyncCanceled !!! *****************"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 156
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterCallService;->mSyncState:I

    .line 158
    return-void
.end method

.method private performSync()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/accounts/OperationCanceledException;
        }
    .end annotation

    .prologue
    .line 122
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterCallService;->mSyncState:I

    .line 124
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterCallService$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterCallService$1;-><init>(Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterCallService;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 136
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterCallService;->suspendSync()V

    .line 137
    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 93
    const-string v0, "SnsGpSync"

    const-string v1, "***************** SnsGpSyncAdapterCallService : onBind !!! *****************"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 96
    iget-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterCallService;->mSyncAdapter:Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterCallService$SyncAdapterImpl;

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterCallService$SyncAdapterImpl;->getSyncAdapterBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 82
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 84
    iget-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterCallService;->mSyncAdapter:Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterCallService$SyncAdapterImpl;

    if-nez v0, :cond_0

    .line 85
    new-instance v0, Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterCallService$SyncAdapterImpl;

    invoke-direct {v0, p0, p0}, Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterCallService$SyncAdapterImpl;-><init>(Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterCallService;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterCallService;->mSyncAdapter:Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterCallService$SyncAdapterImpl;

    .line 88
    :cond_0
    return-void
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 102
    const/4 v0, 0x0

    .line 103
    .local v0, "bActive":Z
    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterCallService;->mAccount:Landroid/accounts/Account;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterCallService;->mAuthority:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 104
    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterCallService;->mAccount:Landroid/accounts/Account;

    iget-object v2, p0, Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterCallService;->mAuthority:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/content/ContentResolver;->isSyncActive(Landroid/accounts/Account;Ljava/lang/String;)Z

    move-result v0

    .line 107
    :cond_0
    const-string v1, "SnsGpSync"

    const-string v2, "***************** SnsGpSyncAdapterCallService : onUnbind !!! *****************"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 110
    if-eqz v0, :cond_2

    iget v1, p0, Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterCallService;->mSyncState:I

    const/4 v2, 0x1

    if-eq v1, v2, :cond_1

    iget v1, p0, Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterCallService;->mSyncState:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_2

    .line 112
    :cond_1
    const-string v1, "SnsGpSync"

    const-string v2, "SnsGpSyncAdapterCallService : onUnbind : SYNC ERROR : performSync was stopped by forced abort or pending problem!!!"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 118
    :goto_0
    invoke-super {p0, p1}, Landroid/app/Service;->onUnbind(Landroid/content/Intent;)Z

    move-result v1

    return v1

    .line 116
    :cond_2
    const-string v1, "SnsGpSync"

    const-string v2, "SnsGpSyncAdapterCallService : onUnbind : COMPLETE STATE!!!"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public declared-synchronized resumeSync()V
    .locals 1

    .prologue
    .line 148
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->notify()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 149
    monitor-exit p0

    return-void

    .line 148
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized suspendSync()V
    .locals 2

    .prologue
    .line 141
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 145
    :goto_0
    monitor-exit p0

    return-void

    .line 142
    :catch_0
    move-exception v0

    .line 143
    .local v0, "e":Ljava/lang/InterruptedException;
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 141
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method
