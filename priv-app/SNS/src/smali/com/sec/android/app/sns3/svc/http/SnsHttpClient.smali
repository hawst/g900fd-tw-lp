.class public Lcom/sec/android/app/sns3/svc/http/SnsHttpClient;
.super Lorg/apache/http/impl/client/DefaultHttpClient;
.source "SnsHttpClient.java"


# static fields
.field private static final CONNECTION_TIMEOUT_DURATION:I = 0x9c40

.field private static final DEFAULT_PORT:I = 0x50

.field private static final SECURE_PORT:I = 0x1bb

.field public static final SNS_HTTP_MAX_CONNECTION:I = 0x1e


# instance fields
.field private mHttpParams:Lorg/apache/http/params/HttpParams;


# direct methods
.method public constructor <init>()V
    .locals 8

    .prologue
    const v7, 0x9c40

    const/16 v6, 0x1e

    .line 57
    invoke-direct {p0}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>()V

    .line 59
    new-instance v3, Lorg/apache/http/params/BasicHttpParams;

    invoke-direct {v3}, Lorg/apache/http/params/BasicHttpParams;-><init>()V

    iput-object v3, p0, Lcom/sec/android/app/sns3/svc/http/SnsHttpClient;->mHttpParams:Lorg/apache/http/params/HttpParams;

    .line 60
    iget-object v3, p0, Lcom/sec/android/app/sns3/svc/http/SnsHttpClient;->mHttpParams:Lorg/apache/http/params/HttpParams;

    const-string v4, "http.conn-manager.max-total"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Lorg/apache/http/params/HttpParams;->setParameter(Ljava/lang/String;Ljava/lang/Object;)Lorg/apache/http/params/HttpParams;

    .line 61
    iget-object v3, p0, Lcom/sec/android/app/sns3/svc/http/SnsHttpClient;->mHttpParams:Lorg/apache/http/params/HttpParams;

    const-string v4, "http.conn-manager.max-per-route"

    new-instance v5, Lorg/apache/http/conn/params/ConnPerRouteBean;

    invoke-direct {v5, v6}, Lorg/apache/http/conn/params/ConnPerRouteBean;-><init>(I)V

    invoke-interface {v3, v4, v5}, Lorg/apache/http/params/HttpParams;->setParameter(Ljava/lang/String;Ljava/lang/Object;)Lorg/apache/http/params/HttpParams;

    .line 63
    iget-object v3, p0, Lcom/sec/android/app/sns3/svc/http/SnsHttpClient;->mHttpParams:Lorg/apache/http/params/HttpParams;

    const-string v4, "http.protocol.expect-continue"

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Lorg/apache/http/params/HttpParams;->setParameter(Ljava/lang/String;Ljava/lang/Object;)Lorg/apache/http/params/HttpParams;

    .line 68
    iget-object v3, p0, Lcom/sec/android/app/sns3/svc/http/SnsHttpClient;->mHttpParams:Lorg/apache/http/params/HttpParams;

    invoke-static {v3, v7}, Lorg/apache/http/params/HttpConnectionParams;->setConnectionTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 69
    iget-object v3, p0, Lcom/sec/android/app/sns3/svc/http/SnsHttpClient;->mHttpParams:Lorg/apache/http/params/HttpParams;

    invoke-static {v3, v7}, Lorg/apache/http/params/HttpConnectionParams;->setSoTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 72
    iget-object v3, p0, Lcom/sec/android/app/sns3/svc/http/SnsHttpClient;->mHttpParams:Lorg/apache/http/params/HttpParams;

    sget-object v4, Lorg/apache/http/HttpVersion;->HTTP_1_1:Lorg/apache/http/HttpVersion;

    invoke-static {v3, v4}, Lorg/apache/http/params/HttpProtocolParams;->setVersion(Lorg/apache/http/params/HttpParams;Lorg/apache/http/ProtocolVersion;)V

    .line 74
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/sns3/SnsApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Proxy;->getHost(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 76
    .local v1, "proxyAddr":Ljava/lang/String;
    const/16 v2, 0x50

    .line 77
    .local v2, "proxyPort":I
    if-eqz v1, :cond_0

    .line 78
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/sns3/SnsApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Proxy;->getPort(Landroid/content/Context;)I

    move-result v2

    .line 81
    new-instance v0, Lorg/apache/http/HttpHost;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/HttpHost;-><init>(Ljava/lang/String;I)V

    .line 82
    .local v0, "httpProxy":Lorg/apache/http/HttpHost;
    iget-object v3, p0, Lcom/sec/android/app/sns3/svc/http/SnsHttpClient;->mHttpParams:Lorg/apache/http/params/HttpParams;

    const-string v4, "http.route.default-proxy"

    invoke-interface {v3, v4, v0}, Lorg/apache/http/params/HttpParams;->setParameter(Ljava/lang/String;Ljava/lang/Object;)Lorg/apache/http/params/HttpParams;

    .line 90
    .end local v0    # "httpProxy":Lorg/apache/http/HttpHost;
    :cond_0
    return-void
.end method


# virtual methods
.method protected createClientConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;
    .locals 6

    .prologue
    .line 94
    new-instance v1, Lorg/apache/http/conn/scheme/SchemeRegistry;

    invoke-direct {v1}, Lorg/apache/http/conn/scheme/SchemeRegistry;-><init>()V

    .line 96
    .local v1, "schemeRegistry":Lorg/apache/http/conn/scheme/SchemeRegistry;
    new-instance v2, Lorg/apache/http/conn/scheme/Scheme;

    const-string v3, "http"

    invoke-static {}, Lorg/apache/http/conn/scheme/PlainSocketFactory;->getSocketFactory()Lorg/apache/http/conn/scheme/PlainSocketFactory;

    move-result-object v4

    const/16 v5, 0x50

    invoke-direct {v2, v3, v4, v5}, Lorg/apache/http/conn/scheme/Scheme;-><init>(Ljava/lang/String;Lorg/apache/http/conn/scheme/SocketFactory;I)V

    invoke-virtual {v1, v2}, Lorg/apache/http/conn/scheme/SchemeRegistry;->register(Lorg/apache/http/conn/scheme/Scheme;)Lorg/apache/http/conn/scheme/Scheme;

    .line 104
    new-instance v2, Lorg/apache/http/conn/scheme/Scheme;

    const-string v3, "https"

    invoke-static {}, Lorg/apache/http/conn/ssl/SSLSocketFactory;->getSocketFactory()Lorg/apache/http/conn/ssl/SSLSocketFactory;

    move-result-object v4

    const/16 v5, 0x1bb

    invoke-direct {v2, v3, v4, v5}, Lorg/apache/http/conn/scheme/Scheme;-><init>(Ljava/lang/String;Lorg/apache/http/conn/scheme/SocketFactory;I)V

    invoke-virtual {v1, v2}, Lorg/apache/http/conn/scheme/SchemeRegistry;->register(Lorg/apache/http/conn/scheme/Scheme;)Lorg/apache/http/conn/scheme/Scheme;

    .line 110
    new-instance v0, Lorg/apache/http/impl/conn/tsccm/ThreadSafeClientConnManager;

    iget-object v2, p0, Lcom/sec/android/app/sns3/svc/http/SnsHttpClient;->mHttpParams:Lorg/apache/http/params/HttpParams;

    invoke-direct {v0, v2, v1}, Lorg/apache/http/impl/conn/tsccm/ThreadSafeClientConnManager;-><init>(Lorg/apache/http/params/HttpParams;Lorg/apache/http/conn/scheme/SchemeRegistry;)V

    .line 113
    .local v0, "connectionMgr":Lorg/apache/http/conn/ClientConnectionManager;
    return-object v0
.end method

.method public setKeepAlive()V
    .locals 1

    .prologue
    .line 117
    new-instance v0, Lcom/sec/android/app/sns3/svc/http/SnsHttpClient$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/sns3/svc/http/SnsHttpClient$1;-><init>(Lcom/sec/android/app/sns3/svc/http/SnsHttpClient;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/sns3/svc/http/SnsHttpClient;->setKeepAliveStrategy(Lorg/apache/http/conn/ConnectionKeepAliveStrategy;)V

    .line 129
    return-void
.end method

.method public declared-synchronized setParams(Lorg/apache/http/params/HttpParams;)V
    .locals 1
    .param p1, "params"    # Lorg/apache/http/params/HttpParams;

    .prologue
    .line 133
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/http/SnsHttpClient;->mHttpParams:Lorg/apache/http/params/HttpParams;

    invoke-super {p0, v0}, Lorg/apache/http/impl/client/DefaultHttpClient;->setParams(Lorg/apache/http/params/HttpParams;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 134
    monitor-exit p0

    return-void

    .line 133
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
