.class public Lcom/sec/android/app/sns3/agent/SnsAgentMgr;
.super Ljava/lang/Object;
.source "SnsAgentMgr.java"


# static fields
.field public static final TAG:Ljava/lang/String; = "SnsAgent"


# instance fields
.field public mCmdMgr:Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "ctxt"    # Landroid/content/Context;

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    new-instance v0, Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;

    invoke-direct {v0}, Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/sns3/agent/SnsAgentMgr;->mCmdMgr:Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;

    .line 33
    iget-object v0, p0, Lcom/sec/android/app/sns3/agent/SnsAgentMgr;->mCmdMgr:Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;->initialize()V

    .line 34
    return-void
.end method


# virtual methods
.method public getCommandMgr()Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/app/sns3/agent/SnsAgentMgr;->mCmdMgr:Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;

    return-object v0
.end method
