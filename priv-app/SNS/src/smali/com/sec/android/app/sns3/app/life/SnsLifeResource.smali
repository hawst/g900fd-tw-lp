.class public Lcom/sec/android/app/sns3/app/life/SnsLifeResource;
.super Ljava/lang/Object;
.source "SnsLifeResource.java"


# static fields
.field public static final CONTENT_URI:Ljava/lang/String; = "SNS_CONTENT_URI"

.field public static final REQUEST_LOGGEDIN_USERID:Ljava/lang/String; = "com.sec.android.app.sns.life.ACTION_LIFE_LOGGEDIN_USER_REQUESTED"

.field public static final REQUEST_PROFILE_FEED:Ljava/lang/String; = "com.sec.android.app.sns.life.ACTION_LIFE_PROFILE_FEED_REQUESTED"

.field public static final RESULT:Ljava/lang/String; = "SNS_RESULT"

.field public static final SNS_BROADCAST_PERMISSION:Ljava/lang/String; = "com.sec.android.app.sns3.permission.RECEIVE_SNS_BROADCAST"

.field public static TAG:Ljava/lang/String; = null

.field public static final UPDATE_LOGGEDIN_USERID:Ljava/lang/String; = "com.sec.android.app.sns.life.ACTION_LIFE_LOGGEDIN_USER_UPDATED"

.field public static final UPDATE_PROFILE_FEED:Ljava/lang/String; = "com.sec.android.app.sns.life.ACTION_LIFE_PROFILE_FEED_UPDATED"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const-string v0, "SnsLifeProfile"

    sput-object v0, Lcom/sec/android/app/sns3/app/life/SnsLifeResource;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
