.class public interface abstract Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserLocationPost$FacebookLocationPost;
.super Ljava/lang/Object;
.source "SnsFbParserLocationPost.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserLocationPost;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "FacebookLocationPost"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserLocationPost$FacebookLocationPost$SnsLocationPostQuery7;,
        Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserLocationPost$FacebookLocationPost$SnsLocationPostQuery6;,
        Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserLocationPost$FacebookLocationPost$SnsLocationPostQuery5;,
        Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserLocationPost$FacebookLocationPost$SnsLocationPostQuery4;,
        Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserLocationPost$FacebookLocationPost$SnsLocationPostQuery3;,
        Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserLocationPost$FacebookLocationPost$SnsLocationPostQuery2;,
        Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserLocationPost$FacebookLocationPost$SnsLocationPostQuery1;
    }
.end annotation


# static fields
.field public static final FQL_RESULT_SET:Ljava/lang/String; = "fql_result_set"

.field public static final QUERY_1:Ljava/lang/String; = "query1"

.field public static final QUERY_2:Ljava/lang/String; = "query2"

.field public static final QUERY_3:Ljava/lang/String; = "query3"

.field public static final QUERY_4:Ljava/lang/String; = "query4"

.field public static final QUERY_5:Ljava/lang/String; = "query5"

.field public static final QUERY_6:Ljava/lang/String; = "query6"

.field public static final QUERY_7:Ljava/lang/String; = "query7"

.field public static final QUERY_NAME:Ljava/lang/String; = "name"
