.class public abstract Lcom/sec/android/app/sns3/svc/sp/instagram/request/SnsInReqGetUser;
.super Lcom/sec/android/app/sns3/svc/sp/instagram/request/SnsInReqBase;
.source "SnsInReqGetUser.java"

# interfaces
.implements Lcom/sec/android/app/sns3/svc/sp/instagram/callback/ISnsInReqCbUser;


# static fields
.field public static final REST_URL_USER:Ljava/lang/String; = "https://api.instagram.com/v1/users/%1$s/?access_token=%2$s"


# instance fields
.field private mObjectId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;)V
    .locals 1
    .param p1, "svcMgr"    # Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    .prologue
    .line 46
    const/16 v0, 0x16

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/sns3/svc/sp/instagram/request/SnsInReqBase;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;I)V

    .line 40
    const-string v0, "self"

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/instagram/request/SnsInReqGetUser;->mObjectId:Ljava/lang/String;

    .line 48
    return-void
.end method


# virtual methods
.method protected compose()Lcom/sec/android/app/sns3/svc/http/SnsHttpRequest;
    .locals 9

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/instagram/request/SnsInReqGetUser;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/SnsSvcMgr;->getTokenMgr()Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;

    move-result-object v0

    const-string v1, "instagram"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;->getToken(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/sns3/svc/sp/instagram/SnsInToken;

    .line 54
    .local v6, "token":Lcom/sec/android/app/sns3/svc/sp/instagram/SnsInToken;
    const-string v2, "GET"

    .line 55
    .local v2, "method":Ljava/lang/String;
    const/4 v3, 0x0

    .line 56
    .local v3, "uri":Ljava/lang/String;
    const/4 v4, 0x0

    .line 57
    .local v4, "header":Landroid/os/Bundle;
    const/4 v5, 0x0

    .line 59
    .local v5, "body":Landroid/os/Bundle;
    const-string v0, "https://api.instagram.com/v1/users/%1$s/?access_token=%2$s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget-object v8, p0, Lcom/sec/android/app/sns3/svc/sp/instagram/request/SnsInReqGetUser;->mObjectId:Ljava/lang/String;

    aput-object v8, v1, v7

    const/4 v7, 0x1

    invoke-virtual {v6}, Lcom/sec/android/app/sns3/svc/sp/instagram/SnsInToken;->getAccessToken()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->encodeUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v1, v7

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 61
    new-instance v0, Lcom/sec/android/app/sns3/svc/http/SnsHttpRequest;

    iget v1, p0, Lcom/sec/android/app/sns3/svc/sp/instagram/request/SnsInReqGetUser;->mReqID:I

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/sns3/svc/http/SnsHttpRequest;-><init>(ILjava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;)V

    return-object v0
.end method

.method protected parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;
    .locals 3
    .param p1, "content"    # Ljava/lang/String;

    .prologue
    .line 66
    const-string v1, "SNS"

    const-string v2, "Instagram  SnsInReqGetUser response SUCCESS!!"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 67
    invoke-super {p0, p1}, Lcom/sec/android/app/sns3/svc/sp/instagram/request/SnsInReqBase;->parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;

    move-result-object v0

    .line 69
    .local v0, "response":Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;
    if-eqz v0, :cond_0

    .line 72
    .end local v0    # "response":Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;
    :goto_0
    return-object v0

    .restart local v0    # "response":Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;
    :cond_0
    invoke-static {p1}, Lcom/sec/android/app/sns3/svc/sp/instagram/parser/SnsInParserUser;->parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseUser;

    move-result-object v0

    goto :goto_0
.end method

.method protected respond(Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;)Z
    .locals 7
    .param p1, "result"    # Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;

    .prologue
    .line 77
    iget v1, p0, Lcom/sec/android/app/sns3/svc/sp/instagram/request/SnsInReqGetUser;->mReqID:I

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->isSuccess()Z

    move-result v2

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->getHttpstatus()I

    move-result v3

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->getErrorCode()I

    move-result v4

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->getReason()Landroid/os/Bundle;

    move-result-object v5

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->getResponse()Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseUser;

    move-object v0, p0

    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/app/sns3/svc/sp/instagram/request/SnsInReqGetUser;->onReqRespond(IZIILandroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseUser;)Z

    .line 80
    const/4 v0, 0x1

    return v0
.end method
