.class public final enum Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;
.super Ljava/lang/Enum;
.source "SnsTwAccountsAPI.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Param"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;

.field public static final enum DESCRIPTION:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;

.field public static final enum DEVICE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;

.field public static final enum END_SLEEP_TIME:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;

.field public static final enum FILENAME:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;

.field public static final enum IMAGE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;

.field public static final enum INCLUDE_ENTITIES:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;

.field public static final enum LANG:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;

.field public static final enum LOCATION:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;

.field public static final enum NAME:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;

.field public static final enum PROFILE_BACKGROUND_COLOR:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;

.field public static final enum PROFILE_LINK_COLOR:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;

.field public static final enum PROFILE_SIDEBAR_BORDER_COLOR:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;

.field public static final enum PROFILE_SIDEBAR_FILL_COLOR:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;

.field public static final enum PROFILE_TEXT_COLOR:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;

.field public static final enum SKIP_STATUS:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;

.field public static final enum SLEEP_TIME_ENABLED:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;

.field public static final enum START_SLEEP_TIME:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;

.field public static final enum TILE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;

.field public static final enum TIME_ZONE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;

.field public static final enum TREND_LOCATION_WOEID:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;

.field public static final enum URL:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;

.field public static final enum USE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;


# instance fields
.field private mParam:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 39
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;

    const-string v1, "INCLUDE_ENTITIES"

    const-string v2, "include_entities"

    invoke-direct {v0, v1, v4, v2}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;->INCLUDE_ENTITIES:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;

    .line 40
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;

    const-string v1, "SKIP_STATUS"

    const-string v2, "skip_status"

    invoke-direct {v0, v1, v5, v2}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;->SKIP_STATUS:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;

    .line 41
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;

    const-string v1, "DEVICE"

    const-string v2, "device"

    invoke-direct {v0, v1, v6, v2}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;->DEVICE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;

    .line 42
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;

    const-string v1, "NAME"

    const-string v2, "name"

    invoke-direct {v0, v1, v7, v2}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;->NAME:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;

    .line 43
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;

    const-string v1, "URL"

    const-string v2, "url"

    invoke-direct {v0, v1, v8, v2}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;->URL:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;

    .line 44
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;

    const-string v1, "LOCATION"

    const/4 v2, 0x5

    const-string v3, "location"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;->LOCATION:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;

    .line 45
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;

    const-string v1, "DESCRIPTION"

    const/4 v2, 0x6

    const-string v3, "description"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;->DESCRIPTION:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;

    .line 46
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;

    const-string v1, "IMAGE"

    const/4 v2, 0x7

    const-string v3, "image"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;->IMAGE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;

    .line 47
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;

    const-string v1, "TILE"

    const/16 v2, 0x8

    const-string v3, "tile"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;->TILE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;

    .line 48
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;

    const-string v1, "USE"

    const/16 v2, 0x9

    const-string v3, "use"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;->USE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;

    .line 49
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;

    const-string v1, "PROFILE_BACKGROUND_COLOR"

    const/16 v2, 0xa

    const-string v3, "profile_background_color"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;->PROFILE_BACKGROUND_COLOR:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;

    .line 50
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;

    const-string v1, "PROFILE_LINK_COLOR"

    const/16 v2, 0xb

    const-string v3, "profile_link_color"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;->PROFILE_LINK_COLOR:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;

    .line 51
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;

    const-string v1, "PROFILE_SIDEBAR_BORDER_COLOR"

    const/16 v2, 0xc

    const-string v3, "profile_sidebar_border_color"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;->PROFILE_SIDEBAR_BORDER_COLOR:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;

    .line 52
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;

    const-string v1, "PROFILE_SIDEBAR_FILL_COLOR"

    const/16 v2, 0xd

    const-string v3, "profile_sidebar_fill_color"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;->PROFILE_SIDEBAR_FILL_COLOR:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;

    .line 53
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;

    const-string v1, "PROFILE_TEXT_COLOR"

    const/16 v2, 0xe

    const-string v3, "profile_text_color"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;->PROFILE_TEXT_COLOR:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;

    .line 54
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;

    const-string v1, "TREND_LOCATION_WOEID"

    const/16 v2, 0xf

    const-string v3, "trend_location_woeid"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;->TREND_LOCATION_WOEID:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;

    .line 55
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;

    const-string v1, "SLEEP_TIME_ENABLED"

    const/16 v2, 0x10

    const-string v3, "sleep_time_enabled"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;->SLEEP_TIME_ENABLED:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;

    .line 56
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;

    const-string v1, "START_SLEEP_TIME"

    const/16 v2, 0x11

    const-string v3, "start_sleep_time"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;->START_SLEEP_TIME:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;

    .line 57
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;

    const-string v1, "END_SLEEP_TIME"

    const/16 v2, 0x12

    const-string v3, "end_sleep_time"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;->END_SLEEP_TIME:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;

    .line 58
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;

    const-string v1, "TIME_ZONE"

    const/16 v2, 0x13

    const-string v3, "time_zone"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;->TIME_ZONE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;

    .line 59
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;

    const-string v1, "LANG"

    const/16 v2, 0x14

    const-string v3, "lang"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;->LANG:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;

    .line 60
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;

    const-string v1, "FILENAME"

    const/16 v2, 0x15

    const-string v3, "filename"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;->FILENAME:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;

    .line 38
    const/16 v0, 0x16

    new-array v0, v0, [Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;

    sget-object v1, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;->INCLUDE_ENTITIES:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;->SKIP_STATUS:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;->DEVICE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;->NAME:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;

    aput-object v1, v0, v7

    sget-object v1, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;->URL:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;->LOCATION:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;->DESCRIPTION:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;->IMAGE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;->TILE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;->USE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;->PROFILE_BACKGROUND_COLOR:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;->PROFILE_LINK_COLOR:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;->PROFILE_SIDEBAR_BORDER_COLOR:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;->PROFILE_SIDEBAR_FILL_COLOR:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;->PROFILE_TEXT_COLOR:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;->TREND_LOCATION_WOEID:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;->SLEEP_TIME_ENABLED:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;->START_SLEEP_TIME:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;->END_SLEEP_TIME:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;->TIME_ZONE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;->LANG:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;->FILENAME:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;->$VALUES:[Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "param"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 64
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 65
    iput-object p3, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;->mParam:Ljava/lang/String;

    .line 66
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 38
    const-class v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;
    .locals 1

    .prologue
    .line 38
    sget-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;->$VALUES:[Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;

    invoke-virtual {v0}, [Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;

    return-object v0
.end method


# virtual methods
.method public getParam()Ljava/lang/String;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;->mParam:Ljava/lang/String;

    return-object v0
.end method
