.class public interface abstract Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$SyncPhotoColumns;
.super Ljava/lang/Object;
.source "SnsFacebookDB.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "SyncPhotoColumns"
.end annotation


# static fields
.field public static final COMMENTS_COUNT:Ljava/lang/String; = "comments_count"

.field public static final CREATED_TIME:Ljava/lang/String; = "created_time"

.field public static final FROM_ID:Ljava/lang/String; = "from_id"

.field public static final FROM_NAME:Ljava/lang/String; = "from_name"

.field public static final HEIGHT:Ljava/lang/String; = "height"

.field public static final ICON:Ljava/lang/String; = "icon"

.field public static final ID:Ljava/lang/String; = "id"

.field public static final LATITUDE:Ljava/lang/String; = "latitude"

.field public static final LIKES_COUNT:Ljava/lang/String; = "likes_count"

.field public static final LINK:Ljava/lang/String; = "link"

.field public static final LOCATION_NAME:Ljava/lang/String; = "location_name"

.field public static final LONGITUDE:Ljava/lang/String; = "longitude"

.field public static final PHOTO_NAME:Ljava/lang/String; = "name"

.field public static final PICTURE:Ljava/lang/String; = "picture"

.field public static final POSITION:Ljava/lang/String; = "position"

.field public static final SOURCE:Ljava/lang/String; = "source"

.field public static final TARGET_ID:Ljava/lang/String; = "target_id"

.field public static final UPDATED_TIME:Ljava/lang/String; = "updated_time"

.field public static final WIDTH:Ljava/lang/String; = "width"
