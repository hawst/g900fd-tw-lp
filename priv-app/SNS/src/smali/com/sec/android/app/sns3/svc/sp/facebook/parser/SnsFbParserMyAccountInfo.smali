.class public Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserMyAccountInfo;
.super Ljava/lang/Object;
.source "SnsFbParserMyAccountInfo.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserMyAccountInfo$FacebookMyAccountInfo;
    }
.end annotation


# instance fields
.field mMyAccountInfo:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMyAccountInfo;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserMyAccountInfo;->mMyAccountInfo:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMyAccountInfo;

    .line 31
    return-void
.end method

.method private parseQuery1(Ljava/lang/String;)V
    .locals 6
    .param p1, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 97
    const/4 v3, 0x0

    .line 100
    .local v3, "jsonObject":Lorg/json/JSONObject;
    :try_start_0
    new-instance v2, Lorg/json/JSONArray;

    invoke-direct {v2, p1}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 102
    .local v2, "ja":Lorg/json/JSONArray;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v4

    if-ge v1, v4, :cond_0

    .line 104
    new-instance v4, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMyAccountInfo;

    invoke-direct {v4}, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMyAccountInfo;-><init>()V

    iput-object v4, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserMyAccountInfo;->mMyAccountInfo:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMyAccountInfo;

    .line 106
    invoke-virtual {v2, v1}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v3

    .line 108
    iget-object v4, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserMyAccountInfo;->mMyAccountInfo:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMyAccountInfo;

    const-string v5, "id"

    invoke-virtual {v3, v5}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMyAccountInfo;->mId:Ljava/lang/String;

    .line 111
    iget-object v4, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserMyAccountInfo;->mMyAccountInfo:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMyAccountInfo;

    const-string v5, "name"

    invoke-virtual {v3, v5}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMyAccountInfo;->mName:Ljava/lang/String;

    .line 114
    iget-object v4, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserMyAccountInfo;->mMyAccountInfo:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMyAccountInfo;

    const-string v5, "pic"

    invoke-virtual {v3, v5}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMyAccountInfo;->mPic:Ljava/lang/String;

    .line 117
    iget-object v4, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserMyAccountInfo;->mMyAccountInfo:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMyAccountInfo;

    const-string v5, "pic_big"

    invoke-virtual {v3, v5}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMyAccountInfo;->mPic_big:Ljava/lang/String;

    .line 120
    iget-object v4, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserMyAccountInfo;->mMyAccountInfo:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMyAccountInfo;

    const-string v5, "username"

    invoke-virtual {v3, v5}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMyAccountInfo;->mUserName:Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 102
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 124
    .end local v1    # "i":I
    .end local v2    # "ja":Lorg/json/JSONArray;
    :catch_0
    move-exception v0

    .line 125
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    .line 127
    .end local v0    # "e":Lorg/json/JSONException;
    :cond_0
    return-void
.end method

.method private parseQuery2(Ljava/lang/String;)V
    .locals 7
    .param p1, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 131
    const/4 v3, 0x0

    .line 134
    .local v3, "jsonObject":Lorg/json/JSONObject;
    :try_start_0
    new-instance v2, Lorg/json/JSONArray;

    invoke-direct {v2, p1}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 136
    .local v2, "ja":Lorg/json/JSONArray;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v5

    if-ge v1, v5, :cond_0

    .line 138
    invoke-virtual {v2, v1}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v3

    .line 140
    const-string v5, "uid"

    invoke-virtual {v3, v5}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 142
    .local v4, "uid":Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserMyAccountInfo;->mMyAccountInfo:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMyAccountInfo;

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserMyAccountInfo;->mMyAccountInfo:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMyAccountInfo;

    iget-object v5, v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMyAccountInfo;->mId:Ljava/lang/String;

    invoke-virtual {v5, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 144
    iget-object v5, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserMyAccountInfo;->mMyAccountInfo:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMyAccountInfo;

    const-string v6, "email"

    invoke-virtual {v3, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMyAccountInfo;->mEmail:Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 154
    .end local v1    # "i":I
    .end local v2    # "ja":Lorg/json/JSONArray;
    .end local v4    # "uid":Ljava/lang/String;
    :cond_0
    :goto_1
    return-void

    .line 136
    .restart local v1    # "i":I
    .restart local v2    # "ja":Lorg/json/JSONArray;
    .restart local v4    # "uid":Ljava/lang/String;
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 151
    .end local v1    # "i":I
    .end local v2    # "ja":Lorg/json/JSONArray;
    .end local v4    # "uid":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 152
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_1
.end method


# virtual methods
.method public parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMyAccountInfo;
    .locals 7
    .param p1, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 69
    :try_start_0
    new-instance v2, Lorg/json/JSONArray;

    invoke-direct {v2, p1}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 71
    .local v2, "ja":Lorg/json/JSONArray;
    if-eqz v2, :cond_2

    .line 73
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v5

    if-ge v1, v5, :cond_2

    .line 75
    invoke-virtual {v2, v1}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v5

    const-string v6, "name"

    invoke-virtual {v5, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 77
    .local v3, "queryName":Ljava/lang/String;
    invoke-virtual {v2, v1}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v5

    const-string v6, "fql_result_set"

    invoke-virtual {v5, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 80
    .local v4, "queryObject":Ljava/lang/String;
    const-string v5, "query1"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 81
    invoke-direct {p0, v4}, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserMyAccountInfo;->parseQuery1(Ljava/lang/String;)V

    .line 73
    :cond_0
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 82
    :cond_1
    const-string v5, "query2"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 83
    invoke-direct {p0, v4}, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserMyAccountInfo;->parseQuery2(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 88
    .end local v1    # "i":I
    .end local v2    # "ja":Lorg/json/JSONArray;
    .end local v3    # "queryName":Ljava/lang/String;
    .end local v4    # "queryObject":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 89
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    .line 92
    .end local v0    # "e":Lorg/json/JSONException;
    :cond_2
    iget-object v5, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserMyAccountInfo;->mMyAccountInfo:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMyAccountInfo;

    return-object v5
.end method
