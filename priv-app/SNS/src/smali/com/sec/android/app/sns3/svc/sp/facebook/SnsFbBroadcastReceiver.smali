.class public Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "SnsFbBroadcastReceiver.java"


# static fields
.field private static mNB:Landroid/app/Notification$Builder;

.field private static mNM:Landroid/app/NotificationManager;

.field private static mProgress:J


# instance fields
.field private mContext:Landroid/content/Context;

.field private final mNotificationId:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 34
    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbBroadcastReceiver;->mNM:Landroid/app/NotificationManager;

    .line 35
    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbBroadcastReceiver;->mNB:Landroid/app/Notification$Builder;

    .line 39
    const-wide/16 v0, 0x0

    sput-wide v0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbBroadcastReceiver;->mProgress:J

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 38
    const v0, 0xb7d65

    iput v0, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbBroadcastReceiver;->mNotificationId:I

    return-void
.end method

.method private UpLoadNotificationInit()V
    .locals 2

    .prologue
    .line 75
    const-string v0, "SNS"

    const-string v1, "SnsFbBroadcastReceiver: UpLoadNotificationInit()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 76
    sget-object v0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbBroadcastReceiver;->mNM:Landroid/app/NotificationManager;

    if-eqz v0, :cond_0

    .line 77
    sget-object v0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbBroadcastReceiver;->mNM:Landroid/app/NotificationManager;

    const v1, 0xb7d65

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    .line 78
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbBroadcastReceiver;->mNB:Landroid/app/Notification$Builder;

    .line 80
    :cond_0
    return-void
.end method

.method private UpLoadStartNotification()V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 83
    const-string v2, "SNS"

    const-string v3, "SnsFbBroadcastReceiver: UpLoadStartNotification"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 84
    sget-object v2, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbBroadcastReceiver;->mNM:Landroid/app/NotificationManager;

    if-nez v2, :cond_0

    .line 85
    iget-object v2, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbBroadcastReceiver;->mContext:Landroid/content/Context;

    const-string v3, "notification"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/NotificationManager;

    sput-object v2, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbBroadcastReceiver;->mNM:Landroid/app/NotificationManager;

    .line 90
    :goto_0
    const-wide/16 v2, 0x0

    sput-wide v2, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbBroadcastReceiver;->mProgress:J

    .line 92
    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.sec.android.app.sns3.svc.sp.facebook.SNS_FB_NOTIFICATION"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 93
    .local v0, "notiIntent":Landroid/content/Intent;
    iget-object v2, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbBroadcastReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 95
    iget-object v2, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbBroadcastReceiver;->mContext:Landroid/content/Context;

    invoke-static {v2, v8, v0, v8}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 97
    .local v1, "pendingIntent":Landroid/app/PendingIntent;
    new-instance v2, Landroid/app/Notification$Builder;

    iget-object v3, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbBroadcastReceiver;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    iget-object v3, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbBroadcastReceiver;->mContext:Landroid/content/Context;

    const v4, 0x7f080040

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbBroadcastReceiver;->mContext:Landroid/content/Context;

    const v4, 0x7f080043

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/Notification$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbBroadcastReceiver;->mContext:Landroid/content/Context;

    const v4, 0x7f080044

    new-array v5, v9, [Ljava/lang/Object;

    sget-wide v6, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbBroadcastReceiver;->mProgress:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Landroid/app/Notification$Builder;->setWhen(J)Landroid/app/Notification$Builder;

    move-result-object v2

    invoke-virtual {v2, v9}, Landroid/app/Notification$Builder;->setOngoing(Z)Landroid/app/Notification$Builder;

    move-result-object v2

    invoke-virtual {v2, v8}, Landroid/app/Notification$Builder;->setAutoCancel(Z)Landroid/app/Notification$Builder;

    move-result-object v2

    const/16 v3, 0x64

    invoke-virtual {v2, v3, v8, v8}, Landroid/app/Notification$Builder;->setProgress(IIZ)Landroid/app/Notification$Builder;

    move-result-object v2

    const v3, 0x7f02001a

    invoke-virtual {v2, v3}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v2

    sput-object v2, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbBroadcastReceiver;->mNB:Landroid/app/Notification$Builder;

    .line 108
    sget-object v2, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbBroadcastReceiver;->mNM:Landroid/app/NotificationManager;

    const v3, 0xb7d65

    sget-object v4, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbBroadcastReceiver;->mNB:Landroid/app/Notification$Builder;

    invoke-virtual {v4}, Landroid/app/Notification$Builder;->getNotification()Landroid/app/Notification;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 109
    return-void

    .line 87
    .end local v0    # "notiIntent":Landroid/content/Intent;
    .end local v1    # "pendingIntent":Landroid/app/PendingIntent;
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbBroadcastReceiver;->UpLoadNotificationInit()V

    goto/16 :goto_0
.end method

.method private UpLoadstateNotification(J)V
    .locals 7
    .param p1, "progressCount"    # J

    .prologue
    const/4 v5, 0x0

    .line 112
    const-string v0, "SNS"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SnsFbBroadcastReceiver: UpLoadstateNotification() uploading...  progress = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " %"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 113
    sget-object v0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbBroadcastReceiver;->mNM:Landroid/app/NotificationManager;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbBroadcastReceiver;->mNB:Landroid/app/Notification$Builder;

    if-eqz v0, :cond_0

    .line 114
    sput-wide p1, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbBroadcastReceiver;->mProgress:J

    .line 116
    sget-object v0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbBroadcastReceiver;->mNB:Landroid/app/Notification$Builder;

    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbBroadcastReceiver;->mContext:Landroid/content/Context;

    const v2, 0x7f080044

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    long-to-int v4, p1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v0

    const/16 v1, 0x64

    long-to-int v2, p1

    invoke-virtual {v0, v1, v2, v5}, Landroid/app/Notification$Builder;->setProgress(IIZ)Landroid/app/Notification$Builder;

    .line 119
    sget-object v0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbBroadcastReceiver;->mNM:Landroid/app/NotificationManager;

    const v1, 0xb7d65

    sget-object v2, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbBroadcastReceiver;->mNB:Landroid/app/Notification$Builder;

    invoke-virtual {v2}, Landroid/app/Notification$Builder;->getNotification()Landroid/app/Notification;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 121
    :cond_0
    return-void
.end method

.method private UpLoadstopNotification(Z)V
    .locals 9
    .param p1, "success"    # Z

    .prologue
    const v8, 0xb7d65

    const/4 v6, 0x0

    .line 124
    const-string v4, "SNS"

    const-string v5, "SnsFbBroadcastReceiver: UpLoadstopNotification() Stop notification"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 126
    sget-object v4, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbBroadcastReceiver;->mNM:Landroid/app/NotificationManager;

    if-eqz v4, :cond_0

    .line 127
    sget-object v4, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbBroadcastReceiver;->mNM:Landroid/app/NotificationManager;

    invoke-virtual {v4, v8}, Landroid/app/NotificationManager;->cancel(I)V

    .line 128
    const/4 v4, 0x0

    sput-object v4, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbBroadcastReceiver;->mNB:Landroid/app/Notification$Builder;

    .line 130
    new-instance v2, Landroid/content/Intent;

    const-string v4, "com.sec.android.app.sns3.svc.sp.facebook.SNS_FB_NOTIFICATION"

    invoke-direct {v2, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 131
    .local v2, "notiIntent":Landroid/content/Intent;
    iget-object v4, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbBroadcastReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 133
    iget-object v4, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbBroadcastReceiver;->mContext:Landroid/content/Context;

    invoke-static {v4, v6, v2, v6}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    .line 136
    .local v3, "pendingIntent":Landroid/app/PendingIntent;
    if-eqz p1, :cond_1

    .line 137
    iget-object v4, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbBroadcastReceiver;->mContext:Landroid/content/Context;

    const v5, 0x7f080042

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 141
    .local v0, "contentText":Ljava/lang/String;
    :goto_0
    new-instance v4, Landroid/app/Notification$Builder;

    iget-object v5, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbBroadcastReceiver;->mContext:Landroid/content/Context;

    invoke-direct {v4, v5}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Landroid/app/Notification$Builder;->setAutoCancel(Z)Landroid/app/Notification$Builder;

    move-result-object v4

    const v5, 0x7f02001a

    invoke-virtual {v4, v5}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbBroadcastReceiver;->mContext:Landroid/content/Context;

    const v6, 0x7f080040

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v4

    const/4 v5, -0x1

    invoke-virtual {v4, v5}, Landroid/app/Notification$Builder;->setDefaults(I)Landroid/app/Notification$Builder;

    move-result-object v4

    invoke-virtual {v4, v0}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Landroid/app/Notification$Builder;->setWhen(J)Landroid/app/Notification$Builder;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v1

    .line 150
    .local v1, "nb":Landroid/app/Notification$Builder;
    sget-object v4, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbBroadcastReceiver;->mNM:Landroid/app/NotificationManager;

    invoke-virtual {v1}, Landroid/app/Notification$Builder;->getNotification()Landroid/app/Notification;

    move-result-object v5

    invoke-virtual {v4, v8, v5}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 152
    .end local v0    # "contentText":Ljava/lang/String;
    .end local v1    # "nb":Landroid/app/Notification$Builder;
    .end local v2    # "notiIntent":Landroid/content/Intent;
    .end local v3    # "pendingIntent":Landroid/app/PendingIntent;
    :cond_0
    return-void

    .line 139
    .restart local v2    # "notiIntent":Landroid/content/Intent;
    .restart local v3    # "pendingIntent":Landroid/app/PendingIntent;
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbBroadcastReceiver;->mContext:Landroid/content/Context;

    const v5, 0x7f080041

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .restart local v0    # "contentText":Ljava/lang/String;
    goto :goto_0
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 43
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 44
    .local v0, "action":Ljava/lang/String;
    const-string v4, "SNS"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "SnsFbBroadcastReceiver: onReceive() action = [ "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " ]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 46
    iput-object p1, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbBroadcastReceiver;->mContext:Landroid/content/Context;

    .line 48
    const-string v4, "com.sec.android.app.sns3.svc.sp.facebook.SNS_FB_NOTIFICATION"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 50
    const-string v4, "SNS_FB_NOTIFICATION.progress"

    const-wide/16 v6, -0x1

    invoke-virtual {p2, v4, v6, v7}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    .line 51
    .local v2, "progress":J
    const-string v4, "SNS_FB_NOTIFICATION.state"

    const/4 v5, -0x1

    invoke-virtual {p2, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 53
    .local v1, "sns_state":I
    const-string v4, "SNS"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "SnsFbBroadcastReceiver: onReceive() -- SnsFbService.SNS_FB_PROGRESS_STATE = [ "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " ]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 55
    packed-switch v1, :pswitch_data_0

    .line 69
    const-string v4, "SNS"

    const-string v5, "SnsFbBroadcastReceiver: onReceive() Not yet Start uploading Facebook Photo!"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 72
    .end local v1    # "sns_state":I
    .end local v2    # "progress":J
    :cond_0
    :goto_0
    return-void

    .line 57
    .restart local v1    # "sns_state":I
    .restart local v2    # "progress":J
    :pswitch_0
    invoke-direct {p0}, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbBroadcastReceiver;->UpLoadStartNotification()V

    goto :goto_0

    .line 60
    :pswitch_1
    invoke-direct {p0, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbBroadcastReceiver;->UpLoadstateNotification(J)V

    goto :goto_0

    .line 63
    :pswitch_2
    const/4 v4, 0x1

    invoke-direct {p0, v4}, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbBroadcastReceiver;->UpLoadstopNotification(Z)V

    goto :goto_0

    .line 66
    :pswitch_3
    const/4 v4, 0x0

    invoke-direct {p0, v4}, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbBroadcastReceiver;->UpLoadstopNotification(Z)V

    goto :goto_0

    .line 55
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
