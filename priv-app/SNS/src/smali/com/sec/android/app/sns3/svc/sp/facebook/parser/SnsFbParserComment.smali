.class public Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserComment;
.super Ljava/lang/Object;
.source "SnsFbParserComment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserComment$FacebookComment;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    return-void
.end method

.method public static parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseComment;
    .locals 4
    .param p0, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 48
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseComment;

    invoke-direct {v0}, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseComment;-><init>()V

    .line 51
    .local v0, "comment":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseComment;
    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 53
    .local v2, "jsonObject":Lorg/json/JSONObject;
    const-string v3, "id"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseComment;->mCommentID:Ljava/lang/String;

    .line 55
    const-string v3, "from"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 56
    const-string v3, "from"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserFrom;->parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFrom;

    move-result-object v3

    iput-object v3, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseComment;->mFrom:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFrom;

    .line 60
    :cond_0
    const-string v3, "message"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseComment;->mMessage:Ljava/lang/String;

    .line 61
    const-string v3, "created_time"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseComment;->mCreatedTime:Ljava/lang/String;

    .line 62
    const-string v3, "can_remove"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseComment;->mCanRemove:Ljava/lang/String;

    .line 63
    const-string v3, "likes"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseComment;->mLikes:Ljava/lang/String;

    .line 64
    const-string v3, "user_likes"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseComment;->mUserLikes:Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 70
    .end local v2    # "jsonObject":Lorg/json/JSONObject;
    :goto_0
    return-object v0

    .line 66
    :catch_0
    move-exception v1

    .line 67
    .local v1, "e":Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method
