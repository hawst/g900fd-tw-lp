.class Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetFeed$1;
.super Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqGetFeeds;
.source "SnsFbCmdGetFeed.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetFeed;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Handler;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetFeed;


# direct methods
.method constructor <init>(Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetFeed;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;)V
    .locals 0
    .param p2, "x0"    # Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    .param p3, "x1"    # Ljava/lang/String;

    .prologue
    .line 51
    iput-object p1, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetFeed$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetFeed;

    invoke-direct {p0, p2, p3}, Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqGetFeeds;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public onReqRespond(IZIILandroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFeed;)Z
    .locals 10
    .param p1, "reqID"    # I
    .param p2, "bSuccess"    # Z
    .param p3, "httpStatus"    # I
    .param p4, "errorCode"    # I
    .param p5, "reason"    # Landroid/os/Bundle;
    .param p6, "feed"    # Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFeed;

    .prologue
    .line 57
    new-instance v6, Landroid/content/ContentValues;

    invoke-direct {v6}, Landroid/content/ContentValues;-><init>()V

    .line 58
    .local v6, "values":Landroid/content/ContentValues;
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/sns3/SnsApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 60
    .local v0, "cr":Landroid/content/ContentResolver;
    if-eqz p2, :cond_4

    .line 61
    if-eqz p6, :cond_3

    .line 63
    move-object/from16 v1, p6

    .line 64
    .local v1, "curFeed":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFeed;
    :goto_0
    if-eqz v1, :cond_3

    .line 65
    iget-object v4, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFeed;->mMessage:Ljava/lang/String;

    .line 66
    .local v4, "message":Ljava/lang/String;
    iget-object v3, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFeed;->mLink:Ljava/lang/String;

    .line 67
    .local v3, "link":Ljava/lang/String;
    iget-object v2, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFeed;->mLatitude:Ljava/lang/String;

    .line 68
    .local v2, "latitude":Ljava/lang/String;
    invoke-virtual {v6}, Landroid/content/ContentValues;->clear()V

    .line 70
    const-string v7, "feed_id"

    iget-object v8, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFeed;->mFeedID:Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    const-string v7, "type"

    iget-object v8, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFeed;->mType:Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    const-string v7, "icon"

    iget-object v8, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFeed;->mIcon:Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    const-string v7, "caption"

    iget-object v8, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFeed;->mCaption:Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_2

    .line 75
    const-string v7, "message"

    invoke-virtual {v6, v7, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    :cond_0
    const-string v7, "description"

    iget-object v8, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFeed;->mDescription:Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    const-string v7, "picture"

    iget-object v8, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFeed;->mPicture:Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    const-string v7, "link"

    iget-object v8, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFeed;->mLink:Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    const-string v7, "name"

    iget-object v8, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFeed;->mName:Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    const-string v7, "source"

    iget-object v8, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFeed;->mSource:Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    const-string v7, "created_time"

    iget-object v8, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFeed;->mCreatedTime:Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    const-string v7, "updated_time"

    iget-object v8, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFeed;->mUpdatedTime:Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    const-string v7, "from_id"

    iget-object v8, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFeed;->mFrom:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFrom;

    iget-object v8, v8, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFrom;->mFromID:Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    const-string v7, "from_name"

    iget-object v8, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFeed;->mFrom:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFrom;

    iget-object v8, v8, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFrom;->mFromName:Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 91
    const-string v7, "location_name"

    iget-object v8, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFeed;->mLocationName:Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    const-string v7, "location_latitude"

    iget-object v8, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFeed;->mLatitude:Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    const-string v7, "location_longitude"

    iget-object v8, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFeed;->mLongitude:Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    const-string v7, "photo_id"

    iget-object v8, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFeed;->mPhotoId:Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    sget-object v7, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$FeedList;->CONTENT_URI:Landroid/net/Uri;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "feed_id=\'"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFeed;->mFeedID:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "\'"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v0, v7, v6, v8, v9}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v5

    .line 97
    .local v5, "update":I
    if-nez v5, :cond_1

    .line 98
    sget-object v7, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$FeedList;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v7, v6}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 101
    :cond_1
    iget-object v1, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFeed;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFeed;

    .line 102
    goto/16 :goto_0

    .line 78
    .end local v5    # "update":I
    :cond_2
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 79
    iget-object v1, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFeed;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFeed;

    .line 80
    goto/16 :goto_0

    .line 104
    .end local v1    # "curFeed":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFeed;
    .end local v2    # "latitude":Ljava/lang/String;
    .end local v3    # "link":Ljava/lang/String;
    .end local v4    # "message":Ljava/lang/String;
    :cond_3
    iget-object v7, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetFeed$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetFeed;

    sget-object v8, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$FeedList;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v8}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetFeed;->setUri(Ljava/lang/String;)V

    .line 111
    :goto_1
    iget-object v7, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetFeed$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetFeed;

    invoke-virtual {v7, p2}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetFeed;->setSuccess(Z)V

    .line 112
    iget-object v7, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetFeed$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetFeed;

    invoke-virtual {v7, p0}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetFeed;->receive(Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;)V

    .line 114
    const/4 v7, 0x1

    return v7

    .line 106
    :cond_4
    iget-object v7, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetFeed$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetFeed;

    new-instance v8, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;

    const-string v9, "facebook"

    invoke-direct {v8, v9, p3, p4, p5}, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;-><init>(Ljava/lang/String;IILandroid/os/Bundle;)V

    invoke-virtual {v7, v8}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetFeed;->setResponseList(Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;)Z

    .line 108
    iget-object v7, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetFeed$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetFeed;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetFeed;->setUri(Ljava/lang/String;)V

    goto :goto_1
.end method
