.class Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSettingActivity$1;
.super Ljava/lang/Object;
.source "SnsAccountFbSyncSettingActivity.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSettingActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSettingActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSettingActivity;)V
    .locals 0

    .prologue
    .line 77
    iput-object p1, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSettingActivity$1;->this$0:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSettingActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 6
    .param p1, "preference"    # Landroid/preference/Preference;
    .param p2, "newValue"    # Ljava/lang/Object;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 80
    instance-of v3, p2, Ljava/lang/Boolean;

    if-eqz v3, :cond_1

    .line 81
    check-cast p2, Ljava/lang/Boolean;

    .end local p2    # "newValue":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 83
    .local v0, "isChecked":Z
    if-eqz v0, :cond_2

    .line 84
    const-string v3, "SnsAccountFbSyncSettingActivity"

    const-string v4, "Sync ON"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 85
    iget-object v3, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSettingActivity$1;->this$0:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSettingActivity;

    # invokes: Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSettingActivity;->isLoggedInSnsFacebook()Z
    invoke-static {v3}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSettingActivity;->access$000(Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSettingActivity;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 86
    iget-object v2, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSettingActivity$1;->this$0:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSettingActivity;

    # invokes: Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSettingActivity;->tryFacebookSSO()V
    invoke-static {v2}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSettingActivity;->access$100(Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSettingActivity;)V

    .line 104
    .end local v0    # "isChecked":Z
    :goto_0
    return v1

    .line 89
    .restart local v0    # "isChecked":Z
    :cond_0
    const-string v1, "SnsAccountFbSyncSettingActivity"

    const-string v3, "already logged in"

    invoke-static {v1, v3}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .end local v0    # "isChecked":Z
    :cond_1
    :goto_1
    move v1, v2

    .line 104
    goto :goto_0

    .line 92
    .restart local v0    # "isChecked":Z
    :cond_2
    const-string v3, "SnsAccountFbSyncSettingActivity"

    const-string v4, "Sync OFF"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 93
    iget-object v3, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSettingActivity$1;->this$0:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSettingActivity;

    # invokes: Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSettingActivity;->isLoggedInSnsFacebook()Z
    invoke-static {v3}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSettingActivity;->access$000(Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSettingActivity;)Z

    move-result v3

    if-ne v3, v2, :cond_3

    .line 94
    iget-object v3, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSettingActivity$1;->this$0:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSettingActivity;

    # invokes: Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSettingActivity;->removeSnsFacebookAccout()V
    invoke-static {v3}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSettingActivity;->access$200(Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSettingActivity;)V

    .line 99
    :goto_2
    iget-object v3, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSettingActivity$1;->this$0:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSettingActivity;

    invoke-virtual {v3}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSettingActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSettingActivity$1;->this$0:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSettingActivity;

    const v5, 0x7f080051

    invoke-virtual {v4, v5}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSettingActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_1

    .line 96
    :cond_3
    const-string v3, "SnsAccountFbSyncSettingActivity"

    const-string v4, "already logged out"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method
