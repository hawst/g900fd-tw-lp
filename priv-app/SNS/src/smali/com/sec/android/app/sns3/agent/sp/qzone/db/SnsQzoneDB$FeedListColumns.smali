.class public interface abstract Lcom/sec/android/app/sns3/agent/sp/qzone/db/SnsQzoneDB$FeedListColumns;
.super Ljava/lang/Object;
.source "SnsQzoneDB.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/agent/sp/qzone/db/SnsQzoneDB;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "FeedListColumns"
.end annotation


# static fields
.field public static final AUTHOR_NAME:Ljava/lang/String; = "author_name"

.field public static final AUTHOR_OPENID:Ljava/lang/String; = "author_openid"

.field public static final AUTHOR_PHONENUMBER:Ljava/lang/String; = "author_phonenumber"

.field public static final COMMENT_COUNT:Ljava/lang/String; = "comment_count"

.field public static final CREATE_TIME:Ljava/lang/String; = "create_time"

.field public static final FEED_ID:Ljava/lang/String; = "feed_id"

.field public static final HOST_OPENID:Ljava/lang/String; = "host_openid"

.field public static final LIKE_COUNT:Ljava/lang/String; = "like_count"

.field public static final LINK_URI:Ljava/lang/String; = "link_uri"

.field public static final MESSAGE:Ljava/lang/String; = "message"

.field public static final ORIGINAL_AUTHOR_NAME:Ljava/lang/String; = "original_author_name"

.field public static final ORIGINAL_MESSAGE:Ljava/lang/String; = "original_message"

.field public static final ORIGINAL_PICTURE:Ljava/lang/String; = "original_picture"

.field public static final ORIGINAL_PICTURE_COUNT:Ljava/lang/String; = "original_picture_count"

.field public static final ORIGINAL_TITLE:Ljava/lang/String; = "original_title"

.field public static final PICTURE:Ljava/lang/String; = "picture"

.field public static final PICTURE_COUNT:Ljava/lang/String; = "picture_count"

.field public static final PROFILE_URL:Ljava/lang/String; = "profile_url"

.field public static final TITLE:Ljava/lang/String; = "title"

.field public static final TYPE:Ljava/lang/String; = "type"
