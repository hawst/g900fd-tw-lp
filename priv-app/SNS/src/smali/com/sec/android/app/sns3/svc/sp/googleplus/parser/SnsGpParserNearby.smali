.class public Lcom/sec/android/app/sns3/svc/sp/googleplus/parser/SnsGpParserNearby;
.super Ljava/lang/Object;
.source "SnsGpParserNearby.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/googleplus/parser/SnsGpParserNearby$GooglePlusNearbySearch;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    return-void
.end method

.method public static parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseNearby;
    .locals 10
    .param p0, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 85
    new-instance v5, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseNearby;

    invoke-direct {v5}, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseNearby;-><init>()V

    .line 86
    .local v5, "result":Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseNearby;
    const/4 v4, 0x0

    .line 88
    .local v4, "nearbyObj":Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseNearbyItem;
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 90
    .local v1, "con":Lorg/json/JSONObject;
    const-string v8, "status"

    invoke-virtual {v1, v8}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, v5, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseNearby;->mStatus:Ljava/lang/String;

    .line 91
    const-string v8, "next_page_token"

    invoke-virtual {v1, v8}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, v5, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseNearby;->mNextPageToken:Ljava/lang/String;

    .line 92
    const-string v8, "html_attributions"

    invoke-virtual {v1, v8}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    .line 93
    .local v0, "attributions":Lorg/json/JSONArray;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v8

    if-ge v3, v8, :cond_0

    .line 94
    iget-object v8, v5, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseNearby;->mHtmlAttributions:Ljava/util/List;

    invoke-virtual {v0, v3}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-interface {v8, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 93
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 96
    :cond_0
    const-string v8, "OK"

    iget-object v9, v5, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseNearby;->mStatus:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 97
    const-string v8, "results"

    invoke-virtual {v1, v8}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v7

    .line 98
    .local v7, "results":Lorg/json/JSONArray;
    const/4 v3, 0x0

    :goto_1
    invoke-virtual {v7}, Lorg/json/JSONArray;->length()I

    move-result v8

    if-ge v3, v8, :cond_1

    .line 99
    invoke-virtual {v7, v3}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v6

    .line 100
    .local v6, "resultItem":Lorg/json/JSONObject;
    invoke-static {v6}, Lcom/sec/android/app/sns3/svc/sp/googleplus/parser/SnsGpParserNearby;->parseNearbyItem(Lorg/json/JSONObject;)Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseNearbyItem;

    move-result-object v4

    .line 101
    iget-object v8, v5, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseNearby;->mPlaces:Ljava/util/List;

    invoke-interface {v8, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 98
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 104
    .end local v0    # "attributions":Lorg/json/JSONArray;
    .end local v1    # "con":Lorg/json/JSONObject;
    .end local v3    # "i":I
    .end local v6    # "resultItem":Lorg/json/JSONObject;
    .end local v7    # "results":Lorg/json/JSONArray;
    :catch_0
    move-exception v2

    .line 105
    .local v2, "e1":Lorg/json/JSONException;
    invoke-virtual {v2}, Lorg/json/JSONException;->printStackTrace()V

    .line 107
    .end local v2    # "e1":Lorg/json/JSONException;
    :cond_1
    return-object v5
.end method

.method public static parseNearbyItem(Lorg/json/JSONObject;)Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseNearbyItem;
    .locals 14
    .param p0, "resultItem"    # Lorg/json/JSONObject;

    .prologue
    .line 112
    new-instance v7, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseNearbyItem;

    invoke-direct {v7}, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseNearbyItem;-><init>()V

    .line 114
    .local v7, "nearbyObj":Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseNearbyItem;
    :try_start_0
    const-string v12, "formatted_address"

    invoke-virtual {p0, v12}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    iput-object v12, v7, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseNearbyItem;->mFormattedAddress:Ljava/lang/String;

    .line 116
    const-string v12, "vicinity"

    invoke-virtual {p0, v12}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    iput-object v12, v7, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseNearbyItem;->mVicinity:Ljava/lang/String;

    .line 120
    const-string v12, "geometry"

    invoke-virtual {p0, v12}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    .line 122
    .local v2, "geometry":Lorg/json/JSONObject;
    const-string v12, "location"

    invoke-virtual {v2, v12}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v6

    .line 124
    .local v6, "location":Lorg/json/JSONObject;
    const-string v12, "lat"

    invoke-virtual {v6, v12}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    iput-object v12, v7, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseNearbyItem;->mLatitude:Ljava/lang/String;

    .line 126
    const-string v12, "lng"

    invoke-virtual {v6, v12}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    iput-object v12, v7, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseNearbyItem;->mLongitude:Ljava/lang/String;

    .line 128
    const-string v12, "icon"

    invoke-virtual {p0, v12}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    iput-object v12, v7, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseNearbyItem;->mIcon:Ljava/lang/String;

    .line 129
    const-string v12, "id"

    invoke-virtual {p0, v12}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    iput-object v12, v7, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseNearbyItem;->mId:Ljava/lang/String;

    .line 130
    const-string v12, "name"

    invoke-virtual {p0, v12}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    iput-object v12, v7, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseNearbyItem;->mName:Ljava/lang/String;

    .line 131
    const-string v12, "reference"

    invoke-virtual {p0, v12}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    iput-object v12, v7, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseNearbyItem;->mReference:Ljava/lang/String;

    .line 133
    const-string v12, "types"

    invoke-virtual {p0, v12}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v11

    .line 135
    .local v11, "types":Lorg/json/JSONArray;
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    iput-object v12, v7, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseNearbyItem;->mTypes:Ljava/util/List;

    .line 136
    const/4 v4, 0x0

    .local v4, "j":I
    :goto_0
    invoke-virtual {v11}, Lorg/json/JSONArray;->length()I

    move-result v12

    if-ge v4, v12, :cond_0

    .line 138
    iget-object v12, v7, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseNearbyItem;->mTypes:Ljava/util/List;

    invoke-virtual {v11, v4}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-interface {v12, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 136
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 141
    :cond_0
    const-string v12, "opening_hours"

    invoke-virtual {p0, v12}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v8

    .line 143
    .local v8, "openingHours":Lorg/json/JSONObject;
    if-eqz v8, :cond_1

    .line 144
    const-string v12, "open_now"

    invoke-virtual {v8, v12}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v12

    invoke-static {v12}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v12

    iput-object v12, v7, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseNearbyItem;->mOpenNow:Ljava/lang/Boolean;

    .line 146
    :cond_1
    const-string v12, "photos"

    invoke-virtual {p0, v12}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v10

    .line 147
    .local v10, "photos":Lorg/json/JSONArray;
    if-eqz v10, :cond_3

    .line 148
    const/4 v5, 0x0

    .local v5, "k":I
    :goto_1
    invoke-virtual {v10}, Lorg/json/JSONArray;->length()I

    move-result v12

    if-ge v5, v12, :cond_3

    .line 149
    invoke-virtual {v10, v5}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v9

    .line 150
    .local v9, "photoItem":Lorg/json/JSONObject;
    const-string v12, "height"

    invoke-virtual {v9, v12}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v12

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    iput-object v12, v7, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseNearbyItem;->mPhotoHeight:Ljava/lang/Integer;

    .line 151
    const-string v12, "html_attributions"

    invoke-virtual {v9, v12}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    .line 153
    .local v0, "attributions":Lorg/json/JSONArray;
    if-eqz v0, :cond_2

    .line 154
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_2
    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v12

    if-ge v3, v12, :cond_2

    .line 155
    iget-object v12, v7, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseNearbyItem;->mPhotoHtmlAttributions:Ljava/util/List;

    invoke-virtual {v0, v3}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-interface {v12, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 154
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 158
    .end local v3    # "i":I
    :cond_2
    const-string v12, "photo_reference"

    invoke-virtual {v9, v12}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    iput-object v12, v7, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseNearbyItem;->mPhotoReference:Ljava/lang/String;

    .line 160
    const-string v12, "width"

    invoke-virtual {v9, v12}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v12

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    iput-object v12, v7, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseNearbyItem;->mPhotoWidth:Ljava/lang/Integer;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 148
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 163
    .end local v0    # "attributions":Lorg/json/JSONArray;
    .end local v2    # "geometry":Lorg/json/JSONObject;
    .end local v4    # "j":I
    .end local v5    # "k":I
    .end local v6    # "location":Lorg/json/JSONObject;
    .end local v8    # "openingHours":Lorg/json/JSONObject;
    .end local v9    # "photoItem":Lorg/json/JSONObject;
    .end local v10    # "photos":Lorg/json/JSONArray;
    .end local v11    # "types":Lorg/json/JSONArray;
    :catch_0
    move-exception v1

    .line 164
    .local v1, "e":Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    .line 166
    .end local v1    # "e":Lorg/json/JSONException;
    :cond_3
    return-object v7
.end method

.method public static parsePlaceDetails(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseNearbyItem;
    .locals 5
    .param p0, "place"    # Ljava/lang/String;

    .prologue
    .line 171
    const/4 v1, 0x0

    .line 173
    .local v1, "placeDetail":Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseNearbyItem;
    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 174
    .local v2, "places":Lorg/json/JSONObject;
    const-string v4, "result"

    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v3

    .line 175
    .local v3, "result":Lorg/json/JSONObject;
    invoke-static {v3}, Lcom/sec/android/app/sns3/svc/sp/googleplus/parser/SnsGpParserNearby;->parseNearbyItem(Lorg/json/JSONObject;)Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseNearbyItem;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 179
    .end local v2    # "places":Lorg/json/JSONObject;
    .end local v3    # "result":Lorg/json/JSONObject;
    :goto_0
    return-object v1

    .line 176
    :catch_0
    move-exception v0

    .line 177
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method
