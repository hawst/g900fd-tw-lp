.class public Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserPhotoStream;
.super Ljava/lang/Object;
.source "SnsFbParserPhotoStream.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserPhotoStream$FacebookPhotoStream;
    }
.end annotation


# instance fields
.field mStream:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhotoStream;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserPhotoStream;->mStream:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhotoStream;

    .line 31
    return-void
.end method

.method private parsePage(Ljava/lang/String;)V
    .locals 7
    .param p1, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 266
    :try_start_0
    new-instance v3, Lorg/json/JSONArray;

    invoke-direct {v3, p1}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 268
    .local v3, "ja":Lorg/json/JSONArray;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v5

    if-ge v2, v5, :cond_2

    .line 270
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserPhotoStream;->mStream:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhotoStream;

    .line 272
    .local v0, "curPhotoStream":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhotoStream;
    invoke-virtual {v3, v2}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    .line 274
    .local v4, "jsonObj":Lorg/json/JSONObject;
    :goto_1
    if-eqz v0, :cond_1

    .line 275
    const-string v5, "page_id"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iget-object v6, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhotoStream;->mActorID:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 277
    const-string v5, "name"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhotoStream;->mName:Ljava/lang/String;

    .line 279
    const-string v5, "pic_square"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhotoStream;->mPicSquare:Ljava/lang/String;

    .line 283
    :cond_0
    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhotoStream;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhotoStream;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 268
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 286
    .end local v0    # "curPhotoStream":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhotoStream;
    .end local v2    # "i":I
    .end local v3    # "ja":Lorg/json/JSONArray;
    .end local v4    # "jsonObj":Lorg/json/JSONObject;
    :catch_0
    move-exception v1

    .line 288
    .local v1, "e":Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    .line 290
    .end local v1    # "e":Lorg/json/JSONException;
    :cond_2
    return-void
.end method

.method private parsePhoto(Ljava/lang/String;)V
    .locals 7
    .param p1, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 210
    :try_start_0
    new-instance v3, Lorg/json/JSONArray;

    invoke-direct {v3, p1}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 212
    .local v3, "ja":Lorg/json/JSONArray;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v5

    if-ge v2, v5, :cond_2

    .line 214
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserPhotoStream;->mStream:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhotoStream;

    .line 216
    .local v0, "curPhotoStream":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhotoStream;
    invoke-virtual {v3, v2}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    .line 218
    .local v4, "jsonObj":Lorg/json/JSONObject;
    :goto_1
    if-eqz v0, :cond_1

    .line 219
    const-string v5, "pid"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iget-object v6, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhotoStream;->mPID:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 221
    const-string v5, "src_big"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhotoStream;->mPhotoURL:Ljava/lang/String;

    .line 225
    :cond_0
    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhotoStream;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhotoStream;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 212
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 228
    .end local v0    # "curPhotoStream":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhotoStream;
    .end local v2    # "i":I
    .end local v3    # "ja":Lorg/json/JSONArray;
    .end local v4    # "jsonObj":Lorg/json/JSONObject;
    :catch_0
    move-exception v1

    .line 230
    .local v1, "e":Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    .line 232
    .end local v1    # "e":Lorg/json/JSONException;
    :cond_2
    return-void
.end method

.method private parseStream(Ljava/lang/String;)V
    .locals 8
    .param p1, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 134
    const/4 v0, 0x0

    .line 136
    .local v0, "curPhotoStream":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhotoStream;
    const/4 v4, 0x0

    .line 139
    .local v4, "jsonObject":Lorg/json/JSONObject;
    :try_start_0
    new-instance v3, Lorg/json/JSONArray;

    invoke-direct {v3, p1}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 141
    .local v3, "ja":Lorg/json/JSONArray;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v6

    if-ge v2, v6, :cond_4

    .line 142
    new-instance v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhotoStream;

    invoke-direct {v5}, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhotoStream;-><init>()V

    .line 144
    .local v5, "newPhotoStream":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhotoStream;
    invoke-virtual {v3, v2}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    .line 146
    const-string v6, "post_id"

    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhotoStream;->mPostID:Ljava/lang/String;

    .line 148
    const-string v6, "actor_id"

    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhotoStream;->mActorID:Ljava/lang/String;

    .line 150
    const-string v6, "message"

    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhotoStream;->mMessage:Ljava/lang/String;

    .line 152
    const-string v6, "updated_time"

    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhotoStream;->mUpdatedTime:Ljava/lang/String;

    .line 154
    const-string v6, "attachment"

    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v6

    const-string v7, "media"

    invoke-virtual {v6, v7}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v6

    const-string v7, "photo"

    invoke-virtual {v6, v7}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v6

    const-string v7, "pid"

    invoke-virtual {v6, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhotoStream;->mPID:Ljava/lang/String;

    .line 159
    const-string v6, "created_time"

    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhotoStream;->mCreatedTime:Ljava/lang/String;

    .line 162
    const-string v6, "0"

    iput-object v6, v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhotoStream;->mLikeCount:Ljava/lang/String;

    .line 163
    const-string v6, "likes"

    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 165
    const-string v6, "likes"

    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v6

    if-eqz v6, :cond_0

    .line 167
    const-string v6, "likes"

    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v6

    const-string v7, "count"

    invoke-virtual {v6, v7}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 169
    const-string v6, "likes"

    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v6

    const-string v7, "count"

    invoke-virtual {v6, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhotoStream;->mLikeCount:Ljava/lang/String;

    .line 176
    :cond_0
    const-string v6, "0"

    iput-object v6, v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhotoStream;->mCommentCount:Ljava/lang/String;

    .line 177
    const-string v6, "comments"

    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 179
    const-string v6, "comments"

    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v6

    if-eqz v6, :cond_1

    .line 181
    const-string v6, "comments"

    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v6

    const-string v7, "count"

    invoke-virtual {v6, v7}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 183
    const-string v6, "comments"

    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v6

    const-string v7, "count"

    invoke-virtual {v6, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhotoStream;->mCommentCount:Ljava/lang/String;

    .line 190
    :cond_1
    if-nez v0, :cond_3

    .line 191
    move-object v0, v5

    .line 197
    :goto_1
    iget-object v6, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserPhotoStream;->mStream:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhotoStream;

    if-nez v6, :cond_2

    .line 198
    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserPhotoStream;->mStream:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhotoStream;

    .line 141
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0

    .line 193
    :cond_3
    iput-object v5, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhotoStream;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhotoStream;

    .line 194
    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhotoStream;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhotoStream;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 201
    .end local v2    # "i":I
    .end local v3    # "ja":Lorg/json/JSONArray;
    .end local v5    # "newPhotoStream":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhotoStream;
    :catch_0
    move-exception v1

    .line 203
    .local v1, "e":Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    .line 205
    .end local v1    # "e":Lorg/json/JSONException;
    :cond_4
    return-void
.end method

.method private parseUser(Ljava/lang/String;)V
    .locals 7
    .param p1, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 237
    :try_start_0
    new-instance v3, Lorg/json/JSONArray;

    invoke-direct {v3, p1}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 239
    .local v3, "ja":Lorg/json/JSONArray;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v5

    if-ge v2, v5, :cond_2

    .line 241
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserPhotoStream;->mStream:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhotoStream;

    .line 243
    .local v0, "curPhotoStream":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhotoStream;
    invoke-virtual {v3, v2}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    .line 245
    .local v4, "jsonObj":Lorg/json/JSONObject;
    :goto_1
    if-eqz v0, :cond_1

    .line 246
    const-string v5, "uid"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iget-object v6, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhotoStream;->mActorID:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 248
    const-string v5, "name"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhotoStream;->mName:Ljava/lang/String;

    .line 250
    const-string v5, "pic_square"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhotoStream;->mPicSquare:Ljava/lang/String;

    .line 254
    :cond_0
    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhotoStream;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhotoStream;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 239
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 257
    .end local v0    # "curPhotoStream":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhotoStream;
    .end local v2    # "i":I
    .end local v3    # "ja":Lorg/json/JSONArray;
    .end local v4    # "jsonObj":Lorg/json/JSONObject;
    :catch_0
    move-exception v1

    .line 259
    .local v1, "e":Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    .line 261
    .end local v1    # "e":Lorg/json/JSONException;
    :cond_2
    return-void
.end method


# virtual methods
.method public parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhotoStream;
    .locals 7
    .param p1, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 104
    :try_start_0
    new-instance v2, Lorg/json/JSONArray;

    invoke-direct {v2, p1}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 106
    .local v2, "ja":Lorg/json/JSONArray;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v5

    if-ge v1, v5, :cond_2

    .line 108
    invoke-virtual {v2, v1}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v5

    const-string v6, "name"

    invoke-virtual {v5, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 109
    .local v3, "queryName":Ljava/lang/String;
    invoke-virtual {v2, v1}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v5

    const-string v6, "fql_result_set"

    invoke-virtual {v5, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 112
    .local v4, "queryObject":Ljava/lang/String;
    const-string v5, "stream_query"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 113
    invoke-direct {p0, v4}, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserPhotoStream;->parseStream(Ljava/lang/String;)V

    .line 106
    :cond_0
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 114
    :cond_1
    const-string v5, "photo_query"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 115
    invoke-direct {p0, v4}, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserPhotoStream;->parsePhoto(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 124
    .end local v1    # "i":I
    .end local v2    # "ja":Lorg/json/JSONArray;
    .end local v3    # "queryName":Ljava/lang/String;
    .end local v4    # "queryObject":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 126
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    .line 129
    .end local v0    # "e":Lorg/json/JSONException;
    :cond_2
    iget-object v5, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserPhotoStream;->mStream:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhotoStream;

    return-object v5

    .line 116
    .restart local v1    # "i":I
    .restart local v2    # "ja":Lorg/json/JSONArray;
    .restart local v3    # "queryName":Ljava/lang/String;
    .restart local v4    # "queryObject":Ljava/lang/String;
    :cond_3
    :try_start_1
    const-string v5, "user_query"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 117
    invoke-direct {p0, v4}, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserPhotoStream;->parseUser(Ljava/lang/String;)V

    goto :goto_1

    .line 118
    :cond_4
    const-string v5, "page_query"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 119
    invoke-direct {p0, v4}, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserPhotoStream;->parsePage(Ljava/lang/String;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method
