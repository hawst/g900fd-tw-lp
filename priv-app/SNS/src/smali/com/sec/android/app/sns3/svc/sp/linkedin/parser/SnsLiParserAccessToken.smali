.class public Lcom/sec/android/app/sns3/svc/sp/linkedin/parser/SnsLiParserAccessToken;
.super Ljava/lang/Object;
.source "SnsLiParserAccessToken.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/linkedin/parser/SnsLiParserAccessToken$LinkedInAccessToken;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    return-void
.end method

.method public static parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseAccessToken;
    .locals 13
    .param p0, "contentObj"    # Ljava/lang/String;

    .prologue
    const/4 v12, 0x1

    const/4 v11, 0x0

    .line 38
    new-instance v6, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseAccessToken;

    invoke-direct {v6}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseAccessToken;-><init>()V

    .line 39
    .local v6, "user":Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseAccessToken;
    const-string v8, "{"

    const-string v9, ""

    invoke-virtual {p0, v8, v9}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v8

    const-string v9, "}"

    const-string v10, ""

    invoke-virtual {v8, v9, v10}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    .line 40
    .local v1, "content":Ljava/lang/String;
    const-string v8, ","

    invoke-virtual {v1, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 42
    .local v5, "splitUrl":[Ljava/lang/String;
    move-object v0, v5

    .local v0, "arr$":[Ljava/lang/String;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_2

    aget-object v4, v0, v2

    .line 43
    .local v4, "parameter":Ljava/lang/String;
    const-string v8, ":"

    invoke-virtual {v4, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    .line 44
    .local v7, "v":[Ljava/lang/String;
    aget-object v8, v7, v11

    invoke-static {v8}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const-string v9, "\"access_token\""

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 45
    aget-object v8, v7, v12

    const-string v9, "\""

    const-string v10, ""

    invoke-virtual {v8, v9, v10}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, v6, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseAccessToken;->mAccessToken:Ljava/lang/String;

    .line 42
    :cond_0
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 47
    :cond_1
    aget-object v8, v7, v11

    invoke-static {v8}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const-string v9, "\"expires_in\""

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 48
    aget-object v8, v7, v12

    invoke-static {v8}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, v6, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseAccessToken;->mExpires:Ljava/lang/String;

    goto :goto_1

    .line 50
    .end local v4    # "parameter":Ljava/lang/String;
    .end local v7    # "v":[Ljava/lang/String;
    :cond_2
    return-object v6
.end method
