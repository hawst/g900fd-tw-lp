.class public Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAlbum;
.super Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;
.source "SnsFbCmdGetAlbum.java"


# instance fields
.field private mAID:Ljava/lang/String;

.field private mAlbumID:Ljava/lang/String;

.field private mLikeDone:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Handler;Ljava/lang/String;)V
    .locals 3
    .param p1, "svcMgr"    # Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    .param p2, "cmdHandler"    # Landroid/os/Handler;
    .param p3, "aID"    # Ljava/lang/String;

    .prologue
    .line 54
    invoke-direct {p0, p2}, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;-><init>(Landroid/os/Handler;)V

    .line 56
    new-instance v1, Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;

    invoke-direct {v1}, Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;-><init>()V

    .line 58
    .local v1, "unit":Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;
    iput-object p3, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAlbum;->mAID:Ljava/lang/String;

    .line 60
    new-instance v0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAlbum$1;

    iget-object v2, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAlbum;->mAID:Ljava/lang/String;

    invoke-direct {v0, p0, p1, v2}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAlbum$1;-><init>(Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAlbum;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;)V

    .line 179
    .local v0, "req1":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    invoke-virtual {v1, v0}, Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;->addRequest(Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;)V

    .line 180
    invoke-virtual {p0, v1}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAlbum;->addCommandUnit(Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;)V

    .line 181
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAlbum;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAlbum;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAlbum;->mAlbumID:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$002(Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAlbum;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAlbum;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 45
    iput-object p1, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAlbum;->mAlbumID:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$100(Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAlbum;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAlbum;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAlbum;->mLikeDone:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$102(Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAlbum;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAlbum;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 45
    iput-object p1, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAlbum;->mLikeDone:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$200(Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAlbum;Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAlbum;
    .param p1, "x1"    # Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;

    .prologue
    .line 45
    invoke-virtual {p0, p1}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAlbum;->addCommandUnit(Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;)V

    return-void
.end method


# virtual methods
.method public getAlbumID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 184
    iget-object v0, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAlbum;->mAlbumID:Ljava/lang/String;

    return-object v0
.end method

.method protected respond()Z
    .locals 5

    .prologue
    .line 189
    const-string v0, "SnsAgent"

    const-string v1, "<SnsFbCmdGetAlbum> respond()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 191
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAlbum;->getCommandCallback()Lcom/sec/android/app/sns3/agent/command/ISnsCommandCallback;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAlbum;->getCommandID()I

    move-result v1

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAlbum;->isSuccess()Z

    move-result v2

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAlbum;->getUri()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAlbum;->getResponseList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/sec/android/app/sns3/agent/command/ISnsCommandCallback;->onCmdRespond(IZLjava/lang/String;Ljava/util/List;)V

    .line 193
    const/4 v0, 0x1

    return v0
.end method
