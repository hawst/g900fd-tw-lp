.class public Lcom/sec/android/app/sns3/svc/sp/foursquare/parser/SnsFsParserFeed;
.super Ljava/lang/Object;
.source "SnsFsParserFeed.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/foursquare/parser/SnsFsParserFeed$FourSquareFeed;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    return-void
.end method

.method public static parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseFeed;
    .locals 13
    .param p0, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 89
    const/4 v3, 0x0

    .line 90
    .local v3, "feed":Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseFeed;
    const/4 v1, 0x0

    .line 93
    .local v1, "curFeed":Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseFeed;
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 94
    .local v0, "con":Lorg/json/JSONObject;
    const-string v11, "meta"

    invoke-virtual {v0, v11}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v6

    .line 95
    .local v6, "meta":Lorg/json/JSONObject;
    const-string v11, "code"

    invoke-virtual {v6, v11}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v11

    const/16 v12, 0xc8

    if-ne v11, v12, :cond_1

    .line 96
    const-string v11, "response"

    invoke-virtual {v0, v11}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v7

    .line 97
    .local v7, "response":Lorg/json/JSONObject;
    const-string v11, "checkins"

    invoke-virtual {v7, v11}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v9

    .line 98
    .local v9, "userCheckins":Lorg/json/JSONObject;
    const-string v11, "count"

    invoke-virtual {v9, v11}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v11

    if-lez v11, :cond_1

    .line 99
    const-string v11, "items"

    invoke-virtual {v9, v11}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v10

    .line 100
    .local v10, "userItems":Lorg/json/JSONArray;
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    invoke-virtual {v10}, Lorg/json/JSONArray;->length()I

    move-result v11

    if-ge v4, v11, :cond_1

    .line 101
    invoke-virtual {v10, v4}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v5

    .line 103
    .local v5, "itemsObject":Lorg/json/JSONObject;
    invoke-static {v5}, Lcom/sec/android/app/sns3/svc/sp/foursquare/parser/SnsFsParserFeed;->parseVenues(Lorg/json/JSONObject;)Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseFeed;

    move-result-object v8

    .line 105
    .local v8, "user":Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseFeed;
    if-nez v3, :cond_0

    .line 106
    move-object v3, v8

    .line 107
    move-object v1, v3

    .line 100
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 109
    :cond_0
    iput-object v8, v1, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseFeed;->mNext:Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseFeed;

    .line 110
    iget-object v1, v1, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseFeed;->mNext:Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseFeed;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 116
    .end local v0    # "con":Lorg/json/JSONObject;
    .end local v4    # "i":I
    .end local v5    # "itemsObject":Lorg/json/JSONObject;
    .end local v6    # "meta":Lorg/json/JSONObject;
    .end local v7    # "response":Lorg/json/JSONObject;
    .end local v8    # "user":Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseFeed;
    .end local v9    # "userCheckins":Lorg/json/JSONObject;
    .end local v10    # "userItems":Lorg/json/JSONArray;
    :catch_0
    move-exception v2

    .line 118
    .local v2, "e":Lorg/json/JSONException;
    invoke-virtual {v2}, Lorg/json/JSONException;->printStackTrace()V

    .line 121
    .end local v2    # "e":Lorg/json/JSONException;
    :cond_1
    return-object v3
.end method

.method public static parseCheckins(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseFeed;
    .locals 12
    .param p0, "content"    # Ljava/lang/String;

    .prologue
    .line 125
    const/4 v5, 0x0

    .line 126
    .local v5, "feed":Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseFeed;
    const/4 v3, 0x0

    .line 128
    .local v3, "curFeed":Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseFeed;
    const/4 v1, 0x0

    .line 130
    .local v1, "checkinObj":Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseFeed;
    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 131
    .local v2, "con":Lorg/json/JSONObject;
    const-string v10, "meta"

    invoke-virtual {v2, v10}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v7

    .line 133
    .local v7, "meta":Lorg/json/JSONObject;
    const-string v10, "code"

    invoke-virtual {v7, v10}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v10

    const/16 v11, 0xc8

    if-ne v10, v11, :cond_1

    .line 135
    const-string v10, "response"

    invoke-virtual {v2, v10}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v9

    .line 137
    .local v9, "response":Lorg/json/JSONObject;
    const-string v10, "recent"

    invoke-virtual {v9, v10}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v8

    .line 138
    .local v8, "recentCheckins":Lorg/json/JSONArray;
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    invoke-virtual {v8}, Lorg/json/JSONArray;->length()I

    move-result v10

    if-ge v6, v10, :cond_1

    .line 139
    invoke-virtual {v8, v6}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v0

    .line 141
    .local v0, "checkinItem":Lorg/json/JSONObject;
    invoke-static {v0}, Lcom/sec/android/app/sns3/svc/sp/foursquare/parser/SnsFsParserFeed;->parseVenues(Lorg/json/JSONObject;)Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseFeed;

    move-result-object v1

    .line 143
    if-nez v5, :cond_0

    .line 144
    move-object v5, v1

    .line 145
    move-object v3, v5

    .line 138
    :goto_1
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 147
    :cond_0
    iput-object v1, v3, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseFeed;->mNext:Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseFeed;

    .line 148
    iget-object v3, v3, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseFeed;->mNext:Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseFeed;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 154
    .end local v0    # "checkinItem":Lorg/json/JSONObject;
    .end local v2    # "con":Lorg/json/JSONObject;
    .end local v6    # "i":I
    .end local v7    # "meta":Lorg/json/JSONObject;
    .end local v8    # "recentCheckins":Lorg/json/JSONArray;
    .end local v9    # "response":Lorg/json/JSONObject;
    :catch_0
    move-exception v4

    .line 155
    .local v4, "e1":Lorg/json/JSONException;
    invoke-virtual {v4}, Lorg/json/JSONException;->printStackTrace()V

    .line 157
    .end local v4    # "e1":Lorg/json/JSONException;
    :cond_1
    return-object v5
.end method

.method private static parseVenues(Lorg/json/JSONObject;)Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseFeed;
    .locals 13
    .param p0, "checkinItem"    # Lorg/json/JSONObject;

    .prologue
    .line 162
    new-instance v7, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseFeed;

    invoke-direct {v7}, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseFeed;-><init>()V

    .line 164
    .local v7, "user":Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseFeed;
    :try_start_0
    const-string v10, "id"

    invoke-virtual {p0, v10}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    iput-object v10, v7, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseFeed;->mCheckIn_Id:Ljava/lang/String;

    .line 165
    const-string v10, "createdAt"

    invoke-virtual {p0, v10}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    iput-object v10, v7, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseFeed;->mCreatedAt:Ljava/lang/String;

    .line 166
    const-string v10, "shout"

    invoke-virtual {p0, v10}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    iput-object v10, v7, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseFeed;->mDescription:Ljava/lang/String;

    .line 167
    const-string v10, "timeZoneOffset"

    invoke-virtual {p0, v10}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    iput-object v10, v7, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseFeed;->mTimeZoneOffSet:Ljava/lang/String;

    .line 169
    const-string v10, "user"

    invoke-virtual {p0, v10}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 170
    const-string v10, "user"

    invoke-virtual {p0, v10}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v8

    .line 171
    .local v8, "userItem":Lorg/json/JSONObject;
    iget-object v10, v7, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseFeed;->mUser:Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;

    const-string v11, "id"

    invoke-virtual {v8, v11}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    iput-object v11, v10, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mUserID:Ljava/lang/String;

    .line 172
    iget-object v10, v7, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseFeed;->mUser:Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;

    const-string v11, "firstName"

    invoke-virtual {v8, v11}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    iput-object v11, v10, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mUserFirstName:Ljava/lang/String;

    .line 173
    iget-object v10, v7, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseFeed;->mUser:Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;

    const-string v11, "lastName"

    invoke-virtual {v8, v11}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    iput-object v11, v10, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mUserLastName:Ljava/lang/String;

    .line 174
    iget-object v10, v7, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseFeed;->mUser:Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v12, v7, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseFeed;->mUser:Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;

    iget-object v12, v12, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mUserFirstName:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v12, v7, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseFeed;->mUser:Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;

    iget-object v12, v12, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mUserLastName:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    iput-object v11, v10, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mFormattedName:Ljava/lang/String;

    .line 176
    iget-object v10, v7, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseFeed;->mUser:Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;

    const-string v11, "relationship"

    invoke-virtual {v8, v11}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    iput-object v11, v10, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mRelationship:Ljava/lang/String;

    .line 178
    const-string v10, "photo"

    invoke-virtual {v8, v10}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v6

    .line 179
    .local v6, "photoItem":Lorg/json/JSONObject;
    iget-object v10, v7, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseFeed;->mUser:Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "prefix"

    invoke-virtual {v6, v12}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "100x100"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "suffix"

    invoke-virtual {v6, v12}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    iput-object v11, v10, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mPhotoUrl:Ljava/lang/String;

    .line 184
    .end local v6    # "photoItem":Lorg/json/JSONObject;
    .end local v8    # "userItem":Lorg/json/JSONObject;
    :cond_0
    const-string v10, "venue"

    invoke-virtual {p0, v10}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v9

    .line 185
    .local v9, "venue":Lorg/json/JSONObject;
    if-eqz v9, :cond_5

    .line 186
    const-string v10, "name"

    invoke-virtual {v9, v10}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    iput-object v10, v7, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseFeed;->mVenue:Ljava/lang/String;

    .line 187
    const-string v10, "location"

    invoke-virtual {v9, v10}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v5

    .line 188
    .local v5, "location":Lorg/json/JSONObject;
    if-eqz v5, :cond_4

    .line 189
    const-string v10, "lat"

    invoke-virtual {v5, v10}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    iput-object v10, v7, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseFeed;->mLocLatitude:Ljava/lang/String;

    .line 190
    const-string v10, "lng"

    invoke-virtual {v5, v10}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    iput-object v10, v7, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseFeed;->mLocLongitude:Ljava/lang/String;

    .line 191
    const-string v10, "address"

    invoke-virtual {v5, v10}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    if-lez v10, :cond_1

    .line 192
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v11, v7, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseFeed;->mVenue:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ","

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "address"

    invoke-virtual {v5, v11}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    iput-object v10, v7, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseFeed;->mVenue:Ljava/lang/String;

    .line 195
    :cond_1
    const-string v10, "city"

    invoke-virtual {v5, v10}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    if-lez v10, :cond_2

    .line 196
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v11, v7, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseFeed;->mVenue:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ","

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "city"

    invoke-virtual {v5, v11}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    iput-object v10, v7, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseFeed;->mVenue:Ljava/lang/String;

    .line 198
    :cond_2
    const-string v10, "state"

    invoke-virtual {v5, v10}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    if-lez v10, :cond_3

    .line 199
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v11, v7, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseFeed;->mVenue:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ","

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "state"

    invoke-virtual {v5, v11}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    iput-object v10, v7, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseFeed;->mVenue:Ljava/lang/String;

    .line 201
    :cond_3
    const-string v10, "country"

    invoke-virtual {v5, v10}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    if-lez v10, :cond_4

    .line 202
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v11, v7, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseFeed;->mVenue:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ","

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "country"

    invoke-virtual {v5, v11}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    iput-object v10, v7, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseFeed;->mVenue:Ljava/lang/String;

    .line 207
    :cond_4
    const-string v10, "categories"

    invoke-virtual {v9, v10}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    .line 208
    .local v0, "categories":Lorg/json/JSONArray;
    if-eqz v0, :cond_5

    .line 209
    const/4 v4, 0x0

    .local v4, "j":I
    :goto_0
    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v10

    if-ge v4, v10, :cond_5

    .line 210
    invoke-virtual {v0, v4}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v1

    .line 211
    .local v1, "categoryItem":Lorg/json/JSONObject;
    const-string v10, "primary"

    invoke-virtual {v1, v10}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 212
    const-string v10, "id"

    invoke-virtual {v1, v10}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    iput-object v10, v7, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseFeed;->mCategoryId:Ljava/lang/String;

    .line 213
    const-string v10, "name"

    invoke-virtual {v1, v10}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    iput-object v10, v7, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseFeed;->mCategoryName:Ljava/lang/String;

    .line 214
    const-string v10, "pluralName"

    invoke-virtual {v1, v10}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    iput-object v10, v7, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseFeed;->mCategoryPluralName:Ljava/lang/String;

    .line 216
    const-string v10, "icon"

    invoke-virtual {v1, v10}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v3

    .line 217
    .local v3, "icon":Lorg/json/JSONObject;
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "prefix"

    invoke-virtual {v3, v11}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "bg_88"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "suffix"

    invoke-virtual {v3, v11}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    iput-object v10, v7, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseFeed;->mCategoryIcon:Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 230
    .end local v0    # "categories":Lorg/json/JSONArray;
    .end local v1    # "categoryItem":Lorg/json/JSONObject;
    .end local v3    # "icon":Lorg/json/JSONObject;
    .end local v4    # "j":I
    .end local v5    # "location":Lorg/json/JSONObject;
    .end local v9    # "venue":Lorg/json/JSONObject;
    :cond_5
    :goto_1
    return-object v7

    .line 209
    .restart local v0    # "categories":Lorg/json/JSONArray;
    .restart local v1    # "categoryItem":Lorg/json/JSONObject;
    .restart local v4    # "j":I
    .restart local v5    # "location":Lorg/json/JSONObject;
    .restart local v9    # "venue":Lorg/json/JSONObject;
    :cond_6
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 227
    .end local v0    # "categories":Lorg/json/JSONArray;
    .end local v1    # "categoryItem":Lorg/json/JSONObject;
    .end local v4    # "j":I
    .end local v5    # "location":Lorg/json/JSONObject;
    .end local v9    # "venue":Lorg/json/JSONObject;
    :catch_0
    move-exception v2

    .line 228
    .local v2, "e1":Lorg/json/JSONException;
    invoke-virtual {v2}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_1
.end method
