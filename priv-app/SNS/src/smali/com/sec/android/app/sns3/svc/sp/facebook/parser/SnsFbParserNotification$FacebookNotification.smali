.class public interface abstract Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserNotification$FacebookNotification;
.super Ljava/lang/Object;
.source "SnsFbParserNotification.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserNotification;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "FacebookNotification"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserNotification$FacebookNotification$SnsFbUserQuery;,
        Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserNotification$FacebookNotification$SnsFbMsgQuery;,
        Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserNotification$FacebookNotification$SnsFbAppQuery;,
        Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserNotification$FacebookNotification$SnsFbThrdQuery;,
        Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserNotification$FacebookNotification$SnsFbNotiQuery;,
        Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserNotification$FacebookNotification$SnsFbFreqQuery;,
        Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserNotification$FacebookNotification$SnsFbEventQuery;
    }
.end annotation


# static fields
.field public static final APP_QUERY_NAME:Ljava/lang/String; = "app_query"

.field public static final EVENT_QUERY_NAME:Ljava/lang/String; = "event_invite_query"

.field public static final FQL_RESULT_SET:Ljava/lang/String; = "fql_result_set"

.field public static final FREQ_QUERY_NAME:Ljava/lang/String; = "freq_query"

.field public static final MSG_QUERY_NAME:Ljava/lang/String; = "msg_query"

.field public static final NOTI_QUERY_NAME:Ljava/lang/String; = "noti_query"

.field public static final QUERY_NAME:Ljava/lang/String; = "name"

.field public static final THRD_QUERY_NAME:Ljava/lang/String; = "thrd_query"

.field public static final USER_QUERY_NAME:Ljava/lang/String; = "user_query"
