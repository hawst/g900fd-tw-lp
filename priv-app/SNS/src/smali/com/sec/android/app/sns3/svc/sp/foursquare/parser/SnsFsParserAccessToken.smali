.class public Lcom/sec/android/app/sns3/svc/sp/foursquare/parser/SnsFsParserAccessToken;
.super Ljava/lang/Object;
.source "SnsFsParserAccessToken.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/foursquare/parser/SnsFsParserAccessToken$FourSquareAccessToken;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    return-void
.end method

.method public static parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseAccessToken;
    .locals 4
    .param p0, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 37
    new-instance v2, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseAccessToken;

    invoke-direct {v2}, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseAccessToken;-><init>()V

    .line 40
    .local v2, "user":Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseAccessToken;
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 41
    .local v1, "jsonObject":Lorg/json/JSONObject;
    const-string v3, "access_token"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseAccessToken;->mAccessToken:Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 48
    .end local v1    # "jsonObject":Lorg/json/JSONObject;
    :goto_0
    return-object v2

    .line 43
    :catch_0
    move-exception v0

    .line 45
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method
