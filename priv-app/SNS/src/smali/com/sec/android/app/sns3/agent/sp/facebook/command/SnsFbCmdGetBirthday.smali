.class public Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetBirthday;
.super Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;
.source "SnsFbCmdGetBirthday.java"


# direct methods
.method public constructor <init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Handler;)V
    .locals 3
    .param p1, "svcMgr"    # Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    .param p2, "cmdHandler"    # Landroid/os/Handler;

    .prologue
    .line 42
    invoke-direct {p0, p2}, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;-><init>(Landroid/os/Handler;)V

    .line 44
    new-instance v1, Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;

    invoke-direct {v1}, Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;-><init>()V

    .line 46
    .local v1, "unit":Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;
    new-instance v0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetBirthday$1;

    const-string v2, "me"

    invoke-direct {v0, p0, p1, v2}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetBirthday$1;-><init>(Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetBirthday;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;)V

    .line 89
    .local v0, "req":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    invoke-virtual {v1, v0}, Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;->addRequest(Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;)V

    .line 91
    invoke-virtual {p0, v1}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetBirthday;->addCommandUnit(Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;)V

    .line 92
    return-void
.end method


# virtual methods
.method protected respond()Z
    .locals 5

    .prologue
    .line 96
    const-string v0, "SnsAgent"

    const-string v1, "<SnsFbCmdGetBirthday> respond()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 98
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetBirthday;->getCommandCallback()Lcom/sec/android/app/sns3/agent/command/ISnsCommandCallback;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetBirthday;->getCommandID()I

    move-result v1

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetBirthday;->isSuccess()Z

    move-result v2

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetBirthday;->getUri()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetBirthday;->getResponseList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/sec/android/app/sns3/agent/command/ISnsCommandCallback;->onCmdRespond(IZLjava/lang/String;Ljava/util/List;)V

    .line 100
    const/4 v0, 0x1

    return v0
.end method
