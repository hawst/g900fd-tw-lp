.class public interface abstract Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$ConnectionsColumns;
.super Ljava/lang/Object;
.source "SnsLinkedInDB.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ConnectionsColumns"
.end annotation


# static fields
.field public static final COUNTRY_CODE:Ljava/lang/String; = "code"

.field public static final DEGREE:Ljava/lang/String; = "degree"

.field public static final FIRST_NAME:Ljava/lang/String; = "first_name"

.field public static final HEADLINE:Ljava/lang/String; = "headline"

.field public static final INDUSTRY:Ljava/lang/String; = "industry"

.field public static final LAST_NAME:Ljava/lang/String; = "last_name"

.field public static final LOCATION_NAME:Ljava/lang/String; = "name"

.field public static final PERSON_ID:Ljava/lang/String; = "id"

.field public static final PICTURE_URL:Ljava/lang/String; = "picture_url"

.field public static final PROFILE_URL:Ljava/lang/String; = "profile_url"
