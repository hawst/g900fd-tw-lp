.class public Lcom/sec/android/app/sns3/agent/sp/googleplus/db/SnsGooglePlusDB;
.super Ljava/lang/Object;
.source "SnsGooglePlusDB.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/agent/sp/googleplus/db/SnsGooglePlusDB$FriendsProfileInfoColumns;,
        Lcom/sec/android/app/sns3/agent/sp/googleplus/db/SnsGooglePlusDB$FriendsProfileInfo;,
        Lcom/sec/android/app/sns3/agent/sp/googleplus/db/SnsGooglePlusDB$StatusStreamColumns;,
        Lcom/sec/android/app/sns3/agent/sp/googleplus/db/SnsGooglePlusDB$StatusStream;,
        Lcom/sec/android/app/sns3/agent/sp/googleplus/db/SnsGooglePlusDB$UserGpFeedInfoColumns;,
        Lcom/sec/android/app/sns3/agent/sp/googleplus/db/SnsGooglePlusDB$UserGpFeedInfo;,
        Lcom/sec/android/app/sns3/agent/sp/googleplus/db/SnsGooglePlusDB$UserProfileInfoColumns;,
        Lcom/sec/android/app/sns3/agent/sp/googleplus/db/SnsGooglePlusDB$UserProfileInfo;,
        Lcom/sec/android/app/sns3/agent/sp/googleplus/db/SnsGooglePlusDB$UserGpBasicInfoColumns;,
        Lcom/sec/android/app/sns3/agent/sp/googleplus/db/SnsGooglePlusDB$UserGpBasicInfo;,
        Lcom/sec/android/app/sns3/agent/sp/googleplus/db/SnsGooglePlusDB$WIPE_GOOGLEPLUS_DATA;
    }
.end annotation


# static fields
.field public static final AUTHORITY:Ljava/lang/String; = "com.sec.android.app.sns3.sp.googleplus"

.field public static final CALL_AUTHORITY:Ljava/lang/String; = "com.sec.android.app.sns3.call"

.field public static final DATABASE_NAME:Ljava/lang/String; = "snsGooglePlusDB.db"

.field public static final DATABASE_VERSION:I = 0x2

.field public static final FRIENDS_PROFILE_TABLE_NAME:Ljava/lang/String; = "friends_profile_info"

.field public static final LIFE_AUTHORITY:Ljava/lang/String; = "com.sec.android.app.sns3.life"

.field public static final STATUS_STREAM_TABLE_NAME:Ljava/lang/String; = "status_stream"

.field public static final USER_BASIC_INFO_TABLE_NAME:Ljava/lang/String; = "user_basic_info"

.field public static final USER_FEED_TABLE_NAME:Ljava/lang/String; = "user_feed_info"

.field public static final USER_PROFILE_TABLE_NAME:Ljava/lang/String; = "user_profile_info"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 217
    return-void
.end method
