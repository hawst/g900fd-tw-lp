.class public interface abstract Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserPost$FacebookPost;
.super Ljava/lang/Object;
.source "SnsFbParserPost.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserPost;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "FacebookPost"
.end annotation


# static fields
.field public static final ACTIONS:Ljava/lang/String; = "actions"

.field public static final APPLICATION:Ljava/lang/String; = "application"

.field public static final CAPTION:Ljava/lang/String; = "caption"

.field public static final COMMENTS:Ljava/lang/String; = "comments"

.field public static final COUNT:Ljava/lang/String; = "count"

.field public static final CREATED_TIME:Ljava/lang/String; = "created_time"

.field public static final DATA:Ljava/lang/String; = "data"

.field public static final DESCRIPTION:Ljava/lang/String; = "description"

.field public static final FROM:Ljava/lang/String; = "from"

.field public static final FROM_ID:Ljava/lang/String; = "id"

.field public static final FROM_NAME:Ljava/lang/String; = "name"

.field public static final ICON:Ljava/lang/String; = "icon"

.field public static final ID:Ljava/lang/String; = "id"

.field public static final LIKES:Ljava/lang/String; = "likes"

.field public static final LINK:Ljava/lang/String; = "link"

.field public static final MESSAGE:Ljava/lang/String; = "message"

.field public static final NAME:Ljava/lang/String; = "name"

.field public static final OBJECT_ID:Ljava/lang/String; = "object_id"

.field public static final PICTURE:Ljava/lang/String; = "picture"

.field public static final PLACE:Ljava/lang/String; = "place"

.field public static final PRIVACY:Ljava/lang/String; = "privacy"

.field public static final PROPERTIES:Ljava/lang/String; = "properties"

.field public static final SHARES:Ljava/lang/String; = "shares"

.field public static final SOURCE:Ljava/lang/String; = "source"

.field public static final STORY:Ljava/lang/String; = "story"

.field public static final TAGS:Ljava/lang/String; = "tags"

.field public static final TARGETING:Ljava/lang/String; = "targeting"

.field public static final TO:Ljava/lang/String; = "to"

.field public static final TO_ID:Ljava/lang/String; = "id"

.field public static final TO_NAME:Ljava/lang/String; = "name"

.field public static final TYPE:Ljava/lang/String; = "type"

.field public static final UPDATED_TIME:Ljava/lang/String; = "updated_time"
