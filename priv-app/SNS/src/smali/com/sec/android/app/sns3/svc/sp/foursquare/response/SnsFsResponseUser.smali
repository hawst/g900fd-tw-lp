.class public final Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;
.super Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;
.source "SnsFsResponseUser.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public mAboutMe:Ljava/lang/String;

.field public mCategoryIcon:Ljava/lang/String;

.field public mCategoryId:Ljava/lang/String;

.field public mCategoryName:Ljava/lang/String;

.field public mCheckIn_Id:Ljava/lang/String;

.field public mCheckinType:Ljava/lang/String;

.field public mCommentsCount:I

.field public mContact:Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseContact;

.field public mCreatedAt:Ljava/lang/String;

.field public mFormattedName:Ljava/lang/String;

.field public mFriends:Ljava/lang/String;

.field public mGender:Ljava/lang/String;

.field public mHomeCity:Ljava/lang/String;

.field public mLikesCount:I

.field public mLocId:Ljava/lang/String;

.field public mLocLatitude:Ljava/lang/String;

.field public mLocLongitude:Ljava/lang/String;

.field public mLocName:Ljava/lang/String;

.field public mNext:Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;

.field public mPhotoUrl:Ljava/lang/String;

.field public mRelationship:Ljava/lang/String;

.field public mTimeZoneOffset:Ljava/lang/String;

.field public mType:Ljava/lang/String;

.field public mUserFirstName:Ljava/lang/String;

.field public mUserID:Ljava/lang/String;

.field public mUserLastName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 80
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser$1;

    invoke-direct {v0}, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser$1;-><init>()V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 91
    invoke-direct {p0}, Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;-><init>()V

    .line 92
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 94
    invoke-direct {p0}, Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;-><init>()V

    .line 95
    invoke-virtual {p0, p1}, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->readFromParcel(Landroid/os/Parcel;)V

    .line 96
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser$1;

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 159
    const/4 v0, 0x0

    return v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 129
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mUserID:Ljava/lang/String;

    .line 130
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mFormattedName:Ljava/lang/String;

    .line 131
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mUserFirstName:Ljava/lang/String;

    .line 132
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mUserLastName:Ljava/lang/String;

    .line 133
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mType:Ljava/lang/String;

    .line 134
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mAboutMe:Ljava/lang/String;

    .line 135
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mPhotoUrl:Ljava/lang/String;

    .line 136
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mRelationship:Ljava/lang/String;

    .line 137
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mFriends:Ljava/lang/String;

    .line 138
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mHomeCity:Ljava/lang/String;

    .line 139
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mGender:Ljava/lang/String;

    .line 140
    const-class v0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseContact;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseContact;

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mContact:Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseContact;

    .line 141
    const-class v0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mNext:Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;

    .line 142
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mCheckIn_Id:Ljava/lang/String;

    .line 143
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mCreatedAt:Ljava/lang/String;

    .line 144
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mCheckinType:Ljava/lang/String;

    .line 145
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mTimeZoneOffset:Ljava/lang/String;

    .line 146
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mLocName:Ljava/lang/String;

    .line 147
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mLocId:Ljava/lang/String;

    .line 148
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mLocLatitude:Ljava/lang/String;

    .line 149
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mLocLongitude:Ljava/lang/String;

    .line 150
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mCategoryId:Ljava/lang/String;

    .line 151
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mCategoryName:Ljava/lang/String;

    .line 152
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mCategoryIcon:Ljava/lang/String;

    .line 153
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mLikesCount:I

    .line 154
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mCommentsCount:I

    .line 155
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flag"    # I

    .prologue
    .line 100
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mUserID:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 101
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mFormattedName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 102
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mUserFirstName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 103
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mUserLastName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 104
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mType:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 105
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mAboutMe:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 106
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mPhotoUrl:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 107
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mRelationship:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 108
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mFriends:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 109
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mHomeCity:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 110
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mGender:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 111
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mContact:Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseContact;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 112
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mNext:Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 113
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mCheckIn_Id:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 114
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mCreatedAt:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 115
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mCheckinType:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 116
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mTimeZoneOffset:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 117
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mLocName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 118
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mLocId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 119
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mLocLatitude:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 120
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mLocLongitude:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 121
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mCategoryId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 122
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mCategoryName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 123
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mCategoryIcon:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 124
    iget v0, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mLikesCount:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 125
    iget v0, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mCommentsCount:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 126
    return-void
.end method
