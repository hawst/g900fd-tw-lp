.class public final enum Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;
.super Ljava/lang/Enum;
.source "SnsTwStatusesAPI.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Param"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;

.field public static final enum CONTRIBUTOR_DETAILS:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;

.field public static final enum COUNT:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;

.field public static final enum DISPLAY_COORDINATES:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;

.field public static final enum EXCLUDE_REPLIES:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;

.field public static final enum FILENAME:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;

.field public static final enum ID:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;

.field public static final enum INCLUDE_ENTITIES:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;

.field public static final enum INCLUDE_RTS:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;

.field public static final enum IN_REPLY_TO_STATUS_ID:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;

.field public static final enum LAT:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;

.field public static final enum LONG:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;

.field public static final enum MAX_ID:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;

.field public static final enum MEDIA:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;

.field public static final enum PAGE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;

.field public static final enum PLACE_ID:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;

.field public static final enum POSSIBLY_SENSITIVE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;

.field public static final enum SCREEN_NAME:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;

.field public static final enum SINCE_ID:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;

.field public static final enum STATUS:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;

.field public static final enum STRINGIFY_IDS:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;

.field public static final enum TRIM_USER:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;

.field public static final enum USER_ID:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;


# instance fields
.field private mParam:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 52
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;

    const-string v1, "SCREEN_NAME"

    const-string v2, "screen_name"

    invoke-direct {v0, v1, v4, v2}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;->SCREEN_NAME:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;

    .line 53
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;

    const-string v1, "USER_ID"

    const-string v2, "user_id"

    invoke-direct {v0, v1, v5, v2}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;->USER_ID:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;

    .line 54
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;

    const-string v1, "ID"

    const-string v2, "id"

    invoke-direct {v0, v1, v6, v2}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;->ID:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;

    .line 55
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;

    const-string v1, "COUNT"

    const-string v2, "count"

    invoke-direct {v0, v1, v7, v2}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;->COUNT:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;

    .line 56
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;

    const-string v1, "SINCE_ID"

    const-string v2, "since_id"

    invoke-direct {v0, v1, v8, v2}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;->SINCE_ID:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;

    .line 57
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;

    const-string v1, "MAX_ID"

    const/4 v2, 0x5

    const-string v3, "max_id"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;->MAX_ID:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;

    .line 58
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;

    const-string v1, "PAGE"

    const/4 v2, 0x6

    const-string v3, "page"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;->PAGE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;

    .line 59
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;

    const-string v1, "INCLUDE_ENTITIES"

    const/4 v2, 0x7

    const-string v3, "include_entities"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;->INCLUDE_ENTITIES:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;

    .line 60
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;

    const-string v1, "INCLUDE_RTS"

    const/16 v2, 0x8

    const-string v3, "include_rts"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;->INCLUDE_RTS:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;

    .line 61
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;

    const-string v1, "EXCLUDE_REPLIES"

    const/16 v2, 0x9

    const-string v3, "exclude_replies"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;->EXCLUDE_REPLIES:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;

    .line 62
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;

    const-string v1, "TRIM_USER"

    const/16 v2, 0xa

    const-string v3, "trim_user"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;->TRIM_USER:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;

    .line 63
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;

    const-string v1, "CONTRIBUTOR_DETAILS"

    const/16 v2, 0xb

    const-string v3, "contributor_details"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;->CONTRIBUTOR_DETAILS:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;

    .line 64
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;

    const-string v1, "STRINGIFY_IDS"

    const/16 v2, 0xc

    const-string v3, "stringify_ids"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;->STRINGIFY_IDS:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;

    .line 65
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;

    const-string v1, "STATUS"

    const/16 v2, 0xd

    const-string v3, "status"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;->STATUS:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;

    .line 66
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;

    const-string v1, "IN_REPLY_TO_STATUS_ID"

    const/16 v2, 0xe

    const-string v3, "in_reply_to_status_id"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;->IN_REPLY_TO_STATUS_ID:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;

    .line 67
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;

    const-string v1, "LAT"

    const/16 v2, 0xf

    const-string v3, "lat"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;->LAT:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;

    .line 68
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;

    const-string v1, "LONG"

    const/16 v2, 0x10

    const-string v3, "long"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;->LONG:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;

    .line 69
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;

    const-string v1, "PLACE_ID"

    const/16 v2, 0x11

    const-string v3, "place_id"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;->PLACE_ID:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;

    .line 70
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;

    const-string v1, "DISPLAY_COORDINATES"

    const/16 v2, 0x12

    const-string v3, "display_coordinates"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;->DISPLAY_COORDINATES:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;

    .line 71
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;

    const-string v1, "MEDIA"

    const/16 v2, 0x13

    const-string v3, "media[]"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;->MEDIA:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;

    .line 72
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;

    const-string v1, "POSSIBLY_SENSITIVE"

    const/16 v2, 0x14

    const-string v3, "possibly_sensitive"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;->POSSIBLY_SENSITIVE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;

    .line 73
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;

    const-string v1, "FILENAME"

    const/16 v2, 0x15

    const-string v3, "filename"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;->FILENAME:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;

    .line 51
    const/16 v0, 0x16

    new-array v0, v0, [Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;

    sget-object v1, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;->SCREEN_NAME:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;->USER_ID:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;->ID:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;->COUNT:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;

    aput-object v1, v0, v7

    sget-object v1, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;->SINCE_ID:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;->MAX_ID:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;->PAGE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;->INCLUDE_ENTITIES:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;->INCLUDE_RTS:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;->EXCLUDE_REPLIES:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;->TRIM_USER:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;->CONTRIBUTOR_DETAILS:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;->STRINGIFY_IDS:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;->STATUS:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;->IN_REPLY_TO_STATUS_ID:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;->LAT:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;->LONG:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;->PLACE_ID:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;->DISPLAY_COORDINATES:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;->MEDIA:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;->POSSIBLY_SENSITIVE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;->FILENAME:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;->$VALUES:[Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "param"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 77
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 78
    iput-object p3, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;->mParam:Ljava/lang/String;

    .line 79
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 51
    const-class v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;
    .locals 1

    .prologue
    .line 51
    sget-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;->$VALUES:[Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;

    invoke-virtual {v0}, [Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;

    return-object v0
.end method


# virtual methods
.method public getParam()Ljava/lang/String;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;->mParam:Ljava/lang/String;

    return-object v0
.end method
