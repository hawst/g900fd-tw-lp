.class public Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdPostDirectMessageNew;
.super Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;
.source "SnsTwCmdPostDirectMessageNew.java"


# direct methods
.method public constructor <init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Handler;Landroid/os/Bundle;)V
    .locals 3
    .param p1, "svcMgr"    # Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    .param p2, "cmdHandler"    # Landroid/os/Handler;
    .param p3, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 38
    invoke-direct {p0, p2}, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;-><init>(Landroid/os/Handler;)V

    .line 40
    new-instance v1, Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;

    invoke-direct {v1}, Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;-><init>()V

    .line 42
    .local v1, "unit":Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;
    new-instance v0, Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdPostDirectMessageNew$1;

    sget-object v2, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwDirectMessagesAPI;->DIRECT_MESSAGES_NEW:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwDirectMessagesAPI;

    invoke-direct {v0, p0, p1, v2, p3}, Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdPostDirectMessageNew$1;-><init>(Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdPostDirectMessageNew;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwDirectMessagesAPI;Landroid/os/Bundle;)V

    .line 60
    .local v0, "req":Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestDirectMessages;
    invoke-virtual {v1, v0}, Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;->addRequest(Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;)V

    .line 62
    invoke-virtual {p0, v1}, Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdPostDirectMessageNew;->addCommandUnit(Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;)V

    .line 63
    return-void
.end method


# virtual methods
.method protected respond()Z
    .locals 5

    .prologue
    .line 67
    const-string v0, "SnsAgent"

    const-string v1, "<SnsTwCmdPostDirectMessageNew> respond()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 69
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdPostDirectMessageNew;->getCommandCallback()Lcom/sec/android/app/sns3/agent/command/ISnsCommandCallback;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdPostDirectMessageNew;->getCommandID()I

    move-result v1

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdPostDirectMessageNew;->isSuccess()Z

    move-result v2

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdPostDirectMessageNew;->getUri()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdPostDirectMessageNew;->getResponseList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/sec/android/app/sns3/agent/command/ISnsCommandCallback;->onCmdRespond(IZLjava/lang/String;Ljava/util/List;)V

    .line 71
    const/4 v0, 0x1

    return v0
.end method
