.class public Lcom/sec/android/app/sns3/svc/sp/instagram/SnsInToken;
.super Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;
.source "SnsInToken.java"


# static fields
.field public static final ACCESS_TOKEN:Ljava/lang/String; = "access_token"

.field public static final PROFILE_PHOTO:Ljava/lang/String; = "profile_photo"

.field public static final USERNAME:Ljava/lang/String; = "username"

.field public static final USER_ID:Ljava/lang/String; = "user_id"


# instance fields
.field private mAccessToken:Ljava/lang/String;

.field private mProfilePhoto:Ljava/lang/String;

.field private final mSharedPref:Landroid/content/SharedPreferences;

.field private mUserID:Ljava/lang/String;

.field private mUserName:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;-><init>()V

    .line 48
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v0

    const-string v1, "Instagram_Token"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/sns3/SnsApplication;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/instagram/SnsInToken;->mSharedPref:Landroid/content/SharedPreferences;

    .line 50
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/svc/sp/instagram/SnsInToken;->readTokenInfo()V

    .line 51
    return-void
.end method


# virtual methods
.method public getAccessToken()Ljava/lang/String;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/instagram/SnsInToken;->mAccessToken:Ljava/lang/String;

    return-object v0
.end method

.method public getProfilePhoto()Ljava/lang/String;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/instagram/SnsInToken;->mProfilePhoto:Ljava/lang/String;

    return-object v0
.end method

.method public getUserID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/instagram/SnsInToken;->mUserID:Ljava/lang/String;

    return-object v0
.end method

.method public getUserName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/instagram/SnsInToken;->mUserName:Ljava/lang/String;

    return-object v0
.end method

.method public isValidAccessTokenNExpires()Z
    .locals 2

    .prologue
    .line 56
    const/4 v0, 0x0

    .line 58
    .local v0, "isValid":Z
    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/instagram/SnsInToken;->mAccessToken:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/instagram/SnsInToken;->mAccessToken:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    .line 59
    const/4 v0, 0x1

    .line 61
    :cond_0
    return v0
.end method

.method public readTokenInfo()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 124
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/instagram/SnsInToken;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v1, "access_token"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/instagram/SnsInToken;->mAccessToken:Ljava/lang/String;

    .line 125
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/instagram/SnsInToken;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v1, "username"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/instagram/SnsInToken;->mUserName:Ljava/lang/String;

    .line 126
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/instagram/SnsInToken;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v1, "user_id"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/instagram/SnsInToken;->mUserID:Ljava/lang/String;

    .line 127
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/instagram/SnsInToken;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v1, "profile_photo"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/instagram/SnsInToken;->mProfilePhoto:Ljava/lang/String;

    .line 129
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/instagram/SnsInToken;->mAccessToken:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/instagram/SnsInToken;->mAccessToken:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 130
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/sns3/svc/sp/instagram/SnsInToken;->setTokenState(I)V

    .line 133
    :goto_0
    return-void

    .line 132
    :cond_0
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/sec/android/app/sns3/svc/sp/instagram/SnsInToken;->setTokenState(I)V

    goto :goto_0
.end method

.method public removeAll()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 102
    invoke-virtual {p0, v0}, Lcom/sec/android/app/sns3/svc/sp/instagram/SnsInToken;->setAccessTokenNExpires(Ljava/lang/String;)V

    .line 104
    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/instagram/SnsInToken;->mUserName:Ljava/lang/String;

    .line 105
    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/instagram/SnsInToken;->mUserID:Ljava/lang/String;

    .line 106
    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/instagram/SnsInToken;->mProfilePhoto:Ljava/lang/String;

    .line 107
    return-void
.end method

.method public setAccessTokenNExpires(Ljava/lang/String;)V
    .locals 1
    .param p1, "accessToken"    # Ljava/lang/String;

    .prologue
    .line 65
    iput-object p1, p0, Lcom/sec/android/app/sns3/svc/sp/instagram/SnsInToken;->mAccessToken:Ljava/lang/String;

    .line 67
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/instagram/SnsInToken;->mAccessToken:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/instagram/SnsInToken;->mAccessToken:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 68
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/sns3/svc/sp/instagram/SnsInToken;->setTokenState(I)V

    .line 72
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/svc/sp/instagram/SnsInToken;->writeTokenInfo()V

    .line 73
    return-void

    .line 70
    :cond_0
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/sec/android/app/sns3/svc/sp/instagram/SnsInToken;->setTokenState(I)V

    goto :goto_0
.end method

.method public setUserInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "userName"    # Ljava/lang/String;
    .param p2, "userID"    # Ljava/lang/String;
    .param p3, "profilePhoto"    # Ljava/lang/String;

    .prologue
    .line 77
    iput-object p1, p0, Lcom/sec/android/app/sns3/svc/sp/instagram/SnsInToken;->mUserName:Ljava/lang/String;

    .line 78
    iput-object p2, p0, Lcom/sec/android/app/sns3/svc/sp/instagram/SnsInToken;->mUserID:Ljava/lang/String;

    .line 79
    iput-object p3, p0, Lcom/sec/android/app/sns3/svc/sp/instagram/SnsInToken;->mProfilePhoto:Ljava/lang/String;

    .line 81
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/svc/sp/instagram/SnsInToken;->writeTokenInfo()V

    .line 82
    return-void
.end method

.method public writeTokenInfo()V
    .locals 3

    .prologue
    .line 112
    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/instagram/SnsInToken;->mSharedPref:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 114
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "access_token"

    iget-object v2, p0, Lcom/sec/android/app/sns3/svc/sp/instagram/SnsInToken;->mAccessToken:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 115
    const-string v1, "username"

    iget-object v2, p0, Lcom/sec/android/app/sns3/svc/sp/instagram/SnsInToken;->mUserName:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 116
    const-string v1, "user_id"

    iget-object v2, p0, Lcom/sec/android/app/sns3/svc/sp/instagram/SnsInToken;->mUserID:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 117
    const-string v1, "profile_photo"

    iget-object v2, p0, Lcom/sec/android/app/sns3/svc/sp/instagram/SnsInToken;->mProfilePhoto:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 119
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 120
    return-void
.end method
