.class public interface abstract Lcom/sec/android/app/sns3/app/home/SnsHomeResource;
.super Ljava/lang/Object;
.source "SnsHomeResource.java"


# static fields
.field public static final ACTION_FACEBOOK_FEED_REQUESTED:Ljava/lang/String; = "com.sec.android.app.sns.home.ACTION_FACEBOOK_FEED_REQUESTED"

.field public static final ACTION_FACEBOOK_FEED_UPDATED:Ljava/lang/String; = "com.sec.android.app.sns.home.ACTION_FACEBOOK_FEED_UPDATED"

.field public static final ACTION_FOURSQUARE_FEED_REQUESTED:Ljava/lang/String; = "com.sec.android.app.sns.home.ACTION_FOURSQUARE_FEED_REQUESTED"

.field public static final ACTION_FOURSQUARE_FEED_UPDATED:Ljava/lang/String; = "com.sec.android.app.sns.home.ACTION_FOURSQUARE_FEED_UPDATED"

.field public static final ACTION_INSTAGRAM_FEED_REQUESTED:Ljava/lang/String; = "com.sec.android.app.sns.home.ACTION_INSTAGRAM_FEED_REQUESTED"

.field public static final ACTION_INSTAGRAM_FEED_UPDATED:Ljava/lang/String; = "com.sec.android.app.sns.home.ACTION_INSTAGRAM_FEED_UPDATED"

.field public static final ACTION_LINKEDIN_FEED_REQUESTED:Ljava/lang/String; = "com.sec.android.app.sns.home.ACTION_LINKEDIN_FEED_REQUESTED"

.field public static final ACTION_LINKEDIN_FEED_UPDATED:Ljava/lang/String; = "com.sec.android.app.sns.home.ACTION_LINKEDIN_FEED_UPDATED"

.field public static final ACTION_TWITTER_FEED_REQUESTED:Ljava/lang/String; = "com.sec.android.app.sns.home.ACTION_TWITTER_FEED_REQUESTED"

.field public static final ACTION_TWITTER_FEED_UPDATED:Ljava/lang/String; = "com.sec.android.app.sns.home.ACTION_TWITTER_FEED_UPDATED"

.field public static final CONTENT_URI:Ljava/lang/String; = "SNS_CONTENT_URI"

.field public static final RESULT:Ljava/lang/String; = "SNS_RESULT"

.field public static final SNS_BROADCAST_PERMISSION:Ljava/lang/String; = "com.sec.android.app.sns3.permission.RECEIVE_SNS_BROADCAST"

.field public static final TAG:Ljava/lang/String; = "SnsHome"
