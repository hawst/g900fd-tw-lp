.class public Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncResource;
.super Ljava/lang/Object;
.source "SnsGpSyncResource.java"


# static fields
.field public static final CONTENT_URI:Ljava/lang/String; = "SNS3_CONTENT_URI"

.field public static final CONTENT_URI_PHOTO:Ljava/lang/String; = "SNS3_CONTENT_URI_PHOTO"

.field public static final CONTENT_URI_PROFILE:Ljava/lang/String; = "SNS3_CONTENT_URI_PROFILE"

.field public static final RESULT:Ljava/lang/String; = "SNS_RESULT"

.field public static final RETRY_LOGIN:Ljava/lang/String; = "RetryLogin"

.field public static final RETRY_LOGIN_ACTION:Ljava/lang/String; = "com.sec.android.app.sns3.RETRY_LOGIN_GOOGLEPLUS"

.field public static final RETRY_SSO_ACTION:Ljava/lang/String; = "com.sec.android.app.sns3.RETRY_SSO_GOOGLEPLUS"

.field public static final SNS_BROADCAST_PERMISSION:Ljava/lang/String; = "com.sec.android.app.sns3.permission.RECEIVE_SNS_BROADCAST"

.field public static final SYNC_TYPE_ALBUM:Ljava/lang/String; = "album"

.field public static final SYNC_TYPE_EVENT:Ljava/lang/String; = "event"

.field public static final SYNC_TYPE_FEED:Ljava/lang/String; = "feed"

.field public static final SYNC_TYPE_PHOTO:Ljava/lang/String; = "photo"

.field public static final SYNC_TYPE_PROFILE:Ljava/lang/String; = "profile"

.field public static final UPDATE_PROFILES:Ljava/lang/String; = "com.sec.android.app.sns3.action.SYNC_GP_PROFILES"

.field public static final UPDATE_PROFILE_FEEDS:Ljava/lang/String; = "com.sec.android.app.sns3.action.SYNC_GP_PROFILE_FEEDS"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
