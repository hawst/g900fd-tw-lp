.class public final enum Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI;
.super Ljava/lang/Enum;
.source "SnsTwBlocksAPI.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI$Param;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI;

.field public static final enum BLOCKS_BLOCKING:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI;

.field public static final enum BLOCKS_BLOCKING_IDS:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI;

.field public static final enum BLOCKS_CREATE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI;

.field public static final enum BLOCKS_DESTROY:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI;

.field public static final enum BLOCKS_EXISTS:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI;

.field public static final enum REPORT_SPAM:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI;


# instance fields
.field private mHttpMethod:Ljava/lang/String;

.field private mUrl:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 29
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI;

    const-string v1, "BLOCKS_BLOCKING"

    const-string v2, "/blocks/blocking.json"

    const-string v3, "GET"

    invoke-direct {v0, v1, v5, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI;->BLOCKS_BLOCKING:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI;

    .line 30
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI;

    const-string v1, "BLOCKS_BLOCKING_IDS"

    const-string v2, "/blocks/blocking/ids.json"

    const-string v3, "GET"

    invoke-direct {v0, v1, v6, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI;->BLOCKS_BLOCKING_IDS:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI;

    .line 31
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI;

    const-string v1, "BLOCKS_EXISTS"

    const-string v2, "/blocks/exists.json"

    const-string v3, "GET"

    invoke-direct {v0, v1, v7, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI;->BLOCKS_EXISTS:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI;

    .line 32
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI;

    const-string v1, "BLOCKS_CREATE"

    const-string v2, "/blocks/create.json"

    const-string v3, "POST"

    invoke-direct {v0, v1, v8, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI;->BLOCKS_CREATE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI;

    .line 33
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI;

    const-string v1, "BLOCKS_DESTROY"

    const-string v2, "/blocks/destroy.json"

    const-string v3, "POST"

    invoke-direct {v0, v1, v9, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI;->BLOCKS_DESTROY:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI;

    .line 34
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI;

    const-string v1, "REPORT_SPAM"

    const/4 v2, 0x5

    const-string v3, "/report_spam.json"

    const-string v4, "POST"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI;->REPORT_SPAM:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI;

    .line 27
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI;

    sget-object v1, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI;->BLOCKS_BLOCKING:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI;->BLOCKS_BLOCKING_IDS:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI;->BLOCKS_EXISTS:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI;

    aput-object v1, v0, v7

    sget-object v1, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI;->BLOCKS_CREATE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI;

    aput-object v1, v0, v8

    sget-object v1, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI;->BLOCKS_DESTROY:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI;

    aput-object v1, v0, v9

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI;->REPORT_SPAM:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI;->$VALUES:[Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p3, "url"    # Ljava/lang/String;
    .param p4, "httpMethod"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 59
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 60
    iput-object p3, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI;->mUrl:Ljava/lang/String;

    .line 61
    iput-object p4, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI;->mHttpMethod:Ljava/lang/String;

    .line 62
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 27
    const-class v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI;
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI;->$VALUES:[Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI;

    invoke-virtual {v0}, [Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI;

    return-object v0
.end method


# virtual methods
.method public getHttpMethod()Ljava/lang/String;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI;->mHttpMethod:Ljava/lang/String;

    return-object v0
.end method

.method public getUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI;->mUrl:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwBlocksAPI;->mUrl:Ljava/lang/String;

    return-object v0
.end method
