.class Lcom/sec/android/app/sns3/svc/sp/sinaweibo/SnsSwService$1;
.super Lcom/sec/android/app/sns3/svc/sp/sinaweibo/api/ISnsSinaweibo$Stub;
.source "SnsSwService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/svc/sp/sinaweibo/SnsSwService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/sns3/svc/sp/sinaweibo/SnsSwService;


# direct methods
.method constructor <init>(Lcom/sec/android/app/sns3/svc/sp/sinaweibo/SnsSwService;)V
    .locals 0

    .prologue
    .line 90
    iput-object p1, p0, Lcom/sec/android/app/sns3/svc/sp/sinaweibo/SnsSwService$1;->this$0:Lcom/sec/android/app/sns3/svc/sp/sinaweibo/SnsSwService;

    invoke-direct {p0}, Lcom/sec/android/app/sns3/svc/sp/sinaweibo/api/ISnsSinaweibo$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public getApplicationInfo()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 94
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 95
    .local v0, "appInfo":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v1, "app_id"

    const-string v2, "852366446"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 96
    const-string v1, "redirect_uri"

    const-string v2, "http://www.samsung.com"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 97
    return-object v0
.end method

.method public getFriendTimeline(Ljava/lang/String;Lcom/sec/android/app/sns3/svc/sp/sinaweibo/api/ISnsSinaweiboCallbackStatus;)I
    .locals 6
    .param p1, "userID"    # Ljava/lang/String;
    .param p2, "cb"    # Lcom/sec/android/app/sns3/svc/sp/sinaweibo/api/ISnsSinaweiboCallbackStatus;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 102
    const-string v1, "SNS"

    const-string v2, "getFeed CALLED!!!"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 104
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/sinaweibo/SnsSwService$1$1;

    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/sinaweibo/SnsSwService$1;->this$0:Lcom/sec/android/app/sns3/svc/sp/sinaweibo/SnsSwService;

    # getter for: Lcom/sec/android/app/sns3/svc/sp/sinaweibo/SnsSwService;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    invoke-static {v1}, Lcom/sec/android/app/sns3/svc/sp/sinaweibo/SnsSwService;->access$000(Lcom/sec/android/app/sns3/svc/sp/sinaweibo/SnsSwService;)Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/sns3/svc/sp/sinaweibo/request/SnsSwStatusesAPI;->STATUSES_FRIENDS_TIMELINE:Lcom/sec/android/app/sns3/svc/sp/sinaweibo/request/SnsSwStatusesAPI;

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/sns3/svc/sp/sinaweibo/SnsSwService$1$1;-><init>(Lcom/sec/android/app/sns3/svc/sp/sinaweibo/SnsSwService$1;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Lcom/sec/android/app/sns3/svc/sp/sinaweibo/request/SnsSwStatusesAPI;Ljava/lang/String;Lcom/sec/android/app/sns3/svc/sp/sinaweibo/api/ISnsSinaweiboCallbackStatus;)V

    .line 120
    .local v0, "req":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->request()Z

    .line 122
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->getReqID()I

    move-result v1

    return v1
.end method
