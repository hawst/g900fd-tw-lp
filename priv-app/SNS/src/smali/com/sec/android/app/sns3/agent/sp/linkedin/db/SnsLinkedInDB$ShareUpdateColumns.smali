.class public interface abstract Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$ShareUpdateColumns;
.super Ljava/lang/Object;
.source "SnsLinkedInDB.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ShareUpdateColumns"
.end annotation


# static fields
.field public static final AUTHOR_PERSON_ID:Ljava/lang/String; = "author_person_id"

.field public static final COMMENT:Ljava/lang/String; = "comment"

.field public static final CURRENT_SHARE_ID:Ljava/lang/String; = "current_share_id"

.field public static final SHARE_UPDATE_KEY:Ljava/lang/String; = "share_update_key"

.field public static final SOURCE_APP:Ljava/lang/String; = "source_app"

.field public static final SOURCE_NAME:Ljava/lang/String; = "source_name"

.field public static final SUBMITTED_URL:Ljava/lang/String; = "submitted_url"

.field public static final TIME_STAMP:Ljava/lang/String; = "time_stamp"

.field public static final TITLE:Ljava/lang/String; = "title"

.field public static final VISIBILITY:Ljava/lang/String; = "visibility"
