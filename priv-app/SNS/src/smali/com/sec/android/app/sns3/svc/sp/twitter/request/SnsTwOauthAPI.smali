.class public final enum Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwOauthAPI;
.super Ljava/lang/Enum;
.source "SnsTwOauthAPI.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwOauthAPI;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwOauthAPI;

.field public static final enum OAUTH_ACCESS_TOKEN:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwOauthAPI;

.field public static final enum OAUTH_REQUEST_TOKEN:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwOauthAPI;


# instance fields
.field private mHttpMethod:Ljava/lang/String;

.field private mUrl:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 26
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwOauthAPI;

    const-string v1, "OAUTH_REQUEST_TOKEN"

    const-string v2, "oauth/request_token"

    const-string v3, "POST"

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwOauthAPI;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwOauthAPI;->OAUTH_REQUEST_TOKEN:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwOauthAPI;

    .line 27
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwOauthAPI;

    const-string v1, "OAUTH_ACCESS_TOKEN"

    const-string v2, "oauth/access_token"

    const-string v3, "POST"

    invoke-direct {v0, v1, v5, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwOauthAPI;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwOauthAPI;->OAUTH_ACCESS_TOKEN:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwOauthAPI;

    .line 24
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwOauthAPI;

    sget-object v1, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwOauthAPI;->OAUTH_REQUEST_TOKEN:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwOauthAPI;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwOauthAPI;->OAUTH_ACCESS_TOKEN:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwOauthAPI;

    aput-object v1, v0, v5

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwOauthAPI;->$VALUES:[Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwOauthAPI;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p3, "url"    # Ljava/lang/String;
    .param p4, "httpMethod"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 32
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 33
    iput-object p3, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwOauthAPI;->mUrl:Ljava/lang/String;

    .line 34
    iput-object p4, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwOauthAPI;->mHttpMethod:Ljava/lang/String;

    .line 35
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwOauthAPI;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 24
    const-class v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwOauthAPI;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwOauthAPI;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwOauthAPI;
    .locals 1

    .prologue
    .line 24
    sget-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwOauthAPI;->$VALUES:[Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwOauthAPI;

    invoke-virtual {v0}, [Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwOauthAPI;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwOauthAPI;

    return-object v0
.end method


# virtual methods
.method public getHttpMethod()Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwOauthAPI;->mHttpMethod:Ljava/lang/String;

    return-object v0
.end method

.method public getUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwOauthAPI;->mUrl:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwOauthAPI;->mUrl:Ljava/lang/String;

    return-object v0
.end method
