.class public final Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseMyAccountInfo;
.super Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;
.source "SnsFsResponseMyAccountInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseMyAccountInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public mEmail:Ljava/lang/String;

.field public mPhotoUrl:Ljava/lang/String;

.field public mUserFirstName:Ljava/lang/String;

.field public mUserID:Ljava/lang/String;

.field public mUserLastName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseMyAccountInfo$1;

    invoke-direct {v0}, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseMyAccountInfo$1;-><init>()V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseMyAccountInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;-><init>()V

    .line 50
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 52
    invoke-direct {p0}, Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;-><init>()V

    .line 53
    invoke-virtual {p0, p1}, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseMyAccountInfo;->readFromParcel(Landroid/os/Parcel;)V

    .line 54
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseMyAccountInfo$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseMyAccountInfo$1;

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseMyAccountInfo;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 75
    const/4 v0, 0x0

    return v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 66
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseMyAccountInfo;->mUserID:Ljava/lang/String;

    .line 67
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseMyAccountInfo;->mUserFirstName:Ljava/lang/String;

    .line 68
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseMyAccountInfo;->mUserLastName:Ljava/lang/String;

    .line 69
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseMyAccountInfo;->mPhotoUrl:Ljava/lang/String;

    .line 70
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseMyAccountInfo;->mEmail:Ljava/lang/String;

    .line 71
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flag"    # I

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseMyAccountInfo;->mUserID:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 59
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseMyAccountInfo;->mUserFirstName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 60
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseMyAccountInfo;->mUserLastName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 61
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseMyAccountInfo;->mPhotoUrl:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 62
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseMyAccountInfo;->mEmail:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 63
    return-void
.end method
