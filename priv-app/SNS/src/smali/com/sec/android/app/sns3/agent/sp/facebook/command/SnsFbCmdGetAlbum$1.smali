.class Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAlbum$1;
.super Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqGetConvertAlbumID;
.source "SnsFbCmdGetAlbum.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAlbum;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Handler;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAlbum;


# direct methods
.method constructor <init>(Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAlbum;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;)V
    .locals 0
    .param p2, "x0"    # Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    .param p3, "x1"    # Ljava/lang/String;

    .prologue
    .line 60
    iput-object p1, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAlbum$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAlbum;

    invoke-direct {p0, p2, p3}, Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqGetConvertAlbumID;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public onReqRespond(IZIILandroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseConvertID;)Z
    .locals 6
    .param p1, "reqID"    # I
    .param p2, "bSuccess"    # Z
    .param p3, "httpStatus"    # I
    .param p4, "errorCode"    # I
    .param p5, "reason"    # Landroid/os/Bundle;
    .param p6, "convertID"    # Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseConvertID;

    .prologue
    .line 65
    if-eqz p2, :cond_0

    .line 67
    iget-object v3, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAlbum$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAlbum;

    iget-object v4, p6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseConvertID;->mObjectID:Ljava/lang/String;

    # setter for: Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAlbum;->mAlbumID:Ljava/lang/String;
    invoke-static {v3, v4}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAlbum;->access$002(Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAlbum;Ljava/lang/String;)Ljava/lang/String;

    .line 69
    new-instance v2, Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;

    invoke-direct {v2}, Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;-><init>()V

    .line 71
    .local v2, "unitNext":Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;
    new-instance v1, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAlbum$1$1;

    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/sns3/SnsApplication;->getSvcMgr()Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAlbum$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAlbum;

    # getter for: Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAlbum;->mAlbumID:Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAlbum;->access$000(Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAlbum;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, p0, v3, v4}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAlbum$1$1;-><init>(Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAlbum$1;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;)V

    .line 108
    .local v1, "req2":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    invoke-virtual {v2, v1}, Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;->addRequest(Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;)V

    .line 110
    new-instance v0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAlbum$1$2;

    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/sns3/SnsApplication;->getSvcMgr()Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAlbum$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAlbum;

    # getter for: Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAlbum;->mAlbumID:Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAlbum;->access$000(Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAlbum;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, p0, v3, v4}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAlbum$1$2;-><init>(Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAlbum$1;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;)V

    .line 162
    .local v0, "req":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    invoke-virtual {v2, v0}, Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;->addRequest(Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;)V

    .line 164
    iget-object v3, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAlbum$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAlbum;

    # invokes: Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAlbum;->addCommandUnit(Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;)V
    invoke-static {v3, v2}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAlbum;->access$200(Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAlbum;Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;)V

    .line 172
    .end local v0    # "req":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    .end local v1    # "req2":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    .end local v2    # "unitNext":Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;
    :goto_0
    iget-object v3, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAlbum$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAlbum;

    invoke-virtual {v3, p2}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAlbum;->setSuccess(Z)V

    .line 173
    iget-object v3, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAlbum$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAlbum;

    invoke-virtual {v3, p0}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAlbum;->receive(Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;)V

    .line 175
    const/4 v3, 0x1

    return v3

    .line 167
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAlbum$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAlbum;

    new-instance v4, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;

    const-string v5, "facebook"

    invoke-direct {v4, v5, p3, p4, p5}, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;-><init>(Ljava/lang/String;IILandroid/os/Bundle;)V

    invoke-virtual {v3, v4}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAlbum;->setResponseList(Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;)Z

    .line 169
    iget-object v3, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAlbum$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAlbum;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAlbum;->setUri(Ljava/lang/String;)V

    goto :goto_0
.end method
