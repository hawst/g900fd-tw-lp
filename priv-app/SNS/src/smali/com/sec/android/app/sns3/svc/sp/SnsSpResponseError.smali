.class public Lcom/sec/android/app/sns3/svc/sp/SnsSpResponseError;
.super Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;
.source "SnsSpResponseError.java"


# instance fields
.field private mErrorBundle:Landroid/os/Bundle;

.field private mErrorCode:I

.field private mErrorMessage:Ljava/lang/String;


# direct methods
.method public constructor <init>(ILjava/lang/String;Landroid/os/Bundle;)V
    .locals 0
    .param p1, "errorCode"    # I
    .param p2, "errorMessage"    # Ljava/lang/String;
    .param p3, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;-><init>()V

    .line 30
    iput p1, p0, Lcom/sec/android/app/sns3/svc/sp/SnsSpResponseError;->mErrorCode:I

    .line 31
    iput-object p2, p0, Lcom/sec/android/app/sns3/svc/sp/SnsSpResponseError;->mErrorMessage:Ljava/lang/String;

    .line 32
    iput-object p3, p0, Lcom/sec/android/app/sns3/svc/sp/SnsSpResponseError;->mErrorBundle:Landroid/os/Bundle;

    .line 33
    return-void
.end method


# virtual methods
.method public getErrorBundle()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/SnsSpResponseError;->mErrorBundle:Landroid/os/Bundle;

    return-object v0
.end method

.method public getErrorCode()I
    .locals 1

    .prologue
    .line 40
    iget v0, p0, Lcom/sec/android/app/sns3/svc/sp/SnsSpResponseError;->mErrorCode:I

    return v0
.end method

.method public getErrorMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/SnsSpResponseError;->mErrorMessage:Ljava/lang/String;

    return-object v0
.end method
