.class public Lcom/sec/android/app/sns3/svc/sp/sinaweibo/parser/SnsSwParserStatus;
.super Ljava/lang/Object;
.source "SnsSwParserStatus.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/sinaweibo/parser/SnsSwParserStatus$SinaweiboFeed;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    return-void
.end method

.method public static parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/sinaweibo/response/SnsSwResponseStatus;
    .locals 10
    .param p0, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 67
    const/4 v2, 0x0

    .line 68
    .local v2, "feed":Lcom/sec/android/app/sns3/svc/sp/sinaweibo/response/SnsSwResponseStatus;
    const/4 v0, 0x0

    .line 70
    .local v0, "curFeed":Lcom/sec/android/app/sns3/svc/sp/sinaweibo/response/SnsSwResponseStatus;
    const/4 v6, 0x0

    .line 73
    .local v6, "jsonObject":Lorg/json/JSONObject;
    :try_start_0
    new-instance v7, Lorg/json/JSONObject;

    invoke-direct {v7, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 75
    .end local v6    # "jsonObject":Lorg/json/JSONObject;
    .local v7, "jsonObject":Lorg/json/JSONObject;
    :try_start_1
    new-instance v5, Lorg/json/JSONArray;

    const-string v9, "statuses"

    invoke-virtual {v7, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v5, v9}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 77
    .local v5, "ja":Lorg/json/JSONArray;
    if-eqz v5, :cond_2

    .line 78
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    invoke-virtual {v5}, Lorg/json/JSONArray;->length()I

    move-result v9

    if-ge v4, v9, :cond_2

    .line 79
    new-instance v8, Lcom/sec/android/app/sns3/svc/sp/sinaweibo/response/SnsSwResponseStatus;

    invoke-direct {v8}, Lcom/sec/android/app/sns3/svc/sp/sinaweibo/response/SnsSwResponseStatus;-><init>()V

    .line 81
    .local v8, "newFeed":Lcom/sec/android/app/sns3/svc/sp/sinaweibo/response/SnsSwResponseStatus;
    invoke-virtual {v5, v4}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v3

    .line 83
    .local v3, "feedJsonObj":Lorg/json/JSONObject;
    const-string v9, "user"

    invoke-virtual {v3, v9}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 84
    const-string v9, "user"

    invoke-virtual {v3, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/sec/android/app/sns3/svc/sp/sinaweibo/parser/SnsSwParserFrom;->parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/sinaweibo/response/SnsSwResponseFrom;

    move-result-object v9

    iput-object v9, v8, Lcom/sec/android/app/sns3/svc/sp/sinaweibo/response/SnsSwResponseStatus;->mFrom:Lcom/sec/android/app/sns3/svc/sp/sinaweibo/response/SnsSwResponseFrom;

    .line 86
    iget-object v9, v8, Lcom/sec/android/app/sns3/svc/sp/sinaweibo/response/SnsSwResponseStatus;->mFrom:Lcom/sec/android/app/sns3/svc/sp/sinaweibo/response/SnsSwResponseFrom;

    if-eqz v9, :cond_0

    .line 87
    iget-object v9, v8, Lcom/sec/android/app/sns3/svc/sp/sinaweibo/response/SnsSwResponseStatus;->mFrom:Lcom/sec/android/app/sns3/svc/sp/sinaweibo/response/SnsSwResponseFrom;

    iget-object v9, v9, Lcom/sec/android/app/sns3/svc/sp/sinaweibo/response/SnsSwResponseFrom;->mFromName:Ljava/lang/String;

    iput-object v9, v8, Lcom/sec/android/app/sns3/svc/sp/sinaweibo/response/SnsSwResponseStatus;->mName:Ljava/lang/String;

    .line 91
    :cond_0
    const-string v9, "id"

    invoke-virtual {v3, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v8, Lcom/sec/android/app/sns3/svc/sp/sinaweibo/response/SnsSwResponseStatus;->mFeedID:Ljava/lang/String;

    .line 92
    const-string v9, "text"

    invoke-virtual {v3, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v8, Lcom/sec/android/app/sns3/svc/sp/sinaweibo/response/SnsSwResponseStatus;->mMessage:Ljava/lang/String;

    .line 94
    const-string v9, "bmiddle_pic"

    invoke-virtual {v3, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v8, Lcom/sec/android/app/sns3/svc/sp/sinaweibo/response/SnsSwResponseStatus;->mPicture:Ljava/lang/String;

    .line 103
    const-string v9, "created_at"

    invoke-virtual {v3, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v8, Lcom/sec/android/app/sns3/svc/sp/sinaweibo/response/SnsSwResponseStatus;->mCreatedTime:Ljava/lang/String;

    .line 104
    if-nez v2, :cond_1

    .line 105
    move-object v2, v8

    .line 106
    move-object v0, v2

    .line 78
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 108
    :cond_1
    iput-object v8, v0, Lcom/sec/android/app/sns3/svc/sp/sinaweibo/response/SnsSwResponseStatus;->mNext:Lcom/sec/android/app/sns3/svc/sp/sinaweibo/response/SnsSwResponseStatus;

    .line 109
    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/sinaweibo/response/SnsSwResponseStatus;->mNext:Lcom/sec/android/app/sns3/svc/sp/sinaweibo/response/SnsSwResponseStatus;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .end local v3    # "feedJsonObj":Lorg/json/JSONObject;
    .end local v4    # "i":I
    .end local v8    # "newFeed":Lcom/sec/android/app/sns3/svc/sp/sinaweibo/response/SnsSwResponseStatus;
    :cond_2
    move-object v6, v7

    .line 119
    .end local v5    # "ja":Lorg/json/JSONArray;
    .end local v7    # "jsonObject":Lorg/json/JSONObject;
    .restart local v6    # "jsonObject":Lorg/json/JSONObject;
    :goto_2
    return-object v2

    .line 114
    :catch_0
    move-exception v1

    .line 116
    .local v1, "e":Lorg/json/JSONException;
    :goto_3
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_2

    .line 114
    .end local v1    # "e":Lorg/json/JSONException;
    .end local v6    # "jsonObject":Lorg/json/JSONObject;
    .restart local v7    # "jsonObject":Lorg/json/JSONObject;
    :catch_1
    move-exception v1

    move-object v6, v7

    .end local v7    # "jsonObject":Lorg/json/JSONObject;
    .restart local v6    # "jsonObject":Lorg/json/JSONObject;
    goto :goto_3
.end method
