.class public interface abstract Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserStatus$FacebookStatus;
.super Ljava/lang/Object;
.source "SnsFbParserStatus.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserStatus;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "FacebookStatus"
.end annotation


# static fields
.field public static final DATA:Ljava/lang/String; = "data"

.field public static final FROM:Ljava/lang/String; = "from"

.field public static final FROM_ID:Ljava/lang/String; = "id"

.field public static final FROM_NAME:Ljava/lang/String; = "name"

.field public static final ID:Ljava/lang/String; = "id"

.field public static final MESSAGE:Ljava/lang/String; = "message"

.field public static final TYPE:Ljava/lang/String; = "type"

.field public static final UPDATED_TIME:Ljava/lang/String; = "updated_time"
