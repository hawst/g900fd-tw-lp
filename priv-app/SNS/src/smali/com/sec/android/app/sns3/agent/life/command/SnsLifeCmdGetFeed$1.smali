.class Lcom/sec/android/app/sns3/agent/life/command/SnsLifeCmdGetFeed$1;
.super Landroid/os/Handler;
.source "SnsLifeCmdGetFeed.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/agent/life/command/SnsLifeCmdGetFeed;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/sns3/agent/life/command/SnsLifeCmdGetFeed;


# direct methods
.method constructor <init>(Lcom/sec/android/app/sns3/agent/life/command/SnsLifeCmdGetFeed;)V
    .locals 0

    .prologue
    .line 77
    iput-object p1, p0, Lcom/sec/android/app/sns3/agent/life/command/SnsLifeCmdGetFeed$1;->this$0:Lcom/sec/android/app/sns3/agent/life/command/SnsLifeCmdGetFeed;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 80
    sget-object v0, Lcom/sec/android/app/sns3/app/life/SnsLifeResource;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handleMessage() : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 81
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 89
    sget-object v0, Lcom/sec/android/app/sns3/app/life/SnsLifeResource;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handleMessage() : unknown message - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 91
    :goto_0
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 92
    return-void

    .line 84
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/sns3/agent/life/command/SnsLifeCmdGetFeed$1;->this$0:Lcom/sec/android/app/sns3/agent/life/command/SnsLifeCmdGetFeed;

    # operator++ for: Lcom/sec/android/app/sns3/agent/life/command/SnsLifeCmdGetFeed;->mRespondCount:I
    invoke-static {v0}, Lcom/sec/android/app/sns3/agent/life/command/SnsLifeCmdGetFeed;->access$008(Lcom/sec/android/app/sns3/agent/life/command/SnsLifeCmdGetFeed;)I

    .line 85
    iget-object v0, p0, Lcom/sec/android/app/sns3/agent/life/command/SnsLifeCmdGetFeed$1;->this$0:Lcom/sec/android/app/sns3/agent/life/command/SnsLifeCmdGetFeed;

    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v1

    # invokes: Lcom/sec/android/app/sns3/agent/life/command/SnsLifeCmdGetFeed;->checkRespondCount(Landroid/content/Context;)V
    invoke-static {v0, v1}, Lcom/sec/android/app/sns3/agent/life/command/SnsLifeCmdGetFeed;->access$100(Lcom/sec/android/app/sns3/agent/life/command/SnsLifeCmdGetFeed;Landroid/content/Context;)V

    goto :goto_0

    .line 81
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
