.class Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity$2;
.super Ljava/lang/Object;
.source "SnsAccountFsAuthActivity.java"

# interfaces
.implements Landroid/accounts/AccountManagerCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/accounts/AccountManagerCallback",
        "<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;)V
    .locals 0

    .prologue
    .line 139
    iput-object p1, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity$2;->this$0:Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run(Landroid/accounts/AccountManagerFuture;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/AccountManagerFuture",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 142
    .local p1, "future":Landroid/accounts/AccountManagerFuture;, "Landroid/accounts/AccountManagerFuture<Ljava/lang/Boolean;>;"
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity$2;->this$0:Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;

    iget-object v2, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity$2;->this$0:Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;

    # getter for: Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;->mAccessToken:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;->access$200(Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "0"

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;->setAuthTokenNExpires(Ljava/lang/String;Ljava/lang/String;)Z

    .line 144
    new-instance v0, Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdAuthLogin;

    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/sns3/SnsApplication;->getSvcMgr()Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v1

    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/sns3/SnsApplication;->getAgentMgr()Lcom/sec/android/app/sns3/agent/SnsAgentMgr;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/sns3/agent/SnsAgentMgr;->getCommandMgr()Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;->getCommandMgrHandle()Landroid/os/Handler;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdAuthLogin;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Handler;Landroid/os/Bundle;)V

    .line 147
    .local v0, "cmd":Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;
    new-instance v1, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity$2$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity$2$1;-><init>(Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity$2;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->setCommandCallback(Lcom/sec/android/app/sns3/agent/command/ISnsCommandCallback;)Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;

    .line 175
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->send()I

    .line 176
    return-void
.end method
