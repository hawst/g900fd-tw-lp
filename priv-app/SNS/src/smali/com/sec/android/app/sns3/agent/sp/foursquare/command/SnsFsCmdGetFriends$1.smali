.class Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdGetFriends$1;
.super Lcom/sec/android/app/sns3/svc/sp/foursquare/request/SnsFsReqGetFriends;
.source "SnsFsCmdGetFriends.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdGetFriends;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Handler;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdGetFriends;


# direct methods
.method constructor <init>(Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdGetFriends;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 0
    .param p2, "x0"    # Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    .param p3, "x1"    # Ljava/lang/String;
    .param p4, "x2"    # Landroid/os/Bundle;

    .prologue
    .line 33
    iput-object p1, p0, Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdGetFriends$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdGetFriends;

    invoke-direct {p0, p2, p3, p4}, Lcom/sec/android/app/sns3/svc/sp/foursquare/request/SnsFsReqGetFriends;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;Landroid/os/Bundle;)V

    return-void
.end method


# virtual methods
.method public onReqRespond(IZIILandroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseFriends;)Z
    .locals 8
    .param p1, "reqID"    # I
    .param p2, "bSuccess"    # Z
    .param p3, "httpStatus"    # I
    .param p4, "errorCode"    # I
    .param p5, "reason"    # Landroid/os/Bundle;
    .param p6, "friends"    # Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseFriends;

    .prologue
    const/4 v7, 0x0

    .line 39
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/sns3/SnsApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 40
    .local v0, "cr":Landroid/content/ContentResolver;
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 41
    .local v3, "valuesList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentValues;>;"
    if-eqz p2, :cond_1

    .line 42
    iget-object v1, p6, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseFriends;->mUsers:Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;

    .line 44
    .local v1, "users":Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;
    :goto_0
    if-eqz v1, :cond_0

    .line 46
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 47
    .local v2, "values":Landroid/content/ContentValues;
    sget-object v4, Lcom/sec/android/app/sns3/agent/sp/foursquare/db/SnsFourSquareDB$FriendsProfileInfo;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v4, v7, v7}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 49
    invoke-virtual {v2}, Landroid/content/ContentValues;->clear()V

    .line 51
    const-string v4, "user_id"

    iget-object v5, v1, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mUserID:Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 52
    const-string v4, "formatted_name"

    iget-object v5, v1, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mFormattedName:Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 53
    const-string v4, "first_name"

    iget-object v5, v1, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mUserFirstName:Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 54
    const-string v4, "last_name"

    iget-object v5, v1, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mUserLastName:Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    const-string v4, "type"

    iget-object v5, v1, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mType:Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    const-string v4, "about_me"

    iget-object v5, v1, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mAboutMe:Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    const-string v4, "image_url"

    iget-object v5, v1, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mPhotoUrl:Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    const-string v4, "relationship"

    iget-object v5, v1, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mRelationship:Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    const-string v4, "friends"

    iget-object v5, v1, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mFriends:Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    const-string v4, "homeCity"

    iget-object v5, v1, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mHomeCity:Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 61
    const-string v4, "gender"

    iget-object v5, v1, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mGender:Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    const-string v4, "phone"

    iget-object v5, v1, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mContact:Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseContact;

    iget-object v5, v5, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseContact;->mPhone:Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    const-string v4, "email"

    iget-object v5, v1, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mContact:Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseContact;

    iget-object v5, v5, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseContact;->mEmail:Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    const-string v4, "twitter"

    iget-object v5, v1, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mContact:Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseContact;

    iget-object v5, v5, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseContact;->mTwitter:Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    const-string v4, "facebook"

    iget-object v5, v1, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mContact:Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseContact;

    iget-object v5, v5, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseContact;->mFacebook:Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 69
    iget-object v1, v1, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mNext:Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;

    .line 71
    goto/16 :goto_0

    .line 72
    .end local v2    # "values":Landroid/content/ContentValues;
    :cond_0
    sget-object v5, Lcom/sec/android/app/sns3/agent/sp/foursquare/db/SnsFourSquareDB$FriendsProfileInfo;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    new-array v4, v4, [Landroid/content/ContentValues;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Landroid/content/ContentValues;

    invoke-virtual {v0, v5, v4}, Landroid/content/ContentResolver;->bulkInsert(Landroid/net/Uri;[Landroid/content/ContentValues;)I

    .line 74
    iget-object v4, p0, Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdGetFriends$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdGetFriends;

    sget-object v5, Lcom/sec/android/app/sns3/agent/sp/foursquare/db/SnsFourSquareDB$FriendsProfileInfo;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v5}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdGetFriends;->setUri(Ljava/lang/String;)V

    .line 80
    .end local v1    # "users":Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;
    :goto_1
    iget-object v4, p0, Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdGetFriends$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdGetFriends;

    invoke-virtual {v4, p2}, Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdGetFriends;->setSuccess(Z)V

    .line 81
    iget-object v4, p0, Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdGetFriends$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdGetFriends;

    invoke-virtual {v4, p0}, Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdGetFriends;->receive(Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;)V

    .line 83
    const/4 v4, 0x1

    return v4

    .line 76
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdGetFriends$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdGetFriends;

    new-instance v5, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;

    const-string v6, "foursquare"

    invoke-direct {v5, v6, p3, p4, p5}, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;-><init>(Ljava/lang/String;IILandroid/os/Bundle;)V

    invoke-virtual {v4, v5}, Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdGetFriends;->setResponseList(Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;)Z

    .line 78
    iget-object v4, p0, Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdGetFriends$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdGetFriends;

    invoke-virtual {v4, v7}, Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdGetFriends;->setUri(Ljava/lang/String;)V

    goto :goto_1
.end method
