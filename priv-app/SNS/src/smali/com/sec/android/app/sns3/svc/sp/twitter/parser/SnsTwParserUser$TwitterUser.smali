.class public interface abstract Lcom/sec/android/app/sns3/svc/sp/twitter/parser/SnsTwParserUser$TwitterUser;
.super Ljava/lang/Object;
.source "SnsTwParserUser.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/svc/sp/twitter/parser/SnsTwParserUser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "TwitterUser"
.end annotation


# static fields
.field public static final CONTRIBUTORS_ENABLED:Ljava/lang/String; = "contributors_enabled"

.field public static final CREATED_AT:Ljava/lang/String; = "created_at"

.field public static final DEFAULT_PROFILE:Ljava/lang/String; = "default_profile"

.field public static final DEFAULT_PROFILE_IMAGE:Ljava/lang/String; = "default_profile_image"

.field public static final DESCRIPTION:Ljava/lang/String; = "description"

.field public static final FAVOURITES_COUNT:Ljava/lang/String; = "favourites_count"

.field public static final FOLLOWERS_COUNT:Ljava/lang/String; = "followers_count"

.field public static final FOLLOWING:Ljava/lang/String; = "following"

.field public static final FOLLOW_REQUEST_SENT:Ljava/lang/String; = "follow_request_sent"

.field public static final FRIENDS_COUNT:Ljava/lang/String; = "friends_count"

.field public static final GEO_ENABLED:Ljava/lang/String; = "geo_enabled"

.field public static final ID:Ljava/lang/String; = "id"

.field public static final IS_TRANSLATOR:Ljava/lang/String; = "is_translator"

.field public static final LANG:Ljava/lang/String; = "lang"

.field public static final LISTED_COUNT:Ljava/lang/String; = "listed_count"

.field public static final LOCATION:Ljava/lang/String; = "location"

.field public static final NAME:Ljava/lang/String; = "name"

.field public static final NOTIFICATIONS:Ljava/lang/String; = "notifications"

.field public static final PROFILE_BACKGROUND_COLOR:Ljava/lang/String; = "profile_background_color"

.field public static final PROFILE_BACKGROUND_IMAGE_URL:Ljava/lang/String; = "profile_background_image_url"

.field public static final PROFILE_BACKGROUND_IMAGE_URL_HTTPS:Ljava/lang/String; = "profile_background_image_url_https"

.field public static final PROFILE_BACKGROUND_TILE:Ljava/lang/String; = "profile_background_tile"

.field public static final PROFILE_IMAGE_URL:Ljava/lang/String; = "profile_image_url"

.field public static final PROFILE_IMAGE_URL_HTTPS:Ljava/lang/String; = "profile_image_url_https"

.field public static final PROFILE_LINK_COLOR:Ljava/lang/String; = "profile_link_color"

.field public static final PROFILE_SIDEBAR_BORDER_COLOR:Ljava/lang/String; = "profile_sidebar_border_color"

.field public static final PROFILE_SIDEBAR_FILL_COLOR:Ljava/lang/String; = "profile_sidebar_fill_color"

.field public static final PROFILE_TEXT_COLOR:Ljava/lang/String; = "profile_text_color"

.field public static final PROTECTED:Ljava/lang/String; = "protected"

.field public static final SCREEN_NAME:Ljava/lang/String; = "screen_name"

.field public static final SHOW_ALL_INLINE_MEDIA:Ljava/lang/String; = "show_all_inline_media"

.field public static final SIZE:Ljava/lang/String; = "size"

.field public static final SLUG:Ljava/lang/String; = "slug"

.field public static final STATUSES_COUNT:Ljava/lang/String; = "statuses_count"

.field public static final TIME_ZONE:Ljava/lang/String; = "time_zone"

.field public static final URL:Ljava/lang/String; = "url"

.field public static final UTC_OFFSET:Ljava/lang/String; = "utc_offset"

.field public static final VERIFIED:Ljava/lang/String; = "verified"
