.class public Lcom/sec/android/app/sns3/svc/sp/twitter/parser/SnsTwParserAccountTotals;
.super Ljava/lang/Object;
.source "SnsTwParserAccountTotals.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseAccountTotals;
    .locals 4
    .param p0, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 30
    const/4 v2, 0x0

    .line 32
    .local v2, "response":Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseAccountTotals;
    invoke-static {p0}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->isJsonEmpty(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 33
    const/4 v3, 0x0

    .line 49
    :goto_0
    return-object v3

    .line 36
    :cond_0
    new-instance v2, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseAccountTotals;

    .end local v2    # "response":Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseAccountTotals;
    invoke-direct {v2}, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseAccountTotals;-><init>()V

    .line 38
    .restart local v2    # "response":Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseAccountTotals;
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 40
    .local v1, "jsonObj":Lorg/json/JSONObject;
    const-string v3, "friends"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v3

    iput v3, v2, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseAccountTotals;->mFriends:I

    .line 41
    const-string v3, "updates"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v3

    iput v3, v2, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseAccountTotals;->mUpdates:I

    .line 42
    const-string v3, "followers"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v3

    iput v3, v2, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseAccountTotals;->mFollowers:I

    .line 43
    const-string v3, "following"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v3

    iput v3, v2, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseAccountTotals;->mFollowing:I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .end local v1    # "jsonObj":Lorg/json/JSONObject;
    :goto_1
    move-object v3, v2

    .line 49
    goto :goto_0

    .line 45
    :catch_0
    move-exception v0

    .line 46
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_1
.end method
