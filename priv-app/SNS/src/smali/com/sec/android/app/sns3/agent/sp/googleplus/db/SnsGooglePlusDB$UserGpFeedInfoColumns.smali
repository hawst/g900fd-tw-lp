.class public interface abstract Lcom/sec/android/app/sns3/agent/sp/googleplus/db/SnsGooglePlusDB$UserGpFeedInfoColumns;
.super Ljava/lang/Object;
.source "SnsGooglePlusDB.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/agent/sp/googleplus/db/SnsGooglePlusDB;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "UserGpFeedInfoColumns"
.end annotation


# static fields
.field public static final HEIGHT:Ljava/lang/String; = "height"

.field public static final LATITUDE:Ljava/lang/String; = "latitude"

.field public static final LINK:Ljava/lang/String; = "link"

.field public static final LOCATION_NAME:Ljava/lang/String; = "location_name"

.field public static final LONGITUDE:Ljava/lang/String; = "longitude"

.field public static final MEDIA_URL:Ljava/lang/String; = "media_url"

.field public static final MESSAGE:Ljava/lang/String; = "message"

.field public static final OBJECT_TYPE:Ljava/lang/String; = "object_type"

.field public static final POST_ID:Ljava/lang/String; = "post_id"

.field public static final TIMESTAMP_UTC:Ljava/lang/String; = "timestamp_utc"

.field public static final TIME_ZONE:Ljava/lang/String; = "time_zone"

.field public static final WIDTH:Ljava/lang/String; = "width"
