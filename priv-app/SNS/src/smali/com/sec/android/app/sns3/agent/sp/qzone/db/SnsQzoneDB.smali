.class public Lcom/sec/android/app/sns3/agent/sp/qzone/db/SnsQzoneDB;
.super Ljava/lang/Object;
.source "SnsQzoneDB.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/agent/sp/qzone/db/SnsQzoneDB$UserTimelineColumns;,
        Lcom/sec/android/app/sns3/agent/sp/qzone/db/SnsQzoneDB$UserTimeLine;,
        Lcom/sec/android/app/sns3/agent/sp/qzone/db/SnsQzoneDB$BirthdayColumns;,
        Lcom/sec/android/app/sns3/agent/sp/qzone/db/SnsQzoneDB$Birthday;,
        Lcom/sec/android/app/sns3/agent/sp/qzone/db/SnsQzoneDB$FeedListColumns;,
        Lcom/sec/android/app/sns3/agent/sp/qzone/db/SnsQzoneDB$FeedList;,
        Lcom/sec/android/app/sns3/agent/sp/qzone/db/SnsQzoneDB$UserColumns;,
        Lcom/sec/android/app/sns3/agent/sp/qzone/db/SnsQzoneDB$User;,
        Lcom/sec/android/app/sns3/agent/sp/qzone/db/SnsQzoneDB$WIPE_QZONE_DATA;
    }
.end annotation


# static fields
.field public static final AUTHORITY:Ljava/lang/String; = "com.sec.android.app.sns3.sp.qzone"

.field public static final BIRTHDAY_TABLE_NAME:Ljava/lang/String; = "birthday"

.field public static final CONTENT_ITEM_TYPE_BASE:Ljava/lang/String; = "vnd.android.cursor.item/vnd.com.sec.android.app.sns3.sp.qzone"

.field public static final CONTENT_TYPE_BASE:Ljava/lang/String; = "vnd.android.cursor.dir/vnd.com.sec.android.app.sns3.sp.qzone"

.field public static final DATABASE_NAME:Ljava/lang/String; = "SnsQzoneDB.db"

.field public static final DATABASE_VERSION:I = 0x14

.field public static final FEED_LIST_TABLE_NAME:Ljava/lang/String; = "feed_list"

.field public static final LIFE_AUTHORITY:Ljava/lang/String; = "com.sec.android.app.sns3.life"

.field public static final USER_TABLE_NAME:Ljava/lang/String; = "user"

.field public static final USER_TIMELINE_TABLE_NAME:Ljava/lang/String; = "usertimeline"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 165
    return-void
.end method
