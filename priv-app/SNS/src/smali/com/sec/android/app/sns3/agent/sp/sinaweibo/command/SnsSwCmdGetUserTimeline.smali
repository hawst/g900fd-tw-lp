.class public Lcom/sec/android/app/sns3/agent/sp/sinaweibo/command/SnsSwCmdGetUserTimeline;
.super Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;
.source "SnsSwCmdGetUserTimeline.java"


# instance fields
.field private mUserID:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Handler;Ljava/lang/String;)V
    .locals 4
    .param p1, "svcMgr"    # Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    .param p2, "cmdHandler"    # Landroid/os/Handler;
    .param p3, "userID"    # Ljava/lang/String;

    .prologue
    .line 45
    invoke-direct {p0, p2}, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;-><init>(Landroid/os/Handler;)V

    .line 47
    new-instance v1, Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;

    invoke-direct {v1}, Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;-><init>()V

    .line 49
    .local v1, "unit":Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;
    iput-object p3, p0, Lcom/sec/android/app/sns3/agent/sp/sinaweibo/command/SnsSwCmdGetUserTimeline;->mUserID:Ljava/lang/String;

    .line 51
    new-instance v0, Lcom/sec/android/app/sns3/agent/sp/sinaweibo/command/SnsSwCmdGetUserTimeline$1;

    sget-object v2, Lcom/sec/android/app/sns3/svc/sp/sinaweibo/request/SnsSwStatusesAPI;->STATUSES_USER_TIMELINE:Lcom/sec/android/app/sns3/svc/sp/sinaweibo/request/SnsSwStatusesAPI;

    iget-object v3, p0, Lcom/sec/android/app/sns3/agent/sp/sinaweibo/command/SnsSwCmdGetUserTimeline;->mUserID:Ljava/lang/String;

    invoke-direct {v0, p0, p1, v2, v3}, Lcom/sec/android/app/sns3/agent/sp/sinaweibo/command/SnsSwCmdGetUserTimeline$1;-><init>(Lcom/sec/android/app/sns3/agent/sp/sinaweibo/command/SnsSwCmdGetUserTimeline;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Lcom/sec/android/app/sns3/svc/sp/sinaweibo/request/SnsSwStatusesAPI;Ljava/lang/String;)V

    .line 100
    .local v0, "req":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    invoke-virtual {v1, v0}, Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;->addRequest(Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;)V

    .line 102
    invoke-virtual {p0, v1}, Lcom/sec/android/app/sns3/agent/sp/sinaweibo/command/SnsSwCmdGetUserTimeline;->addCommandUnit(Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;)V

    .line 103
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/sns3/agent/sp/sinaweibo/command/SnsSwCmdGetUserTimeline;)Ljava/lang/Long;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sns3/agent/sp/sinaweibo/command/SnsSwCmdGetUserTimeline;

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/sec/android/app/sns3/agent/sp/sinaweibo/command/SnsSwCmdGetUserTimeline;->getCurrentTime()Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method private getCurrentTime()Ljava/lang/Long;
    .locals 4

    .prologue
    .line 111
    new-instance v0, Ljava/util/GregorianCalendar;

    invoke-direct {v0}, Ljava/util/GregorianCalendar;-><init>()V

    .line 113
    .local v0, "calendar":Ljava/util/GregorianCalendar;
    invoke-virtual {v0}, Ljava/util/GregorianCalendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    return-object v1
.end method


# virtual methods
.method public getUserID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/sec/android/app/sns3/agent/sp/sinaweibo/command/SnsSwCmdGetUserTimeline;->mUserID:Ljava/lang/String;

    return-object v0
.end method

.method protected respond()Z
    .locals 5

    .prologue
    .line 118
    const-string v0, "SnsAgent"

    const-string v1, "<SnsSwCmdGetUserTimeline> respond()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 120
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/sinaweibo/command/SnsSwCmdGetUserTimeline;->getCommandCallback()Lcom/sec/android/app/sns3/agent/command/ISnsCommandCallback;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/sinaweibo/command/SnsSwCmdGetUserTimeline;->getCommandID()I

    move-result v1

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/sinaweibo/command/SnsSwCmdGetUserTimeline;->isSuccess()Z

    move-result v2

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/sinaweibo/command/SnsSwCmdGetUserTimeline;->getUri()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/sinaweibo/command/SnsSwCmdGetUserTimeline;->getResponseList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/sec/android/app/sns3/agent/command/ISnsCommandCallback;->onCmdRespond(IZLjava/lang/String;Ljava/util/List;)V

    .line 122
    const/4 v0, 0x1

    return v0
.end method
