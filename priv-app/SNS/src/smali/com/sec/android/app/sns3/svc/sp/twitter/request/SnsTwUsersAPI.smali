.class public final enum Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwUsersAPI;
.super Ljava/lang/Enum;
.source "SnsTwUsersAPI.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwUsersAPI$Param;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwUsersAPI;",
        ">;",
        "Landroid/os/Parcelable;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwUsersAPI;

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwUsersAPI;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum USERS_CONTRIBUTEES:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwUsersAPI;

.field public static final enum USERS_CONTRIBUTORS:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwUsersAPI;

.field public static final enum USERS_LOOKUP:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwUsersAPI;

.field public static final enum USERS_PROFILEIMAGE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwUsersAPI;

.field public static final enum USERS_SEARCH:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwUsersAPI;

.field public static final enum USERS_SHOW:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwUsersAPI;

.field public static final enum USERS_SUGGESTIONS:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwUsersAPI;

.field public static final enum USERS_SUGGESTIONS_SLUG:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwUsersAPI;

.field public static final enum USERS_SUGGESTIONS_SLUG_MEMBERS:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwUsersAPI;


# instance fields
.field private mHttpMethod:Ljava/lang/String;

.field private mUrl:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 30
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwUsersAPI;

    const-string v1, "USERS_LOOKUP"

    const-string v2, "/users/lookup.json"

    const-string v3, "GET"

    invoke-direct {v0, v1, v5, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwUsersAPI;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwUsersAPI;->USERS_LOOKUP:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwUsersAPI;

    .line 31
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwUsersAPI;

    const-string v1, "USERS_PROFILEIMAGE"

    const-string v2, "/users/profile_image/"

    const-string v3, "GET"

    invoke-direct {v0, v1, v6, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwUsersAPI;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwUsersAPI;->USERS_PROFILEIMAGE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwUsersAPI;

    .line 32
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwUsersAPI;

    const-string v1, "USERS_SEARCH"

    const-string v2, "/users/search.json"

    const-string v3, "GET"

    invoke-direct {v0, v1, v7, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwUsersAPI;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwUsersAPI;->USERS_SEARCH:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwUsersAPI;

    .line 33
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwUsersAPI;

    const-string v1, "USERS_SHOW"

    const-string v2, "/users/show.json"

    const-string v3, "GET"

    invoke-direct {v0, v1, v8, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwUsersAPI;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwUsersAPI;->USERS_SHOW:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwUsersAPI;

    .line 34
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwUsersAPI;

    const-string v1, "USERS_CONTRIBUTEES"

    const-string v2, "/users/contributees.json"

    const-string v3, "GET"

    invoke-direct {v0, v1, v9, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwUsersAPI;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwUsersAPI;->USERS_CONTRIBUTEES:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwUsersAPI;

    .line 35
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwUsersAPI;

    const-string v1, "USERS_CONTRIBUTORS"

    const/4 v2, 0x5

    const-string v3, "/users/contributors.json"

    const-string v4, "GET"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwUsersAPI;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwUsersAPI;->USERS_CONTRIBUTORS:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwUsersAPI;

    .line 36
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwUsersAPI;

    const-string v1, "USERS_SUGGESTIONS"

    const/4 v2, 0x6

    const-string v3, "/users/suggestions.json"

    const-string v4, "GET"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwUsersAPI;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwUsersAPI;->USERS_SUGGESTIONS:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwUsersAPI;

    .line 37
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwUsersAPI;

    const-string v1, "USERS_SUGGESTIONS_SLUG"

    const/4 v2, 0x7

    const-string v3, "/users/suggestions/"

    const-string v4, "GET"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwUsersAPI;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwUsersAPI;->USERS_SUGGESTIONS_SLUG:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwUsersAPI;

    .line 38
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwUsersAPI;

    const-string v1, "USERS_SUGGESTIONS_SLUG_MEMBERS"

    const/16 v2, 0x8

    const-string v3, "/users/suggestions/"

    const-string v4, "GET"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwUsersAPI;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwUsersAPI;->USERS_SUGGESTIONS_SLUG_MEMBERS:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwUsersAPI;

    .line 28
    const/16 v0, 0x9

    new-array v0, v0, [Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwUsersAPI;

    sget-object v1, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwUsersAPI;->USERS_LOOKUP:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwUsersAPI;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwUsersAPI;->USERS_PROFILEIMAGE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwUsersAPI;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwUsersAPI;->USERS_SEARCH:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwUsersAPI;

    aput-object v1, v0, v7

    sget-object v1, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwUsersAPI;->USERS_SHOW:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwUsersAPI;

    aput-object v1, v0, v8

    sget-object v1, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwUsersAPI;->USERS_CONTRIBUTEES:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwUsersAPI;

    aput-object v1, v0, v9

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwUsersAPI;->USERS_CONTRIBUTORS:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwUsersAPI;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwUsersAPI;->USERS_SUGGESTIONS:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwUsersAPI;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwUsersAPI;->USERS_SUGGESTIONS_SLUG:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwUsersAPI;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwUsersAPI;->USERS_SUGGESTIONS_SLUG_MEMBERS:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwUsersAPI;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwUsersAPI;->$VALUES:[Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwUsersAPI;

    .line 84
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwUsersAPI$1;

    invoke-direct {v0}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwUsersAPI$1;-><init>()V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwUsersAPI;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p3, "url"    # Ljava/lang/String;
    .param p4, "httpMethod"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 66
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 67
    iput-object p3, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwUsersAPI;->mUrl:Ljava/lang/String;

    .line 68
    iput-object p4, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwUsersAPI;->mHttpMethod:Ljava/lang/String;

    .line 69
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwUsersAPI;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 28
    const-class v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwUsersAPI;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwUsersAPI;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwUsersAPI;
    .locals 1

    .prologue
    .line 28
    sget-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwUsersAPI;->$VALUES:[Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwUsersAPI;

    invoke-virtual {v0}, [Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwUsersAPI;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwUsersAPI;

    return-object v0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 100
    const/4 v0, 0x0

    return v0
.end method

.method public getHttpMethod()Ljava/lang/String;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwUsersAPI;->mHttpMethod:Ljava/lang/String;

    return-object v0
.end method

.method public getUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwUsersAPI;->mUrl:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwUsersAPI;->mUrl:Ljava/lang/String;

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 105
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwUsersAPI;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 106
    return-void
.end method
