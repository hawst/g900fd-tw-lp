.class Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity$4;
.super Landroid/webkit/WebViewClient;
.source "SnsAccountGpAuthActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;)V
    .locals 0

    .prologue
    .line 260
    iput-object p1, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity$4;->this$0:Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "errorCode"    # I
    .param p3, "description"    # Ljava/lang/String;
    .param p4, "failingUrl"    # Ljava/lang/String;

    .prologue
    .line 307
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity$4;->this$0:Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;

    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity$4;->this$0:Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;

    # invokes: Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->handleError(Landroid/content/Context;I)V
    invoke-static {v0, v1, p2}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->access$800(Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;Landroid/content/Context;I)V

    .line 308
    invoke-super {p0, p1, p2, p3, p4}, Landroid/webkit/WebViewClient;->onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V

    .line 309
    return-void
.end method

.method public shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 7
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "url"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    const/4 v4, -0x1

    .line 264
    invoke-static {}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->isLoggable()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 265
    # getter for: Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->access$100()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "####### WebView shouldOverrideUrlLoading URL : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 268
    :cond_0
    if-eqz p2, :cond_2

    const-string v1, "http://localhost"

    invoke-virtual {p2, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 269
    invoke-virtual {p1}, Landroid/webkit/WebView;->stopLoading()V

    .line 270
    invoke-virtual {p1}, Landroid/webkit/WebView;->clearView()V

    .line 273
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity$4;->this$0:Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;

    new-instance v2, Landroid/webkit/WebView;

    iget-object v3, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity$4;->this$0:Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;

    # getter for: Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->mAppContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->access$500(Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;)Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->setContentView(Landroid/view/View;)V

    .line 275
    const/4 v0, 0x0

    .line 277
    .local v0, "code":Ljava/lang/String;
    const-string v1, "http://localhost"

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x7

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {p2, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 279
    invoke-static {}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->isLoggable()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 280
    # getter for: Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->access$100()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SnsAccountFbAuthActivity : shouldOverrideUrlLoading() - code = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 285
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity$4;->this$0:Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;

    # setter for: Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->mCode:Ljava/lang/String;
    invoke-static {v1, v0}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->access$602(Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 287
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity$4;->this$0:Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;

    # getter for: Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->mCode:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->access$600(Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 288
    new-instance v1, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity$getAccessTokenTask;

    iget-object v2, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity$4;->this$0:Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;

    invoke-direct {v1, v2, v6}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity$getAccessTokenTask;-><init>(Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity$1;)V

    new-array v2, v5, [Ljava/lang/Void;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity$getAccessTokenTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 301
    .end local v0    # "code":Ljava/lang/String;
    :cond_2
    :goto_0
    return v5

    .line 294
    .restart local v0    # "code":Ljava/lang/String;
    :cond_3
    # getter for: Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->access$100()Ljava/lang/String;

    move-result-object v1

    const-string v2, "SnsAccountFbAuthActivity - state is wrong!!"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 296
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity$4;->this$0:Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;

    invoke-virtual {v1, v4, v4, v6}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->loginFail(IILjava/lang/String;)V

    goto :goto_0
.end method
