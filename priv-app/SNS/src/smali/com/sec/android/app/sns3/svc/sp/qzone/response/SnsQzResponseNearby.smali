.class public final Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseNearby;
.super Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;
.source "SnsQzResponseNearby.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseNearby;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public mAuthorUID:Ljava/lang/String;

.field public mDetailParam:Ljava/lang/String;

.field public mHostOpenID:Ljava/lang/String;

.field public mID:Ljava/lang/String;

.field public mLatitude:Ljava/lang/String;

.field public mLongitude:Ljava/lang/String;

.field public mMessage:Ljava/lang/String;

.field public mNext:Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseNearby;

.field public mPageID:Ljava/lang/String;

.field public mPlaceName:Ljava/lang/String;

.field public mPostId:Ljava/lang/String;

.field public mSex:Ljava/lang/String;

.field public mTimestamp:Ljava/lang/String;

.field public mType:Ljava/lang/String;

.field public mUserName:Ljava/lang/String;

.field public mUserPicUrl:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 62
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseNearby$1;

    invoke-direct {v0}, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseNearby$1;-><init>()V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseNearby;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 73
    invoke-direct {p0}, Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;-><init>()V

    .line 60
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseNearby;->mNext:Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseNearby;

    .line 74
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 76
    invoke-direct {p0}, Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;-><init>()V

    .line 60
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseNearby;->mNext:Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseNearby;

    .line 77
    invoke-virtual {p0, p1}, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseNearby;->readFromParcel(Landroid/os/Parcel;)V

    .line 78
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseNearby$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseNearby$1;

    .prologue
    .line 28
    invoke-direct {p0, p1}, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseNearby;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 123
    const/4 v0, 0x0

    return v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 102
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseNearby;->mAuthorUID:Ljava/lang/String;

    .line 103
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseNearby;->mID:Ljava/lang/String;

    .line 104
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseNearby;->mPageID:Ljava/lang/String;

    .line 105
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseNearby;->mTimestamp:Ljava/lang/String;

    .line 106
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseNearby;->mPlaceName:Ljava/lang/String;

    .line 107
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseNearby;->mLatitude:Ljava/lang/String;

    .line 108
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseNearby;->mLongitude:Ljava/lang/String;

    .line 109
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseNearby;->mUserName:Ljava/lang/String;

    .line 110
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseNearby;->mUserPicUrl:Ljava/lang/String;

    .line 111
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseNearby;->mType:Ljava/lang/String;

    .line 112
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseNearby;->mPostId:Ljava/lang/String;

    .line 113
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseNearby;->mMessage:Ljava/lang/String;

    .line 114
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseNearby;->mSex:Ljava/lang/String;

    .line 115
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseNearby;->mHostOpenID:Ljava/lang/String;

    .line 116
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseNearby;->mDetailParam:Ljava/lang/String;

    .line 118
    const-class v0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseNearby;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseNearby;

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseNearby;->mNext:Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseNearby;

    .line 119
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flag"    # I

    .prologue
    .line 82
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseNearby;->mAuthorUID:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 83
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseNearby;->mID:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 84
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseNearby;->mPageID:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 85
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseNearby;->mTimestamp:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 86
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseNearby;->mPlaceName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 87
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseNearby;->mLatitude:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 88
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseNearby;->mLongitude:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 89
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseNearby;->mUserName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 90
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseNearby;->mUserPicUrl:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 91
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseNearby;->mType:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 92
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseNearby;->mPostId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 93
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseNearby;->mMessage:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 94
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseNearby;->mSex:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 95
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseNearby;->mHostOpenID:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 96
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseNearby;->mDetailParam:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 98
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseNearby;->mNext:Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseNearby;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 99
    return-void
.end method
