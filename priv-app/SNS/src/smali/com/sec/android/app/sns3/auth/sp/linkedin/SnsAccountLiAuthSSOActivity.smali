.class public Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;
.super Landroid/accounts/AccountAuthenticatorActivity;
.source "SnsAccountLiAuthSSOActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity$GetAccessTokenTask;
    }
.end annotation


# static fields
.field private static final GOOGLE_ACCOUNT_TYPE:Ljava/lang/String; = "com.google"

.field private static final LINKEDIN_APP_SIGNATURE:Ljava/lang/String; = "3082036830820250a00302010202044d07f422300d06092a864886f70d01010505003076310b3009060355040613025553310b3009060355040813024341311630140603550407130d4d6f756e7461696e205669657731163014060355040a130d4c696e6b6564496e20436f7270310c300a060355040b13034c4544311c301a060355040313134a61696b756d61722052616d616e617468616e301e170d3130313231343232343830325a170d3338303530313232343830325a3076310b3009060355040613025553310b3009060355040813024341311630140603550407130d4d6f756e7461696e205669657731163014060355040a130d4c696e6b6564496e20436f7270310c300a060355040b13034c4544311c301a060355040313134a61696b756d61722052616d616e617468616e30820122300d06092a864886f70d01010105000382010f003082010a02820101008164a246b2fe4ee3b80e2c16b941abfaddd652a5a7080b4911f61cc9c73bcf288791825b041e454156433de1ea4c9a3861c220637cce78d445ff820887adbe52b0da8a1e8d157f376c79f724833c611af9bcb537eb0c3a3a8aa953e99933eb19676ccaedf74cb152274ae78e809af2b96770b24b27e09c3a16b02b4e43b0d77802c2a9fad7539301f35ffae81f45dda143e1d88d1642e4e68ff1dca79a84731adbbe812db7ffe71d4c8ec0a005e1cd034d8a60ffac3e3d941f7224d37b10431890a5bf60a3f9e51cd8fcf83d9498d9850d775d617ffa68b2496343f5190a2e0be0067f660215deae1bdae09c7761f3a82fa7a8c6a0430ee54e910da61d2c2bd30203010001300d06092a864886f70d010105050003820101006ea1432edcc3e302a49604d41e569397bc158df2b583a34d3e57f597943a98179b2b6d41b7055ce3b247b99ed88bb7e12a213c6de0c1adac46567b0a190d924c0a2990be0bd774d7269300534ca4e88e146fbc49b2cff957cfec84bcc9aeacfe2572391943aac521c88b792c0645c0de6803250867268ed2c1fda13f8c6cdb943214de2fb7737dd578e0e60db4e16e1778c39f1bed2bb567e03dbb14803f6fc5824da3f34e8d1e2e9e93ce49e911c321ac36d8ff10e223c8d29b30ebeacfa42abcff52e079a84542dca6d3c06a62b08dfe426cdc4ead83eb3119f096b7d100964b771d4af46f8ac77ddee79c4fc801bd656b06a4a28f872f4ff308515c3c5a08"

.field private static final LINKEDIN_APP_SSO_ENABLED_VERSION:I = 0x56

.field private static final LINKEDIN_MARKET_URI:Ljava/lang/String; = "market://details?id=com.linkedin.android&referrer=utm_source%3Dsamsung_galaxy"

.field private static final LINKEDIN_PACKAGE_NAME:Ljava/lang/String; = "com.linkedin.android"

.field private static final REQUEST_SYNC_INTERVAL:I = 0x3e8

.field private static final TAG:Ljava/lang/String; = "SnsAccountLiAuthSSOActivity"

.field private static final TIME_TO_HOLD_ACTIVITY:I = 0xbb8

.field private static final WATCHDOG_TIMEOUT:I = 0x2710

.field private static final mAppId:Ljava/lang/String;

.field private static mInstanceCount:I = 0x0

.field private static final mScope:Ljava/lang/String; = "r_fullprofile r_emailaddress r_network r_contactinfo rw_nus rw_groups w_messages"

.field private static final mSecret:Ljava/lang/String;


# instance fields
.field private mAccessTokenTask:Landroid/os/AsyncTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/AsyncTask",
            "<***>;"
        }
    .end annotation
.end field

.field private mAccountManagerCallback:Landroid/accounts/AccountManagerCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/accounts/AccountManagerCallback",
            "<",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation
.end field

.field private mAppContext:Landroid/content/Context;

.field private mConnectionProgressDialog:Landroid/app/ProgressDialog;

.field private mHandler:Landroid/os/Handler;

.field private mRetryLogin:Z

.field private mSSOFromApp:Z

.field private mSkipSSONoti:Z

.field private mSkipSyncSetup:Z

.field private mWatchdogRunnable:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 98
    invoke-static {}, Lcom/sec/android/app/sns3/svc/sp/linkedin/SnsLiAppIdManager;->getInstance()Lcom/sec/android/app/sns3/svc/sp/linkedin/SnsLiAppIdManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/SnsLiAppIdManager;->getAppKey()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->mAppId:Ljava/lang/String;

    .line 99
    invoke-static {}, Lcom/sec/android/app/sns3/svc/sp/linkedin/SnsLiAppIdManager;->getInstance()Lcom/sec/android/app/sns3/svc/sp/linkedin/SnsLiAppIdManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/SnsLiAppIdManager;->getSecretKey()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->mSecret:Ljava/lang/String;

    .line 107
    const/4 v0, 0x0

    sput v0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->mInstanceCount:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 66
    invoke-direct {p0}, Landroid/accounts/AccountAuthenticatorActivity;-><init>()V

    .line 109
    iput-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->mAppContext:Landroid/content/Context;

    .line 110
    iput-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->mHandler:Landroid/os/Handler;

    .line 111
    iput-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->mAccessTokenTask:Landroid/os/AsyncTask;

    .line 112
    iput-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->mConnectionProgressDialog:Landroid/app/ProgressDialog;

    .line 113
    iput-boolean v0, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->mRetryLogin:Z

    .line 114
    iput-boolean v0, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->mSSOFromApp:Z

    .line 115
    iput-boolean v0, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->mSkipSSONoti:Z

    .line 116
    iput-boolean v0, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->mSkipSyncSetup:Z

    .line 118
    new-instance v0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity$1;-><init>(Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->mAccountManagerCallback:Landroid/accounts/AccountManagerCallback;

    .line 172
    new-instance v0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity$2;-><init>(Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->mWatchdogRunnable:Ljava/lang/Runnable;

    .line 469
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;

    .prologue
    .line 66
    iget-boolean v0, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->mSkipSyncSetup:Z

    return v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;

    .prologue
    .line 66
    iget-boolean v0, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->mRetryLogin:Z

    return v0
.end method

.method static synthetic access$1000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 66
    sget-object v0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->mSecret:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;

    .prologue
    .line 66
    invoke-direct {p0}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->registerRetryNotification()V

    return-void
.end method

.method static synthetic access$1200(Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 66
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->loginSuccess(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1400(Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;

    .prologue
    .line 66
    iget-boolean v0, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->mSkipSSONoti:Z

    return v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;

    .prologue
    .line 66
    invoke-direct {p0}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->isLoggedInLinkedin()Z

    move-result v0

    return v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->mWatchdogRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->mAppContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;)Landroid/app/ProgressDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->mConnectionProgressDialog:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$602(Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;
    .param p1, "x1"    # Landroid/app/ProgressDialog;

    .prologue
    .line 66
    iput-object p1, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->mConnectionProgressDialog:Landroid/app/ProgressDialog;

    return-object p1
.end method

.method static synthetic access$700(Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;

    .prologue
    .line 66
    iget-boolean v0, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->mSSOFromApp:Z

    return v0
.end method

.method static synthetic access$900()Ljava/lang/String;
    .locals 1

    .prologue
    .line 66
    sget-object v0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->mAppId:Ljava/lang/String;

    return-object v0
.end method

.method private checkLinkedinAppSignature()Z
    .locals 10

    .prologue
    const/4 v6, 0x0

    .line 454
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v7

    const-string v8, "com.linkedin.android"

    const/16 v9, 0x40

    invoke-virtual {v7, v8, v9}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 460
    .local v4, "pi":Landroid/content/pm/PackageInfo;
    iget-object v0, v4, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    .local v0, "arr$":[Landroid/content/pm/Signature;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v5, v0, v2

    .line 461
    .local v5, "s":Landroid/content/pm/Signature;
    const-string v7, "3082036830820250a00302010202044d07f422300d06092a864886f70d01010505003076310b3009060355040613025553310b3009060355040813024341311630140603550407130d4d6f756e7461696e205669657731163014060355040a130d4c696e6b6564496e20436f7270310c300a060355040b13034c4544311c301a060355040313134a61696b756d61722052616d616e617468616e301e170d3130313231343232343830325a170d3338303530313232343830325a3076310b3009060355040613025553310b3009060355040813024341311630140603550407130d4d6f756e7461696e205669657731163014060355040a130d4c696e6b6564496e20436f7270310c300a060355040b13034c4544311c301a060355040313134a61696b756d61722052616d616e617468616e30820122300d06092a864886f70d01010105000382010f003082010a02820101008164a246b2fe4ee3b80e2c16b941abfaddd652a5a7080b4911f61cc9c73bcf288791825b041e454156433de1ea4c9a3861c220637cce78d445ff820887adbe52b0da8a1e8d157f376c79f724833c611af9bcb537eb0c3a3a8aa953e99933eb19676ccaedf74cb152274ae78e809af2b96770b24b27e09c3a16b02b4e43b0d77802c2a9fad7539301f35ffae81f45dda143e1d88d1642e4e68ff1dca79a84731adbbe812db7ffe71d4c8ec0a005e1cd034d8a60ffac3e3d941f7224d37b10431890a5bf60a3f9e51cd8fcf83d9498d9850d775d617ffa68b2496343f5190a2e0be0067f660215deae1bdae09c7761f3a82fa7a8c6a0430ee54e910da61d2c2bd30203010001300d06092a864886f70d010105050003820101006ea1432edcc3e302a49604d41e569397bc158df2b583a34d3e57f597943a98179b2b6d41b7055ce3b247b99ed88bb7e12a213c6de0c1adac46567b0a190d924c0a2990be0bd774d7269300534ca4e88e146fbc49b2cff957cfec84bcc9aeacfe2572391943aac521c88b792c0645c0de6803250867268ed2c1fda13f8c6cdb943214de2fb7737dd578e0e60db4e16e1778c39f1bed2bb567e03dbb14803f6fc5824da3f34e8d1e2e9e93ce49e911c321ac36d8ff10e223c8d29b30ebeacfa42abcff52e079a84542dca6d3c06a62b08dfe426cdc4ead83eb3119f096b7d100964b771d4af46f8ac77ddee79c4fc801bd656b06a4a28f872f4ff308515c3c5a08"

    invoke-virtual {v5}, Landroid/content/pm/Signature;->toCharsString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 462
    const/4 v6, 0x1

    .line 466
    .end local v0    # "arr$":[Landroid/content/pm/Signature;
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    .end local v4    # "pi":Landroid/content/pm/PackageInfo;
    .end local v5    # "s":Landroid/content/pm/Signature;
    :cond_0
    :goto_1
    return v6

    .line 456
    :catch_0
    move-exception v1

    .line 457
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    goto :goto_1

    .line 460
    .end local v1    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v0    # "arr$":[Landroid/content/pm/Signature;
    .restart local v2    # "i$":I
    .restart local v3    # "len$":I
    .restart local v4    # "pi":Landroid/content/pm/PackageInfo;
    .restart local v5    # "s":Landroid/content/pm/Signature;
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method private checkLinkedinAppVersion()Z
    .locals 5

    .prologue
    .line 437
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const-string v3, "com.linkedin.android"

    const/16 v4, 0x40

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 439
    .local v1, "pi":Landroid/content/pm/PackageInfo;
    iget v2, v1, Landroid/content/pm/PackageInfo;->versionCode:I

    const/16 v3, 0x56

    if-lt v2, v3, :cond_0

    .line 440
    const/4 v2, 0x1

    .line 448
    .end local v1    # "pi":Landroid/content/pm/PackageInfo;
    :goto_0
    return v2

    .line 442
    .restart local v1    # "pi":Landroid/content/pm/PackageInfo;
    :cond_0
    const-string v2, "SnsAccountLiAuthSSOActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "App version : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v1, Landroid/content/pm/PackageInfo;->versionCode:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v1, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 448
    .end local v1    # "pi":Landroid/content/pm/PackageInfo;
    :goto_1
    const/4 v2, 0x0

    goto :goto_0

    .line 444
    :catch_0
    move-exception v0

    .line 445
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_1
.end method

.method private gotoMarketForLinkedin()V
    .locals 5

    .prologue
    .line 410
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 411
    .local v1, "marketIntent":Landroid/content/Intent;
    const-string v2, "market://details?id=com.linkedin.android&referrer=utm_source%3Dsamsung_galaxy"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 412
    const/high16 v2, 0x10000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 414
    :try_start_0
    invoke-virtual {p0, v1}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 419
    :goto_0
    return-void

    .line 415
    :catch_0
    move-exception v0

    .line 416
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f080035

    const/4 v4, 0x1

    invoke-static {v2, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method private invokeLinkedinSSOAuth()V
    .locals 6

    .prologue
    .line 331
    invoke-direct {p0}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->checkLinkedinAppVersion()Z

    move-result v1

    if-nez v1, :cond_1

    .line 332
    const-string v1, "SnsAccountLiAuthSSOActivity"

    const-string v2, "Linkedin app is old version"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 333
    iget-boolean v1, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->mSSOFromApp:Z

    if-nez v1, :cond_0

    .line 334
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 335
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->startActivity(Landroid/content/Intent;)V

    .line 337
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->finish()V

    .line 379
    :goto_0
    return-void

    .line 341
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->checkLinkedinAppSignature()Z

    move-result v1

    if-nez v1, :cond_2

    .line 342
    const-string v1, "SnsAccountLiAuthSSOActivity"

    const-string v2, "signature of Linkedin app is invalid"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 343
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->mAppContext:Landroid/content/Context;

    const-string v2, "Current Linkedin application is not official version"

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 345
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->finish()V

    goto :goto_0

    .line 349
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->mWatchdogRunnable:Ljava/lang/Runnable;

    const-wide/16 v4, 0x2710

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 350
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->mHandler:Landroid/os/Handler;

    new-instance v2, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity$4;

    invoke-direct {v2, p0}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity$4;-><init>(Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;)V

    const-wide/16 v4, 0xc8

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 378
    new-instance v1, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity$GetAccessTokenTask;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity$GetAccessTokenTask;-><init>(Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity$1;)V

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity$GetAccessTokenTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->mAccessTokenTask:Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method private isLinkedinAppInstalled()Z
    .locals 4

    .prologue
    .line 383
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const-string v2, "com.linkedin.android"

    const/16 v3, 0x80

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 387
    const/4 v1, 0x1

    :goto_0
    return v1

    .line 384
    :catch_0
    move-exception v0

    .line 385
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private isLoggedInGoogle()Z
    .locals 3

    .prologue
    .line 401
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    const-string v2, "com.google"

    invoke-virtual {v1, v2}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 402
    .local v0, "account":[Landroid/accounts/Account;
    if-eqz v0, :cond_0

    array-length v1, v0

    if-lez v1, :cond_0

    .line 403
    const/4 v1, 0x1

    .line 405
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private isLoggedInLinkedin()Z
    .locals 3

    .prologue
    .line 391
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    const-string v2, "com.linkedin.android"

    invoke-virtual {v1, v2}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 393
    .local v0, "account":[Landroid/accounts/Account;
    if-eqz v0, :cond_0

    array-length v1, v0

    if-lez v1, :cond_0

    .line 394
    const/4 v1, 0x1

    .line 396
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isRunning()Z
    .locals 1

    .prologue
    .line 271
    sget v0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->mInstanceCount:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private loginSuccess(Ljava/lang/String;Ljava/lang/String;)V
    .locals 10
    .param p1, "userName"    # Ljava/lang/String;
    .param p2, "userId"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 275
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 276
    .local v4, "options":Landroid/os/Bundle;
    invoke-static {}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->isLoggable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 277
    const-string v0, "SnsAccountLiAuthSSOActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SnsAccountLiAuthSSOActivity : addAccount() :  userName = "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 280
    :cond_0
    const-string v0, "username"

    invoke-virtual {v4, v0, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 282
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->mAppContext:Landroid/content/Context;

    if-nez v0, :cond_1

    .line 283
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    const-string v1, "com.sec.android.app.sns3.linkedin"

    move-object v3, v2

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Landroid/accounts/AccountManager;->addAccount(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Landroid/os/Bundle;Landroid/app/Activity;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    .line 289
    :goto_0
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/SnsApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    .line 290
    .local v8, "cr":Landroid/content/ContentResolver;
    new-instance v9, Landroid/content/ContentValues;

    invoke-direct {v9}, Landroid/content/ContentValues;-><init>()V

    .line 292
    .local v9, "values":Landroid/content/ContentValues;
    invoke-virtual {v9}, Landroid/content/ContentValues;->clear()V

    .line 294
    const-string v0, "id"

    invoke-virtual {v9, v0, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 295
    const-string v0, "formatted_name"

    invoke-virtual {v9, v0, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 296
    sget-object v0, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$UserBasicInfo;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v8, v0, v9}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 297
    return-void

    .line 286
    .end local v8    # "cr":Landroid/content/ContentResolver;
    .end local v9    # "values":Landroid/content/ContentValues;
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->mAppContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    const-string v1, "com.sec.android.app.sns3.linkedin"

    iget-object v6, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->mAccountManagerCallback:Landroid/accounts/AccountManagerCallback;

    move-object v3, v2

    move-object v5, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Landroid/accounts/AccountManager;->addAccount(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Landroid/os/Bundle;Landroid/app/Activity;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    goto :goto_0
.end method

.method private registerRetryNotification()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 300
    new-instance v0, Landroid/content/Intent;

    const-class v6, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;

    invoke-direct {v0, p0, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 301
    .local v0, "i":Landroid/content/Intent;
    const/high16 v6, 0x10000000

    invoke-virtual {v0, v6}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 302
    const/16 v6, 0xbb8

    invoke-static {p0, v6, v0, v7}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 305
    .local v1, "intent":Landroid/app/PendingIntent;
    const v6, 0x7f08003d

    invoke-virtual {p0, v6}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 306
    .local v4, "ticker":Ljava/lang/String;
    const v6, 0x7f08002e

    invoke-virtual {p0, v6}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 308
    .local v5, "title":Ljava/lang/String;
    new-instance v3, Landroid/app/Notification$Builder;

    iget-object v6, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->mAppContext:Landroid/content/Context;

    invoke-direct {v3, v6}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    .line 309
    .local v3, "noti":Landroid/app/Notification$Builder;
    invoke-virtual {v3, v5}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    .line 310
    invoke-virtual {v3, v4}, Landroid/app/Notification$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    .line 311
    invoke-virtual {v3, v4}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    .line 312
    invoke-virtual {v3, v1}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    .line 313
    invoke-virtual {v3, v7}, Landroid/app/Notification$Builder;->setAutoCancel(Z)Landroid/app/Notification$Builder;

    .line 314
    new-instance v6, Landroid/app/Notification$BigTextStyle;

    invoke-direct {v6}, Landroid/app/Notification$BigTextStyle;-><init>()V

    invoke-virtual {v6, v4}, Landroid/app/Notification$BigTextStyle;->bigText(Ljava/lang/CharSequence;)Landroid/app/Notification$BigTextStyle;

    move-result-object v6

    invoke-virtual {v3, v6}, Landroid/app/Notification$Builder;->setStyle(Landroid/app/Notification$Style;)Landroid/app/Notification$Builder;

    .line 315
    const/4 v6, 0x1

    invoke-virtual {v3, v6}, Landroid/app/Notification$Builder;->setDefaults(I)Landroid/app/Notification$Builder;

    .line 316
    const v6, 0x7f02001e

    invoke-virtual {v3, v6}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    .line 318
    const-string v6, "notification"

    invoke-virtual {p0, v6}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/NotificationManager;

    .line 319
    .local v2, "nm":Landroid/app/NotificationManager;
    const/16 v6, 0xc1c

    invoke-virtual {v3}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v7

    invoke-virtual {v2, v6, v7}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 321
    iget-object v6, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->mHandler:Landroid/os/Handler;

    new-instance v7, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity$3;

    invoke-direct {v7, p0}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity$3;-><init>(Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;)V

    invoke-virtual {v6, v7}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 328
    return-void
.end method

.method private showAlertDialog()V
    .locals 8

    .prologue
    const v7, 0x7f08002e

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 422
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/sns3/auth/SnsAccountAlertDialogActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 423
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "icon_id"

    const v2, 0x7f020017

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 424
    const-string v1, "title"

    const v2, 0x7f080046

    new-array v3, v6, [Ljava/lang/Object;

    invoke-virtual {p0, v7}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {p0, v2, v3}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 426
    const-string v1, "dlg_title"

    const v2, 0x7f08000d

    invoke-virtual {p0, v2}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 427
    const-string v1, "dlg_message"

    const v2, 0x7f08003c

    new-array v3, v6, [Ljava/lang/Object;

    invoke-virtual {p0, v7}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {p0, v2, v3}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 429
    const-string v1, "url"

    const-string v2, "market://details?id=com.linkedin.android&referrer=utm_source%3Dsamsung_galaxy"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 430
    const/high16 v1, 0x800000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 431
    invoke-virtual {p0, v0}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->startActivity(Landroid/content/Intent;)V

    .line 432
    return-void
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 265
    const/16 v0, 0x3e8

    if-ne p1, v0, :cond_0

    .line 266
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->finish()V

    .line 268
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 11
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const v3, 0x7f08002e

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 190
    const v0, 0x103006e

    invoke-virtual {p0, v0}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->setTheme(I)V

    .line 192
    invoke-super {p0, p1}, Landroid/accounts/AccountAuthenticatorActivity;->onCreate(Landroid/os/Bundle;)V

    .line 194
    sget v0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->mInstanceCount:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->mInstanceCount:I

    .line 195
    sget v0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->mInstanceCount:I

    if-le v0, v5, :cond_0

    .line 196
    const-string v0, "SnsAccountLiAuthSSOActivity"

    const-string v1, "multi instance"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 199
    :cond_0
    const v0, 0x7f080046

    new-array v1, v5, [Ljava/lang/Object;

    invoke-virtual {p0, v3}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 201
    iput-object p0, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->mAppContext:Landroid/content/Context;

    .line 202
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->mHandler:Landroid/os/Handler;

    .line 204
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "RetryLogin"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->mRetryLogin:Z

    .line 205
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "sso_from_app"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->mSSOFromApp:Z

    .line 206
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "skip_sso_noti"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->mSkipSSONoti:Z

    .line 207
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "skip_sync_setup"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->mSkipSyncSetup:Z

    .line 209
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    const-string v1, "com.sec.android.app.sns3.linkedin"

    invoke-virtual {v0, v1}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v8

    .line 210
    .local v8, "accounts":[Landroid/accounts/Account;
    array-length v0, v8

    if-lez v0, :cond_2

    iget-boolean v0, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->mRetryLogin:Z

    if-nez v0, :cond_2

    .line 211
    invoke-direct {p0}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->isLoggedInLinkedin()Z

    move-result v0

    if-nez v0, :cond_1

    .line 212
    const-string v0, "SnsAccountLiAuthSSOActivity"

    const-string v1, "login to linkedin app only"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 214
    :try_start_0
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    const-string v1, "com.linkedin.android"

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v5, p0

    invoke-virtual/range {v0 .. v7}, Landroid/accounts/AccountManager;->addAccount(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Landroid/os/Bundle;Landroid/app/Activity;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 225
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->finish()V

    .line 245
    :goto_1
    return-void

    .line 216
    :catch_0
    move-exception v9

    .line 217
    .local v9, "e":Ljava/lang/Exception;
    invoke-virtual {v9}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 220
    .end local v9    # "e":Ljava/lang/Exception;
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const/high16 v1, 0x7f080000

    new-array v2, v5, [Ljava/lang/Object;

    invoke-virtual {p0, v3}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 229
    :cond_2
    const-string v0, "notification"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/app/NotificationManager;

    .line 230
    .local v10, "notiMgr":Landroid/app/NotificationManager;
    const/16 v0, 0xc1c

    invoke-virtual {v10, v0}, Landroid/app/NotificationManager;->cancel(I)V

    .line 231
    const/16 v0, 0xce4

    invoke-virtual {v10, v0}, Landroid/app/NotificationManager;->cancel(I)V

    .line 233
    invoke-direct {p0}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->isLinkedinAppInstalled()Z

    move-result v0

    if-nez v0, :cond_4

    .line 234
    const-string v0, "SnsAccountLiAuthSSOActivity"

    const-string v1, "Linkedin app is not installed"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 235
    invoke-direct {p0}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->isLoggedInGoogle()Z

    move-result v0

    if-ne v0, v5, :cond_3

    .line 236
    invoke-direct {p0}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->gotoMarketForLinkedin()V

    .line 241
    :goto_2
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->finish()V

    goto :goto_1

    .line 238
    :cond_3
    invoke-direct {p0}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->showAlertDialog()V

    goto :goto_2

    .line 243
    :cond_4
    invoke-direct {p0}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->invokeLinkedinSSOAuth()V

    goto :goto_1
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 249
    sget v0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->mInstanceCount:I

    add-int/lit8 v0, v0, -0x1

    sput v0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->mInstanceCount:I

    .line 251
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->mConnectionProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    .line 252
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->mConnectionProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 253
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->mConnectionProgressDialog:Landroid/app/ProgressDialog;

    .line 256
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->mAccessTokenTask:Landroid/os/AsyncTask;

    if-eqz v0, :cond_1

    .line 257
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->mAccessTokenTask:Landroid/os/AsyncTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->cancel(Z)Z

    .line 260
    :cond_1
    invoke-super {p0}, Landroid/accounts/AccountAuthenticatorActivity;->onDestroy()V

    .line 261
    return-void
.end method
