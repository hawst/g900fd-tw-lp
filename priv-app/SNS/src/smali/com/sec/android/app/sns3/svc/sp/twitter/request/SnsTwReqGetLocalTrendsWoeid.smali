.class public abstract Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwReqGetLocalTrendsWoeid;
.super Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestBase;
.source "SnsTwReqGetLocalTrendsWoeid.java"

# interfaces
.implements Lcom/sec/android/app/sns3/svc/sp/twitter/callback/ISnsTwReqCbTrendWoeid;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwTrendsAPI;Landroid/os/Bundle;)V
    .locals 1
    .param p1, "svcMgr"    # Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    .param p2, "trends"    # Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwTrendsAPI;
    .param p3, "param"    # Landroid/os/Bundle;

    .prologue
    .line 40
    const/16 v0, 0x16

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestBase;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;I)V

    .line 41
    invoke-virtual {p2}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwTrendsAPI;->getUrl()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwReqGetLocalTrendsWoeid;->mUrl:Ljava/lang/String;

    .line 42
    invoke-virtual {p2}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwTrendsAPI;->getHttpMethod()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwReqGetLocalTrendsWoeid;->mHttpMethod:Ljava/lang/String;

    .line 43
    iput-object p3, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwReqGetLocalTrendsWoeid;->mReqParams:Landroid/os/Bundle;

    .line 44
    return-void
.end method


# virtual methods
.method protected composeMore()V
    .locals 6

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwReqGetLocalTrendsWoeid;->mParams:Landroid/os/Bundle;

    const-string v1, "oauth_token"

    iget-object v2, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwReqGetLocalTrendsWoeid;->mToken:Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;

    invoke-virtual {v2}, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;->getAccessToken()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->encodeUrlEx(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwReqGetLocalTrendsWoeid;->mHttpMethod:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwReqGetLocalTrendsWoeid;->mReqUri:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwReqGetLocalTrendsWoeid;->mParams:Landroid/os/Bundle;

    iget-object v3, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwReqGetLocalTrendsWoeid;->mSubParams:Landroid/os/Bundle;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwitter;->CONSUMER_SECRET:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "&"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwReqGetLocalTrendsWoeid;->mToken:Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;

    invoke-virtual {v5}, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;->getAccessTokenSecret()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v1, v2, v3, v4}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/composer/SnsTwComposerUtils;->generateSignature(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwReqGetLocalTrendsWoeid;->mSig:Ljava/lang/String;

    .line 52
    return-void
.end method

.method protected parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;
    .locals 3
    .param p1, "content"    # Ljava/lang/String;

    .prologue
    .line 56
    const-string v1, "SNS"

    const-string v2, "<SnsTwReqGetLocalTrendsWoeid> parse()"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 57
    invoke-super {p0, p1}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestBase;->parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;

    move-result-object v0

    .line 59
    .local v0, "response":Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;
    if-eqz v0, :cond_0

    .line 62
    .end local v0    # "response":Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;
    :goto_0
    return-object v0

    .restart local v0    # "response":Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;
    :cond_0
    invoke-static {p1}, Lcom/sec/android/app/sns3/svc/sp/twitter/parser/SnsTwParserTrendWoeid;->parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseTrendWoeid;

    move-result-object v0

    goto :goto_0
.end method

.method protected respond(Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;)Z
    .locals 7
    .param p1, "result"    # Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;

    .prologue
    .line 67
    const-string v0, "SNS"

    const-string v1, "<SnsTwReqGetLocalTrendsWoeid> respond()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 69
    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->getReqID()I

    move-result v1

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->isSuccess()Z

    move-result v2

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->getHttpstatus()I

    move-result v3

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->getErrorCode()I

    move-result v4

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->getReason()Landroid/os/Bundle;

    move-result-object v5

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->getResponse()Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseTrendWoeid;

    move-object v0, p0

    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwReqGetLocalTrendsWoeid;->onReqRespond(IZIILandroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseTrendWoeid;)Z

    .line 73
    const/4 v0, 0x1

    return v0
.end method

.method protected setUrl()Ljava/lang/String;
    .locals 4

    .prologue
    .line 78
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 79
    .local v0, "sb":Ljava/lang/StringBuilder;
    iget-object v2, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwReqGetLocalTrendsWoeid;->mSubParams:Landroid/os/Bundle;

    const-string v3, "woeid"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 81
    .local v1, "weoid":Ljava/lang/String;
    const-string v2, "1.1"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 82
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwReqGetLocalTrendsWoeid;->mUrl:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".json"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 83
    iget-object v2, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwReqGetLocalTrendsWoeid;->mSubParams:Landroid/os/Bundle;

    const-string v3, "woeid"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 85
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method
