.class public interface abstract Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$LikesColumns;
.super Ljava/lang/Object;
.source "SnsFacebookDB.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "LikesColumns"
.end annotation


# static fields
.field public static final CATEGORY:Ljava/lang/String; = "category"

.field public static final CREATE_TIME:Ljava/lang/String; = "create_time"

.field public static final ID:Ljava/lang/String; = "likes_id"

.field public static final NAME:Ljava/lang/String; = "name"

.field public static final PHOTO_URL:Ljava/lang/String; = "photo_url"

.field public static final PROFILE_ID:Ljava/lang/String; = "profile_id"

.field public static final TARGET_ID:Ljava/lang/String; = "target_id"
