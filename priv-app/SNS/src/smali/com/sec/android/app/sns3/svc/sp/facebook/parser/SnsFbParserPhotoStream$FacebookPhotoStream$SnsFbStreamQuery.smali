.class public interface abstract Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserPhotoStream$FacebookPhotoStream$SnsFbStreamQuery;
.super Ljava/lang/Object;
.source "SnsFbParserPhotoStream.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserPhotoStream$FacebookPhotoStream;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "SnsFbStreamQuery"
.end annotation


# static fields
.field public static final ACTOR_ID:Ljava/lang/String; = "actor_id"

.field public static final ATTACHMENT:Ljava/lang/String; = "attachment"

.field public static final COMMENTS:Ljava/lang/String; = "comments"

.field public static final COUNT:Ljava/lang/String; = "count"

.field public static final CREATED_TIME:Ljava/lang/String; = "created_time"

.field public static final LIKES:Ljava/lang/String; = "likes"

.field public static final MEDIA:Ljava/lang/String; = "media"

.field public static final MESSAGE:Ljava/lang/String; = "message"

.field public static final PHOTO:Ljava/lang/String; = "photo"

.field public static final PID:Ljava/lang/String; = "pid"

.field public static final POST_ID:Ljava/lang/String; = "post_id"

.field public static final UPDATED_TIME:Ljava/lang/String; = "updated_time"
