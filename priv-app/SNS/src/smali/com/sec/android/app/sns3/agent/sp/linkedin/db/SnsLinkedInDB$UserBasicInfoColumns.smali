.class public interface abstract Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$UserBasicInfoColumns;
.super Ljava/lang/Object;
.source "SnsLinkedInDB.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "UserBasicInfoColumns"
.end annotation


# static fields
.field public static final DATE_OF_BIRTH:Ljava/lang/String; = "date_of_birth"

.field public static final EMAIL_ID:Ljava/lang/String; = "email_address"

.field public static final FIRST_NAME:Ljava/lang/String; = "first_name"

.field public static final HEADLINE:Ljava/lang/String; = "headline"

.field public static final INDUSTRY:Ljava/lang/String; = "industry"

.field public static final LAST_NAME:Ljava/lang/String; = "last_name"

.field public static final LOCATION_COUNTRY_CODE:Ljava/lang/String; = "location_code"

.field public static final LOCATION_NAME:Ljava/lang/String; = "location"

.field public static final NUM_CONNECTIONS:Ljava/lang/String; = "num_connections"

.field public static final PICTURE_URL:Ljava/lang/String; = "picture_url"

.field public static final PUBLIC_PROFILE_URL:Ljava/lang/String; = "public_profile_url"

.field public static final USER_ID:Ljava/lang/String; = "id"

.field public static final USER_NAME:Ljava/lang/String; = "formatted_name"
