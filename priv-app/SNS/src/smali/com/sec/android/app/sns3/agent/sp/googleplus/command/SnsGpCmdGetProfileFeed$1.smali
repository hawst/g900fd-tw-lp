.class Lcom/sec/android/app/sns3/agent/sp/googleplus/command/SnsGpCmdGetProfileFeed$1;
.super Lcom/sec/android/app/sns3/svc/sp/googleplus/request/SnsGpReqGetFeed;
.source "SnsGpCmdGetProfileFeed.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/sns3/agent/sp/googleplus/command/SnsGpCmdGetProfileFeed;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Handler;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/sns3/agent/sp/googleplus/command/SnsGpCmdGetProfileFeed;


# direct methods
.method constructor <init>(Lcom/sec/android/app/sns3/agent/sp/googleplus/command/SnsGpCmdGetProfileFeed;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 0
    .param p2, "x0"    # Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    .param p3, "x1"    # Ljava/lang/String;
    .param p4, "x2"    # Landroid/os/Bundle;

    .prologue
    .line 50
    iput-object p1, p0, Lcom/sec/android/app/sns3/agent/sp/googleplus/command/SnsGpCmdGetProfileFeed$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/googleplus/command/SnsGpCmdGetProfileFeed;

    invoke-direct {p0, p2, p3, p4}, Lcom/sec/android/app/sns3/svc/sp/googleplus/request/SnsGpReqGetFeed;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;Landroid/os/Bundle;)V

    return-void
.end method


# virtual methods
.method public onReqRespond(IZIILandroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;)Z
    .locals 8
    .param p1, "reqID"    # I
    .param p2, "bSuccess"    # Z
    .param p3, "httpStatus"    # I
    .param p4, "errorCode"    # I
    .param p5, "reason"    # Landroid/os/Bundle;
    .param p6, "feed"    # Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;

    .prologue
    const/4 v7, 0x0

    .line 55
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 56
    .local v3, "values":Landroid/content/ContentValues;
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/sns3/SnsApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 58
    .local v0, "cr":Landroid/content/ContentResolver;
    if-eqz p2, :cond_2

    .line 59
    if-eqz p6, :cond_1

    .line 61
    move-object v1, p6

    .line 62
    .local v1, "curFeed":Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;
    :goto_0
    if-eqz v1, :cond_1

    .line 63
    invoke-virtual {v3}, Landroid/content/ContentValues;->clear()V

    .line 65
    const-string v4, "post_id"

    iget-object v5, v1, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;->mPostId:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    const-string v4, "message"

    iget-object v5, v1, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;->mTitle:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    const-string v4, "timestamp_utc"

    iget-object v5, v1, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;->mUpdateTime:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    const-string v4, "object_type"

    iget-object v5, v1, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;->mObjectType:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    const-string v4, "media_url"

    iget-object v5, v1, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;->mMediaUrl:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    const-string v4, "link"

    iget-object v5, v1, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;->mContentUrl:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    const-string v4, "width"

    iget-object v5, v1, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;->mWidth:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    const-string v4, "height"

    iget-object v5, v1, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;->mHeight:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    const-string v4, "location_name"

    iget-object v5, v1, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;->mLocationName:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    const-string v4, "latitude"

    iget-object v5, v1, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;->mLatitude:Ljava/lang/Double;

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 75
    const-string v4, "longitude"

    iget-object v5, v1, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;->mLongitude:Ljava/lang/Double;

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 76
    sget-object v4, Lcom/sec/android/app/sns3/agent/sp/googleplus/db/SnsGooglePlusDB$UserGpFeedInfo;->CONTENT_URI:Landroid/net/Uri;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "post_id=\'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v1, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;->mPostId:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v3, v5, v7}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    .line 78
    .local v2, "update":I
    if-nez v2, :cond_0

    .line 79
    sget-object v4, Lcom/sec/android/app/sns3/agent/sp/googleplus/db/SnsGooglePlusDB$UserGpFeedInfo;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v4, v3}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 82
    :cond_0
    iget-object v1, v1, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;->mNext:Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;

    .line 83
    goto :goto_0

    .line 85
    .end local v1    # "curFeed":Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;
    .end local v2    # "update":I
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/sns3/agent/sp/googleplus/command/SnsGpCmdGetProfileFeed$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/googleplus/command/SnsGpCmdGetProfileFeed;

    sget-object v5, Lcom/sec/android/app/sns3/agent/sp/googleplus/db/SnsGooglePlusDB$UserGpFeedInfo;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v5}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/sns3/agent/sp/googleplus/command/SnsGpCmdGetProfileFeed;->setUri(Ljava/lang/String;)V

    .line 92
    :goto_1
    iget-object v4, p0, Lcom/sec/android/app/sns3/agent/sp/googleplus/command/SnsGpCmdGetProfileFeed$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/googleplus/command/SnsGpCmdGetProfileFeed;

    invoke-virtual {v4, p2}, Lcom/sec/android/app/sns3/agent/sp/googleplus/command/SnsGpCmdGetProfileFeed;->setSuccess(Z)V

    .line 93
    iget-object v4, p0, Lcom/sec/android/app/sns3/agent/sp/googleplus/command/SnsGpCmdGetProfileFeed$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/googleplus/command/SnsGpCmdGetProfileFeed;

    invoke-virtual {v4, p0}, Lcom/sec/android/app/sns3/agent/sp/googleplus/command/SnsGpCmdGetProfileFeed;->receive(Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;)V

    .line 95
    const/4 v4, 0x1

    return v4

    .line 87
    :cond_2
    iget-object v4, p0, Lcom/sec/android/app/sns3/agent/sp/googleplus/command/SnsGpCmdGetProfileFeed$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/googleplus/command/SnsGpCmdGetProfileFeed;

    new-instance v5, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;

    const-string v6, "googleplus"

    invoke-direct {v5, v6, p3, p4, p5}, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;-><init>(Ljava/lang/String;IILandroid/os/Bundle;)V

    invoke-virtual {v4, v5}, Lcom/sec/android/app/sns3/agent/sp/googleplus/command/SnsGpCmdGetProfileFeed;->setResponseList(Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;)Z

    .line 89
    iget-object v4, p0, Lcom/sec/android/app/sns3/agent/sp/googleplus/command/SnsGpCmdGetProfileFeed$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/googleplus/command/SnsGpCmdGetProfileFeed;

    invoke-virtual {v4, v7}, Lcom/sec/android/app/sns3/agent/sp/googleplus/command/SnsGpCmdGetProfileFeed;->setUri(Ljava/lang/String;)V

    goto :goto_1
.end method
