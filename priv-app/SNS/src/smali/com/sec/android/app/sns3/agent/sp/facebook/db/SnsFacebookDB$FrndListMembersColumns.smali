.class public interface abstract Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$FrndListMembersColumns;
.super Ljava/lang/Object;
.source "SnsFacebookDB.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "FrndListMembersColumns"
.end annotation


# static fields
.field public static final GROUP_ID:Ljava/lang/String; = "group_id"

.field public static final GROUP_NAME:Ljava/lang/String; = "group_name"

.field public static final GROUP_TYPE:Ljava/lang/String; = "group_type"

.field public static final MEMBER_ID:Ljava/lang/String; = "mem_id"

.field public static final MEMBER_NAME:Ljava/lang/String; = "mem_name"
