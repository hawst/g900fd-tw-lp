.class public Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserTags;
.super Ljava/lang/Object;
.source "SnsFbParserTags.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserTags$FacebookTags;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    return-void
.end method

.method public static parse(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTags;
    .locals 13
    .param p0, "contentObj"    # Ljava/lang/String;
    .param p1, "photoId"    # Ljava/lang/String;

    .prologue
    .line 50
    const/4 v9, 0x0

    .line 51
    .local v9, "tags":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTags;
    const/4 v0, 0x0

    .line 52
    .local v0, "curTags":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTags;
    const/4 v7, 0x0

    .line 55
    .local v7, "newTags":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTags;
    :try_start_0
    new-instance v6, Lorg/json/JSONObject;

    invoke-direct {v6, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 56
    .local v6, "jsonObject":Lorg/json/JSONObject;
    new-instance v3, Lorg/json/JSONArray;

    const-string v10, "data"

    invoke-virtual {v6, v10}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v3, v10}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 59
    .local v3, "jArrTags":Lorg/json/JSONArray;
    if-eqz v3, :cond_1

    .line 60
    const/4 v2, 0x0

    .local v2, "i":I
    move-object v8, v7

    .end local v7    # "newTags":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTags;
    .local v8, "newTags":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTags;
    :goto_0
    :try_start_1
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v10

    if-ge v2, v10, :cond_2

    .line 61
    invoke-virtual {v3, v2}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v5

    .line 63
    .local v5, "jsonObj":Lorg/json/JSONObject;
    new-instance v7, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTags;

    invoke-direct {v7}, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTags;-><init>()V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    .line 65
    .end local v8    # "newTags":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTags;
    .restart local v7    # "newTags":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTags;
    :try_start_2
    const-string v10, "id"

    invoke-virtual {v5, v10}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    iput-object v10, v7, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTags;->mTagID:Ljava/lang/String;

    .line 66
    const-string v10, "name"

    invoke-virtual {v5, v10}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    iput-object v10, v7, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTags;->mTagName:Ljava/lang/String;

    .line 67
    const-string v10, "x"

    invoke-virtual {v5, v10}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    iput-object v10, v7, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTags;->mTagX:Ljava/lang/String;

    .line 68
    const-string v10, "y"

    invoke-virtual {v5, v10}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    iput-object v10, v7, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTags;->mTagY:Ljava/lang/String;

    .line 69
    const-string v10, "created_time"

    invoke-virtual {v5, v10}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    iput-object v10, v7, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTags;->mCreatedTime:Ljava/lang/String;

    .line 70
    const-string v10, "type"

    invoke-virtual {v5, v10}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    iput-object v10, v7, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTags;->mType:Ljava/lang/String;

    .line 71
    iput-object p1, v7, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTags;->mPhotoId:Ljava/lang/String;

    .line 73
    if-nez v9, :cond_0

    .line 74
    move-object v9, v7

    .line 75
    move-object v0, v9

    .line 60
    :goto_1
    add-int/lit8 v2, v2, 0x1

    move-object v8, v7

    .end local v7    # "newTags":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTags;
    .restart local v8    # "newTags":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTags;
    goto :goto_0

    .line 77
    .end local v8    # "newTags":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTags;
    .restart local v7    # "newTags":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTags;
    :cond_0
    iput-object v7, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTags;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTags;

    .line 78
    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTags;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTags;
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_1

    .line 82
    .end local v2    # "i":I
    .end local v3    # "jArrTags":Lorg/json/JSONArray;
    .end local v5    # "jsonObj":Lorg/json/JSONObject;
    .end local v6    # "jsonObject":Lorg/json/JSONObject;
    :catch_0
    move-exception v4

    .line 83
    .local v4, "je":Lorg/json/JSONException;
    :goto_2
    const-string v10, "SNS"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "parser : JSONException error :"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v4}, Lorg/json/JSONException;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 88
    .end local v4    # "je":Lorg/json/JSONException;
    :cond_1
    :goto_3
    return-object v9

    .line 84
    :catch_1
    move-exception v1

    .line 85
    .local v1, "e":Ljava/lang/Exception;
    :goto_4
    const-string v10, "SNS"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "parser : Exception error : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 84
    .end local v1    # "e":Ljava/lang/Exception;
    .end local v7    # "newTags":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTags;
    .restart local v2    # "i":I
    .restart local v3    # "jArrTags":Lorg/json/JSONArray;
    .restart local v6    # "jsonObject":Lorg/json/JSONObject;
    .restart local v8    # "newTags":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTags;
    :catch_2
    move-exception v1

    move-object v7, v8

    .end local v8    # "newTags":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTags;
    .restart local v7    # "newTags":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTags;
    goto :goto_4

    .line 82
    .end local v7    # "newTags":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTags;
    .restart local v8    # "newTags":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTags;
    :catch_3
    move-exception v4

    move-object v7, v8

    .end local v8    # "newTags":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTags;
    .restart local v7    # "newTags":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTags;
    goto :goto_2

    .end local v7    # "newTags":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTags;
    .restart local v8    # "newTags":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTags;
    :cond_2
    move-object v7, v8

    .end local v8    # "newTags":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTags;
    .restart local v7    # "newTags":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTags;
    goto :goto_3
.end method
