.class public interface abstract Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserLatestPhoto$FacebookLatestPhotoPost;
.super Ljava/lang/Object;
.source "SnsFbParserLatestPhoto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserLatestPhoto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "FacebookLatestPhotoPost"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserLatestPhoto$FacebookLatestPhotoPost$SnsFbPhotoQuery;,
        Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserLatestPhoto$FacebookLatestPhotoPost$SnsFbStreamQuery;
    }
.end annotation


# static fields
.field public static final FQL_RESULT_SET:Ljava/lang/String; = "fql_result_set"

.field public static final PHTO_QUERY_NAME:Ljava/lang/String; = "photo_query"

.field public static final QUERY_NAME:Ljava/lang/String; = "name"

.field public static final STREAM_QUERY_NAME:Ljava/lang/String; = "stream_query"
