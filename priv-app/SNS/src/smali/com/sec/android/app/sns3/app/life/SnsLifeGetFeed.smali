.class public Lcom/sec/android/app/sns3/app/life/SnsLifeGetFeed;
.super Ljava/lang/Object;
.source "SnsLifeGetFeed.java"


# static fields
.field private static mCmdHandler:Landroid/os/Handler;

.field private static mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000(Landroid/content/Context;Ljava/lang/Boolean;)V
    .locals 0
    .param p0, "x0"    # Landroid/content/Context;
    .param p1, "x1"    # Ljava/lang/Boolean;

    .prologue
    .line 35
    invoke-static {p0, p1}, Lcom/sec/android/app/sns3/app/life/SnsLifeGetFeed;->invokeBroadcast(Landroid/content/Context;Ljava/lang/Boolean;)V

    return-void
.end method

.method private static invokeBroadcast(Landroid/content/Context;Ljava/lang/Boolean;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "bSuccess"    # Ljava/lang/Boolean;

    .prologue
    .line 72
    const/4 v1, 0x0

    .line 74
    .local v1, "intent":Landroid/content/Intent;
    const-string v0, "com.sec.android.app.sns.life.ACTION_LIFE_PROFILE_FEED_UPDATED"

    .line 76
    .local v0, "action":Ljava/lang/String;
    new-instance v1, Landroid/content/Intent;

    .end local v1    # "intent":Landroid/content/Intent;
    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 77
    .restart local v1    # "intent":Landroid/content/Intent;
    const-string v2, "SNS_RESULT"

    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 79
    const-string v2, "com.sec.android.app.sns3.permission.RECEIVE_SNS_BROADCAST"

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    .line 81
    return-void
.end method

.method public static loadSnsLifeGetFeed(Landroid/content/Context;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 42
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/sns3/SnsApplication;->getSvcMgr()Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v1

    sput-object v1, Lcom/sec/android/app/sns3/app/life/SnsLifeGetFeed;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    .line 43
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/sns3/SnsApplication;->getAgentMgr()Lcom/sec/android/app/sns3/agent/SnsAgentMgr;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/sns3/agent/SnsAgentMgr;->getCommandMgr()Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;->getCommandMgrHandle()Landroid/os/Handler;

    move-result-object v1

    sput-object v1, Lcom/sec/android/app/sns3/app/life/SnsLifeGetFeed;->mCmdHandler:Landroid/os/Handler;

    .line 46
    sget-object v1, Lcom/sec/android/app/sns3/app/life/SnsLifeGetFeed;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    if-nez v1, :cond_0

    .line 47
    sget-object v1, Lcom/sec/android/app/sns3/app/life/SnsLifeResource;->TAG:Ljava/lang/String;

    const-string v2, "[SNS] mSvcMgr is null !!"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 68
    :goto_0
    return-void

    .line 50
    :cond_0
    sget-object v1, Lcom/sec/android/app/sns3/app/life/SnsLifeGetFeed;->mCmdHandler:Landroid/os/Handler;

    if-nez v1, :cond_1

    .line 51
    sget-object v1, Lcom/sec/android/app/sns3/app/life/SnsLifeResource;->TAG:Ljava/lang/String;

    const-string v2, "[SNS] mCmdHandler is null !!"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 55
    :cond_1
    new-instance v0, Lcom/sec/android/app/sns3/agent/life/command/SnsLifeCmdGetFeed;

    sget-object v1, Lcom/sec/android/app/sns3/app/life/SnsLifeGetFeed;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    sget-object v2, Lcom/sec/android/app/sns3/app/life/SnsLifeGetFeed;->mCmdHandler:Landroid/os/Handler;

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/sns3/agent/life/command/SnsLifeCmdGetFeed;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Handler;)V

    .line 56
    .local v0, "cmd":Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;
    new-instance v1, Lcom/sec/android/app/sns3/app/life/SnsLifeGetFeed$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/sns3/app/life/SnsLifeGetFeed$1;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->setCommandCallback(Lcom/sec/android/app/sns3/agent/command/ISnsCommandCallback;)Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;

    .line 67
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->send()I

    goto :goto_0
.end method
