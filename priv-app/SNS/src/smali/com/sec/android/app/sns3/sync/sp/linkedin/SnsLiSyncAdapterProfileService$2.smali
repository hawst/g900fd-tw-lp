.class Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService$2;
.super Lcom/sec/android/app/sns3/svc/sp/linkedin/request/SnsLiReqGetUpdates;
.source "SnsLiSyncAdapterProfileService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;->performSync()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;


# direct methods
.method constructor <init>(Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 0
    .param p2, "x0"    # Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    .param p3, "x1"    # Ljava/lang/String;
    .param p4, "x2"    # Landroid/os/Bundle;

    .prologue
    .line 221
    iput-object p1, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService$2;->this$0:Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;

    invoke-direct {p0, p2, p3, p4}, Lcom/sec/android/app/sns3/svc/sp/linkedin/request/SnsLiReqGetUpdates;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;Landroid/os/Bundle;)V

    return-void
.end method


# virtual methods
.method public onReqRespond(IZIILandroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdates;)Z
    .locals 5
    .param p1, "reqID"    # I
    .param p2, "bSuccess"    # Z
    .param p3, "httpStatus"    # I
    .param p4, "errorCode"    # I
    .param p5, "reason"    # Landroid/os/Bundle;
    .param p6, "updates"    # Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdates;

    .prologue
    .line 226
    if-eqz p2, :cond_1

    .line 227
    iget-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService$2;->this$0:Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;

    # getter for: Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;
    invoke-static {v0}, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;->access$500(Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;)Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;

    move-result-object v0

    invoke-virtual {v0, p6}, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;->insertFeeds(Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdates;)V

    .line 228
    iget v0, p6, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdates;->mStart:I

    iget v1, p6, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdates;->mCount:I

    add-int/2addr v0, v1

    iget v1, p6, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdates;->mTotal:I

    if-ge v0, v1, :cond_0

    .line 229
    iget-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService$2;->this$0:Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;

    # getter for: Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;->mBundleUpdatesQuery:Landroid/os/Bundle;
    invoke-static {v0}, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;->access$400(Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;)Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "start"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p6, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdates;->mStart:I

    iget v4, p6, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdates;->mCount:I

    add-int/2addr v3, v4

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 241
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService$2;->this$0:Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;

    # getter for: Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;
    invoke-static {v0}, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;->access$500(Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;)Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;->resumeSync()V

    .line 242
    const/4 v0, 0x0

    return v0

    .line 235
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService$2;->this$0:Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;

    const/4 v1, 0x2

    # setter for: Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;->mSyncState:I
    invoke-static {v0, v1}, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;->access$702(Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;I)I

    .line 236
    iget-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService$2;->this$0:Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;->mBundleUpdatesQuery:Landroid/os/Bundle;
    invoke-static {v0, v1}, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;->access$402(Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;Landroid/os/Bundle;)Landroid/os/Bundle;

    goto :goto_0

    .line 239
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService$2;->this$0:Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;

    const/4 v1, -0x1

    # setter for: Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;->mSyncState:I
    invoke-static {v0, v1}, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;->access$702(Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;I)I

    goto :goto_0
.end method
