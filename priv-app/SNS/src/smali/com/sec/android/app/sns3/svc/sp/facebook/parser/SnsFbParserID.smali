.class public Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserID;
.super Ljava/lang/Object;
.source "SnsFbParserID.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserID$FacebookID;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    return-void
.end method

.method public static parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseID;
    .locals 4
    .param p0, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 36
    new-instance v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseID;

    invoke-direct {v1}, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseID;-><init>()V

    .line 38
    .local v1, "id":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseID;
    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 40
    .local v2, "jsonObject":Lorg/json/JSONObject;
    if-eqz v2, :cond_0

    .line 41
    const-string v3, "id"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseID;->mID:Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 48
    .end local v2    # "jsonObject":Lorg/json/JSONObject;
    :cond_0
    :goto_0
    return-object v1

    .line 43
    :catch_0
    move-exception v0

    .line 45
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method
