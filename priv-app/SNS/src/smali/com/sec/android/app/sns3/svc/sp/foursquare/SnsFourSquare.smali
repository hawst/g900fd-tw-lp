.class public Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFourSquare;
.super Ljava/lang/Object;
.source "SnsFourSquare.java"


# static fields
.field public static final ACCOUNT_TYPE:Ljava/lang/String; = "com.sec.android.app.sns3.foursquare"

.field public static final API_VERSION_DATE:Ljava/lang/String; = "20140731"

.field public static final CATEGORY_ICON_SIZE:Ljava/lang/String; = "bg_88"

.field public static final CLIENT_ID:Ljava/lang/String;

.field public static final CLIENT_SECRET:Ljava/lang/String;

.field public static final FOURSQUARE_ACCOUNT_TYPE:Ljava/lang/String; = "com.foursquare.android.auth.login"

.field public static final MARKET_URI:Ljava/lang/String; = "market://details"

.field public static final REDIRECT_URI:Ljava/lang/String; = "x-oauthflow-foursquare://callback"

.field public static final REST_URL:Ljava/lang/String; = "https://api.foursquare.com/v2/"

.field public static final SP:Ljava/lang/String; = "foursquare"

.field public static final USER_PHOTO_SIZE:Ljava/lang/String; = "100x100"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    invoke-static {}, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsAppIdManager;->getInstance()Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsAppIdManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsAppIdManager;->getClientId()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFourSquare;->CLIENT_ID:Ljava/lang/String;

    .line 31
    invoke-static {}, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsAppIdManager;->getInstance()Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsAppIdManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsAppIdManager;->getClientSecret()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFourSquare;->CLIENT_SECRET:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
