.class Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr$1;
.super Landroid/os/Handler;
.source "SnsRequestMgr.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;


# direct methods
.method constructor <init>(Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;)V
    .locals 0

    .prologue
    .line 75
    iput-object p1, p0, Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr$1;->this$0:Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 10
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v5, 0x0

    const/4 v2, 0x0

    .line 77
    const/4 v8, 0x0

    .line 78
    .local v8, "req":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    const/4 v9, 0x0

    .line 80
    .local v9, "result":Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 151
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr$1;->this$0:Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;

    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr$1;->this$0:Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;

    # invokes: Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;->getCurrentHttpClientState()I
    invoke-static {v1}, Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;->access$900(Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;)I

    move-result v1

    # setter for: Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;->mState:I
    invoke-static {v0, v1}, Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;->access$802(Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;I)I

    .line 153
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr$1;->this$0:Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;

    # getter for: Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;->mState:I
    invoke-static {v0}, Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;->access$800(Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;)I

    move-result v0

    packed-switch v0, :pswitch_data_1

    .line 168
    :cond_1
    :goto_1
    return-void

    .line 82
    :pswitch_0
    const-string v0, "SNS"

    const-string v1, "SnsRequestMgr : handleMessage() : MESSAGE_REQUEST"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 84
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    if-eqz v0, :cond_0

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    instance-of v0, v0, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;

    if-eqz v0, :cond_0

    .line 85
    iget-object v8, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .end local v8    # "req":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    check-cast v8, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;

    .line 87
    .restart local v8    # "req":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    invoke-virtual {v8}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->getCategory()I

    move-result v0

    const/16 v1, 0x14

    if-ne v0, v1, :cond_2

    .line 88
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr$1;->this$0:Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;

    # getter for: Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;->mAuthRequestMap:Ljava/util/Map;
    invoke-static {v0}, Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;->access$000(Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;)Ljava/util/Map;

    move-result-object v0

    invoke-virtual {v8}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->getReqID()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 93
    :goto_2
    const-string v0, "SNS"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SnsRequestMgr : handleMessage() - Insert request - mRequestMap ["

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v8}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->getCategory()I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "] count is "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr$1;->this$0:Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;

    # getter for: Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;->mRequestMap:Ljava/util/Map;
    invoke-static {v3}, Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;->access$200(Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;)Ljava/util/Map;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Map;->size()I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 98
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr$1;->this$0:Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;

    # invokes: Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;->getCurrentRequestCount()I
    invoke-static {v0}, Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;->access$300(Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;)I

    move-result v0

    const/16 v1, 0x32

    if-le v0, v1, :cond_0

    .line 99
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr$1;->this$0:Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;

    invoke-virtual {v8}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->getReqID()I

    move-result v1

    const/16 v4, 0x7d3

    move v3, v2

    move-object v6, v5

    # invokes: Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;->processError(IZIILandroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;)V
    invoke-static/range {v0 .. v6}, Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;->access$400(Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;IZIILandroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;)V

    goto :goto_1

    .line 89
    :cond_2
    invoke-virtual {v8}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->getCategory()I

    move-result v0

    const/16 v1, 0x15

    if-ne v0, v1, :cond_3

    .line 90
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr$1;->this$0:Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;

    # getter for: Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;->mPollingRequestMap:Ljava/util/Map;
    invoke-static {v0}, Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;->access$100(Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;)Ljava/util/Map;

    move-result-object v0

    invoke-virtual {v8}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->getReqID()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 92
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr$1;->this$0:Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;

    # getter for: Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;->mRequestMap:Ljava/util/Map;
    invoke-static {v0}, Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;->access$200(Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;)Ljava/util/Map;

    move-result-object v0

    invoke-virtual {v8}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->getReqID()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 108
    :pswitch_1
    const-string v0, "SNS"

    const-string v1, "SnsRequestMgr : handleMessage() : MESSAGE_RESPONSE"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 110
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    if-eqz v0, :cond_0

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    instance-of v0, v0, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;

    if-eqz v0, :cond_0

    .line 111
    iget-object v9, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .end local v9    # "result":Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;
    check-cast v9, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;

    .line 112
    .restart local v9    # "result":Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr$1;->this$0:Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;

    invoke-virtual {v9}, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->getReqID()I

    move-result v1

    # invokes: Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;->getRequest(I)Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    invoke-static {v0, v1}, Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;->access$500(Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;I)Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;

    move-result-object v8

    .line 114
    if-eqz v8, :cond_0

    .line 115
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr$1;->this$0:Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;

    # operator-- for: Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;->mSentReqCnt:I
    invoke-static {v0}, Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;->access$610(Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;)I

    .line 117
    invoke-virtual {v9}, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->getErrorCode()I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_4

    .line 118
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr$1;->this$0:Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;

    # invokes: Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;->processResponse(Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;)V
    invoke-static {v0, v9}, Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;->access$700(Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;)V

    goto/16 :goto_0

    .line 120
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr$1;->this$0:Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;

    invoke-virtual {v9}, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->getReqID()I

    move-result v1

    invoke-virtual {v9}, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->getHttpstatus()I

    move-result v3

    invoke-virtual {v9}, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->getErrorCode()I

    move-result v4

    invoke-virtual {v9}, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->getReason()Landroid/os/Bundle;

    move-result-object v5

    invoke-virtual {v9}, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->getResponse()Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;

    move-result-object v6

    # invokes: Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;->processError(IZIILandroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;)V
    invoke-static/range {v0 .. v6}, Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;->access$400(Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;IZIILandroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;)V

    goto/16 :goto_0

    .line 129
    :pswitch_2
    const-string v0, "SNS"

    const-string v1, "SnsRequestMgr : handleMessage() : MESSAGE_REQUEST_TIMEOUT"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 132
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr$1;->this$0:Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;

    # operator-- for: Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;->mSentReqCnt:I
    invoke-static {v0}, Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;->access$610(Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;)I

    .line 133
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr$1;->this$0:Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;

    iget v1, p1, Landroid/os/Message;->arg1:I

    const/16 v4, 0x7d4

    move v3, v2

    move-object v6, v5

    # invokes: Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;->processError(IZIILandroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;)V
    invoke-static/range {v0 .. v6}, Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;->access$400(Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;IZIILandroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;)V

    goto/16 :goto_0

    .line 138
    :pswitch_3
    const-string v0, "SNS"

    const-string v1, "SnsRequestMgr : handleMessage() : MESSAGE_REQUEST_NULL"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 140
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr$1;->this$0:Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;

    # operator-- for: Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;->mSentReqCnt:I
    invoke-static {v0}, Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;->access$610(Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;)I

    .line 141
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr$1;->this$0:Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;

    iget v1, p1, Landroid/os/Message;->arg1:I

    const/16 v4, 0x7d7

    move v3, v2

    move-object v6, v5

    # invokes: Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;->processError(IZIILandroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;)V
    invoke-static/range {v0 .. v6}, Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;->access$400(Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;IZIILandroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;)V

    goto/16 :goto_0

    .line 155
    :pswitch_4
    const-string v0, "SNS"

    const-string v1, "SnsRequestMgr : handleMessage() : STATE_FREE"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 156
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr$1;->this$0:Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;->getReservedRequest()Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;

    move-result-object v7

    .line 157
    .local v7, "curReq":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    if-eqz v7, :cond_1

    .line 158
    const/4 v0, 0x2

    invoke-virtual {v7, v0}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->setState(I)V

    .line 159
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr$1;->this$0:Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;

    # getter for: Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;->access$1000(Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v7, v0}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->process(Landroid/os/Handler;)Z

    .line 160
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr$1;->this$0:Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;

    # operator++ for: Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;->mSentReqCnt:I
    invoke-static {v0}, Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;->access$608(Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;)I

    goto/16 :goto_1

    .line 165
    .end local v7    # "curReq":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    :pswitch_5
    const-string v0, "SNS"

    const-string v1, "SnsRequestMgr : handleMessage() : STATE_LOCK"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 80
    nop

    :pswitch_data_0
    .packed-switch 0x14
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 153
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
