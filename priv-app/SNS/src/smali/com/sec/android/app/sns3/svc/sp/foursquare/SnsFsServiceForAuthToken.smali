.class public Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsServiceForAuthToken;
.super Landroid/app/Service;
.source "SnsFsServiceForAuthToken.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsServiceForAuthToken$SnsFsServiceBinder;
    }
.end annotation


# instance fields
.field private final mBinder:Landroid/os/IBinder;

.field private mHandler:Landroid/os/Handler;

.field private final mSnsSvcFoursquareForAuthTokenBinder:Lcom/sec/android/app/sns3/svc/sp/foursquare/auth/api/ISnsFourSquareForAuthToken$Stub;

.field private mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 45
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsServiceForAuthToken$SnsFsServiceBinder;

    invoke-direct {v0, p0}, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsServiceForAuthToken$SnsFsServiceBinder;-><init>(Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsServiceForAuthToken;)V

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsServiceForAuthToken;->mBinder:Landroid/os/IBinder;

    .line 55
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsServiceForAuthToken;->mHandler:Landroid/os/Handler;

    .line 83
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsServiceForAuthToken$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsServiceForAuthToken$1;-><init>(Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsServiceForAuthToken;)V

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsServiceForAuthToken;->mSnsSvcFoursquareForAuthTokenBinder:Lcom/sec/android/app/sns3/svc/sp/foursquare/auth/api/ISnsFourSquareForAuthToken$Stub;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsServiceForAuthToken;)Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsServiceForAuthToken;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsServiceForAuthToken;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsServiceForAuthToken;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsServiceForAuthToken;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsServiceForAuthToken;->mHandler:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 67
    const-string v0, "SNS"

    const-string v1, "SnsFsServiceForAuthToken : onBind()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 69
    const-class v0, Lcom/sec/android/app/sns3/svc/sp/foursquare/auth/api/ISnsFourSquareForAuthToken;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 70
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsServiceForAuthToken;->mSnsSvcFoursquareForAuthTokenBinder:Lcom/sec/android/app/sns3/svc/sp/foursquare/auth/api/ISnsFourSquareForAuthToken$Stub;

    .line 73
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsServiceForAuthToken;->mBinder:Landroid/os/IBinder;

    goto :goto_0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 59
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 60
    const-string v0, "SNS"

    const-string v1, "SnsFsServiceForAuthToken : onCreate()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 62
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/SnsApplication;->getSvcMgr()Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsServiceForAuthToken;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    .line 63
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 78
    const-string v0, "SNS"

    const-string v1, "SnsFsServiceForAuthToken : onStartCommand()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 80
    const/4 v0, 0x1

    return v0
.end method
