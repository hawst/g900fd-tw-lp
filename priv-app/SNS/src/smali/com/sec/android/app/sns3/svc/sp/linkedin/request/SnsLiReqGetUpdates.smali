.class public abstract Lcom/sec/android/app/sns3/svc/sp/linkedin/request/SnsLiReqGetUpdates;
.super Lcom/sec/android/app/sns3/svc/sp/linkedin/request/SnsLiReqBase;
.source "SnsLiReqGetUpdates.java"

# interfaces
.implements Lcom/sec/android/app/sns3/svc/sp/linkedin/callback/ISnsLiReqCbUpdates;


# static fields
.field public static final PARAM_AFTER:Ljava/lang/String; = "after"

.field public static final PARAM_COUNT:Ljava/lang/String; = "count"

.field public static final PARAM_SCOPE:Ljava/lang/String; = "scope"

.field public static final PARAM_SCOPE_VALUE_SELF:Ljava/lang/String; = "self"

.field public static final REST_URL_STATUS_STREAM:Ljava/lang/String; = "https://api.linkedin.com/v1/people/%1$s/network/updates?oauth2_access_token=%2$s"

.field private static TAG:Ljava/lang/String;

.field public static final UPDATE_TYPES_ALL:[Ljava/lang/String;

.field public static final UPDATE_TYPES_SELF:[Ljava/lang/String;

.field public static final UPDATE_TYPES_STREAM:[Ljava/lang/String;


# instance fields
.field private mBundle:Landroid/os/Bundle;

.field private mUpdateTypeArray:[Ljava/lang/String;

.field private mUserId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 44
    const-string v0, "SnsLiReqGetUpdates"

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/request/SnsLiReqGetUpdates;->TAG:Ljava/lang/String;

    .line 46
    const/16 v0, 0xc

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "APPS"

    aput-object v1, v0, v3

    const-string v1, "CMPY"

    aput-object v1, v0, v4

    const-string v1, "CONN"

    aput-object v1, v0, v5

    const/4 v1, 0x3

    const-string v2, "JOBS"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "JGRP"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "PICT"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "PRFX"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "RECU"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "PRFU"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "SHAR"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "STAT"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "VIRL"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/request/SnsLiReqGetUpdates;->UPDATE_TYPES_ALL:[Ljava/lang/String;

    .line 51
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "SHAR"

    aput-object v1, v0, v3

    const-string v1, "PRFU"

    aput-object v1, v0, v4

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/request/SnsLiReqGetUpdates;->UPDATE_TYPES_STREAM:[Ljava/lang/String;

    .line 55
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "SHAR"

    aput-object v1, v0, v3

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/request/SnsLiReqGetUpdates;->UPDATE_TYPES_SELF:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 1
    .param p1, "svcMgr"    # Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    .param p2, "userID"    # Ljava/lang/String;
    .param p3, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 71
    const/16 v0, 0x16

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/request/SnsLiReqBase;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;I)V

    .line 42
    const-string v0, "~"

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/request/SnsLiReqGetUpdates;->mUserId:Ljava/lang/String;

    .line 59
    sget-object v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/request/SnsLiReqGetUpdates;->UPDATE_TYPES_ALL:[Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/request/SnsLiReqGetUpdates;->mUpdateTypeArray:[Ljava/lang/String;

    .line 73
    iput-object p3, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/request/SnsLiReqGetUpdates;->mBundle:Landroid/os/Bundle;

    .line 74
    if-eqz p2, :cond_0

    .line 75
    iput-object p2, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/request/SnsLiReqGetUpdates;->mUserId:Ljava/lang/String;

    .line 78
    :cond_0
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;Landroid/os/Bundle;[Ljava/lang/String;)V
    .locals 0
    .param p1, "svcMgr"    # Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    .param p2, "userID"    # Ljava/lang/String;
    .param p3, "bundle"    # Landroid/os/Bundle;
    .param p4, "updateTypesArray"    # [Ljava/lang/String;

    .prologue
    .line 82
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/sns3/svc/sp/linkedin/request/SnsLiReqGetUpdates;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 83
    iput-object p4, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/request/SnsLiReqGetUpdates;->mUpdateTypeArray:[Ljava/lang/String;

    .line 85
    return-void
.end method


# virtual methods
.method protected compose()Lcom/sec/android/app/sns3/svc/http/SnsHttpRequest;
    .locals 13

    .prologue
    .line 90
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/request/SnsLiReqGetUpdates;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/SnsSvcMgr;->getTokenMgr()Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;

    move-result-object v0

    const-string v1, "linkedin"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;->getToken(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;

    move-result-object v9

    check-cast v9, Lcom/sec/android/app/sns3/svc/sp/linkedin/SnsLiToken;

    .line 92
    .local v9, "token":Lcom/sec/android/app/sns3/svc/sp/linkedin/SnsLiToken;
    const-string v2, "GET"

    .line 93
    .local v2, "method":Ljava/lang/String;
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    .line 94
    .local v11, "uri":Ljava/lang/StringBuilder;
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 95
    .local v4, "header":Landroid/os/Bundle;
    const/4 v5, 0x0

    .line 97
    .local v5, "body":Landroid/os/Bundle;
    const-string v0, "https://api.linkedin.com/v1/people/%1$s/network/updates?oauth2_access_token=%2$s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v12, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/request/SnsLiReqGetUpdates;->mUserId:Ljava/lang/String;

    aput-object v12, v1, v3

    const/4 v3, 0x1

    invoke-virtual {v9}, Lcom/sec/android/app/sns3/svc/sp/linkedin/SnsLiToken;->getAccessToken()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->encodeUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    aput-object v12, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 99
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/request/SnsLiReqGetUpdates;->mBundle:Landroid/os/Bundle;

    if-eqz v0, :cond_0

    .line 100
    const-string v0, "&"

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/request/SnsLiReqGetUpdates;->mBundle:Landroid/os/Bundle;

    invoke-static {v1}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->bundle2QueryString(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 102
    :cond_0
    iget-object v6, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/request/SnsLiReqGetUpdates;->mUpdateTypeArray:[Ljava/lang/String;

    .local v6, "arr$":[Ljava/lang/String;
    array-length v8, v6

    .local v8, "len$":I
    const/4 v7, 0x0

    .local v7, "i$":I
    :goto_0
    if-ge v7, v8, :cond_1

    aget-object v10, v6, v7

    .line 103
    .local v10, "type":Ljava/lang/String;
    const-string v0, "&type="

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 102
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 105
    .end local v10    # "type":Ljava/lang/String;
    :cond_1
    const-string v0, "x-li-format"

    const-string v1, "json"

    invoke-virtual {v4, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 107
    new-instance v0, Lcom/sec/android/app/sns3/svc/http/SnsHttpRequest;

    iget v1, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/request/SnsLiReqGetUpdates;->mReqID:I

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/sns3/svc/http/SnsHttpRequest;-><init>(ILjava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;)V

    return-object v0
.end method

.method protected parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;
    .locals 3
    .param p1, "content"    # Ljava/lang/String;

    .prologue
    .line 112
    const-string v1, "SNS"

    const-string v2, "LinkedIn  SnsLiReqGetProfile response SUCCESS!!"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 113
    invoke-super {p0, p1}, Lcom/sec/android/app/sns3/svc/sp/linkedin/request/SnsLiReqBase;->parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;

    move-result-object v0

    .line 115
    .local v0, "response":Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;
    if-eqz v0, :cond_0

    .line 118
    .end local v0    # "response":Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;
    :goto_0
    return-object v0

    .restart local v0    # "response":Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;
    :cond_0
    invoke-static {p1}, Lcom/sec/android/app/sns3/svc/sp/linkedin/parser/SnsLiParserUpdates;->parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdates;

    move-result-object v0

    goto :goto_0
.end method

.method protected respond(Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;)Z
    .locals 7
    .param p1, "result"    # Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;

    .prologue
    .line 123
    iget v1, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/request/SnsLiReqGetUpdates;->mReqID:I

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->isSuccess()Z

    move-result v2

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->getHttpstatus()I

    move-result v3

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->getErrorCode()I

    move-result v4

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->getReason()Landroid/os/Bundle;

    move-result-object v5

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->getResponse()Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdates;

    move-object v0, p0

    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/app/sns3/svc/sp/linkedin/request/SnsLiReqGetUpdates;->onReqRespond(IZIILandroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdates;)Z

    .line 126
    const/4 v0, 0x1

    return v0
.end method
