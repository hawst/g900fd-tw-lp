.class interface abstract Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseFollowingInfo$LinkedInFollowing;
.super Ljava/lang/Object;
.source "SnsLiResponseFollowingInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseFollowingInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x608
    name = "LinkedInFollowing"
.end annotation


# static fields
.field public static final COMPANIES:Ljava/lang/String; = "companies"

.field public static final FOLLOWING:Ljava/lang/String; = "following"

.field public static final FOLLOWING_ID:Ljava/lang/String; = "id"

.field public static final INDUSTRIES:Ljava/lang/String; = "industries"

.field public static final NAME:Ljava/lang/String; = "name"

.field public static final PEOPLE:Ljava/lang/String; = "people"

.field public static final SPECIAL_EDITIONS:Ljava/lang/String; = "specialEditions"

.field public static final TOTAL:Ljava/lang/String; = "_total"

.field public static final VALUES:Ljava/lang/String; = "values"
