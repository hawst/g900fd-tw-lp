.class public Lcom/sec/android/app/sns3/svc/sp/twitter/parser/SnsTwParserRateLimitStatus;
.super Ljava/lang/Object;
.source "SnsTwParserRateLimitStatus.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseRateLimitStatus;
    .locals 4
    .param p0, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 31
    const/4 v2, 0x0

    .line 33
    .local v2, "response":Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseRateLimitStatus;
    invoke-static {p0}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->isJsonEmpty(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 34
    const/4 v3, 0x0

    .line 50
    :goto_0
    return-object v3

    .line 37
    :cond_0
    new-instance v2, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseRateLimitStatus;

    .end local v2    # "response":Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseRateLimitStatus;
    invoke-direct {v2}, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseRateLimitStatus;-><init>()V

    .line 39
    .restart local v2    # "response":Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseRateLimitStatus;
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 41
    .local v1, "jsonObj":Lorg/json/JSONObject;
    const-string v3, "remaining_hits"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v3

    iput v3, v2, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseRateLimitStatus;->mRemainingHits:I

    .line 42
    const-string v3, "reset_time_in_seconds"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v3

    iput v3, v2, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseRateLimitStatus;->mResetTimeInSeconds:I

    .line 43
    const-string v3, "hourly_limit"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v3

    iput v3, v2, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseRateLimitStatus;->mHourlyLimit:I

    .line 44
    const-string v3, "reset_time"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseRateLimitStatus;->mResetTime:Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .end local v1    # "jsonObj":Lorg/json/JSONObject;
    :goto_1
    move-object v3, v2

    .line 50
    goto :goto_0

    .line 46
    :catch_0
    move-exception v0

    .line 47
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_1
.end method
