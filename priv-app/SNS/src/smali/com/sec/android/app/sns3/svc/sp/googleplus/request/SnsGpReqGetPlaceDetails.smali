.class public abstract Lcom/sec/android/app/sns3/svc/sp/googleplus/request/SnsGpReqGetPlaceDetails;
.super Lcom/sec/android/app/sns3/svc/sp/googleplus/request/SnsGpReqBase;
.source "SnsGpReqGetPlaceDetails.java"

# interfaces
.implements Lcom/sec/android/app/sns3/svc/sp/googleplus/callback/ISnsGpReqCbPlaceDetails;


# static fields
.field public static final API_KEY:Ljava/lang/String; = "AIzaSyDxDJx3nERwrHj2mldW77SvHVv5Vane-Y8"

.field public static final REST_URL_PLACE_DETAILS:Ljava/lang/String; = "https://maps.googleapis.com/maps/api/place/details/json?"


# instance fields
.field public mQueryString:Ljava/lang/StringBuilder;

.field private mReference:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Bundle;)V
    .locals 4
    .param p1, "svcMgr"    # Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    .param p2, "bundle"    # Landroid/os/Bundle;

    .prologue
    const/4 v3, 0x0

    .line 51
    const/16 v2, 0x16

    invoke-direct {p0, p1, v2}, Lcom/sec/android/app/sns3/svc/sp/googleplus/request/SnsGpReqBase;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;I)V

    .line 46
    iput-object v3, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/request/SnsGpReqGetPlaceDetails;->mReference:Ljava/lang/String;

    .line 48
    iput-object v3, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/request/SnsGpReqGetPlaceDetails;->mQueryString:Ljava/lang/StringBuilder;

    .line 52
    if-eqz p2, :cond_2

    .line 54
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "key="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iput-object v2, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/request/SnsGpReqGetPlaceDetails;->mQueryString:Ljava/lang/StringBuilder;

    .line 55
    iget-object v2, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/request/SnsGpReqGetPlaceDetails;->mQueryString:Ljava/lang/StringBuilder;

    const-string v3, "AIzaSyDxDJx3nERwrHj2mldW77SvHVv5Vane-Y8"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 59
    const-string v2, "reference"

    invoke-virtual {p2, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/request/SnsGpReqGetPlaceDetails;->mReference:Ljava/lang/String;

    .line 60
    iget-object v2, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/request/SnsGpReqGetPlaceDetails;->mReference:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 61
    iget-object v2, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/request/SnsGpReqGetPlaceDetails;->mQueryString:Ljava/lang/StringBuilder;

    const-string v3, "&reference="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/request/SnsGpReqGetPlaceDetails;->mReference:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 62
    const-string v2, "reference"

    invoke-virtual {p2, v2}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 66
    invoke-virtual {p2}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 67
    .local v1, "key":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/request/SnsGpReqGetPlaceDetails;->mQueryString:Ljava/lang/StringBuilder;

    const-string v3, "&"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->encodeUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 64
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "key":Ljava/lang/String;
    :cond_0
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Must provide reference"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 70
    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/request/SnsGpReqGetPlaceDetails;->mQueryString:Ljava/lang/StringBuilder;

    const-string v3, "&sensor=true"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 73
    .end local v0    # "i$":Ljava/util/Iterator;
    :cond_2
    return-void
.end method


# virtual methods
.method protected compose()Lcom/sec/android/app/sns3/svc/http/SnsHttpRequest;
    .locals 6

    .prologue
    .line 78
    const-string v2, "GET"

    .line 79
    .local v2, "method":Ljava/lang/String;
    const/4 v3, 0x0

    .line 80
    .local v3, "uri":Ljava/lang/String;
    const/4 v4, 0x0

    .line 81
    .local v4, "header":Landroid/os/Bundle;
    const/4 v5, 0x0

    .line 83
    .local v5, "body":Landroid/os/Bundle;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "https://maps.googleapis.com/maps/api/place/details/json?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/request/SnsGpReqGetPlaceDetails;->mQueryString:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 84
    new-instance v0, Lcom/sec/android/app/sns3/svc/http/SnsHttpRequest;

    iget v1, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/request/SnsGpReqGetPlaceDetails;->mReqID:I

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/sns3/svc/http/SnsHttpRequest;-><init>(ILjava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;)V

    return-object v0
.end method

.method protected parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;
    .locals 4
    .param p1, "content"    # Ljava/lang/String;

    .prologue
    .line 89
    const-string v1, "SNS"

    const-string v2, "GooglePlus  SnsGpReqGetNearby response SUCCESS!!"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 90
    const-string v1, "SNS"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "response: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 91
    invoke-super {p0, p1}, Lcom/sec/android/app/sns3/svc/sp/googleplus/request/SnsGpReqBase;->parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;

    move-result-object v0

    .line 93
    .local v0, "response":Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;
    if-eqz v0, :cond_0

    .line 96
    .end local v0    # "response":Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;
    :goto_0
    return-object v0

    .restart local v0    # "response":Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;
    :cond_0
    invoke-static {p1}, Lcom/sec/android/app/sns3/svc/sp/googleplus/parser/SnsGpParserNearby;->parsePlaceDetails(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseNearbyItem;

    move-result-object v0

    goto :goto_0
.end method

.method protected respond(Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;)Z
    .locals 7
    .param p1, "result"    # Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;

    .prologue
    .line 101
    iget v1, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/request/SnsGpReqGetPlaceDetails;->mReqID:I

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->isSuccess()Z

    move-result v2

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->getHttpstatus()I

    move-result v3

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->getErrorCode()I

    move-result v4

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->getReason()Landroid/os/Bundle;

    move-result-object v5

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->getResponse()Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseNearbyItem;

    move-object v0, p0

    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/app/sns3/svc/sp/googleplus/request/SnsGpReqGetPlaceDetails;->onReqRespond(IZIILandroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseNearbyItem;)Z

    .line 103
    const/4 v0, 0x1

    return v0
.end method
