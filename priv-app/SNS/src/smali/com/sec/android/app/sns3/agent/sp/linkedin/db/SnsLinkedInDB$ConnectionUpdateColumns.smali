.class public interface abstract Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$ConnectionUpdateColumns;
.super Ljava/lang/Object;
.source "SnsLinkedInDB.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ConnectionUpdateColumns"
.end annotation


# static fields
.field public static final CONNECTION_UPDATE_KEY:Ljava/lang/String; = "connection_update_key"

.field public static final NEW_CONNECTION_ID:Ljava/lang/String; = "new_connection_id"
