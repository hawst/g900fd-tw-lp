.class Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInAuthActivity$4;
.super Landroid/webkit/WebViewClient;
.source "SnsAccountInAuthActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInAuthActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInAuthActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInAuthActivity;)V
    .locals 0

    .prologue
    .line 232
    iput-object p1, p0, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInAuthActivity$4;->this$0:Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInAuthActivity;

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "errorCode"    # I
    .param p3, "description"    # Ljava/lang/String;
    .param p4, "failingUrl"    # Ljava/lang/String;

    .prologue
    .line 349
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInAuthActivity$4;->this$0:Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInAuthActivity;

    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInAuthActivity$4;->this$0:Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInAuthActivity;

    # invokes: Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInAuthActivity;->handleError(Landroid/content/Context;I)V
    invoke-static {v0, v1, p2}, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInAuthActivity;->access$500(Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInAuthActivity;Landroid/content/Context;I)V

    .line 350
    invoke-super {p0, p1, p2, p3, p4}, Landroid/webkit/WebViewClient;->onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V

    .line 351
    return-void
.end method

.method public shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 17
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "url"    # Ljava/lang/String;

    .prologue
    .line 236
    invoke-static {}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->isLoggable()Z

    move-result v13

    if-eqz v13, :cond_0

    .line 237
    # getter for: Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInAuthActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInAuthActivity;->access$000()Ljava/lang/String;

    move-result-object v13

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "####### WebView shouldOverrideUrlLoading URL : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p2

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 240
    :cond_0
    if-eqz p2, :cond_2

    const-string v13, "http://localhost/success/?error=access_denied"

    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_2

    .line 241
    invoke-virtual/range {p1 .. p1}, Landroid/webkit/WebView;->stopLoading()V

    .line 242
    invoke-virtual/range {p1 .. p1}, Landroid/webkit/WebView;->clearView()V

    .line 244
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInAuthActivity$4;->this$0:Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInAuthActivity;

    invoke-virtual {v13}, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInAuthActivity;->finish()V

    .line 343
    :cond_1
    :goto_0
    const/4 v13, 0x0

    :goto_1
    return v13

    .line 246
    :cond_2
    if-eqz p2, :cond_a

    const-string v13, "http://localhost/success"

    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_a

    .line 247
    invoke-virtual/range {p1 .. p1}, Landroid/webkit/WebView;->stopLoading()V

    .line 248
    invoke-virtual/range {p1 .. p1}, Landroid/webkit/WebView;->clearView()V

    .line 251
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInAuthActivity$4;->this$0:Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInAuthActivity;

    new-instance v14, Landroid/webkit/WebView;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInAuthActivity$4;->this$0:Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInAuthActivity;

    # getter for: Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInAuthActivity;->mAppContext:Landroid/content/Context;
    invoke-static {v15}, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInAuthActivity;->access$300(Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInAuthActivity;)Landroid/content/Context;

    move-result-object v15

    invoke-direct {v14, v15}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;)V

    invoke-virtual {v13, v14}, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInAuthActivity;->setContentView(Landroid/view/View;)V

    .line 253
    const-string v13, "http://localhost/success?"

    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v13

    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v11

    .line 254
    .local v11, "subUrl":Ljava/lang/String;
    const-string v13, "#"

    invoke-virtual {v11, v13}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    .line 255
    .local v9, "splitUrl":[Ljava/lang/String;
    const/4 v10, 0x0

    .line 256
    .local v10, "state":Ljava/lang/String;
    const/4 v1, 0x0

    .line 258
    .local v1, "accessToken":Ljava/lang/String;
    move-object v4, v9

    .local v4, "arr$":[Ljava/lang/String;
    array-length v7, v4

    .local v7, "len$":I
    const/4 v6, 0x0

    .local v6, "i$":I
    :goto_2
    if-ge v6, v7, :cond_5

    aget-object v8, v4, v6

    .line 259
    .local v8, "parameter":Ljava/lang/String;
    const-string v13, "="

    invoke-virtual {v8, v13}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v12

    .line 260
    .local v12, "v":[Ljava/lang/String;
    const/4 v13, 0x0

    aget-object v13, v12, v13

    invoke-static {v13}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    const-string v14, "state"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_4

    .line 261
    const/4 v13, 0x1

    aget-object v13, v12, v13

    invoke-static {v13}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 258
    :cond_3
    :goto_3
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    .line 263
    :cond_4
    const/4 v13, 0x0

    aget-object v13, v12, v13

    invoke-static {v13}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    const-string v14, "access_token"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_3

    .line 264
    const/4 v13, 0x1

    aget-object v13, v12, v13

    invoke-static {v13}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_3

    .line 268
    .end local v8    # "parameter":Ljava/lang/String;
    .end local v12    # "v":[Ljava/lang/String;
    :cond_5
    invoke-static {}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->isLoggable()Z

    move-result v13

    if-eqz v13, :cond_6

    .line 269
    # getter for: Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInAuthActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInAuthActivity;->access$000()Ljava/lang/String;

    move-result-object v13

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "SnsAccountInAuthActivity : shouldOverrideUrlLoading() - state = "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ", Access token = "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 274
    :cond_6
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInAuthActivity$4;->this$0:Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInAuthActivity;

    # setter for: Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInAuthActivity;->mAccessToken:Ljava/lang/String;
    invoke-static {v13, v1}, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInAuthActivity;->access$202(Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInAuthActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 276
    if-eqz v1, :cond_9

    .line 278
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInAuthActivity$4;->this$0:Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInAuthActivity;

    # getter for: Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInAuthActivity;->mRetryLogin:Z
    invoke-static {v13}, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInAuthActivity;->access$100(Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInAuthActivity;)Z

    move-result v13

    if-eqz v13, :cond_8

    .line 279
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInAuthActivity$4;->this$0:Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInAuthActivity;

    # setter for: Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInAuthActivity;->mAccessToken:Ljava/lang/String;
    invoke-static {v13, v1}, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInAuthActivity;->access$202(Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInAuthActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 281
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInAuthActivity$4;->this$0:Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInAuthActivity;

    # getter for: Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInAuthActivity;->mAppContext:Landroid/content/Context;
    invoke-static {v13}, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInAuthActivity;->access$300(Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInAuthActivity;)Landroid/content/Context;

    move-result-object v13

    invoke-static {v13}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v13

    const-string v14, "com.sec.android.app.sns3.instagram"

    invoke-virtual {v13, v14}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v2

    .line 283
    .local v2, "accounts":[Landroid/accounts/Account;
    if-eqz v2, :cond_7

    array-length v13, v2

    if-lez v13, :cond_7

    .line 284
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInAuthActivity$4;->this$0:Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInAuthActivity;

    # getter for: Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInAuthActivity;->mAppContext:Landroid/content/Context;
    invoke-static {v13}, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInAuthActivity;->access$300(Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInAuthActivity;)Landroid/content/Context;

    move-result-object v13

    invoke-static {v13}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v13

    const/4 v14, 0x0

    aget-object v14, v2, v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInAuthActivity$4;->this$0:Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInAuthActivity;

    iget-object v15, v15, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInAuthActivity;->mAccountManagerRemoveCallback:Landroid/accounts/AccountManagerCallback;

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Landroid/accounts/AccountManager;->removeAccount(Landroid/accounts/Account;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    .line 287
    :cond_7
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInAuthActivity$4;->this$0:Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInAuthActivity;

    const/4 v14, 0x0

    # setter for: Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInAuthActivity;->mRetryLogin:Z
    invoke-static {v13, v14}, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInAuthActivity;->access$102(Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInAuthActivity;Z)Z

    goto/16 :goto_0

    .line 290
    .end local v2    # "accounts":[Landroid/accounts/Account;
    :cond_8
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInAuthActivity$4;->this$0:Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInAuthActivity;

    invoke-virtual {v13, v1}, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInAuthActivity;->setAuthTokenNExpires(Ljava/lang/String;)Z

    .line 292
    new-instance v5, Lcom/sec/android/app/sns3/agent/sp/instagram/command/SnsInCmdAuthLogin;

    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v13

    invoke-virtual {v13}, Lcom/sec/android/app/sns3/SnsApplication;->getSvcMgr()Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v13

    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v14

    invoke-virtual {v14}, Lcom/sec/android/app/sns3/SnsApplication;->getAgentMgr()Lcom/sec/android/app/sns3/agent/SnsAgentMgr;

    move-result-object v14

    invoke-virtual {v14}, Lcom/sec/android/app/sns3/agent/SnsAgentMgr;->getCommandMgr()Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;

    move-result-object v14

    invoke-virtual {v14}, Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;->getCommandMgrHandle()Landroid/os/Handler;

    move-result-object v14

    const/4 v15, 0x0

    invoke-direct {v5, v13, v14, v15}, Lcom/sec/android/app/sns3/agent/sp/instagram/command/SnsInCmdAuthLogin;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Handler;Landroid/os/Bundle;)V

    .line 296
    .local v5, "cmd":Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;
    new-instance v13, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInAuthActivity$4$1;

    move-object/from16 v0, p0

    invoke-direct {v13, v0}, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInAuthActivity$4$1;-><init>(Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInAuthActivity$4;)V

    invoke-virtual {v5, v13}, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->setCommandCallback(Lcom/sec/android/app/sns3/agent/command/ISnsCommandCallback;)Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;

    .line 325
    invoke-virtual {v5}, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->send()I

    goto/16 :goto_0

    .line 330
    .end local v5    # "cmd":Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;
    :cond_9
    # getter for: Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInAuthActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInAuthActivity;->access$000()Ljava/lang/String;

    move-result-object v13

    const-string v14, "SnsAccountInAuthActivity - access token is null!!"

    invoke-static {v13, v14}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 332
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInAuthActivity$4;->this$0:Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInAuthActivity;

    const/4 v14, -0x1

    const/4 v15, -0x1

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInAuthActivity;->loginFail(IILjava/lang/String;)V

    goto/16 :goto_0

    .line 335
    .end local v1    # "accessToken":Ljava/lang/String;
    .end local v4    # "arr$":[Ljava/lang/String;
    .end local v6    # "i$":I
    .end local v7    # "len$":I
    .end local v9    # "splitUrl":[Ljava/lang/String;
    .end local v10    # "state":Ljava/lang/String;
    .end local v11    # "subUrl":Ljava/lang/String;
    :cond_a
    if-eqz p2, :cond_1

    const-string v13, "http://localhost/cancel"

    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_1

    .line 336
    new-instance v3, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInAuthActivity$UIDialogThreadRunnable;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInAuthActivity$4;->this$0:Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInAuthActivity;

    # getter for: Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInAuthActivity;->mAppContext:Landroid/content/Context;
    invoke-static {v13}, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInAuthActivity;->access$300(Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInAuthActivity;)Landroid/content/Context;

    move-result-object v13

    const/4 v14, 0x0

    invoke-direct {v3, v13, v14}, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInAuthActivity$UIDialogThreadRunnable;-><init>(Landroid/content/Context;I)V

    .line 338
    .local v3, "another":Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInAuthActivity$UIDialogThreadRunnable;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInAuthActivity$4;->this$0:Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInAuthActivity;

    # getter for: Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInAuthActivity;->mHandler:Landroid/os/Handler;
    invoke-static {v13}, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInAuthActivity;->access$400(Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInAuthActivity;)Landroid/os/Handler;

    move-result-object v13

    invoke-virtual {v13, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 340
    const/4 v13, 0x1

    goto/16 :goto_1
.end method
