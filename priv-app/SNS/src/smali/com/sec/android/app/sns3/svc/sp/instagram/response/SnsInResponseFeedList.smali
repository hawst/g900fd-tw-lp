.class public final Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseFeedList;
.super Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;
.source "SnsInResponseFeedList.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseFeedList;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field mFeeds:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseFeed;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 51
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseFeedList$1;

    invoke-direct {v0}, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseFeedList$1;-><init>()V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseFeedList;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;-><init>()V

    .line 40
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseFeedList;->mFeeds:Ljava/util/List;

    .line 41
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;-><init>()V

    .line 62
    invoke-virtual {p0, p1}, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseFeedList;->readFromParcel(Landroid/os/Parcel;)V

    .line 63
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseFeedList$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseFeedList$1;

    .prologue
    .line 29
    invoke-direct {p0, p1}, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseFeedList;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseFeed;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 34
    .local p1, "mFeeds":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseFeed;>;"
    invoke-direct {p0}, Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseFeedList;->mFeeds:Ljava/util/List;

    .line 36
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 80
    const/4 v0, 0x0

    return v0
.end method

.method public getFeeds()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseFeed;",
            ">;"
        }
    .end annotation

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseFeedList;->mFeeds:Ljava/util/List;

    return-object v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 73
    const-class v0, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseFeed;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseFeedList;->mFeeds:Ljava/util/List;

    .line 75
    return-void
.end method

.method public setFeeds(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseFeed;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 48
    .local p1, "mFeeds":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseFeed;>;"
    iput-object p1, p0, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseFeedList;->mFeeds:Ljava/util/List;

    .line 49
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flag"    # I

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseFeedList;->mFeeds:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 69
    return-void
.end method
