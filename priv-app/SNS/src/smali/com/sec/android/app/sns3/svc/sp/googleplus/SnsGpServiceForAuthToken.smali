.class public Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpServiceForAuthToken;
.super Landroid/app/Service;
.source "SnsGpServiceForAuthToken.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpServiceForAuthToken$SnsGpServiceBinder;
    }
.end annotation


# instance fields
.field private final mBinder:Landroid/os/IBinder;

.field private mHandler:Landroid/os/Handler;

.field private final mSnsSvcGooglePlusForAuthTokenBinder:Lcom/sec/android/app/sns3/svc/sp/googleplus/auth/api/ISnsGooglePlusForAuthToken$Stub;

.field private mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 44
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpServiceForAuthToken$SnsGpServiceBinder;

    invoke-direct {v0, p0}, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpServiceForAuthToken$SnsGpServiceBinder;-><init>(Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpServiceForAuthToken;)V

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpServiceForAuthToken;->mBinder:Landroid/os/IBinder;

    .line 54
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpServiceForAuthToken;->mHandler:Landroid/os/Handler;

    .line 82
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpServiceForAuthToken$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpServiceForAuthToken$1;-><init>(Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpServiceForAuthToken;)V

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpServiceForAuthToken;->mSnsSvcGooglePlusForAuthTokenBinder:Lcom/sec/android/app/sns3/svc/sp/googleplus/auth/api/ISnsGooglePlusForAuthToken$Stub;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpServiceForAuthToken;)Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpServiceForAuthToken;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpServiceForAuthToken;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpServiceForAuthToken;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpServiceForAuthToken;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpServiceForAuthToken;->mHandler:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 66
    const-string v0, "SNS"

    const-string v1, "SnsGpServiceForAuthToken : onBind()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 68
    const-class v0, Lcom/sec/android/app/sns3/svc/sp/googleplus/auth/api/ISnsGooglePlusForAuthToken;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 69
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpServiceForAuthToken;->mSnsSvcGooglePlusForAuthTokenBinder:Lcom/sec/android/app/sns3/svc/sp/googleplus/auth/api/ISnsGooglePlusForAuthToken$Stub;

    .line 72
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpServiceForAuthToken;->mBinder:Landroid/os/IBinder;

    goto :goto_0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 58
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 59
    const-string v0, "SNS"

    const-string v1, "SnsGpServiceForAuthToken : onCreate()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 61
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/SnsApplication;->getSvcMgr()Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpServiceForAuthToken;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    .line 62
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 77
    const-string v0, "SNS"

    const-string v1, "SnsGpServiceForAuthToken : onStartCommand()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 79
    const/4 v0, 0x1

    return v0
.end method
