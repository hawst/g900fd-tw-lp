.class public Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuth;
.super Ljava/lang/Object;
.source "SnsAccountGpAuth.java"


# static fields
.field public static final ACTION_SNS_GOOGLEPLUS_LOGGED_IN:Ljava/lang/String; = "com.sec.android.app.sns.ACTION_SNS_GOOGLEPLUS_LOGGED_IN"

.field public static final ACTION_SNS_GOOGLEPLUS_LOGGED_OUT:Ljava/lang/String; = "com.sec.android.app.sns.ACTION_SNS_GOOGLEPLUS_LOGGED_OUT"

.field public static final CALL_PREFERENCE_KEY:Ljava/lang/String; = "GP_call_sync_interval"

.field public static final EXTRA_SKIP_SYNC_SETUP:Ljava/lang/String; = "skip_sync_setup"

.field public static final GOOGLEPLUS_SSO_NOTIFICATION_ID:I = 0x13ec

.field public static final LOGIN_GOOGLEPLUS_NOTIFICATION_ID:I = 0x1450

.field public static final LOGIN_REQUEST_CODE:I = 0x1388

.field public static final MARKET_APP_SSO:Z = true

.field public static final PROFILE_FEED_PREFERENCE_KEY:Ljava/lang/String; = "GP_profile_feed_sync_interval"

.field public static final RETRY_LOGIN_NOTIFICATION_ID:I = 0x14b4

.field public static final SCOPES:Ljava/lang/String; = "oauth2:https://www.googleapis.com/auth/plus.login https://www.googleapis.com/auth/plus.me https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/userinfo.profile"

.field public static final SNS_BROADCAST_PERMISSION:Ljava/lang/String; = "com.sec.android.app.sns3.permission.RECEIVE_SNS_BROADCAST"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
