.class public Lcom/sec/android/app/sns3/svc/sp/twitter/parser/SnsTwParserTrendDate;
.super Ljava/lang/Object;
.source "SnsTwParserTrendDate.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseTrendDate;
    .locals 9
    .param p0, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 34
    const/4 v6, 0x0

    .line 36
    .local v6, "response":Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseTrendDate;
    invoke-static {p0}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->isJsonEmpty(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 37
    const/4 v7, 0x0

    .line 70
    :goto_0
    return-object v7

    .line 41
    :cond_0
    :try_start_0
    invoke-static {p0}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->isJsonArray(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 43
    new-instance v3, Lorg/json/JSONArray;

    invoke-direct {v3, p0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 45
    .local v3, "jsonArray":Lorg/json/JSONArray;
    const/4 v0, 0x0

    .line 46
    .local v0, "curNode":Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseTrendDate;
    const/4 v5, 0x0

    .line 48
    .local v5, "newNode":Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseTrendDate;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v7

    if-ge v2, v7, :cond_3

    .line 49
    invoke-virtual {v3, v2}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/app/sns3/svc/sp/twitter/parser/SnsTwParserTrendDate;->parseTrendDate(Lorg/json/JSONObject;)Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseTrendDate;

    move-result-object v5

    .line 51
    if-nez v6, :cond_1

    .line 52
    move-object v6, v5

    .line 53
    move-object v0, v6

    .line 48
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 55
    :cond_1
    iput-object v5, v0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseTrendDate;->mNext:Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseTrendDate;

    .line 56
    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseTrendDate;->mNext:Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseTrendDate;

    goto :goto_2

    .line 61
    .end local v0    # "curNode":Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseTrendDate;
    .end local v2    # "i":I
    .end local v3    # "jsonArray":Lorg/json/JSONArray;
    .end local v5    # "newNode":Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseTrendDate;
    :cond_2
    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 62
    .local v4, "jsonObj":Lorg/json/JSONObject;
    invoke-static {v4}, Lcom/sec/android/app/sns3/svc/sp/twitter/parser/SnsTwParserTrendDate;->parseTrendDate(Lorg/json/JSONObject;)Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseTrendDate;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .end local v4    # "jsonObj":Lorg/json/JSONObject;
    :cond_3
    :goto_3
    move-object v7, v6

    .line 70
    goto :goto_0

    .line 65
    :catch_0
    move-exception v1

    .line 66
    .local v1, "e":Lorg/json/JSONException;
    const-string v7, "SNS"

    invoke-virtual {v1}, Lorg/json/JSONException;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 67
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_3
.end method

.method private static parseTrendDate(Lorg/json/JSONObject;)Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseTrendDate;
    .locals 5
    .param p0, "jsonObj"    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 74
    new-instance v2, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseTrendDate;

    invoke-direct {v2}, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseTrendDate;-><init>()V

    .line 76
    .local v2, "response":Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseTrendDate;
    invoke-virtual {p0}, Lorg/json/JSONObject;->names()Lorg/json/JSONArray;

    move-result-object v1

    .line 77
    .local v1, "names":Lorg/json/JSONArray;
    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v3

    new-array v3, v3, [Ljava/lang/String;

    iput-object v3, v2, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseTrendDate;->mDateArray:[Ljava/lang/String;

    .line 79
    const/4 v0, 0x0

    .local v0, "mDateCnt":I
    :goto_0
    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-ge v0, v3, :cond_0

    .line 81
    iget-object v3, v2, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseTrendDate;->mDateArray:[Ljava/lang/String;

    invoke-virtual {v1, v0}, Lorg/json/JSONArray;->optString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v0

    .line 82
    invoke-virtual {v1, v0}, Lorg/json/JSONArray;->optString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/parser/SnsTwParserTrendDateValue;->parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseTrendDateValue;

    move-result-object v3

    iput-object v3, v2, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseTrendDate;->mTrendDateValue:Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseTrendDateValue;

    .line 79
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 85
    :cond_0
    return-object v2
.end method
