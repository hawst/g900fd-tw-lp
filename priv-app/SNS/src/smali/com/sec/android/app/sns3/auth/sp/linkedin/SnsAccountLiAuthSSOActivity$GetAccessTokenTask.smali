.class Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity$GetAccessTokenTask;
.super Landroid/os/AsyncTask;
.source "SnsAccountLiAuthSSOActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GetAccessTokenTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# static fields
.field private static final ACCESS_TOKEN:Ljava/lang/String; = "access_token"

.field private static final ERROR_MSG:Ljava/lang/String; = "error_msg"

.field private static final EXPIRES_IN:Ljava/lang/String; = "expires_in"


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;)V
    .locals 0

    .prologue
    .line 469
    iput-object p1, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity$GetAccessTokenTask;->this$0:Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;
    .param p2, "x1"    # Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity$1;

    .prologue
    .line 469
    invoke-direct {p0, p1}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity$GetAccessTokenTask;-><init>(Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;)V

    return-void
.end method

.method static synthetic access$1300(Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity$GetAccessTokenTask;J)V
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity$GetAccessTokenTask;
    .param p1, "x1"    # J

    .prologue
    .line 469
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity$GetAccessTokenTask;->pauseThread(J)V

    return-void
.end method

.method private pauseThread(J)V
    .locals 9
    .param p1, "startTime"    # J

    .prologue
    const-wide/16 v6, 0xbb8

    .line 606
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long v2, v4, p1

    .line 607
    .local v2, "passedTime":J
    cmp-long v1, v2, v6

    if-gez v1, :cond_0

    .line 609
    sub-long v4, v6, v2

    :try_start_0
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 614
    :cond_0
    :goto_0
    return-void

    .line 610
    :catch_0
    move-exception v0

    .line 611
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 469
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity$GetAccessTokenTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 15
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    .line 477
    const-string v11, "SnsAccountLiAuthSSOActivity"

    const-string v12, "GetAccessTokenTask doInBackground..."

    invoke-static {v11, v12}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 480
    iget-object v11, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity$GetAccessTokenTask;->this$0:Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;

    # invokes: Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->isLoggedInLinkedin()Z
    invoke-static {v11}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->access$200(Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 481
    const/4 v5, 0x0

    .line 486
    .local v5, "isNeedToHold":Z
    :goto_0
    const/4 v1, 0x1

    .line 487
    .local v1, "consentPageCode":I
    const/4 v7, 0x0

    .line 489
    .local v7, "response":Landroid/os/Bundle;
    :try_start_0
    iget-object v11, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity$GetAccessTokenTask;->this$0:Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;

    # getter for: Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->mAppId:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->access$900()Ljava/lang/String;

    move-result-object v12

    # getter for: Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->mSecret:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->access$1000()Ljava/lang/String;

    move-result-object v13

    const-string v14, "r_fullprofile r_emailaddress r_network r_contactinfo rw_nus rw_groups w_messages"

    invoke-static {v11, v12, v13, v14, v1}, Lcom/linkedin/android/authsso/LinkedInAuthUtil;->getToken(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Landroid/os/Bundle;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v7

    .line 498
    iget-object v11, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity$GetAccessTokenTask;->this$0:Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;

    # getter for: Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->mHandler:Landroid/os/Handler;
    invoke-static {v11}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->access$400(Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;)Landroid/os/Handler;

    move-result-object v11

    iget-object v12, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity$GetAccessTokenTask;->this$0:Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;

    # getter for: Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->mWatchdogRunnable:Ljava/lang/Runnable;
    invoke-static {v12}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->access$300(Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;)Ljava/lang/Runnable;

    move-result-object v12

    invoke-virtual {v11, v12}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 499
    iget-object v11, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity$GetAccessTokenTask;->this$0:Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;

    # getter for: Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->mHandler:Landroid/os/Handler;
    invoke-static {v11}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->access$400(Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;)Landroid/os/Handler;

    move-result-object v11

    new-instance v12, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity$GetAccessTokenTask$1;

    invoke-direct {v12, p0}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity$GetAccessTokenTask$1;-><init>(Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity$GetAccessTokenTask;)V

    invoke-virtual {v11, v12}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 513
    if-eqz v7, :cond_8

    .line 514
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    .line 516
    .local v8, "responseReceivedTime":J
    const-string v11, "access_token"

    const/4 v12, 0x0

    invoke-virtual {v7, v11, v12}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 517
    .local v10, "token":Ljava/lang/String;
    const-string v11, "expires_in"

    const/4 v12, 0x0

    invoke-virtual {v7, v11, v12}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v4

    .line 518
    .local v4, "expires":I
    const-string v11, "error_msg"

    const/4 v12, 0x0

    invoke-virtual {v7, v11, v12}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 520
    .local v3, "error":Ljava/lang/String;
    const-string v12, "SnsAccountLiAuthSSOActivity"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "token : "

    invoke-virtual {v11, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    if-eqz v10, :cond_1

    const/4 v11, 0x1

    :goto_1
    invoke-virtual {v13, v11}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v12, v11}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 521
    const-string v11, "SnsAccountLiAuthSSOActivity"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "expires : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 523
    if-eqz v10, :cond_2

    if-lez v4, :cond_2

    .line 524
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v11

    invoke-virtual {v11}, Lcom/sec/android/app/sns3/SnsApplication;->getSvcMgr()Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v11

    invoke-virtual {v11}, Lcom/sec/android/app/sns3/svc/SnsSvcMgr;->getTokenMgr()Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;

    move-result-object v11

    const-string v12, "linkedin"

    invoke-virtual {v11, v12}, Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;->getToken(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/sns3/svc/sp/linkedin/SnsLiToken;

    .line 527
    .local v6, "liToken":Lcom/sec/android/app/sns3/svc/sp/linkedin/SnsLiToken;
    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v6, v10, v11}, Lcom/sec/android/app/sns3/svc/sp/linkedin/SnsLiToken;->setAccessTokenNExpires(Ljava/lang/String;Ljava/lang/String;)V

    .line 529
    new-instance v0, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdAuthLogin;

    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v11

    invoke-virtual {v11}, Lcom/sec/android/app/sns3/SnsApplication;->getSvcMgr()Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v11

    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v12

    invoke-virtual {v12}, Lcom/sec/android/app/sns3/SnsApplication;->getAgentMgr()Lcom/sec/android/app/sns3/agent/SnsAgentMgr;

    move-result-object v12

    invoke-virtual {v12}, Lcom/sec/android/app/sns3/agent/SnsAgentMgr;->getCommandMgr()Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;

    move-result-object v12

    invoke-virtual {v12}, Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;->getCommandMgrHandle()Landroid/os/Handler;

    move-result-object v12

    const/4 v13, 0x0

    invoke-direct {v0, v11, v12, v13}, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdAuthLogin;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Handler;Landroid/os/Bundle;)V

    .line 532
    .local v0, "cmd":Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;
    new-instance v11, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity$GetAccessTokenTask$2;

    invoke-direct {v11, p0, v5, v8, v9}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity$GetAccessTokenTask$2;-><init>(Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity$GetAccessTokenTask;ZJ)V

    invoke-virtual {v0, v11}, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->setCommandCallback(Lcom/sec/android/app/sns3/agent/command/ISnsCommandCallback;)Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;

    .line 565
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->send()I

    .line 602
    .end local v0    # "cmd":Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;
    .end local v3    # "error":Ljava/lang/String;
    .end local v4    # "expires":I
    .end local v6    # "liToken":Lcom/sec/android/app/sns3/svc/sp/linkedin/SnsLiToken;
    .end local v8    # "responseReceivedTime":J
    .end local v10    # "token":Ljava/lang/String;
    :goto_2
    const/4 v11, 0x0

    :goto_3
    return-object v11

    .line 483
    .end local v1    # "consentPageCode":I
    .end local v5    # "isNeedToHold":Z
    .end local v7    # "response":Landroid/os/Bundle;
    :cond_0
    const/4 v5, 0x1

    .restart local v5    # "isNeedToHold":Z
    goto/16 :goto_0

    .line 491
    .restart local v1    # "consentPageCode":I
    .restart local v7    # "response":Landroid/os/Bundle;
    :catch_0
    move-exception v2

    .line 492
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    .line 493
    iget-object v11, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity$GetAccessTokenTask;->this$0:Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;

    # invokes: Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->registerRetryNotification()V
    invoke-static {v11}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->access$1100(Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;)V

    .line 494
    iget-object v11, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity$GetAccessTokenTask;->this$0:Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;

    invoke-virtual {v11}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->finish()V

    .line 495
    const/4 v11, 0x0

    goto :goto_3

    .line 520
    .end local v2    # "e":Ljava/lang/Exception;
    .restart local v3    # "error":Ljava/lang/String;
    .restart local v4    # "expires":I
    .restart local v8    # "responseReceivedTime":J
    .restart local v10    # "token":Ljava/lang/String;
    :cond_1
    const/4 v11, 0x0

    goto/16 :goto_1

    .line 566
    :cond_2
    if-eqz v3, :cond_3

    const-string v11, "User is not authenticated"

    invoke-virtual {v3, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v11

    if-eqz v11, :cond_6

    .line 567
    :cond_3
    const-string v11, "SnsAccountLiAuthSSOActivity"

    const-string v12, "canceled"

    invoke-static {v11, v12}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 569
    iget-object v11, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity$GetAccessTokenTask;->this$0:Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;

    # invokes: Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->isLoggedInLinkedin()Z
    invoke-static {v11}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->access$200(Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 571
    if-eqz v5, :cond_4

    .line 572
    invoke-direct {p0, v8, v9}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity$GetAccessTokenTask;->pauseThread(J)V

    .line 575
    :cond_4
    iget-object v11, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity$GetAccessTokenTask;->this$0:Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;

    # getter for: Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->mSkipSSONoti:Z
    invoke-static {v11}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->access$1400(Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;)Z

    move-result v11

    if-nez v11, :cond_5

    .line 576
    iget-object v11, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity$GetAccessTokenTask;->this$0:Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;

    new-instance v12, Landroid/content/Intent;

    const-string v13, "com.sec.android.app.sns3.auth.sp.linkedin.SNS_LOGIN_LINKEDIN_NOTIFICATION"

    invoke-direct {v12, v13}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v12}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 581
    :cond_5
    iget-object v11, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity$GetAccessTokenTask;->this$0:Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;

    invoke-virtual {v11}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->finish()V

    goto :goto_2

    .line 583
    :cond_6
    const-string v11, "SnsAccountLiAuthSSOActivity"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "error : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 586
    if-eqz v5, :cond_7

    .line 587
    invoke-direct {p0, v8, v9}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity$GetAccessTokenTask;->pauseThread(J)V

    .line 590
    :cond_7
    iget-object v11, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity$GetAccessTokenTask;->this$0:Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;

    # invokes: Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->registerRetryNotification()V
    invoke-static {v11}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->access$1100(Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;)V

    .line 591
    iget-object v11, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity$GetAccessTokenTask;->this$0:Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;

    invoke-virtual {v11}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->finish()V

    goto :goto_2

    .line 594
    .end local v3    # "error":Ljava/lang/String;
    .end local v4    # "expires":I
    .end local v8    # "responseReceivedTime":J
    .end local v10    # "token":Ljava/lang/String;
    :cond_8
    const-string v11, "SnsAccountLiAuthSSOActivity"

    const-string v12, "response of AuthUtil is null - canceled"

    invoke-static {v11, v12}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 595
    iget-object v11, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity$GetAccessTokenTask;->this$0:Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;

    # invokes: Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->isLoggedInLinkedin()Z
    invoke-static {v11}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->access$200(Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;)Z

    move-result v11

    if-eqz v11, :cond_9

    iget-object v11, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity$GetAccessTokenTask;->this$0:Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;

    # getter for: Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->mSkipSSONoti:Z
    invoke-static {v11}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->access$1400(Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;)Z

    move-result v11

    if-nez v11, :cond_9

    .line 596
    iget-object v11, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity$GetAccessTokenTask;->this$0:Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;

    new-instance v12, Landroid/content/Intent;

    const-string v13, "com.sec.android.app.sns3.auth.sp.linkedin.SNS_LOGIN_LINKEDIN_NOTIFICATION"

    invoke-direct {v12, v13}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v12}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 599
    :cond_9
    iget-object v11, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity$GetAccessTokenTask;->this$0:Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;

    invoke-virtual {v11}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->finish()V

    goto/16 :goto_2
.end method
