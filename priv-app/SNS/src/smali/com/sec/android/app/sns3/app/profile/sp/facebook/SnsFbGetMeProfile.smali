.class public Lcom/sec/android/app/sns3/app/profile/sp/facebook/SnsFbGetMeProfile;
.super Ljava/lang/Object;
.source "SnsFbGetMeProfile.java"


# static fields
.field protected static final TAG:Ljava/lang/String; = "SnsFbGetMeProfile"

.field private static mCmdHandler:Landroid/os/Handler;

.field private static mServiceMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000(Landroid/content/Context;ILcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMyProfile;)V
    .locals 0
    .param p0, "x0"    # Landroid/content/Context;
    .param p1, "x1"    # I
    .param p2, "x2"    # Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMyProfile;

    .prologue
    .line 38
    invoke-static {p0, p1, p2}, Lcom/sec/android/app/sns3/app/profile/sp/facebook/SnsFbGetMeProfile;->invokeBroadcast(Landroid/content/Context;ILcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMyProfile;)V

    return-void
.end method

.method public static getMeProfile(Landroid/content/Context;Ljava/lang/String;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "userId"    # Ljava/lang/String;

    .prologue
    .line 47
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/sns3/SnsApplication;->getSvcMgr()Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v2

    sput-object v2, Lcom/sec/android/app/sns3/app/profile/sp/facebook/SnsFbGetMeProfile;->mServiceMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    .line 48
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/sns3/SnsApplication;->getAgentMgr()Lcom/sec/android/app/sns3/agent/SnsAgentMgr;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/sns3/agent/SnsAgentMgr;->getCommandMgr()Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;->getCommandMgrHandle()Landroid/os/Handler;

    move-result-object v2

    sput-object v2, Lcom/sec/android/app/sns3/app/profile/sp/facebook/SnsFbGetMeProfile;->mCmdHandler:Landroid/os/Handler;

    .line 50
    sget-object v2, Lcom/sec/android/app/sns3/app/profile/sp/facebook/SnsFbGetMeProfile;->mServiceMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    if-nez v2, :cond_0

    .line 51
    const-string v2, "SnsFbGetMeProfile"

    const-string v3, "[SNS] mSvcMgr is null !!"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 91
    :goto_0
    return-void

    .line 54
    :cond_0
    sget-object v2, Lcom/sec/android/app/sns3/app/profile/sp/facebook/SnsFbGetMeProfile;->mCmdHandler:Landroid/os/Handler;

    if-nez v2, :cond_1

    .line 55
    const-string v2, "SnsFbGetMeProfile"

    const-string v3, "[SNS] mCmdHandler is null !!"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 59
    :cond_1
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v2

    const-string v3, "com.sec.android.app.sns3.facebook"

    invoke-virtual {v2, v3}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 60
    .local v0, "account":[Landroid/accounts/Account;
    array-length v2, v0

    if-lez v2, :cond_2

    .line 61
    new-instance v1, Lcom/sec/android/app/sns3/app/profile/sp/facebook/SnsFbGetMeProfile$1;

    sget-object v2, Lcom/sec/android/app/sns3/app/profile/sp/facebook/SnsFbGetMeProfile;->mServiceMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    invoke-direct {v1, v2, p0}, Lcom/sec/android/app/sns3/app/profile/sp/facebook/SnsFbGetMeProfile$1;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/content/Context;)V

    .line 86
    .local v1, "req":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    invoke-virtual {v1}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->request()Z

    goto :goto_0

    .line 90
    .end local v1    # "req":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    :cond_2
    const/4 v2, -0x1

    const/4 v3, 0x0

    invoke-static {p0, v2, v3}, Lcom/sec/android/app/sns3/app/profile/sp/facebook/SnsFbGetMeProfile;->invokeBroadcast(Landroid/content/Context;ILcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMyProfile;)V

    goto :goto_0
.end method

.method private static invokeBroadcast(Landroid/content/Context;ILcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMyProfile;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "resultCode"    # I
    .param p2, "profile"    # Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMyProfile;

    .prologue
    .line 94
    const-string v1, "SnsFbGetMeProfile"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "invokeBroadcast() : resultCode = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 95
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.app.sns.profile.ACTION_FACEBOOK_ME_PROFILE_UPDATED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 96
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "result"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 97
    if-eqz p2, :cond_0

    .line 98
    const-string v1, "name"

    iget-object v2, p2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMyProfile;->mName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 99
    const-string v1, "birthday"

    iget-object v2, p2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMyProfile;->mBirthdayDate:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 100
    const-string v1, "email"

    iget-object v2, p2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMyProfile;->mEmail:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 101
    const-string v1, "address"

    iget-object v2, p2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMyProfile;->mCurrentLocation:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 102
    const-string v1, "work"

    iget-object v2, p2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMyProfile;->mWork:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 103
    const-string v1, "profile_url"

    iget-object v2, p2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMyProfile;->mPic:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 105
    :cond_0
    const-string v1, "com.sec.android.app.sns3.permission.RECEIVE_SNS_BROADCAST"

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    .line 106
    return-void
.end method
