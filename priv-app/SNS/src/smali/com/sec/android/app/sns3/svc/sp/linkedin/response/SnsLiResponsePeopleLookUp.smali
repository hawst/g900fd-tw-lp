.class public final Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp;
.super Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;
.source "SnsLiResponsePeopleLookUp.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mCompanyName:Ljava/lang/String;

.field private mEmail:Ljava/lang/String;

.field private mFirstName:Ljava/lang/String;

.field private mFormattedName:Ljava/lang/String;

.field private mHeadline:Ljava/lang/String;

.field private mLargePictureUrl:Ljava/lang/String;

.field private mLastName:Ljava/lang/String;

.field private mMemberId:Ljava/lang/String;

.field public mNext:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp;

.field private mPhoneNumber:Ljava/lang/String;

.field private mPictureUrl:Ljava/lang/String;

.field private mPositionTitle:Ljava/lang/String;

.field private mProfileUrl:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 76
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp$1;

    invoke-direct {v0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp$1;-><init>()V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 90
    invoke-direct {p0}, Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;-><init>()V

    .line 91
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 94
    invoke-direct {p0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp;-><init>()V

    .line 95
    invoke-virtual {p0, p1}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp;->readFromParcel(Landroid/os/Parcel;)V

    .line 96
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp$1;

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 56
    const/4 v0, 0x0

    return v0
.end method

.method public getCompanyName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 187
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp;->mCompanyName:Ljava/lang/String;

    return-object v0
.end method

.method public getEmail()Ljava/lang/String;
    .locals 1

    .prologue
    .line 155
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp;->mEmail:Ljava/lang/String;

    return-object v0
.end method

.method public getFirstName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp;->mFirstName:Ljava/lang/String;

    return-object v0
.end method

.method public getFormattedName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp;->mFormattedName:Ljava/lang/String;

    return-object v0
.end method

.method public getHeadline()Ljava/lang/String;
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp;->mHeadline:Ljava/lang/String;

    return-object v0
.end method

.method public getLargePictureUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 203
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp;->mLargePictureUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getLastName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp;->mLastName:Ljava/lang/String;

    return-object v0
.end method

.method public getMemberId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp;->mMemberId:Ljava/lang/String;

    return-object v0
.end method

.method public getPhoneNumber()Ljava/lang/String;
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp;->mPhoneNumber:Ljava/lang/String;

    return-object v0
.end method

.method public getPictureUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 195
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp;->mPictureUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getPositionTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 179
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp;->mPositionTitle:Ljava/lang/String;

    return-object v0
.end method

.method public getProfileUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 171
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp;->mProfileUrl:Ljava/lang/String;

    return-object v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 99
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp;->mMemberId:Ljava/lang/String;

    .line 100
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp;->mFirstName:Ljava/lang/String;

    .line 101
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp;->mLastName:Ljava/lang/String;

    .line 102
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp;->mFormattedName:Ljava/lang/String;

    .line 103
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp;->mHeadline:Ljava/lang/String;

    .line 104
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp;->mEmail:Ljava/lang/String;

    .line 105
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp;->mPhoneNumber:Ljava/lang/String;

    .line 106
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp;->mProfileUrl:Ljava/lang/String;

    .line 107
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp;->mPositionTitle:Ljava/lang/String;

    .line 108
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp;->mCompanyName:Ljava/lang/String;

    .line 109
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp;->mPictureUrl:Ljava/lang/String;

    .line 110
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp;->mLargePictureUrl:Ljava/lang/String;

    .line 111
    const-class v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp;

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp;->mNext:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp;

    .line 112
    return-void
.end method

.method public setCompanyName(Ljava/lang/String;)V
    .locals 0
    .param p1, "mCompanyName"    # Ljava/lang/String;

    .prologue
    .line 191
    iput-object p1, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp;->mCompanyName:Ljava/lang/String;

    .line 192
    return-void
.end method

.method public setEmail(Ljava/lang/String;)V
    .locals 0
    .param p1, "mEmail"    # Ljava/lang/String;

    .prologue
    .line 159
    iput-object p1, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp;->mEmail:Ljava/lang/String;

    .line 160
    return-void
.end method

.method public setFirstName(Ljava/lang/String;)V
    .locals 0
    .param p1, "mFirstName"    # Ljava/lang/String;

    .prologue
    .line 127
    iput-object p1, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp;->mFirstName:Ljava/lang/String;

    .line 128
    return-void
.end method

.method public setFormattedName(Ljava/lang/String;)V
    .locals 0
    .param p1, "mFormattedName"    # Ljava/lang/String;

    .prologue
    .line 143
    iput-object p1, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp;->mFormattedName:Ljava/lang/String;

    .line 144
    return-void
.end method

.method public setHeadline(Ljava/lang/String;)V
    .locals 0
    .param p1, "mHeadline"    # Ljava/lang/String;

    .prologue
    .line 151
    iput-object p1, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp;->mHeadline:Ljava/lang/String;

    .line 152
    return-void
.end method

.method public setLargePictureUrl(Ljava/lang/String;)V
    .locals 0
    .param p1, "mLargePictureUrl"    # Ljava/lang/String;

    .prologue
    .line 207
    iput-object p1, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp;->mLargePictureUrl:Ljava/lang/String;

    .line 208
    return-void
.end method

.method public setLastName(Ljava/lang/String;)V
    .locals 0
    .param p1, "mLastName"    # Ljava/lang/String;

    .prologue
    .line 135
    iput-object p1, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp;->mLastName:Ljava/lang/String;

    .line 136
    return-void
.end method

.method public setMemberId(Ljava/lang/String;)V
    .locals 0
    .param p1, "mMemberId"    # Ljava/lang/String;

    .prologue
    .line 119
    iput-object p1, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp;->mMemberId:Ljava/lang/String;

    .line 120
    return-void
.end method

.method public setPhoneNumber(Ljava/lang/String;)V
    .locals 0
    .param p1, "mPhoneNumber"    # Ljava/lang/String;

    .prologue
    .line 167
    iput-object p1, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp;->mPhoneNumber:Ljava/lang/String;

    .line 168
    return-void
.end method

.method public setPictureUrl(Ljava/lang/String;)V
    .locals 0
    .param p1, "mPictureUrl"    # Ljava/lang/String;

    .prologue
    .line 199
    iput-object p1, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp;->mPictureUrl:Ljava/lang/String;

    .line 200
    return-void
.end method

.method public setPositionTitle(Ljava/lang/String;)V
    .locals 0
    .param p1, "mPositionTitle"    # Ljava/lang/String;

    .prologue
    .line 183
    iput-object p1, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp;->mPositionTitle:Ljava/lang/String;

    .line 184
    return-void
.end method

.method public setProfileUrl(Ljava/lang/String;)V
    .locals 0
    .param p1, "mProfileUrl"    # Ljava/lang/String;

    .prologue
    .line 175
    iput-object p1, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp;->mProfileUrl:Ljava/lang/String;

    .line 176
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flag"    # I

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp;->mMemberId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 62
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp;->mFirstName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 63
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp;->mLastName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 64
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp;->mFormattedName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 65
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp;->mHeadline:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 66
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp;->mEmail:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 67
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp;->mPhoneNumber:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 68
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp;->mProfileUrl:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 69
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp;->mPositionTitle:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 70
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp;->mCompanyName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 71
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp;->mPictureUrl:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 72
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp;->mLargePictureUrl:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 73
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp;->mNext:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 74
    return-void
.end method
