.class public interface abstract Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$StatusStreamColumns;
.super Ljava/lang/Object;
.source "SnsFacebookDB.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "StatusStreamColumns"
.end annotation


# static fields
.field public static final ACTIONS:Ljava/lang/String; = "actions"

.field public static final ACTOR_PHOTO_URL:Ljava/lang/String; = "_actor_photo_url"

.field public static final APPLICATION_ID:Ljava/lang/String; = "application_id"

.field public static final APPLICATION_NAME:Ljava/lang/String; = "application_name"

.field public static final CAPTION:Ljava/lang/String; = "caption"

.field public static final CITY:Ljava/lang/String; = "city"

.field public static final COMMENTS_COUNT:Ljava/lang/String; = "comments_count"

.field public static final COUNTRY:Ljava/lang/String; = "country"

.field public static final CREATE_TIME:Ljava/lang/String; = "create_time"

.field public static final DESCRIPTION:Ljava/lang/String; = "description"

.field public static final FROM_ID:Ljava/lang/String; = "from_id"

.field public static final FROM_NAME:Ljava/lang/String; = "from_name"

.field public static final ICON:Ljava/lang/String; = "icon"

.field public static final ID:Ljava/lang/String; = "post_id"

.field public static final LATITUDE:Ljava/lang/String; = "latitude"

.field public static final LIKES_COUNT:Ljava/lang/String; = "likes_count"

.field public static final LIKES_NAME:Ljava/lang/String; = "likes_name"

.field public static final LIKE_DONE:Ljava/lang/String; = "like_done"

.field public static final LINK:Ljava/lang/String; = "link"

.field public static final LONGITUDE:Ljava/lang/String; = "longitude"

.field public static final MESSAGE:Ljava/lang/String; = "message"

.field public static final NAME:Ljava/lang/String; = "name"

.field public static final OBJECT_ID:Ljava/lang/String; = "object_id"

.field public static final PICTURE:Ljava/lang/String; = "picture"

.field public static final PLACE_ID:Ljava/lang/String; = "place_id"

.field public static final PLACE_NAME:Ljava/lang/String; = "place_name"

.field public static final PRIVACY:Ljava/lang/String; = "privacy"

.field public static final PROPERTIES:Ljava/lang/String; = "properties"

.field public static final SHARES:Ljava/lang/String; = "shares"

.field public static final SOURCE:Ljava/lang/String; = "source"

.field public static final STATE:Ljava/lang/String; = "state"

.field public static final STREET:Ljava/lang/String; = "street"

.field public static final TARGETING:Ljava/lang/String; = "targeting"

.field public static final TO_ID:Ljava/lang/String; = "to_id"

.field public static final TO_NAME:Ljava/lang/String; = "to_name"

.field public static final TYPE:Ljava/lang/String; = "type"

.field public static final UPDATED_TIME:Ljava/lang/String; = "updated_time"

.field public static final ZIP:Ljava/lang/String; = "zip"
