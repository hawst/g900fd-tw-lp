.class public final enum Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;
.super Ljava/lang/Enum;
.source "SnsTwStatusesAPI.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;",
        ">;",
        "Landroid/os/Parcelable;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum STATUESE_HOME_TIMELINE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;

.field public static final enum STATUSES_DESTROY:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;

.field public static final enum STATUSES_MENTIONS:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;

.field public static final enum STATUSES_PUBLIC_TIMELINE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;

.field public static final enum STATUSES_RETWEET:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;

.field public static final enum STATUSES_RETWEETED_BY_ME:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;

.field public static final enum STATUSES_RETWEETED_BY_USER:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;

.field public static final enum STATUSES_RETWEETED_TO_ME:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;

.field public static final enum STATUSES_RETWEETED_TO_USER:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;

.field public static final enum STATUSES_RETWEETS:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;

.field public static final enum STATUSES_RETWEETS_OF_ME:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;

.field public static final enum STATUSES_SHOW:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;

.field public static final enum STATUSES_UPDATE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;

.field public static final enum STATUSES_UPDATE_WITH_MEDIA:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;

.field public static final enum STATUSES_USER_TIMELINE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;


# instance fields
.field private mHttpMethod:Ljava/lang/String;

.field private mUrl:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 32
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;

    const-string v1, "STATUESE_HOME_TIMELINE"

    const-string v2, "/statuses/home_timeline.json"

    const-string v3, "GET"

    invoke-direct {v0, v1, v5, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;->STATUESE_HOME_TIMELINE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;

    .line 33
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;

    const-string v1, "STATUSES_MENTIONS"

    const-string v2, "/statuses/mentions.json"

    const-string v3, "GET"

    invoke-direct {v0, v1, v6, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;->STATUSES_MENTIONS:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;

    .line 34
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;

    const-string v1, "STATUSES_PUBLIC_TIMELINE"

    const-string v2, "/statuses/public_timeline.json"

    const-string v3, "GET"

    invoke-direct {v0, v1, v7, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;->STATUSES_PUBLIC_TIMELINE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;

    .line 35
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;

    const-string v1, "STATUSES_RETWEETED_TO_ME"

    const-string v2, "/statuses/retweeted_to_me.json"

    const-string v3, "GET"

    invoke-direct {v0, v1, v8, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;->STATUSES_RETWEETED_TO_ME:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;

    .line 36
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;

    const-string v1, "STATUSES_RETWEETED_BY_ME"

    const-string v2, "/statuses/retweeted_by_me.json"

    const-string v3, "GET"

    invoke-direct {v0, v1, v9, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;->STATUSES_RETWEETED_BY_ME:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;

    .line 37
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;

    const-string v1, "STATUSES_RETWEETS_OF_ME"

    const/4 v2, 0x5

    const-string v3, "/statuses/retweets_of_me.json"

    const-string v4, "GET"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;->STATUSES_RETWEETS_OF_ME:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;

    .line 38
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;

    const-string v1, "STATUSES_USER_TIMELINE"

    const/4 v2, 0x6

    const-string v3, "/statuses/user_timeline.json"

    const-string v4, "GET"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;->STATUSES_USER_TIMELINE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;

    .line 39
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;

    const-string v1, "STATUSES_RETWEETED_TO_USER"

    const/4 v2, 0x7

    const-string v3, "/statuses/retweeted_to_user.json"

    const-string v4, "GET"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;->STATUSES_RETWEETED_TO_USER:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;

    .line 40
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;

    const-string v1, "STATUSES_RETWEETED_BY_USER"

    const/16 v2, 0x8

    const-string v3, "/statuses/retweeted_by_user.json"

    const-string v4, "GET"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;->STATUSES_RETWEETED_BY_USER:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;

    .line 43
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;

    const-string v1, "STATUSES_SHOW"

    const/16 v2, 0x9

    const-string v3, "/statuses/show/"

    const-string v4, "GET"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;->STATUSES_SHOW:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;

    .line 44
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;

    const-string v1, "STATUSES_RETWEET"

    const/16 v2, 0xa

    const-string v3, "/statuses/retweet/"

    const-string v4, "POST"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;->STATUSES_RETWEET:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;

    .line 45
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;

    const-string v1, "STATUSES_RETWEETS"

    const/16 v2, 0xb

    const-string v3, "/statuses/retweets/"

    const-string v4, "GET"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;->STATUSES_RETWEETS:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;

    .line 46
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;

    const-string v1, "STATUSES_DESTROY"

    const/16 v2, 0xc

    const-string v3, "/statuses/destroy/"

    const-string v4, "POST"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;->STATUSES_DESTROY:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;

    .line 47
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;

    const-string v1, "STATUSES_UPDATE"

    const/16 v2, 0xd

    const-string v3, "/statuses/update.json"

    const-string v4, "POST"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;->STATUSES_UPDATE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;

    .line 48
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;

    const-string v1, "STATUSES_UPDATE_WITH_MEDIA"

    const/16 v2, 0xe

    const-string v3, "/statuses/update_with_media.json"

    const-string v4, "POST"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;->STATUSES_UPDATE_WITH_MEDIA:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;

    .line 29
    const/16 v0, 0xf

    new-array v0, v0, [Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;

    sget-object v1, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;->STATUESE_HOME_TIMELINE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;->STATUSES_MENTIONS:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;->STATUSES_PUBLIC_TIMELINE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;

    aput-object v1, v0, v7

    sget-object v1, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;->STATUSES_RETWEETED_TO_ME:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;

    aput-object v1, v0, v8

    sget-object v1, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;->STATUSES_RETWEETED_BY_ME:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;

    aput-object v1, v0, v9

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;->STATUSES_RETWEETS_OF_ME:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;->STATUSES_USER_TIMELINE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;->STATUSES_RETWEETED_TO_USER:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;->STATUSES_RETWEETED_BY_USER:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;->STATUSES_SHOW:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;->STATUSES_RETWEET:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;->STATUSES_RETWEETS:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;->STATUSES_DESTROY:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;->STATUSES_UPDATE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;->STATUSES_UPDATE_WITH_MEDIA:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;->$VALUES:[Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;

    .line 107
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$1;

    invoke-direct {v0}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$1;-><init>()V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p3, "url"    # Ljava/lang/String;
    .param p4, "httpMethod"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 89
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 90
    iput-object p3, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;->mUrl:Ljava/lang/String;

    .line 91
    iput-object p4, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;->mHttpMethod:Ljava/lang/String;

    .line 92
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 29
    const-class v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;->$VALUES:[Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;

    invoke-virtual {v0}, [Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;

    return-object v0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 123
    const/4 v0, 0x0

    return v0
.end method

.method public getHttpMethod()Ljava/lang/String;
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;->mHttpMethod:Ljava/lang/String;

    return-object v0
.end method

.method public getUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;->mUrl:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;->mUrl:Ljava/lang/String;

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 128
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 129
    return-void
.end method
