.class Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInAuthActivity$2$1;
.super Ljava/lang/Object;
.source "SnsAccountInAuthActivity.java"

# interfaces
.implements Lcom/sec/android/app/sns3/agent/command/ISnsCommandCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInAuthActivity$2;->run(Landroid/accounts/AccountManagerFuture;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInAuthActivity$2;


# direct methods
.method constructor <init>(Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInAuthActivity$2;)V
    .locals 0

    .prologue
    .line 142
    iput-object p1, p0, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInAuthActivity$2$1;->this$1:Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInAuthActivity$2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCmdRespond(IZLjava/lang/String;Ljava/util/List;)V
    .locals 6
    .param p1, "cmdID"    # I
    .param p2, "bSuccess"    # Z
    .param p3, "uri"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IZ",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p4, "responseList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;>;"
    const/4 v5, -0x1

    .line 148
    const/4 v0, 0x0

    .line 149
    .local v0, "reason":Landroid/os/Bundle;
    const/4 v2, 0x0

    .line 150
    .local v2, "userName":Ljava/lang/String;
    const/4 v1, 0x0

    .line 152
    .local v1, "userId":Ljava/lang/String;
    if-eqz p4, :cond_0

    .line 153
    invoke-interface {p4}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_0

    .line 154
    const/4 v3, 0x0

    invoke-interface {p4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;

    invoke-virtual {v3}, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;->getReason()Landroid/os/Bundle;

    move-result-object v0

    .line 156
    if-eqz v0, :cond_0

    .line 157
    const-string v3, "userName"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 158
    const-string v3, "userID"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 163
    :cond_0
    if-eqz v2, :cond_1

    .line 164
    iget-object v3, p0, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInAuthActivity$2$1;->this$1:Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInAuthActivity$2;

    iget-object v3, v3, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInAuthActivity$2;->this$0:Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInAuthActivity;

    invoke-virtual {v3, v2, v1}, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInAuthActivity;->loginSuccess(Ljava/lang/String;Ljava/lang/String;)V

    .line 167
    :goto_0
    return-void

    .line 166
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInAuthActivity$2$1;->this$1:Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInAuthActivity$2;

    iget-object v3, v3, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInAuthActivity$2;->this$0:Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInAuthActivity;

    const/4 v4, 0x0

    invoke-virtual {v3, v5, v5, v4}, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInAuthActivity;->loginFail(IILjava/lang/String;)V

    goto :goto_0
.end method
