.class Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInAuthActivity$UIDialogThreadRunnable;
.super Ljava/lang/Object;
.source "SnsAccountInAuthActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInAuthActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "UIDialogThreadRunnable"
.end annotation


# instance fields
.field mActionType:I

.field mAppContext:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/content/Context;I)V
    .locals 1
    .param p1, "ctxt"    # Landroid/content/Context;
    .param p2, "actionType"    # I

    .prologue
    .line 550
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 546
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInAuthActivity$UIDialogThreadRunnable;->mAppContext:Landroid/content/Context;

    .line 548
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInAuthActivity$UIDialogThreadRunnable;->mActionType:I

    .line 551
    iput-object p1, p0, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInAuthActivity$UIDialogThreadRunnable;->mAppContext:Landroid/content/Context;

    .line 552
    iput p2, p0, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInAuthActivity$UIDialogThreadRunnable;->mActionType:I

    .line 553
    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 556
    iget v0, p0, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInAuthActivity$UIDialogThreadRunnable;->mActionType:I

    packed-switch v0, :pswitch_data_0

    .line 576
    :goto_0
    return-void

    .line 559
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInAuthActivity$UIDialogThreadRunnable;->mAppContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInAuthActivity;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInAuthActivity;->showDialog(I)V

    goto :goto_0

    .line 564
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInAuthActivity$UIDialogThreadRunnable;->mAppContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInAuthActivity;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInAuthActivity;->showDialog(I)V

    goto :goto_0

    .line 569
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInAuthActivity$UIDialogThreadRunnable;->mAppContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInAuthActivity;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInAuthActivity;->showDialog(I)V

    goto :goto_0

    .line 556
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
