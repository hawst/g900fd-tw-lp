.class Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1$8;
.super Ljava/lang/Object;
.source "SnsFbService.java"

# interfaces
.implements Lcom/sec/android/app/sns3/svc/http/SnsCustomMultiPartEntity$ProgressListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1;->postPhoto(Ljava/lang/String;Landroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/facebook/api/ISnsFacebookCallbackID;)I
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1;


# direct methods
.method constructor <init>(Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1;)V
    .locals 0

    .prologue
    .line 369
    iput-object p1, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1$8;->this$1:Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public transferred(J)V
    .locals 7
    .param p1, "num"    # J

    .prologue
    .line 373
    long-to-float v3, p1

    sget-wide v4, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;->mContentSize:J

    long-to-float v4, v4

    div-float/2addr v3, v4

    const/high16 v4, 0x42c80000    # 100.0f

    mul-float/2addr v3, v4

    float-to-long v0, v3

    .line 375
    .local v0, "currentProgress":J
    iget-object v3, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1$8;->this$1:Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1;

    iget-object v3, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1;->this$0:Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;

    # getter for: Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;->mProgressNum:J
    invoke-static {v3}, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;->access$100(Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;)J

    move-result-wide v4

    cmp-long v3, v4, v0

    if-eqz v3, :cond_0

    .line 376
    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.sec.android.app.sns3.svc.sp.facebook.SNS_FB_NOTIFICATION"

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1$8;->this$1:Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1;

    iget-object v5, v5, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1;->this$0:Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;

    const-class v6, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbBroadcastReceiver;

    invoke-direct {v2, v3, v4, v5, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 377
    .local v2, "intent":Landroid/content/Intent;
    const-string v3, "SNS_FB_NOTIFICATION.progress"

    invoke-virtual {v2, v3, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 378
    const-string v3, "SNS_FB_NOTIFICATION.state"

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 379
    iget-object v3, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1$8;->this$1:Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1;

    iget-object v3, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1;->this$0:Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;

    invoke-virtual {v3, v2}, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;->sendBroadcast(Landroid/content/Intent;)V

    .line 380
    iget-object v3, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1$8;->this$1:Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1;

    iget-object v3, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1;->this$0:Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;

    # setter for: Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;->mProgressNum:J
    invoke-static {v3, v0, v1}, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;->access$102(Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;J)J

    .line 382
    .end local v2    # "intent":Landroid/content/Intent;
    :cond_0
    return-void
.end method
