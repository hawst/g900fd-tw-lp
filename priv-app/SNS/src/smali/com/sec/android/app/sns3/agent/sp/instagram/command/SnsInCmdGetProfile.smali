.class public Lcom/sec/android/app/sns3/agent/sp/instagram/command/SnsInCmdGetProfile;
.super Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;
.source "SnsInCmdGetProfile.java"


# instance fields
.field private mUserID:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Handler;Ljava/lang/String;)V
    .locals 4
    .param p1, "svcMgr"    # Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    .param p2, "cmdHandler"    # Landroid/os/Handler;
    .param p3, "userID"    # Ljava/lang/String;

    .prologue
    .line 44
    invoke-direct {p0, p2}, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;-><init>(Landroid/os/Handler;)V

    .line 46
    new-instance v1, Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;

    invoke-direct {v1}, Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;-><init>()V

    .line 48
    .local v1, "unit":Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;
    iput-object p3, p0, Lcom/sec/android/app/sns3/agent/sp/instagram/command/SnsInCmdGetProfile;->mUserID:Ljava/lang/String;

    .line 50
    new-instance v0, Lcom/sec/android/app/sns3/agent/sp/instagram/command/SnsInCmdGetProfile$1;

    iget-object v2, p0, Lcom/sec/android/app/sns3/agent/sp/instagram/command/SnsInCmdGetProfile;->mUserID:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-direct {v0, p0, p1, v2, v3}, Lcom/sec/android/app/sns3/agent/sp/instagram/command/SnsInCmdGetProfile$1;-><init>(Lcom/sec/android/app/sns3/agent/sp/instagram/command/SnsInCmdGetProfile;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 91
    .local v0, "req":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    invoke-virtual {v1, v0}, Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;->addRequest(Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;)V

    .line 93
    invoke-virtual {p0, v1}, Lcom/sec/android/app/sns3/agent/sp/instagram/command/SnsInCmdGetProfile;->addCommandUnit(Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;)V

    .line 94
    return-void
.end method


# virtual methods
.method public getUserID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/android/app/sns3/agent/sp/instagram/command/SnsInCmdGetProfile;->mUserID:Ljava/lang/String;

    return-object v0
.end method

.method protected respond()Z
    .locals 5

    .prologue
    .line 102
    const-string v0, "SnsAgent"

    const-string v1, "<SnsInCmdGetProfile> respond()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 104
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/instagram/command/SnsInCmdGetProfile;->getCommandCallback()Lcom/sec/android/app/sns3/agent/command/ISnsCommandCallback;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/instagram/command/SnsInCmdGetProfile;->getCommandID()I

    move-result v1

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/instagram/command/SnsInCmdGetProfile;->isSuccess()Z

    move-result v2

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/instagram/command/SnsInCmdGetProfile;->getUri()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/instagram/command/SnsInCmdGetProfile;->getResponseList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/sec/android/app/sns3/agent/command/ISnsCommandCallback;->onCmdRespond(IZLjava/lang/String;Ljava/util/List;)V

    .line 106
    const/4 v0, 0x1

    return v0
.end method
