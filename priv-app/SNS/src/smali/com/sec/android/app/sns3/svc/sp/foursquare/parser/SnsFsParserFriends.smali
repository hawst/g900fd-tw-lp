.class public Lcom/sec/android/app/sns3/svc/sp/foursquare/parser/SnsFsParserFriends;
.super Ljava/lang/Object;
.source "SnsFsParserFriends.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/foursquare/parser/SnsFsParserFriends$FourSquareFriends;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    return-void
.end method

.method public static parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseFriends;
    .locals 11
    .param p0, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 40
    new-instance v4, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseFriends;

    invoke-direct {v4}, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseFriends;-><init>()V

    .line 42
    .local v4, "friendsObj":Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseFriends;
    const/4 v2, 0x0

    .line 43
    .local v2, "friends":Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;
    const/4 v0, 0x0

    .line 46
    .local v0, "curFriends":Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;
    :try_start_0
    new-instance v10, Lorg/json/JSONTokener;

    invoke-direct {v10, p0}, Lorg/json/JSONTokener;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Lorg/json/JSONTokener;->nextValue()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lorg/json/JSONObject;

    .line 48
    .local v7, "jsonObject":Lorg/json/JSONObject;
    const-string v10, "response"

    invoke-virtual {v7, v10}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lorg/json/JSONObject;

    .line 49
    .local v9, "response":Lorg/json/JSONObject;
    const-string v10, "friends"

    invoke-virtual {v9, v10}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/json/JSONObject;

    .line 51
    .local v5, "friendsResponse":Lorg/json/JSONObject;
    if-eqz v5, :cond_1

    .line 53
    const-string v10, "items"

    invoke-virtual {v5, v10}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v3

    .line 54
    .local v3, "friendsItems":Lorg/json/JSONArray;
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v10

    if-ge v6, v10, :cond_1

    .line 55
    invoke-virtual {v3, v6}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/sec/android/app/sns3/svc/sp/foursquare/parser/SnsFsParserFriends;->parseUser(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;

    move-result-object v8

    .line 57
    .local v8, "newFriend":Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;
    if-nez v2, :cond_0

    .line 58
    move-object v2, v8

    .line 59
    move-object v0, v2

    .line 54
    :goto_1
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 61
    :cond_0
    iput-object v8, v0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mNext:Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;

    .line 62
    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mNext:Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;

    goto :goto_1

    .line 67
    .end local v3    # "friendsItems":Lorg/json/JSONArray;
    .end local v6    # "i":I
    .end local v8    # "newFriend":Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;
    :cond_1
    iput-object v2, v4, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseFriends;->mUsers:Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;

    .line 68
    const-string v10, "count"

    invoke-virtual {v5, v10}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v10

    iput v10, v4, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseFriends;->mCount:I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 79
    .end local v5    # "friendsResponse":Lorg/json/JSONObject;
    .end local v7    # "jsonObject":Lorg/json/JSONObject;
    .end local v9    # "response":Lorg/json/JSONObject;
    :goto_2
    return-object v4

    .line 75
    :catch_0
    move-exception v1

    .line 77
    .local v1, "e":Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_2
.end method

.method private static parseUser(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;
    .locals 6
    .param p0, "response"    # Ljava/lang/String;

    .prologue
    .line 84
    new-instance v2, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;

    invoke-direct {v2}, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;-><init>()V

    .line 88
    .local v2, "user":Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;
    :try_start_0
    new-instance v4, Lorg/json/JSONTokener;

    invoke-direct {v4, p0}, Lorg/json/JSONTokener;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Lorg/json/JSONTokener;->nextValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/json/JSONObject;

    .line 89
    .local v3, "userResponse":Lorg/json/JSONObject;
    const-string v4, "id"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v2, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mUserID:Ljava/lang/String;

    .line 90
    const-string v4, "firstName"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v2, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mUserFirstName:Ljava/lang/String;

    .line 91
    const-string v4, "lastName"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v2, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mUserLastName:Ljava/lang/String;

    .line 92
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, v2, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mUserFirstName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v2, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mUserLastName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v2, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mFormattedName:Ljava/lang/String;

    .line 93
    const-string v4, "type"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v2, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mType:Ljava/lang/String;

    .line 94
    const-string v4, "relationship"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v2, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mRelationship:Ljava/lang/String;

    .line 95
    const-string v4, "friends"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v2, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mFriends:Ljava/lang/String;

    .line 96
    const-string v4, "homeCity"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v2, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mHomeCity:Ljava/lang/String;

    .line 97
    const-string v4, "gender"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v2, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mGender:Ljava/lang/String;

    .line 98
    const-string v4, "contact"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 99
    const-string v4, "contact"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/sns3/svc/sp/foursquare/parser/SnsFsParserContact;->parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseContact;

    move-result-object v0

    .line 100
    .local v0, "contact":Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseContact;
    iput-object v0, v2, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mContact:Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseContact;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 108
    .end local v0    # "contact":Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseContact;
    .end local v3    # "userResponse":Lorg/json/JSONObject;
    :cond_0
    :goto_0
    return-object v2

    .line 103
    :catch_0
    move-exception v1

    .line 104
    .local v1, "e":Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method
