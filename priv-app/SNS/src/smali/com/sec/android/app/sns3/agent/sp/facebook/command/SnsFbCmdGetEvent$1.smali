.class Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetEvent$1;
.super Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqGetEvent;
.source "SnsFbCmdGetEvent.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetEvent;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Handler;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetEvent;


# direct methods
.method constructor <init>(Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetEvent;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;)V
    .locals 0
    .param p2, "x0"    # Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    .param p3, "x1"    # Ljava/lang/String;

    .prologue
    .line 52
    iput-object p1, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetEvent$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetEvent;

    invoke-direct {p0, p2, p3}, Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqGetEvent;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public onReqRespond(IZIILandroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;)Z
    .locals 9
    .param p1, "reqID"    # I
    .param p2, "bSuccess"    # Z
    .param p3, "httpStatus"    # I
    .param p4, "errorCode"    # I
    .param p5, "reason"    # Landroid/os/Bundle;
    .param p6, "event"    # Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;

    .prologue
    const/4 v2, 0x0

    .line 58
    new-instance v7, Landroid/content/ContentValues;

    invoke-direct {v7}, Landroid/content/ContentValues;-><init>()V

    .line 59
    .local v7, "values":Landroid/content/ContentValues;
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/sns3/SnsApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 61
    .local v0, "cr":Landroid/content/ContentResolver;
    if-eqz p2, :cond_3

    .line 62
    if-eqz p6, :cond_1

    .line 63
    invoke-virtual {v7}, Landroid/content/ContentValues;->clear()V

    .line 65
    const-string v1, "event_id"

    iget-object v3, p6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;->mID:Ljava/lang/String;

    invoke-virtual {v7, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    iget-object v1, p6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;->mOwner:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFrom;

    if-eqz v1, :cond_0

    .line 68
    const-string v1, "owner_id"

    iget-object v3, p6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;->mOwner:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFrom;

    iget-object v3, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFrom;->mFromID:Ljava/lang/String;

    invoke-virtual {v7, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    const-string v1, "owner_name"

    iget-object v3, p6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;->mOwner:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFrom;

    iget-object v3, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFrom;->mFromName:Ljava/lang/String;

    invoke-virtual {v7, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    const-string v1, "_actor_photo_url"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "https://graph.facebook.com/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;->mOwner:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFrom;

    iget-object v4, v4, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFrom;->mFromID:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/picture?type=normal"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v7, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    :cond_0
    const-string v1, "name"

    iget-object v3, p6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;->mEventName:Ljava/lang/String;

    invoke-virtual {v7, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    const-string v1, "description"

    iget-object v3, p6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;->mDescription:Ljava/lang/String;

    invoke-virtual {v7, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    const-string v1, "start_time"

    iget-object v3, p6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;->mStartTime:Ljava/lang/String;

    invoke-static {v3}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->toTimestamp(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v7, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 77
    const-string v1, "end_time"

    iget-object v3, p6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;->mEndTime:Ljava/lang/String;

    invoke-static {v3}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->toTimestamp(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v7, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 78
    const-string v1, "location"

    iget-object v3, p6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;->mLocation:Ljava/lang/String;

    invoke-virtual {v7, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    const-string v1, "venue_id"

    iget-object v3, p6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;->mVenueID:Ljava/lang/String;

    invoke-virtual {v7, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    const-string v1, "street"

    iget-object v3, p6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;->mStreet:Ljava/lang/String;

    invoke-virtual {v7, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    const-string v1, "city"

    iget-object v3, p6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;->mCity:Ljava/lang/String;

    invoke-virtual {v7, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    const-string v1, "state"

    iget-object v3, p6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;->mState:Ljava/lang/String;

    invoke-virtual {v7, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    const-string v1, "zip"

    iget-object v3, p6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;->mZip:Ljava/lang/String;

    invoke-virtual {v7, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    const-string v1, "country"

    iget-object v3, p6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;->mCountry:Ljava/lang/String;

    invoke-virtual {v7, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    const-string v1, "latitude"

    iget-object v3, p6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;->mLatitude:Ljava/lang/String;

    invoke-virtual {v7, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    const-string v1, "longitude"

    iget-object v3, p6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;->mLongitude:Ljava/lang/String;

    invoke-virtual {v7, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    const-string v1, "privacy"

    iget-object v3, p6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;->mPrivacy:Ljava/lang/String;

    invoke-virtual {v7, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    const-string v1, "updated_time"

    iget-object v3, p6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;->mUpdatedTime:Ljava/lang/String;

    invoke-static {v3}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->toTimestamp(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v7, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 89
    const-string v1, "type"

    iget-object v3, p6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;->mType:Ljava/lang/String;

    invoke-virtual {v7, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 91
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    .line 92
    .local v8, "where":Ljava/lang/StringBuilder;
    const-string v1, "event_id"

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 93
    const-string v1, " = \'"

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 94
    iget-object v1, p6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;->mID:Ljava/lang/String;

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 95
    const-string v1, "\'"

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 97
    sget-object v1, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$Event;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 99
    .local v6, "c":Landroid/database/Cursor;
    if-eqz v6, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_2

    .line 100
    sget-object v1, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$Event;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v7, v3, v2}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 104
    :goto_0
    if-eqz v6, :cond_1

    .line 105
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 108
    .end local v6    # "c":Landroid/database/Cursor;
    .end local v8    # "where":Ljava/lang/StringBuilder;
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetEvent$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetEvent;

    sget-object v2, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$Event;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetEvent;->setUri(Ljava/lang/String;)V

    .line 115
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetEvent$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetEvent;

    invoke-virtual {v1, p2}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetEvent;->setSuccess(Z)V

    .line 116
    iget-object v1, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetEvent$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetEvent;

    invoke-virtual {v1, p0}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetEvent;->receive(Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;)V

    .line 118
    const/4 v1, 0x1

    return v1

    .line 102
    .restart local v6    # "c":Landroid/database/Cursor;
    .restart local v8    # "where":Ljava/lang/StringBuilder;
    :cond_2
    sget-object v1, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$Event;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1, v7}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    goto :goto_0

    .line 110
    .end local v6    # "c":Landroid/database/Cursor;
    .end local v8    # "where":Ljava/lang/StringBuilder;
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetEvent$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetEvent;

    new-instance v3, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;

    const-string v4, "facebook"

    invoke-direct {v3, v4, p3, p4, p5}, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;-><init>(Ljava/lang/String;IILandroid/os/Bundle;)V

    invoke-virtual {v1, v3}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetEvent;->setResponseList(Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;)Z

    .line 112
    iget-object v1, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetEvent$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetEvent;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetEvent;->setUri(Ljava/lang/String;)V

    goto :goto_1
.end method
