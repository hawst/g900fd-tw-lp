.class public Lcom/sec/android/app/sns3/svc/util/SnsUtil;
.super Ljava/lang/Object;
.source "SnsUtil.java"


# static fields
.field private static final DEBUG_KEY:Ljava/lang/String; = "debug_key"

.field private static final PLATFORM_KEY_SIGNATURE:Ljava/lang/String; = "308204d4308203bca003020102020900d20995a79c0daad6300d06092a864886f70d01010505003081a2310b3009060355040613024b52311430120603550408130b536f757468204b6f726561311330110603550407130a5375776f6e2043697479311c301a060355040a131353616d73756e6720436f72706f726174696f6e310c300a060355040b1303444d43311530130603550403130c53616d73756e6720436572743125302306092a864886f70d0109011616616e64726f69642e6f734073616d73756e672e636f6d301e170d3131303632323132323531325a170d3338313130373132323531325a3081a2310b3009060355040613024b52311430120603550408130b536f757468204b6f726561311330110603550407130a5375776f6e2043697479311c301a060355040a131353616d73756e6720436f72706f726174696f6e310c300a060355040b1303444d43311530130603550403130c53616d73756e6720436572743125302306092a864886f70d0109011616616e64726f69642e6f734073616d73756e672e636f6d30820120300d06092a864886f70d01010105000382010d00308201080282010100c986384a3e1f2fb206670e78ef232215c0d26f45a22728db99a44da11c35ac33a71fe071c4a2d6825a9b4c88b333ed96f3c5e6c666d60f3ee94c490885abcf8dc660f707aabc77ead3e2d0d8aee8108c15cd260f2e85042c28d2f292daa3c6da0c7bf2391db7841aade8fdf0c9d0defcf77124e6d2de0a9e0d2da746c3670e4ffcdc85b701bb4744861b96ff7311da3603c5a10336e55ffa34b4353eedc85f51015e1518c67e309e39f87639ff178107f109cd18411a6077f26964b6e63f8a70b9619db04306a323c1a1d23af867e19f14f570ffe573d0e3a0c2b30632aaec3173380994be1e341e3a90bd2e4b615481f46db39ea83816448ec35feb1735c1f3020103a382010b30820107301d0603551d0e04160414932c3af70b627a0c7610b5a0e7427d6cfaea3f1e3081d70603551d230481cf3081cc8014932c3af70b627a0c7610b5a0e7427d6cfaea3f1ea181a8a481a53081a2310b3009060355040613024b52311430120603550408130b536f757468204b6f726561311330110603550407130a5375776f6e2043697479311c301a060355040a131353616d73756e6720436f72706f726174696f6e310c300a060355040b1303444d43311530130603550403130c53616d73756e6720436572743125302306092a864886f70d0109011616616e64726f69642e6f734073616d73756e672e636f6d820900d20995a79c0daad6300c0603551d13040530030101ff300d06092a864886f70d01010505000382010100329601fe40e036a4a86cc5d49dd8c1b5415998e72637538b0d430369ac51530f63aace8c019a1a66616a2f1bb2c5fabd6f313261f380e3471623f053d9e3c53f5fd6d1965d7b000e4dc244c1b27e2fe9a323ff077f52c4675e86247aa801187137e30c9bbf01c567a4299db4bf0b25b7d7107a7b81ee102f72ff47950164e26752e114c42f8b9d2a42e7308897ec640ea1924ed13abbe9d120912b62f4926493a86db94c0b46f44c6161d58c2f648164890c512dfb28d42c855bf470dbee2dab6960cad04e81f71525ded46cdd0f359f99c460db9f007d96ce83b4b218ac2d82c48f12608d469733f05a3375594669ccbf8a495544d6c5701e9369c08c810158"

.field private static TAG:Ljava/lang/String; = null

.field private static final THEME_TW_DARK:I = 0x0

.field private static final THEME_TW_LIGHT:I = 0x1

.field private static mActionMenuTextColor:I

.field public static mAvailableSPCount:I

.field private static mDeviceThemeStyle:I

.field private static mbIsLoggable:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, -0x1

    .line 63
    const-string v0, "SnsUtil"

    sput-object v0, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->TAG:Ljava/lang/String;

    .line 99
    sput-boolean v2, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->mbIsLoggable:Z

    .line 105
    sput v1, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->mDeviceThemeStyle:I

    .line 107
    sput v1, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->mActionMenuTextColor:I

    .line 109
    sput v2, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->mAvailableSPCount:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static arrayToString([Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "array"    # [Ljava/lang/String;
    .param p1, "separator"    # Ljava/lang/String;

    .prologue
    .line 428
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 429
    :cond_0
    const/4 v2, 0x0

    .line 443
    :goto_0
    return-object v2

    .line 432
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 434
    .local v1, "result":Ljava/lang/StringBuilder;
    array-length v2, p0

    if-lez v2, :cond_2

    .line 435
    const/4 v2, 0x0

    aget-object v2, p0, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 437
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_1
    array-length v2, p0

    if-ge v0, v2, :cond_2

    .line 438
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 439
    aget-object v2, p0, v0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 437
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 443
    .end local v0    # "i":I
    :cond_2
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method public static bundle2QueryString(Landroid/os/Bundle;)Ljava/lang/String;
    .locals 6
    .param p0, "parameters"    # Landroid/os/Bundle;

    .prologue
    .line 391
    const/4 v0, 0x0

    .line 392
    .local v0, "cnt":I
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 394
    .local v3, "sb":Ljava/lang/StringBuilder;
    if-nez p0, :cond_0

    .line 395
    const/4 v4, 0x0

    .line 406
    :goto_0
    return-object v4

    .line 397
    :cond_0
    invoke-virtual {p0}, Landroid/os/Bundle;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_2

    .line 398
    invoke-virtual {p0}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 399
    .local v2, "key":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->encodeUrlEx(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 400
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0}, Landroid/os/Bundle;->size()I

    move-result v4

    if-eq v0, v4, :cond_1

    .line 401
    const-string v4, "&"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 406
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "key":Ljava/lang/String;
    :cond_2
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_0
.end method

.method public static checkAvailableSP()V
    .locals 8

    .prologue
    .line 622
    const/4 v6, 0x0

    sput v6, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->mAvailableSPCount:I

    .line 624
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v6

    invoke-static {v6}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v6

    const-string v7, "com.sec.android.app.sns3.facebook"

    invoke-virtual {v6, v7}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 626
    .local v0, "fbAccounts":[Landroid/accounts/Account;
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v6

    invoke-static {v6}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v6

    const-string v7, "com.sec.android.app.sns3.twitter"

    invoke-virtual {v6, v7}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v5

    .line 628
    .local v5, "twAccounts":[Landroid/accounts/Account;
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v6

    invoke-static {v6}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v6

    const-string v7, "com.sec.android.app.sns3.googleplus"

    invoke-virtual {v6, v7}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v2

    .line 630
    .local v2, "gpAccounts":[Landroid/accounts/Account;
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v6

    invoke-static {v6}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v6

    const-string v7, "com.sec.android.app.sns3.foursquare"

    invoke-virtual {v6, v7}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v1

    .line 632
    .local v1, "fsAccounts":[Landroid/accounts/Account;
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v6

    invoke-static {v6}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v6

    const-string v7, "com.sec.android.app.sns3.instagram"

    invoke-virtual {v6, v7}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v3

    .line 634
    .local v3, "inAccounts":[Landroid/accounts/Account;
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v6

    invoke-static {v6}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v6

    const-string v7, "com.sec.android.app.sns3.linkedin"

    invoke-virtual {v6, v7}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v4

    .line 637
    .local v4, "liAccounts":[Landroid/accounts/Account;
    array-length v6, v0

    if-lez v6, :cond_0

    .line 638
    sget v6, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->mAvailableSPCount:I

    add-int/lit8 v6, v6, 0x1

    sput v6, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->mAvailableSPCount:I

    .line 640
    :cond_0
    array-length v6, v5

    if-lez v6, :cond_1

    .line 641
    sget v6, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->mAvailableSPCount:I

    add-int/lit8 v6, v6, 0x1

    sput v6, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->mAvailableSPCount:I

    .line 643
    :cond_1
    array-length v6, v2

    if-lez v6, :cond_2

    .line 644
    sget v6, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->mAvailableSPCount:I

    add-int/lit8 v6, v6, 0x1

    sput v6, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->mAvailableSPCount:I

    .line 646
    :cond_2
    array-length v6, v1

    if-lez v6, :cond_3

    .line 647
    sget v6, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->mAvailableSPCount:I

    add-int/lit8 v6, v6, 0x1

    sput v6, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->mAvailableSPCount:I

    .line 649
    :cond_3
    array-length v6, v3

    if-lez v6, :cond_4

    .line 650
    sget v6, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->mAvailableSPCount:I

    add-int/lit8 v6, v6, 0x1

    sput v6, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->mAvailableSPCount:I

    .line 652
    :cond_4
    array-length v6, v4

    if-lez v6, :cond_5

    .line 653
    sget v6, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->mAvailableSPCount:I

    add-int/lit8 v6, v6, 0x1

    sput v6, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->mAvailableSPCount:I

    .line 656
    :cond_5
    return-void
.end method

.method public static checkAvailableSPChina()V
    .locals 4

    .prologue
    .line 660
    const/4 v2, 0x0

    sput v2, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->mAvailableSPCount:I

    .line 662
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v2

    invoke-static {v2}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v2

    const-string v3, "com.sec.android.app.sns3.qzone"

    invoke-virtual {v2, v3}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 664
    .local v0, "qzAccounts":[Landroid/accounts/Account;
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v2

    invoke-static {v2}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v2

    const-string v3, "com.sec.android.app.sns3.sinaweibo"

    invoke-virtual {v2, v3}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v1

    .line 667
    .local v1, "swAccounts":[Landroid/accounts/Account;
    array-length v2, v0

    if-lez v2, :cond_0

    .line 668
    sget v2, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->mAvailableSPCount:I

    add-int/lit8 v2, v2, 0x1

    sput v2, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->mAvailableSPCount:I

    .line 670
    :cond_0
    array-length v2, v1

    if-lez v2, :cond_1

    .line 671
    sget v2, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->mAvailableSPCount:I

    add-int/lit8 v2, v2, 0x1

    sput v2, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->mAvailableSPCount:I

    .line 674
    :cond_1
    return-void
.end method

.method private static checkSignatureOrSystem(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "requestingPackage"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x1

    .line 728
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v7

    const/16 v8, 0xc0

    invoke-virtual {v7, p1, v8}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v4

    .line 731
    .local v4, "pkgInfo":Landroid/content/pm/PackageInfo;
    iget-object v7, v4, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v7, v7, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/lit8 v7, v7, 0x1

    if-nez v7, :cond_0

    iget-object v7, v4, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v7, v7, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/lit16 v7, v7, 0x80

    if-eqz v7, :cond_1

    .line 746
    .end local v4    # "pkgInfo":Landroid/content/pm/PackageInfo;
    :cond_0
    :goto_0
    return v6

    .line 736
    .restart local v4    # "pkgInfo":Landroid/content/pm/PackageInfo;
    :cond_1
    iget-object v0, v4, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    .local v0, "arr$":[Landroid/content/pm/Signature;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_1
    if-ge v2, v3, :cond_2

    aget-object v5, v0, v2

    .line 737
    .local v5, "signature":Landroid/content/pm/Signature;
    const-string v7, "308204d4308203bca003020102020900d20995a79c0daad6300d06092a864886f70d01010505003081a2310b3009060355040613024b52311430120603550408130b536f757468204b6f726561311330110603550407130a5375776f6e2043697479311c301a060355040a131353616d73756e6720436f72706f726174696f6e310c300a060355040b1303444d43311530130603550403130c53616d73756e6720436572743125302306092a864886f70d0109011616616e64726f69642e6f734073616d73756e672e636f6d301e170d3131303632323132323531325a170d3338313130373132323531325a3081a2310b3009060355040613024b52311430120603550408130b536f757468204b6f726561311330110603550407130a5375776f6e2043697479311c301a060355040a131353616d73756e6720436f72706f726174696f6e310c300a060355040b1303444d43311530130603550403130c53616d73756e6720436572743125302306092a864886f70d0109011616616e64726f69642e6f734073616d73756e672e636f6d30820120300d06092a864886f70d01010105000382010d00308201080282010100c986384a3e1f2fb206670e78ef232215c0d26f45a22728db99a44da11c35ac33a71fe071c4a2d6825a9b4c88b333ed96f3c5e6c666d60f3ee94c490885abcf8dc660f707aabc77ead3e2d0d8aee8108c15cd260f2e85042c28d2f292daa3c6da0c7bf2391db7841aade8fdf0c9d0defcf77124e6d2de0a9e0d2da746c3670e4ffcdc85b701bb4744861b96ff7311da3603c5a10336e55ffa34b4353eedc85f51015e1518c67e309e39f87639ff178107f109cd18411a6077f26964b6e63f8a70b9619db04306a323c1a1d23af867e19f14f570ffe573d0e3a0c2b30632aaec3173380994be1e341e3a90bd2e4b615481f46db39ea83816448ec35feb1735c1f3020103a382010b30820107301d0603551d0e04160414932c3af70b627a0c7610b5a0e7427d6cfaea3f1e3081d70603551d230481cf3081cc8014932c3af70b627a0c7610b5a0e7427d6cfaea3f1ea181a8a481a53081a2310b3009060355040613024b52311430120603550408130b536f757468204b6f726561311330110603550407130a5375776f6e2043697479311c301a060355040a131353616d73756e6720436f72706f726174696f6e310c300a060355040b1303444d43311530130603550403130c53616d73756e6720436572743125302306092a864886f70d0109011616616e64726f69642e6f734073616d73756e672e636f6d820900d20995a79c0daad6300c0603551d13040530030101ff300d06092a864886f70d01010505000382010100329601fe40e036a4a86cc5d49dd8c1b5415998e72637538b0d430369ac51530f63aace8c019a1a66616a2f1bb2c5fabd6f313261f380e3471623f053d9e3c53f5fd6d1965d7b000e4dc244c1b27e2fe9a323ff077f52c4675e86247aa801187137e30c9bbf01c567a4299db4bf0b25b7d7107a7b81ee102f72ff47950164e26752e114c42f8b9d2a42e7308897ec640ea1924ed13abbe9d120912b62f4926493a86db94c0b46f44c6161d58c2f648164890c512dfb28d42c855bf470dbee2dab6960cad04e81f71525ded46cdd0f359f99c460db9f007d96ce83b4b218ac2d82c48f12608d469733f05a3375594669ccbf8a495544d6c5701e9369c08c810158"

    invoke-virtual {v5}, Landroid/content/pm/Signature;->toCharsString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v7

    if-nez v7, :cond_0

    .line 736
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 741
    .end local v0    # "arr$":[Landroid/content/pm/Signature;
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    .end local v4    # "pkgInfo":Landroid/content/pm/PackageInfo;
    .end local v5    # "signature":Landroid/content/pm/Signature;
    :catch_0
    move-exception v1

    .line 742
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    sget-object v6, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "can\'t find "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 746
    .end local v1    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_2
    :goto_2
    const/4 v6, 0x0

    goto :goto_0

    .line 743
    :catch_1
    move-exception v1

    .line 744
    .local v1, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v1}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_2
.end method

.method private static convert(Ljava/lang/String;Ljava/lang/StringBuilder;)V
    .locals 5
    .param p0, "s"    # Ljava/lang/String;
    .param p1, "buf"    # Ljava/lang/StringBuilder;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    .prologue
    .line 168
    const-string v1, "0123456789ABCDEF"

    .line 170
    .local v1, "digits":Ljava/lang/String;
    const-string v3, "UTF-8"

    invoke-virtual {p0, v3}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    .line 171
    .local v0, "bytes":[B
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_0
    array-length v3, v0

    if-ge v2, v3, :cond_0

    .line 172
    const/16 v3, 0x25

    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 173
    const-string v3, "0123456789ABCDEF"

    aget-byte v4, v0, v2

    and-int/lit16 v4, v4, 0xf0

    shr-int/lit8 v4, v4, 0x4

    invoke-virtual {v3, v4}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 174
    const-string v3, "0123456789ABCDEF"

    aget-byte v4, v0, v2

    and-int/lit8 v4, v4, 0xf

    invoke-virtual {v3, v4}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 171
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 176
    :cond_0
    return-void
.end method

.method public static decodeUrl2Bundle(Ljava/lang/String;)Landroid/os/Bundle;
    .locals 9
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    .line 187
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 188
    .local v5, "params":Landroid/os/Bundle;
    if-eqz p0, :cond_1

    .line 189
    const-string v7, "&"

    invoke-virtual {p0, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 190
    .local v1, "array":[Ljava/lang/String;
    move-object v0, v1

    .local v0, "arr$":[Ljava/lang/String;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v4, v0, v2

    .line 191
    .local v4, "parameter":Ljava/lang/String;
    const-string v7, "="

    invoke-virtual {v4, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    .line 195
    .local v6, "v":[Ljava/lang/String;
    array-length v7, v6

    const/4 v8, 0x2

    if-lt v7, v8, :cond_0

    .line 197
    const/4 v7, 0x0

    aget-object v7, v6, v7

    invoke-static {v7}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x1

    aget-object v8, v6, v8

    invoke-static {v8}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v7, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 190
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 201
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v1    # "array":[Ljava/lang/String;
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    .end local v4    # "parameter":Ljava/lang/String;
    .end local v6    # "v":[Ljava/lang/String;
    :cond_1
    return-object v5
.end method

.method public static encodeUrl(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "in"    # Ljava/lang/String;

    .prologue
    .line 112
    const/4 v1, 0x0

    .line 114
    .local v1, "value":Ljava/lang/String;
    :try_start_0
    const-string v2, " "

    const-string v3, "%20"

    invoke-virtual {p0, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p0

    .line 115
    const-string v2, "UTF-8"

    invoke-static {p0, v2}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 121
    :goto_0
    return-object v1

    .line 116
    :catch_0
    move-exception v0

    .line 118
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static encodeUrlEx(Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    .line 126
    if-nez p0, :cond_0

    .line 127
    new-instance v6, Ljava/lang/NullPointerException;

    invoke-direct {v6}, Ljava/lang/NullPointerException;-><init>()V

    throw v6

    .line 129
    :cond_0
    const/4 v0, 0x0

    .line 132
    .local v0, "buf":Ljava/lang/StringBuilder;
    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x10

    invoke-direct {v1, v6}, Ljava/lang/StringBuilder;-><init>(I)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_1

    .line 133
    .end local v0    # "buf":Ljava/lang/StringBuilder;
    .local v1, "buf":Ljava/lang/StringBuilder;
    const/4 v5, -0x1

    .line 134
    .local v5, "start":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    :try_start_1
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v6

    if-ge v4, v6, :cond_9

    .line 135
    invoke-virtual {p0, v4}, Ljava/lang/String;->charAt(I)C

    move-result v2

    .line 138
    .local v2, "ch":C
    const/16 v6, 0x61

    if-lt v2, v6, :cond_1

    const/16 v6, 0x7a

    if-le v2, v6, :cond_4

    :cond_1
    const/16 v6, 0x41

    if-lt v2, v6, :cond_2

    const/16 v6, 0x5a

    if-le v2, v6, :cond_4

    :cond_2
    const/16 v6, 0x30

    if-lt v2, v6, :cond_3

    const/16 v6, 0x39

    if-le v2, v6, :cond_4

    :cond_3
    const-string v6, "-._~"

    invoke-virtual {v6, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v6

    const/4 v7, -0x1

    if-le v6, v7, :cond_8

    .line 140
    :cond_4
    if-ltz v5, :cond_5

    .line 141
    invoke-virtual {p0, v5, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6, v1}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->convert(Ljava/lang/String;Ljava/lang/StringBuilder;)V

    .line 142
    const/4 v5, -0x1

    .line 144
    :cond_5
    const/16 v6, 0x20

    if-eq v2, v6, :cond_7

    .line 145
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 134
    :cond_6
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 147
    :cond_7
    const-string v6, "%20"

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 159
    .end local v2    # "ch":C
    :catch_0
    move-exception v3

    move-object v0, v1

    .line 161
    .end local v1    # "buf":Ljava/lang/StringBuilder;
    .end local v4    # "i":I
    .end local v5    # "start":I
    .restart local v0    # "buf":Ljava/lang/StringBuilder;
    .local v3, "e":Ljava/io/UnsupportedEncodingException;
    :goto_2
    invoke-virtual {v3}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    .line 164
    .end local v3    # "e":Ljava/io/UnsupportedEncodingException;
    :goto_3
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    return-object v6

    .line 150
    .end local v0    # "buf":Ljava/lang/StringBuilder;
    .restart local v1    # "buf":Ljava/lang/StringBuilder;
    .restart local v2    # "ch":C
    .restart local v4    # "i":I
    .restart local v5    # "start":I
    :cond_8
    if-gez v5, :cond_6

    .line 151
    move v5, v4

    goto :goto_1

    .line 155
    .end local v2    # "ch":C
    :cond_9
    if-ltz v5, :cond_a

    .line 156
    :try_start_2
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v6

    invoke-virtual {p0, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6, v1}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->convert(Ljava/lang/String;Ljava/lang/StringBuilder;)V
    :try_end_2
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_2 .. :try_end_2} :catch_0

    :cond_a
    move-object v0, v1

    .line 162
    .end local v1    # "buf":Ljava/lang/StringBuilder;
    .restart local v0    # "buf":Ljava/lang/StringBuilder;
    goto :goto_3

    .line 159
    .end local v4    # "i":I
    .end local v5    # "start":I
    :catch_1
    move-exception v3

    goto :goto_2
.end method

.method public static generateNonce()Ljava/lang/String;
    .locals 2

    .prologue
    .line 183
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    invoke-virtual {v0}, Ljava/util/Random;->nextLong()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static generateTimestamp()Ljava/lang/String;
    .locals 4

    .prologue
    .line 179
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static hasAccessToRestrictedData(Landroid/content/Context;)Z
    .locals 8
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 709
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v5

    .line 711
    .local v5, "uid":I
    if-eqz p0, :cond_2

    .line 712
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    invoke-virtual {v6, v5}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;

    move-result-object v2

    .line 713
    .local v2, "callerPackages":[Ljava/lang/String;
    move-object v0, v2

    .local v0, "arr$":[Ljava/lang/String;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_1

    aget-object v1, v0, v3

    .line 714
    .local v1, "callerPackage":Ljava/lang/String;
    invoke-static {p0, v1}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->checkSignatureOrSystem(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 715
    const/4 v6, 0x1

    .line 723
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v1    # "callerPackage":Ljava/lang/String;
    .end local v2    # "callerPackages":[Ljava/lang/String;
    .end local v3    # "i$":I
    .end local v4    # "len$":I
    :goto_1
    return v6

    .line 713
    .restart local v0    # "arr$":[Ljava/lang/String;
    .restart local v1    # "callerPackage":Ljava/lang/String;
    .restart local v2    # "callerPackages":[Ljava/lang/String;
    .restart local v3    # "i$":I
    .restart local v4    # "len$":I
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 718
    .end local v1    # "callerPackage":Ljava/lang/String;
    :cond_1
    sget-object v6, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->TAG:Ljava/lang/String;

    const-string v7, "Refuse unknown access"

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 723
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v2    # "callerPackages":[Ljava/lang/String;
    .end local v3    # "i$":I
    .end local v4    # "len$":I
    :goto_2
    const/4 v6, 0x0

    goto :goto_1

    .line 720
    :cond_2
    sget-object v6, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->TAG:Ljava/lang/String;

    const-string v7, "context is null"

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method public static initDebugKey(Landroid/content/Context;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 466
    const-string v1, "debug_key"

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 468
    .local v0, "DebugKeyPref":Landroid/content/SharedPreferences;
    const-string v1, "debug_key"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    sput-boolean v1, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->mbIsLoggable:Z

    .line 469
    return-void
.end method

.method public static isActionbarLightTheme(Landroid/content/Context;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 578
    sget v0, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->mActionMenuTextColor:I

    if-gez v0, :cond_0

    .line 579
    invoke-static {p0}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->parseDeviceTheme(Landroid/content/Context;)V

    .line 582
    :cond_0
    sget v0, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->mActionMenuTextColor:I

    const v1, 0x888888

    if-ge v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isJsonArray(Ljava/lang/String;)Z
    .locals 3
    .param p0, "content"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 382
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 383
    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v1

    const/16 v2, 0x5b

    if-ne v1, v2, :cond_0

    .line 384
    const/4 v0, 0x1

    .line 387
    :cond_0
    return v0
.end method

.method public static isJsonEmpty(Ljava/lang/String;)Z
    .locals 4
    .param p0, "content"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 448
    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 452
    :cond_0
    :goto_0
    return v0

    .line 450
    :cond_1
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x5b

    if-ne v2, v3, :cond_2

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v3, 0x2

    if-le v2, v3, :cond_0

    :cond_2
    move v0, v1

    .line 452
    goto :goto_0
.end method

.method public static isLightTheme(Landroid/content/Context;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x1

    .line 570
    sget v1, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->mDeviceThemeStyle:I

    if-gez v1, :cond_0

    .line 571
    invoke-static {p0}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->parseDeviceTheme(Landroid/content/Context;)V

    .line 574
    :cond_0
    sget v1, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->mDeviceThemeStyle:I

    if-ne v1, v0, :cond_1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isLoggable()Z
    .locals 1

    .prologue
    .line 472
    sget-boolean v0, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->mbIsLoggable:Z

    return v0
.end method

.method public static notiUrl2Bundle(Ljava/lang/String;)Landroid/os/Bundle;
    .locals 15
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    .line 205
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 206
    .local v6, "params":Landroid/os/Bundle;
    if-eqz p0, :cond_5

    .line 207
    const-string v12, "[?&#]"

    invoke-virtual {p0, v12}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 208
    .local v1, "data":[Ljava/lang/String;
    const-string v12, "[/\\.]"

    invoke-virtual {p0, v12}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v11

    .line 210
    .local v11, "uris":[Ljava/lang/String;
    move-object v0, v1

    .local v0, "arr$":[Ljava/lang/String;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_3

    aget-object v5, v0, v2

    .line 211
    .local v5, "parameter":Ljava/lang/String;
    const-string v12, "="

    invoke-virtual {v5, v12}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v8

    .line 212
    .local v8, "states":[Ljava/lang/String;
    array-length v12, v8

    if-nez v12, :cond_1

    .line 210
    :cond_0
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 214
    :cond_1
    array-length v12, v8

    const/4 v13, 0x1

    if-ne v12, v13, :cond_2

    .line 215
    const/4 v12, 0x0

    aget-object v12, v8, v12

    const-string v13, "[/\\.]"

    invoke-virtual {v12, v13}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 216
    .local v4, "name":[Ljava/lang/String;
    array-length v12, v4

    const/4 v13, 0x5

    if-le v12, v13, :cond_0

    .line 217
    const-string v12, "TYPE"

    const/4 v13, 0x0

    aget-object v13, v8, v13

    const-string v14, "[/\\.]"

    invoke-virtual {v13, v14}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v13

    const/4 v14, 0x5

    aget-object v13, v13, v14

    invoke-virtual {v6, v12, v13}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 220
    .end local v4    # "name":[Ljava/lang/String;
    :cond_2
    array-length v12, v8

    const/4 v13, 0x2

    if-ne v12, v13, :cond_0

    .line 221
    const/4 v12, 0x0

    aget-object v12, v8, v12

    const/4 v13, 0x1

    aget-object v13, v8, v13

    invoke-virtual {v6, v12, v13}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 227
    .end local v5    # "parameter":Ljava/lang/String;
    .end local v8    # "states":[Ljava/lang/String;
    :cond_3
    const-string v12, "\\.php"

    invoke-virtual {p0, v12}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    .line 228
    .local v7, "php":[Ljava/lang/String;
    array-length v12, v7

    if-lez v12, :cond_4

    .line 229
    const/4 v12, 0x0

    aget-object v12, v7, v12

    const-string v13, "/"

    invoke-virtual {v12, v13}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v10

    .line 230
    .local v10, "uphp":[Ljava/lang/String;
    const-string v12, ".php"

    array-length v13, v10

    add-int/lit8 v13, v13, -0x1

    aget-object v13, v10, v13

    invoke-virtual {v6, v12, v13}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 233
    .end local v10    # "uphp":[Ljava/lang/String;
    :cond_4
    const-string v12, "TYPE"

    invoke-virtual {v6, v12}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 234
    .local v9, "type":Ljava/lang/String;
    const-string v12, "photo"

    invoke-virtual {v12, v9}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_6

    .line 235
    const-string v12, "ID"

    const-string v13, "pid"

    invoke-virtual {v6, v13}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v6, v12, v13}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 259
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v1    # "data":[Ljava/lang/String;
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    .end local v7    # "php":[Ljava/lang/String;
    .end local v9    # "type":Ljava/lang/String;
    .end local v11    # "uris":[Ljava/lang/String;
    :cond_5
    :goto_2
    return-object v6

    .line 236
    .restart local v0    # "arr$":[Ljava/lang/String;
    .restart local v1    # "data":[Ljava/lang/String;
    .restart local v2    # "i$":I
    .restart local v3    # "len$":I
    .restart local v7    # "php":[Ljava/lang/String;
    .restart local v9    # "type":Ljava/lang/String;
    .restart local v11    # "uris":[Ljava/lang/String;
    :cond_6
    const-string v12, "profile"

    invoke-virtual {v12, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_7

    .line 237
    const-string v12, "ID"

    const-string v13, "id"

    invoke-virtual {v6, v13}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v6, v12, v13}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 238
    :cond_7
    const-string v12, "permalink"

    invoke-virtual {v12, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_8

    .line 239
    const-string v12, "ID"

    const-string v13, "id"

    invoke-virtual {v6, v13}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v6, v12, v13}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 240
    :cond_8
    const-string v12, "group"

    invoke-virtual {v12, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_9

    .line 241
    const-string v12, "ID"

    const-string v13, "gid"

    invoke-virtual {v6, v13}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v6, v12, v13}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 242
    :cond_9
    const-string v12, "event"

    invoke-virtual {v12, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_a

    .line 243
    const-string v12, "ID"

    const-string v13, "eid"

    invoke-virtual {v6, v13}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v6, v12, v13}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 244
    :cond_a
    const-string v12, "album"

    invoke-virtual {v12, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_b

    .line 245
    const-string v12, "ID"

    const-string v13, "aid"

    invoke-virtual {v6, v13}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v6, v12, v13}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 246
    :cond_b
    const-string v12, "note"

    invoke-virtual {v12, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_c

    .line 247
    const-string v12, "ID"

    const-string v13, "note_id"

    invoke-virtual {v6, v13}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v6, v12, v13}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 248
    :cond_c
    const-string v12, "notes"

    invoke-virtual {v12, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_d

    .line 249
    const-string v12, "TYPE"

    const-string v13, "note"

    invoke-virtual {v6, v12, v13}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 250
    const-string v12, "note_id"

    array-length v13, v11

    add-int/lit8 v13, v13, -0x1

    aget-object v13, v11, v13

    invoke-virtual {v6, v12, v13}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 251
    const-string v12, "ID"

    const-string v13, "note_id"

    invoke-virtual {v6, v13}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v6, v12, v13}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 252
    :cond_d
    const-string v12, "video"

    invoke-virtual {v12, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_e

    .line 253
    const-string v12, "ID"

    const-string v13, "v"

    invoke-virtual {v6, v13}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v6, v12, v13}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 254
    :cond_e
    const-string v12, "l"

    invoke-virtual {v12, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_5

    .line 255
    const-string v12, "TYPE"

    const-string v13, "link"

    invoke-virtual {v6, v12, v13}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 256
    const-string v12, "ID"

    const-string v13, "u"

    invoke-virtual {v6, v13}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v6, v12, v13}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2
.end method

.method public static pagingUrl2Bundle(Ljava/lang/String;)Landroid/os/Bundle;
    .locals 11
    .param p0, "inputUrl"    # Ljava/lang/String;

    .prologue
    const/4 v10, 0x1

    .line 411
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 413
    .local v6, "params":Landroid/os/Bundle;
    if-eqz p0, :cond_1

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_1

    .line 414
    const-string v8, "\\?"

    invoke-virtual {p0, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 415
    .local v1, "array1":[Ljava/lang/String;
    aget-object v8, v1, v10

    const-string v9, "&"

    invoke-virtual {v8, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 416
    .local v2, "array2":[Ljava/lang/String;
    move-object v0, v2

    .local v0, "arr$":[Ljava/lang/String;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_1

    aget-object v5, v0, v3

    .line 417
    .local v5, "parameter":Ljava/lang/String;
    if-eqz v5, :cond_0

    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    if-lez v8, :cond_0

    .line 418
    const-string v8, "="

    invoke-virtual {v5, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    .line 419
    .local v7, "v":[Ljava/lang/String;
    const/4 v8, 0x0

    aget-object v8, v7, v8

    invoke-static {v8}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    aget-object v9, v7, v10

    invoke-static {v9}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v8, v9}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 416
    .end local v7    # "v":[Ljava/lang/String;
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 424
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v1    # "array1":[Ljava/lang/String;
    .end local v2    # "array2":[Ljava/lang/String;
    .end local v3    # "i$":I
    .end local v4    # "len$":I
    .end local v5    # "parameter":Ljava/lang/String;
    :cond_1
    return-object v6
.end method

.method private static parseDeviceTheme(Landroid/content/Context;)V
    .locals 14
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const v13, 0xffffff

    const/4 v12, 0x1

    const/4 v11, 0x0

    .line 586
    const-string v1, "com.android.settings"

    .line 589
    .local v1, "appName":Ljava/lang/String;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    const-string v7, "com.android.settings"

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    .line 590
    .local v0, "appInfo":Landroid/content/pm/ApplicationInfo;
    const-string v6, "com.android.settings"

    const/4 v7, 0x0

    invoke-virtual {p0, v6, v7}, Landroid/content/Context;->createPackageContext(Ljava/lang/String;I)Landroid/content/Context;

    move-result-object v4

    .line 591
    .local v4, "settingsApp":Landroid/content/Context;
    new-instance v6, Landroid/view/ContextThemeWrapper;

    iget v7, v0, Landroid/content/pm/ApplicationInfo;->theme:I

    invoke-direct {v6, v4, v7}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v6}, Landroid/view/ContextThemeWrapper;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v5

    .line 594
    .local v5, "settingsTheme":Landroid/content/res/Resources$Theme;
    new-instance v3, Landroid/util/TypedValue;

    invoke-direct {v3}, Landroid/util/TypedValue;-><init>()V

    .line 595
    .local v3, "outValue":Landroid/util/TypedValue;
    const v6, 0x1010085

    const/4 v7, 0x1

    invoke-virtual {v5, v6, v3, v7}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 596
    iget v6, v3, Landroid/util/TypedValue;->resourceId:I

    sparse-switch v6, :sswitch_data_0

    .line 604
    const/4 v6, 0x0

    sput v6, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->mDeviceThemeStyle:I

    .line 606
    :goto_0
    sget-object v6, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->TAG:Ljava/lang/String;

    const-string v7, "webViewStyle = 0x%x, deviceThemeStyle = %d"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    iget v10, v3, Landroid/util/TypedValue;->resourceId:I

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x1

    sget v10, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->mDeviceThemeStyle:I

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 609
    const v6, 0x1010361

    const/4 v7, 0x1

    invoke-virtual {v5, v6, v3, v7}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 610
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    iget v7, v3, Landroid/util/TypedValue;->resourceId:I

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    and-int/2addr v6, v13

    sput v6, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->mActionMenuTextColor:I

    .line 611
    sget-object v6, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->TAG:Ljava/lang/String;

    const-string v7, "actionMenuTextColor = 0x%08x, %6x"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    iget v10, v3, Landroid/util/TypedValue;->resourceId:I

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x1

    sget v10, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->mActionMenuTextColor:I

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 618
    .end local v0    # "appInfo":Landroid/content/pm/ApplicationInfo;
    .end local v3    # "outValue":Landroid/util/TypedValue;
    .end local v4    # "settingsApp":Landroid/content/Context;
    .end local v5    # "settingsTheme":Landroid/content/res/Resources$Theme;
    :goto_1
    return-void

    .line 599
    .restart local v0    # "appInfo":Landroid/content/pm/ApplicationInfo;
    .restart local v3    # "outValue":Landroid/util/TypedValue;
    .restart local v4    # "settingsApp":Landroid/content/Context;
    .restart local v5    # "settingsTheme":Landroid/content/res/Resources$Theme;
    :sswitch_0
    const/4 v6, 0x1

    sput v6, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->mDeviceThemeStyle:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 612
    .end local v0    # "appInfo":Landroid/content/pm/ApplicationInfo;
    .end local v3    # "outValue":Landroid/util/TypedValue;
    .end local v4    # "settingsApp":Landroid/content/Context;
    .end local v5    # "settingsTheme":Landroid/content/res/Resources$Theme;
    :catch_0
    move-exception v2

    .line 613
    .local v2, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    sget-object v6, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->TAG:Ljava/lang/String;

    const-string v7, "%s not found"

    new-array v8, v12, [Ljava/lang/Object;

    const-string v9, "com.android.settings"

    aput-object v9, v8, v11

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 615
    sput v11, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->mDeviceThemeStyle:I

    .line 616
    sput v13, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->mActionMenuTextColor:I

    goto :goto_1

    .line 596
    :sswitch_data_0
    .sparse-switch
        0x10300d7 -> :sswitch_0
        0x1030197 -> :sswitch_0
    .end sparse-switch
.end method

.method public static parseQueryParam(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 9
    .param p0, "selection"    # Ljava/lang/String;
    .param p1, "selectionArgs"    # [Ljava/lang/String;
    .param p2, "param"    # Ljava/lang/String;

    .prologue
    const/4 v8, 0x1

    .line 677
    const/4 v3, 0x0

    .line 678
    .local v3, "parsed":Ljava/lang/String;
    if-eqz p0, :cond_2

    .line 679
    const-string v6, "[\\w_]+\\s*[=><][\\s\']*[\\?\\w.@_-]+"

    invoke-static {v6}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v5

    .line 681
    .local v5, "pattern":Ljava/util/regex/Pattern;
    invoke-virtual {v5, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    .line 682
    .local v1, "matcher":Ljava/util/regex/Matcher;
    const/4 v2, -0x1

    .line 683
    .local v2, "paramIndex":I
    :cond_0
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->find()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 684
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->group()Ljava/lang/String;

    move-result-object v0

    .line 685
    .local v0, "condition":Ljava/lang/String;
    invoke-virtual {v0, p2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 686
    const-string v6, "\\s*=\\s*"

    invoke-virtual {v0, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 687
    .local v4, "parts":[Ljava/lang/String;
    array-length v6, v4

    const/4 v7, 0x2

    if-ne v6, v7, :cond_1

    .line 688
    const-string v6, "?"

    aget-object v7, v4, v8

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 689
    add-int/lit8 v2, v2, 0x1

    .line 700
    .end local v0    # "condition":Ljava/lang/String;
    .end local v4    # "parts":[Ljava/lang/String;
    :cond_1
    :goto_0
    if-nez v3, :cond_2

    if-eqz p1, :cond_2

    .line 701
    aget-object v3, p1, v2

    .line 705
    .end local v1    # "matcher":Ljava/util/regex/Matcher;
    .end local v2    # "paramIndex":I
    .end local v5    # "pattern":Ljava/util/regex/Pattern;
    :cond_2
    return-object v3

    .line 691
    .restart local v0    # "condition":Ljava/lang/String;
    .restart local v1    # "matcher":Ljava/util/regex/Matcher;
    .restart local v2    # "paramIndex":I
    .restart local v4    # "parts":[Ljava/lang/String;
    .restart local v5    # "pattern":Ljava/util/regex/Pattern;
    :cond_3
    aget-object v6, v4, v8

    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    .line 692
    const-string v6, "\'"

    invoke-virtual {v3, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 693
    const-string v6, "\'"

    const-string v7, ""

    invoke-virtual {v3, v6, v7}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method

.method public static setDebugKey(Landroid/content/Context;Z)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "checkKey"    # Z

    .prologue
    .line 456
    const-string v2, "debug_key"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 458
    .local v0, "DebugKeyPref":Landroid/content/SharedPreferences;
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 459
    .local v1, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "debug_key"

    invoke-interface {v1, v2, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 460
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 462
    sput-boolean p1, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->mbIsLoggable:Z

    .line 463
    return-void
.end method

.method public static toTimestamp(Ljava/lang/String;)Ljava/lang/Long;
    .locals 6
    .param p0, "parsedTime"    # Ljava/lang/String;

    .prologue
    .line 263
    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string v4, "yyyy-MM-dd\'T\'HH:mm:ssZ"

    invoke-direct {v2, v4}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 264
    .local v2, "formatter":Ljava/text/SimpleDateFormat;
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    .line 267
    .local v0, "date":Ljava/util/Date;
    :try_start_0
    invoke-virtual {v2, p0}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 272
    :goto_0
    new-instance v3, Ljava/sql/Timestamp;

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    invoke-direct {v3, v4, v5}, Ljava/sql/Timestamp;-><init>(J)V

    .line 273
    .local v3, "timestamp":Ljava/sql/Timestamp;
    invoke-virtual {v3}, Ljava/sql/Timestamp;->getTime()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    return-object v4

    .line 268
    .end local v3    # "timestamp":Ljava/sql/Timestamp;
    :catch_0
    move-exception v1

    .line 269
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static toTimestamp(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Long;
    .locals 18
    .param p0, "parsedTime"    # Ljava/lang/String;
    .param p1, "startTime"    # Ljava/lang/String;

    .prologue
    .line 477
    const-string v3, "[0-9]{4}-[0-9]{2}-[0-9]{2}"

    .line 478
    .local v3, "Date_format":Ljava/lang/String;
    const/16 v12, 0xa

    .line 479
    .local v12, "yyyyMMdd_format_len":I
    const/16 v11, 0x13

    .line 480
    .local v11, "yyyyMMddTHHmmss_format_len":I
    const/16 v10, 0x18

    .line 482
    .local v10, "yyyyMMddTHHmmssZ_format_len":I
    invoke-static {v3}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v8

    .line 483
    .local v8, "pattern":Ljava/util/regex/Pattern;
    const-wide/16 v14, 0x0

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    .line 484
    .local v9, "timestamp":Ljava/lang/Long;
    const/4 v6, 0x0

    .line 488
    .local v6, "endTime":I
    sget-object v13, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->TAG:Ljava/lang/String;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "toTimestamp: parsedTime = "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ", startTime = "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 490
    move-object/from16 v0, p0

    invoke-virtual {v8, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v13

    invoke-virtual {v13}, Ljava/util/regex/Matcher;->find()Z

    move-result v13

    if-eqz v13, :cond_3

    .line 492
    sget-object v13, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->TAG:Ljava/lang/String;

    const-string v14, "toTimestamp: parsedTime is DATE Format"

    invoke-static {v13, v14}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 494
    const/4 v7, 0x0

    .line 496
    .local v7, "formatter":Ljava/text/SimpleDateFormat;
    invoke-virtual/range {p0 .. p0}, Ljava/lang/String;->length()I

    move-result v13

    if-ne v13, v12, :cond_1

    .line 497
    new-instance v7, Ljava/text/SimpleDateFormat;

    .end local v7    # "formatter":Ljava/text/SimpleDateFormat;
    const-string v13, "yyyy-MM-dd"

    invoke-direct {v7, v13}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 504
    .restart local v7    # "formatter":Ljava/text/SimpleDateFormat;
    :cond_0
    :goto_0
    new-instance v4, Ljava/util/Date;

    invoke-direct {v4}, Ljava/util/Date;-><init>()V

    .line 507
    .local v4, "date":Ljava/util/Date;
    :try_start_0
    move-object/from16 v0, p0

    invoke-virtual {v7, v0}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 519
    :goto_1
    new-instance v2, Ljava/sql/Timestamp;

    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v14

    invoke-direct {v2, v14, v15}, Ljava/sql/Timestamp;-><init>(J)V

    .line 521
    .local v2, "Caltimestamp":Ljava/sql/Timestamp;
    invoke-virtual {v2}, Ljava/sql/Timestamp;->getTime()J

    move-result-wide v14

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    .line 564
    .end local v2    # "Caltimestamp":Ljava/sql/Timestamp;
    .end local v4    # "date":Ljava/util/Date;
    .end local v7    # "formatter":Ljava/text/SimpleDateFormat;
    :goto_2
    sget-object v13, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->TAG:Ljava/lang/String;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "toTimestamp: timestamp return value = "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 566
    return-object v9

    .line 498
    .restart local v7    # "formatter":Ljava/text/SimpleDateFormat;
    :cond_1
    invoke-virtual/range {p0 .. p0}, Ljava/lang/String;->length()I

    move-result v13

    if-ne v13, v10, :cond_2

    .line 499
    new-instance v7, Ljava/text/SimpleDateFormat;

    .end local v7    # "formatter":Ljava/text/SimpleDateFormat;
    const-string v13, "yyyy-MM-dd\'T\'HH:mm:ssZ"

    invoke-direct {v7, v13}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .restart local v7    # "formatter":Ljava/text/SimpleDateFormat;
    goto :goto_0

    .line 500
    :cond_2
    invoke-virtual/range {p0 .. p0}, Ljava/lang/String;->length()I

    move-result v13

    if-ne v13, v11, :cond_0

    .line 501
    new-instance v7, Ljava/text/SimpleDateFormat;

    .end local v7    # "formatter":Ljava/text/SimpleDateFormat;
    const-string v13, "yyyy-MM-dd\'T\'HH:mm:ss"

    invoke-direct {v7, v13}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .restart local v7    # "formatter":Ljava/text/SimpleDateFormat;
    goto :goto_0

    .line 508
    .restart local v4    # "date":Ljava/util/Date;
    :catch_0
    move-exception v5

    .line 509
    .local v5, "e":Ljava/lang/Exception;
    invoke-virtual {v5}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 523
    .end local v4    # "date":Ljava/util/Date;
    .end local v5    # "e":Ljava/lang/Exception;
    .end local v7    # "formatter":Ljava/text/SimpleDateFormat;
    :cond_3
    const-string v13, "null"

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_7

    if-eqz p1, :cond_7

    .line 525
    sget-object v13, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->TAG:Ljava/lang/String;

    const-string v14, "toTimestamp: parsedTime is \"null\""

    invoke-static {v13, v14}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 527
    const/4 v7, 0x0

    .line 529
    .restart local v7    # "formatter":Ljava/text/SimpleDateFormat;
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v13

    if-ne v13, v12, :cond_5

    .line 530
    new-instance v7, Ljava/text/SimpleDateFormat;

    .end local v7    # "formatter":Ljava/text/SimpleDateFormat;
    const-string v13, "yyyy-MM-dd"

    invoke-direct {v7, v13}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 531
    .restart local v7    # "formatter":Ljava/text/SimpleDateFormat;
    const v6, 0x15180

    .line 540
    :cond_4
    :goto_3
    new-instance v4, Ljava/util/Date;

    invoke-direct {v4}, Ljava/util/Date;-><init>()V

    .line 543
    .restart local v4    # "date":Ljava/util/Date;
    :try_start_1
    move-object/from16 v0, p1

    invoke-virtual {v7, v0}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v4

    .line 555
    :goto_4
    new-instance v2, Ljava/sql/Timestamp;

    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v14

    invoke-direct {v2, v14, v15}, Ljava/sql/Timestamp;-><init>(J)V

    .line 557
    .restart local v2    # "Caltimestamp":Ljava/sql/Timestamp;
    invoke-virtual {v2}, Ljava/sql/Timestamp;->getTime()J

    move-result-wide v14

    mul-int/lit16 v13, v6, 0x3e8

    int-to-long v0, v13

    move-wide/from16 v16, v0

    add-long v14, v14, v16

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    .line 559
    goto/16 :goto_2

    .line 532
    .end local v2    # "Caltimestamp":Ljava/sql/Timestamp;
    .end local v4    # "date":Ljava/util/Date;
    :cond_5
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v13

    if-ne v13, v10, :cond_6

    .line 533
    new-instance v7, Ljava/text/SimpleDateFormat;

    .end local v7    # "formatter":Ljava/text/SimpleDateFormat;
    const-string v13, "yyyy-MM-dd\'T\'HH:mm:ssZ"

    invoke-direct {v7, v13}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 534
    .restart local v7    # "formatter":Ljava/text/SimpleDateFormat;
    const/4 v6, 0x0

    goto :goto_3

    .line 535
    :cond_6
    invoke-virtual/range {p0 .. p0}, Ljava/lang/String;->length()I

    move-result v13

    if-ne v13, v11, :cond_4

    .line 536
    new-instance v7, Ljava/text/SimpleDateFormat;

    .end local v7    # "formatter":Ljava/text/SimpleDateFormat;
    const-string v13, "yyyy-MM-dd\'T\'HH:mm:ss"

    invoke-direct {v7, v13}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 537
    .restart local v7    # "formatter":Ljava/text/SimpleDateFormat;
    const/4 v6, 0x0

    goto :goto_3

    .line 544
    .restart local v4    # "date":Ljava/util/Date;
    :catch_1
    move-exception v5

    .line 545
    .restart local v5    # "e":Ljava/lang/Exception;
    invoke-virtual {v5}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_4

    .line 561
    .end local v4    # "date":Ljava/util/Date;
    .end local v5    # "e":Ljava/lang/Exception;
    .end local v7    # "formatter":Ljava/text/SimpleDateFormat;
    :cond_7
    invoke-static/range {p0 .. p0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v14

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    goto/16 :goto_2
.end method

.method public static toTimestampForBirthday(Ljava/lang/String;)Ljava/lang/Long;
    .locals 12
    .param p0, "parsedTime"    # Ljava/lang/String;

    .prologue
    .line 278
    const-string v5, "(\\d{2})(\\/)(\\d{2})(\\/)(\\d{4})"

    .line 280
    .local v5, "regexMMDDYYYY":Ljava/lang/String;
    const-string v4, "(\\d{2})(\\/)(\\d{2})"

    .line 282
    .local v4, "regexMMDD":Ljava/lang/String;
    new-instance v3, Ljava/text/SimpleDateFormat;

    const-string v8, "MM/dd/yyyy"

    invoke-direct {v3, v8}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 284
    .local v3, "formatter":Ljava/text/SimpleDateFormat;
    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    .line 285
    .local v1, "date":Ljava/util/Date;
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 287
    .local v0, "calendar":Ljava/util/Calendar;
    invoke-virtual {p0, v5}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 288
    invoke-virtual {p0, v4}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 290
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 292
    .local v6, "sb":Ljava/lang/StringBuilder;
    invoke-virtual {v6, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 293
    const-string v8, "/"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 294
    const/4 v8, 0x1

    invoke-virtual {v0, v8}, Ljava/util/Calendar;->get(I)I

    move-result v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 296
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 304
    .end local v6    # "sb":Ljava/lang/StringBuilder;
    :cond_0
    :goto_0
    :try_start_0
    invoke-virtual {v3, p0}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 309
    :goto_1
    invoke-virtual {v0, v1}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 311
    new-instance v7, Ljava/sql/Timestamp;

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v8

    const/16 v10, 0xf

    invoke-virtual {v0, v10}, Ljava/util/Calendar;->get(I)I

    move-result v10

    const/16 v11, 0x10

    invoke-virtual {v0, v11}, Ljava/util/Calendar;->get(I)I

    move-result v11

    add-int/2addr v10, v11

    int-to-long v10, v10

    add-long/2addr v8, v10

    invoke-direct {v7, v8, v9}, Ljava/sql/Timestamp;-><init>(J)V

    .line 313
    .local v7, "timestamp":Ljava/sql/Timestamp;
    invoke-virtual {v7}, Ljava/sql/Timestamp;->getTime()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    return-object v8

    .line 299
    .end local v7    # "timestamp":Ljava/sql/Timestamp;
    :cond_1
    sget-object v8, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->TAG:Ljava/lang/String;

    const-string v9, "toTimestampForBirthday is Fail. parsedTime is wrong"

    invoke-static {v8, v9}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 305
    :catch_0
    move-exception v2

    .line 306
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method public static toTimestampForGooglePlus(Ljava/lang/String;)Ljava/lang/Long;
    .locals 6
    .param p0, "parsedTime"    # Ljava/lang/String;

    .prologue
    .line 366
    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string v4, "yyyy-MM-dd\'T\'HH:mm:SSS"

    invoke-direct {v2, v4}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 367
    .local v2, "formatter":Ljava/text/SimpleDateFormat;
    const-string v4, "GMT"

    invoke-static {v4}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 368
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    .line 371
    .local v0, "date":Ljava/util/Date;
    :try_start_0
    invoke-virtual {v2, p0}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 376
    :goto_0
    new-instance v3, Ljava/sql/Timestamp;

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    invoke-direct {v3, v4, v5}, Ljava/sql/Timestamp;-><init>(J)V

    .line 377
    .local v3, "timestamp":Ljava/sql/Timestamp;
    invoke-virtual {v3}, Ljava/sql/Timestamp;->getTime()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    return-object v4

    .line 372
    .end local v3    # "timestamp":Ljava/sql/Timestamp;
    :catch_0
    move-exception v1

    .line 373
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static toTimestampForQzone(Ljava/lang/String;)Ljava/lang/Long;
    .locals 6
    .param p0, "longtypedTime"    # Ljava/lang/String;

    .prologue
    .line 332
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    .line 334
    .local v2, "time":Ljava/lang/Long;
    :try_start_0
    invoke-static {p0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 339
    :goto_0
    new-instance v0, Ljava/util/Date;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-direct {v0, v4, v5}, Ljava/util/Date;-><init>(J)V

    .line 341
    .local v0, "date":Ljava/util/Date;
    new-instance v3, Ljava/sql/Timestamp;

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    invoke-direct {v3, v4, v5}, Ljava/sql/Timestamp;-><init>(J)V

    .line 342
    .local v3, "timestamp":Ljava/sql/Timestamp;
    invoke-virtual {v3}, Ljava/sql/Timestamp;->getTime()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    return-object v4

    .line 335
    .end local v0    # "date":Ljava/util/Date;
    .end local v3    # "timestamp":Ljava/sql/Timestamp;
    :catch_0
    move-exception v1

    .line 336
    .local v1, "e":Ljava/lang/NumberFormatException;
    invoke-virtual {v1}, Ljava/lang/NumberFormatException;->printStackTrace()V

    goto :goto_0
.end method

.method public static toTimestampForSinaweibo(Ljava/lang/String;)Ljava/lang/Long;
    .locals 6
    .param p0, "parsedTime"    # Ljava/lang/String;

    .prologue
    .line 347
    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string v4, "EEE MMM dd HH:mm:ss Z yyyy"

    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v2, v4, v5}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 348
    .local v2, "formatter":Ljava/text/SimpleDateFormat;
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    .line 351
    .local v0, "date":Ljava/util/Date;
    :try_start_0
    invoke-virtual {v2, p0}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 356
    :goto_0
    new-instance v3, Ljava/sql/Timestamp;

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    invoke-direct {v3, v4, v5}, Ljava/sql/Timestamp;-><init>(J)V

    .line 357
    .local v3, "timestamp":Ljava/sql/Timestamp;
    invoke-virtual {v3}, Ljava/sql/Timestamp;->getTime()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    return-object v4

    .line 352
    .end local v3    # "timestamp":Ljava/sql/Timestamp;
    :catch_0
    move-exception v1

    .line 353
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static toTimestampForTwitter(Ljava/lang/String;)Ljava/lang/Long;
    .locals 6
    .param p0, "parsedTime"    # Ljava/lang/String;

    .prologue
    .line 318
    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string v4, "EEE MMM dd HH:mm:ss Z yyyy"

    sget-object v5, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-direct {v2, v4, v5}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 319
    .local v2, "formatter":Ljava/text/SimpleDateFormat;
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    .line 322
    .local v0, "date":Ljava/util/Date;
    :try_start_0
    invoke-virtual {v2, p0}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 327
    :goto_0
    new-instance v3, Ljava/sql/Timestamp;

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    invoke-direct {v3, v4, v5}, Ljava/sql/Timestamp;-><init>(J)V

    .line 328
    .local v3, "timestamp":Ljava/sql/Timestamp;
    invoke-virtual {v3}, Ljava/sql/Timestamp;->getTime()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    return-object v4

    .line 323
    .end local v3    # "timestamp":Ljava/sql/Timestamp;
    :catch_0
    move-exception v1

    .line 324
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static toTimestampMillies(Ljava/lang/String;)Ljava/lang/Long;
    .locals 4
    .param p0, "parsedTime"    # Ljava/lang/String;

    .prologue
    .line 362
    invoke-static {p0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method
