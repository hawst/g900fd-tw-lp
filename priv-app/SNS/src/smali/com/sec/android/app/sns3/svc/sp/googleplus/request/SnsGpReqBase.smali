.class public abstract Lcom/sec/android/app/sns3/svc/sp/googleplus/request/SnsGpReqBase;
.super Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
.source "SnsGpReqBase.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "SnsGpReqBase"


# direct methods
.method protected constructor <init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;I)V
    .locals 1
    .param p1, "svcMgr"    # Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    .param p2, "reqCategory"    # I

    .prologue
    .line 51
    const-string v0, "googleplus"

    invoke-direct {p0, p1, v0, p2}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;I)V

    .line 52
    return-void
.end method


# virtual methods
.method protected check(IILcom/sec/android/app/sns3/svc/sp/SnsSpResponse;)I
    .locals 6
    .param p1, "httpStatus"    # I
    .param p2, "prevErrorCode"    # I
    .param p3, "resp"    # Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;

    .prologue
    .line 57
    const/4 v1, -0x1

    .line 59
    .local v1, "errorCode":I
    const/4 v3, -0x1

    if-le p2, v3, :cond_1

    .line 60
    move v1, p2

    .line 80
    :cond_0
    :goto_0
    return v1

    .line 62
    :cond_1
    instance-of v3, p3, Lcom/sec/android/app/sns3/svc/sp/SnsSpResponseError;

    if-eqz v3, :cond_0

    move-object v2, p3

    .line 63
    check-cast v2, Lcom/sec/android/app/sns3/svc/sp/SnsSpResponseError;

    .line 64
    .local v2, "errorResp":Lcom/sec/android/app/sns3/svc/sp/SnsSpResponseError;
    invoke-virtual {v2}, Lcom/sec/android/app/sns3/svc/sp/SnsSpResponseError;->getErrorCode()I

    move-result v1

    .line 66
    invoke-virtual {v2}, Lcom/sec/android/app/sns3/svc/sp/SnsSpResponseError;->getErrorBundle()Landroid/os/Bundle;

    move-result-object v0

    .line 67
    .local v0, "errorBundle":Landroid/os/Bundle;
    const-string v3, "SnsGpReqBase"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[SnsGpReqBase] check. httpStatus: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " prevErrorCode: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " errorBundle: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 71
    if-eqz v0, :cond_0

    .line 72
    const-string v3, "401"

    const-string v4, "error_code"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 73
    const/16 v1, 0xfa1

    .line 74
    const-string v3, "SnsGpReqBase"

    const-string v4, "Session Error"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;
    .locals 12
    .param p1, "content"    # Ljava/lang/String;

    .prologue
    const/4 v9, 0x0

    .line 85
    const/4 v3, -0x1

    .line 86
    .local v3, "errorCode":I
    const/4 v4, 0x0

    .line 87
    .local v4, "errorMsg":Ljava/lang/String;
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 90
    .local v2, "errorBundle":Landroid/os/Bundle;
    if-eqz p1, :cond_0

    :try_start_0
    const-string v10, "true"

    invoke-virtual {p1, v10}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_0

    const-string v10, "false"

    invoke-virtual {p1, v10}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_0

    invoke-static {p1}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->isJsonArray(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 120
    :cond_0
    :goto_0
    return-object v9

    .line 94
    :cond_1
    new-instance v8, Lorg/json/JSONObject;

    invoke-direct {v8, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 96
    .local v8, "jsonObj":Lorg/json/JSONObject;
    const-string v10, "error"

    invoke-virtual {v8, v10}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 97
    const-string v10, "error"

    invoke-virtual {v8, v10}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    .line 98
    .local v1, "error":Lorg/json/JSONObject;
    const-string v10, "errors"

    invoke-virtual {v1, v10}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v7

    .line 99
    .local v7, "ja":Lorg/json/JSONArray;
    const/16 v3, 0x3e8

    .line 101
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_1
    invoke-virtual {v7}, Lorg/json/JSONArray;->length()I

    move-result v10

    if-ge v6, v10, :cond_2

    .line 102
    invoke-virtual {v7, v6}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v5

    .line 103
    .local v5, "errorObj":Lorg/json/JSONObject;
    const-string v10, "reason"

    const-string v11, "reason"

    invoke-virtual {v5, v11}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v2, v10, v11}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    const-string v10, "domain"

    const-string v11, "domain"

    invoke-virtual {v5, v11}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v2, v10, v11}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    const-string v10, "message"

    invoke-virtual {v5, v10}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 101
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 108
    .end local v5    # "errorObj":Lorg/json/JSONObject;
    :cond_2
    const-string v10, "error_code"

    const-string v11, "code"

    invoke-virtual {v1, v11}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v2, v10, v11}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 117
    .end local v1    # "error":Lorg/json/JSONObject;
    .end local v6    # "i":I
    .end local v7    # "ja":Lorg/json/JSONArray;
    .end local v8    # "jsonObj":Lorg/json/JSONObject;
    :cond_3
    :goto_2
    const/16 v10, 0x3e8

    if-ne v3, v10, :cond_0

    .line 118
    new-instance v9, Lcom/sec/android/app/sns3/svc/sp/SnsSpResponseError;

    invoke-direct {v9, v3, v4, v2}, Lcom/sec/android/app/sns3/svc/sp/SnsSpResponseError;-><init>(ILjava/lang/String;Landroid/os/Bundle;)V

    goto :goto_0

    .line 111
    :catch_0
    move-exception v0

    .line 112
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_2

    .line 113
    .end local v0    # "e":Lorg/json/JSONException;
    :catch_1
    move-exception v0

    .line 114
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2
.end method

.method protected restoreToken()V
    .locals 15

    .prologue
    .line 125
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v12

    invoke-virtual {v12}, Lcom/sec/android/app/sns3/SnsApplication;->getSvcMgr()Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v12

    invoke-virtual {v12}, Lcom/sec/android/app/sns3/svc/SnsSvcMgr;->getTokenMgr()Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;

    move-result-object v12

    const-string v13, "googleplus"

    invoke-virtual {v12, v13}, Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;->getToken(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpToken;

    .line 128
    .local v6, "gpToken":Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpToken;
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v9

    .line 130
    .local v9, "mContext":Landroid/content/Context;
    invoke-static {v9}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    .line 131
    .local v1, "accountManager":Landroid/accounts/AccountManager;
    const-string v12, "com.google"

    invoke-virtual {v6}, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpToken;->getAccessToken()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v1, v12, v13}, Landroid/accounts/AccountManager;->invalidateAuthToken(Ljava/lang/String;Ljava/lang/String;)V

    .line 134
    const-string v12, "com.google"

    invoke-virtual {v1, v12}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v5

    .line 135
    .local v5, "gaAccounts":[Landroid/accounts/Account;
    const/4 v10, 0x0

    .line 136
    .local v10, "selectedAccount":Landroid/accounts/Account;
    array-length v12, v5

    if-lez v12, :cond_4

    .line 137
    move-object v2, v5

    .local v2, "arr$":[Landroid/accounts/Account;
    array-length v8, v2

    .local v8, "len$":I
    const/4 v7, 0x0

    .local v7, "i$":I
    :goto_0
    if-ge v7, v8, :cond_1

    aget-object v0, v2, v7

    .line 138
    .local v0, "account":Landroid/accounts/Account;
    iget-object v12, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v6}, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpToken;->getAccountName()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_0

    .line 139
    move-object v10, v0

    .line 137
    :cond_0
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 142
    .end local v0    # "account":Landroid/accounts/Account;
    :cond_1
    if-nez v10, :cond_2

    .line 143
    const-string v12, "SnsGpReqBase"

    const-string v13, "can\'t find profer Google account --> select default account"

    invoke-static {v12, v13}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 144
    const/4 v12, 0x0

    aget-object v10, v5, v12

    .line 157
    :cond_2
    :try_start_0
    const-string v12, "oauth2:https://www.googleapis.com/auth/plus.login https://www.googleapis.com/auth/plus.me https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/userinfo.profile"

    const/4 v13, 0x1

    invoke-virtual {v1, v10, v12, v13}, Landroid/accounts/AccountManager;->blockingGetAuthToken(Landroid/accounts/Account;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v3

    .line 159
    .local v3, "authToken":Ljava/lang/String;
    invoke-virtual {v6, v3, v3}, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpToken;->setAccessTokenNExpires(Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    const-string v12, "SnsGpReqBase"

    const-string v13, "AccessToken refresh success"

    invoke-static {v12, v13}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    .line 169
    .end local v2    # "arr$":[Landroid/accounts/Account;
    .end local v3    # "authToken":Ljava/lang/String;
    .end local v7    # "i$":I
    .end local v8    # "len$":I
    :cond_3
    :goto_1
    return-void

    .line 147
    :cond_4
    const-string v12, "SnsGpReqBase"

    const-string v13, "can\'t find Google account --> remove SNS account"

    invoke-static {v12, v13}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 148
    const-string v12, "com.sec.android.app.sns3.googleplus"

    invoke-virtual {v1, v12}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v11

    .line 149
    .local v11, "snsAccounts":[Landroid/accounts/Account;
    array-length v12, v11

    if-lez v12, :cond_3

    .line 150
    const/4 v12, 0x0

    aget-object v12, v11, v12

    const/4 v13, 0x0

    const/4 v14, 0x0

    invoke-virtual {v1, v12, v13, v14}, Landroid/accounts/AccountManager;->removeAccount(Landroid/accounts/Account;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    goto :goto_1

    .line 161
    .end local v11    # "snsAccounts":[Landroid/accounts/Account;
    .restart local v2    # "arr$":[Landroid/accounts/Account;
    .restart local v7    # "i$":I
    .restart local v8    # "len$":I
    :catch_0
    move-exception v4

    .line 162
    .local v4, "e":Landroid/accounts/OperationCanceledException;
    invoke-virtual {v4}, Landroid/accounts/OperationCanceledException;->printStackTrace()V

    goto :goto_1

    .line 163
    .end local v4    # "e":Landroid/accounts/OperationCanceledException;
    :catch_1
    move-exception v4

    .line 164
    .local v4, "e":Landroid/accounts/AuthenticatorException;
    invoke-virtual {v4}, Landroid/accounts/AuthenticatorException;->printStackTrace()V

    goto :goto_1

    .line 165
    .end local v4    # "e":Landroid/accounts/AuthenticatorException;
    :catch_2
    move-exception v4

    .line 166
    .local v4, "e":Ljava/io/IOException;
    const-string v12, "SnsGpReqBase"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "IOException Error refreshing accessToken: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 167
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1
.end method
