.class Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpService$1;
.super Lcom/sec/android/app/sns3/svc/sp/googleplus/api/ISnsGooglePlus$Stub;
.source "SnsGpService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpService;


# direct methods
.method constructor <init>(Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpService;)V
    .locals 0

    .prologue
    .line 87
    iput-object p1, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpService$1;->this$0:Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpService;

    invoke-direct {p0}, Lcom/sec/android/app/sns3/svc/sp/googleplus/api/ISnsGooglePlus$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public getApplicationInfo()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 145
    const-string v1, "SNS"

    const-string v2, "SnsGpService : getApplicationInfo() called"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 146
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 147
    .local v0, "appInfo":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v1, "client_id"

    sget-object v2, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGooglePlus;->CLIENT_ID:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 148
    const-string v1, "redirect_uri"

    const-string v2, "http://localhost"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 149
    const-string v1, "account_type"

    const-string v2, "com.sec.android.app.sns3.googleplus"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 150
    return-object v0
.end method

.method public getFeed(Lcom/sec/android/app/sns3/svc/sp/googleplus/api/ISnsGooglePlusCallbackFeeds;)I
    .locals 6
    .param p1, "cb"    # Lcom/sec/android/app/sns3/svc/sp/googleplus/api/ISnsGooglePlusCallbackFeeds;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 124
    const-string v1, "SNS"

    const-string v2, "SnsGpService : getFeed() called"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 125
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpService$1$2;

    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpService$1;->this$0:Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpService;

    # getter for: Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpService;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    invoke-static {v1}, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpService;->access$000(Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpService;)Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v2

    const-string v3, "me"

    const/4 v4, 0x0

    move-object v1, p0

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpService$1$2;-><init>(Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpService$1;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;Landroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/googleplus/api/ISnsGooglePlusCallbackFeeds;)V

    .line 138
    .local v0, "req":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->request()Z

    .line 140
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->getReqID()I

    move-result v1

    return v1
.end method

.method public getPlaceByNearbySearch(Landroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/googleplus/api/ISnsGooglePlusCallbackNearby;)I
    .locals 3
    .param p1, "bundle"    # Landroid/os/Bundle;
    .param p2, "cb"    # Lcom/sec/android/app/sns3/svc/sp/googleplus/api/ISnsGooglePlusCallbackNearby;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 177
    const-string v1, "SNS"

    const-string v2, "SnsGpService : getNearby() called"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 178
    const-string v1, "searchType"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 179
    const-string v1, "searchType"

    const-string v2, "nearbysearch"

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 181
    :cond_0
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpService$1$4;

    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpService$1;->this$0:Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpService;

    # getter for: Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpService;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    invoke-static {v1}, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpService;->access$000(Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpService;)Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v1

    invoke-direct {v0, p0, v1, p1, p2}, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpService$1$4;-><init>(Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpService$1;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/googleplus/api/ISnsGooglePlusCallbackNearby;)V

    .line 195
    .local v0, "req":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->request()Z

    .line 197
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->getReqID()I

    move-result v1

    return v1
.end method

.method public getPlaceByRadarSearch(Landroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/googleplus/api/ISnsGooglePlusCallbackNearby;)I
    .locals 2
    .param p1, "bundle"    # Landroid/os/Bundle;
    .param p2, "cb"    # Lcom/sec/android/app/sns3/svc/sp/googleplus/api/ISnsGooglePlusCallbackNearby;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 211
    const-string v0, "searchType"

    const-string v1, "radarsearch"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 213
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpService$1;->getPlaceByNearbySearch(Landroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/googleplus/api/ISnsGooglePlusCallbackNearby;)I

    move-result v0

    return v0
.end method

.method public getPlaceByTextSearch(Landroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/googleplus/api/ISnsGooglePlusCallbackNearby;)I
    .locals 2
    .param p1, "bundle"    # Landroid/os/Bundle;
    .param p2, "cb"    # Lcom/sec/android/app/sns3/svc/sp/googleplus/api/ISnsGooglePlusCallbackNearby;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 203
    const-string v0, "searchType"

    const-string v1, "textsearch"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 205
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpService$1;->getPlaceByNearbySearch(Landroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/googleplus/api/ISnsGooglePlusCallbackNearby;)I

    move-result v0

    return v0
.end method

.method public getPlaceDetails(Landroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/googleplus/api/ISnsGooglePlusCallbackPlaceDetails;)I
    .locals 3
    .param p1, "bundle"    # Landroid/os/Bundle;
    .param p2, "cb"    # Lcom/sec/android/app/sns3/svc/sp/googleplus/api/ISnsGooglePlusCallbackPlaceDetails;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 219
    const-string v1, "SNS"

    const-string v2, "getPlaceDetails : getPlaceDetails() called"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 220
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpService$1$5;

    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpService$1;->this$0:Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpService;

    # getter for: Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpService;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    invoke-static {v1}, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpService;->access$000(Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpService;)Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v1

    invoke-direct {v0, p0, v1, p1, p2}, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpService$1$5;-><init>(Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpService$1;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/googleplus/api/ISnsGooglePlusCallbackPlaceDetails;)V

    .line 233
    .local v0, "req":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->request()Z

    .line 235
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->getReqID()I

    move-result v1

    return v1
.end method

.method public getProfile(Lcom/sec/android/app/sns3/svc/sp/googleplus/api/ISnsGooglePlusCallbackProfile;)I
    .locals 6
    .param p1, "cb"    # Lcom/sec/android/app/sns3/svc/sp/googleplus/api/ISnsGooglePlusCallbackProfile;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 155
    const-string v1, "SNS"

    const-string v2, "SnsGpService : getProfile() called"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 156
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpService$1$3;

    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpService$1;->this$0:Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpService;

    # getter for: Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpService;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    invoke-static {v1}, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpService;->access$000(Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpService;)Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v2

    const-string v3, "me"

    const/4 v4, 0x0

    move-object v1, p0

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpService$1$3;-><init>(Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpService$1;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;Landroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/googleplus/api/ISnsGooglePlusCallbackProfile;)V

    .line 169
    .local v0, "req":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->request()Z

    .line 171
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->getReqID()I

    move-result v1

    return v1
.end method

.method public getUser(Lcom/sec/android/app/sns3/svc/sp/googleplus/api/ISnsGooglePlusCallbackUser;)I
    .locals 3
    .param p1, "cb"    # Lcom/sec/android/app/sns3/svc/sp/googleplus/api/ISnsGooglePlusCallbackUser;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 102
    const-string v1, "SNS"

    const-string v2, "SnsGpService : getUser() called"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 103
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpService$1$1;

    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpService$1;->this$0:Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpService;

    # getter for: Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpService;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    invoke-static {v1}, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpService;->access$000(Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpService;)Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v1

    const-string v2, "me"

    invoke-direct {v0, p0, v1, v2, p1}, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpService$1$1;-><init>(Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpService$1;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;Lcom/sec/android/app/sns3/svc/sp/googleplus/api/ISnsGooglePlusCallbackUser;)V

    .line 116
    .local v0, "req":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->request()Z

    .line 118
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->getReqID()I

    move-result v1

    return v1
.end method

.method public setAuthTokenNExpires(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3
    .param p1, "accessToken"    # Ljava/lang/String;
    .param p2, "expires"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 91
    const-string v1, "SNS"

    const-string v2, "SnsGpService : setAuthTokenNExpires() called"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 92
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 93
    :cond_0
    const/4 v1, 0x0

    .line 97
    :goto_0
    return v1

    .line 95
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpService$1;->this$0:Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpService;

    # getter for: Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpService;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    invoke-static {v1}, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpService;->access$000(Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpService;)Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/sns3/svc/SnsSvcMgr;->getTokenMgr()Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;

    move-result-object v1

    const-string v2, "googleplus"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;->getToken(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpToken;

    .line 96
    .local v0, "gpToken":Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpToken;
    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpToken;->setAccessTokenNExpires(Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    const/4 v1, 0x1

    goto :goto_0
.end method
