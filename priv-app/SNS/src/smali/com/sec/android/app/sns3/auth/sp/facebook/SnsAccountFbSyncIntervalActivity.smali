.class public Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;
.super Landroid/preference/PreferenceActivity;
.source "SnsAccountFbSyncIntervalActivity.java"


# static fields
.field private static final DEFAULT_SYNC_INTERVAL:J = 0x0L

.field private static final SYNC_INTERVAL_KEY:Ljava/lang/String; = "sync_interval"

.field private static final TAG:Ljava/lang/String; = "SnsAccountFbAuth"


# instance fields
.field private mAppContext:Landroid/content/Context;

.field private mCalendarIntervalPreference:Landroid/preference/ListPreference;

.field private mCalendarSharedPref:Landroid/content/SharedPreferences;

.field private mContactIntervalPreference:Landroid/preference/ListPreference;

.field private mContactSharedPref:Landroid/content/SharedPreferences;

.field private mGalleryIntervalPreference:Landroid/preference/ListPreference;

.field private mGallerySharedPref:Landroid/content/SharedPreferences;

.field private mHomeFeedIntervalPreference:Landroid/preference/ListPreference;

.field private mHomeFeedSharedPref:Landroid/content/SharedPreferences;

.field private mProfileFeedIntervalPreference:Landroid/preference/ListPreference;

.field private mProfileFeedSharedPref:Landroid/content/SharedPreferences;

.field private mProfilesIntervalPreference:Landroid/preference/ListPreference;

.field private mProfilesSharedPref:Landroid/content/SharedPreferences;

.field private mStreamsIntervalPreference:Landroid/preference/ListPreference;

.field private mStreamsSharedPref:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 49
    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    .line 59
    iput-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mCalendarIntervalPreference:Landroid/preference/ListPreference;

    .line 60
    iput-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mGalleryIntervalPreference:Landroid/preference/ListPreference;

    .line 61
    iput-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mContactIntervalPreference:Landroid/preference/ListPreference;

    .line 62
    iput-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mProfileFeedIntervalPreference:Landroid/preference/ListPreference;

    .line 63
    iput-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mHomeFeedIntervalPreference:Landroid/preference/ListPreference;

    .line 64
    iput-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mProfilesIntervalPreference:Landroid/preference/ListPreference;

    .line 65
    iput-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mStreamsIntervalPreference:Landroid/preference/ListPreference;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;)Landroid/preference/ListPreference;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mContactIntervalPreference:Landroid/preference/ListPreference;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;)Landroid/preference/ListPreference;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mCalendarIntervalPreference:Landroid/preference/ListPreference;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;)Landroid/preference/ListPreference;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mGalleryIntervalPreference:Landroid/preference/ListPreference;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;)Landroid/preference/ListPreference;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mHomeFeedIntervalPreference:Landroid/preference/ListPreference;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;)Landroid/preference/ListPreference;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mProfileFeedIntervalPreference:Landroid/preference/ListPreference;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;)Landroid/preference/ListPreference;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mProfilesIntervalPreference:Landroid/preference/ListPreference;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;)Landroid/preference/ListPreference;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mStreamsIntervalPreference:Landroid/preference/ListPreference;

    return-object v0
.end method


# virtual methods
.method isTable()Z
    .locals 2

    .prologue
    .line 736
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit8 v0, v0, 0x3

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    .line 739
    :cond_0
    const/4 v0, 0x1

    .line 741
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 13
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 78
    iput-object p0, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mAppContext:Landroid/content/Context;

    .line 80
    iget-object v9, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mAppContext:Landroid/content/Context;

    invoke-static {v9}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->isLightTheme(Landroid/content/Context;)Z

    move-result v9

    if-eqz v9, :cond_22

    const v9, 0x103012b

    :goto_0
    invoke-virtual {p0, v9}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->setTheme(I)V

    .line 83
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 85
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 86
    .local v0, "actionBar":Landroid/app/ActionBar;
    const/4 v9, 0x1

    invoke-virtual {v0, v9}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 87
    const/4 v9, 0x0

    invoke-virtual {v0, v9}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 89
    const v9, 0x7f030005

    invoke-virtual {p0, v9}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->setContentView(I)V

    .line 90
    const v9, 0x7f080046

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    const v12, 0x7f080020

    invoke-virtual {p0, v12}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->getString(I)Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-virtual {p0, v9, v10}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v9}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 91
    const v9, 0x7f04000d

    invoke-virtual {p0, v9}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->addPreferencesFromResource(I)V

    .line 93
    const-string v9, "sync_interval"

    invoke-virtual {p0, v9}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v4

    check-cast v4, Landroid/preference/PreferenceScreen;

    .line 95
    .local v4, "preferenceScreen":Landroid/preference/PreferenceScreen;
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v9

    const-string v10, "com.facebook.auth.login"

    invoke-virtual {v9, v10}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v1

    .line 98
    .local v1, "appAccount":[Landroid/accounts/Account;
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v9

    const-string v10, "com.sec.android.app.sns3.facebook"

    invoke-virtual {v9, v10}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v8

    .line 101
    .local v8, "sns3Account":[Landroid/accounts/Account;
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->getIntent()Landroid/content/Intent;

    move-result-object v9

    const-string v10, "skip_sync_marketapp"

    const/4 v11, 0x1

    invoke-virtual {v9, v10, v11}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v7

    .line 103
    .local v7, "skipMarketApp":Z
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/app/sns3/SnsApplication;->getFeatureMgr()Lcom/sec/android/app/sns3/SnsFeatureManager;

    move-result-object v2

    .line 104
    .local v2, "featureMgr":Lcom/sec/android/app/sns3/SnsFeatureManager;
    const/4 v6, 0x0

    .line 105
    .local v6, "providerLabel":Ljava/lang/CharSequence;
    const/4 v5, 0x0

    .line 108
    .local v5, "providerInfo":Landroid/content/pm/ProviderInfo;
    if-nez v7, :cond_4

    .line 109
    array-length v9, v1

    if-lez v9, :cond_4

    .line 110
    const/4 v6, 0x0

    .line 111
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v9

    const-string v10, "com.android.contacts"

    const/4 v11, 0x0

    invoke-virtual {v9, v10, v11}, Landroid/content/pm/PackageManager;->resolveContentProvider(Ljava/lang/String;I)Landroid/content/pm/ProviderInfo;

    move-result-object v5

    .line 112
    if-eqz v5, :cond_0

    .line 113
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v9

    invoke-virtual {v5, v9}, Landroid/content/pm/ProviderInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v6

    .line 115
    :cond_0
    if-eqz v6, :cond_1

    invoke-interface {v6}, Ljava/lang/CharSequence;->length()I

    move-result v9

    if-nez v9, :cond_2

    .line 116
    :cond_1
    const-string v9, "SnsAccountFbAuth"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "providerLabel is null - "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 117
    const-string v6, "Contacts"

    .line 120
    :cond_2
    new-instance v9, Landroid/preference/ListPreference;

    invoke-direct {v9, p0}, Landroid/preference/ListPreference;-><init>(Landroid/content/Context;)V

    iput-object v9, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mContactIntervalPreference:Landroid/preference/ListPreference;

    .line 121
    iget-object v9, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mContactIntervalPreference:Landroid/preference/ListPreference;

    const v10, 0x7f080009

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    aput-object v6, v11, v12

    invoke-virtual {p0, v10, v11}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/preference/ListPreference;->setTitle(Ljava/lang/CharSequence;)V

    .line 123
    iget-object v9, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mContactIntervalPreference:Landroid/preference/ListPreference;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/preference/ListPreference;->setPersistent(Z)V

    .line 124
    iget-object v9, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mContactIntervalPreference:Landroid/preference/ListPreference;

    const/high16 v10, 0x7f050000

    invoke-virtual {v9, v10}, Landroid/preference/ListPreference;->setEntries(I)V

    .line 125
    iget-object v9, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mContactIntervalPreference:Landroid/preference/ListPreference;

    const v10, 0x7f050001

    invoke-virtual {v9, v10}, Landroid/preference/ListPreference;->setEntryValues(I)V

    .line 126
    sget-object v9, Lcom/sec/android/app/sns3/SnsFeatureManager;->ANGLE_INDICATION_SUPPORTED:Ljava/lang/Boolean;

    invoke-virtual {v9}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v9

    if-eqz v9, :cond_3

    .line 127
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v9

    invoke-static {v9}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->isLightTheme(Landroid/content/Context;)Z

    move-result v9

    if-eqz v9, :cond_23

    .line 128
    iget-object v9, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mContactIntervalPreference:Landroid/preference/ListPreference;

    const v10, 0x7f030003

    invoke-virtual {v9, v10}, Landroid/preference/ListPreference;->setWidgetLayoutResource(I)V

    .line 133
    :cond_3
    :goto_1
    iget-object v9, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mContactIntervalPreference:Landroid/preference/ListPreference;

    const v10, 0x7f080009

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    aput-object v6, v11, v12

    invoke-virtual {p0, v10, v11}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/preference/ListPreference;->setDialogTitle(Ljava/lang/CharSequence;)V

    .line 136
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->readContactSyncInterval()V

    .line 138
    iget-object v9, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mContactIntervalPreference:Landroid/preference/ListPreference;

    new-instance v10, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity$1;

    invoke-direct {v10, p0, v1}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity$1;-><init>(Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;[Landroid/accounts/Account;)V

    invoke-virtual {v9, v10}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 173
    iget-object v9, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mContactIntervalPreference:Landroid/preference/ListPreference;

    invoke-virtual {v4, v9}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 178
    :cond_4
    const/4 v6, 0x0

    .line 179
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v9

    const-string v10, "com.android.calendar"

    const/4 v11, 0x0

    invoke-virtual {v9, v10, v11}, Landroid/content/pm/PackageManager;->resolveContentProvider(Ljava/lang/String;I)Landroid/content/pm/ProviderInfo;

    move-result-object v5

    .line 180
    if-eqz v5, :cond_5

    .line 181
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v9

    invoke-virtual {v5, v9}, Landroid/content/pm/ProviderInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v6

    .line 183
    :cond_5
    if-eqz v6, :cond_6

    invoke-interface {v6}, Ljava/lang/CharSequence;->length()I

    move-result v9

    if-nez v9, :cond_7

    .line 184
    :cond_6
    const-string v9, "SnsAccountFbAuth"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "providerLabel is null - "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 185
    const-string v6, "Calendar"

    .line 188
    :cond_7
    new-instance v9, Landroid/preference/ListPreference;

    invoke-direct {v9, p0}, Landroid/preference/ListPreference;-><init>(Landroid/content/Context;)V

    iput-object v9, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mCalendarIntervalPreference:Landroid/preference/ListPreference;

    .line 189
    iget-object v9, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mCalendarIntervalPreference:Landroid/preference/ListPreference;

    const v10, 0x7f080009

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    aput-object v6, v11, v12

    invoke-virtual {p0, v10, v11}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/preference/ListPreference;->setTitle(Ljava/lang/CharSequence;)V

    .line 191
    iget-object v9, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mCalendarIntervalPreference:Landroid/preference/ListPreference;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/preference/ListPreference;->setPersistent(Z)V

    .line 192
    iget-object v9, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mCalendarIntervalPreference:Landroid/preference/ListPreference;

    const/high16 v10, 0x7f050000

    invoke-virtual {v9, v10}, Landroid/preference/ListPreference;->setEntries(I)V

    .line 193
    iget-object v9, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mCalendarIntervalPreference:Landroid/preference/ListPreference;

    const v10, 0x7f050001

    invoke-virtual {v9, v10}, Landroid/preference/ListPreference;->setEntryValues(I)V

    .line 194
    sget-object v9, Lcom/sec/android/app/sns3/SnsFeatureManager;->ANGLE_INDICATION_SUPPORTED:Ljava/lang/Boolean;

    invoke-virtual {v9}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v9

    if-eqz v9, :cond_8

    .line 195
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v9

    invoke-static {v9}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->isLightTheme(Landroid/content/Context;)Z

    move-result v9

    if-eqz v9, :cond_24

    .line 196
    iget-object v9, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mCalendarIntervalPreference:Landroid/preference/ListPreference;

    const v10, 0x7f030003

    invoke-virtual {v9, v10}, Landroid/preference/ListPreference;->setWidgetLayoutResource(I)V

    .line 201
    :cond_8
    :goto_2
    iget-object v9, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mCalendarIntervalPreference:Landroid/preference/ListPreference;

    const v10, 0x7f080009

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    aput-object v6, v11, v12

    invoke-virtual {p0, v10, v11}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/preference/ListPreference;->setDialogTitle(Ljava/lang/CharSequence;)V

    .line 204
    array-length v9, v8

    if-lez v9, :cond_25

    .line 205
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->readCalendarSyncInterval()V

    .line 207
    iget-object v9, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mCalendarIntervalPreference:Landroid/preference/ListPreference;

    new-instance v10, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity$2;

    invoke-direct {v10, p0, v8}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity$2;-><init>(Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;[Landroid/accounts/Account;)V

    invoke-virtual {v9, v10}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 240
    :goto_3
    iget-object v9, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mCalendarIntervalPreference:Landroid/preference/ListPreference;

    invoke-virtual {v4, v9}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 243
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    const-string v10, "content://com.sec.android.gallery3d.sns.contentprovider/support_sns"

    invoke-static {v10}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/content/ContentResolver;->getType(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v3

    .line 246
    .local v3, "isGallerySync":Ljava/lang/String;
    const-string v9, "true"

    invoke-virtual {v9, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_d

    .line 247
    const/4 v6, 0x0

    .line 248
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v9

    const-string v10, "com.sec.android.gallery3d.sns.contentprovider"

    const/4 v11, 0x0

    invoke-virtual {v9, v10, v11}, Landroid/content/pm/PackageManager;->resolveContentProvider(Ljava/lang/String;I)Landroid/content/pm/ProviderInfo;

    move-result-object v5

    .line 249
    if-eqz v5, :cond_9

    .line 250
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v9

    invoke-virtual {v5, v9}, Landroid/content/pm/ProviderInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v6

    .line 252
    :cond_9
    if-eqz v6, :cond_a

    invoke-interface {v6}, Ljava/lang/CharSequence;->length()I

    move-result v9

    if-nez v9, :cond_b

    .line 253
    :cond_a
    const-string v9, "SnsAccountFbAuth"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "providerLabel is null - "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 254
    const-string v6, "Gallery"

    .line 257
    :cond_b
    new-instance v9, Landroid/preference/ListPreference;

    invoke-direct {v9, p0}, Landroid/preference/ListPreference;-><init>(Landroid/content/Context;)V

    iput-object v9, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mGalleryIntervalPreference:Landroid/preference/ListPreference;

    .line 258
    iget-object v9, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mGalleryIntervalPreference:Landroid/preference/ListPreference;

    const v10, 0x7f080009

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    aput-object v6, v11, v12

    invoke-virtual {p0, v10, v11}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/preference/ListPreference;->setTitle(Ljava/lang/CharSequence;)V

    .line 260
    iget-object v9, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mGalleryIntervalPreference:Landroid/preference/ListPreference;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/preference/ListPreference;->setPersistent(Z)V

    .line 261
    iget-object v9, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mGalleryIntervalPreference:Landroid/preference/ListPreference;

    const/high16 v10, 0x7f050000

    invoke-virtual {v9, v10}, Landroid/preference/ListPreference;->setEntries(I)V

    .line 262
    iget-object v9, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mGalleryIntervalPreference:Landroid/preference/ListPreference;

    const v10, 0x7f050001

    invoke-virtual {v9, v10}, Landroid/preference/ListPreference;->setEntryValues(I)V

    .line 263
    sget-object v9, Lcom/sec/android/app/sns3/SnsFeatureManager;->ANGLE_INDICATION_SUPPORTED:Ljava/lang/Boolean;

    invoke-virtual {v9}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v9

    if-eqz v9, :cond_c

    .line 264
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v9

    invoke-static {v9}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->isLightTheme(Landroid/content/Context;)Z

    move-result v9

    if-eqz v9, :cond_26

    .line 265
    iget-object v9, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mGalleryIntervalPreference:Landroid/preference/ListPreference;

    const v10, 0x7f030003

    invoke-virtual {v9, v10}, Landroid/preference/ListPreference;->setWidgetLayoutResource(I)V

    .line 270
    :cond_c
    :goto_4
    iget-object v9, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mGalleryIntervalPreference:Landroid/preference/ListPreference;

    const v10, 0x7f080009

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    aput-object v6, v11, v12

    invoke-virtual {p0, v10, v11}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/preference/ListPreference;->setDialogTitle(Ljava/lang/CharSequence;)V

    .line 273
    array-length v9, v8

    if-lez v9, :cond_27

    .line 274
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->readGallerySyncInterval()V

    .line 276
    iget-object v9, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mGalleryIntervalPreference:Landroid/preference/ListPreference;

    new-instance v10, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity$3;

    invoke-direct {v10, p0, v8}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity$3;-><init>(Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;[Landroid/accounts/Account;)V

    invoke-virtual {v9, v10}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 312
    :goto_5
    iget-object v9, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mGalleryIntervalPreference:Landroid/preference/ListPreference;

    invoke-virtual {v4, v9}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 316
    :cond_d
    invoke-virtual {v2}, Lcom/sec/android/app/sns3/SnsFeatureManager;->isSyncAdapterForHomeFeedsNeeded()Z

    move-result v9

    if-eqz v9, :cond_12

    .line 317
    const/4 v6, 0x0

    .line 318
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v9

    const-string v10, "com.sec.android.app.sns3.home"

    const/4 v11, 0x0

    invoke-virtual {v9, v10, v11}, Landroid/content/pm/PackageManager;->resolveContentProvider(Ljava/lang/String;I)Landroid/content/pm/ProviderInfo;

    move-result-object v5

    .line 319
    if-eqz v5, :cond_e

    .line 320
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v9

    invoke-virtual {v5, v9}, Landroid/content/pm/ProviderInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v6

    .line 322
    :cond_e
    if-eqz v6, :cond_f

    invoke-interface {v6}, Ljava/lang/CharSequence;->length()I

    move-result v9

    if-nez v9, :cond_10

    .line 323
    :cond_f
    const-string v9, "SnsAccountFbAuth"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "providerLabel is null - "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 324
    const-string v6, "Home"

    .line 327
    :cond_10
    new-instance v9, Landroid/preference/ListPreference;

    invoke-direct {v9, p0}, Landroid/preference/ListPreference;-><init>(Landroid/content/Context;)V

    iput-object v9, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mHomeFeedIntervalPreference:Landroid/preference/ListPreference;

    .line 328
    iget-object v9, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mHomeFeedIntervalPreference:Landroid/preference/ListPreference;

    const v10, 0x7f080009

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    aput-object v6, v11, v12

    invoke-virtual {p0, v10, v11}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/preference/ListPreference;->setTitle(Ljava/lang/CharSequence;)V

    .line 330
    iget-object v9, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mHomeFeedIntervalPreference:Landroid/preference/ListPreference;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/preference/ListPreference;->setPersistent(Z)V

    .line 331
    iget-object v9, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mHomeFeedIntervalPreference:Landroid/preference/ListPreference;

    const/high16 v10, 0x7f050000

    invoke-virtual {v9, v10}, Landroid/preference/ListPreference;->setEntries(I)V

    .line 332
    iget-object v9, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mHomeFeedIntervalPreference:Landroid/preference/ListPreference;

    const v10, 0x7f050001

    invoke-virtual {v9, v10}, Landroid/preference/ListPreference;->setEntryValues(I)V

    .line 333
    sget-object v9, Lcom/sec/android/app/sns3/SnsFeatureManager;->ANGLE_INDICATION_SUPPORTED:Ljava/lang/Boolean;

    invoke-virtual {v9}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v9

    if-eqz v9, :cond_11

    .line 334
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v9

    invoke-static {v9}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->isLightTheme(Landroid/content/Context;)Z

    move-result v9

    if-eqz v9, :cond_28

    .line 335
    iget-object v9, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mHomeFeedIntervalPreference:Landroid/preference/ListPreference;

    const v10, 0x7f030003

    invoke-virtual {v9, v10}, Landroid/preference/ListPreference;->setWidgetLayoutResource(I)V

    .line 340
    :cond_11
    :goto_6
    iget-object v9, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mHomeFeedIntervalPreference:Landroid/preference/ListPreference;

    const v10, 0x7f080009

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    aput-object v6, v11, v12

    invoke-virtual {p0, v10, v11}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/preference/ListPreference;->setDialogTitle(Ljava/lang/CharSequence;)V

    .line 343
    array-length v9, v8

    if-lez v9, :cond_29

    .line 344
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->readHomeFeedSyncInterval()V

    .line 346
    iget-object v9, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mHomeFeedIntervalPreference:Landroid/preference/ListPreference;

    new-instance v10, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity$4;

    invoke-direct {v10, p0, v8}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity$4;-><init>(Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;[Landroid/accounts/Account;)V

    invoke-virtual {v9, v10}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 380
    :goto_7
    iget-object v9, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mHomeFeedIntervalPreference:Landroid/preference/ListPreference;

    invoke-virtual {v4, v9}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 384
    :cond_12
    invoke-virtual {v2}, Lcom/sec/android/app/sns3/SnsFeatureManager;->isSyncAdapterForProfileFeedsNeeded()Z

    move-result v9

    if-eqz v9, :cond_17

    .line 385
    const/4 v6, 0x0

    .line 386
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v9

    const-string v10, "com.sec.android.app.sns3.life"

    const/4 v11, 0x0

    invoke-virtual {v9, v10, v11}, Landroid/content/pm/PackageManager;->resolveContentProvider(Ljava/lang/String;I)Landroid/content/pm/ProviderInfo;

    move-result-object v5

    .line 387
    if-eqz v5, :cond_13

    .line 388
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v9

    invoke-virtual {v5, v9}, Landroid/content/pm/ProviderInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v6

    .line 390
    :cond_13
    if-eqz v6, :cond_14

    invoke-interface {v6}, Ljava/lang/CharSequence;->length()I

    move-result v9

    if-nez v9, :cond_15

    .line 391
    :cond_14
    const-string v9, "SnsAccountFbAuth"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "providerLabel is null - "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 392
    const-string v6, "Life Times"

    .line 395
    :cond_15
    new-instance v9, Landroid/preference/ListPreference;

    invoke-direct {v9, p0}, Landroid/preference/ListPreference;-><init>(Landroid/content/Context;)V

    iput-object v9, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mProfileFeedIntervalPreference:Landroid/preference/ListPreference;

    .line 396
    iget-object v9, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mProfileFeedIntervalPreference:Landroid/preference/ListPreference;

    const v10, 0x7f080009

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    aput-object v6, v11, v12

    invoke-virtual {p0, v10, v11}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/preference/ListPreference;->setTitle(Ljava/lang/CharSequence;)V

    .line 398
    iget-object v9, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mProfileFeedIntervalPreference:Landroid/preference/ListPreference;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/preference/ListPreference;->setPersistent(Z)V

    .line 399
    iget-object v9, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mProfileFeedIntervalPreference:Landroid/preference/ListPreference;

    const/high16 v10, 0x7f050000

    invoke-virtual {v9, v10}, Landroid/preference/ListPreference;->setEntries(I)V

    .line 400
    iget-object v9, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mProfileFeedIntervalPreference:Landroid/preference/ListPreference;

    const v10, 0x7f050001

    invoke-virtual {v9, v10}, Landroid/preference/ListPreference;->setEntryValues(I)V

    .line 401
    sget-object v9, Lcom/sec/android/app/sns3/SnsFeatureManager;->ANGLE_INDICATION_SUPPORTED:Ljava/lang/Boolean;

    invoke-virtual {v9}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v9

    if-eqz v9, :cond_16

    .line 402
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v9

    invoke-static {v9}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->isLightTheme(Landroid/content/Context;)Z

    move-result v9

    if-eqz v9, :cond_2a

    .line 403
    iget-object v9, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mProfileFeedIntervalPreference:Landroid/preference/ListPreference;

    const v10, 0x7f030003

    invoke-virtual {v9, v10}, Landroid/preference/ListPreference;->setWidgetLayoutResource(I)V

    .line 408
    :cond_16
    :goto_8
    iget-object v9, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mProfileFeedIntervalPreference:Landroid/preference/ListPreference;

    const v10, 0x7f080009

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    aput-object v6, v11, v12

    invoke-virtual {p0, v10, v11}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/preference/ListPreference;->setDialogTitle(Ljava/lang/CharSequence;)V

    .line 411
    array-length v9, v8

    if-lez v9, :cond_2b

    .line 412
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->readProfileFeedSyncInterval()V

    .line 414
    iget-object v9, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mProfileFeedIntervalPreference:Landroid/preference/ListPreference;

    new-instance v10, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity$5;

    invoke-direct {v10, p0, v8}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity$5;-><init>(Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;[Landroid/accounts/Account;)V

    invoke-virtual {v9, v10}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 450
    :goto_9
    iget-object v9, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mProfileFeedIntervalPreference:Landroid/preference/ListPreference;

    invoke-virtual {v4, v9}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 454
    :cond_17
    invoke-virtual {v2}, Lcom/sec/android/app/sns3/SnsFeatureManager;->isSyncAdapterForProfilesNeeded()Z

    move-result v9

    if-eqz v9, :cond_1c

    .line 455
    const/4 v6, 0x0

    .line 456
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v9

    const-string v10, "com.sec.android.app.sns3.profiles"

    const/4 v11, 0x0

    invoke-virtual {v9, v10, v11}, Landroid/content/pm/PackageManager;->resolveContentProvider(Ljava/lang/String;I)Landroid/content/pm/ProviderInfo;

    move-result-object v5

    .line 457
    if-eqz v5, :cond_18

    .line 458
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v9

    invoke-virtual {v5, v9}, Landroid/content/pm/ProviderInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v6

    .line 460
    :cond_18
    if-eqz v6, :cond_19

    invoke-interface {v6}, Ljava/lang/CharSequence;->length()I

    move-result v9

    if-nez v9, :cond_1a

    .line 461
    :cond_19
    const-string v9, "SnsAccountFbAuth"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "providerLabel is null - "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 462
    const-string v6, "Profiles"

    .line 465
    :cond_1a
    new-instance v9, Landroid/preference/ListPreference;

    invoke-direct {v9, p0}, Landroid/preference/ListPreference;-><init>(Landroid/content/Context;)V

    iput-object v9, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mProfilesIntervalPreference:Landroid/preference/ListPreference;

    .line 466
    iget-object v9, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mProfilesIntervalPreference:Landroid/preference/ListPreference;

    const v10, 0x7f080009

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    aput-object v6, v11, v12

    invoke-virtual {p0, v10, v11}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/preference/ListPreference;->setTitle(Ljava/lang/CharSequence;)V

    .line 468
    iget-object v9, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mProfilesIntervalPreference:Landroid/preference/ListPreference;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/preference/ListPreference;->setPersistent(Z)V

    .line 469
    iget-object v9, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mProfilesIntervalPreference:Landroid/preference/ListPreference;

    const/high16 v10, 0x7f050000

    invoke-virtual {v9, v10}, Landroid/preference/ListPreference;->setEntries(I)V

    .line 470
    iget-object v9, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mProfilesIntervalPreference:Landroid/preference/ListPreference;

    const v10, 0x7f050001

    invoke-virtual {v9, v10}, Landroid/preference/ListPreference;->setEntryValues(I)V

    .line 471
    sget-object v9, Lcom/sec/android/app/sns3/SnsFeatureManager;->ANGLE_INDICATION_SUPPORTED:Ljava/lang/Boolean;

    invoke-virtual {v9}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v9

    if-eqz v9, :cond_1b

    .line 472
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v9

    invoke-static {v9}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->isLightTheme(Landroid/content/Context;)Z

    move-result v9

    if-eqz v9, :cond_2c

    .line 473
    iget-object v9, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mProfilesIntervalPreference:Landroid/preference/ListPreference;

    const v10, 0x7f030003

    invoke-virtual {v9, v10}, Landroid/preference/ListPreference;->setWidgetLayoutResource(I)V

    .line 478
    :cond_1b
    :goto_a
    iget-object v9, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mProfilesIntervalPreference:Landroid/preference/ListPreference;

    const v10, 0x7f080009

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    aput-object v6, v11, v12

    invoke-virtual {p0, v10, v11}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/preference/ListPreference;->setDialogTitle(Ljava/lang/CharSequence;)V

    .line 481
    array-length v9, v8

    if-lez v9, :cond_2d

    .line 482
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->readProfilesSyncInterval()V

    .line 484
    iget-object v9, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mProfilesIntervalPreference:Landroid/preference/ListPreference;

    new-instance v10, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity$6;

    invoke-direct {v10, p0, v8}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity$6;-><init>(Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;[Landroid/accounts/Account;)V

    invoke-virtual {v9, v10}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 518
    :goto_b
    iget-object v9, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mProfilesIntervalPreference:Landroid/preference/ListPreference;

    invoke-virtual {v4, v9}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 523
    :cond_1c
    invoke-virtual {v2}, Lcom/sec/android/app/sns3/SnsFeatureManager;->isSyncAdapterForStreamsNeeded()Z

    move-result v9

    if-eqz v9, :cond_21

    .line 524
    const/4 v6, 0x0

    .line 525
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v9

    const-string v10, "com.sec.android.app.sns3.streams"

    const/4 v11, 0x0

    invoke-virtual {v9, v10, v11}, Landroid/content/pm/PackageManager;->resolveContentProvider(Ljava/lang/String;I)Landroid/content/pm/ProviderInfo;

    move-result-object v5

    .line 526
    if-eqz v5, :cond_1d

    .line 527
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v9

    invoke-virtual {v5, v9}, Landroid/content/pm/ProviderInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v6

    .line 529
    :cond_1d
    if-eqz v6, :cond_1e

    invoke-interface {v6}, Ljava/lang/CharSequence;->length()I

    move-result v9

    if-nez v9, :cond_1f

    .line 530
    :cond_1e
    const-string v9, "SnsAccountFbAuth"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "providerLabel is null - "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 531
    const-string v6, "Streams"

    .line 534
    :cond_1f
    new-instance v9, Landroid/preference/ListPreference;

    invoke-direct {v9, p0}, Landroid/preference/ListPreference;-><init>(Landroid/content/Context;)V

    iput-object v9, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mStreamsIntervalPreference:Landroid/preference/ListPreference;

    .line 535
    iget-object v9, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mStreamsIntervalPreference:Landroid/preference/ListPreference;

    const v10, 0x7f080009

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    aput-object v6, v11, v12

    invoke-virtual {p0, v10, v11}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/preference/ListPreference;->setTitle(Ljava/lang/CharSequence;)V

    .line 537
    iget-object v9, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mStreamsIntervalPreference:Landroid/preference/ListPreference;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/preference/ListPreference;->setPersistent(Z)V

    .line 538
    iget-object v9, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mStreamsIntervalPreference:Landroid/preference/ListPreference;

    const/high16 v10, 0x7f050000

    invoke-virtual {v9, v10}, Landroid/preference/ListPreference;->setEntries(I)V

    .line 539
    iget-object v9, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mStreamsIntervalPreference:Landroid/preference/ListPreference;

    const v10, 0x7f050001

    invoke-virtual {v9, v10}, Landroid/preference/ListPreference;->setEntryValues(I)V

    .line 540
    sget-object v9, Lcom/sec/android/app/sns3/SnsFeatureManager;->ANGLE_INDICATION_SUPPORTED:Ljava/lang/Boolean;

    invoke-virtual {v9}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v9

    if-eqz v9, :cond_20

    .line 541
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v9

    invoke-static {v9}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->isLightTheme(Landroid/content/Context;)Z

    move-result v9

    if-eqz v9, :cond_2e

    .line 542
    iget-object v9, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mStreamsIntervalPreference:Landroid/preference/ListPreference;

    const v10, 0x7f030003

    invoke-virtual {v9, v10}, Landroid/preference/ListPreference;->setWidgetLayoutResource(I)V

    .line 547
    :cond_20
    :goto_c
    iget-object v9, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mStreamsIntervalPreference:Landroid/preference/ListPreference;

    const v10, 0x7f080009

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    aput-object v6, v11, v12

    invoke-virtual {p0, v10, v11}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/preference/ListPreference;->setDialogTitle(Ljava/lang/CharSequence;)V

    .line 550
    array-length v9, v8

    if-lez v9, :cond_2f

    .line 551
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->readStreamsSyncInterval()V

    .line 553
    iget-object v9, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mStreamsIntervalPreference:Landroid/preference/ListPreference;

    new-instance v10, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity$7;

    invoke-direct {v10, p0, v8}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity$7;-><init>(Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;[Landroid/accounts/Account;)V

    invoke-virtual {v9, v10}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 587
    :goto_d
    iget-object v9, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mStreamsIntervalPreference:Landroid/preference/ListPreference;

    invoke-virtual {v4, v9}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 590
    :cond_21
    return-void

    .line 80
    .end local v0    # "actionBar":Landroid/app/ActionBar;
    .end local v1    # "appAccount":[Landroid/accounts/Account;
    .end local v2    # "featureMgr":Lcom/sec/android/app/sns3/SnsFeatureManager;
    .end local v3    # "isGallerySync":Ljava/lang/String;
    .end local v4    # "preferenceScreen":Landroid/preference/PreferenceScreen;
    .end local v5    # "providerInfo":Landroid/content/pm/ProviderInfo;
    .end local v6    # "providerLabel":Ljava/lang/CharSequence;
    .end local v7    # "skipMarketApp":Z
    .end local v8    # "sns3Account":[Landroid/accounts/Account;
    :cond_22
    const v9, 0x1030128

    goto/16 :goto_0

    .line 130
    .restart local v0    # "actionBar":Landroid/app/ActionBar;
    .restart local v1    # "appAccount":[Landroid/accounts/Account;
    .restart local v2    # "featureMgr":Lcom/sec/android/app/sns3/SnsFeatureManager;
    .restart local v4    # "preferenceScreen":Landroid/preference/PreferenceScreen;
    .restart local v5    # "providerInfo":Landroid/content/pm/ProviderInfo;
    .restart local v6    # "providerLabel":Ljava/lang/CharSequence;
    .restart local v7    # "skipMarketApp":Z
    .restart local v8    # "sns3Account":[Landroid/accounts/Account;
    :cond_23
    iget-object v9, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mContactIntervalPreference:Landroid/preference/ListPreference;

    const v10, 0x7f030002

    invoke-virtual {v9, v10}, Landroid/preference/ListPreference;->setWidgetLayoutResource(I)V

    goto/16 :goto_1

    .line 198
    :cond_24
    iget-object v9, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mCalendarIntervalPreference:Landroid/preference/ListPreference;

    const v10, 0x7f030002

    invoke-virtual {v9, v10}, Landroid/preference/ListPreference;->setWidgetLayoutResource(I)V

    goto/16 :goto_2

    .line 237
    :cond_25
    iget-object v9, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mCalendarIntervalPreference:Landroid/preference/ListPreference;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/preference/ListPreference;->setEnabled(Z)V

    goto/16 :goto_3

    .line 267
    .restart local v3    # "isGallerySync":Ljava/lang/String;
    :cond_26
    iget-object v9, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mGalleryIntervalPreference:Landroid/preference/ListPreference;

    const v10, 0x7f030002

    invoke-virtual {v9, v10}, Landroid/preference/ListPreference;->setWidgetLayoutResource(I)V

    goto/16 :goto_4

    .line 309
    :cond_27
    iget-object v9, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mGalleryIntervalPreference:Landroid/preference/ListPreference;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/preference/ListPreference;->setEnabled(Z)V

    goto/16 :goto_5

    .line 337
    :cond_28
    iget-object v9, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mHomeFeedIntervalPreference:Landroid/preference/ListPreference;

    const v10, 0x7f030002

    invoke-virtual {v9, v10}, Landroid/preference/ListPreference;->setWidgetLayoutResource(I)V

    goto/16 :goto_6

    .line 377
    :cond_29
    iget-object v9, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mHomeFeedIntervalPreference:Landroid/preference/ListPreference;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/preference/ListPreference;->setEnabled(Z)V

    goto/16 :goto_7

    .line 405
    :cond_2a
    iget-object v9, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mProfileFeedIntervalPreference:Landroid/preference/ListPreference;

    const v10, 0x7f030002

    invoke-virtual {v9, v10}, Landroid/preference/ListPreference;->setWidgetLayoutResource(I)V

    goto/16 :goto_8

    .line 447
    :cond_2b
    iget-object v9, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mProfileFeedIntervalPreference:Landroid/preference/ListPreference;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/preference/ListPreference;->setEnabled(Z)V

    goto/16 :goto_9

    .line 475
    :cond_2c
    iget-object v9, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mProfilesIntervalPreference:Landroid/preference/ListPreference;

    const v10, 0x7f030002

    invoke-virtual {v9, v10}, Landroid/preference/ListPreference;->setWidgetLayoutResource(I)V

    goto/16 :goto_a

    .line 515
    :cond_2d
    iget-object v9, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mProfilesIntervalPreference:Landroid/preference/ListPreference;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/preference/ListPreference;->setEnabled(Z)V

    goto/16 :goto_b

    .line 544
    :cond_2e
    iget-object v9, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mStreamsIntervalPreference:Landroid/preference/ListPreference;

    const v10, 0x7f030002

    invoke-virtual {v9, v10}, Landroid/preference/ListPreference;->setWidgetLayoutResource(I)V

    goto/16 :goto_c

    .line 584
    :cond_2f
    iget-object v9, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mStreamsIntervalPreference:Landroid/preference/ListPreference;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/preference/ListPreference;->setEnabled(Z)V

    goto :goto_d
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 595
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 600
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 597
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->finish()V

    .line 598
    const/4 v0, 0x1

    goto :goto_0

    .line 595
    nop

    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 605
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onResume()V

    .line 606
    return-void
.end method

.method public readCalendarSyncInterval()V
    .locals 6

    .prologue
    .line 627
    const-string v1, "FB_calendar_sync_interval"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mCalendarSharedPref:Landroid/content/SharedPreferences;

    .line 629
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mCalendarSharedPref:Landroid/content/SharedPreferences;

    const-string v2, "FB_calendar_sync_interval"

    const-wide/16 v4, 0x0

    invoke-interface {v1, v2, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    .line 632
    .local v0, "savedCalendarInterval":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mCalendarIntervalPreference:Landroid/preference/ListPreference;

    invoke-virtual {v1, v0}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 634
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mCalendarIntervalPreference:Landroid/preference/ListPreference;

    invoke-virtual {v1}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 635
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mCalendarIntervalPreference:Landroid/preference/ListPreference;

    iget-object v2, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mCalendarIntervalPreference:Landroid/preference/ListPreference;

    invoke-virtual {v2}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 637
    :cond_0
    return-void
.end method

.method public readContactSyncInterval()V
    .locals 6

    .prologue
    .line 609
    const-string v1, "FB_contact_sync_interval"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mContactSharedPref:Landroid/content/SharedPreferences;

    .line 611
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mContactSharedPref:Landroid/content/SharedPreferences;

    const-string v2, "FB_contact_sync_interval"

    const-wide/16 v4, 0x0

    invoke-interface {v1, v2, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    .line 614
    .local v0, "savedContactInterval":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mContactIntervalPreference:Landroid/preference/ListPreference;

    invoke-virtual {v1, v0}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 616
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mContactIntervalPreference:Landroid/preference/ListPreference;

    invoke-virtual {v1}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 617
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mContactIntervalPreference:Landroid/preference/ListPreference;

    iget-object v2, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mContactIntervalPreference:Landroid/preference/ListPreference;

    invoke-virtual {v2}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 618
    :cond_0
    return-void
.end method

.method public readGallerySyncInterval()V
    .locals 6

    .prologue
    .line 646
    const-string v1, "FB_gallery_sync_interval"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mGallerySharedPref:Landroid/content/SharedPreferences;

    .line 648
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mGallerySharedPref:Landroid/content/SharedPreferences;

    const-string v2, "FB_gallery_sync_interval"

    const-wide/16 v4, 0x0

    invoke-interface {v1, v2, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    .line 651
    .local v0, "savedGalleryInterval":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mGalleryIntervalPreference:Landroid/preference/ListPreference;

    invoke-virtual {v1, v0}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 653
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mGalleryIntervalPreference:Landroid/preference/ListPreference;

    invoke-virtual {v1}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 654
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mGalleryIntervalPreference:Landroid/preference/ListPreference;

    iget-object v2, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mGalleryIntervalPreference:Landroid/preference/ListPreference;

    invoke-virtual {v2}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 655
    :cond_0
    return-void
.end method

.method public readHomeFeedSyncInterval()V
    .locals 6

    .prologue
    .line 664
    const-string v1, "FB_home_feed_sync_interval"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mHomeFeedSharedPref:Landroid/content/SharedPreferences;

    .line 666
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mHomeFeedSharedPref:Landroid/content/SharedPreferences;

    const-string v2, "FB_home_feed_sync_interval"

    const-wide/16 v4, 0x0

    invoke-interface {v1, v2, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    .line 669
    .local v0, "savedHomeFeedInterval":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mHomeFeedIntervalPreference:Landroid/preference/ListPreference;

    invoke-virtual {v1, v0}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 671
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mHomeFeedIntervalPreference:Landroid/preference/ListPreference;

    invoke-virtual {v1}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 672
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mHomeFeedIntervalPreference:Landroid/preference/ListPreference;

    iget-object v2, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mHomeFeedIntervalPreference:Landroid/preference/ListPreference;

    invoke-virtual {v2}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 673
    :cond_0
    return-void
.end method

.method public readProfileFeedSyncInterval()V
    .locals 6

    .prologue
    .line 682
    const-string v1, "FB_profile_feed_sync_interval"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mProfileFeedSharedPref:Landroid/content/SharedPreferences;

    .line 684
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mProfileFeedSharedPref:Landroid/content/SharedPreferences;

    const-string v2, "FB_profile_feed_sync_interval"

    const-wide/16 v4, 0x0

    invoke-interface {v1, v2, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    .line 687
    .local v0, "savedProfileFeedInterval":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mProfileFeedIntervalPreference:Landroid/preference/ListPreference;

    invoke-virtual {v1, v0}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 689
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mProfileFeedIntervalPreference:Landroid/preference/ListPreference;

    invoke-virtual {v1}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 690
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mProfileFeedIntervalPreference:Landroid/preference/ListPreference;

    iget-object v2, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mProfileFeedIntervalPreference:Landroid/preference/ListPreference;

    invoke-virtual {v2}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 691
    :cond_0
    return-void
.end method

.method public readProfilesSyncInterval()V
    .locals 6

    .prologue
    .line 700
    const-string v1, "FB_profiles_sync_interval"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mProfilesSharedPref:Landroid/content/SharedPreferences;

    .line 702
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mProfilesSharedPref:Landroid/content/SharedPreferences;

    const-string v2, "FB_profiles_sync_interval"

    const-wide/16 v4, 0x0

    invoke-interface {v1, v2, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    .line 705
    .local v0, "savedProfilesInterval":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mProfilesIntervalPreference:Landroid/preference/ListPreference;

    invoke-virtual {v1, v0}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 707
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mProfilesIntervalPreference:Landroid/preference/ListPreference;

    invoke-virtual {v1}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 708
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mProfilesIntervalPreference:Landroid/preference/ListPreference;

    iget-object v2, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mProfilesIntervalPreference:Landroid/preference/ListPreference;

    invoke-virtual {v2}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 709
    :cond_0
    return-void
.end method

.method public readStreamsSyncInterval()V
    .locals 6

    .prologue
    .line 718
    const-string v1, "FB_streams_sync_interval"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mStreamsSharedPref:Landroid/content/SharedPreferences;

    .line 720
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mStreamsSharedPref:Landroid/content/SharedPreferences;

    const-string v2, "FB_streams_sync_interval"

    const-wide/16 v4, 0x0

    invoke-interface {v1, v2, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    .line 723
    .local v0, "savedStreamsInterval":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mStreamsIntervalPreference:Landroid/preference/ListPreference;

    invoke-virtual {v1, v0}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 725
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mStreamsIntervalPreference:Landroid/preference/ListPreference;

    invoke-virtual {v1}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 726
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mStreamsIntervalPreference:Landroid/preference/ListPreference;

    iget-object v2, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mStreamsIntervalPreference:Landroid/preference/ListPreference;

    invoke-virtual {v2}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 727
    :cond_0
    return-void
.end method

.method public writeCalendarInterval(Ljava/lang/Long;)V
    .locals 4
    .param p1, "value"    # Ljava/lang/Long;

    .prologue
    .line 640
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mCalendarSharedPref:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 641
    .local v0, "calendarEditor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "FB_calendar_sync_interval"

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 642
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 643
    return-void
.end method

.method public writeContactInterval(Ljava/lang/Long;)V
    .locals 4
    .param p1, "value"    # Ljava/lang/Long;

    .prologue
    .line 621
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mContactSharedPref:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 622
    .local v0, "contactEditor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "FB_contact_sync_interval"

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 623
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 624
    return-void
.end method

.method public writeGalleryInterval(Ljava/lang/Long;)V
    .locals 4
    .param p1, "value"    # Ljava/lang/Long;

    .prologue
    .line 658
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mGallerySharedPref:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 659
    .local v0, "galleryEditor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "FB_gallery_sync_interval"

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 660
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 661
    return-void
.end method

.method public writeHomeFeedInterval(Ljava/lang/Long;)V
    .locals 4
    .param p1, "value"    # Ljava/lang/Long;

    .prologue
    .line 676
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mHomeFeedSharedPref:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 677
    .local v0, "homeFeedEditor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "FB_home_feed_sync_interval"

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 678
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 679
    return-void
.end method

.method public writeProfileFeedInterval(Ljava/lang/Long;)V
    .locals 4
    .param p1, "value"    # Ljava/lang/Long;

    .prologue
    .line 694
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mProfileFeedSharedPref:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 695
    .local v0, "profileFeedEditor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "FB_profile_feed_sync_interval"

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 696
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 697
    return-void
.end method

.method public writeProfilesInterval(Ljava/lang/Long;)V
    .locals 4
    .param p1, "value"    # Ljava/lang/Long;

    .prologue
    .line 712
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mProfilesSharedPref:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 713
    .local v0, "profilesEditor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "FB_profiles_sync_interval"

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 714
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 715
    return-void
.end method

.method public writeStreamsInterval(Ljava/lang/Long;)V
    .locals 4
    .param p1, "value"    # Ljava/lang/Long;

    .prologue
    .line 730
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncIntervalActivity;->mStreamsSharedPref:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 731
    .local v0, "streamsEditor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "FB_streams_sync_interval"

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 732
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 733
    return-void
.end method
