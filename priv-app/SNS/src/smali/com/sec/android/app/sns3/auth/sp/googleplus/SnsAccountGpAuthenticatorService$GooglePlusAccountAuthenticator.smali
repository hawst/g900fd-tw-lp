.class public Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthenticatorService$GooglePlusAccountAuthenticator;
.super Landroid/accounts/AbstractAccountAuthenticator;
.source "SnsAccountGpAuthenticatorService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthenticatorService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "GooglePlusAccountAuthenticator"
.end annotation


# instance fields
.field private mAppContext:Landroid/content/Context;

.field final synthetic this$0:Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthenticatorService;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthenticatorService;Landroid/content/Context;)V
    .locals 0
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 79
    iput-object p1, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthenticatorService$GooglePlusAccountAuthenticator;->this$0:Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthenticatorService;

    .line 80
    invoke-direct {p0, p2}, Landroid/accounts/AbstractAccountAuthenticator;-><init>(Landroid/content/Context;)V

    .line 81
    iput-object p2, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthenticatorService$GooglePlusAccountAuthenticator;->mAppContext:Landroid/content/Context;

    .line 82
    return-void
.end method


# virtual methods
.method public addAccount(Landroid/accounts/AccountAuthenticatorResponse;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 7
    .param p1, "response"    # Landroid/accounts/AccountAuthenticatorResponse;
    .param p2, "accountType"    # Ljava/lang/String;
    .param p3, "authTokenType"    # Ljava/lang/String;
    .param p4, "requiredFeatures"    # [Ljava/lang/String;
    .param p5, "options"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/accounts/NetworkErrorException;
        }
    .end annotation

    .prologue
    .line 88
    invoke-static {}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->isLoggable()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 89
    # getter for: Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthenticatorService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthenticatorService;->access$000()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "GooglePlusAuthenticatorService : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " - "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 93
    :cond_0
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 95
    .local v3, "result":Landroid/os/Bundle;
    if-eqz p5, :cond_2

    # getter for: Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthenticatorService;->OPTIONS_USERNAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthenticatorService;->access$100()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p5, v4}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 96
    new-instance v0, Landroid/accounts/Account;

    # getter for: Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthenticatorService;->OPTIONS_USERNAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthenticatorService;->access$100()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p5, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "com.sec.android.app.sns3.googleplus"

    invoke-direct {v0, v4, v5}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    .local v0, "account":Landroid/accounts/Account;
    iget-object v4, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthenticatorService$GooglePlusAccountAuthenticator;->mAppContext:Landroid/content/Context;

    invoke-static {v4}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    .line 100
    .local v1, "am":Landroid/accounts/AccountManager;
    # getter for: Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthenticatorService;->OPTIONS_PASSWORD:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthenticatorService;->access$200()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p5, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v1, v0, v4, v5}, Landroid/accounts/AccountManager;->addAccountExplicitly(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 101
    const-string v4, "authAccount"

    # getter for: Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthenticatorService;->OPTIONS_USERNAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthenticatorService;->access$100()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p5, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    const-string v4, "accountType"

    const-string v5, "com.sec.android.app.sns3.googleplus"

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    new-instance v2, Landroid/content/Intent;

    const-string v4, "com.sec.android.app.sns.ACTION_SNS_GOOGLEPLUS_LOGGED_IN"

    invoke-direct {v2, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 107
    .local v2, "intent":Landroid/content/Intent;
    iget-object v4, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthenticatorService$GooglePlusAccountAuthenticator;->this$0:Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthenticatorService;

    const-string v5, "com.sec.android.app.sns3.permission.RECEIVE_SNS_BROADCAST"

    invoke-virtual {v4, v2, v5}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthenticatorService;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    .line 121
    .end local v0    # "account":Landroid/accounts/Account;
    .end local v1    # "am":Landroid/accounts/AccountManager;
    .end local v2    # "intent":Landroid/content/Intent;
    :cond_1
    :goto_0
    return-object v3

    .line 110
    :cond_2
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 112
    .restart local v2    # "intent":Landroid/content/Intent;
    iget-object v4, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthenticatorService$GooglePlusAccountAuthenticator;->mAppContext:Landroid/content/Context;

    const-class v5, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 116
    const-string v4, "accountAuthenticatorResponse"

    invoke-virtual {v2, v4, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 117
    const-string v4, "manageAccount"

    invoke-virtual {v2, v4, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 118
    const-string v4, "intent"

    invoke-virtual {v3, v4, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    goto :goto_0
.end method

.method public confirmCredentials(Landroid/accounts/AccountAuthenticatorResponse;Landroid/accounts/Account;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 2
    .param p1, "response"    # Landroid/accounts/AccountAuthenticatorResponse;
    .param p2, "account"    # Landroid/accounts/Account;
    .param p3, "options"    # Landroid/os/Bundle;

    .prologue
    .line 127
    # getter for: Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthenticatorService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthenticatorService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "GooglePlusAuthenticatorService : confirmCredentials"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 128
    const/4 v0, 0x0

    return-object v0
.end method

.method public editProperties(Landroid/accounts/AccountAuthenticatorResponse;Ljava/lang/String;)Landroid/os/Bundle;
    .locals 2
    .param p1, "response"    # Landroid/accounts/AccountAuthenticatorResponse;
    .param p2, "accountType"    # Ljava/lang/String;

    .prologue
    .line 133
    # getter for: Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthenticatorService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthenticatorService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "GooglePlusAuthenticatorService : editProperties"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 134
    const/4 v0, 0x0

    return-object v0
.end method

.method public getAccountRemovalAllowed(Landroid/accounts/AccountAuthenticatorResponse;Landroid/accounts/Account;)Landroid/os/Bundle;
    .locals 10
    .param p1, "response"    # Landroid/accounts/AccountAuthenticatorResponse;
    .param p2, "account"    # Landroid/accounts/Account;

    .prologue
    .line 139
    # getter for: Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthenticatorService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthenticatorService;->access$000()Ljava/lang/String;

    move-result-object v6

    const-string v7, "GooglePlusAuthenticatorService : getAccountRemovalAllowed"

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 141
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v0

    .line 142
    .local v0, "app":Lcom/sec/android/app/sns3/SnsApplication;
    new-instance v1, Lcom/sec/android/app/sns3/agent/sp/googleplus/command/SnsGpCmdAuthLogout;

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/SnsApplication;->getSvcMgr()Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v6

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/SnsApplication;->getAgentMgr()Lcom/sec/android/app/sns3/agent/SnsAgentMgr;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/sns3/agent/SnsAgentMgr;->getCommandMgr()Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;->getCommandMgrHandle()Landroid/os/Handler;

    move-result-object v7

    invoke-direct {v1, v6, v7}, Lcom/sec/android/app/sns3/agent/sp/googleplus/command/SnsGpCmdAuthLogout;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Handler;)V

    .line 144
    .local v1, "cmd":Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;
    new-instance v6, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthenticatorService$GooglePlusAccountAuthenticator$1;

    invoke-direct {v6, p0}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthenticatorService$GooglePlusAccountAuthenticator$1;-><init>(Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthenticatorService$GooglePlusAccountAuthenticator;)V

    invoke-virtual {v1, v6}, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->setCommandCallback(Lcom/sec/android/app/sns3/agent/command/ISnsCommandCallback;)Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;

    .line 154
    invoke-virtual {v1}, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->send()I

    .line 157
    iget-object v6, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthenticatorService$GooglePlusAccountAuthenticator;->this$0:Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthenticatorService;

    const-string v7, "GP_profile_feed_sync_interval"

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthenticatorService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v6

    invoke-interface {v6}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    .line 159
    .local v4, "profileFeedEditor":Landroid/content/SharedPreferences$Editor;
    const-string v6, "GP_profile_feed_sync_interval"

    const-wide/16 v8, 0x0

    invoke-interface {v4, v6, v8, v9}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 160
    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 163
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v6

    const-string v7, "notification"

    invoke-virtual {v6, v7}, Lcom/sec/android/app/sns3/SnsApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/app/NotificationManager;

    move-object v3, v6

    check-cast v3, Landroid/app/NotificationManager;

    .line 165
    .local v3, "notiMgr":Landroid/app/NotificationManager;
    const/16 v6, 0x14b4

    invoke-virtual {v3, v6}, Landroid/app/NotificationManager;->cancel(I)V

    .line 168
    new-instance v2, Landroid/content/Intent;

    const-string v6, "com.sec.android.app.sns.ACTION_SNS_GOOGLEPLUS_LOGGED_OUT"

    invoke-direct {v2, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 169
    .local v2, "intent":Landroid/content/Intent;
    iget-object v6, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthenticatorService$GooglePlusAccountAuthenticator;->this$0:Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthenticatorService;

    const-string v7, "com.sec.android.app.sns3.permission.RECEIVE_SNS_BROADCAST"

    invoke-virtual {v6, v2, v7}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthenticatorService;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    .line 171
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 172
    .local v5, "result":Landroid/os/Bundle;
    const-string v6, "booleanResult"

    const/4 v7, 0x1

    invoke-virtual {v5, v6, v7}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 173
    return-object v5
.end method

.method public getAuthToken(Landroid/accounts/AccountAuthenticatorResponse;Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 2
    .param p1, "response"    # Landroid/accounts/AccountAuthenticatorResponse;
    .param p2, "account"    # Landroid/accounts/Account;
    .param p3, "authTokenType"    # Ljava/lang/String;
    .param p4, "loginOptions"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/accounts/NetworkErrorException;
        }
    .end annotation

    .prologue
    .line 179
    # getter for: Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthenticatorService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthenticatorService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "GooglePlusAuthenticatorService : getAuthToken"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 180
    const/4 v0, 0x0

    return-object v0
.end method

.method public getAuthTokenLabel(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "authTokenType"    # Ljava/lang/String;

    .prologue
    .line 185
    # getter for: Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthenticatorService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthenticatorService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "GooglePlusAuthenticatorService : getAuthTokenLabel"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 186
    const/4 v0, 0x0

    return-object v0
.end method

.method public hasFeatures(Landroid/accounts/AccountAuthenticatorResponse;Landroid/accounts/Account;[Ljava/lang/String;)Landroid/os/Bundle;
    .locals 2
    .param p1, "response"    # Landroid/accounts/AccountAuthenticatorResponse;
    .param p2, "account"    # Landroid/accounts/Account;
    .param p3, "features"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/accounts/NetworkErrorException;
        }
    .end annotation

    .prologue
    .line 192
    # getter for: Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthenticatorService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthenticatorService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "GooglePlusAuthenticatorService : hasFeatures"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 193
    const/4 v0, 0x0

    return-object v0
.end method

.method public updateCredentials(Landroid/accounts/AccountAuthenticatorResponse;Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 2
    .param p1, "response"    # Landroid/accounts/AccountAuthenticatorResponse;
    .param p2, "account"    # Landroid/accounts/Account;
    .param p3, "authTokenType"    # Ljava/lang/String;
    .param p4, "loginOptions"    # Landroid/os/Bundle;

    .prologue
    .line 199
    # getter for: Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthenticatorService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthenticatorService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "GooglePlusAuthenticatorService : updateCredentials"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 200
    const/4 v0, 0x0

    return-object v0
.end method
