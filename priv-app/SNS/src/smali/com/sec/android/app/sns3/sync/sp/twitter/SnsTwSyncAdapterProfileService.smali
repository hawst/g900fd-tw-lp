.class public Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterProfileService;
.super Landroid/app/Service;
.source "SnsTwSyncAdapterProfileService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterProfileService$SyncAdapterImpl;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "SnsTwSync"


# instance fields
.field private mAccount:Landroid/accounts/Account;

.field private mAuthority:Ljava/lang/String;

.field private mCmdHandler:Landroid/os/Handler;

.field private mContext:Landroid/content/Context;

.field private mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

.field private mSyncAdapter:Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterProfileService$SyncAdapterImpl;

.field private mSyncResult:Landroid/content/SyncResult;

.field private mSyncState:I

.field private mTwToken:Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 56
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 60
    iput-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterProfileService;->mSyncAdapter:Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterProfileService$SyncAdapterImpl;

    .line 62
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterProfileService;->mSyncState:I

    .line 64
    iput-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterProfileService;->mSyncResult:Landroid/content/SyncResult;

    .line 66
    iput-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterProfileService;->mAccount:Landroid/accounts/Account;

    .line 68
    iput-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterProfileService;->mAuthority:Ljava/lang/String;

    .line 74
    iput-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterProfileService;->mTwToken:Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;

    .line 76
    return-void
.end method

.method static synthetic access$002(Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterProfileService;Landroid/content/Context;)Landroid/content/Context;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterProfileService;
    .param p1, "x1"    # Landroid/content/Context;

    .prologue
    .line 56
    iput-object p1, p0, Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterProfileService;->mContext:Landroid/content/Context;

    return-object p1
.end method

.method static synthetic access$102(Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterProfileService;Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;)Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterProfileService;
    .param p1, "x1"    # Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;

    .prologue
    .line 56
    iput-object p1, p0, Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterProfileService;->mTwToken:Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;

    return-object p1
.end method

.method static synthetic access$202(Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterProfileService;Landroid/accounts/Account;)Landroid/accounts/Account;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterProfileService;
    .param p1, "x1"    # Landroid/accounts/Account;

    .prologue
    .line 56
    iput-object p1, p0, Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterProfileService;->mAccount:Landroid/accounts/Account;

    return-object p1
.end method

.method static synthetic access$302(Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterProfileService;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterProfileService;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 56
    iput-object p1, p0, Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterProfileService;->mAuthority:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$402(Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterProfileService;Landroid/content/SyncResult;)Landroid/content/SyncResult;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterProfileService;
    .param p1, "x1"    # Landroid/content/SyncResult;

    .prologue
    .line 56
    iput-object p1, p0, Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterProfileService;->mSyncResult:Landroid/content/SyncResult;

    return-object p1
.end method

.method static synthetic access$500(Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterProfileService;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterProfileService;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/accounts/OperationCanceledException;
        }
    .end annotation

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterProfileService;->performSync()V

    return-void
.end method

.method static synthetic access$600(Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterProfileService;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterProfileService;

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterProfileService;->onSyncCanceled()V

    return-void
.end method

.method static synthetic access$702(Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterProfileService;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterProfileService;
    .param p1, "x1"    # I

    .prologue
    .line 56
    iput p1, p0, Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterProfileService;->mSyncState:I

    return p1
.end method

.method private handleSessionExpired()I
    .locals 11

    .prologue
    const/4 v9, 0x1

    const/4 v10, 0x0

    .line 212
    iget-object v7, p0, Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterProfileService;->mAccount:Landroid/accounts/Account;

    const-string v8, "com.sec.android.app.sns3.home"

    invoke-static {v7, v8}, Landroid/content/ContentResolver;->cancelSync(Landroid/accounts/Account;Ljava/lang/String;)V

    .line 214
    iget-object v7, p0, Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterProfileService;->mContext:Landroid/content/Context;

    const-string v8, "notification"

    invoke-virtual {v7, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/app/NotificationManager;

    .line 217
    .local v4, "notiMgr":Landroid/app/NotificationManager;
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 219
    .local v3, "notiIntent":Landroid/content/Intent;
    const-string v7, "com.sec.android.app.sns3.RETRY_SSO_TWITTER"

    invoke-virtual {v3, v7}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 223
    const/high16 v7, 0x800000

    invoke-virtual {v3, v7}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 224
    const-string v7, "RetryLogin"

    invoke-virtual {v3, v7, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 225
    iget-object v7, p0, Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterProfileService;->mContext:Landroid/content/Context;

    invoke-static {v7, v10, v3, v10}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 227
    .local v1, "launchIntent":Landroid/app/PendingIntent;
    const v6, 0x7f08003f

    .line 228
    .local v6, "titleID":I
    const v5, 0x7f08006d

    .line 229
    .local v5, "spID":I
    const v0, 0x7f02001f

    .line 231
    .local v0, "iconID":I
    new-instance v2, Landroid/app/Notification$Builder;

    iget-object v7, p0, Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterProfileService;->mContext:Landroid/content/Context;

    invoke-direct {v2, v7}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    .line 232
    .local v2, "notiBuilder":Landroid/app/Notification$Builder;
    iget-object v7, p0, Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterProfileService;->mContext:Landroid/content/Context;

    new-array v8, v9, [Ljava/lang/Object;

    iget-object v9, p0, Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterProfileService;->mContext:Landroid/content/Context;

    invoke-virtual {v9, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v10

    invoke-virtual {v7, v6, v8}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v7

    invoke-virtual {v7, v0}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterProfileService;->mContext:Landroid/content/Context;

    const v9, 0x7f08003e

    invoke-virtual {v8, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v7

    invoke-virtual {v7, v1}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v7

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Landroid/app/Notification$Builder;->setWhen(J)Landroid/app/Notification$Builder;

    .line 237
    const/16 v7, 0x8fc

    invoke-virtual {v2}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v8

    invoke-virtual {v4, v7, v8}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 239
    const/4 v7, -0x1

    return v7
.end method

.method private invokeBroadcast()V
    .locals 7

    .prologue
    .line 265
    const/4 v2, 0x0

    .line 267
    .local v2, "intent":Landroid/content/Intent;
    sget-object v3, Lcom/sec/android/app/sns3/agent/sp/twitter/db/SnsTwitterDB$FriendsProfileInfo;->CONTENT_URI:Landroid/net/Uri;

    .line 268
    .local v3, "uri":Landroid/net/Uri;
    const-string v0, "com.sec.android.app.sns3.action.SYNC_TW_PROFILES"

    .line 269
    .local v0, "action":Ljava/lang/String;
    iget v4, p0, Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterProfileService;->mSyncState:I

    const/4 v5, 0x2

    if-ne v4, v5, :cond_1

    const/4 v1, 0x1

    .line 271
    .local v1, "bResult":Z
    :goto_0
    new-instance v2, Landroid/content/Intent;

    .end local v2    # "intent":Landroid/content/Intent;
    invoke-direct {v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 272
    .restart local v2    # "intent":Landroid/content/Intent;
    const-string v4, "SNS3_CONTENT_URI_FRIENDS"

    invoke-virtual {v2, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 273
    const-string v4, "SNS_RESULT"

    invoke-virtual {v2, v4, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 275
    const-string v4, "com.sec.android.app.sns3.permission.RECEIVE_SNS_BROADCAST"

    invoke-virtual {p0, v2, v4}, Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterProfileService;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    .line 276
    invoke-static {}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->isLoggable()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 277
    const-string v4, "SnsTwSync"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "SnsTwSyncAdapterProfileService - invokeBroadcast() : action = ["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "], uri = ["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "], result = ["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 280
    :cond_0
    return-void

    .line 269
    .end local v1    # "bResult":Z
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private onSyncCanceled()V
    .locals 2

    .prologue
    .line 256
    const-string v0, "SnsTwSync"

    const-string v1, "***************** SnsTwSyncAdapterProfileService : onSyncCanceled !!! *****************"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 259
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterProfileService;->mSyncState:I

    .line 261
    return-void
.end method

.method private performSync()V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/accounts/OperationCanceledException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 159
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/sns3/SnsApplication;->getAgentMgr()Lcom/sec/android/app/sns3/agent/SnsAgentMgr;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/sns3/agent/SnsAgentMgr;->getCommandMgr()Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;->getCommandMgrHandle()Landroid/os/Handler;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterProfileService;->mCmdHandler:Landroid/os/Handler;

    .line 161
    iput v2, p0, Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterProfileService;->mSyncState:I

    .line 163
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterProfileService;->mTwToken:Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;

    invoke-virtual {v1}, Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;->getTokenState()I

    move-result v1

    if-eq v1, v2, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterProfileService;->mTwToken:Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;

    invoke-virtual {v1}, Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;->getTokenState()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    .line 165
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterProfileService;->handleSessionExpired()I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterProfileService;->mSyncState:I

    .line 166
    new-instance v1, Ljava/lang/Exception;

    const-string v2, "Session expired or invalid!!!"

    invoke-direct {v1, v2}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 196
    :catch_0
    move-exception v6

    .line 197
    .local v6, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v6}, Ljava/lang/Exception;->printStackTrace()V

    .line 198
    const-string v1, "SnsTwSync"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SnsTwSyncAdapterProfileService : EXCEPTION !!! "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v6}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 200
    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterProfileService;->mSyncResult:Landroid/content/SyncResult;

    iget-object v1, v1, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    const-wide/16 v2, 0x1

    iput-wide v2, v1, Landroid/content/SyncStats;->numConflictDetectedExceptions:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 203
    invoke-direct {p0}, Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterProfileService;->invokeBroadcast()V

    .line 205
    const-string v1, "SnsTwSync"

    const-string v2, "***************** SnsTwSyncAdapterProfileService : performSync - FINISHED !!! *****************"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 209
    .end local v6    # "e":Ljava/lang/Exception;
    :goto_0
    return-void

    .line 168
    :cond_1
    :try_start_2
    new-instance v0, Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdGetFollowers;

    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterProfileService;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    iget-object v2, p0, Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterProfileService;->mCmdHandler:Landroid/os/Handler;

    const-string v3, "me"

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdGetFollowers;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Handler;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 170
    .local v0, "cmd":Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;
    new-instance v1, Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterProfileService$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterProfileService$1;-><init>(Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterProfileService;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->setCommandCallback(Lcom/sec/android/app/sns3/agent/command/ISnsCommandCallback;)Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;

    .line 186
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->send()I

    .line 188
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterProfileService;->suspendSync()V

    .line 191
    iget v1, p0, Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterProfileService;->mSyncState:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_2

    .line 192
    new-instance v1, Ljava/lang/Exception;

    const-string v2, "[SnsTwSyncAdapterProfileService] updates sync is failed!!!"

    invoke-direct {v1, v2}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 203
    .end local v0    # "cmd":Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;
    :catchall_0
    move-exception v1

    invoke-direct {p0}, Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterProfileService;->invokeBroadcast()V

    .line 205
    const-string v2, "SnsTwSync"

    const-string v3, "***************** SnsTwSyncAdapterProfileService : performSync - FINISHED !!! *****************"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    throw v1

    .line 203
    .restart local v0    # "cmd":Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterProfileService;->invokeBroadcast()V

    .line 205
    const-string v1, "SnsTwSync"

    const-string v2, "***************** SnsTwSyncAdapterProfileService : performSync - FINISHED !!! *****************"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 128
    const-string v0, "SnsTwSync"

    const-string v1, "***************** SnsTwSyncAdapterProfileService : onBind !!! *****************"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 131
    iget-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterProfileService;->mSyncAdapter:Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterProfileService$SyncAdapterImpl;

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterProfileService$SyncAdapterImpl;->getSyncAdapterBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 115
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 117
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/SnsApplication;->getSvcMgr()Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterProfileService;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    .line 119
    iget-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterProfileService;->mSyncAdapter:Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterProfileService$SyncAdapterImpl;

    if-nez v0, :cond_0

    .line 120
    new-instance v0, Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterProfileService$SyncAdapterImpl;

    invoke-direct {v0, p0, p0}, Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterProfileService$SyncAdapterImpl;-><init>(Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterProfileService;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterProfileService;->mSyncAdapter:Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterProfileService$SyncAdapterImpl;

    .line 123
    :cond_0
    return-void
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 137
    const/4 v0, 0x0

    .line 138
    .local v0, "bActive":Z
    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterProfileService;->mAccount:Landroid/accounts/Account;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterProfileService;->mAuthority:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 139
    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterProfileService;->mAccount:Landroid/accounts/Account;

    iget-object v2, p0, Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterProfileService;->mAuthority:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/content/ContentResolver;->isSyncActive(Landroid/accounts/Account;Ljava/lang/String;)Z

    move-result v0

    .line 142
    :cond_0
    const-string v1, "SnsTwSync"

    const-string v2, "***************** SnsTwSyncAdapterProfileService : onUnbind !!! *****************"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 145
    if-eqz v0, :cond_2

    iget v1, p0, Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterProfileService;->mSyncState:I

    const/4 v2, 0x1

    if-eq v1, v2, :cond_1

    iget v1, p0, Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterProfileService;->mSyncState:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_2

    .line 147
    :cond_1
    const-string v1, "SnsTwSync"

    const-string v2, "SnsTwSyncAdapterProfileService : onUnbind : SYNC ERROR : performSync was stopped by forced abort or pending problem!!!"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 153
    :goto_0
    invoke-super {p0, p1}, Landroid/app/Service;->onUnbind(Landroid/content/Intent;)Z

    move-result v1

    return v1

    .line 151
    :cond_2
    const-string v1, "SnsTwSync"

    const-string v2, "SnsTwSyncAdapterProfileService : onUnbind : COMPLETE STATE!!!"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public declared-synchronized resumeSync()V
    .locals 1

    .prologue
    .line 251
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->notify()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 252
    monitor-exit p0

    return-void

    .line 251
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized suspendSync()V
    .locals 2

    .prologue
    .line 244
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 248
    :goto_0
    monitor-exit p0

    return-void

    .line 245
    :catch_0
    move-exception v0

    .line 246
    .local v0, "e":Ljava/lang/InterruptedException;
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 244
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method
