.class public Lcom/sec/android/app/sns3/svc/sp/qzone/parser/SnsQzParserBirthday;
.super Ljava/lang/Object;
.source "SnsQzParserBirthday.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/qzone/parser/SnsQzParserBirthday$QzoneBirthday;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    return-void
.end method

.method public static parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseBirthday;
    .locals 12
    .param p0, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 57
    const/4 v1, 0x0

    .line 58
    .local v1, "birthday":Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseBirthday;
    const/4 v3, 0x0

    .line 60
    .local v3, "curBirthday":Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseBirthday;
    const/4 v8, 0x0

    .line 63
    .local v8, "jsonObject":Lorg/json/JSONObject;
    :try_start_0
    new-instance v9, Lorg/json/JSONObject;

    invoke-direct {v9, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 65
    .end local v8    # "jsonObject":Lorg/json/JSONObject;
    .local v9, "jsonObject":Lorg/json/JSONObject;
    :try_start_1
    new-instance v7, Lorg/json/JSONObject;

    const-string v11, "data"

    invoke-virtual {v9, v11}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v7, v11}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 67
    .local v7, "jdata":Lorg/json/JSONObject;
    const-string v11, "hostopenid"

    invoke-virtual {v7, v11}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 69
    .local v0, "HostID":Ljava/lang/String;
    new-instance v6, Lorg/json/JSONArray;

    const-string v11, "friends"

    invoke-virtual {v7, v11}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v6, v11}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 71
    .local v6, "jabirthdays":Lorg/json/JSONArray;
    if-eqz v6, :cond_1

    .line 73
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    invoke-virtual {v6}, Lorg/json/JSONArray;->length()I

    move-result v11

    if-ge v5, v11, :cond_1

    .line 74
    new-instance v10, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseBirthday;

    invoke-direct {v10}, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseBirthday;-><init>()V

    .line 76
    .local v10, "newBirthday":Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseBirthday;
    invoke-virtual {v6, v5}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v2

    .line 78
    .local v2, "birthdayJsonObj":Lorg/json/JSONObject;
    const-string v11, "openid"

    invoke-virtual {v2, v11}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    iput-object v11, v10, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseBirthday;->mFriendOpenID:Ljava/lang/String;

    .line 79
    iput-object v0, v10, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseBirthday;->mHostOpenID:Ljava/lang/String;

    .line 80
    const-string v11, "gender"

    invoke-virtual {v2, v11}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    iput-object v11, v10, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseBirthday;->mGender:Ljava/lang/String;

    .line 81
    const-string v11, "month"

    invoke-virtual {v2, v11}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    iput-object v11, v10, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseBirthday;->mBirthdayMonth:Ljava/lang/String;

    .line 83
    const-string v11, "day"

    invoke-virtual {v2, v11}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    iput-object v11, v10, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseBirthday;->mBirthdayDay:Ljava/lang/String;

    .line 84
    const-string v11, "nickname"

    invoke-virtual {v2, v11}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    iput-object v11, v10, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseBirthday;->mNickName:Ljava/lang/String;

    .line 85
    const-string v11, "lunarflag"

    invoke-virtual {v2, v11}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    iput-object v11, v10, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseBirthday;->mLunarFlag:Ljava/lang/String;

    .line 86
    if-nez v1, :cond_0

    .line 87
    move-object v1, v10

    .line 88
    move-object v3, v1

    .line 73
    :goto_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 90
    :cond_0
    iput-object v10, v3, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseBirthday;->mNext:Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseBirthday;

    .line 91
    iget-object v3, v3, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseBirthday;->mNext:Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseBirthday;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .end local v2    # "birthdayJsonObj":Lorg/json/JSONObject;
    .end local v5    # "i":I
    .end local v10    # "newBirthday":Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseBirthday;
    :cond_1
    move-object v8, v9

    .line 101
    .end local v0    # "HostID":Ljava/lang/String;
    .end local v6    # "jabirthdays":Lorg/json/JSONArray;
    .end local v7    # "jdata":Lorg/json/JSONObject;
    .end local v9    # "jsonObject":Lorg/json/JSONObject;
    .restart local v8    # "jsonObject":Lorg/json/JSONObject;
    :goto_2
    return-object v1

    .line 96
    :catch_0
    move-exception v4

    .line 98
    .local v4, "e":Lorg/json/JSONException;
    :goto_3
    invoke-virtual {v4}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_2

    .line 96
    .end local v4    # "e":Lorg/json/JSONException;
    .end local v8    # "jsonObject":Lorg/json/JSONObject;
    .restart local v9    # "jsonObject":Lorg/json/JSONObject;
    :catch_1
    move-exception v4

    move-object v8, v9

    .end local v9    # "jsonObject":Lorg/json/JSONObject;
    .restart local v8    # "jsonObject":Lorg/json/JSONObject;
    goto :goto_3
.end method
