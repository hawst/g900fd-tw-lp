.class Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdAuthLogin$1;
.super Lcom/sec/android/app/sns3/svc/sp/foursquare/request/SnsFsReqGetUser;
.source "SnsFsCmdAuthLogin.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdAuthLogin;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Handler;Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdAuthLogin;


# direct methods
.method constructor <init>(Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdAuthLogin;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;)V
    .locals 0
    .param p2, "x0"    # Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    .param p3, "x1"    # Ljava/lang/String;

    .prologue
    .line 43
    iput-object p1, p0, Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdAuthLogin$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdAuthLogin;

    invoke-direct {p0, p2, p3}, Lcom/sec/android/app/sns3/svc/sp/foursquare/request/SnsFsReqGetUser;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public onReqRespond(IZIILandroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;)Z
    .locals 6
    .param p1, "reqID"    # I
    .param p2, "bSuccess"    # Z
    .param p3, "httpStatus"    # I
    .param p4, "errorCode"    # I
    .param p5, "reason"    # Landroid/os/Bundle;
    .param p6, "user"    # Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;

    .prologue
    const/4 v3, 0x0

    .line 48
    const-string v2, "SnsAgent"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "SnsFsCmdAuthLogin : getUser - onResponse() - bSuccess = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 51
    const/4 v1, 0x0

    .line 52
    .local v1, "userName":Ljava/lang/String;
    const/4 v0, 0x0

    .line 54
    .local v0, "userID":Ljava/lang/String;
    if-eqz p2, :cond_3

    if-eqz p6, :cond_3

    .line 55
    if-nez p5, :cond_0

    .line 56
    new-instance p5, Landroid/os/Bundle;

    .end local p5    # "reason":Landroid/os/Bundle;
    invoke-direct {p5}, Landroid/os/Bundle;-><init>()V

    .line 58
    .restart local p5    # "reason":Landroid/os/Bundle;
    :cond_0
    const-string v2, "userID"

    iget-object v4, p6, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mUserID:Ljava/lang/String;

    invoke-virtual {p5, v2, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    const-string v2, "userName"

    iget-object v4, p6, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mFormattedName:Ljava/lang/String;

    invoke-virtual {p5, v2, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    const-string v4, "email"

    iget-object v2, p6, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mContact:Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseContact;

    if-eqz v2, :cond_2

    iget-object v2, p6, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mContact:Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseContact;

    iget-object v2, v2, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseContact;->mEmail:Ljava/lang/String;

    :goto_0
    invoke-virtual {p5, v4, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 61
    invoke-static {}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->isLoggable()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 62
    const-string v2, "SnsAgent"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "SnsFsCmdAuthLogin : getUser - onResponse() - userID = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", screenName = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 73
    :cond_1
    :goto_1
    iget-object v2, p0, Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdAuthLogin$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdAuthLogin;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdAuthLogin;->setUri(Ljava/lang/String;)V

    .line 74
    iget-object v2, p0, Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdAuthLogin$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdAuthLogin;

    invoke-virtual {v2, p2}, Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdAuthLogin;->setSuccess(Z)V

    .line 75
    iget-object v2, p0, Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdAuthLogin$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdAuthLogin;

    new-instance v3, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;

    const-string v4, "foursquare"

    invoke-direct {v3, v4, p3, p4, p5}, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;-><init>(Ljava/lang/String;IILandroid/os/Bundle;)V

    invoke-virtual {v2, v3}, Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdAuthLogin;->setResponseList(Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;)Z

    .line 77
    iget-object v2, p0, Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdAuthLogin$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdAuthLogin;

    invoke-virtual {v2, p0}, Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdAuthLogin;->receive(Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;)V

    .line 79
    const/4 v2, 0x1

    return v2

    :cond_2
    move-object v2, v3

    .line 60
    goto :goto_0

    .line 67
    :cond_3
    const-string v2, "SnsAgent"

    const-string v4, " Get user fail"

    invoke-static {v2, v4}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 68
    if-eqz p5, :cond_1

    .line 69
    const-string v2, "SnsAgent"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " error message : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "error_message"

    invoke-virtual {p5, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method
