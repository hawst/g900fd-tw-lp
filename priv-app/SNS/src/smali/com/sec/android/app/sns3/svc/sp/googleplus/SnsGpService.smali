.class public Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpService;
.super Landroid/app/Service;
.source "SnsGpService.java"


# static fields
.field public static final KEY_SEARCH_TYPE:Ljava/lang/String; = "searchType"

.field private static final NEARBY_SEARCH:Ljava/lang/String; = "nearbysearch"

.field private static final RADAR_SEARCH:Ljava/lang/String; = "radarsearch"

.field protected static final TAG:Ljava/lang/String; = "SnsGpService"

.field private static final TEXT_SEARCH:Ljava/lang/String; = "textsearch"


# instance fields
.field private mSnsSvcGooglePlusBinder:Lcom/sec/android/app/sns3/svc/sp/googleplus/api/ISnsGooglePlus$Stub;

.field private mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 51
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 87
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpService$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpService$1;-><init>(Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpService;)V

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpService;->mSnsSvcGooglePlusBinder:Lcom/sec/android/app/sns3/svc/sp/googleplus/api/ISnsGooglePlus$Stub;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpService;)Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpService;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpService;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    return-object v0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 75
    const-string v0, "SNS"

    const-string v1, "SnsGpService : onBind()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 77
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpService;->mSnsSvcGooglePlusBinder:Lcom/sec/android/app/sns3/svc/sp/googleplus/api/ISnsGooglePlus$Stub;

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 67
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 68
    const-string v0, "SNS"

    const-string v1, "SnsGpService : onCreate()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 70
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/SnsApplication;->getSvcMgr()Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpService;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    .line 71
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 82
    const-string v0, "SNS"

    const-string v1, "SnsGpService : onStartCommand()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 84
    const/4 v0, 0x1

    return v0
.end method
