.class public Lcom/sec/android/app/sns3/auth/SnsAccountAlertDialogActivity$AlertDialogFragment;
.super Landroid/app/DialogFragment;
.source "SnsAccountAlertDialogActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/auth/SnsAccountAlertDialogActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "AlertDialogFragment"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 94
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    return-void
.end method

.method public static newInstance(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/android/app/sns3/auth/SnsAccountAlertDialogActivity$AlertDialogFragment;
    .locals 3
    .param p0, "title"    # Ljava/lang/String;
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 97
    new-instance v1, Lcom/sec/android/app/sns3/auth/SnsAccountAlertDialogActivity$AlertDialogFragment;

    invoke-direct {v1}, Lcom/sec/android/app/sns3/auth/SnsAccountAlertDialogActivity$AlertDialogFragment;-><init>()V

    .line 98
    .local v1, "frag":Lcom/sec/android/app/sns3/auth/SnsAccountAlertDialogActivity$AlertDialogFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 99
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "dlg_title"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    const-string v2, "dlg_message"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    invoke-virtual {v1, v0}, Lcom/sec/android/app/sns3/auth/SnsAccountAlertDialogActivity$AlertDialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 102
    return-object v1
.end method


# virtual methods
.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 1
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 126
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCancel(Landroid/content/DialogInterface;)V

    .line 127
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/SnsAccountAlertDialogActivity$AlertDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 128
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 107
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/SnsAccountAlertDialogActivity$AlertDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "dlg_title"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 108
    .local v1, "title":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/SnsAccountAlertDialogActivity$AlertDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "dlg_message"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 110
    .local v0, "message":Ljava/lang/String;
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/SnsAccountAlertDialogActivity$AlertDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x104000a

    new-instance v4, Lcom/sec/android/app/sns3/auth/SnsAccountAlertDialogActivity$AlertDialogFragment$2;

    invoke-direct {v4, p0}, Lcom/sec/android/app/sns3/auth/SnsAccountAlertDialogActivity$AlertDialogFragment$2;-><init>(Lcom/sec/android/app/sns3/auth/SnsAccountAlertDialogActivity$AlertDialogFragment;)V

    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const/high16 v3, 0x1040000

    new-instance v4, Lcom/sec/android/app/sns3/auth/SnsAccountAlertDialogActivity$AlertDialogFragment$1;

    invoke-direct {v4, p0}, Lcom/sec/android/app/sns3/auth/SnsAccountAlertDialogActivity$AlertDialogFragment$1;-><init>(Lcom/sec/android/app/sns3/auth/SnsAccountAlertDialogActivity$AlertDialogFragment;)V

    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    return-object v2
.end method
