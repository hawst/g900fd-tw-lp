.class interface abstract Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseGroups$LinkedInGroups;
.super Ljava/lang/Object;
.source "SnsLiResponseGroups.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseGroups;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x608
    name = "LinkedInGroups"
.end annotation


# static fields
.field public static final GROUPS:Ljava/lang/String; = "group"

.field public static final GROUP_ID:Ljava/lang/String; = "id"

.field public static final GROUP_KEY:Ljava/lang/String; = "_key"

.field public static final GROUP_NAME:Ljava/lang/String; = "name"

.field public static final MEMBERSIP_CODE:Ljava/lang/String; = "code"

.field public static final MEMBERSIP_STATE:Ljava/lang/String; = "membershipState"

.field public static final VALUE_TAG:Ljava/lang/String; = "values"
