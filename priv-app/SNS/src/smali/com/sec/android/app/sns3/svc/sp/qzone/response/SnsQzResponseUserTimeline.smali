.class public final Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseUserTimeline;
.super Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;
.source "SnsQzResponseUserTimeline.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseUserTimeline;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public mAuthorName:Ljava/lang/String;

.field public mAuthorPhonenumber:Ljava/lang/String;

.field public mCommentCount:I

.field public mCreatedTime:Ljava/lang/String;

.field public mFeedID:Ljava/lang/String;

.field public mHostOpenID:Ljava/lang/String;

.field public mLikeCount:I

.field public mLinkUri:Ljava/lang/String;

.field public mMessage:Ljava/lang/String;

.field public mNext:Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseUserTimeline;

.field public mOpenID:Ljava/lang/String;

.field public mOriginalAuthorName:Ljava/lang/String;

.field public mOriginalMessage:Ljava/lang/String;

.field public mOriginalPicture:Ljava/lang/String;

.field public mOriginalPictureCount:I

.field public mOriginalTitle:Ljava/lang/String;

.field public mPicture:Ljava/lang/String;

.field public mPictureCount:I

.field public mProfileUrl:Ljava/lang/String;

.field public mTitle:Ljava/lang/String;

.field public mType:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 71
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseUserTimeline$1;

    invoke-direct {v0}, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseUserTimeline$1;-><init>()V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseUserTimeline;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 82
    invoke-direct {p0}, Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;-><init>()V

    .line 83
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 85
    invoke-direct {p0}, Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;-><init>()V

    .line 86
    invoke-virtual {p0, p1}, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseUserTimeline;->readFromParcel(Landroid/os/Parcel;)V

    .line 87
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseUserTimeline$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseUserTimeline$1;

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseUserTimeline;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 144
    const/4 v0, 0x0

    return v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 117
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseUserTimeline;->mFeedID:Ljava/lang/String;

    .line 118
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseUserTimeline;->mTitle:Ljava/lang/String;

    .line 119
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseUserTimeline;->mMessage:Ljava/lang/String;

    .line 120
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseUserTimeline;->mPicture:Ljava/lang/String;

    .line 121
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseUserTimeline;->mPictureCount:I

    .line 122
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseUserTimeline;->mLikeCount:I

    .line 123
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseUserTimeline;->mCommentCount:I

    .line 124
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseUserTimeline;->mAuthorName:Ljava/lang/String;

    .line 125
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseUserTimeline;->mAuthorPhonenumber:Ljava/lang/String;

    .line 126
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseUserTimeline;->mHostOpenID:Ljava/lang/String;

    .line 127
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseUserTimeline;->mOpenID:Ljava/lang/String;

    .line 128
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseUserTimeline;->mProfileUrl:Ljava/lang/String;

    .line 129
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseUserTimeline;->mType:Ljava/lang/String;

    .line 130
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseUserTimeline;->mOriginalTitle:Ljava/lang/String;

    .line 131
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseUserTimeline;->mOriginalMessage:Ljava/lang/String;

    .line 132
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseUserTimeline;->mOriginalPicture:Ljava/lang/String;

    .line 133
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseUserTimeline;->mOriginalPictureCount:I

    .line 134
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseUserTimeline;->mOriginalAuthorName:Ljava/lang/String;

    .line 135
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseUserTimeline;->mLinkUri:Ljava/lang/String;

    .line 136
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseUserTimeline;->mCreatedTime:Ljava/lang/String;

    .line 138
    const-class v0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseUserTimeline;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseUserTimeline;

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseUserTimeline;->mNext:Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseUserTimeline;

    .line 139
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flag"    # I

    .prologue
    .line 91
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseUserTimeline;->mFeedID:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 92
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseUserTimeline;->mTitle:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 93
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseUserTimeline;->mMessage:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 94
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseUserTimeline;->mPicture:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 95
    iget v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseUserTimeline;->mPictureCount:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 96
    iget v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseUserTimeline;->mLikeCount:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 97
    iget v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseUserTimeline;->mCommentCount:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 98
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseUserTimeline;->mAuthorName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 99
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseUserTimeline;->mAuthorPhonenumber:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 100
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseUserTimeline;->mHostOpenID:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 101
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseUserTimeline;->mOpenID:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 102
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseUserTimeline;->mProfileUrl:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 103
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseUserTimeline;->mType:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 104
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseUserTimeline;->mOriginalTitle:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 105
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseUserTimeline;->mOriginalMessage:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 106
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseUserTimeline;->mOriginalPicture:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 107
    iget v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseUserTimeline;->mOriginalPictureCount:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 108
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseUserTimeline;->mOriginalAuthorName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 109
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseUserTimeline;->mLinkUri:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 110
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseUserTimeline;->mCreatedTime:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 112
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseUserTimeline;->mNext:Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseUserTimeline;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 114
    return-void
.end method
