.class public Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFacebook;
.super Ljava/lang/Object;
.source "SnsFacebook.java"


# static fields
.field public static final ACCOUNT_TYPE:Ljava/lang/String; = "com.sec.android.app.sns3.facebook"

.field public static final BUNDLE_KEY_LIMIT:Ljava/lang/String; = "limit"

.field public static final BUNDLE_KEY_WHERE:Ljava/lang/String; = "where"

.field public static final CANCEL_URI:Ljava/lang/String; = "fbconnect://cancel"

.field public static final DIALOG_BASE_URL:Ljava/lang/String; = "https://m.facebook.com/dialog/"

.field public static final FACEBOOK_APP_TYPE:Ljava/lang/String; = "com.facebook.auth.login"

.field public static final GALLERY_AUTHORITY:Ljava/lang/String; = "com.sec.android.gallery3d.sns.contentprovider"

.field public static final GALLERY_SUPPORT_PATH:Ljava/lang/String; = "/support_sns"

.field public static final GRAPH_URL:Ljava/lang/String; = "https://graph.facebook.com/"

.field public static final MARKET_URI:Ljava/lang/String; = "market://details"

.field public static final REDIRECT_URI:Ljava/lang/String; = "fbconnect://success"

.field public static final REST_OLD_URL:Ljava/lang/String; = "https://api.facebook.com/restserver.php"

.field public static final REST_URL:Ljava/lang/String; = "https://api.facebook.com/"

.field public static final SP:Ljava/lang/String; = "facebook"

.field public static final USER_DENIED_URI:Ljava/lang/String; = "fbconnect://success?error_reason=user_denied"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
