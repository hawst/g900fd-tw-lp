.class Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterProfileFeedService$SyncAdapterImpl;
.super Landroid/content/AbstractThreadedSyncAdapter;
.source "SnsGpSyncAdapterProfileFeedService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterProfileFeedService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SyncAdapterImpl"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterProfileFeedService;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterProfileFeedService;Landroid/content/Context;)V
    .locals 1
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 72
    iput-object p1, p0, Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterProfileFeedService$SyncAdapterImpl;->this$0:Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterProfileFeedService;

    .line 73
    const/4 v0, 0x1

    invoke-direct {p0, p2, v0}, Landroid/content/AbstractThreadedSyncAdapter;-><init>(Landroid/content/Context;Z)V

    .line 74
    return-void
.end method


# virtual methods
.method public onPerformSync(Landroid/accounts/Account;Landroid/os/Bundle;Ljava/lang/String;Landroid/content/ContentProviderClient;Landroid/content/SyncResult;)V
    .locals 3
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "extras"    # Landroid/os/Bundle;
    .param p3, "authority"    # Ljava/lang/String;
    .param p4, "provider"    # Landroid/content/ContentProviderClient;
    .param p5, "syncResult"    # Landroid/content/SyncResult;

    .prologue
    .line 80
    const-string v1, "SnsGpSync"

    const-string v2, "***************** SnsGpSyncAdapterProfileFeedService : onPerformSync!!! *****************"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 83
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterProfileFeedService$SyncAdapterImpl;->this$0:Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterProfileFeedService;

    # setter for: Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterProfileFeedService;->mAccount:Landroid/accounts/Account;
    invoke-static {v1, p1}, Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterProfileFeedService;->access$002(Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterProfileFeedService;Landroid/accounts/Account;)Landroid/accounts/Account;

    .line 84
    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterProfileFeedService$SyncAdapterImpl;->this$0:Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterProfileFeedService;

    # setter for: Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterProfileFeedService;->mAuthority:Ljava/lang/String;
    invoke-static {v1, p3}, Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterProfileFeedService;->access$102(Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterProfileFeedService;Ljava/lang/String;)Ljava/lang/String;

    .line 85
    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterProfileFeedService$SyncAdapterImpl;->this$0:Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterProfileFeedService;

    # setter for: Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterProfileFeedService;->mSyncResult:Landroid/content/SyncResult;
    invoke-static {v1, p5}, Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterProfileFeedService;->access$202(Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterProfileFeedService;Landroid/content/SyncResult;)Landroid/content/SyncResult;

    .line 87
    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterProfileFeedService$SyncAdapterImpl;->this$0:Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterProfileFeedService;

    # invokes: Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterProfileFeedService;->performSync()V
    invoke-static {v1}, Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterProfileFeedService;->access$300(Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterProfileFeedService;)V
    :try_end_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 97
    :goto_0
    return-void

    .line 89
    :catch_0
    move-exception v0

    .line 90
    .local v0, "e":Landroid/accounts/OperationCanceledException;
    const-string v1, "SnsGpSync"

    const-string v2, "SnsGpSyncAdapterProfileFeedService : onPerformSync is CANCELED!!!"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 91
    invoke-virtual {v0}, Landroid/accounts/OperationCanceledException;->printStackTrace()V

    goto :goto_0

    .line 92
    .end local v0    # "e":Landroid/accounts/OperationCanceledException;
    :catch_1
    move-exception v0

    .line 93
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "SnsGpSync"

    const-string v2, "SnsGpSyncAdapterProfileFeedService : Abnormal Syncing!!!"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 94
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public onSyncCanceled()V
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterProfileFeedService$SyncAdapterImpl;->this$0:Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterProfileFeedService;

    # invokes: Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterProfileFeedService;->onSyncCanceled()V
    invoke-static {v0}, Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterProfileFeedService;->access$400(Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterProfileFeedService;)V

    .line 102
    return-void
.end method
