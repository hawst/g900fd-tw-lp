.class public Lcom/sec/android/app/sns3/agent/db/SnsSvcContentProvider;
.super Landroid/content/ContentProvider;
.source "SnsSvcContentProvider.java"


# static fields
.field private static final SSO_ACCOUNT:I = 0x64

.field private static final TAG:Ljava/lang/String; = "SnsSvcContentProvider"

.field private static final uriMatcher:Landroid/content/UriMatcher;


# instance fields
.field private mLocale:Ljava/lang/String;

.field private mOpenHelper:Lcom/sec/android/app/sns3/agent/db/SnsSvcDBHelper;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 259
    new-instance v0, Landroid/content/UriMatcher;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    sput-object v0, Lcom/sec/android/app/sns3/agent/db/SnsSvcContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    .line 260
    sget-object v0, Lcom/sec/android/app/sns3/agent/db/SnsSvcContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.sns3"

    const-string v2, "sso_account"

    const/16 v3, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 261
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 52
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    .line 56
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/sns3/agent/db/SnsSvcContentProvider;->mOpenHelper:Lcom/sec/android/app/sns3/agent/db/SnsSvcDBHelper;

    return-void
.end method

.method private deleteSSOAccount()I
    .locals 6

    .prologue
    .line 187
    const/4 v1, 0x0

    .line 188
    .local v1, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v0, -0x1

    .line 191
    .local v0, "count":I
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/sns3/agent/db/SnsSvcContentProvider;->mOpenHelper:Lcom/sec/android/app/sns3/agent/db/SnsSvcDBHelper;

    invoke-virtual {v3}, Lcom/sec/android/app/sns3/agent/db/SnsSvcDBHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 192
    const-string v3, "sso_account"

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-static {v1, v3, v4, v5}, Lcom/sec/android/app/sns3/agent/db/SnsDBWrapper;->onDelete(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 197
    :goto_0
    return v0

    .line 193
    :catch_0
    move-exception v2

    .line 194
    .local v2, "e":Landroid/database/sqlite/SQLiteException;
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    goto :goto_0
.end method

.method private declared-synchronized initSSOAccounts()V
    .locals 15

    .prologue
    .line 83
    monitor-enter p0

    :try_start_0
    new-instance v11, Landroid/content/ContentValues;

    invoke-direct {v11}, Landroid/content/ContentValues;-><init>()V

    .line 84
    .local v11, "values":Landroid/content/ContentValues;
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/db/SnsSvcContentProvider;->getContext()Landroid/content/Context;

    move-result-object v12

    const v13, 0x7f080045

    invoke-virtual {v12, v13}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 86
    .local v9, "serviceAppName":Ljava/lang/String;
    invoke-direct {p0}, Lcom/sec/android/app/sns3/agent/db/SnsSvcContentProvider;->deleteSSOAccount()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 89
    :try_start_1
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 97
    :goto_0
    const/4 v2, 0x0

    .line 98
    .local v2, "facebookEnabled":Z
    const/4 v4, 0x0

    .line 99
    .local v4, "foursquareEnabled":Z
    const/4 v5, 0x0

    .line 100
    .local v5, "googleplusEnabled":Z
    const/4 v7, 0x0

    .line 101
    .local v7, "instagramEnabled":Z
    const/4 v8, 0x0

    .line 102
    .local v8, "linkedinEnabled":Z
    const/4 v10, 0x0

    .line 104
    .local v10, "twitterEnabled":Z
    :try_start_2
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/db/SnsSvcContentProvider;->getContext()Landroid/content/Context;

    move-result-object v12

    invoke-static {v12}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v12

    invoke-virtual {v12}, Landroid/accounts/AccountManager;->getAuthenticatorTypes()[Landroid/accounts/AuthenticatorDescription;

    move-result-object v0

    .line 106
    .local v0, "authDescs":[Landroid/accounts/AuthenticatorDescription;
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_1
    array-length v12, v0

    if-ge v6, v12, :cond_6

    .line 107
    const-string v12, "com.sec.android.app.sns3.facebook"

    aget-object v13, v0, v6

    iget-object v13, v13, Landroid/accounts/AuthenticatorDescription;->type:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_1

    .line 108
    const/4 v2, 0x1

    .line 106
    :cond_0
    :goto_2
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 90
    .end local v0    # "authDescs":[Landroid/accounts/AuthenticatorDescription;
    .end local v2    # "facebookEnabled":Z
    .end local v4    # "foursquareEnabled":Z
    .end local v5    # "googleplusEnabled":Z
    .end local v6    # "i":I
    .end local v7    # "instagramEnabled":Z
    .end local v8    # "linkedinEnabled":Z
    .end local v10    # "twitterEnabled":Z
    :catch_0
    move-exception v1

    .line 91
    .local v1, "e":Ljava/lang/IllegalStateException;
    const-string v12, "SnsSvcContentProvider"

    const-string v13, "SnsApplication is NOT created yet. Accounts should be checked"

    invoke-static {v12, v13}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 93
    new-instance v3, Lcom/sec/android/app/sns3/SnsFeatureManager;

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/db/SnsSvcContentProvider;->getContext()Landroid/content/Context;

    move-result-object v12

    invoke-direct {v3, v12}, Lcom/sec/android/app/sns3/SnsFeatureManager;-><init>(Landroid/content/Context;)V

    .line 94
    .local v3, "featureMgr":Lcom/sec/android/app/sns3/SnsFeatureManager;
    invoke-virtual {v3}, Lcom/sec/android/app/sns3/SnsFeatureManager;->checkAccounts()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 83
    .end local v1    # "e":Ljava/lang/IllegalStateException;
    .end local v3    # "featureMgr":Lcom/sec/android/app/sns3/SnsFeatureManager;
    .end local v9    # "serviceAppName":Ljava/lang/String;
    .end local v11    # "values":Landroid/content/ContentValues;
    :catchall_0
    move-exception v12

    monitor-exit p0

    throw v12

    .line 109
    .restart local v0    # "authDescs":[Landroid/accounts/AuthenticatorDescription;
    .restart local v2    # "facebookEnabled":Z
    .restart local v4    # "foursquareEnabled":Z
    .restart local v5    # "googleplusEnabled":Z
    .restart local v6    # "i":I
    .restart local v7    # "instagramEnabled":Z
    .restart local v8    # "linkedinEnabled":Z
    .restart local v9    # "serviceAppName":Ljava/lang/String;
    .restart local v10    # "twitterEnabled":Z
    .restart local v11    # "values":Landroid/content/ContentValues;
    :cond_1
    :try_start_3
    const-string v12, "com.sec.android.app.sns3.foursquare"

    aget-object v13, v0, v6

    iget-object v13, v13, Landroid/accounts/AuthenticatorDescription;->type:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 110
    const/4 v4, 0x1

    goto :goto_2

    .line 111
    :cond_2
    const-string v12, "com.sec.android.app.sns3.googleplus"

    aget-object v13, v0, v6

    iget-object v13, v13, Landroid/accounts/AuthenticatorDescription;->type:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 112
    const/4 v5, 0x1

    goto :goto_2

    .line 113
    :cond_3
    const-string v12, "com.sec.android.app.sns3.instagram"

    aget-object v13, v0, v6

    iget-object v13, v13, Landroid/accounts/AuthenticatorDescription;->type:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_4

    .line 114
    const/4 v7, 0x1

    goto :goto_2

    .line 115
    :cond_4
    const-string v12, "com.sec.android.app.sns3.linkedin"

    aget-object v13, v0, v6

    iget-object v13, v13, Landroid/accounts/AuthenticatorDescription;->type:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_5

    .line 116
    const/4 v8, 0x1

    goto :goto_2

    .line 117
    :cond_5
    const-string v12, "com.sec.android.app.sns3.twitter"

    aget-object v13, v0, v6

    iget-object v13, v13, Landroid/accounts/AuthenticatorDescription;->type:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_0

    .line 118
    const/4 v10, 0x1

    goto :goto_2

    .line 123
    :cond_6
    if-eqz v2, :cond_7

    .line 124
    invoke-virtual {v11}, Landroid/content/ContentValues;->clear()V

    .line 125
    const-string v12, "account_type"

    const-string v13, "com.sec.android.app.sns3.facebook"

    invoke-virtual {v11, v12, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 126
    const-string v12, "sso_account_type"

    const-string v13, "com.facebook.auth.login"

    invoke-virtual {v11, v12, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    const-string v12, "sso_account_label"

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/db/SnsSvcContentProvider;->getContext()Landroid/content/Context;

    move-result-object v13

    const v14, 0x7f080001

    invoke-virtual {v13, v14}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v11, v12, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 130
    const-string v12, "service_app_name"

    invoke-virtual {v11, v12, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    invoke-direct {p0, v11}, Lcom/sec/android/app/sns3/agent/db/SnsSvcContentProvider;->insertSSOAccount(Landroid/content/ContentValues;)Z

    .line 135
    :cond_7
    if-eqz v4, :cond_8

    .line 140
    :cond_8
    if-eqz v5, :cond_9

    .line 145
    :cond_9
    if-eqz v7, :cond_a

    .line 150
    :cond_a
    if-eqz v8, :cond_b

    .line 151
    invoke-virtual {v11}, Landroid/content/ContentValues;->clear()V

    .line 152
    const-string v12, "account_type"

    const-string v13, "com.sec.android.app.sns3.linkedin"

    invoke-virtual {v11, v12, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 153
    const-string v12, "sso_account_type"

    const-string v13, "com.linkedin.android"

    invoke-virtual {v11, v12, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    const-string v12, "sso_account_label"

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/db/SnsSvcContentProvider;->getContext()Landroid/content/Context;

    move-result-object v13

    const v14, 0x7f08002e

    invoke-virtual {v13, v14}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v11, v12, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 157
    invoke-direct {p0, v11}, Lcom/sec/android/app/sns3/agent/db/SnsSvcContentProvider;->insertSSOAccount(Landroid/content/ContentValues;)Z

    .line 161
    :cond_b
    if-eqz v10, :cond_c

    .line 162
    invoke-virtual {v11}, Landroid/content/ContentValues;->clear()V

    .line 163
    const-string v12, "account_type"

    const-string v13, "com.sec.android.app.sns3.twitter"

    invoke-virtual {v11, v12, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 164
    const-string v12, "sso_account_type"

    const-string v13, "com.twitter.android.auth.login"

    invoke-virtual {v11, v12, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 165
    const-string v12, "sso_account_label"

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/db/SnsSvcContentProvider;->getContext()Landroid/content/Context;

    move-result-object v13

    const v14, 0x7f08006d

    invoke-virtual {v13, v14}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v11, v12, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 166
    const-string v12, "sso_retry_action"

    const-string v13, "com.sec.android.app.sns3.RETRY_SSO_TWITTER"

    invoke-virtual {v11, v12, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    invoke-direct {p0, v11}, Lcom/sec/android/app/sns3/agent/db/SnsSvcContentProvider;->insertSSOAccount(Landroid/content/ContentValues;)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 170
    :cond_c
    monitor-exit p0

    return-void
.end method

.method private insertSSOAccount(Landroid/content/ContentValues;)Z
    .locals 6
    .param p1, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 173
    const/4 v0, 0x0

    .line 174
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    const-wide/16 v2, -0x1

    .line 177
    .local v2, "rowId":J
    :try_start_0
    iget-object v4, p0, Lcom/sec/android/app/sns3/agent/db/SnsSvcContentProvider;->mOpenHelper:Lcom/sec/android/app/sns3/agent/db/SnsSvcDBHelper;

    invoke-virtual {v4}, Lcom/sec/android/app/sns3/agent/db/SnsSvcDBHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 178
    const-string v4, "sso_account"

    const/4 v5, 0x0

    invoke-virtual {v0, v4, v5, p1}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 183
    :goto_0
    const-wide/16 v4, -0x1

    cmp-long v4, v2, v4

    if-nez v4, :cond_0

    const/4 v4, 0x0

    :goto_1
    return v4

    .line 179
    :catch_0
    move-exception v1

    .line 180
    .local v1, "e":Landroid/database/sqlite/SQLiteException;
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    goto :goto_0

    .line 183
    .end local v1    # "e":Landroid/database/sqlite/SQLiteException;
    :cond_0
    const/4 v4, 0x1

    goto :goto_1
.end method


# virtual methods
.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "selection"    # Ljava/lang/String;
    .param p3, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 250
    const/4 v0, 0x0

    return v0
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 3
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 234
    sget-object v0, Lcom/sec/android/app/sns3/agent/db/SnsSvcContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 239
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown URI : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 236
    :pswitch_0
    const-string v0, "vnd.android.cursor.dir/vnd.com.sec.android.app.sns3.ssoaccount"

    return-object v0

    .line 234
    nop

    :pswitch_data_0
    .packed-switch 0x64
        :pswitch_0
    .end packed-switch
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 245
    const/4 v0, 0x0

    return-object v0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 74
    invoke-super {p0, p1}, Landroid/content/ContentProvider;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 76
    iget-object v0, p0, Lcom/sec/android/app/sns3/agent/db/SnsSvcContentProvider;->mLocale:Ljava/lang/String;

    iget-object v1, p1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 77
    iget-object v0, p1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/agent/db/SnsSvcContentProvider;->mLocale:Ljava/lang/String;

    .line 78
    invoke-direct {p0}, Lcom/sec/android/app/sns3/agent/db/SnsSvcContentProvider;->initSSOAccounts()V

    .line 80
    :cond_0
    return-void
.end method

.method public onCreate()Z
    .locals 2

    .prologue
    .line 66
    new-instance v0, Lcom/sec/android/app/sns3/agent/db/SnsSvcDBHelper;

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/db/SnsSvcContentProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/app/sns3/agent/db/SnsSvcDBHelper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/sns3/agent/db/SnsSvcContentProvider;->mOpenHelper:Lcom/sec/android/app/sns3/agent/db/SnsSvcDBHelper;

    .line 67
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/db/SnsSvcContentProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/agent/db/SnsSvcContentProvider;->mLocale:Ljava/lang/String;

    .line 68
    invoke-direct {p0}, Lcom/sec/android/app/sns3/agent/db/SnsSvcContentProvider;->initSSOAccounts()V

    .line 69
    const/4 v0, 0x0

    return v0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 11
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "sortOrder"    # Ljava/lang/String;

    .prologue
    .line 203
    const/4 v1, 0x0

    .line 204
    .local v1, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v8, 0x0

    .line 206
    .local v8, "c":Landroid/database/Cursor;
    sget-object v2, Lcom/sec/android/app/sns3/agent/db/SnsSvcContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v2, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v10

    .line 208
    .local v10, "match":I
    const/4 v5, 0x0

    .line 210
    .local v5, "groupby":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/app/sns3/agent/db/SnsSvcContentProvider;->mOpenHelper:Lcom/sec/android/app/sns3/agent/db/SnsSvcDBHelper;

    invoke-virtual {v2}, Lcom/sec/android/app/sns3/agent/db/SnsSvcDBHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 211
    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 213
    .local v0, "queryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    packed-switch v10, :pswitch_data_0

    .line 219
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unknown URI : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 215
    :pswitch_0
    const-string v2, "sso_account"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 223
    const/4 v6, 0x0

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object/from16 v7, p5

    :try_start_0
    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v8

    .line 229
    :goto_0
    return-object v8

    .line 225
    :catch_0
    move-exception v9

    .line 226
    .local v9, "e":Landroid/database/sqlite/SQLiteException;
    invoke-virtual {v9}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    goto :goto_0

    .line 213
    nop

    :pswitch_data_0
    .packed-switch 0x64
        :pswitch_0
    .end packed-switch
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 255
    const/4 v0, 0x0

    return v0
.end method
