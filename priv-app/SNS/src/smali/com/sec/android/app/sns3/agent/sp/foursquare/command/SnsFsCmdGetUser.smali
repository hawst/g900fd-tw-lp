.class public Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdGetUser;
.super Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;
.source "SnsFsCmdGetUser.java"


# instance fields
.field private mUserID:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Handler;Ljava/lang/String;)V
    .locals 3
    .param p1, "svcMgr"    # Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    .param p2, "cmdHandler"    # Landroid/os/Handler;
    .param p3, "userID"    # Ljava/lang/String;

    .prologue
    .line 44
    invoke-direct {p0, p2}, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;-><init>(Landroid/os/Handler;)V

    .line 46
    new-instance v1, Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;

    invoke-direct {v1}, Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;-><init>()V

    .line 48
    .local v1, "unit":Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;
    iput-object p3, p0, Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdGetUser;->mUserID:Ljava/lang/String;

    .line 50
    new-instance v0, Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdGetUser$1;

    iget-object v2, p0, Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdGetUser;->mUserID:Ljava/lang/String;

    invoke-direct {v0, p0, p1, v2}, Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdGetUser$1;-><init>(Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdGetUser;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;)V

    .line 95
    .local v0, "req":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    invoke-virtual {v1, v0}, Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;->addRequest(Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;)V

    .line 97
    invoke-virtual {p0, v1}, Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdGetUser;->addCommandUnit(Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;)V

    .line 98
    return-void
.end method


# virtual methods
.method protected respond()Z
    .locals 5

    .prologue
    .line 102
    const-string v0, "SnsAgent"

    const-string v1, "<SnsFsCmdGetUser> respond()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 104
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdGetUser;->getCommandCallback()Lcom/sec/android/app/sns3/agent/command/ISnsCommandCallback;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdGetUser;->getCommandID()I

    move-result v1

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdGetUser;->isSuccess()Z

    move-result v2

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdGetUser;->getUri()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdGetUser;->getResponseList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/sec/android/app/sns3/agent/command/ISnsCommandCallback;->onCmdRespond(IZLjava/lang/String;Ljava/util/List;)V

    .line 106
    const/4 v0, 0x1

    return v0
.end method
