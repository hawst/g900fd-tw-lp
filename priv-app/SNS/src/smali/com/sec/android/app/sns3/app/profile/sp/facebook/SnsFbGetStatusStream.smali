.class public Lcom/sec/android/app/sns3/app/profile/sp/facebook/SnsFbGetStatusStream;
.super Ljava/lang/Object;
.source "SnsFbGetStatusStream.java"


# static fields
.field protected static final TAG:Ljava/lang/String; = "SnsFbGetStatusStream"

.field private static mCmdHandler:Landroid/os/Handler;

.field private static mServiceMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static invokeBroadcast(Landroid/content/Context;ILjava/lang/String;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "resultCode"    # I
    .param p2, "id"    # Ljava/lang/String;

    .prologue
    .line 89
    const-string v1, "SnsFbGetStatusStream"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "invokeBroadcast() : resultCode = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 90
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.app.sns.profile.ACTION_FACEBOOK_UPDATED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 91
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 92
    const-string v1, "result"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 93
    const-string v1, "com.sec.android.app.sns3.permission.RECEIVE_SNS_BROADCAST"

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    .line 94
    return-void
.end method

.method public static updateStatus(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "userId"    # Ljava/lang/String;

    .prologue
    .line 49
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/SnsApplication;->getSvcMgr()Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/sns3/app/profile/sp/facebook/SnsFbGetStatusStream;->mServiceMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    .line 50
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/SnsApplication;->getAgentMgr()Lcom/sec/android/app/sns3/agent/SnsAgentMgr;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/agent/SnsAgentMgr;->getCommandMgr()Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;->getCommandMgrHandle()Landroid/os/Handler;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/sns3/app/profile/sp/facebook/SnsFbGetStatusStream;->mCmdHandler:Landroid/os/Handler;

    .line 52
    sget-object v0, Lcom/sec/android/app/sns3/app/profile/sp/facebook/SnsFbGetStatusStream;->mServiceMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    if-nez v0, :cond_0

    .line 53
    const-string v0, "SnsFbGetStatusStream"

    const-string v1, "[SNS] mSvcMgr is null !!"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 86
    :goto_0
    return-void

    .line 56
    :cond_0
    sget-object v0, Lcom/sec/android/app/sns3/app/profile/sp/facebook/SnsFbGetStatusStream;->mCmdHandler:Landroid/os/Handler;

    if-nez v0, :cond_1

    .line 57
    const-string v0, "SnsFbGetStatusStream"

    const-string v1, "[SNS] mCmdHandler is null !!"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 85
    :cond_1
    const/4 v0, -0x1

    invoke-static {p0, v0, p1}, Lcom/sec/android/app/sns3/app/profile/sp/facebook/SnsFbGetStatusStream;->invokeBroadcast(Landroid/content/Context;ILjava/lang/String;)V

    goto :goto_0
.end method
