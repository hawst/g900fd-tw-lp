.class public Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserLatestPhoto;
.super Ljava/lang/Object;
.source "SnsFbParserLatestPhoto.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserLatestPhoto$FacebookLatestPhotoPost;
    }
.end annotation


# instance fields
.field mPicture:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePicture;

.field mStream:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhotoStream;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserLatestPhoto;->mStream:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhotoStream;

    .line 32
    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserLatestPhoto;->mPicture:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePicture;

    .line 34
    return-void
.end method

.method private parsePhoto(Ljava/lang/String;)V
    .locals 8
    .param p1, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 148
    const/4 v3, 0x0

    .line 150
    .local v3, "isMatch":Z
    :try_start_0
    new-instance v4, Lorg/json/JSONArray;

    invoke-direct {v4, p1}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 152
    .local v4, "ja":Lorg/json/JSONArray;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v6

    if-ge v2, v6, :cond_1

    .line 154
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserLatestPhoto;->mStream:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhotoStream;

    .line 156
    .local v0, "curPhotoStream":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhotoStream;
    invoke-virtual {v4, v2}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v5

    .line 158
    .local v5, "jsonObj":Lorg/json/JSONObject;
    :goto_1
    if-eqz v0, :cond_0

    .line 159
    const-string v6, "pid"

    invoke-virtual {v5, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iget-object v7, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhotoStream;->mPID:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 161
    iget-object v6, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserLatestPhoto;->mPicture:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePicture;

    const-string v7, "src_big"

    invoke-virtual {v5, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePicture;->mURL:Ljava/lang/String;

    .line 163
    iget-object v6, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserLatestPhoto;->mPicture:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePicture;

    iget-object v6, v6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePicture;->mURL:Ljava/lang/String;

    if-eqz v6, :cond_2

    iget-object v6, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserLatestPhoto;->mPicture:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePicture;

    iget-object v6, v6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePicture;->mURL:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    if-lez v6, :cond_2

    .line 164
    const/4 v3, 0x1

    .line 172
    :cond_0
    if-eqz v3, :cond_3

    .line 179
    .end local v0    # "curPhotoStream":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhotoStream;
    .end local v2    # "i":I
    .end local v4    # "ja":Lorg/json/JSONArray;
    .end local v5    # "jsonObj":Lorg/json/JSONObject;
    :cond_1
    :goto_2
    return-void

    .line 169
    .restart local v0    # "curPhotoStream":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhotoStream;
    .restart local v2    # "i":I
    .restart local v4    # "ja":Lorg/json/JSONArray;
    .restart local v5    # "jsonObj":Lorg/json/JSONObject;
    :cond_2
    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhotoStream;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhotoStream;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 152
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 175
    .end local v0    # "curPhotoStream":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhotoStream;
    .end local v2    # "i":I
    .end local v4    # "ja":Lorg/json/JSONArray;
    .end local v5    # "jsonObj":Lorg/json/JSONObject;
    :catch_0
    move-exception v1

    .line 177
    .local v1, "e":Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_2
.end method

.method private parseStream(Ljava/lang/String;)V
    .locals 8
    .param p1, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 104
    const/4 v0, 0x0

    .line 106
    .local v0, "curPhotoStream":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhotoStream;
    const/4 v4, 0x0

    .line 109
    .local v4, "jsonObject":Lorg/json/JSONObject;
    :try_start_0
    new-instance v3, Lorg/json/JSONArray;

    invoke-direct {v3, p1}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 111
    .local v3, "ja":Lorg/json/JSONArray;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v6

    if-ge v2, v6, :cond_2

    .line 112
    new-instance v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhotoStream;

    invoke-direct {v5}, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhotoStream;-><init>()V

    .line 114
    .local v5, "newPhotoStream":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhotoStream;
    invoke-virtual {v3, v2}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    .line 116
    const-string v6, "post_id"

    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhotoStream;->mPostID:Ljava/lang/String;

    .line 117
    const-string v6, "attachment"

    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v6

    const-string v7, "media"

    invoke-virtual {v6, v7}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v6

    const-string v7, "photo"

    invoke-virtual {v6, v7}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v6

    const-string v7, "pid"

    invoke-virtual {v6, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhotoStream;->mPID:Ljava/lang/String;

    .line 122
    const-string v6, "message"

    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhotoStream;->mMessage:Ljava/lang/String;

    .line 123
    const-string v6, "updated_time"

    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhotoStream;->mUpdatedTime:Ljava/lang/String;

    .line 125
    const-string v6, "created_time"

    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhotoStream;->mCreatedTime:Ljava/lang/String;

    .line 128
    if-nez v0, :cond_1

    .line 129
    move-object v0, v5

    .line 135
    :goto_1
    iget-object v6, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserLatestPhoto;->mStream:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhotoStream;

    if-nez v6, :cond_0

    .line 136
    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserLatestPhoto;->mStream:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhotoStream;

    .line 111
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 131
    :cond_1
    iput-object v5, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhotoStream;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhotoStream;

    .line 132
    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhotoStream;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhotoStream;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 139
    .end local v2    # "i":I
    .end local v3    # "ja":Lorg/json/JSONArray;
    .end local v5    # "newPhotoStream":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhotoStream;
    :catch_0
    move-exception v1

    .line 141
    .local v1, "e":Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    .line 143
    .end local v1    # "e":Lorg/json/JSONException;
    :cond_2
    return-void
.end method


# virtual methods
.method public parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePicture;
    .locals 7
    .param p1, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 76
    new-instance v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePicture;

    invoke-direct {v5}, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePicture;-><init>()V

    iput-object v5, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserLatestPhoto;->mPicture:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePicture;

    .line 79
    :try_start_0
    new-instance v2, Lorg/json/JSONArray;

    invoke-direct {v2, p1}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 81
    .local v2, "ja":Lorg/json/JSONArray;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v5

    if-ge v1, v5, :cond_2

    .line 83
    invoke-virtual {v2, v1}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v5

    const-string v6, "name"

    invoke-virtual {v5, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 84
    .local v3, "queryName":Ljava/lang/String;
    invoke-virtual {v2, v1}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v5

    const-string v6, "fql_result_set"

    invoke-virtual {v5, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 86
    .local v4, "queryObject":Ljava/lang/String;
    const-string v5, "stream_query"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 87
    invoke-direct {p0, v4}, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserLatestPhoto;->parseStream(Ljava/lang/String;)V

    .line 81
    :cond_0
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 88
    :cond_1
    const-string v5, "photo_query"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 89
    invoke-direct {p0, v4}, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserLatestPhoto;->parsePhoto(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 94
    .end local v1    # "i":I
    .end local v2    # "ja":Lorg/json/JSONArray;
    .end local v3    # "queryName":Ljava/lang/String;
    .end local v4    # "queryObject":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 96
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    .line 99
    .end local v0    # "e":Lorg/json/JSONException;
    :cond_2
    iget-object v5, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserLatestPhoto;->mPicture:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePicture;

    return-object v5
.end method
