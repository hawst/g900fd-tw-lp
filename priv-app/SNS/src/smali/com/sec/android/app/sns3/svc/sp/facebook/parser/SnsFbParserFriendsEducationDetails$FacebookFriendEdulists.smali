.class public interface abstract Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserFriendsEducationDetails$FacebookFriendEdulists;
.super Ljava/lang/Object;
.source "SnsFbParserFriendsEducationDetails.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserFriendsEducationDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "FacebookFriendEdulists"
.end annotation


# static fields
.field public static final DEGREE:Ljava/lang/String; = "degree"

.field public static final EDUCATION:Ljava/lang/String; = "education"

.field public static final ID:Ljava/lang/String; = "id"

.field public static final NAME:Ljava/lang/String; = "name"

.field public static final SCHOOL:Ljava/lang/String; = "school"

.field public static final TYPE:Ljava/lang/String; = "type"

.field public static final UID:Ljava/lang/String; = "uid"

.field public static final YEAR:Ljava/lang/String; = "year"
