.class public Lcom/sec/android/app/sns3/svc/sp/linkedin/parser/SnsLiParserPeopleLookUp;
.super Ljava/lang/Object;
.source "SnsLiParserPeopleLookUp.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/linkedin/parser/SnsLiParserPeopleLookUp$LinkedInPeople;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    return-void
.end method

.method public static parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp;
    .locals 9
    .param p0, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 69
    const/4 v5, 0x0

    .line 70
    .local v5, "peopleObj":Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp;
    const/4 v0, 0x0

    .line 73
    .local v0, "curPeopleObj":Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp;
    :try_start_0
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 75
    .local v3, "jsonObject":Lorg/json/JSONObject;
    const-string v8, "_total"

    invoke-virtual {v3, v8}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v8

    if-lez v8, :cond_2

    .line 76
    const-string v8, "values"

    invoke-virtual {v3, v8}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v7

    .line 77
    .local v7, "values":Lorg/json/JSONArray;
    const/4 v4, 0x0

    .line 78
    .local v4, "people1":Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp;
    if-eqz v7, :cond_1

    .line 79
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {v7}, Lorg/json/JSONArray;->length()I

    move-result v8

    if-ge v2, v8, :cond_2

    .line 80
    invoke-virtual {v7, v2}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v6

    .line 81
    .local v6, "peopleObject":Lorg/json/JSONObject;
    invoke-static {v6}, Lcom/sec/android/app/sns3/svc/sp/linkedin/parser/SnsLiParserPeopleLookUp;->parserPeopleLookup(Lorg/json/JSONObject;)Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp;

    move-result-object v4

    .line 82
    if-nez v5, :cond_0

    .line 83
    move-object v5, v4

    .line 84
    move-object v0, v5

    .line 79
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 86
    :cond_0
    iput-object v4, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp;->mNext:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp;

    .line 87
    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp;->mNext:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp;

    goto :goto_1

    .line 91
    .end local v2    # "i":I
    .end local v6    # "peopleObject":Lorg/json/JSONObject;
    :cond_1
    invoke-static {v3}, Lcom/sec/android/app/sns3/svc/sp/linkedin/parser/SnsLiParserPeopleLookUp;->parserPeopleLookup(Lorg/json/JSONObject;)Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v5

    .line 97
    .end local v3    # "jsonObject":Lorg/json/JSONObject;
    .end local v4    # "people1":Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp;
    .end local v7    # "values":Lorg/json/JSONArray;
    :cond_2
    :goto_2
    return-object v5

    .line 94
    :catch_0
    move-exception v1

    .line 95
    .local v1, "e":Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_2
.end method

.method private static parserPeopleLookup(Lorg/json/JSONObject;)Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp;
    .locals 15
    .param p0, "peopleObject"    # Lorg/json/JSONObject;

    .prologue
    .line 101
    new-instance v7, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp;

    invoke-direct {v7}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp;-><init>()V

    .line 103
    .local v7, "people":Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp;
    :try_start_0
    const-string v13, "id"

    invoke-virtual {p0, v13}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v7, v13}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp;->setMemberId(Ljava/lang/String;)V

    .line 104
    const-string v13, "firstName"

    invoke-virtual {p0, v13}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v7, v13}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp;->setFirstName(Ljava/lang/String;)V

    .line 105
    const-string v13, "lastName"

    invoke-virtual {p0, v13}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v7, v13}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp;->setLastName(Ljava/lang/String;)V

    .line 106
    const-string v13, "formattedName"

    invoke-virtual {p0, v13}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v7, v13}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp;->setFormattedName(Ljava/lang/String;)V

    .line 107
    const-string v13, "_key"

    invoke-virtual {p0, v13}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    const-string v14, "email"

    invoke-virtual {v13, v14}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v13

    if-eqz v13, :cond_0

    .line 108
    const-string v13, "_key"

    invoke-virtual {p0, v13}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    const-string v14, "="

    invoke-virtual {v13, v14}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 109
    .local v2, "email":[Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 110
    const/4 v13, 0x1

    aget-object v13, v2, v13

    invoke-virtual {v13}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v7, v13}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp;->setEmail(Ljava/lang/String;)V

    .line 115
    .end local v2    # "email":[Ljava/lang/String;
    :cond_0
    const-string v13, "headline"

    invoke-virtual {p0, v13}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v7, v13}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp;->setHeadline(Ljava/lang/String;)V

    .line 117
    const-string v13, "publicProfileUrl"

    invoke-virtual {p0, v13}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_1

    .line 118
    const-string v13, "publicProfileUrl"

    invoke-virtual {p0, v13}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v7, v13}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp;->setProfileUrl(Ljava/lang/String;)V

    .line 121
    :cond_1
    const-string v13, "positions"

    invoke-virtual {p0, v13}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v10

    .line 122
    .local v10, "position":Lorg/json/JSONObject;
    if-eqz v10, :cond_3

    const-string v13, "_total"

    invoke-virtual {v10, v13}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v13

    if-lez v13, :cond_3

    .line 123
    const-string v13, "values"

    invoke-virtual {v10, v13}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v11

    .line 124
    .local v11, "positionValues":Lorg/json/JSONArray;
    const/4 v4, 0x0

    .local v4, "j":I
    :goto_0
    invoke-virtual {v11}, Lorg/json/JSONArray;->length()I

    move-result v13

    if-ge v4, v13, :cond_3

    .line 125
    invoke-virtual {v11, v4}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v6

    .line 127
    .local v6, "object":Lorg/json/JSONObject;
    const-string v13, "isCurrent"

    invoke-virtual {v6, v13}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    .line 128
    .local v3, "isCurrent":Z
    const-string v13, "company"

    invoke-virtual {v6, v13}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 129
    .local v0, "company":Lorg/json/JSONObject;
    if-eqz v3, :cond_2

    if-eqz v0, :cond_2

    .line 130
    const-string v13, "name"

    invoke-virtual {v0, v13}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v7, v13}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp;->setCompanyName(Ljava/lang/String;)V

    .line 131
    const-string v13, "title"

    invoke-virtual {v6, v13}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v7, v13}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp;->setPositionTitle(Ljava/lang/String;)V

    .line 124
    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 136
    .end local v0    # "company":Lorg/json/JSONObject;
    .end local v3    # "isCurrent":Z
    .end local v4    # "j":I
    .end local v6    # "object":Lorg/json/JSONObject;
    .end local v11    # "positionValues":Lorg/json/JSONArray;
    :cond_3
    const-string v13, "pictureUrls"

    invoke-virtual {p0, v13}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v8

    .line 137
    .local v8, "pictureUrls":Lorg/json/JSONObject;
    if-eqz v8, :cond_6

    const-string v13, "_total"

    invoke-virtual {v8, v13}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v13

    if-lez v13, :cond_6

    .line 138
    const-string v13, "values"

    invoke-virtual {v8, v13}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v9

    .line 139
    .local v9, "pictureValues":Lorg/json/JSONArray;
    const/4 v4, 0x0

    .restart local v4    # "j":I
    :goto_1
    invoke-virtual {v9}, Lorg/json/JSONArray;->length()I

    move-result v13

    if-ge v4, v13, :cond_6

    .line 140
    invoke-virtual {v9, v4}, Lorg/json/JSONArray;->optString(I)Ljava/lang/String;

    move-result-object v5

    .line 141
    .local v5, "key":Ljava/lang/String;
    invoke-virtual {v9, v4}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/String;

    .line 142
    .local v12, "url":Ljava/lang/String;
    if-eqz v12, :cond_4

    .line 143
    const-string v13, "100x100"

    invoke-virtual {v13, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_5

    .line 144
    invoke-virtual {v7, v12}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp;->setPictureUrl(Ljava/lang/String;)V

    .line 139
    :cond_4
    :goto_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 145
    :cond_5
    const-string v13, "original"

    invoke-virtual {v13, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_7

    .line 146
    invoke-virtual {v7, v12}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp;->setLargePictureUrl(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 158
    .end local v4    # "j":I
    .end local v5    # "key":Ljava/lang/String;
    .end local v8    # "pictureUrls":Lorg/json/JSONObject;
    .end local v9    # "pictureValues":Lorg/json/JSONArray;
    .end local v10    # "position":Lorg/json/JSONObject;
    .end local v12    # "url":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 159
    .local v1, "e":Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    .line 161
    .end local v1    # "e":Lorg/json/JSONException;
    :cond_6
    return-object v7

    .line 149
    .restart local v4    # "j":I
    .restart local v5    # "key":Ljava/lang/String;
    .restart local v8    # "pictureUrls":Lorg/json/JSONObject;
    .restart local v9    # "pictureValues":Lorg/json/JSONArray;
    .restart local v10    # "position":Lorg/json/JSONObject;
    .restart local v12    # "url":Ljava/lang/String;
    :cond_7
    if-nez v4, :cond_8

    .line 150
    :try_start_1
    invoke-virtual {v7, v12}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp;->setPictureUrl(Ljava/lang/String;)V

    goto :goto_2

    .line 152
    :cond_8
    invoke-virtual {v7, v12}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp;->setLargePictureUrl(Ljava/lang/String;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2
.end method
