.class public interface abstract Lcom/sec/android/app/sns3/agent/sp/googleplus/db/SnsGooglePlusDB$FriendsProfileInfoColumns;
.super Ljava/lang/Object;
.source "SnsGooglePlusDB.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/agent/sp/googleplus/db/SnsGooglePlusDB;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "FriendsProfileInfoColumns"
.end annotation


# static fields
.field public static final FRIEND_OBJECT_TYPE:Ljava/lang/String; = "object_type"

.field public static final FRIEND_PROFILE_ID:Ljava/lang/String; = "profile_id"

.field public static final FRIEND_PROFILE_IMAGE_URL:Ljava/lang/String; = "profile_image"

.field public static final FRIEND_PROFILE_NAME:Ljava/lang/String; = "profile_name"

.field public static final FRIEND_PROFILE_URL:Ljava/lang/String; = "profile_url"
