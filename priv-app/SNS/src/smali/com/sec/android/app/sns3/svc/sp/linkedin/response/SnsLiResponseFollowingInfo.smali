.class public final Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseFollowingInfo;
.super Ljava/lang/Object;
.source "SnsLiResponseFollowingInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseFollowingInfo$LinkedInFollowing;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseFollowingInfo;",
            ">;"
        }
    .end annotation
.end field

.field public static final INFO_TYPE_COMPANIES:Ljava/lang/String; = "COMPANIES"

.field public static final INFO_TYPE_INDUSTRIES:Ljava/lang/String; = "INDUSTRIES"

.field public static final INFO_TYPE_PEOPLE:Ljava/lang/String; = "PEOPLE"

.field public static final INFO_TYPE_SPECIALEDITIONS:Ljava/lang/String; = "SPECIALEDITIONS"


# instance fields
.field private mFollowingType:Ljava/lang/String;

.field private mId:Ljava/lang/String;

.field private mName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 93
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseFollowingInfo$1;

    invoke-direct {v0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseFollowingInfo$1;-><init>()V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseFollowingInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 104
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 105
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 107
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 108
    invoke-virtual {p0, p1}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseFollowingInfo;->readFromParcel(Landroid/os/Parcel;)V

    .line 109
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseFollowingInfo$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseFollowingInfo$1;

    .prologue
    .line 31
    invoke-direct {p0, p1}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseFollowingInfo;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "mInfoType"    # Ljava/lang/String;
    .param p2, "id"    # Ljava/lang/String;

    .prologue
    .line 131
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseFollowingInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "mInfoType"    # Ljava/lang/String;
    .param p2, "id"    # Ljava/lang/String;
    .param p3, "mValue"    # Ljava/lang/String;

    .prologue
    .line 135
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 136
    iput-object p1, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseFollowingInfo;->mFollowingType:Ljava/lang/String;

    .line 137
    iput-object p2, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseFollowingInfo;->mId:Ljava/lang/String;

    .line 138
    iput-object p3, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseFollowingInfo;->mName:Ljava/lang/String;

    .line 139
    return-void
.end method

.method public static parse(Lorg/json/JSONObject;)Ljava/util/List;
    .locals 8
    .param p0, "data"    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/json/JSONObject;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseFollowingInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 142
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 143
    .local v2, "followingInfoList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseFollowingInfo;>;"
    if-nez p0, :cond_1

    .line 180
    :cond_0
    :goto_0
    return-object v2

    .line 146
    :cond_1
    :try_start_0
    const-string v7, "companies"

    invoke-virtual {p0, v7}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 147
    const-string v7, "companies"

    invoke-virtual {p0, v7}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 148
    .local v0, "company":Lorg/json/JSONObject;
    const-string v7, "_total"

    invoke-virtual {v0, v7}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v6

    .line 149
    .local v6, "total":I
    if-lez v6, :cond_2

    .line 150
    invoke-static {v0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseFollowingInfo;->parseCompanies(Lorg/json/JSONObject;)Ljava/util/List;

    move-result-object v7

    invoke-interface {v2, v7}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 153
    .end local v0    # "company":Lorg/json/JSONObject;
    .end local v6    # "total":I
    :cond_2
    const-string v7, "industries"

    invoke-virtual {p0, v7}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 154
    const-string v7, "industries"

    invoke-virtual {p0, v7}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v3

    .line 155
    .local v3, "industry":Lorg/json/JSONObject;
    const-string v7, "_total"

    invoke-virtual {v3, v7}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v6

    .line 156
    .restart local v6    # "total":I
    if-lez v6, :cond_3

    .line 157
    invoke-static {v3}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseFollowingInfo;->parseIndustries(Lorg/json/JSONObject;)Ljava/util/List;

    move-result-object v7

    invoke-interface {v2, v7}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 161
    .end local v3    # "industry":Lorg/json/JSONObject;
    .end local v6    # "total":I
    :cond_3
    const-string v7, "people"

    invoke-virtual {p0, v7}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 162
    const-string v7, "people"

    invoke-virtual {p0, v7}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    .line 163
    .local v4, "people":Lorg/json/JSONObject;
    const-string v7, "_total"

    invoke-virtual {v4, v7}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v6

    .line 164
    .restart local v6    # "total":I
    if-lez v6, :cond_4

    .line 165
    invoke-static {v4}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseFollowingInfo;->parsePeople(Lorg/json/JSONObject;)Ljava/util/List;

    move-result-object v7

    invoke-interface {v2, v7}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 169
    .end local v4    # "people":Lorg/json/JSONObject;
    .end local v6    # "total":I
    :cond_4
    const-string v7, "specialEditions"

    invoke-virtual {p0, v7}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 170
    const-string v7, "specialEditions"

    invoke-virtual {p0, v7}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v5

    .line 171
    .local v5, "specialEdition":Lorg/json/JSONObject;
    const-string v7, "_total"

    invoke-virtual {v5, v7}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v6

    .line 172
    .restart local v6    # "total":I
    if-lez v6, :cond_0

    .line 173
    invoke-static {v5}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseFollowingInfo;->parseSpecialEditions(Lorg/json/JSONObject;)Ljava/util/List;

    move-result-object v7

    invoke-interface {v2, v7}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 177
    .end local v5    # "specialEdition":Lorg/json/JSONObject;
    .end local v6    # "total":I
    :catch_0
    move-exception v1

    .line 178
    .local v1, "e":Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public static parseCompanies(Lorg/json/JSONObject;)Ljava/util/List;
    .locals 9
    .param p0, "data"    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/json/JSONObject;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseFollowingInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 184
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 187
    .local v2, "followingInfoList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseFollowingInfo;>;"
    :try_start_0
    const-string v6, "values"

    invoke-virtual {p0, v6}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v5

    .line 189
    .local v5, "infoList":Lorg/json/JSONArray;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-virtual {v5}, Lorg/json/JSONArray;->length()I

    move-result v6

    if-ge v3, v6, :cond_0

    .line 190
    invoke-virtual {v5, v3}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    .line 192
    .local v4, "info":Lorg/json/JSONObject;
    new-instance v1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseFollowingInfo;

    const-string v6, "COMPANIES"

    const-string v7, "id"

    invoke-virtual {v4, v7}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "name"

    invoke-virtual {v4, v8}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v1, v6, v7, v8}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseFollowingInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 195
    .local v1, "followingInfo":Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseFollowingInfo;
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 189
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 197
    .end local v1    # "followingInfo":Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseFollowingInfo;
    .end local v3    # "i":I
    .end local v4    # "info":Lorg/json/JSONObject;
    .end local v5    # "infoList":Lorg/json/JSONArray;
    :catch_0
    move-exception v0

    .line 198
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    .line 201
    .end local v0    # "e":Lorg/json/JSONException;
    :cond_0
    return-object v2
.end method

.method public static parseIndustries(Lorg/json/JSONObject;)Ljava/util/List;
    .locals 8
    .param p0, "data"    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/json/JSONObject;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseFollowingInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 206
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 209
    .local v2, "followingInfoList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseFollowingInfo;>;"
    :try_start_0
    const-string v6, "values"

    invoke-virtual {p0, v6}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v5

    .line 211
    .local v5, "infoList":Lorg/json/JSONArray;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-virtual {v5}, Lorg/json/JSONArray;->length()I

    move-result v6

    if-ge v3, v6, :cond_0

    .line 212
    invoke-virtual {v5, v3}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    .line 214
    .local v4, "info":Lorg/json/JSONObject;
    new-instance v1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseFollowingInfo;

    const-string v6, "INDUSTRIES"

    const-string v7, "id"

    invoke-virtual {v4, v7}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v1, v6, v7}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseFollowingInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 216
    .local v1, "followingInfo":Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseFollowingInfo;
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 211
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 218
    .end local v1    # "followingInfo":Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseFollowingInfo;
    .end local v3    # "i":I
    .end local v4    # "info":Lorg/json/JSONObject;
    .end local v5    # "infoList":Lorg/json/JSONArray;
    :catch_0
    move-exception v0

    .line 219
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    .line 222
    .end local v0    # "e":Lorg/json/JSONException;
    :cond_0
    return-object v2
.end method

.method public static parsePeople(Lorg/json/JSONObject;)Ljava/util/List;
    .locals 9
    .param p0, "data"    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/json/JSONObject;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseFollowingInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 227
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 230
    .local v2, "followingInfoList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseFollowingInfo;>;"
    :try_start_0
    const-string v6, "values"

    invoke-virtual {p0, v6}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v5

    .line 232
    .local v5, "infoList":Lorg/json/JSONArray;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-virtual {v5}, Lorg/json/JSONArray;->length()I

    move-result v6

    if-ge v3, v6, :cond_0

    .line 233
    invoke-virtual {v5, v3}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    .line 235
    .local v4, "info":Lorg/json/JSONObject;
    new-instance v1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseFollowingInfo;

    const-string v6, "PEOPLE"

    const-string v7, "id"

    invoke-virtual {v4, v7}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "name"

    invoke-virtual {v4, v8}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v1, v6, v7, v8}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseFollowingInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 238
    .local v1, "followingInfo":Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseFollowingInfo;
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 232
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 240
    .end local v1    # "followingInfo":Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseFollowingInfo;
    .end local v3    # "i":I
    .end local v4    # "info":Lorg/json/JSONObject;
    .end local v5    # "infoList":Lorg/json/JSONArray;
    :catch_0
    move-exception v0

    .line 241
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    .line 244
    .end local v0    # "e":Lorg/json/JSONException;
    :cond_0
    return-object v2
.end method

.method public static parseSpecialEditions(Lorg/json/JSONObject;)Ljava/util/List;
    .locals 9
    .param p0, "data"    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/json/JSONObject;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseFollowingInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 249
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 252
    .local v2, "followingInfoList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseFollowingInfo;>;"
    :try_start_0
    const-string v6, "values"

    invoke-virtual {p0, v6}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v5

    .line 254
    .local v5, "infoList":Lorg/json/JSONArray;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-virtual {v5}, Lorg/json/JSONArray;->length()I

    move-result v6

    if-ge v3, v6, :cond_0

    .line 255
    invoke-virtual {v5, v3}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    .line 257
    .local v4, "info":Lorg/json/JSONObject;
    new-instance v1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseFollowingInfo;

    const-string v6, "SPECIALEDITIONS"

    const-string v7, "id"

    invoke-virtual {v4, v7}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "name"

    invoke-virtual {v4, v8}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v1, v6, v7, v8}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseFollowingInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 260
    .local v1, "followingInfo":Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseFollowingInfo;
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 254
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 262
    .end local v1    # "followingInfo":Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseFollowingInfo;
    .end local v3    # "i":I
    .end local v4    # "info":Lorg/json/JSONObject;
    .end local v5    # "infoList":Lorg/json/JSONArray;
    :catch_0
    move-exception v0

    .line 263
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    .line 266
    .end local v0    # "e":Lorg/json/JSONException;
    :cond_0
    return-object v2
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 127
    const/4 v0, 0x0

    return v0
.end method

.method public getFollowingType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseFollowingInfo;->mFollowingType:Ljava/lang/String;

    return-object v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseFollowingInfo;->mId:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseFollowingInfo;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "parcel"    # Landroid/os/Parcel;

    .prologue
    .line 119
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseFollowingInfo;->mFollowingType:Ljava/lang/String;

    .line 120
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseFollowingInfo;->mName:Ljava/lang/String;

    .line 121
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseFollowingInfo;->mId:Ljava/lang/String;

    .line 123
    return-void
.end method

.method public setFollowingType(Ljava/lang/String;)V
    .locals 0
    .param p1, "mFollowingType"    # Ljava/lang/String;

    .prologue
    .line 74
    iput-object p1, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseFollowingInfo;->mFollowingType:Ljava/lang/String;

    .line 75
    return-void
.end method

.method public setId(Ljava/lang/String;)V
    .locals 0
    .param p1, "mId"    # Ljava/lang/String;

    .prologue
    .line 82
    iput-object p1, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseFollowingInfo;->mId:Ljava/lang/String;

    .line 83
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .param p1, "mName"    # Ljava/lang/String;

    .prologue
    .line 90
    iput-object p1, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseFollowingInfo;->mName:Ljava/lang/String;

    .line 91
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 272
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SnsLiResponseFollowingInfo [mFollowingType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseFollowingInfo;->mFollowingType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseFollowingInfo;->mId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseFollowingInfo;->mName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "parcel"    # Landroid/os/Parcel;
    .param p2, "flag"    # I

    .prologue
    .line 113
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseFollowingInfo;->mFollowingType:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 114
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseFollowingInfo;->mName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 115
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseFollowingInfo;->mId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 116
    return-void
.end method
