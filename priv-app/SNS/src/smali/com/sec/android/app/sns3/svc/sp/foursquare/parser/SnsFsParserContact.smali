.class public Lcom/sec/android/app/sns3/svc/sp/foursquare/parser/SnsFsParserContact;
.super Ljava/lang/Object;
.source "SnsFsParserContact.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/foursquare/parser/SnsFsParserContact$FourSquareContact;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    return-void
.end method

.method public static parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseContact;
    .locals 4
    .param p0, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 22
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseContact;

    invoke-direct {v0}, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseContact;-><init>()V

    .line 25
    .local v0, "contact":Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseContact;
    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 27
    .local v2, "jsonObject":Lorg/json/JSONObject;
    const-string v3, "phone"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseContact;->mPhone:Ljava/lang/String;

    .line 28
    const-string v3, "email"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseContact;->mEmail:Ljava/lang/String;

    .line 29
    const-string v3, "twitter"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseContact;->mTwitter:Ljava/lang/String;

    .line 30
    const-string v3, "facebook"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseContact;->mFacebook:Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 36
    .end local v2    # "jsonObject":Lorg/json/JSONObject;
    :goto_0
    return-object v0

    .line 32
    :catch_0
    move-exception v1

    .line 33
    .local v1, "e":Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method
