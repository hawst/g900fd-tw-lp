.class public Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthenticatorService$LinkedInAccountAuthenticator;
.super Landroid/accounts/AbstractAccountAuthenticator;
.source "SnsAccountLiAuthenticatorService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthenticatorService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "LinkedInAccountAuthenticator"
.end annotation


# instance fields
.field private mAppContext:Landroid/content/Context;

.field final synthetic this$0:Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthenticatorService;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthenticatorService;Landroid/content/Context;)V
    .locals 0
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 78
    iput-object p1, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthenticatorService$LinkedInAccountAuthenticator;->this$0:Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthenticatorService;

    .line 79
    invoke-direct {p0, p2}, Landroid/accounts/AbstractAccountAuthenticator;-><init>(Landroid/content/Context;)V

    .line 80
    iput-object p2, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthenticatorService$LinkedInAccountAuthenticator;->mAppContext:Landroid/content/Context;

    .line 81
    return-void
.end method


# virtual methods
.method public addAccount(Landroid/accounts/AccountAuthenticatorResponse;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 7
    .param p1, "response"    # Landroid/accounts/AccountAuthenticatorResponse;
    .param p2, "accountType"    # Ljava/lang/String;
    .param p3, "authTokenType"    # Ljava/lang/String;
    .param p4, "requiredFeatures"    # [Ljava/lang/String;
    .param p5, "options"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/accounts/NetworkErrorException;
        }
    .end annotation

    .prologue
    .line 87
    invoke-static {}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->isLoggable()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 88
    # getter for: Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthenticatorService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthenticatorService;->access$000()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "LinkedInAuthenticatorService : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " - "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 91
    :cond_0
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 93
    .local v3, "result":Landroid/os/Bundle;
    if-eqz p5, :cond_2

    # getter for: Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthenticatorService;->OPTIONS_USERNAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthenticatorService;->access$100()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p5, v4}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 94
    new-instance v0, Landroid/accounts/Account;

    # getter for: Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthenticatorService;->OPTIONS_USERNAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthenticatorService;->access$100()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p5, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "com.sec.android.app.sns3.linkedin"

    invoke-direct {v0, v4, v5}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    .local v0, "account":Landroid/accounts/Account;
    iget-object v4, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthenticatorService$LinkedInAccountAuthenticator;->mAppContext:Landroid/content/Context;

    invoke-static {v4}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    .line 99
    .local v1, "am":Landroid/accounts/AccountManager;
    # getter for: Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthenticatorService;->OPTIONS_PASSWORD:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthenticatorService;->access$200()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p5, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v1, v0, v4, v5}, Landroid/accounts/AccountManager;->addAccountExplicitly(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 101
    const-string v4, "authAccount"

    # getter for: Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthenticatorService;->OPTIONS_USERNAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthenticatorService;->access$100()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p5, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    const-string v4, "accountType"

    const-string v5, "com.sec.android.app.sns3.linkedin"

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 107
    new-instance v2, Landroid/content/Intent;

    const-string v4, "com.sec.android.app.sns.ACTION_SNS_LINKEDIN_LOGGED_IN"

    invoke-direct {v2, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 108
    .local v2, "intent":Landroid/content/Intent;
    iget-object v4, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthenticatorService$LinkedInAccountAuthenticator;->this$0:Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthenticatorService;

    const-string v5, "com.sec.android.app.sns3.permission.RECEIVE_SNS_BROADCAST"

    invoke-virtual {v4, v2, v5}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthenticatorService;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    .line 122
    .end local v0    # "account":Landroid/accounts/Account;
    .end local v1    # "am":Landroid/accounts/AccountManager;
    .end local v2    # "intent":Landroid/content/Intent;
    :cond_1
    :goto_0
    return-object v3

    .line 111
    :cond_2
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 113
    .restart local v2    # "intent":Landroid/content/Intent;
    iget-object v4, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthenticatorService$LinkedInAccountAuthenticator;->mAppContext:Landroid/content/Context;

    const-class v5, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 117
    const-string v4, "accountAuthenticatorResponse"

    invoke-virtual {v2, v4, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 118
    const-string v4, "manageAccount"

    invoke-virtual {v2, v4, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 119
    const-string v4, "intent"

    invoke-virtual {v3, v4, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    goto :goto_0
.end method

.method public confirmCredentials(Landroid/accounts/AccountAuthenticatorResponse;Landroid/accounts/Account;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 2
    .param p1, "response"    # Landroid/accounts/AccountAuthenticatorResponse;
    .param p2, "account"    # Landroid/accounts/Account;
    .param p3, "options"    # Landroid/os/Bundle;

    .prologue
    .line 128
    # getter for: Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthenticatorService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthenticatorService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "LinkedInAuthenticatorService : confirmCredentials"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 129
    const/4 v0, 0x0

    return-object v0
.end method

.method public editProperties(Landroid/accounts/AccountAuthenticatorResponse;Ljava/lang/String;)Landroid/os/Bundle;
    .locals 2
    .param p1, "response"    # Landroid/accounts/AccountAuthenticatorResponse;
    .param p2, "accountType"    # Ljava/lang/String;

    .prologue
    .line 134
    # getter for: Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthenticatorService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthenticatorService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "LinkedInAuthenticatorService : editProperties"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 135
    const/4 v0, 0x0

    return-object v0
.end method

.method public getAccountRemovalAllowed(Landroid/accounts/AccountAuthenticatorResponse;Landroid/accounts/Account;)Landroid/os/Bundle;
    .locals 12
    .param p1, "response"    # Landroid/accounts/AccountAuthenticatorResponse;
    .param p2, "account"    # Landroid/accounts/Account;

    .prologue
    .line 140
    # getter for: Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthenticatorService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthenticatorService;->access$000()Ljava/lang/String;

    move-result-object v9

    const-string v10, "LinkedInAuthenticatorService : getAccountRemovalAllowed"

    invoke-static {v9, v10}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 142
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v0

    .line 143
    .local v0, "app":Lcom/sec/android/app/sns3/SnsApplication;
    new-instance v1, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdAuthLogout;

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/SnsApplication;->getSvcMgr()Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v9

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/SnsApplication;->getAgentMgr()Lcom/sec/android/app/sns3/agent/SnsAgentMgr;

    move-result-object v10

    invoke-virtual {v10}, Lcom/sec/android/app/sns3/agent/SnsAgentMgr;->getCommandMgr()Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;

    move-result-object v10

    invoke-virtual {v10}, Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;->getCommandMgrHandle()Landroid/os/Handler;

    move-result-object v10

    invoke-direct {v1, v9, v10}, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdAuthLogout;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Handler;)V

    .line 145
    .local v1, "cmd":Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;
    new-instance v9, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthenticatorService$LinkedInAccountAuthenticator$1;

    invoke-direct {v9, p0}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthenticatorService$LinkedInAccountAuthenticator$1;-><init>(Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthenticatorService$LinkedInAccountAuthenticator;)V

    invoke-virtual {v1, v9}, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->setCommandCallback(Lcom/sec/android/app/sns3/agent/command/ISnsCommandCallback;)Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;

    .line 154
    invoke-virtual {v1}, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->send()I

    .line 158
    iget-object v9, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthenticatorService$LinkedInAccountAuthenticator;->this$0:Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthenticatorService;

    const-string v10, "LI_contact_sync_interval"

    const/4 v11, 0x0

    invoke-virtual {v9, v10, v11}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthenticatorService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v9

    invoke-interface {v9}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 160
    .local v2, "contactEditor":Landroid/content/SharedPreferences$Editor;
    const-string v9, "LI_contact_sync_interval"

    const-wide/16 v10, 0x0

    invoke-interface {v2, v9, v10, v11}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 161
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 163
    iget-object v9, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthenticatorService$LinkedInAccountAuthenticator;->this$0:Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthenticatorService;

    const-string v10, "LI_linkedin_app_sync_interval"

    const/4 v11, 0x0

    invoke-virtual {v9, v10, v11}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthenticatorService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v9

    invoke-interface {v9}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    .line 165
    .local v4, "linkedinEditor":Landroid/content/SharedPreferences$Editor;
    const-string v9, "LI_linkedin_app_sync_interval"

    const-wide/16 v10, 0x0

    invoke-interface {v4, v9, v10, v11}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 166
    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 169
    iget-object v9, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthenticatorService$LinkedInAccountAuthenticator;->this$0:Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthenticatorService;

    const-string v10, "LI_profile_feed_sync_interval"

    const/4 v11, 0x0

    invoke-virtual {v9, v10, v11}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthenticatorService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v9

    invoke-interface {v9}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    .line 171
    .local v6, "profileFeedEditor":Landroid/content/SharedPreferences$Editor;
    const-string v9, "LI_profile_feed_sync_interval"

    const-wide/16 v10, 0x0

    invoke-interface {v6, v9, v10, v11}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 172
    invoke-interface {v6}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 174
    iget-object v9, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthenticatorService$LinkedInAccountAuthenticator;->this$0:Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthenticatorService;

    const-string v10, "LI_profiles_sync_interval"

    const/4 v11, 0x0

    invoke-virtual {v9, v10, v11}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthenticatorService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v9

    invoke-interface {v9}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v7

    .line 176
    .local v7, "profilesEditor":Landroid/content/SharedPreferences$Editor;
    const-string v9, "LI_profiles_sync_interval"

    const-wide/16 v10, 0x0

    invoke-interface {v7, v9, v10, v11}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 177
    invoke-interface {v7}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 180
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v9

    const-string v10, "notification"

    invoke-virtual {v9, v10}, Lcom/sec/android/app/sns3/SnsApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/app/NotificationManager;

    move-object v5, v9

    check-cast v5, Landroid/app/NotificationManager;

    .line 182
    .local v5, "notiMgr":Landroid/app/NotificationManager;
    const/16 v9, 0xce4

    invoke-virtual {v5, v9}, Landroid/app/NotificationManager;->cancel(I)V

    .line 185
    new-instance v3, Landroid/content/Intent;

    const-string v9, "com.sec.android.app.sns.ACTION_SNS_LINKEDIN_LOGGED_OUT"

    invoke-direct {v3, v9}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 186
    .local v3, "intent":Landroid/content/Intent;
    iget-object v9, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthenticatorService$LinkedInAccountAuthenticator;->this$0:Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthenticatorService;

    const-string v10, "com.sec.android.app.sns3.permission.RECEIVE_SNS_BROADCAST"

    invoke-virtual {v9, v3, v10}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthenticatorService;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    .line 188
    new-instance v8, Landroid/os/Bundle;

    invoke-direct {v8}, Landroid/os/Bundle;-><init>()V

    .line 189
    .local v8, "result":Landroid/os/Bundle;
    const-string v9, "booleanResult"

    const/4 v10, 0x1

    invoke-virtual {v8, v9, v10}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 190
    return-object v8
.end method

.method public getAuthToken(Landroid/accounts/AccountAuthenticatorResponse;Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 2
    .param p1, "response"    # Landroid/accounts/AccountAuthenticatorResponse;
    .param p2, "account"    # Landroid/accounts/Account;
    .param p3, "authTokenType"    # Ljava/lang/String;
    .param p4, "loginOptions"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/accounts/NetworkErrorException;
        }
    .end annotation

    .prologue
    .line 196
    # getter for: Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthenticatorService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthenticatorService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "LinkedInAuthenticatorService : getAuthToken"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 197
    const/4 v0, 0x0

    return-object v0
.end method

.method public getAuthTokenLabel(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "authTokenType"    # Ljava/lang/String;

    .prologue
    .line 202
    # getter for: Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthenticatorService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthenticatorService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "LinkedInAuthenticatorService : getAuthTokenLabel"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 203
    const/4 v0, 0x0

    return-object v0
.end method

.method public hasFeatures(Landroid/accounts/AccountAuthenticatorResponse;Landroid/accounts/Account;[Ljava/lang/String;)Landroid/os/Bundle;
    .locals 2
    .param p1, "response"    # Landroid/accounts/AccountAuthenticatorResponse;
    .param p2, "account"    # Landroid/accounts/Account;
    .param p3, "features"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/accounts/NetworkErrorException;
        }
    .end annotation

    .prologue
    .line 209
    # getter for: Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthenticatorService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthenticatorService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "LinkedInAuthenticatorService : hasFeatures"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 210
    const/4 v0, 0x0

    return-object v0
.end method

.method public updateCredentials(Landroid/accounts/AccountAuthenticatorResponse;Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 2
    .param p1, "response"    # Landroid/accounts/AccountAuthenticatorResponse;
    .param p2, "account"    # Landroid/accounts/Account;
    .param p3, "authTokenType"    # Ljava/lang/String;
    .param p4, "loginOptions"    # Landroid/os/Bundle;

    .prologue
    .line 216
    # getter for: Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthenticatorService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthenticatorService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "LinkedInAuthenticatorService : updateCredentials"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 217
    const/4 v0, 0x0

    return-object v0
.end method
