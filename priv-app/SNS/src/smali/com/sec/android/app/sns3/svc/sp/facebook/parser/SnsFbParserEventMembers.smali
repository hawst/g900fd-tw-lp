.class public Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserEventMembers;
.super Ljava/lang/Object;
.source "SnsFbParserEventMembers.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserEventMembers$FacebookUsers;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    return-void
.end method

.method public static parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEventMembers;
    .locals 12
    .param p0, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 44
    const/4 v8, 0x0

    .line 45
    .local v8, "users":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEventMembers;
    const/4 v0, 0x0

    .line 48
    .local v0, "curUsers":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEventMembers;
    :try_start_0
    new-instance v6, Lorg/json/JSONObject;

    invoke-direct {v6, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 50
    .local v6, "jsonObject":Lorg/json/JSONObject;
    new-instance v3, Lorg/json/JSONArray;

    const-string v9, "data"

    invoke-virtual {v6, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v3, v9}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 52
    .local v3, "jArrUsers":Lorg/json/JSONArray;
    if-eqz v3, :cond_1

    .line 53
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v9

    if-ge v2, v9, :cond_1

    .line 54
    invoke-virtual {v3, v2}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v5

    .line 56
    .local v5, "jsonObj":Lorg/json/JSONObject;
    new-instance v7, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEventMembers;

    invoke-direct {v7}, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEventMembers;-><init>()V

    .line 58
    .local v7, "newUsers":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEventMembers;
    const-string v9, "id"

    invoke-virtual {v5, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v7, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEventMembers;->mUserID:Ljava/lang/String;

    .line 59
    const-string v9, "name"

    invoke-virtual {v5, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v7, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEventMembers;->mName:Ljava/lang/String;

    .line 60
    const-string v9, "rsvp_status"

    invoke-virtual {v5, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v7, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEventMembers;->mRsvp_status:Ljava/lang/String;

    .line 62
    if-nez v8, :cond_0

    .line 63
    move-object v8, v7

    .line 64
    move-object v0, v8

    .line 53
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 66
    :cond_0
    iput-object v7, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEventMembers;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEventMembers;

    .line 67
    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEventMembers;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEventMembers;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_1

    .line 72
    .end local v2    # "i":I
    .end local v3    # "jArrUsers":Lorg/json/JSONArray;
    .end local v5    # "jsonObj":Lorg/json/JSONObject;
    .end local v6    # "jsonObject":Lorg/json/JSONObject;
    .end local v7    # "newUsers":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEventMembers;
    :catch_0
    move-exception v4

    .line 73
    .local v4, "je":Lorg/json/JSONException;
    const-string v9, "SNS"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "parser : JSONException error :"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v4}, Lorg/json/JSONException;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 78
    .end local v4    # "je":Lorg/json/JSONException;
    :cond_1
    :goto_2
    return-object v8

    .line 74
    :catch_1
    move-exception v1

    .line 75
    .local v1, "e":Ljava/lang/Exception;
    const-string v9, "SNS"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "parser : Exception error : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method
