.class public Lcom/sec/android/app/sns3/agent/sp/instagram/command/SnsInCmdGetFeed;
.super Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;
.source "SnsInCmdGetFeed.java"


# instance fields
.field private mUserID:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Handler;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 3
    .param p1, "svcMgr"    # Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    .param p2, "cmdHandler"    # Landroid/os/Handler;
    .param p3, "userID"    # Ljava/lang/String;
    .param p4, "params"    # Landroid/os/Bundle;

    .prologue
    .line 45
    invoke-direct {p0, p2}, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;-><init>(Landroid/os/Handler;)V

    .line 47
    new-instance v1, Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;

    invoke-direct {v1}, Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;-><init>()V

    .line 49
    .local v1, "unit":Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;
    iput-object p3, p0, Lcom/sec/android/app/sns3/agent/sp/instagram/command/SnsInCmdGetFeed;->mUserID:Ljava/lang/String;

    .line 51
    new-instance v0, Lcom/sec/android/app/sns3/agent/sp/instagram/command/SnsInCmdGetFeed$1;

    iget-object v2, p0, Lcom/sec/android/app/sns3/agent/sp/instagram/command/SnsInCmdGetFeed;->mUserID:Ljava/lang/String;

    invoke-direct {v0, p0, p1, v2, p4}, Lcom/sec/android/app/sns3/agent/sp/instagram/command/SnsInCmdGetFeed$1;-><init>(Lcom/sec/android/app/sns3/agent/sp/instagram/command/SnsInCmdGetFeed;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 100
    .local v0, "req":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    invoke-virtual {v1, v0}, Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;->addRequest(Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;)V

    .line 102
    invoke-virtual {p0, v1}, Lcom/sec/android/app/sns3/agent/sp/instagram/command/SnsInCmdGetFeed;->addCommandUnit(Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;)V

    .line 103
    return-void
.end method


# virtual methods
.method public getUserID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/sec/android/app/sns3/agent/sp/instagram/command/SnsInCmdGetFeed;->mUserID:Ljava/lang/String;

    return-object v0
.end method

.method protected respond()Z
    .locals 5

    .prologue
    .line 111
    const-string v0, "SnsAgent"

    const-string v1, "<SnsInCmdGetUser> respond()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 113
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/instagram/command/SnsInCmdGetFeed;->getCommandCallback()Lcom/sec/android/app/sns3/agent/command/ISnsCommandCallback;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/instagram/command/SnsInCmdGetFeed;->getCommandID()I

    move-result v1

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/instagram/command/SnsInCmdGetFeed;->isSuccess()Z

    move-result v2

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/instagram/command/SnsInCmdGetFeed;->getUri()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/instagram/command/SnsInCmdGetFeed;->getResponseList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/sec/android/app/sns3/agent/command/ISnsCommandCallback;->onCmdRespond(IZLjava/lang/String;Ljava/util/List;)V

    .line 115
    const/4 v0, 0x1

    return v0
.end method
