.class public Lcom/sec/android/app/sns3/agent/sp/instagram/command/SnsInCmdGetHome;
.super Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;
.source "SnsInCmdGetHome.java"


# direct methods
.method public constructor <init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Handler;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 2
    .param p1, "svcMgr"    # Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    .param p2, "cmdHandler"    # Landroid/os/Handler;
    .param p3, "userID"    # Ljava/lang/String;
    .param p4, "params"    # Landroid/os/Bundle;

    .prologue
    .line 43
    invoke-direct {p0, p2}, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;-><init>(Landroid/os/Handler;)V

    .line 45
    new-instance v1, Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;

    invoke-direct {v1}, Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;-><init>()V

    .line 47
    .local v1, "unit":Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;
    new-instance v0, Lcom/sec/android/app/sns3/agent/sp/instagram/command/SnsInCmdGetHome$1;

    invoke-direct {v0, p0, p1, p4}, Lcom/sec/android/app/sns3/agent/sp/instagram/command/SnsInCmdGetHome$1;-><init>(Lcom/sec/android/app/sns3/agent/sp/instagram/command/SnsInCmdGetHome;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Bundle;)V

    .line 91
    .local v0, "req":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    invoke-virtual {v1, v0}, Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;->addRequest(Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;)V

    .line 93
    invoke-virtual {p0, v1}, Lcom/sec/android/app/sns3/agent/sp/instagram/command/SnsInCmdGetHome;->addCommandUnit(Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;)V

    .line 94
    return-void
.end method


# virtual methods
.method protected respond()Z
    .locals 5

    .prologue
    .line 98
    const-string v0, "SnsAgent"

    const-string v1, "<SnsInCmdGetHome> respond()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 100
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/instagram/command/SnsInCmdGetHome;->getCommandCallback()Lcom/sec/android/app/sns3/agent/command/ISnsCommandCallback;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/instagram/command/SnsInCmdGetHome;->getCommandID()I

    move-result v1

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/instagram/command/SnsInCmdGetHome;->isSuccess()Z

    move-result v2

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/instagram/command/SnsInCmdGetHome;->getUri()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/instagram/command/SnsInCmdGetHome;->getResponseList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/sec/android/app/sns3/agent/command/ISnsCommandCallback;->onCmdRespond(IZLjava/lang/String;Ljava/util/List;)V

    .line 102
    const/4 v0, 0x1

    return v0
.end method
