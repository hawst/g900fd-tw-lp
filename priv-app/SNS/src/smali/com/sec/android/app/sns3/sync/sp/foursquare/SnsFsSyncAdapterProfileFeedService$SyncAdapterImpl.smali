.class Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileFeedService$SyncAdapterImpl;
.super Landroid/content/AbstractThreadedSyncAdapter;
.source "SnsFsSyncAdapterProfileFeedService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileFeedService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SyncAdapterImpl"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileFeedService;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileFeedService;Landroid/content/Context;)V
    .locals 1
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 80
    iput-object p1, p0, Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileFeedService$SyncAdapterImpl;->this$0:Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileFeedService;

    .line 81
    const/4 v0, 0x1

    invoke-direct {p0, p2, v0}, Landroid/content/AbstractThreadedSyncAdapter;-><init>(Landroid/content/Context;Z)V

    .line 82
    # setter for: Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileFeedService;->mContext:Landroid/content/Context;
    invoke-static {p1, p2}, Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileFeedService;->access$002(Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileFeedService;Landroid/content/Context;)Landroid/content/Context;

    .line 83
    return-void
.end method


# virtual methods
.method public onPerformSync(Landroid/accounts/Account;Landroid/os/Bundle;Ljava/lang/String;Landroid/content/ContentProviderClient;Landroid/content/SyncResult;)V
    .locals 3
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "extras"    # Landroid/os/Bundle;
    .param p3, "authority"    # Ljava/lang/String;
    .param p4, "provider"    # Landroid/content/ContentProviderClient;
    .param p5, "syncResult"    # Landroid/content/SyncResult;

    .prologue
    .line 89
    const-string v1, "SnsFsSync"

    const-string v2, "***************** SnsFsSyncAdapterProfileFeedService : onPerformSync!!! *****************"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 93
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileFeedService$SyncAdapterImpl;->this$0:Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileFeedService;

    # setter for: Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileFeedService;->mAccount:Landroid/accounts/Account;
    invoke-static {v1, p1}, Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileFeedService;->access$102(Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileFeedService;Landroid/accounts/Account;)Landroid/accounts/Account;

    .line 94
    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileFeedService$SyncAdapterImpl;->this$0:Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileFeedService;

    # setter for: Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileFeedService;->mAuthority:Ljava/lang/String;
    invoke-static {v1, p3}, Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileFeedService;->access$202(Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileFeedService;Ljava/lang/String;)Ljava/lang/String;

    .line 95
    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileFeedService$SyncAdapterImpl;->this$0:Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileFeedService;

    # setter for: Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileFeedService;->mSyncResult:Landroid/content/SyncResult;
    invoke-static {v1, p5}, Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileFeedService;->access$302(Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileFeedService;Landroid/content/SyncResult;)Landroid/content/SyncResult;

    .line 97
    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileFeedService$SyncAdapterImpl;->this$0:Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileFeedService;

    # invokes: Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileFeedService;->performSync()V
    invoke-static {v1}, Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileFeedService;->access$400(Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileFeedService;)V
    :try_end_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 107
    :goto_0
    return-void

    .line 99
    :catch_0
    move-exception v0

    .line 100
    .local v0, "e":Landroid/accounts/OperationCanceledException;
    const-string v1, "SnsFsSync"

    const-string v2, "SnsFsSyncAdapterProfileFeedService : onPerformSync is CANCELED!!!"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 101
    invoke-virtual {v0}, Landroid/accounts/OperationCanceledException;->printStackTrace()V

    goto :goto_0

    .line 102
    .end local v0    # "e":Landroid/accounts/OperationCanceledException;
    :catch_1
    move-exception v0

    .line 103
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "SnsFsSync"

    const-string v2, "SnsFsSyncAdapterProfileFeedService : Abnormal Syncing!!!"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 104
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public onSyncCanceled()V
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileFeedService$SyncAdapterImpl;->this$0:Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileFeedService;

    # invokes: Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileFeedService;->onSyncCanceled()V
    invoke-static {v0}, Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileFeedService;->access$500(Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileFeedService;)V

    .line 112
    return-void
.end method
