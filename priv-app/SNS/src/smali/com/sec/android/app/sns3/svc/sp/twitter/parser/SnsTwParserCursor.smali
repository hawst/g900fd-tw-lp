.class public Lcom/sec/android/app/sns3/svc/sp/twitter/parser/SnsTwParserCursor;
.super Ljava/lang/Object;
.source "SnsTwParserCursor.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseCursor;
    .locals 5
    .param p0, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 31
    const/4 v0, 0x0

    .line 33
    .local v0, "cursor":Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseCursor;
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    const/4 v4, 0x2

    if-gt v3, v4, :cond_0

    .line 34
    const/4 v3, 0x0

    .line 51
    :goto_0
    return-object v3

    .line 37
    :cond_0
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseCursor;

    .end local v0    # "cursor":Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseCursor;
    invoke-direct {v0}, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseCursor;-><init>()V

    .line 40
    .restart local v0    # "cursor":Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseCursor;
    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 41
    .local v2, "jsonObj":Lorg/json/JSONObject;
    const-string v3, "next_cursor"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseCursor;->mNextCursor:Ljava/lang/String;

    .line 42
    const-string v3, "previous_cursor"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseCursor;->mPreviousCursor:Ljava/lang/String;

    .line 44
    const-string v3, "ids"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/parser/SnsTwParserStringArray;->parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStringArray;

    move-result-object v3

    iput-object v3, v0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseCursor;->mIds:Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStringArray;

    .line 45
    const-string v3, "users"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/parser/SnsTwParserUser;->parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseUser;

    move-result-object v3

    iput-object v3, v0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseCursor;->mUser:Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseUser;

    .line 46
    const-string v3, "lists"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/parser/SnsTwParserList;->parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseList;

    move-result-object v3

    iput-object v3, v0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseCursor;->mList:Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseList;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .end local v2    # "jsonObj":Lorg/json/JSONObject;
    :goto_1
    move-object v3, v0

    .line 51
    goto :goto_0

    .line 47
    :catch_0
    move-exception v1

    .line 49
    .local v1, "e":Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_1
.end method
