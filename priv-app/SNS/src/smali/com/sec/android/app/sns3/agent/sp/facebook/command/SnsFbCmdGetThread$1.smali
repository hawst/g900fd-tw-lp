.class Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetThread$1;
.super Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqGetThread;
.source "SnsFbCmdGetThread.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetThread;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Handler;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetThread;


# direct methods
.method constructor <init>(Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetThread;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;)V
    .locals 0
    .param p2, "x0"    # Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    .param p3, "x1"    # Ljava/lang/String;

    .prologue
    .line 52
    iput-object p1, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetThread$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetThread;

    invoke-direct {p0, p2, p3}, Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqGetThread;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public onReqRespond(IZIILandroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseThread;)Z
    .locals 12
    .param p1, "reqID"    # I
    .param p2, "bSuccess"    # Z
    .param p3, "httpStatus"    # I
    .param p4, "errorCode"    # I
    .param p5, "reason"    # Landroid/os/Bundle;
    .param p6, "thread"    # Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseThread;

    .prologue
    .line 57
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    .line 58
    .local v5, "values":Landroid/content/ContentValues;
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/sns3/SnsApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 60
    .local v2, "cr":Landroid/content/ContentResolver;
    sget-object v6, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$Thread;->CONTENT_URI:Landroid/net/Uri;

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual {v2, v6, v7, v8}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 62
    if-eqz p2, :cond_3

    .line 63
    if-eqz p6, :cond_2

    .line 65
    move-object/from16 v0, p6

    iget-object v3, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseThread;->mMsg:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMsgQueryResult;

    .line 66
    .local v3, "curMsg":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMsgQueryResult;
    const/4 v4, 0x0

    .line 68
    .local v4, "curUser":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseUserQueryResult;
    :goto_0
    if-eqz v3, :cond_2

    .line 70
    invoke-virtual {v5}, Landroid/content/ContentValues;->clear()V

    .line 71
    move-object/from16 v0, p6

    iget-object v4, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseThread;->mUser:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseUserQueryResult;

    .line 73
    const-string v6, "message_id"

    iget-object v7, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMsgQueryResult;->mMessageId:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    const-string v6, "thread_id"

    iget-object v7, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMsgQueryResult;->mThreadId:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    const-string v6, "author_id"

    iget-object v7, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMsgQueryResult;->mAuthorId:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    const-string v6, "body"

    iget-object v7, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMsgQueryResult;->mBody:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    const-string v6, "created_time"

    iget-object v7, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMsgQueryResult;->mCreatedTime:Ljava/lang/Long;

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    const-wide/16 v10, 0x3e8

    mul-long/2addr v8, v10

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 78
    const-string v6, "viewer_id"

    iget-object v7, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMsgQueryResult;->mViwerId:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    :goto_1
    if-eqz v4, :cond_0

    .line 81
    iget-object v6, v4, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseUserQueryResult;->mUserId:Ljava/lang/String;

    iget-object v7, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMsgQueryResult;->mAuthorId:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 83
    const-string v6, "author_id"

    iget-object v7, v4, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseUserQueryResult;->mUserId:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    const-string v6, "author_name"

    iget-object v7, v4, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseUserQueryResult;->mName:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    const-string v6, "author_pic"

    iget-object v7, v4, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseUserQueryResult;->mPicSquare:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    :cond_0
    sget-object v6, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$Thread;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2, v6, v5}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 94
    iget-object v3, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMsgQueryResult;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMsgQueryResult;

    goto :goto_0

    .line 89
    :cond_1
    iget-object v4, v4, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseUserQueryResult;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseUserQueryResult;

    goto :goto_1

    .line 97
    .end local v3    # "curMsg":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMsgQueryResult;
    .end local v4    # "curUser":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseUserQueryResult;
    :cond_2
    iget-object v6, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetThread$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetThread;

    sget-object v7, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$Thread;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v7}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetThread;->setUri(Ljava/lang/String;)V

    .line 104
    :goto_2
    iget-object v6, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetThread$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetThread;

    invoke-virtual {v6, p2}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetThread;->setSuccess(Z)V

    .line 105
    iget-object v6, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetThread$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetThread;

    invoke-virtual {v6, p0}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetThread;->receive(Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;)V

    .line 107
    const/4 v6, 0x1

    return v6

    .line 99
    :cond_3
    iget-object v6, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetThread$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetThread;

    new-instance v7, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;

    const-string v8, "facebook"

    move/from16 v0, p4

    move-object/from16 v1, p5

    invoke-direct {v7, v8, p3, v0, v1}, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;-><init>(Ljava/lang/String;IILandroid/os/Bundle;)V

    invoke-virtual {v6, v7}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetThread;->setResponseList(Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;)Z

    .line 101
    iget-object v6, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetThread$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetThread;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetThread;->setUri(Ljava/lang/String;)V

    goto :goto_2
.end method
