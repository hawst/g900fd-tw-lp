.class public Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncSetupActivity;
.super Landroid/preference/PreferenceActivity;
.source "SnsAccountInSyncSetupActivity.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "SnsAccountInAuth"


# instance fields
.field private mAppContext:Landroid/content/Context;

.field private mFeedsCheckBox:Landroid/preference/CheckBoxPreference;

.field private mGalleryCheckBox:Landroid/preference/CheckBoxPreference;

.field private mIsGallerySync:Ljava/lang/String;

.field private mSyncIntervalSetting:Landroid/preference/PreferenceScreen;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    .line 49
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncSetupActivity;->mAppContext:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method isTable()Z
    .locals 2

    .prologue
    .line 216
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncSetupActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit8 v0, v0, 0x3

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncSetupActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    .line 219
    :cond_0
    const/4 v0, 0x1

    .line 221
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onClickDone()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 168
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncSetupActivity;->mAppContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    const-string v2, "com.sec.android.app.sns3.instagram"

    invoke-virtual {v1, v2}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 171
    .local v0, "account":[Landroid/accounts/Account;
    array-length v1, v0

    if-lez v1, :cond_1

    .line 172
    const-string v1, "true"

    iget-object v2, p0, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncSetupActivity;->mIsGallerySync:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 173
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncSetupActivity;->mGalleryCheckBox:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 174
    aget-object v1, v0, v3

    const-string v2, "com.sec.android.gallery3d.instagram.contentprovider"

    invoke-static {v1, v2, v4}, Landroid/content/ContentResolver;->setIsSyncable(Landroid/accounts/Account;Ljava/lang/String;I)V

    .line 175
    aget-object v1, v0, v3

    const-string v2, "com.sec.android.gallery3d.instagram.contentprovider"

    invoke-static {v1, v2, v4}, Landroid/content/ContentResolver;->setSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;Z)V

    .line 179
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncSetupActivity;->mFeedsCheckBox:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 180
    aget-object v1, v0, v3

    const-string v2, "com.sec.android.app.sns3.life"

    invoke-static {v1, v2, v4}, Landroid/content/ContentResolver;->setIsSyncable(Landroid/accounts/Account;Ljava/lang/String;I)V

    .line 181
    aget-object v1, v0, v3

    const-string v2, "com.sec.android.app.sns3.life"

    invoke-static {v1, v2, v4}, Landroid/content/ContentResolver;->setSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;Z)V

    .line 185
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncSetupActivity;->finish()V

    .line 186
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 12
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const v11, 0x7f080058

    const v8, 0x7f080025

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 63
    iput-object p0, p0, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncSetupActivity;->mAppContext:Landroid/content/Context;

    .line 65
    iget-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncSetupActivity;->mAppContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->isLightTheme(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_9

    const v5, 0x103012b

    :goto_0
    invoke-virtual {p0, v5}, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncSetupActivity;->setTheme(I)V

    .line 68
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 70
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncSetupActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    .line 71
    .local v1, "actionBar":Landroid/app/ActionBar;
    invoke-virtual {v1, v9}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 73
    const/high16 v5, 0x7f030000

    invoke-virtual {p0, v5}, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncSetupActivity;->setContentView(I)V

    .line 74
    const v5, 0x7f040006

    invoke-virtual {p0, v5}, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncSetupActivity;->addPreferencesFromResource(I)V

    .line 76
    const v5, 0x7f080046

    new-array v6, v10, [Ljava/lang/Object;

    invoke-virtual {p0, v8}, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncSetupActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-virtual {p0, v5, v6}, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncSetupActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncSetupActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 78
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncSetupActivity;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncSetupActivity;->mAppContext:Landroid/content/Context;

    invoke-virtual {v5, v6}, Landroid/preference/PreferenceManager;->createPreferenceScreen(Landroid/content/Context;)Landroid/preference/PreferenceScreen;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncSetupActivity;->mSyncIntervalSetting:Landroid/preference/PreferenceScreen;

    .line 79
    iget-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncSetupActivity;->mSyncIntervalSetting:Landroid/preference/PreferenceScreen;

    const v6, 0x7f080056

    invoke-virtual {v5, v6}, Landroid/preference/PreferenceScreen;->setTitle(I)V

    .line 80
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncSetupActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncSetupActivity;->mSyncIntervalSetting:Landroid/preference/PreferenceScreen;

    invoke-virtual {v5, v6}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 82
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncSetupActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "content://com.sec.android.gallery3d.instagram.contentprovider/support_instagram"

    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/content/ContentResolver;->getType(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncSetupActivity;->mIsGallerySync:Ljava/lang/String;

    .line 84
    iget-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncSetupActivity;->mAppContext:Landroid/content/Context;

    invoke-static {v5}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v5

    const-string v6, "com.sec.android.app.sns3.instagram"

    invoke-virtual {v5, v6}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 86
    .local v0, "account":[Landroid/accounts/Account;
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/sns3/SnsApplication;->getFeatureMgr()Lcom/sec/android/app/sns3/SnsFeatureManager;

    move-result-object v2

    .line 87
    .local v2, "featureMgr":Lcom/sec/android/app/sns3/SnsFeatureManager;
    const/4 v4, 0x0

    .line 88
    .local v4, "providerLabel":Ljava/lang/CharSequence;
    const/4 v3, 0x0

    .line 91
    .local v3, "providerInfo":Landroid/content/pm/ProviderInfo;
    const-string v5, "true"

    iget-object v6, p0, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncSetupActivity;->mIsGallerySync:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_a

    .line 92
    const/4 v4, 0x0

    .line 93
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncSetupActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    const-string v6, "com.sec.android.gallery3d.instagram.contentprovider"

    invoke-virtual {v5, v6, v9}, Landroid/content/pm/PackageManager;->resolveContentProvider(Ljava/lang/String;I)Landroid/content/pm/ProviderInfo;

    move-result-object v3

    .line 94
    if-eqz v3, :cond_0

    .line 95
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncSetupActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/content/pm/ProviderInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v4

    .line 97
    :cond_0
    if-eqz v4, :cond_1

    invoke-interface {v4}, Ljava/lang/CharSequence;->length()I

    move-result v5

    if-nez v5, :cond_2

    .line 98
    :cond_1
    const-string v5, "SnsAccountInAuth"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "providerLabel is null - "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 99
    const-string v4, "Gallery"

    .line 101
    :cond_2
    new-instance v5, Landroid/preference/CheckBoxPreference;

    iget-object v6, p0, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncSetupActivity;->mAppContext:Landroid/content/Context;

    invoke-direct {v5, v6}, Landroid/preference/CheckBoxPreference;-><init>(Landroid/content/Context;)V

    iput-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncSetupActivity;->mGalleryCheckBox:Landroid/preference/CheckBoxPreference;

    .line 102
    iget-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncSetupActivity;->mGalleryCheckBox:Landroid/preference/CheckBoxPreference;

    new-array v6, v10, [Ljava/lang/Object;

    aput-object v4, v6, v9

    invoke-virtual {p0, v11, v6}, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncSetupActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/preference/CheckBoxPreference;->setTitle(Ljava/lang/CharSequence;)V

    .line 103
    iget-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncSetupActivity;->mGalleryCheckBox:Landroid/preference/CheckBoxPreference;

    const v6, 0x7f080054

    invoke-virtual {p0, v6}, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncSetupActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/preference/CheckBoxPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 104
    iget-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncSetupActivity;->mGalleryCheckBox:Landroid/preference/CheckBoxPreference;

    const-string v6, "snsaccount_sync_gallery"

    invoke-virtual {v5, v6}, Landroid/preference/CheckBoxPreference;->setKey(Ljava/lang/String;)V

    .line 105
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncSetupActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncSetupActivity;->mGalleryCheckBox:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v5, v6}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 106
    iget-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncSetupActivity;->mGalleryCheckBox:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v5, v10}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 113
    :cond_3
    :goto_1
    const/4 v4, 0x0

    .line 114
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncSetupActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    const-string v6, "com.sec.android.app.sns3.life"

    invoke-virtual {v5, v6, v9}, Landroid/content/pm/PackageManager;->resolveContentProvider(Ljava/lang/String;I)Landroid/content/pm/ProviderInfo;

    move-result-object v3

    .line 115
    if-eqz v3, :cond_4

    .line 116
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncSetupActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/content/pm/ProviderInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v4

    .line 118
    :cond_4
    if-eqz v4, :cond_5

    invoke-interface {v4}, Ljava/lang/CharSequence;->length()I

    move-result v5

    if-nez v5, :cond_6

    .line 119
    :cond_5
    const-string v5, "SnsAccountInAuth"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "providerLabel is null - "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 120
    const-string v4, "Life Times"

    .line 122
    :cond_6
    new-instance v5, Landroid/preference/CheckBoxPreference;

    iget-object v6, p0, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncSetupActivity;->mAppContext:Landroid/content/Context;

    invoke-direct {v5, v6}, Landroid/preference/CheckBoxPreference;-><init>(Landroid/content/Context;)V

    iput-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncSetupActivity;->mFeedsCheckBox:Landroid/preference/CheckBoxPreference;

    .line 123
    iget-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncSetupActivity;->mFeedsCheckBox:Landroid/preference/CheckBoxPreference;

    new-array v6, v10, [Ljava/lang/Object;

    aput-object v4, v6, v9

    invoke-virtual {p0, v11, v6}, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncSetupActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/preference/CheckBoxPreference;->setTitle(Ljava/lang/CharSequence;)V

    .line 124
    iget-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncSetupActivity;->mFeedsCheckBox:Landroid/preference/CheckBoxPreference;

    const v6, 0x7f08005b

    new-array v7, v10, [Ljava/lang/Object;

    invoke-virtual {p0, v8}, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncSetupActivity;->getString(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v9

    invoke-virtual {p0, v6, v7}, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncSetupActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/preference/CheckBoxPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 126
    iget-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncSetupActivity;->mFeedsCheckBox:Landroid/preference/CheckBoxPreference;

    const-string v6, "snsaccount_sync_feeds"

    invoke-virtual {v5, v6}, Landroid/preference/CheckBoxPreference;->setKey(Ljava/lang/String;)V

    .line 127
    invoke-virtual {v2}, Lcom/sec/android/app/sns3/SnsFeatureManager;->isSyncAdapterForProfileFeedsNeeded()Z

    move-result v5

    if-eqz v5, :cond_7

    .line 128
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncSetupActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncSetupActivity;->mFeedsCheckBox:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v5, v6}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 129
    iget-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncSetupActivity;->mFeedsCheckBox:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v5, v10}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 132
    :cond_7
    iget-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncSetupActivity;->mSyncIntervalSetting:Landroid/preference/PreferenceScreen;

    new-instance v6, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncSetupActivity$1;

    invoke-direct {v6, p0}, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncSetupActivity$1;-><init>(Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncSetupActivity;)V

    invoke-virtual {v5, v6}, Landroid/preference/PreferenceScreen;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 142
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncSetupActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v5

    invoke-virtual {v5}, Landroid/preference/PreferenceScreen;->getPreferenceCount()I

    move-result v5

    if-gt v5, v10, :cond_8

    .line 143
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncSetupActivity;->finish()V

    .line 145
    :cond_8
    return-void

    .line 65
    .end local v0    # "account":[Landroid/accounts/Account;
    .end local v1    # "actionBar":Landroid/app/ActionBar;
    .end local v2    # "featureMgr":Lcom/sec/android/app/sns3/SnsFeatureManager;
    .end local v3    # "providerInfo":Landroid/content/pm/ProviderInfo;
    .end local v4    # "providerLabel":Ljava/lang/CharSequence;
    :cond_9
    const v5, 0x1030128

    goto/16 :goto_0

    .line 108
    .restart local v0    # "account":[Landroid/accounts/Account;
    .restart local v1    # "actionBar":Landroid/app/ActionBar;
    .restart local v2    # "featureMgr":Lcom/sec/android/app/sns3/SnsFeatureManager;
    .restart local v3    # "providerInfo":Landroid/content/pm/ProviderInfo;
    .restart local v4    # "providerLabel":Ljava/lang/CharSequence;
    :cond_a
    array-length v5, v0

    if-lez v5, :cond_3

    .line 109
    aget-object v5, v0, v9

    const-string v6, "com.sec.android.gallery3d.instagram.contentprovider"

    invoke-static {v5, v6, v9}, Landroid/content/ContentResolver;->setIsSyncable(Landroid/accounts/Account;Ljava/lang/String;I)V

    goto/16 :goto_1
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 190
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    .line 192
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncSetupActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->isActionbarLightTheme(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 193
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncSetupActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0a0001

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 197
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 195
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncSetupActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const/high16 v1, 0x7f0a0000

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 226
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onDestroy()V

    .line 227
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 202
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 210
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    .line 212
    :goto_0
    return v0

    .line 204
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncSetupActivity;->onClickDone()V

    .line 212
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 207
    :pswitch_1
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncSetupActivity;->finish()V

    goto :goto_1

    .line 202
    nop

    :pswitch_data_0
    .packed-switch 0x7f0b0005
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 149
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 151
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncSetupActivity;->mFeedsCheckBox:Landroid/preference/CheckBoxPreference;

    const-string v1, "feeds"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 152
    const-string v0, "true"

    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncSetupActivity;->mIsGallerySync:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 153
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncSetupActivity;->mGalleryCheckBox:Landroid/preference/CheckBoxPreference;

    const-string v1, "gallery"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 155
    :cond_0
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 159
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 161
    const-string v0, "feeds"

    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncSetupActivity;->mFeedsCheckBox:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 162
    const-string v0, "true"

    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncSetupActivity;->mIsGallerySync:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 163
    const-string v0, "gallery"

    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncSetupActivity;->mGalleryCheckBox:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 165
    :cond_0
    return-void
.end method
