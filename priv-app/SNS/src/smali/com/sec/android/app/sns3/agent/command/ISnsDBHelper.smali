.class public interface abstract Lcom/sec/android/app/sns3/agent/command/ISnsDBHelper;
.super Ljava/lang/Object;
.source "ISnsDBHelper.java"


# static fields
.field public static final SNS_DB_BIRTHDAY_TYPE:Ljava/lang/String; = "VARCHAR(15)"

.field public static final SNS_DB_BOOLEAN_TYPE:Ljava/lang/String; = "BOOLEAN"

.field public static final SNS_DB_CONTENT_TYPE:Ljava/lang/String; = "TEXT"

.field public static final SNS_DB_EMAIL_TYPE:Ljava/lang/String; = "VARCHAR(150)"

.field public static final SNS_DB_ID_TYPE:Ljava/lang/String; = "VARCHAR(50)"

.field public static final SNS_DB_INTEGER_TYPE:Ljava/lang/String; = "INTEGER"

.field public static final SNS_DB_NAME_TYPE:Ljava/lang/String; = "VARCHAR(100)"

.field public static final SNS_DB_PHONENUMBER_TYPE:Ljava/lang/String; = "VARCHAR(40)"

.field public static final SNS_DB_PHONETYPE_TYPE:Ljava/lang/String; = "VARCHAR(20)"

.field public static final SNS_DB_PRIMARY_KEY_TYPE:Ljava/lang/String; = "INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL"

.field public static final SNS_DB_SP_TYPE:Ljava/lang/String; = "INTEGER"

.field public static final SNS_DB_TEXT_TYPE:Ljava/lang/String; = "TEXT"

.field public static final SNS_DB_TIME_TYPE:Ljava/lang/String; = "TIMESTAMP"

.field public static final SNS_DB_TITLE_TYPE:Ljava/lang/String; = "TEXT"

.field public static final SNS_DB_URL_FILEPATH:Ljava/lang/String; = "VARCHAR(512)"

.field public static final SNS_DB_URL_TYPE:Ljava/lang/String; = "VARCHAR(1024)"
