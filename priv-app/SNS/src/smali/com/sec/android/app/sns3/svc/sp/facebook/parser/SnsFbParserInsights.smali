.class public Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserInsights;
.super Ljava/lang/Object;
.source "SnsFbParserInsights.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserInsights$FacebookInsights;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    return-void
.end method

.method public static parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseInsights;
    .locals 10
    .param p0, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 48
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseInsights;

    invoke-direct {v0}, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseInsights;-><init>()V

    .line 50
    .local v0, "Insights":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseInsights;
    const/4 v4, 0x0

    .line 51
    .local v4, "insights":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseInsight;
    const/4 v1, 0x0

    .line 54
    .local v1, "curInsights":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseInsight;
    :try_start_0
    new-instance v7, Lorg/json/JSONObject;

    invoke-direct {v7, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 56
    .local v7, "jsonObject":Lorg/json/JSONObject;
    new-instance v5, Lorg/json/JSONArray;

    const-string v9, "data"

    invoke-virtual {v7, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v5, v9}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 58
    .local v5, "ja":Lorg/json/JSONArray;
    if-eqz v5, :cond_1

    .line 60
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-virtual {v5}, Lorg/json/JSONArray;->length()I

    move-result v9

    if-ge v3, v9, :cond_1

    .line 61
    new-instance v8, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseInsight;

    invoke-direct {v8}, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseInsight;-><init>()V

    .line 62
    .local v8, "newInsight":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseInsight;
    new-instance v6, Lorg/json/JSONObject;

    invoke-virtual {v5, v3}, Lorg/json/JSONArray;->optString(I)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v6, v9}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 64
    .local v6, "jsonObj":Lorg/json/JSONObject;
    const-string v9, "id"

    invoke-virtual {v6, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v8, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseInsight;->mID:Ljava/lang/String;

    .line 65
    const-string v9, "name"

    invoke-virtual {v6, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v8, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseInsight;->mName:Ljava/lang/String;

    .line 66
    const-string v9, "period"

    invoke-virtual {v6, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v8, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseInsight;->mPeriod:Ljava/lang/String;

    .line 67
    const-string v9, "description"

    invoke-virtual {v6, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v8, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseInsight;->mDescription:Ljava/lang/String;

    .line 69
    const-string v9, "values"

    invoke-virtual {v6, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserValue;->parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseValue;

    move-result-object v9

    iput-object v9, v8, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseInsight;->mValues:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseValue;

    .line 72
    if-nez v4, :cond_0

    .line 73
    move-object v4, v8

    .line 74
    move-object v1, v4

    .line 60
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 76
    :cond_0
    iput-object v8, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseInsight;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseInsight;

    .line 77
    iget-object v1, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseInsight;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseInsight;

    goto :goto_1

    .line 82
    .end local v3    # "i":I
    .end local v6    # "jsonObj":Lorg/json/JSONObject;
    .end local v8    # "newInsight":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseInsight;
    :cond_1
    iput-object v4, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseInsights;->mInsights:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseInsight;

    .line 84
    const-string v9, "paging"

    invoke-virtual {v7, v9}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 85
    const-string v9, "paging"

    invoke-virtual {v7, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserPaging;->parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePaging;

    move-result-object v9

    iput-object v9, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseInsights;->mPaging:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePaging;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 94
    .end local v5    # "ja":Lorg/json/JSONArray;
    .end local v7    # "jsonObject":Lorg/json/JSONObject;
    :cond_2
    :goto_2
    return-object v0

    .line 89
    :catch_0
    move-exception v2

    .line 91
    .local v2, "e":Lorg/json/JSONException;
    invoke-virtual {v2}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_2
.end method
