.class Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$SessionStatusCallback;
.super Ljava/lang/Object;
.source "SnsAccountFbAuthSSOActivity.java"

# interfaces
.implements Lcom/facebook/Session$StatusCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SessionStatusCallback"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;)V
    .locals 0

    .prologue
    .line 643
    iput-object p1, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$SessionStatusCallback;->this$0:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;
    .param p2, "x1"    # Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$1;

    .prologue
    .line 643
    invoke-direct {p0, p1}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$SessionStatusCallback;-><init>(Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;)V

    return-void
.end method


# virtual methods
.method public call(Lcom/facebook/Session;Lcom/facebook/SessionState;Ljava/lang/Exception;)V
    .locals 7
    .param p1, "session"    # Lcom/facebook/Session;
    .param p2, "state"    # Lcom/facebook/SessionState;
    .param p3, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 647
    # getter for: Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->access$400()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "SessionStatusCallback : SessionState = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 649
    if-eqz p3, :cond_4

    .line 650
    instance-of v4, p3, Lcom/facebook/FacebookOperationCanceledException;

    if-eqz v4, :cond_2

    .line 651
    # getter for: Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->access$400()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "FacebookOperationCanceledException - "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p3}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 652
    iget-object v4, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$SessionStatusCallback;->this$0:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;

    # getter for: Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->mSkipSSONoti:Z
    invoke-static {v4}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->access$1300(Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 653
    iget-object v4, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$SessionStatusCallback;->this$0:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;

    new-instance v5, Landroid/content/Intent;

    const-string v6, "com.sec.android.app.sns3.auth.sp.facebook.SNS_LOGIN_FACEBOOK_NOTIFICATION"

    invoke-direct {v5, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v5}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 664
    :cond_0
    :goto_0
    iget-object v4, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$SessionStatusCallback;->this$0:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;

    invoke-virtual {v4}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->finish()V

    .line 721
    :cond_1
    :goto_1
    return-void

    .line 656
    :cond_2
    instance-of v4, p3, Lcom/facebook/FacebookException;

    if-eqz v4, :cond_3

    .line 657
    # getter for: Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->access$400()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "FacebookException - "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p3}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 658
    iget-object v4, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$SessionStatusCallback;->this$0:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;

    # invokes: Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->registerRetryNotification()V
    invoke-static {v4}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->access$1400(Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;)V

    goto :goto_0

    .line 660
    :cond_3
    # getter for: Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->access$400()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Exception - "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p3}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 661
    iget-object v4, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$SessionStatusCallback;->this$0:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;

    # invokes: Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->registerRetryNotification()V
    invoke-static {v4}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->access$1400(Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;)V

    goto :goto_0

    .line 668
    :cond_4
    :try_start_0
    invoke-virtual {p1}, Lcom/facebook/Session;->isOpened()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 669
    iget-object v4, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$SessionStatusCallback;->this$0:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;

    # getter for: Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->mHandler:Landroid/os/Handler;
    invoke-static {v4}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->access$300(Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;)Landroid/os/Handler;

    move-result-object v4

    new-instance v5, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$SessionStatusCallback$1;

    invoke-direct {v5, p0}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$SessionStatusCallback$1;-><init>(Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$SessionStatusCallback;)V

    invoke-virtual {v4, v5}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 683
    invoke-virtual {p1}, Lcom/facebook/Session;->getAccessToken()Ljava/lang/String;

    move-result-object v0

    .line 684
    .local v0, "accessToken":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/facebook/Session;->getExpirationDate()Ljava/util/Date;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    .line 687
    .local v2, "expires":Ljava/lang/String;
    new-instance v4, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$SessionStatusCallback$2;

    invoke-direct {v4, p0, v0, v2}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$SessionStatusCallback$2;-><init>(Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$SessionStatusCallback;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p1, v4}, Lcom/facebook/Request;->newMeRequest(Lcom/facebook/Session;Lcom/facebook/Request$GraphUserCallback;)Lcom/facebook/Request;

    move-result-object v3

    .line 707
    .local v3, "request":Lcom/facebook/Request;
    const/4 v4, 0x1

    new-array v4, v4, [Lcom/facebook/Request;

    const/4 v5, 0x0

    aput-object v3, v4, v5

    invoke-static {v4}, Lcom/facebook/Request;->executeBatchAsync([Lcom/facebook/Request;)Lcom/facebook/RequestAsyncTask;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_1

    .line 715
    .end local v0    # "accessToken":Ljava/lang/String;
    .end local v2    # "expires":Ljava/lang/String;
    .end local v3    # "request":Lcom/facebook/Request;
    :catch_0
    move-exception v1

    .line 716
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 717
    iget-object v4, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$SessionStatusCallback;->this$0:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;

    # invokes: Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->registerRetryNotification()V
    invoke-static {v4}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->access$1400(Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;)V

    .line 718
    iget-object v4, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$SessionStatusCallback;->this$0:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;

    invoke-virtual {v4}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->finish()V

    goto/16 :goto_1

    .line 708
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_5
    :try_start_1
    invoke-virtual {p1}, Lcom/facebook/Session;->isClosed()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 709
    # getter for: Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->access$400()Ljava/lang/String;

    move-result-object v4

    const-string v5, "session is closed"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 710
    iget-object v4, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$SessionStatusCallback;->this$0:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;

    # invokes: Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->registerRetryNotification()V
    invoke-static {v4}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->access$1400(Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;)V

    .line 711
    iget-object v4, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$SessionStatusCallback;->this$0:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;

    invoke-virtual {v4}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->finish()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_1
.end method
