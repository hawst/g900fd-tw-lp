.class final Lcom/sec/android/app/sns3/app/profile/sp/facebook/SnsFbGetMeProfile$1;
.super Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqGetMyProfile;
.source "SnsFbGetMeProfile.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/sns3/app/profile/sp/facebook/SnsFbGetMeProfile;->getMeProfile(Landroid/content/Context;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/content/Context;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    .prologue
    .line 61
    iput-object p2, p0, Lcom/sec/android/app/sns3/app/profile/sp/facebook/SnsFbGetMeProfile$1;->val$context:Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqGetMyProfile;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;)V

    return-void
.end method


# virtual methods
.method public onReqRespond(IZIILandroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMyProfile;)Z
    .locals 4
    .param p1, "reqID"    # I
    .param p2, "bSuccess"    # Z
    .param p3, "httpStatus"    # I
    .param p4, "errorCode"    # I
    .param p5, "reason"    # Landroid/os/Bundle;
    .param p6, "profile"    # Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMyProfile;

    .prologue
    .line 66
    const/4 v0, -0x1

    .line 68
    .local v0, "resultCode":I
    if-eqz p2, :cond_0

    .line 69
    const/4 v0, 0x0

    .line 71
    const-string v1, "SnsFbGetMeProfile"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SnsFbReqGetMyProfile - NAME : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMyProfile;->mName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " EMAIL "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMyProfile;->mEmail:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " BIRTHDAY "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMyProfile;->mBirthdayDate:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ADDRESS "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMyProfile;->mCurrentLocation:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " WORK "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMyProfile;->mWork:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " PROFILE URL "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMyProfile;->mPic:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 82
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/sns3/app/profile/sp/facebook/SnsFbGetMeProfile$1;->val$context:Landroid/content/Context;

    # invokes: Lcom/sec/android/app/sns3/app/profile/sp/facebook/SnsFbGetMeProfile;->invokeBroadcast(Landroid/content/Context;ILcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMyProfile;)V
    invoke-static {v1, v0, p6}, Lcom/sec/android/app/sns3/app/profile/sp/facebook/SnsFbGetMeProfile;->access$000(Landroid/content/Context;ILcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMyProfile;)V

    .line 83
    const/4 v1, 0x1

    return v1

    .line 76
    :cond_0
    const/4 v0, -0x1

    .line 78
    const-string v1, "SnsFbGetMeProfile"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SnsFbReqGetMyProfile errorCode : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", reason : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
