.class public Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzToken;
.super Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;
.source "SnsQzToken.java"


# static fields
.field public static final ACCESS_TOKEN:Ljava/lang/String; = "access_token"

.field public static final EXPIRES:Ljava/lang/String; = "expires_in"

.field public static final USER_ID:Ljava/lang/String; = "user_id"


# instance fields
.field private mAccessToken:Ljava/lang/String;

.field private mExpires:Ljava/lang/String;

.field private final mSharedPref:Landroid/content/SharedPreferences;

.field private mUserID:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;-><init>()V

    .line 44
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v0

    const-string v1, "Qzone_Token"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/sns3/SnsApplication;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzToken;->mSharedPref:Landroid/content/SharedPreferences;

    .line 46
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzToken;->readTokenInfo()V

    .line 47
    return-void
.end method


# virtual methods
.method public getAccessToken()Ljava/lang/String;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzToken;->mAccessToken:Ljava/lang/String;

    return-object v0
.end method

.method public getExpires()Ljava/lang/String;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzToken;->mExpires:Ljava/lang/String;

    return-object v0
.end method

.method public getUserID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzToken;->mUserID:Ljava/lang/String;

    return-object v0
.end method

.method public isValidAccessTokenNExpires()Z
    .locals 2

    .prologue
    .line 52
    const/4 v0, 0x0

    .line 54
    .local v0, "isValid":Z
    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzToken;->mAccessToken:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzToken;->mAccessToken:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzToken;->mExpires:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzToken;->mExpires:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    .line 56
    const/4 v0, 0x1

    .line 58
    :cond_0
    return v0
.end method

.method public readTokenInfo()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 108
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzToken;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v1, "access_token"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzToken;->mAccessToken:Ljava/lang/String;

    .line 109
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzToken;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v1, "expires_in"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzToken;->mExpires:Ljava/lang/String;

    .line 110
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzToken;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v1, "user_id"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzToken;->mUserID:Ljava/lang/String;

    .line 112
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzToken;->mAccessToken:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzToken;->mAccessToken:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzToken;->mExpires:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzToken;->mExpires:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 114
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzToken;->setTokenState(I)V

    .line 117
    :goto_0
    return-void

    .line 116
    :cond_0
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzToken;->setTokenState(I)V

    goto :goto_0
.end method

.method public removeAll()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 91
    invoke-virtual {p0, v0, v0, v0}, Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzToken;->setTokenInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    return-void
.end method

.method public setTokenInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "accessToken"    # Ljava/lang/String;
    .param p2, "expires"    # Ljava/lang/String;
    .param p3, "userID"    # Ljava/lang/String;

    .prologue
    .line 62
    iput-object p1, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzToken;->mAccessToken:Ljava/lang/String;

    .line 63
    iput-object p2, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzToken;->mExpires:Ljava/lang/String;

    .line 64
    iput-object p3, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzToken;->mUserID:Ljava/lang/String;

    .line 66
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzToken;->mAccessToken:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzToken;->mAccessToken:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzToken;->mExpires:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzToken;->mExpires:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzToken;->mUserID:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzToken;->mUserID:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 69
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzToken;->setTokenState(I)V

    .line 74
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzToken;->writeTokenInfo()V

    .line 75
    return-void

    .line 71
    :cond_0
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzToken;->setTokenState(I)V

    goto :goto_0
.end method

.method public writeTokenInfo()V
    .locals 3

    .prologue
    .line 97
    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzToken;->mSharedPref:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 99
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "access_token"

    iget-object v2, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzToken;->mAccessToken:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 100
    const-string v1, "expires_in"

    iget-object v2, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzToken;->mExpires:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 101
    const-string v1, "user_id"

    iget-object v2, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzToken;->mUserID:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 103
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 104
    return-void
.end method
