.class interface abstract Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseSkills$LinkedInSkills;
.super Ljava/lang/Object;
.source "SnsLiResponseSkills.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseSkills;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x608
    name = "LinkedInSkills"
.end annotation


# static fields
.field public static final SKILL:Ljava/lang/String; = "skill"

.field public static final SKILL_ID:Ljava/lang/String; = "id"

.field public static final SKILL_NAME:Ljava/lang/String; = "name"

.field public static final VALUES:Ljava/lang/String; = "values"
