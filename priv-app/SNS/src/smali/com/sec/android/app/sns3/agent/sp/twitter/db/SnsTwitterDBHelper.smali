.class public Lcom/sec/android/app/sns3/agent/sp/twitter/db/SnsTwitterDBHelper;
.super Lcom/sec/android/app/sns3/agent/db/SnsDBHelperBase;
.source "SnsTwitterDBHelper.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 31
    const-string v0, "snsTwitterDB.db"

    const/4 v1, 0x0

    const/16 v2, 0xd

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/sec/android/app/sns3/agent/db/SnsDBHelperBase;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 32
    return-void
.end method


# virtual methods
.method public createTables(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 49
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 51
    const-string v0, "CREATE TABLE IF NOT EXISTS user_basic_info (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,user_id VARCHAR(50),user_name TEXT, UNIQUE (user_id));"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 57
    const-string v0, "SnsAgent"

    const-string v1, "DB TABLE(user_basic_info) Has been created."

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 61
    const-string v0, "CREATE TABLE IF NOT EXISTS timeline (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,tweet_id VARCHAR(50),user_name TEXT,user_screen_name TEXT,message TEXT,picture TEXT,media_url VARCHAR(1024),create_at TIMESTAMP, UNIQUE (_id));"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 72
    const-string v0, "SnsAgent"

    const-string v1, "DB TABLE(timeline) Has been created."

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 75
    const-string v0, "CREATE TABLE IF NOT EXISTS usertimeline (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,tweet_id VARCHAR(50),user_name TEXT,user_screen_name TEXT,message TEXT,picture TEXT,link TEXT,media_url VARCHAR(1024),create_at TIMESTAMP, UNIQUE (_id));"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 87
    const-string v0, "SnsAgent"

    const-string v1, "DB TABLE(usertimeline) Has been created."

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 90
    const-string v0, "CREATE TABLE IF NOT EXISTS detailview (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,user_name VARCHAR(100),user_screen_name VARCHAR(100),user_id VARCHAR(50),target_name VARCHAR(100),target_screen_name VARCHAR(100),target_id VARCHAR(50),in_reply_to_status_id VARCHAR(50),mention_id VARCHAR(50),text TEXT,retweeted BOOLEAN,favorited BOOLEAN,icon_url VARCHAR(1024),poto_url VARCHAR(1024),poto_size VARCHAR(1024),link_url VARCHAR(1024),create_at TIMESTAMP);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 109
    const-string v0, "SnsAgent"

    const-string v1, "DB TABLE(detailview) Has been created."

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 113
    const-string v0, "CREATE TABLE IF NOT EXISTS status_stream (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,tweet_id VARCHAR(50),user_id VARCHAR(50),user_name TEXT,user_screen_name TEXT,retweet_count INTEGER,favourites_count INTEGER,message TEXT,picture TEXT,link TEXT,media_url VARCHAR(1024),create_at TIMESTAMP, UNIQUE (_id));"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 128
    const-string v0, "SnsAgent"

    const-string v1, "DB TABLE(status_stream) Has been created."

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 132
    const-string v0, "CREATE TABLE IF NOT EXISTS friends_profile_info (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,profile_id VARCHAR(50),profile_name TEXT,screen_name TEXT,message TEXT,picture TEXT,link TEXT,media_url VARCHAR(1024),create_at TIMESTAMP, UNIQUE (profile_id));"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 144
    const-string v0, "SnsAgent"

    const-string v1, "DB TABLE(friends_profile_info) Has been created."

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 146
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 147
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 148
    return-void
.end method

.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 0
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 36
    invoke-super {p0, p1}, Lcom/sec/android/app/sns3/agent/db/SnsDBHelperBase;->onCreate(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 38
    invoke-virtual {p0, p1}, Lcom/sec/android/app/sns3/agent/sp/twitter/db/SnsTwitterDBHelper;->createTables(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 39
    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 0
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "oldVersion"    # I
    .param p3, "newVersion"    # I

    .prologue
    .line 43
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/app/sns3/agent/db/SnsDBHelperBase;->onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V

    .line 45
    invoke-virtual {p0, p1}, Lcom/sec/android/app/sns3/agent/sp/twitter/db/SnsTwitterDBHelper;->upgradeTables(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 46
    return-void
.end method

.method public upgradeTables(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 153
    const-string v0, "DROP TABLE IF EXISTS user_basic_info"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 154
    const-string v0, "DROP TABLE IF EXISTS timeline"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 155
    const-string v0, "DROP TABLE IF EXISTS usertimeline"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 156
    const-string v0, "DROP TABLE IF EXISTS detailview"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 157
    const-string v0, "DROP TABLE IF EXISTS status_stream"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 158
    const-string v0, "DROP TABLE IF EXISTS friends_profile_info"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 159
    invoke-virtual {p0, p1}, Lcom/sec/android/app/sns3/agent/sp/twitter/db/SnsTwitterDBHelper;->createTables(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 160
    return-void
.end method

.method public wipeData(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 165
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 167
    :try_start_0
    const-string v0, "DELETE FROM user_basic_info"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 168
    const-string v0, "DELETE FROM timeline"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 169
    const-string v0, "DELETE FROM usertimeline"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 170
    const-string v0, "DELETE FROM detailview"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 171
    const-string v0, "DELETE FROM status_stream"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 172
    const-string v0, "DELETE FROM friends_profile_info"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 173
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 175
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 178
    return-void

    .line 175
    :catchall_0
    move-exception v0

    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0
.end method
