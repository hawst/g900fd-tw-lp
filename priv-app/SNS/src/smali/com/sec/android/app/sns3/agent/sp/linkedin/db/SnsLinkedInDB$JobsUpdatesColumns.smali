.class public interface abstract Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$JobsUpdatesColumns;
.super Ljava/lang/Object;
.source "SnsLinkedInDB.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "JobsUpdatesColumns"
.end annotation


# static fields
.field public static final COMPANY:Ljava/lang/String; = "company"

.field public static final JOBS_UPDATE_KEY:Ljava/lang/String; = "jobs_update_key"

.field public static final JOB_ID:Ljava/lang/String; = "job_id"

.field public static final JOB_POSITION:Ljava/lang/String; = "position"

.field public static final JOB_POSTER:Ljava/lang/String; = "job_poster"

.field public static final JOB_URL:Ljava/lang/String; = "job_url"
