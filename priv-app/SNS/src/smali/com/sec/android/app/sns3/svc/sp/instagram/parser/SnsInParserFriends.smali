.class public Lcom/sec/android/app/sns3/svc/sp/instagram/parser/SnsInParserFriends;
.super Ljava/lang/Object;
.source "SnsInParserFriends.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/instagram/parser/SnsInParserFriends$InstagramFriend;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    return-void
.end method

.method public static parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseFriends;
    .locals 9
    .param p0, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 38
    new-instance v4, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseFriends;

    invoke-direct {v4}, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseFriends;-><init>()V

    .line 40
    .local v4, "friendsObj":Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseFriends;
    const/4 v3, 0x0

    .line 41
    .local v3, "friends":Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseProfile;
    const/4 v0, 0x0

    .line 44
    .local v0, "curFriends":Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseProfile;
    :try_start_0
    new-instance v6, Lorg/json/JSONObject;

    invoke-direct {v6, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 45
    .local v6, "job":Lorg/json/JSONObject;
    const-string v8, "data"

    invoke-virtual {v6, v8}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v1

    .line 47
    .local v1, "dataArray":Lorg/json/JSONArray;
    if-eqz v1, :cond_1

    .line 48
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v8

    if-ge v5, v8, :cond_1

    .line 49
    invoke-virtual {v1, v5}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/sec/android/app/sns3/svc/sp/instagram/parser/SnsInParserFriends;->parseFriend(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseProfile;

    move-result-object v7

    .line 51
    .local v7, "newFriend":Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseProfile;
    if-nez v3, :cond_0

    .line 52
    move-object v3, v7

    .line 53
    move-object v0, v3

    .line 48
    :goto_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 55
    :cond_0
    iput-object v7, v0, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseProfile;->mNext:Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseProfile;

    .line 56
    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseProfile;->mNext:Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseProfile;

    goto :goto_1

    .line 60
    .end local v5    # "i":I
    .end local v7    # "newFriend":Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseProfile;
    :cond_1
    iput-object v3, v4, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseFriends;->mFriends:Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseProfile;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 65
    .end local v1    # "dataArray":Lorg/json/JSONArray;
    .end local v6    # "job":Lorg/json/JSONObject;
    :goto_2
    return-object v4

    .line 61
    :catch_0
    move-exception v2

    .line 62
    .local v2, "e1":Lorg/json/JSONException;
    invoke-virtual {v2}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_2
.end method

.method private static parseFriend(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseProfile;
    .locals 4
    .param p0, "response"    # Ljava/lang/String;

    .prologue
    .line 70
    new-instance v1, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseProfile;

    invoke-direct {v1}, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseProfile;-><init>()V

    .line 73
    .local v1, "profile":Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseProfile;
    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 75
    .local v2, "profileObj":Lorg/json/JSONObject;
    const-string v3, "id"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseProfile;->mUserID:Ljava/lang/String;

    .line 76
    const-string v3, "username"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseProfile;->mUserName:Ljava/lang/String;

    .line 77
    const-string v3, "full_name"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseProfile;->mFullName:Ljava/lang/String;

    .line 78
    const-string v3, "bio"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseProfile;->mBio:Ljava/lang/String;

    .line 79
    const-string v3, "website"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseProfile;->mWebsite:Ljava/lang/String;

    .line 80
    const-string v3, "profile_picture"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseProfile;->mImageUrl:Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 84
    .end local v2    # "profileObj":Lorg/json/JSONObject;
    :goto_0
    return-object v1

    .line 81
    :catch_0
    move-exception v0

    .line 82
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method
