.class public Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserPaging;
.super Ljava/lang/Object;
.source "SnsFbParserPaging.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserPaging$FacebookPaging;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    return-void
.end method

.method public static parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePaging;
    .locals 4
    .param p0, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 36
    new-instance v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePaging;

    invoke-direct {v2}, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePaging;-><init>()V

    .line 39
    .local v2, "paging":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePaging;
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 41
    .local v1, "jsonObject":Lorg/json/JSONObject;
    const-string v3, "previous"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePaging;->mPreviousPaging:Ljava/lang/String;

    .line 42
    const-string v3, "next"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePaging;->mNextPaging:Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 49
    .end local v1    # "jsonObject":Lorg/json/JSONObject;
    :goto_0
    return-object v2

    .line 44
    :catch_0
    move-exception v0

    .line 46
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method
