.class public interface abstract Lcom/sec/android/app/sns3/agent/sp/twitter/db/SnsTwitterDB$DetailViewColumns;
.super Ljava/lang/Object;
.source "SnsTwitterDB.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/agent/sp/twitter/db/SnsTwitterDB;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "DetailViewColumns"
.end annotation


# static fields
.field public static final CREATE_AT:Ljava/lang/String; = "create_at"

.field public static final FAVORITED:Ljava/lang/String; = "favorited"

.field public static final ICON_URL:Ljava/lang/String; = "icon_url"

.field public static final IN_REPLY_TO_STATUS_ID:Ljava/lang/String; = "in_reply_to_status_id"

.field public static final LINK_URL:Ljava/lang/String; = "link_url"

.field public static final MENTION_ID:Ljava/lang/String; = "mention_id"

.field public static final POTO_SIZE:Ljava/lang/String; = "poto_size"

.field public static final POTO_URL:Ljava/lang/String; = "poto_url"

.field public static final RETWEETED:Ljava/lang/String; = "retweeted"

.field public static final TARGET_ID:Ljava/lang/String; = "target_id"

.field public static final TARGET_NAME:Ljava/lang/String; = "target_name"

.field public static final TARGET_SCREEN_NAME:Ljava/lang/String; = "target_screen_name"

.field public static final TEXT:Ljava/lang/String; = "text"

.field public static final USER_ID:Ljava/lang/String; = "user_id"

.field public static final USER_NAME:Ljava/lang/String; = "user_name"

.field public static final USER_SCREEN_NAME:Ljava/lang/String; = "user_screen_name"

.field public static final _ID:Ljava/lang/String; = "_id"
