.class public Lcom/sec/android/app/sns3/app/home/sp/instagram/SnsInHomeGetFeed;
.super Ljava/lang/Object;
.source "SnsInHomeGetFeed.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "SnsInHomeGetFeed"

.field private static mCmdHandler:Landroid/os/Handler;

.field private static mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000(Landroid/content/Context;ZLjava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Landroid/content/Context;
    .param p1, "x1"    # Z
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 40
    invoke-static {p0, p1, p2}, Lcom/sec/android/app/sns3/app/home/sp/instagram/SnsInHomeGetFeed;->invokeBroadcast(Landroid/content/Context;ZLjava/lang/String;)V

    return-void
.end method

.method private static invokeBroadcast(Landroid/content/Context;ZLjava/lang/String;)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "bResult"    # Z
    .param p2, "uri"    # Ljava/lang/String;

    .prologue
    .line 86
    const/4 v1, 0x0

    .line 88
    .local v1, "intent":Landroid/content/Intent;
    const-string v0, "com.sec.android.app.sns.home.ACTION_INSTAGRAM_FEED_UPDATED"

    .line 90
    .local v0, "action":Ljava/lang/String;
    new-instance v1, Landroid/content/Intent;

    .end local v1    # "intent":Landroid/content/Intent;
    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 91
    .restart local v1    # "intent":Landroid/content/Intent;
    const-string v2, "SNS_CONTENT_URI"

    invoke-virtual {v1, v2, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 92
    const-string v2, "SNS_RESULT"

    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 94
    const-string v2, "com.sec.android.app.sns3.permission.RECEIVE_SNS_BROADCAST"

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    .line 96
    invoke-static {}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->isLoggable()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 97
    const-string v2, "SnsHome"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SnsInHomeGetFeed - invokeBroadcast() : action = ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "], uri = ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "], result = ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 100
    :cond_0
    return-void
.end method

.method public static loadSnsInCmdGetHome(Landroid/content/Context;)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    .line 49
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/sns3/SnsApplication;->getSvcMgr()Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v2

    sput-object v2, Lcom/sec/android/app/sns3/app/home/sp/instagram/SnsInHomeGetFeed;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    .line 50
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/sns3/SnsApplication;->getAgentMgr()Lcom/sec/android/app/sns3/agent/SnsAgentMgr;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/sns3/agent/SnsAgentMgr;->getCommandMgr()Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;->getCommandMgrHandle()Landroid/os/Handler;

    move-result-object v2

    sput-object v2, Lcom/sec/android/app/sns3/app/home/sp/instagram/SnsInHomeGetFeed;->mCmdHandler:Landroid/os/Handler;

    .line 53
    sget-object v2, Lcom/sec/android/app/sns3/app/home/sp/instagram/SnsInHomeGetFeed;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    if-nez v2, :cond_0

    .line 54
    const-string v2, "SnsInHomeGetFeed"

    const-string v3, "[SNS] mSvcMgr is null !!"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 82
    :goto_0
    return-void

    .line 57
    :cond_0
    sget-object v2, Lcom/sec/android/app/sns3/app/home/sp/instagram/SnsInHomeGetFeed;->mCmdHandler:Landroid/os/Handler;

    if-nez v2, :cond_1

    .line 58
    const-string v2, "SnsInHomeGetFeed"

    const-string v3, "[SNS] mCmdHandler is null !!"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 61
    :cond_1
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v2

    const-string v3, "com.sec.android.app.sns3.instagram"

    invoke-virtual {v2, v3}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 63
    .local v0, "account":[Landroid/accounts/Account;
    array-length v2, v0

    if-lez v2, :cond_2

    .line 64
    new-instance v1, Lcom/sec/android/app/sns3/agent/sp/instagram/command/SnsInCmdGetHome;

    sget-object v2, Lcom/sec/android/app/sns3/app/home/sp/instagram/SnsInHomeGetFeed;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    sget-object v3, Lcom/sec/android/app/sns3/app/home/sp/instagram/SnsInHomeGetFeed;->mCmdHandler:Landroid/os/Handler;

    invoke-direct {v1, v2, v3, v4, v4}, Lcom/sec/android/app/sns3/agent/sp/instagram/command/SnsInCmdGetHome;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Handler;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 65
    .local v1, "cmd":Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;
    new-instance v2, Lcom/sec/android/app/sns3/app/home/sp/instagram/SnsInHomeGetFeed$1;

    invoke-direct {v2, p0}, Lcom/sec/android/app/sns3/app/home/sp/instagram/SnsInHomeGetFeed$1;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->setCommandCallback(Lcom/sec/android/app/sns3/agent/command/ISnsCommandCallback;)Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;

    .line 78
    invoke-virtual {v1}, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->send()I

    goto :goto_0

    .line 81
    .end local v1    # "cmd":Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;
    :cond_2
    const/4 v2, 0x0

    invoke-static {p0, v2, v4}, Lcom/sec/android/app/sns3/app/home/sp/instagram/SnsInHomeGetFeed;->invokeBroadcast(Landroid/content/Context;ZLjava/lang/String;)V

    goto :goto_0
.end method
