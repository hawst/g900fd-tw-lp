.class public Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserProfilePicture;
.super Ljava/lang/Object;
.source "SnsFbParserProfilePicture.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserProfilePicture$FacebookProfilePicture;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    return-void
.end method

.method public static parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseProfilePicture;
    .locals 5
    .param p0, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 45
    new-instance v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseProfilePicture;

    invoke-direct {v3}, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseProfilePicture;-><init>()V

    .line 49
    .local v3, "profilePicture":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseProfilePicture;
    :try_start_0
    new-instance v1, Lorg/json/JSONArray;

    invoke-direct {v1, p0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 51
    .local v1, "ja":Lorg/json/JSONArray;
    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v4

    if-lez v4, :cond_0

    .line 53
    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v2

    .line 55
    .local v2, "jsonObj":Lorg/json/JSONObject;
    const-string v4, "id"

    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseProfilePicture;->mId:Ljava/lang/String;

    .line 56
    const-string v4, "url"

    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseProfilePicture;->mUrl:Ljava/lang/String;

    .line 57
    const-string v4, "is_silhouette"

    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseProfilePicture;->mIsSilhouette:Ljava/lang/String;

    .line 58
    const-string v4, "real_width"

    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseProfilePicture;->mWidth:Ljava/lang/String;

    .line 59
    const-string v4, "real_height"

    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseProfilePicture;->mHeight:Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 67
    .end local v1    # "ja":Lorg/json/JSONArray;
    .end local v2    # "jsonObj":Lorg/json/JSONObject;
    :cond_0
    :goto_0
    return-object v3

    .line 62
    :catch_0
    move-exception v0

    .line 64
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method
