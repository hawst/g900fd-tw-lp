.class public Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserMyLikes;
.super Ljava/lang/Object;
.source "SnsFbParserMyLikes.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserMyLikes$FacebookMyLikes;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    return-void
.end method

.method public static parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMyLikes;
    .locals 6
    .param p0, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 39
    new-instance v4, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMyLikes;

    invoke-direct {v4}, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMyLikes;-><init>()V

    .line 43
    .local v4, "myLikes":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMyLikes;
    :try_start_0
    new-instance v2, Lorg/json/JSONArray;

    invoke-direct {v2, p0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 45
    .local v2, "ja":Lorg/json/JSONArray;
    if-eqz v2, :cond_0

    .line 47
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v5

    if-ge v1, v5, :cond_0

    .line 49
    invoke-virtual {v2, v1}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v3

    .line 50
    .local v3, "jsonObj":Lorg/json/JSONObject;
    const-string v5, "user_id"

    invoke-virtual {v3, v5}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMyLikes;->mID:Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 47
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 55
    .end local v1    # "i":I
    .end local v2    # "ja":Lorg/json/JSONArray;
    .end local v3    # "jsonObj":Lorg/json/JSONObject;
    :catch_0
    move-exception v0

    .line 57
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    .line 60
    .end local v0    # "e":Lorg/json/JSONException;
    :cond_0
    return-object v4
.end method
