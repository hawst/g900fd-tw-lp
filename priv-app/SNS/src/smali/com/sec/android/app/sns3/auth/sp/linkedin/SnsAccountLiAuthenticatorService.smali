.class public Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthenticatorService;
.super Landroid/app/Service;
.source "SnsAccountLiAuthenticatorService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthenticatorService$LinkedInAccountAuthenticator;
    }
.end annotation


# static fields
.field private static OPTIONS_PASSWORD:Ljava/lang/String;

.field private static OPTIONS_USERNAME:Ljava/lang/String;

.field private static TAG:Ljava/lang/String;


# instance fields
.field private mAccAuth:Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthenticatorService$LinkedInAccountAuthenticator;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 46
    const-string v0, "SnsAccountLiAuthenticatorService"

    sput-object v0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthenticatorService;->TAG:Ljava/lang/String;

    .line 48
    const-string v0, "username"

    sput-object v0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthenticatorService;->OPTIONS_USERNAME:Ljava/lang/String;

    .line 49
    const-string v0, "password"

    sput-object v0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthenticatorService;->OPTIONS_PASSWORD:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 75
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    sget-object v0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthenticatorService;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100()Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    sget-object v0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthenticatorService;->OPTIONS_USERNAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200()Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    sget-object v0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthenticatorService;->OPTIONS_PASSWORD:Ljava/lang/String;

    return-object v0
.end method

.method private getAccountAuthenticator()Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthenticatorService$LinkedInAccountAuthenticator;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthenticatorService;->mAccAuth:Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthenticatorService$LinkedInAccountAuthenticator;

    if-nez v0, :cond_0

    .line 71
    new-instance v0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthenticatorService$LinkedInAccountAuthenticator;

    invoke-direct {v0, p0, p0}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthenticatorService$LinkedInAccountAuthenticator;-><init>(Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthenticatorService;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthenticatorService;->mAccAuth:Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthenticatorService$LinkedInAccountAuthenticator;

    .line 72
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthenticatorService;->mAccAuth:Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthenticatorService$LinkedInAccountAuthenticator;

    return-object v0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 55
    sget-object v1, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthenticatorService;->TAG:Ljava/lang/String;

    const-string v2, "LinkedInAuthenticatorService : onBind"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 56
    const/4 v0, 0x0

    .line 58
    .local v0, "ret":Landroid/os/IBinder;
    const-string v1, "android.accounts.AccountAuthenticator"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 59
    invoke-direct {p0}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthenticatorService;->getAccountAuthenticator()Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthenticatorService$LinkedInAccountAuthenticator;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthenticatorService$LinkedInAccountAuthenticator;->getIBinder()Landroid/os/IBinder;

    move-result-object v0

    .line 61
    :cond_0
    return-object v0
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 66
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 67
    return-void
.end method
