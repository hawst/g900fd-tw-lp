.class public interface abstract Lcom/sec/android/app/sns3/agent/sp/foursquare/db/SnsFourSquareDB$UserInfoColumns;
.super Ljava/lang/Object;
.source "SnsFourSquareDB.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/agent/sp/foursquare/db/SnsFourSquareDB;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "UserInfoColumns"
.end annotation


# static fields
.field public static final ABOUT_ME:Ljava/lang/String; = "about_me"

.field public static final FACEBOOK:Ljava/lang/String; = "facebook"

.field public static final PHOTO_URL:Ljava/lang/String; = "image_url"

.field public static final TWITTER:Ljava/lang/String; = "twitter"

.field public static final TYPE:Ljava/lang/String; = "type"

.field public static final USER_FIRST_NAME:Ljava/lang/String; = "first_name"

.field public static final USER_FORMATTED_NAME:Ljava/lang/String; = "formatted_name"

.field public static final USER_ID:Ljava/lang/String; = "user_id"

.field public static final USER_LAST_NAME:Ljava/lang/String; = "last_name"
