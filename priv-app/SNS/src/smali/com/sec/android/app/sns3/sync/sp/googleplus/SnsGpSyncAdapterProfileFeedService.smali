.class public Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterProfileFeedService;
.super Landroid/app/Service;
.source "SnsGpSyncAdapterProfileFeedService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterProfileFeedService$SyncAdapterImpl;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "SnsGpSync"


# instance fields
.field private mAccount:Landroid/accounts/Account;

.field private mAuthority:Ljava/lang/String;

.field private mCmdHandler:Landroid/os/Handler;

.field private mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

.field private mSyncAdapter:Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterProfileFeedService$SyncAdapterImpl;

.field private mSyncResult:Landroid/content/SyncResult;

.field private mSyncState:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 53
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 57
    iput-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterProfileFeedService;->mSyncAdapter:Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterProfileFeedService$SyncAdapterImpl;

    .line 59
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterProfileFeedService;->mSyncState:I

    .line 61
    iput-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterProfileFeedService;->mSyncResult:Landroid/content/SyncResult;

    .line 63
    iput-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterProfileFeedService;->mAccount:Landroid/accounts/Account;

    .line 65
    iput-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterProfileFeedService;->mAuthority:Ljava/lang/String;

    .line 70
    return-void
.end method

.method static synthetic access$002(Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterProfileFeedService;Landroid/accounts/Account;)Landroid/accounts/Account;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterProfileFeedService;
    .param p1, "x1"    # Landroid/accounts/Account;

    .prologue
    .line 53
    iput-object p1, p0, Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterProfileFeedService;->mAccount:Landroid/accounts/Account;

    return-object p1
.end method

.method static synthetic access$102(Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterProfileFeedService;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterProfileFeedService;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 53
    iput-object p1, p0, Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterProfileFeedService;->mAuthority:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$202(Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterProfileFeedService;Landroid/content/SyncResult;)Landroid/content/SyncResult;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterProfileFeedService;
    .param p1, "x1"    # Landroid/content/SyncResult;

    .prologue
    .line 53
    iput-object p1, p0, Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterProfileFeedService;->mSyncResult:Landroid/content/SyncResult;

    return-object p1
.end method

.method static synthetic access$300(Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterProfileFeedService;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterProfileFeedService;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/accounts/OperationCanceledException;
        }
    .end annotation

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterProfileFeedService;->performSync()V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterProfileFeedService;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterProfileFeedService;

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterProfileFeedService;->onSyncCanceled()V

    return-void
.end method

.method static synthetic access$502(Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterProfileFeedService;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterProfileFeedService;
    .param p1, "x1"    # I

    .prologue
    .line 53
    iput p1, p0, Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterProfileFeedService;->mSyncState:I

    return p1
.end method

.method private invokeBroadcast()V
    .locals 7

    .prologue
    .line 294
    const/4 v2, 0x0

    .line 296
    .local v2, "intent":Landroid/content/Intent;
    sget-object v3, Lcom/sec/android/app/sns3/agent/sp/twitter/db/SnsTwitterDB$TimeLine;->CONTENT_URI:Landroid/net/Uri;

    .line 297
    .local v3, "uri":Landroid/net/Uri;
    const-string v0, "com.sec.android.app.sns3.action.SYNC_GP_PROFILE_FEEDS"

    .line 298
    .local v0, "action":Ljava/lang/String;
    iget v4, p0, Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterProfileFeedService;->mSyncState:I

    const/4 v5, 0x2

    if-ne v4, v5, :cond_1

    const/4 v1, 0x1

    .line 300
    .local v1, "bResult":Z
    :goto_0
    new-instance v2, Landroid/content/Intent;

    .end local v2    # "intent":Landroid/content/Intent;
    invoke-direct {v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 301
    .restart local v2    # "intent":Landroid/content/Intent;
    const-string v4, "SNS3_CONTENT_URI_PROFILE"

    invoke-virtual {v2, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 302
    const-string v4, "SNS_RESULT"

    invoke-virtual {v2, v4, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 304
    const-string v4, "com.sec.android.app.sns3.permission.RECEIVE_SNS_BROADCAST"

    invoke-virtual {p0, v2, v4}, Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterProfileFeedService;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    .line 305
    invoke-static {}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->isLoggable()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 306
    const-string v4, "SnsGpSync"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "SnsGpSyncAdapterProfileFeedService - invokeBroadcast() : action = ["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "], uri = ["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "], result = ["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 309
    :cond_0
    return-void

    .line 298
    .end local v1    # "bResult":Z
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private onSyncCanceled()V
    .locals 2

    .prologue
    .line 285
    const-string v0, "SnsGpSync"

    const-string v1, "***************** SnsGpSyncAdapterProfileFeedService : onSyncCanceled !!! *****************"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 288
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterProfileFeedService;->mSyncState:I

    .line 290
    return-void
.end method

.method private performSync()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/accounts/OperationCanceledException;
        }
    .end annotation

    .prologue
    .line 152
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/sns3/SnsApplication;->getAgentMgr()Lcom/sec/android/app/sns3/agent/SnsAgentMgr;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/sns3/agent/SnsAgentMgr;->getCommandMgr()Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;->getCommandMgrHandle()Landroid/os/Handler;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterProfileFeedService;->mCmdHandler:Landroid/os/Handler;

    .line 154
    const/4 v2, 0x1

    iput v2, p0, Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterProfileFeedService;->mSyncState:I

    .line 156
    :try_start_0
    new-instance v0, Lcom/sec/android/app/sns3/agent/sp/googleplus/command/SnsGpCmdGetProfileFeed;

    iget-object v2, p0, Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterProfileFeedService;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    iget-object v3, p0, Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterProfileFeedService;->mCmdHandler:Landroid/os/Handler;

    const-string v4, "me"

    invoke-direct {v0, v2, v3, v4}, Lcom/sec/android/app/sns3/agent/sp/googleplus/command/SnsGpCmdGetProfileFeed;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Handler;Ljava/lang/String;)V

    .line 157
    .local v0, "cmd":Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;
    new-instance v2, Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterProfileFeedService$1;

    invoke-direct {v2, p0}, Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterProfileFeedService$1;-><init>(Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterProfileFeedService;)V

    invoke-virtual {v0, v2}, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->setCommandCallback(Lcom/sec/android/app/sns3/agent/command/ISnsCommandCallback;)Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;

    .line 245
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->send()I

    .line 247
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterProfileFeedService;->suspendSync()V

    .line 249
    iget v2, p0, Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterProfileFeedService;->mSyncState:I

    const/4 v3, -0x1

    if-ne v2, v3, :cond_0

    .line 250
    new-instance v2, Ljava/lang/Exception;

    const-string v3, "[SnsGpSyncAdapterProfileFeedService] updates sync is failed!!!"

    invoke-direct {v2, v3}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 254
    .end local v0    # "cmd":Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;
    :catch_0
    move-exception v1

    .line 255
    .local v1, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 256
    const-string v2, "SnsGpSync"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SnsGpSyncAdapterProfileFeedService : EXCEPTION !!! "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 258
    iget-object v2, p0, Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterProfileFeedService;->mSyncResult:Landroid/content/SyncResult;

    iget-object v2, v2, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    const-wide/16 v4, 0x1

    iput-wide v4, v2, Landroid/content/SyncStats;->numConflictDetectedExceptions:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 262
    invoke-direct {p0}, Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterProfileFeedService;->invokeBroadcast()V

    .line 264
    const-string v2, "SnsGpSync"

    const-string v3, "***************** SnsGpSyncAdapterProfileFeedService : performSync - FINISHED !!! *****************"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 269
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_0
    return-void

    .line 262
    .restart local v0    # "cmd":Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterProfileFeedService;->invokeBroadcast()V

    .line 264
    const-string v2, "SnsGpSync"

    const-string v3, "***************** SnsGpSyncAdapterProfileFeedService : performSync - FINISHED !!! *****************"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 262
    .end local v0    # "cmd":Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;
    :catchall_0
    move-exception v2

    invoke-direct {p0}, Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterProfileFeedService;->invokeBroadcast()V

    .line 264
    const-string v3, "SnsGpSync"

    const-string v4, "***************** SnsGpSyncAdapterProfileFeedService : performSync - FINISHED !!! *****************"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    throw v2
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 120
    const-string v0, "SnsGpSync"

    const-string v1, "***************** SnsGpSyncAdapterProfileFeedService : onBind !!! *****************"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 123
    iget-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterProfileFeedService;->mSyncAdapter:Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterProfileFeedService$SyncAdapterImpl;

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterProfileFeedService$SyncAdapterImpl;->getSyncAdapterBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 107
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 109
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/SnsApplication;->getSvcMgr()Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterProfileFeedService;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    .line 111
    iget-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterProfileFeedService;->mSyncAdapter:Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterProfileFeedService$SyncAdapterImpl;

    if-nez v0, :cond_0

    .line 112
    new-instance v0, Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterProfileFeedService$SyncAdapterImpl;

    invoke-direct {v0, p0, p0}, Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterProfileFeedService$SyncAdapterImpl;-><init>(Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterProfileFeedService;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterProfileFeedService;->mSyncAdapter:Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterProfileFeedService$SyncAdapterImpl;

    .line 115
    :cond_0
    return-void
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 129
    const/4 v0, 0x0

    .line 130
    .local v0, "bActive":Z
    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterProfileFeedService;->mAccount:Landroid/accounts/Account;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterProfileFeedService;->mAuthority:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 131
    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterProfileFeedService;->mAccount:Landroid/accounts/Account;

    iget-object v2, p0, Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterProfileFeedService;->mAuthority:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/content/ContentResolver;->isSyncActive(Landroid/accounts/Account;Ljava/lang/String;)Z

    move-result v0

    .line 134
    :cond_0
    const-string v1, "SnsGpSync"

    const-string v2, "***************** SnsGpSyncAdapterProfileFeedService : onUnbind !!! *****************"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 137
    if-eqz v0, :cond_2

    iget v1, p0, Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterProfileFeedService;->mSyncState:I

    const/4 v2, 0x1

    if-eq v1, v2, :cond_1

    iget v1, p0, Lcom/sec/android/app/sns3/sync/sp/googleplus/SnsGpSyncAdapterProfileFeedService;->mSyncState:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_2

    .line 139
    :cond_1
    const-string v1, "SnsGpSync"

    const-string v2, "SnsGpSyncAdapterProfileFeedService : onUnbind : SYNC ERROR : performSync was stopped by forced abort or pending problem!!!"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 145
    :goto_0
    invoke-super {p0, p1}, Landroid/app/Service;->onUnbind(Landroid/content/Intent;)Z

    move-result v1

    return v1

    .line 143
    :cond_2
    const-string v1, "SnsGpSync"

    const-string v2, "SnsGpSyncAdapterProfileFeedService : onUnbind : COMPLETE STATE!!!"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public declared-synchronized resumeSync()V
    .locals 1

    .prologue
    .line 280
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->notify()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 281
    monitor-exit p0

    return-void

    .line 280
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized suspendSync()V
    .locals 2

    .prologue
    .line 273
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 277
    :goto_0
    monitor-exit p0

    return-void

    .line 274
    :catch_0
    move-exception v0

    .line 275
    .local v0, "e":Ljava/lang/InterruptedException;
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 273
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method
