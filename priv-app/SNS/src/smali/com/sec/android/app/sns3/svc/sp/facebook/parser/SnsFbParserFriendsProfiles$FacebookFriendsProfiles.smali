.class public interface abstract Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserFriendsProfiles$FacebookFriendsProfiles;
.super Ljava/lang/Object;
.source "SnsFbParserFriendsProfiles.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserFriendsProfiles;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "FacebookFriendsProfiles"
.end annotation


# static fields
.field public static final BIRTHDAY_DATE:Ljava/lang/String; = "birthday_date"

.field public static final CURRENT_LOCATION:Ljava/lang/String; = "current_location"

.field public static final DATA:Ljava/lang/String; = "data"

.field public static final DEGREE:Ljava/lang/String; = "degree"

.field public static final EMAIL:Ljava/lang/String; = "email"

.field public static final EMPLOYER:Ljava/lang/String; = "employer"

.field public static final END_DATE:Ljava/lang/String; = "end_date"

.field public static final FROM:Ljava/lang/String; = "from"

.field public static final GENDER:Ljava/lang/String; = "sex"

.field public static final HOMETOWN_LOCATION:Ljava/lang/String; = "hometown_location"

.field public static final ID:Ljava/lang/String; = "uid"

.field public static final ID1:Ljava/lang/String; = "id"

.field public static final INTERESTS:Ljava/lang/String; = "interests"

.field public static final LOCALE:Ljava/lang/String; = "locale"

.field public static final LOCATION:Ljava/lang/String; = "location"

.field public static final NAME:Ljava/lang/String; = "name"

.field public static final ONLINE_PRESENCE:Ljava/lang/String; = "online_presence"

.field public static final PIC:Ljava/lang/String; = "pic"

.field public static final POSITION:Ljava/lang/String; = "position"

.field public static final RELATIONSHIP_STATUS:Ljava/lang/String; = "relationship_status"

.field public static final SCHOOL:Ljava/lang/String; = "school"

.field public static final START_DATE:Ljava/lang/String; = "start_date"

.field public static final STATUS:Ljava/lang/String; = "status"

.field public static final STATUS_COMMENT_COUNT:Ljava/lang/String; = "comment_count"

.field public static final STATUS_MESSAGE:Ljava/lang/String; = "message"

.field public static final STATUS_TIME:Ljava/lang/String; = "time"

.field public static final UPDATE_TIME:Ljava/lang/String; = "profile_update_time"

.field public static final WORK:Ljava/lang/String; = "work"

.field public static final YEAR:Ljava/lang/String; = "year"
