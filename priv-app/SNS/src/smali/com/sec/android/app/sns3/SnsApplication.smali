.class public Lcom/sec/android/app/sns3/SnsApplication;
.super Landroid/app/Application;
.source "SnsApplication.java"


# static fields
.field public static final TAG:Ljava/lang/String; = "SNS"

.field private static mInstance:Lcom/sec/android/app/sns3/SnsApplication;


# instance fields
.field private mAgentMgr:Lcom/sec/android/app/sns3/agent/SnsAgentMgr;

.field private mFeatureMgr:Lcom/sec/android/app/sns3/SnsFeatureManager;

.field private mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/sns3/SnsApplication;->mInstance:Lcom/sec/android/app/sns3/SnsApplication;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Landroid/app/Application;-><init>()V

    return-void
.end method

.method public static getInstance()Lcom/sec/android/app/sns3/SnsApplication;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 38
    sget-object v0, Lcom/sec/android/app/sns3/SnsApplication;->mInstance:Lcom/sec/android/app/sns3/SnsApplication;

    if-nez v0, :cond_0

    .line 39
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "SnsApplication is NOT created !!!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 41
    :cond_0
    sget-object v0, Lcom/sec/android/app/sns3/SnsApplication;->mInstance:Lcom/sec/android/app/sns3/SnsApplication;

    return-object v0
.end method


# virtual methods
.method public getAgentMgr()Lcom/sec/android/app/sns3/agent/SnsAgentMgr;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/app/sns3/SnsApplication;->mAgentMgr:Lcom/sec/android/app/sns3/agent/SnsAgentMgr;

    return-object v0
.end method

.method public getFeatureMgr()Lcom/sec/android/app/sns3/SnsFeatureManager;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/android/app/sns3/SnsApplication;->mFeatureMgr:Lcom/sec/android/app/sns3/SnsFeatureManager;

    return-object v0
.end method

.method public getSvcMgr()Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/app/sns3/SnsApplication;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 47
    invoke-super {p0}, Landroid/app/Application;->onCreate()V

    .line 49
    sput-object p0, Lcom/sec/android/app/sns3/SnsApplication;->mInstance:Lcom/sec/android/app/sns3/SnsApplication;

    .line 51
    new-instance v0, Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    invoke-direct {v0}, Lcom/sec/android/app/sns3/svc/SnsSvcMgr;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/sns3/SnsApplication;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    .line 52
    new-instance v0, Lcom/sec/android/app/sns3/agent/SnsAgentMgr;

    invoke-direct {v0, p0}, Lcom/sec/android/app/sns3/agent/SnsAgentMgr;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/sns3/SnsApplication;->mAgentMgr:Lcom/sec/android/app/sns3/agent/SnsAgentMgr;

    .line 53
    new-instance v0, Lcom/sec/android/app/sns3/SnsFeatureManager;

    invoke-direct {v0, p0}, Lcom/sec/android/app/sns3/SnsFeatureManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/sns3/SnsApplication;->mFeatureMgr:Lcom/sec/android/app/sns3/SnsFeatureManager;

    .line 56
    iget-object v0, p0, Lcom/sec/android/app/sns3/SnsApplication;->mFeatureMgr:Lcom/sec/android/app/sns3/SnsFeatureManager;

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/SnsFeatureManager;->checkAccounts()V

    .line 57
    return-void
.end method

.method public onTerminate()V
    .locals 0

    .prologue
    .line 61
    invoke-super {p0}, Landroid/app/Application;->onTerminate()V

    .line 62
    return-void
.end method
