.class public Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserBoolean;
.super Ljava/lang/Object;
.source "SnsFbParserBoolean.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseBoolean;
    .locals 2
    .param p0, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 27
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseBoolean;

    invoke-direct {v0}, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseBoolean;-><init>()V

    .line 30
    .local v0, "bool":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseBoolean;
    if-eqz p0, :cond_0

    .line 31
    :try_start_0
    iput-object p0, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseBoolean;->mBeSuccess:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 37
    :cond_0
    :goto_0
    return-object v0

    .line 33
    :catch_0
    move-exception v1

    .line 34
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
