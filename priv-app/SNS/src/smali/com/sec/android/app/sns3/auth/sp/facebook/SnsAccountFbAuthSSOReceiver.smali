.class public Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOReceiver;
.super Landroid/content/BroadcastReceiver;
.source "SnsAccountFbAuthSSOReceiver.java"


# static fields
.field private static final ACCOUNT_PREFERENCE_NAME:Ljava/lang/String; = "FB_account"

.field private static final APP_ACCOUNT_PREFERENCE_KEY:Ljava/lang/String; = "app_account"

.field private static final FB_NOTI_INTERVAL:J = 0xf731400L

.field public static final SNS_ACCOUNT_CHECK:Ljava/lang/String; = "com.sec.android.app.sns3.auth.ACTION_CHECK_ACCOUNT"

.field private static final SNS_ACCOUNT_PREFERENCE_KEY:Ljava/lang/String; = "sns_account"

.field public static final SNS_LOGIN_FACEBOOK_NOTIFICATION:Ljava/lang/String; = "com.sec.android.app.sns3.auth.sp.facebook.SNS_LOGIN_FACEBOOK_NOTIFICATION"

.field private static final TAG:Ljava/lang/String; = "FbAuthSSOReceiver"


# instance fields
.field private mSharedPref:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method private registerLoginNotification(Landroid/content/Context;)V
    .locals 11
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 180
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 182
    .local v0, "i":Landroid/content/Intent;
    const-class v6, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;

    invoke-virtual {v0, p1, v6}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 186
    const/high16 v6, 0x10000000

    invoke-virtual {v0, v6}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 187
    const/high16 v6, 0x800000

    invoke-virtual {v0, v6}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 188
    const-string v6, "skip_sso_noti"

    invoke-virtual {v0, v6, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 189
    const/16 v6, 0x3e8

    invoke-static {p1, v6, v0, v9}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 192
    .local v1, "intent":Landroid/app/PendingIntent;
    const v6, 0x7f080030

    new-array v7, v10, [Ljava/lang/Object;

    const v8, 0x7f080020

    invoke-virtual {p1, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v9

    invoke-virtual {p1, v6, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 194
    .local v4, "ticker":Ljava/lang/String;
    const v6, 0x7f08000a

    invoke-virtual {p1, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 196
    .local v5, "title":Ljava/lang/String;
    new-instance v2, Landroid/app/Notification$Builder;

    invoke-direct {v2, p1}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    .line 197
    .local v2, "noti":Landroid/app/Notification$Builder;
    invoke-virtual {v2, v5}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    .line 198
    invoke-virtual {v2, v4}, Landroid/app/Notification$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    .line 199
    invoke-virtual {v2, v4}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    .line 200
    invoke-virtual {v2, v1}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    .line 201
    invoke-virtual {v2, v9}, Landroid/app/Notification$Builder;->setAutoCancel(Z)Landroid/app/Notification$Builder;

    .line 202
    new-instance v6, Landroid/app/Notification$BigTextStyle;

    invoke-direct {v6}, Landroid/app/Notification$BigTextStyle;-><init>()V

    invoke-virtual {v6, v4}, Landroid/app/Notification$BigTextStyle;->bigText(Ljava/lang/CharSequence;)Landroid/app/Notification$BigTextStyle;

    move-result-object v6

    invoke-virtual {v2, v6}, Landroid/app/Notification$Builder;->setStyle(Landroid/app/Notification$Style;)Landroid/app/Notification$Builder;

    .line 203
    invoke-virtual {v2, v10}, Landroid/app/Notification$Builder;->setDefaults(I)Landroid/app/Notification$Builder;

    .line 204
    const v6, 0x7f020020

    invoke-virtual {v2, v6}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    .line 206
    const-string v6, "notification"

    invoke-virtual {p1, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/NotificationManager;

    .line 209
    .local v3, "notiMgr":Landroid/app/NotificationManager;
    const/16 v6, 0x44c

    invoke-virtual {v2}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v7

    invoke-virtual {v3, v6, v7}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 213
    return-void
.end method

.method private startFbLoginNotiAlarm(Landroid/content/Context;)V
    .locals 11
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const-wide/32 v4, 0xf731400

    const/4 v10, 0x0

    .line 162
    const-string v1, "alarm"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 165
    .local v0, "notiAlarmManager":Landroid/app/AlarmManager;
    new-instance v7, Landroid/content/Intent;

    const-string v1, "com.sec.android.app.sns3.auth.sp.facebook.SNS_LOGIN_FACEBOOK_NOTIFICATION"

    invoke-direct {v7, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 166
    .local v7, "notiIntent":Landroid/content/Intent;
    invoke-static {p1, v10, v7, v10}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v6

    .line 168
    .local v6, "pendingIntent":Landroid/app/PendingIntent;
    if-eqz v0, :cond_0

    .line 169
    invoke-virtual {v0, v6}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 171
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    .line 172
    .local v8, "currnetTime":J
    add-long v2, v8, v4

    .line 174
    .local v2, "triggerAtTime":J
    const/4 v1, 0x1

    invoke-virtual/range {v0 .. v6}, Landroid/app/AlarmManager;->setRepeating(IJJLandroid/app/PendingIntent;)V

    .line 177
    .end local v2    # "triggerAtTime":J
    .end local v8    # "currnetTime":J
    :cond_0
    return-void
.end method

.method private storeAccountStatus(II)V
    .locals 2
    .param p1, "snsAccount"    # I
    .param p2, "appAccount"    # I

    .prologue
    .line 154
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOReceiver;->mSharedPref:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 155
    .local v0, "prefEditor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "sns_account"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 156
    const-string v1, "app_account"

    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 157
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 158
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 16
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 58
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    .line 60
    .local v3, "action":Ljava/lang/String;
    const-string v12, "FB_account"

    const/4 v13, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v13}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v12

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOReceiver;->mSharedPref:Landroid/content/SharedPreferences;

    .line 62
    const-string v12, "FbAuthSSOReceiver"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Facebook SSO Receiver : "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 64
    invoke-static/range {p1 .. p1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v12

    const-string v13, "com.sec.android.app.sns3.facebook"

    invoke-virtual {v12, v13}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v11

    .line 67
    .local v11, "sns3Accounts":[Landroid/accounts/Account;
    invoke-static/range {p1 .. p1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v12

    const-string v13, "com.facebook.auth.login"

    invoke-virtual {v12, v13}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v4

    .line 70
    .local v4, "appAccounts":[Landroid/accounts/Account;
    const-string v12, "notification"

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/app/NotificationManager;

    .line 73
    .local v9, "notiMgr":Landroid/app/NotificationManager;
    const-string v12, "com.sec.android.app.sns3.auth.ACTION_CHECK_ACCOUNT"

    invoke-virtual {v12, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_1

    .line 74
    array-length v12, v11

    array-length v13, v4

    move-object/from16 v0, p0

    invoke-direct {v0, v12, v13}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOReceiver;->storeAccountStatus(II)V

    .line 151
    :cond_0
    :goto_0
    return-void

    .line 75
    :cond_1
    const-string v12, "com.sec.android.app.sns3.auth.sp.facebook.SNS_LOGIN_FACEBOOK_NOTIFICATION"

    invoke-virtual {v12, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 76
    array-length v12, v4

    if-lez v12, :cond_0

    .line 77
    array-length v12, v11

    if-nez v12, :cond_0

    .line 78
    invoke-direct/range {p0 .. p1}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOReceiver;->registerLoginNotification(Landroid/content/Context;)V

    .line 79
    invoke-static {}, Lcom/sec/android/app/sns3/SnsFeatureManager;->isVerizonDevice()Z

    move-result v12

    if-nez v12, :cond_0

    goto :goto_0

    .line 84
    :cond_2
    const-string v12, "android.accounts.LOGIN_ACCOUNTS_CHANGED"

    invoke-virtual {v12, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_0

    .line 85
    const-string v12, "operation"

    move-object/from16 v0, p2

    invoke-virtual {v0, v12}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 86
    .local v10, "operation":Ljava/lang/String;
    const/4 v5, 0x0

    .line 88
    .local v5, "changedAccountType":Ljava/lang/String;
    if-eqz v10, :cond_8

    .line 89
    const-string v12, "account"

    move-object/from16 v0, p2

    invoke-virtual {v0, v12}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/accounts/Account;

    .line 90
    .local v1, "account":Landroid/accounts/Account;
    const-string v12, "accounts"

    move-object/from16 v0, p2

    invoke-virtual {v0, v12}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    .line 91
    .local v2, "accounts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/accounts/Account;>;"
    if-nez v1, :cond_3

    if-eqz v2, :cond_3

    .line 92
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_1
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v12

    if-ge v6, v12, :cond_3

    .line 93
    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "account":Landroid/accounts/Account;
    check-cast v1, Landroid/accounts/Account;

    .line 94
    .restart local v1    # "account":Landroid/accounts/Account;
    if-eqz v1, :cond_7

    const-string v12, "com.sec.android.app.sns3.facebook"

    iget-object v13, v1, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_7

    .line 99
    .end local v6    # "i":I
    :cond_3
    if-eqz v1, :cond_4

    .line 100
    iget-object v5, v1, Landroid/accounts/Account;->type:Ljava/lang/String;

    .line 121
    .end local v1    # "account":Landroid/accounts/Account;
    .end local v2    # "accounts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/accounts/Account;>;"
    :cond_4
    :goto_2
    if-eqz v5, :cond_5

    .line 122
    const-string v12, "FbAuthSSOReceiver"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v13, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 125
    :cond_5
    const-string v12, "add"

    invoke-virtual {v12, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_c

    const-string v12, "com.facebook.auth.login"

    invoke-virtual {v12, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_c

    .line 127
    array-length v12, v11

    if-nez v12, :cond_6

    invoke-static {}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->isRunning()Z

    move-result v12

    if-nez v12, :cond_6

    .line 129
    invoke-direct/range {p0 .. p1}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOReceiver;->registerLoginNotification(Landroid/content/Context;)V

    .line 149
    :cond_6
    :goto_3
    array-length v12, v11

    array-length v13, v4

    move-object/from16 v0, p0

    invoke-direct {v0, v12, v13}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOReceiver;->storeAccountStatus(II)V

    goto/16 :goto_0

    .line 92
    .restart local v1    # "account":Landroid/accounts/Account;
    .restart local v2    # "accounts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/accounts/Account;>;"
    .restart local v6    # "i":I
    :cond_7
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 103
    .end local v1    # "account":Landroid/accounts/Account;
    .end local v2    # "accounts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/accounts/Account;>;"
    .end local v6    # "i":I
    :cond_8
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOReceiver;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v13, "app_account"

    const/4 v14, 0x0

    invoke-interface {v12, v13, v14}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v7

    .line 104
    .local v7, "lastAppAccountStatus":I
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOReceiver;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v13, "sns_account"

    const/4 v14, 0x0

    invoke-interface {v12, v13, v14}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v8

    .line 106
    .local v8, "lastSnsAccountStatus":I
    array-length v12, v4

    if-le v12, v7, :cond_9

    .line 107
    const-string v5, "com.facebook.auth.login"

    .line 108
    const-string v10, "add"

    goto :goto_2

    .line 109
    :cond_9
    array-length v12, v4

    if-ge v12, v7, :cond_a

    .line 110
    const-string v5, "com.facebook.auth.login"

    .line 111
    const-string v10, "remove"

    goto :goto_2

    .line 112
    :cond_a
    array-length v12, v11

    if-le v12, v8, :cond_b

    .line 113
    const-string v5, "com.sec.android.app.sns3.facebook"

    .line 114
    const-string v10, "add"

    goto :goto_2

    .line 115
    :cond_b
    array-length v12, v11

    if-ge v12, v8, :cond_4

    .line 116
    const-string v5, "com.sec.android.app.sns3.facebook"

    .line 117
    const-string v10, "remove"

    goto :goto_2

    .line 136
    .end local v7    # "lastAppAccountStatus":I
    .end local v8    # "lastSnsAccountStatus":I
    :cond_c
    const-string v12, "remove"

    invoke-virtual {v12, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_6

    const-string v12, "com.facebook.auth.login"

    invoke-virtual {v12, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_6

    .line 138
    const/16 v12, 0x44c

    invoke-virtual {v9, v12}, Landroid/app/NotificationManager;->cancel(I)V

    .line 139
    const/16 v12, 0x4b0

    invoke-virtual {v9, v12}, Landroid/app/NotificationManager;->cancel(I)V

    .line 142
    array-length v12, v11

    if-lez v12, :cond_6

    .line 143
    invoke-static/range {p1 .. p1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v12

    const/4 v13, 0x0

    aget-object v13, v11, v13

    const/4 v14, 0x0

    const/4 v15, 0x0

    invoke-virtual {v12, v13, v14, v15}, Landroid/accounts/AccountManager;->removeAccount(Landroid/accounts/Account;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    goto :goto_3
.end method
