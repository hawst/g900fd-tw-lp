.class public interface abstract Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserNotification$FacebookNotification$SnsFbThrdQuery;
.super Ljava/lang/Object;
.source "SnsFbParserNotification.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserNotification$FacebookNotification;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "SnsFbThrdQuery"
.end annotation


# static fields
.field public static final FOLDER_ID:Ljava/lang/String; = "folder_id"

.field public static final MESSAGE_COUNT:Ljava/lang/String; = "message_count"

.field public static final RECIPIENTS:Ljava/lang/String; = "recipients"

.field public static final SUBJECT:Ljava/lang/String; = "subject"

.field public static final THREAD_ID:Ljava/lang/String; = "thread_id"

.field public static final UNREAD:Ljava/lang/String; = "unread"

.field public static final UPDATE_TIME:Ljava/lang/String; = "update_time"

.field public static final VIEWER_ID:Ljava/lang/String; = "viewer_id"
