.class public Lcom/sec/android/app/sns3/agent/life/db/SnsLifeDBHelper;
.super Lcom/sec/android/app/sns3/agent/db/SnsDBHelperBase;
.source "SnsLifeDBHelper.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 31
    const-string v0, "snsLifeDB.db"

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/sec/android/app/sns3/agent/db/SnsDBHelperBase;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 32
    return-void
.end method


# virtual methods
.method public createTables(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 52
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 54
    const-string v0, "CREATE TABLE IF NOT EXISTS timeline (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,timestamp_utc TIMESTAMP,time_zone VARCHAR(40),sp_type TEXT,post_id VARCHAR(50),message TEXT,object_type TEXT,media_url VARCHAR(1024),link VARCHAR(1024),location_name TEXT,latitude DOUBLE,longitude DOUBLE,width INTEGER,height INTEGER,album_contents VARCHAR(1024),updated_time TIMESTAMP,category_id VARCHAR(50),category_name TEXT,category_icon TEXT, UNIQUE (post_id));"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 76
    const-string v0, "SnsAgent"

    const-string v1, "DB TABLE(timeline) Has been created."

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 79
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 80
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 81
    return-void
.end method

.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 0
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 36
    invoke-super {p0, p1}, Lcom/sec/android/app/sns3/agent/db/SnsDBHelperBase;->onCreate(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 38
    invoke-virtual {p0, p1}, Lcom/sec/android/app/sns3/agent/life/db/SnsLifeDBHelper;->createTables(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 39
    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 2
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "oldVersion"    # I
    .param p3, "newVersion"    # I

    .prologue
    .line 43
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/app/sns3/agent/db/SnsDBHelperBase;->onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V

    .line 45
    const-string v0, "SnsAgent"

    const-string v1, "onUpgrade()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 47
    invoke-virtual {p0, p1}, Lcom/sec/android/app/sns3/agent/life/db/SnsLifeDBHelper;->upgradeTables(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 48
    return-void
.end method

.method public upgradeTables(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 86
    const-string v0, "DROP TABLE IF EXISTS timeline"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 88
    invoke-virtual {p0, p1}, Lcom/sec/android/app/sns3/agent/life/db/SnsLifeDBHelper;->createTables(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 89
    return-void
.end method

.method public wipeData(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 94
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 96
    :try_start_0
    const-string v0, "DELETE FROM timeline"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 98
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 100
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 102
    return-void

    .line 100
    :catchall_0
    move-exception v0

    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0
.end method
