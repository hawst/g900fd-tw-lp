.class Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity$4$1;
.super Ljava/lang/Object;
.source "SnsAccountFsAuthActivity.java"

# interfaces
.implements Lcom/sec/android/app/sns3/agent/command/ISnsCommandCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity$4;->shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity$4;


# direct methods
.method constructor <init>(Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity$4;)V
    .locals 0

    .prologue
    .line 270
    iput-object p1, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity$4$1;->this$1:Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity$4;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCmdRespond(IZLjava/lang/String;Ljava/util/List;)V
    .locals 10
    .param p1, "cmdID"    # I
    .param p2, "bSuccess"    # Z
    .param p3, "uri"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IZ",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p4, "responseList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;>;"
    const/4 v9, -0x1

    const/4 v8, 0x0

    const/4 v7, 0x0

    .line 276
    const/4 v3, 0x0

    .line 277
    .local v3, "reason":Landroid/os/Bundle;
    const/4 v0, 0x0

    .line 279
    .local v0, "accessToken":Ljava/lang/String;
    if-eqz p4, :cond_0

    .line 280
    invoke-interface {p4}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_0

    .line 281
    invoke-interface {p4, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;

    invoke-virtual {v4}, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;->getReason()Landroid/os/Bundle;

    move-result-object v3

    .line 283
    if-eqz v3, :cond_0

    .line 284
    const-string v4, "access_token"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 290
    :cond_0
    invoke-static {}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->isLoggable()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 291
    # getter for: Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;->access$000()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "SnsAccountFsAuthActivity - onResponse() - access_token = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 296
    :cond_1
    if-eqz v0, :cond_4

    .line 298
    iget-object v4, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity$4$1;->this$1:Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity$4;

    iget-object v4, v4, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity$4;->this$0:Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;

    # getter for: Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;->mRetryLogin:Z
    invoke-static {v4}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;->access$100(Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 299
    iget-object v4, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity$4$1;->this$1:Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity$4;

    iget-object v4, v4, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity$4;->this$0:Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;

    # setter for: Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;->mAccessToken:Ljava/lang/String;
    invoke-static {v4, v0}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;->access$202(Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 301
    iget-object v4, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity$4$1;->this$1:Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity$4;

    iget-object v4, v4, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity$4;->this$0:Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;

    # getter for: Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;->mAppContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;->access$300(Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;)Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v4

    const-string v5, "com.sec.android.app.sns3.foursquare"

    invoke-virtual {v4, v5}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v1

    .line 303
    .local v1, "accounts":[Landroid/accounts/Account;
    if-eqz v1, :cond_2

    array-length v4, v1

    if-lez v4, :cond_2

    .line 304
    iget-object v4, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity$4$1;->this$1:Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity$4;

    iget-object v4, v4, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity$4;->this$0:Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;

    # getter for: Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;->mAppContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;->access$300(Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;)Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v4

    aget-object v5, v1, v7

    iget-object v6, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity$4$1;->this$1:Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity$4;

    iget-object v6, v6, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity$4;->this$0:Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;

    iget-object v6, v6, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;->mAccountManagerRemoveCallback:Landroid/accounts/AccountManagerCallback;

    invoke-virtual {v4, v5, v6, v8}, Landroid/accounts/AccountManager;->removeAccount(Landroid/accounts/Account;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    .line 308
    :cond_2
    iget-object v4, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity$4$1;->this$1:Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity$4;

    iget-object v4, v4, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity$4;->this$0:Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;

    # setter for: Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;->mRetryLogin:Z
    invoke-static {v4, v7}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;->access$102(Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;Z)Z

    .line 358
    .end local v1    # "accounts":[Landroid/accounts/Account;
    :goto_0
    return-void

    .line 311
    :cond_3
    iget-object v4, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity$4$1;->this$1:Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity$4;

    iget-object v4, v4, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity$4;->this$0:Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;

    const-string v5, "0"

    invoke-virtual {v4, v0, v5}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;->setAuthTokenNExpires(Ljava/lang/String;Ljava/lang/String;)Z

    .line 313
    new-instance v2, Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdAuthLogin;

    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/sns3/SnsApplication;->getSvcMgr()Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v4

    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/sns3/SnsApplication;->getAgentMgr()Lcom/sec/android/app/sns3/agent/SnsAgentMgr;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/sns3/agent/SnsAgentMgr;->getCommandMgr()Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;->getCommandMgrHandle()Landroid/os/Handler;

    move-result-object v5

    invoke-direct {v2, v4, v5, v8}, Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdAuthLogin;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Handler;Landroid/os/Bundle;)V

    .line 319
    .local v2, "cmd":Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;
    new-instance v4, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity$4$1$1;

    invoke-direct {v4, p0}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity$4$1$1;-><init>(Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity$4$1;)V

    invoke-virtual {v2, v4}, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->setCommandCallback(Lcom/sec/android/app/sns3/agent/command/ISnsCommandCallback;)Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;

    .line 348
    invoke-virtual {v2}, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->send()I

    goto :goto_0

    .line 354
    .end local v2    # "cmd":Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;
    :cond_4
    # getter for: Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;->access$000()Ljava/lang/String;

    move-result-object v4

    const-string v5, "SnsAccountLiAuthActivity - access token is null!!"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 356
    iget-object v4, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity$4$1;->this$1:Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity$4;

    iget-object v4, v4, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity$4;->this$0:Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;

    invoke-virtual {v4, v9, v9, v8}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;->loginFail(IILjava/lang/String;)V

    goto :goto_0
.end method
