.class public Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;
.super Ljava/lang/Object;
.source "SnsLiFeedItem.java"


# instance fields
.field private comment:Ljava/lang/String;

.field private commentsCount:I

.field private fromIconUrl:Ljava/lang/String;

.field private fromId:Ljava/lang/String;

.field private fromName:Ljava/lang/String;

.field private id:J

.field private likesCount:I

.field private message:Ljava/lang/String;

.field private submittedUrl:Ljava/lang/String;

.field private thumbnailUrl:Ljava/lang/String;

.field private timestamp:J

.field private title:Ljava/lang/String;

.field private updateKey:Ljava/lang/String;

.field private updateType:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$UpdateType;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getComment()Ljava/lang/String;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->comment:Ljava/lang/String;

    return-object v0
.end method

.method public getCommentsCount()I
    .locals 1

    .prologue
    .line 141
    iget v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->commentsCount:I

    return v0
.end method

.method public getFromIconUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->fromIconUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getFromId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->fromId:Ljava/lang/String;

    return-object v0
.end method

.method public getFromName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->fromName:Ljava/lang/String;

    return-object v0
.end method

.method public getId()J
    .locals 2

    .prologue
    .line 37
    iget-wide v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->id:J

    return-wide v0
.end method

.method public getLikesCount()I
    .locals 1

    .prologue
    .line 133
    iget v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->likesCount:I

    return v0
.end method

.method public getMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->message:Ljava/lang/String;

    return-object v0
.end method

.method public getSubmittedUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->submittedUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getThumbnailUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->thumbnailUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getTimestamp()J
    .locals 2

    .prologue
    .line 101
    iget-wide v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->timestamp:J

    return-wide v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->title:Ljava/lang/String;

    return-object v0
.end method

.method public getUpdateKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->updateKey:Ljava/lang/String;

    return-object v0
.end method

.method public getUpdateType()Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$UpdateType;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->updateType:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$UpdateType;

    return-object v0
.end method

.method public setComment(Ljava/lang/String;)V
    .locals 0
    .param p1, "comment"    # Ljava/lang/String;

    .prologue
    .line 121
    iput-object p1, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->comment:Ljava/lang/String;

    .line 122
    return-void
.end method

.method public setCommentsCount(I)V
    .locals 0
    .param p1, "commentsCount"    # I

    .prologue
    .line 145
    iput p1, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->commentsCount:I

    .line 146
    return-void
.end method

.method public setFromIconUrl(Ljava/lang/String;)V
    .locals 0
    .param p1, "fromIconUrl"    # Ljava/lang/String;

    .prologue
    .line 129
    iput-object p1, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->fromIconUrl:Ljava/lang/String;

    .line 130
    return-void
.end method

.method public setFromId(Ljava/lang/String;)V
    .locals 0
    .param p1, "fromId"    # Ljava/lang/String;

    .prologue
    .line 49
    iput-object p1, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->fromId:Ljava/lang/String;

    .line 50
    return-void
.end method

.method public setFromName(Ljava/lang/String;)V
    .locals 0
    .param p1, "fromName"    # Ljava/lang/String;

    .prologue
    .line 57
    iput-object p1, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->fromName:Ljava/lang/String;

    .line 58
    return-void
.end method

.method public setId(J)V
    .locals 1
    .param p1, "id"    # J

    .prologue
    .line 41
    iput-wide p1, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->id:J

    .line 42
    return-void
.end method

.method public setLikesCount(I)V
    .locals 0
    .param p1, "likesCount"    # I

    .prologue
    .line 137
    iput p1, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->likesCount:I

    .line 138
    return-void
.end method

.method public setMessage(Ljava/lang/String;)V
    .locals 0
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 81
    iput-object p1, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->message:Ljava/lang/String;

    .line 82
    return-void
.end method

.method public setSubmittedUrl(Ljava/lang/String;)V
    .locals 0
    .param p1, "submittedUrl"    # Ljava/lang/String;

    .prologue
    .line 89
    iput-object p1, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->submittedUrl:Ljava/lang/String;

    .line 90
    return-void
.end method

.method public setThumbnailUrl(Ljava/lang/String;)V
    .locals 0
    .param p1, "thumbnailUrl"    # Ljava/lang/String;

    .prologue
    .line 97
    iput-object p1, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->thumbnailUrl:Ljava/lang/String;

    .line 98
    return-void
.end method

.method public setTimestamp(J)V
    .locals 1
    .param p1, "timestamp"    # J

    .prologue
    .line 105
    iput-wide p1, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->timestamp:J

    .line 106
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 0
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 73
    iput-object p1, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->title:Ljava/lang/String;

    .line 74
    return-void
.end method

.method public setUpdateKey(Ljava/lang/String;)V
    .locals 0
    .param p1, "updateKey"    # Ljava/lang/String;

    .prologue
    .line 113
    iput-object p1, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->updateKey:Ljava/lang/String;

    .line 114
    return-void
.end method

.method public setUpdateType(Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$UpdateType;)V
    .locals 0
    .param p1, "updateType"    # Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$UpdateType;

    .prologue
    .line 65
    iput-object p1, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->updateType:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$UpdateType;

    .line 66
    return-void
.end method
