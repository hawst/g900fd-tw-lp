.class Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetAccessToken$1;
.super Lcom/sec/android/app/sns3/svc/sp/linkedin/request/SnsLiReqGetAccessToken;
.source "SnsLiCmdGetAccessToken.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetAccessToken;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Handler;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetAccessToken;


# direct methods
.method constructor <init>(Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetAccessToken;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p2, "x0"    # Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    .param p3, "x1"    # Ljava/lang/String;
    .param p4, "x2"    # Ljava/lang/String;

    .prologue
    .line 43
    iput-object p1, p0, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetAccessToken$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetAccessToken;

    invoke-direct {p0, p2, p3, p4}, Lcom/sec/android/app/sns3/svc/sp/linkedin/request/SnsLiReqGetAccessToken;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public onReqRespond(IZIILandroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseAccessToken;)Z
    .locals 3
    .param p1, "reqID"    # I
    .param p2, "bSuccess"    # Z
    .param p3, "httpStatus"    # I
    .param p4, "errorCode"    # I
    .param p5, "reason"    # Landroid/os/Bundle;
    .param p6, "accessToken"    # Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseAccessToken;

    .prologue
    .line 48
    const-string v0, "SnsAgent"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SnsLiCmdGetAccessToken - onResponse() - bSuccess = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 51
    if-eqz p2, :cond_3

    if-eqz p6, :cond_3

    .line 52
    if-nez p5, :cond_0

    .line 53
    new-instance p5, Landroid/os/Bundle;

    .end local p5    # "reason":Landroid/os/Bundle;
    invoke-direct {p5}, Landroid/os/Bundle;-><init>()V

    .line 55
    .restart local p5    # "reason":Landroid/os/Bundle;
    :cond_0
    const-string v0, "access_token"

    iget-object v1, p6, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseAccessToken;->mAccessToken:Ljava/lang/String;

    invoke-virtual {p5, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    iget-object v0, p6, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseAccessToken;->mExpires:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 58
    const-string v0, "expires_in"

    iget-object v1, p6, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseAccessToken;->mExpires:Ljava/lang/String;

    invoke-virtual {p5, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    :goto_0
    invoke-static {}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->isLoggable()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 65
    const-string v0, "SnsAgent"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "-SnsLiReqGetAccessToken - onResponse() - access_token = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p6, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseAccessToken;->mAccessToken:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mExpires = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p6, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseAccessToken;->mExpires:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 76
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetAccessToken$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetAccessToken;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetAccessToken;->setUri(Ljava/lang/String;)V

    .line 77
    iget-object v0, p0, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetAccessToken$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetAccessToken;

    invoke-virtual {v0, p2}, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetAccessToken;->setSuccess(Z)V

    .line 78
    iget-object v0, p0, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetAccessToken$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetAccessToken;

    new-instance v1, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;

    const-string v2, "linkedin"

    invoke-direct {v1, v2, p3, p4, p5}, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;-><init>(Ljava/lang/String;IILandroid/os/Bundle;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetAccessToken;->setResponseList(Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;)Z

    .line 80
    iget-object v0, p0, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetAccessToken$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetAccessToken;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetAccessToken;->receive(Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;)V

    .line 82
    const/4 v0, 0x1

    return v0

    .line 60
    :cond_2
    const-string v0, "expires_in"

    const-string v1, "0"

    invoke-virtual {p5, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 70
    :cond_3
    const-string v0, "SnsAgent"

    const-string v1, " Get AccessToken fail"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 71
    if-eqz p5, :cond_1

    .line 72
    const-string v0, "SnsAgent"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " error message : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "error_message"

    invoke-virtual {p5, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method
