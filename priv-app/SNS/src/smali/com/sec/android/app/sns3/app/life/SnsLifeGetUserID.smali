.class public Lcom/sec/android/app/sns3/app/life/SnsLifeGetUserID;
.super Ljava/lang/Object;
.source "SnsLifeGetUserID.java"


# static fields
.field protected static final MSG_CHECK_RESPOND_COUNT:I = 0x1

.field private static mMainHandler:Landroid/os/Handler;

.field private static mRespondCount:I

.field private static userDetails:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 54
    const/4 v0, 0x0

    sput v0, Lcom/sec/android/app/sns3/app/life/SnsLifeGetUserID;->mRespondCount:I

    .line 56
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/sns3/app/life/SnsLifeGetUserID;->userDetails:Ljava/util/HashMap;

    .line 58
    new-instance v0, Lcom/sec/android/app/sns3/app/life/SnsLifeGetUserID$1;

    invoke-direct {v0}, Lcom/sec/android/app/sns3/app/life/SnsLifeGetUserID$1;-><init>()V

    sput-object v0, Lcom/sec/android/app/sns3/app/life/SnsLifeGetUserID;->mMainHandler:Landroid/os/Handler;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$008()I
    .locals 2

    .prologue
    .line 50
    sget v0, Lcom/sec/android/app/sns3/app/life/SnsLifeGetUserID;->mRespondCount:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/android/app/sns3/app/life/SnsLifeGetUserID;->mRespondCount:I

    return v0
.end method

.method static synthetic access$100(Landroid/content/Context;)V
    .locals 0
    .param p0, "x0"    # Landroid/content/Context;

    .prologue
    .line 50
    invoke-static {p0}, Lcom/sec/android/app/sns3/app/life/SnsLifeGetUserID;->checkRespondCount(Landroid/content/Context;)V

    return-void
.end method

.method private static checkRespondCount(Landroid/content/Context;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 190
    sget-object v0, Lcom/sec/android/app/sns3/app/life/SnsLifeResource;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mRespondCount="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/sec/android/app/sns3/app/life/SnsLifeGetUserID;->mRespondCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "mAvailableSPCount="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->mAvailableSPCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 193
    sget v0, Lcom/sec/android/app/sns3/app/life/SnsLifeGetUserID;->mRespondCount:I

    sget v1, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->mAvailableSPCount:I

    if-lt v0, v1, :cond_0

    .line 194
    const/4 v0, 0x0

    sput v0, Lcom/sec/android/app/sns3/app/life/SnsLifeGetUserID;->mRespondCount:I

    .line 195
    invoke-static {p0}, Lcom/sec/android/app/sns3/app/life/SnsLifeGetUserID;->invokeBroadcast(Landroid/content/Context;)V

    .line 196
    sget-object v0, Lcom/sec/android/app/sns3/app/life/SnsLifeResource;->TAG:Ljava/lang/String;

    const-string v1, "Invoked loggedIn user broadcast"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 198
    :cond_0
    return-void
.end method

.method private static invokeBroadcast(Landroid/content/Context;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 202
    const/4 v1, 0x0

    .line 204
    .local v1, "intent":Landroid/content/Intent;
    const-string v0, "com.sec.android.app.sns.life.ACTION_LIFE_LOGGEDIN_USER_UPDATED"

    .line 206
    .local v0, "action":Ljava/lang/String;
    new-instance v1, Landroid/content/Intent;

    .end local v1    # "intent":Landroid/content/Intent;
    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 207
    .restart local v1    # "intent":Landroid/content/Intent;
    const-string v2, "userIds"

    sget-object v3, Lcom/sec/android/app/sns3/app/life/SnsLifeGetUserID;->userDetails:Ljava/util/HashMap;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 209
    const-string v2, "com.sec.android.app.sns3.permission.RECEIVE_SNS_BROADCAST"

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    .line 211
    return-void
.end method

.method public static loadSnsLifeCmdGetUserID(Landroid/content/Context;)V
    .locals 15
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v14, 0x1

    const/4 v2, 0x0

    .line 79
    invoke-static {}, Lcom/sec/android/app/sns3/SnsFeatureManager;->isChineseModel()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 80
    invoke-static {}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->checkAvailableSPChina()V

    .line 81
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/sec/android/app/sns3/app/life/SnsLifeGetUserID;->userDetails:Ljava/util/HashMap;

    .line 84
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/sns3/agent/sp/qzone/db/SnsQzoneDB$User;->CONTENT_URI:Landroid/net/Uri;

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 86
    .local v11, "qzCursor":Landroid/database/Cursor;
    if-eqz v11, :cond_1

    .line 87
    invoke-interface {v11}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 88
    sget-object v0, Lcom/sec/android/app/sns3/app/life/SnsLifeGetUserID;->userDetails:Ljava/util/HashMap;

    const-string v1, "qzone"

    const-string v3, "user_id"

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 89
    sget-object v0, Lcom/sec/android/app/sns3/app/life/SnsLifeGetUserID;->mMainHandler:Landroid/os/Handler;

    invoke-virtual {v0, v14}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 91
    :cond_0
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 95
    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/sns3/agent/sp/sinaweibo/db/SnsSinaweiboDB$User;->CONTENT_URI:Landroid/net/Uri;

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 98
    .local v12, "swCursor":Landroid/database/Cursor;
    if-eqz v12, :cond_3

    .line 99
    invoke-interface {v12}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 100
    sget-object v0, Lcom/sec/android/app/sns3/app/life/SnsLifeGetUserID;->userDetails:Ljava/util/HashMap;

    const-string v1, "sinaweibo"

    const-string v2, "user_id"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 101
    sget-object v0, Lcom/sec/android/app/sns3/app/life/SnsLifeGetUserID;->mMainHandler:Landroid/os/Handler;

    invoke-virtual {v0, v14}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 103
    :cond_2
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 187
    .end local v11    # "qzCursor":Landroid/database/Cursor;
    .end local v12    # "swCursor":Landroid/database/Cursor;
    :cond_3
    :goto_0
    return-void

    .line 107
    :cond_4
    invoke-static {}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->checkAvailableSP()V

    .line 108
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/sec/android/app/sns3/app/life/SnsLifeGetUserID;->userDetails:Ljava/util/HashMap;

    .line 111
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$User;->CONTENT_URI:Landroid/net/Uri;

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 114
    .local v6, "fbCursor":Landroid/database/Cursor;
    if-eqz v6, :cond_6

    .line 115
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 116
    sget-object v0, Lcom/sec/android/app/sns3/app/life/SnsLifeGetUserID;->userDetails:Ljava/util/HashMap;

    const-string v1, "facebook"

    const-string v3, "user_id"

    invoke-interface {v6, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v6, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 118
    sget-object v0, Lcom/sec/android/app/sns3/app/life/SnsLifeGetUserID;->mMainHandler:Landroid/os/Handler;

    invoke-virtual {v0, v14}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 120
    :cond_5
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 124
    :cond_6
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/sns3/agent/sp/twitter/db/SnsTwitterDB$UserTwBasicInfo;->CONTENT_URI:Landroid/net/Uri;

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v13

    .line 127
    .local v13, "twCursor":Landroid/database/Cursor;
    if-eqz v13, :cond_8

    .line 128
    invoke-interface {v13}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 129
    sget-object v0, Lcom/sec/android/app/sns3/app/life/SnsLifeGetUserID;->userDetails:Ljava/util/HashMap;

    const-string v1, "twitter"

    const-string v3, "user_name"

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 131
    sget-object v0, Lcom/sec/android/app/sns3/app/life/SnsLifeGetUserID;->mMainHandler:Landroid/os/Handler;

    invoke-virtual {v0, v14}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 133
    :cond_7
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    .line 137
    :cond_8
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/sns3/agent/sp/googleplus/db/SnsGooglePlusDB$UserGpBasicInfo;->CONTENT_URI:Landroid/net/Uri;

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 140
    .local v8, "gpCursor":Landroid/database/Cursor;
    if-eqz v8, :cond_a

    .line 141
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 142
    sget-object v0, Lcom/sec/android/app/sns3/app/life/SnsLifeGetUserID;->userDetails:Ljava/util/HashMap;

    const-string v1, "googleplus"

    const-string v3, "user_id"

    invoke-interface {v8, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v8, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 144
    sget-object v0, Lcom/sec/android/app/sns3/app/life/SnsLifeGetUserID;->mMainHandler:Landroid/os/Handler;

    invoke-virtual {v0, v14}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 146
    :cond_9
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 150
    :cond_a
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/sns3/agent/sp/foursquare/db/SnsFourSquareDB$UserFsBasicInfo;->CONTENT_URI:Landroid/net/Uri;

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 153
    .local v7, "fsCursor":Landroid/database/Cursor;
    if-eqz v7, :cond_c

    .line 154
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 155
    sget-object v0, Lcom/sec/android/app/sns3/app/life/SnsLifeGetUserID;->userDetails:Ljava/util/HashMap;

    const-string v1, "foursquare"

    const-string v3, "user_id"

    invoke-interface {v7, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v7, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 157
    sget-object v0, Lcom/sec/android/app/sns3/app/life/SnsLifeGetUserID;->mMainHandler:Landroid/os/Handler;

    invoke-virtual {v0, v14}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 159
    :cond_b
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 163
    :cond_c
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/sns3/agent/sp/instagram/db/SnsInstagramDB$UserInBasicInfo;->CONTENT_URI:Landroid/net/Uri;

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 166
    .local v9, "inCursor":Landroid/database/Cursor;
    if-eqz v9, :cond_e

    .line 167
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 168
    sget-object v0, Lcom/sec/android/app/sns3/app/life/SnsLifeGetUserID;->userDetails:Ljava/util/HashMap;

    const-string v1, "instagram"

    const-string v3, "user_id"

    invoke-interface {v9, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v9, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 170
    sget-object v0, Lcom/sec/android/app/sns3/app/life/SnsLifeGetUserID;->mMainHandler:Landroid/os/Handler;

    invoke-virtual {v0, v14}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 172
    :cond_d
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 176
    :cond_e
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$UserBasicInfo;->CONTENT_URI:Landroid/net/Uri;

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 179
    .local v10, "liCursor":Landroid/database/Cursor;
    if-eqz v10, :cond_3

    .line 180
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 181
    sget-object v0, Lcom/sec/android/app/sns3/app/life/SnsLifeGetUserID;->userDetails:Ljava/util/HashMap;

    const-string v1, "linkedin"

    const-string v2, "id"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 182
    sget-object v0, Lcom/sec/android/app/sns3/app/life/SnsLifeGetUserID;->mMainHandler:Landroid/os/Handler;

    invoke-virtual {v0, v14}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 184
    :cond_f
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0
.end method
