.class Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPhotoStream$1;
.super Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqGetPhotoStream;
.source "SnsFbCmdGetPhotoStream.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPhotoStream;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Handler;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPhotoStream;


# direct methods
.method constructor <init>(Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPhotoStream;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;)V
    .locals 0
    .param p2, "x0"    # Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    .param p3, "x1"    # Ljava/lang/String;

    .prologue
    .line 50
    iput-object p1, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPhotoStream$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPhotoStream;

    invoke-direct {p0, p2, p3}, Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqGetPhotoStream;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public onReqRespond(IZIILandroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhotoStream;)Z
    .locals 7
    .param p1, "reqID"    # I
    .param p2, "bSuccess"    # Z
    .param p3, "httpStatus"    # I
    .param p4, "errorCode"    # I
    .param p5, "reason"    # Landroid/os/Bundle;
    .param p6, "photostream"    # Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhotoStream;

    .prologue
    const/4 v6, 0x0

    .line 55
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 56
    .local v2, "values":Landroid/content/ContentValues;
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/sns3/SnsApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 58
    .local v0, "cr":Landroid/content/ContentResolver;
    sget-object v3, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$PhotoStream;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v3, v6, v6}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 60
    if-eqz p2, :cond_1

    .line 61
    if-eqz p6, :cond_0

    .line 62
    move-object v1, p6

    .line 63
    .local v1, "curPhotoStream":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhotoStream;
    :goto_0
    if-eqz v1, :cond_0

    .line 65
    invoke-virtual {v2}, Landroid/content/ContentValues;->clear()V

    .line 67
    const-string v3, "post_id"

    iget-object v4, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhotoStream;->mPostID:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    const-string v3, "actor_id"

    iget-object v4, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhotoStream;->mActorID:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    const-string v3, "message"

    iget-object v4, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhotoStream;->mMessage:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    const-string v3, "likes_count"

    iget-object v4, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhotoStream;->mLikeCount:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    const-string v3, "comments_count"

    iget-object v4, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhotoStream;->mCommentCount:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    const-string v3, "created_time"

    iget-object v4, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhotoStream;->mCreatedTime:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    const-string v3, "updated_time"

    iget-object v4, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhotoStream;->mUpdatedTime:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    const-string v3, "photo_url"

    iget-object v4, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhotoStream;->mPhotoURL:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    const-string v3, "user_name"

    iget-object v4, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhotoStream;->mName:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    const-string v3, "profile_url"

    iget-object v4, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhotoStream;->mPicSquare:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    sget-object v3, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$PhotoStream;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v3, v2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 79
    iget-object v1, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhotoStream;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhotoStream;

    goto :goto_0

    .line 82
    .end local v1    # "curPhotoStream":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhotoStream;
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPhotoStream$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPhotoStream;

    sget-object v4, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$PhotoStream;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPhotoStream;->setUri(Ljava/lang/String;)V

    .line 89
    :goto_1
    iget-object v3, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPhotoStream$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPhotoStream;

    invoke-virtual {v3, p2}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPhotoStream;->setSuccess(Z)V

    .line 90
    iget-object v3, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPhotoStream$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPhotoStream;

    invoke-virtual {v3, p0}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPhotoStream;->receive(Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;)V

    .line 92
    const/4 v3, 0x1

    return v3

    .line 84
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPhotoStream$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPhotoStream;

    new-instance v4, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;

    const-string v5, "facebook"

    invoke-direct {v4, v5, p3, p4, p5}, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;-><init>(Ljava/lang/String;IILandroid/os/Bundle;)V

    invoke-virtual {v3, v4}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPhotoStream;->setResponseList(Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;)Z

    .line 86
    iget-object v3, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPhotoStream$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPhotoStream;

    invoke-virtual {v3, v6}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPhotoStream;->setUri(Ljava/lang/String;)V

    goto :goto_1
.end method
