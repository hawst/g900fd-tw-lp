.class public Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthSSOActivity;
.super Landroid/accounts/AccountAuthenticatorActivity;
.source "SnsAccountFsAuthSSOActivity.java"


# static fields
.field private static final FOURSQUARE_MARKET_URI:Ljava/lang/String; = "market://details?id=com.joelapenna.foursquared"

.field private static final GOOGLE_ACCOUNT_TYPE:Ljava/lang/String; = "com.google"

.field private static final REQUEST_CODE_FS_CONNECT:I = 0x3e9

.field private static final REQUEST_CODE_FS_GET_TOKEN:I = 0x3ea

.field private static final REQUEST_SYNC_INTERVAL:I = 0x3e8

.field private static TAG:Ljava/lang/String;

.field private static mInstanceCount:I


# instance fields
.field private mAccountManagerCallback:Landroid/accounts/AccountManagerCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/accounts/AccountManagerCallback",
            "<",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation
.end field

.field private mAppContext:Landroid/content/Context;

.field private final mHandler:Landroid/os/Handler;

.field private mRetryLogin:Z

.field private mSkipSSONoti:Z

.field private mSkipSyncSetup:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 64
    const-string v0, "FoursquareSSOActivity"

    sput-object v0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthSSOActivity;->TAG:Ljava/lang/String;

    .line 73
    const/4 v0, 0x0

    sput v0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthSSOActivity;->mInstanceCount:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 62
    invoke-direct {p0}, Landroid/accounts/AccountAuthenticatorActivity;-><init>()V

    .line 74
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthSSOActivity;->mAppContext:Landroid/content/Context;

    .line 75
    iput-boolean v1, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthSSOActivity;->mRetryLogin:Z

    .line 76
    iput-boolean v1, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthSSOActivity;->mSkipSSONoti:Z

    .line 77
    iput-boolean v1, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthSSOActivity;->mSkipSyncSetup:Z

    .line 78
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthSSOActivity;->mHandler:Landroid/os/Handler;

    .line 80
    new-instance v0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthSSOActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthSSOActivity$1;-><init>(Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthSSOActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthSSOActivity;->mAccountManagerCallback:Landroid/accounts/AccountManagerCallback;

    return-void
.end method

.method private FoursquareSSOAuth()V
    .locals 3

    .prologue
    .line 205
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthSSOActivity;->mAppContext:Landroid/content/Context;

    sget-object v2, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFourSquare;->CLIENT_ID:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/foursquare/android/nativeoauth/FoursquareOAuth;->getConnectIntent(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 206
    .local v0, "authIntent":Landroid/content/Intent;
    invoke-static {v0}, Lcom/foursquare/android/nativeoauth/FoursquareOAuth;->isPlayStoreIntent(Landroid/content/Intent;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 207
    invoke-direct {p0}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthSSOActivity;->isLoggedInGoogle()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 208
    invoke-virtual {p0, v0}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthSSOActivity;->startActivity(Landroid/content/Intent;)V

    .line 213
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthSSOActivity;->finish()V

    .line 217
    :goto_1
    return-void

    .line 210
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthSSOActivity;->showAlertDialog()V

    goto :goto_0

    .line 215
    :cond_1
    const/16 v1, 0x3e9

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthSSOActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_1
.end method

.method static synthetic access$000(Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthSSOActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthSSOActivity;

    .prologue
    .line 62
    iget-boolean v0, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthSSOActivity;->mSkipSyncSetup:Z

    return v0
.end method

.method static synthetic access$100()Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    sget-object v0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthSSOActivity;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthSSOActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthSSOActivity;

    .prologue
    .line 62
    iget-boolean v0, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthSSOActivity;->mRetryLogin:Z

    return v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthSSOActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthSSOActivity;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Ljava/lang/String;

    .prologue
    .line 62
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthSSOActivity;->loginSuccess(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthSSOActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthSSOActivity;

    .prologue
    .line 62
    invoke-direct {p0}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthSSOActivity;->registerRetryNotification()V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthSSOActivity;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthSSOActivity;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthSSOActivity;->mAppContext:Landroid/content/Context;

    return-object v0
.end method

.method private getAccessToken(ILandroid/content/Intent;)V
    .locals 9
    .param p1, "resultCode"    # I
    .param p2, "data"    # Landroid/content/Intent;

    .prologue
    .line 287
    const/4 v4, 0x0

    .line 288
    .local v4, "exception":Ljava/lang/Exception;
    const/4 v0, 0x0

    .line 290
    .local v0, "accessToken":Ljava/lang/String;
    invoke-static {p1, p2}, Lcom/foursquare/android/nativeoauth/FoursquareOAuth;->getTokenFromResult(ILandroid/content/Intent;)Lcom/foursquare/android/nativeoauth/model/AccessTokenResponse;

    move-result-object v5

    .line 292
    .local v5, "tokenResponse":Lcom/foursquare/android/nativeoauth/model/AccessTokenResponse;
    if-eqz v5, :cond_0

    .line 293
    invoke-virtual {v5}, Lcom/foursquare/android/nativeoauth/model/AccessTokenResponse;->getException()Ljava/lang/Exception;

    move-result-object v4

    .line 294
    invoke-virtual {v5}, Lcom/foursquare/android/nativeoauth/model/AccessTokenResponse;->getAccessToken()Ljava/lang/String;

    move-result-object v0

    .line 297
    :cond_0
    if-nez v4, :cond_1

    if-eqz v0, :cond_1

    .line 298
    const-string v6, "0"

    invoke-direct {p0, v0, v6}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthSSOActivity;->setAuthTokenNExpires(Ljava/lang/String;Ljava/lang/String;)Z

    .line 300
    new-instance v1, Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdAuthLogin;

    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/sns3/SnsApplication;->getSvcMgr()Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v6

    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/sns3/SnsApplication;->getAgentMgr()Lcom/sec/android/app/sns3/agent/SnsAgentMgr;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/sns3/agent/SnsAgentMgr;->getCommandMgr()Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;->getCommandMgrHandle()Landroid/os/Handler;

    move-result-object v7

    const/4 v8, 0x0

    invoke-direct {v1, v6, v7, v8}, Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdAuthLogin;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Handler;Landroid/os/Bundle;)V

    .line 303
    .local v1, "command":Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;
    new-instance v6, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthSSOActivity$2;

    invoke-direct {v6, p0}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthSSOActivity$2;-><init>(Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthSSOActivity;)V

    invoke-virtual {v1, v6}, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->setCommandCallback(Lcom/sec/android/app/sns3/agent/command/ISnsCommandCallback;)Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;

    .line 333
    invoke-virtual {v1}, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->send()I

    .line 348
    .end local v1    # "command":Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;
    .end local v4    # "exception":Ljava/lang/Exception;
    :goto_0
    return-void

    .line 335
    .restart local v4    # "exception":Ljava/lang/Exception;
    :cond_1
    instance-of v6, v4, Lcom/foursquare/android/nativeoauth/FoursquareOAuthException;

    if-eqz v6, :cond_2

    move-object v6, v4

    .line 337
    check-cast v6, Lcom/foursquare/android/nativeoauth/FoursquareOAuthException;

    invoke-virtual {v6}, Lcom/foursquare/android/nativeoauth/FoursquareOAuthException;->getMessage()Ljava/lang/String;

    move-result-object v3

    .line 338
    .local v3, "errorMessage":Ljava/lang/String;
    check-cast v4, Lcom/foursquare/android/nativeoauth/FoursquareOAuthException;

    .end local v4    # "exception":Ljava/lang/Exception;
    invoke-virtual {v4}, Lcom/foursquare/android/nativeoauth/FoursquareOAuthException;->getErrorCode()Ljava/lang/String;

    move-result-object v2

    .line 339
    .local v2, "errorCode":Ljava/lang/String;
    sget-object v6, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthSSOActivity;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "FoursquareOAuthException "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ","

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 345
    .end local v2    # "errorCode":Ljava/lang/String;
    .end local v3    # "errorMessage":Ljava/lang/String;
    :goto_1
    invoke-direct {p0}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthSSOActivity;->registerRetryNotification()V

    .line 346
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthSSOActivity;->finish()V

    goto :goto_0

    .line 342
    .restart local v4    # "exception":Ljava/lang/Exception;
    :cond_2
    sget-object v6, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthSSOActivity;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "getAccessToken() error : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private getCode(ILandroid/content/Intent;)V
    .locals 8
    .param p1, "resultCode"    # I
    .param p2, "data"    # Landroid/content/Intent;

    .prologue
    .line 242
    invoke-static {p1, p2}, Lcom/foursquare/android/nativeoauth/FoursquareOAuth;->getAuthCodeFromResult(ILandroid/content/Intent;)Lcom/foursquare/android/nativeoauth/model/AuthCodeResponse;

    move-result-object v1

    .line 243
    .local v1, "codeResponse":Lcom/foursquare/android/nativeoauth/model/AuthCodeResponse;
    invoke-virtual {v1}, Lcom/foursquare/android/nativeoauth/model/AuthCodeResponse;->getException()Ljava/lang/Exception;

    move-result-object v4

    .line 245
    .local v4, "exception":Ljava/lang/Exception;
    if-nez v4, :cond_0

    .line 247
    invoke-virtual {v1}, Lcom/foursquare/android/nativeoauth/model/AuthCodeResponse;->getCode()Ljava/lang/String;

    move-result-object v0

    .line 248
    .local v0, "code":Ljava/lang/String;
    invoke-direct {p0, v0}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthSSOActivity;->performTokenExchange(Ljava/lang/String;)V

    .line 278
    .end local v0    # "code":Ljava/lang/String;
    .end local v4    # "exception":Ljava/lang/Exception;
    :goto_0
    return-void

    .line 251
    .restart local v4    # "exception":Ljava/lang/Exception;
    :cond_0
    instance-of v5, v4, Lcom/foursquare/android/nativeoauth/FoursquareCancelException;

    if-eqz v5, :cond_2

    .line 253
    sget-object v5, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthSSOActivity;->TAG:Ljava/lang/String;

    const-string v6, "CANCEL"

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 272
    .end local v4    # "exception":Ljava/lang/Exception;
    :goto_1
    iget-boolean v5, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthSSOActivity;->mSkipSSONoti:Z

    if-nez v5, :cond_1

    .line 273
    new-instance v5, Landroid/content/Intent;

    const-string v6, "com.sec.android.app.sns3.auth.sp.foursquare.SNS_LOGIN_FOURSQUARE_NOTIFICATION"

    invoke-direct {v5, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v5}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthSSOActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 276
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthSSOActivity;->finish()V

    goto :goto_0

    .line 254
    .restart local v4    # "exception":Ljava/lang/Exception;
    :cond_2
    instance-of v5, v4, Lcom/foursquare/android/nativeoauth/FoursquareDenyException;

    if-eqz v5, :cond_3

    .line 256
    sget-object v5, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthSSOActivity;->TAG:Ljava/lang/String;

    const-string v6, "DENY"

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 257
    :cond_3
    instance-of v5, v4, Lcom/foursquare/android/nativeoauth/FoursquareOAuthException;

    if-eqz v5, :cond_4

    .line 259
    invoke-virtual {v4}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    .line 260
    .local v3, "errorMessage":Ljava/lang/String;
    check-cast v4, Lcom/foursquare/android/nativeoauth/FoursquareOAuthException;

    .end local v4    # "exception":Ljava/lang/Exception;
    invoke-virtual {v4}, Lcom/foursquare/android/nativeoauth/FoursquareOAuthException;->getErrorCode()Ljava/lang/String;

    move-result-object v2

    .line 261
    .local v2, "errorCode":Ljava/lang/String;
    sget-object v5, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthSSOActivity;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "ERROR"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ","

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 262
    .end local v2    # "errorCode":Ljava/lang/String;
    .end local v3    # "errorMessage":Ljava/lang/String;
    .restart local v4    # "exception":Ljava/lang/Exception;
    :cond_4
    instance-of v5, v4, Lcom/foursquare/android/nativeoauth/FoursquareUnsupportedVersionException;

    if-eqz v5, :cond_5

    .line 264
    sget-object v5, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthSSOActivity;->TAG:Ljava/lang/String;

    const-string v6, "UNSUPPORTED"

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 265
    :cond_5
    instance-of v5, v4, Lcom/foursquare/android/nativeoauth/FoursquareInvalidRequestException;

    if-eqz v5, :cond_6

    .line 267
    sget-object v5, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthSSOActivity;->TAG:Ljava/lang/String;

    const-string v6, "INVALID"

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 269
    :cond_6
    sget-object v5, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthSSOActivity;->TAG:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private isLoggedInGoogle()Z
    .locals 3

    .prologue
    .line 220
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    const-string v2, "com.google"

    invoke-virtual {v1, v2}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 222
    .local v0, "account":[Landroid/accounts/Account;
    if-eqz v0, :cond_0

    array-length v1, v0

    if-lez v1, :cond_0

    .line 223
    const/4 v1, 0x1

    .line 225
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isRunning()Z
    .locals 1

    .prologue
    .line 201
    sget v0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthSSOActivity;->mInstanceCount:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private loginSuccess(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 10
    .param p1, "userName"    # Ljava/lang/String;
    .param p2, "id"    # Ljava/lang/String;
    .param p3, "email"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 361
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 362
    .local v4, "options":Landroid/os/Bundle;
    invoke-static {}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->isLoggable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 363
    sget-object v0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthSSOActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "LoginSuccess : addAccount() :  userName = "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 366
    :cond_0
    if-eqz p3, :cond_1

    .line 367
    const-string v0, "username"

    invoke-virtual {v4, v0, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 372
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthSSOActivity;->mAppContext:Landroid/content/Context;

    if-nez v0, :cond_2

    .line 373
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthSSOActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    const-string v1, "com.sec.android.app.sns3.foursquare"

    move-object v3, v2

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Landroid/accounts/AccountManager;->addAccount(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Landroid/os/Bundle;Landroid/app/Activity;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    .line 380
    :goto_1
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/SnsApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    .line 381
    .local v8, "cr":Landroid/content/ContentResolver;
    new-instance v9, Landroid/content/ContentValues;

    invoke-direct {v9}, Landroid/content/ContentValues;-><init>()V

    .line 382
    .local v9, "values":Landroid/content/ContentValues;
    sget-object v0, Lcom/sec/android/app/sns3/agent/sp/foursquare/db/SnsFourSquareDB$UserFsBasicInfo;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v8, v0, v2, v2}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 383
    invoke-virtual {v9}, Landroid/content/ContentValues;->clear()V

    .line 385
    const-string v0, "user_id"

    invoke-virtual {v9, v0, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 386
    const-string v0, "formatted_name"

    invoke-virtual {v9, v0, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 387
    sget-object v0, Lcom/sec/android/app/sns3/agent/sp/foursquare/db/SnsFourSquareDB$UserFsBasicInfo;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v8, v0, v9}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 388
    return-void

    .line 369
    .end local v8    # "cr":Landroid/content/ContentResolver;
    .end local v9    # "values":Landroid/content/ContentValues;
    :cond_1
    const-string v0, "username"

    invoke-virtual {v4, v0, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 376
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthSSOActivity;->mAppContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    const-string v1, "com.sec.android.app.sns3.foursquare"

    iget-object v6, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthSSOActivity;->mAccountManagerCallback:Landroid/accounts/AccountManagerCallback;

    move-object v3, v2

    move-object v5, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Landroid/accounts/AccountManager;->addAccount(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Landroid/os/Bundle;Landroid/app/Activity;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    goto :goto_1
.end method

.method private performTokenExchange(Ljava/lang/String;)V
    .locals 3
    .param p1, "code"    # Ljava/lang/String;

    .prologue
    .line 281
    sget-object v1, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFourSquare;->CLIENT_ID:Ljava/lang/String;

    sget-object v2, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFourSquare;->CLIENT_SECRET:Ljava/lang/String;

    invoke-static {p0, v1, v2, p1}, Lcom/foursquare/android/nativeoauth/FoursquareOAuth;->getTokenExchangeIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 283
    .local v0, "intent":Landroid/content/Intent;
    const/16 v1, 0x3ea

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthSSOActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 284
    return-void
.end method

.method private registerRetryNotification()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 391
    new-instance v0, Landroid/content/Intent;

    const-class v6, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthSSOActivity;

    invoke-direct {v0, p0, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 392
    .local v0, "i":Landroid/content/Intent;
    const/high16 v6, 0x10000000

    invoke-virtual {v0, v6}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 393
    const/16 v6, 0xfa0

    invoke-static {p0, v6, v0, v7}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 396
    .local v1, "intent":Landroid/app/PendingIntent;
    const v6, 0x7f08003d

    invoke-virtual {p0, v6}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthSSOActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 397
    .local v4, "ticker":Ljava/lang/String;
    const v6, 0x7f080021

    invoke-virtual {p0, v6}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthSSOActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 399
    .local v5, "title":Ljava/lang/String;
    new-instance v3, Landroid/app/Notification$Builder;

    iget-object v6, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthSSOActivity;->mAppContext:Landroid/content/Context;

    invoke-direct {v3, v6}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    .line 400
    .local v3, "noti":Landroid/app/Notification$Builder;
    invoke-virtual {v3, v5}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    .line 401
    invoke-virtual {v3, v4}, Landroid/app/Notification$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    .line 402
    invoke-virtual {v3, v4}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    .line 403
    invoke-virtual {v3, v1}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    .line 404
    invoke-virtual {v3, v7}, Landroid/app/Notification$Builder;->setAutoCancel(Z)Landroid/app/Notification$Builder;

    .line 405
    new-instance v6, Landroid/app/Notification$BigTextStyle;

    invoke-direct {v6}, Landroid/app/Notification$BigTextStyle;-><init>()V

    invoke-virtual {v6, v4}, Landroid/app/Notification$BigTextStyle;->bigText(Ljava/lang/CharSequence;)Landroid/app/Notification$BigTextStyle;

    move-result-object v6

    invoke-virtual {v3, v6}, Landroid/app/Notification$Builder;->setStyle(Landroid/app/Notification$Style;)Landroid/app/Notification$Builder;

    .line 406
    const/4 v6, 0x1

    invoke-virtual {v3, v6}, Landroid/app/Notification$Builder;->setDefaults(I)Landroid/app/Notification$Builder;

    .line 407
    const v6, 0x7f02001b

    invoke-virtual {v3, v6}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    .line 409
    const-string v6, "notification"

    invoke-virtual {p0, v6}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthSSOActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/NotificationManager;

    .line 410
    .local v2, "nm":Landroid/app/NotificationManager;
    const/16 v6, 0x1004

    invoke-virtual {v3}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v7

    invoke-virtual {v2, v6, v7}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 412
    iget-object v6, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthSSOActivity;->mHandler:Landroid/os/Handler;

    new-instance v7, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthSSOActivity$3;

    invoke-direct {v7, p0}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthSSOActivity$3;-><init>(Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthSSOActivity;)V

    invoke-virtual {v6, v7}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 419
    return-void
.end method

.method private setAuthTokenNExpires(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3
    .param p1, "accessToken"    # Ljava/lang/String;
    .param p2, "expires"    # Ljava/lang/String;

    .prologue
    .line 351
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 352
    :cond_0
    const/4 v1, 0x0

    .line 357
    :goto_0
    return v1

    .line 354
    :cond_1
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/sns3/SnsApplication;->getSvcMgr()Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/sns3/svc/SnsSvcMgr;->getTokenMgr()Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;

    move-result-object v1

    const-string v2, "foursquare"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;->getToken(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsToken;

    .line 356
    .local v0, "fsToken":Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsToken;
    invoke-virtual {v0, p1}, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsToken;->setAccessToken(Ljava/lang/String;)V

    .line 357
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private showAlertDialog()V
    .locals 8

    .prologue
    const v7, 0x7f080021

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 229
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/sns3/auth/SnsAccountAlertDialogActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 230
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "icon_id"

    const v2, 0x7f020014

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 231
    const-string v1, "title"

    const v2, 0x7f080046

    new-array v3, v6, [Ljava/lang/Object;

    invoke-virtual {p0, v7}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthSSOActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {p0, v2, v3}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthSSOActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 233
    const-string v1, "dlg_title"

    const v2, 0x7f08000d

    invoke-virtual {p0, v2}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthSSOActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 234
    const-string v1, "dlg_message"

    const v2, 0x7f08003c

    new-array v3, v6, [Ljava/lang/Object;

    invoke-virtual {p0, v7}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthSSOActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {p0, v2, v3}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthSSOActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 236
    const-string v1, "url"

    const-string v2, "market://details?id=com.joelapenna.foursquared"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 237
    const/high16 v1, 0x800000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 238
    invoke-virtual {p0, v0}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthSSOActivity;->startActivity(Landroid/content/Intent;)V

    .line 239
    return-void
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 0
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 182
    packed-switch p1, :pswitch_data_0

    .line 196
    invoke-super {p0, p1, p2, p3}, Landroid/accounts/AccountAuthenticatorActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 198
    :goto_0
    return-void

    .line 184
    :pswitch_0
    invoke-direct {p0, p2, p3}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthSSOActivity;->getCode(ILandroid/content/Intent;)V

    goto :goto_0

    .line 188
    :pswitch_1
    invoke-direct {p0, p2, p3}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthSSOActivity;->getAccessToken(ILandroid/content/Intent;)V

    goto :goto_0

    .line 192
    :pswitch_2
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthSSOActivity;->finish()V

    goto :goto_0

    .line 182
    nop

    :pswitch_data_0
    .packed-switch 0x3e8
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 142
    invoke-static {p0}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->isLightTheme(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_1

    const v2, 0x1030132

    :goto_0
    invoke-virtual {p0, v2}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthSSOActivity;->setTheme(I)V

    .line 145
    invoke-super {p0, p1}, Landroid/accounts/AccountAuthenticatorActivity;->onCreate(Landroid/os/Bundle;)V

    .line 147
    sget v2, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthSSOActivity;->mInstanceCount:I

    add-int/lit8 v2, v2, 0x1

    sput v2, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthSSOActivity;->mInstanceCount:I

    .line 148
    sget v2, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthSSOActivity;->mInstanceCount:I

    if-le v2, v7, :cond_0

    .line 149
    sget-object v2, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthSSOActivity;->TAG:Ljava/lang/String;

    const-string v3, "multi instance"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 152
    :cond_0
    iput-object p0, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthSSOActivity;->mAppContext:Landroid/content/Context;

    .line 154
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthSSOActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "RetryLogin"

    invoke-virtual {v2, v3, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthSSOActivity;->mRetryLogin:Z

    .line 155
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthSSOActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "skip_sso_noti"

    invoke-virtual {v2, v3, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthSSOActivity;->mSkipSSONoti:Z

    .line 156
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthSSOActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "skip_sync_setup"

    invoke-virtual {v2, v3, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthSSOActivity;->mSkipSyncSetup:Z

    .line 158
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v2

    const-string v3, "com.sec.android.app.sns3.foursquare"

    invoke-virtual {v2, v3}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 159
    .local v0, "accounts":[Landroid/accounts/Account;
    array-length v2, v0

    if-lez v2, :cond_2

    iget-boolean v2, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthSSOActivity;->mRetryLogin:Z

    if-nez v2, :cond_2

    .line 160
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthSSOActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const/high16 v3, 0x7f080000

    new-array v4, v7, [Ljava/lang/Object;

    const v5, 0x7f080021

    invoke-virtual {p0, v5}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthSSOActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {p0, v3, v4}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthSSOActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 163
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthSSOActivity;->finish()V

    .line 172
    :goto_1
    return-void

    .line 142
    .end local v0    # "accounts":[Landroid/accounts/Account;
    :cond_1
    const v2, 0x103012e

    goto :goto_0

    .line 167
    .restart local v0    # "accounts":[Landroid/accounts/Account;
    :cond_2
    const-string v2, "notification"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthSSOActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/NotificationManager;

    .line 168
    .local v1, "notiMgr":Landroid/app/NotificationManager;
    const/16 v2, 0x1004

    invoke-virtual {v1, v2}, Landroid/app/NotificationManager;->cancel(I)V

    .line 169
    const/16 v2, 0x10cc

    invoke-virtual {v1, v2}, Landroid/app/NotificationManager;->cancel(I)V

    .line 171
    invoke-direct {p0}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthSSOActivity;->FoursquareSSOAuth()V

    goto :goto_1
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 176
    sget v0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthSSOActivity;->mInstanceCount:I

    add-int/lit8 v0, v0, -0x1

    sput v0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthSSOActivity;->mInstanceCount:I

    .line 177
    invoke-super {p0}, Landroid/accounts/AccountAuthenticatorActivity;->onDestroy()V

    .line 178
    return-void
.end method
