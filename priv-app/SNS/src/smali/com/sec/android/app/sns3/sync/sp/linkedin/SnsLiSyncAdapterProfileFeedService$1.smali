.class Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService$1;
.super Ljava/lang/Object;
.source "SnsLiSyncAdapterProfileFeedService.java"

# interfaces
.implements Lcom/sec/android/app/sns3/agent/command/ISnsCommandCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService;->performSync()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService;


# direct methods
.method constructor <init>(Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService;)V
    .locals 0

    .prologue
    .line 180
    iput-object p1, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService$1;->this$0:Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCmdRespond(IZLjava/lang/String;Ljava/util/List;)V
    .locals 9
    .param p1, "cmdID"    # I
    .param p2, "bSuccess"    # Z
    .param p3, "uri"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IZ",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p4, "responseList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;>;"
    const/4 v2, 0x0

    .line 185
    if-eqz p2, :cond_7

    .line 187
    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService$1;->this$0:Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService;

    const/4 v3, 0x2

    # setter for: Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService;->mSyncState:I
    invoke-static {v1, v3}, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService;->access$502(Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService;I)I

    .line 189
    new-instance v8, Landroid/content/ContentValues;

    invoke-direct {v8}, Landroid/content/ContentValues;-><init>()V

    .line 190
    .local v8, "values":Landroid/content/ContentValues;
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/sns3/SnsApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 191
    .local v0, "cr":Landroid/content/ContentResolver;
    sget-object v1, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$ProfileFeedList;->CONTENT_URI:Landroid/net/Uri;

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 194
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_6

    .line 195
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 198
    :cond_0
    invoke-virtual {v8}, Landroid/content/ContentValues;->clear()V

    .line 200
    const-string v1, "sp_type"

    const-string v3, "linkedin"

    invoke-virtual {v8, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 201
    const-string v1, "post_id"

    const-string v3, "update_key"

    invoke-interface {v6, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v6, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v8, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 203
    const-string v1, "comment"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 204
    const-string v1, "message"

    const-string v3, "comment"

    invoke-interface {v6, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v6, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v8, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 208
    :cond_1
    const-string v1, "message"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 209
    const-string v1, "message"

    const-string v3, "message"

    invoke-interface {v6, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v6, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v8, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 213
    :cond_2
    const-string v1, "updated_time"

    const-string v3, "timestamp"

    invoke-interface {v6, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v6, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v8, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 216
    const-string v1, "media_url"

    const-string v3, "thumbnail_url"

    invoke-interface {v6, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v6, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v8, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 219
    const-string v1, "link"

    const-string v3, "submitted_url"

    invoke-interface {v6, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v6, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v8, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    const-string v1, "object_type"

    const-string v3, "update_type"

    invoke-interface {v6, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v6, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v8, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 226
    sget-object v1, Lcom/sec/android/app/sns3/agent/life/db/SnsLifeDB$LifeTimeline;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->getType(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_5

    .line 227
    sget-object v1, Lcom/sec/android/app/sns3/agent/life/db/SnsLifeDB$LifeTimeline;->CONTENT_URI:Landroid/net/Uri;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "post_id = \'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "update_key"

    invoke-interface {v6, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v6, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v8, v3, v2}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v7

    .line 236
    .local v7, "update":I
    if-nez v7, :cond_3

    .line 237
    const-string v1, "timestamp_utc"

    const-string v3, "timestamp"

    invoke-interface {v6, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v6, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v8, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 241
    sget-object v1, Lcom/sec/android/app/sns3/agent/life/db/SnsLifeDB$LifeTimeline;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1, v8}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 251
    :cond_3
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_0

    .line 253
    .end local v7    # "update":I
    :cond_4
    :goto_0
    sget-object v1, Lcom/sec/android/app/sns3/app/life/SnsLifeResource;->TAG:Ljava/lang/String;

    const-string v2, "getLinkedin() cursor has data "

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 254
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 263
    .end local v0    # "cr":Landroid/content/ContentResolver;
    .end local v6    # "cursor":Landroid/database/Cursor;
    .end local v8    # "values":Landroid/content/ContentValues;
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService$1;->this$0:Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService;

    # getter for: Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;
    invoke-static {v1}, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService;->access$600(Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService;)Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;->resumeSync()V

    .line 265
    return-void

    .line 246
    .restart local v0    # "cr":Landroid/content/ContentResolver;
    .restart local v6    # "cursor":Landroid/database/Cursor;
    .restart local v8    # "values":Landroid/content/ContentValues;
    :cond_5
    sget-object v1, Lcom/sec/android/app/sns3/app/life/SnsLifeResource;->TAG:Ljava/lang/String;

    const-string v2, "Content URI Linkedin not available"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 256
    :cond_6
    sget-object v1, Lcom/sec/android/app/sns3/app/life/SnsLifeResource;->TAG:Ljava/lang/String;

    const-string v2, "getLinkedin() cursor no data"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 260
    .end local v0    # "cr":Landroid/content/ContentResolver;
    .end local v6    # "cursor":Landroid/database/Cursor;
    .end local v8    # "values":Landroid/content/ContentValues;
    :cond_7
    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService$1;->this$0:Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService;

    const/4 v2, -0x1

    # setter for: Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService;->mSyncState:I
    invoke-static {v1, v2}, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService;->access$502(Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService;I)I

    goto :goto_1
.end method
