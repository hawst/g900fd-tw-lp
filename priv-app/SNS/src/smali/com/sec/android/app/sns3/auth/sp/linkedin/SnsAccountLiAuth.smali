.class public Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuth;
.super Ljava/lang/Object;
.source "SnsAccountLiAuth.java"


# static fields
.field public static final ACTION_SNS_LINKEDIN_LOGGED_IN:Ljava/lang/String; = "com.sec.android.app.sns.ACTION_SNS_LINKEDIN_LOGGED_IN"

.field public static final ACTION_SNS_LINKEDIN_LOGGED_OUT:Ljava/lang/String; = "com.sec.android.app.sns.ACTION_SNS_LINKEDIN_LOGGED_OUT"

.field public static final CONTACT_PREFERENCE_KEY:Ljava/lang/String; = "LI_contact_sync_interval"

.field public static final EXTRA_SKIP_SSO_NOTI:Ljava/lang/String; = "skip_sso_noti"

.field public static final EXTRA_SKIP_SYNC_ADAPTER_OF_MARKET_APP:Ljava/lang/String; = "skip_sync_marketapp"

.field public static final EXTRA_SKIP_SYNC_SETUP:Ljava/lang/String; = "skip_sync_setup"

.field public static final EXTRA_SSO_FROM_APP:Ljava/lang/String; = "sso_from_app"

.field public static final LINKEDIN_APP_PREFERENCE_KEY:Ljava/lang/String; = "LI_linkedin_app_sync_interval"

.field public static final LINKEDIN_SSO_NOTIFICATION_ID:I = 0xc1c

.field public static final LOGIN_LINKEDIN_NOTIFICATION_ID:I = 0xc80

.field public static final LOGIN_REQUEST_CODE:I = 0xbb8

.field public static final MARKET_APP_SSO:Z = true

.field public static final PERMISSION_SCOPE:Ljava/lang/String; = "r_fullprofile r_emailaddress r_network r_contactinfo rw_nus rw_groups w_messages"

.field public static final PROFILES_PREFERENCE_KEY:Ljava/lang/String; = "LI_profiles_sync_interval"

.field public static final PROFILE_FEED_PREFERENCE_KEY:Ljava/lang/String; = "LI_profile_feed_sync_interval"

.field public static final RETRY_LOGIN_NOTIFICATION_ID:I = 0xce4

.field public static final SNS_BROADCAST_PERMISSION:Ljava/lang/String; = "com.sec.android.app.sns3.permission.RECEIVE_SNS_BROADCAST"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
