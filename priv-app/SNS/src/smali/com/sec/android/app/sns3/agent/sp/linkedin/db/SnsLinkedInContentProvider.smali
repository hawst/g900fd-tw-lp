.class public Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInContentProvider;
.super Landroid/content/ContentProvider;
.source "SnsLinkedInContentProvider.java"


# static fields
.field private static final CONTACTS_PACKAGE:Ljava/lang/String; = "com.android.contacts"

.field private static final FEED_LIST:I = 0x834

.field private static final HOME_FEED_LIST:I = 0x8fc

.field private static final MAX_RECORDS_PEOPLE_LOOKUP:I = 0x3e8

.field private static final PEOPLE_LOOKUP:I = 0x9c4

.field private static final PROFILE_FEED_LIST:I = 0x898

.field private static final STATUS_STREAM:I = 0x960

.field private static final TAG:Ljava/lang/String; = "SnsLinkedInContentProvider"

.field private static final USER_APPS_UPDATE:I = 0x5dc

.field private static final USER_BASIC_INFO:I = 0x64

.field private static final USER_COMMENTS:I = 0x44c

.field private static final USER_COMPANIES:I = 0x6a4

.field private static final USER_COMPANY_UPDATES:I = 0x708

.field private static final USER_CONNECTIONS:I = 0x12c

.field private static final USER_CONNECTION_UPDATE:I = 0x4b0

.field private static final USER_CONTACT:I = 0x258

.field private static final USER_EDUCATION:I = 0x578

.field private static final USER_FOLLOWINGS:I = 0x1f4

.field private static final USER_GROUP:I = 0x2bc

.field private static final USER_GROUPS_UPDATE:I = 0x514

.field private static final USER_JOBS_UPDATE:I = 0x640

.field private static final USER_LIKES:I = 0x3e8

.field private static final USER_RECOMMENDATION_UPDATE:I = 0x7d0

.field private static final USER_SHARE_UPDATES:I = 0x384

.field private static final USER_SKILLS:I = 0x190

.field private static final USER_UPDATES:I = 0x320

.field private static final USER_WORK_INFO:I = 0xc8

.field private static final VIRAL_UPDATES:I = 0x76c

.field private static final WIPE_LI_DATA:I = 0x1f40

.field private static final uriMatcher:Landroid/content/UriMatcher;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mOpenHelper:Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDBHelper;

.field private mUnrestrictedPackages:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 951
    new-instance v0, Landroid/content/UriMatcher;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    sput-object v0, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    .line 953
    sget-object v0, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.sns3.sp.linkedin"

    const-string v2, "user_basic_info"

    const/16 v3, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 955
    sget-object v0, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.sns3.sp.linkedin"

    const-string v2, "user_work_info"

    const/16 v3, 0xc8

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 957
    sget-object v0, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.sns3.sp.linkedin"

    const-string v2, "user_connections"

    const/16 v3, 0x12c

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 959
    sget-object v0, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.sns3.sp.linkedin"

    const-string v2, "user_skills"

    const/16 v3, 0x190

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 961
    sget-object v0, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.sns3.sp.linkedin"

    const-string v2, "followings"

    const/16 v3, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 963
    sget-object v0, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.sns3.sp.linkedin"

    const-string v2, "user_contact"

    const/16 v3, 0x258

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 965
    sget-object v0, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.sns3.sp.linkedin"

    const-string v2, "user_groups"

    const/16 v3, 0x2bc

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 967
    sget-object v0, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.sns3.sp.linkedin"

    const-string v2, "user_updates"

    const/16 v3, 0x320

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 969
    sget-object v0, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.sns3.sp.linkedin"

    const-string v2, "user_share_updates"

    const/16 v3, 0x384

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 971
    sget-object v0, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.sns3.sp.linkedin"

    const-string v2, "user_likes"

    const/16 v3, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 972
    sget-object v0, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.sns3.sp.linkedin"

    const-string v2, "user_comments"

    const/16 v3, 0x44c

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 974
    sget-object v0, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.sns3.sp.linkedin"

    const-string v2, "user_connection_updates"

    const/16 v3, 0x4b0

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 976
    sget-object v0, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.sns3.sp.linkedin"

    const-string v2, "group_connection_updates"

    const/16 v3, 0x514

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 978
    sget-object v0, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.sns3.sp.linkedin"

    const-string v2, "education"

    const/16 v3, 0x578

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 980
    sget-object v0, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.sns3.sp.linkedin"

    const-string v2, "user_apps_updates"

    const/16 v3, 0x5dc

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 982
    sget-object v0, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.sns3.sp.linkedin"

    const-string v2, "user_jobs_updates"

    const/16 v3, 0x640

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 984
    sget-object v0, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.sns3.sp.linkedin"

    const-string v2, "user_companies"

    const/16 v3, 0x6a4

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 986
    sget-object v0, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.sns3.sp.linkedin"

    const-string v2, "company_updates"

    const/16 v3, 0x708

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 988
    sget-object v0, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.sns3.sp.linkedin"

    const-string v2, "viral_updates"

    const/16 v3, 0x76c

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 990
    sget-object v0, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.sns3.sp.linkedin"

    const-string v2, "user_recommendation_updates"

    const/16 v3, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 992
    sget-object v0, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.sns3.sp.linkedin"

    const-string v2, "feed_list"

    const/16 v3, 0x834

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 993
    sget-object v0, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.sns3.sp.linkedin"

    const-string v2, "profile_feed_list"

    const/16 v3, 0x898

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 995
    sget-object v0, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.sns3.sp.linkedin"

    const-string v2, "home_feed"

    const/16 v3, 0x8fc

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 997
    sget-object v0, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.sns3.sp.linkedin"

    const-string v2, "status_stream"

    const/16 v3, 0x960

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 999
    sget-object v0, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.sns3.sp.linkedin"

    const-string v2, "people_lookup"

    const/16 v3, 0x9c4

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1001
    sget-object v0, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.sns3.sp.linkedin"

    const-string v2, "wipe_li_data"

    const/16 v3, 0x1f40

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1004
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 61
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    .line 67
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInContentProvider;->mOpenHelper:Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDBHelper;

    return-void
.end method

.method private checkAccessForPackage(Ljava/lang/String;)Z
    .locals 3
    .param p1, "requestingPackage"    # Ljava/lang/String;

    .prologue
    .line 940
    iget-object v2, p0, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInContentProvider;->mUnrestrictedPackages:Ljava/util/ArrayList;

    if-eqz v2, :cond_1

    .line 941
    iget-object v2, p0, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInContentProvider;->mUnrestrictedPackages:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 942
    .local v0, "allowedPackage":Ljava/lang/String;
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 943
    const/4 v2, 0x1

    .line 947
    .end local v0    # "allowedPackage":Ljava/lang/String;
    .end local v1    # "i$":Ljava/util/Iterator;
    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private deleteOldRecords(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 4
    .param p1, "tableName"    # Ljava/lang/String;
    .param p2, "criteria"    # Ljava/lang/String;
    .param p3, "sortOrder"    # Ljava/lang/String;
    .param p4, "limit"    # I

    .prologue
    .line 580
    iget-object v2, p0, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInContentProvider;->mOpenHelper:Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDBHelper;

    invoke-virtual {v2}, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDBHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 581
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "DELETE FROM "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " WHERE "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " NOT IN (SELECT "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " FROM "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ORDER BY "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " LIMIT "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 584
    .local v1, "query":Ljava/lang/String;
    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 585
    return-void
.end method

.method private hasAccessToRestrictedData()Z
    .locals 8

    .prologue
    .line 925
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v5

    .line 927
    .local v5, "uid":I
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInContentProvider;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    invoke-virtual {v6, v5}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;

    move-result-object v2

    .line 928
    .local v2, "callerPackages":[Ljava/lang/String;
    move-object v0, v2

    .local v0, "arr$":[Ljava/lang/String;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_1

    aget-object v1, v0, v3

    .line 930
    .local v1, "callerPackage":Ljava/lang/String;
    invoke-direct {p0, v1}, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInContentProvider;->checkAccessForPackage(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 931
    const/4 v6, 0x1

    .line 936
    .end local v1    # "callerPackage":Ljava/lang/String;
    :goto_1
    return v6

    .line 928
    .restart local v1    # "callerPackage":Ljava/lang/String;
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 935
    .end local v1    # "callerPackage":Ljava/lang/String;
    :cond_1
    const-string v6, "SnsLinkedInContentProvider"

    const-string v7, "Access denied"

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 936
    const/4 v6, 0x0

    goto :goto_1
.end method


# virtual methods
.method public bulkInsert(Landroid/net/Uri;[Landroid/content/ContentValues;)I
    .locals 4
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # [Landroid/content/ContentValues;

    .prologue
    .line 590
    const/4 v0, 0x0

    .line 591
    .local v0, "cnt":I
    array-length v2, p2

    .line 593
    .local v2, "numValues":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_1

    .line 594
    aget-object v3, p2, v1

    invoke-virtual {p0, p1, v3}, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInContentProvider;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 595
    add-int/lit8 v0, v0, 0x1

    .line 593
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 597
    :cond_1
    return v0
.end method

.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 6
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "selection"    # Ljava/lang/String;
    .param p3, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 603
    const/4 v1, 0x0

    .line 604
    .local v1, "db":Landroid/database/sqlite/SQLiteDatabase;
    sget-object v4, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v4, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v2

    .line 605
    .local v2, "match":I
    const/4 v0, -0x1

    .line 607
    .local v0, "count":I
    iget-object v4, p0, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInContentProvider;->mOpenHelper:Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDBHelper;

    invoke-virtual {v4}, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDBHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 609
    sparse-switch v2, :sswitch_data_0

    .line 771
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Unknown URI : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 611
    :sswitch_0
    const-string v3, "user_basic_info"

    invoke-static {v1, v3, p2, p3}, Lcom/sec/android/app/sns3/agent/db/SnsDBWrapper;->onDelete(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 773
    :goto_0
    iget-object v3, p0, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInContentProvider;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, p1, v4}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    move v3, v0

    .line 774
    :cond_0
    return v3

    .line 617
    :sswitch_1
    const-string v3, "user_work_info"

    invoke-static {v1, v3, p2, p3}, Lcom/sec/android/app/sns3/agent/db/SnsDBWrapper;->onDelete(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 620
    goto :goto_0

    .line 623
    :sswitch_2
    const-string v3, "user_connections"

    invoke-static {v1, v3, p2, p3}, Lcom/sec/android/app/sns3/agent/db/SnsDBWrapper;->onDelete(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 626
    goto :goto_0

    .line 629
    :sswitch_3
    const-string v3, "user_skills"

    invoke-static {v1, v3, p2, p3}, Lcom/sec/android/app/sns3/agent/db/SnsDBWrapper;->onDelete(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 632
    goto :goto_0

    .line 635
    :sswitch_4
    const-string v3, "followings"

    invoke-static {v1, v3, p2, p3}, Lcom/sec/android/app/sns3/agent/db/SnsDBWrapper;->onDelete(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 638
    goto :goto_0

    .line 641
    :sswitch_5
    const-string v3, "user_contact"

    invoke-static {v1, v3, p2, p3}, Lcom/sec/android/app/sns3/agent/db/SnsDBWrapper;->onDelete(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 644
    goto :goto_0

    .line 647
    :sswitch_6
    const-string v3, "user_groups"

    invoke-static {v1, v3, p2, p3}, Lcom/sec/android/app/sns3/agent/db/SnsDBWrapper;->onDelete(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 650
    goto :goto_0

    .line 653
    :sswitch_7
    const-string v3, "user_updates"

    invoke-static {v1, v3, p2, p3}, Lcom/sec/android/app/sns3/agent/db/SnsDBWrapper;->onDelete(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 656
    goto :goto_0

    .line 659
    :sswitch_8
    const-string v3, "user_share_updates"

    invoke-static {v1, v3, p2, p3}, Lcom/sec/android/app/sns3/agent/db/SnsDBWrapper;->onDelete(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 662
    goto :goto_0

    .line 665
    :sswitch_9
    const-string v3, "user_likes"

    invoke-static {v1, v3, p2, p3}, Lcom/sec/android/app/sns3/agent/db/SnsDBWrapper;->onDelete(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 668
    goto :goto_0

    .line 671
    :sswitch_a
    const-string v3, "user_comments"

    invoke-static {v1, v3, p2, p3}, Lcom/sec/android/app/sns3/agent/db/SnsDBWrapper;->onDelete(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 674
    goto :goto_0

    .line 677
    :sswitch_b
    const-string v3, "user_connection_updates"

    invoke-static {v1, v3, p2, p3}, Lcom/sec/android/app/sns3/agent/db/SnsDBWrapper;->onDelete(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 680
    goto :goto_0

    .line 683
    :sswitch_c
    const-string v3, "group_connection_updates"

    invoke-static {v1, v3, p2, p3}, Lcom/sec/android/app/sns3/agent/db/SnsDBWrapper;->onDelete(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 686
    goto :goto_0

    .line 689
    :sswitch_d
    const-string v3, "education"

    invoke-static {v1, v3, p2, p3}, Lcom/sec/android/app/sns3/agent/db/SnsDBWrapper;->onDelete(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 692
    goto :goto_0

    .line 695
    :sswitch_e
    const-string v3, "user_apps_updates"

    invoke-static {v1, v3, p2, p3}, Lcom/sec/android/app/sns3/agent/db/SnsDBWrapper;->onDelete(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 698
    goto :goto_0

    .line 701
    :sswitch_f
    const-string v3, "user_jobs_updates"

    invoke-static {v1, v3, p2, p3}, Lcom/sec/android/app/sns3/agent/db/SnsDBWrapper;->onDelete(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 704
    goto :goto_0

    .line 707
    :sswitch_10
    const-string v3, "user_companies"

    invoke-static {v1, v3, p2, p3}, Lcom/sec/android/app/sns3/agent/db/SnsDBWrapper;->onDelete(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 710
    goto :goto_0

    .line 713
    :sswitch_11
    const-string v3, "company_updates"

    invoke-static {v1, v3, p2, p3}, Lcom/sec/android/app/sns3/agent/db/SnsDBWrapper;->onDelete(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 716
    goto/16 :goto_0

    .line 719
    :sswitch_12
    const-string v3, "viral_updates"

    invoke-static {v1, v3, p2, p3}, Lcom/sec/android/app/sns3/agent/db/SnsDBWrapper;->onDelete(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 722
    goto/16 :goto_0

    .line 725
    :sswitch_13
    const-string v3, "user_recommendation_updates"

    invoke-static {v1, v3, p2, p3}, Lcom/sec/android/app/sns3/agent/db/SnsDBWrapper;->onDelete(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 729
    goto/16 :goto_0

    .line 732
    :sswitch_14
    const-string v3, "feed_list"

    invoke-static {v1, v3, p2, p3}, Lcom/sec/android/app/sns3/agent/db/SnsDBWrapper;->onDelete(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 735
    goto/16 :goto_0

    .line 738
    :sswitch_15
    const-string v3, "profile_feed_list"

    invoke-static {v1, v3, p2, p3}, Lcom/sec/android/app/sns3/agent/db/SnsDBWrapper;->onDelete(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 741
    goto/16 :goto_0

    .line 743
    :sswitch_16
    const-string v3, "home_feed"

    invoke-static {v1, v3, p2, p3}, Lcom/sec/android/app/sns3/agent/db/SnsDBWrapper;->onDelete(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 746
    goto/16 :goto_0

    .line 748
    :sswitch_17
    invoke-direct {p0}, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInContentProvider;->hasAccessToRestrictedData()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 752
    const-string v3, "status_stream"

    invoke-static {v1, v3, p2, p3}, Lcom/sec/android/app/sns3/agent/db/SnsDBWrapper;->onDelete(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 755
    goto/16 :goto_0

    .line 757
    :sswitch_18
    invoke-direct {p0}, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInContentProvider;->hasAccessToRestrictedData()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 761
    const-string v3, "people_lookup"

    invoke-static {v1, v3, p2, p3}, Lcom/sec/android/app/sns3/agent/db/SnsDBWrapper;->onDelete(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 764
    goto/16 :goto_0

    .line 766
    :sswitch_19
    iget-object v3, p0, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInContentProvider;->mOpenHelper:Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDBHelper;

    invoke-virtual {v3, v1}, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDBHelper;->wipeData(Landroid/database/sqlite/SQLiteDatabase;)V

    goto/16 :goto_0

    .line 609
    :sswitch_data_0
    .sparse-switch
        0x64 -> :sswitch_0
        0xc8 -> :sswitch_1
        0x12c -> :sswitch_2
        0x190 -> :sswitch_3
        0x1f4 -> :sswitch_4
        0x258 -> :sswitch_5
        0x2bc -> :sswitch_6
        0x320 -> :sswitch_7
        0x384 -> :sswitch_8
        0x3e8 -> :sswitch_9
        0x44c -> :sswitch_a
        0x4b0 -> :sswitch_b
        0x514 -> :sswitch_c
        0x578 -> :sswitch_d
        0x5dc -> :sswitch_e
        0x640 -> :sswitch_f
        0x6a4 -> :sswitch_10
        0x708 -> :sswitch_11
        0x76c -> :sswitch_12
        0x7d0 -> :sswitch_13
        0x834 -> :sswitch_14
        0x898 -> :sswitch_15
        0x8fc -> :sswitch_16
        0x960 -> :sswitch_17
        0x9c4 -> :sswitch_18
        0x1f40 -> :sswitch_19
    .end sparse-switch
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 3
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 329
    sget-object v0, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 423
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown URI : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 331
    :sswitch_0
    const-string v0, "vnd.android.cursor.dir/vnd.com.sec.android.app.sns3.sp.linkedin.user"

    .line 419
    :goto_0
    return-object v0

    .line 335
    :sswitch_1
    const-string v0, "vnd.android.cursor.dir/vnd.com.sec.android.app.sns3.sp.linkedin.workinfo"

    goto :goto_0

    .line 339
    :sswitch_2
    const-string v0, "vnd.android.cursor.dir/vnd.com.sec.android.app.sns3.sp.linkedin.connections"

    goto :goto_0

    .line 343
    :sswitch_3
    const-string v0, "vnd.android.cursor.dir/vnd.com.sec.android.app.sns3.sp.linkedin.skills"

    goto :goto_0

    .line 346
    :sswitch_4
    const-string v0, "vnd.android.cursor.dir/vnd.com.sec.android.app.sns3.sp.linkedin.followings"

    goto :goto_0

    .line 349
    :sswitch_5
    const-string v0, "vnd.android.cursor.dir/vnd.com.sec.android.app.sns3.sp.linkedin.contacts"

    goto :goto_0

    .line 353
    :sswitch_6
    const-string v0, "vnd.android.cursor.dir/vnd.com.sec.android.app.sns3.sp.linkedin.groups"

    goto :goto_0

    .line 357
    :sswitch_7
    const-string v0, "vnd.android.cursor.dir/vnd.com.sec.android.app.sns3.sp.linkedin.updates"

    goto :goto_0

    .line 361
    :sswitch_8
    const-string v0, "vnd.android.cursor.dir/vnd.com.sec.android.app.sns3.sp.linkedin.workinfo"

    goto :goto_0

    .line 365
    :sswitch_9
    const-string v0, "vnd.android.cursor.dir/vnd.com.sec.android.app.sns3.sp.linkedin.connections"

    goto :goto_0

    .line 369
    :sswitch_a
    const-string v0, "vnd.android.cursor.dir/vnd.com.sec.android.app.sns3.sp.linkedin.skills"

    goto :goto_0

    .line 372
    :sswitch_b
    const-string v0, "vnd.android.cursor.dir/vnd.com.sec.android.app.sns3.sp.linkedin.followings"

    goto :goto_0

    .line 375
    :sswitch_c
    const-string v0, "vnd.android.cursor.dir/vnd.com.sec.android.app.sns3.sp.linkedin.contacts"

    goto :goto_0

    .line 379
    :sswitch_d
    const-string v0, "vnd.android.cursor.dir/vnd.com.sec.android.app.sns3.sp.linkedin.groups"

    goto :goto_0

    .line 383
    :sswitch_e
    const-string v0, "vnd.android.cursor.dir/vnd.com.sec.android.app.sns3.sp.linkedin.apps"

    goto :goto_0

    .line 387
    :sswitch_f
    const-string v0, "vnd.android.cursor.dir/vnd.com.sec.android.app.sns3.sp.linkedin.jobs"

    goto :goto_0

    .line 391
    :sswitch_10
    const-string v0, "vnd.android.cursor.dir/vnd.com.sec.android.app.sns3.sp.linkedin.companies"

    goto :goto_0

    .line 395
    :sswitch_11
    const-string v0, "vnd.android.cursor.dir/vnd.com.sec.android.app.sns3.sp.linkedin.companyUpdates"

    goto :goto_0

    .line 399
    :sswitch_12
    const-string v0, "vnd.android.cursor.dir/vnd.com.sec.android.app.sns3.sp.linkedin.recommendation"

    goto :goto_0

    .line 403
    :sswitch_13
    const-string v0, "vnd.android.cursor.dir/vnd.com.sec.android.app.sns3.sp.linkedin.feedlist"

    goto :goto_0

    .line 407
    :sswitch_14
    const-string v0, "vnd.android.cursor.dir/vnd.com.sec.android.app.sns3.sp.linkedin.profilefeedlist"

    goto :goto_0

    .line 411
    :sswitch_15
    const-string v0, "vnd.android.cursor.dir/vnd.com.sec.android.app.sns3.sp.linkedin.homefeedlist"

    goto :goto_0

    .line 415
    :sswitch_16
    const-string v0, "vnd.android.cursor.dir/vnd.com.sec.android.app.sns3.sp.linkedin.statusStream"

    goto :goto_0

    .line 419
    :sswitch_17
    const-string v0, "vnd.android.cursor.dir/vnd.com.sec.android.app.sns3.sp.linkedin.peopleLookUp"

    goto :goto_0

    .line 329
    :sswitch_data_0
    .sparse-switch
        0x64 -> :sswitch_0
        0xc8 -> :sswitch_1
        0x12c -> :sswitch_2
        0x190 -> :sswitch_3
        0x1f4 -> :sswitch_4
        0x258 -> :sswitch_5
        0x2bc -> :sswitch_6
        0x320 -> :sswitch_7
        0x384 -> :sswitch_8
        0x3e8 -> :sswitch_9
        0x44c -> :sswitch_a
        0x4b0 -> :sswitch_b
        0x514 -> :sswitch_c
        0x578 -> :sswitch_d
        0x5dc -> :sswitch_e
        0x640 -> :sswitch_f
        0x6a4 -> :sswitch_10
        0x76c -> :sswitch_11
        0x7d0 -> :sswitch_12
        0x834 -> :sswitch_13
        0x898 -> :sswitch_14
        0x8fc -> :sswitch_15
        0x960 -> :sswitch_16
        0x9c4 -> :sswitch_17
    .end sparse-switch
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 10
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;

    .prologue
    const/4 v3, 0x0

    .line 430
    const/4 v0, 0x0

    .line 431
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    sget-object v6, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v6, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v2

    .line 432
    .local v2, "match":I
    const-wide/16 v4, -0x1

    .line 435
    .local v4, "rowId":J
    :try_start_0
    iget-object v6, p0, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInContentProvider;->mOpenHelper:Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDBHelper;

    invoke-virtual {v6}, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDBHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 437
    sparse-switch v2, :sswitch_data_0

    .line 569
    new-instance v6, Ljava/lang/IllegalArgumentException;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Unknown URI : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v6
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 572
    :catch_0
    move-exception v1

    .line 573
    .local v1, "e":Landroid/database/sqlite/SQLiteException;
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    .line 576
    .end local v1    # "e":Landroid/database/sqlite/SQLiteException;
    :goto_0
    const-wide/16 v6, -0x1

    cmp-long v6, v4, v6

    if-nez v6, :cond_0

    move-object p1, v3

    .end local p1    # "uri":Landroid/net/Uri;
    :cond_0
    move-object v3, p1

    :cond_1
    return-object v3

    .line 439
    .restart local p1    # "uri":Landroid/net/Uri;
    :sswitch_0
    :try_start_1
    const-string v6, "user_basic_info"

    const/4 v7, 0x0

    invoke-virtual {v0, v6, v7, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v4

    .line 571
    :goto_1
    iget-object v6, p0, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInContentProvider;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v6, p1, v7}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    goto :goto_0

    .line 444
    :sswitch_1
    const-string v6, "user_work_info"

    const/4 v7, 0x0

    invoke-virtual {v0, v6, v7, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v4

    .line 446
    goto :goto_1

    .line 449
    :sswitch_2
    const-string v6, "user_connections"

    const/4 v7, 0x0

    invoke-virtual {v0, v6, v7, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v4

    .line 451
    goto :goto_1

    .line 454
    :sswitch_3
    const-string v6, "user_skills"

    const/4 v7, 0x0

    invoke-virtual {v0, v6, v7, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v4

    .line 456
    goto :goto_1

    .line 459
    :sswitch_4
    const-string v6, "followings"

    const/4 v7, 0x0

    invoke-virtual {v0, v6, v7, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v4

    .line 461
    goto :goto_1

    .line 464
    :sswitch_5
    const-string v6, "user_contact"

    const/4 v7, 0x0

    invoke-virtual {v0, v6, v7, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v4

    .line 466
    goto :goto_1

    .line 469
    :sswitch_6
    const-string v6, "user_groups"

    const/4 v7, 0x0

    invoke-virtual {v0, v6, v7, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v4

    .line 471
    goto :goto_1

    .line 473
    :sswitch_7
    const-string v6, "user_updates"

    const/4 v7, 0x0

    invoke-virtual {v0, v6, v7, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v4

    .line 475
    goto :goto_1

    .line 478
    :sswitch_8
    const-string v6, "user_share_updates"

    const/4 v7, 0x0

    invoke-virtual {v0, v6, v7, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v4

    .line 480
    goto :goto_1

    .line 483
    :sswitch_9
    const-string v6, "user_likes"

    const/4 v7, 0x0

    invoke-virtual {v0, v6, v7, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v4

    .line 485
    goto :goto_1

    .line 488
    :sswitch_a
    const-string v6, "user_comments"

    const/4 v7, 0x0

    invoke-virtual {v0, v6, v7, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v4

    .line 490
    goto :goto_1

    .line 493
    :sswitch_b
    const-string v6, "user_connection_updates"

    const/4 v7, 0x0

    invoke-virtual {v0, v6, v7, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v4

    .line 496
    goto :goto_1

    .line 499
    :sswitch_c
    const-string v6, "group_connection_updates"

    const/4 v7, 0x0

    invoke-virtual {v0, v6, v7, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v4

    .line 501
    goto :goto_1

    .line 504
    :sswitch_d
    const-string v6, "education"

    const/4 v7, 0x0

    invoke-virtual {v0, v6, v7, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v4

    .line 506
    goto :goto_1

    .line 509
    :sswitch_e
    const-string v6, "user_apps_updates"

    const/4 v7, 0x0

    invoke-virtual {v0, v6, v7, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v4

    .line 511
    goto :goto_1

    .line 514
    :sswitch_f
    const-string v6, "user_jobs_updates"

    const/4 v7, 0x0

    invoke-virtual {v0, v6, v7, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v4

    .line 516
    goto/16 :goto_1

    .line 519
    :sswitch_10
    const-string v6, "user_companies"

    const/4 v7, 0x0

    invoke-virtual {v0, v6, v7, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v4

    .line 521
    goto/16 :goto_1

    .line 524
    :sswitch_11
    const-string v6, "company_updates"

    const/4 v7, 0x0

    invoke-virtual {v0, v6, v7, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v4

    .line 526
    goto/16 :goto_1

    .line 528
    :sswitch_12
    const-string v6, "viral_updates"

    const/4 v7, 0x0

    invoke-virtual {v0, v6, v7, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v4

    .line 530
    goto/16 :goto_1

    .line 533
    :sswitch_13
    const-string v6, "user_recommendation_updates"

    const/4 v7, 0x0

    invoke-virtual {v0, v6, v7, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v4

    .line 536
    goto/16 :goto_1

    .line 539
    :sswitch_14
    const-string v6, "feed_list"

    const/4 v7, 0x0

    invoke-virtual {v0, v6, v7, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v4

    .line 541
    goto/16 :goto_1

    .line 543
    :sswitch_15
    const-string v6, "profile_feed_list"

    const/4 v7, 0x0

    invoke-virtual {v0, v6, v7, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v4

    .line 545
    goto/16 :goto_1

    .line 547
    :sswitch_16
    const-string v6, "home_feed"

    const/4 v7, 0x0

    invoke-virtual {v0, v6, v7, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v4

    .line 549
    goto/16 :goto_1

    .line 551
    :sswitch_17
    invoke-direct {p0}, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInContentProvider;->hasAccessToRestrictedData()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 555
    const-string v6, "status_stream"

    const/4 v7, 0x0

    invoke-virtual {v0, v6, v7, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v4

    .line 557
    goto/16 :goto_1

    .line 559
    :sswitch_18
    invoke-direct {p0}, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInContentProvider;->hasAccessToRestrictedData()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 563
    const-string v6, "people_lookup"

    const/4 v7, 0x0

    invoke-virtual {v0, v6, v7, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v4

    .line 564
    const-string v6, "people_lookup"

    const-string v7, "last_updated_timestamp"

    const-string v8, "DESC"

    const/16 v9, 0x3e8

    invoke-direct {p0, v6, v7, v8, v9}, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInContentProvider;->deleteOldRecords(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_1

    .line 437
    :sswitch_data_0
    .sparse-switch
        0x64 -> :sswitch_0
        0xc8 -> :sswitch_1
        0x12c -> :sswitch_2
        0x190 -> :sswitch_3
        0x1f4 -> :sswitch_4
        0x258 -> :sswitch_5
        0x2bc -> :sswitch_6
        0x320 -> :sswitch_7
        0x384 -> :sswitch_8
        0x3e8 -> :sswitch_9
        0x44c -> :sswitch_a
        0x4b0 -> :sswitch_b
        0x514 -> :sswitch_c
        0x578 -> :sswitch_d
        0x5dc -> :sswitch_e
        0x640 -> :sswitch_f
        0x6a4 -> :sswitch_10
        0x708 -> :sswitch_11
        0x76c -> :sswitch_12
        0x7d0 -> :sswitch_13
        0x834 -> :sswitch_14
        0x898 -> :sswitch_15
        0x8fc -> :sswitch_16
        0x960 -> :sswitch_17
        0x9c4 -> :sswitch_18
    .end sparse-switch
.end method

.method public onCreate()Z
    .locals 9

    .prologue
    .line 131
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInContentProvider;->getContext()Landroid/content/Context;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInContentProvider;->mContext:Landroid/content/Context;

    .line 132
    new-instance v7, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDBHelper;

    iget-object v8, p0, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInContentProvider;->mContext:Landroid/content/Context;

    invoke-direct {v7, v8}, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDBHelper;-><init>(Landroid/content/Context;)V

    iput-object v7, p0, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInContentProvider;->mOpenHelper:Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDBHelper;

    .line 134
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    iput-object v7, p0, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInContentProvider;->mUnrestrictedPackages:Ljava/util/ArrayList;

    .line 135
    iget-object v7, p0, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInContentProvider;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f050002

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v6

    .line 137
    .local v6, "unrestrictedPackages":[Ljava/lang/String;
    const/4 v3, 0x0

    .line 138
    .local v3, "needToReplaceContactsPkg":Z
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v7

    const-string v8, "CscFeature_Contact_ReplacePackageAs"

    invoke-virtual {v7, v8}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 140
    .local v5, "replaceContactsPkg":Ljava/lang/String;
    if-eqz v5, :cond_0

    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_0

    .line 141
    const/4 v3, 0x1

    .line 144
    :cond_0
    move-object v0, v6

    .local v0, "arr$":[Ljava/lang/String;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_2

    aget-object v4, v0, v1

    .line 145
    .local v4, "pkg":Ljava/lang/String;
    if-eqz v3, :cond_1

    const-string v7, "com.android.contacts"

    invoke-virtual {v7, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 146
    iget-object v7, p0, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInContentProvider;->mUnrestrictedPackages:Ljava/util/ArrayList;

    invoke-virtual {v7, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 144
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 148
    :cond_1
    iget-object v7, p0, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInContentProvider;->mUnrestrictedPackages:Ljava/util/ArrayList;

    invoke-virtual {v7, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 152
    .end local v4    # "pkg":Ljava/lang/String;
    :cond_2
    const/4 v7, 0x0

    return v7
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 16
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "sortOrder"    # Ljava/lang/String;

    .prologue
    .line 158
    const/4 v3, 0x0

    .line 159
    .local v3, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v10, 0x0

    .line 161
    .local v10, "c":Landroid/database/Cursor;
    sget-object v4, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v13

    .line 163
    .local v13, "match":I
    const/4 v7, 0x0

    .line 165
    .local v7, "groupby":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInContentProvider;->mOpenHelper:Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDBHelper;

    invoke-virtual {v4}, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDBHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    .line 166
    new-instance v2, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v2}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 168
    .local v2, "queryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    sparse-switch v13, :sswitch_data_0

    .line 311
    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Unknown URI : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 170
    :sswitch_0
    const-string v4, "user_basic_info"

    invoke-virtual {v2, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 316
    :cond_0
    :goto_0
    const/4 v8, 0x0

    move-object/from16 v4, p2

    move-object/from16 v5, p3

    move-object/from16 v6, p4

    move-object/from16 v9, p5

    :try_start_0
    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v10

    :goto_1
    move-object v4, v10

    .line 323
    :goto_2
    return-object v4

    .line 175
    :sswitch_1
    const-string v4, "user_work_info"

    invoke-virtual {v2, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto :goto_0

    .line 180
    :sswitch_2
    const-string v4, "user_connections"

    invoke-virtual {v2, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto :goto_0

    .line 185
    :sswitch_3
    const-string v4, "user_skills"

    invoke-virtual {v2, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto :goto_0

    .line 190
    :sswitch_4
    const-string v4, "user_share_updates"

    invoke-virtual {v2, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto :goto_0

    .line 195
    :sswitch_5
    const-string v4, "user_likes"

    invoke-virtual {v2, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto :goto_0

    .line 200
    :sswitch_6
    const-string v4, "user_comments"

    invoke-virtual {v2, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto :goto_0

    .line 205
    :sswitch_7
    const-string v4, "user_connection_updates"

    invoke-virtual {v2, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto :goto_0

    .line 210
    :sswitch_8
    const-string v4, "followings"

    invoke-virtual {v2, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto :goto_0

    .line 215
    :sswitch_9
    const-string v4, "user_contact"

    invoke-virtual {v2, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto :goto_0

    .line 220
    :sswitch_a
    const-string v4, "group_connection_updates"

    invoke-virtual {v2, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto :goto_0

    .line 225
    :sswitch_b
    const-string v4, "education"

    invoke-virtual {v2, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto :goto_0

    .line 230
    :sswitch_c
    const-string v4, "user_groups"

    invoke-virtual {v2, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto :goto_0

    .line 235
    :sswitch_d
    const-string v4, "user_updates"

    invoke-virtual {v2, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto :goto_0

    .line 240
    :sswitch_e
    const-string v4, "user_apps_updates"

    invoke-virtual {v2, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto :goto_0

    .line 245
    :sswitch_f
    const-string v4, "user_jobs_updates"

    invoke-virtual {v2, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto :goto_0

    .line 250
    :sswitch_10
    const-string v4, "user_companies"

    invoke-virtual {v2, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto :goto_0

    .line 255
    :sswitch_11
    const-string v4, "company_updates"

    invoke-virtual {v2, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto :goto_0

    .line 259
    :sswitch_12
    const-string v4, "viral_updates"

    invoke-virtual {v2, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto :goto_0

    .line 264
    :sswitch_13
    const-string v4, "user_recommendation_updates"

    invoke-virtual {v2, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto :goto_0

    .line 269
    :sswitch_14
    const-string v4, "feed_list"

    invoke-virtual {v2, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 273
    :sswitch_15
    const-string v4, "profile_feed_list"

    invoke-virtual {v2, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 277
    :sswitch_16
    const-string v4, "home_feed"

    invoke-virtual {v2, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 281
    :sswitch_17
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInContentProvider;->hasAccessToRestrictedData()Z

    move-result v4

    if-nez v4, :cond_1

    .line 282
    const/4 v4, 0x0

    goto/16 :goto_2

    .line 285
    :cond_1
    const-string v4, "status_stream"

    invoke-virtual {v2, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 287
    const-string v4, "update"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 288
    .local v14, "updateParameter":Ljava/lang/String;
    const-string v4, "true"

    invoke-virtual {v4, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 289
    const-string v4, "from_id"

    move-object/from16 v0, p3

    move-object/from16 v1, p4

    invoke-static {v0, v1, v4}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->parseQueryParam(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 291
    .local v15, "userId":Ljava/lang/String;
    if-eqz v15, :cond_0

    .line 292
    new-instance v12, Landroid/content/Intent;

    const-string v4, "com.sec.android.app.sns.profile.ACTION_LINKEDIN_REQUESTED"

    invoke-direct {v12, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 294
    .local v12, "intent":Landroid/content/Intent;
    const-string v4, "id"

    invoke-virtual {v12, v4, v15}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 295
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInContentProvider;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v12}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 303
    .end local v12    # "intent":Landroid/content/Intent;
    .end local v14    # "updateParameter":Ljava/lang/String;
    .end local v15    # "userId":Ljava/lang/String;
    :sswitch_18
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInContentProvider;->hasAccessToRestrictedData()Z

    move-result v4

    if-nez v4, :cond_2

    .line 304
    const/4 v4, 0x0

    goto/16 :goto_2

    .line 307
    :cond_2
    const-string v4, "people_lookup"

    invoke-virtual {v2, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 319
    :catch_0
    move-exception v11

    .line 320
    .local v11, "e":Landroid/database/sqlite/SQLiteException;
    invoke-virtual {v11}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    goto/16 :goto_1

    .line 168
    :sswitch_data_0
    .sparse-switch
        0x64 -> :sswitch_0
        0xc8 -> :sswitch_1
        0x12c -> :sswitch_2
        0x190 -> :sswitch_3
        0x1f4 -> :sswitch_8
        0x258 -> :sswitch_9
        0x2bc -> :sswitch_c
        0x320 -> :sswitch_d
        0x384 -> :sswitch_4
        0x3e8 -> :sswitch_5
        0x44c -> :sswitch_6
        0x4b0 -> :sswitch_7
        0x514 -> :sswitch_a
        0x578 -> :sswitch_b
        0x5dc -> :sswitch_e
        0x640 -> :sswitch_f
        0x6a4 -> :sswitch_10
        0x708 -> :sswitch_11
        0x76c -> :sswitch_12
        0x7d0 -> :sswitch_13
        0x834 -> :sswitch_14
        0x898 -> :sswitch_15
        0x8fc -> :sswitch_16
        0x960 -> :sswitch_17
        0x9c4 -> :sswitch_18
    .end sparse-switch
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 7
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 779
    const/4 v0, 0x0

    .line 780
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v3, 0x0

    .line 781
    .local v3, "table":Ljava/lang/String;
    sget-object v4, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v4, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v1

    .line 783
    .local v1, "match":I
    iget-object v4, p0, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInContentProvider;->mOpenHelper:Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDBHelper;

    invoke-virtual {v4}, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDBHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 785
    sparse-switch v1, :sswitch_data_0

    .line 915
    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Unknown URI : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 787
    :sswitch_0
    const-string v3, "user_basic_info"

    .line 918
    :goto_0
    invoke-static {v0, v3, p2, p3, p4}, Lcom/sec/android/app/sns3/agent/db/SnsDBWrapper;->onUpdate(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    .line 919
    .local v2, "result":I
    iget-object v4, p0, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInContentProvider;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, p1, v5}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 920
    .end local v2    # "result":I
    :cond_0
    return v2

    .line 792
    :sswitch_1
    const-string v3, "user_work_info"

    .line 794
    goto :goto_0

    .line 797
    :sswitch_2
    const-string v3, "user_connections"

    .line 799
    goto :goto_0

    .line 802
    :sswitch_3
    const-string v3, "user_skills"

    .line 804
    goto :goto_0

    .line 807
    :sswitch_4
    const-string v3, "followings"

    .line 809
    goto :goto_0

    .line 812
    :sswitch_5
    const-string v3, "user_contact"

    .line 814
    goto :goto_0

    .line 817
    :sswitch_6
    const-string v3, "user_groups"

    .line 819
    goto :goto_0

    .line 822
    :sswitch_7
    const-string v3, "user_updates"

    .line 824
    goto :goto_0

    .line 827
    :sswitch_8
    const-string v3, "user_share_updates"

    .line 829
    goto :goto_0

    .line 832
    :sswitch_9
    const-string v3, "user_likes"

    .line 834
    goto :goto_0

    .line 837
    :sswitch_a
    const-string v3, "user_comments"

    .line 839
    goto :goto_0

    .line 842
    :sswitch_b
    const-string v3, "user_connection_updates"

    .line 844
    goto :goto_0

    .line 847
    :sswitch_c
    const-string v3, "group_connection_updates"

    .line 849
    goto :goto_0

    .line 852
    :sswitch_d
    const-string v3, "education"

    .line 854
    goto :goto_0

    .line 857
    :sswitch_e
    const-string v3, "user_apps_updates"

    .line 859
    goto :goto_0

    .line 862
    :sswitch_f
    const-string v3, "user_jobs_updates"

    .line 864
    goto :goto_0

    .line 867
    :sswitch_10
    const-string v3, "user_companies"

    .line 869
    goto :goto_0

    .line 872
    :sswitch_11
    const-string v3, "company_updates"

    .line 874
    goto :goto_0

    .line 877
    :sswitch_12
    const-string v3, "viral_updates"

    .line 879
    goto :goto_0

    .line 882
    :sswitch_13
    const-string v3, "user_recommendation_updates"

    .line 884
    goto :goto_0

    .line 887
    :sswitch_14
    const-string v3, "feed_list"

    .line 889
    goto :goto_0

    .line 891
    :sswitch_15
    const-string v3, "profile_feed_list"

    .line 893
    goto :goto_0

    .line 895
    :sswitch_16
    const-string v3, "home_feed"

    .line 897
    goto :goto_0

    .line 899
    :sswitch_17
    invoke-direct {p0}, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInContentProvider;->hasAccessToRestrictedData()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 903
    const-string v3, "status_stream"

    .line 905
    goto :goto_0

    .line 907
    :sswitch_18
    invoke-direct {p0}, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInContentProvider;->hasAccessToRestrictedData()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 911
    const-string v3, "people_lookup"

    .line 913
    goto :goto_0

    .line 785
    :sswitch_data_0
    .sparse-switch
        0x64 -> :sswitch_0
        0xc8 -> :sswitch_1
        0x12c -> :sswitch_2
        0x190 -> :sswitch_3
        0x1f4 -> :sswitch_4
        0x258 -> :sswitch_5
        0x2bc -> :sswitch_6
        0x320 -> :sswitch_7
        0x384 -> :sswitch_8
        0x3e8 -> :sswitch_9
        0x44c -> :sswitch_a
        0x4b0 -> :sswitch_b
        0x514 -> :sswitch_c
        0x578 -> :sswitch_d
        0x5dc -> :sswitch_e
        0x640 -> :sswitch_f
        0x6a4 -> :sswitch_10
        0x708 -> :sswitch_11
        0x76c -> :sswitch_12
        0x7d0 -> :sswitch_13
        0x834 -> :sswitch_14
        0x898 -> :sswitch_15
        0x8fc -> :sswitch_16
        0x960 -> :sswitch_17
        0x9c4 -> :sswitch_18
    .end sparse-switch
.end method
