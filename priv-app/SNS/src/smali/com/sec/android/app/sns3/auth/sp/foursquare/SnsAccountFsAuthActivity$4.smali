.class Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity$4;
.super Landroid/webkit/WebViewClient;
.source "SnsAccountFsAuthActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;)V
    .locals 0

    .prologue
    .line 237
    iput-object p1, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity$4;->this$0:Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "errorCode"    # I
    .param p3, "description"    # Ljava/lang/String;
    .param p4, "failingUrl"    # Ljava/lang/String;

    .prologue
    .line 386
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity$4;->this$0:Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;

    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity$4;->this$0:Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;

    # invokes: Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;->handleError(Landroid/content/Context;I)V
    invoke-static {v0, v1, p2}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;->access$500(Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;Landroid/content/Context;I)V

    .line 387
    invoke-super {p0, p1, p2, p3, p4}, Landroid/webkit/WebViewClient;->onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V

    .line 388
    return-void
.end method

.method public shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 9
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "url"    # Ljava/lang/String;

    .prologue
    const/4 v8, 0x0

    const/4 v7, -0x1

    .line 241
    invoke-static {}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->isLoggable()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 242
    # getter for: Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;->access$000()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "####### WebView shouldOverrideUrlLoading URL : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 245
    :cond_0
    if-eqz p2, :cond_4

    const-string v4, "x-oauthflow-foursquare://callback"

    invoke-virtual {p2, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 246
    invoke-virtual {p1}, Landroid/webkit/WebView;->stopLoading()V

    .line 247
    invoke-virtual {p1}, Landroid/webkit/WebView;->clearView()V

    .line 250
    iget-object v4, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity$4;->this$0:Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;

    new-instance v5, Landroid/webkit/WebView;

    iget-object v6, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity$4;->this$0:Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;

    # getter for: Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;->mAppContext:Landroid/content/Context;
    invoke-static {v6}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;->access$300(Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;)Landroid/content/Context;

    move-result-object v6

    invoke-direct {v5, v6}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;)V

    invoke-virtual {v4, v5}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;->setContentView(Landroid/view/View;)V

    .line 252
    const/4 v1, 0x0

    .line 254
    .local v1, "code":Ljava/lang/String;
    const-string v4, "x-oauthflow-foursquare://callback"

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x6

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {p2, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 256
    invoke-static {}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->isLoggable()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 257
    # getter for: Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;->access$000()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "SnsAccountFsAuthActivity : shouldOverrideUrlLoading() - code = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 262
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity$4;->this$0:Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;

    # setter for: Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;->mCode:Ljava/lang/String;
    invoke-static {v4, v1}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;->access$402(Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 264
    iget-object v4, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity$4;->this$0:Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;

    # getter for: Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;->mCode:Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;->access$400(Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_3

    .line 266
    new-instance v0, Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdGetAccessToken;

    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/sns3/SnsApplication;->getSvcMgr()Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v4

    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/sns3/SnsApplication;->getAgentMgr()Lcom/sec/android/app/sns3/agent/SnsAgentMgr;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/sns3/agent/SnsAgentMgr;->getCommandMgr()Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;->getCommandMgrHandle()Landroid/os/Handler;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity$4;->this$0:Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;

    # getter for: Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;->mCode:Ljava/lang/String;
    invoke-static {v6}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;->access$400(Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v0, v4, v5, v6}, Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdGetAccessToken;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Handler;Ljava/lang/String;)V

    .line 270
    .local v0, "cmd":Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;
    new-instance v4, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity$4$1;

    invoke-direct {v4, p0}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity$4$1;-><init>(Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity$4;)V

    invoke-virtual {v0, v4}, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->setCommandCallback(Lcom/sec/android/app/sns3/agent/command/ISnsCommandCallback;)Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;

    .line 361
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->send()I

    .line 380
    .end local v0    # "cmd":Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;
    .end local v1    # "code":Ljava/lang/String;
    :cond_2
    :goto_0
    return v8

    .line 365
    .restart local v1    # "code":Ljava/lang/String;
    :cond_3
    # getter for: Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;->access$000()Ljava/lang/String;

    move-result-object v4

    const-string v5, "SnsAccountFbAuthActivity - state is wrong!!"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 367
    iget-object v4, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity$4;->this$0:Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;

    const/4 v5, 0x0

    invoke-virtual {v4, v7, v7, v5}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;->loginFail(IILjava/lang/String;)V

    goto :goto_0

    .line 370
    .end local v1    # "code":Ljava/lang/String;
    :cond_4
    if-eqz p2, :cond_2

    const-string v4, "market://details"

    invoke-virtual {p2, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 371
    invoke-virtual {p1}, Landroid/webkit/WebView;->stopLoading()V

    .line 373
    const-string v4, "&"

    invoke-virtual {p2, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 375
    .local v3, "splitUrl":[Ljava/lang/String;
    new-instance v2, Landroid/content/Intent;

    const-string v4, "android.intent.action.VIEW"

    invoke-direct {v2, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 376
    .local v2, "intent":Landroid/content/Intent;
    aget-object v4, v3, v8

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 377
    iget-object v4, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity$4;->this$0:Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;

    invoke-virtual {v4, v2}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method
