.class public final enum Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwTrendsAPI$Param;
.super Ljava/lang/Enum;
.source "SnsTwTrendsAPI.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwTrendsAPI;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Param"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwTrendsAPI$Param;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwTrendsAPI$Param;

.field public static final enum DATE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwTrendsAPI$Param;

.field public static final enum EXCLUDE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwTrendsAPI$Param;

.field public static final enum LAT:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwTrendsAPI$Param;

.field public static final enum LONG:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwTrendsAPI$Param;

.field public static final enum WOEID:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwTrendsAPI$Param;


# instance fields
.field private mParam:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 38
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwTrendsAPI$Param;

    const-string v1, "DATE"

    const-string v2, "date"

    invoke-direct {v0, v1, v3, v2}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwTrendsAPI$Param;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwTrendsAPI$Param;->DATE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwTrendsAPI$Param;

    .line 39
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwTrendsAPI$Param;

    const-string v1, "EXCLUDE"

    const-string v2, "exclude"

    invoke-direct {v0, v1, v4, v2}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwTrendsAPI$Param;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwTrendsAPI$Param;->EXCLUDE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwTrendsAPI$Param;

    .line 40
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwTrendsAPI$Param;

    const-string v1, "WOEID"

    const-string v2, "woeid"

    invoke-direct {v0, v1, v5, v2}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwTrendsAPI$Param;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwTrendsAPI$Param;->WOEID:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwTrendsAPI$Param;

    .line 41
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwTrendsAPI$Param;

    const-string v1, "LAT"

    const-string v2, "lat"

    invoke-direct {v0, v1, v6, v2}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwTrendsAPI$Param;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwTrendsAPI$Param;->LAT:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwTrendsAPI$Param;

    .line 42
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwTrendsAPI$Param;

    const-string v1, "LONG"

    const-string v2, "long"

    invoke-direct {v0, v1, v7, v2}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwTrendsAPI$Param;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwTrendsAPI$Param;->LONG:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwTrendsAPI$Param;

    .line 37
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwTrendsAPI$Param;

    sget-object v1, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwTrendsAPI$Param;->DATE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwTrendsAPI$Param;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwTrendsAPI$Param;->EXCLUDE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwTrendsAPI$Param;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwTrendsAPI$Param;->WOEID:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwTrendsAPI$Param;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwTrendsAPI$Param;->LAT:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwTrendsAPI$Param;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwTrendsAPI$Param;->LONG:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwTrendsAPI$Param;

    aput-object v1, v0, v7

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwTrendsAPI$Param;->$VALUES:[Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwTrendsAPI$Param;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "param"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 46
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 47
    iput-object p3, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwTrendsAPI$Param;->mParam:Ljava/lang/String;

    .line 48
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwTrendsAPI$Param;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 37
    const-class v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwTrendsAPI$Param;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwTrendsAPI$Param;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwTrendsAPI$Param;
    .locals 1

    .prologue
    .line 37
    sget-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwTrendsAPI$Param;->$VALUES:[Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwTrendsAPI$Param;

    invoke-virtual {v0}, [Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwTrendsAPI$Param;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwTrendsAPI$Param;

    return-object v0
.end method


# virtual methods
.method public getParam()Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwTrendsAPI$Param;->mParam:Ljava/lang/String;

    return-object v0
.end method
