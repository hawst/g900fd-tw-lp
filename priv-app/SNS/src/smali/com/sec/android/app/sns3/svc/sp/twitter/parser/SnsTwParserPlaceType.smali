.class public Lcom/sec/android/app/sns3/svc/sp/twitter/parser/SnsTwParserPlaceType;
.super Ljava/lang/Object;
.source "SnsTwParserPlaceType.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponsePlaceType;
    .locals 4
    .param p0, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 30
    const/4 v2, 0x0

    .line 32
    .local v2, "response":Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponsePlaceType;
    invoke-static {p0}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->isJsonEmpty(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 33
    const/4 v3, 0x0

    .line 47
    :goto_0
    return-object v3

    .line 36
    :cond_0
    new-instance v2, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponsePlaceType;

    .end local v2    # "response":Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponsePlaceType;
    invoke-direct {v2}, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponsePlaceType;-><init>()V

    .line 38
    .restart local v2    # "response":Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponsePlaceType;
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 40
    .local v1, "jsonObj":Lorg/json/JSONObject;
    const-string v3, "name"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponsePlaceType;->mName:Ljava/lang/String;

    .line 41
    const-string v3, "code"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v3

    iput v3, v2, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponsePlaceType;->mCode:I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .end local v1    # "jsonObj":Lorg/json/JSONObject;
    :goto_1
    move-object v3, v2

    .line 47
    goto :goto_0

    .line 43
    :catch_0
    move-exception v0

    .line 44
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_1
.end method
