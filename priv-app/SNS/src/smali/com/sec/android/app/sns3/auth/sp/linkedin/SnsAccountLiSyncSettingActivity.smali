.class public Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncSettingActivity;
.super Landroid/app/Activity;
.source "SnsAccountLiSyncSettingActivity.java"


# static fields
.field private static final LINKEDIN_MARKET_URI:Ljava/lang/String; = "market://details?id=com.linkedin.android&referrer=utm_source%3Dsamsung_galaxy"

.field private static final LINKEDIN_PACKAGE_NAME:Ljava/lang/String; = "com.linkedin.android"

.field private static final LINKEDIN_SYNC_SETTING_URI:Ljava/lang/String; = "linkedin://contactsyncconsent/settings?referrer_from=os_settings"

.field private static final TAG:Ljava/lang/String; = "SnsAccountLiSyncSettingActivity"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method private gotoAccountSyncSettings(Z)V
    .locals 5
    .param p1, "isSnsAccount"    # Z

    .prologue
    .line 83
    const/4 v0, 0x0

    .line 84
    .local v0, "accountType":Ljava/lang/String;
    if-eqz p1, :cond_1

    .line 85
    const-string v0, "com.sec.android.app.sns3.linkedin"

    .line 90
    :goto_0
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v1

    .line 91
    .local v1, "accounts":[Landroid/accounts/Account;
    array-length v3, v1

    if-lez v3, :cond_0

    .line 92
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 93
    .local v2, "intent":Landroid/content/Intent;
    const-string v3, "android.settings.ACCOUNT_SYNC_SETTINGS"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 94
    const-string v3, "account"

    const/4 v4, 0x0

    aget-object v4, v1, v4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 95
    invoke-virtual {p0, v2}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncSettingActivity;->startActivity(Landroid/content/Intent;)V

    .line 97
    .end local v2    # "intent":Landroid/content/Intent;
    :cond_0
    return-void

    .line 87
    .end local v1    # "accounts":[Landroid/accounts/Account;
    :cond_1
    const-string v0, "com.linkedin.android"

    goto :goto_0
.end method

.method private isLinkedinAppInstalled()Z
    .locals 4

    .prologue
    .line 75
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncSettingActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const-string v2, "com.linkedin.android"

    const/16 v3, 0x80

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 79
    const/4 v1, 0x1

    :goto_0
    return v1

    .line 76
    :catch_0
    move-exception v0

    .line 77
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 45
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 53
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 54
    .local v1, "intent":Landroid/content/Intent;
    const-string v2, "android.intent.action.VIEW"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 55
    const/high16 v2, 0x10000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 56
    const/high16 v2, 0x800000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 57
    invoke-direct {p0}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncSettingActivity;->isLinkedinAppInstalled()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 58
    const-string v2, "linkedin://contactsyncconsent/settings?referrer_from=os_settings"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 64
    :goto_0
    :try_start_0
    invoke-virtual {p0, v1}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncSettingActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 70
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncSettingActivity;->finish()V

    .line 71
    return-void

    .line 60
    :cond_0
    const-string v2, "market://details?id=com.linkedin.android&referrer=utm_source%3Dsamsung_galaxy"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    goto :goto_0

    .line 65
    :catch_0
    move-exception v0

    .line 66
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    const-string v2, "SnsAccountLiSyncSettingActivity"

    const-string v3, "ActivityNotFoundException occurred"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 67
    const/4 v2, 0x0

    invoke-direct {p0, v2}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncSettingActivity;->gotoAccountSyncSettings(Z)V

    goto :goto_1
.end method
