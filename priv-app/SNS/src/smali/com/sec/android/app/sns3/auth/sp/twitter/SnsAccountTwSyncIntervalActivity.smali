.class public Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwSyncIntervalActivity;
.super Landroid/preference/PreferenceActivity;
.source "SnsAccountTwSyncIntervalActivity.java"


# static fields
.field private static final DEFAULT_SYNC_INTERVAL:J = 0x0L

.field private static final SYNC_INTERVAL_KEY:Ljava/lang/String; = "sync_interval"

.field private static final TAG:Ljava/lang/String; = "SnsAccountTwAuth"


# instance fields
.field private mAppContext:Landroid/content/Context;

.field private mHomeFeedIntervalPreference:Landroid/preference/ListPreference;

.field private mHomeFeedSharedPref:Landroid/content/SharedPreferences;

.field private mProfileFeedIntervalPreference:Landroid/preference/ListPreference;

.field private mProfileFeedSharedPref:Landroid/content/SharedPreferences;

.field private mTwitterAppIntervalPreference:Landroid/preference/ListPreference;

.field private mTwitterAppSharedPref:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 46
    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    .line 56
    iput-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwSyncIntervalActivity;->mTwitterAppIntervalPreference:Landroid/preference/ListPreference;

    .line 57
    iput-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwSyncIntervalActivity;->mHomeFeedIntervalPreference:Landroid/preference/ListPreference;

    .line 58
    iput-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwSyncIntervalActivity;->mProfileFeedIntervalPreference:Landroid/preference/ListPreference;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwSyncIntervalActivity;)Landroid/preference/ListPreference;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwSyncIntervalActivity;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwSyncIntervalActivity;->mTwitterAppIntervalPreference:Landroid/preference/ListPreference;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwSyncIntervalActivity;)Landroid/preference/ListPreference;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwSyncIntervalActivity;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwSyncIntervalActivity;->mHomeFeedIntervalPreference:Landroid/preference/ListPreference;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwSyncIntervalActivity;)Landroid/preference/ListPreference;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwSyncIntervalActivity;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwSyncIntervalActivity;->mProfileFeedIntervalPreference:Landroid/preference/ListPreference;

    return-object v0
.end method


# virtual methods
.method isTable()Z
    .locals 2

    .prologue
    .line 376
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwSyncIntervalActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit8 v0, v0, 0x3

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwSyncIntervalActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    .line 379
    :cond_0
    const/4 v0, 0x1

    .line 381
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 12
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 67
    iput-object p0, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwSyncIntervalActivity;->mAppContext:Landroid/content/Context;

    .line 69
    iget-object v8, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwSyncIntervalActivity;->mAppContext:Landroid/content/Context;

    invoke-static {v8}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->isLightTheme(Landroid/content/Context;)Z

    move-result v8

    if-eqz v8, :cond_f

    const v8, 0x103012b

    :goto_0
    invoke-virtual {p0, v8}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwSyncIntervalActivity;->setTheme(I)V

    .line 72
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 74
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwSyncIntervalActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 75
    .local v0, "actionBar":Landroid/app/ActionBar;
    const/4 v8, 0x1

    invoke-virtual {v0, v8}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 76
    const/4 v8, 0x0

    invoke-virtual {v0, v8}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 78
    const v8, 0x7f030005

    invoke-virtual {p0, v8}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwSyncIntervalActivity;->setContentView(I)V

    .line 79
    const v8, 0x7f080046

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    const v11, 0x7f08006d

    invoke-virtual {p0, v11}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwSyncIntervalActivity;->getString(I)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-virtual {p0, v8, v9}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwSyncIntervalActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v8}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwSyncIntervalActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 80
    const v8, 0x7f04000d

    invoke-virtual {p0, v8}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwSyncIntervalActivity;->addPreferencesFromResource(I)V

    .line 82
    const-string v8, "sync_interval"

    invoke-virtual {p0, v8}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwSyncIntervalActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v3

    check-cast v3, Landroid/preference/PreferenceScreen;

    .line 84
    .local v3, "preferenceScreen":Landroid/preference/PreferenceScreen;
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v8

    const-string v9, "com.twitter.android.auth.login"

    invoke-virtual {v8, v9}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v1

    .line 87
    .local v1, "appAccount":[Landroid/accounts/Account;
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v8

    const-string v9, "com.sec.android.app.sns3.twitter"

    invoke-virtual {v8, v9}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v7

    .line 90
    .local v7, "sns3Account":[Landroid/accounts/Account;
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwSyncIntervalActivity;->getIntent()Landroid/content/Intent;

    move-result-object v8

    const-string v9, "skip_sync_marketapp"

    const/4 v10, 0x1

    invoke-virtual {v8, v9, v10}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v6

    .line 92
    .local v6, "skipMarketApp":Z
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/app/sns3/SnsApplication;->getFeatureMgr()Lcom/sec/android/app/sns3/SnsFeatureManager;

    move-result-object v2

    .line 93
    .local v2, "featureMgr":Lcom/sec/android/app/sns3/SnsFeatureManager;
    const/4 v5, 0x0

    .line 94
    .local v5, "providerLabel":Ljava/lang/CharSequence;
    const/4 v4, 0x0

    .line 97
    .local v4, "providerInfo":Landroid/content/pm/ProviderInfo;
    if-nez v6, :cond_4

    .line 98
    array-length v8, v1

    if-lez v8, :cond_4

    .line 99
    const/4 v5, 0x0

    .line 100
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwSyncIntervalActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v8

    const-string v9, "com.twitter.android.provider.TwitterProvider"

    const/4 v10, 0x0

    invoke-virtual {v8, v9, v10}, Landroid/content/pm/PackageManager;->resolveContentProvider(Ljava/lang/String;I)Landroid/content/pm/ProviderInfo;

    move-result-object v4

    .line 101
    if-eqz v4, :cond_0

    .line 102
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwSyncIntervalActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v8

    invoke-virtual {v4, v8}, Landroid/content/pm/ProviderInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v5

    .line 104
    :cond_0
    if-eqz v5, :cond_1

    invoke-interface {v5}, Ljava/lang/CharSequence;->length()I

    move-result v8

    if-nez v8, :cond_2

    .line 105
    :cond_1
    const-string v8, "SnsAccountTwAuth"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "providerLabel is null - "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 106
    const-string v5, "Twitter"

    .line 109
    :cond_2
    new-instance v8, Landroid/preference/ListPreference;

    invoke-direct {v8, p0}, Landroid/preference/ListPreference;-><init>(Landroid/content/Context;)V

    iput-object v8, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwSyncIntervalActivity;->mTwitterAppIntervalPreference:Landroid/preference/ListPreference;

    .line 110
    iget-object v8, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwSyncIntervalActivity;->mTwitterAppIntervalPreference:Landroid/preference/ListPreference;

    const v9, 0x7f080009

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    aput-object v5, v10, v11

    invoke-virtual {p0, v9, v10}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwSyncIntervalActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/preference/ListPreference;->setTitle(Ljava/lang/CharSequence;)V

    .line 112
    iget-object v8, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwSyncIntervalActivity;->mTwitterAppIntervalPreference:Landroid/preference/ListPreference;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/preference/ListPreference;->setPersistent(Z)V

    .line 113
    iget-object v8, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwSyncIntervalActivity;->mTwitterAppIntervalPreference:Landroid/preference/ListPreference;

    const/high16 v9, 0x7f050000

    invoke-virtual {v8, v9}, Landroid/preference/ListPreference;->setEntries(I)V

    .line 114
    iget-object v8, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwSyncIntervalActivity;->mTwitterAppIntervalPreference:Landroid/preference/ListPreference;

    const v9, 0x7f050001

    invoke-virtual {v8, v9}, Landroid/preference/ListPreference;->setEntryValues(I)V

    .line 115
    sget-object v8, Lcom/sec/android/app/sns3/SnsFeatureManager;->ANGLE_INDICATION_SUPPORTED:Ljava/lang/Boolean;

    invoke-virtual {v8}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v8

    if-eqz v8, :cond_3

    .line 116
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwSyncIntervalActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v8

    invoke-static {v8}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->isLightTheme(Landroid/content/Context;)Z

    move-result v8

    if-eqz v8, :cond_10

    .line 117
    iget-object v8, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwSyncIntervalActivity;->mTwitterAppIntervalPreference:Landroid/preference/ListPreference;

    const v9, 0x7f030003

    invoke-virtual {v8, v9}, Landroid/preference/ListPreference;->setWidgetLayoutResource(I)V

    .line 122
    :cond_3
    :goto_1
    iget-object v8, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwSyncIntervalActivity;->mTwitterAppIntervalPreference:Landroid/preference/ListPreference;

    const v9, 0x7f080009

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    aput-object v5, v10, v11

    invoke-virtual {p0, v9, v10}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwSyncIntervalActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/preference/ListPreference;->setDialogTitle(Ljava/lang/CharSequence;)V

    .line 125
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwSyncIntervalActivity;->readTwitterAppSyncInterval()V

    .line 127
    iget-object v8, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwSyncIntervalActivity;->mTwitterAppIntervalPreference:Landroid/preference/ListPreference;

    new-instance v9, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwSyncIntervalActivity$1;

    invoke-direct {v9, p0, v1}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwSyncIntervalActivity$1;-><init>(Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwSyncIntervalActivity;[Landroid/accounts/Account;)V

    invoke-virtual {v8, v9}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 162
    iget-object v8, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwSyncIntervalActivity;->mTwitterAppIntervalPreference:Landroid/preference/ListPreference;

    invoke-virtual {v3, v8}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 167
    :cond_4
    invoke-virtual {v2}, Lcom/sec/android/app/sns3/SnsFeatureManager;->isSyncAdapterForHomeFeedsNeeded()Z

    move-result v8

    if-eqz v8, :cond_9

    .line 168
    const/4 v5, 0x0

    .line 169
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwSyncIntervalActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v8

    const-string v9, "com.sec.android.app.sns3.home"

    const/4 v10, 0x0

    invoke-virtual {v8, v9, v10}, Landroid/content/pm/PackageManager;->resolveContentProvider(Ljava/lang/String;I)Landroid/content/pm/ProviderInfo;

    move-result-object v4

    .line 170
    if-eqz v4, :cond_5

    .line 171
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwSyncIntervalActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v8

    invoke-virtual {v4, v8}, Landroid/content/pm/ProviderInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v5

    .line 173
    :cond_5
    if-eqz v5, :cond_6

    invoke-interface {v5}, Ljava/lang/CharSequence;->length()I

    move-result v8

    if-nez v8, :cond_7

    .line 174
    :cond_6
    const-string v8, "SnsAccountTwAuth"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "providerLabel is null - "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 175
    const-string v5, "Home"

    .line 178
    :cond_7
    new-instance v8, Landroid/preference/ListPreference;

    invoke-direct {v8, p0}, Landroid/preference/ListPreference;-><init>(Landroid/content/Context;)V

    iput-object v8, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwSyncIntervalActivity;->mHomeFeedIntervalPreference:Landroid/preference/ListPreference;

    .line 179
    iget-object v8, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwSyncIntervalActivity;->mHomeFeedIntervalPreference:Landroid/preference/ListPreference;

    const v9, 0x7f080009

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    aput-object v5, v10, v11

    invoke-virtual {p0, v9, v10}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwSyncIntervalActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/preference/ListPreference;->setTitle(Ljava/lang/CharSequence;)V

    .line 181
    iget-object v8, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwSyncIntervalActivity;->mHomeFeedIntervalPreference:Landroid/preference/ListPreference;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/preference/ListPreference;->setPersistent(Z)V

    .line 182
    iget-object v8, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwSyncIntervalActivity;->mHomeFeedIntervalPreference:Landroid/preference/ListPreference;

    const/high16 v9, 0x7f050000

    invoke-virtual {v8, v9}, Landroid/preference/ListPreference;->setEntries(I)V

    .line 183
    iget-object v8, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwSyncIntervalActivity;->mHomeFeedIntervalPreference:Landroid/preference/ListPreference;

    const v9, 0x7f050001

    invoke-virtual {v8, v9}, Landroid/preference/ListPreference;->setEntryValues(I)V

    .line 184
    sget-object v8, Lcom/sec/android/app/sns3/SnsFeatureManager;->ANGLE_INDICATION_SUPPORTED:Ljava/lang/Boolean;

    invoke-virtual {v8}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v8

    if-eqz v8, :cond_8

    .line 185
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwSyncIntervalActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v8

    invoke-static {v8}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->isLightTheme(Landroid/content/Context;)Z

    move-result v8

    if-eqz v8, :cond_11

    .line 186
    iget-object v8, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwSyncIntervalActivity;->mHomeFeedIntervalPreference:Landroid/preference/ListPreference;

    const v9, 0x7f030003

    invoke-virtual {v8, v9}, Landroid/preference/ListPreference;->setWidgetLayoutResource(I)V

    .line 191
    :cond_8
    :goto_2
    iget-object v8, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwSyncIntervalActivity;->mHomeFeedIntervalPreference:Landroid/preference/ListPreference;

    const v9, 0x7f080009

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    aput-object v5, v10, v11

    invoke-virtual {p0, v9, v10}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwSyncIntervalActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/preference/ListPreference;->setDialogTitle(Ljava/lang/CharSequence;)V

    .line 194
    array-length v8, v7

    if-lez v8, :cond_12

    .line 195
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwSyncIntervalActivity;->readHomeFeedSyncInterval()V

    .line 197
    iget-object v8, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwSyncIntervalActivity;->mHomeFeedIntervalPreference:Landroid/preference/ListPreference;

    new-instance v9, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwSyncIntervalActivity$2;

    invoke-direct {v9, p0, v7}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwSyncIntervalActivity$2;-><init>(Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwSyncIntervalActivity;[Landroid/accounts/Account;)V

    invoke-virtual {v8, v9}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 230
    :goto_3
    iget-object v8, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwSyncIntervalActivity;->mHomeFeedIntervalPreference:Landroid/preference/ListPreference;

    invoke-virtual {v3, v8}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 234
    :cond_9
    invoke-virtual {v2}, Lcom/sec/android/app/sns3/SnsFeatureManager;->isSyncAdapterForProfileFeedsNeeded()Z

    move-result v8

    if-eqz v8, :cond_e

    .line 235
    const/4 v5, 0x0

    .line 236
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwSyncIntervalActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v8

    const-string v9, "com.sec.android.app.sns3.life"

    const/4 v10, 0x0

    invoke-virtual {v8, v9, v10}, Landroid/content/pm/PackageManager;->resolveContentProvider(Ljava/lang/String;I)Landroid/content/pm/ProviderInfo;

    move-result-object v4

    .line 237
    if-eqz v4, :cond_a

    .line 238
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwSyncIntervalActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v8

    invoke-virtual {v4, v8}, Landroid/content/pm/ProviderInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v5

    .line 240
    :cond_a
    if-eqz v5, :cond_b

    invoke-interface {v5}, Ljava/lang/CharSequence;->length()I

    move-result v8

    if-nez v8, :cond_c

    .line 241
    :cond_b
    const-string v8, "SnsAccountTwAuth"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "providerLabel is null - "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 242
    const-string v5, "Life Times"

    .line 245
    :cond_c
    new-instance v8, Landroid/preference/ListPreference;

    invoke-direct {v8, p0}, Landroid/preference/ListPreference;-><init>(Landroid/content/Context;)V

    iput-object v8, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwSyncIntervalActivity;->mProfileFeedIntervalPreference:Landroid/preference/ListPreference;

    .line 246
    iget-object v8, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwSyncIntervalActivity;->mProfileFeedIntervalPreference:Landroid/preference/ListPreference;

    const v9, 0x7f080009

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    aput-object v5, v10, v11

    invoke-virtual {p0, v9, v10}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwSyncIntervalActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/preference/ListPreference;->setTitle(Ljava/lang/CharSequence;)V

    .line 248
    iget-object v8, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwSyncIntervalActivity;->mProfileFeedIntervalPreference:Landroid/preference/ListPreference;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/preference/ListPreference;->setPersistent(Z)V

    .line 249
    iget-object v8, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwSyncIntervalActivity;->mProfileFeedIntervalPreference:Landroid/preference/ListPreference;

    const/high16 v9, 0x7f050000

    invoke-virtual {v8, v9}, Landroid/preference/ListPreference;->setEntries(I)V

    .line 250
    iget-object v8, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwSyncIntervalActivity;->mProfileFeedIntervalPreference:Landroid/preference/ListPreference;

    const v9, 0x7f050001

    invoke-virtual {v8, v9}, Landroid/preference/ListPreference;->setEntryValues(I)V

    .line 251
    sget-object v8, Lcom/sec/android/app/sns3/SnsFeatureManager;->ANGLE_INDICATION_SUPPORTED:Ljava/lang/Boolean;

    invoke-virtual {v8}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v8

    if-eqz v8, :cond_d

    .line 252
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwSyncIntervalActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v8

    invoke-static {v8}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->isLightTheme(Landroid/content/Context;)Z

    move-result v8

    if-eqz v8, :cond_13

    .line 253
    iget-object v8, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwSyncIntervalActivity;->mProfileFeedIntervalPreference:Landroid/preference/ListPreference;

    const v9, 0x7f030003

    invoke-virtual {v8, v9}, Landroid/preference/ListPreference;->setWidgetLayoutResource(I)V

    .line 258
    :cond_d
    :goto_4
    iget-object v8, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwSyncIntervalActivity;->mProfileFeedIntervalPreference:Landroid/preference/ListPreference;

    const v9, 0x7f080009

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    aput-object v5, v10, v11

    invoke-virtual {p0, v9, v10}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwSyncIntervalActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/preference/ListPreference;->setDialogTitle(Ljava/lang/CharSequence;)V

    .line 261
    array-length v8, v7

    if-lez v8, :cond_14

    .line 262
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwSyncIntervalActivity;->readProfileFeedSyncInterval()V

    .line 264
    iget-object v8, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwSyncIntervalActivity;->mProfileFeedIntervalPreference:Landroid/preference/ListPreference;

    new-instance v9, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwSyncIntervalActivity$3;

    invoke-direct {v9, p0, v7}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwSyncIntervalActivity$3;-><init>(Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwSyncIntervalActivity;[Landroid/accounts/Account;)V

    invoke-virtual {v8, v9}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 300
    :goto_5
    iget-object v8, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwSyncIntervalActivity;->mProfileFeedIntervalPreference:Landroid/preference/ListPreference;

    invoke-virtual {v3, v8}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 302
    :cond_e
    return-void

    .line 69
    .end local v0    # "actionBar":Landroid/app/ActionBar;
    .end local v1    # "appAccount":[Landroid/accounts/Account;
    .end local v2    # "featureMgr":Lcom/sec/android/app/sns3/SnsFeatureManager;
    .end local v3    # "preferenceScreen":Landroid/preference/PreferenceScreen;
    .end local v4    # "providerInfo":Landroid/content/pm/ProviderInfo;
    .end local v5    # "providerLabel":Ljava/lang/CharSequence;
    .end local v6    # "skipMarketApp":Z
    .end local v7    # "sns3Account":[Landroid/accounts/Account;
    :cond_f
    const v8, 0x1030128

    goto/16 :goto_0

    .line 119
    .restart local v0    # "actionBar":Landroid/app/ActionBar;
    .restart local v1    # "appAccount":[Landroid/accounts/Account;
    .restart local v2    # "featureMgr":Lcom/sec/android/app/sns3/SnsFeatureManager;
    .restart local v3    # "preferenceScreen":Landroid/preference/PreferenceScreen;
    .restart local v4    # "providerInfo":Landroid/content/pm/ProviderInfo;
    .restart local v5    # "providerLabel":Ljava/lang/CharSequence;
    .restart local v6    # "skipMarketApp":Z
    .restart local v7    # "sns3Account":[Landroid/accounts/Account;
    :cond_10
    iget-object v8, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwSyncIntervalActivity;->mTwitterAppIntervalPreference:Landroid/preference/ListPreference;

    const v9, 0x7f030002

    invoke-virtual {v8, v9}, Landroid/preference/ListPreference;->setWidgetLayoutResource(I)V

    goto/16 :goto_1

    .line 188
    :cond_11
    iget-object v8, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwSyncIntervalActivity;->mHomeFeedIntervalPreference:Landroid/preference/ListPreference;

    const v9, 0x7f030002

    invoke-virtual {v8, v9}, Landroid/preference/ListPreference;->setWidgetLayoutResource(I)V

    goto/16 :goto_2

    .line 227
    :cond_12
    iget-object v8, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwSyncIntervalActivity;->mHomeFeedIntervalPreference:Landroid/preference/ListPreference;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/preference/ListPreference;->setEnabled(Z)V

    goto/16 :goto_3

    .line 255
    :cond_13
    iget-object v8, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwSyncIntervalActivity;->mProfileFeedIntervalPreference:Landroid/preference/ListPreference;

    const v9, 0x7f030002

    invoke-virtual {v8, v9}, Landroid/preference/ListPreference;->setWidgetLayoutResource(I)V

    goto :goto_4

    .line 297
    :cond_14
    iget-object v8, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwSyncIntervalActivity;->mProfileFeedIntervalPreference:Landroid/preference/ListPreference;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/preference/ListPreference;->setEnabled(Z)V

    goto :goto_5
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 307
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 312
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 309
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwSyncIntervalActivity;->finish()V

    .line 310
    const/4 v0, 0x1

    goto :goto_0

    .line 307
    nop

    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 317
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onResume()V

    .line 318
    return-void
.end method

.method public readHomeFeedSyncInterval()V
    .locals 6

    .prologue
    .line 340
    const-string v1, "TW_home_feed_sync_interval"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwSyncIntervalActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwSyncIntervalActivity;->mHomeFeedSharedPref:Landroid/content/SharedPreferences;

    .line 342
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwSyncIntervalActivity;->mHomeFeedSharedPref:Landroid/content/SharedPreferences;

    const-string v2, "TW_home_feed_sync_interval"

    const-wide/16 v4, 0x0

    invoke-interface {v1, v2, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    .line 345
    .local v0, "savedHomeFeedInterval":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwSyncIntervalActivity;->mHomeFeedIntervalPreference:Landroid/preference/ListPreference;

    invoke-virtual {v1, v0}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 347
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwSyncIntervalActivity;->mHomeFeedIntervalPreference:Landroid/preference/ListPreference;

    invoke-virtual {v1}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 348
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwSyncIntervalActivity;->mHomeFeedIntervalPreference:Landroid/preference/ListPreference;

    iget-object v2, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwSyncIntervalActivity;->mHomeFeedIntervalPreference:Landroid/preference/ListPreference;

    invoke-virtual {v2}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 349
    :cond_0
    return-void
.end method

.method public readProfileFeedSyncInterval()V
    .locals 6

    .prologue
    .line 358
    const-string v1, "TW_profile_feed_sync_interval"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwSyncIntervalActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwSyncIntervalActivity;->mProfileFeedSharedPref:Landroid/content/SharedPreferences;

    .line 360
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwSyncIntervalActivity;->mProfileFeedSharedPref:Landroid/content/SharedPreferences;

    const-string v2, "TW_profile_feed_sync_interval"

    const-wide/16 v4, 0x0

    invoke-interface {v1, v2, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    .line 363
    .local v0, "savedProfileFeedInterval":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwSyncIntervalActivity;->mProfileFeedIntervalPreference:Landroid/preference/ListPreference;

    invoke-virtual {v1, v0}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 365
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwSyncIntervalActivity;->mProfileFeedIntervalPreference:Landroid/preference/ListPreference;

    invoke-virtual {v1}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 366
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwSyncIntervalActivity;->mProfileFeedIntervalPreference:Landroid/preference/ListPreference;

    iget-object v2, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwSyncIntervalActivity;->mProfileFeedIntervalPreference:Landroid/preference/ListPreference;

    invoke-virtual {v2}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 367
    :cond_0
    return-void
.end method

.method public readTwitterAppSyncInterval()V
    .locals 6

    .prologue
    .line 321
    const-string v1, "TW_twitter_app_sync_interval"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwSyncIntervalActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwSyncIntervalActivity;->mTwitterAppSharedPref:Landroid/content/SharedPreferences;

    .line 323
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwSyncIntervalActivity;->mTwitterAppSharedPref:Landroid/content/SharedPreferences;

    const-string v2, "TW_twitter_app_sync_interval"

    const-wide/16 v4, 0x0

    invoke-interface {v1, v2, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    .line 326
    .local v0, "savedTwitterInterval":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwSyncIntervalActivity;->mTwitterAppIntervalPreference:Landroid/preference/ListPreference;

    invoke-virtual {v1, v0}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 328
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwSyncIntervalActivity;->mTwitterAppIntervalPreference:Landroid/preference/ListPreference;

    invoke-virtual {v1}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 329
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwSyncIntervalActivity;->mTwitterAppIntervalPreference:Landroid/preference/ListPreference;

    iget-object v2, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwSyncIntervalActivity;->mTwitterAppIntervalPreference:Landroid/preference/ListPreference;

    invoke-virtual {v2}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 331
    :cond_0
    return-void
.end method

.method public writeHomeFeedInterval(Ljava/lang/Long;)V
    .locals 4
    .param p1, "value"    # Ljava/lang/Long;

    .prologue
    .line 352
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwSyncIntervalActivity;->mHomeFeedSharedPref:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 353
    .local v0, "homeFeedEditor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "TW_home_feed_sync_interval"

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 354
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 355
    return-void
.end method

.method public writeProfileFeedInterval(Ljava/lang/Long;)V
    .locals 4
    .param p1, "value"    # Ljava/lang/Long;

    .prologue
    .line 370
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwSyncIntervalActivity;->mProfileFeedSharedPref:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 371
    .local v0, "profileFeedEditor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "TW_profile_feed_sync_interval"

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 372
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 373
    return-void
.end method

.method public writeTwitterAppInterval(Ljava/lang/Long;)V
    .locals 4
    .param p1, "value"    # Ljava/lang/Long;

    .prologue
    .line 334
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwSyncIntervalActivity;->mTwitterAppSharedPref:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 335
    .local v0, "twitterEditor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "TW_twitter_app_sync_interval"

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 336
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 337
    return-void
.end method
