.class public Lcom/sec/android/app/sns3/app/ticker/sp/sinaweibo/SnsSwTickerGetHome;
.super Ljava/lang/Object;
.source "SnsSwTickerGetHome.java"


# static fields
.field private static mCmdHandler:Landroid/os/Handler;

.field private static mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

.field private static mUserID:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000(Landroid/content/Context;ZLjava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Landroid/content/Context;
    .param p1, "x1"    # Z
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 34
    invoke-static {p0, p1, p2}, Lcom/sec/android/app/sns3/app/ticker/sp/sinaweibo/SnsSwTickerGetHome;->invokeBroadcast(Landroid/content/Context;ZLjava/lang/String;)V

    return-void
.end method

.method private static invokeBroadcast(Landroid/content/Context;ZLjava/lang/String;)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "bResult"    # Z
    .param p2, "uri"    # Ljava/lang/String;

    .prologue
    .line 74
    const/4 v1, 0x0

    .line 76
    .local v1, "intent":Landroid/content/Intent;
    const-string v0, "com.android.internal.policy.impl.intent.action.ACTION_SINAWEIBO_DATE_UPDATED"

    .line 78
    .local v0, "action":Ljava/lang/String;
    new-instance v1, Landroid/content/Intent;

    .end local v1    # "intent":Landroid/content/Intent;
    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 79
    .restart local v1    # "intent":Landroid/content/Intent;
    const-string v2, "SNS_CONTENT_URI"

    invoke-virtual {v1, v2, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 80
    const-string v2, "SNS_RESULT"

    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 82
    const-string v2, "com.sec.android.app.sns3.permission.RECEIVE_SNS_BROADCAST"

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    .line 84
    invoke-static {}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->isLoggable()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 85
    sget-object v2, Lcom/sec/android/app/sns3/app/ticker/sp/sinaweibo/SnsSwTickerResource;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SnsSwTickerGetHome - invokeBroadcast() : action = ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "], uri = ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "], result = ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 88
    :cond_0
    return-void
.end method

.method public static loadSnsSwCmdGetHome(Landroid/content/Context;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 42
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/sns3/SnsApplication;->getSvcMgr()Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v1

    sput-object v1, Lcom/sec/android/app/sns3/app/ticker/sp/sinaweibo/SnsSwTickerGetHome;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    .line 43
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/sns3/SnsApplication;->getAgentMgr()Lcom/sec/android/app/sns3/agent/SnsAgentMgr;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/sns3/agent/SnsAgentMgr;->getCommandMgr()Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;->getCommandMgrHandle()Landroid/os/Handler;

    move-result-object v1

    sput-object v1, Lcom/sec/android/app/sns3/app/ticker/sp/sinaweibo/SnsSwTickerGetHome;->mCmdHandler:Landroid/os/Handler;

    .line 44
    const-string v1, "me"

    sput-object v1, Lcom/sec/android/app/sns3/app/ticker/sp/sinaweibo/SnsSwTickerGetHome;->mUserID:Ljava/lang/String;

    .line 46
    sget-object v1, Lcom/sec/android/app/sns3/app/ticker/sp/sinaweibo/SnsSwTickerGetHome;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    if-nez v1, :cond_0

    .line 47
    sget-object v1, Lcom/sec/android/app/sns3/app/ticker/sp/sinaweibo/SnsSwTickerResource;->TAG:Ljava/lang/String;

    const-string v2, "[SNS] mSvcMgr is null !!"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 70
    :goto_0
    return-void

    .line 50
    :cond_0
    sget-object v1, Lcom/sec/android/app/sns3/app/ticker/sp/sinaweibo/SnsSwTickerGetHome;->mCmdHandler:Landroid/os/Handler;

    if-nez v1, :cond_1

    .line 51
    sget-object v1, Lcom/sec/android/app/sns3/app/ticker/sp/sinaweibo/SnsSwTickerResource;->TAG:Ljava/lang/String;

    const-string v2, "[SNS] mCmdHandler is null !!"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 55
    :cond_1
    new-instance v0, Lcom/sec/android/app/sns3/agent/sp/sinaweibo/command/SnsSwCmdGetFriendTimeline;

    sget-object v1, Lcom/sec/android/app/sns3/app/ticker/sp/sinaweibo/SnsSwTickerGetHome;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    sget-object v2, Lcom/sec/android/app/sns3/app/ticker/sp/sinaweibo/SnsSwTickerGetHome;->mCmdHandler:Landroid/os/Handler;

    sget-object v3, Lcom/sec/android/app/sns3/app/ticker/sp/sinaweibo/SnsSwTickerGetHome;->mUserID:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/sns3/agent/sp/sinaweibo/command/SnsSwCmdGetFriendTimeline;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Handler;Ljava/lang/String;)V

    .line 56
    .local v0, "cmd":Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;
    new-instance v1, Lcom/sec/android/app/sns3/app/ticker/sp/sinaweibo/SnsSwTickerGetHome$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/sns3/app/ticker/sp/sinaweibo/SnsSwTickerGetHome$1;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->setCommandCallback(Lcom/sec/android/app/sns3/agent/command/ISnsCommandCallback;)Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;

    .line 68
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->send()I

    goto :goto_0
.end method
