.class public Lcom/sec/android/app/sns3/svc/sp/twitter/parser/SnsTwParserTrend;
.super Ljava/lang/Object;
.source "SnsTwParserTrend.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseTrend;
    .locals 9
    .param p0, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 34
    const/4 v6, 0x0

    .line 36
    .local v6, "response":Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseTrend;
    invoke-static {p0}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->isJsonEmpty(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 37
    const/4 v7, 0x0

    .line 70
    :goto_0
    return-object v7

    .line 41
    :cond_0
    :try_start_0
    invoke-static {p0}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->isJsonArray(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 43
    new-instance v3, Lorg/json/JSONArray;

    invoke-direct {v3, p0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 45
    .local v3, "jsonArray":Lorg/json/JSONArray;
    const/4 v0, 0x0

    .line 46
    .local v0, "curNode":Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseTrend;
    const/4 v5, 0x0

    .line 48
    .local v5, "newNode":Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseTrend;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v7

    if-ge v2, v7, :cond_3

    .line 49
    invoke-virtual {v3, v2}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/app/sns3/svc/sp/twitter/parser/SnsTwParserTrend;->parseTrend(Lorg/json/JSONObject;)Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseTrend;

    move-result-object v5

    .line 51
    if-nez v6, :cond_1

    .line 52
    move-object v6, v5

    .line 53
    move-object v0, v6

    .line 48
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 55
    :cond_1
    iput-object v5, v0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseTrend;->mNext:Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseTrend;

    .line 56
    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseTrend;->mNext:Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseTrend;

    goto :goto_2

    .line 61
    .end local v0    # "curNode":Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseTrend;
    .end local v2    # "i":I
    .end local v3    # "jsonArray":Lorg/json/JSONArray;
    .end local v5    # "newNode":Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseTrend;
    :cond_2
    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 62
    .local v4, "jsonObj":Lorg/json/JSONObject;
    invoke-static {v4}, Lcom/sec/android/app/sns3/svc/sp/twitter/parser/SnsTwParserTrend;->parseTrend(Lorg/json/JSONObject;)Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseTrend;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .end local v4    # "jsonObj":Lorg/json/JSONObject;
    :cond_3
    :goto_3
    move-object v7, v6

    .line 70
    goto :goto_0

    .line 65
    :catch_0
    move-exception v1

    .line 66
    .local v1, "e":Lorg/json/JSONException;
    const-string v7, "SNS"

    invoke-virtual {v1}, Lorg/json/JSONException;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 67
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_3
.end method

.method private static parseTrend(Lorg/json/JSONObject;)Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseTrend;
    .locals 2
    .param p0, "jsonObj"    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 74
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseTrend;

    invoke-direct {v0}, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseTrend;-><init>()V

    .line 76
    .local v0, "response":Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseTrend;
    const-string v1, "name"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseTrend;->mName:Ljava/lang/String;

    .line 77
    const-string v1, "query"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseTrend;->mQuery:Ljava/lang/String;

    .line 78
    const-string v1, "url"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseTrend;->mUrl:Ljava/lang/String;

    .line 79
    const-string v1, "as_of"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseTrend;->mAsOf:Ljava/lang/String;

    .line 81
    const-string v1, "trends"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/sns3/svc/sp/twitter/parser/SnsTwParserTrendDate;->parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseTrendDate;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseTrend;->mTrendDate:Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseTrendDate;

    .line 83
    return-object v0
.end method
