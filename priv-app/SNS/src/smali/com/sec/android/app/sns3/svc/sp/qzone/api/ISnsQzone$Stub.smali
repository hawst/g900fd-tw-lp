.class public abstract Lcom/sec/android/app/sns3/svc/sp/qzone/api/ISnsQzone$Stub;
.super Landroid/os/Binder;
.source "ISnsQzone.java"

# interfaces
.implements Lcom/sec/android/app/sns3/svc/sp/qzone/api/ISnsQzone;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/svc/sp/qzone/api/ISnsQzone;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/qzone/api/ISnsQzone$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.sec.android.app.sns3.svc.sp.qzone.api.ISnsQzone"

.field static final TRANSACTION_getApplicationInfo:I = 0x1

.field static final TRANSACTION_getBirthday:I = 0x4

.field static final TRANSACTION_getFeed:I = 0x2

.field static final TRANSACTION_getLocationPost:I = 0x5

.field static final TRANSACTION_getNearby:I = 0x6

.field static final TRANSACTION_getStatus:I = 0x3


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 15
    const-string v0, "com.sec.android.app.sns3.svc.sp.qzone.api.ISnsQzone"

    invoke-virtual {p0, p0, v0}, Lcom/sec/android/app/sns3/svc/sp/qzone/api/ISnsQzone$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 16
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/sec/android/app/sns3/svc/sp/qzone/api/ISnsQzone;
    .locals 2
    .param p0, "obj"    # Landroid/os/IBinder;

    .prologue
    .line 23
    if-nez p0, :cond_0

    .line 24
    const/4 v0, 0x0

    .line 30
    :goto_0
    return-object v0

    .line 26
    :cond_0
    const-string v1, "com.sec.android.app.sns3.svc.sp.qzone.api.ISnsQzone"

    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 27
    .local v0, "iin":Landroid/os/IInterface;
    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/sec/android/app/sns3/svc/sp/qzone/api/ISnsQzone;

    if-eqz v1, :cond_1

    .line 28
    check-cast v0, Lcom/sec/android/app/sns3/svc/sp/qzone/api/ISnsQzone;

    goto :goto_0

    .line 30
    :cond_1
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/qzone/api/ISnsQzone$Stub$Proxy;

    .end local v0    # "iin":Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/sec/android/app/sns3/svc/sp/qzone/api/ISnsQzone$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 34
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 5
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 38
    sparse-switch p1, :sswitch_data_0

    .line 124
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v3

    :goto_0
    return v3

    .line 42
    :sswitch_0
    const-string v4, "com.sec.android.app.sns3.svc.sp.qzone.api.ISnsQzone"

    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 47
    :sswitch_1
    const-string v4, "com.sec.android.app.sns3.svc.sp.qzone.api.ISnsQzone"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 48
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/svc/sp/qzone/api/ISnsQzone$Stub;->getApplicationInfo()Ljava/util/Map;

    move-result-object v2

    .line 49
    .local v2, "_result":Ljava/util/Map;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 50
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeMap(Ljava/util/Map;)V

    goto :goto_0

    .line 55
    .end local v2    # "_result":Ljava/util/Map;
    :sswitch_2
    const-string v4, "com.sec.android.app.sns3.svc.sp.qzone.api.ISnsQzone"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 57
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 59
    .local v0, "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/sns3/svc/sp/qzone/api/ISnsQzoneCallbackFeed$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/android/app/sns3/svc/sp/qzone/api/ISnsQzoneCallbackFeed;

    move-result-object v1

    .line 60
    .local v1, "_arg1":Lcom/sec/android/app/sns3/svc/sp/qzone/api/ISnsQzoneCallbackFeed;
    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/sns3/svc/sp/qzone/api/ISnsQzone$Stub;->getFeed(Ljava/lang/String;Lcom/sec/android/app/sns3/svc/sp/qzone/api/ISnsQzoneCallbackFeed;)I

    move-result v2

    .line 61
    .local v2, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 62
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 67
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v1    # "_arg1":Lcom/sec/android/app/sns3/svc/sp/qzone/api/ISnsQzoneCallbackFeed;
    .end local v2    # "_result":I
    :sswitch_3
    const-string v4, "com.sec.android.app.sns3.svc.sp.qzone.api.ISnsQzone"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 69
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 71
    .restart local v0    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/sns3/svc/sp/qzone/api/ISnsQzoneCallbackStatus$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/android/app/sns3/svc/sp/qzone/api/ISnsQzoneCallbackStatus;

    move-result-object v1

    .line 72
    .local v1, "_arg1":Lcom/sec/android/app/sns3/svc/sp/qzone/api/ISnsQzoneCallbackStatus;
    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/sns3/svc/sp/qzone/api/ISnsQzone$Stub;->getStatus(Ljava/lang/String;Lcom/sec/android/app/sns3/svc/sp/qzone/api/ISnsQzoneCallbackStatus;)I

    move-result v2

    .line 73
    .restart local v2    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 74
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 79
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v1    # "_arg1":Lcom/sec/android/app/sns3/svc/sp/qzone/api/ISnsQzoneCallbackStatus;
    .end local v2    # "_result":I
    :sswitch_4
    const-string v4, "com.sec.android.app.sns3.svc.sp.qzone.api.ISnsQzone"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 81
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 83
    .restart local v0    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/sns3/svc/sp/qzone/api/ISnsQzoneCallbackBirthday$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/android/app/sns3/svc/sp/qzone/api/ISnsQzoneCallbackBirthday;

    move-result-object v1

    .line 84
    .local v1, "_arg1":Lcom/sec/android/app/sns3/svc/sp/qzone/api/ISnsQzoneCallbackBirthday;
    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/sns3/svc/sp/qzone/api/ISnsQzone$Stub;->getBirthday(Ljava/lang/String;Lcom/sec/android/app/sns3/svc/sp/qzone/api/ISnsQzoneCallbackBirthday;)I

    move-result v2

    .line 85
    .restart local v2    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 86
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 91
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v1    # "_arg1":Lcom/sec/android/app/sns3/svc/sp/qzone/api/ISnsQzoneCallbackBirthday;
    .end local v2    # "_result":I
    :sswitch_5
    const-string v4, "com.sec.android.app.sns3.svc.sp.qzone.api.ISnsQzone"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 93
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_0

    .line 94
    sget-object v4, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v4, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 100
    .local v0, "_arg0":Landroid/os/Bundle;
    :goto_1
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/sns3/svc/sp/qzone/api/ISnsQzoneCallbackLocationPost$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/android/app/sns3/svc/sp/qzone/api/ISnsQzoneCallbackLocationPost;

    move-result-object v1

    .line 101
    .local v1, "_arg1":Lcom/sec/android/app/sns3/svc/sp/qzone/api/ISnsQzoneCallbackLocationPost;
    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/sns3/svc/sp/qzone/api/ISnsQzone$Stub;->getLocationPost(Landroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/qzone/api/ISnsQzoneCallbackLocationPost;)I

    move-result v2

    .line 102
    .restart local v2    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 103
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 97
    .end local v0    # "_arg0":Landroid/os/Bundle;
    .end local v1    # "_arg1":Lcom/sec/android/app/sns3/svc/sp/qzone/api/ISnsQzoneCallbackLocationPost;
    .end local v2    # "_result":I
    :cond_0
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/os/Bundle;
    goto :goto_1

    .line 108
    .end local v0    # "_arg0":Landroid/os/Bundle;
    :sswitch_6
    const-string v4, "com.sec.android.app.sns3.svc.sp.qzone.api.ISnsQzone"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 110
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_1

    .line 111
    sget-object v4, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v4, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 117
    .restart local v0    # "_arg0":Landroid/os/Bundle;
    :goto_2
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/sns3/svc/sp/qzone/api/ISnsQzoneCallbackNearby$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/android/app/sns3/svc/sp/qzone/api/ISnsQzoneCallbackNearby;

    move-result-object v1

    .line 118
    .local v1, "_arg1":Lcom/sec/android/app/sns3/svc/sp/qzone/api/ISnsQzoneCallbackNearby;
    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/sns3/svc/sp/qzone/api/ISnsQzone$Stub;->getNearby(Landroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/qzone/api/ISnsQzoneCallbackNearby;)I

    move-result v2

    .line 119
    .restart local v2    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 120
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 114
    .end local v0    # "_arg0":Landroid/os/Bundle;
    .end local v1    # "_arg1":Lcom/sec/android/app/sns3/svc/sp/qzone/api/ISnsQzoneCallbackNearby;
    .end local v2    # "_result":I
    :cond_1
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/os/Bundle;
    goto :goto_2

    .line 38
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
