.class public Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetHome;
.super Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;
.source "SnsLiCmdGetHome.java"


# static fields
.field private static mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;


# instance fields
.field private mBundle:Landroid/os/Bundle;

.field private mUserID:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetHome;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Handler;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 4
    .param p1, "svcMgr"    # Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    .param p2, "cmdHandler"    # Landroid/os/Handler;
    .param p3, "userID"    # Ljava/lang/String;
    .param p4, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 46
    invoke-direct {p0, p2}, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;-><init>(Landroid/os/Handler;)V

    .line 48
    new-instance v1, Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;

    invoke-direct {v1}, Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;-><init>()V

    .line 50
    .local v1, "unit":Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;
    iput-object p3, p0, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetHome;->mUserID:Ljava/lang/String;

    .line 51
    iput-object p4, p0, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetHome;->mBundle:Landroid/os/Bundle;

    .line 52
    sget-object v2, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetHome;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;

    if-nez v2, :cond_0

    .line 53
    new-instance v2, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;

    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/sns3/SnsApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;-><init>(Landroid/content/Context;)V

    sput-object v2, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetHome;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;

    .line 57
    :cond_0
    new-instance v0, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetHome$1;

    iget-object v2, p0, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetHome;->mUserID:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetHome;->mBundle:Landroid/os/Bundle;

    invoke-direct {v0, p0, p1, v2, v3}, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetHome$1;-><init>(Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetHome;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 78
    .local v0, "req":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    invoke-virtual {v1, v0}, Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;->addRequest(Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;)V

    .line 80
    invoke-virtual {p0, v1}, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetHome;->addCommandUnit(Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;)V

    .line 81
    return-void
.end method

.method static synthetic access$000()Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;
    .locals 1

    .prologue
    .line 37
    sget-object v0, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetHome;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;

    return-object v0
.end method


# virtual methods
.method public getUserID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetHome;->mUserID:Ljava/lang/String;

    return-object v0
.end method

.method protected respond()Z
    .locals 5

    .prologue
    .line 90
    const-string v0, "SnsAgent"

    const-string v1, "<SnsLiCmdGetHome> respond()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 92
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetHome;->getCommandCallback()Lcom/sec/android/app/sns3/agent/command/ISnsCommandCallback;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetHome;->getCommandID()I

    move-result v1

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetHome;->isSuccess()Z

    move-result v2

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetHome;->getUri()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetHome;->getResponseList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/sec/android/app/sns3/agent/command/ISnsCommandCallback;->onCmdRespond(IZLjava/lang/String;Ljava/util/List;)V

    .line 94
    const/4 v0, 0x1

    return v0
.end method
