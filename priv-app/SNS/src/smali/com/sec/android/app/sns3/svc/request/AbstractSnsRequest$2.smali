.class Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest$2;
.super Lcom/sec/android/app/sns3/svc/http/ISnsHttpCallback;
.source "AbstractSnsRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->process(Landroid/os/Handler;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;

.field final synthetic val$reqMgrHandler:Landroid/os/Handler;


# direct methods
.method constructor <init>(Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;Landroid/os/Handler;)V
    .locals 0

    .prologue
    .line 149
    iput-object p1, p0, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest$2;->this$0:Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;

    iput-object p2, p0, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest$2;->val$reqMgrHandler:Landroid/os/Handler;

    invoke-direct {p0}, Lcom/sec/android/app/sns3/svc/http/ISnsHttpCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onResponse(IILjava/lang/String;)V
    .locals 16
    .param p1, "httpStatus"    # I
    .param p2, "errorCode"    # I
    .param p3, "content"    # Ljava/lang/String;

    .prologue
    .line 156
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest$2;->this$0:Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;

    iget-object v5, v5, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    invoke-virtual {v5}, Lcom/sec/android/app/sns3/svc/SnsSvcMgr;->getTokenMgr()Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest$2;->this$0:Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;

    iget-object v6, v6, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->mSpType:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;->getToken(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;

    move-result-object v15

    .line 157
    .local v15, "token":Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;
    invoke-virtual {v15}, Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;->isValidAccessTokenNExpires()Z

    move-result v12

    .line 159
    .local v12, "bValidity":Z
    new-instance v3, Lcom/sec/android/app/sns3/svc/util/logging/SnsLogging;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest$2;->this$0:Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;

    iget v4, v5, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->mReqID:I

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest$2;->this$0:Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;

    iget v5, v5, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->mCategory:I

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest$2;->this$0:Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;

    iget-object v6, v6, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->mSpType:Ljava/lang/String;

    if-eqz v12, :cond_0

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest$2;->this$0:Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;

    invoke-virtual {v7}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->compose()Lcom/sec/android/app/sns3/svc/http/SnsHttpRequest;

    move-result-object v7

    if-eqz v7, :cond_0

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest$2;->this$0:Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;

    invoke-virtual {v7}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->compose()Lcom/sec/android/app/sns3/svc/http/SnsHttpRequest;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/sns3/svc/http/SnsHttpRequest;->getURI()Ljava/net/URI;

    move-result-object v8

    :goto_0
    move/from16 v7, p1

    move-object/from16 v9, p3

    invoke-direct/range {v3 .. v9}, Lcom/sec/android/app/sns3/svc/util/logging/SnsLogging;-><init>(IILjava/lang/String;ILjava/net/URI;Ljava/lang/String;)V

    .line 162
    .local v3, "logs":Lcom/sec/android/app/sns3/svc/util/logging/SnsLogging;
    invoke-virtual {v3}, Lcom/sec/android/app/sns3/svc/util/logging/SnsLogging;->start()V

    .line 165
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest$2;->this$0:Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;

    move/from16 v0, p1

    move/from16 v1, p2

    move-object/from16 v2, p3

    invoke-virtual {v5, v0, v1, v2}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->printResponseLog(IILjava/lang/String;)V

    .line 167
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest$2;->this$0:Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;

    iget v5, v5, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->mState:I

    const/4 v6, 0x4

    if-ne v5, v6, :cond_1

    .line 168
    const-string v5, "SNS"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "onResponse : mReqID("

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest$2;->this$0:Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;

    iget v7, v7, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->mReqID:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ") is finished!!"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 214
    :goto_1
    return-void

    .line 159
    .end local v3    # "logs":Lcom/sec/android/app/sns3/svc/util/logging/SnsLogging;
    :cond_0
    const/4 v8, 0x0

    goto :goto_0

    .line 170
    .restart local v3    # "logs":Lcom/sec/android/app/sns3/svc/util/logging/SnsLogging;
    :cond_1
    move/from16 v8, p2

    .line 171
    .local v8, "curErrorCode":I
    const/4 v10, 0x0

    .line 172
    .local v10, "response":Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;
    const/4 v13, 0x0

    .line 173
    .local v13, "errorResp":Lcom/sec/android/app/sns3/svc/sp/SnsSpResponseError;
    new-instance v9, Landroid/os/Bundle;

    invoke-direct {v9}, Landroid/os/Bundle;-><init>()V

    .line 174
    .local v9, "reason":Landroid/os/Bundle;
    const/4 v11, 0x1

    .line 176
    .local v11, "bSuccess":Z
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest$2;->this$0:Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;

    const/4 v6, 0x3

    invoke-virtual {v5, v6}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->setState(I)V

    .line 177
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest$2;->this$0:Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;

    invoke-virtual {v5}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->cancelRequestTimer()V

    .line 179
    if-eqz p3, :cond_2

    .line 180
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest$2;->this$0:Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;

    move-object/from16 v0, p3

    invoke-virtual {v5, v0}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;

    move-result-object v10

    .line 182
    :cond_2
    if-eqz v10, :cond_5

    instance-of v5, v10, Lcom/sec/android/app/sns3/svc/sp/SnsSpResponseError;

    if-eqz v5, :cond_5

    .line 183
    const/4 v11, 0x0

    move-object v13, v10

    .line 185
    check-cast v13, Lcom/sec/android/app/sns3/svc/sp/SnsSpResponseError;

    .line 186
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest$2;->this$0:Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;

    move/from16 v0, p1

    move/from16 v1, p2

    invoke-virtual {v5, v0, v1, v13}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->check(IILcom/sec/android/app/sns3/svc/sp/SnsSpResponse;)I

    move-result v8

    .line 188
    const/4 v5, -0x1

    if-ne v8, v5, :cond_3

    .line 189
    const/16 v8, 0x7d0

    .line 198
    :cond_3
    :goto_2
    if-nez v11, :cond_4

    .line 199
    const-string v6, "error_message"

    if-eqz v13, :cond_6

    invoke-virtual {v13}, Lcom/sec/android/app/sns3/svc/sp/SnsSpResponseError;->getErrorMessage()Ljava/lang/String;

    move-result-object v5

    :goto_3
    invoke-virtual {v9, v6, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 201
    const-string v5, "status"

    move/from16 v0, p1

    invoke-virtual {v9, v5, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 202
    const/4 v10, 0x0

    .line 206
    :cond_4
    new-instance v4, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest$2;->this$0:Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;

    iget v5, v5, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->mReqID:I

    const/4 v6, -0x1

    if-ne v8, v6, :cond_7

    const/4 v6, 0x1

    :goto_4
    move/from16 v7, p1

    invoke-direct/range {v4 .. v10}, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;-><init>(IZIILandroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;)V

    .line 210
    .local v4, "result":Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest$2;->val$reqMgrHandler:Landroid/os/Handler;

    const/16 v6, 0x15

    invoke-virtual {v5, v6, v4}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v14

    .line 212
    .local v14, "msg":Landroid/os/Message;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest$2;->val$reqMgrHandler:Landroid/os/Handler;

    invoke-virtual {v5, v14}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_1

    .line 191
    .end local v4    # "result":Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;
    .end local v14    # "msg":Landroid/os/Message;
    :cond_5
    const/16 v5, 0xc8

    move/from16 v0, p1

    if-eq v0, v5, :cond_3

    .line 192
    const/4 v11, 0x0

    .line 194
    const/4 v5, -0x1

    if-ne v8, v5, :cond_3

    .line 195
    const/16 v8, 0x7d0

    goto :goto_2

    .line 199
    :cond_6
    const/4 v5, 0x0

    goto :goto_3

    .line 206
    :cond_7
    const/4 v6, 0x0

    goto :goto_4
.end method
