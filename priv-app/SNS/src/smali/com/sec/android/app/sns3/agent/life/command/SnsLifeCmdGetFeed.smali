.class public Lcom/sec/android/app/sns3/agent/life/command/SnsLifeCmdGetFeed;
.super Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;
.source "SnsLifeCmdGetFeed.java"


# static fields
.field protected static final MSG_CHECK_RESPOND_COUNT:I = 0x1


# instance fields
.field private mCmdHandler:Landroid/os/Handler;

.field private mMainHandler:Landroid/os/Handler;

.field private mRespondCount:I

.field private mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Handler;)V
    .locals 2
    .param p1, "svcMgr"    # Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    .param p2, "cmdHandler"    # Landroid/os/Handler;

    .prologue
    .line 97
    invoke-direct {p0, p2}, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;-><init>(Landroid/os/Handler;)V

    .line 75
    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/android/app/sns3/agent/life/command/SnsLifeCmdGetFeed;->mRespondCount:I

    .line 77
    new-instance v1, Lcom/sec/android/app/sns3/agent/life/command/SnsLifeCmdGetFeed$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/sns3/agent/life/command/SnsLifeCmdGetFeed$1;-><init>(Lcom/sec/android/app/sns3/agent/life/command/SnsLifeCmdGetFeed;)V

    iput-object v1, p0, Lcom/sec/android/app/sns3/agent/life/command/SnsLifeCmdGetFeed;->mMainHandler:Landroid/os/Handler;

    .line 98
    iput-object p1, p0, Lcom/sec/android/app/sns3/agent/life/command/SnsLifeCmdGetFeed;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    .line 99
    iput-object p2, p0, Lcom/sec/android/app/sns3/agent/life/command/SnsLifeCmdGetFeed;->mCmdHandler:Landroid/os/Handler;

    .line 100
    new-instance v0, Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;

    invoke-direct {v0}, Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;-><init>()V

    .line 101
    .local v0, "unit":Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;
    iget-object v1, p0, Lcom/sec/android/app/sns3/agent/life/command/SnsLifeCmdGetFeed;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    invoke-virtual {p0, v1, p2}, Lcom/sec/android/app/sns3/agent/life/command/SnsLifeCmdGetFeed;->loadSnsLifeCmdGetFeed(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Handler;)V

    .line 102
    invoke-virtual {p0, v0}, Lcom/sec/android/app/sns3/agent/life/command/SnsLifeCmdGetFeed;->addCommandUnit(Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;)V

    .line 103
    return-void
.end method

.method static synthetic access$008(Lcom/sec/android/app/sns3/agent/life/command/SnsLifeCmdGetFeed;)I
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/sns3/agent/life/command/SnsLifeCmdGetFeed;

    .prologue
    .line 67
    iget v0, p0, Lcom/sec/android/app/sns3/agent/life/command/SnsLifeCmdGetFeed;->mRespondCount:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/android/app/sns3/agent/life/command/SnsLifeCmdGetFeed;->mRespondCount:I

    return v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/sns3/agent/life/command/SnsLifeCmdGetFeed;Landroid/content/Context;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/agent/life/command/SnsLifeCmdGetFeed;
    .param p1, "x1"    # Landroid/content/Context;

    .prologue
    .line 67
    invoke-direct {p0, p1}, Lcom/sec/android/app/sns3/agent/life/command/SnsLifeCmdGetFeed;->checkRespondCount(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/sns3/agent/life/command/SnsLifeCmdGetFeed;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sns3/agent/life/command/SnsLifeCmdGetFeed;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/sec/android/app/sns3/agent/life/command/SnsLifeCmdGetFeed;->mMainHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private checkRespondCount(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 715
    sget-object v0, Lcom/sec/android/app/sns3/app/life/SnsLifeResource;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mRespondCount="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/sns3/agent/life/command/SnsLifeCmdGetFeed;->mRespondCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "mAvailableSPCount="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->mAvailableSPCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 718
    iget v0, p0, Lcom/sec/android/app/sns3/agent/life/command/SnsLifeCmdGetFeed;->mRespondCount:I

    sget v1, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->mAvailableSPCount:I

    if-lt v0, v1, :cond_0

    .line 719
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/sns3/agent/life/command/SnsLifeCmdGetFeed;->mRespondCount:I

    .line 720
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/life/command/SnsLifeCmdGetFeed;->getCommandCallback()Lcom/sec/android/app/sns3/agent/command/ISnsCommandCallback;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/life/command/SnsLifeCmdGetFeed;->getCommandID()I

    move-result v1

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/life/command/SnsLifeCmdGetFeed;->getUri()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/life/command/SnsLifeCmdGetFeed;->getResponseList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/sec/android/app/sns3/agent/command/ISnsCommandCallback;->onCmdRespond(IZLjava/lang/String;Ljava/util/List;)V

    .line 722
    :cond_0
    return-void
.end method


# virtual methods
.method public loadSnsLifeCmdGetFeed(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Handler;)V
    .locals 14
    .param p1, "svcMgr"    # Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    .param p2, "cmdHandler"    # Landroid/os/Handler;

    .prologue
    .line 116
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/sns3/SnsApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    .line 117
    .local v6, "cr":Landroid/content/ContentResolver;
    sget-object v1, Lcom/sec/android/app/sns3/agent/life/db/SnsLifeDB$LifeTimeline;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v6, v1, v2, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 118
    invoke-static {}, Lcom/sec/android/app/sns3/SnsFeatureManager;->isChineseModel()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 119
    invoke-static {}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->checkAvailableSPChina()V

    .line 122
    new-instance v12, Lcom/sec/android/app/sns3/agent/sp/qzone/command/SnsQzCmdGetUserTimeline;

    iget-object v1, p0, Lcom/sec/android/app/sns3/agent/life/command/SnsLifeCmdGetFeed;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    iget-object v2, p0, Lcom/sec/android/app/sns3/agent/life/command/SnsLifeCmdGetFeed;->mCmdHandler:Landroid/os/Handler;

    const/4 v3, 0x0

    const/16 v4, 0xa

    invoke-direct {v12, v1, v2, v3, v4}, Lcom/sec/android/app/sns3/agent/sp/qzone/command/SnsQzCmdGetUserTimeline;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Handler;Ljava/lang/String;I)V

    .line 123
    .local v12, "qzCmd":Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;
    new-instance v1, Lcom/sec/android/app/sns3/agent/life/command/SnsLifeCmdGetFeed$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/sns3/agent/life/command/SnsLifeCmdGetFeed$2;-><init>(Lcom/sec/android/app/sns3/agent/life/command/SnsLifeCmdGetFeed;)V

    invoke-virtual {v12, v1}, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->setCommandCallback(Lcom/sec/android/app/sns3/agent/command/ISnsCommandCallback;)Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;

    .line 175
    invoke-virtual {v12}, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->send()I

    .line 178
    new-instance v13, Lcom/sec/android/app/sns3/agent/sp/sinaweibo/command/SnsSwCmdGetUserTimeline;

    iget-object v1, p0, Lcom/sec/android/app/sns3/agent/life/command/SnsLifeCmdGetFeed;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    iget-object v2, p0, Lcom/sec/android/app/sns3/agent/life/command/SnsLifeCmdGetFeed;->mCmdHandler:Landroid/os/Handler;

    const/4 v3, 0x0

    invoke-direct {v13, v1, v2, v3}, Lcom/sec/android/app/sns3/agent/sp/sinaweibo/command/SnsSwCmdGetUserTimeline;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Handler;Ljava/lang/String;)V

    .line 179
    .local v13, "swCmd":Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;
    new-instance v1, Lcom/sec/android/app/sns3/agent/life/command/SnsLifeCmdGetFeed$3;

    invoke-direct {v1, p0}, Lcom/sec/android/app/sns3/agent/life/command/SnsLifeCmdGetFeed$3;-><init>(Lcom/sec/android/app/sns3/agent/life/command/SnsLifeCmdGetFeed;)V

    invoke-virtual {v13, v1}, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->setCommandCallback(Lcom/sec/android/app/sns3/agent/command/ISnsCommandCallback;)Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;

    .line 231
    invoke-virtual {v13}, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->send()I

    .line 712
    .end local v12    # "qzCmd":Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;
    .end local v13    # "swCmd":Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;
    :goto_0
    return-void

    .line 235
    :cond_0
    invoke-static {}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->checkAvailableSP()V

    .line 238
    new-instance v7, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetFeed;

    iget-object v1, p0, Lcom/sec/android/app/sns3/agent/life/command/SnsLifeCmdGetFeed;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    iget-object v2, p0, Lcom/sec/android/app/sns3/agent/life/command/SnsLifeCmdGetFeed;->mCmdHandler:Landroid/os/Handler;

    const-string v3, "me"

    invoke-direct {v7, v1, v2, v3}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetFeed;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Handler;Ljava/lang/String;)V

    .line 239
    .local v7, "fbCmd":Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;
    new-instance v1, Lcom/sec/android/app/sns3/agent/life/command/SnsLifeCmdGetFeed$4;

    invoke-direct {v1, p0}, Lcom/sec/android/app/sns3/agent/life/command/SnsLifeCmdGetFeed$4;-><init>(Lcom/sec/android/app/sns3/agent/life/command/SnsLifeCmdGetFeed;)V

    invoke-virtual {v7, v1}, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->setCommandCallback(Lcom/sec/android/app/sns3/agent/command/ISnsCommandCallback;)Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;

    .line 311
    invoke-virtual {v7}, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->send()I

    .line 314
    new-instance v0, Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdGetUserTimeline;

    iget-object v1, p0, Lcom/sec/android/app/sns3/agent/life/command/SnsLifeCmdGetFeed;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    iget-object v2, p0, Lcom/sec/android/app/sns3/agent/life/command/SnsLifeCmdGetFeed;->mCmdHandler:Landroid/os/Handler;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdGetUserTimeline;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Handler;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 316
    .local v0, "twCmd":Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;
    new-instance v1, Lcom/sec/android/app/sns3/agent/life/command/SnsLifeCmdGetFeed$5;

    invoke-direct {v1, p0}, Lcom/sec/android/app/sns3/agent/life/command/SnsLifeCmdGetFeed$5;-><init>(Lcom/sec/android/app/sns3/agent/life/command/SnsLifeCmdGetFeed;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->setCommandCallback(Lcom/sec/android/app/sns3/agent/command/ISnsCommandCallback;)Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;

    .line 383
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->send()I

    .line 386
    new-instance v9, Lcom/sec/android/app/sns3/agent/sp/googleplus/command/SnsGpCmdGetProfileFeed;

    iget-object v1, p0, Lcom/sec/android/app/sns3/agent/life/command/SnsLifeCmdGetFeed;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    iget-object v2, p0, Lcom/sec/android/app/sns3/agent/life/command/SnsLifeCmdGetFeed;->mCmdHandler:Landroid/os/Handler;

    const-string v3, "me"

    invoke-direct {v9, v1, v2, v3}, Lcom/sec/android/app/sns3/agent/sp/googleplus/command/SnsGpCmdGetProfileFeed;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Handler;Ljava/lang/String;)V

    .line 387
    .local v9, "gpCmd":Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;
    new-instance v1, Lcom/sec/android/app/sns3/agent/life/command/SnsLifeCmdGetFeed$6;

    invoke-direct {v1, p0}, Lcom/sec/android/app/sns3/agent/life/command/SnsLifeCmdGetFeed$6;-><init>(Lcom/sec/android/app/sns3/agent/life/command/SnsLifeCmdGetFeed;)V

    invoke-virtual {v9, v1}, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->setCommandCallback(Lcom/sec/android/app/sns3/agent/command/ISnsCommandCallback;)Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;

    .line 471
    invoke-virtual {v9}, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->send()I

    .line 474
    new-instance v8, Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdGetProfileFeed;

    iget-object v1, p0, Lcom/sec/android/app/sns3/agent/life/command/SnsLifeCmdGetFeed;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    iget-object v2, p0, Lcom/sec/android/app/sns3/agent/life/command/SnsLifeCmdGetFeed;->mCmdHandler:Landroid/os/Handler;

    const-string v3, "me"

    invoke-direct {v8, v1, v2, v3}, Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdGetProfileFeed;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Handler;Ljava/lang/String;)V

    .line 475
    .local v8, "fsCmd":Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;
    new-instance v1, Lcom/sec/android/app/sns3/agent/life/command/SnsLifeCmdGetFeed$7;

    invoke-direct {v1, p0}, Lcom/sec/android/app/sns3/agent/life/command/SnsLifeCmdGetFeed$7;-><init>(Lcom/sec/android/app/sns3/agent/life/command/SnsLifeCmdGetFeed;)V

    invoke-virtual {v8, v1}, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->setCommandCallback(Lcom/sec/android/app/sns3/agent/command/ISnsCommandCallback;)Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;

    .line 552
    invoke-virtual {v8}, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->send()I

    .line 555
    new-instance v10, Lcom/sec/android/app/sns3/agent/sp/instagram/command/SnsInCmdGetProfileFeed;

    iget-object v1, p0, Lcom/sec/android/app/sns3/agent/life/command/SnsLifeCmdGetFeed;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    iget-object v2, p0, Lcom/sec/android/app/sns3/agent/life/command/SnsLifeCmdGetFeed;->mCmdHandler:Landroid/os/Handler;

    const-string v3, "self"

    invoke-direct {v10, v1, v2, v3}, Lcom/sec/android/app/sns3/agent/sp/instagram/command/SnsInCmdGetProfileFeed;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Handler;Ljava/lang/String;)V

    .line 556
    .local v10, "inCmd":Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;
    new-instance v1, Lcom/sec/android/app/sns3/agent/life/command/SnsLifeCmdGetFeed$8;

    invoke-direct {v1, p0}, Lcom/sec/android/app/sns3/agent/life/command/SnsLifeCmdGetFeed$8;-><init>(Lcom/sec/android/app/sns3/agent/life/command/SnsLifeCmdGetFeed;)V

    invoke-virtual {v10, v1}, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->setCommandCallback(Lcom/sec/android/app/sns3/agent/command/ISnsCommandCallback;)Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;

    .line 635
    invoke-virtual {v10}, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->send()I

    .line 638
    new-instance v11, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetProfileFeeds;

    iget-object v1, p0, Lcom/sec/android/app/sns3/agent/life/command/SnsLifeCmdGetFeed;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/sns3/SnsApplication;->getAgentMgr()Lcom/sec/android/app/sns3/agent/SnsAgentMgr;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/sns3/agent/SnsAgentMgr;->getCommandMgr()Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;->getCommandMgrHandle()Landroid/os/Handler;

    move-result-object v2

    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/sns3/SnsApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v11, v1, v2, v3}, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetProfileFeeds;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Handler;Landroid/content/Context;)V

    .line 641
    .local v11, "liCmd":Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;
    new-instance v1, Lcom/sec/android/app/sns3/agent/life/command/SnsLifeCmdGetFeed$9;

    invoke-direct {v1, p0}, Lcom/sec/android/app/sns3/agent/life/command/SnsLifeCmdGetFeed$9;-><init>(Lcom/sec/android/app/sns3/agent/life/command/SnsLifeCmdGetFeed;)V

    invoke-virtual {v11, v1}, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->setCommandCallback(Lcom/sec/android/app/sns3/agent/command/ISnsCommandCallback;)Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;

    .line 711
    invoke-virtual {v11}, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->send()I

    goto/16 :goto_0
.end method

.method protected respond()Z
    .locals 5

    .prologue
    .line 107
    const-string v0, "SnsAgent"

    const-string v1, "<SnsCmdGetSnsData> respond()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 109
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/life/command/SnsLifeCmdGetFeed;->getCommandCallback()Lcom/sec/android/app/sns3/agent/command/ISnsCommandCallback;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/life/command/SnsLifeCmdGetFeed;->getCommandID()I

    move-result v1

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/life/command/SnsLifeCmdGetFeed;->isSuccess()Z

    move-result v2

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/life/command/SnsLifeCmdGetFeed;->getUri()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/life/command/SnsLifeCmdGetFeed;->getResponseList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/sec/android/app/sns3/agent/command/ISnsCommandCallback;->onCmdRespond(IZLjava/lang/String;Ljava/util/List;)V

    .line 111
    const/4 v0, 0x1

    return v0
.end method
