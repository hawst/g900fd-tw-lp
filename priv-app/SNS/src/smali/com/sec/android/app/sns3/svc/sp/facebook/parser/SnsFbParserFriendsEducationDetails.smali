.class public Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserFriendsEducationDetails;
.super Ljava/lang/Object;
.source "SnsFbParserFriendsEducationDetails.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserFriendsEducationDetails$FacebookFriendEdulists;
    }
.end annotation


# static fields
.field static final FQL_RESULT_SET:Ljava/lang/String; = "fql_result_set"

.field static final QUERY_NAME:Ljava/lang/String; = "name"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    return-void
.end method

.method public static parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsEducationDetails;
    .locals 22
    .param p0, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 55
    const/4 v4, 0x0

    .line 56
    .local v4, "friendEdulists":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsEducationDetails;
    const/4 v2, 0x0

    .line 57
    .local v2, "curFriendEdulists":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsEducationDetails;
    const/16 v16, 0x0

    .line 60
    .local v16, "newFriendEdulists":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsEducationDetails;
    :try_start_0
    new-instance v7, Lorg/json/JSONArray;

    move-object/from16 v0, p0

    invoke-direct {v7, v0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 61
    .local v7, "jArr":Lorg/json/JSONArray;
    const/4 v6, 0x0

    .local v6, "j":I
    :goto_0
    invoke-virtual {v7}, Lorg/json/JSONArray;->length()I

    move-result v20

    move/from16 v0, v20

    if-ge v6, v0, :cond_9

    .line 63
    invoke-virtual {v7, v6}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v20

    const-string v21, "name"

    invoke-virtual/range {v20 .. v21}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    .line 64
    .local v18, "queryName":Ljava/lang/String;
    invoke-virtual {v7, v6}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v20

    const-string v21, "fql_result_set"

    invoke-virtual/range {v20 .. v21}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    .line 67
    .local v19, "queryObject":Ljava/lang/String;
    const-string v20, "user_query"

    move-object/from16 v0, v20

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v20

    if-eqz v20, :cond_8

    .line 69
    new-instance v9, Lorg/json/JSONArray;

    move-object/from16 v0, v19

    invoke-direct {v9, v0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 71
    .local v9, "ja":Lorg/json/JSONArray;
    if-eqz v9, :cond_8

    .line 73
    const/4 v5, 0x0

    .local v5, "i":I
    move-object/from16 v17, v16

    .end local v16    # "newFriendEdulists":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsEducationDetails;
    .local v17, "newFriendEdulists":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsEducationDetails;
    :goto_1
    :try_start_1
    invoke-virtual {v9}, Lorg/json/JSONArray;->length()I

    move-result v20

    move/from16 v0, v20

    if-ge v5, v0, :cond_7

    .line 75
    invoke-virtual {v9, v5}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v12

    .line 77
    .local v12, "jsonObj":Lorg/json/JSONObject;
    const-string v20, "education"

    move-object/from16 v0, v20

    invoke-virtual {v12, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v20

    if-eqz v20, :cond_5

    const-string v20, "education"

    move-object/from16 v0, v20

    invoke-virtual {v12, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    const-string v21, ""

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_0

    const-string v20, "education"

    move-object/from16 v0, v20

    invoke-virtual {v12, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    const-string v21, "null"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-nez v20, :cond_5

    .line 84
    :cond_0
    new-instance v8, Lorg/json/JSONArray;

    const-string v20, "education"

    move-object/from16 v0, v20

    invoke-virtual {v12, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-direct {v8, v0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 87
    .local v8, "jArrEdu":Lorg/json/JSONArray;
    if-eqz v8, :cond_a

    .line 88
    const/4 v15, 0x0

    .local v15, "k":I
    :goto_2
    invoke-virtual {v8}, Lorg/json/JSONArray;->length()I

    move-result v20

    move/from16 v0, v20

    if-ge v15, v0, :cond_a

    .line 89
    new-instance v16, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsEducationDetails;

    invoke-direct/range {v16 .. v16}, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsEducationDetails;-><init>()V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    .line 90
    .end local v17    # "newFriendEdulists":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsEducationDetails;
    .restart local v16    # "newFriendEdulists":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsEducationDetails;
    :try_start_2
    invoke-virtual {v8, v15}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v11

    .line 92
    .local v11, "jsonEduObj":Lorg/json/JSONObject;
    const-string v20, "school"

    move-object/from16 v0, v20

    invoke-virtual {v11, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v20

    if-eqz v20, :cond_1

    .line 94
    new-instance v13, Lorg/json/JSONObject;

    const-string v20, "school"

    move-object/from16 v0, v20

    invoke-virtual {v11, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-direct {v13, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 98
    .local v13, "jsonSchoolObj":Lorg/json/JSONObject;
    if-eqz v13, :cond_1

    .line 99
    const-string v20, "id"

    move-object/from16 v0, v20

    invoke-virtual {v13, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, v16

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsEducationDetails;->mSchoolId:Ljava/lang/String;

    .line 101
    const-string v20, "name"

    move-object/from16 v0, v20

    invoke-virtual {v13, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, v16

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsEducationDetails;->mSchoolName:Ljava/lang/String;

    .line 106
    .end local v13    # "jsonSchoolObj":Lorg/json/JSONObject;
    :cond_1
    const-string v20, "degree"

    move-object/from16 v0, v20

    invoke-virtual {v11, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v20

    if-eqz v20, :cond_2

    .line 109
    new-instance v10, Lorg/json/JSONObject;

    const-string v20, "degree"

    move-object/from16 v0, v20

    invoke-virtual {v11, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-direct {v10, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 112
    .local v10, "jsonDegreeObj":Lorg/json/JSONObject;
    if-eqz v10, :cond_2

    .line 114
    const-string v20, "id"

    move-object/from16 v0, v20

    invoke-virtual {v10, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, v16

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsEducationDetails;->mDegreeId:Ljava/lang/String;

    .line 116
    const-string v20, "name"

    move-object/from16 v0, v20

    invoke-virtual {v10, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, v16

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsEducationDetails;->mDegreeName:Ljava/lang/String;

    .line 121
    .end local v10    # "jsonDegreeObj":Lorg/json/JSONObject;
    :cond_2
    const-string v20, "year"

    move-object/from16 v0, v20

    invoke-virtual {v11, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v20

    if-eqz v20, :cond_3

    .line 124
    new-instance v14, Lorg/json/JSONObject;

    const-string v20, "year"

    move-object/from16 v0, v20

    invoke-virtual {v11, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-direct {v14, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 127
    .local v14, "jsonYearObj":Lorg/json/JSONObject;
    if-eqz v14, :cond_3

    .line 129
    const-string v20, "id"

    move-object/from16 v0, v20

    invoke-virtual {v14, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, v16

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsEducationDetails;->mYearId:Ljava/lang/String;

    .line 131
    const-string v20, "name"

    move-object/from16 v0, v20

    invoke-virtual {v14, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, v16

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsEducationDetails;->mYearName:Ljava/lang/String;

    .line 137
    .end local v14    # "jsonYearObj":Lorg/json/JSONObject;
    :cond_3
    const-string v20, "type"

    move-object/from16 v0, v20

    invoke-virtual {v11, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, v16

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsEducationDetails;->mType:Ljava/lang/String;

    .line 139
    const-string v20, "uid"

    move-object/from16 v0, v20

    invoke-virtual {v12, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, v16

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsEducationDetails;->mFriendID:Ljava/lang/String;

    .line 141
    const-string v20, "name"

    move-object/from16 v0, v20

    invoke-virtual {v12, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, v16

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsEducationDetails;->mName:Ljava/lang/String;

    .line 144
    if-nez v4, :cond_4

    .line 145
    move-object/from16 v4, v16

    .line 146
    move-object v2, v4

    .line 88
    :goto_3
    add-int/lit8 v15, v15, 0x1

    move-object/from16 v17, v16

    .end local v16    # "newFriendEdulists":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsEducationDetails;
    .restart local v17    # "newFriendEdulists":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsEducationDetails;
    goto/16 :goto_2

    .line 148
    .end local v17    # "newFriendEdulists":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsEducationDetails;
    .restart local v16    # "newFriendEdulists":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsEducationDetails;
    :cond_4
    move-object/from16 v0, v16

    iput-object v0, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsEducationDetails;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsEducationDetails;

    .line 149
    iget-object v2, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsEducationDetails;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsEducationDetails;
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_3

    .line 155
    .end local v8    # "jArrEdu":Lorg/json/JSONArray;
    .end local v11    # "jsonEduObj":Lorg/json/JSONObject;
    .end local v15    # "k":I
    .end local v16    # "newFriendEdulists":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsEducationDetails;
    .restart local v17    # "newFriendEdulists":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsEducationDetails;
    :cond_5
    :try_start_3
    new-instance v16, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsEducationDetails;

    invoke-direct/range {v16 .. v16}, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsEducationDetails;-><init>()V
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_1

    .line 156
    .end local v17    # "newFriendEdulists":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsEducationDetails;
    .restart local v16    # "newFriendEdulists":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsEducationDetails;
    :try_start_4
    const-string v20, "uid"

    move-object/from16 v0, v20

    invoke-virtual {v12, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, v16

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsEducationDetails;->mFriendID:Ljava/lang/String;

    .line 158
    const-string v20, "name"

    move-object/from16 v0, v20

    invoke-virtual {v12, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, v16

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsEducationDetails;->mName:Ljava/lang/String;

    .line 161
    if-nez v4, :cond_6

    .line 162
    move-object/from16 v4, v16

    .line 163
    move-object v2, v4

    .line 73
    :goto_4
    add-int/lit8 v5, v5, 0x1

    move-object/from16 v17, v16

    .end local v16    # "newFriendEdulists":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsEducationDetails;
    .restart local v17    # "newFriendEdulists":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsEducationDetails;
    goto/16 :goto_1

    .line 165
    .end local v17    # "newFriendEdulists":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsEducationDetails;
    .restart local v16    # "newFriendEdulists":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsEducationDetails;
    :cond_6
    move-object/from16 v0, v16

    iput-object v0, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsEducationDetails;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsEducationDetails;

    .line 166
    iget-object v2, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsEducationDetails;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsEducationDetails;
    :try_end_4
    .catch Lorg/json/JSONException; {:try_start_4 .. :try_end_4} :catch_0

    goto :goto_4

    .end local v12    # "jsonObj":Lorg/json/JSONObject;
    .end local v16    # "newFriendEdulists":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsEducationDetails;
    .restart local v17    # "newFriendEdulists":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsEducationDetails;
    :cond_7
    move-object/from16 v16, v17

    .line 61
    .end local v5    # "i":I
    .end local v9    # "ja":Lorg/json/JSONArray;
    .end local v17    # "newFriendEdulists":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsEducationDetails;
    .restart local v16    # "newFriendEdulists":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsEducationDetails;
    :cond_8
    add-int/lit8 v6, v6, 0x1

    goto/16 :goto_0

    .line 175
    .end local v6    # "j":I
    .end local v7    # "jArr":Lorg/json/JSONArray;
    .end local v18    # "queryName":Ljava/lang/String;
    .end local v19    # "queryObject":Ljava/lang/String;
    :catch_0
    move-exception v3

    .line 177
    .local v3, "e":Lorg/json/JSONException;
    :goto_5
    invoke-virtual {v3}, Lorg/json/JSONException;->printStackTrace()V

    .line 180
    .end local v3    # "e":Lorg/json/JSONException;
    :cond_9
    return-object v4

    .line 175
    .end local v16    # "newFriendEdulists":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsEducationDetails;
    .restart local v5    # "i":I
    .restart local v6    # "j":I
    .restart local v7    # "jArr":Lorg/json/JSONArray;
    .restart local v9    # "ja":Lorg/json/JSONArray;
    .restart local v17    # "newFriendEdulists":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsEducationDetails;
    .restart local v18    # "queryName":Ljava/lang/String;
    .restart local v19    # "queryObject":Ljava/lang/String;
    :catch_1
    move-exception v3

    move-object/from16 v16, v17

    .end local v17    # "newFriendEdulists":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsEducationDetails;
    .restart local v16    # "newFriendEdulists":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsEducationDetails;
    goto :goto_5

    .end local v16    # "newFriendEdulists":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsEducationDetails;
    .restart local v8    # "jArrEdu":Lorg/json/JSONArray;
    .restart local v12    # "jsonObj":Lorg/json/JSONObject;
    .restart local v17    # "newFriendEdulists":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsEducationDetails;
    :cond_a
    move-object/from16 v16, v17

    .end local v17    # "newFriendEdulists":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsEducationDetails;
    .restart local v16    # "newFriendEdulists":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsEducationDetails;
    goto :goto_4
.end method
