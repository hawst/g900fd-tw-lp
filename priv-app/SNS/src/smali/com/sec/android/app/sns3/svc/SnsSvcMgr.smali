.class public Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
.super Ljava/lang/Object;
.source "SnsSvcMgr.java"


# static fields
.field public static final TAG:Ljava/lang/String; = "SNS"


# instance fields
.field private mHttpMgr:Lcom/sec/android/app/sns3/svc/http/SnsHttpMgr;

.field private mRequestMgr:Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;

.field private mTokenMgr:Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    new-instance v0, Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;

    invoke-direct {v0}, Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/SnsSvcMgr;->mTokenMgr:Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;

    .line 40
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/SnsSvcMgr;->mTokenMgr:Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;->initialize()V

    .line 42
    new-instance v0, Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;

    invoke-direct {v0}, Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/SnsSvcMgr;->mRequestMgr:Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;

    .line 43
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/SnsSvcMgr;->mRequestMgr:Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;

    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/SnsSvcMgr;->mTokenMgr:Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;->initialize(Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;)V

    .line 45
    new-instance v0, Lcom/sec/android/app/sns3/svc/http/SnsHttpMgr;

    invoke-direct {v0}, Lcom/sec/android/app/sns3/svc/http/SnsHttpMgr;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/SnsSvcMgr;->mHttpMgr:Lcom/sec/android/app/sns3/svc/http/SnsHttpMgr;

    .line 47
    return-void
.end method


# virtual methods
.method public getHttpMgr()Lcom/sec/android/app/sns3/svc/http/SnsHttpMgr;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/SnsSvcMgr;->mHttpMgr:Lcom/sec/android/app/sns3/svc/http/SnsHttpMgr;

    return-object v0
.end method

.method public getRequestMgr()Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/SnsSvcMgr;->mRequestMgr:Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;

    return-object v0
.end method

.method public getRequestMgrHandler()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/SnsSvcMgr;->mRequestMgr:Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;->getRequestMgrHandle()Landroid/os/Handler;

    move-result-object v0

    return-object v0
.end method

.method public getTokenMgr()Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/SnsSvcMgr;->mTokenMgr:Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;

    return-object v0
.end method
