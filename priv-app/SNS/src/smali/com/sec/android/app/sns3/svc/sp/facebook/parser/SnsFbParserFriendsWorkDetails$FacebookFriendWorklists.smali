.class public interface abstract Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserFriendsWorkDetails$FacebookFriendWorklists;
.super Ljava/lang/Object;
.source "SnsFbParserFriendsWorkDetails.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserFriendsWorkDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "FacebookFriendWorklists"
.end annotation


# static fields
.field public static final EMPLOYER:Ljava/lang/String; = "employer"

.field public static final END_DATE:Ljava/lang/String; = "end_date"

.field public static final FROM:Ljava/lang/String; = "from"

.field public static final ID:Ljava/lang/String; = "uid"

.field public static final ID1:Ljava/lang/String; = "id"

.field public static final LOCATION:Ljava/lang/String; = "location"

.field public static final NAME:Ljava/lang/String; = "name"

.field public static final POSITION:Ljava/lang/String; = "position"

.field public static final START_DATE:Ljava/lang/String; = "start_date"

.field public static final WORK:Ljava/lang/String; = "work"
