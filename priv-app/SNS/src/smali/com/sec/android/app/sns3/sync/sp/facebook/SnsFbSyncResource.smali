.class public Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncResource;
.super Ljava/lang/Object;
.source "SnsFbSyncResource.java"


# static fields
.field public static final CONTENT_URI:Ljava/lang/String; = "SNS3_CONTENT_URI"

.field public static final CONTENT_URI_ALBUM:Ljava/lang/String; = "SNS3_CONTENT_URI_ALBUM"

.field public static final CONTENT_URI_COMMENTS:Ljava/lang/String; = "SNS3_CONTENT_URI_COMMENTS"

.field public static final CONTENT_URI_EDUCATION:Ljava/lang/String; = "SNS3_CONTENT_URI_EDUCATION"

.field public static final CONTENT_URI_FEEDS:Ljava/lang/String; = "SNS3_CONTENT_URI_FEEDS"

.field public static final CONTENT_URI_FRIENDLISTS:Ljava/lang/String; = "SNS3_CONTENT_URI_FRIENDLISTS"

.field public static final CONTENT_URI_FRIENDS:Ljava/lang/String; = "SNS3_CONTENT_URI_FRIENDS"

.field public static final CONTENT_URI_GROUPS:Ljava/lang/String; = "SNS3_CONTENT_URI_GROUPS"

.field public static final CONTENT_URI_HOME:Ljava/lang/String; = "SNS3_CONTENT_URI_HOME"

.field public static final CONTENT_URI_PHOTO:Ljava/lang/String; = "SNS3_CONTENT_URI_PHOTO"

.field public static final CONTENT_URI_PHOTOS_OF_USER:Ljava/lang/String; = "SNS3_CONTENT_URI_PHOTOS_OF_USER"

.field public static final CONTENT_URI_POSTS:Ljava/lang/String; = "SNS3_CONTENT_URI_POSTS"

.field public static final CONTENT_URI_WORK:Ljava/lang/String; = "SNS3_CONTENT_URI_WORK"

.field public static final RESULT:Ljava/lang/String; = "SNS_RESULT"

.field public static final RETRY_LOGIN:Ljava/lang/String; = "RetryLogin"

.field public static final RETRY_LOGIN_ACTION:Ljava/lang/String; = "com.sec.android.app.sns3.RETRY_LOGIN_FACEBOOK"

.field public static final RETRY_SSO_ACTION:Ljava/lang/String; = "com.sec.android.app.sns3.RETRY_SSO_FACEBOOK"

.field public static final SNS_BROADCAST_PERMISSION:Ljava/lang/String; = "com.sec.android.app.sns3.permission.RECEIVE_SNS_BROADCAST"

.field public static final SYNC_TYPE_ALBUM:Ljava/lang/String; = "album"

.field public static final SYNC_TYPE_CONTACT:Ljava/lang/String; = "contact"

.field public static final SYNC_TYPE_EVENT:Ljava/lang/String; = "event"

.field public static final SYNC_TYPE_PHOTO:Ljava/lang/String; = "photo"

.field public static final SYNC_TYPE_STREAMS:Ljava/lang/String; = "streams"

.field public static final UPDATE_ALBUM:Ljava/lang/String; = "com.sec.android.app.sns3.action.SYNC_GALLERY"

.field public static final UPDATE_EVENT:Ljava/lang/String; = "com.sec.android.app.sns3.action.SYNC_EVENT"

.field public static final UPDATE_HOME_FEEDS:Ljava/lang/String; = "com.sec.android.app.sns3.action.SYNC_FB_HOME_FEEDS"

.field public static final UPDATE_PROFILES:Ljava/lang/String; = "com.sec.android.app.sns3.action.SYNC_FB_PROFILES"

.field public static final UPDATE_PROFILE_FEEDS:Ljava/lang/String; = "com.sec.android.app.sns3.action.SYNC_FB_PROFILE_FEEDS"

.field public static final UPDATE_STREAMS:Ljava/lang/String; = "com.sec.android.app.sns3.action.SYNC_FB_STREAMS"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
