.class public Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserTabs;
.super Ljava/lang/Object;
.source "SnsFbParserTabs.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserTabs$FacebookTabs;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    return-void
.end method

.method public static parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTabs;
    .locals 12
    .param p0, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 54
    const/4 v8, 0x0

    .line 55
    .local v8, "tabs":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTabs;
    const/4 v0, 0x0

    .line 58
    .local v0, "curTabs":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTabs;
    :try_start_0
    new-instance v6, Lorg/json/JSONObject;

    invoke-direct {v6, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 59
    .local v6, "jsonObject":Lorg/json/JSONObject;
    new-instance v3, Lorg/json/JSONArray;

    const-string v9, "data"

    invoke-virtual {v6, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v3, v9}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 61
    .local v3, "ja":Lorg/json/JSONArray;
    if-eqz v3, :cond_1

    .line 62
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v9

    if-ge v2, v9, :cond_1

    .line 64
    new-instance v7, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTabs;

    invoke-direct {v7}, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTabs;-><init>()V

    .line 65
    .local v7, "newTab":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTabs;
    invoke-virtual {v3, v2}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v5

    .line 67
    .local v5, "jsonObj":Lorg/json/JSONObject;
    const-string v9, "id"

    invoke-virtual {v5, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v7, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTabs;->mID:Ljava/lang/String;

    .line 68
    const-string v9, "name"

    invoke-virtual {v5, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v7, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTabs;->mName:Ljava/lang/String;

    .line 69
    const-string v9, "link"

    invoke-virtual {v5, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v7, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTabs;->mLink:Ljava/lang/String;

    .line 70
    const-string v9, "custom_name"

    invoke-virtual {v5, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v7, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTabs;->mCustomName:Ljava/lang/String;

    .line 71
    const-string v9, "is_permanent"

    invoke-virtual {v5, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v7, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTabs;->mIsPermanent:Ljava/lang/String;

    .line 72
    const-string v9, "position"

    invoke-virtual {v5, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v7, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTabs;->mPosition:Ljava/lang/String;

    .line 73
    const-string v9, "is_non_connection_landing_tab"

    invoke-virtual {v5, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v7, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTabs;->mIsNonConnectionLandingTab:Ljava/lang/String;

    .line 75
    const-string v9, "application"

    invoke-virtual {v5, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserApplication;->parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseApplication;

    move-result-object v9

    iput-object v9, v7, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTabs;->mApplication:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseApplication;

    .line 77
    if-nez v8, :cond_0

    .line 78
    move-object v8, v7

    .line 79
    move-object v0, v8

    .line 62
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 81
    :cond_0
    iput-object v7, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTabs;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTabs;

    .line 82
    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTabs;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTabs;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_1

    .line 86
    .end local v2    # "i":I
    .end local v3    # "ja":Lorg/json/JSONArray;
    .end local v5    # "jsonObj":Lorg/json/JSONObject;
    .end local v6    # "jsonObject":Lorg/json/JSONObject;
    .end local v7    # "newTab":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTabs;
    :catch_0
    move-exception v4

    .line 87
    .local v4, "je":Lorg/json/JSONException;
    const-string v9, "SNS"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "parser : JSONException error :"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v4}, Lorg/json/JSONException;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 92
    .end local v4    # "je":Lorg/json/JSONException;
    :cond_1
    :goto_2
    return-object v8

    .line 88
    :catch_1
    move-exception v1

    .line 89
    .local v1, "e":Ljava/lang/Exception;
    const-string v9, "SNS"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "parser : Exception error : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method
