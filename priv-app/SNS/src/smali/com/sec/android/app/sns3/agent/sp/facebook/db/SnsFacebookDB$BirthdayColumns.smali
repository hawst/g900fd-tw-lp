.class public interface abstract Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$BirthdayColumns;
.super Ljava/lang/Object;
.source "SnsFacebookDB.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "BirthdayColumns"
.end annotation


# static fields
.field public static final BIRTHDAY_DATE:Ljava/lang/String; = "birthday_date"

.field public static final FIRST_NAME:Ljava/lang/String; = "first_name"

.field public static final LAST_NAME:Ljava/lang/String; = "last_name"

.field public static final NAME:Ljava/lang/String; = "name"

.field public static final PIC_URL:Ljava/lang/String; = "pic_url"

.field public static final UID:Ljava/lang/String; = "uid"
