.class Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPhoto$1$1;
.super Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqGetLikeDone;
.source "SnsFbCmdGetPhoto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPhoto$1;->onReqRespond(IZIILandroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseConvertID;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPhoto$1;


# direct methods
.method constructor <init>(Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPhoto$1;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;)V
    .locals 0
    .param p2, "x0"    # Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    .param p3, "x1"    # Ljava/lang/String;

    .prologue
    .line 79
    iput-object p1, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPhoto$1$1;->this$1:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPhoto$1;

    invoke-direct {p0, p2, p3}, Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqGetLikeDone;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public onReqRespond(IZIILandroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseBoolean;)Z
    .locals 8
    .param p1, "reqID"    # I
    .param p2, "bSuccess"    # Z
    .param p3, "httpStatus"    # I
    .param p4, "errorCode"    # I
    .param p5, "reason"    # Landroid/os/Bundle;
    .param p6, "bool"    # Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseBoolean;

    .prologue
    const/4 v2, 0x0

    .line 85
    if-eqz p2, :cond_2

    .line 86
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/sns3/SnsApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 88
    .local v0, "cr":Landroid/content/ContentResolver;
    new-instance v7, Landroid/content/ContentValues;

    invoke-direct {v7}, Landroid/content/ContentValues;-><init>()V

    .line 90
    .local v7, "values":Landroid/content/ContentValues;
    sget-object v1, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$Photo;->CONTENT_URI:Landroid/net/Uri;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "id= \'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPhoto$1$1;->this$1:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPhoto$1;

    iget-object v4, v4, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPhoto$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPhoto;

    # getter for: Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPhoto;->mPhotoID:Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPhoto;->access$000(Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPhoto;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 93
    .local v6, "c":Landroid/database/Cursor;
    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_1

    .line 94
    const-string v1, "like_done"

    iget-object v3, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPhoto$1$1;->this$1:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPhoto$1;

    iget-object v3, v3, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPhoto$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPhoto;

    # getter for: Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPhoto;->mLikeDone:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPhoto;->access$100(Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPhoto;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v7, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    sget-object v1, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$Post;->CONTENT_URI:Landroid/net/Uri;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "id= \'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPhoto$1$1;->this$1:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPhoto$1;

    iget-object v4, v4, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPhoto$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPhoto;

    # getter for: Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPhoto;->mPhotoID:Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPhoto;->access$000(Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPhoto;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v7, v3, v2}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 100
    :goto_0
    if-eqz v6, :cond_0

    .line 101
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 109
    .end local v0    # "cr":Landroid/content/ContentResolver;
    .end local v6    # "c":Landroid/database/Cursor;
    .end local v7    # "values":Landroid/content/ContentValues;
    :cond_0
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPhoto$1$1;->this$1:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPhoto$1;

    iget-object v1, v1, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPhoto$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPhoto;

    invoke-virtual {v1, p2}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPhoto;->setSuccess(Z)V

    .line 110
    iget-object v1, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPhoto$1$1;->this$1:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPhoto$1;

    iget-object v1, v1, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPhoto$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPhoto;

    invoke-virtual {v1, p0}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPhoto;->receive(Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;)V

    .line 112
    const/4 v1, 0x1

    return v1

    .line 98
    .restart local v0    # "cr":Landroid/content/ContentResolver;
    .restart local v6    # "c":Landroid/database/Cursor;
    .restart local v7    # "values":Landroid/content/ContentValues;
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPhoto$1$1;->this$1:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPhoto$1;

    iget-object v1, v1, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPhoto$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPhoto;

    iget-object v2, p6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseBoolean;->mBeSuccess:Ljava/lang/String;

    # setter for: Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPhoto;->mLikeDone:Ljava/lang/String;
    invoke-static {v1, v2}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPhoto;->access$102(Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPhoto;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0

    .line 104
    .end local v0    # "cr":Landroid/content/ContentResolver;
    .end local v6    # "c":Landroid/database/Cursor;
    .end local v7    # "values":Landroid/content/ContentValues;
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPhoto$1$1;->this$1:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPhoto$1;

    iget-object v1, v1, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPhoto$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPhoto;

    new-instance v3, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;

    const-string v4, "facebook"

    invoke-direct {v3, v4, p3, p4, p5}, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;-><init>(Ljava/lang/String;IILandroid/os/Bundle;)V

    invoke-virtual {v1, v3}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPhoto;->setResponseList(Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;)Z

    .line 106
    iget-object v1, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPhoto$1$1;->this$1:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPhoto$1;

    iget-object v1, v1, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPhoto$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPhoto;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPhoto;->setUri(Ljava/lang/String;)V

    goto :goto_1
.end method
