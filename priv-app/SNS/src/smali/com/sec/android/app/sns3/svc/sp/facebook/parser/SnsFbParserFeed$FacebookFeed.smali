.class public interface abstract Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserFeed$FacebookFeed;
.super Ljava/lang/Object;
.source "SnsFbParserFeed.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserFeed;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "FacebookFeed"
.end annotation


# static fields
.field public static final CAPTION:Ljava/lang/String; = "caption"

.field public static final CITY:Ljava/lang/String; = "city"

.field public static final COUNTRY:Ljava/lang/String; = "country"

.field public static final CREATED_TIME:Ljava/lang/String; = "created_time"

.field public static final DATA:Ljava/lang/String; = "data"

.field public static final DESCRIPTION:Ljava/lang/String; = "description"

.field public static final FEED_ID:Ljava/lang/String; = "id"

.field public static final FROM:Ljava/lang/String; = "from"

.field public static final ICON:Ljava/lang/String; = "icon"

.field public static final LINK:Ljava/lang/String; = "link"

.field public static final LOCATION_LATITUDE:Ljava/lang/String; = "latitude"

.field public static final LOCATION_LONGITUDE:Ljava/lang/String; = "longitude"

.field public static final LOCATION_NAME:Ljava/lang/String; = "name"

.field public static final MESSAGE:Ljava/lang/String; = "message"

.field public static final NAME:Ljava/lang/String; = "name"

.field public static final PHOTO_ID:Ljava/lang/String; = "object_id"

.field public static final PICTURE:Ljava/lang/String; = "picture"

.field public static final SOURCE:Ljava/lang/String; = "source"

.field public static final STATUS_TYPE:Ljava/lang/String; = "status_type"

.field public static final STORY:Ljava/lang/String; = "story"

.field public static final TYPE:Ljava/lang/String; = "type"

.field public static final UPDATED_TIME:Ljava/lang/String; = "updated_time"
