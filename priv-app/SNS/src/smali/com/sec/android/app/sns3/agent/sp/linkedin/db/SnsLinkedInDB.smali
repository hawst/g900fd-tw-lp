.class public Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB;
.super Ljava/lang/Object;
.source "SnsLinkedInDB.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$PeopleLookUpColumns;,
        Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$PeopleLookUp;,
        Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$StatusStreamColumns;,
        Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$StatusStream;,
        Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$HomeFeedListColumns;,
        Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$HomeFeedList;,
        Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$ProfileFeedListColumns;,
        Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$ProfileFeedList;,
        Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$FeedListColumns;,
        Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$FeedList;,
        Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$RecommendationUpdatesColumns;,
        Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$RecommendationInfo;,
        Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$ViralUpdatesColumns;,
        Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$ViralUpdates;,
        Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$CompanyUpdatesColumns;,
        Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$CompanyUpdates;,
        Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$CompaniesColumns;,
        Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$CompanyInfo;,
        Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$JobsUpdatesColumns;,
        Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$JobInfo;,
        Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$AppsUpdatesColumns;,
        Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$AppInfo;,
        Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$educationColumns;,
        Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$Education;,
        Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$GroupUpdateColumns;,
        Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$GroupUpdates;,
        Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$ConnectionUpdateColumns;,
        Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$ConnectionUpdates;,
        Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$CommentColumns;,
        Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$Comments;,
        Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$LikeColumns;,
        Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$Likes;,
        Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$ShareUpdateColumns;,
        Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$ShareUpdates;,
        Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$UpdateColumns;,
        Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$Updates;,
        Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$GroupColumns;,
        Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$GroupInfo;,
        Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$ContactColumns;,
        Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$ContactInfo;,
        Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$FollowingListColumns;,
        Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$FollowingList;,
        Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$SkillsColumns;,
        Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$SkillsInfo;,
        Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$ConnectionsColumns;,
        Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$Connections;,
        Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$UserWorkInfoColumns;,
        Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$USerWorkInfo;,
        Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$UserBasicInfoColumns;,
        Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$UserBasicInfo;,
        Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$WIPE_LINKEDIN_DATA;
    }
.end annotation


# static fields
.field public static final APPS_UPDATES_INFO_TABLE_NAME:Ljava/lang/String; = "user_apps_updates"

.field public static final AUTHORITY:Ljava/lang/String; = "com.sec.android.app.sns3.sp.linkedin"

.field public static final COMMENTS_INFO_TABLE_NAME:Ljava/lang/String; = "user_comments"

.field public static final COMPANIES_INFO_TABLE_NAME:Ljava/lang/String; = "user_companies"

.field public static final COMPANY_UPDATES_TABLE_NAME:Ljava/lang/String; = "company_updates"

.field public static final CONNECTION_UPDATES_INFO_TABLE_NAME:Ljava/lang/String; = "user_connection_updates"

.field public static final DATABASE_NAME:Ljava/lang/String; = "snsLinkedInDB.db"

.field public static final DATABASE_VERSION:I = 0x3

.field public static final EDUCATION_INFO_TABLE_NAME:Ljava/lang/String; = "education"

.field public static final FEED_LIST_TABLE_NAME:Ljava/lang/String; = "feed_list"

.field public static final FOLLOWING_LIST_TABLE_NAME:Ljava/lang/String; = "followings"

.field public static final GROUP_UPDATES_INFO_TABLE_NAME:Ljava/lang/String; = "group_connection_updates"

.field public static final HOME_FEED_LIST_TABLE_NAME:Ljava/lang/String; = "home_feed"

.field public static final JOBS_UPDATES_INFO_TABLE_NAME:Ljava/lang/String; = "user_jobs_updates"

.field public static final LIFE_AUTHORITY:Ljava/lang/String; = "com.sec.android.app.sns3.life"

.field public static final LIKES_INFO_TABLE_NAME:Ljava/lang/String; = "user_likes"

.field public static final PEOPLE_LOOKUP_TABLE_NAME:Ljava/lang/String; = "people_lookup"

.field public static final PROFILES_AUTHORITY:Ljava/lang/String; = "com.sec.android.app.sns3.profiles"

.field public static final PROFILE_FEED_LIST_TABLE_NAME:Ljava/lang/String; = "profile_feed_list"

.field public static final RECOMMENDATION_UPDATES_INFO_TABLE_NAME:Ljava/lang/String; = "user_recommendation_updates"

.field public static final SHARE_UPDATES_INFO_TABLE_NAME:Ljava/lang/String; = "user_share_updates"

.field public static final STATUS_STREAM_TABLE_NAME:Ljava/lang/String; = "status_stream"

.field public static final UPDATES_INFO_TABLE_NAME:Ljava/lang/String; = "user_updates"

.field public static final USER_BASIC_INFO_TABLE_NAME:Ljava/lang/String; = "user_basic_info"

.field public static final USER_CONNECTIONS_TABLE_NAME:Ljava/lang/String; = "user_connections"

.field public static final USER_CONTACTS_INFO_TABLE_NAME:Ljava/lang/String; = "user_contact"

.field public static final USER_GROUPS_INFO_TABLE_NAME:Ljava/lang/String; = "user_groups"

.field public static final USER_SKILLS_TABLE_NAME:Ljava/lang/String; = "user_skills"

.field public static final USER_WORK_INFO_TABLE_NAME:Ljava/lang/String; = "user_work_info"

.field public static final VIRAL_UPDATES_TABLE_NAME:Ljava/lang/String; = "viral_updates"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 827
    return-void
.end method
