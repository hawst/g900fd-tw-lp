.class public interface abstract Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserNote$FacebookNote;
.super Ljava/lang/Object;
.source "SnsFbParserNote.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserNote;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "FacebookNote"
.end annotation


# static fields
.field public static final CAN_DELETE:Ljava/lang/String; = "can_delete"

.field public static final CAN_EDIT:Ljava/lang/String; = "can_edit"

.field public static final COMMENTS:Ljava/lang/String; = "comments"

.field public static final COUNT:Ljava/lang/String; = "count"

.field public static final CREATED_TIME:Ljava/lang/String; = "created_time"

.field public static final DATA:Ljava/lang/String; = "data"

.field public static final FROM:Ljava/lang/String; = "from"

.field public static final FROM_ID:Ljava/lang/String; = "id"

.field public static final FROM_NAME:Ljava/lang/String; = "name"

.field public static final ICON:Ljava/lang/String; = "icon"

.field public static final MESSAGE:Ljava/lang/String; = "message"

.field public static final NOTE_ID:Ljava/lang/String; = "id"

.field public static final REVISION:Ljava/lang/String; = "revision"

.field public static final SUBJECT:Ljava/lang/String; = "subject"

.field public static final UPDATED_TIME:Ljava/lang/String; = "updated_time"
