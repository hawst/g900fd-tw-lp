.class public interface abstract Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserComment$FacebookComment;
.super Ljava/lang/Object;
.source "SnsFbParserComment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserComment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "FacebookComment"
.end annotation


# static fields
.field public static final CAN_REMOVE:Ljava/lang/String; = "can_remove"

.field public static final CREATED_TIME:Ljava/lang/String; = "created_time"

.field public static final DATA:Ljava/lang/String; = "data"

.field public static final FROM:Ljava/lang/String; = "from"

.field public static final ID:Ljava/lang/String; = "id"

.field public static final LIKES:Ljava/lang/String; = "likes"

.field public static final MESSAGE:Ljava/lang/String; = "message"

.field public static final USER_LIKES:Ljava/lang/String; = "user_likes"
