.class public interface abstract Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserThread$FacebookThread;
.super Ljava/lang/Object;
.source "SnsFbParserThread.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserThread;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "FacebookThread"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserThread$FacebookThread$SnsFbUserQuery;,
        Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserThread$FacebookThread$SnsFbMsgQuery;
    }
.end annotation


# static fields
.field public static final FQL_RESULT_SET:Ljava/lang/String; = "fql_result_set"

.field public static final MSG_QUERY_NAME:Ljava/lang/String; = "thread_query"

.field public static final QUERY_NAME:Ljava/lang/String; = "name"

.field public static final USER_QUERY_NAME:Ljava/lang/String; = "user_query"
