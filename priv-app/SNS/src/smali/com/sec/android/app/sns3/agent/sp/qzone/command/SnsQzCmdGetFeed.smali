.class public Lcom/sec/android/app/sns3/agent/sp/qzone/command/SnsQzCmdGetFeed;
.super Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;
.source "SnsQzCmdGetFeed.java"


# static fields
.field private static final MIME_TYPE_PHONE_V2:Ljava/lang/String; = "vnd.android.cursor.item/phone_v2"

.field private static final QZONE_ACCOUNT_TYPE:Ljava/lang/String; = "com.qzone.account"

.field public static timeStampformat:Ljava/text/SimpleDateFormat;


# instance fields
.field private mFeedCount:I

.field private mUserID:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 50
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyy-MM-dd\'T\'HH:mm:ssZ"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/agent/sp/qzone/command/SnsQzCmdGetFeed;->timeStampformat:Ljava/text/SimpleDateFormat;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Handler;Ljava/lang/String;I)V
    .locals 4
    .param p1, "svcMgr"    # Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    .param p2, "cmdHandler"    # Landroid/os/Handler;
    .param p3, "userID"    # Ljava/lang/String;
    .param p4, "feedCount"    # I

    .prologue
    .line 59
    invoke-direct {p0, p2}, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;-><init>(Landroid/os/Handler;)V

    .line 61
    new-instance v1, Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;

    invoke-direct {v1}, Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;-><init>()V

    .line 63
    .local v1, "unit":Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;
    iput-object p3, p0, Lcom/sec/android/app/sns3/agent/sp/qzone/command/SnsQzCmdGetFeed;->mUserID:Ljava/lang/String;

    .line 65
    iput p4, p0, Lcom/sec/android/app/sns3/agent/sp/qzone/command/SnsQzCmdGetFeed;->mFeedCount:I

    .line 67
    new-instance v0, Lcom/sec/android/app/sns3/agent/sp/qzone/command/SnsQzCmdGetFeed$1;

    iget-object v2, p0, Lcom/sec/android/app/sns3/agent/sp/qzone/command/SnsQzCmdGetFeed;->mUserID:Ljava/lang/String;

    iget v3, p0, Lcom/sec/android/app/sns3/agent/sp/qzone/command/SnsQzCmdGetFeed;->mFeedCount:I

    invoke-direct {v0, p0, p1, v2, v3}, Lcom/sec/android/app/sns3/agent/sp/qzone/command/SnsQzCmdGetFeed$1;-><init>(Lcom/sec/android/app/sns3/agent/sp/qzone/command/SnsQzCmdGetFeed;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;I)V

    .line 190
    .local v0, "req":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    invoke-virtual {v1, v0}, Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;->addRequest(Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;)V

    .line 192
    invoke-virtual {p0, v1}, Lcom/sec/android/app/sns3/agent/sp/qzone/command/SnsQzCmdGetFeed;->addCommandUnit(Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;)V

    .line 193
    return-void
.end method

.method private getCurrentTime()Ljava/lang/Long;
    .locals 4

    .prologue
    .line 201
    new-instance v0, Ljava/util/GregorianCalendar;

    invoke-direct {v0}, Ljava/util/GregorianCalendar;-><init>()V

    .line 202
    .local v0, "calendar":Ljava/util/GregorianCalendar;
    invoke-virtual {v0}, Ljava/util/GregorianCalendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    return-object v1
.end method


# virtual methods
.method public getUserID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 196
    iget-object v0, p0, Lcom/sec/android/app/sns3/agent/sp/qzone/command/SnsQzCmdGetFeed;->mUserID:Ljava/lang/String;

    return-object v0
.end method

.method protected respond()Z
    .locals 5

    .prologue
    .line 207
    const-string v0, "SnsAgent"

    const-string v1, "<SnsQzCmdGetFeed> respond()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 209
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/qzone/command/SnsQzCmdGetFeed;->getCommandCallback()Lcom/sec/android/app/sns3/agent/command/ISnsCommandCallback;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/qzone/command/SnsQzCmdGetFeed;->getCommandID()I

    move-result v1

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/qzone/command/SnsQzCmdGetFeed;->isSuccess()Z

    move-result v2

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/qzone/command/SnsQzCmdGetFeed;->getUri()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/qzone/command/SnsQzCmdGetFeed;->getResponseList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/sec/android/app/sns3/agent/command/ISnsCommandCallback;->onCmdRespond(IZLjava/lang/String;Ljava/util/List;)V

    .line 211
    const/4 v0, 0x1

    return v0
.end method
