.class Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$SessionStatusCallback$2;
.super Ljava/lang/Object;
.source "SnsAccountFbAuthSSOActivity.java"

# interfaces
.implements Lcom/facebook/Request$GraphUserCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$SessionStatusCallback;->call(Lcom/facebook/Session;Lcom/facebook/SessionState;Ljava/lang/Exception;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$SessionStatusCallback;

.field final synthetic val$accessToken:Ljava/lang/String;

.field final synthetic val$expires:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$SessionStatusCallback;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 688
    iput-object p1, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$SessionStatusCallback$2;->this$1:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$SessionStatusCallback;

    iput-object p2, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$SessionStatusCallback$2;->val$accessToken:Ljava/lang/String;

    iput-object p3, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$SessionStatusCallback$2;->val$expires:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCompleted(Lcom/facebook/model/GraphUser;Lcom/facebook/Response;)V
    .locals 4
    .param p1, "user"    # Lcom/facebook/model/GraphUser;
    .param p2, "response"    # Lcom/facebook/Response;

    .prologue
    .line 691
    # getter for: Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->access$400()Ljava/lang/String;

    move-result-object v1

    const-string v2, "newMeRequest is onCompleted"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 692
    if-eqz p1, :cond_2

    .line 693
    const-string v1, "email"

    invoke-interface {p1, v1}, Lcom/facebook/model/GraphUser;->getProperty(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 694
    .local v0, "userName":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_1

    .line 695
    :cond_0
    invoke-interface {p1}, Lcom/facebook/model/GraphUser;->getName()Ljava/lang/String;

    move-result-object v0

    .line 698
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$SessionStatusCallback$2;->this$1:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$SessionStatusCallback;

    iget-object v1, v1, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$SessionStatusCallback;->this$0:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;

    iget-object v2, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$SessionStatusCallback$2;->val$accessToken:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$SessionStatusCallback$2;->val$expires:Ljava/lang/String;

    # invokes: Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->setAuthTokenNExpires(Ljava/lang/String;Ljava/lang/String;)Z
    invoke-static {v1, v2, v3}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->access$1500(Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;Ljava/lang/String;Ljava/lang/String;)Z

    .line 699
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$SessionStatusCallback$2;->this$1:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$SessionStatusCallback;

    iget-object v1, v1, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$SessionStatusCallback;->this$0:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;

    invoke-interface {p1}, Lcom/facebook/model/GraphUser;->getId()Ljava/lang/String;

    move-result-object v2

    # invokes: Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->loginSuccess(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v1, v0, v2}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->access$1600(Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;Ljava/lang/String;Ljava/lang/String;)V

    .line 705
    .end local v0    # "userName":Ljava/lang/String;
    :goto_0
    return-void

    .line 701
    :cond_2
    # getter for: Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->access$400()Ljava/lang/String;

    move-result-object v1

    const-string v2, "user is null."

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 702
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$SessionStatusCallback$2;->this$1:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$SessionStatusCallback;

    iget-object v1, v1, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$SessionStatusCallback;->this$0:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;

    # invokes: Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->registerRetryNotification()V
    invoke-static {v1}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->access$1400(Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;)V

    .line 703
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$SessionStatusCallback$2;->this$1:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$SessionStatusCallback;

    iget-object v1, v1, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$SessionStatusCallback;->this$0:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->finish()V

    goto :goto_0
.end method
