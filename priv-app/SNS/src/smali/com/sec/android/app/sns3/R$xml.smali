.class public final Lcom/sec/android/app/sns3/R$xml;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "xml"
.end annotation


# static fields
.field public static final account_preferences_facebook:I = 0x7f040000

.field public static final account_preferences_foursquare:I = 0x7f040001

.field public static final account_preferences_google_plus:I = 0x7f040002

.field public static final account_preferences_instagram:I = 0x7f040003

.field public static final account_preferences_linkedin:I = 0x7f040004

.field public static final account_preferences_twitter:I = 0x7f040005

.field public static final auth_sync_setup_preference:I = 0x7f040006

.field public static final authenticator_facebook:I = 0x7f040007

.field public static final authenticator_foursquare:I = 0x7f040008

.field public static final authenticator_googleplus:I = 0x7f040009

.field public static final authenticator_instagram:I = 0x7f04000a

.field public static final authenticator_linkedin:I = 0x7f04000b

.field public static final authenticator_twitter:I = 0x7f04000c

.field public static final interval_settings_preference:I = 0x7f04000d

.field public static final settings_sync_apps_preference:I = 0x7f04000e

.field public static final syncadapter_facebook_event:I = 0x7f04000f

.field public static final syncadapter_facebook_gallery:I = 0x7f040010

.field public static final syncadapter_facebook_home_feed:I = 0x7f040011

.field public static final syncadapter_facebook_profile_feed:I = 0x7f040012

.field public static final syncadapter_facebook_profiles:I = 0x7f040013

.field public static final syncadapter_facebook_streams:I = 0x7f040014

.field public static final syncadapter_foursquare_profile_feed:I = 0x7f040015

.field public static final syncadapter_foursquare_profiles:I = 0x7f040016

.field public static final syncadapter_googleplus_call:I = 0x7f040017

.field public static final syncadapter_googleplus_profile_feed:I = 0x7f040018

.field public static final syncadapter_googleplus_profiles:I = 0x7f040019

.field public static final syncadapter_instagram_gallery:I = 0x7f04001a

.field public static final syncadapter_instagram_profile_feed:I = 0x7f04001b

.field public static final syncadapter_instagram_profiles:I = 0x7f04001c

.field public static final syncadapter_linkedin_profile_feed:I = 0x7f04001d

.field public static final syncadapter_linkedin_profiles:I = 0x7f04001e

.field public static final syncadapter_twitter_profiles:I = 0x7f04001f

.field public static final syncadapter_twitter_timeline_home:I = 0x7f040020

.field public static final syncadapter_twitter_timeline_user:I = 0x7f040021


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 368
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
