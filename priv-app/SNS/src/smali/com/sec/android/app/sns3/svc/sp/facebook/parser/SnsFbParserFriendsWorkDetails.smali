.class public Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserFriendsWorkDetails;
.super Ljava/lang/Object;
.source "SnsFbParserFriendsWorkDetails.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserFriendsWorkDetails$FacebookFriendWorklists;
    }
.end annotation


# static fields
.field static final FQL_RESULT_SET:Ljava/lang/String; = "fql_result_set"

.field static final QUERY_NAME:Ljava/lang/String; = "name"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    return-void
.end method

.method public static parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendWorkDetails;
    .locals 23
    .param p0, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 59
    const/4 v4, 0x0

    .line 60
    .local v4, "friendWorklists":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendWorkDetails;
    const/4 v2, 0x0

    .line 61
    .local v2, "curFriendWorklists":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendWorkDetails;
    const/16 v17, 0x0

    .line 64
    .local v17, "newFriendWorklists":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendWorkDetails;
    :try_start_0
    new-instance v7, Lorg/json/JSONArray;

    move-object/from16 v0, p0

    invoke-direct {v7, v0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 65
    .local v7, "jArr":Lorg/json/JSONArray;
    const/4 v6, 0x0

    .local v6, "j":I
    :goto_0
    invoke-virtual {v7}, Lorg/json/JSONArray;->length()I

    move-result v21

    move/from16 v0, v21

    if-ge v6, v0, :cond_c

    .line 67
    invoke-virtual {v7, v6}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v21

    const-string v22, "name"

    invoke-virtual/range {v21 .. v22}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    .line 68
    .local v19, "queryName":Ljava/lang/String;
    invoke-virtual {v7, v6}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v21

    const-string v22, "fql_result_set"

    invoke-virtual/range {v21 .. v22}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    .line 71
    .local v20, "queryObject":Ljava/lang/String;
    const-string v21, "user_query"

    move-object/from16 v0, v21

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_b

    .line 73
    new-instance v9, Lorg/json/JSONArray;

    move-object/from16 v0, v20

    invoke-direct {v9, v0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 74
    .local v9, "ja":Lorg/json/JSONArray;
    if-eqz v9, :cond_b

    .line 76
    const/4 v5, 0x0

    .local v5, "i":I
    move-object/from16 v18, v17

    .end local v17    # "newFriendWorklists":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendWorkDetails;
    .local v18, "newFriendWorklists":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendWorkDetails;
    :goto_1
    :try_start_1
    invoke-virtual {v9}, Lorg/json/JSONArray;->length()I

    move-result v21

    move/from16 v0, v21

    if-ge v5, v0, :cond_a

    .line 78
    invoke-virtual {v9, v5}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v13

    .line 80
    .local v13, "jsonObj":Lorg/json/JSONObject;
    const-string v21, "work"

    move-object/from16 v0, v21

    invoke-virtual {v13, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_8

    const-string v21, "work"

    move-object/from16 v0, v21

    invoke-virtual {v13, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v21

    const-string v22, ""

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_0

    const-string v21, "work"

    move-object/from16 v0, v21

    invoke-virtual {v13, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v21

    const-string v22, "null"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-nez v21, :cond_8

    .line 87
    :cond_0
    new-instance v8, Lorg/json/JSONArray;

    const-string v21, "work"

    move-object/from16 v0, v21

    invoke-virtual {v13, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-direct {v8, v0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 90
    .local v8, "jArrWORK":Lorg/json/JSONArray;
    if-eqz v8, :cond_d

    .line 91
    const/16 v16, 0x0

    .local v16, "k":I
    :goto_2
    invoke-virtual {v8}, Lorg/json/JSONArray;->length()I

    move-result v21

    move/from16 v0, v16

    move/from16 v1, v21

    if-ge v0, v1, :cond_d

    .line 92
    new-instance v17, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendWorkDetails;

    invoke-direct/range {v17 .. v17}, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendWorkDetails;-><init>()V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    .line 93
    .end local v18    # "newFriendWorklists":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendWorkDetails;
    .restart local v17    # "newFriendWorklists":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendWorkDetails;
    :try_start_2
    move/from16 v0, v16

    invoke-virtual {v8, v0}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v15

    .line 95
    .local v15, "jsonWorkObj":Lorg/json/JSONObject;
    const-string v21, "employer"

    move-object/from16 v0, v21

    invoke-virtual {v15, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_1

    .line 97
    new-instance v10, Lorg/json/JSONObject;

    const-string v21, "employer"

    move-object/from16 v0, v21

    invoke-virtual {v15, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-direct {v10, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 101
    .local v10, "jsonEmpObj":Lorg/json/JSONObject;
    if-eqz v10, :cond_1

    .line 102
    const-string v21, "id"

    move-object/from16 v0, v21

    invoke-virtual {v10, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, v17

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendWorkDetails;->mEmployerID:Ljava/lang/String;

    .line 104
    const-string v21, "name"

    move-object/from16 v0, v21

    invoke-virtual {v10, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, v17

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendWorkDetails;->mEmployerName:Ljava/lang/String;

    .line 108
    .end local v10    # "jsonEmpObj":Lorg/json/JSONObject;
    :cond_1
    const-string v21, "location"

    move-object/from16 v0, v21

    invoke-virtual {v15, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_2

    .line 111
    new-instance v12, Lorg/json/JSONObject;

    const-string v21, "location"

    move-object/from16 v0, v21

    invoke-virtual {v15, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-direct {v12, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 114
    .local v12, "jsonLocObj":Lorg/json/JSONObject;
    if-eqz v12, :cond_2

    .line 116
    const-string v21, "id"

    move-object/from16 v0, v21

    invoke-virtual {v12, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, v17

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendWorkDetails;->mLocationId:Ljava/lang/String;

    .line 118
    const-string v21, "name"

    move-object/from16 v0, v21

    invoke-virtual {v12, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, v17

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendWorkDetails;->mLocationName:Ljava/lang/String;

    .line 122
    .end local v12    # "jsonLocObj":Lorg/json/JSONObject;
    :cond_2
    const-string v21, "position"

    move-object/from16 v0, v21

    invoke-virtual {v15, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_3

    .line 125
    new-instance v14, Lorg/json/JSONObject;

    const-string v21, "position"

    move-object/from16 v0, v21

    invoke-virtual {v15, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-direct {v14, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 128
    .local v14, "jsonPosObj":Lorg/json/JSONObject;
    if-eqz v14, :cond_3

    .line 130
    const-string v21, "id"

    move-object/from16 v0, v21

    invoke-virtual {v14, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, v17

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendWorkDetails;->mPositionId:Ljava/lang/String;

    .line 132
    const-string v21, "name"

    move-object/from16 v0, v21

    invoke-virtual {v14, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, v17

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendWorkDetails;->mPositionName:Ljava/lang/String;

    .line 137
    .end local v14    # "jsonPosObj":Lorg/json/JSONObject;
    :cond_3
    const-string v21, "from"

    move-object/from16 v0, v21

    invoke-virtual {v15, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_4

    .line 140
    new-instance v11, Lorg/json/JSONObject;

    const-string v21, "from"

    move-object/from16 v0, v21

    invoke-virtual {v15, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-direct {v11, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 143
    .local v11, "jsonFromObj":Lorg/json/JSONObject;
    if-eqz v11, :cond_4

    .line 145
    const-string v21, "id"

    move-object/from16 v0, v21

    invoke-virtual {v11, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, v17

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendWorkDetails;->mLocationId:Ljava/lang/String;

    .line 147
    const-string v21, "name"

    move-object/from16 v0, v21

    invoke-virtual {v11, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, v17

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendWorkDetails;->mLocationName:Ljava/lang/String;

    .line 152
    .end local v11    # "jsonFromObj":Lorg/json/JSONObject;
    :cond_4
    const-string v21, "start_date"

    move-object/from16 v0, v21

    invoke-virtual {v15, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_5

    .line 154
    const-string v21, "start_date"

    move-object/from16 v0, v21

    invoke-virtual {v15, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, v17

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendWorkDetails;->mStartDate:Ljava/lang/String;

    .line 157
    :cond_5
    const-string v21, "end_date"

    move-object/from16 v0, v21

    invoke-virtual {v15, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_6

    .line 159
    const-string v21, "end_date"

    move-object/from16 v0, v21

    invoke-virtual {v15, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, v17

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendWorkDetails;->mEndDate:Ljava/lang/String;

    .line 163
    :cond_6
    const-string v21, "uid"

    move-object/from16 v0, v21

    invoke-virtual {v13, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, v17

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendWorkDetails;->mFriendID:Ljava/lang/String;

    .line 165
    const-string v21, "name"

    move-object/from16 v0, v21

    invoke-virtual {v13, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, v17

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendWorkDetails;->mName:Ljava/lang/String;

    .line 168
    if-nez v4, :cond_7

    .line 169
    move-object/from16 v4, v17

    .line 170
    move-object v2, v4

    .line 91
    :goto_3
    add-int/lit8 v16, v16, 0x1

    move-object/from16 v18, v17

    .end local v17    # "newFriendWorklists":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendWorkDetails;
    .restart local v18    # "newFriendWorklists":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendWorkDetails;
    goto/16 :goto_2

    .line 172
    .end local v18    # "newFriendWorklists":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendWorkDetails;
    .restart local v17    # "newFriendWorklists":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendWorkDetails;
    :cond_7
    move-object/from16 v0, v17

    iput-object v0, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendWorkDetails;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendWorkDetails;

    .line 173
    iget-object v2, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendWorkDetails;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendWorkDetails;
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_3

    .line 180
    .end local v8    # "jArrWORK":Lorg/json/JSONArray;
    .end local v15    # "jsonWorkObj":Lorg/json/JSONObject;
    .end local v16    # "k":I
    .end local v17    # "newFriendWorklists":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendWorkDetails;
    .restart local v18    # "newFriendWorklists":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendWorkDetails;
    :cond_8
    :try_start_3
    new-instance v17, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendWorkDetails;

    invoke-direct/range {v17 .. v17}, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendWorkDetails;-><init>()V
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_1

    .line 181
    .end local v18    # "newFriendWorklists":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendWorkDetails;
    .restart local v17    # "newFriendWorklists":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendWorkDetails;
    :try_start_4
    const-string v21, "uid"

    move-object/from16 v0, v21

    invoke-virtual {v13, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, v17

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendWorkDetails;->mFriendID:Ljava/lang/String;

    .line 183
    const-string v21, "name"

    move-object/from16 v0, v21

    invoke-virtual {v13, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, v17

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendWorkDetails;->mName:Ljava/lang/String;

    .line 185
    if-nez v4, :cond_9

    .line 186
    move-object/from16 v4, v17

    .line 187
    move-object v2, v4

    .line 76
    :goto_4
    add-int/lit8 v5, v5, 0x1

    move-object/from16 v18, v17

    .end local v17    # "newFriendWorklists":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendWorkDetails;
    .restart local v18    # "newFriendWorklists":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendWorkDetails;
    goto/16 :goto_1

    .line 189
    .end local v18    # "newFriendWorklists":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendWorkDetails;
    .restart local v17    # "newFriendWorklists":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendWorkDetails;
    :cond_9
    move-object/from16 v0, v17

    iput-object v0, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendWorkDetails;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendWorkDetails;

    .line 190
    iget-object v2, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendWorkDetails;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendWorkDetails;
    :try_end_4
    .catch Lorg/json/JSONException; {:try_start_4 .. :try_end_4} :catch_0

    goto :goto_4

    .end local v13    # "jsonObj":Lorg/json/JSONObject;
    .end local v17    # "newFriendWorklists":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendWorkDetails;
    .restart local v18    # "newFriendWorklists":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendWorkDetails;
    :cond_a
    move-object/from16 v17, v18

    .line 65
    .end local v5    # "i":I
    .end local v9    # "ja":Lorg/json/JSONArray;
    .end local v18    # "newFriendWorklists":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendWorkDetails;
    .restart local v17    # "newFriendWorklists":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendWorkDetails;
    :cond_b
    add-int/lit8 v6, v6, 0x1

    goto/16 :goto_0

    .line 198
    .end local v6    # "j":I
    .end local v7    # "jArr":Lorg/json/JSONArray;
    .end local v19    # "queryName":Ljava/lang/String;
    .end local v20    # "queryObject":Ljava/lang/String;
    :catch_0
    move-exception v3

    .line 199
    .local v3, "e":Lorg/json/JSONException;
    :goto_5
    invoke-virtual {v3}, Lorg/json/JSONException;->printStackTrace()V

    .line 202
    .end local v3    # "e":Lorg/json/JSONException;
    :cond_c
    return-object v4

    .line 198
    .end local v17    # "newFriendWorklists":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendWorkDetails;
    .restart local v5    # "i":I
    .restart local v6    # "j":I
    .restart local v7    # "jArr":Lorg/json/JSONArray;
    .restart local v9    # "ja":Lorg/json/JSONArray;
    .restart local v18    # "newFriendWorklists":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendWorkDetails;
    .restart local v19    # "queryName":Ljava/lang/String;
    .restart local v20    # "queryObject":Ljava/lang/String;
    :catch_1
    move-exception v3

    move-object/from16 v17, v18

    .end local v18    # "newFriendWorklists":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendWorkDetails;
    .restart local v17    # "newFriendWorklists":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendWorkDetails;
    goto :goto_5

    .end local v17    # "newFriendWorklists":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendWorkDetails;
    .restart local v8    # "jArrWORK":Lorg/json/JSONArray;
    .restart local v13    # "jsonObj":Lorg/json/JSONObject;
    .restart local v18    # "newFriendWorklists":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendWorkDetails;
    :cond_d
    move-object/from16 v17, v18

    .end local v18    # "newFriendWorklists":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendWorkDetails;
    .restart local v17    # "newFriendWorklists":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendWorkDetails;
    goto :goto_4
.end method
