.class public Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthenticatorService$FacebookAccountAuthenticator;
.super Landroid/accounts/AbstractAccountAuthenticator;
.source "SnsAccountFbAuthenticatorService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthenticatorService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "FacebookAccountAuthenticator"
.end annotation


# instance fields
.field private mAppContext:Landroid/content/Context;

.field final synthetic this$0:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthenticatorService;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthenticatorService;Landroid/content/Context;)V
    .locals 0
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 87
    iput-object p1, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthenticatorService$FacebookAccountAuthenticator;->this$0:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthenticatorService;

    .line 88
    invoke-direct {p0, p2}, Landroid/accounts/AbstractAccountAuthenticator;-><init>(Landroid/content/Context;)V

    .line 89
    iput-object p2, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthenticatorService$FacebookAccountAuthenticator;->mAppContext:Landroid/content/Context;

    .line 90
    return-void
.end method


# virtual methods
.method public addAccount(Landroid/accounts/AccountAuthenticatorResponse;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 7
    .param p1, "response"    # Landroid/accounts/AccountAuthenticatorResponse;
    .param p2, "accountType"    # Ljava/lang/String;
    .param p3, "authTokenType"    # Ljava/lang/String;
    .param p4, "requiredFeatures"    # [Ljava/lang/String;
    .param p5, "options"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/accounts/NetworkErrorException;
        }
    .end annotation

    .prologue
    .line 96
    invoke-static {}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->isLoggable()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 97
    # getter for: Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthenticatorService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthenticatorService;->access$000()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "FacebookAuthenticatorService : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " - "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 100
    :cond_0
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 102
    .local v3, "result":Landroid/os/Bundle;
    if-eqz p5, :cond_2

    # getter for: Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthenticatorService;->OPTIONS_USERNAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthenticatorService;->access$100()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p5, v4}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 103
    new-instance v0, Landroid/accounts/Account;

    # getter for: Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthenticatorService;->OPTIONS_USERNAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthenticatorService;->access$100()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p5, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "com.sec.android.app.sns3.facebook"

    invoke-direct {v0, v4, v5}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    .local v0, "account":Landroid/accounts/Account;
    iget-object v4, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthenticatorService$FacebookAccountAuthenticator;->mAppContext:Landroid/content/Context;

    invoke-static {v4}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    .line 108
    .local v1, "am":Landroid/accounts/AccountManager;
    # getter for: Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthenticatorService;->OPTIONS_PASSWORD:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthenticatorService;->access$200()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p5, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v1, v0, v4, v5}, Landroid/accounts/AccountManager;->addAccountExplicitly(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 110
    const-string v4, "authAccount"

    # getter for: Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthenticatorService;->OPTIONS_USERNAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthenticatorService;->access$100()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p5, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    const-string v4, "accountType"

    const-string v5, "com.sec.android.app.sns3.facebook"

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 116
    new-instance v2, Landroid/content/Intent;

    const-string v4, "com.sec.android.app.sns.ACTION_SNS_FACEBOOK_LOGGED_IN"

    invoke-direct {v2, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 117
    .local v2, "intent":Landroid/content/Intent;
    iget-object v4, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthenticatorService$FacebookAccountAuthenticator;->this$0:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthenticatorService;

    const-string v5, "com.sec.android.app.sns3.permission.RECEIVE_SNS_BROADCAST"

    invoke-virtual {v4, v2, v5}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthenticatorService;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    .line 131
    .end local v0    # "account":Landroid/accounts/Account;
    .end local v1    # "am":Landroid/accounts/AccountManager;
    .end local v2    # "intent":Landroid/content/Intent;
    :cond_1
    :goto_0
    return-object v3

    .line 120
    :cond_2
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 122
    .restart local v2    # "intent":Landroid/content/Intent;
    iget-object v4, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthenticatorService$FacebookAccountAuthenticator;->mAppContext:Landroid/content/Context;

    const-class v5, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 126
    const-string v4, "accountAuthenticatorResponse"

    invoke-virtual {v2, v4, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 127
    const-string v4, "manageAccount"

    invoke-virtual {v2, v4, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 128
    const-string v4, "intent"

    invoke-virtual {v3, v4, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    goto :goto_0
.end method

.method public confirmCredentials(Landroid/accounts/AccountAuthenticatorResponse;Landroid/accounts/Account;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 2
    .param p1, "response"    # Landroid/accounts/AccountAuthenticatorResponse;
    .param p2, "account"    # Landroid/accounts/Account;
    .param p3, "options"    # Landroid/os/Bundle;

    .prologue
    .line 137
    # getter for: Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthenticatorService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthenticatorService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "FacebookAuthenticatorService : confirmCredentials"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 138
    const/4 v0, 0x0

    return-object v0
.end method

.method public editProperties(Landroid/accounts/AccountAuthenticatorResponse;Ljava/lang/String;)Landroid/os/Bundle;
    .locals 2
    .param p1, "response"    # Landroid/accounts/AccountAuthenticatorResponse;
    .param p2, "accountType"    # Ljava/lang/String;

    .prologue
    .line 143
    # getter for: Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthenticatorService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthenticatorService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "FacebookAuthenticatorService : editProperties"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 144
    const/4 v0, 0x0

    return-object v0
.end method

.method public getAccountRemovalAllowed(Landroid/accounts/AccountAuthenticatorResponse;Landroid/accounts/Account;)Landroid/os/Bundle;
    .locals 18
    .param p1, "response"    # Landroid/accounts/AccountAuthenticatorResponse;
    .param p2, "account"    # Landroid/accounts/Account;

    .prologue
    .line 149
    # getter for: Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthenticatorService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthenticatorService;->access$000()Ljava/lang/String;

    move-result-object v14

    const-string v15, "FacebookAuthenticatorService : getAccountRemovalAllowed"

    invoke-static {v14, v15}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 151
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v2

    .line 152
    .local v2, "app":Lcom/sec/android/app/sns3/SnsApplication;
    new-instance v4, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdAuthLogout;

    invoke-virtual {v2}, Lcom/sec/android/app/sns3/SnsApplication;->getSvcMgr()Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v14

    invoke-virtual {v2}, Lcom/sec/android/app/sns3/SnsApplication;->getAgentMgr()Lcom/sec/android/app/sns3/agent/SnsAgentMgr;

    move-result-object v15

    invoke-virtual {v15}, Lcom/sec/android/app/sns3/agent/SnsAgentMgr;->getCommandMgr()Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;

    move-result-object v15

    invoke-virtual {v15}, Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;->getCommandMgrHandle()Landroid/os/Handler;

    move-result-object v15

    invoke-direct {v4, v14, v15}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdAuthLogout;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Handler;)V

    .line 154
    .local v4, "cmd":Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;
    new-instance v14, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthenticatorService$FacebookAccountAuthenticator$1;

    move-object/from16 v0, p0

    invoke-direct {v14, v0}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthenticatorService$FacebookAccountAuthenticator$1;-><init>(Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthenticatorService$FacebookAccountAuthenticator;)V

    invoke-virtual {v4, v14}, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->setCommandCallback(Lcom/sec/android/app/sns3/agent/command/ISnsCommandCallback;)Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;

    .line 163
    invoke-virtual {v4}, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->send()I

    .line 167
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthenticatorService$FacebookAccountAuthenticator;->this$0:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthenticatorService;

    const-string v15, "FB_contact_sync_interval"

    const/16 v16, 0x0

    invoke-virtual/range {v14 .. v16}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthenticatorService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v14

    invoke-interface {v14}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    .line 169
    .local v5, "contactEditor":Landroid/content/SharedPreferences$Editor;
    const-string v14, "FB_contact_sync_interval"

    const-wide/16 v16, 0x0

    move-wide/from16 v0, v16

    invoke-interface {v5, v14, v0, v1}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 170
    invoke-interface {v5}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 173
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthenticatorService$FacebookAccountAuthenticator;->this$0:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthenticatorService;

    const-string v15, "FB_calendar_sync_interval"

    const/16 v16, 0x0

    invoke-virtual/range {v14 .. v16}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthenticatorService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v14

    invoke-interface {v14}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    .line 175
    .local v3, "caledarEditor":Landroid/content/SharedPreferences$Editor;
    const-string v14, "FB_calendar_sync_interval"

    const-wide/16 v16, 0x0

    move-wide/from16 v0, v16

    invoke-interface {v3, v14, v0, v1}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 176
    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 178
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthenticatorService$FacebookAccountAuthenticator;->this$0:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthenticatorService;

    const-string v15, "FB_gallery_sync_interval"

    const/16 v16, 0x0

    invoke-virtual/range {v14 .. v16}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthenticatorService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v14

    invoke-interface {v14}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    .line 180
    .local v6, "galleryEditor":Landroid/content/SharedPreferences$Editor;
    const-string v14, "FB_gallery_sync_interval"

    const-wide/16 v16, 0x0

    move-wide/from16 v0, v16

    invoke-interface {v6, v14, v0, v1}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 181
    invoke-interface {v6}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 183
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthenticatorService$FacebookAccountAuthenticator;->this$0:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthenticatorService;

    const-string v15, "FB_home_feed_sync_interval"

    const/16 v16, 0x0

    invoke-virtual/range {v14 .. v16}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthenticatorService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v14

    invoke-interface {v14}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v7

    .line 185
    .local v7, "homeFeedEditor":Landroid/content/SharedPreferences$Editor;
    const-string v14, "FB_home_feed_sync_interval"

    const-wide/16 v16, 0x0

    move-wide/from16 v0, v16

    invoke-interface {v7, v14, v0, v1}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 186
    invoke-interface {v7}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 188
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthenticatorService$FacebookAccountAuthenticator;->this$0:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthenticatorService;

    const-string v15, "FB_profile_feed_sync_interval"

    const/16 v16, 0x0

    invoke-virtual/range {v14 .. v16}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthenticatorService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v14

    invoke-interface {v14}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v10

    .line 190
    .local v10, "profileFeedEditor":Landroid/content/SharedPreferences$Editor;
    const-string v14, "FB_profile_feed_sync_interval"

    const-wide/16 v16, 0x0

    move-wide/from16 v0, v16

    invoke-interface {v10, v14, v0, v1}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 191
    invoke-interface {v10}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 193
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthenticatorService$FacebookAccountAuthenticator;->this$0:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthenticatorService;

    const-string v15, "FB_profiles_sync_interval"

    const/16 v16, 0x0

    invoke-virtual/range {v14 .. v16}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthenticatorService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v14

    invoke-interface {v14}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v11

    .line 195
    .local v11, "profilesEditor":Landroid/content/SharedPreferences$Editor;
    const-string v14, "FB_profiles_sync_interval"

    const-wide/16 v16, 0x0

    move-wide/from16 v0, v16

    invoke-interface {v11, v14, v0, v1}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 196
    invoke-interface {v11}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 198
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthenticatorService$FacebookAccountAuthenticator;->this$0:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthenticatorService;

    const-string v15, "FB_streams_sync_interval"

    const/16 v16, 0x0

    invoke-virtual/range {v14 .. v16}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthenticatorService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v14

    invoke-interface {v14}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v13

    .line 200
    .local v13, "streamsFeedEditor":Landroid/content/SharedPreferences$Editor;
    const-string v14, "FB_streams_sync_interval"

    const-wide/16 v16, 0x0

    move-wide/from16 v0, v16

    invoke-interface {v13, v14, v0, v1}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 201
    invoke-interface {v13}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 204
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v14

    const-string v15, "notification"

    invoke-virtual {v14, v15}, Lcom/sec/android/app/sns3/SnsApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Landroid/app/NotificationManager;

    move-object v9, v14

    check-cast v9, Landroid/app/NotificationManager;

    .line 206
    .local v9, "notiMgr":Landroid/app/NotificationManager;
    const/16 v14, 0x514

    invoke-virtual {v9, v14}, Landroid/app/NotificationManager;->cancel(I)V

    .line 209
    new-instance v8, Landroid/content/Intent;

    const-string v14, "com.sec.android.app.sns.ACTION_SNS_FACEBOOK_LOGGED_OUT"

    invoke-direct {v8, v14}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 210
    .local v8, "intent":Landroid/content/Intent;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthenticatorService$FacebookAccountAuthenticator;->this$0:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthenticatorService;

    const-string v15, "com.sec.android.app.sns3.permission.RECEIVE_SNS_BROADCAST"

    invoke-virtual {v14, v8, v15}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthenticatorService;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    .line 212
    new-instance v12, Landroid/os/Bundle;

    invoke-direct {v12}, Landroid/os/Bundle;-><init>()V

    .line 213
    .local v12, "result":Landroid/os/Bundle;
    const-string v14, "booleanResult"

    const/4 v15, 0x1

    invoke-virtual {v12, v14, v15}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 214
    return-object v12
.end method

.method public getAuthToken(Landroid/accounts/AccountAuthenticatorResponse;Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 2
    .param p1, "response"    # Landroid/accounts/AccountAuthenticatorResponse;
    .param p2, "account"    # Landroid/accounts/Account;
    .param p3, "authTokenType"    # Ljava/lang/String;
    .param p4, "loginOptions"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/accounts/NetworkErrorException;
        }
    .end annotation

    .prologue
    .line 220
    # getter for: Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthenticatorService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthenticatorService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "FacebookAuthenticatorService : getAuthToken"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 221
    const/4 v0, 0x0

    return-object v0
.end method

.method public getAuthTokenLabel(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "authTokenType"    # Ljava/lang/String;

    .prologue
    .line 226
    # getter for: Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthenticatorService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthenticatorService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "FacebookAuthenticatorService : getAuthTokenLabel"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 227
    const/4 v0, 0x0

    return-object v0
.end method

.method public hasFeatures(Landroid/accounts/AccountAuthenticatorResponse;Landroid/accounts/Account;[Ljava/lang/String;)Landroid/os/Bundle;
    .locals 2
    .param p1, "response"    # Landroid/accounts/AccountAuthenticatorResponse;
    .param p2, "account"    # Landroid/accounts/Account;
    .param p3, "features"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/accounts/NetworkErrorException;
        }
    .end annotation

    .prologue
    .line 233
    # getter for: Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthenticatorService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthenticatorService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "FacebookAuthenticatorService : hasFeatures"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 234
    const/4 v0, 0x0

    return-object v0
.end method

.method public updateCredentials(Landroid/accounts/AccountAuthenticatorResponse;Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 2
    .param p1, "response"    # Landroid/accounts/AccountAuthenticatorResponse;
    .param p2, "account"    # Landroid/accounts/Account;
    .param p3, "authTokenType"    # Ljava/lang/String;
    .param p4, "loginOptions"    # Landroid/os/Bundle;

    .prologue
    .line 240
    # getter for: Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthenticatorService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthenticatorService;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "FacebookAuthenticatorService : updateCredentials"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 241
    const/4 v0, 0x0

    return-object v0
.end method
