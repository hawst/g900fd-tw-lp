.class public abstract Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqPostPhoto;
.super Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqBase;
.source "SnsFbReqPostPhoto.java"

# interfaces
.implements Lcom/sec/android/app/sns3/svc/sp/facebook/callback/ISnsFbReqCbID;


# instance fields
.field private mBundle:Landroid/os/Bundle;

.field private mObjectID:Ljava/lang/String;

.field private mReqEntity:Lorg/apache/http/entity/mime/MultipartEntity;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 1
    .param p1, "svcMgr"    # Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    .param p2, "objectID"    # Ljava/lang/String;
    .param p3, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 49
    const/16 v0, 0x17

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqBase;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;I)V

    .line 51
    iput-object p2, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqPostPhoto;->mObjectID:Ljava/lang/String;

    .line 53
    iput-object p3, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqPostPhoto;->mBundle:Landroid/os/Bundle;

    .line 54
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;Landroid/os/Bundle;Lorg/apache/http/entity/mime/MultipartEntity;)V
    .locals 0
    .param p1, "svcMgr"    # Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    .param p2, "objectID"    # Ljava/lang/String;
    .param p3, "bundle"    # Landroid/os/Bundle;
    .param p4, "reqEntity"    # Lorg/apache/http/entity/mime/MultipartEntity;

    .prologue
    .line 58
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqPostPhoto;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 60
    iput-object p4, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqPostPhoto;->mReqEntity:Lorg/apache/http/entity/mime/MultipartEntity;

    .line 61
    return-void
.end method


# virtual methods
.method protected compose()Lcom/sec/android/app/sns3/svc/http/SnsHttpRequest;
    .locals 8

    .prologue
    .line 66
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqPostPhoto;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/SnsSvcMgr;->getTokenMgr()Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;

    move-result-object v0

    const-string v1, "facebook"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;->getToken(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;

    move-result-object v7

    check-cast v7, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbToken;

    .line 68
    .local v7, "token":Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbToken;
    const-string v2, "POST"

    .line 69
    .local v2, "method":Ljava/lang/String;
    const/4 v3, 0x0

    .line 70
    .local v3, "uri":Ljava/lang/String;
    const/4 v4, 0x0

    .line 72
    .local v4, "header":Landroid/os/Bundle;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "https://graph.facebook.com/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqPostPhoto;->mObjectID:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/photos?access_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v7}, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbToken;->getAccessToken()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->encodeUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&expires_in="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v7}, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbToken;->getExpires()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->encodeUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 76
    new-instance v0, Lcom/sec/android/app/sns3/svc/http/SnsHttpRequest;

    iget v1, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqPostPhoto;->mReqID:I

    iget-object v5, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqPostPhoto;->mBundle:Landroid/os/Bundle;

    iget-object v6, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqPostPhoto;->mReqEntity:Lorg/apache/http/entity/mime/MultipartEntity;

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/sns3/svc/http/SnsHttpRequest;-><init>(ILjava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;Lorg/apache/http/entity/mime/MultipartEntity;)V

    return-object v0
.end method

.method protected parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;
    .locals 3
    .param p1, "content"    # Ljava/lang/String;

    .prologue
    .line 81
    const-string v1, "SNS"

    const-string v2, "Facebook  SnsFbReqPostPhoto response SUCCESS!!"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 82
    invoke-super {p0, p1}, Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqBase;->parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;

    move-result-object v0

    .line 84
    .local v0, "response":Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;
    if-eqz v0, :cond_0

    .line 87
    .end local v0    # "response":Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;
    :goto_0
    return-object v0

    .restart local v0    # "response":Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;
    :cond_0
    invoke-static {p1}, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserID;->parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseID;

    move-result-object v0

    goto :goto_0
.end method

.method protected respond(Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;)Z
    .locals 7
    .param p1, "result"    # Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;

    .prologue
    .line 92
    iget v1, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqPostPhoto;->mReqID:I

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->isSuccess()Z

    move-result v2

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->getHttpstatus()I

    move-result v3

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->getErrorCode()I

    move-result v4

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->getReason()Landroid/os/Bundle;

    move-result-object v5

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->getResponse()Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseID;

    move-object v0, p0

    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqPostPhoto;->onReqRespond(IZIILandroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseID;)Z

    .line 95
    const/4 v0, 0x1

    return v0
.end method
