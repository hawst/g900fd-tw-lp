.class public abstract Lcom/sec/android/app/sns3/svc/sp/foursquare/request/SnsFsReqGetSuggestCompletion;
.super Lcom/sec/android/app/sns3/svc/sp/foursquare/request/SnsFsReqBase;
.source "SnsFsReqGetSuggestCompletion.java"

# interfaces
.implements Lcom/sec/android/app/sns3/svc/sp/foursquare/callback/ISnsFsReqCbSuggestCompletion;


# static fields
.field public static final REST_URL_VENUES:Ljava/lang/String; = "https://api.foursquare.com/v2/venues/suggestcompletion?oauth_token="

.field public static final REST_URL_VENUES_NO_OAUTH:Ljava/lang/String; = "https://api.foursquare.com/v2/venues/suggestcompletion?"


# instance fields
.field private mLatitude:Ljava/lang/String;

.field private mLongitude:Ljava/lang/String;

.field private mQuery:Ljava/lang/String;

.field public mQueryString:Ljava/lang/StringBuilder;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Bundle;)V
    .locals 5
    .param p1, "svcMgr"    # Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    .param p2, "bundle"    # Landroid/os/Bundle;

    .prologue
    const/4 v3, 0x0

    .line 58
    const/16 v2, 0x18

    invoke-direct {p0, p1, v2}, Lcom/sec/android/app/sns3/svc/sp/foursquare/request/SnsFsReqBase;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;I)V

    .line 49
    iput-object v3, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/request/SnsFsReqGetSuggestCompletion;->mLatitude:Ljava/lang/String;

    .line 51
    iput-object v3, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/request/SnsFsReqGetSuggestCompletion;->mLongitude:Ljava/lang/String;

    .line 53
    iput-object v3, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/request/SnsFsReqGetSuggestCompletion;->mQuery:Ljava/lang/String;

    .line 55
    iput-object v3, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/request/SnsFsReqGetSuggestCompletion;->mQueryString:Ljava/lang/StringBuilder;

    .line 60
    if-eqz p2, :cond_4

    .line 61
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "&v="

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iput-object v2, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/request/SnsFsReqGetSuggestCompletion;->mQueryString:Ljava/lang/StringBuilder;

    .line 62
    iget-object v2, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/request/SnsFsReqGetSuggestCompletion;->mQueryString:Ljava/lang/StringBuilder;

    const-string v4, "20140731"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 66
    const-string v2, "latitude"

    invoke-virtual {p2, v2}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_1

    const-string v2, "latitude"

    invoke-virtual {p2, v2}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    :goto_0
    iput-object v2, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/request/SnsFsReqGetSuggestCompletion;->mLatitude:Ljava/lang/String;

    .line 68
    const-string v2, "longitude"

    invoke-virtual {p2, v2}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_0

    const-string v2, "longitude"

    invoke-virtual {p2, v2}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    :cond_0
    iput-object v3, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/request/SnsFsReqGetSuggestCompletion;->mLongitude:Ljava/lang/String;

    .line 70
    const-string v2, "query"

    invoke-virtual {p2, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/request/SnsFsReqGetSuggestCompletion;->mQuery:Ljava/lang/String;

    .line 72
    iget-object v2, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/request/SnsFsReqGetSuggestCompletion;->mLatitude:Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/request/SnsFsReqGetSuggestCompletion;->mLongitude:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 73
    iget-object v2, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/request/SnsFsReqGetSuggestCompletion;->mQueryString:Ljava/lang/StringBuilder;

    const-string v3, "&ll="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/request/SnsFsReqGetSuggestCompletion;->mLatitude:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/request/SnsFsReqGetSuggestCompletion;->mLongitude:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 74
    const-string v2, "latitude"

    invoke-virtual {p2, v2}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 75
    const-string v2, "longitude"

    invoke-virtual {p2, v2}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 83
    :goto_1
    invoke-virtual {p2}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 84
    .local v1, "key":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/request/SnsFsReqGetSuggestCompletion;->mQueryString:Ljava/lang/StringBuilder;

    const-string v3, "&"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->encodeUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "key":Ljava/lang/String;
    :cond_1
    move-object v2, v3

    .line 66
    goto :goto_0

    .line 76
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/request/SnsFsReqGetSuggestCompletion;->mQuery:Ljava/lang/String;

    if-eqz v2, :cond_3

    .line 77
    iget-object v2, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/request/SnsFsReqGetSuggestCompletion;->mQueryString:Ljava/lang/StringBuilder;

    const-string v3, "&query="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/request/SnsFsReqGetSuggestCompletion;->mQuery:Ljava/lang/String;

    invoke-static {v3}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->encodeUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 78
    iget-object v2, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/request/SnsFsReqGetSuggestCompletion;->mQuery:Ljava/lang/String;

    invoke-virtual {p2, v2}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    goto :goto_1

    .line 80
    :cond_3
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Must provide parameter (latitude & longitude) & query"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 89
    :cond_4
    return-void
.end method


# virtual methods
.method protected compose()Lcom/sec/android/app/sns3/svc/http/SnsHttpRequest;
    .locals 7

    .prologue
    .line 93
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/request/SnsFsReqGetSuggestCompletion;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/SnsSvcMgr;->getTokenMgr()Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;

    move-result-object v0

    const-string v1, "foursquare"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;->getToken(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsToken;

    .line 95
    .local v6, "token":Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsToken;
    const-string v2, "GET"

    .line 96
    .local v2, "method":Ljava/lang/String;
    const/4 v3, 0x0

    .line 97
    .local v3, "uri":Ljava/lang/String;
    const/4 v4, 0x0

    .line 98
    .local v4, "header":Landroid/os/Bundle;
    const/4 v5, 0x0

    .line 99
    .local v5, "body":Landroid/os/Bundle;
    if-eqz v6, :cond_0

    invoke-virtual {v6}, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsToken;->getTokenState()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 100
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "https://api.foursquare.com/v2/venues/suggestcompletion?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/request/SnsFsReqGetSuggestCompletion;->mQueryString:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&client_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFourSquare;->CLIENT_ID:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&client_secret="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFourSquare;->CLIENT_SECRET:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 107
    :goto_0
    new-instance v0, Lcom/sec/android/app/sns3/svc/http/SnsHttpRequest;

    iget v1, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/request/SnsFsReqGetSuggestCompletion;->mReqID:I

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/sns3/svc/http/SnsHttpRequest;-><init>(ILjava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;)V

    return-object v0

    .line 103
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "https://api.foursquare.com/v2/venues/suggestcompletion?oauth_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v6}, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsToken;->getAccessToken()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->encodeUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/request/SnsFsReqGetSuggestCompletion;->mQueryString:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method

.method protected parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;
    .locals 3
    .param p1, "content"    # Ljava/lang/String;

    .prologue
    .line 112
    const-string v1, "SNS"

    const-string v2, "foursquare  SnsFsReqGetSuggestCompletion response SUCCESS!!"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 113
    invoke-super {p0, p1}, Lcom/sec/android/app/sns3/svc/sp/foursquare/request/SnsFsReqBase;->parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;

    move-result-object v0

    .line 115
    .local v0, "response":Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;
    if-eqz v0, :cond_0

    .line 118
    .end local v0    # "response":Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;
    :goto_0
    return-object v0

    .restart local v0    # "response":Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;
    :cond_0
    invoke-static {p1}, Lcom/sec/android/app/sns3/svc/sp/foursquare/parser/SnsFsParserVenues;->parseSuggested(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseVenues;

    move-result-object v0

    goto :goto_0
.end method

.method protected respond(Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;)Z
    .locals 7
    .param p1, "result"    # Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;

    .prologue
    .line 123
    iget v1, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/request/SnsFsReqGetSuggestCompletion;->mReqID:I

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->isSuccess()Z

    move-result v2

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->getHttpstatus()I

    move-result v3

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->getErrorCode()I

    move-result v4

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->getReason()Landroid/os/Bundle;

    move-result-object v5

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->getResponse()Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseVenues;

    move-object v0, p0

    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/app/sns3/svc/sp/foursquare/request/SnsFsReqGetSuggestCompletion;->onReqRespond(IZIILandroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseVenues;)Z

    .line 125
    const/4 v0, 0x1

    return v0
.end method
