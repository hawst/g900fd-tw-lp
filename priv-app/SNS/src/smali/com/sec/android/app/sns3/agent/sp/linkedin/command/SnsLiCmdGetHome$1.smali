.class Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetHome$1;
.super Lcom/sec/android/app/sns3/svc/sp/linkedin/request/SnsLiReqGetUpdates;
.source "SnsLiCmdGetHome.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetHome;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Handler;Ljava/lang/String;Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetHome;


# direct methods
.method constructor <init>(Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetHome;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 0
    .param p2, "x0"    # Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    .param p3, "x1"    # Ljava/lang/String;
    .param p4, "x2"    # Landroid/os/Bundle;

    .prologue
    .line 57
    iput-object p1, p0, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetHome$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetHome;

    invoke-direct {p0, p2, p3, p4}, Lcom/sec/android/app/sns3/svc/sp/linkedin/request/SnsLiReqGetUpdates;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;Landroid/os/Bundle;)V

    return-void
.end method


# virtual methods
.method public onReqRespond(IZIILandroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdates;)Z
    .locals 3
    .param p1, "reqID"    # I
    .param p2, "bSuccess"    # Z
    .param p3, "httpStatus"    # I
    .param p4, "errorCode"    # I
    .param p5, "reason"    # Landroid/os/Bundle;
    .param p6, "updates"    # Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdates;

    .prologue
    .line 62
    if-eqz p2, :cond_1

    .line 63
    if-eqz p6, :cond_0

    .line 64
    # getter for: Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetHome;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;
    invoke-static {}, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetHome;->access$000()Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;

    move-result-object v0

    invoke-virtual {v0, p6}, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;->insertHomeFeeds(Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdates;)V

    .line 72
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetHome$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetHome;

    invoke-virtual {v0, p2}, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetHome;->setSuccess(Z)V

    .line 73
    iget-object v0, p0, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetHome$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetHome;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetHome;->receive(Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;)V

    .line 74
    const/4 v0, 0x1

    return v0

    .line 67
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetHome$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetHome;

    new-instance v1, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;

    const-string v2, "facebook"

    invoke-direct {v1, v2, p3, p4, p5}, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;-><init>(Ljava/lang/String;IILandroid/os/Bundle;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetHome;->setResponseList(Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;)Z

    .line 69
    iget-object v0, p0, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetHome$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetHome;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetHome;->setUri(Ljava/lang/String;)V

    goto :goto_0
.end method
