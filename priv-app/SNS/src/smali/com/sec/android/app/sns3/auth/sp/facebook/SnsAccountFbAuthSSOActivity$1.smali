.class Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$1;
.super Ljava/lang/Object;
.source "SnsAccountFbAuthSSOActivity.java"

# interfaces
.implements Landroid/accounts/AccountManagerCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/accounts/AccountManagerCallback",
        "<",
        "Landroid/os/Bundle;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;)V
    .locals 0

    .prologue
    .line 108
    iput-object p1, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$1;->this$0:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run(Landroid/accounts/AccountManagerFuture;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/AccountManagerFuture",
            "<",
            "Landroid/os/Bundle;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 111
    .local p1, "future":Landroid/accounts/AccountManagerFuture;, "Landroid/accounts/AccountManagerFuture<Landroid/os/Bundle;>;"
    const/4 v1, 0x1

    .line 112
    .local v1, "done":Z
    iget-object v4, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$1;->this$0:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;

    const/4 v5, 0x2

    # setter for: Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->mState:I
    invoke-static {v4, v5}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->access$102(Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;I)I

    .line 113
    iget-object v4, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$1;->this$0:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;

    # getter for: Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->mHandler:Landroid/os/Handler;
    invoke-static {v4}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->access$300(Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;)Landroid/os/Handler;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$1;->this$0:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;

    # getter for: Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->mFinishRunnable:Ljava/lang/Runnable;
    invoke-static {v5}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->access$200(Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;)Ljava/lang/Runnable;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 116
    :try_start_0
    invoke-interface {p1}, Landroid/accounts/AccountManagerFuture;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 117
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v4, "intent"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Intent;

    .line 118
    .local v3, "intent":Landroid/content/Intent;
    if-eqz v3, :cond_2

    .line 120
    const/4 v1, 0x0

    .line 121
    iget-object v4, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$1;->this$0:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;

    const/4 v5, 0x1

    # setter for: Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->mState:I
    invoke-static {v4, v5}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->access$102(Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;I)I

    .line 123
    # getter for: Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->access$400()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "AccountManagerCallback : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " - "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v6

    invoke-virtual {v6}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 125
    iget-object v4, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$1;->this$0:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;

    const/16 v5, 0x3e9

    invoke-virtual {v4, v3, v5}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 141
    :goto_0
    iget-object v4, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$1;->this$0:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;

    # invokes: Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->isLoggedInFacebook()Z
    invoke-static {v4}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->access$600(Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 142
    const/4 v1, 0x0

    .line 143
    iget-object v4, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$1;->this$0:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;

    # invokes: Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->showDialogForSSO()V
    invoke-static {v4}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->access$700(Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;)V

    .line 145
    iget-object v4, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$1;->this$0:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;

    # getter for: Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->mHandler:Landroid/os/Handler;
    invoke-static {v4}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->access$300(Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;)Landroid/os/Handler;

    move-result-object v4

    new-instance v5, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$1$1;

    invoke-direct {v5, p0}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$1$1;-><init>(Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$1;)V

    invoke-virtual {v4, v5}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 156
    :cond_0
    if-eqz v1, :cond_1

    .line 157
    iget-object v4, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$1;->this$0:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;

    invoke-virtual {v4}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->finish()V

    .line 160
    .end local v0    # "bundle":Landroid/os/Bundle;
    .end local v3    # "intent":Landroid/content/Intent;
    :cond_1
    :goto_1
    return-void

    .line 128
    .restart local v0    # "bundle":Landroid/os/Bundle;
    .restart local v3    # "intent":Landroid/content/Intent;
    :cond_2
    :try_start_1
    # getter for: Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->access$400()Ljava/lang/String;

    move-result-object v4

    const-string v5, "AccountManagerCallback : intent for future is null"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/accounts/OperationCanceledException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Landroid/accounts/AuthenticatorException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 130
    .end local v0    # "bundle":Landroid/os/Bundle;
    .end local v3    # "intent":Landroid/content/Intent;
    :catch_0
    move-exception v2

    .line 131
    .local v2, "e":Landroid/accounts/OperationCanceledException;
    :try_start_2
    # getter for: Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->access$400()Ljava/lang/String;

    move-result-object v4

    const-string v5, "addAccount was canceled"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 141
    iget-object v4, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$1;->this$0:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;

    # invokes: Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->isLoggedInFacebook()Z
    invoke-static {v4}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->access$600(Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 142
    const/4 v1, 0x0

    .line 143
    iget-object v4, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$1;->this$0:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;

    # invokes: Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->showDialogForSSO()V
    invoke-static {v4}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->access$700(Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;)V

    .line 145
    iget-object v4, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$1;->this$0:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;

    # getter for: Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->mHandler:Landroid/os/Handler;
    invoke-static {v4}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->access$300(Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;)Landroid/os/Handler;

    move-result-object v4

    new-instance v5, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$1$1;

    invoke-direct {v5, p0}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$1$1;-><init>(Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$1;)V

    invoke-virtual {v4, v5}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 156
    :cond_3
    if-eqz v1, :cond_1

    .line 157
    iget-object v4, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$1;->this$0:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;

    invoke-virtual {v4}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->finish()V

    goto :goto_1

    .line 132
    .end local v2    # "e":Landroid/accounts/OperationCanceledException;
    :catch_1
    move-exception v2

    .line 133
    .local v2, "e":Ljava/io/IOException;
    :try_start_3
    # getter for: Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->access$400()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "addAccount failed: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 141
    iget-object v4, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$1;->this$0:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;

    # invokes: Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->isLoggedInFacebook()Z
    invoke-static {v4}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->access$600(Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 142
    const/4 v1, 0x0

    .line 143
    iget-object v4, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$1;->this$0:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;

    # invokes: Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->showDialogForSSO()V
    invoke-static {v4}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->access$700(Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;)V

    .line 145
    iget-object v4, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$1;->this$0:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;

    # getter for: Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->mHandler:Landroid/os/Handler;
    invoke-static {v4}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->access$300(Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;)Landroid/os/Handler;

    move-result-object v4

    new-instance v5, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$1$1;

    invoke-direct {v5, p0}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$1$1;-><init>(Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$1;)V

    invoke-virtual {v4, v5}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 156
    :cond_4
    if-eqz v1, :cond_1

    .line 157
    iget-object v4, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$1;->this$0:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;

    invoke-virtual {v4}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->finish()V

    goto :goto_1

    .line 134
    .end local v2    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v2

    .line 135
    .local v2, "e":Landroid/accounts/AuthenticatorException;
    :try_start_4
    # getter for: Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->access$400()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "addAccount failed: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 141
    iget-object v4, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$1;->this$0:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;

    # invokes: Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->isLoggedInFacebook()Z
    invoke-static {v4}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->access$600(Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 142
    const/4 v1, 0x0

    .line 143
    iget-object v4, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$1;->this$0:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;

    # invokes: Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->showDialogForSSO()V
    invoke-static {v4}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->access$700(Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;)V

    .line 145
    iget-object v4, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$1;->this$0:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;

    # getter for: Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->mHandler:Landroid/os/Handler;
    invoke-static {v4}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->access$300(Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;)Landroid/os/Handler;

    move-result-object v4

    new-instance v5, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$1$1;

    invoke-direct {v5, p0}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$1$1;-><init>(Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$1;)V

    invoke-virtual {v4, v5}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 156
    :cond_5
    if-eqz v1, :cond_1

    .line 157
    iget-object v4, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$1;->this$0:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;

    invoke-virtual {v4}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->finish()V

    goto/16 :goto_1

    .line 136
    .end local v2    # "e":Landroid/accounts/AuthenticatorException;
    :catch_3
    move-exception v2

    .line 137
    .local v2, "e":Ljava/lang/SecurityException;
    :try_start_5
    # getter for: Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->access$400()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "addAccount failed: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 138
    const/4 v1, 0x0

    .line 139
    iget-object v4, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$1;->this$0:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;

    # invokes: Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->invokeFacebookSSOAuth()V
    invoke-static {v4}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->access$500(Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 141
    iget-object v4, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$1;->this$0:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;

    # invokes: Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->isLoggedInFacebook()Z
    invoke-static {v4}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->access$600(Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 142
    const/4 v1, 0x0

    .line 143
    iget-object v4, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$1;->this$0:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;

    # invokes: Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->showDialogForSSO()V
    invoke-static {v4}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->access$700(Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;)V

    .line 145
    iget-object v4, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$1;->this$0:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;

    # getter for: Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->mHandler:Landroid/os/Handler;
    invoke-static {v4}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->access$300(Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;)Landroid/os/Handler;

    move-result-object v4

    new-instance v5, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$1$1;

    invoke-direct {v5, p0}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$1$1;-><init>(Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$1;)V

    invoke-virtual {v4, v5}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 156
    :cond_6
    if-eqz v1, :cond_1

    .line 157
    iget-object v4, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$1;->this$0:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;

    invoke-virtual {v4}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->finish()V

    goto/16 :goto_1

    .line 141
    .end local v2    # "e":Ljava/lang/SecurityException;
    :catchall_0
    move-exception v4

    iget-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$1;->this$0:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;

    # invokes: Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->isLoggedInFacebook()Z
    invoke-static {v5}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->access$600(Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 142
    const/4 v1, 0x0

    .line 143
    iget-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$1;->this$0:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;

    # invokes: Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->showDialogForSSO()V
    invoke-static {v5}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->access$700(Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;)V

    .line 145
    iget-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$1;->this$0:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;

    # getter for: Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->mHandler:Landroid/os/Handler;
    invoke-static {v5}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->access$300(Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;)Landroid/os/Handler;

    move-result-object v5

    new-instance v6, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$1$1;

    invoke-direct {v6, p0}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$1$1;-><init>(Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$1;)V

    invoke-virtual {v5, v6}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 156
    :cond_7
    if-eqz v1, :cond_8

    .line 157
    iget-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$1;->this$0:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;

    invoke-virtual {v5}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->finish()V

    :cond_8
    throw v4
.end method
