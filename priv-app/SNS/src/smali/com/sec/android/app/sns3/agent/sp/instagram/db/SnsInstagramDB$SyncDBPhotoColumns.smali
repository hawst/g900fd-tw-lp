.class public interface abstract Lcom/sec/android/app/sns3/agent/sp/instagram/db/SnsInstagramDB$SyncDBPhotoColumns;
.super Ljava/lang/Object;
.source "SnsInstagramDB.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/agent/sp/instagram/db/SnsInstagramDB;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "SyncDBPhotoColumns"
.end annotation


# static fields
.field public static final PHOTO_CREATE_TIME:Ljava/lang/String; = "created_time"

.field public static final PHOTO_FROM_ID:Ljava/lang/String; = "from_id"

.field public static final PHOTO_FROM_NAME:Ljava/lang/String; = "from_name"

.field public static final PHOTO_HEIGHT:Ljava/lang/String; = "height"

.field public static final PHOTO_ICON:Ljava/lang/String; = "icon"

.field public static final PHOTO_ID:Ljava/lang/String; = "id"

.field public static final PHOTO_IMAGES_HEIGHT:Ljava/lang/String; = "image_height"

.field public static final PHOTO_IMAGES_URL:Ljava/lang/String; = "url"

.field public static final PHOTO_IMAGES_WIDTH:Ljava/lang/String; = "image_width"

.field public static final PHOTO_LATITUDE:Ljava/lang/String; = "latitude"

.field public static final PHOTO_LINK:Ljava/lang/String; = "link"

.field public static final PHOTO_LOCATION_NAME:Ljava/lang/String; = "location_name"

.field public static final PHOTO_LONGITUDE:Ljava/lang/String; = "longitude"

.field public static final PHOTO_NAME:Ljava/lang/String; = "name"

.field public static final PHOTO_PARENT_ID:Ljava/lang/String; = "target_id"

.field public static final PHOTO_POSITION:Ljava/lang/String; = "position"

.field public static final PHOTO_SCREENNAIL_URL:Ljava/lang/String; = "source"

.field public static final PHOTO_THUMBNAIL_URL:Ljava/lang/String; = "picture"

.field public static final PHOTO_UPDATED_TIME:Ljava/lang/String; = "updated_time"

.field public static final PHOTO_WIDTH:Ljava/lang/String; = "width"
