.class public final enum Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI;
.super Ljava/lang/Enum;
.source "SnsTwGeoAPI.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI$Param;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI;

.field public static final enum GEO_ID:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI;

.field public static final enum GEO_PLACE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI;

.field public static final enum GEO_REVERSE_GEOCODE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI;

.field public static final enum GEO_SEARCH:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI;

.field public static final enum GEO_SIMILAR_PLACES:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI;


# instance fields
.field private mHttpMethod:Ljava/lang/String;

.field private mUrl:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 28
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI;

    const-string v1, "GEO_ID"

    const-string v2, "/geo/id/"

    const-string v3, "GET"

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI;->GEO_ID:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI;

    .line 29
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI;

    const-string v1, "GEO_REVERSE_GEOCODE"

    const-string v2, "/geo/reverse_geocode.json"

    const-string v3, "GET"

    invoke-direct {v0, v1, v5, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI;->GEO_REVERSE_GEOCODE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI;

    .line 30
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI;

    const-string v1, "GEO_SEARCH"

    const-string v2, "/geo/search.json"

    const-string v3, "GET"

    invoke-direct {v0, v1, v6, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI;->GEO_SEARCH:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI;

    .line 31
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI;

    const-string v1, "GEO_SIMILAR_PLACES"

    const-string v2, "/geo/similar_places.json"

    const-string v3, "GET"

    invoke-direct {v0, v1, v7, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI;->GEO_SIMILAR_PLACES:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI;

    .line 32
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI;

    const-string v1, "GEO_PLACE"

    const-string v2, "/geo/place.json"

    const-string v3, "POST"

    invoke-direct {v0, v1, v8, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI;->GEO_PLACE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI;

    .line 26
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI;

    sget-object v1, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI;->GEO_ID:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI;->GEO_REVERSE_GEOCODE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI;->GEO_SEARCH:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI;->GEO_SIMILAR_PLACES:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI;

    aput-object v1, v0, v7

    sget-object v1, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI;->GEO_PLACE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI;

    aput-object v1, v0, v8

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI;->$VALUES:[Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p3, "url"    # Ljava/lang/String;
    .param p4, "httpMethod"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 63
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 64
    iput-object p3, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI;->mUrl:Ljava/lang/String;

    .line 65
    iput-object p4, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI;->mHttpMethod:Ljava/lang/String;

    .line 66
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 26
    const-class v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI;
    .locals 1

    .prologue
    .line 26
    sget-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI;->$VALUES:[Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI;

    invoke-virtual {v0}, [Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI;

    return-object v0
.end method


# virtual methods
.method public getHttpMethod()Ljava/lang/String;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI;->mHttpMethod:Ljava/lang/String;

    return-object v0
.end method

.method public getUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI;->mUrl:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI;->mUrl:Ljava/lang/String;

    return-object v0
.end method
