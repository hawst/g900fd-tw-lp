.class public interface abstract Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserNotification$FacebookNotification$SnsFbEventQuery;
.super Ljava/lang/Object;
.source "SnsFbParserNotification.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserNotification$FacebookNotification;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "SnsFbEventQuery"
.end annotation


# static fields
.field public static final CREATOR:Ljava/lang/String; = "creator"

.field public static final DESCRIPTION:Ljava/lang/String; = "description"

.field public static final END_TIME:Ljava/lang/String; = "end_time"

.field public static final EVENT_ID:Ljava/lang/String; = "eid"

.field public static final EVENT_NAME:Ljava/lang/String; = "name"

.field public static final HOST:Ljava/lang/String; = "host"

.field public static final PRIVACY:Ljava/lang/String; = "privacy "

.field public static final START_TIME:Ljava/lang/String; = "start_time"

.field public static final UPDATE_TIME:Ljava/lang/String; = "update_time"
