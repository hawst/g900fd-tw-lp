.class public Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;
.super Ljava/lang/Object;
.source "SnsFbSyncDataMgr.java"


# static fields
.field public static final EVENT_LIMIT:Ljava/lang/String; = "limit 50"

.field public static final GALLERY_LIMIT:Ljava/lang/String; = "50"

.field private static final LIMIT_COUNT:I = 0xbb8

.field private static final MAX_ROW_COUNT_ALBUM:I = 0x64

.field private static final MAX_ROW_COUNT_PHOTO:I = 0x3e8


# instance fields
.field private mNextPaging:Ljava/lang/String;

.field private mPrevPaging:Ljava/lang/String;

.field private mlastStartTime:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 106
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 100
    iput-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->mPrevPaging:Ljava/lang/String;

    .line 102
    iput-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->mNextPaging:Ljava/lang/String;

    .line 104
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->mlastStartTime:Ljava/lang/String;

    .line 107
    return-void
.end method

.method private fromUTCtoLATimestamp(Ljava/lang/String;)Ljava/lang/Long;
    .locals 8
    .param p1, "timestamp"    # Ljava/lang/String;

    .prologue
    .line 809
    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    .line 810
    .local v2, "date":Ljava/util/Date;
    new-instance v0, Ljava/util/GregorianCalendar;

    invoke-direct {v0}, Ljava/util/GregorianCalendar;-><init>()V

    .line 811
    .local v0, "calendar":Ljava/util/Calendar;
    const-string v4, "America/Los_Angeles"

    invoke-static {v4}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v3

    .line 813
    .local v3, "la":Ljava/util/TimeZone;
    invoke-static {p1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    mul-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 815
    .local v1, "convertTimestamp":Ljava/lang/Long;
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/util/Date;->setTime(J)V

    .line 816
    invoke-virtual {v0, v2}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 818
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {v3, v6, v7}, Ljava/util/TimeZone;->getOffset(J)I

    move-result v6

    int-to-long v6, v6

    add-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    return-object v4
.end method

.method private isMaxCount(Landroid/net/Uri;Ljava/lang/String;I)Z
    .locals 10
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "albumId"    # Ljava/lang/String;
    .param p3, "maxCount"    # I

    .prologue
    .line 772
    const/4 v8, 0x0

    .line 773
    .local v8, "snsCursor":Landroid/database/Cursor;
    const/4 v6, 0x0

    .line 775
    .local v6, "bResult":Z
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/sns3/SnsApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 776
    .local v0, "cr":Landroid/content/ContentResolver;
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    .line 777
    .local v9, "where":Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    invoke-virtual {v9, v1, v2}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 779
    if-eqz p2, :cond_2

    .line 780
    const-string v1, "target_id"

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 781
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "=\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 782
    const-string v1, " AND "

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 783
    const-string v1, "_is_valid"

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 784
    const-string v1, "=0"

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 792
    :goto_0
    const/4 v2, 0x0

    :try_start_0
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 794
    if-eqz v8, :cond_0

    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-lt v1, p3, :cond_0

    .line 795
    const/4 v6, 0x1

    .line 800
    :cond_0
    if-eqz v8, :cond_1

    .line 801
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 804
    :cond_1
    :goto_1
    return v6

    .line 786
    :cond_2
    const-string v1, "_is_valid"

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 787
    const-string v1, "=0"

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 797
    :catch_0
    move-exception v7

    .line 798
    .local v7, "e":Ljava/lang/IllegalStateException;
    :try_start_1
    invoke-virtual {v7}, Ljava/lang/IllegalStateException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 800
    if-eqz v8, :cond_1

    .line 801
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 800
    .end local v7    # "e":Ljava/lang/IllegalStateException;
    :catchall_0
    move-exception v1

    if-eqz v8, :cond_3

    .line 801
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v1
.end method


# virtual methods
.method public clearTable(Landroid/net/Uri;)V
    .locals 3
    .param p1, "tableUri"    # Landroid/net/Uri;

    .prologue
    const/4 v2, 0x0

    .line 1592
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/sns3/SnsApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1593
    .local v0, "cr":Landroid/content/ContentResolver;
    invoke-virtual {v0, p1, v2, v2}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1595
    return-void
.end method

.method public deletePrevSyncState(Ljava/lang/String;)V
    .locals 5
    .param p1, "type"    # Ljava/lang/String;

    .prologue
    .line 751
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/sns3/SnsApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 752
    .local v0, "cr":Landroid/content/ContentResolver;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 755
    .local v1, "where":Ljava/lang/StringBuilder;
    const/4 v2, 0x0

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 756
    const-string v2, "sync_type"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 758
    const-string v2, "album"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 759
    const-string v2, "=\'album\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 760
    const-string v2, " OR "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 761
    const-string v2, "sync_type"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 762
    const-string v2, "=\'photo\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 767
    :cond_0
    :goto_0
    sget-object v2, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$PreviousSyncState;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v0, v2, v3, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 768
    return-void

    .line 763
    :cond_1
    const-string v2, "event"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 764
    const-string v2, "=\'event\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method public deleteSyncValue(Ljava/lang/String;I)V
    .locals 5
    .param p1, "type"    # Ljava/lang/String;
    .param p2, "value"    # I

    .prologue
    const/4 v4, 0x0

    .line 732
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/sns3/SnsApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 733
    .local v0, "cr":Landroid/content/ContentResolver;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 736
    .local v1, "where":Ljava/lang/StringBuilder;
    const/4 v2, 0x0

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 737
    const-string v2, "_is_valid"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 738
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 739
    const-string v2, "album"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 740
    sget-object v2, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$SyncAlbum;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 741
    sget-object v2, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$SyncPhoto;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 742
    sget-object v2, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$SyncPhotoImages;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 748
    :cond_0
    :goto_0
    return-void

    .line 743
    :cond_1
    const-string v2, "event"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 744
    sget-object v2, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$SyncEvent;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_0

    .line 745
    :cond_2
    const-string v2, "streams"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 746
    sget-object v2, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$PhotosOfUser;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_0
.end method

.method public fromCurrenttoUTCTimestamp()Ljava/lang/Long;
    .locals 6

    .prologue
    .line 823
    new-instance v0, Ljava/util/GregorianCalendar;

    invoke-direct {v0}, Ljava/util/GregorianCalendar;-><init>()V

    .line 825
    .local v0, "calendar":Ljava/util/Calendar;
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    return-object v1
.end method

.method public fromUTCTimestampToDate(J)Ljava/lang/String;
    .locals 5
    .param p1, "millis"    # J

    .prologue
    .line 830
    new-instance v0, Ljava/util/GregorianCalendar;

    invoke-direct {v0}, Ljava/util/GregorianCalendar;-><init>()V

    .line 831
    .local v0, "calendar":Ljava/util/Calendar;
    const-wide/16 v2, 0x3e8

    mul-long/2addr v2, p1

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 833
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v2, "yyyy-MM-dd"

    invoke-direct {v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 835
    .local v1, "sdf":Ljava/text/SimpleDateFormat;
    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method public getAlbumIdCursor()Landroid/database/Cursor;
    .locals 8

    .prologue
    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 111
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/sns3/SnsApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 112
    .local v0, "cr":Landroid/content/ContentResolver;
    const/4 v6, 0x0

    .line 114
    .local v6, "snsCursor":Landroid/database/Cursor;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 115
    .local v7, "where":Ljava/lang/StringBuilder;
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    invoke-virtual {v7, v5, v1}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 116
    const-string v1, "_is_valid"

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 117
    const-string v1, "=0"

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 119
    sget-object v1, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$SyncAlbum;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "id"

    aput-object v3, v2, v5

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 123
    return-object v6
.end method

.method public insertAlbums(Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAlbums;)Landroid/os/Bundle;
    .locals 11
    .param p1, "album"    # Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAlbums;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 286
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 288
    .local v0, "AlbumList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentValues;>;"
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/app/sns3/SnsApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 290
    .local v2, "cr":Landroid/content/ContentResolver;
    const/4 v3, 0x0

    .line 291
    .local v3, "curAlbum":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAlbum;
    const/4 v4, 0x0

    .line 293
    .local v4, "curPaging":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePaging;
    const/4 v1, 0x0

    .line 295
    .local v1, "bundle":Landroid/os/Bundle;
    if-eqz p1, :cond_8

    .line 296
    iget-object v3, p1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAlbums;->mAlbums:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAlbum;

    .line 297
    iget-object v4, p1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAlbums;->mPaging:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePaging;

    .line 301
    if-eqz v3, :cond_7

    sget-object v8, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$SyncAlbum;->CONTENT_URI:Landroid/net/Uri;

    const/4 v9, 0x0

    const/16 v10, 0x64

    invoke-direct {p0, v8, v9, v10}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->isMaxCount(Landroid/net/Uri;Ljava/lang/String;I)Z

    move-result v8

    if-nez v8, :cond_7

    .line 305
    if-eqz v4, :cond_0

    iget-object v8, v4, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePaging;->mNextPaging:Ljava/lang/String;

    invoke-virtual {v8}, Ljava/lang/String;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_0

    .line 306
    iget-object v8, p1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAlbums;->mPaging:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePaging;

    iget-object v8, v8, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePaging;->mNextPaging:Ljava/lang/String;

    invoke-static {v8}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->pagingUrl2Bundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    .line 308
    const-string v8, "limit"

    const-string v9, "50"

    invoke-virtual {v1, v8, v9}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 311
    iget-object v8, v4, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePaging;->mPreviousPaging:Ljava/lang/String;

    iput-object v8, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->mPrevPaging:Ljava/lang/String;

    .line 312
    iget-object v8, v4, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePaging;->mNextPaging:Ljava/lang/String;

    iput-object v8, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->mNextPaging:Ljava/lang/String;

    .line 315
    :cond_0
    :goto_0
    if-eqz v3, :cond_5

    .line 317
    iget-object v8, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAlbum;->mCount:Ljava/lang/String;

    if-eqz v8, :cond_1

    iget-object v8, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAlbum;->mCount:Ljava/lang/String;

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    if-eqz v8, :cond_1

    iget-object v8, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAlbum;->mCount:Ljava/lang/String;

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    if-lez v8, :cond_2

    iget-object v8, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAlbum;->mCount:Ljava/lang/String;

    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    if-gtz v8, :cond_2

    .line 321
    :cond_1
    iget-object v3, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAlbum;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAlbum;

    .line 322
    goto :goto_0

    .line 325
    :cond_2
    new-instance v7, Landroid/content/ContentValues;

    invoke-direct {v7}, Landroid/content/ContentValues;-><init>()V

    .line 327
    .local v7, "values":Landroid/content/ContentValues;
    invoke-virtual {v7}, Landroid/content/ContentValues;->clear()V

    .line 329
    const-string v8, "id"

    iget-object v9, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAlbum;->mAlbumID:Ljava/lang/String;

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 330
    const-string v8, "from_id"

    iget-object v9, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAlbum;->mFrom:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFrom;

    iget-object v9, v9, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFrom;->mFromID:Ljava/lang/String;

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 331
    const-string v8, "from_name"

    iget-object v9, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAlbum;->mFrom:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFrom;

    iget-object v9, v9, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFrom;->mFromName:Ljava/lang/String;

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 332
    const-string v8, "name"

    iget-object v9, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAlbum;->mAlbumName:Ljava/lang/String;

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 333
    const-string v8, "description"

    iget-object v9, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAlbum;->mDescription:Ljava/lang/String;

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 334
    const-string v8, "location"

    iget-object v9, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAlbum;->mLocation:Ljava/lang/String;

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 335
    const-string v8, "link"

    iget-object v9, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAlbum;->mLink:Ljava/lang/String;

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 336
    const-string v8, "cover_photo"

    iget-object v9, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAlbum;->mCoverPhoto:Ljava/lang/String;

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 337
    const-string v8, "privacy"

    iget-object v9, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAlbum;->mPrivacy:Ljava/lang/String;

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 338
    const-string v8, "count"

    iget-object v9, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAlbum;->mCount:Ljava/lang/String;

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 339
    const-string v8, "created_time"

    iget-object v9, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAlbum;->mCreatedTime:Ljava/lang/String;

    invoke-static {v9}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->toTimestamp(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 341
    const-string v8, "updated_time"

    iget-object v9, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAlbum;->mUpdatedTime:Ljava/lang/String;

    invoke-static {v9}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->toTimestamp(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 343
    const-string v8, "type"

    iget-object v9, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAlbum;->mType:Ljava/lang/String;

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 344
    iget-object v8, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->mPrevPaging:Ljava/lang/String;

    if-eqz v8, :cond_3

    .line 345
    const-string v8, "paging_previous"

    iget-object v9, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->mPrevPaging:Ljava/lang/String;

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 347
    :cond_3
    iget-object v8, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->mNextPaging:Ljava/lang/String;

    if-eqz v8, :cond_4

    .line 348
    const-string v8, "paging_next"

    iget-object v9, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->mNextPaging:Ljava/lang/String;

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 350
    :cond_4
    const-string v8, "_is_valid"

    const/4 v9, 0x0

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 352
    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 354
    iget-object v3, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAlbum;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAlbum;

    .line 355
    goto/16 :goto_0

    .line 357
    .end local v7    # "values":Landroid/content/ContentValues;
    :cond_5
    sget-object v9, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$SyncAlbum;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v8

    new-array v8, v8, [Landroid/content/ContentValues;

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v8

    check-cast v8, [Landroid/content/ContentValues;

    invoke-virtual {v2, v9, v8}, Landroid/content/ContentResolver;->bulkInsert(Landroid/net/Uri;[Landroid/content/ContentValues;)I

    move-result v6

    .line 360
    .local v6, "rowsCnt":I
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-eq v6, v8, :cond_6

    .line 362
    :try_start_0
    new-instance v8, Ljava/lang/Exception;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "insertAlbums - Response values have problems! rowsCnt : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", ListSize : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v8
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 366
    :catch_0
    move-exception v5

    .line 367
    .local v5, "e":Ljava/lang/Exception;
    throw v5

    .line 371
    .end local v5    # "e":Ljava/lang/Exception;
    :cond_6
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 374
    const-string v8, "album"

    invoke-virtual {p0, v8}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->deletePrevSyncState(Ljava/lang/String;)V

    .line 376
    new-instance v7, Landroid/content/ContentValues;

    invoke-direct {v7}, Landroid/content/ContentValues;-><init>()V

    .line 377
    .restart local v7    # "values":Landroid/content/ContentValues;
    const-string v8, "sync_type"

    const-string v9, "album"

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 379
    const-string v8, "previous_state"

    const/4 v9, 0x1

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 381
    const-string v8, "paging_previous"

    iget-object v9, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->mPrevPaging:Ljava/lang/String;

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 382
    const-string v8, "paging_next"

    iget-object v9, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->mNextPaging:Ljava/lang/String;

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 384
    sget-object v8, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$PreviousSyncState;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2, v8, v7}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 393
    .end local v6    # "rowsCnt":I
    .end local v7    # "values":Landroid/content/ContentValues;
    :goto_1
    return-object v1

    .line 387
    :cond_7
    const/4 v1, 0x0

    goto :goto_1

    .line 390
    :cond_8
    const/4 v1, 0x0

    goto :goto_1
.end method

.method insertBirthdays(Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseBirthday;)V
    .locals 9
    .param p1, "birthdayEvents"    # Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseBirthday;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 895
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 896
    .local v0, "BirthdayEventList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentValues;>;"
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/sns3/SnsApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 898
    .local v1, "cr":Landroid/content/ContentResolver;
    if-eqz p1, :cond_3

    .line 899
    move-object v2, p1

    .line 901
    .local v2, "curBirthdayEvents":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseBirthday;
    :goto_0
    if-eqz v2, :cond_1

    .line 903
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    .line 905
    .local v5, "values":Landroid/content/ContentValues;
    invoke-virtual {v5}, Landroid/content/ContentValues;->clear()V

    .line 907
    iget-object v6, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseBirthday;->mBirthdayDate:Ljava/lang/String;

    const-string v7, "null"

    if-eq v6, v7, :cond_0

    .line 909
    const-string v6, "uid"

    iget-object v7, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseBirthday;->mUId:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 910
    const-string v6, "first_name"

    iget-object v7, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseBirthday;->mFirstName:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 912
    const-string v6, "last_name"

    iget-object v7, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseBirthday;->mLastName:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 913
    const-string v6, "name"

    iget-object v7, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseBirthday;->mName:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 914
    const-string v6, "birthday_date"

    iget-object v7, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseBirthday;->mBirthdayDate:Ljava/lang/String;

    invoke-static {v7}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->toTimestampForBirthday(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 917
    const-string v6, "pic_url"

    iget-object v7, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseBirthday;->mPicUrl:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 919
    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 922
    :cond_0
    iget-object v2, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseBirthday;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseBirthday;

    .line 923
    goto :goto_0

    .line 925
    .end local v5    # "values":Landroid/content/ContentValues;
    :cond_1
    sget-object v7, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$Birthday;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v6

    new-array v6, v6, [Landroid/content/ContentValues;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [Landroid/content/ContentValues;

    invoke-virtual {v1, v7, v6}, Landroid/content/ContentResolver;->bulkInsert(Landroid/net/Uri;[Landroid/content/ContentValues;)I

    move-result v4

    .line 928
    .local v4, "rowsCnt":I
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-eq v4, v6, :cond_2

    .line 930
    :try_start_0
    new-instance v6, Ljava/lang/Exception;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "insertBirthdayEvents - Response values have problems! rowsCnt : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", ListSize : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v6
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 934
    :catch_0
    move-exception v3

    .line 935
    .local v3, "e":Ljava/lang/Exception;
    throw v3

    .line 939
    .end local v3    # "e":Ljava/lang/Exception;
    :cond_2
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 944
    .end local v2    # "curBirthdayEvents":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseBirthday;
    .end local v4    # "rowsCnt":I
    :cond_3
    return-void
.end method

.method public insertComments(Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseComments;Ljava/lang/String;)Landroid/os/Bundle;
    .locals 9
    .param p1, "comments"    # Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseComments;
    .param p2, "photoId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 1027
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/sns3/SnsApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 1028
    .local v1, "cr":Landroid/content/ContentResolver;
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    .line 1030
    .local v5, "values":Landroid/content/ContentValues;
    const/4 v2, 0x0

    .line 1031
    .local v2, "curComment":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseComment;
    const/4 v3, 0x0

    .line 1033
    .local v3, "curPaging":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePaging;
    const/4 v0, 0x0

    .line 1035
    .local v0, "bundle":Landroid/os/Bundle;
    if-eqz p1, :cond_2

    .line 1036
    iget-object v2, p1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseComments;->mComment:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseComment;

    .line 1037
    iget-object v3, p1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseComments;->mPaging:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePaging;

    .line 1039
    if-eqz v3, :cond_0

    iget-object v6, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePaging;->mNextPaging:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_0

    .line 1040
    iget-object v6, p1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseComments;->mPaging:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePaging;

    iget-object v6, v6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePaging;->mNextPaging:Ljava/lang/String;

    invoke-static {v6}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->pagingUrl2Bundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 1043
    :cond_0
    :goto_0
    if-eqz v2, :cond_3

    .line 1045
    invoke-virtual {v5}, Landroid/content/ContentValues;->clear()V

    .line 1046
    const-string v6, "comments_id"

    iget-object v7, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseComment;->mCommentID:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1047
    const-string v6, "target_id"

    invoke-virtual {v5, v6, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1048
    const-string v6, "target_type"

    iget-object v7, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseComment;->mFrom:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFrom;

    iget-object v7, v7, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFrom;->mFromName:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1049
    const-string v6, "from_id"

    iget-object v7, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseComment;->mFrom:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFrom;

    iget-object v7, v7, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFrom;->mFromID:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1050
    const-string v6, "from_name"

    iget-object v7, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseComment;->mFrom:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFrom;

    iget-object v7, v7, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFrom;->mFromName:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1051
    const-string v6, "message"

    iget-object v7, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseComment;->mMessage:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1052
    const-string v6, "created_time"

    iget-object v7, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseComment;->mCreatedTime:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1053
    const-string v6, "can_remove"

    iget-object v7, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseComment;->mCanRemove:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1054
    const-string v6, "likes"

    iget-object v7, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseComment;->mLikes:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1055
    const-string v6, "user_likes"

    iget-object v7, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseComment;->mUserLikes:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1057
    sget-object v6, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$Comments;->CONTENT_URI:Landroid/net/Uri;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "comments_id=\'"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseComment;->mCommentID:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "\'"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v1, v6, v5, v7, v8}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v4

    .line 1059
    .local v4, "update":I
    if-nez v4, :cond_1

    .line 1060
    sget-object v6, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$Comments;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1, v6, v5}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 1063
    :cond_1
    iget-object v2, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseComment;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseComment;

    .line 1064
    goto :goto_0

    .line 1067
    .end local v4    # "update":I
    :cond_2
    const/4 v0, 0x0

    .line 1070
    :cond_3
    return-object v0
.end method

.method public insertFeeds(Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFeed;)V
    .locals 7
    .param p1, "feed"    # Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFeed;

    .prologue
    const/4 v5, 0x0

    .line 1420
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1421
    .local v0, "contentValueList":Ljava/util/List;, "Ljava/util/List<Landroid/content/ContentValues;>;"
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/sns3/SnsApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 1423
    .local v1, "cr":Landroid/content/ContentResolver;
    if-eqz p1, :cond_0

    .line 1425
    sget-object v4, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$FeedList;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1, v4, v5, v5}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1426
    :goto_0
    if-eqz p1, :cond_0

    .line 1428
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 1429
    .local v2, "cv":Landroid/content/ContentValues;
    const-string v4, "feed_id"

    iget-object v5, p1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFeed;->mFeedID:Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1430
    const-string v4, "type"

    iget-object v5, p1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFeed;->mType:Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1431
    const-string v4, "icon"

    iget-object v5, p1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFeed;->mIcon:Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1432
    const-string v4, "caption"

    iget-object v5, p1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFeed;->mCaption:Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1433
    const-string v4, "message"

    iget-object v5, p1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFeed;->mMessage:Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1434
    const-string v4, "description"

    iget-object v5, p1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFeed;->mDescription:Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1435
    const-string v4, "picture"

    iget-object v5, p1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFeed;->mPicture:Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1436
    const-string v4, "story"

    iget-object v5, p1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFeed;->mStory:Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1437
    const-string v4, "status_type"

    iget-object v5, p1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFeed;->mStatusType:Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1438
    const-string v4, "created_time"

    iget-object v5, p1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFeed;->mCreatedTime:Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1439
    const-string v4, "updated_time"

    iget-object v5, p1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFeed;->mUpdatedTime:Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1440
    const-string v4, "from_id"

    iget-object v5, p1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFeed;->mFrom:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFrom;

    iget-object v5, v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFrom;->mFromID:Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1441
    const-string v4, "from_name"

    iget-object v5, p1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFeed;->mFrom:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFrom;

    iget-object v5, v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFrom;->mFromName:Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1442
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1443
    iget-object p1, p1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFeed;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFeed;

    .line 1444
    goto :goto_0

    .line 1447
    .end local v2    # "cv":Landroid/content/ContentValues;
    :cond_0
    sget-object v5, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$FeedList;->CONTENT_URI:Landroid/net/Uri;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    new-array v4, v4, [Landroid/content/ContentValues;

    invoke-interface {v0, v4}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Landroid/content/ContentValues;

    invoke-virtual {v1, v5, v4}, Landroid/content/ContentResolver;->bulkInsert(Landroid/net/Uri;[Landroid/content/ContentValues;)I

    move-result v3

    .line 1450
    .local v3, "rowsCnt":I
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    if-eq v3, v4, :cond_1

    .line 1452
    :try_start_0
    new-instance v4, Ljava/lang/Exception;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "insertFeeds - Response values have problems! rowsCnt : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", ListSize : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1456
    :catch_0
    move-exception v4

    .line 1461
    :cond_1
    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1462
    return-void
.end method

.method public insertFriends(Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsProfiles;)V
    .locals 8
    .param p1, "frndList"    # Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsProfiles;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 1184
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1185
    .local v0, "contentValueList":Ljava/util/List;, "Ljava/util/List<Landroid/content/ContentValues;>;"
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/sns3/SnsApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 1187
    .local v2, "cr":Landroid/content/ContentResolver;
    if-eqz p1, :cond_2

    .line 1188
    :goto_0
    if-eqz p1, :cond_0

    .line 1190
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 1191
    .local v1, "contentValues":Landroid/content/ContentValues;
    invoke-virtual {v1}, Landroid/content/ContentValues;->clear()V

    .line 1192
    const-string v5, "friend_id"

    iget-object v6, p1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsProfiles;->mProfileID:Ljava/lang/String;

    invoke-virtual {v1, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1193
    const-string v5, "friend_name"

    iget-object v6, p1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsProfiles;->mName:Ljava/lang/String;

    invoke-virtual {v1, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1194
    const-string v5, "friend_email"

    iget-object v6, p1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsProfiles;->mEmail:Ljava/lang/String;

    invoke-virtual {v1, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1195
    const-string v5, "friend_pic"

    iget-object v6, p1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsProfiles;->mPic:Ljava/lang/String;

    invoke-virtual {v1, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1196
    const-string v5, "friend_birthdate"

    iget-object v6, p1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsProfiles;->mBirthdayDate:Ljava/lang/String;

    invoke-virtual {v1, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1197
    const-string v5, "friend_availability"

    iget-object v6, p1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsProfiles;->mOnlinePresence:Ljava/lang/String;

    invoke-virtual {v1, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1199
    const-string v5, "friend_relation_status"

    iget-object v6, p1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsProfiles;->mRelationShipStatus:Ljava/lang/String;

    invoke-virtual {v1, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1201
    const-string v5, "friend_current_status"

    iget-object v6, p1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsProfiles;->mCurrentStatus:Ljava/lang/String;

    invoke-virtual {v1, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1203
    const-string v5, "friend_current_status_time"

    iget-object v6, p1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsProfiles;->mCurrentStatusTime:Ljava/lang/String;

    invoke-virtual {v1, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1205
    const-string v5, "friend_current_status_comment_count"

    iget-object v6, p1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsProfiles;->mCurrentStatusCommentCount:Ljava/lang/String;

    invoke-virtual {v1, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1207
    const-string v5, "friend_relationship"

    iget-object v6, p1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsProfiles;->mRelationShip:Ljava/lang/String;

    invoke-virtual {v1, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1208
    const-string v5, "interests"

    iget-object v6, p1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsProfiles;->mInterests:Ljava/lang/String;

    invoke-virtual {v1, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1209
    const-string v5, "update_time"

    iget-object v6, p1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsProfiles;->mUpdateTime:Ljava/lang/Long;

    invoke-virtual {v1, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1210
    const-string v5, "gender"

    iget-object v6, p1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsProfiles;->mGender:Ljava/lang/String;

    invoke-virtual {v1, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1211
    const-string v5, "current_location"

    iget-object v6, p1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsProfiles;->mCurrentLocation:Ljava/lang/String;

    invoke-virtual {v1, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1213
    const-string v5, "hometown_location"

    iget-object v6, p1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsProfiles;->mHomeTownLocation:Ljava/lang/String;

    invoke-virtual {v1, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1215
    const-string v5, "locale"

    iget-object v6, p1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsProfiles;->mLocale:Ljava/lang/String;

    invoke-virtual {v1, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1219
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1220
    iget-object p1, p1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsProfiles;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsProfiles;

    .line 1221
    goto/16 :goto_0

    .line 1223
    .end local v1    # "contentValues":Landroid/content/ContentValues;
    :cond_0
    sget-object v6, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$Friends;->CONTENT_URI:Landroid/net/Uri;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v5

    new-array v5, v5, [Landroid/content/ContentValues;

    invoke-interface {v0, v5}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [Landroid/content/ContentValues;

    invoke-virtual {v2, v6, v5}, Landroid/content/ContentResolver;->bulkInsert(Landroid/net/Uri;[Landroid/content/ContentValues;)I

    move-result v4

    .line 1226
    .local v4, "rowsCnt":I
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v5

    if-eq v4, v5, :cond_1

    .line 1228
    :try_start_0
    new-instance v5, Ljava/lang/Exception;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "insertFriends - Response values have problems! rowsCnt : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", ListSize : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v5
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1232
    :catch_0
    move-exception v3

    .line 1233
    .local v3, "e":Ljava/lang/Exception;
    throw v3

    .line 1237
    .end local v3    # "e":Ljava/lang/Exception;
    :cond_1
    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1240
    .end local v4    # "rowsCnt":I
    :cond_2
    return-void
.end method

.method public insertFriendsEduDetails(Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsEducationDetails;)V
    .locals 8
    .param p1, "frndList"    # Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsEducationDetails;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 1321
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1322
    .local v0, "contentValueList":Ljava/util/List;, "Ljava/util/List<Landroid/content/ContentValues;>;"
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/sns3/SnsApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 1323
    .local v2, "cr":Landroid/content/ContentResolver;
    if-eqz p1, :cond_2

    .line 1326
    sget-object v5, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$FriendsEducation;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2, v5, v6, v6}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1328
    :goto_0
    if-eqz p1, :cond_0

    .line 1329
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 1330
    .local v1, "contentValues":Landroid/content/ContentValues;
    invoke-virtual {v1}, Landroid/content/ContentValues;->clear()V

    .line 1331
    const-string v5, "profile_id"

    iget-object v6, p1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsEducationDetails;->mFriendID:Ljava/lang/String;

    invoke-virtual {v1, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1333
    const-string v5, "name"

    iget-object v6, p1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsEducationDetails;->mName:Ljava/lang/String;

    invoke-virtual {v1, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1334
    const-string v5, "school_id"

    iget-object v6, p1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsEducationDetails;->mSchoolId:Ljava/lang/String;

    invoke-virtual {v1, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1336
    const-string v5, "school_name"

    iget-object v6, p1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsEducationDetails;->mSchoolName:Ljava/lang/String;

    invoke-virtual {v1, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1338
    const-string v5, "degree_id"

    iget-object v6, p1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsEducationDetails;->mDegreeId:Ljava/lang/String;

    invoke-virtual {v1, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1340
    const-string v5, "degree_name"

    iget-object v6, p1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsEducationDetails;->mDegreeName:Ljava/lang/String;

    invoke-virtual {v1, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1342
    const-string v5, "year_id"

    iget-object v6, p1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsEducationDetails;->mYearId:Ljava/lang/String;

    invoke-virtual {v1, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1343
    const-string v5, "year_name"

    iget-object v6, p1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsEducationDetails;->mYearName:Ljava/lang/String;

    invoke-virtual {v1, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1345
    const-string v5, "type"

    iget-object v6, p1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsEducationDetails;->mType:Ljava/lang/String;

    invoke-virtual {v1, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1349
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1350
    iget-object p1, p1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsEducationDetails;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsEducationDetails;

    .line 1351
    goto :goto_0

    .line 1353
    .end local v1    # "contentValues":Landroid/content/ContentValues;
    :cond_0
    sget-object v6, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$FriendsEducation;->CONTENT_URI:Landroid/net/Uri;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v5

    new-array v5, v5, [Landroid/content/ContentValues;

    invoke-interface {v0, v5}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [Landroid/content/ContentValues;

    invoke-virtual {v2, v6, v5}, Landroid/content/ContentResolver;->bulkInsert(Landroid/net/Uri;[Landroid/content/ContentValues;)I

    move-result v4

    .line 1357
    .local v4, "rowsCnt":I
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v5

    if-eq v4, v5, :cond_1

    .line 1359
    :try_start_0
    new-instance v5, Ljava/lang/Exception;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "insertEducation - Response values have problems! rowsCnt : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", ListSize : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v5
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1363
    :catch_0
    move-exception v3

    .line 1364
    .local v3, "e":Ljava/lang/Exception;
    throw v3

    .line 1368
    .end local v3    # "e":Ljava/lang/Exception;
    :cond_1
    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1371
    .end local v4    # "rowsCnt":I
    :cond_2
    return-void
.end method

.method public insertFriendsWorkDetails(Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendWorkDetails;)V
    .locals 8
    .param p1, "frndList"    # Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendWorkDetails;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 1263
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1264
    .local v0, "contentValueList":Ljava/util/List;, "Ljava/util/List<Landroid/content/ContentValues;>;"
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/sns3/SnsApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 1265
    .local v2, "cr":Landroid/content/ContentResolver;
    if-eqz p1, :cond_2

    .line 1268
    sget-object v5, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$FriendsWork;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2, v5, v6, v6}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1270
    :goto_0
    if-eqz p1, :cond_0

    .line 1271
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 1272
    .local v1, "contentValues":Landroid/content/ContentValues;
    invoke-virtual {v1}, Landroid/content/ContentValues;->clear()V

    .line 1273
    const-string v5, "profile_id"

    iget-object v6, p1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendWorkDetails;->mFriendID:Ljava/lang/String;

    invoke-virtual {v1, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1274
    const-string v5, "name"

    iget-object v6, p1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendWorkDetails;->mName:Ljava/lang/String;

    invoke-virtual {v1, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1275
    const-string v5, "employer_id"

    iget-object v6, p1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendWorkDetails;->mEmployerID:Ljava/lang/String;

    invoke-virtual {v1, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1277
    const-string v5, "employer_name"

    iget-object v6, p1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendWorkDetails;->mEmployerName:Ljava/lang/String;

    invoke-virtual {v1, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1279
    const-string v5, "position_id"

    iget-object v6, p1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendWorkDetails;->mPositionId:Ljava/lang/String;

    invoke-virtual {v1, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1281
    const-string v5, "position_name"

    iget-object v6, p1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendWorkDetails;->mPositionName:Ljava/lang/String;

    invoke-virtual {v1, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1283
    const-string v5, "location_id"

    iget-object v6, p1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendWorkDetails;->mLocationId:Ljava/lang/String;

    invoke-virtual {v1, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1285
    const-string v5, "location_name"

    iget-object v6, p1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendWorkDetails;->mLocationName:Ljava/lang/String;

    invoke-virtual {v1, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1287
    const-string v5, "from_id"

    iget-object v6, p1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendWorkDetails;->mFromId:Ljava/lang/String;

    invoke-virtual {v1, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1288
    const-string v5, "from_name"

    iget-object v6, p1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendWorkDetails;->mFromName:Ljava/lang/String;

    invoke-virtual {v1, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1289
    const-string v5, "start_date"

    iget-object v6, p1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendWorkDetails;->mStartDate:Ljava/lang/String;

    invoke-virtual {v1, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1290
    const-string v5, "end_date"

    iget-object v6, p1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendWorkDetails;->mEndDate:Ljava/lang/String;

    invoke-virtual {v1, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1294
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1295
    iget-object p1, p1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendWorkDetails;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendWorkDetails;

    .line 1296
    goto :goto_0

    .line 1298
    .end local v1    # "contentValues":Landroid/content/ContentValues;
    :cond_0
    sget-object v6, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$FriendsWork;->CONTENT_URI:Landroid/net/Uri;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v5

    new-array v5, v5, [Landroid/content/ContentValues;

    invoke-interface {v0, v5}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [Landroid/content/ContentValues;

    invoke-virtual {v2, v6, v5}, Landroid/content/ContentResolver;->bulkInsert(Landroid/net/Uri;[Landroid/content/ContentValues;)I

    move-result v4

    .line 1302
    .local v4, "rowsCnt":I
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v5

    if-eq v4, v5, :cond_1

    .line 1304
    :try_start_0
    new-instance v5, Ljava/lang/Exception;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "insertPost - Response values have problems! rowsCnt : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", ListSize : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v5
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1308
    :catch_0
    move-exception v3

    .line 1309
    .local v3, "e":Ljava/lang/Exception;
    throw v3

    .line 1313
    .end local v3    # "e":Ljava/lang/Exception;
    :cond_1
    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1316
    .end local v4    # "rowsCnt":I
    :cond_2
    return-void
.end method

.method public insertGroupList(Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseGroupList;)V
    .locals 8
    .param p1, "groups"    # Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseGroupList;

    .prologue
    const/4 v6, 0x0

    .line 1374
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 1375
    .local v4, "valList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentValues;>;"
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/sns3/SnsApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1376
    .local v0, "cr":Landroid/content/ContentResolver;
    if-eqz p1, :cond_1

    .line 1378
    sget-object v5, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$Group;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v5, v6, v6}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1380
    iget-object v1, p1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseGroupList;->mGroups:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseGroup;

    .line 1382
    .local v1, "group":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseGroup;
    :goto_0
    if-eqz v1, :cond_0

    .line 1383
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 1384
    .local v3, "val":Landroid/content/ContentValues;
    invoke-virtual {v3}, Landroid/content/ContentValues;->clear()V

    .line 1385
    const-string v5, "id"

    iget-object v6, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseGroup;->mGroupID:Ljava/lang/String;

    invoke-virtual {v3, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1386
    const-string v5, "name"

    iget-object v6, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseGroup;->mGroupName:Ljava/lang/String;

    invoke-virtual {v3, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1387
    const-string v5, "administrator"

    iget-object v6, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseGroup;->mAdministrator:Ljava/lang/String;

    invoke-virtual {v3, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1388
    const-string v5, "owner_id"

    iget-object v6, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseGroup;->mOwnerID:Ljava/lang/String;

    invoke-virtual {v3, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1389
    const-string v5, "owner_name"

    iget-object v6, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseGroup;->mOwnerName:Ljava/lang/String;

    invoke-virtual {v3, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1390
    const-string v5, "icon"

    iget-object v6, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseGroup;->mIcon:Ljava/lang/String;

    invoke-virtual {v3, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1391
    const-string v5, "link"

    iget-object v6, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseGroup;->mLink:Ljava/lang/String;

    invoke-virtual {v3, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1392
    const-string v5, "privacy"

    iget-object v6, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseGroup;->mPrivacy:Ljava/lang/String;

    invoke-virtual {v3, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1393
    const-string v5, "updated_time"

    iget-object v6, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseGroup;->mUpdatedTime:Ljava/lang/String;

    invoke-virtual {v3, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1394
    const-string v5, "bookmark_order"

    iget-object v6, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseGroup;->mBookmarkOrder:Ljava/lang/String;

    invoke-virtual {v3, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1396
    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1397
    iget-object v1, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseGroup;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseGroup;

    .line 1398
    goto :goto_0

    .line 1400
    .end local v3    # "val":Landroid/content/ContentValues;
    :cond_0
    sget-object v6, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$Group;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v5

    new-array v5, v5, [Landroid/content/ContentValues;

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [Landroid/content/ContentValues;

    invoke-virtual {v0, v6, v5}, Landroid/content/ContentResolver;->bulkInsert(Landroid/net/Uri;[Landroid/content/ContentValues;)I

    move-result v2

    .line 1403
    .local v2, "rowsCnt":I
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-eq v2, v5, :cond_1

    .line 1405
    :try_start_0
    new-instance v5, Ljava/lang/Exception;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "insertGroup - Response values have problems! rowsCnt : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", ListSize : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v5
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1409
    :catch_0
    move-exception v5

    .line 1416
    .end local v1    # "group":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseGroup;
    .end local v2    # "rowsCnt":I
    :cond_1
    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    .line 1417
    return-void
.end method

.method public insertMembers(Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMembers;)V
    .locals 4
    .param p1, "members"    # Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMembers;

    .prologue
    .line 1527
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/sns3/SnsApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1529
    .local v0, "cr":Landroid/content/ContentResolver;
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 1531
    .local v1, "cv":Landroid/content/ContentValues;
    const-string v2, "group_id"

    iget-object v3, p1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMembers;->mGroupId:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1532
    const-string v2, "group_name"

    iget-object v3, p1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMembers;->mGroupName:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1533
    const-string v2, "group_type"

    iget-object v3, p1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMembers;->mGroupType:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1534
    const-string v2, "mem_id"

    iget-object v3, p1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMembers;->mID:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1535
    const-string v2, "mem_name"

    iget-object v3, p1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMembers;->mName:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1537
    sget-object v2, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$FrndListMembers;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 1539
    return-void
.end method

.method public insertNotification(Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNotiQueryResult;)V
    .locals 7
    .param p1, "mNoti"    # Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNotiQueryResult;

    .prologue
    const/4 v5, 0x0

    .line 1466
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1467
    .local v0, "contentValueList":Ljava/util/List;, "Ljava/util/List<Landroid/content/ContentValues;>;"
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/sns3/SnsApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 1469
    .local v2, "cr":Landroid/content/ContentResolver;
    if-eqz p1, :cond_2

    .line 1472
    sget-object v4, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$FriendsWork;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2, v4, v5, v5}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1474
    :goto_0
    if-eqz p1, :cond_0

    .line 1476
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 1477
    .local v1, "contentValues":Landroid/content/ContentValues;
    invoke-virtual {v1}, Landroid/content/ContentValues;->clear()V

    .line 1478
    const-string v4, "notification_id"

    iget-object v5, p1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNotiQueryResult;->mNotificationId:Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1480
    const-string v4, "sender_id"

    iget-object v5, p1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNotiQueryResult;->mSenderId:Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1481
    const-string v4, "recipient_id"

    iget-object v5, p1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNotiQueryResult;->mRecipientID:Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1483
    const-string v4, "object_id"

    iget-object v5, p1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNotiQueryResult;->mObjectID:Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1484
    const-string v4, "object_type"

    iget-object v5, p1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNotiQueryResult;->mObjectType:Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1485
    const-string v4, "created_time"

    iget-object v5, p1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNotiQueryResult;->mCreatedTime:Ljava/lang/Long;

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1487
    const-string v4, "updated_time"

    iget-object v5, p1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNotiQueryResult;->mUpdatedTime:Ljava/lang/Long;

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1489
    const-string v4, "title_html"

    iget-object v5, p1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNotiQueryResult;->mTitleHtml:Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1490
    const-string v4, "title_text"

    iget-object v5, p1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNotiQueryResult;->mTitleText:Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1491
    const-string v4, "body_html"

    iget-object v5, p1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNotiQueryResult;->mBodyHtml:Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1492
    const-string v4, "body_text"

    iget-object v5, p1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNotiQueryResult;->mBodyText:Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1493
    const-string v4, "href"

    iget-object v5, p1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNotiQueryResult;->mHref:Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1494
    const-string v4, "app_id"

    iget-object v5, p1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNotiQueryResult;->mAppId:Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1495
    const-string v4, "is_unread"

    iget-object v5, p1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNotiQueryResult;->mIsUnread:Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1496
    const-string v4, "is_hidden"

    iget-object v5, p1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNotiQueryResult;->mIsHidden:Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1497
    const-string v4, "icon_url"

    iget-object v5, p1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNotiQueryResult;->mIconUrl:Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1501
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1502
    iget-object p1, p1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNotiQueryResult;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNotiQueryResult;

    .line 1503
    goto :goto_0

    .line 1505
    .end local v1    # "contentValues":Landroid/content/ContentValues;
    :cond_0
    sget-object v5, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$FriendsWork;->CONTENT_URI:Landroid/net/Uri;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    new-array v4, v4, [Landroid/content/ContentValues;

    invoke-interface {v0, v4}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Landroid/content/ContentValues;

    invoke-virtual {v2, v5, v4}, Landroid/content/ContentResolver;->bulkInsert(Landroid/net/Uri;[Landroid/content/ContentValues;)I

    move-result v3

    .line 1509
    .local v3, "rowsCnt":I
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    if-eq v3, v4, :cond_1

    .line 1511
    :try_start_0
    new-instance v4, Ljava/lang/Exception;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "insertPost - Response values have problems! rowsCnt : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", ListSize : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1515
    :catch_0
    move-exception v4

    .line 1520
    :cond_1
    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1523
    .end local v3    # "rowsCnt":I
    :cond_2
    return-void
.end method

.method public insertPhotoLikes(Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLikes;Ljava/lang/String;)Landroid/os/Bundle;
    .locals 9
    .param p1, "likes"    # Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLikes;
    .param p2, "photoId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 1543
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/sns3/SnsApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 1544
    .local v1, "cr":Landroid/content/ContentResolver;
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    .line 1546
    .local v5, "values":Landroid/content/ContentValues;
    const/4 v2, 0x0

    .line 1547
    .local v2, "curLike":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLike;
    const/4 v3, 0x0

    .line 1549
    .local v3, "curPaging":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePaging;
    const/4 v0, 0x0

    .line 1551
    .local v0, "bundle":Landroid/os/Bundle;
    if-eqz p1, :cond_2

    .line 1552
    iget-object v2, p1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLikes;->mLike:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLike;

    .line 1553
    iget-object v3, p1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLikes;->mPaging:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePaging;

    .line 1555
    if-eqz v3, :cond_0

    iget-object v6, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePaging;->mNextPaging:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_0

    .line 1556
    iget-object v6, p1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLikes;->mPaging:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePaging;

    iget-object v6, v6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePaging;->mNextPaging:Ljava/lang/String;

    invoke-static {v6}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->pagingUrl2Bundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 1559
    :cond_0
    :goto_0
    if-eqz v2, :cond_3

    .line 1561
    invoke-virtual {v5}, Landroid/content/ContentValues;->clear()V

    .line 1563
    const-string v6, "likes_id"

    iget-object v7, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLike;->mLikeID:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1564
    const-string v6, "name"

    iget-object v7, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLike;->mName:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1565
    const-string v6, "category"

    iget-object v7, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLike;->mCategory:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1566
    const-string v6, "create_time"

    iget-object v7, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLike;->mCreatedTime:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1567
    const-string v6, "target_id"

    invoke-virtual {v5, v6, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1568
    const-string v6, "photo_url"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "https://graph.facebook.com/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLike;->mLikeID:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "/picture?type=normal"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1571
    sget-object v6, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$Likes;->CONTENT_URI:Landroid/net/Uri;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "likes_id=\'"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLike;->mLikeID:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "\' AND "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "target_id"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "=\'"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "\'"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v1, v6, v5, v7, v8}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v4

    .line 1573
    .local v4, "update":I
    if-nez v4, :cond_1

    .line 1574
    sget-object v6, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$Likes;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1, v6, v5}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 1577
    :cond_1
    iget-object v2, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLike;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLike;

    .line 1578
    goto/16 :goto_0

    .line 1581
    .end local v4    # "update":I
    :cond_2
    const/4 v0, 0x0

    .line 1584
    :cond_3
    return-object v0
.end method

.method public insertPhotos(Ljava/lang/String;Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhotos;)Landroid/os/Bundle;
    .locals 24
    .param p1, "albumId"    # Ljava/lang/String;
    .param p2, "photos"    # Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhotos;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 398
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 399
    .local v5, "PhotoList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentValues;>;"
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 401
    .local v4, "PhotoImageList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentValues;>;"
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/app/sns3/SnsApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    .line 403
    .local v9, "cr":Landroid/content/ContentResolver;
    const/4 v14, 0x0

    .line 404
    .local v14, "curPhoto":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;
    const/4 v13, 0x0

    .line 405
    .local v13, "curPaging":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePaging;
    const/4 v11, 0x0

    .line 406
    .local v11, "curImage":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseImage;
    const/4 v12, 0x0

    .line 407
    .local v12, "curLike":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLike;
    const/4 v10, 0x0

    .line 409
    .local v10, "curComment":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseComment;
    const/4 v6, 0x0

    .line 411
    .local v6, "bundle":Landroid/os/Bundle;
    if-eqz p2, :cond_b

    .line 412
    move-object/from16 v0, p2

    iget-object v14, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhotos;->mPhoto:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;

    .line 413
    move-object/from16 v0, p2

    iget-object v13, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhotos;->mPaging:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePaging;

    .line 417
    if-eqz v14, :cond_a

    sget-object v21, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$SyncPhoto;->CONTENT_URI:Landroid/net/Uri;

    const/16 v22, 0x3e8

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move-object/from16 v2, p1

    move/from16 v3, v22

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->isMaxCount(Landroid/net/Uri;Ljava/lang/String;I)Z

    move-result v21

    if-nez v21, :cond_a

    .line 420
    if-eqz v13, :cond_0

    iget-object v0, v13, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePaging;->mNextPaging:Ljava/lang/String;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Ljava/lang/String;->isEmpty()Z

    move-result v21

    if-nez v21, :cond_0

    .line 421
    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhotos;->mPaging:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePaging;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePaging;->mNextPaging:Ljava/lang/String;

    move-object/from16 v21, v0

    invoke-static/range {v21 .. v21}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->pagingUrl2Bundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v6

    .line 422
    const-string v21, "limit"

    const-string v22, "50"

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-virtual {v6, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 424
    iget-object v0, v13, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePaging;->mPreviousPaging:Ljava/lang/String;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->mPrevPaging:Ljava/lang/String;

    .line 425
    iget-object v0, v13, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePaging;->mNextPaging:Ljava/lang/String;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->mNextPaging:Ljava/lang/String;

    .line 428
    :cond_0
    :goto_0
    if-eqz v14, :cond_7

    .line 430
    new-instance v20, Landroid/content/ContentValues;

    invoke-direct/range {v20 .. v20}, Landroid/content/ContentValues;-><init>()V

    .line 432
    .local v20, "values":Landroid/content/ContentValues;
    invoke-virtual/range {v20 .. v20}, Landroid/content/ContentValues;->clear()V

    .line 434
    const-string v21, "id"

    iget-object v0, v14, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;->mPhotoID:Ljava/lang/String;

    move-object/from16 v22, v0

    invoke-virtual/range {v20 .. v22}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 435
    const-string v21, "target_id"

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    move-object/from16 v2, p1

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 436
    const-string v21, "from_id"

    iget-object v0, v14, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;->mFrom:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFrom;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFrom;->mFromID:Ljava/lang/String;

    move-object/from16 v22, v0

    invoke-virtual/range {v20 .. v22}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 437
    const-string v21, "from_name"

    iget-object v0, v14, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;->mFrom:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFrom;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFrom;->mFromName:Ljava/lang/String;

    move-object/from16 v22, v0

    invoke-virtual/range {v20 .. v22}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 438
    const-string v21, "name"

    iget-object v0, v14, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;->mPhotoName:Ljava/lang/String;

    move-object/from16 v22, v0

    invoke-virtual/range {v20 .. v22}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 439
    const-string v21, "picture"

    iget-object v0, v14, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;->mPicture:Ljava/lang/String;

    move-object/from16 v22, v0

    invoke-virtual/range {v20 .. v22}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 440
    const-string v21, "source"

    iget-object v0, v14, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;->mSource:Ljava/lang/String;

    move-object/from16 v22, v0

    invoke-virtual/range {v20 .. v22}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 441
    const-string v21, "height"

    iget v0, v14, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;->mHeight:I

    move/from16 v22, v0

    invoke-static/range {v22 .. v22}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v22

    invoke-virtual/range {v20 .. v22}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 442
    const-string v21, "width"

    iget v0, v14, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;->mWidth:I

    move/from16 v22, v0

    invoke-static/range {v22 .. v22}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v22

    invoke-virtual/range {v20 .. v22}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 443
    const-string v21, "link"

    iget-object v0, v14, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;->mLink:Ljava/lang/String;

    move-object/from16 v22, v0

    invoke-virtual/range {v20 .. v22}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 444
    const-string v21, "icon"

    iget-object v0, v14, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;->mIcon:Ljava/lang/String;

    move-object/from16 v22, v0

    invoke-virtual/range {v20 .. v22}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 445
    const-string v21, "created_time"

    iget-object v0, v14, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;->mCreatedTime:Ljava/lang/String;

    move-object/from16 v22, v0

    invoke-static/range {v22 .. v22}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->toTimestamp(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v22

    invoke-virtual/range {v20 .. v22}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 446
    const-string v21, "updated_time"

    iget-object v0, v14, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;->mUpdatedTime:Ljava/lang/String;

    move-object/from16 v22, v0

    invoke-static/range {v22 .. v22}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->toTimestamp(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v22

    invoke-virtual/range {v20 .. v22}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 447
    const-string v21, "position"

    iget v0, v14, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;->mPosition:I

    move/from16 v22, v0

    invoke-static/range {v22 .. v22}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v22

    invoke-virtual/range {v20 .. v22}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 448
    iget-object v0, v14, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;->mPlace:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePlace;

    move-object/from16 v21, v0

    if-eqz v21, :cond_1

    .line 449
    const-string v21, "latitude"

    iget-object v0, v14, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;->mPlace:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePlace;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePlace;->mLatitude:Ljava/lang/String;

    move-object/from16 v22, v0

    invoke-virtual/range {v20 .. v22}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 450
    const-string v21, "location_name"

    iget-object v0, v14, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;->mPlace:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePlace;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePlace;->mPlaceName:Ljava/lang/String;

    move-object/from16 v22, v0

    invoke-virtual/range {v20 .. v22}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 451
    const-string v21, "longitude"

    iget-object v0, v14, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;->mPlace:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePlace;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePlace;->mLongitude:Ljava/lang/String;

    move-object/from16 v22, v0

    invoke-virtual/range {v20 .. v22}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 453
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->mPrevPaging:Ljava/lang/String;

    move-object/from16 v21, v0

    if-eqz v21, :cond_2

    .line 454
    const-string v21, "paging_previous"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->mPrevPaging:Ljava/lang/String;

    move-object/from16 v22, v0

    invoke-virtual/range {v20 .. v22}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 456
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->mNextPaging:Ljava/lang/String;

    move-object/from16 v21, v0

    if-eqz v21, :cond_3

    .line 457
    const-string v21, "paging_previous"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->mNextPaging:Ljava/lang/String;

    move-object/from16 v22, v0

    invoke-virtual/range {v20 .. v22}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 459
    :cond_3
    const-string v21, "_is_valid"

    const/16 v22, 0x0

    invoke-static/range {v22 .. v22}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v22

    invoke-virtual/range {v20 .. v22}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 461
    iget-object v11, v14, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;->mImage:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseImage;

    .line 463
    :goto_1
    if-eqz v11, :cond_4

    .line 464
    new-instance v16, Landroid/content/ContentValues;

    invoke-direct/range {v16 .. v16}, Landroid/content/ContentValues;-><init>()V

    .line 466
    .local v16, "imageValues":Landroid/content/ContentValues;
    const-string v21, "target_id"

    iget-object v0, v14, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;->mPhotoID:Ljava/lang/String;

    move-object/from16 v22, v0

    move-object/from16 v0, v16

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 467
    const-string v21, "width"

    iget v0, v11, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseImage;->mWidth:I

    move/from16 v22, v0

    invoke-static/range {v22 .. v22}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v22

    move-object/from16 v0, v16

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 468
    const-string v21, "height"

    iget v0, v11, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseImage;->mHeight:I

    move/from16 v22, v0

    invoke-static/range {v22 .. v22}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v22

    move-object/from16 v0, v16

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 469
    const-string v21, "url"

    iget-object v0, v11, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseImage;->mSource:Ljava/lang/String;

    move-object/from16 v22, v0

    move-object/from16 v0, v16

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 470
    const-string v21, "_is_valid"

    const/16 v22, 0x0

    invoke-static/range {v22 .. v22}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v22

    move-object/from16 v0, v16

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 472
    move-object/from16 v0, v16

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 473
    iget-object v11, v11, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseImage;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseImage;

    .line 474
    goto :goto_1

    .line 476
    .end local v16    # "imageValues":Landroid/content/ContentValues;
    :cond_4
    iget-object v12, v14, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;->mLike:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLike;

    .line 477
    const/16 v17, 0x0

    .line 478
    .local v17, "likeCount":I
    :goto_2
    if-eqz v12, :cond_5

    .line 479
    new-instance v18, Landroid/content/ContentValues;

    invoke-direct/range {v18 .. v18}, Landroid/content/ContentValues;-><init>()V

    .line 480
    .local v18, "likeValues":Landroid/content/ContentValues;
    const-string v21, "target_id"

    iget-object v0, v14, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;->mPhotoID:Ljava/lang/String;

    move-object/from16 v22, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 481
    const-string v21, "profile_id"

    iget-object v0, v12, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLike;->mLikeID:Ljava/lang/String;

    move-object/from16 v22, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 482
    const-string v21, "name"

    iget-object v0, v12, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLike;->mName:Ljava/lang/String;

    move-object/from16 v22, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 484
    add-int/lit8 v17, v17, 0x1

    .line 485
    iget-object v12, v12, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLike;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLike;

    .line 486
    goto :goto_2

    .line 487
    .end local v18    # "likeValues":Landroid/content/ContentValues;
    :cond_5
    const-string v21, "likes_count"

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v22

    invoke-virtual/range {v20 .. v22}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 489
    iget-object v10, v14, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;->mComment:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseComment;

    .line 490
    const/4 v7, 0x0

    .line 491
    .local v7, "commentCount":I
    :goto_3
    if-eqz v10, :cond_6

    .line 492
    new-instance v8, Landroid/content/ContentValues;

    invoke-direct {v8}, Landroid/content/ContentValues;-><init>()V

    .line 493
    .local v8, "commentValues":Landroid/content/ContentValues;
    const-string v21, "target_id"

    iget-object v0, v14, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;->mPhotoID:Ljava/lang/String;

    move-object/from16 v22, v0

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-virtual {v8, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 494
    const-string v21, "from_id"

    iget-object v0, v10, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseComment;->mFrom:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFrom;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFrom;->mFromID:Ljava/lang/String;

    move-object/from16 v22, v0

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-virtual {v8, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 495
    const-string v21, "from_name"

    iget-object v0, v10, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseComment;->mFrom:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFrom;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFrom;->mFromName:Ljava/lang/String;

    move-object/from16 v22, v0

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-virtual {v8, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 496
    const-string v21, "message"

    iget-object v0, v10, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseComment;->mMessage:Ljava/lang/String;

    move-object/from16 v22, v0

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-virtual {v8, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 497
    const-string v21, "created_time"

    iget-object v0, v10, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseComment;->mCreatedTime:Ljava/lang/String;

    move-object/from16 v22, v0

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-virtual {v8, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 499
    add-int/lit8 v7, v7, 0x1

    .line 500
    iget-object v10, v10, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseComment;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseComment;

    .line 501
    goto :goto_3

    .line 502
    .end local v8    # "commentValues":Landroid/content/ContentValues;
    :cond_6
    const-string v21, "comments_count"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v22

    invoke-virtual/range {v20 .. v22}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 504
    move-object/from16 v0, v20

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 506
    iget-object v14, v14, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;

    .line 507
    goto/16 :goto_0

    .line 509
    .end local v7    # "commentCount":I
    .end local v17    # "likeCount":I
    .end local v20    # "values":Landroid/content/ContentValues;
    :cond_7
    sget-object v22, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$SyncPhoto;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v21

    move/from16 v0, v21

    new-array v0, v0, [Landroid/content/ContentValues;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v21

    check-cast v21, [Landroid/content/ContentValues;

    move-object/from16 v0, v22

    move-object/from16 v1, v21

    invoke-virtual {v9, v0, v1}, Landroid/content/ContentResolver;->bulkInsert(Landroid/net/Uri;[Landroid/content/ContentValues;)I

    move-result v19

    .line 512
    .local v19, "rowsCnt":I
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v21

    move/from16 v0, v19

    move/from16 v1, v21

    if-eq v0, v1, :cond_8

    .line 514
    :try_start_0
    new-instance v21, Ljava/lang/Exception;

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "insertPhotos - Response values have problems! rowsCnt : "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, ", ListSize : "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-direct/range {v21 .. v22}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v21
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 517
    :catch_0
    move-exception v15

    .line 518
    .local v15, "e":Ljava/lang/Exception;
    throw v15

    .line 522
    .end local v15    # "e":Ljava/lang/Exception;
    :cond_8
    invoke-virtual {v5}, Ljava/util/ArrayList;->clear()V

    .line 524
    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v21

    if-nez v21, :cond_9

    .line 525
    sget-object v22, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$SyncPhotoImages;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v21

    move/from16 v0, v21

    new-array v0, v0, [Landroid/content/ContentValues;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v21

    check-cast v21, [Landroid/content/ContentValues;

    move-object/from16 v0, v22

    move-object/from16 v1, v21

    invoke-virtual {v9, v0, v1}, Landroid/content/ContentResolver;->bulkInsert(Landroid/net/Uri;[Landroid/content/ContentValues;)I

    .line 528
    :cond_9
    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    .line 531
    const-string v21, "album"

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->deletePrevSyncState(Ljava/lang/String;)V

    .line 533
    new-instance v20, Landroid/content/ContentValues;

    invoke-direct/range {v20 .. v20}, Landroid/content/ContentValues;-><init>()V

    .line 534
    .restart local v20    # "values":Landroid/content/ContentValues;
    const-string v21, "sync_type"

    const-string v22, "photo"

    invoke-virtual/range {v20 .. v22}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 535
    const-string v21, "previous_state"

    const/16 v22, 0x1

    invoke-static/range {v22 .. v22}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v22

    invoke-virtual/range {v20 .. v22}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 536
    const-string v21, "paging_previous"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->mPrevPaging:Ljava/lang/String;

    move-object/from16 v22, v0

    invoke-virtual/range {v20 .. v22}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 537
    const-string v21, "paging_next"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->mNextPaging:Ljava/lang/String;

    move-object/from16 v22, v0

    invoke-virtual/range {v20 .. v22}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 539
    sget-object v21, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$PreviousSyncState;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, v21

    move-object/from16 v1, v20

    invoke-virtual {v9, v0, v1}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 548
    .end local v19    # "rowsCnt":I
    .end local v20    # "values":Landroid/content/ContentValues;
    :goto_4
    return-object v6

    .line 542
    :cond_a
    const/4 v6, 0x0

    goto :goto_4

    .line 545
    :cond_b
    const/4 v6, 0x0

    goto :goto_4
.end method

.method public insertPhotosOfUser(Ljava/lang/String;Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhotos;Ljava/util/ArrayList;)Landroid/os/Bundle;
    .locals 9
    .param p1, "albumId"    # Ljava/lang/String;
    .param p2, "photos"    # Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhotos;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhotos;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Landroid/os/Bundle;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .local p3, "photoIdList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v8, 0x0

    .line 554
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 556
    .local v0, "PhotoList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentValues;>;"
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/sns3/SnsApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 558
    .local v2, "cr":Landroid/content/ContentResolver;
    const/4 v4, 0x0

    .line 559
    .local v4, "curPhoto":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;
    const/4 v3, 0x0

    .line 561
    .local v3, "curPaging":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePaging;
    const/4 v1, 0x0

    .line 563
    .local v1, "bundle":Landroid/os/Bundle;
    if-eqz p2, :cond_d

    .line 564
    iget-object v4, p2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhotos;->mPhoto:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;

    .line 565
    iget-object v3, p2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhotos;->mPaging:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePaging;

    .line 569
    if-eqz v4, :cond_c

    sget-object v6, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$PhotosOfUser;->CONTENT_URI:Landroid/net/Uri;

    const/16 v7, 0x3e8

    invoke-direct {p0, v6, p1, v7}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->isMaxCount(Landroid/net/Uri;Ljava/lang/String;I)Z

    move-result v6

    if-nez v6, :cond_c

    .line 573
    if-eqz v3, :cond_0

    iget-object v6, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePaging;->mNextPaging:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_0

    .line 574
    iget-object v6, p2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhotos;->mPaging:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePaging;

    iget-object v6, v6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePaging;->mNextPaging:Ljava/lang/String;

    invoke-static {v6}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->pagingUrl2Bundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    .line 576
    const-string v6, "limit"

    const-string v7, "50"

    invoke-virtual {v1, v6, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 579
    iget-object v6, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePaging;->mPreviousPaging:Ljava/lang/String;

    iput-object v6, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->mPrevPaging:Ljava/lang/String;

    .line 580
    iget-object v6, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePaging;->mNextPaging:Ljava/lang/String;

    iput-object v6, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->mNextPaging:Ljava/lang/String;

    .line 583
    :cond_0
    :goto_0
    if-eqz v4, :cond_a

    .line 584
    const/4 v5, 0x0

    .line 586
    .local v5, "values":Landroid/content/ContentValues;
    if-nez p3, :cond_6

    .line 588
    new-instance v5, Landroid/content/ContentValues;

    .end local v5    # "values":Landroid/content/ContentValues;
    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    .line 590
    .restart local v5    # "values":Landroid/content/ContentValues;
    invoke-virtual {v5}, Landroid/content/ContentValues;->clear()V

    .line 592
    const-string v6, "id"

    iget-object v7, v4, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;->mPhotoID:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 593
    const-string v6, "target_id"

    invoke-virtual {v5, v6, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 594
    const-string v6, "from_id"

    iget-object v7, v4, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;->mFrom:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFrom;

    iget-object v7, v7, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFrom;->mFromID:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 595
    const-string v6, "from_name"

    iget-object v7, v4, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;->mFrom:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFrom;

    iget-object v7, v7, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFrom;->mFromName:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 597
    const-string v6, "name"

    iget-object v7, v4, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;->mPhotoName:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 598
    const-string v6, "picture"

    iget-object v7, v4, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;->mPicture:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 599
    const-string v6, "source"

    iget-object v7, v4, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;->mSource:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 600
    const-string v6, "height"

    iget v7, v4, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;->mHeight:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 601
    const-string v6, "width"

    iget v7, v4, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;->mWidth:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 602
    const-string v6, "link"

    iget-object v7, v4, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;->mLink:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 603
    const-string v6, "icon"

    iget-object v7, v4, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;->mIcon:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 604
    const-string v6, "created_time"

    iget-object v7, v4, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;->mCreatedTime:Ljava/lang/String;

    invoke-static {v7}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->toTimestamp(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 606
    const-string v6, "updated_time"

    iget-object v7, v4, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;->mUpdatedTime:Ljava/lang/String;

    invoke-static {v7}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->toTimestamp(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 608
    const-string v6, "position"

    iget v7, v4, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;->mPosition:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 609
    iget-object v6, v4, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;->mPlace:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePlace;

    if-eqz v6, :cond_1

    .line 610
    const-string v6, "latitude"

    iget-object v7, v4, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;->mPlace:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePlace;

    iget-object v7, v7, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePlace;->mLatitude:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 612
    const-string v6, "location_name"

    iget-object v7, v4, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;->mPlace:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePlace;

    iget-object v7, v7, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePlace;->mPlaceName:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 614
    const-string v6, "longitude"

    iget-object v7, v4, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;->mPlace:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePlace;

    iget-object v7, v7, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePlace;->mLongitude:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 617
    :cond_1
    iget-object v6, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->mPrevPaging:Ljava/lang/String;

    if-eqz v6, :cond_2

    .line 618
    const-string v6, "paging_previous"

    iget-object v7, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->mPrevPaging:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 620
    :cond_2
    iget-object v6, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->mNextPaging:Ljava/lang/String;

    if-eqz v6, :cond_3

    .line 621
    const-string v6, "paging_previous"

    iget-object v7, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->mNextPaging:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 623
    :cond_3
    const-string v6, "_is_valid"

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 665
    :cond_4
    :goto_1
    if-eqz v5, :cond_5

    .line 666
    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 668
    :cond_5
    iget-object v4, v4, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;

    .line 669
    goto/16 :goto_0

    .line 624
    :cond_6
    if-eqz p3, :cond_4

    iget-object v6, v4, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;->mPhotoID:Ljava/lang/String;

    invoke-virtual {p3, v6}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_4

    .line 627
    new-instance v5, Landroid/content/ContentValues;

    .end local v5    # "values":Landroid/content/ContentValues;
    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    .line 629
    .restart local v5    # "values":Landroid/content/ContentValues;
    invoke-virtual {v5}, Landroid/content/ContentValues;->clear()V

    .line 631
    const-string v6, "id"

    iget-object v7, v4, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;->mPhotoID:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 632
    const-string v6, "target_id"

    invoke-virtual {v5, v6, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 633
    const-string v6, "from_id"

    iget-object v7, v4, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;->mFrom:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFrom;

    iget-object v7, v7, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFrom;->mFromID:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 634
    const-string v6, "from_name"

    iget-object v7, v4, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;->mFrom:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFrom;

    iget-object v7, v7, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFrom;->mFromName:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 636
    const-string v6, "name"

    iget-object v7, v4, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;->mPhotoName:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 637
    const-string v6, "picture"

    iget-object v7, v4, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;->mPicture:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 638
    const-string v6, "source"

    iget-object v7, v4, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;->mSource:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 639
    const-string v6, "height"

    iget v7, v4, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;->mHeight:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 640
    const-string v6, "width"

    iget v7, v4, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;->mWidth:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 641
    const-string v6, "link"

    iget-object v7, v4, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;->mLink:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 642
    const-string v6, "icon"

    iget-object v7, v4, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;->mIcon:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 643
    const-string v6, "created_time"

    iget-object v7, v4, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;->mCreatedTime:Ljava/lang/String;

    invoke-static {v7}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->toTimestamp(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 645
    const-string v6, "updated_time"

    iget-object v7, v4, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;->mUpdatedTime:Ljava/lang/String;

    invoke-static {v7}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->toTimestamp(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 647
    const-string v6, "position"

    iget v7, v4, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;->mPosition:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 648
    iget-object v6, v4, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;->mPlace:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePlace;

    if-eqz v6, :cond_7

    .line 649
    const-string v6, "latitude"

    iget-object v7, v4, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;->mPlace:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePlace;

    iget-object v7, v7, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePlace;->mLatitude:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 651
    const-string v6, "location_name"

    iget-object v7, v4, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;->mPlace:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePlace;

    iget-object v7, v7, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePlace;->mPlaceName:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 653
    const-string v6, "longitude"

    iget-object v7, v4, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;->mPlace:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePlace;

    iget-object v7, v7, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePlace;->mLongitude:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 656
    :cond_7
    iget-object v6, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->mPrevPaging:Ljava/lang/String;

    if-eqz v6, :cond_8

    .line 657
    const-string v6, "paging_previous"

    iget-object v7, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->mPrevPaging:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 659
    :cond_8
    iget-object v6, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->mNextPaging:Ljava/lang/String;

    if-eqz v6, :cond_9

    .line 660
    const-string v6, "paging_previous"

    iget-object v7, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->mNextPaging:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 662
    :cond_9
    const-string v6, "_is_valid"

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_1

    .line 671
    .end local v5    # "values":Landroid/content/ContentValues;
    :cond_a
    if-eqz v0, :cond_b

    .line 672
    sget-object v7, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$PhotosOfUser;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v6

    new-array v6, v6, [Landroid/content/ContentValues;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [Landroid/content/ContentValues;

    invoke-virtual {v2, v7, v6}, Landroid/content/ContentResolver;->bulkInsert(Landroid/net/Uri;[Landroid/content/ContentValues;)I

    .line 674
    :cond_b
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 683
    :goto_2
    return-object v1

    .line 677
    :cond_c
    const/4 v1, 0x0

    goto :goto_2

    .line 680
    :cond_d
    const/4 v1, 0x0

    goto :goto_2
.end method

.method public insertPosts(Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePosts;)V
    .locals 6
    .param p1, "posts"    # Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePosts;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 948
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 949
    .local v0, "contentValueList":Ljava/util/List;, "Ljava/util/List<Landroid/content/ContentValues;>;"
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/sns3/SnsApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 950
    .local v2, "cr":Landroid/content/ContentResolver;
    if-eqz p1, :cond_1

    .line 952
    sget-object v4, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$Post;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2, v4, v5, v5}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 954
    iget-object v3, p1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePosts;->mPosts:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;

    .line 955
    .local v3, "tempPost":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;
    :goto_0
    if-eqz v3, :cond_0

    .line 957
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 958
    .local v1, "contentValues":Landroid/content/ContentValues;
    invoke-virtual {v1}, Landroid/content/ContentValues;->clear()V

    .line 959
    const-string v4, "post_id"

    iget-object v5, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mPostID:Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 960
    const-string v4, "from_id"

    iget-object v5, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mFrom:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFrom;

    iget-object v5, v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFrom;->mFromID:Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 961
    const-string v4, "from_name"

    iget-object v5, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mFrom:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFrom;

    iget-object v5, v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFrom;->mFromName:Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 962
    const-string v4, "to_id"

    iget-object v5, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mToID:Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 963
    const-string v4, "to_name"

    iget-object v5, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mToName:Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 964
    const-string v4, "message"

    iget-object v5, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mMessage:Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 965
    const-string v4, "picture"

    iget-object v5, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mPicture:Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 966
    const-string v4, "link"

    iget-object v5, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mLink:Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 967
    const-string v4, "name"

    iget-object v5, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mName:Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 968
    const-string v4, "caption"

    iget-object v5, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mCaption:Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 969
    const-string v4, "description"

    iget-object v5, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mDescription:Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 970
    const-string v4, "source"

    iget-object v5, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mSource:Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 971
    const-string v4, "properties"

    iget-object v5, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mProperties:Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 972
    const-string v4, "icon"

    iget-object v5, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mIcon:Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 973
    const-string v4, "actions"

    iget-object v5, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mActions:Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 974
    const-string v4, "privacy"

    iget-object v5, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mPrivacy:Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 975
    const-string v4, "type"

    iget-object v5, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mType:Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 977
    const-string v4, "likes_count"

    iget-object v5, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mLikeCount:Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 978
    const-string v4, "comments_count"

    iget-object v5, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mCommentCount:Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 979
    const-string v4, "object_id"

    iget-object v5, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mObjectID:Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 984
    const-string v4, "create_time"

    iget-object v5, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mCreatedTime:Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 985
    const-string v4, "updated_time"

    iget-object v5, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mUpdatedTime:Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 986
    const-string v4, "targeting"

    iget-object v5, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mTargeting:Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1001
    const-string v4, "shares"

    iget v5, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mShares:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1002
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1003
    sget-object v4, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$Post;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2, v4, v1}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 1004
    iget-object v3, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;

    .line 1005
    goto/16 :goto_0

    .line 1021
    .end local v1    # "contentValues":Landroid/content/ContentValues;
    :cond_0
    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1023
    .end local v3    # "tempPost":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;
    :cond_1
    return-void
.end method

.method public insertTagInfo(Ljava/util/ArrayList;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhotos;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 1077
    .local p1, "photoArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhotos;>;"
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1078
    .local v0, "contentValueList":Ljava/util/List;, "Ljava/util/List<Landroid/content/ContentValues;>;"
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/app/sns3/SnsApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 1081
    .local v2, "cr":Landroid/content/ContentResolver;
    const/4 v3, 0x0

    .line 1082
    .local v3, "curPhoto":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;
    const/4 v4, 0x0

    .line 1084
    .local v4, "curTags":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTags;
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .local v6, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_4

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhotos;

    .line 1085
    .local v7, "photo":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhotos;
    if-eqz v7, :cond_0

    .line 1089
    iget-object v3, v7, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhotos;->mPhoto:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;

    .line 1091
    const/4 v1, 0x0

    .line 1093
    .local v1, "contentValues":Landroid/content/ContentValues;
    :goto_1
    if-eqz v3, :cond_2

    .line 1094
    iget-object v4, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;->mTags:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTags;

    .line 1096
    :goto_2
    if-eqz v4, :cond_1

    .line 1097
    new-instance v1, Landroid/content/ContentValues;

    .end local v1    # "contentValues":Landroid/content/ContentValues;
    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 1098
    .restart local v1    # "contentValues":Landroid/content/ContentValues;
    invoke-virtual {v1}, Landroid/content/ContentValues;->clear()V

    .line 1099
    const-string v9, "target_id"

    iget-object v10, v4, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTags;->mPhotoId:Ljava/lang/String;

    invoke-virtual {v1, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1100
    const-string v9, "target_type"

    const-string v10, "photo"

    invoke-virtual {v1, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1101
    const-string v9, "tagged_name"

    iget-object v10, v4, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTags;->mTagName:Ljava/lang/String;

    invoke-virtual {v1, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1102
    const-string v9, "profile_id"

    iget-object v10, v4, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTags;->mTagID:Ljava/lang/String;

    invoke-virtual {v1, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1103
    const-string v9, "x"

    iget-object v10, v4, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTags;->mTagX:Ljava/lang/String;

    invoke-virtual {v1, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1104
    const-string v9, "y"

    iget-object v10, v4, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTags;->mTagY:Ljava/lang/String;

    invoke-virtual {v1, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1105
    const-string v9, "created_time"

    iget-object v10, v4, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTags;->mCreatedTime:Ljava/lang/String;

    invoke-static {v10}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->toTimestamp(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v1, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1110
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1111
    iget-object v4, v4, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTags;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTags;

    goto :goto_2

    .line 1114
    :cond_1
    iget-object v3, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;

    goto :goto_1

    .line 1117
    :cond_2
    sget-object v10, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$SyncTags;->CONTENT_URI:Landroid/net/Uri;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v9

    new-array v9, v9, [Landroid/content/ContentValues;

    invoke-interface {v0, v9}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v9

    check-cast v9, [Landroid/content/ContentValues;

    invoke-virtual {v2, v10, v9}, Landroid/content/ContentResolver;->bulkInsert(Landroid/net/Uri;[Landroid/content/ContentValues;)I

    move-result v8

    .line 1120
    .local v8, "rowsCnt":I
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v9

    if-eq v8, v9, :cond_3

    .line 1122
    :try_start_0
    new-instance v9, Ljava/lang/Exception;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "insertPost - Response values have problems! rowsCnt : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ", ListSize : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v9
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1126
    :catch_0
    move-exception v5

    .line 1127
    .local v5, "e":Ljava/lang/Exception;
    throw v5

    .line 1131
    .end local v5    # "e":Ljava/lang/Exception;
    :cond_3
    invoke-interface {v0}, Ljava/util/List;->clear()V

    goto/16 :goto_0

    .line 1135
    .end local v1    # "contentValues":Landroid/content/ContentValues;
    .end local v7    # "photo":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhotos;
    .end local v8    # "rowsCnt":I
    :cond_4
    return-void
.end method

.method public insertUser(Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMyProfile;)V
    .locals 4
    .param p1, "myProfile"    # Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMyProfile;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 1140
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/sns3/SnsApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 1142
    .local v1, "cr":Landroid/content/ContentResolver;
    if-eqz p1, :cond_0

    .line 1144
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 1145
    .local v0, "contentValues":Landroid/content/ContentValues;
    invoke-virtual {v0}, Landroid/content/ContentValues;->clear()V

    .line 1146
    const-string v2, "friend_id"

    iget-object v3, p1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMyProfile;->mId:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1147
    const-string v2, "friend_name"

    iget-object v3, p1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMyProfile;->mName:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1148
    const-string v2, "friend_email"

    iget-object v3, p1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMyProfile;->mEmail:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1149
    const-string v2, "friend_pic"

    iget-object v3, p1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMyProfile;->mPic:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1150
    const-string v2, "friend_birthdate"

    iget-object v3, p1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMyProfile;->mBirthdayDate:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1151
    const-string v2, "friend_availability"

    iget-object v3, p1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMyProfile;->mOnlinePresence:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1153
    const-string v2, "friend_relation_status"

    iget-object v3, p1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMyProfile;->mRelationShipStatus:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1155
    const-string v2, "friend_current_status"

    iget-object v3, p1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMyProfile;->mCurrentStatus:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1157
    const-string v2, "friend_current_status_time"

    iget-object v3, p1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMyProfile;->mCurrentStatusTime:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1159
    const-string v2, "friend_current_status_comment_count"

    iget-object v3, p1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMyProfile;->mCurrentStatusCommentCount:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1161
    const-string v2, "friend_relationship"

    iget-object v3, p1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMyProfile;->mRelationShip:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1163
    const-string v2, "interests"

    iget-object v3, p1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMyProfile;->mInterests:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1164
    const-string v2, "update_time"

    iget-object v3, p1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMyProfile;->mUpdateTime:Ljava/lang/Long;

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1165
    const-string v2, "gender"

    iget-object v3, p1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMyProfile;->mGender:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1166
    const-string v2, "current_location"

    iget-object v3, p1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMyProfile;->mCurrentLocation:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1168
    const-string v2, "hometown_location"

    iget-object v3, p1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMyProfile;->mHomeTownLocation:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1170
    const-string v2, "locale"

    iget-object v3, p1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMyProfile;->mLocale:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1175
    sget-object v2, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$Friends;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 1179
    .end local v0    # "contentValues":Landroid/content/ContentValues;
    :cond_0
    return-void
.end method

.method public insetBirthdayEvents(Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseBirthday;)Landroid/os/Bundle;
    .locals 10
    .param p1, "birthdayEvents"    # Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseBirthday;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 220
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 222
    .local v0, "BirthdayEventList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentValues;>;"
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/sns3/SnsApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 223
    .local v2, "cr":Landroid/content/ContentResolver;
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 225
    .local v1, "bundle":Landroid/os/Bundle;
    if-eqz p1, :cond_3

    .line 226
    move-object v3, p1

    .line 228
    .local v3, "curBirthdayEvents":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseBirthday;
    :goto_0
    if-eqz v3, :cond_1

    .line 230
    new-instance v6, Landroid/content/ContentValues;

    invoke-direct {v6}, Landroid/content/ContentValues;-><init>()V

    .line 232
    .local v6, "values":Landroid/content/ContentValues;
    invoke-virtual {v6}, Landroid/content/ContentValues;->clear()V

    .line 234
    iget-object v7, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseBirthday;->mBirthdayDate:Ljava/lang/String;

    const-string v8, "null"

    if-eq v7, v8, :cond_0

    .line 236
    const-string v7, "owner_id"

    iget-object v8, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseBirthday;->mUId:Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 237
    const-string v7, "owner_name"

    iget-object v8, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseBirthday;->mName:Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 238
    const-string v7, "start_time"

    iget-object v8, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseBirthday;->mBirthdayDate:Ljava/lang/String;

    invoke-static {v8}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->toTimestampForBirthday(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 241
    const-string v7, "end_time"

    iget-object v8, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseBirthday;->mBirthdayDate:Ljava/lang/String;

    invoke-static {v8}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->toTimestampForBirthday(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 244
    const-string v7, "name"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v9, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseBirthday;->mName:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " Birthday"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 246
    const-string v7, "event_pic"

    iget-object v8, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseBirthday;->mPicUrl:Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 247
    const-string v7, "description"

    iget-object v8, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseBirthday;->mGender:Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 248
    const-string v7, "_is_valid"

    const/4 v8, 0x0

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 250
    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 253
    :cond_0
    iget-object v3, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseBirthday;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseBirthday;

    .line 254
    goto :goto_0

    .line 256
    .end local v6    # "values":Landroid/content/ContentValues;
    :cond_1
    sget-object v8, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$SyncEvent;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v7

    new-array v7, v7, [Landroid/content/ContentValues;

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v7

    check-cast v7, [Landroid/content/ContentValues;

    invoke-virtual {v2, v8, v7}, Landroid/content/ContentResolver;->bulkInsert(Landroid/net/Uri;[Landroid/content/ContentValues;)I

    move-result v5

    .line 263
    .local v5, "rowsCnt":I
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-eq v5, v7, :cond_2

    .line 265
    :try_start_0
    new-instance v7, Ljava/lang/Exception;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "insertBirthdayEvents - Response values have problems! rowsCnt : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", ListSize : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v7
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 269
    :catch_0
    move-exception v4

    .line 270
    .local v4, "e":Ljava/lang/Exception;
    throw v4

    .line 274
    .end local v4    # "e":Ljava/lang/Exception;
    :cond_2
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 280
    .end local v3    # "curBirthdayEvents":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseBirthday;
    .end local v5    # "rowsCnt":I
    :goto_1
    return-object v1

    .line 277
    :cond_3
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public insetEvents(Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvents;)Landroid/os/Bundle;
    .locals 10
    .param p1, "events"    # Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvents;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 128
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 130
    .local v0, "EventList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentValues;>;"
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/sns3/SnsApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 131
    .local v2, "cr":Landroid/content/ContentResolver;
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 133
    .local v1, "bundle":Landroid/os/Bundle;
    if-eqz p1, :cond_4

    .line 134
    iget-object v3, p1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvents;->mEvents:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;

    .line 136
    .local v3, "curEvent":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;
    if-eqz v3, :cond_3

    .line 138
    :goto_0
    if-eqz v3, :cond_1

    .line 140
    new-instance v6, Landroid/content/ContentValues;

    invoke-direct {v6}, Landroid/content/ContentValues;-><init>()V

    .line 142
    .local v6, "values":Landroid/content/ContentValues;
    invoke-virtual {v6}, Landroid/content/ContentValues;->clear()V

    .line 144
    const-string v7, "event_id"

    iget-object v8, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;->mID:Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 145
    const-string v7, "owner_name"

    iget-object v8, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;->mOwner:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFrom;

    iget-object v8, v8, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFrom;->mFromName:Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    const-string v7, "owner_id"

    iget-object v8, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;->mOwner:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFrom;

    iget-object v8, v8, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFrom;->mFromID:Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 147
    const-string v7, "name"

    iget-object v8, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;->mEventName:Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    const-string v7, "description"

    iget-object v8, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;->mDescription:Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 149
    const-string v7, "start_time"

    iget-object v8, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;->mStartTime:Ljava/lang/String;

    const/4 v9, 0x0

    invoke-static {v8, v9}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->toTimestamp(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 151
    const-string v7, "end_time"

    iget-object v8, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;->mEndTime:Ljava/lang/String;

    iget-object v9, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;->mStartTime:Ljava/lang/String;

    invoke-static {v8, v9}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->toTimestamp(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 153
    const-string v7, "location"

    iget-object v8, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;->mLocation:Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    const-string v7, "privacy"

    iget-object v8, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;->mPrivacy:Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    const-string v7, "updated_time"

    iget-object v8, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;->mUpdatedTime:Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 156
    const-string v7, "rsvp_status"

    iget-object v8, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;->mRsvpStatus:Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 157
    const-string v7, "street"

    iget-object v8, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;->mStreet:Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 158
    const-string v7, "city"

    iget-object v8, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;->mCity:Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 159
    const-string v7, "state"

    iget-object v8, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;->mState:Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    const-string v7, "zip"

    iget-object v8, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;->mZip:Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 161
    const-string v7, "country"

    iget-object v8, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;->mCountry:Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 162
    const-string v7, "latitude"

    iget-object v8, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;->mLatitude:Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    const-string v7, "longitude"

    iget-object v8, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;->mLongitude:Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 164
    const-string v7, "vanue_id"

    iget-object v8, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;->mVenueID:Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 165
    const-string v7, "event_pic"

    iget-object v8, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;->mEventPicture:Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 166
    const-string v7, "_is_valid"

    const/4 v8, 0x0

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 168
    iget-object v7, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;

    if-nez v7, :cond_0

    .line 169
    iget-object v7, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;->mStartTime:Ljava/lang/String;

    iput-object v7, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->mlastStartTime:Ljava/lang/String;

    .line 171
    const-string v7, "paging_next"

    const-string v8, "where"

    invoke-virtual {v1, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    const-string v7, "paging_previous"

    const-string v8, "limit"

    invoke-virtual {v1, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 176
    invoke-virtual {v1}, Landroid/os/Bundle;->clear()V

    .line 178
    const-string v7, "limit"

    const-string v8, "limit 50"

    invoke-virtual {v1, v7, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 180
    const-string v7, "where"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "start_time > \'"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->mlastStartTime:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "\'"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1, v7, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 184
    :cond_0
    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 186
    iget-object v3, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;

    .line 187
    goto/16 :goto_0

    .line 189
    .end local v6    # "values":Landroid/content/ContentValues;
    :cond_1
    sget-object v8, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$SyncEvent;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v7

    new-array v7, v7, [Landroid/content/ContentValues;

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v7

    check-cast v7, [Landroid/content/ContentValues;

    invoke-virtual {v2, v8, v7}, Landroid/content/ContentResolver;->bulkInsert(Landroid/net/Uri;[Landroid/content/ContentValues;)I

    move-result v5

    .line 192
    .local v5, "rowsCnt":I
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-eq v5, v7, :cond_2

    .line 194
    :try_start_0
    new-instance v7, Ljava/lang/Exception;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "insertEvents - Response values have problems! rowsCnt : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", ListSize : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v7
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 198
    :catch_0
    move-exception v4

    .line 199
    .local v4, "e":Ljava/lang/Exception;
    throw v4

    .line 203
    .end local v4    # "e":Ljava/lang/Exception;
    :cond_2
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 214
    .end local v3    # "curEvent":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;
    .end local v5    # "rowsCnt":I
    :goto_1
    return-object v1

    .line 207
    .restart local v3    # "curEvent":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;
    :cond_3
    const/4 v1, 0x0

    goto :goto_1

    .line 211
    .end local v3    # "curEvent":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;
    :cond_4
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public isEventLimit(Landroid/os/Bundle;)Z
    .locals 14
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 840
    if-nez p1, :cond_0

    .line 841
    const/4 v1, 0x1

    .line 878
    :goto_0
    return v1

    .line 843
    :cond_0
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/sns3/SnsApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 845
    .local v0, "cr":Landroid/content/ContentResolver;
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    .line 846
    .local v11, "where":Ljava/lang/StringBuilder;
    const-string v1, "_is_valid"

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 847
    const-string v1, " = \'"

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 848
    const/4 v1, 0x0

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 849
    const-string v1, "\'"

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 851
    sget-object v1, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$SyncEvent;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 854
    .local v6, "c":Landroid/database/Cursor;
    if-eqz v6, :cond_1

    .line 855
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v8

    .line 856
    .local v8, "count":I
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 858
    const/16 v1, 0xbb8

    if-lt v8, v1, :cond_1

    .line 859
    const/4 v1, 0x1

    goto :goto_0

    .line 863
    .end local v8    # "count":I
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->mlastStartTime:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    .line 864
    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->mlastStartTime:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->fromUTCtoLATimestamp(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    .line 866
    .local v10, "lastStartTime":Ljava/lang/Long;
    new-instance v9, Ljava/util/Date;

    invoke-direct {v9}, Ljava/util/Date;-><init>()V

    .line 867
    .local v9, "currentTime":Ljava/util/Date;
    new-instance v7, Ljava/util/GregorianCalendar;

    invoke-direct {v7}, Ljava/util/GregorianCalendar;-><init>()V

    .line 869
    .local v7, "calendar":Ljava/util/Calendar;
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v1

    invoke-virtual {v7, v1}, Ljava/util/Calendar;->setTimeZone(Ljava/util/TimeZone;)V

    .line 870
    invoke-virtual {v7, v9}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 873
    invoke-virtual {v10}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v7}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    const-wide/32 v12, 0x57b12c00

    add-long/2addr v4, v12

    cmp-long v1, v2, v4

    if-lez v1, :cond_2

    .line 874
    const/4 v1, 0x1

    goto :goto_0

    .line 878
    .end local v7    # "calendar":Ljava/util/Calendar;
    .end local v9    # "currentTime":Ljava/util/Date;
    .end local v10    # "lastStartTime":Ljava/lang/Long;
    :cond_2
    const/4 v1, 0x0

    goto/16 :goto_0
.end method

.method public declared-synchronized resumeSync()V
    .locals 1

    .prologue
    .line 891
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->notify()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 892
    monitor-exit p0

    return-void

    .line 891
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized suspendSync()V
    .locals 2

    .prologue
    .line 884
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 888
    :goto_0
    monitor-exit p0

    return-void

    .line 885
    :catch_0
    move-exception v0

    .line 886
    .local v0, "e":Ljava/lang/InterruptedException;
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 884
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public updateCurrentSyncValue(Ljava/lang/String;)V
    .locals 6
    .param p1, "type"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    .line 687
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/sns3/SnsApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 688
    .local v0, "cr":Landroid/content/ContentResolver;
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 689
    .local v1, "values":Landroid/content/ContentValues;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 692
    .local v2, "where":Ljava/lang/StringBuilder;
    invoke-virtual {v1}, Landroid/content/ContentValues;->clear()V

    .line 693
    const-string v3, "_is_valid"

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 695
    const/4 v3, 0x0

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 696
    const-string v3, "_is_valid"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 697
    const-string v3, "=0"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 698
    const-string v3, "album"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 699
    sget-object v3, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$SyncAlbum;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v1, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 700
    sget-object v3, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$SyncPhoto;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v1, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 701
    sget-object v3, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$SyncPhotoImages;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v1, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 709
    :cond_0
    :goto_0
    return-void

    .line 703
    :cond_1
    const-string v3, "event"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 704
    sget-object v3, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$SyncEvent;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v1, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_0

    .line 705
    :cond_2
    const-string v3, "streams"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 706
    sget-object v3, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$PhotosOfUser;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v1, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_0
.end method

.method public updateFriends(Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFamily;)V
    .locals 6
    .param p1, "family"    # Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFamily;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 1243
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 1244
    .local v0, "contentValues":Landroid/content/ContentValues;
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/sns3/SnsApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 1245
    .local v1, "cr":Landroid/content/ContentResolver;
    const/4 v3, 0x0

    .line 1246
    .local v3, "where":Ljava/lang/String;
    :goto_0
    if-eqz p1, :cond_0

    .line 1247
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "friend_id = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFamily;->mID:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1248
    const-string v4, "friend_relationship"

    iget-object v5, p1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFamily;->mRelationship:Ljava/lang/String;

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1250
    :try_start_0
    sget-object v4, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$Friends;->CONTENT_URI:Landroid/net/Uri;

    const/4 v5, 0x0

    invoke-virtual {v1, v4, v0, v3, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1255
    iget-object p1, p1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFamily;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFamily;

    goto :goto_0

    .line 1251
    :catch_0
    move-exception v2

    .line 1252
    .local v2, "e":Ljava/lang/Exception;
    new-instance v4, Ljava/lang/Exception;

    const-string v5, "Could not update Friends table with family info"

    invoke-direct {v4, v5}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v4

    .line 1258
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_0
    return-void
.end method

.method public updatePrevSyncState(Ljava/lang/String;I)V
    .locals 6
    .param p1, "type"    # Ljava/lang/String;
    .param p2, "state"    # I

    .prologue
    .line 712
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/sns3/SnsApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 713
    .local v0, "cr":Landroid/content/ContentResolver;
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 714
    .local v1, "values":Landroid/content/ContentValues;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 716
    .local v2, "where":Ljava/lang/StringBuilder;
    invoke-virtual {v1}, Landroid/content/ContentValues;->clear()V

    .line 717
    const-string v3, "previous_state"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 719
    const/4 v3, 0x0

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 720
    const-string v3, "sync_type"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 721
    const-string v3, "album"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 722
    const-string v3, "=\'album\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 728
    :cond_0
    :goto_0
    sget-object v3, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$PreviousSyncState;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v0, v3, v1, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 729
    return-void

    .line 723
    :cond_1
    const-string v3, "photo"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 724
    const-string v3, "=\'photo\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 725
    :cond_2
    const-string v3, "event"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 726
    const-string v3, "=\'event\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method
