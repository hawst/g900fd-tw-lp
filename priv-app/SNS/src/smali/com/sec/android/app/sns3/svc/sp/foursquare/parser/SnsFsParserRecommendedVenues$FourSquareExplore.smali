.class public interface abstract Lcom/sec/android/app/sns3/svc/sp/foursquare/parser/SnsFsParserRecommendedVenues$FourSquareExplore;
.super Ljava/lang/Object;
.source "SnsFsParserRecommendedVenues.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/svc/sp/foursquare/parser/SnsFsParserRecommendedVenues;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "FourSquareExplore"
.end annotation


# static fields
.field public static final ADDRESS:Ljava/lang/String; = "address"

.field public static final CANONICAL_URL:Ljava/lang/String; = "canonicalUrl"

.field public static final CATEGORIES:Ljava/lang/String; = "categories"

.field public static final CATEGORY:Ljava/lang/String; = "categories"

.field public static final CITY:Ljava/lang/String; = "city"

.field public static final CODE:Ljava/lang/String; = "code"

.field public static final CONTACT:Ljava/lang/String; = "contact"

.field public static final COUNTRY:Ljava/lang/String; = "country"

.field public static final COUNTRY_CODE:Ljava/lang/String; = "cc"

.field public static final CROSS_STREET:Ljava/lang/String; = "crossStreet"

.field public static final DEFAULT_SIZES:[Ljava/lang/Integer;

.field public static final DISTANCE:Ljava/lang/String; = "distance"

.field public static final FORMATTED_PHONE:Ljava/lang/String; = "formattedPhone"

.field public static final GROUPS:Ljava/lang/String; = "groups"

.field public static final HEADER_LOCATION:Ljava/lang/String; = "headerFullLocation"

.field public static final HEADER_LOCATION_GRANULARITY:Ljava/lang/String; = "headerLocationGranularity"

.field public static final HEADER_MESSAGE:Ljava/lang/String; = "headerMessage"

.field public static final ICON:Ljava/lang/String; = "icon"

.field public static final ICON_PREFIX:Ljava/lang/String; = "prefix"

.field public static final ICON_SUFFIX:Ljava/lang/String; = "suffix"

.field public static final ID:Ljava/lang/String; = "id"

.field public static final ITEMS:Ljava/lang/String; = "items"

.field public static final LATITUDE:Ljava/lang/String; = "lat"

.field public static final LOCATION:Ljava/lang/String; = "location"

.field public static final LONGITUDE:Ljava/lang/String; = "lng"

.field public static final META:Ljava/lang/String; = "meta"

.field public static final NAME:Ljava/lang/String; = "name"

.field public static final PHONE:Ljava/lang/String; = "phone"

.field public static final POSTAL_CODE:Ljava/lang/String; = "postalCode"

.field public static final QUERY:Ljava/lang/String; = "query"

.field public static final RATING:Ljava/lang/String; = "rating"

.field public static final RESPONSE:Ljava/lang/String; = "response"

.field public static final SIZE:Ljava/lang/String; = "sizes"

.field public static final STATE:Ljava/lang/String; = "state"

.field public static final SUGGESTED_RADIUS:Ljava/lang/String; = "suggestedRadius"

.field public static final TEXT:Ljava/lang/String; = "text"

.field public static final TIPS:Ljava/lang/String; = "tips"

.field public static final TOTAL_RESULT:Ljava/lang/String; = "totalResults"

.field public static final TYPE:Ljava/lang/String; = "type"

.field public static final URL:Ljava/lang/String; = "url"

.field public static final VENUE:Ljava/lang/String; = "venue"

.field public static final WARNING:Ljava/lang/String; = "warning"


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 113
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Integer;

    const/4 v1, 0x0

    const/16 v2, 0x20

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const/16 v2, 0x2c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const/16 v2, 0x40

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const/16 v2, 0x58

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const/16 v2, 0x100

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/foursquare/parser/SnsFsParserRecommendedVenues$FourSquareExplore;->DEFAULT_SIZES:[Ljava/lang/Integer;

    return-void
.end method
