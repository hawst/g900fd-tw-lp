.class public final enum Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI$Param;
.super Ljava/lang/Enum;
.source "SnsTwGeoAPI.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Param"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI$Param;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI$Param;

.field public static final enum ACCURACY:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI$Param;

.field public static final enum ATTRIBUTE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI$Param;

.field public static final enum CALLBACK:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI$Param;

.field public static final enum CONTAINED_WITHIN:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI$Param;

.field public static final enum GRANULARITY:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI$Param;

.field public static final enum IP:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI$Param;

.field public static final enum LAT:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI$Param;

.field public static final enum LONG:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI$Param;

.field public static final enum MAX_RESULTS:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI$Param;

.field public static final enum NAME:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI$Param;

.field public static final enum PLACE_ID:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI$Param;

.field public static final enum QUERY:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI$Param;

.field public static final enum TOKEN:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI$Param;


# instance fields
.field private mParam:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 35
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI$Param;

    const-string v1, "PLACE_ID"

    const-string v2, "place_id"

    invoke-direct {v0, v1, v4, v2}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI$Param;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI$Param;->PLACE_ID:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI$Param;

    .line 36
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI$Param;

    const-string v1, "LAT"

    const-string v2, "lat"

    invoke-direct {v0, v1, v5, v2}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI$Param;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI$Param;->LAT:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI$Param;

    .line 37
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI$Param;

    const-string v1, "LONG"

    const-string v2, "long"

    invoke-direct {v0, v1, v6, v2}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI$Param;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI$Param;->LONG:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI$Param;

    .line 38
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI$Param;

    const-string v1, "ACCURACY"

    const-string v2, "accuracy"

    invoke-direct {v0, v1, v7, v2}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI$Param;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI$Param;->ACCURACY:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI$Param;

    .line 39
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI$Param;

    const-string v1, "GRANULARITY"

    const-string v2, "granularity"

    invoke-direct {v0, v1, v8, v2}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI$Param;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI$Param;->GRANULARITY:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI$Param;

    .line 40
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI$Param;

    const-string v1, "MAX_RESULTS"

    const/4 v2, 0x5

    const-string v3, "max_results"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI$Param;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI$Param;->MAX_RESULTS:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI$Param;

    .line 41
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI$Param;

    const-string v1, "CALLBACK"

    const/4 v2, 0x6

    const-string v3, "callback"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI$Param;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI$Param;->CALLBACK:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI$Param;

    .line 42
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI$Param;

    const-string v1, "QUERY"

    const/4 v2, 0x7

    const-string v3, "query"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI$Param;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI$Param;->QUERY:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI$Param;

    .line 43
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI$Param;

    const-string v1, "IP"

    const/16 v2, 0x8

    const-string v3, "ip"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI$Param;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI$Param;->IP:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI$Param;

    .line 44
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI$Param;

    const-string v1, "CONTAINED_WITHIN"

    const/16 v2, 0x9

    const-string v3, "contained_within"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI$Param;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI$Param;->CONTAINED_WITHIN:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI$Param;

    .line 45
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI$Param;

    const-string v1, "ATTRIBUTE"

    const/16 v2, 0xa

    const-string v3, "attribute:street_address"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI$Param;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI$Param;->ATTRIBUTE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI$Param;

    .line 46
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI$Param;

    const-string v1, "TOKEN"

    const/16 v2, 0xb

    const-string v3, "token"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI$Param;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI$Param;->TOKEN:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI$Param;

    .line 47
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI$Param;

    const-string v1, "NAME"

    const/16 v2, 0xc

    const-string v3, "name"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI$Param;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI$Param;->NAME:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI$Param;

    .line 34
    const/16 v0, 0xd

    new-array v0, v0, [Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI$Param;

    sget-object v1, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI$Param;->PLACE_ID:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI$Param;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI$Param;->LAT:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI$Param;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI$Param;->LONG:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI$Param;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI$Param;->ACCURACY:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI$Param;

    aput-object v1, v0, v7

    sget-object v1, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI$Param;->GRANULARITY:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI$Param;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI$Param;->MAX_RESULTS:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI$Param;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI$Param;->CALLBACK:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI$Param;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI$Param;->QUERY:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI$Param;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI$Param;->IP:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI$Param;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI$Param;->CONTAINED_WITHIN:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI$Param;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI$Param;->ATTRIBUTE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI$Param;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI$Param;->TOKEN:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI$Param;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI$Param;->NAME:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI$Param;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI$Param;->$VALUES:[Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI$Param;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "param"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 51
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 52
    iput-object p3, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI$Param;->mParam:Ljava/lang/String;

    .line 53
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI$Param;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 34
    const-class v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI$Param;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI$Param;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI$Param;
    .locals 1

    .prologue
    .line 34
    sget-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI$Param;->$VALUES:[Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI$Param;

    invoke-virtual {v0}, [Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI$Param;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI$Param;

    return-object v0
.end method


# virtual methods
.method public getParam()Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwGeoAPI$Param;->mParam:Ljava/lang/String;

    return-object v0
.end method
