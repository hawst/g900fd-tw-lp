.class public final Lcom/sec/android/app/sns3/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final Single_sign_on:I = 0x7f080070

.field public static final account_already_added:I = 0x7f080000

.field public static final account_label_facebook:I = 0x7f080001

.field public static final account_label_foursquare:I = 0x7f080002

.field public static final account_label_googleplus:I = 0x7f080003

.field public static final account_label_instagram:I = 0x7f080004

.field public static final account_label_linkedin:I = 0x7f080005

.field public static final account_label_qzone:I = 0x7f080006

.field public static final account_label_sinaweibo:I = 0x7f080007

.field public static final account_label_twitter:I = 0x7f080008

.field public static final adapter_sync_interval:I = 0x7f080009

.field public static final allow_access:I = 0x7f08000a

.field public static final and:I = 0x7f08000b

.field public static final app_name:I = 0x7f08000c

.field public static final attention:I = 0x7f08000d

.field public static final calendar_sync_interval:I = 0x7f080071

.field public static final com_facebook_dialogloginactivity_ok_button:I = 0x7f08000e

.field public static final com_facebook_internet_permission_error_message:I = 0x7f08000f

.field public static final com_facebook_internet_permission_error_title:I = 0x7f080010

.field public static final com_facebook_loading:I = 0x7f080011

.field public static final com_facebook_loginview_cancel_action:I = 0x7f080012

.field public static final com_facebook_loginview_log_in_button:I = 0x7f080013

.field public static final com_facebook_loginview_log_out_action:I = 0x7f080014

.field public static final com_facebook_loginview_log_out_button:I = 0x7f080015

.field public static final com_facebook_loginview_logged_in_as:I = 0x7f080016

.field public static final com_facebook_loginview_logged_in_using_facebook:I = 0x7f080017

.field public static final com_facebook_logo_content_description:I = 0x7f080018

.field public static final com_facebook_requesterror_password_changed:I = 0x7f080019

.field public static final com_facebook_requesterror_permissions:I = 0x7f08001a

.field public static final com_facebook_requesterror_reconnect:I = 0x7f08001b

.field public static final com_facebook_requesterror_relogin:I = 0x7f08001c

.field public static final com_facebook_requesterror_web_login:I = 0x7f08001d

.field public static final comment:I = 0x7f08001e

.field public static final connection_timeout:I = 0x7f08001f

.field public static final contacts_sync_interval:I = 0x7f080072

.field public static final facebook:I = 0x7f080020

.field public static final failed_to_single_sign_on:I = 0x7f080073

.field public static final foursquare:I = 0x7f080021

.field public static final gallery_sync_interval:I = 0x7f080074

.field public static final general_sync_setting:I = 0x7f080022

.field public static final googleplus:I = 0x7f080023

.field public static final home:I = 0x7f080024

.field public static final instagram:I = 0x7f080025

.field public static final interval_value_12hours:I = 0x7f080026

.field public static final interval_value_3hours:I = 0x7f080027

.field public static final interval_value_6hours:I = 0x7f080028

.field public static final interval_value_none:I = 0x7f080029

.field public static final interval_value_onceaday:I = 0x7f08002a

.field public static final job_description:I = 0x7f08002b

.field public static final job_post_other:I = 0x7f08002c

.field public static final liked:I = 0x7f08002d

.field public static final linkedin:I = 0x7f08002e

.field public static final loading:I = 0x7f08002f

.field public static final log_in_to_sync:I = 0x7f080030

.field public static final login_failed:I = 0x7f080031

.field public static final login_failed_message:I = 0x7f080032

.field public static final network_unavailable:I = 0x7f080033

.field public static final okay_action:I = 0x7f080034

.field public static final playstore_not_installed:I = 0x7f080035

.field public static final post:I = 0x7f080036

.field public static final post_other:I = 0x7f080037

.field public static final prof_post_other:I = 0x7f080038

.field public static final profile_picture:I = 0x7f080039

.field public static final profiles:I = 0x7f08003a

.field public static final qzone:I = 0x7f08003b

.field public static final register_google_account:I = 0x7f08003c

.field public static final restart_single_sign_on:I = 0x7f08003d

.field public static final retry_login_noti_body:I = 0x7f08003e

.field public static final retry_login_noti_title:I = 0x7f08003f

.field public static final retry_login_noti_title_facebook:I = 0x7f080075

.field public static final s_note_greeting_title:I = 0x7f080040

.field public static final s_note_greeting_upload_failed:I = 0x7f080041

.field public static final s_note_greeting_uploaded:I = 0x7f080042

.field public static final s_note_greeting_uploading_indicator:I = 0x7f080043

.field public static final s_note_greeting_uploading_progress:I = 0x7f080044

.field public static final samsung_galaxy:I = 0x7f080045

.field public static final set_up_account:I = 0x7f080046

.field public static final sinaweibo:I = 0x7f080047

.field public static final single_sign_on:I = 0x7f080048

.field public static final single_sign_on_error_occured:I = 0x7f080049

.field public static final status_other:I = 0x7f08004a

.field public static final stms_appgroup:I = 0x7f08004b

.field public static final stms_version:I = 0x7f08004c

.field public static final streams:I = 0x7f08004d

.field public static final sync_button_cancel:I = 0x7f08004e

.field public static final sync_button_done:I = 0x7f08004f

.field public static final sync_calendar:I = 0x7f080076

.field public static final sync_calendar_summary:I = 0x7f080050

.field public static final sync_disabled:I = 0x7f080051

.field public static final sync_facebook_app_desc:I = 0x7f080052

.field public static final sync_gallay_summary:I = 0x7f080053

.field public static final sync_gallery:I = 0x7f080077

.field public static final sync_gallery_summary_instagram:I = 0x7f080054

.field public static final sync_home_summary:I = 0x7f080055

.field public static final sync_interval_setting:I = 0x7f080056

.field public static final sync_profiles_summary:I = 0x7f080057

.field public static final sync_provider:I = 0x7f080058

.field public static final sync_setting:I = 0x7f080059

.field public static final sync_streams_summary:I = 0x7f08005a

.field public static final sync_timeline_summary:I = 0x7f08005b

.field public static final sync_with_your_device:I = 0x7f08005c

.field public static final timeline:I = 0x7f08005d

.field public static final title_cmpy_new_position:I = 0x7f08005e

.field public static final title_cmpy_person:I = 0x7f08005f

.field public static final title_cmpy_product:I = 0x7f080060

.field public static final title_conn:I = 0x7f080061

.field public static final title_conn_multiple:I = 0x7f080062

.field public static final title_cprof:I = 0x7f080063

.field public static final title_jgrp:I = 0x7f080064

.field public static final title_jobp:I = 0x7f080065

.field public static final title_ncon:I = 0x7f080066

.field public static final title_prec_recommendee:I = 0x7f080067

.field public static final title_prec_recommender:I = 0x7f080068

.field public static final title_prof:I = 0x7f080069

.field public static final title_shar:I = 0x7f08006a

.field public static final title_shar_other:I = 0x7f08006b

.field public static final title_stat:I = 0x7f08006c

.field public static final twitter:I = 0x7f08006d

.field public static final voice_call:I = 0x7f08006e

.field public static final voice_call_additional_charges:I = 0x7f08006f


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 242
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
