.class public Lcom/sec/android/app/sns3/svc/sp/twitter/parser/SnsTwParserSavedSearch;
.super Ljava/lang/Object;
.source "SnsTwParserSavedSearch.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseSavedSearch;
    .locals 9
    .param p0, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 34
    const/4 v6, 0x0

    .line 37
    .local v6, "response":Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseSavedSearch;
    invoke-static {p0}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->isJsonEmpty(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 38
    const/4 v7, 0x0

    .line 68
    :goto_0
    return-object v7

    .line 42
    :cond_0
    :try_start_0
    invoke-static {p0}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->isJsonArray(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 43
    new-instance v3, Lorg/json/JSONArray;

    invoke-direct {v3, p0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 45
    .local v3, "jsonArray":Lorg/json/JSONArray;
    const/4 v0, 0x0

    .line 46
    .local v0, "curNode":Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseSavedSearch;
    const/4 v5, 0x0

    .line 48
    .local v5, "newNode":Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseSavedSearch;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v7

    if-ge v2, v7, :cond_3

    .line 49
    invoke-virtual {v3, v2}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/app/sns3/svc/sp/twitter/parser/SnsTwParserSavedSearch;->parseSavedSearch(Lorg/json/JSONObject;)Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseSavedSearch;

    move-result-object v5

    .line 51
    if-nez v6, :cond_1

    .line 52
    move-object v6, v5

    .line 53
    move-object v0, v6

    .line 48
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 55
    :cond_1
    iput-object v5, v0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseSavedSearch;->mNext:Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseSavedSearch;

    .line 56
    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseSavedSearch;->mNext:Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseSavedSearch;

    goto :goto_2

    .line 60
    .end local v0    # "curNode":Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseSavedSearch;
    .end local v2    # "i":I
    .end local v3    # "jsonArray":Lorg/json/JSONArray;
    .end local v5    # "newNode":Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseSavedSearch;
    :cond_2
    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 61
    .local v4, "jsonObj":Lorg/json/JSONObject;
    invoke-static {v4}, Lcom/sec/android/app/sns3/svc/sp/twitter/parser/SnsTwParserSavedSearch;->parseSavedSearch(Lorg/json/JSONObject;)Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseSavedSearch;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .end local v4    # "jsonObj":Lorg/json/JSONObject;
    :cond_3
    :goto_3
    move-object v7, v6

    .line 68
    goto :goto_0

    .line 63
    :catch_0
    move-exception v1

    .line 64
    .local v1, "e":Lorg/json/JSONException;
    const-string v7, "SNS"

    invoke-virtual {v1}, Lorg/json/JSONException;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 65
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_3
.end method

.method private static parseSavedSearch(Lorg/json/JSONObject;)Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseSavedSearch;
    .locals 2
    .param p0, "jsonObj"    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 73
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseSavedSearch;

    invoke-direct {v0}, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseSavedSearch;-><init>()V

    .line 75
    .local v0, "response":Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseSavedSearch;
    const-string v1, "name"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseSavedSearch;->mName:Ljava/lang/String;

    .line 76
    const-string v1, "position"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseSavedSearch;->mPosition:Ljava/lang/String;

    .line 77
    const-string v1, "created_at"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseSavedSearch;->mCreatedAt:Ljava/lang/String;

    .line 78
    const-string v1, "id"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseSavedSearch;->mId:Ljava/lang/String;

    .line 79
    const-string v1, "query"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseSavedSearch;->mQuery:Ljava/lang/String;

    .line 81
    return-object v0
.end method
