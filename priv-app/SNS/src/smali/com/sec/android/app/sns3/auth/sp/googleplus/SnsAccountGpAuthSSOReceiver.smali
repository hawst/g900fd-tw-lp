.class public Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOReceiver;
.super Landroid/content/BroadcastReceiver;
.source "SnsAccountGpAuthSSOReceiver.java"


# static fields
.field private static final ACCOUNT_PREFERENCE_NAME:Ljava/lang/String; = "GP_account"

.field private static final APP_ACCOUNT_PREFERENCE_KEY:Ljava/lang/String; = "app_account"

.field public static final SNS_ACCOUNT_CHECK:Ljava/lang/String; = "com.sec.android.app.sns3.auth.ACTION_CHECK_ACCOUNT"

.field private static final SNS_ACCOUNT_PREFERENCE_KEY:Ljava/lang/String; = "sns_account"


# instance fields
.field private final TAG:Ljava/lang/String;

.field private mSharedPref:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 38
    const-string v0, "GpAuthSSOReceiver"

    iput-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOReceiver;->TAG:Ljava/lang/String;

    return-void
.end method

.method private storeAccountStatus(II)V
    .locals 2
    .param p1, "snsAccount"    # I
    .param p2, "appAccount"    # I

    .prologue
    .line 125
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOReceiver;->mSharedPref:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 126
    .local v0, "prefEditor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "sns_account"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 127
    const-string v1, "app_account"

    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 128
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 130
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 16
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 50
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    .line 52
    .local v3, "action":Ljava/lang/String;
    const-string v12, "GP_account"

    const/4 v13, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v13}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v12

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOReceiver;->mSharedPref:Landroid/content/SharedPreferences;

    .line 54
    const-string v12, "GpAuthSSOReceiver"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "GooglePlus SSO Receiver : "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 56
    invoke-static/range {p1 .. p1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v12

    const-string v13, "com.sec.android.app.sns3.googleplus"

    invoke-virtual {v12, v13}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v11

    .line 59
    .local v11, "sns3Accounts":[Landroid/accounts/Account;
    invoke-static/range {p1 .. p1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v12

    const-string v13, "com.google"

    invoke-virtual {v12, v13}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v4

    .line 62
    .local v4, "appAccounts":[Landroid/accounts/Account;
    const-string v12, "notification"

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/app/NotificationManager;

    .line 64
    .local v9, "notiMgr":Landroid/app/NotificationManager;
    const-string v12, "com.sec.android.app.sns3.auth.ACTION_CHECK_ACCOUNT"

    invoke-virtual {v12, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_1

    .line 65
    array-length v12, v11

    array-length v13, v4

    move-object/from16 v0, p0

    invoke-direct {v0, v12, v13}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOReceiver;->storeAccountStatus(II)V

    .line 122
    :cond_0
    :goto_0
    return-void

    .line 66
    :cond_1
    const-string v12, "android.accounts.LOGIN_ACCOUNTS_CHANGED"

    invoke-virtual {v12, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_0

    .line 67
    const-string v12, "operation"

    move-object/from16 v0, p2

    invoke-virtual {v0, v12}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 68
    .local v10, "operation":Ljava/lang/String;
    const/4 v5, 0x0

    .line 70
    .local v5, "changedAccountType":Ljava/lang/String;
    if-eqz v10, :cond_7

    .line 71
    const-string v12, "account"

    move-object/from16 v0, p2

    invoke-virtual {v0, v12}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/accounts/Account;

    .line 72
    .local v1, "account":Landroid/accounts/Account;
    const-string v12, "accounts"

    move-object/from16 v0, p2

    invoke-virtual {v0, v12}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    .line 73
    .local v2, "accounts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/accounts/Account;>;"
    if-nez v1, :cond_2

    if-eqz v2, :cond_2

    .line 74
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_1
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v12

    if-ge v6, v12, :cond_2

    .line 75
    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "account":Landroid/accounts/Account;
    check-cast v1, Landroid/accounts/Account;

    .line 76
    .restart local v1    # "account":Landroid/accounts/Account;
    if-eqz v1, :cond_6

    const-string v12, "com.sec.android.app.sns3.googleplus"

    iget-object v13, v1, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_6

    .line 81
    .end local v6    # "i":I
    :cond_2
    if-eqz v1, :cond_3

    .line 82
    iget-object v5, v1, Landroid/accounts/Account;->type:Ljava/lang/String;

    .line 103
    .end local v1    # "account":Landroid/accounts/Account;
    .end local v2    # "accounts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/accounts/Account;>;"
    :cond_3
    :goto_2
    if-eqz v5, :cond_4

    .line 104
    const-string v12, "GpAuthSSOReceiver"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v13, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 108
    :cond_4
    const-string v12, "add"

    invoke-virtual {v12, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_b

    const-string v12, "com.google"

    invoke-virtual {v12, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_b

    .line 120
    :cond_5
    :goto_3
    array-length v12, v11

    array-length v13, v4

    move-object/from16 v0, p0

    invoke-direct {v0, v12, v13}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOReceiver;->storeAccountStatus(II)V

    goto :goto_0

    .line 74
    .restart local v1    # "account":Landroid/accounts/Account;
    .restart local v2    # "accounts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/accounts/Account;>;"
    .restart local v6    # "i":I
    :cond_6
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 85
    .end local v1    # "account":Landroid/accounts/Account;
    .end local v2    # "accounts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/accounts/Account;>;"
    .end local v6    # "i":I
    :cond_7
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOReceiver;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v13, "app_account"

    const/4 v14, 0x0

    invoke-interface {v12, v13, v14}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v7

    .line 86
    .local v7, "lastAppAccountStatus":I
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOReceiver;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v13, "sns_account"

    const/4 v14, 0x0

    invoke-interface {v12, v13, v14}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v8

    .line 88
    .local v8, "lastSnsAccountStatus":I
    array-length v12, v4

    if-le v12, v7, :cond_8

    .line 89
    const-string v5, "com.google"

    .line 90
    const-string v10, "add"

    goto :goto_2

    .line 91
    :cond_8
    array-length v12, v4

    if-ge v12, v7, :cond_9

    .line 92
    const-string v5, "com.google"

    .line 93
    const-string v10, "remove"

    goto :goto_2

    .line 94
    :cond_9
    array-length v12, v11

    if-le v12, v8, :cond_a

    .line 95
    const-string v5, "com.sec.android.app.sns3.googleplus"

    .line 96
    const-string v10, "add"

    goto :goto_2

    .line 97
    :cond_a
    array-length v12, v11

    if-ge v12, v8, :cond_3

    .line 98
    const-string v5, "com.sec.android.app.sns3.googleplus"

    .line 99
    const-string v10, "remove"

    goto :goto_2

    .line 111
    .end local v7    # "lastAppAccountStatus":I
    .end local v8    # "lastSnsAccountStatus":I
    :cond_b
    const-string v12, "remove"

    invoke-virtual {v12, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_5

    const-string v12, "com.google"

    invoke-virtual {v12, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_5

    .line 113
    const/16 v12, 0x13ec

    invoke-virtual {v9, v12}, Landroid/app/NotificationManager;->cancel(I)V

    .line 114
    array-length v12, v11

    if-lez v12, :cond_5

    .line 115
    invoke-static/range {p1 .. p1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v12

    const/4 v13, 0x0

    aget-object v13, v11, v13

    const/4 v14, 0x0

    const/4 v15, 0x0

    invoke-virtual {v12, v13, v14, v15}, Landroid/accounts/AccountManager;->removeAccount(Landroid/accounts/Account;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    goto :goto_3
.end method
