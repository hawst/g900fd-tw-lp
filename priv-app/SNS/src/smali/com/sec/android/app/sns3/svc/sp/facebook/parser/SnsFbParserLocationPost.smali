.class public Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserLocationPost;
.super Ljava/lang/Object;
.source "SnsFbParserLocationPost.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserLocationPost$FacebookLocationPost;
    }
.end annotation


# instance fields
.field mLocationPost:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPost;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserLocationPost;->mLocationPost:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPost;

    .line 32
    return-void
.end method

.method private parseQuery1(Ljava/lang/String;)V
    .locals 13
    .param p1, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 163
    const/4 v0, 0x0

    .line 165
    .local v0, "curLocationPost":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPost;
    const/4 v7, 0x0

    .line 168
    .local v7, "jsonObject":Lorg/json/JSONObject;
    :try_start_0
    new-instance v5, Lorg/json/JSONArray;

    invoke-direct {v5, p1}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 170
    .local v5, "ja":Lorg/json/JSONArray;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-virtual {v5}, Lorg/json/JSONArray;->length()I

    move-result v12

    if-ge v3, v12, :cond_5

    .line 171
    new-instance v9, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPost;

    invoke-direct {v9}, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPost;-><init>()V

    .line 173
    .local v9, "newLocationPost":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPost;
    invoke-virtual {v5, v3}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v7

    .line 175
    const-string v12, "author_uid"

    invoke-virtual {v7, v12}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    iput-object v12, v9, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPost;->mAuthorUID:Ljava/lang/String;

    .line 178
    const-string v12, "id"

    invoke-virtual {v7, v12}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    iput-object v12, v9, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPost;->mPostID:Ljava/lang/String;

    .line 181
    const-string v12, "page_id"

    invoke-virtual {v7, v12}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    iput-object v12, v9, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPost;->mPageID:Ljava/lang/String;

    .line 184
    const-string v12, "timestamp"

    invoke-virtual {v7, v12}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    iput-object v12, v9, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPost;->mTimestamp:Ljava/lang/String;

    .line 187
    const-string v12, "type"

    invoke-virtual {v7, v12}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    iput-object v12, v9, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPost;->mType:Ljava/lang/String;

    .line 190
    const-string v12, "tagged_uids"

    invoke-virtual {v7, v12}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 193
    .local v11, "taggedUids":Ljava/lang/String;
    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v12

    if-lez v12, :cond_2

    const-string v12, "[]"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v12

    if-nez v12, :cond_2

    .line 195
    new-instance v8, Lorg/json/JSONObject;

    invoke-direct {v8, v11}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 197
    .local v8, "jsonTaggedUidObject":Lorg/json/JSONObject;
    invoke-virtual {v8}, Lorg/json/JSONObject;->names()Lorg/json/JSONArray;

    move-result-object v6

    .line 199
    .local v6, "jaTaggedUidName":Lorg/json/JSONArray;
    const/4 v1, 0x0

    .line 201
    .local v1, "curTaggedUser":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTaggedUser;
    const/4 v4, 0x0

    .local v4, "j":I
    :goto_1
    invoke-virtual {v6}, Lorg/json/JSONArray;->length()I

    move-result v12

    if-ge v4, v12, :cond_2

    .line 203
    new-instance v10, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTaggedUser;

    invoke-direct {v10}, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTaggedUser;-><init>()V

    .line 204
    .local v10, "newTaggedUser":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTaggedUser;
    invoke-virtual {v6, v4}, Lorg/json/JSONArray;->optString(I)Ljava/lang/String;

    move-result-object v12

    iput-object v12, v10, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTaggedUser;->mUID:Ljava/lang/String;

    .line 206
    if-nez v1, :cond_1

    .line 207
    move-object v1, v10

    .line 213
    :goto_2
    iget-object v12, v9, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPost;->mTaggedUser:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTaggedUser;

    if-nez v12, :cond_0

    .line 214
    iput-object v1, v9, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPost;->mTaggedUser:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTaggedUser;

    .line 201
    :cond_0
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 209
    :cond_1
    iput-object v10, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTaggedUser;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTaggedUser;

    .line 210
    iget-object v1, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTaggedUser;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTaggedUser;

    goto :goto_2

    .line 219
    .end local v1    # "curTaggedUser":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTaggedUser;
    .end local v4    # "j":I
    .end local v6    # "jaTaggedUidName":Lorg/json/JSONArray;
    .end local v8    # "jsonTaggedUidObject":Lorg/json/JSONObject;
    .end local v10    # "newTaggedUser":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTaggedUser;
    :cond_2
    if-nez v0, :cond_4

    .line 220
    move-object v0, v9

    .line 226
    :goto_3
    iget-object v12, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserLocationPost;->mLocationPost:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPost;

    if-nez v12, :cond_3

    .line 227
    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserLocationPost;->mLocationPost:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPost;

    .line 170
    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    .line 222
    :cond_4
    iput-object v9, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPost;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPost;

    .line 223
    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPost;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPost;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3

    .line 230
    .end local v3    # "i":I
    .end local v5    # "ja":Lorg/json/JSONArray;
    .end local v9    # "newLocationPost":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPost;
    .end local v11    # "taggedUids":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 231
    .local v2, "e":Lorg/json/JSONException;
    invoke-virtual {v2}, Lorg/json/JSONException;->printStackTrace()V

    .line 233
    .end local v2    # "e":Lorg/json/JSONException;
    :cond_5
    return-void
.end method

.method private parseQuery2(Ljava/lang/String;)V
    .locals 7
    .param p1, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 238
    :try_start_0
    new-instance v3, Lorg/json/JSONArray;

    invoke-direct {v3, p1}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 240
    .local v3, "ja":Lorg/json/JSONArray;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v5

    if-ge v2, v5, :cond_2

    .line 242
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserLocationPost;->mLocationPost:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPost;

    .line 244
    .local v0, "curLocationPost":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPost;
    invoke-virtual {v3, v2}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    .line 246
    .local v4, "jsonObj":Lorg/json/JSONObject;
    :goto_1
    if-eqz v0, :cond_1

    .line 247
    const-string v5, "page_id"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iget-object v6, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPost;->mPageID:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 250
    const-string v5, "name"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPost;->mPlaceName:Ljava/lang/String;

    .line 252
    const-string v5, "latitude"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPost;->mLatitude:Ljava/lang/String;

    .line 254
    const-string v5, "longitude"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPost;->mLongitude:Ljava/lang/String;

    .line 259
    :cond_0
    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPost;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPost;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 240
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 262
    .end local v0    # "curLocationPost":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPost;
    .end local v2    # "i":I
    .end local v3    # "ja":Lorg/json/JSONArray;
    .end local v4    # "jsonObj":Lorg/json/JSONObject;
    :catch_0
    move-exception v1

    .line 263
    .local v1, "e":Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    .line 265
    .end local v1    # "e":Lorg/json/JSONException;
    :cond_2
    return-void
.end method

.method private parseQuery3(Ljava/lang/String;)V
    .locals 8
    .param p1, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 270
    :try_start_0
    new-instance v4, Lorg/json/JSONArray;

    invoke-direct {v4, p1}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 272
    .local v4, "ja":Lorg/json/JSONArray;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v6

    if-ge v3, v6, :cond_3

    .line 274
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserLocationPost;->mLocationPost:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPost;

    .line 276
    .local v0, "curLocationPost":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPost;
    invoke-virtual {v4, v3}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v5

    .line 278
    .local v5, "jsonObj":Lorg/json/JSONObject;
    :goto_1
    if-eqz v0, :cond_2

    .line 280
    iget-object v1, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPost;->mTaggedUser:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTaggedUser;

    .line 282
    .local v1, "curTaggedUser":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTaggedUser;
    :goto_2
    if-eqz v1, :cond_1

    .line 284
    const-string v6, "uid"

    invoke-virtual {v5, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iget-object v7, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTaggedUser;->mUID:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 287
    const-string v6, "name"

    invoke-virtual {v5, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTaggedUser;->mUserName:Ljava/lang/String;

    .line 289
    const-string v6, "pic_small"

    invoke-virtual {v5, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTaggedUser;->mUserPicUrl:Ljava/lang/String;

    .line 293
    :cond_0
    iget-object v1, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTaggedUser;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTaggedUser;

    goto :goto_2

    .line 296
    :cond_1
    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPost;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPost;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 297
    goto :goto_1

    .line 272
    .end local v1    # "curTaggedUser":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTaggedUser;
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 300
    .end local v0    # "curLocationPost":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPost;
    .end local v3    # "i":I
    .end local v4    # "ja":Lorg/json/JSONArray;
    .end local v5    # "jsonObj":Lorg/json/JSONObject;
    :catch_0
    move-exception v2

    .line 301
    .local v2, "e":Lorg/json/JSONException;
    invoke-virtual {v2}, Lorg/json/JSONException;->printStackTrace()V

    .line 303
    .end local v2    # "e":Lorg/json/JSONException;
    :cond_3
    return-void
.end method

.method private parseQuery4(Ljava/lang/String;)V
    .locals 7
    .param p1, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 308
    :try_start_0
    new-instance v3, Lorg/json/JSONArray;

    invoke-direct {v3, p1}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 310
    .local v3, "ja":Lorg/json/JSONArray;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v5

    if-ge v2, v5, :cond_2

    .line 312
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserLocationPost;->mLocationPost:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPost;

    .line 314
    .local v0, "curLocationPost":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPost;
    invoke-virtual {v3, v2}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    .line 316
    .local v4, "jsonObj":Lorg/json/JSONObject;
    :goto_1
    if-eqz v0, :cond_1

    .line 317
    const-string v5, "object_id"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iget-object v6, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPost;->mPostID:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 320
    const-string v5, "src_big"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPost;->mImageSrc:Ljava/lang/String;

    .line 322
    const-string v5, "caption"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPost;->mCaption:Ljava/lang/String;

    .line 326
    :cond_0
    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPost;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPost;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 310
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 329
    .end local v0    # "curLocationPost":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPost;
    .end local v2    # "i":I
    .end local v3    # "ja":Lorg/json/JSONArray;
    .end local v4    # "jsonObj":Lorg/json/JSONObject;
    :catch_0
    move-exception v1

    .line 330
    .local v1, "e":Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    .line 332
    .end local v1    # "e":Lorg/json/JSONException;
    :cond_2
    return-void
.end method

.method private parseQuery5(Ljava/lang/String;)V
    .locals 7
    .param p1, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 337
    :try_start_0
    new-instance v3, Lorg/json/JSONArray;

    invoke-direct {v3, p1}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 339
    .local v3, "ja":Lorg/json/JSONArray;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v5

    if-ge v2, v5, :cond_2

    .line 341
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserLocationPost;->mLocationPost:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPost;

    .line 343
    .local v0, "curLocationPost":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPost;
    invoke-virtual {v3, v2}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    .line 345
    .local v4, "jsonObj":Lorg/json/JSONObject;
    :goto_1
    if-eqz v0, :cond_1

    .line 346
    const-string v5, "status_id"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iget-object v6, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPost;->mPostID:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 349
    const-string v5, "message"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPost;->mCaption:Ljava/lang/String;

    .line 353
    :cond_0
    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPost;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPost;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 339
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 356
    .end local v0    # "curLocationPost":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPost;
    .end local v2    # "i":I
    .end local v3    # "ja":Lorg/json/JSONArray;
    .end local v4    # "jsonObj":Lorg/json/JSONObject;
    :catch_0
    move-exception v1

    .line 357
    .local v1, "e":Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    .line 359
    .end local v1    # "e":Lorg/json/JSONException;
    :cond_2
    return-void
.end method

.method private parseQuery6(Ljava/lang/String;)V
    .locals 7
    .param p1, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 364
    :try_start_0
    new-instance v3, Lorg/json/JSONArray;

    invoke-direct {v3, p1}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 366
    .local v3, "ja":Lorg/json/JSONArray;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v5

    if-ge v2, v5, :cond_2

    .line 368
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserLocationPost;->mLocationPost:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPost;

    .line 370
    .local v0, "curLocationPost":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPost;
    invoke-virtual {v3, v2}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    .line 372
    .local v4, "jsonObj":Lorg/json/JSONObject;
    :goto_1
    if-eqz v0, :cond_1

    .line 373
    const-string v5, "checkin_id"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iget-object v6, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPost;->mPostID:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 376
    const-string v5, "message"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPost;->mCaption:Ljava/lang/String;

    .line 380
    :cond_0
    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPost;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPost;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 366
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 383
    .end local v0    # "curLocationPost":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPost;
    .end local v2    # "i":I
    .end local v3    # "ja":Lorg/json/JSONArray;
    .end local v4    # "jsonObj":Lorg/json/JSONObject;
    :catch_0
    move-exception v1

    .line 384
    .local v1, "e":Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    .line 386
    .end local v1    # "e":Lorg/json/JSONException;
    :cond_2
    return-void
.end method

.method private parseQuery7(Ljava/lang/String;)V
    .locals 7
    .param p1, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 391
    :try_start_0
    new-instance v3, Lorg/json/JSONArray;

    invoke-direct {v3, p1}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 393
    .local v3, "ja":Lorg/json/JSONArray;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v5

    if-ge v2, v5, :cond_2

    .line 395
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserLocationPost;->mLocationPost:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPost;

    .line 397
    .local v0, "curLocationPost":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPost;
    invoke-virtual {v3, v2}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    .line 399
    .local v4, "jsonObj":Lorg/json/JSONObject;
    :goto_1
    if-eqz v0, :cond_1

    .line 400
    const-string v5, "vid"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iget-object v6, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPost;->mPostID:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 403
    const-string v5, "description "

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPost;->mCaption:Ljava/lang/String;

    .line 407
    :cond_0
    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPost;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPost;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 393
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 410
    .end local v0    # "curLocationPost":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPost;
    .end local v2    # "i":I
    .end local v3    # "ja":Lorg/json/JSONArray;
    .end local v4    # "jsonObj":Lorg/json/JSONObject;
    :catch_0
    move-exception v1

    .line 411
    .local v1, "e":Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    .line 413
    .end local v1    # "e":Lorg/json/JSONException;
    :cond_2
    return-void
.end method


# virtual methods
.method public parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPost;
    .locals 7
    .param p1, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 125
    :try_start_0
    new-instance v2, Lorg/json/JSONArray;

    invoke-direct {v2, p1}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 127
    .local v2, "ja":Lorg/json/JSONArray;
    if-eqz v2, :cond_2

    .line 129
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v5

    if-ge v1, v5, :cond_2

    .line 131
    invoke-virtual {v2, v1}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v5

    const-string v6, "name"

    invoke-virtual {v5, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 133
    .local v3, "queryName":Ljava/lang/String;
    invoke-virtual {v2, v1}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v5

    const-string v6, "fql_result_set"

    invoke-virtual {v5, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 136
    .local v4, "queryObject":Ljava/lang/String;
    const-string v5, "query1"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 137
    invoke-direct {p0, v4}, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserLocationPost;->parseQuery1(Ljava/lang/String;)V

    .line 129
    :cond_0
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 138
    :cond_1
    const-string v5, "query2"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 139
    invoke-direct {p0, v4}, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserLocationPost;->parseQuery2(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 154
    .end local v1    # "i":I
    .end local v2    # "ja":Lorg/json/JSONArray;
    .end local v3    # "queryName":Ljava/lang/String;
    .end local v4    # "queryObject":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 155
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    .line 158
    .end local v0    # "e":Lorg/json/JSONException;
    :cond_2
    iget-object v5, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserLocationPost;->mLocationPost:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLocationPost;

    return-object v5

    .line 140
    .restart local v1    # "i":I
    .restart local v2    # "ja":Lorg/json/JSONArray;
    .restart local v3    # "queryName":Ljava/lang/String;
    .restart local v4    # "queryObject":Ljava/lang/String;
    :cond_3
    :try_start_1
    const-string v5, "query3"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 141
    invoke-direct {p0, v4}, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserLocationPost;->parseQuery3(Ljava/lang/String;)V

    goto :goto_1

    .line 142
    :cond_4
    const-string v5, "query4"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 143
    invoke-direct {p0, v4}, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserLocationPost;->parseQuery4(Ljava/lang/String;)V

    goto :goto_1

    .line 144
    :cond_5
    const-string v5, "query5"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 145
    invoke-direct {p0, v4}, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserLocationPost;->parseQuery5(Ljava/lang/String;)V

    goto :goto_1

    .line 146
    :cond_6
    const-string v5, "query6"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 147
    invoke-direct {p0, v4}, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserLocationPost;->parseQuery6(Ljava/lang/String;)V

    goto :goto_1

    .line 148
    :cond_7
    const-string v5, "query7"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 149
    invoke-direct {p0, v4}, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserLocationPost;->parseQuery7(Ljava/lang/String;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method
