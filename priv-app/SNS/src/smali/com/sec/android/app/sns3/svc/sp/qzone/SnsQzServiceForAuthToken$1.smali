.class Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzServiceForAuthToken$1;
.super Lcom/sec/android/app/sns3/svc/sp/qzone/auth/api/ISnsQzoneForAuthToken$Stub;
.source "SnsQzServiceForAuthToken.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzServiceForAuthToken;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzServiceForAuthToken;


# direct methods
.method constructor <init>(Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzServiceForAuthToken;)V
    .locals 0

    .prologue
    .line 73
    iput-object p1, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzServiceForAuthToken$1;->this$0:Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzServiceForAuthToken;

    invoke-direct {p0}, Lcom/sec/android/app/sns3/svc/sp/qzone/auth/api/ISnsQzoneForAuthToken$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public getAuthTokenInfo()Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 78
    const-string v2, "SNS"

    const-string v3, "SnsQzServiceForAuthToken : getAuthTokenNExpires() CALLED !!"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 80
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 82
    .local v0, "Info":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v2, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzServiceForAuthToken$1;->this$0:Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzServiceForAuthToken;

    # getter for: Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzServiceForAuthToken;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    invoke-static {v2}, Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzServiceForAuthToken;->access$000(Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzServiceForAuthToken;)Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/sns3/svc/SnsSvcMgr;->getTokenMgr()Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;

    move-result-object v2

    const-string v3, "qzone"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;->getToken(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzToken;

    .line 84
    .local v1, "qzToken":Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzToken;
    const-string v2, "access_token"

    invoke-virtual {v1}, Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzToken;->getAccessToken()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 85
    const-string v2, "user_id"

    invoke-virtual {v1}, Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzToken;->getUserID()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 87
    return-object v0
.end method
