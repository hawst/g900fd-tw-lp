.class Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsServiceForAuthToken$1$1;
.super Lcom/sec/android/app/sns3/svc/sp/foursquare/request/SnsFsReqGetUser;
.source "SnsFsServiceForAuthToken.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsServiceForAuthToken$1;->getMyAccountInfo(Lcom/sec/android/app/sns3/svc/sp/foursquare/auth/api/ISnsFourSquareCallbackMyAccountInfo;)I
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsServiceForAuthToken$1;

.field final synthetic val$cb:Lcom/sec/android/app/sns3/svc/sp/foursquare/auth/api/ISnsFourSquareCallbackMyAccountInfo;


# direct methods
.method constructor <init>(Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsServiceForAuthToken$1;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;Lcom/sec/android/app/sns3/svc/sp/foursquare/auth/api/ISnsFourSquareCallbackMyAccountInfo;)V
    .locals 0
    .param p2, "x0"    # Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    .param p3, "x1"    # Ljava/lang/String;

    .prologue
    .line 107
    iput-object p1, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsServiceForAuthToken$1$1;->this$1:Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsServiceForAuthToken$1;

    iput-object p4, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsServiceForAuthToken$1$1;->val$cb:Lcom/sec/android/app/sns3/svc/sp/foursquare/auth/api/ISnsFourSquareCallbackMyAccountInfo;

    invoke-direct {p0, p2, p3}, Lcom/sec/android/app/sns3/svc/sp/foursquare/request/SnsFsReqGetUser;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public onReqRespond(IZIILandroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;)Z
    .locals 8
    .param p1, "reqID"    # I
    .param p2, "bSuccess"    # Z
    .param p3, "httpStatus"    # I
    .param p4, "errorCode"    # I
    .param p5, "reason"    # Landroid/os/Bundle;
    .param p6, "userInfo"    # Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;

    .prologue
    .line 113
    const/4 v6, 0x0

    .line 114
    .local v6, "myAccountInfo":Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseMyAccountInfo;
    if-eqz p6, :cond_0

    .line 115
    new-instance v6, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseMyAccountInfo;

    .end local v6    # "myAccountInfo":Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseMyAccountInfo;
    invoke-direct {v6}, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseMyAccountInfo;-><init>()V

    .line 116
    .restart local v6    # "myAccountInfo":Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseMyAccountInfo;
    iget-object v0, p6, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mUserID:Ljava/lang/String;

    iput-object v0, v6, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseMyAccountInfo;->mUserID:Ljava/lang/String;

    .line 117
    iget-object v0, p6, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mUserFirstName:Ljava/lang/String;

    iput-object v0, v6, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseMyAccountInfo;->mUserFirstName:Ljava/lang/String;

    .line 118
    iget-object v0, p6, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mUserLastName:Ljava/lang/String;

    iput-object v0, v6, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseMyAccountInfo;->mUserLastName:Ljava/lang/String;

    .line 119
    iget-object v0, p6, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mPhotoUrl:Ljava/lang/String;

    iput-object v0, v6, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseMyAccountInfo;->mPhotoUrl:Ljava/lang/String;

    .line 120
    iget-object v0, p6, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mContact:Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseContact;

    if-eqz v0, :cond_0

    .line 121
    iget-object v0, p6, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mContact:Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseContact;

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseContact;->mEmail:Ljava/lang/String;

    iput-object v0, v6, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseMyAccountInfo;->mEmail:Ljava/lang/String;

    .line 125
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsServiceForAuthToken$1$1;->val$cb:Lcom/sec/android/app/sns3/svc/sp/foursquare/auth/api/ISnsFourSquareCallbackMyAccountInfo;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    invoke-interface/range {v0 .. v6}, Lcom/sec/android/app/sns3/svc/sp/foursquare/auth/api/ISnsFourSquareCallbackMyAccountInfo;->onResponse(IZIILandroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseMyAccountInfo;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 130
    :goto_0
    const/4 v0, 0x0

    return v0

    .line 126
    :catch_0
    move-exception v7

    .line 127
    .local v7, "e":Landroid/os/RemoteException;
    invoke-virtual {v7}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method
