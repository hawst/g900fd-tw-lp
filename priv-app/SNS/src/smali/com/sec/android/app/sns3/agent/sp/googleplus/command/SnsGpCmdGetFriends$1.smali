.class Lcom/sec/android/app/sns3/agent/sp/googleplus/command/SnsGpCmdGetFriends$1;
.super Lcom/sec/android/app/sns3/svc/sp/googleplus/request/SnsGpReqGetFriends;
.source "SnsGpCmdGetFriends.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/sns3/agent/sp/googleplus/command/SnsGpCmdGetFriends;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Handler;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/sns3/agent/sp/googleplus/command/SnsGpCmdGetFriends;


# direct methods
.method constructor <init>(Lcom/sec/android/app/sns3/agent/sp/googleplus/command/SnsGpCmdGetFriends;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 0
    .param p2, "x0"    # Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    .param p3, "x1"    # Ljava/lang/String;
    .param p4, "x2"    # Landroid/os/Bundle;

    .prologue
    .line 51
    iput-object p1, p0, Lcom/sec/android/app/sns3/agent/sp/googleplus/command/SnsGpCmdGetFriends$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/googleplus/command/SnsGpCmdGetFriends;

    invoke-direct {p0, p2, p3, p4}, Lcom/sec/android/app/sns3/svc/sp/googleplus/request/SnsGpReqGetFriends;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;Landroid/os/Bundle;)V

    return-void
.end method


# virtual methods
.method public onReqRespond(IZIILandroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFriends;)Z
    .locals 7
    .param p1, "reqID"    # I
    .param p2, "bSuccess"    # Z
    .param p3, "httpStatus"    # I
    .param p4, "errorCode"    # I
    .param p5, "reason"    # Landroid/os/Bundle;
    .param p6, "friends"    # Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFriends;

    .prologue
    const/4 v6, 0x0

    .line 56
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 57
    .local v2, "values":Landroid/content/ContentValues;
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/sns3/SnsApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 59
    .local v0, "cr":Landroid/content/ContentResolver;
    if-eqz p2, :cond_1

    .line 60
    if-eqz p6, :cond_0

    .line 62
    sget-object v3, Lcom/sec/android/app/sns3/agent/sp/googleplus/db/SnsGooglePlusDB$FriendsProfileInfo;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v3, v6, v6}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 64
    iget-object v1, p6, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFriends;->mFriends:Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseProfile;

    .line 65
    .local v1, "curFriend":Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseProfile;
    :goto_0
    if-eqz v1, :cond_0

    .line 66
    invoke-virtual {v2}, Landroid/content/ContentValues;->clear()V

    .line 68
    const-string v3, "profile_id"

    iget-object v4, v1, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseProfile;->mUserID:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    const-string v3, "profile_name"

    iget-object v4, v1, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseProfile;->mUserName:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    const-string v3, "profile_image"

    iget-object v4, v1, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseProfile;->mImageUrl:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    const-string v3, "profile_url"

    iget-object v4, v1, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseProfile;->mProfileUrl:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    const-string v3, "object_type"

    iget-object v4, v1, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseProfile;->mType:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    sget-object v3, Lcom/sec/android/app/sns3/agent/sp/googleplus/db/SnsGooglePlusDB$FriendsProfileInfo;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v3, v2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 76
    iget-object v1, v1, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseProfile;->mNext:Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseProfile;

    goto :goto_0

    .line 80
    .end local v1    # "curFriend":Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseProfile;
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/sns3/agent/sp/googleplus/command/SnsGpCmdGetFriends$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/googleplus/command/SnsGpCmdGetFriends;

    sget-object v4, Lcom/sec/android/app/sns3/agent/sp/googleplus/db/SnsGooglePlusDB$FriendsProfileInfo;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/sns3/agent/sp/googleplus/command/SnsGpCmdGetFriends;->setUri(Ljava/lang/String;)V

    .line 87
    :goto_1
    iget-object v3, p0, Lcom/sec/android/app/sns3/agent/sp/googleplus/command/SnsGpCmdGetFriends$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/googleplus/command/SnsGpCmdGetFriends;

    invoke-virtual {v3, p2}, Lcom/sec/android/app/sns3/agent/sp/googleplus/command/SnsGpCmdGetFriends;->setSuccess(Z)V

    .line 88
    iget-object v3, p0, Lcom/sec/android/app/sns3/agent/sp/googleplus/command/SnsGpCmdGetFriends$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/googleplus/command/SnsGpCmdGetFriends;

    invoke-virtual {v3, p0}, Lcom/sec/android/app/sns3/agent/sp/googleplus/command/SnsGpCmdGetFriends;->receive(Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;)V

    .line 90
    const/4 v3, 0x1

    return v3

    .line 82
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/sns3/agent/sp/googleplus/command/SnsGpCmdGetFriends$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/googleplus/command/SnsGpCmdGetFriends;

    new-instance v4, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;

    const-string v5, "googleplus"

    invoke-direct {v4, v5, p3, p4, p5}, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;-><init>(Ljava/lang/String;IILandroid/os/Bundle;)V

    invoke-virtual {v3, v4}, Lcom/sec/android/app/sns3/agent/sp/googleplus/command/SnsGpCmdGetFriends;->setResponseList(Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;)Z

    .line 84
    iget-object v3, p0, Lcom/sec/android/app/sns3/agent/sp/googleplus/command/SnsGpCmdGetFriends$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/googleplus/command/SnsGpCmdGetFriends;

    invoke-virtual {v3, v6}, Lcom/sec/android/app/sns3/agent/sp/googleplus/command/SnsGpCmdGetFriends;->setUri(Ljava/lang/String;)V

    goto :goto_1
.end method
