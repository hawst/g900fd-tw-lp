.class public Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;
.super Ljava/lang/Object;
.source "SnsCommandUnit.java"


# static fields
.field public static final STATE_FINISHED:I = 0x2

.field public static final STATE_INIT:I = 0x0

.field public static final STATE_WAIT:I = 0x1


# instance fields
.field private mFinishedRequests:I

.field private mIndex:I

.field private mRequests:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;",
            ">;"
        }
    .end annotation
.end field

.field private mUnitState:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput v0, p0, Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;->mFinishedRequests:I

    .line 37
    iput v0, p0, Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;->mUnitState:I

    .line 40
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;->mRequests:Ljava/util/Map;

    .line 41
    return-void
.end method


# virtual methods
.method public addRequest(Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;)V
    .locals 2
    .param p1, "req"    # Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;->mRequests:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->getReqID()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 45
    return-void
.end method

.method getIndex()I
    .locals 1

    .prologue
    .line 69
    iget v0, p0, Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;->mIndex:I

    return v0
.end method

.method getRequestMap()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;",
            ">;"
        }
    .end annotation

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;->mRequests:Ljava/util/Map;

    return-object v0
.end method

.method getState()I
    .locals 1

    .prologue
    .line 73
    iget v0, p0, Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;->mUnitState:I

    return v0
.end method

.method isEmpty()Z
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;->mRequests:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public receiveReqResponse()V
    .locals 2

    .prologue
    .line 48
    iget v0, p0, Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;->mFinishedRequests:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;->mFinishedRequests:I

    .line 50
    iget-object v0, p0, Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;->mRequests:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    iget v1, p0, Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;->mFinishedRequests:I

    if-ne v0, v1, :cond_0

    .line 51
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;->setState(I)V

    .line 53
    :cond_0
    return-void
.end method

.method setIndex(I)V
    .locals 0
    .param p1, "index"    # I

    .prologue
    .line 65
    iput p1, p0, Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;->mIndex:I

    .line 66
    return-void
.end method

.method setState(I)V
    .locals 0
    .param p1, "unitState"    # I

    .prologue
    .line 77
    iput p1, p0, Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;->mUnitState:I

    .line 78
    return-void
.end method
