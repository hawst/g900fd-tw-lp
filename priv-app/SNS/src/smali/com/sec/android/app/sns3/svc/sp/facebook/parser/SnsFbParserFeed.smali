.class public Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserFeed;
.super Ljava/lang/Object;
.source "SnsFbParserFeed.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserFeed$CommandCallback;,
        Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserFeed$FacebookFeed;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 229
    return-void
.end method

.method public static parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFeed;
    .locals 25
    .param p0, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 99
    const/4 v7, 0x0

    .line 100
    .local v7, "feed":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFeed;
    const/4 v5, 0x0

    .line 102
    .local v5, "curFeed":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFeed;
    const/4 v12, 0x0

    .line 105
    .local v12, "jsonObject":Lorg/json/JSONObject;
    :try_start_0
    new-instance v13, Lorg/json/JSONObject;

    move-object/from16 v0, p0

    invoke-direct {v13, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1

    .line 107
    .end local v12    # "jsonObject":Lorg/json/JSONObject;
    .local v13, "jsonObject":Lorg/json/JSONObject;
    :try_start_1
    new-instance v11, Lorg/json/JSONArray;

    const-string v22, "data"

    move-object/from16 v0, v22

    invoke-virtual {v13, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-direct {v11, v0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 109
    .local v11, "ja":Lorg/json/JSONArray;
    if-eqz v11, :cond_8

    .line 111
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_0
    invoke-virtual {v11}, Lorg/json/JSONArray;->length()I

    move-result v22

    move/from16 v0, v22

    if-ge v9, v0, :cond_8

    .line 112
    new-instance v16, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFeed;

    invoke-direct/range {v16 .. v16}, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFeed;-><init>()V

    .line 114
    .local v16, "newFeed":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFeed;
    invoke-virtual {v11, v9}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v8

    .line 116
    .local v8, "feedJsonObj":Lorg/json/JSONObject;
    const-string v22, "from"

    move-object/from16 v0, v22

    invoke-virtual {v8, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_0

    .line 117
    const-string v22, "from"

    move-object/from16 v0, v22

    invoke-virtual {v8, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v22 .. v22}, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserFrom;->parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFrom;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, v16

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFeed;->mFrom:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFrom;

    .line 121
    :cond_0
    const-string v22, "id"

    move-object/from16 v0, v22

    invoke-virtual {v8, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, v16

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFeed;->mFeedID:Ljava/lang/String;

    .line 122
    const-string v22, "message"

    move-object/from16 v0, v22

    invoke-virtual {v8, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, v16

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFeed;->mMessage:Ljava/lang/String;

    .line 123
    const-string v22, "story"

    move-object/from16 v0, v22

    invoke-virtual {v8, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, v16

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFeed;->mStory:Ljava/lang/String;

    .line 124
    const-string v22, "link"

    move-object/from16 v0, v22

    invoke-virtual {v8, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, v16

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFeed;->mLink:Ljava/lang/String;

    .line 125
    const-string v22, "object_id"

    move-object/from16 v0, v22

    invoke-virtual {v8, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, v16

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFeed;->mPhotoId:Ljava/lang/String;

    .line 127
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFeed;->mLink:Ljava/lang/String;

    move-object/from16 v22, v0

    const-string v23, "&"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v20

    .line 129
    .local v20, "splitUrl":[Ljava/lang/String;
    const/16 v18, 0x0

    .line 131
    .local v18, "photo_count":Ljava/lang/String;
    move-object/from16 v3, v20

    .local v3, "arr$":[Ljava/lang/String;
    array-length v15, v3

    .local v15, "len$":I
    const/4 v10, 0x0

    .local v10, "i$":I
    :goto_1
    if-ge v10, v15, :cond_2

    aget-object v17, v3, v10

    .line 132
    .local v17, "parameter":Ljava/lang/String;
    const-string v22, "="

    move-object/from16 v0, v17

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v21

    .line 133
    .local v21, "v":[Ljava/lang/String;
    const/16 v22, 0x0

    aget-object v22, v21, v22

    const-string v23, "relevant_count"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_1

    .line 134
    const/16 v22, 0x1

    aget-object v18, v21, v22

    .line 131
    :cond_1
    add-int/lit8 v10, v10, 0x1

    goto :goto_1

    .line 136
    .end local v17    # "parameter":Ljava/lang/String;
    .end local v21    # "v":[Ljava/lang/String;
    :cond_2
    sget-object v22, Lcom/sec/android/app/sns3/app/life/SnsLifeResource;->TAG:Ljava/lang/String;

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "Photo count is "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 138
    if-eqz v18, :cond_6

    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v22

    const/16 v23, 0x1

    move/from16 v0, v22

    move/from16 v1, v23

    if-le v0, v1, :cond_6

    .line 140
    const-string v22, "album"

    move-object/from16 v0, v22

    move-object/from16 v1, v16

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFeed;->mType:Ljava/lang/String;

    .line 142
    new-instance v4, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAllPhotos;

    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Lcom/sec/android/app/sns3/SnsApplication;->getSvcMgr()Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v22

    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Lcom/sec/android/app/sns3/SnsApplication;->getAgentMgr()Lcom/sec/android/app/sns3/agent/SnsAgentMgr;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Lcom/sec/android/app/sns3/agent/SnsAgentMgr;->getCommandMgr()Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;->getCommandMgrHandle()Landroid/os/Handler;

    move-result-object v23

    const-string v24, "me"

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    move-object/from16 v2, v24

    invoke-direct {v4, v0, v1, v2}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAllPhotos;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Handler;Ljava/lang/String;)V

    .line 145
    .local v4, "cmd":Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;
    new-instance v22, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserFeed$CommandCallback;

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFeed;->mFeedID:Ljava/lang/String;

    move-object/from16 v23, v0

    invoke-direct/range {v22 .. v23}, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserFeed$CommandCallback;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v22

    invoke-virtual {v4, v0}, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->setCommandCallback(Lcom/sec/android/app/sns3/agent/command/ISnsCommandCallback;)Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;

    .line 146
    invoke-virtual {v4}, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->send()I

    .line 157
    .end local v4    # "cmd":Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;
    :cond_3
    :goto_2
    const-string v22, "updated_time"

    move-object/from16 v0, v22

    invoke-virtual {v8, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, v16

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFeed;->mUpdatedTime:Ljava/lang/String;

    .line 158
    const-string v22, "created_time"

    move-object/from16 v0, v22

    invoke-virtual {v8, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, v16

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFeed;->mCreatedTime:Ljava/lang/String;

    .line 159
    const-string v22, "source"

    move-object/from16 v0, v22

    invoke-virtual {v8, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, v16

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFeed;->mSource:Ljava/lang/String;

    .line 160
    const-string v22, "name"

    move-object/from16 v0, v22

    invoke-virtual {v8, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, v16

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFeed;->mName:Ljava/lang/String;

    .line 161
    const-string v22, "caption"

    move-object/from16 v0, v22

    invoke-virtual {v8, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, v16

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFeed;->mCaption:Ljava/lang/String;

    .line 162
    const-string v22, "description"

    move-object/from16 v0, v22

    invoke-virtual {v8, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, v16

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFeed;->mDescription:Ljava/lang/String;

    .line 163
    const-string v22, "icon"

    move-object/from16 v0, v22

    invoke-virtual {v8, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, v16

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFeed;->mIcon:Ljava/lang/String;

    .line 164
    const-string v22, "status_type"

    move-object/from16 v0, v22

    invoke-virtual {v8, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, v16

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFeed;->mStatusType:Ljava/lang/String;

    .line 166
    const-string v22, "place"

    move-object/from16 v0, v22

    invoke-virtual {v8, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_5

    .line 167
    new-instance v19, Lorg/json/JSONObject;

    const-string v22, "place"

    move-object/from16 v0, v22

    invoke-virtual {v8, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    invoke-direct {v0, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 169
    .local v19, "place":Lorg/json/JSONObject;
    new-instance v14, Lorg/json/JSONObject;

    const-string v22, "location"

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-direct {v14, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 171
    .local v14, "latAndLong":Lorg/json/JSONObject;
    const-string v22, "latitude"

    move-object/from16 v0, v22

    invoke-virtual {v14, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, v16

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFeed;->mLatitude:Ljava/lang/String;

    .line 172
    const-string v22, "longitude"

    move-object/from16 v0, v22

    invoke-virtual {v14, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, v16

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFeed;->mLongitude:Ljava/lang/String;

    .line 173
    const-string v22, "name"

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, v16

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFeed;->mLocationName:Ljava/lang/String;

    .line 174
    const-string v22, "city"

    move-object/from16 v0, v22

    invoke-virtual {v14, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/String;->length()I

    move-result v22

    if-lez v22, :cond_4

    .line 175
    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFeed;->mLocationName:Ljava/lang/String;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, ","

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, "city"

    move-object/from16 v0, v23

    invoke-virtual {v14, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, v16

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFeed;->mLocationName:Ljava/lang/String;

    .line 178
    :cond_4
    const-string v22, "country"

    move-object/from16 v0, v22

    invoke-virtual {v14, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/String;->length()I

    move-result v22

    if-lez v22, :cond_5

    .line 179
    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFeed;->mLocationName:Ljava/lang/String;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, ","

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, "country"

    move-object/from16 v0, v23

    invoke-virtual {v14, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, v16

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFeed;->mLocationName:Ljava/lang/String;

    .line 185
    .end local v14    # "latAndLong":Lorg/json/JSONObject;
    .end local v19    # "place":Lorg/json/JSONObject;
    :cond_5
    if-nez v7, :cond_7

    .line 186
    move-object/from16 v7, v16

    .line 187
    move-object v5, v7

    .line 111
    :goto_3
    add-int/lit8 v9, v9, 0x1

    goto/16 :goto_0

    .line 149
    :cond_6
    const-string v22, "picture"

    move-object/from16 v0, v22

    invoke-virtual {v8, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, v16

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFeed;->mPicture:Ljava/lang/String;

    .line 150
    const-string v22, "type"

    move-object/from16 v0, v22

    invoke-virtual {v8, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, v16

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFeed;->mType:Ljava/lang/String;

    .line 152
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFeed;->mPhotoId:Ljava/lang/String;

    move-object/from16 v22, v0

    if-eqz v22, :cond_3

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFeed;->mPhotoId:Ljava/lang/String;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Ljava/lang/String;->isEmpty()Z

    move-result v22

    if-nez v22, :cond_3

    .line 153
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFeed;->mFeedID:Ljava/lang/String;

    move-object/from16 v22, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFeed;->mPhotoId:Ljava/lang/String;

    move-object/from16 v23, v0

    invoke-static/range {v22 .. v23}, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserFeed;->updatePhotoSize(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_2

    .line 195
    .end local v3    # "arr$":[Ljava/lang/String;
    .end local v8    # "feedJsonObj":Lorg/json/JSONObject;
    .end local v9    # "i":I
    .end local v10    # "i$":I
    .end local v11    # "ja":Lorg/json/JSONArray;
    .end local v15    # "len$":I
    .end local v16    # "newFeed":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFeed;
    .end local v18    # "photo_count":Ljava/lang/String;
    .end local v20    # "splitUrl":[Ljava/lang/String;
    :catch_0
    move-exception v6

    move-object v12, v13

    .line 197
    .end local v13    # "jsonObject":Lorg/json/JSONObject;
    .local v6, "e":Lorg/json/JSONException;
    .restart local v12    # "jsonObject":Lorg/json/JSONObject;
    :goto_4
    invoke-virtual {v6}, Lorg/json/JSONException;->printStackTrace()V

    .line 200
    .end local v6    # "e":Lorg/json/JSONException;
    :goto_5
    return-object v7

    .line 189
    .end local v12    # "jsonObject":Lorg/json/JSONObject;
    .restart local v3    # "arr$":[Ljava/lang/String;
    .restart local v8    # "feedJsonObj":Lorg/json/JSONObject;
    .restart local v9    # "i":I
    .restart local v10    # "i$":I
    .restart local v11    # "ja":Lorg/json/JSONArray;
    .restart local v13    # "jsonObject":Lorg/json/JSONObject;
    .restart local v15    # "len$":I
    .restart local v16    # "newFeed":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFeed;
    .restart local v18    # "photo_count":Ljava/lang/String;
    .restart local v20    # "splitUrl":[Ljava/lang/String;
    :cond_7
    :try_start_2
    move-object/from16 v0, v16

    iput-object v0, v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFeed;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFeed;

    .line 190
    iget-object v5, v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFeed;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFeed;
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_3

    .end local v3    # "arr$":[Ljava/lang/String;
    .end local v8    # "feedJsonObj":Lorg/json/JSONObject;
    .end local v9    # "i":I
    .end local v10    # "i$":I
    .end local v15    # "len$":I
    .end local v16    # "newFeed":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFeed;
    .end local v18    # "photo_count":Ljava/lang/String;
    .end local v20    # "splitUrl":[Ljava/lang/String;
    :cond_8
    move-object v12, v13

    .line 198
    .end local v13    # "jsonObject":Lorg/json/JSONObject;
    .restart local v12    # "jsonObject":Lorg/json/JSONObject;
    goto :goto_5

    .line 195
    .end local v11    # "ja":Lorg/json/JSONArray;
    :catch_1
    move-exception v6

    goto :goto_4
.end method

.method private static updatePhotoSize(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p0, "postID"    # Ljava/lang/String;
    .param p1, "photoID"    # Ljava/lang/String;

    .prologue
    .line 204
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserFeed$1;

    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/sns3/SnsApplication;->getSvcMgr()Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v1

    invoke-direct {v0, v1, p1, p0}, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserFeed$1;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;Ljava/lang/String;)V

    .line 226
    .local v0, "req":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->request()Z

    .line 227
    return-void
.end method
