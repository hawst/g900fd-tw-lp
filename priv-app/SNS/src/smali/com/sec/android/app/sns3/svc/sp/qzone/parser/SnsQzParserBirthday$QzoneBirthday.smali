.class public interface abstract Lcom/sec/android/app/sns3/svc/sp/qzone/parser/SnsQzParserBirthday$QzoneBirthday;
.super Ljava/lang/Object;
.source "SnsQzParserBirthday.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/svc/sp/qzone/parser/SnsQzParserBirthday;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "QzoneBirthday"
.end annotation


# static fields
.field public static final BIRTHDAY_DAY:Ljava/lang/String; = "day"

.field public static final BIRTHDAY_MONTH:Ljava/lang/String; = "month"

.field public static final DATA:Ljava/lang/String; = "data"

.field public static final FRIENDS:Ljava/lang/String; = "friends"

.field public static final FRIEND_OPENID:Ljava/lang/String; = "openid"

.field public static final GENDER:Ljava/lang/String; = "gender"

.field public static final HOST_OPENID:Ljava/lang/String; = "hostopenid"

.field public static final LUNAR_FLAG:Ljava/lang/String; = "lunarflag"

.field public static final NICKNAME:Ljava/lang/String; = "nickname"

.field public static final PIC_URL:Ljava/lang/String; = "portraiturl"
