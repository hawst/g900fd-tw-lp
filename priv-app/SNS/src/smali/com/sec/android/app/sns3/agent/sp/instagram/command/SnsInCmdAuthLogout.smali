.class public Lcom/sec/android/app/sns3/agent/sp/instagram/command/SnsInCmdAuthLogout;
.super Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;
.source "SnsInCmdAuthLogout.java"


# direct methods
.method public constructor <init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Handler;)V
    .locals 6
    .param p1, "svcMgr"    # Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    .param p2, "cmdHandler"    # Landroid/os/Handler;

    .prologue
    const/4 v5, 0x0

    .line 38
    invoke-direct {p0, p2}, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;-><init>(Landroid/os/Handler;)V

    .line 40
    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/SnsSvcMgr;->getRequestMgr()Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;

    move-result-object v3

    const-string v4, "instagram"

    invoke-virtual {v3, v4}, Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;->clearReservedRequestsBySp(Ljava/lang/String;)Z

    .line 42
    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/SnsSvcMgr;->getTokenMgr()Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;

    move-result-object v2

    .line 43
    .local v2, "tokenMgr":Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;
    const-string v3, "instagram"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;->getToken(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/sns3/svc/sp/instagram/SnsInToken;

    invoke-virtual {v3}, Lcom/sec/android/app/sns3/svc/sp/instagram/SnsInToken;->removeAll()V

    .line 45
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/sns3/SnsApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 46
    .local v0, "cr":Landroid/content/ContentResolver;
    sget-object v3, Lcom/sec/android/app/sns3/agent/sp/instagram/db/SnsInstagramDB$WIPE_INSTAGRAM_DATA;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v3, v5, v5}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 48
    const-string v1, "sp_type = instagram"

    .line 49
    .local v1, "selection":Ljava/lang/String;
    sget-object v3, Lcom/sec/android/app/sns3/agent/life/db/SnsLifeDB$LifeTimeline;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v3, v1, v5}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 51
    return-void
.end method


# virtual methods
.method protected respond()Z
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 62
    const-string v0, "SnsAgent"

    const-string v1, "<SnsInCmdAuthLogout> respond()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 63
    invoke-virtual {p0, v5}, Lcom/sec/android/app/sns3/agent/sp/instagram/command/SnsInCmdAuthLogout;->setSuccess(Z)V

    .line 64
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/sns3/agent/sp/instagram/command/SnsInCmdAuthLogout;->setUri(Ljava/lang/String;)V

    .line 65
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/instagram/command/SnsInCmdAuthLogout;->getCommandCallback()Lcom/sec/android/app/sns3/agent/command/ISnsCommandCallback;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/instagram/command/SnsInCmdAuthLogout;->getCommandID()I

    move-result v1

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/instagram/command/SnsInCmdAuthLogout;->isSuccess()Z

    move-result v2

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/instagram/command/SnsInCmdAuthLogout;->getUri()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/instagram/command/SnsInCmdAuthLogout;->getResponseList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/sec/android/app/sns3/agent/command/ISnsCommandCallback;->onCmdRespond(IZLjava/lang/String;Ljava/util/List;)V

    .line 66
    return v5
.end method

.method public send()I
    .locals 1

    .prologue
    .line 56
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/instagram/command/SnsInCmdAuthLogout;->respond()Z

    .line 57
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/instagram/command/SnsInCmdAuthLogout;->getCommandID()I

    move-result v0

    return v0
.end method
