.class public Lcom/sec/android/app/sns3/svc/sp/qzone/parser/SnsQzParserLocationPost;
.super Ljava/lang/Object;
.source "SnsQzParserLocationPost.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/qzone/parser/SnsQzParserLocationPost$QzoneLocationPost;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    return-void
.end method


# virtual methods
.method public parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseLocationPost;
    .locals 13
    .param p1, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 66
    const/4 v9, 0x0

    .line 67
    .local v9, "mLocationPost":Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseLocationPost;
    const/4 v0, 0x0

    .line 68
    .local v0, "curLocationPost":Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseLocationPost;
    const/4 v6, 0x0

    .line 71
    .local v6, "jsonObject":Lorg/json/JSONObject;
    :try_start_0
    new-instance v7, Lorg/json/JSONObject;

    invoke-direct {v7, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 73
    .end local v6    # "jsonObject":Lorg/json/JSONObject;
    .local v7, "jsonObject":Lorg/json/JSONObject;
    :try_start_1
    new-instance v5, Lorg/json/JSONObject;

    const-string v11, "data"

    invoke-virtual {v7, v11}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v5, v11}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 77
    .local v5, "jdata":Lorg/json/JSONObject;
    new-instance v4, Lorg/json/JSONArray;

    const-string v11, "checkins"

    invoke-virtual {v5, v11}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v4, v11}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 79
    .local v4, "janearby":Lorg/json/JSONArray;
    if-eqz v4, :cond_2

    .line 81
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v11

    if-ge v2, v11, :cond_2

    .line 82
    new-instance v10, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseLocationPost;

    invoke-direct {v10}, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseLocationPost;-><init>()V

    .line 84
    .local v10, "newLocationPost":Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseLocationPost;
    invoke-virtual {v4, v2}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v8

    .line 86
    .local v8, "locationJsonObj":Lorg/json/JSONObject;
    const-string v11, "cellid"

    invoke-virtual {v8, v11}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    iput-object v11, v10, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseLocationPost;->mPostID:Ljava/lang/String;

    .line 87
    const-string v11, "latitude"

    invoke-virtual {v8, v11}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    iput-object v11, v10, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseLocationPost;->mLatitude:Ljava/lang/String;

    .line 88
    const-string v11, "longitude"

    invoke-virtual {v8, v11}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    iput-object v11, v10, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseLocationPost;->mLongitude:Ljava/lang/String;

    .line 89
    const-string v11, "positionname"

    invoke-virtual {v8, v11}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    iput-object v11, v10, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseLocationPost;->mPlaceName:Ljava/lang/String;

    .line 91
    const-string v11, "openid"

    invoke-virtual {v8, v11}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    iput-object v11, v10, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseLocationPost;->mAuthorUID:Ljava/lang/String;

    .line 92
    const-string v11, "caption"

    invoke-virtual {v8, v11}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    iput-object v11, v10, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseLocationPost;->mCaption:Ljava/lang/String;

    .line 93
    const-string v11, "detailparam"

    invoke-virtual {v8, v11}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    iput-object v11, v10, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseLocationPost;->mDetailParam:Ljava/lang/String;

    .line 94
    const-string v11, "time"

    invoke-virtual {v8, v11}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    iput-object v11, v10, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseLocationPost;->mTimestamp:Ljava/lang/String;

    .line 96
    const-string v11, "pictureurl"

    invoke-virtual {v8, v11}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 97
    new-instance v3, Lorg/json/JSONArray;

    const-string v11, "pictureurl"

    invoke-virtual {v8, v11}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v3, v11}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 99
    .local v3, "jaPicture":Lorg/json/JSONArray;
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v11

    if-lez v11, :cond_0

    .line 100
    const/4 v11, 0x0

    invoke-virtual {v3, v11}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v11

    const-string v12, "pictureurl_origin"

    invoke-virtual {v11, v12}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    iput-object v11, v10, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseLocationPost;->mImageSrc:Ljava/lang/String;

    .line 104
    .end local v3    # "jaPicture":Lorg/json/JSONArray;
    :cond_0
    if-nez v9, :cond_1

    .line 105
    move-object v9, v10

    .line 106
    move-object v0, v9

    .line 81
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0

    .line 108
    :cond_1
    iput-object v10, v0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseLocationPost;->mNext:Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseLocationPost;

    .line 109
    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseLocationPost;->mNext:Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseLocationPost;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .end local v2    # "i":I
    .end local v8    # "locationJsonObj":Lorg/json/JSONObject;
    .end local v10    # "newLocationPost":Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseLocationPost;
    :cond_2
    move-object v6, v7

    .line 119
    .end local v4    # "janearby":Lorg/json/JSONArray;
    .end local v5    # "jdata":Lorg/json/JSONObject;
    .end local v7    # "jsonObject":Lorg/json/JSONObject;
    .restart local v6    # "jsonObject":Lorg/json/JSONObject;
    :goto_2
    return-object v9

    .line 114
    :catch_0
    move-exception v1

    .line 116
    .local v1, "e":Lorg/json/JSONException;
    :goto_3
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_2

    .line 114
    .end local v1    # "e":Lorg/json/JSONException;
    .end local v6    # "jsonObject":Lorg/json/JSONObject;
    .restart local v7    # "jsonObject":Lorg/json/JSONObject;
    :catch_1
    move-exception v1

    move-object v6, v7

    .end local v7    # "jsonObject":Lorg/json/JSONObject;
    .restart local v6    # "jsonObject":Lorg/json/JSONObject;
    goto :goto_3
.end method
