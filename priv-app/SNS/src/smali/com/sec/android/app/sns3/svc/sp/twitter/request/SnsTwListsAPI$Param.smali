.class public final enum Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;
.super Ljava/lang/Enum;
.source "SnsTwListsAPI.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Param"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;

.field public static final enum CURSOR:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;

.field public static final enum DESCRIPTION:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;

.field public static final enum FILTER_TO_OWNED_LISTS:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;

.field public static final enum ID:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;

.field public static final enum INCLUDE_ENTITIES:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;

.field public static final enum LIST_ID:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;

.field public static final enum MAX_ID:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;

.field public static final enum MODE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;

.field public static final enum NAME:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;

.field public static final enum OWNER_ID:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;

.field public static final enum OWNER_SCREEN_NAME:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;

.field public static final enum PAGE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;

.field public static final enum PER_PAGE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;

.field public static final enum SCREEN_NAME:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;

.field public static final enum SINCE_ID:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;

.field public static final enum SKIP_STATUS:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;

.field public static final enum SLUG:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;

.field public static final enum USER_ID:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;


# instance fields
.field private mParam:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 48
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;

    const-string v1, "ID"

    const-string v2, "id"

    invoke-direct {v0, v1, v4, v2}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;->ID:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;

    .line 49
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;

    const-string v1, "PAGE"

    const-string v2, "page"

    invoke-direct {v0, v1, v5, v2}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;->PAGE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;

    .line 50
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;

    const-string v1, "USER_ID"

    const-string v2, "user_id"

    invoke-direct {v0, v1, v6, v2}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;->USER_ID:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;

    .line 51
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;

    const-string v1, "SCREEN_NAME"

    const-string v2, "screen_name"

    invoke-direct {v0, v1, v7, v2}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;->SCREEN_NAME:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;

    .line 52
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;

    const-string v1, "INCLUDE_ENTITIES"

    const-string v2, "include_entities"

    invoke-direct {v0, v1, v8, v2}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;->INCLUDE_ENTITIES:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;

    .line 53
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;

    const-string v1, "LIST_ID"

    const/4 v2, 0x5

    const-string v3, "list_id"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;->LIST_ID:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;

    .line 54
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;

    const-string v1, "SLUG"

    const/4 v2, 0x6

    const-string v3, "slug"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;->SLUG:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;

    .line 55
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;

    const-string v1, "OWNER_ID"

    const/4 v2, 0x7

    const-string v3, "owner_id"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;->OWNER_ID:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;

    .line 56
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;

    const-string v1, "OWNER_SCREEN_NAME"

    const/16 v2, 0x8

    const-string v3, "owner_screen_name"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;->OWNER_SCREEN_NAME:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;

    .line 57
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;

    const-string v1, "SINCE_ID"

    const/16 v2, 0x9

    const-string v3, "since_id"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;->SINCE_ID:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;

    .line 58
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;

    const-string v1, "MAX_ID"

    const/16 v2, 0xa

    const-string v3, "max_id"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;->MAX_ID:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;

    .line 59
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;

    const-string v1, "PER_PAGE"

    const/16 v2, 0xb

    const-string v3, "per_page"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;->PER_PAGE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;

    .line 60
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;

    const-string v1, "CURSOR"

    const/16 v2, 0xc

    const-string v3, "cursor"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;->CURSOR:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;

    .line 61
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;

    const-string v1, "FILTER_TO_OWNED_LISTS"

    const/16 v2, 0xd

    const-string v3, "filter_to_owned_lists"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;->FILTER_TO_OWNED_LISTS:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;

    .line 62
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;

    const-string v1, "SKIP_STATUS"

    const/16 v2, 0xe

    const-string v3, "skip_status"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;->SKIP_STATUS:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;

    .line 63
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;

    const-string v1, "NAME"

    const/16 v2, 0xf

    const-string v3, "name"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;->NAME:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;

    .line 64
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;

    const-string v1, "MODE"

    const/16 v2, 0x10

    const-string v3, "mode"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;->MODE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;

    .line 65
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;

    const-string v1, "DESCRIPTION"

    const/16 v2, 0x11

    const-string v3, "description"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;->DESCRIPTION:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;

    .line 47
    const/16 v0, 0x12

    new-array v0, v0, [Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;

    sget-object v1, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;->ID:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;->PAGE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;->USER_ID:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;->SCREEN_NAME:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;

    aput-object v1, v0, v7

    sget-object v1, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;->INCLUDE_ENTITIES:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;->LIST_ID:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;->SLUG:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;->OWNER_ID:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;->OWNER_SCREEN_NAME:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;->SINCE_ID:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;->MAX_ID:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;->PER_PAGE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;->CURSOR:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;->FILTER_TO_OWNED_LISTS:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;->SKIP_STATUS:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;->NAME:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;->MODE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;->DESCRIPTION:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;->$VALUES:[Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "param"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 69
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 70
    iput-object p3, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;->mParam:Ljava/lang/String;

    .line 71
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 47
    const-class v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;
    .locals 1

    .prologue
    .line 47
    sget-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;->$VALUES:[Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;

    invoke-virtual {v0}, [Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;

    return-object v0
.end method


# virtual methods
.method public getParam()Ljava/lang/String;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;->mParam:Ljava/lang/String;

    return-object v0
.end method
