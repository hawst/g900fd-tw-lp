.class Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService$SyncAdapterImpl;
.super Landroid/content/AbstractThreadedSyncAdapter;
.source "SnsLiSyncAdapterProfileService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SyncAdapterImpl"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;Landroid/content/Context;)V
    .locals 1
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 87
    iput-object p1, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService$SyncAdapterImpl;->this$0:Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;

    .line 88
    const/4 v0, 0x1

    invoke-direct {p0, p2, v0}, Landroid/content/AbstractThreadedSyncAdapter;-><init>(Landroid/content/Context;Z)V

    .line 89
    # setter for: Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;->mContext:Landroid/content/Context;
    invoke-static {p1, p2}, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;->access$002(Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;Landroid/content/Context;)Landroid/content/Context;

    .line 90
    return-void
.end method


# virtual methods
.method public onPerformSync(Landroid/accounts/Account;Landroid/os/Bundle;Ljava/lang/String;Landroid/content/ContentProviderClient;Landroid/content/SyncResult;)V
    .locals 6
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "extras"    # Landroid/os/Bundle;
    .param p3, "authority"    # Ljava/lang/String;
    .param p4, "provider"    # Landroid/content/ContentProviderClient;
    .param p5, "syncResult"    # Landroid/content/SyncResult;

    .prologue
    .line 96
    const-string v1, "SnsLiSync"

    const-string v2, "***************** SnsLiSyncAdapterProfileService : onPerformSync!!! *****************"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 100
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService$SyncAdapterImpl;->this$0:Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;

    # setter for: Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;->mAccount:Landroid/accounts/Account;
    invoke-static {v1, p1}, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;->access$102(Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;Landroid/accounts/Account;)Landroid/accounts/Account;

    .line 101
    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService$SyncAdapterImpl;->this$0:Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;

    # setter for: Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;->mAuthority:Ljava/lang/String;
    invoke-static {v1, p3}, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;->access$202(Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;Ljava/lang/String;)Ljava/lang/String;

    .line 102
    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService$SyncAdapterImpl;->this$0:Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;

    # setter for: Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;->mSyncResult:Landroid/content/SyncResult;
    invoke-static {v1, p5}, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;->access$302(Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;Landroid/content/SyncResult;)Landroid/content/SyncResult;

    .line 104
    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService$SyncAdapterImpl;->this$0:Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;

    # getter for: Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;->mBundleUpdatesQuery:Landroid/os/Bundle;
    invoke-static {v1}, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;->access$400(Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;)Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "count"

    const-string v3, "50"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService$SyncAdapterImpl;->this$0:Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;

    # getter for: Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;->mBundleUpdatesQuery:Landroid/os/Bundle;
    invoke-static {v1}, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;->access$400(Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;)Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "after"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService$SyncAdapterImpl;->this$0:Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;

    # getter for: Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;
    invoke-static {v4}, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;->access$500(Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;)Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;

    move-result-object v4

    sget-object v5, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$FeedList;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v4, v5}, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;->getLastUpdateTimeStamp(Landroid/net/Uri;)J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService$SyncAdapterImpl;->this$0:Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;

    invoke-virtual {v1}, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;->performSync()V
    :try_end_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 118
    :goto_0
    return-void

    .line 110
    :catch_0
    move-exception v0

    .line 111
    .local v0, "e":Landroid/accounts/OperationCanceledException;
    const-string v1, "SnsLiSync"

    const-string v2, "SnsLiSyncAdapterProfileService : onPerformSync is CANCELED!!!"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 112
    invoke-virtual {v0}, Landroid/accounts/OperationCanceledException;->printStackTrace()V

    goto :goto_0

    .line 113
    .end local v0    # "e":Landroid/accounts/OperationCanceledException;
    :catch_1
    move-exception v0

    .line 114
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "SnsLiSync"

    const-string v2, "SnsLiSyncAdapterProfileService : Abnormal Syncing!!!"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 115
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public onSyncCanceled()V
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService$SyncAdapterImpl;->this$0:Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;

    # invokes: Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;->onSyncCanceled()V
    invoke-static {v0}, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;->access$600(Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;)V

    .line 123
    return-void
.end method
