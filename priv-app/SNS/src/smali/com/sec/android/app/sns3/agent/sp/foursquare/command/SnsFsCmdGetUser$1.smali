.class Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdGetUser$1;
.super Lcom/sec/android/app/sns3/svc/sp/foursquare/request/SnsFsReqGetUser;
.source "SnsFsCmdGetUser.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdGetUser;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Handler;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdGetUser;


# direct methods
.method constructor <init>(Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdGetUser;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;)V
    .locals 0
    .param p2, "x0"    # Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    .param p3, "x1"    # Ljava/lang/String;

    .prologue
    .line 50
    iput-object p1, p0, Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdGetUser$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdGetUser;

    invoke-direct {p0, p2, p3}, Lcom/sec/android/app/sns3/svc/sp/foursquare/request/SnsFsReqGetUser;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public onReqRespond(IZIILandroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;)Z
    .locals 9
    .param p1, "reqID"    # I
    .param p2, "bSuccess"    # Z
    .param p3, "httpStatus"    # I
    .param p4, "errorCode"    # I
    .param p5, "reason"    # Landroid/os/Bundle;
    .param p6, "user"    # Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;

    .prologue
    const/4 v8, 0x1

    .line 55
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 56
    .local v2, "values":Landroid/content/ContentValues;
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/sns3/SnsApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 58
    .local v0, "cr":Landroid/content/ContentResolver;
    if-eqz p2, :cond_2

    .line 59
    if-eqz p6, :cond_1

    .line 60
    move-object v1, p6

    .line 61
    .local v1, "curUser":Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;
    sget-object v3, Lcom/sec/android/app/sns3/agent/sp/foursquare/db/SnsFourSquareDB$UserInfo;->CONTENT_URI:Landroid/net/Uri;

    const-string v4, "user_id=?"

    new-array v5, v8, [Ljava/lang/String;

    const/4 v6, 0x0

    iget-object v7, v1, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mUserID:Ljava/lang/String;

    aput-object v7, v5, v6

    invoke-virtual {v0, v3, v4, v5}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 65
    invoke-virtual {v2}, Landroid/content/ContentValues;->clear()V

    .line 66
    const-string v3, "user_id"

    iget-object v4, v1, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mUserID:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    const-string v3, "formatted_name"

    iget-object v4, v1, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mFormattedName:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    const-string v3, "first_name"

    iget-object v4, v1, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mUserFirstName:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    const-string v3, "last_name"

    iget-object v4, v1, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mUserLastName:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    const-string v3, "type"

    iget-object v4, v1, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mType:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    const-string v3, "about_me"

    iget-object v4, v1, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mAboutMe:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    const-string v3, "image_url"

    iget-object v4, v1, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mPhotoUrl:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    iget-object v3, v1, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mContact:Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseContact;

    if-eqz v3, :cond_0

    .line 74
    const-string v3, "twitter"

    iget-object v4, v1, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mContact:Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseContact;

    iget-object v4, v4, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseContact;->mTwitter:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    const-string v3, "facebook"

    iget-object v4, v1, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mContact:Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseContact;

    iget-object v4, v4, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseContact;->mFacebook:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    :cond_0
    sget-object v3, Lcom/sec/android/app/sns3/agent/sp/foursquare/db/SnsFourSquareDB$UserInfo;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v3, v2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 81
    .end local v1    # "curUser":Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdGetUser$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdGetUser;

    sget-object v4, Lcom/sec/android/app/sns3/agent/sp/foursquare/db/SnsFourSquareDB$UserInfo;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdGetUser;->setUri(Ljava/lang/String;)V

    .line 88
    :goto_0
    iget-object v3, p0, Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdGetUser$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdGetUser;

    invoke-virtual {v3, p2}, Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdGetUser;->setSuccess(Z)V

    .line 89
    iget-object v3, p0, Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdGetUser$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdGetUser;

    invoke-virtual {v3, p0}, Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdGetUser;->receive(Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;)V

    .line 91
    return v8

    .line 83
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdGetUser$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdGetUser;

    new-instance v4, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;

    const-string v5, "foursquare"

    invoke-direct {v4, v5, p3, p4, p5}, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;-><init>(Ljava/lang/String;IILandroid/os/Bundle;)V

    invoke-virtual {v3, v4}, Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdGetUser;->setResponseList(Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;)Z

    .line 85
    iget-object v3, p0, Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdGetUser$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdGetUser;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdGetUser;->setUri(Ljava/lang/String;)V

    goto :goto_0
.end method
