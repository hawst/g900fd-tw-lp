.class Lcom/sec/android/app/sns3/agent/sp/qzone/command/SnsQzCmdGetBirthday$1;
.super Lcom/sec/android/app/sns3/svc/sp/qzone/request/SnsQzReqGetBirthday;
.source "SnsQzCmdGetBirthday.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/sns3/agent/sp/qzone/command/SnsQzCmdGetBirthday;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Handler;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/sns3/agent/sp/qzone/command/SnsQzCmdGetBirthday;


# direct methods
.method constructor <init>(Lcom/sec/android/app/sns3/agent/sp/qzone/command/SnsQzCmdGetBirthday;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;)V
    .locals 0
    .param p2, "x0"    # Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    .param p3, "x1"    # Ljava/lang/String;

    .prologue
    .line 57
    iput-object p1, p0, Lcom/sec/android/app/sns3/agent/sp/qzone/command/SnsQzCmdGetBirthday$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/qzone/command/SnsQzCmdGetBirthday;

    invoke-direct {p0, p2, p3}, Lcom/sec/android/app/sns3/svc/sp/qzone/request/SnsQzReqGetBirthday;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public onReqRespond(IZIILandroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseBirthday;)Z
    .locals 8
    .param p1, "reqID"    # I
    .param p2, "bSuccess"    # Z
    .param p3, "httpStatus"    # I
    .param p4, "errorCode"    # I
    .param p5, "reason"    # Landroid/os/Bundle;
    .param p6, "birthday"    # Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseBirthday;

    .prologue
    const/4 v7, 0x0

    .line 63
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 64
    .local v3, "values":Landroid/content/ContentValues;
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/sns3/SnsApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 66
    .local v0, "cr":Landroid/content/ContentResolver;
    const/4 v1, 0x0

    .line 68
    .local v1, "createTime":Ljava/lang/String;
    if-eqz p2, :cond_1

    .line 69
    if-eqz p6, :cond_0

    .line 71
    sget-object v4, Lcom/sec/android/app/sns3/agent/sp/qzone/db/SnsQzoneDB$Birthday;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v4, v7, v7}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 73
    move-object v2, p6

    .line 74
    .local v2, "curBirthday":Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseBirthday;
    :goto_0
    if-eqz v2, :cond_0

    .line 76
    invoke-virtual {v3}, Landroid/content/ContentValues;->clear()V

    .line 78
    const-string v4, "friend_openid"

    iget-object v5, v2, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseBirthday;->mFriendOpenID:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    const-string v4, "host_openid"

    iget-object v5, v2, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseBirthday;->mHostOpenID:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    const-string v4, "gender"

    iget-object v5, v2, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseBirthday;->mGender:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    const-string v4, "month"

    iget-object v5, v2, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseBirthday;->mBirthdayMonth:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    const-string v4, "day"

    iget-object v5, v2, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseBirthday;->mBirthdayDay:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    const-string v4, "name"

    iget-object v5, v2, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseBirthday;->mNickName:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    const-string v4, "lunar_flag"

    iget-object v5, v2, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseBirthday;->mLunarFlag:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    const-string v4, "pic_url"

    iget-object v5, v2, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseBirthday;->mPicUrl:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    sget-object v4, Lcom/sec/android/app/sns3/agent/sp/qzone/db/SnsQzoneDB$Birthday;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v4, v3}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 99
    iget-object v2, v2, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseBirthday;->mNext:Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseBirthday;

    goto :goto_0

    .line 102
    .end local v2    # "curBirthday":Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseBirthday;
    :cond_0
    iget-object v4, p0, Lcom/sec/android/app/sns3/agent/sp/qzone/command/SnsQzCmdGetBirthday$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/qzone/command/SnsQzCmdGetBirthday;

    sget-object v5, Lcom/sec/android/app/sns3/agent/sp/qzone/db/SnsQzoneDB$Birthday;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v5}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/sns3/agent/sp/qzone/command/SnsQzCmdGetBirthday;->setUri(Ljava/lang/String;)V

    .line 109
    :goto_1
    iget-object v4, p0, Lcom/sec/android/app/sns3/agent/sp/qzone/command/SnsQzCmdGetBirthday$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/qzone/command/SnsQzCmdGetBirthday;

    invoke-virtual {v4, p2}, Lcom/sec/android/app/sns3/agent/sp/qzone/command/SnsQzCmdGetBirthday;->setSuccess(Z)V

    .line 110
    iget-object v4, p0, Lcom/sec/android/app/sns3/agent/sp/qzone/command/SnsQzCmdGetBirthday$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/qzone/command/SnsQzCmdGetBirthday;

    invoke-virtual {v4, p0}, Lcom/sec/android/app/sns3/agent/sp/qzone/command/SnsQzCmdGetBirthday;->receive(Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;)V

    .line 112
    const/4 v4, 0x1

    return v4

    .line 104
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/sns3/agent/sp/qzone/command/SnsQzCmdGetBirthday$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/qzone/command/SnsQzCmdGetBirthday;

    new-instance v5, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;

    const-string v6, "qzone"

    invoke-direct {v5, v6, p3, p4, p5}, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;-><init>(Ljava/lang/String;IILandroid/os/Bundle;)V

    invoke-virtual {v4, v5}, Lcom/sec/android/app/sns3/agent/sp/qzone/command/SnsQzCmdGetBirthday;->setResponseList(Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;)Z

    .line 106
    iget-object v4, p0, Lcom/sec/android/app/sns3/agent/sp/qzone/command/SnsQzCmdGetBirthday$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/qzone/command/SnsQzCmdGetBirthday;

    invoke-virtual {v4, v7}, Lcom/sec/android/app/sns3/agent/sp/qzone/command/SnsQzCmdGetBirthday;->setUri(Ljava/lang/String;)V

    goto :goto_1
.end method
