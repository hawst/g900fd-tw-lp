.class public abstract Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestBase;
.super Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
.source "SnsTwRequestBase.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "SnsTwReqBase"


# instance fields
.field protected mBodyBundle:Landroid/os/Bundle;

.field protected mHeaderBundle:Landroid/os/Bundle;

.field protected mHttpMethod:Ljava/lang/String;

.field protected mParams:Landroid/os/Bundle;

.field protected mReqParams:Landroid/os/Bundle;

.field protected mReqUri:Ljava/lang/String;

.field protected mSig:Ljava/lang/String;

.field protected mSubParams:Landroid/os/Bundle;

.field protected mToken:Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;

.field protected mUrl:Ljava/lang/String;


# direct methods
.method protected constructor <init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;I)V
    .locals 2
    .param p1, "svcMgr"    # Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    .param p2, "reqCategory"    # I

    .prologue
    .line 74
    const-string v0, "twitter"

    invoke-direct {p0, p1, v0, p2}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;I)V

    .line 76
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestBase;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/SnsSvcMgr;->getTokenMgr()Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;

    move-result-object v0

    const-string v1, "twitter"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;->getToken(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestBase;->mToken:Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;

    .line 77
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestBase;->mParams:Landroid/os/Bundle;

    .line 78
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestBase;->mSubParams:Landroid/os/Bundle;

    .line 80
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestBase;->mHeaderBundle:Landroid/os/Bundle;

    .line 81
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestBase;->mBodyBundle:Landroid/os/Bundle;

    .line 82
    return-void
.end method


# virtual methods
.method protected check(IILcom/sec/android/app/sns3/svc/sp/SnsSpResponse;)I
    .locals 6
    .param p1, "httpStatus"    # I
    .param p2, "prevErrorCode"    # I
    .param p3, "resp"    # Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;

    .prologue
    .line 125
    const/4 v1, -0x1

    .line 126
    .local v1, "errorCode":I
    const/4 v3, -0x1

    if-le p2, v3, :cond_1

    .line 127
    move v1, p2

    .line 148
    :cond_0
    :goto_0
    return v1

    .line 129
    :cond_1
    instance-of v3, p3, Lcom/sec/android/app/sns3/svc/sp/SnsSpResponseError;

    if-eqz v3, :cond_0

    move-object v2, p3

    .line 130
    check-cast v2, Lcom/sec/android/app/sns3/svc/sp/SnsSpResponseError;

    .line 131
    .local v2, "errorResp":Lcom/sec/android/app/sns3/svc/sp/SnsSpResponseError;
    invoke-virtual {v2}, Lcom/sec/android/app/sns3/svc/sp/SnsSpResponseError;->getErrorCode()I

    move-result v1

    .line 132
    invoke-virtual {v2}, Lcom/sec/android/app/sns3/svc/sp/SnsSpResponseError;->getErrorBundle()Landroid/os/Bundle;

    move-result-object v0

    .line 133
    .local v0, "errorBundle":Landroid/os/Bundle;
    const-string v3, "SnsTwReqBase"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[SnsLiReqBase] check. httpStatus: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " prevErrorCode: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " errorBundle: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 136
    if-eqz v0, :cond_0

    .line 137
    const-string v3, "89"

    const-string v4, "error_code"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 138
    const/16 v1, 0x7d1

    .line 139
    const-string v3, "SnsTwReqBase"

    const-string v4, "Invalid or expired token"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 140
    :cond_2
    const-string v3, "88"

    const-string v4, "error_code"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 141
    const/16 v1, 0x7d3

    .line 142
    const-string v3, "SnsTwReqBase"

    const-string v4, "Rate limit exceeded"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected compose()Lcom/sec/android/app/sns3/svc/http/SnsHttpRequest;
    .locals 8

    .prologue
    .line 86
    const-string v1, "SNS"

    const-string v2, "<SnsTwRequestBase> compose()"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 90
    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestBase;->mReqParams:Landroid/os/Bundle;

    iget-object v2, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestBase;->mHttpMethod:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestBase;->mBodyBundle:Landroid/os/Bundle;

    iget-object v4, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestBase;->mSubParams:Landroid/os/Bundle;

    invoke-static {v1, v2, v3, v4}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/composer/SnsTwComposerUtils;->setParamsToBundle(Landroid/os/Bundle;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;)V

    .line 92
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "https://api.twitter.com/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestBase;->setUrl()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestBase;->mReqUri:Ljava/lang/String;

    .line 94
    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestBase;->mSubParams:Landroid/os/Bundle;

    invoke-static {v1}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/composer/SnsTwComposerUtils;->createQueryString(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v7

    .line 97
    .local v7, "qs":Ljava/lang/String;
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestBase;->mParams:Landroid/os/Bundle;

    const-string v2, "oauth_consumer_key"

    sget-object v3, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwitter;->CONSUMER_KEY:Ljava/lang/String;

    invoke-static {v3}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->encodeUrlEx(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 99
    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestBase;->mParams:Landroid/os/Bundle;

    const-string v2, "oauth_signature_method"

    const-string v3, "HMAC-SHA1"

    invoke-static {v3}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->encodeUrlEx(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestBase;->mParams:Landroid/os/Bundle;

    const-string v2, "oauth_version"

    const-string v3, "1.0"

    invoke-static {v3}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->encodeUrlEx(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestBase;->mParams:Landroid/os/Bundle;

    const-string v2, "oauth_timestamp"

    invoke-static {}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->generateTimestamp()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->encodeUrlEx(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestBase;->mParams:Landroid/os/Bundle;

    const-string v2, "oauth_nonce"

    invoke-static {}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->generateNonce()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->encodeUrlEx(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestBase;->composeMore()V

    .line 110
    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestBase;->mParams:Landroid/os/Bundle;

    const-string v2, "oauth_signature"

    iget-object v3, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestBase;->mSig:Ljava/lang/String;

    invoke-static {v3}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->encodeUrlEx(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestBase;->mHeaderBundle:Landroid/os/Bundle;

    const-string v2, "Authorization"

    iget-object v3, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestBase;->mParams:Landroid/os/Bundle;

    invoke-static {v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/composer/SnsTwComposerUtils;->createTrucatedHeader(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    new-instance v0, Lcom/sec/android/app/sns3/svc/http/SnsHttpRequest;

    iget v1, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestBase;->mReqID:I

    iget-object v2, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestBase;->mHttpMethod:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestBase;->mReqUri:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestBase;->mHeaderBundle:Landroid/os/Bundle;

    iget-object v5, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestBase;->mBodyBundle:Landroid/os/Bundle;

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/sns3/svc/http/SnsHttpRequest;-><init>(ILjava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 120
    .local v0, "request":Lcom/sec/android/app/sns3/svc/http/SnsHttpRequest;
    :goto_0
    return-object v0

    .line 116
    .end local v0    # "request":Lcom/sec/android/app/sns3/svc/http/SnsHttpRequest;
    :catch_0
    move-exception v6

    .line 117
    .local v6, "e":Ljava/lang/NullPointerException;
    const/4 v0, 0x0

    .restart local v0    # "request":Lcom/sec/android/app/sns3/svc/http/SnsHttpRequest;
    goto :goto_0
.end method

.method protected abstract composeMore()V
.end method

.method protected parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;
    .locals 12
    .param p1, "content"    # Ljava/lang/String;

    .prologue
    const/4 v8, 0x0

    .line 154
    const/4 v3, -0x1

    .line 155
    .local v3, "errorCode":I
    const/4 v5, 0x0

    .line 156
    .local v5, "errorMsg":Ljava/lang/String;
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 159
    .local v2, "errorBundle":Landroid/os/Bundle;
    if-eqz p1, :cond_0

    :try_start_0
    const-string v9, "true"

    invoke-virtual {p1, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_0

    const-string v9, "false"

    invoke-virtual {p1, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_0

    invoke-static {p1}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->isJsonArray(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 194
    :cond_0
    :goto_0
    return-object v8

    .line 163
    :cond_1
    new-instance v7, Lorg/json/JSONObject;

    invoke-direct {v7, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 165
    .local v7, "jsonObj":Lorg/json/JSONObject;
    const-string v9, "error"

    invoke-virtual {v7, v9}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 166
    const-string v9, "error"

    invoke-virtual {v7, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v5

    .line 167
    const/16 v3, 0x3e8

    .line 191
    .end local v7    # "jsonObj":Lorg/json/JSONObject;
    :cond_2
    :goto_1
    const/16 v9, 0x3e8

    if-ne v3, v9, :cond_0

    .line 192
    new-instance v8, Lcom/sec/android/app/sns3/svc/sp/SnsSpResponseError;

    invoke-direct {v8, v3, v5, v2}, Lcom/sec/android/app/sns3/svc/sp/SnsSpResponseError;-><init>(ILjava/lang/String;Landroid/os/Bundle;)V

    goto :goto_0

    .line 168
    .restart local v7    # "jsonObj":Lorg/json/JSONObject;
    :cond_3
    :try_start_1
    const-string v9, "errors"

    invoke-virtual {v7, v9}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 169
    const-string v9, "errors"

    invoke-virtual {v7, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 170
    .local v4, "errorContent":Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->isJsonArray(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 171
    new-instance v1, Lorg/json/JSONArray;

    const-string v9, "errors"

    invoke-virtual {v7, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v1, v9}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 172
    .local v1, "errorArray":Lorg/json/JSONArray;
    if-eqz v1, :cond_5

    .line 173
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_2
    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v9

    if-ge v6, v9, :cond_5

    .line 174
    invoke-virtual {v1, v6}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v9

    const-string v10, "message"

    invoke-virtual {v9, v10}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 175
    const-string v9, "error_code"

    invoke-virtual {v1, v6}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v10

    const-string v11, "code"

    invoke-virtual {v10, v11}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v2, v9, v10}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 173
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    .line 180
    .end local v1    # "errorArray":Lorg/json/JSONArray;
    .end local v6    # "i":I
    :cond_4
    move-object v5, v4

    .line 182
    :cond_5
    const/16 v3, 0x3e8

    goto :goto_1

    .line 185
    .end local v4    # "errorContent":Ljava/lang/String;
    .end local v7    # "jsonObj":Lorg/json/JSONObject;
    :catch_0
    move-exception v0

    .line 186
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_1

    .line 187
    .end local v0    # "e":Lorg/json/JSONException;
    :catch_1
    move-exception v0

    .line 188
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method protected restoreToken()V
    .locals 12

    .prologue
    const/4 v9, 0x1

    const/4 v10, 0x0

    .line 199
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v0

    .line 200
    .local v0, "context":Landroid/content/Context;
    const-string v8, "notification"

    invoke-virtual {v0, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/NotificationManager;

    .line 203
    .local v5, "notiMgr":Landroid/app/NotificationManager;
    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4}, Landroid/content/Intent;-><init>()V

    .line 205
    .local v4, "notiIntent":Landroid/content/Intent;
    const-string v8, "com.sec.android.app.sns3.RETRY_SSO_TWITTER"

    invoke-virtual {v4, v8}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 209
    const/high16 v8, 0x800000

    invoke-virtual {v4, v8}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 210
    const-string v8, "RetryLogin"

    invoke-virtual {v4, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 211
    invoke-static {v0, v10, v4, v10}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    .line 213
    .local v2, "launchIntent":Landroid/app/PendingIntent;
    const v7, 0x7f08003f

    .line 214
    .local v7, "titleID":I
    const v6, 0x7f08006d

    .line 215
    .local v6, "spID":I
    const v1, 0x7f02001f

    .line 217
    .local v1, "iconID":I
    new-instance v3, Landroid/app/Notification$Builder;

    invoke-direct {v3, v0}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    .line 218
    .local v3, "notiBuilder":Landroid/app/Notification$Builder;
    new-array v8, v9, [Ljava/lang/Object;

    invoke-virtual {v0, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v10

    invoke-virtual {v0, v7, v8}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v8}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v8

    invoke-virtual {v8, v1}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v8

    const v9, 0x7f08003e

    invoke-virtual {v0, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v8

    invoke-virtual {v8, v2}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v8

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    invoke-virtual {v8, v10, v11}, Landroid/app/Notification$Builder;->setWhen(J)Landroid/app/Notification$Builder;

    .line 224
    const/16 v8, 0x8fc

    invoke-virtual {v3}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v9

    invoke-virtual {v5, v8, v9}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 225
    return-void
.end method

.method protected abstract setUrl()Ljava/lang/String;
.end method
