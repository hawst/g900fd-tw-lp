.class public abstract Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;
.super Ljava/lang/Object;
.source "AbstractSnsCommand.java"


# static fields
.field public static final INVALID_ID:I = -0x1

.field private static mCommandID_Base:I


# instance fields
.field private mCommandCallback:Lcom/sec/android/app/sns3/agent/command/ISnsCommandCallback;

.field private mCommandHandler:Landroid/os/Handler;

.field private mCommandID:I

.field private mCurrentUnit:Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;

.field private mResponseList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;",
            ">;"
        }
    .end annotation
.end field

.field private mUnitList:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;",
            ">;"
        }
    .end annotation
.end field

.field private mUri:Ljava/lang/String;

.field private mbSuccess:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const/4 v0, -0x1

    sput v0, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->mCommandID_Base:I

    return-void
.end method

.method public constructor <init>(Landroid/os/Handler;)V
    .locals 1
    .param p1, "cmdHandler"    # Landroid/os/Handler;

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->mCommandID:I

    .line 55
    sget v0, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->mCommandID_Base:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->mCommandID_Base:I

    .line 56
    sget v0, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->mCommandID_Base:I

    iput v0, p0, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->mCommandID:I

    .line 58
    iput-object p1, p0, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->mCommandHandler:Landroid/os/Handler;

    .line 60
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->mUnitList:Ljava/util/Map;

    .line 61
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->mResponseList:Ljava/util/List;

    .line 62
    return-void
.end method


# virtual methods
.method protected addCommandUnit(Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;)V
    .locals 3
    .param p1, "unit"    # Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;

    .prologue
    .line 111
    iget-object v1, p0, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->mUnitList:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v0

    .line 112
    .local v0, "index":I
    invoke-virtual {p1, v0}, Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;->setIndex(I)V

    .line 114
    iget-object v1, p0, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->mUnitList:Ljava/util/Map;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 115
    return-void
.end method

.method public cancel()Z
    .locals 5

    .prologue
    .line 90
    iget-object v3, p0, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->mCurrentUnit:Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;

    invoke-virtual {v3}, Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;->getRequestMap()Ljava/util/Map;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 91
    .local v0, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    const/4 v2, 0x0

    .line 92
    .local v2, "req":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    const/4 v1, 0x0

    .line 94
    .local v1, "msg":Landroid/os/Message;
    :goto_0
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 95
    iget-object v3, p0, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->mCurrentUnit:Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;

    invoke-virtual {v3}, Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;->getRequestMap()Ljava/util/Map;

    move-result-object v3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "req":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    check-cast v2, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;

    .line 96
    .restart local v2    # "req":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    invoke-virtual {v2}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->abort()Z

    .line 98
    iget-object v3, p0, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->mCommandHandler:Landroid/os/Handler;

    const/16 v4, 0xb

    invoke-virtual {v3, v4, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    .line 99
    iget-object v3, p0, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->mCommandHandler:Landroid/os/Handler;

    invoke-virtual {v3, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    .line 102
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->mCurrentUnit:Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;

    const/4 v4, 0x2

    invoke-virtual {v3, v4}, Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;->setState(I)V

    .line 103
    iget-object v3, p0, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->mUnitList:Ljava/util/Map;

    iget-object v4, p0, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->mCurrentUnit:Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;

    invoke-virtual {v4}, Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;->getIndex()I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 105
    const/4 v3, 0x1

    return v3
.end method

.method public checkCurrentUnitState()V
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->mCurrentUnit:Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;->receiveReqResponse()V

    .line 86
    return-void
.end method

.method public getCommandCallback()Lcom/sec/android/app/sns3/agent/command/ISnsCommandCallback;
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->mCommandCallback:Lcom/sec/android/app/sns3/agent/command/ISnsCommandCallback;

    return-object v0
.end method

.method public getCommandID()I
    .locals 1

    .prologue
    .line 133
    iget v0, p0, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->mCommandID:I

    return v0
.end method

.method protected getCurrentCommandUnit()Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;
    .locals 2

    .prologue
    .line 118
    iget-object v0, p0, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->mCurrentUnit:Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;

    if-nez v0, :cond_0

    .line 119
    iget-object v0, p0, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->mUnitList:Ljava/util/Map;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;

    iput-object v0, p0, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->mCurrentUnit:Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;

    .line 121
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->mCurrentUnit:Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;

    return-object v0
.end method

.method protected getNextCommandUnit(I)Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;
    .locals 2
    .param p1, "unitIndex"    # I

    .prologue
    .line 129
    iget-object v0, p0, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->mUnitList:Ljava/util/Map;

    add-int/lit8 v1, p1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;

    return-object v0
.end method

.method public getResponseList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;",
            ">;"
        }
    .end annotation

    .prologue
    .line 158
    iget-object v0, p0, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->mResponseList:Ljava/util/List;

    return-object v0
.end method

.method public getUri()Ljava/lang/String;
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->mUri:Ljava/lang/String;

    return-object v0
.end method

.method public isSuccess()Z
    .locals 1

    .prologue
    .line 142
    iget-boolean v0, p0, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->mbSuccess:Z

    return v0
.end method

.method public receive(Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;)V
    .locals 3
    .param p1, "req"    # Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;

    .prologue
    .line 80
    iget-object v1, p0, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->mCommandHandler:Landroid/os/Handler;

    const/16 v2, 0xb

    invoke-virtual {v1, v2, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 81
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->mCommandHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 82
    return-void
.end method

.method protected abstract respond()Z
.end method

.method public send()I
    .locals 3

    .prologue
    .line 68
    iget-object v1, p0, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->mCommandCallback:Lcom/sec/android/app/sns3/agent/command/ISnsCommandCallback;

    if-nez v1, :cond_0

    .line 69
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "mCommandCallback is NULL."

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 71
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->mUnitList:Ljava/util/Map;

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;

    iput-object v1, p0, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->mCurrentUnit:Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;

    .line 73
    iget-object v1, p0, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->mCommandHandler:Landroid/os/Handler;

    const/16 v2, 0xa

    invoke-virtual {v1, v2, p0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 74
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->mCommandHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 76
    iget v1, p0, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->mCommandID:I

    return v1
.end method

.method public setCommandCallback(Lcom/sec/android/app/sns3/agent/command/ISnsCommandCallback;)Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;
    .locals 0
    .param p1, "callback"    # Lcom/sec/android/app/sns3/agent/command/ISnsCommandCallback;

    .prologue
    .line 166
    iput-object p1, p0, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->mCommandCallback:Lcom/sec/android/app/sns3/agent/command/ISnsCommandCallback;

    .line 168
    return-object p0
.end method

.method protected setCurrentCommandUnit(Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;)V
    .locals 0
    .param p1, "curUnit"    # Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;

    .prologue
    .line 125
    iput-object p1, p0, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->mCurrentUnit:Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;

    .line 126
    return-void
.end method

.method public setResponseList(Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;)Z
    .locals 1
    .param p1, "cmdResponse"    # Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;

    .prologue
    .line 154
    iget-object v0, p0, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->mResponseList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public setSuccess(Z)V
    .locals 0
    .param p1, "bSuccess"    # Z

    .prologue
    .line 138
    iput-boolean p1, p0, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->mbSuccess:Z

    .line 139
    return-void
.end method

.method public setUri(Ljava/lang/String;)V
    .locals 0
    .param p1, "uri"    # Ljava/lang/String;

    .prologue
    .line 146
    iput-object p1, p0, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->mUri:Ljava/lang/String;

    .line 147
    return-void
.end method
