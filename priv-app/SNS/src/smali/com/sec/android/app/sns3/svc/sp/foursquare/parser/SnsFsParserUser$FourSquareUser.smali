.class public interface abstract Lcom/sec/android/app/sns3/svc/sp/foursquare/parser/SnsFsParserUser$FourSquareUser;
.super Ljava/lang/Object;
.source "SnsFsParserUser.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/svc/sp/foursquare/parser/SnsFsParserUser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "FourSquareUser"
.end annotation


# static fields
.field public static final ABOUT_ME:Ljava/lang/String; = "bio"

.field public static final CODE:Ljava/lang/String; = "code"

.field public static final COMMENTS:Ljava/lang/String; = "comments"

.field public static final CONTACT:Ljava/lang/String; = "contact"

.field public static final FRIENDS:Ljava/lang/String; = "friends"

.field public static final GENDER:Ljava/lang/String; = "gender"

.field public static final HOME_CITY:Ljava/lang/String; = "homeCity"

.field public static final LIKES:Ljava/lang/String; = "likes"

.field public static final META:Ljava/lang/String; = "meta"

.field public static final PHOTO_URL:Ljava/lang/String; = "photo"

.field public static final PHOTO_URL_PREFIX:Ljava/lang/String; = "prefix"

.field public static final PHOTO_URL_SUFFIX:Ljava/lang/String; = "suffix"

.field public static final RELATIONSHIP:Ljava/lang/String; = "relationship"

.field public static final RESPONSE:Ljava/lang/String; = "response"

.field public static final TYPE:Ljava/lang/String; = "type"

.field public static final USER:Ljava/lang/String; = "user"

.field public static final USER_FIRST_NAME:Ljava/lang/String; = "firstName"

.field public static final USER_ID:Ljava/lang/String; = "id"

.field public static final USER_LAST_NAME:Ljava/lang/String; = "lastName"
