.class public Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;
.super Landroid/accounts/AccountAuthenticatorActivity;
.source "SnsAccountGpAuthSSOActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity$GetAuthTokenTask;
    }
.end annotation


# static fields
.field private static final KEY_SUPPRESS_PROGRESS_SCREEN:Ljava/lang/String; = "suppressProgressScreen"

.field private static final REQUEST_CODE_PICK_ACCOUNT:I = 0x3e9

.field private static final REQUEST_CODE_RESOLVE_ERROR:I = 0x3ea

.field private static final REQUEST_SYNC_INTERVAL:I = 0x3e8

.field private static final TAG:Ljava/lang/String; = "SnsAccountGpAuthSSOActivity"

.field private static final TIME_TO_WAIT_CONSENT_PAGE:I = 0xc8

.field private static final TIME_TO_WAIT_TOKEN:I = 0x7530

.field private static mInstanceCount:I


# instance fields
.field private mAccountManagerCallback:Landroid/accounts/AccountManagerCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/accounts/AccountManagerCallback",
            "<",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation
.end field

.field private mAppContext:Landroid/content/Context;

.field private mCheckStatusRunnable:Ljava/lang/Runnable;

.field private mConnectionProgressDialog:Landroid/app/ProgressDialog;

.field private mConsentPageShown:Z

.field private mGoogleAccountCredential:Landroid/accounts/AccountManagerFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/accounts/AccountManagerFuture",
            "<",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation
.end field

.field private mHandler:Landroid/os/Handler;

.field private mRetryLogin:Z

.field private mSelectedAccount:Landroid/accounts/Account;

.field private mSkipSyncSetup:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 74
    const/4 v0, 0x0

    sput v0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->mInstanceCount:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 57
    invoke-direct {p0}, Landroid/accounts/AccountAuthenticatorActivity;-><init>()V

    .line 75
    iput-boolean v1, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->mSkipSyncSetup:Z

    .line 76
    iput-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->mSelectedAccount:Landroid/accounts/Account;

    .line 77
    iput-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->mGoogleAccountCredential:Landroid/accounts/AccountManagerFuture;

    .line 78
    iput-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->mHandler:Landroid/os/Handler;

    .line 79
    iput-boolean v1, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->mConsentPageShown:Z

    .line 81
    new-instance v0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity$1;-><init>(Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->mAccountManagerCallback:Landroid/accounts/AccountManagerCallback;

    .line 140
    new-instance v0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity$2;-><init>(Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->mCheckStatusRunnable:Ljava/lang/Runnable;

    .line 300
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;

    .prologue
    .line 57
    iget-boolean v0, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->mSkipSyncSetup:Z

    return v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;

    .prologue
    .line 57
    iget-boolean v0, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->mRetryLogin:Z

    return v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;

    .prologue
    .line 57
    invoke-direct {p0}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->loginFail()V

    return-void
.end method

.method static synthetic access$1100(Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 57
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->loginSuccess(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;

    .prologue
    .line 57
    iget-boolean v0, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->mConsentPageShown:Z

    return v0
.end method

.method static synthetic access$202(Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 57
    iput-boolean p1, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->mConsentPageShown:Z

    return p1
.end method

.method static synthetic access$400(Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 57
    invoke-direct {p0, p1}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->showProgressDialog(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;)Landroid/accounts/AccountManagerFuture;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->mGoogleAccountCredential:Landroid/accounts/AccountManagerFuture;

    return-object v0
.end method

.method static synthetic access$502(Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;Landroid/accounts/AccountManagerFuture;)Landroid/accounts/AccountManagerFuture;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;
    .param p1, "x1"    # Landroid/accounts/AccountManagerFuture;

    .prologue
    .line 57
    iput-object p1, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->mGoogleAccountCredential:Landroid/accounts/AccountManagerFuture;

    return-object p1
.end method

.method static synthetic access$600(Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;)Landroid/accounts/Account;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->mSelectedAccount:Landroid/accounts/Account;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->mAppContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Ljava/lang/String;

    .prologue
    .line 57
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->setAuthTokenNExpires(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;

    .prologue
    .line 57
    invoke-direct {p0}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->getUserInfo()V

    return-void
.end method

.method private getAccountByTypeAndName(Ljava/lang/String;Ljava/lang/String;)Landroid/accounts/Account;
    .locals 7
    .param p1, "type"    # Ljava/lang/String;
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 281
    iget-object v6, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->mAppContext:Landroid/content/Context;

    invoke-static {v6}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v6

    invoke-virtual {v6, p1}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v1

    .line 282
    .local v1, "accounts":[Landroid/accounts/Account;
    const/4 v5, 0x0

    .line 283
    .local v5, "selectedAccount":Landroid/accounts/Account;
    move-object v2, v1

    .local v2, "arr$":[Landroid/accounts/Account;
    array-length v4, v2

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_1

    aget-object v0, v2, v3

    .line 284
    .local v0, "account":Landroid/accounts/Account;
    iget-object v6, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v6, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 285
    move-object v5, v0

    .line 283
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 289
    .end local v0    # "account":Landroid/accounts/Account;
    :cond_1
    return-object v5
.end method

.method private getGoogleAuthToken()V
    .locals 2

    .prologue
    .line 293
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->mSelectedAccount:Landroid/accounts/Account;

    if-nez v0, :cond_0

    .line 294
    invoke-direct {p0}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->pickUserAccount()V

    .line 298
    :goto_0
    return-void

    .line 296
    :cond_0
    new-instance v0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity$GetAuthTokenTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity$GetAuthTokenTask;-><init>(Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity$1;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity$GetAuthTokenTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method private getUserInfo()V
    .locals 4

    .prologue
    .line 426
    new-instance v1, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity$4;

    invoke-direct {v1, p0}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity$4;-><init>(Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;)V

    invoke-virtual {p0, v1}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 433
    new-instance v0, Lcom/sec/android/app/sns3/agent/sp/googleplus/command/SnsGpCmdAuthLogin;

    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/sns3/SnsApplication;->getSvcMgr()Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v1

    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/sns3/SnsApplication;->getAgentMgr()Lcom/sec/android/app/sns3/agent/SnsAgentMgr;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/sns3/agent/SnsAgentMgr;->getCommandMgr()Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;->getCommandMgrHandle()Landroid/os/Handler;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/sns3/agent/sp/googleplus/command/SnsGpCmdAuthLogin;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Handler;Landroid/os/Bundle;)V

    .line 437
    .local v0, "cmd":Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;
    new-instance v1, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity$5;

    invoke-direct {v1, p0}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity$5;-><init>(Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->setCommandCallback(Lcom/sec/android/app/sns3/agent/command/ISnsCommandCallback;)Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;

    .line 473
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->send()I

    .line 474
    return-void
.end method

.method private hideProgressDialog()V
    .locals 1

    .prologue
    .line 265
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->mConnectionProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    .line 266
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->mConnectionProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 267
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->mConnectionProgressDialog:Landroid/app/ProgressDialog;

    .line 269
    :cond_0
    return-void
.end method

.method public static isRunning()Z
    .locals 1

    .prologue
    .line 477
    sget v0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->mInstanceCount:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private loginFail()V
    .locals 1

    .prologue
    .line 378
    new-instance v0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity$3;-><init>(Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 387
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->finish()V

    .line 388
    return-void
.end method

.method private loginSuccess(Ljava/lang/String;Ljava/lang/String;)V
    .locals 10
    .param p1, "userName"    # Ljava/lang/String;
    .param p2, "id"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 391
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 392
    .local v4, "options":Landroid/os/Bundle;
    invoke-static {}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->isLoggable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 393
    const-string v0, "SnsAccountGpAuthSSOActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SnsAccountFbSSOAuthActivity : addAccount() :  userName = "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 396
    :cond_0
    const-string v0, "username"

    invoke-virtual {v4, v0, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 398
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->mAppContext:Landroid/content/Context;

    if-nez v0, :cond_1

    .line 399
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    const-string v1, "com.sec.android.app.sns3.googleplus"

    move-object v3, v2

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Landroid/accounts/AccountManager;->addAccount(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Landroid/os/Bundle;Landroid/app/Activity;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    .line 406
    :goto_0
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/SnsApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    .line 407
    .local v8, "cr":Landroid/content/ContentResolver;
    new-instance v9, Landroid/content/ContentValues;

    invoke-direct {v9}, Landroid/content/ContentValues;-><init>()V

    .line 408
    .local v9, "values":Landroid/content/ContentValues;
    invoke-virtual {v9}, Landroid/content/ContentValues;->clear()V

    .line 409
    const-string v0, "user_id"

    invoke-virtual {v9, v0, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 410
    const-string v0, "user_name"

    invoke-virtual {v9, v0, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 411
    sget-object v0, Lcom/sec/android/app/sns3/agent/sp/googleplus/db/SnsGooglePlusDB$UserGpBasicInfo;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v8, v0, v9}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 412
    return-void

    .line 402
    .end local v8    # "cr":Landroid/content/ContentResolver;
    .end local v9    # "values":Landroid/content/ContentValues;
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->mAppContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    const-string v1, "com.sec.android.app.sns3.googleplus"

    iget-object v6, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->mAccountManagerCallback:Landroid/accounts/AccountManagerCallback;

    move-object v3, v2

    move-object v5, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Landroid/accounts/AccountManager;->addAccount(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Landroid/os/Bundle;Landroid/app/Activity;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    goto :goto_0
.end method

.method private pickUserAccount()V
    .locals 9

    .prologue
    const/4 v3, 0x0

    const/4 v0, 0x0

    .line 272
    const/4 v1, 0x1

    new-array v2, v1, [Ljava/lang/String;

    const-string v1, "com.google"

    aput-object v1, v2, v3

    .local v2, "accountTypes":[Ljava/lang/String;
    move-object v1, v0

    move-object v4, v0

    move-object v5, v0

    move-object v6, v0

    move-object v7, v0

    .line 275
    invoke-static/range {v0 .. v7}, Landroid/accounts/AccountManager;->newChooseAccountIntent(Landroid/accounts/Account;Ljava/util/ArrayList;[Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;[Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v8

    .line 277
    .local v8, "intent":Landroid/content/Intent;
    const/16 v0, 0x3e9

    invoke-virtual {p0, v8, v0}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 278
    return-void
.end method

.method private setAuthTokenNExpires(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3
    .param p1, "accessToken"    # Ljava/lang/String;
    .param p2, "refreshToken"    # Ljava/lang/String;
    .param p3, "accountName"    # Ljava/lang/String;

    .prologue
    .line 415
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 416
    :cond_0
    const/4 v1, 0x0

    .line 422
    :goto_0
    return v1

    .line 418
    :cond_1
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/sns3/SnsApplication;->getSvcMgr()Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/sns3/svc/SnsSvcMgr;->getTokenMgr()Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;

    move-result-object v1

    const-string v2, "googleplus"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;->getToken(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpToken;

    .line 420
    .local v0, "gpToken":Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpToken;
    invoke-virtual {v0, p3}, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpToken;->setAccountName(Ljava/lang/String;)V

    .line 421
    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpToken;->setAccessTokenNExpires(Ljava/lang/String;Ljava/lang/String;)V

    .line 422
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private showProgressDialog(Ljava/lang/String;)V
    .locals 2
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 256
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->mConnectionProgressDialog:Landroid/app/ProgressDialog;

    if-nez v0, :cond_0

    .line 257
    new-instance v0, Landroid/app/ProgressDialog;

    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->mAppContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->mConnectionProgressDialog:Landroid/app/ProgressDialog;

    .line 259
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->mConnectionProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0, p1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 260
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->mConnectionProgressDialog:Landroid/app/ProgressDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 261
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->mConnectionProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 262
    return-void
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 6
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v1, -0x1

    .line 220
    packed-switch p1, :pswitch_data_0

    .line 250
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->finish()V

    .line 253
    :cond_0
    :goto_0
    return-void

    .line 222
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->finish()V

    goto :goto_0

    .line 226
    :pswitch_1
    if-ne p2, v1, :cond_1

    .line 227
    if-eqz p3, :cond_0

    .line 228
    const-string v1, "authAccount"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 229
    .local v0, "email":Ljava/lang/String;
    const-string v1, "com.google"

    invoke-direct {p0, v1, v0}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->getAccountByTypeAndName(Ljava/lang/String;Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->mSelectedAccount:Landroid/accounts/Account;

    .line 231
    invoke-direct {p0}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->getGoogleAuthToken()V

    goto :goto_0

    .line 233
    .end local v0    # "email":Ljava/lang/String;
    :cond_1
    if-nez p2, :cond_0

    .line 234
    const-string v1, "SnsAccountGpAuthSSOActivity"

    const-string v2, "No account selected"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 235
    invoke-direct {p0}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->hideProgressDialog()V

    .line 236
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->finish()V

    goto :goto_0

    .line 241
    :pswitch_2
    if-ne p2, v1, :cond_2

    .line 242
    invoke-direct {p0}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->getGoogleAuthToken()V

    goto :goto_0

    .line 243
    :cond_2
    if-nez p2, :cond_0

    .line 245
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->mCheckStatusRunnable:Ljava/lang/Runnable;

    const-wide/16 v4, 0xc8

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 220
    :pswitch_data_0
    .packed-switch 0x3e8
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1, "icicle"    # Landroid/os/Bundle;

    .prologue
    const v7, 0x7f080023

    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 152
    const v2, 0x103006e

    invoke-virtual {p0, v2}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->setTheme(I)V

    .line 154
    invoke-super {p0, p1}, Landroid/accounts/AccountAuthenticatorActivity;->onCreate(Landroid/os/Bundle;)V

    .line 156
    sget v2, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->mInstanceCount:I

    add-int/lit8 v2, v2, 0x1

    sput v2, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->mInstanceCount:I

    .line 157
    sget v2, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->mInstanceCount:I

    if-le v2, v5, :cond_0

    .line 158
    const-string v2, "SnsAccountGpAuthSSOActivity"

    const-string v3, "multi instance"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 161
    :cond_0
    if-eqz p1, :cond_1

    .line 162
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->finish()V

    .line 190
    :goto_0
    return-void

    .line 166
    :cond_1
    iput-object p0, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->mAppContext:Landroid/content/Context;

    .line 167
    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    iput-object v2, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->mHandler:Landroid/os/Handler;

    .line 169
    const v2, 0x7f080046

    new-array v3, v5, [Ljava/lang/Object;

    invoke-virtual {p0, v7}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {p0, v2, v3}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 171
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "RetryLogin"

    invoke-virtual {v2, v3, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->mRetryLogin:Z

    .line 172
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "skip_sync_setup"

    invoke-virtual {v2, v3, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->mSkipSyncSetup:Z

    .line 174
    iget-object v2, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->mAppContext:Landroid/content/Context;

    invoke-static {v2}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v2

    const-string v3, "com.sec.android.app.sns3.googleplus"

    invoke-virtual {v2, v3}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 177
    .local v0, "accounts":[Landroid/accounts/Account;
    array-length v2, v0

    if-lez v2, :cond_2

    iget-boolean v2, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->mRetryLogin:Z

    if-nez v2, :cond_2

    .line 178
    iget-object v2, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->mAppContext:Landroid/content/Context;

    const/high16 v3, 0x7f080000

    new-array v4, v5, [Ljava/lang/Object;

    invoke-virtual {p0, v7}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {p0, v3, v4}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 181
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->finish()V

    goto :goto_0

    .line 185
    :cond_2
    const-string v2, "notification"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/NotificationManager;

    .line 186
    .local v1, "notiMgr":Landroid/app/NotificationManager;
    const/16 v2, 0x13ec

    invoke-virtual {v1, v2}, Landroid/app/NotificationManager;->cancel(I)V

    .line 187
    const/16 v2, 0x14b4

    invoke-virtual {v1, v2}, Landroid/app/NotificationManager;->cancel(I)V

    .line 189
    invoke-direct {p0}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->pickUserAccount()V

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 212
    invoke-direct {p0}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->hideProgressDialog()V

    .line 214
    sget v0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->mInstanceCount:I

    add-int/lit8 v0, v0, -0x1

    sput v0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->mInstanceCount:I

    .line 215
    invoke-super {p0}, Landroid/accounts/AccountAuthenticatorActivity;->onDestroy()V

    .line 216
    return-void
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 203
    invoke-super {p0}, Landroid/accounts/AccountAuthenticatorActivity;->onPause()V

    .line 205
    iget-boolean v0, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->mConsentPageShown:Z

    if-eqz v0, :cond_0

    .line 206
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->mCheckStatusRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 208
    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 4

    .prologue
    .line 194
    invoke-super {p0}, Landroid/accounts/AccountAuthenticatorActivity;->onResume()V

    .line 196
    iget-boolean v0, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->mConsentPageShown:Z

    if-eqz v0, :cond_0

    .line 197
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->mCheckStatusRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 199
    :cond_0
    return-void
.end method
