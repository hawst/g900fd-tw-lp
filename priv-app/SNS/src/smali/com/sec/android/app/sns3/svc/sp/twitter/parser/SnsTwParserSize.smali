.class public Lcom/sec/android/app/sns3/svc/sp/twitter/parser/SnsTwParserSize;
.super Ljava/lang/Object;
.source "SnsTwParserSize.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseSize;
    .locals 4
    .param p0, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 31
    invoke-static {p0}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->isJsonEmpty(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 32
    const/4 v2, 0x0

    .line 47
    :goto_0
    return-object v2

    .line 35
    :cond_0
    new-instance v2, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseSize;

    invoke-direct {v2}, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseSize;-><init>()V

    .line 37
    .local v2, "response":Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseSize;
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 39
    .local v1, "jsonObj":Lorg/json/JSONObject;
    const-string v3, "w"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseSize;->mW:Ljava/lang/String;

    .line 40
    const-string v3, "resize"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v3

    iput-boolean v3, v2, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseSize;->mResize:Z

    .line 41
    const-string v3, "h"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseSize;->mH:Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 43
    .end local v1    # "jsonObj":Lorg/json/JSONObject;
    :catch_0
    move-exception v0

    .line 44
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method
