.class public final Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseProfile;
.super Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;
.source "SnsInResponseProfile.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseProfile;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public mBio:Ljava/lang/String;

.field public mFullName:Ljava/lang/String;

.field public mImageUrl:Ljava/lang/String;

.field public mNext:Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseProfile;

.field public mUserID:Ljava/lang/String;

.field public mUserName:Ljava/lang/String;

.field public mWebsite:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseProfile$1;

    invoke-direct {v0}, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseProfile$1;-><init>()V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseProfile;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;-><init>()V

    .line 54
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;-><init>()V

    .line 57
    invoke-virtual {p0, p1}, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseProfile;->readFromParcel(Landroid/os/Parcel;)V

    .line 58
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseProfile$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseProfile$1;

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseProfile;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 84
    const/4 v0, 0x0

    return v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 72
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseProfile;->mUserID:Ljava/lang/String;

    .line 73
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseProfile;->mUserName:Ljava/lang/String;

    .line 74
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseProfile;->mImageUrl:Ljava/lang/String;

    .line 75
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseProfile;->mFullName:Ljava/lang/String;

    .line 76
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseProfile;->mBio:Ljava/lang/String;

    .line 77
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseProfile;->mWebsite:Ljava/lang/String;

    .line 78
    const-class v0, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseProfile;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseProfile;

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseProfile;->mNext:Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseProfile;

    .line 80
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flag"    # I

    .prologue
    .line 62
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseProfile;->mUserID:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 63
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseProfile;->mUserName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 64
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseProfile;->mImageUrl:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 65
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseProfile;->mFullName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 66
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseProfile;->mBio:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 67
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseProfile;->mWebsite:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 68
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseProfile;->mNext:Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseProfile;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 69
    return-void
.end method
