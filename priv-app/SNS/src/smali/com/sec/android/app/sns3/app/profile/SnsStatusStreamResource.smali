.class public interface abstract Lcom/sec/android/app/sns3/app/profile/SnsStatusStreamResource;
.super Ljava/lang/Object;
.source "SnsStatusStreamResource.java"


# static fields
.field public static final ACTION_CHECK_CONTACTS:Ljava/lang/String; = "com.sec.android.app.sns.profile.ACTION_CHECK_CONTACTS"

.field public static final ACTION_CHECK_OLD_DATA:Ljava/lang/String; = "com.sec.android.app.sns.profile.ACTION_CHECK_OLD_DATA"

.field public static final ACTION_CHECK_START_ALARM:Ljava/lang/String; = "com.sec.android.app.sns.profile.ACTION_CHECK_START_ALARM"

.field public static final ACTION_DELETE_ALL_DATA:Ljava/lang/String; = "com.sec.android.app.snsimagecache.action.DELETE_ALL_DATA"

.field public static final ACTION_DOWNLOAD_IMAGE:Ljava/lang/String; = "com.sec.android.app.snsimagecache.action.DOWNLOAD_IMAGE"

.field public static final ACTION_FACEBOOK_ME_PROFILE_REQUESTED:Ljava/lang/String; = "com.sec.android.app.sns.profile.ACTION_FACEBOOK_ME_PROFILE_REQUESTED"

.field public static final ACTION_FACEBOOK_ME_PROFILE_UPDATED:Ljava/lang/String; = "com.sec.android.app.sns.profile.ACTION_FACEBOOK_ME_PROFILE_UPDATED"

.field public static final ACTION_FACEBOOK_REQUESTED:Ljava/lang/String; = "com.sec.android.app.sns.profile.ACTION_FACEBOOK_REQUESTED"

.field public static final ACTION_FACEBOOK_UPDATED:Ljava/lang/String; = "com.sec.android.app.sns.profile.ACTION_FACEBOOK_UPDATED"

.field public static final ACTION_FOURSQUARE_REQUESTED:Ljava/lang/String; = "com.sec.android.app.sns.profile.ACTION_FOURSQUARE_REQUESTED"

.field public static final ACTION_FOURSQUARE_UPDATED:Ljava/lang/String; = "com.sec.android.app.sns.profile.ACTION_FOURSQUARE_UPDATED"

.field public static final ACTION_GOOGLEPLUS_ME_PROFILE_REQUESTED:Ljava/lang/String; = "com.sec.android.app.sns.profile.ACTION_GOOGLEPLUS_ME_PROFILE_REQUESTED"

.field public static final ACTION_GOOGLEPLUS_ME_PROFILE_UPDATED:Ljava/lang/String; = "com.sec.android.app.sns.profile.ACTION_GOOGLEPLUS_ME_PROFILE_UPDATED"

.field public static final ACTION_GOOGLEPLUS_REQUESTED:Ljava/lang/String; = "com.sec.android.app.sns.profile.ACTION_GOOGLEPLUS_REQUESTED"

.field public static final ACTION_GOOGLEPLUS_UPDATED:Ljava/lang/String; = "com.sec.android.app.sns.profile.ACTION_GOOGLEPLUS_UPDATED"

.field public static final ACTION_IMAGE_DELETED:Ljava/lang/String; = "com.sec.android.app.snsimagecache.action.IMAGE_DELETED"

.field public static final ACTION_IMAGE_DOWNLOADED:Ljava/lang/String; = "com.sec.android.app.snsimagecache.action.IMAGE_DOWNLOADED"

.field public static final ACTION_INSTAGRAM_REQUESTED:Ljava/lang/String; = "com.sec.android.app.sns.profile.ACTION_INSTAGRAM_REQUESTED"

.field public static final ACTION_INSTAGRAM_UPDATED:Ljava/lang/String; = "com.sec.android.app.sns.profile.ACTION_INSTAGRAM_UPDATED"

.field public static final ACTION_LINKEDIN_PEOPLE_LOOKUP_REQUESTED:Ljava/lang/String; = "com.sec.android.app.sns.profile.ACTION_LINKEDIN_PEOPLE_LOOKUP_REQUESTED"

.field public static final ACTION_LINKEDIN_PEOPLE_LOOKUP_UPDATED:Ljava/lang/String; = "com.sec.android.app.sns.profile.ACTION_LINKEDIN_PEOPLE_LOOKUP_UPDATED"

.field public static final ACTION_LINKEDIN_REQUESTED:Ljava/lang/String; = "com.sec.android.app.sns.profile.ACTION_LINKEDIN_REQUESTED"

.field public static final ACTION_LINKEDIN_UPDATED:Ljava/lang/String; = "com.sec.android.app.sns.profile.ACTION_LINKEDIN_UPDATED"

.field public static final ACTION_TWITTER_REQUESTED:Ljava/lang/String; = "com.sec.android.app.sns.profile.ACTION_TWITTER_REQUESTED"

.field public static final ACTION_TWITTER_UPDATED:Ljava/lang/String; = "com.sec.android.app.sns.profile.ACTION_TWITTER_UPDATED"

.field public static final EXTRA_ADDRESS:Ljava/lang/String; = "address"

.field public static final EXTRA_BIRTHDAY:Ljava/lang/String; = "birthday"

.field public static final EXTRA_CACHE_URI:Ljava/lang/String; = "cacheUri"

.field public static final EXTRA_DOWNLOAD_PHOTO:Ljava/lang/String; = "downloadPhoto"

.field public static final EXTRA_EMAIL:Ljava/lang/String; = "email"

.field public static final EXTRA_EMAIL_LIST:Ljava/lang/String; = "emailList"

.field public static final EXTRA_IMAGE_URL:Ljava/lang/String; = "imageUrl"

.field public static final EXTRA_IMAGE_URL_LIST:Ljava/lang/String; = "imageUrlList"

.field public static final EXTRA_NAME:Ljava/lang/String; = "name"

.field public static final EXTRA_PROFILE_URL:Ljava/lang/String; = "profile_url"

.field public static final EXTRA_RESULT_CODE:Ljava/lang/String; = "result"

.field public static final EXTRA_USER_ID:Ljava/lang/String; = "id"

.field public static final EXTRA_WORK:Ljava/lang/String; = "work"

.field public static final PERMISSION_PEOPLE_LOOKUP:Ljava/lang/String; = "com.sec.android.app.sns3.permission.REQUEST_PEOPLE_LOOKUP"

.field public static final RESULT_FAIL:I = -0x1

.field public static final RESULT_OK:I = 0x0

.field public static final TAG:Ljava/lang/String; = "SnsStautsStream"
