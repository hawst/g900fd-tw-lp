.class public Lcom/sec/android/app/sns3/app/profile/SnsStatusStreamBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "SnsStatusStreamBroadcastReceiver.java"


# static fields
.field private static final CHECK_OLD_DATA_INTERVAL:J = 0x5265c00L

.field private static final ONE_DAY:J = 0x5265c00L

.field private static final REQUEST_CHECK_CONTACTS:I = 0x3e9

.field private static final REQUEST_CHECK_OLD_DATA:I = 0x3e8

.field private static final UPDATE_LINKEDIN_INTERVAL:J = 0x240c8400L

.field private static final UPDATE_STREAM_INTERVAL:J = 0xb43e9400L

.field private static mPendingIntentForContacts:Landroid/app/PendingIntent;

.field private static mPendingIntentForOldData:Landroid/app/PendingIntent;


# instance fields
.field private mSnsImageCacheAvailable:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 66
    sput-object v0, Lcom/sec/android/app/sns3/app/profile/SnsStatusStreamBroadcastReceiver;->mPendingIntentForOldData:Landroid/app/PendingIntent;

    .line 67
    sput-object v0, Lcom/sec/android/app/sns3/app/profile/SnsStatusStreamBroadcastReceiver;->mPendingIntentForContacts:Landroid/app/PendingIntent;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 56
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 69
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/SnsApplication;->getFeatureMgr()Lcom/sec/android/app/sns3/SnsFeatureManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/SnsFeatureManager;->isSnsImageCacheAvailable()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/sns3/app/profile/SnsStatusStreamBroadcastReceiver;->mSnsImageCacheAvailable:Z

    return-void
.end method

.method private cancelAlarmToCheckContacts(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 193
    const-string v1, "alarm"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 195
    .local v0, "alarmManager":Landroid/app/AlarmManager;
    sget-object v1, Lcom/sec/android/app/sns3/app/profile/SnsStatusStreamBroadcastReceiver;->mPendingIntentForContacts:Landroid/app/PendingIntent;

    if-eqz v1, :cond_0

    .line 196
    sget-object v1, Lcom/sec/android/app/sns3/app/profile/SnsStatusStreamBroadcastReceiver;->mPendingIntentForContacts:Landroid/app/PendingIntent;

    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 198
    :cond_0
    return-void
.end method

.method private getProfileForContacts(Landroid/content/Context;)V
    .locals 14
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 201
    const/4 v10, 0x0

    .line 202
    .local v10, "emailList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v7, 0x0

    .line 205
    .local v7, "cursor":Landroid/database/Cursor;
    :try_start_0
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    .line 206
    .local v13, "where":Ljava/lang/StringBuilder;
    const-string v0, "account_type != \'com.linkedin.android\'"

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 207
    const-string v0, " AND mimetype = \'vnd.android.cursor.item/email_v2\'"

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 208
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/SnsApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/ContactsContract$CommonDataKinds$Email;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "data1"

    aput-object v4, v2, v3

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 217
    if-eqz v7, :cond_3

    .line 218
    const-string v0, "data1"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    .line 219
    .local v9, "emailIdx":I
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 221
    .end local v10    # "emailList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .local v11, "emailList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :try_start_1
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    .line 222
    :goto_0
    invoke-interface {v7}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v0

    if-nez v0, :cond_2

    .line 223
    invoke-interface {v7, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 224
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_0

    .line 228
    :catch_0
    move-exception v8

    move-object v10, v11

    .line 229
    .end local v9    # "emailIdx":I
    .end local v11    # "emailList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v13    # "where":Ljava/lang/StringBuilder;
    .local v8, "e":Ljava/lang/Exception;
    .restart local v10    # "emailList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :goto_1
    :try_start_2
    invoke-virtual {v8}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 231
    if-eqz v7, :cond_0

    .line 232
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 236
    .end local v8    # "e":Ljava/lang/Exception;
    :cond_0
    :goto_2
    if-eqz v10, :cond_1

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eqz v0, :cond_1

    .line 237
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 238
    .local v6, "bundle":Landroid/os/Bundle;
    const-string v0, "emailList"

    invoke-virtual {v6, v0, v10}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 239
    const-string v0, "downloadPhoto"

    iget-boolean v1, p0, Lcom/sec/android/app/sns3/app/profile/SnsStatusStreamBroadcastReceiver;->mSnsImageCacheAvailable:Z

    invoke-virtual {v6, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 241
    new-instance v12, Landroid/content/Intent;

    const-string v0, "com.sec.android.app.sns.profile.ACTION_LINKEDIN_PEOPLE_LOOKUP_REQUESTED"

    invoke-direct {v12, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 243
    .local v12, "requestIntent":Landroid/content/Intent;
    invoke-virtual {v12, v6}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 244
    const-string v0, "com.sec.android.app.sns3.permission.REQUEST_PEOPLE_LOOKUP"

    invoke-virtual {p1, v12, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    .line 246
    .end local v6    # "bundle":Landroid/os/Bundle;
    .end local v12    # "requestIntent":Landroid/content/Intent;
    :cond_1
    return-void

    .end local v10    # "emailList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v9    # "emailIdx":I
    .restart local v11    # "emailList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v13    # "where":Ljava/lang/StringBuilder;
    :cond_2
    move-object v10, v11

    .line 231
    .end local v9    # "emailIdx":I
    .end local v11    # "emailList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v10    # "emailList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_3
    if-eqz v7, :cond_0

    .line 232
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto :goto_2

    .line 231
    .end local v13    # "where":Ljava/lang/StringBuilder;
    :catchall_0
    move-exception v0

    :goto_3
    if-eqz v7, :cond_4

    .line 232
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0

    .line 231
    .end local v10    # "emailList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v9    # "emailIdx":I
    .restart local v11    # "emailList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v13    # "where":Ljava/lang/StringBuilder;
    :catchall_1
    move-exception v0

    move-object v10, v11

    .end local v11    # "emailList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v10    # "emailList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    goto :goto_3

    .line 228
    .end local v9    # "emailIdx":I
    .end local v13    # "where":Ljava/lang/StringBuilder;
    :catch_1
    move-exception v8

    goto :goto_1
.end method

.method private removeOldData()V
    .locals 16

    .prologue
    .line 129
    const-string v12, "SnsStautsStream"

    const-string v13, "removing old data"

    invoke-static {v12, v13}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 131
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v12

    invoke-virtual {v12}, Lcom/sec/android/app/sns3/SnsApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 132
    .local v0, "cr":Landroid/content/ContentResolver;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    const-wide/32 v14, 0x240c8400

    sub-long v4, v12, v14

    .line 133
    .local v4, "daysAgo7":J
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    const-wide v14, 0xb43e9400L

    sub-long v2, v12, v14

    .line 135
    .local v2, "daysAgo35":J
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "last_updated_timestamp < "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 136
    .local v10, "selectionPeople":Ljava/lang/String;
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "updated_time < "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 137
    .local v1, "selectionFbStream":Ljava/lang/String;
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "timestamp_utc < "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 138
    .local v7, "selectionGpStream":Ljava/lang/String;
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "timestamp < "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 139
    .local v9, "selectionLiStream":Ljava/lang/String;
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "create_at < "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 140
    .local v11, "selectionTwStream":Ljava/lang/String;
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "timestamp_utc < "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 141
    .local v6, "selectionFsStream":Ljava/lang/String;
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "timestamp_utc < "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 143
    .local v8, "selectionInStream":Ljava/lang/String;
    sget-object v12, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$PeopleLookUp;->CONTENT_URI:Landroid/net/Uri;

    const/4 v13, 0x0

    invoke-virtual {v0, v12, v10, v13}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 144
    sget-object v12, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$StatusStream;->CONTENT_URI:Landroid/net/Uri;

    const/4 v13, 0x0

    invoke-virtual {v0, v12, v1, v13}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 145
    sget-object v12, Lcom/sec/android/app/sns3/agent/sp/googleplus/db/SnsGooglePlusDB$StatusStream;->CONTENT_URI:Landroid/net/Uri;

    const/4 v13, 0x0

    invoke-virtual {v0, v12, v7, v13}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 146
    sget-object v12, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$StatusStream;->CONTENT_URI:Landroid/net/Uri;

    const/4 v13, 0x0

    invoke-virtual {v0, v12, v9, v13}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 147
    sget-object v12, Lcom/sec/android/app/sns3/agent/sp/twitter/db/SnsTwitterDB$StatusStream;->CONTENT_URI:Landroid/net/Uri;

    const/4 v13, 0x0

    invoke-virtual {v0, v12, v11, v13}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 148
    sget-object v12, Lcom/sec/android/app/sns3/agent/sp/foursquare/db/SnsFourSquareDB$StatusStream;->CONTENT_URI:Landroid/net/Uri;

    const/4 v13, 0x0

    invoke-virtual {v0, v12, v6, v13}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 149
    sget-object v12, Lcom/sec/android/app/sns3/agent/sp/instagram/db/SnsInstagramDB$StatusStream;->CONTENT_URI:Landroid/net/Uri;

    const/4 v13, 0x0

    invoke-virtual {v0, v12, v8, v13}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 151
    invoke-static {}, Lcom/sec/android/app/sns3/app/profile/sp/linkedin/SnsLiGetPeopleLookUp;->clearEmailCache()V

    .line 152
    return-void
.end method

.method private setAlarmToCheckContacts(Landroid/content/Context;J)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "firstTriggerTimeFromCurrent"    # J

    .prologue
    .line 174
    const-string v1, "SnsStautsStream"

    const-string v4, "alarm for contacts calling"

    invoke-static {v1, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 176
    const-string v1, "alarm"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 178
    .local v0, "alarmManager":Landroid/app/AlarmManager;
    sget-object v1, Lcom/sec/android/app/sns3/app/profile/SnsStatusStreamBroadcastReceiver;->mPendingIntentForContacts:Landroid/app/PendingIntent;

    if-nez v1, :cond_0

    .line 179
    new-instance v7, Landroid/content/Intent;

    const-string v1, "com.sec.android.app.sns.profile.ACTION_CHECK_CONTACTS"

    invoke-direct {v7, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 180
    .local v7, "intent":Landroid/content/Intent;
    const/16 v1, 0x3e9

    const/high16 v4, 0x10000000

    invoke-static {p1, v1, v7, v4}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    sput-object v1, Lcom/sec/android/app/sns3/app/profile/SnsStatusStreamBroadcastReceiver;->mPendingIntentForContacts:Landroid/app/PendingIntent;

    .line 186
    .end local v7    # "intent":Landroid/content/Intent;
    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    .line 187
    .local v8, "currnetTime":J
    add-long v2, v8, p2

    .line 188
    .local v2, "triggerAtTime":J
    const/4 v1, 0x1

    const-wide/32 v4, 0x240c8400

    sget-object v6, Lcom/sec/android/app/sns3/app/profile/SnsStatusStreamBroadcastReceiver;->mPendingIntentForContacts:Landroid/app/PendingIntent;

    invoke-virtual/range {v0 .. v6}, Landroid/app/AlarmManager;->setRepeating(IJJLandroid/app/PendingIntent;)V

    .line 190
    return-void

    .line 183
    .end local v2    # "triggerAtTime":J
    .end local v8    # "currnetTime":J
    :cond_0
    sget-object v1, Lcom/sec/android/app/sns3/app/profile/SnsStatusStreamBroadcastReceiver;->mPendingIntentForContacts:Landroid/app/PendingIntent;

    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    goto :goto_0
.end method

.method private setAlarmToCheckOldData(Landroid/content/Context;J)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "firstTriggerTimeFromCurrent"    # J

    .prologue
    .line 155
    const-string v1, "SnsStautsStream"

    const-string v4, "alarm for old data calling"

    invoke-static {v1, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 157
    const-string v1, "alarm"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 159
    .local v0, "alarmManager":Landroid/app/AlarmManager;
    sget-object v1, Lcom/sec/android/app/sns3/app/profile/SnsStatusStreamBroadcastReceiver;->mPendingIntentForOldData:Landroid/app/PendingIntent;

    if-nez v1, :cond_0

    .line 160
    new-instance v7, Landroid/content/Intent;

    const-string v1, "com.sec.android.app.sns.profile.ACTION_CHECK_OLD_DATA"

    invoke-direct {v7, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 161
    .local v7, "intent":Landroid/content/Intent;
    const/16 v1, 0x3e8

    const/high16 v4, 0x10000000

    invoke-static {p1, v1, v7, v4}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    sput-object v1, Lcom/sec/android/app/sns3/app/profile/SnsStatusStreamBroadcastReceiver;->mPendingIntentForOldData:Landroid/app/PendingIntent;

    .line 167
    .end local v7    # "intent":Landroid/content/Intent;
    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    .line 168
    .local v8, "currnetTime":J
    add-long v2, v8, p2

    .line 169
    .local v2, "triggerAtTime":J
    const/4 v1, 0x1

    const-wide/32 v4, 0x5265c00

    sget-object v6, Lcom/sec/android/app/sns3/app/profile/SnsStatusStreamBroadcastReceiver;->mPendingIntentForOldData:Landroid/app/PendingIntent;

    invoke-virtual/range {v0 .. v6}, Landroid/app/AlarmManager;->setRepeating(IJJLandroid/app/PendingIntent;)V

    .line 171
    return-void

    .line 164
    .end local v2    # "triggerAtTime":J
    .end local v8    # "currnetTime":J
    :cond_0
    sget-object v1, Lcom/sec/android/app/sns3/app/profile/SnsStatusStreamBroadcastReceiver;->mPendingIntentForOldData:Landroid/app/PendingIntent;

    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    goto :goto_0
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 74
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    .line 75
    .local v1, "action":Ljava/lang/String;
    const-string v5, "id"

    invoke-virtual {p2, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 77
    .local v4, "userId":Ljava/lang/String;
    const-string v5, "SnsStautsStream"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "SnsStatusStreamBroadcastReceiver : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 79
    const-string v5, "com.sec.android.app.sns.profile.ACTION_FACEBOOK_REQUESTED"

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 80
    invoke-static {p1, v4}, Lcom/sec/android/app/sns3/app/profile/sp/facebook/SnsFbGetStatusStream;->updateStatus(Landroid/content/Context;Ljava/lang/String;)V

    .line 126
    :cond_0
    :goto_0
    return-void

    .line 81
    :cond_1
    const-string v5, "com.sec.android.app.sns.profile.ACTION_LINKEDIN_REQUESTED"

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 82
    invoke-static {p1, v4}, Lcom/sec/android/app/sns3/app/profile/sp/linkedin/SnsLiGetStatusStream;->updateStatus(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    .line 83
    :cond_2
    const-string v5, "com.sec.android.app.sns.profile.ACTION_GOOGLEPLUS_REQUESTED"

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 84
    invoke-static {p1, v4}, Lcom/sec/android/app/sns3/app/profile/sp/googleplus/SnsGpGetStatusStream;->updateStatus(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    .line 85
    :cond_3
    const-string v5, "com.sec.android.app.sns.profile.ACTION_TWITTER_REQUESTED"

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 86
    invoke-static {p1, v4}, Lcom/sec/android/app/sns3/app/profile/sp/twitter/SnsTwGetStatusStream;->updateStatus(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    .line 87
    :cond_4
    const-string v5, "com.sec.android.app.sns.profile.ACTION_FOURSQUARE_REQUESTED"

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 88
    invoke-static {p1, v4}, Lcom/sec/android/app/sns3/app/profile/sp/foursquare/SnsFsGetStatusStream;->updateStatus(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    .line 89
    :cond_5
    const-string v5, "com.sec.android.app.sns.profile.ACTION_INSTAGRAM_REQUESTED"

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 90
    invoke-static {p1, v4}, Lcom/sec/android/app/sns3/app/profile/sp/instagram/SnsInGetStatusStream;->updateStatus(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    .line 91
    :cond_6
    const-string v5, "com.sec.android.app.sns.profile.ACTION_LINKEDIN_PEOPLE_LOOKUP_REQUESTED"

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 92
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v5

    invoke-static {p1, v5}, Lcom/sec/android/app/sns3/app/profile/sp/linkedin/SnsLiGetPeopleLookUp;->lookUpPeople(Landroid/content/Context;Landroid/os/Bundle;)V

    goto :goto_0

    .line 93
    :cond_7
    const-string v5, "com.sec.android.app.sns.profile.ACTION_CHECK_START_ALARM"

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_9

    .line 95
    invoke-static {p1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v5

    const-string v6, "com.sec.android.app.sns3.linkedin"

    invoke-virtual {v5, v6}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 97
    .local v0, "account":[Landroid/accounts/Account;
    array-length v5, v0

    if-lez v5, :cond_8

    .line 98
    const-wide/32 v6, 0xea60

    invoke-direct {p0, p1, v6, v7}, Lcom/sec/android/app/sns3/app/profile/SnsStatusStreamBroadcastReceiver;->setAlarmToCheckContacts(Landroid/content/Context;J)V

    .line 102
    :cond_8
    const-wide/32 v6, 0x55d4a80

    invoke-direct {p0, p1, v6, v7}, Lcom/sec/android/app/sns3/app/profile/SnsStatusStreamBroadcastReceiver;->setAlarmToCheckOldData(Landroid/content/Context;J)V

    goto :goto_0

    .line 103
    .end local v0    # "account":[Landroid/accounts/Account;
    :cond_9
    const-string v5, "com.sec.android.app.sns.profile.ACTION_CHECK_OLD_DATA"

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_a

    .line 104
    invoke-direct {p0}, Lcom/sec/android/app/sns3/app/profile/SnsStatusStreamBroadcastReceiver;->removeOldData()V

    goto :goto_0

    .line 105
    :cond_a
    const-string v5, "com.sec.android.app.sns.profile.ACTION_CHECK_CONTACTS"

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_b

    .line 106
    invoke-static {}, Lcom/sec/android/app/sns3/app/profile/sp/linkedin/SnsLiGetPeopleLookUp;->clearEmailCache()V

    .line 107
    invoke-direct {p0, p1}, Lcom/sec/android/app/sns3/app/profile/SnsStatusStreamBroadcastReceiver;->getProfileForContacts(Landroid/content/Context;)V

    goto/16 :goto_0

    .line 108
    :cond_b
    const-string v5, "com.sec.android.app.sns.ACTION_SNS_LINKEDIN_LOGGED_IN"

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_c

    .line 109
    const-wide/16 v6, 0x3e8

    invoke-direct {p0, p1, v6, v7}, Lcom/sec/android/app/sns3/app/profile/SnsStatusStreamBroadcastReceiver;->setAlarmToCheckContacts(Landroid/content/Context;J)V

    goto/16 :goto_0

    .line 110
    :cond_c
    const-string v5, "com.sec.android.app.sns.ACTION_SNS_LINKEDIN_LOGGED_OUT"

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_d

    .line 111
    invoke-static {}, Lcom/sec/android/app/sns3/app/profile/sp/linkedin/SnsLiGetPeopleLookUp;->clearEmailCache()V

    .line 112
    invoke-static {p1}, Lcom/sec/android/app/sns3/app/profile/sp/linkedin/SnsLiGetPeopleLookUp;->clearImageCache(Landroid/content/Context;)V

    .line 113
    invoke-direct {p0, p1}, Lcom/sec/android/app/sns3/app/profile/SnsStatusStreamBroadcastReceiver;->cancelAlarmToCheckContacts(Landroid/content/Context;)V

    goto/16 :goto_0

    .line 114
    :cond_d
    const-string v5, "com.sec.android.app.snsimagecache.action.IMAGE_DOWNLOADED"

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_e

    .line 115
    const-string v5, "imageUrl"

    invoke-virtual {p2, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 116
    .local v3, "imageUrl":Ljava/lang/String;
    const-string v5, "cacheUri"

    invoke-virtual {p2, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 117
    .local v2, "cacheUri":Ljava/lang/String;
    invoke-static {v3, v2}, Lcom/sec/android/app/sns3/app/profile/sp/linkedin/SnsLiGetPeopleLookUp;->insertPictureCacheUri(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 118
    .end local v2    # "cacheUri":Ljava/lang/String;
    .end local v3    # "imageUrl":Ljava/lang/String;
    :cond_e
    const-string v5, "com.sec.android.app.snsimagecache.action.IMAGE_DELETED"

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_f

    .line 119
    const-string v5, "cacheUri"

    invoke-virtual {p2, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 120
    .restart local v2    # "cacheUri":Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/sns3/app/profile/sp/linkedin/SnsLiGetPeopleLookUp;->deletePictureCacheUri(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 121
    .end local v2    # "cacheUri":Ljava/lang/String;
    :cond_f
    const-string v5, "com.sec.android.app.sns.profile.ACTION_FACEBOOK_ME_PROFILE_REQUESTED"

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 123
    const-string v5, "com.sec.android.app.sns.profile.ACTION_GOOGLEPLUS_ME_PROFILE_REQUESTED"

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    goto/16 :goto_0
.end method
