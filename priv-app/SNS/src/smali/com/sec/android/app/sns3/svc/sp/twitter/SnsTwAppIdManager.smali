.class public Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwAppIdManager;
.super Ljava/lang/Object;
.source "SnsTwAppIdManager.java"


# static fields
.field private static final DEFAULT_CONSUMER_KEY:Ljava/lang/String; = "58ZzcSLi9DA9PxIffHtS7wLBZ"

.field private static final DEFAULT_CONSUMER_SECRET:Ljava/lang/String; = "ck3E0UdcS4vYEFK9t2DwaRhxel1dQNMr1RNF3ZhvtjU3WigTtd"

.field private static final PROPERTIES_FILE:Ljava/lang/String; = "/etc/snstw.conf"

.field private static final TAG:Ljava/lang/String; = "SnsTwAppIdManager"

.field private static mInstance:Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwAppIdManager;


# instance fields
.field private mConsumerKey:Ljava/lang/String;

.field private mConsumerSecret:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwAppIdManager;->mInstance:Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwAppIdManager;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwAppIdManager;->mConsumerKey:Ljava/lang/String;

    .line 43
    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwAppIdManager;->mConsumerSecret:Ljava/lang/String;

    .line 46
    invoke-direct {p0}, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwAppIdManager;->loadTwAppIdAndKey()V

    .line 47
    return-void
.end method

.method public static getInstance()Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwAppIdManager;
    .locals 1

    .prologue
    .line 85
    sget-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwAppIdManager;->mInstance:Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwAppIdManager;

    if-nez v0, :cond_0

    .line 86
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwAppIdManager;

    invoke-direct {v0}, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwAppIdManager;-><init>()V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwAppIdManager;->mInstance:Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwAppIdManager;

    .line 88
    :cond_0
    sget-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwAppIdManager;->mInstance:Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwAppIdManager;

    return-object v0
.end method

.method private loadTwAppIdAndKey()V
    .locals 10

    .prologue
    .line 50
    new-instance v3, Ljava/util/Properties;

    invoke-direct {v3}, Ljava/util/Properties;-><init>()V

    .line 51
    .local v3, "properties":Ljava/util/Properties;
    new-instance v1, Ljava/io/File;

    const-string v7, "/etc/snstw.conf"

    invoke-direct {v1, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 52
    .local v1, "file":Ljava/io/File;
    const/4 v5, 0x0

    .line 55
    .local v5, "stream":Ljava/io/FileInputStream;
    :try_start_0
    new-instance v6, Ljava/io/FileInputStream;

    invoke-direct {v6, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 56
    .end local v5    # "stream":Ljava/io/FileInputStream;
    .local v6, "stream":Ljava/io/FileInputStream;
    :try_start_1
    invoke-virtual {v3, v6}, Ljava/util/Properties;->load(Ljava/io/InputStream;)V

    .line 58
    const-string v7, "KEY"

    invoke-virtual {v3, v7}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 59
    .local v2, "key":Ljava/lang/String;
    const-string v7, "SECRET"

    invoke-virtual {v3, v7}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 61
    .local v4, "secret":Ljava/lang/String;
    const-string v7, "SnsTwAppIdManager"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "TW : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 63
    iput-object v2, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwAppIdManager;->mConsumerKey:Ljava/lang/String;

    .line 64
    iput-object v4, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwAppIdManager;->mConsumerSecret:Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 68
    if-eqz v6, :cond_4

    .line 70
    :try_start_2
    invoke-virtual {v6}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-object v5, v6

    .line 77
    .end local v2    # "key":Ljava/lang/String;
    .end local v4    # "secret":Ljava/lang/String;
    .end local v6    # "stream":Ljava/io/FileInputStream;
    .restart local v5    # "stream":Ljava/io/FileInputStream;
    :cond_0
    :goto_0
    iget-object v7, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwAppIdManager;->mConsumerKey:Ljava/lang/String;

    if-eqz v7, :cond_1

    iget-object v7, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwAppIdManager;->mConsumerSecret:Ljava/lang/String;

    if-nez v7, :cond_2

    .line 79
    :cond_1
    const-string v7, "58ZzcSLi9DA9PxIffHtS7wLBZ"

    iput-object v7, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwAppIdManager;->mConsumerKey:Ljava/lang/String;

    .line 80
    const-string v7, "ck3E0UdcS4vYEFK9t2DwaRhxel1dQNMr1RNF3ZhvtjU3WigTtd"

    iput-object v7, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwAppIdManager;->mConsumerSecret:Ljava/lang/String;

    .line 82
    :cond_2
    return-void

    .line 71
    .end local v5    # "stream":Ljava/io/FileInputStream;
    .restart local v2    # "key":Ljava/lang/String;
    .restart local v4    # "secret":Ljava/lang/String;
    .restart local v6    # "stream":Ljava/io/FileInputStream;
    :catch_0
    move-exception v0

    .line 72
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    move-object v5, v6

    .line 73
    .end local v6    # "stream":Ljava/io/FileInputStream;
    .restart local v5    # "stream":Ljava/io/FileInputStream;
    goto :goto_0

    .line 65
    .end local v0    # "e":Ljava/io/IOException;
    .end local v2    # "key":Ljava/lang/String;
    .end local v4    # "secret":Ljava/lang/String;
    :catch_1
    move-exception v0

    .line 66
    .restart local v0    # "e":Ljava/io/IOException;
    :goto_1
    :try_start_3
    const-string v7, "SnsTwAppIdManager"

    const-string v8, "TW configuration file not existed"

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 68
    if-eqz v5, :cond_0

    .line 70
    :try_start_4
    invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 71
    :catch_2
    move-exception v0

    .line 72
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 68
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v7

    :goto_2
    if-eqz v5, :cond_3

    .line 70
    :try_start_5
    invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 73
    :cond_3
    :goto_3
    throw v7

    .line 71
    :catch_3
    move-exception v0

    .line 72
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 68
    .end local v0    # "e":Ljava/io/IOException;
    .end local v5    # "stream":Ljava/io/FileInputStream;
    .restart local v6    # "stream":Ljava/io/FileInputStream;
    :catchall_1
    move-exception v7

    move-object v5, v6

    .end local v6    # "stream":Ljava/io/FileInputStream;
    .restart local v5    # "stream":Ljava/io/FileInputStream;
    goto :goto_2

    .line 65
    .end local v5    # "stream":Ljava/io/FileInputStream;
    .restart local v6    # "stream":Ljava/io/FileInputStream;
    :catch_4
    move-exception v0

    move-object v5, v6

    .end local v6    # "stream":Ljava/io/FileInputStream;
    .restart local v5    # "stream":Ljava/io/FileInputStream;
    goto :goto_1

    .end local v5    # "stream":Ljava/io/FileInputStream;
    .restart local v2    # "key":Ljava/lang/String;
    .restart local v4    # "secret":Ljava/lang/String;
    .restart local v6    # "stream":Ljava/io/FileInputStream;
    :cond_4
    move-object v5, v6

    .end local v6    # "stream":Ljava/io/FileInputStream;
    .restart local v5    # "stream":Ljava/io/FileInputStream;
    goto :goto_0
.end method


# virtual methods
.method public getConsumerKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwAppIdManager;->mConsumerKey:Ljava/lang/String;

    return-object v0
.end method

.method public getConsumerSecret()Ljava/lang/String;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwAppIdManager;->mConsumerSecret:Ljava/lang/String;

    return-object v0
.end method
