.class public interface abstract Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserNoteEx$FacebookNoteEx$SnsFbCommentQuery;
.super Ljava/lang/Object;
.source "SnsFbParserNoteEx.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserNoteEx$FacebookNoteEx;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "SnsFbCommentQuery"
.end annotation


# static fields
.field public static final COMMENTS_ID:Ljava/lang/String; = "id"

.field public static final CREATED_TIME:Ljava/lang/String; = "time"

.field public static final FROME_NAME:Ljava/lang/String; = "username"

.field public static final FROM_ID:Ljava/lang/String; = "fromid"

.field public static final LIKES:Ljava/lang/String; = "likes"

.field public static final MESSAGE:Ljava/lang/String; = "text"

.field public static final TARGET_ID:Ljava/lang/String; = "object_id"

.field public static final USER_LIKES:Ljava/lang/String; = "user_likes"
