.class public abstract Lcom/sec/android/app/sns3/svc/sp/twitter/api/ISnsTwitter$Stub;
.super Landroid/os/Binder;
.source "ISnsTwitter.java"

# interfaces
.implements Lcom/sec/android/app/sns3/svc/sp/twitter/api/ISnsTwitter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/svc/sp/twitter/api/ISnsTwitter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/twitter/api/ISnsTwitter$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.sec.android.app.sns3.svc.sp.twitter.api.ISnsTwitter"

.field static final TRANSACTION_getApplicationInfo:I = 0x1

.field static final TRANSACTION_getHomeTimeline:I = 0x5

.field static final TRANSACTION_getUsersLookup:I = 0x4

.field static final TRANSACTION_postStatus:I = 0x2

.field static final TRANSACTION_postStatusWithMedia:I = 0x3


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 15
    const-string v0, "com.sec.android.app.sns3.svc.sp.twitter.api.ISnsTwitter"

    invoke-virtual {p0, p0, v0}, Lcom/sec/android/app/sns3/svc/sp/twitter/api/ISnsTwitter$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 16
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/sec/android/app/sns3/svc/sp/twitter/api/ISnsTwitter;
    .locals 2
    .param p0, "obj"    # Landroid/os/IBinder;

    .prologue
    .line 23
    if-nez p0, :cond_0

    .line 24
    const/4 v0, 0x0

    .line 30
    :goto_0
    return-object v0

    .line 26
    :cond_0
    const-string v1, "com.sec.android.app.sns3.svc.sp.twitter.api.ISnsTwitter"

    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 27
    .local v0, "iin":Landroid/os/IInterface;
    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/sec/android/app/sns3/svc/sp/twitter/api/ISnsTwitter;

    if-eqz v1, :cond_1

    .line 28
    check-cast v0, Lcom/sec/android/app/sns3/svc/sp/twitter/api/ISnsTwitter;

    goto :goto_0

    .line 30
    :cond_1
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/api/ISnsTwitter$Stub$Proxy;

    .end local v0    # "iin":Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/sec/android/app/sns3/svc/sp/twitter/api/ISnsTwitter$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 34
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 5
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 38
    sparse-switch p1, :sswitch_data_0

    .line 122
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v3

    :goto_0
    return v3

    .line 42
    :sswitch_0
    const-string v4, "com.sec.android.app.sns3.svc.sp.twitter.api.ISnsTwitter"

    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 47
    :sswitch_1
    const-string v4, "com.sec.android.app.sns3.svc.sp.twitter.api.ISnsTwitter"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 48
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/svc/sp/twitter/api/ISnsTwitter$Stub;->getApplicationInfo()Ljava/util/Map;

    move-result-object v2

    .line 49
    .local v2, "_result":Ljava/util/Map;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 50
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeMap(Ljava/util/Map;)V

    goto :goto_0

    .line 55
    .end local v2    # "_result":Ljava/util/Map;
    :sswitch_2
    const-string v4, "com.sec.android.app.sns3.svc.sp.twitter.api.ISnsTwitter"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 57
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_0

    .line 58
    sget-object v4, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v4, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 64
    .local v0, "_arg0":Landroid/os/Bundle;
    :goto_1
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/sns3/svc/sp/twitter/api/ISnsTwitterCallbackStatus$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/android/app/sns3/svc/sp/twitter/api/ISnsTwitterCallbackStatus;

    move-result-object v1

    .line 65
    .local v1, "_arg1":Lcom/sec/android/app/sns3/svc/sp/twitter/api/ISnsTwitterCallbackStatus;
    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/sns3/svc/sp/twitter/api/ISnsTwitter$Stub;->postStatus(Landroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/twitter/api/ISnsTwitterCallbackStatus;)I

    move-result v2

    .line 66
    .local v2, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 67
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 61
    .end local v0    # "_arg0":Landroid/os/Bundle;
    .end local v1    # "_arg1":Lcom/sec/android/app/sns3/svc/sp/twitter/api/ISnsTwitterCallbackStatus;
    .end local v2    # "_result":I
    :cond_0
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/os/Bundle;
    goto :goto_1

    .line 72
    .end local v0    # "_arg0":Landroid/os/Bundle;
    :sswitch_3
    const-string v4, "com.sec.android.app.sns3.svc.sp.twitter.api.ISnsTwitter"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 74
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_1

    .line 75
    sget-object v4, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v4, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 81
    .restart local v0    # "_arg0":Landroid/os/Bundle;
    :goto_2
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/sns3/svc/sp/twitter/api/ISnsTwitterCallbackStatus$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/android/app/sns3/svc/sp/twitter/api/ISnsTwitterCallbackStatus;

    move-result-object v1

    .line 82
    .restart local v1    # "_arg1":Lcom/sec/android/app/sns3/svc/sp/twitter/api/ISnsTwitterCallbackStatus;
    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/sns3/svc/sp/twitter/api/ISnsTwitter$Stub;->postStatusWithMedia(Landroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/twitter/api/ISnsTwitterCallbackStatus;)I

    move-result v2

    .line 83
    .restart local v2    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 84
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 78
    .end local v0    # "_arg0":Landroid/os/Bundle;
    .end local v1    # "_arg1":Lcom/sec/android/app/sns3/svc/sp/twitter/api/ISnsTwitterCallbackStatus;
    .end local v2    # "_result":I
    :cond_1
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/os/Bundle;
    goto :goto_2

    .line 89
    .end local v0    # "_arg0":Landroid/os/Bundle;
    :sswitch_4
    const-string v4, "com.sec.android.app.sns3.svc.sp.twitter.api.ISnsTwitter"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 91
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_2

    .line 92
    sget-object v4, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v4, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 98
    .restart local v0    # "_arg0":Landroid/os/Bundle;
    :goto_3
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/sns3/svc/sp/twitter/api/ISnsTwitterCallbackUser$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/android/app/sns3/svc/sp/twitter/api/ISnsTwitterCallbackUser;

    move-result-object v1

    .line 99
    .local v1, "_arg1":Lcom/sec/android/app/sns3/svc/sp/twitter/api/ISnsTwitterCallbackUser;
    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/sns3/svc/sp/twitter/api/ISnsTwitter$Stub;->getUsersLookup(Landroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/twitter/api/ISnsTwitterCallbackUser;)I

    move-result v2

    .line 100
    .restart local v2    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 101
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 95
    .end local v0    # "_arg0":Landroid/os/Bundle;
    .end local v1    # "_arg1":Lcom/sec/android/app/sns3/svc/sp/twitter/api/ISnsTwitterCallbackUser;
    .end local v2    # "_result":I
    :cond_2
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/os/Bundle;
    goto :goto_3

    .line 106
    .end local v0    # "_arg0":Landroid/os/Bundle;
    :sswitch_5
    const-string v4, "com.sec.android.app.sns3.svc.sp.twitter.api.ISnsTwitter"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 108
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_3

    .line 109
    sget-object v4, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v4, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 115
    .restart local v0    # "_arg0":Landroid/os/Bundle;
    :goto_4
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/sns3/svc/sp/twitter/api/ISnsTwitterCallbackStatus$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/android/app/sns3/svc/sp/twitter/api/ISnsTwitterCallbackStatus;

    move-result-object v1

    .line 116
    .local v1, "_arg1":Lcom/sec/android/app/sns3/svc/sp/twitter/api/ISnsTwitterCallbackStatus;
    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/sns3/svc/sp/twitter/api/ISnsTwitter$Stub;->getHomeTimeline(Landroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/twitter/api/ISnsTwitterCallbackStatus;)I

    move-result v2

    .line 117
    .restart local v2    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 118
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 112
    .end local v0    # "_arg0":Landroid/os/Bundle;
    .end local v1    # "_arg1":Lcom/sec/android/app/sns3/svc/sp/twitter/api/ISnsTwitterCallbackStatus;
    .end local v2    # "_result":I
    :cond_3
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/os/Bundle;
    goto :goto_4

    .line 38
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
