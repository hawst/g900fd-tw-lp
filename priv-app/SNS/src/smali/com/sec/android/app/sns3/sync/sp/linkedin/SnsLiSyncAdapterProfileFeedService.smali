.class public Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService;
.super Landroid/app/Service;
.source "SnsLiSyncAdapterProfileFeedService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService$SyncAdapterImpl;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "SnsLiSync"


# instance fields
.field private mAccount:Landroid/accounts/Account;

.field private mAuthority:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field private mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

.field private mSyncAdapter:Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService$SyncAdapterImpl;

.field private mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;

.field private mSyncResult:Landroid/content/SyncResult;

.field private mSyncState:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 60
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 64
    iput-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService;->mSyncAdapter:Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService$SyncAdapterImpl;

    .line 66
    iput-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;

    .line 68
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService;->mSyncState:I

    .line 70
    iput-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService;->mSyncResult:Landroid/content/SyncResult;

    .line 72
    iput-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService;->mAccount:Landroid/accounts/Account;

    .line 74
    iput-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService;->mAuthority:Ljava/lang/String;

    .line 80
    return-void
.end method

.method static synthetic access$002(Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService;Landroid/content/Context;)Landroid/content/Context;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService;
    .param p1, "x1"    # Landroid/content/Context;

    .prologue
    .line 60
    iput-object p1, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService;->mContext:Landroid/content/Context;

    return-object p1
.end method

.method static synthetic access$102(Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService;Landroid/accounts/Account;)Landroid/accounts/Account;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService;
    .param p1, "x1"    # Landroid/accounts/Account;

    .prologue
    .line 60
    iput-object p1, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService;->mAccount:Landroid/accounts/Account;

    return-object p1
.end method

.method static synthetic access$202(Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 60
    iput-object p1, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService;->mAuthority:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$302(Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService;Landroid/content/SyncResult;)Landroid/content/SyncResult;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService;
    .param p1, "x1"    # Landroid/content/SyncResult;

    .prologue
    .line 60
    iput-object p1, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService;->mSyncResult:Landroid/content/SyncResult;

    return-object p1
.end method

.method static synthetic access$400(Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService;

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService;->onSyncCanceled()V

    return-void
.end method

.method static synthetic access$502(Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService;
    .param p1, "x1"    # I

    .prologue
    .line 60
    iput p1, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService;->mSyncState:I

    return p1
.end method

.method static synthetic access$600(Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService;)Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;

    return-object v0
.end method

.method private handleSessionExpired()I
    .locals 11

    .prologue
    const/4 v9, 0x1

    const/4 v10, 0x0

    .line 293
    iget-object v7, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService;->mAccount:Landroid/accounts/Account;

    const-string v8, "com.sec.android.app.sns3.life"

    invoke-static {v7, v8}, Landroid/content/ContentResolver;->cancelSync(Landroid/accounts/Account;Ljava/lang/String;)V

    .line 295
    iget-object v7, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService;->mContext:Landroid/content/Context;

    const-string v8, "notification"

    invoke-virtual {v7, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/app/NotificationManager;

    .line 298
    .local v4, "notiMgr":Landroid/app/NotificationManager;
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 300
    .local v3, "notiIntent":Landroid/content/Intent;
    const-string v7, "com.sec.android.app.sns3.RETRY_SSO_LINKEDIN"

    invoke-virtual {v3, v7}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 304
    const/high16 v7, 0x800000

    invoke-virtual {v3, v7}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 305
    const-string v7, "RetryLogin"

    invoke-virtual {v3, v7, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 306
    iget-object v7, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService;->mContext:Landroid/content/Context;

    invoke-static {v7, v10, v3, v10}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 308
    .local v1, "launchIntent":Landroid/app/PendingIntent;
    const v6, 0x7f08003f

    .line 309
    .local v6, "titleID":I
    const v5, 0x7f08002e

    .line 310
    .local v5, "spID":I
    const v0, 0x7f02001e

    .line 312
    .local v0, "iconID":I
    new-instance v2, Landroid/app/Notification$Builder;

    iget-object v7, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService;->mContext:Landroid/content/Context;

    invoke-direct {v2, v7}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    .line 313
    .local v2, "notiBuilder":Landroid/app/Notification$Builder;
    iget-object v7, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService;->mContext:Landroid/content/Context;

    new-array v8, v9, [Ljava/lang/Object;

    iget-object v9, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService;->mContext:Landroid/content/Context;

    invoke-virtual {v9, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v10

    invoke-virtual {v7, v6, v8}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v7

    invoke-virtual {v7, v0}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService;->mContext:Landroid/content/Context;

    const v9, 0x7f08003e

    invoke-virtual {v8, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v7

    invoke-virtual {v7, v1}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v7

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Landroid/app/Notification$Builder;->setWhen(J)Landroid/app/Notification$Builder;

    .line 318
    const/16 v7, 0xce4

    invoke-virtual {v2}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v8

    invoke-virtual {v4, v7, v8}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 320
    const/4 v7, -0x1

    return v7
.end method

.method private invokeBroadcast()V
    .locals 7

    .prologue
    .line 334
    const/4 v2, 0x0

    .line 336
    .local v2, "intent":Landroid/content/Intent;
    sget-object v3, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$ProfileFeedList;->CONTENT_URI:Landroid/net/Uri;

    .line 337
    .local v3, "uri_profile":Landroid/net/Uri;
    const-string v0, "com.sec.android.app.sns3.action.SYNC_LI_PROFILE_FEEDS"

    .line 338
    .local v0, "action":Ljava/lang/String;
    iget v4, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService;->mSyncState:I

    const/4 v5, 0x2

    if-ne v4, v5, :cond_1

    const/4 v1, 0x1

    .line 340
    .local v1, "bResult":Z
    :goto_0
    new-instance v2, Landroid/content/Intent;

    .end local v2    # "intent":Landroid/content/Intent;
    invoke-direct {v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 341
    .restart local v2    # "intent":Landroid/content/Intent;
    const-string v4, "SNS3_CONTENT_URI_PROFILE"

    invoke-virtual {v2, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 342
    const-string v4, "SNS_RESULT"

    invoke-virtual {v2, v4, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 344
    const-string v4, "com.sec.android.app.sns3.permission.RECEIVE_SNS_BROADCAST"

    invoke-virtual {p0, v2, v4}, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    .line 345
    invoke-static {}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->isLoggable()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 346
    const-string v4, "SnsLiSync"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "SnsLiSyncAdapterProfileFeedService - invokeBroadcast() : action = ["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "], uri(profile) = ["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "], result = ["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 351
    :cond_0
    return-void

    .line 338
    .end local v1    # "bResult":Z
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private onSyncCanceled()V
    .locals 2

    .prologue
    .line 325
    const-string v0, "SnsLiSync"

    const-string v1, "***************** SnsLiSyncAdapterProfileFeedService : onSyncCanceled !!! *****************"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 328
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService;->mSyncState:I

    .line 330
    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 133
    const-string v0, "SnsLiSync"

    const-string v1, "***************** SnsLiSyncAdapterProfileFeedService : onBind !!! *****************"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 136
    iget-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService;->mSyncAdapter:Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService$SyncAdapterImpl;

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService$SyncAdapterImpl;->getSyncAdapterBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 118
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 120
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/SnsApplication;->getSvcMgr()Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    .line 121
    iget-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService;->mSyncAdapter:Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService$SyncAdapterImpl;

    if-nez v0, :cond_0

    .line 122
    new-instance v0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService$SyncAdapterImpl;

    invoke-direct {v0, p0, p0}, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService$SyncAdapterImpl;-><init>(Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService;->mSyncAdapter:Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService$SyncAdapterImpl;

    .line 125
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;

    if-nez v0, :cond_1

    .line 126
    new-instance v0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;

    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;

    .line 128
    :cond_1
    return-void
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 142
    const/4 v0, 0x0

    .line 143
    .local v0, "bActive":Z
    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService;->mAccount:Landroid/accounts/Account;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService;->mAuthority:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 144
    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService;->mAccount:Landroid/accounts/Account;

    iget-object v2, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService;->mAuthority:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/content/ContentResolver;->isSyncActive(Landroid/accounts/Account;Ljava/lang/String;)Z

    move-result v0

    .line 147
    :cond_0
    const-string v1, "SnsLiSync"

    const-string v2, "***************** SnsLiSyncAdapterProfileFeedService : onUnbind !!! *****************"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 150
    if-eqz v0, :cond_2

    iget v1, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService;->mSyncState:I

    const/4 v2, 0x1

    if-eq v1, v2, :cond_1

    iget v1, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService;->mSyncState:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_2

    .line 152
    :cond_1
    const-string v1, "SnsLiSync"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SnsLiSyncAdapterProfileFeedService : onUnbind : SYNC ERROR : performSync was stopped by forced abort or pending problem!!! "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService;->mSyncState:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 159
    :goto_0
    invoke-super {p0, p1}, Landroid/app/Service;->onUnbind(Landroid/content/Intent;)Z

    move-result v1

    return v1

    .line 157
    :cond_2
    const-string v1, "SnsLiSync"

    const-string v2, "SnsLiSyncAdapterProfileFeedService : onUnbind : COMPLETE STATE!!!"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public performSync()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/accounts/OperationCanceledException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    .line 164
    const-string v3, "SnsLiSync"

    const-string v4, "***************** SnsLiSyncAdapterProfileFeedService : performSync - START !!! *****************"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 167
    iput v5, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService;->mSyncState:I

    .line 168
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/sns3/SnsApplication;->getSvcMgr()Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/sns3/svc/SnsSvcMgr;->getTokenMgr()Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;

    move-result-object v3

    const-string v4, "linkedin"

    invoke-virtual {v3, v4}, Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;->getToken(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/sns3/svc/sp/linkedin/SnsLiToken;

    .line 171
    .local v2, "mLiToken":Lcom/sec/android/app/sns3/svc/sp/linkedin/SnsLiToken;
    :try_start_0
    invoke-virtual {v2}, Lcom/sec/android/app/sns3/svc/sp/linkedin/SnsLiToken;->getTokenState()I

    move-result v3

    if-eq v3, v5, :cond_0

    invoke-virtual {v2}, Lcom/sec/android/app/sns3/svc/sp/linkedin/SnsLiToken;->getTokenState()I

    move-result v3

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    .line 173
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService;->handleSessionExpired()I

    move-result v3

    iput v3, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService;->mSyncState:I

    .line 174
    new-instance v3, Ljava/lang/Exception;

    const-string v4, "Session expired or invalid!!!"

    invoke-direct {v3, v4}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 277
    :catch_0
    move-exception v1

    .line 278
    .local v1, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 279
    const-string v3, "SnsLiSync"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "SnsLiSyncAdapterProfileFeedService : EXCEPTION !!! "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 281
    iget-object v3, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService;->mSyncResult:Landroid/content/SyncResult;

    iget-object v3, v3, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    const-wide/16 v4, 0x1

    iput-wide v4, v3, Landroid/content/SyncStats;->numConflictDetectedExceptions:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 284
    invoke-direct {p0}, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService;->invokeBroadcast()V

    .line 285
    const-string v3, "SnsLiSync"

    const-string v4, "***************** SnsLiSyncAdapterProfileFeedService : performSync - FINISHED !!! *****************"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 290
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_0
    return-void

    .line 176
    :cond_1
    :try_start_2
    new-instance v0, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetProfileFeeds;

    iget-object v3, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/sns3/SnsApplication;->getAgentMgr()Lcom/sec/android/app/sns3/agent/SnsAgentMgr;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/sns3/agent/SnsAgentMgr;->getCommandMgr()Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;->getCommandMgrHandle()Landroid/os/Handler;

    move-result-object v4

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v0, v3, v4, v5}, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetProfileFeeds;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Handler;Landroid/content/Context;)V

    .line 180
    .local v0, "cmdSyncProfileFeeds":Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;
    new-instance v3, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService$1;

    invoke-direct {v3, p0}, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService$1;-><init>(Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService;)V

    invoke-virtual {v0, v3}, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->setCommandCallback(Lcom/sec/android/app/sns3/agent/command/ISnsCommandCallback;)Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;

    .line 267
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->send()I

    .line 269
    iget-object v3, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;

    invoke-virtual {v3}, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;->suspendSync()V

    .line 272
    iget v3, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService;->mSyncState:I

    const/4 v4, -0x1

    if-ne v3, v4, :cond_2

    .line 273
    new-instance v3, Ljava/lang/Exception;

    const-string v4, "[SnsLiSyncAdapterProfileFeedService] updates sync is failed!!!"

    invoke-direct {v3, v4}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 284
    .end local v0    # "cmdSyncProfileFeeds":Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;
    :catchall_0
    move-exception v3

    invoke-direct {p0}, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService;->invokeBroadcast()V

    .line 285
    const-string v4, "SnsLiSync"

    const-string v5, "***************** SnsLiSyncAdapterProfileFeedService : performSync - FINISHED !!! *****************"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    throw v3

    .line 284
    .restart local v0    # "cmdSyncProfileFeeds":Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService;->invokeBroadcast()V

    .line 285
    const-string v3, "SnsLiSync"

    const-string v4, "***************** SnsLiSyncAdapterProfileFeedService : performSync - FINISHED !!! *****************"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
