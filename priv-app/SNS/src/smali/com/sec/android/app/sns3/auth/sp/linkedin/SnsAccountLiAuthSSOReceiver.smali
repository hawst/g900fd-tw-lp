.class public Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOReceiver;
.super Landroid/content/BroadcastReceiver;
.source "SnsAccountLiAuthSSOReceiver.java"


# static fields
.field private static final ACCOUNT_PREFERENCE_NAME:Ljava/lang/String; = "LI_account"

.field private static final APP_ACCOUNT_PREFERENCE_KEY:Ljava/lang/String; = "app_account"

.field private static final INTENT_LI_ACTION_LOGGED_IN:Ljava/lang/String; = "com.linkedin.android.intent.ACTION_LOGGED_IN"

.field private static final INTENT_LI_ACTION_LOGGED_OUT:Ljava/lang/String; = "com.linkedin.android.intent.ACTION_LOGGED_OUT"

.field private static final INTENT_LI_ACTION_SYNC_OFF:Ljava/lang/String; = "com.linkedin.android.intent.ACTION_SYNC_OFF"

.field private static final INTENT_LI_ACTION_SYNC_ON:Ljava/lang/String; = "com.linkedin.android.intent.ACTION_SYNC_ON"

.field public static final SNS_ACCOUNT_CHECK:Ljava/lang/String; = "com.sec.android.app.sns3.auth.ACTION_CHECK_ACCOUNT"

.field private static final SNS_ACCOUNT_PREFERENCE_KEY:Ljava/lang/String; = "sns_account"

.field public static final SNS_LOGIN_LINKEDIN_NOTIFICATION:Ljava/lang/String; = "com.sec.android.app.sns3.auth.sp.linkedin.SNS_LOGIN_LINKEDIN_NOTIFICATION"


# instance fields
.field private final TAG:Ljava/lang/String;

.field private mSharedPref:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 41
    const-string v0, "LiAuthSSOReceiver"

    iput-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOReceiver;->TAG:Ljava/lang/String;

    return-void
.end method

.method private addLinkedinSSOAccount(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x1

    .line 158
    const-string v1, "LiAuthSSOReceiver"

    const-string v2, "Linkedin SSO Receiver : addLinkedinSSOAccount"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 160
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 161
    .local v0, "i":Landroid/content/Intent;
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 162
    const/high16 v1, 0x800000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 163
    const-string v1, "sso_from_app"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 164
    const-string v1, "skip_sync_setup"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 165
    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 166
    return-void
.end method

.method private registerLoginNotification(Landroid/content/Context;)V
    .locals 11
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 183
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 185
    .local v0, "i":Landroid/content/Intent;
    const-class v6, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;

    invoke-virtual {v0, p1, v6}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 189
    const/high16 v6, 0x10000000

    invoke-virtual {v0, v6}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 190
    const/high16 v6, 0x800000

    invoke-virtual {v0, v6}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 191
    const-string v6, "skip_sso_noti"

    invoke-virtual {v0, v6, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 192
    const/16 v6, 0xbb8

    invoke-static {p1, v6, v0, v9}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 195
    .local v1, "intent":Landroid/app/PendingIntent;
    const v6, 0x7f080030

    new-array v7, v10, [Ljava/lang/Object;

    const v8, 0x7f08002e

    invoke-virtual {p1, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v9

    invoke-virtual {p1, v6, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 197
    .local v4, "ticker":Ljava/lang/String;
    const v6, 0x7f08000a

    invoke-virtual {p1, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 199
    .local v5, "title":Ljava/lang/String;
    new-instance v2, Landroid/app/Notification$Builder;

    invoke-direct {v2, p1}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    .line 200
    .local v2, "noti":Landroid/app/Notification$Builder;
    invoke-virtual {v2, v5}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    .line 201
    invoke-virtual {v2, v4}, Landroid/app/Notification$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    .line 202
    invoke-virtual {v2, v4}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    .line 203
    invoke-virtual {v2, v1}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    .line 204
    invoke-virtual {v2, v9}, Landroid/app/Notification$Builder;->setAutoCancel(Z)Landroid/app/Notification$Builder;

    .line 205
    new-instance v6, Landroid/app/Notification$BigTextStyle;

    invoke-direct {v6}, Landroid/app/Notification$BigTextStyle;-><init>()V

    invoke-virtual {v6, v4}, Landroid/app/Notification$BigTextStyle;->bigText(Ljava/lang/CharSequence;)Landroid/app/Notification$BigTextStyle;

    move-result-object v6

    invoke-virtual {v2, v6}, Landroid/app/Notification$Builder;->setStyle(Landroid/app/Notification$Style;)Landroid/app/Notification$Builder;

    .line 206
    invoke-virtual {v2, v10}, Landroid/app/Notification$Builder;->setDefaults(I)Landroid/app/Notification$Builder;

    .line 207
    const v6, 0x7f02001e

    invoke-virtual {v2, v6}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    .line 209
    const-string v6, "notification"

    invoke-virtual {p1, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/NotificationManager;

    .line 212
    .local v3, "notiMgr":Landroid/app/NotificationManager;
    const/16 v6, 0xc1c

    invoke-virtual {v2}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v7

    invoke-virtual {v3, v6, v7}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 216
    return-void
.end method

.method private removeLinkedinSSOAccount(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    .line 169
    const-string v2, "LiAuthSSOReceiver"

    const-string v3, "Linkedin SSO Receiver : removeLinkedinSSOAccount"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 171
    const-string v2, "notification"

    invoke-virtual {p1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/NotificationManager;

    .line 173
    .local v1, "nm":Landroid/app/NotificationManager;
    const/16 v2, 0xc1c

    invoke-virtual {v1, v2}, Landroid/app/NotificationManager;->cancel(I)V

    .line 175
    invoke-static {p1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v2

    const-string v3, "com.sec.android.app.sns3.linkedin"

    invoke-virtual {v2, v3}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 177
    .local v0, "accounts":[Landroid/accounts/Account;
    array-length v2, v0

    if-lez v2, :cond_0

    .line 178
    invoke-static {p1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v2

    const/4 v3, 0x0

    aget-object v3, v0, v3

    invoke-virtual {v2, v3, v4, v4}, Landroid/accounts/AccountManager;->removeAccount(Landroid/accounts/Account;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    .line 180
    :cond_0
    return-void
.end method

.method private storeAccountStatus(II)V
    .locals 2
    .param p1, "snsAccount"    # I
    .param p2, "appAccount"    # I

    .prologue
    .line 151
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOReceiver;->mSharedPref:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 152
    .local v0, "prefEditor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "sns_account"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 153
    const-string v1, "app_account"

    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 154
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 155
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 13
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 59
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    .line 60
    .local v2, "action":Ljava/lang/String;
    const-string v10, "LI_account"

    const/4 v11, 0x0

    invoke-virtual {p1, v10, v11}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v10

    iput-object v10, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOReceiver;->mSharedPref:Landroid/content/SharedPreferences;

    .line 62
    const-string v10, "LiAuthSSOReceiver"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Linkedin SSO Receiver : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 64
    invoke-static {p1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v10

    const-string v11, "com.sec.android.app.sns3.linkedin"

    invoke-virtual {v10, v11}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v9

    .line 67
    .local v9, "sns3Accounts":[Landroid/accounts/Account;
    invoke-static {p1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v10

    const-string v11, "com.linkedin.android"

    invoke-virtual {v10, v11}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v3

    .line 70
    .local v3, "appAccounts":[Landroid/accounts/Account;
    const-string v10, "com.sec.android.app.sns3.auth.ACTION_CHECK_ACCOUNT"

    invoke-virtual {v10, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 71
    array-length v10, v9

    array-length v11, v3

    invoke-direct {p0, v10, v11}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOReceiver;->storeAccountStatus(II)V

    .line 148
    :cond_0
    :goto_0
    return-void

    .line 72
    :cond_1
    const-string v10, "com.sec.android.app.sns3.auth.sp.linkedin.SNS_LOGIN_LINKEDIN_NOTIFICATION"

    invoke-virtual {v10, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 73
    array-length v10, v3

    if-lez v10, :cond_0

    array-length v10, v9

    if-nez v10, :cond_0

    .line 74
    invoke-direct {p0, p1}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOReceiver;->registerLoginNotification(Landroid/content/Context;)V

    goto :goto_0

    .line 76
    :cond_2
    const-string v10, "com.linkedin.android.intent.ACTION_LOGGED_IN"

    invoke-virtual {v10, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 77
    array-length v10, v9

    if-nez v10, :cond_0

    invoke-static {}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->isRunning()Z

    move-result v10

    if-nez v10, :cond_0

    .line 79
    invoke-direct {p0, p1}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOReceiver;->addLinkedinSSOAccount(Landroid/content/Context;)V

    goto :goto_0

    .line 81
    :cond_3
    const-string v10, "com.linkedin.android.intent.ACTION_LOGGED_OUT"

    invoke-virtual {v10, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 82
    array-length v10, v9

    if-lez v10, :cond_0

    .line 83
    invoke-direct {p0, p1}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOReceiver;->removeLinkedinSSOAccount(Landroid/content/Context;)V

    goto :goto_0

    .line 85
    :cond_4
    const-string v10, "com.linkedin.android.intent.ACTION_SYNC_ON"

    invoke-virtual {v10, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 86
    array-length v10, v9

    if-nez v10, :cond_0

    .line 87
    invoke-direct {p0, p1}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOReceiver;->addLinkedinSSOAccount(Landroid/content/Context;)V

    goto :goto_0

    .line 89
    :cond_5
    const-string v10, "com.linkedin.android.intent.ACTION_SYNC_OFF"

    invoke-virtual {v10, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 90
    array-length v10, v9

    if-lez v10, :cond_0

    .line 91
    invoke-direct {p0, p1}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOReceiver;->removeLinkedinSSOAccount(Landroid/content/Context;)V

    goto :goto_0

    .line 93
    :cond_6
    const-string v10, "android.accounts.LOGIN_ACCOUNTS_CHANGED"

    invoke-virtual {v10, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 94
    const-string v10, "operation"

    invoke-virtual {p2, v10}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 95
    .local v8, "operation":Ljava/lang/String;
    const/4 v4, 0x0

    .line 97
    .local v4, "changedAccountType":Ljava/lang/String;
    if-eqz v8, :cond_c

    .line 98
    const-string v10, "account"

    invoke-virtual {p2, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    .line 99
    .local v0, "account":Landroid/accounts/Account;
    const-string v10, "accounts"

    invoke-virtual {p2, v10}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    .line 100
    .local v1, "accounts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/accounts/Account;>;"
    if-nez v0, :cond_7

    if-eqz v1, :cond_7

    .line 101
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_1
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v10

    if-ge v5, v10, :cond_7

    .line 102
    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "account":Landroid/accounts/Account;
    check-cast v0, Landroid/accounts/Account;

    .line 103
    .restart local v0    # "account":Landroid/accounts/Account;
    if-eqz v0, :cond_b

    const-string v10, "com.linkedin.android"

    iget-object v11, v0, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_b

    .line 109
    .end local v5    # "i":I
    :cond_7
    if-eqz v0, :cond_8

    .line 110
    iget-object v4, v0, Landroid/accounts/Account;->type:Ljava/lang/String;

    .line 131
    .end local v0    # "account":Landroid/accounts/Account;
    .end local v1    # "accounts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/accounts/Account;>;"
    :cond_8
    :goto_2
    if-eqz v4, :cond_9

    .line 132
    const-string v10, "LiAuthSSOReceiver"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 135
    :cond_9
    const-string v10, "add"

    invoke-virtual {v10, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_10

    const-string v10, "com.linkedin.android"

    invoke-virtual {v10, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_10

    .line 137
    array-length v10, v9

    if-nez v10, :cond_a

    invoke-static {}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->isRunning()Z

    move-result v10

    if-nez v10, :cond_a

    .line 139
    invoke-direct {p0, p1}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOReceiver;->addLinkedinSSOAccount(Landroid/content/Context;)V

    .line 146
    :cond_a
    :goto_3
    array-length v10, v9

    array-length v11, v3

    invoke-direct {p0, v10, v11}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOReceiver;->storeAccountStatus(II)V

    goto/16 :goto_0

    .line 101
    .restart local v0    # "account":Landroid/accounts/Account;
    .restart local v1    # "accounts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/accounts/Account;>;"
    .restart local v5    # "i":I
    :cond_b
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 113
    .end local v0    # "account":Landroid/accounts/Account;
    .end local v1    # "accounts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/accounts/Account;>;"
    .end local v5    # "i":I
    :cond_c
    iget-object v10, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOReceiver;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v11, "app_account"

    const/4 v12, 0x0

    invoke-interface {v10, v11, v12}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v6

    .line 114
    .local v6, "lastAppAccountStatus":I
    iget-object v10, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOReceiver;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v11, "sns_account"

    const/4 v12, 0x0

    invoke-interface {v10, v11, v12}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v7

    .line 116
    .local v7, "lastSnsAccountStatus":I
    array-length v10, v3

    if-le v10, v6, :cond_d

    .line 117
    const-string v4, "com.linkedin.android"

    .line 118
    const-string v8, "add"

    goto :goto_2

    .line 119
    :cond_d
    array-length v10, v3

    if-ge v10, v6, :cond_e

    .line 120
    const-string v4, "com.linkedin.android"

    .line 121
    const-string v8, "remove"

    goto :goto_2

    .line 122
    :cond_e
    array-length v10, v9

    if-le v10, v7, :cond_f

    .line 123
    const-string v4, "com.sec.android.app.sns3.linkedin"

    .line 124
    const-string v8, "add"

    goto :goto_2

    .line 125
    :cond_f
    array-length v10, v9

    if-ge v10, v7, :cond_8

    .line 126
    const-string v4, "com.sec.android.app.sns3.linkedin"

    .line 127
    const-string v8, "remove"

    goto :goto_2

    .line 141
    .end local v6    # "lastAppAccountStatus":I
    .end local v7    # "lastSnsAccountStatus":I
    :cond_10
    const-string v10, "remove"

    invoke-virtual {v10, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_a

    const-string v10, "com.linkedin.android"

    invoke-virtual {v10, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_a

    .line 143
    invoke-direct {p0, p1}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOReceiver;->removeLinkedinSSOAccount(Landroid/content/Context;)V

    goto :goto_3
.end method
