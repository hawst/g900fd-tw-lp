.class public Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncIntervalActivity;
.super Landroid/preference/PreferenceActivity;
.source "SnsAccountInSyncIntervalActivity.java"


# static fields
.field private static final DEFAULT_SYNC_INTERVAL:J = 0x0L

.field private static final SYNC_INTERVAL_KEY:Ljava/lang/String; = "sync_interval"

.field private static final TAG:Ljava/lang/String; = "SnsAccountInAuth"


# instance fields
.field private mAppContext:Landroid/content/Context;

.field private mFeedsIntervalPreference:Landroid/preference/ListPreference;

.field private mGalleryIntervalPreference:Landroid/preference/ListPreference;

.field private mGallerySharedPref:Landroid/content/SharedPreferences;

.field private mProfileFeedSharedPref:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 47
    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    .line 57
    iput-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncIntervalActivity;->mGalleryIntervalPreference:Landroid/preference/ListPreference;

    .line 58
    iput-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncIntervalActivity;->mFeedsIntervalPreference:Landroid/preference/ListPreference;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncIntervalActivity;)Landroid/preference/ListPreference;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncIntervalActivity;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncIntervalActivity;->mGalleryIntervalPreference:Landroid/preference/ListPreference;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncIntervalActivity;)Landroid/preference/ListPreference;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncIntervalActivity;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncIntervalActivity;->mFeedsIntervalPreference:Landroid/preference/ListPreference;

    return-object v0
.end method


# virtual methods
.method isTable()Z
    .locals 2

    .prologue
    .line 291
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncIntervalActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit8 v0, v0, 0x3

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncIntervalActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    .line 294
    :cond_0
    const/4 v0, 0x1

    .line 296
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 11
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 66
    iput-object p0, p0, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncIntervalActivity;->mAppContext:Landroid/content/Context;

    .line 68
    iget-object v7, p0, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncIntervalActivity;->mAppContext:Landroid/content/Context;

    invoke-static {v7}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->isLightTheme(Landroid/content/Context;)Z

    move-result v7

    if-eqz v7, :cond_a

    const v7, 0x103012b

    :goto_0
    invoke-virtual {p0, v7}, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncIntervalActivity;->setTheme(I)V

    .line 71
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 73
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncIntervalActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 74
    .local v0, "actionBar":Landroid/app/ActionBar;
    const/4 v7, 0x1

    invoke-virtual {v0, v7}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 75
    const/4 v7, 0x0

    invoke-virtual {v0, v7}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 77
    const v7, 0x7f030005

    invoke-virtual {p0, v7}, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncIntervalActivity;->setContentView(I)V

    .line 78
    const v7, 0x7f080046

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    const v10, 0x7f080025

    invoke-virtual {p0, v10}, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncIntervalActivity;->getString(I)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-virtual {p0, v7, v8}, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncIntervalActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncIntervalActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 79
    const v7, 0x7f04000d

    invoke-virtual {p0, v7}, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncIntervalActivity;->addPreferencesFromResource(I)V

    .line 81
    const-string v7, "sync_interval"

    invoke-virtual {p0, v7}, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncIntervalActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v3

    check-cast v3, Landroid/preference/PreferenceScreen;

    .line 83
    .local v3, "preferenceScreen":Landroid/preference/PreferenceScreen;
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v7

    const-string v8, "com.sec.android.app.sns3.instagram"

    invoke-virtual {v7, v8}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v6

    .line 86
    .local v6, "sns3Account":[Landroid/accounts/Account;
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/sns3/SnsApplication;->getFeatureMgr()Lcom/sec/android/app/sns3/SnsFeatureManager;

    move-result-object v1

    .line 87
    .local v1, "featureMgr":Lcom/sec/android/app/sns3/SnsFeatureManager;
    const/4 v5, 0x0

    .line 88
    .local v5, "providerLabel":Ljava/lang/CharSequence;
    const/4 v4, 0x0

    .line 91
    .local v4, "providerInfo":Landroid/content/pm/ProviderInfo;
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncIntervalActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    const-string v8, "content://com.sec.android.gallery3d.instagram.contentprovider/support_instagram"

    invoke-static {v8}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/content/ContentResolver;->getType(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    .line 93
    .local v2, "isGallerySync":Ljava/lang/String;
    const-string v7, "true"

    invoke-virtual {v7, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 94
    const/4 v5, 0x0

    .line 95
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncIntervalActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v7

    const-string v8, "com.sec.android.gallery3d.instagram.contentprovider"

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v9}, Landroid/content/pm/PackageManager;->resolveContentProvider(Ljava/lang/String;I)Landroid/content/pm/ProviderInfo;

    move-result-object v4

    .line 96
    if-eqz v4, :cond_0

    .line 97
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncIntervalActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v7

    invoke-virtual {v4, v7}, Landroid/content/pm/ProviderInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v5

    .line 99
    :cond_0
    if-eqz v5, :cond_1

    invoke-interface {v5}, Ljava/lang/CharSequence;->length()I

    move-result v7

    if-nez v7, :cond_2

    .line 100
    :cond_1
    const-string v7, "SnsAccountInAuth"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "providerLabel is null - "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 101
    const-string v5, "Gallery"

    .line 104
    :cond_2
    new-instance v7, Landroid/preference/ListPreference;

    invoke-direct {v7, p0}, Landroid/preference/ListPreference;-><init>(Landroid/content/Context;)V

    iput-object v7, p0, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncIntervalActivity;->mGalleryIntervalPreference:Landroid/preference/ListPreference;

    .line 105
    iget-object v7, p0, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncIntervalActivity;->mGalleryIntervalPreference:Landroid/preference/ListPreference;

    const v8, 0x7f080009

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    aput-object v5, v9, v10

    invoke-virtual {p0, v8, v9}, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncIntervalActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/preference/ListPreference;->setTitle(Ljava/lang/CharSequence;)V

    .line 107
    iget-object v7, p0, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncIntervalActivity;->mGalleryIntervalPreference:Landroid/preference/ListPreference;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/preference/ListPreference;->setPersistent(Z)V

    .line 108
    iget-object v7, p0, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncIntervalActivity;->mGalleryIntervalPreference:Landroid/preference/ListPreference;

    const/high16 v8, 0x7f050000

    invoke-virtual {v7, v8}, Landroid/preference/ListPreference;->setEntries(I)V

    .line 109
    iget-object v7, p0, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncIntervalActivity;->mGalleryIntervalPreference:Landroid/preference/ListPreference;

    const v8, 0x7f050001

    invoke-virtual {v7, v8}, Landroid/preference/ListPreference;->setEntryValues(I)V

    .line 110
    sget-object v7, Lcom/sec/android/app/sns3/SnsFeatureManager;->ANGLE_INDICATION_SUPPORTED:Ljava/lang/Boolean;

    invoke-virtual {v7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    if-eqz v7, :cond_3

    .line 111
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncIntervalActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->isLightTheme(Landroid/content/Context;)Z

    move-result v7

    if-eqz v7, :cond_b

    .line 112
    iget-object v7, p0, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncIntervalActivity;->mGalleryIntervalPreference:Landroid/preference/ListPreference;

    const v8, 0x7f030003

    invoke-virtual {v7, v8}, Landroid/preference/ListPreference;->setWidgetLayoutResource(I)V

    .line 117
    :cond_3
    :goto_1
    iget-object v7, p0, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncIntervalActivity;->mGalleryIntervalPreference:Landroid/preference/ListPreference;

    const v8, 0x7f080009

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    aput-object v5, v9, v10

    invoke-virtual {p0, v8, v9}, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncIntervalActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/preference/ListPreference;->setDialogTitle(Ljava/lang/CharSequence;)V

    .line 120
    array-length v7, v6

    if-lez v7, :cond_c

    .line 121
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncIntervalActivity;->readGallerySyncInterval()V

    .line 123
    iget-object v7, p0, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncIntervalActivity;->mGalleryIntervalPreference:Landroid/preference/ListPreference;

    new-instance v8, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncIntervalActivity$1;

    invoke-direct {v8, p0}, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncIntervalActivity$1;-><init>(Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncIntervalActivity;)V

    invoke-virtual {v7, v8}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 162
    :goto_2
    iget-object v7, p0, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncIntervalActivity;->mGalleryIntervalPreference:Landroid/preference/ListPreference;

    invoke-virtual {v3, v7}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 166
    :cond_4
    invoke-virtual {v1}, Lcom/sec/android/app/sns3/SnsFeatureManager;->isSyncAdapterForProfileFeedsNeeded()Z

    move-result v7

    if-eqz v7, :cond_9

    .line 167
    const/4 v5, 0x0

    .line 168
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncIntervalActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v7

    const-string v8, "com.sec.android.app.sns3.life"

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v9}, Landroid/content/pm/PackageManager;->resolveContentProvider(Ljava/lang/String;I)Landroid/content/pm/ProviderInfo;

    move-result-object v4

    .line 169
    if-eqz v4, :cond_5

    .line 170
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncIntervalActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v7

    invoke-virtual {v4, v7}, Landroid/content/pm/ProviderInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v5

    .line 172
    :cond_5
    if-eqz v5, :cond_6

    invoke-interface {v5}, Ljava/lang/CharSequence;->length()I

    move-result v7

    if-nez v7, :cond_7

    .line 173
    :cond_6
    const-string v7, "SnsAccountInAuth"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "providerLabel is null - "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 174
    const-string v5, "Life Times"

    .line 177
    :cond_7
    new-instance v7, Landroid/preference/ListPreference;

    invoke-direct {v7, p0}, Landroid/preference/ListPreference;-><init>(Landroid/content/Context;)V

    iput-object v7, p0, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncIntervalActivity;->mFeedsIntervalPreference:Landroid/preference/ListPreference;

    .line 178
    iget-object v7, p0, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncIntervalActivity;->mFeedsIntervalPreference:Landroid/preference/ListPreference;

    const v8, 0x7f080009

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    aput-object v5, v9, v10

    invoke-virtual {p0, v8, v9}, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncIntervalActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/preference/ListPreference;->setTitle(Ljava/lang/CharSequence;)V

    .line 180
    iget-object v7, p0, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncIntervalActivity;->mFeedsIntervalPreference:Landroid/preference/ListPreference;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/preference/ListPreference;->setPersistent(Z)V

    .line 181
    iget-object v7, p0, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncIntervalActivity;->mFeedsIntervalPreference:Landroid/preference/ListPreference;

    const/high16 v8, 0x7f050000

    invoke-virtual {v7, v8}, Landroid/preference/ListPreference;->setEntries(I)V

    .line 182
    iget-object v7, p0, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncIntervalActivity;->mFeedsIntervalPreference:Landroid/preference/ListPreference;

    const v8, 0x7f050001

    invoke-virtual {v7, v8}, Landroid/preference/ListPreference;->setEntryValues(I)V

    .line 183
    sget-object v7, Lcom/sec/android/app/sns3/SnsFeatureManager;->ANGLE_INDICATION_SUPPORTED:Ljava/lang/Boolean;

    invoke-virtual {v7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    if-eqz v7, :cond_8

    .line 184
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncIntervalActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->isLightTheme(Landroid/content/Context;)Z

    move-result v7

    if-eqz v7, :cond_d

    .line 185
    iget-object v7, p0, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncIntervalActivity;->mFeedsIntervalPreference:Landroid/preference/ListPreference;

    const v8, 0x7f030003

    invoke-virtual {v7, v8}, Landroid/preference/ListPreference;->setWidgetLayoutResource(I)V

    .line 190
    :cond_8
    :goto_3
    iget-object v7, p0, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncIntervalActivity;->mFeedsIntervalPreference:Landroid/preference/ListPreference;

    const v8, 0x7f080009

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    aput-object v5, v9, v10

    invoke-virtual {p0, v8, v9}, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncIntervalActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/preference/ListPreference;->setDialogTitle(Ljava/lang/CharSequence;)V

    .line 193
    array-length v7, v6

    if-lez v7, :cond_e

    .line 194
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncIntervalActivity;->readFeedsSyncInterval()V

    .line 196
    iget-object v7, p0, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncIntervalActivity;->mFeedsIntervalPreference:Landroid/preference/ListPreference;

    new-instance v8, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncIntervalActivity$2;

    invoke-direct {v8, p0}, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncIntervalActivity$2;-><init>(Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncIntervalActivity;)V

    invoke-virtual {v7, v8}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 235
    :goto_4
    iget-object v7, p0, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncIntervalActivity;->mFeedsIntervalPreference:Landroid/preference/ListPreference;

    invoke-virtual {v3, v7}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 237
    :cond_9
    return-void

    .line 68
    .end local v0    # "actionBar":Landroid/app/ActionBar;
    .end local v1    # "featureMgr":Lcom/sec/android/app/sns3/SnsFeatureManager;
    .end local v2    # "isGallerySync":Ljava/lang/String;
    .end local v3    # "preferenceScreen":Landroid/preference/PreferenceScreen;
    .end local v4    # "providerInfo":Landroid/content/pm/ProviderInfo;
    .end local v5    # "providerLabel":Ljava/lang/CharSequence;
    .end local v6    # "sns3Account":[Landroid/accounts/Account;
    :cond_a
    const v7, 0x1030128

    goto/16 :goto_0

    .line 114
    .restart local v0    # "actionBar":Landroid/app/ActionBar;
    .restart local v1    # "featureMgr":Lcom/sec/android/app/sns3/SnsFeatureManager;
    .restart local v2    # "isGallerySync":Ljava/lang/String;
    .restart local v3    # "preferenceScreen":Landroid/preference/PreferenceScreen;
    .restart local v4    # "providerInfo":Landroid/content/pm/ProviderInfo;
    .restart local v5    # "providerLabel":Ljava/lang/CharSequence;
    .restart local v6    # "sns3Account":[Landroid/accounts/Account;
    :cond_b
    iget-object v7, p0, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncIntervalActivity;->mGalleryIntervalPreference:Landroid/preference/ListPreference;

    const v8, 0x7f030002

    invoke-virtual {v7, v8}, Landroid/preference/ListPreference;->setWidgetLayoutResource(I)V

    goto/16 :goto_1

    .line 159
    :cond_c
    iget-object v7, p0, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncIntervalActivity;->mGalleryIntervalPreference:Landroid/preference/ListPreference;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/preference/ListPreference;->setEnabled(Z)V

    goto/16 :goto_2

    .line 187
    :cond_d
    iget-object v7, p0, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncIntervalActivity;->mFeedsIntervalPreference:Landroid/preference/ListPreference;

    const v8, 0x7f030002

    invoke-virtual {v7, v8}, Landroid/preference/ListPreference;->setWidgetLayoutResource(I)V

    goto :goto_3

    .line 232
    :cond_e
    iget-object v7, p0, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncIntervalActivity;->mFeedsIntervalPreference:Landroid/preference/ListPreference;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/preference/ListPreference;->setEnabled(Z)V

    goto :goto_4
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 242
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 247
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 244
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncIntervalActivity;->finish()V

    .line 245
    const/4 v0, 0x1

    goto :goto_0

    .line 242
    nop

    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 252
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onResume()V

    .line 253
    return-void
.end method

.method public readFeedsSyncInterval()V
    .locals 6

    .prologue
    .line 273
    const-string v1, "IN_profile_feed_sync_interval"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncIntervalActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncIntervalActivity;->mProfileFeedSharedPref:Landroid/content/SharedPreferences;

    .line 275
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncIntervalActivity;->mProfileFeedSharedPref:Landroid/content/SharedPreferences;

    const-string v2, "IN_profile_feed_sync_interval"

    const-wide/16 v4, 0x0

    invoke-interface {v1, v2, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    .line 278
    .local v0, "savedFeedsInterval":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncIntervalActivity;->mFeedsIntervalPreference:Landroid/preference/ListPreference;

    invoke-virtual {v1, v0}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 280
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncIntervalActivity;->mFeedsIntervalPreference:Landroid/preference/ListPreference;

    invoke-virtual {v1}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 281
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncIntervalActivity;->mFeedsIntervalPreference:Landroid/preference/ListPreference;

    iget-object v2, p0, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncIntervalActivity;->mFeedsIntervalPreference:Landroid/preference/ListPreference;

    invoke-virtual {v2}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 282
    :cond_0
    return-void
.end method

.method public readGallerySyncInterval()V
    .locals 6

    .prologue
    .line 256
    const-string v1, "IN_gallery_sync_interval"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncIntervalActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncIntervalActivity;->mGallerySharedPref:Landroid/content/SharedPreferences;

    .line 258
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncIntervalActivity;->mGallerySharedPref:Landroid/content/SharedPreferences;

    const-string v2, "IN_gallery_sync_interval"

    const-wide/16 v4, 0x0

    invoke-interface {v1, v2, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    .line 261
    .local v0, "savedGalleryInterval":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncIntervalActivity;->mGalleryIntervalPreference:Landroid/preference/ListPreference;

    invoke-virtual {v1, v0}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 263
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncIntervalActivity;->mGalleryIntervalPreference:Landroid/preference/ListPreference;

    invoke-virtual {v1}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 264
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncIntervalActivity;->mGalleryIntervalPreference:Landroid/preference/ListPreference;

    iget-object v2, p0, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncIntervalActivity;->mGalleryIntervalPreference:Landroid/preference/ListPreference;

    invoke-virtual {v2}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 265
    :cond_0
    return-void
.end method

.method public writeFeedsInterval(Ljava/lang/Long;)V
    .locals 4
    .param p1, "value"    # Ljava/lang/Long;

    .prologue
    .line 285
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncIntervalActivity;->mProfileFeedSharedPref:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 286
    .local v0, "profileFeedEditor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "IN_profile_feed_sync_interval"

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 287
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 288
    return-void
.end method

.method public writeGalleryInterval(Ljava/lang/Long;)V
    .locals 4
    .param p1, "value"    # Ljava/lang/Long;

    .prologue
    .line 268
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/instagram/SnsAccountInSyncIntervalActivity;->mGallerySharedPref:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 269
    .local v0, "galleryEditor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "IN_gallery_sync_interval"

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 270
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 271
    return-void
.end method
