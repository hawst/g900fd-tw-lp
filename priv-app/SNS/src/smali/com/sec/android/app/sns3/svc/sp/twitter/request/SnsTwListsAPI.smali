.class public final enum Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;
.super Ljava/lang/Enum;
.source "SnsTwListsAPI.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI$Param;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;

.field public static final enum LISTS:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;

.field public static final enum LISTS_ALL:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;

.field public static final enum LISTS_CREATE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;

.field public static final enum LISTS_DESTROY:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;

.field public static final enum LISTS_MEMBERS:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;

.field public static final enum LISTS_MEMBERSHIPS:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;

.field public static final enum LISTS_MEMBERS_CREATE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;

.field public static final enum LISTS_MEMBERS_CREATEALL:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;

.field public static final enum LISTS_MEMBERS_DESTROY:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;

.field public static final enum LISTS_MEMBERS_SHOW:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;

.field public static final enum LISTS_SHOW:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;

.field public static final enum LISTS_STATUSES:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;

.field public static final enum LISTS_SUBSCRIBERS:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;

.field public static final enum LISTS_SUBSCRIBERS_CREATE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;

.field public static final enum LISTS_SUBSCRIBERS_DESTROY:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;

.field public static final enum LISTS_SUBSCRIBERS_SHOW:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;

.field public static final enum LISTS_UPDATE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;


# instance fields
.field private mHttpMethod:Ljava/lang/String;

.field private mUrl:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 28
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;

    const-string v1, "LISTS_ALL"

    const-string v2, "/lists/all.json"

    const-string v3, "GET"

    invoke-direct {v0, v1, v5, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;->LISTS_ALL:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;

    .line 29
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;

    const-string v1, "LISTS_STATUSES"

    const-string v2, "/lists/statuses.json"

    const-string v3, "GET"

    invoke-direct {v0, v1, v6, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;->LISTS_STATUSES:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;

    .line 30
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;

    const-string v1, "LISTS_MEMBERS_DESTROY"

    const-string v2, "/lists/members/destroy.json"

    const-string v3, "POST"

    invoke-direct {v0, v1, v7, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;->LISTS_MEMBERS_DESTROY:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;

    .line 31
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;

    const-string v1, "LISTS_MEMBERSHIPS"

    const-string v2, "/lists/memberships.json"

    const-string v3, "GET"

    invoke-direct {v0, v1, v8, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;->LISTS_MEMBERSHIPS:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;

    .line 32
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;

    const-string v1, "LISTS_SUBSCRIBERS"

    const-string v2, "/lists/subscribers.json"

    const-string v3, "GET"

    invoke-direct {v0, v1, v9, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;->LISTS_SUBSCRIBERS:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;

    .line 33
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;

    const-string v1, "LISTS_SUBSCRIBERS_CREATE"

    const/4 v2, 0x5

    const-string v3, "/lists/subscribers/create.json"

    const-string v4, "POST"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;->LISTS_SUBSCRIBERS_CREATE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;

    .line 34
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;

    const-string v1, "LISTS_SUBSCRIBERS_SHOW"

    const/4 v2, 0x6

    const-string v3, "/lists/subscribers/show.json"

    const-string v4, "GET"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;->LISTS_SUBSCRIBERS_SHOW:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;

    .line 35
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;

    const-string v1, "LISTS_SUBSCRIBERS_DESTROY"

    const/4 v2, 0x7

    const-string v3, "/lists/subscribers/destroy.json"

    const-string v4, "POST"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;->LISTS_SUBSCRIBERS_DESTROY:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;

    .line 36
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;

    const-string v1, "LISTS_MEMBERS_CREATEALL"

    const/16 v2, 0x8

    const-string v3, "/lists/members/create_all.json"

    const-string v4, "POST"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;->LISTS_MEMBERS_CREATEALL:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;

    .line 37
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;

    const-string v1, "LISTS_MEMBERS_SHOW"

    const/16 v2, 0x9

    const-string v3, "/lists/members/show.json"

    const-string v4, "GET"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;->LISTS_MEMBERS_SHOW:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;

    .line 38
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;

    const-string v1, "LISTS_MEMBERS"

    const/16 v2, 0xa

    const-string v3, "/lists/members.json"

    const-string v4, "GET"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;->LISTS_MEMBERS:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;

    .line 39
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;

    const-string v1, "LISTS_MEMBERS_CREATE"

    const/16 v2, 0xb

    const-string v3, "/lists/members/create.json"

    const-string v4, "POST"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;->LISTS_MEMBERS_CREATE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;

    .line 40
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;

    const-string v1, "LISTS_DESTROY"

    const/16 v2, 0xc

    const-string v3, "/lists/destroy.json"

    const-string v4, "POST"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;->LISTS_DESTROY:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;

    .line 41
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;

    const-string v1, "LISTS_UPDATE"

    const/16 v2, 0xd

    const-string v3, "/lists/update.json"

    const-string v4, "POST"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;->LISTS_UPDATE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;

    .line 42
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;

    const-string v1, "LISTS_CREATE"

    const/16 v2, 0xe

    const-string v3, "/lists/create.json"

    const-string v4, "POST"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;->LISTS_CREATE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;

    .line 43
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;

    const-string v1, "LISTS"

    const/16 v2, 0xf

    const-string v3, "/lists.json"

    const-string v4, "GET"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;->LISTS:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;

    .line 44
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;

    const-string v1, "LISTS_SHOW"

    const/16 v2, 0x10

    const-string v3, "/lists/show.json"

    const-string v4, "GET"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;->LISTS_SHOW:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;

    .line 26
    const/16 v0, 0x11

    new-array v0, v0, [Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;

    sget-object v1, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;->LISTS_ALL:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;->LISTS_STATUSES:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;->LISTS_MEMBERS_DESTROY:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;

    aput-object v1, v0, v7

    sget-object v1, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;->LISTS_MEMBERSHIPS:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;

    aput-object v1, v0, v8

    sget-object v1, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;->LISTS_SUBSCRIBERS:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;

    aput-object v1, v0, v9

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;->LISTS_SUBSCRIBERS_CREATE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;->LISTS_SUBSCRIBERS_SHOW:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;->LISTS_SUBSCRIBERS_DESTROY:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;->LISTS_MEMBERS_CREATEALL:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;->LISTS_MEMBERS_SHOW:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;->LISTS_MEMBERS:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;->LISTS_MEMBERS_CREATE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;->LISTS_DESTROY:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;->LISTS_UPDATE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;->LISTS_CREATE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;->LISTS:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;->LISTS_SHOW:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;->$VALUES:[Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p3, "url"    # Ljava/lang/String;
    .param p4, "httpMethod"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 81
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 82
    iput-object p3, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;->mUrl:Ljava/lang/String;

    .line 83
    iput-object p4, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;->mHttpMethod:Ljava/lang/String;

    .line 84
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 26
    const-class v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;
    .locals 1

    .prologue
    .line 26
    sget-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;->$VALUES:[Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;

    invoke-virtual {v0}, [Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;

    return-object v0
.end method


# virtual methods
.method public getHttpMethod()Ljava/lang/String;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;->mHttpMethod:Ljava/lang/String;

    return-object v0
.end method

.method public getUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;->mUrl:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;->mUrl:Ljava/lang/String;

    return-object v0
.end method
