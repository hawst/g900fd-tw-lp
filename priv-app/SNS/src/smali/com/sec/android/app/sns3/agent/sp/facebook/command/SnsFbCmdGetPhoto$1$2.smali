.class Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPhoto$1$2;
.super Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqGetPhoto;
.source "SnsFbCmdGetPhoto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPhoto$1;->onReqRespond(IZIILandroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseConvertID;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPhoto$1;


# direct methods
.method constructor <init>(Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPhoto$1;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;)V
    .locals 0
    .param p2, "x0"    # Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    .param p3, "x1"    # Ljava/lang/String;

    .prologue
    .line 118
    iput-object p1, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPhoto$1$2;->this$1:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPhoto$1;

    invoke-direct {p0, p2, p3}, Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqGetPhoto;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public onReqRespond(IZIILandroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;)Z
    .locals 9
    .param p1, "reqID"    # I
    .param p2, "bSuccess"    # Z
    .param p3, "httpStatus"    # I
    .param p4, "errorCode"    # I
    .param p5, "reason"    # Landroid/os/Bundle;
    .param p6, "photo"    # Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;

    .prologue
    .line 124
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 125
    .local v3, "values":Landroid/content/ContentValues;
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 126
    .local v4, "valuesComment":Landroid/content/ContentValues;
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    .line 127
    .local v5, "valuesTags":Landroid/content/ContentValues;
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/sns3/SnsApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 129
    .local v1, "cr":Landroid/content/ContentResolver;
    if-eqz p2, :cond_4

    .line 130
    if-eqz p6, :cond_3

    .line 131
    sget-object v6, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$Photo;->CONTENT_URI:Landroid/net/Uri;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "id= \'"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;->mPhotoID:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "\'"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v1, v6, v7, v8}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 134
    invoke-virtual {v3}, Landroid/content/ContentValues;->clear()V

    .line 136
    const-string v6, "id"

    iget-object v7, p6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;->mPhotoID:Ljava/lang/String;

    invoke-virtual {v3, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 137
    iget-object v6, p6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;->mFrom:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFrom;

    if-eqz v6, :cond_0

    .line 138
    const-string v6, "from_id"

    iget-object v7, p6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;->mFrom:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFrom;

    iget-object v7, v7, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFrom;->mFromID:Ljava/lang/String;

    invoke-virtual {v3, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 139
    const-string v6, "from_name"

    iget-object v7, p6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;->mFrom:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFrom;

    iget-object v7, v7, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFrom;->mFromName:Ljava/lang/String;

    invoke-virtual {v3, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    :cond_0
    const-string v6, "name"

    iget-object v7, p6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;->mPhotoName:Ljava/lang/String;

    invoke-virtual {v3, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    const-string v6, "picture"

    iget-object v7, p6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;->mPicture:Ljava/lang/String;

    invoke-virtual {v3, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    const-string v6, "source"

    iget-object v7, p6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;->mSource:Ljava/lang/String;

    invoke-virtual {v3, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    const-string v6, "height"

    iget v7, p6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;->mHeight:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v3, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 145
    const-string v6, "width"

    iget v7, p6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;->mWidth:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v3, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 146
    const-string v6, "link"

    iget-object v7, p6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;->mLink:Ljava/lang/String;

    invoke-virtual {v3, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 147
    const-string v6, "icon"

    iget-object v7, p6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;->mIcon:Ljava/lang/String;

    invoke-virtual {v3, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    const-string v6, "position"

    iget v7, p6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;->mPosition:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v3, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 149
    const-string v6, "created_time"

    iget-object v7, p6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;->mCreatedTime:Ljava/lang/String;

    invoke-static {v7}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->toTimestamp(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v3, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 151
    const-string v6, "updated_time"

    iget-object v7, p6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;->mUpdatedTime:Ljava/lang/String;

    invoke-static {v7}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->toTimestamp(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v3, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 153
    const-string v6, "like_done"

    iget-object v7, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPhoto$1$2;->this$1:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPhoto$1;

    iget-object v7, v7, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPhoto$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPhoto;

    # getter for: Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPhoto;->mLikeDone:Ljava/lang/String;
    invoke-static {v7}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPhoto;->access$100(Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPhoto;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 156
    iget-object v6, p6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;->mComment:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseComment;

    if-eqz v6, :cond_1

    .line 157
    iget-object v0, p6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;->mComment:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseComment;

    .line 159
    .local v0, "comm":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseComment;
    sget-object v6, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$Comments;->CONTENT_URI:Landroid/net/Uri;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "target_id= \'"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;->mPhotoID:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "\'"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v1, v6, v7, v8}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 162
    :goto_0
    if-eqz v0, :cond_1

    .line 163
    invoke-virtual {v4}, Landroid/content/ContentValues;->clear()V

    .line 165
    const-string v6, "comments_id"

    iget-object v7, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseComment;->mCommentID:Ljava/lang/String;

    invoke-virtual {v4, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 166
    const-string v6, "target_id"

    iget-object v7, p6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;->mPhotoID:Ljava/lang/String;

    invoke-virtual {v4, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 167
    const-string v6, "target_type"

    const-string v7, "photo"

    invoke-virtual {v4, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 169
    const-string v6, "from_id"

    iget-object v7, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseComment;->mFrom:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFrom;

    iget-object v7, v7, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFrom;->mFromID:Ljava/lang/String;

    invoke-virtual {v4, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 170
    const-string v6, "from_name"

    iget-object v7, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseComment;->mFrom:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFrom;

    iget-object v7, v7, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFrom;->mFromName:Ljava/lang/String;

    invoke-virtual {v4, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 172
    const-string v6, "message"

    iget-object v7, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseComment;->mMessage:Ljava/lang/String;

    invoke-virtual {v4, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    const-string v6, "created_time"

    iget-object v7, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseComment;->mCreatedTime:Ljava/lang/String;

    invoke-static {v7}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->toTimestamp(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 175
    const-string v6, "can_remove"

    iget-object v7, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseComment;->mCanRemove:Ljava/lang/String;

    invoke-virtual {v4, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 176
    const-string v6, "likes"

    iget-object v7, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseComment;->mLikes:Ljava/lang/String;

    invoke-virtual {v4, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 177
    const-string v6, "user_likes"

    iget-object v7, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseComment;->mUserLikes:Ljava/lang/String;

    invoke-virtual {v4, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 178
    const-string v6, "message"

    iget-object v7, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseComment;->mMessage:Ljava/lang/String;

    invoke-virtual {v4, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    const-string v6, "_actor_photo_url"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "https://graph.facebook.com/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseComment;->mFrom:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFrom;

    iget-object v8, v8, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFrom;->mFromID:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "/picture?type=normal"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 183
    sget-object v6, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$Comments;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1, v6, v4}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 185
    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseComment;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseComment;

    goto/16 :goto_0

    .line 190
    .end local v0    # "comm":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseComment;
    :cond_1
    iget-object v6, p6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;->mTags:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTags;

    if-eqz v6, :cond_2

    .line 191
    iget-object v2, p6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;->mTags:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTags;

    .line 193
    .local v2, "tags":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTags;
    :goto_1
    if-eqz v2, :cond_2

    .line 194
    sget-object v6, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$Tags;->CONTENT_URI:Landroid/net/Uri;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "target_id= \'"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;->mPhotoID:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "\'"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v1, v6, v7, v8}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 197
    invoke-virtual {v5}, Landroid/content/ContentValues;->clear()V

    .line 199
    const-string v6, "profile_id"

    iget-object v7, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTags;->mTagID:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 200
    const-string v6, "target_id"

    iget-object v7, p6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;->mPhotoID:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 201
    const-string v6, "target_type"

    const-string v7, "photo"

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 203
    const-string v6, "tagged_name"

    iget-object v7, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTags;->mTagName:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 204
    const-string v6, "x"

    iget-object v7, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTags;->mTagX:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 205
    const-string v6, "y"

    iget-object v7, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTags;->mTagY:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 206
    const-string v6, "created_time"

    iget-object v7, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTags;->mCreatedTime:Ljava/lang/String;

    invoke-static {v7}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->toTimestamp(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 209
    sget-object v6, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$Tags;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1, v6, v5}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 211
    iget-object v2, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTags;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTags;

    goto :goto_1

    .line 215
    .end local v2    # "tags":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTags;
    :cond_2
    sget-object v6, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$Photo;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1, v6, v3}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 217
    :cond_3
    iget-object v6, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPhoto$1$2;->this$1:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPhoto$1;

    iget-object v6, v6, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPhoto$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPhoto;

    sget-object v7, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$Photo;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v7}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPhoto;->setUri(Ljava/lang/String;)V

    .line 224
    :goto_2
    iget-object v6, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPhoto$1$2;->this$1:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPhoto$1;

    iget-object v6, v6, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPhoto$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPhoto;

    invoke-virtual {v6, p2}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPhoto;->setSuccess(Z)V

    .line 225
    iget-object v6, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPhoto$1$2;->this$1:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPhoto$1;

    iget-object v6, v6, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPhoto$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPhoto;

    invoke-virtual {v6, p0}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPhoto;->receive(Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;)V

    .line 227
    const/4 v6, 0x1

    return v6

    .line 219
    :cond_4
    iget-object v6, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPhoto$1$2;->this$1:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPhoto$1;

    iget-object v6, v6, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPhoto$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPhoto;

    new-instance v7, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;

    const-string v8, "facebook"

    invoke-direct {v7, v8, p3, p4, p5}, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;-><init>(Ljava/lang/String;IILandroid/os/Bundle;)V

    invoke-virtual {v6, v7}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPhoto;->setResponseList(Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;)Z

    .line 221
    iget-object v6, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPhoto$1$2;->this$1:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPhoto$1;

    iget-object v6, v6, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPhoto$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPhoto;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetPhoto;->setUri(Ljava/lang/String;)V

    goto :goto_2
.end method
