.class public Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserCheckins;
.super Ljava/lang/Object;
.source "SnsFbParserCheckins.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserCheckins$FacebookCheckins;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    return-void
.end method

.method public static parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseCheckins;
    .locals 9
    .param p0, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 38
    new-instance v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseCheckins;

    invoke-direct {v1}, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseCheckins;-><init>()V

    .line 40
    .local v1, "checkinsObj":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseCheckins;
    const/4 v0, 0x0

    .line 41
    .local v0, "checkins":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseCheckin;
    const/4 v2, 0x0

    .line 44
    .local v2, "curCheckins":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseCheckin;
    :try_start_0
    new-instance v6, Lorg/json/JSONObject;

    invoke-direct {v6, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 46
    .local v6, "jsonObject":Lorg/json/JSONObject;
    new-instance v5, Lorg/json/JSONArray;

    const-string v8, "data"

    invoke-virtual {v6, v8}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v5, v8}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 48
    .local v5, "ja":Lorg/json/JSONArray;
    if-eqz v5, :cond_1

    .line 50
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    invoke-virtual {v5}, Lorg/json/JSONArray;->length()I

    move-result v8

    if-ge v4, v8, :cond_1

    .line 51
    invoke-virtual {v5, v4}, Lorg/json/JSONArray;->optString(I)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserCheckin;->parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseCheckin;

    move-result-object v7

    .line 53
    .local v7, "newCheckins":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseCheckin;
    if-nez v0, :cond_0

    .line 54
    move-object v0, v7

    .line 55
    move-object v2, v0

    .line 50
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 57
    :cond_0
    iput-object v7, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseCheckin;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseCheckin;

    .line 58
    iget-object v2, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseCheckin;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseCheckin;

    goto :goto_1

    .line 63
    .end local v4    # "i":I
    .end local v7    # "newCheckins":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseCheckin;
    :cond_1
    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseCheckins;->mCheckins:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseCheckin;

    .line 65
    const-string v8, "paging"

    invoke-virtual {v6, v8}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 66
    const-string v8, "paging"

    invoke-virtual {v6, v8}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserPaging;->parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePaging;

    move-result-object v8

    iput-object v8, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseCheckins;->mPaging:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePaging;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 75
    .end local v5    # "ja":Lorg/json/JSONArray;
    .end local v6    # "jsonObject":Lorg/json/JSONObject;
    :cond_2
    :goto_2
    return-object v1

    .line 70
    :catch_0
    move-exception v3

    .line 72
    .local v3, "e":Lorg/json/JSONException;
    invoke-virtual {v3}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_2
.end method
