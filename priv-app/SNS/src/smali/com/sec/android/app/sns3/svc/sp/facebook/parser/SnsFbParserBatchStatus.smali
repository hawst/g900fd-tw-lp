.class public Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserBatchStatus;
.super Ljava/lang/Object;
.source "SnsFbParserBatchStatus.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserBatchStatus$FacebookBatchPosts;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    return-void
.end method

.method public static parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePosts;
    .locals 12
    .param p0, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 40
    new-instance v9, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePosts;

    invoke-direct {v9}, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePosts;-><init>()V

    .line 41
    .local v9, "postsObj":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePosts;
    const/4 v3, 0x0

    .line 43
    .local v3, "currPost":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;
    :try_start_0
    new-instance v6, Lorg/json/JSONArray;

    invoke-direct {v6, p0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 46
    .local v6, "ja":Lorg/json/JSONArray;
    if-eqz v6, :cond_2

    .line 47
    const/4 v11, 0x1

    invoke-virtual {v6, v11}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v7

    .line 48
    .local v7, "jsonObject":Lorg/json/JSONObject;
    const-string v11, "body"

    invoke-virtual {v7, v11}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 50
    .local v0, "batchStatusBody":Ljava/lang/String;
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 51
    .local v2, "bodyJsonObject":Lorg/json/JSONObject;
    invoke-virtual {v2}, Lorg/json/JSONObject;->names()Lorg/json/JSONArray;

    move-result-object v1

    .line 52
    .local v1, "bodyJa":Lorg/json/JSONArray;
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v11

    if-ge v5, v11, :cond_1

    .line 53
    invoke-virtual {v1, v5}, Lorg/json/JSONArray;->optString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v2, v11}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 55
    .local v10, "realStatusData":Ljava/lang/String;
    invoke-static {v10}, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserPost;->parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;

    move-result-object v8

    .line 56
    .local v8, "newPost":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;
    if-nez v3, :cond_0

    .line 57
    move-object v3, v8

    .line 58
    const/4 v11, 0x0

    iput-object v11, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;

    .line 59
    iput-object v3, v9, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePosts;->mPosts:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;

    .line 52
    :goto_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 61
    :cond_0
    const/4 v11, 0x0

    iput-object v11, v8, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;

    .line 62
    iput-object v8, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;

    .line 63
    iget-object v3, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;

    goto :goto_1

    .line 67
    .end local v8    # "newPost":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;
    .end local v10    # "realStatusData":Ljava/lang/String;
    :cond_1
    const-string v11, "paging"

    invoke-virtual {v7, v11}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 68
    const-string v11, "paging"

    invoke-virtual {v7, v11}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserPaging;->parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePaging;

    move-result-object v11

    iput-object v11, v9, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePosts;->mPaging:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePaging;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 76
    .end local v0    # "batchStatusBody":Ljava/lang/String;
    .end local v1    # "bodyJa":Lorg/json/JSONArray;
    .end local v2    # "bodyJsonObject":Lorg/json/JSONObject;
    .end local v5    # "i":I
    .end local v6    # "ja":Lorg/json/JSONArray;
    .end local v7    # "jsonObject":Lorg/json/JSONObject;
    :cond_2
    :goto_2
    return-object v9

    .line 72
    :catch_0
    move-exception v4

    .line 73
    .local v4, "e":Lorg/json/JSONException;
    invoke-virtual {v4}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_2
.end method
