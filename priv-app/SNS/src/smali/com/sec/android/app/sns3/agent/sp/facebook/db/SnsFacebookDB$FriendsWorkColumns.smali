.class public interface abstract Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$FriendsWorkColumns;
.super Ljava/lang/Object;
.source "SnsFacebookDB.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "FriendsWorkColumns"
.end annotation


# static fields
.field public static final EMPLOYER_ID:Ljava/lang/String; = "employer_id"

.field public static final EMPLOYER_NAME:Ljava/lang/String; = "employer_name"

.field public static final END_DATE:Ljava/lang/String; = "end_date"

.field public static final FROM_ID:Ljava/lang/String; = "from_id"

.field public static final FROM_NAME:Ljava/lang/String; = "from_name"

.field public static final LOCATION_ID:Ljava/lang/String; = "location_id"

.field public static final LOCATION_NAME:Ljava/lang/String; = "location_name"

.field public static final NAME:Ljava/lang/String; = "name"

.field public static final POSITION_ID:Ljava/lang/String; = "position_id"

.field public static final POSITION_NAME:Ljava/lang/String; = "position_name"

.field public static final PROFILE_ID:Ljava/lang/String; = "profile_id"

.field public static final START_DATE:Ljava/lang/String; = "start_date"
