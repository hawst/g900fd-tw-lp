.class public Lcom/sec/android/app/sns3/svc/sp/foursquare/parser/SnsFsParserRecommendedVenues;
.super Ljava/lang/Object;
.source "SnsFsParserRecommendedVenues.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/foursquare/parser/SnsFsParserRecommendedVenues$FourSquareExplore;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    return-void
.end method

.method public static parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseRecommendedVenues;
    .locals 21
    .param p0, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 119
    new-instance v5, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseRecommendedVenues;

    invoke-direct {v5}, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseRecommendedVenues;-><init>()V

    .line 121
    .local v5, "exploreObj":Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseRecommendedVenues;
    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 122
    .local v2, "con":Lorg/json/JSONObject;
    const-string v19, "meta"

    move-object/from16 v0, v19

    invoke-virtual {v2, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v13

    .line 124
    .local v13, "meta":Lorg/json/JSONObject;
    const-string v19, "code"

    move-object/from16 v0, v19

    invoke-virtual {v13, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v19

    const/16 v20, 0xc8

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_3

    .line 126
    const-string v19, "response"

    move-object/from16 v0, v19

    invoke-virtual {v2, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v14

    .line 127
    .local v14, "response":Lorg/json/JSONObject;
    const-string v19, "warning"

    move-object/from16 v0, v19

    invoke-virtual {v14, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v18

    .line 128
    .local v18, "warningObj":Lorg/json/JSONObject;
    if-eqz v18, :cond_0

    .line 129
    const-string v19, "text"

    invoke-virtual/range {v18 .. v19}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    iput-object v0, v5, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseRecommendedVenues;->mWarning:Ljava/lang/String;

    .line 132
    :cond_0
    const-string v19, "suggestedRadius"

    move-object/from16 v0, v19

    invoke-virtual {v14, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    iput-object v0, v5, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseRecommendedVenues;->mSuggestedRadius:Ljava/lang/String;

    .line 133
    const-string v19, "headerFullLocation"

    move-object/from16 v0, v19

    invoke-virtual {v14, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    iput-object v0, v5, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseRecommendedVenues;->mHeaderLocation:Ljava/lang/String;

    .line 134
    const-string v19, "headerLocationGranularity"

    move-object/from16 v0, v19

    invoke-virtual {v14, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    iput-object v0, v5, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseRecommendedVenues;->mHeaderLocationGranularity:Ljava/lang/String;

    .line 136
    const-string v19, "headerMessage"

    move-object/from16 v0, v19

    invoke-virtual {v14, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    iput-object v0, v5, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseRecommendedVenues;->mHeaderMessage:Ljava/lang/String;

    .line 137
    const-string v19, "query"

    move-object/from16 v0, v19

    invoke-virtual {v14, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    iput-object v0, v5, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseRecommendedVenues;->mQuery:Ljava/lang/String;

    .line 139
    const-string v19, "totalResults"

    move-object/from16 v0, v19

    invoke-virtual {v14, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v19

    if-lez v19, :cond_3

    .line 140
    const-string v19, "groups"

    move-object/from16 v0, v19

    invoke-virtual {v14, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v7

    .line 141
    .local v7, "groups":Lorg/json/JSONArray;
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_0
    invoke-virtual {v7}, Lorg/json/JSONArray;->length()I

    move-result v19

    move/from16 v0, v19

    if-ge v8, v0, :cond_3

    .line 142
    invoke-virtual {v7, v8}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v6

    .line 144
    .local v6, "groupItem":Lorg/json/JSONObject;
    const-string v19, "items"

    move-object/from16 v0, v19

    invoke-virtual {v6, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v10

    .line 146
    .local v10, "items":Lorg/json/JSONArray;
    const/4 v11, 0x0

    .local v11, "j":I
    :goto_1
    invoke-virtual {v10}, Lorg/json/JSONArray;->length()I

    move-result v19

    move/from16 v0, v19

    if-ge v11, v0, :cond_2

    .line 147
    invoke-virtual {v10, v11}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v9

    .line 149
    .local v9, "itemValue":Lorg/json/JSONObject;
    new-instance v4, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseRecommendedVenues$Explore;

    invoke-direct {v4}, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseRecommendedVenues$Explore;-><init>()V

    .line 150
    .local v4, "explore":Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseRecommendedVenues$Explore;
    const-string v19, "type"

    move-object/from16 v0, v19

    invoke-virtual {v6, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    iput-object v0, v4, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseRecommendedVenues$Explore;->mGroupType:Ljava/lang/String;

    .line 151
    const-string v19, "name"

    move-object/from16 v0, v19

    invoke-virtual {v6, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    iput-object v0, v4, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseRecommendedVenues$Explore;->mGroupName:Ljava/lang/String;

    .line 153
    const-string v19, "venue"

    move-object/from16 v0, v19

    invoke-virtual {v9, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v17

    .line 154
    .local v17, "venueItem":Lorg/json/JSONObject;
    invoke-static/range {v17 .. v17}, Lcom/sec/android/app/sns3/svc/sp/foursquare/parser/SnsFsParserVenues;->parseVenue(Lorg/json/JSONObject;)Lcom/sec/android/app/sns3/svc/sp/foursquare/response/Venue;

    move-result-object v19

    move-object/from16 v0, v19

    iput-object v0, v4, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseRecommendedVenues$Explore;->mVenue:Lcom/sec/android/app/sns3/svc/sp/foursquare/response/Venue;

    .line 156
    const-string v19, "tips"

    move-object/from16 v0, v19

    invoke-virtual {v9, v0}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v19

    if-nez v19, :cond_1

    .line 157
    const-string v19, "tips"

    move-object/from16 v0, v19

    invoke-virtual {v9, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v15

    .line 159
    .local v15, "tips":Lorg/json/JSONArray;
    const/4 v12, 0x0

    .local v12, "m":I
    :goto_2
    invoke-virtual {v15}, Lorg/json/JSONArray;->length()I

    move-result v19

    move/from16 v0, v19

    if-ge v12, v0, :cond_1

    .line 160
    invoke-virtual {v15, v12}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v16

    .line 161
    .local v16, "tipsItem":Lorg/json/JSONObject;
    const-string v19, "text"

    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    iput-object v0, v4, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseRecommendedVenues$Explore;->mTipsText:Ljava/lang/String;

    .line 162
    const-string v19, "url"

    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    iput-object v0, v4, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseRecommendedVenues$Explore;->mTipsUrl:Ljava/lang/String;

    .line 163
    const-string v19, "canonicalUrl"

    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    iput-object v0, v4, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseRecommendedVenues$Explore;->mTipsCanonicalUrl:Ljava/lang/String;

    .line 159
    add-int/lit8 v12, v12, 0x1

    goto :goto_2

    .line 168
    .end local v12    # "m":I
    .end local v15    # "tips":Lorg/json/JSONArray;
    .end local v16    # "tipsItem":Lorg/json/JSONObject;
    :cond_1
    iget-object v0, v5, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseRecommendedVenues;->mExploreValues:Ljava/util/List;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 146
    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_1

    .line 141
    .end local v4    # "explore":Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseRecommendedVenues$Explore;
    .end local v9    # "itemValue":Lorg/json/JSONObject;
    .end local v17    # "venueItem":Lorg/json/JSONObject;
    :cond_2
    add-int/lit8 v8, v8, 0x1

    goto/16 :goto_0

    .line 177
    .end local v2    # "con":Lorg/json/JSONObject;
    .end local v6    # "groupItem":Lorg/json/JSONObject;
    .end local v7    # "groups":Lorg/json/JSONArray;
    .end local v8    # "i":I
    .end local v10    # "items":Lorg/json/JSONArray;
    .end local v11    # "j":I
    .end local v13    # "meta":Lorg/json/JSONObject;
    .end local v14    # "response":Lorg/json/JSONObject;
    .end local v18    # "warningObj":Lorg/json/JSONObject;
    :catch_0
    move-exception v3

    .line 178
    .local v3, "e1":Lorg/json/JSONException;
    invoke-virtual {v3}, Lorg/json/JSONException;->printStackTrace()V

    .line 180
    .end local v3    # "e1":Lorg/json/JSONException;
    :cond_3
    return-object v5
.end method
