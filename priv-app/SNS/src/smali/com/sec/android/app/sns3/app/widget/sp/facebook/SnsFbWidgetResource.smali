.class public Lcom/sec/android/app/sns3/app/widget/sp/facebook/SnsFbWidgetResource;
.super Ljava/lang/Object;
.source "SnsFbWidgetResource.java"


# static fields
.field public static final CONTENT_URI:Ljava/lang/String; = "SNS_CONTENT_URI"

.field public static final REQUEST_PHOTO_STREAM:Ljava/lang/String; = "com.sec.android.app.sns.widget.ACTION_FACEBOOK_PHOTO_STREAM_REQUESTED"

.field public static final RESULT:Ljava/lang/String; = "SNS_RESULT"

.field public static final SNS_BROADCAST_PERMISSION:Ljava/lang/String; = "com.sec.android.app.sns3.permission.RECEIVE_SNS_BROADCAST"

.field public static TAG:Ljava/lang/String; = null

.field public static final UPDATE_PHOTO_STREAM:Ljava/lang/String; = "com.sec.android.app.sns.widget.ACTION_FACEBOOK_PHOTO_STREAM_UPDATED"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 5
    const-string v0, "SnsFbWidget"

    sput-object v0, Lcom/sec/android/app/sns3/app/widget/sp/facebook/SnsFbWidgetResource;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
