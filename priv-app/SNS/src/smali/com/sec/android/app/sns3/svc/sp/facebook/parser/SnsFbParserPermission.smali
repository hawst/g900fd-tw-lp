.class public Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserPermission;
.super Ljava/lang/Object;
.source "SnsFbParserPermission.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePermission;
    .locals 9
    .param p0, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 32
    const/4 v7, 0x0

    .line 33
    .local v7, "permission":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePermission;
    const/4 v0, 0x0

    .line 39
    .local v0, "curPermission":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePermission;
    :try_start_0
    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 42
    .local v4, "jsonObj":Lorg/json/JSONObject;
    invoke-virtual {v4}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v3

    .line 44
    .local v3, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {v4}, Lorg/json/JSONObject;->length()I

    move-result v8

    if-ge v2, v8, :cond_1

    .line 46
    new-instance v6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePermission;

    invoke-direct {v6}, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePermission;-><init>()V

    .line 47
    .local v6, "newPermission":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePermission;
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 48
    .local v5, "key":Ljava/lang/String;
    iput-object v5, v6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePermission;->mPermission:Ljava/lang/String;

    .line 49
    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v8

    iput v8, v6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePermission;->mValue:I

    .line 51
    if-nez v7, :cond_0

    .line 52
    move-object v7, v6

    .line 53
    move-object v0, v7

    .line 44
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 55
    :cond_0
    iput-object v6, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePermission;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePermission;

    .line 56
    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePermission;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePermission;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 60
    .end local v2    # "i":I
    .end local v3    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    .end local v4    # "jsonObj":Lorg/json/JSONObject;
    .end local v5    # "key":Ljava/lang/String;
    .end local v6    # "newPermission":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePermission;
    :catch_0
    move-exception v1

    .line 62
    .local v1, "e":Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    .line 65
    .end local v1    # "e":Lorg/json/JSONException;
    :cond_1
    return-object v7
.end method
