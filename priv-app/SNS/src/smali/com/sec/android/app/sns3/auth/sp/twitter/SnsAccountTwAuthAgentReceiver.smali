.class public Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthAgentReceiver;
.super Landroid/content/BroadcastReceiver;
.source "SnsAccountTwAuthAgentReceiver.java"


# static fields
.field public static final SNS_TW_LOGOUT:Ljava/lang/String; = "com.sec.android.app.sns3.auth.sp.twitter.SNS_TW_LOGOUT"

.field private static final TAG:Ljava/lang/String; = "TwAuthAgentReceiver"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v6, 0x0

    .line 40
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 42
    .local v0, "action":Ljava/lang/String;
    const-string v3, "TwAuthAgentReceiver"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Twitter Agent Receiver : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 44
    const-string v3, "com.sec.android.app.sns3.auth.sp.twitter.SNS_TW_LOGOUT"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 45
    const-string v3, "notification"

    invoke-virtual {p1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/NotificationManager;

    .line 47
    .local v1, "notiMgr":Landroid/app/NotificationManager;
    const/16 v3, 0x834

    invoke-virtual {v1, v3}, Landroid/app/NotificationManager;->cancel(I)V

    .line 48
    const/16 v3, 0x898

    invoke-virtual {v1, v3}, Landroid/app/NotificationManager;->cancel(I)V

    .line 50
    invoke-static {p1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v3

    const-string v4, "com.sec.android.app.sns3.twitter"

    invoke-virtual {v3, v4}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v2

    .line 52
    .local v2, "sns3Accounts":[Landroid/accounts/Account;
    array-length v3, v2

    if-lez v3, :cond_0

    .line 53
    invoke-static {p1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v3

    const/4 v4, 0x0

    aget-object v4, v2, v4

    invoke-virtual {v3, v4, v6, v6}, Landroid/accounts/AccountManager;->removeAccount(Landroid/accounts/Account;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    .line 56
    .end local v1    # "notiMgr":Landroid/app/NotificationManager;
    .end local v2    # "sns3Accounts":[Landroid/accounts/Account;
    :cond_0
    return-void
.end method
