.class public interface abstract Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$CommentsColumns;
.super Ljava/lang/Object;
.source "SnsFacebookDB.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "CommentsColumns"
.end annotation


# static fields
.field public static final ACTOR_PHOTO_URL:Ljava/lang/String; = "_actor_photo_url"

.field public static final CAN_REMOVE:Ljava/lang/String; = "can_remove"

.field public static final CREATED_TIME:Ljava/lang/String; = "created_time"

.field public static final FROM_ID:Ljava/lang/String; = "from_id"

.field public static final FROM_NAME:Ljava/lang/String; = "from_name"

.field public static final ID:Ljava/lang/String; = "comments_id"

.field public static final LIKES:Ljava/lang/String; = "likes"

.field public static final MESSAGE:Ljava/lang/String; = "message"

.field public static final TARGET_ID:Ljava/lang/String; = "target_id"

.field public static final TARGET_TYPE:Ljava/lang/String; = "target_type"

.field public static final USER_LIKES:Ljava/lang/String; = "user_likes"
