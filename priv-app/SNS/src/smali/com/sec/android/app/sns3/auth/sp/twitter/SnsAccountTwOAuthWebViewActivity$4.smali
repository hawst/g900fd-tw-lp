.class Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity$4;
.super Landroid/webkit/WebViewClient;
.source "SnsAccountTwOAuthWebViewActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;)V
    .locals 0

    .prologue
    .line 229
    iput-object p1, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity$4;->this$0:Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    return-void
.end method


# virtual methods
.method public onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 15
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "url"    # Ljava/lang/String;
    .param p3, "favicon"    # Landroid/graphics/Bitmap;

    .prologue
    .line 233
    invoke-static {}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->isLoggable()Z

    move-result v12

    if-eqz v12, :cond_0

    .line 234
    const-string v12, "SnsAccountTwAuth"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "WebViewClient : onPageStarted "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p2

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 235
    const-string v12, "SnsAccountTwAuth"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "WebViewClient : Redirect URL: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p2

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 237
    :cond_0
    if-eqz p2, :cond_4

    const-string v12, "http://www.facebook.com/SamsungMobile"

    move-object/from16 v0, p2

    invoke-virtual {v0, v12}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_4

    .line 238
    const-string v12, "http://www.facebook.com/SamsungMobile?"

    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v12

    move-object/from16 v0, p2

    invoke-virtual {v0, v12}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v9

    .line 239
    .local v9, "subUrl":Ljava/lang/String;
    const-string v12, "&"

    invoke-virtual {v9, v12}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v8

    .line 240
    .local v8, "splitUrl":[Ljava/lang/String;
    const/4 v5, 0x0

    .line 241
    .local v5, "oauthToken":Ljava/lang/String;
    const/4 v6, 0x0

    .line 243
    .local v6, "oauthVerifier":Ljava/lang/String;
    move-object v1, v8

    .local v1, "arr$":[Ljava/lang/String;
    array-length v4, v1

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_3

    aget-object v7, v1, v3

    .line 244
    .local v7, "parameter":Ljava/lang/String;
    const-string v12, "="

    invoke-virtual {v7, v12}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v11

    .line 245
    .local v11, "v":[Ljava/lang/String;
    const/4 v12, 0x0

    aget-object v12, v11, v12

    invoke-static {v12}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    const-string v13, "oauth_token"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 246
    const/4 v12, 0x1

    aget-object v12, v11, v12

    invoke-static {v12}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 243
    :cond_1
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 248
    :cond_2
    const/4 v12, 0x0

    aget-object v12, v11, v12

    invoke-static {v12}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    const-string v13, "oauth_verifier"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_1

    .line 249
    const/4 v12, 0x1

    aget-object v12, v11, v12

    invoke-static {v12}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    goto :goto_1

    .line 252
    .end local v7    # "parameter":Ljava/lang/String;
    .end local v11    # "v":[Ljava/lang/String;
    :cond_3
    iget-object v12, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity$4;->this$0:Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;

    # getter for: Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    invoke-static {v12}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;->access$200(Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;)Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v12

    invoke-virtual {v12}, Lcom/sec/android/app/sns3/svc/SnsSvcMgr;->getTokenMgr()Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;

    move-result-object v12

    const-string v13, "twitter"

    invoke-virtual {v12, v13}, Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;->getToken(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;

    move-result-object v10

    check-cast v10, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;

    .line 253
    .local v10, "token":Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;
    invoke-virtual {v10, v5}, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;->setUserAuthToken(Ljava/lang/String;)V

    .line 254
    invoke-virtual {v10, v6}, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;->setUserAuthVerifier(Ljava/lang/String;)V

    .line 256
    new-instance v2, Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdGetAccessToken;

    iget-object v12, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity$4;->this$0:Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;

    # getter for: Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    invoke-static {v12}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;->access$200(Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;)Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v12

    iget-object v13, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity$4;->this$0:Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;

    # getter for: Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;->mCmdHandler:Landroid/os/Handler;
    invoke-static {v13}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;->access$300(Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;)Landroid/os/Handler;

    move-result-object v13

    invoke-direct {v2, v12, v13}, Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdGetAccessToken;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Handler;)V

    .line 257
    .local v2, "cmd":Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;
    new-instance v12, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity$4$1;

    invoke-direct {v12, p0}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity$4$1;-><init>(Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity$4;)V

    invoke-virtual {v2, v12}, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->setCommandCallback(Lcom/sec/android/app/sns3/agent/command/ISnsCommandCallback;)Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;

    .line 286
    invoke-virtual {v2}, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->send()I

    .line 288
    .end local v1    # "arr$":[Ljava/lang/String;
    .end local v2    # "cmd":Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;
    .end local v3    # "i$":I
    .end local v4    # "len$":I
    .end local v5    # "oauthToken":Ljava/lang/String;
    .end local v6    # "oauthVerifier":Ljava/lang/String;
    .end local v8    # "splitUrl":[Ljava/lang/String;
    .end local v9    # "subUrl":Ljava/lang/String;
    .end local v10    # "token":Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;
    :cond_4
    return-void
.end method

.method public shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 3
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "url"    # Ljava/lang/String;

    .prologue
    .line 292
    invoke-static {}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->isLoggable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 293
    const-string v0, "SnsAccountTwAuth"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "####### WebView shouldOverrideUrlLoading URL : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 295
    :cond_0
    const/4 v0, 0x0

    return v0
.end method
