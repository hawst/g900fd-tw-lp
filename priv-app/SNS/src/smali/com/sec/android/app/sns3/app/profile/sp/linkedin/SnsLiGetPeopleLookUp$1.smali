.class final Lcom/sec/android/app/sns3/app/profile/sp/linkedin/SnsLiGetPeopleLookUp$1;
.super Ljava/lang/Object;
.source "SnsLiGetPeopleLookUp.java"

# interfaces
.implements Lcom/sec/android/app/sns3/agent/command/ISnsCommandCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/sns3/app/profile/sp/linkedin/SnsLiGetPeopleLookUp;->requestPeopleLookUpAPI(Landroid/content/Context;Ljava/util/ArrayList;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field resultCode:I

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$downloadPhoto:Z

.field final synthetic val$emailListToQuery:Ljava/util/ArrayList;


# direct methods
.method constructor <init>(ZLandroid/content/Context;Ljava/util/ArrayList;)V
    .locals 1

    .prologue
    .line 156
    iput-boolean p1, p0, Lcom/sec/android/app/sns3/app/profile/sp/linkedin/SnsLiGetPeopleLookUp$1;->val$downloadPhoto:Z

    iput-object p2, p0, Lcom/sec/android/app/sns3/app/profile/sp/linkedin/SnsLiGetPeopleLookUp$1;->val$context:Landroid/content/Context;

    iput-object p3, p0, Lcom/sec/android/app/sns3/app/profile/sp/linkedin/SnsLiGetPeopleLookUp$1;->val$emailListToQuery:Ljava/util/ArrayList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 157
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/sns3/app/profile/sp/linkedin/SnsLiGetPeopleLookUp$1;->resultCode:I

    return-void
.end method


# virtual methods
.method public onCmdRespond(IZLjava/lang/String;Ljava/util/List;)V
    .locals 2
    .param p1, "cmdID"    # I
    .param p2, "bSuccess"    # Z
    .param p3, "uri"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IZ",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 163
    .local p4, "responseList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;>;"
    if-eqz p2, :cond_1

    .line 164
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/sns3/app/profile/sp/linkedin/SnsLiGetPeopleLookUp$1;->resultCode:I

    .line 165
    iget-boolean v0, p0, Lcom/sec/android/app/sns3/app/profile/sp/linkedin/SnsLiGetPeopleLookUp$1;->val$downloadPhoto:Z

    if-eqz v0, :cond_0

    .line 166
    iget-object v0, p0, Lcom/sec/android/app/sns3/app/profile/sp/linkedin/SnsLiGetPeopleLookUp$1;->val$context:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/sns3/app/profile/sp/linkedin/SnsLiGetPeopleLookUp$1;->val$emailListToQuery:Ljava/util/ArrayList;

    # invokes: Lcom/sec/android/app/sns3/app/profile/sp/linkedin/SnsLiGetPeopleLookUp;->downloadPhotoImage(Landroid/content/Context;Ljava/util/ArrayList;)V
    invoke-static {v0, v1}, Lcom/sec/android/app/sns3/app/profile/sp/linkedin/SnsLiGetPeopleLookUp;->access$000(Landroid/content/Context;Ljava/util/ArrayList;)V

    .line 173
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/sns3/app/profile/sp/linkedin/SnsLiGetPeopleLookUp$1;->val$context:Landroid/content/Context;

    iget v1, p0, Lcom/sec/android/app/sns3/app/profile/sp/linkedin/SnsLiGetPeopleLookUp$1;->resultCode:I

    # invokes: Lcom/sec/android/app/sns3/app/profile/sp/linkedin/SnsLiGetPeopleLookUp;->invokeBroadcast(Landroid/content/Context;I)V
    invoke-static {v0, v1}, Lcom/sec/android/app/sns3/app/profile/sp/linkedin/SnsLiGetPeopleLookUp;->access$100(Landroid/content/Context;I)V

    .line 174
    return-void

    .line 169
    :cond_1
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/sns3/app/profile/sp/linkedin/SnsLiGetPeopleLookUp$1;->resultCode:I

    .line 170
    invoke-static {}, Lcom/sec/android/app/sns3/app/profile/sp/linkedin/SnsLiGetPeopleLookUp;->clearEmailCache()V

    goto :goto_0
.end method
