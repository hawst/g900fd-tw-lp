.class public Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;
.super Landroid/accounts/AccountAuthenticatorActivity;
.source "SnsAccountTwOAuthWebViewActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity$UIDialogThreadRunnable;
    }
.end annotation


# static fields
.field private static final LOGIN_FAIL_BY_ANOTHER:I = 0x0

.field private static final NETWORK_UNAVAILABLE:I = 0x1

.field private static final REQUEST_SYNC_INTERVAL:I = 0x3e8

.field private static final TAG:Ljava/lang/String; = "SnsAccountTwAuth"

.field private static mbRetryLogin:Z


# instance fields
.field mAccountManagerCallback:Landroid/accounts/AccountManagerCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/accounts/AccountManagerCallback",
            "<",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation
.end field

.field private mAppContext:Landroid/content/Context;

.field private mCmdHandler:Landroid/os/Handler;

.field private mHandler:Landroid/os/Handler;

.field private mRetryLogin:Z

.field private mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

.field private mWebView:Landroid/webkit/WebView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 80
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;->mbRetryLogin:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 68
    invoke-direct {p0}, Landroid/accounts/AccountAuthenticatorActivity;-><init>()V

    .line 88
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;->mRetryLogin:Z

    .line 90
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;->mHandler:Landroid/os/Handler;

    .line 92
    new-instance v0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity$1;-><init>(Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;->mAccountManagerCallback:Landroid/accounts/AccountManagerCallback;

    .line 465
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;

    .prologue
    .line 68
    iget-boolean v0, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;->mRetryLogin:Z

    return v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;)Landroid/webkit/WebView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;->mWebView:Landroid/webkit/WebView;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;)Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;->mCmdHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$400()Z
    .locals 1

    .prologue
    .line 68
    sget-boolean v0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;->mbRetryLogin:Z

    return v0
.end method

.method static synthetic access$402(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 68
    sput-boolean p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;->mbRetryLogin:Z

    return p0
.end method

.method static synthetic access$500(Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;->mAppContext:Landroid/content/Context;

    return-object v0
.end method

.method private checkNetwork(Landroid/content/Context;)Z
    .locals 5
    .param p1, "mAppContext"    # Landroid/content/Context;

    .prologue
    .line 496
    const-string v3, "connectivity"

    invoke-virtual {p1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/ConnectivityManager;

    .line 498
    .local v1, "cm":Landroid/net/ConnectivityManager;
    const/4 v0, 0x0

    .line 500
    .local v0, "bNetworkStatus":Z
    const/4 v2, 0x0

    .line 502
    .local v2, "netInfo":Landroid/net/NetworkInfo;
    invoke-virtual {v1}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 503
    invoke-virtual {v2}, Landroid/net/NetworkInfo;->isAvailable()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 505
    const-string v3, "SnsAccountTwAuth"

    const-string v4, "Checked that network is available!"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 507
    invoke-virtual {v2}, Landroid/net/NetworkInfo;->isConnectedOrConnecting()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 508
    const-string v3, "SnsAccountTwAuth"

    const-string v4, "Checked that network is now connected or connecting!"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 509
    const/4 v0, 0x1

    .line 514
    :cond_0
    return v0
.end method


# virtual methods
.method public loginFail(IILjava/lang/String;)V
    .locals 4
    .param p1, "errCode"    # I
    .param p2, "subErrCode"    # I
    .param p3, "reason"    # Ljava/lang/String;

    .prologue
    .line 446
    iget-object v2, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;->mAppContext:Landroid/content/Context;

    if-eqz v2, :cond_0

    .line 451
    const-string v2, "Network unreachable"

    invoke-virtual {v2, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 452
    new-instance v1, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity$UIDialogThreadRunnable;

    iget-object v2, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;->mAppContext:Landroid/content/Context;

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity$UIDialogThreadRunnable;-><init>(Landroid/content/Context;I)V

    .line 454
    .local v1, "networkFail":Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity$UIDialogThreadRunnable;
    iget-object v2, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 463
    .end local v1    # "networkFail":Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity$UIDialogThreadRunnable;
    :cond_0
    :goto_0
    return-void

    .line 456
    :cond_1
    new-instance v0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity$UIDialogThreadRunnable;

    iget-object v2, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;->mAppContext:Landroid/content/Context;

    const/4 v3, 0x0

    invoke-direct {v0, v2, v3}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity$UIDialogThreadRunnable;-><init>(Landroid/content/Context;I)V

    .line 458
    .local v0, "another":Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity$UIDialogThreadRunnable;
    iget-object v2, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public loginSuccess(Ljava/lang/String;Ljava/lang/String;)V
    .locals 10
    .param p1, "userName"    # Ljava/lang/String;
    .param p2, "userID"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 421
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 423
    .local v4, "options":Landroid/os/Bundle;
    const-string v0, "SnsAccountTwAuth"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SnsAccountTwOAuthWebViewActivity : addAccount() :  userName = "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 425
    const-string v0, "username"

    invoke-virtual {v4, v0, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 427
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;->mAppContext:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 428
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    const-string v1, "com.sec.android.app.sns3.twitter"

    move-object v3, v2

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Landroid/accounts/AccountManager;->addAccount(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Landroid/os/Bundle;Landroid/app/Activity;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    .line 435
    :goto_0
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/SnsApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    .line 436
    .local v8, "cr":Landroid/content/ContentResolver;
    new-instance v9, Landroid/content/ContentValues;

    invoke-direct {v9}, Landroid/content/ContentValues;-><init>()V

    .line 437
    .local v9, "values":Landroid/content/ContentValues;
    sget-object v0, Lcom/sec/android/app/sns3/agent/sp/twitter/db/SnsTwitterDB$UserTwBasicInfo;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v8, v0, v2, v2}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 438
    invoke-virtual {v9}, Landroid/content/ContentValues;->clear()V

    .line 440
    const-string v0, "user_id"

    invoke-virtual {v9, v0, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 441
    const-string v0, "user_name"

    invoke-virtual {v9, v0, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 442
    sget-object v0, Lcom/sec/android/app/sns3/agent/sp/twitter/db/SnsTwitterDB$UserTwBasicInfo;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v8, v0, v9}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 443
    return-void

    .line 431
    .end local v8    # "cr":Landroid/content/ContentResolver;
    .end local v9    # "values":Landroid/content/ContentValues;
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;->mAppContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    const-string v1, "com.sec.android.app.sns3.twitter"

    iget-object v6, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;->mAccountManagerCallback:Landroid/accounts/AccountManagerCallback;

    move-object v3, v2

    move-object v5, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Landroid/accounts/AccountManager;->addAccount(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Landroid/os/Bundle;Landroid/app/Activity;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    goto :goto_0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 324
    const/16 v0, 0x3e8

    if-ne p1, v0, :cond_0

    .line 325
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;->finish()V

    .line 327
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 12
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const v11, 0x7f08006d

    const/4 v8, 0x2

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 169
    iput-object p0, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;->mAppContext:Landroid/content/Context;

    .line 171
    iget-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;->mAppContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->isLightTheme(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_0

    const v5, 0x103012b

    :goto_0
    invoke-virtual {p0, v5}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;->setTheme(I)V

    .line 173
    invoke-super {p0, p1}, Landroid/accounts/AccountAuthenticatorActivity;->onCreate(Landroid/os/Bundle;)V

    .line 175
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/sns3/SnsApplication;->getSvcMgr()Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    .line 176
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/sns3/SnsApplication;->getAgentMgr()Lcom/sec/android/app/sns3/agent/SnsAgentMgr;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/sns3/agent/SnsAgentMgr;->getCommandMgr()Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;->getCommandMgrHandle()Landroid/os/Handler;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;->mCmdHandler:Landroid/os/Handler;

    .line 178
    new-instance v5, Landroid/os/Handler;

    invoke-direct {v5}, Landroid/os/Handler;-><init>()V

    iput-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;->mHandler:Landroid/os/Handler;

    .line 180
    invoke-virtual {p0, v8}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;->requestWindowFeature(I)Z

    .line 182
    const v5, 0x7f080046

    new-array v6, v10, [Ljava/lang/Object;

    invoke-virtual {p0, v11}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-virtual {p0, v5, v6}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 184
    iget-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;->mAppContext:Landroid/content/Context;

    invoke-direct {p0, v5}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;->checkNetwork(Landroid/content/Context;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 185
    new-instance v3, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity$UIDialogThreadRunnable;

    iget-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;->mAppContext:Landroid/content/Context;

    invoke-direct {v3, v5, v10}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity$UIDialogThreadRunnable;-><init>(Landroid/content/Context;I)V

    .line 187
    .local v3, "networkUnavailable":Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity$UIDialogThreadRunnable;
    iget-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;->mAppContext:Landroid/content/Context;

    check-cast v5, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;

    invoke-virtual {v5, v3}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 320
    .end local v3    # "networkUnavailable":Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity$UIDialogThreadRunnable;
    :goto_1
    return-void

    .line 171
    :cond_0
    const v5, 0x1030128

    goto :goto_0

    .line 191
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    const-string v6, "RetryLogin"

    invoke-virtual {v5, v6, v9}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    iput-boolean v5, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;->mRetryLogin:Z

    .line 193
    iget-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;->mAppContext:Landroid/content/Context;

    invoke-static {v5}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v5

    const-string v6, "com.sec.android.app.sns3.twitter"

    invoke-virtual {v5, v6}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 196
    .local v0, "accounts":[Landroid/accounts/Account;
    array-length v5, v0

    if-lez v5, :cond_2

    iget-boolean v5, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;->mRetryLogin:Z

    if-nez v5, :cond_2

    .line 197
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    const/high16 v6, 0x7f080000

    new-array v7, v10, [Ljava/lang/Object;

    invoke-virtual {p0, v11}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;->getString(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v9

    invoke-virtual {p0, v6, v7}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6, v10}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    .line 200
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;->finish()V

    goto :goto_1

    .line 204
    :cond_2
    const-string v5, "notification"

    invoke-virtual {p0, v5}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/app/NotificationManager;

    .line 205
    .local v4, "notiMgr":Landroid/app/NotificationManager;
    const/16 v5, 0x898

    invoke-virtual {v4, v5}, Landroid/app/NotificationManager;->cancel(I)V

    .line 206
    const/16 v5, 0x8fc

    invoke-virtual {v4, v5}, Landroid/app/NotificationManager;->cancel(I)V

    .line 208
    new-instance v5, Landroid/webkit/WebView;

    iget-object v6, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;->mAppContext:Landroid/content/Context;

    invoke-direct {v5, v6}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;)V

    iput-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;->mWebView:Landroid/webkit/WebView;

    .line 210
    iget-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v5}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v5

    invoke-virtual {v5, v9}, Landroid/webkit/WebSettings;->setAllowFileAccess(Z)V

    .line 211
    iget-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v5}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v5

    invoke-virtual {v5, v9}, Landroid/webkit/WebSettings;->setAppCacheEnabled(Z)V

    .line 212
    iget-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v5}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v5

    invoke-virtual {v5, v8}, Landroid/webkit/WebSettings;->setCacheMode(I)V

    .line 213
    iget-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v5}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v5

    invoke-virtual {v5, v9}, Landroid/webkit/WebSettings;->setDatabaseEnabled(Z)V

    .line 214
    iget-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v5}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v5

    invoke-virtual {v5, v9}, Landroid/webkit/WebSettings;->setSaveFormData(Z)V

    .line 215
    iget-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v5}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v5

    invoke-virtual {v5, v10}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 219
    iget-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;->mAppContext:Landroid/content/Context;

    invoke-static {v5}, Landroid/webkit/CookieSyncManager;->createInstance(Landroid/content/Context;)Landroid/webkit/CookieSyncManager;

    .line 220
    invoke-static {}, Landroid/webkit/CookieManager;->getInstance()Landroid/webkit/CookieManager;

    move-result-object v2

    .line 221
    .local v2, "cookieManager":Landroid/webkit/CookieManager;
    invoke-virtual {v2}, Landroid/webkit/CookieManager;->removeAllCookie()V

    .line 223
    iget-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;->mWebView:Landroid/webkit/WebView;

    new-instance v6, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity$3;

    invoke-direct {v6, p0}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity$3;-><init>(Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;)V

    invoke-virtual {v5, v6}, Landroid/webkit/WebView;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    .line 229
    iget-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;->mWebView:Landroid/webkit/WebView;

    new-instance v6, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity$4;

    invoke-direct {v6, p0}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity$4;-><init>(Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;)V

    invoke-virtual {v5, v6}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 299
    iget-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {p0, v5}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;->setContentView(Landroid/view/View;)V

    .line 301
    new-instance v1, Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdGetRequestToken;

    iget-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    iget-object v6, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;->mCmdHandler:Landroid/os/Handler;

    invoke-direct {v1, v5, v6}, Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdGetRequestToken;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Handler;)V

    .line 302
    .local v1, "cmd":Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;
    new-instance v5, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity$5;

    invoke-direct {v5, p0}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity$5;-><init>(Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;)V

    invoke-virtual {v1, v5}, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->setCommandCallback(Lcom/sec/android/app/sns3/agent/command/ISnsCommandCallback;)Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;

    .line 318
    invoke-virtual {v1}, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->send()I

    goto/16 :goto_1
.end method

.method public onCreateDialog(I)Landroid/app/Dialog;
    .locals 3
    .param p1, "id"    # I

    .prologue
    const v2, 0x7f080034

    const v1, 0x7f080031

    .line 353
    packed-switch p1, :pswitch_data_0

    .line 411
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 355
    :pswitch_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f080032

    invoke-virtual {p0, v1}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity$7;

    invoke-direct {v1, p0}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity$7;-><init>(Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity$6;

    invoke-direct {v1, p0}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity$6;-><init>(Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;)V

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    .line 382
    :pswitch_1
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f080033

    invoke-virtual {p0, v1}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity$9;

    invoke-direct {v1, p0}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity$9;-><init>(Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity$8;

    invoke-direct {v1, p0}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity$8;-><init>(Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;)V

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    .line 353
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 416
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;->mAppContext:Landroid/content/Context;

    .line 417
    invoke-super {p0}, Landroid/accounts/AccountAuthenticatorActivity;->onDestroy()V

    .line 418
    return-void
.end method

.method public onPrepareDialog(ILandroid/app/Dialog;)V
    .locals 3
    .param p1, "id"    # I
    .param p2, "dialog"    # Landroid/app/Dialog;

    .prologue
    .line 331
    packed-switch p1, :pswitch_data_0

    .line 349
    :cond_0
    :goto_0
    return-void

    .line 333
    :pswitch_0
    instance-of v2, p2, Landroid/app/AlertDialog;

    if-eqz v2, :cond_0

    move-object v0, p2

    .line 334
    check-cast v0, Landroid/app/AlertDialog;

    .line 335
    .local v0, "alert":Landroid/app/AlertDialog;
    const v2, 0x7f080032

    invoke-virtual {p0, v2}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 340
    .end local v0    # "alert":Landroid/app/AlertDialog;
    :pswitch_1
    instance-of v2, p2, Landroid/app/AlertDialog;

    if-eqz v2, :cond_0

    move-object v1, p2

    .line 341
    check-cast v1, Landroid/app/AlertDialog;

    .line 342
    .local v1, "alert1":Landroid/app/AlertDialog;
    const v2, 0x7f080033

    invoke-virtual {p0, v2}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 331
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public refreshWebViewUI(Ljava/lang/String;)V
    .locals 4
    .param p1, "rUri"    # Ljava/lang/String;

    .prologue
    .line 151
    move-object v0, p1

    .line 153
    .local v0, "uri":Ljava/lang/String;
    const-string v1, "SnsAccountTwAuth"

    const-string v2, "###################### refreshImageViewUI Enter #####################"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 154
    invoke-static {}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->isLoggable()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 155
    const-string v1, "SnsAccountTwAuth"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "uri = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 157
    :cond_0
    if-eqz v0, :cond_1

    .line 158
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;->mHandler:Landroid/os/Handler;

    new-instance v2, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity$2;

    invoke-direct {v2, p0, v0}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity$2;-><init>(Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 165
    :cond_1
    return-void
.end method
