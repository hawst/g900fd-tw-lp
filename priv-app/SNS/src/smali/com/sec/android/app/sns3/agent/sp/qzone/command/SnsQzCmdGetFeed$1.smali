.class Lcom/sec/android/app/sns3/agent/sp/qzone/command/SnsQzCmdGetFeed$1;
.super Lcom/sec/android/app/sns3/svc/sp/qzone/request/SnsQzReqGetFeed;
.source "SnsQzCmdGetFeed.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/sns3/agent/sp/qzone/command/SnsQzCmdGetFeed;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Handler;Ljava/lang/String;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/sns3/agent/sp/qzone/command/SnsQzCmdGetFeed;


# direct methods
.method constructor <init>(Lcom/sec/android/app/sns3/agent/sp/qzone/command/SnsQzCmdGetFeed;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;I)V
    .locals 0
    .param p2, "x0"    # Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    .param p3, "x1"    # Ljava/lang/String;
    .param p4, "x2"    # I

    .prologue
    .line 67
    iput-object p1, p0, Lcom/sec/android/app/sns3/agent/sp/qzone/command/SnsQzCmdGetFeed$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/qzone/command/SnsQzCmdGetFeed;

    invoke-direct {p0, p2, p3, p4}, Lcom/sec/android/app/sns3/svc/sp/qzone/request/SnsQzReqGetFeed;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;I)V

    return-void
.end method


# virtual methods
.method public onReqRespond(IZIILandroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseFeed;)Z
    .locals 22
    .param p1, "reqID"    # I
    .param p2, "bSuccess"    # Z
    .param p3, "httpStatus"    # I
    .param p4, "errorCode"    # I
    .param p5, "reason"    # Landroid/os/Bundle;
    .param p6, "feed"    # Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseFeed;

    .prologue
    .line 73
    new-instance v20, Landroid/content/ContentValues;

    invoke-direct/range {v20 .. v20}, Landroid/content/ContentValues;-><init>()V

    .line 74
    .local v20, "values":Landroid/content/ContentValues;
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/sns3/SnsApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    .line 76
    .local v4, "cr":Landroid/content/ContentResolver;
    const/4 v12, 0x0

    .line 78
    .local v12, "createTime":Ljava/lang/String;
    if-eqz p2, :cond_d

    .line 79
    if-eqz p6, :cond_c

    .line 81
    sget-object v5, Lcom/sec/android/app/sns3/agent/sp/qzone/db/SnsQzoneDB$FeedList;->CONTENT_URI:Landroid/net/Uri;

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v4, v5, v6, v7}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 83
    move-object/from16 v13, p6

    .line 84
    .local v13, "curFeed":Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseFeed;
    :goto_0
    if-eqz v13, :cond_c

    .line 86
    invoke-virtual/range {v20 .. v20}, Landroid/content/ContentValues;->clear()V

    .line 88
    const-string v5, "feed_id"

    iget-object v6, v13, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseFeed;->mFeedID:Ljava/lang/String;

    move-object/from16 v0, v20

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    const-string v5, "profile_url"

    iget-object v6, v13, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseFeed;->mProfileUrl:Ljava/lang/String;

    move-object/from16 v0, v20

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    const-string v5, "title"

    iget-object v6, v13, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseFeed;->mTitle:Ljava/lang/String;

    move-object/from16 v0, v20

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 91
    const-string v5, "message"

    iget-object v6, v13, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseFeed;->mMessage:Ljava/lang/String;

    move-object/from16 v0, v20

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    const-string v5, "picture"

    iget-object v6, v13, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseFeed;->mPicture:Ljava/lang/String;

    move-object/from16 v0, v20

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    const-string v5, "picture_count"

    iget v6, v13, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseFeed;->mPictureCount:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move-object/from16 v0, v20

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 94
    const-string v5, "author_name"

    iget-object v6, v13, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseFeed;->mAuthorName:Ljava/lang/String;

    move-object/from16 v0, v20

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    const-string v5, "author_phonenumber"

    iget-object v6, v13, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseFeed;->mAuthorPhonenumber:Ljava/lang/String;

    move-object/from16 v0, v20

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    const-string v5, "host_openid"

    iget-object v6, v13, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseFeed;->mHostOpenID:Ljava/lang/String;

    move-object/from16 v0, v20

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    const-string v5, "link_uri"

    iget-object v6, v13, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseFeed;->mLinkUri:Ljava/lang/String;

    move-object/from16 v0, v20

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    const-string v5, "author_openid"

    iget-object v6, v13, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseFeed;->mOpenID:Ljava/lang/String;

    move-object/from16 v0, v20

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 99
    const-string v5, "like_count"

    iget v6, v13, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseFeed;->mLikeCount:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move-object/from16 v0, v20

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 100
    const-string v5, "comment_count"

    iget v6, v13, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseFeed;->mCommentCount:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move-object/from16 v0, v20

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 101
    const-string v5, "type"

    iget-object v6, v13, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseFeed;->mType:Ljava/lang/String;

    move-object/from16 v0, v20

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    const-string v5, "original_title"

    iget-object v6, v13, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseFeed;->mOriginalTitle:Ljava/lang/String;

    move-object/from16 v0, v20

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    const-string v5, "original_message"

    iget-object v6, v13, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseFeed;->mOriginalMessage:Ljava/lang/String;

    move-object/from16 v0, v20

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    const-string v5, "original_author_name"

    iget-object v6, v13, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseFeed;->mOriginalAuthorName:Ljava/lang/String;

    move-object/from16 v0, v20

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    const-string v5, "original_picture"

    iget-object v6, v13, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseFeed;->mOriginalPicture:Ljava/lang/String;

    move-object/from16 v0, v20

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    const-string v5, "original_picture_count"

    iget v6, v13, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseFeed;->mOriginalPictureCount:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move-object/from16 v0, v20

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 109
    :try_start_0
    new-instance v18, Ljava/util/Date;

    iget-object v5, v13, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseFeed;->mCreatedTime:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    const-wide/16 v8, 0x3e8

    mul-long/2addr v6, v8

    move-object/from16 v0, v18

    invoke-direct {v0, v6, v7}, Ljava/util/Date;-><init>(J)V

    .line 110
    .local v18, "qzoneDate":Ljava/util/Date;
    sget-object v5, Lcom/sec/android/app/sns3/agent/sp/qzone/command/SnsQzCmdGetFeed;->timeStampformat:Ljava/text/SimpleDateFormat;

    move-object/from16 v0, v18

    invoke-virtual {v5, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v12

    .line 114
    .end local v18    # "qzoneDate":Ljava/util/Date;
    :goto_1
    const-string v5, "create_time"

    move-object/from16 v0, v20

    invoke-virtual {v0, v5, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 116
    const/16 v19, 0x0

    .line 117
    .local v19, "rawCursor":Landroid/database/Cursor;
    const/4 v10, 0x0

    .line 120
    .local v10, "contactCursor":Landroid/database/Cursor;
    const/4 v11, 0x0

    .line 122
    .local v11, "contact_id":Ljava/lang/String;
    :try_start_1
    sget-object v5, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v8, "contact_id"

    aput-object v8, v6, v7

    const-string v7, "sourceid = ? and account_type = ?"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    iget-object v0, v13, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseFeed;->mOpenID:Ljava/lang/String;

    move-object/from16 v21, v0

    aput-object v21, v8, v9

    const/4 v9, 0x1

    const-string v21, "com.qzone.account"

    aput-object v21, v8, v9

    const/4 v9, 0x0

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v19

    .line 126
    if-eqz v19, :cond_0

    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 127
    const/4 v5, 0x0

    move-object/from16 v0, v19

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 129
    :cond_0
    if-eqz v19, :cond_1

    .line 130
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->close()V

    .line 131
    const/16 v19, 0x0

    .line 134
    :cond_1
    if-eqz v11, :cond_6

    .line 135
    sget-object v5, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v8, "data1"

    aput-object v8, v6, v7

    const/4 v7, 0x1

    const-string v8, "is_primary"

    aput-object v8, v6, v7

    const-string v7, "contact_id = ? and mimetype = ?"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    aput-object v11, v8, v9

    const/4 v9, 0x1

    const-string v21, "vnd.android.cursor.item/phone_v2"

    aput-object v21, v8, v9

    const/4 v9, 0x0

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 138
    const-string v17, ""

    .line 139
    .local v17, "phoneNumber":Ljava/lang/String;
    if-eqz v10, :cond_5

    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 140
    invoke-interface {v10}, Landroid/database/Cursor;->getCount()I

    move-result v14

    .line 141
    .local v14, "cursorCount":I
    const/4 v5, 0x1

    if-le v14, v5, :cond_4

    .line 142
    const/16 v16, 0x0

    .local v16, "i":I
    :goto_2
    move/from16 v0, v16

    if-ge v0, v14, :cond_5

    .line 143
    if-nez v16, :cond_3

    .line 144
    const/4 v5, 0x0

    invoke-interface {v10, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v17

    .line 148
    :cond_2
    :goto_3
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 142
    add-int/lit8 v16, v16, 0x1

    goto :goto_2

    .line 111
    .end local v10    # "contactCursor":Landroid/database/Cursor;
    .end local v11    # "contact_id":Ljava/lang/String;
    .end local v14    # "cursorCount":I
    .end local v16    # "i":I
    .end local v17    # "phoneNumber":Ljava/lang/String;
    .end local v19    # "rawCursor":Landroid/database/Cursor;
    :catch_0
    move-exception v15

    .line 112
    .local v15, "e":Ljava/lang/Exception;
    const-string v5, "SnsAgent"

    const-string v6, "exception while making CreateTime format"

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 145
    .end local v15    # "e":Ljava/lang/Exception;
    .restart local v10    # "contactCursor":Landroid/database/Cursor;
    .restart local v11    # "contact_id":Ljava/lang/String;
    .restart local v14    # "cursorCount":I
    .restart local v16    # "i":I
    .restart local v17    # "phoneNumber":Ljava/lang/String;
    .restart local v19    # "rawCursor":Landroid/database/Cursor;
    :cond_3
    const/4 v5, 0x1

    :try_start_2
    invoke-interface {v10, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_2

    .line 146
    const/4 v5, 0x0

    invoke-interface {v10, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v17

    goto :goto_3

    .line 151
    .end local v16    # "i":I
    :cond_4
    const/4 v5, 0x0

    invoke-interface {v10, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v17

    .line 155
    .end local v14    # "cursorCount":I
    :cond_5
    const-string v5, "author_phonenumber"

    move-object/from16 v0, v20

    move-object/from16 v1, v17

    invoke-virtual {v0, v5, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 157
    if-eqz v10, :cond_6

    .line 158
    invoke-interface {v10}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 159
    const/4 v10, 0x0

    .line 165
    .end local v17    # "phoneNumber":Ljava/lang/String;
    :cond_6
    if-eqz v19, :cond_7

    .line 166
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->close()V

    .line 168
    :cond_7
    if-eqz v10, :cond_8

    .line 169
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 173
    :cond_8
    :goto_4
    sget-object v5, Lcom/sec/android/app/sns3/agent/sp/qzone/db/SnsQzoneDB$FeedList;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, v20

    invoke-virtual {v4, v5, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 175
    iget-object v13, v13, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseFeed;->mNext:Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseFeed;

    .line 176
    goto/16 :goto_0

    .line 162
    :catch_1
    move-exception v15

    .line 163
    .restart local v15    # "e":Ljava/lang/Exception;
    :try_start_3
    const-string v5, "SnsAgent"

    const-string v6, "exception while insert phone number"

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 165
    if-eqz v19, :cond_9

    .line 166
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->close()V

    .line 168
    :cond_9
    if-eqz v10, :cond_8

    .line 169
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    goto :goto_4

    .line 165
    .end local v15    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v5

    if-eqz v19, :cond_a

    .line 166
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->close()V

    .line 168
    :cond_a
    if-eqz v10, :cond_b

    .line 169
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    :cond_b
    throw v5

    .line 178
    .end local v10    # "contactCursor":Landroid/database/Cursor;
    .end local v11    # "contact_id":Ljava/lang/String;
    .end local v13    # "curFeed":Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseFeed;
    .end local v19    # "rawCursor":Landroid/database/Cursor;
    :cond_c
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/sns3/agent/sp/qzone/command/SnsQzCmdGetFeed$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/qzone/command/SnsQzCmdGetFeed;

    sget-object v6, Lcom/sec/android/app/sns3/agent/sp/qzone/db/SnsQzoneDB$FeedList;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v6}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/sec/android/app/sns3/agent/sp/qzone/command/SnsQzCmdGetFeed;->setUri(Ljava/lang/String;)V

    .line 184
    :goto_5
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/sns3/agent/sp/qzone/command/SnsQzCmdGetFeed$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/qzone/command/SnsQzCmdGetFeed;

    move/from16 v0, p2

    invoke-virtual {v5, v0}, Lcom/sec/android/app/sns3/agent/sp/qzone/command/SnsQzCmdGetFeed;->setSuccess(Z)V

    .line 185
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/sns3/agent/sp/qzone/command/SnsQzCmdGetFeed$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/qzone/command/SnsQzCmdGetFeed;

    move-object/from16 v0, p0

    invoke-virtual {v5, v0}, Lcom/sec/android/app/sns3/agent/sp/qzone/command/SnsQzCmdGetFeed;->receive(Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;)V

    .line 187
    const/4 v5, 0x1

    return v5

    .line 180
    :cond_d
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/sns3/agent/sp/qzone/command/SnsQzCmdGetFeed$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/qzone/command/SnsQzCmdGetFeed;

    new-instance v6, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;

    const-string v7, "qzone"

    move/from16 v0, p3

    move/from16 v1, p4

    move-object/from16 v2, p5

    invoke-direct {v6, v7, v0, v1, v2}, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;-><init>(Ljava/lang/String;IILandroid/os/Bundle;)V

    invoke-virtual {v5, v6}, Lcom/sec/android/app/sns3/agent/sp/qzone/command/SnsQzCmdGetFeed;->setResponseList(Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;)Z

    .line 181
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/sns3/agent/sp/qzone/command/SnsQzCmdGetFeed$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/qzone/command/SnsQzCmdGetFeed;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Lcom/sec/android/app/sns3/agent/sp/qzone/command/SnsQzCmdGetFeed;->setUri(Ljava/lang/String;)V

    goto :goto_5
.end method
