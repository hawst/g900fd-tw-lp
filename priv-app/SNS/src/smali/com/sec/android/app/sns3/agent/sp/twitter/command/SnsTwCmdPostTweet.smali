.class public Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdPostTweet;
.super Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;
.source "SnsTwCmdPostTweet.java"


# direct methods
.method public constructor <init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Handler;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "svcMgr"    # Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    .param p2, "cmdHandler"    # Landroid/os/Handler;
    .param p3, "id"    # Ljava/lang/String;
    .param p4, "tweetType"    # Ljava/lang/String;

    .prologue
    .line 38
    invoke-direct {p0, p2}, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;-><init>(Landroid/os/Handler;)V

    .line 40
    new-instance v2, Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;

    invoke-direct {v2}, Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;-><init>()V

    .line 41
    .local v2, "unit":Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 43
    .local v0, "bundle":Landroid/os/Bundle;
    sget-object v3, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;->ID:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;

    invoke-virtual {v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;->getParam()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 45
    const-string v3, "Retweet"

    invoke-virtual {p4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 47
    new-instance v1, Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdPostTweet$1;

    sget-object v3, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;->STATUSES_RETWEET:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;

    invoke-direct {v1, p0, p1, v3, v0}, Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdPostTweet$1;-><init>(Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdPostTweet;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;Landroid/os/Bundle;)V

    .line 65
    .local v1, "req":Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestStatuses;
    invoke-virtual {v2, v1}, Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;->addRequest(Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;)V

    .line 89
    .end local v1    # "req":Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestStatuses;
    :cond_0
    :goto_0
    invoke-virtual {p0, v2}, Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdPostTweet;->addCommandUnit(Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;)V

    .line 90
    return-void

    .line 67
    :cond_1
    const-string v3, "Delete"

    invoke-virtual {p4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 69
    new-instance v1, Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdPostTweet$2;

    sget-object v3, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;->STATUSES_DESTROY:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;

    invoke-direct {v1, p0, p1, v3, v0}, Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdPostTweet$2;-><init>(Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdPostTweet;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;Landroid/os/Bundle;)V

    .line 87
    .restart local v1    # "req":Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestStatuses;
    invoke-virtual {v2, v1}, Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;->addRequest(Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;)V

    goto :goto_0
.end method


# virtual methods
.method protected respond()Z
    .locals 5

    .prologue
    .line 94
    const-string v0, "SnsAgent"

    const-string v1, "<SnsTwCmdPostTweet> respond()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 96
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdPostTweet;->getCommandCallback()Lcom/sec/android/app/sns3/agent/command/ISnsCommandCallback;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdPostTweet;->getCommandID()I

    move-result v1

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdPostTweet;->isSuccess()Z

    move-result v2

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdPostTweet;->getUri()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdPostTweet;->getResponseList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/sec/android/app/sns3/agent/command/ISnsCommandCallback;->onCmdRespond(IZLjava/lang/String;Ljava/util/List;)V

    .line 98
    const/4 v0, 0x1

    return v0
.end method
