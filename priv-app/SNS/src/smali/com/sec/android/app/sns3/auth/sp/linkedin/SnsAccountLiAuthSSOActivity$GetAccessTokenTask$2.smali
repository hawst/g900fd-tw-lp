.class Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity$GetAccessTokenTask$2;
.super Ljava/lang/Object;
.source "SnsAccountLiAuthSSOActivity.java"

# interfaces
.implements Lcom/sec/android/app/sns3/agent/command/ISnsCommandCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity$GetAccessTokenTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity$GetAccessTokenTask;

.field final synthetic val$isNeedToHold:Z

.field final synthetic val$responseReceivedTime:J


# direct methods
.method constructor <init>(Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity$GetAccessTokenTask;ZJ)V
    .locals 1

    .prologue
    .line 532
    iput-object p1, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity$GetAccessTokenTask$2;->this$1:Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity$GetAccessTokenTask;

    iput-boolean p2, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity$GetAccessTokenTask$2;->val$isNeedToHold:Z

    iput-wide p3, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity$GetAccessTokenTask$2;->val$responseReceivedTime:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCmdRespond(IZLjava/lang/String;Ljava/util/List;)V
    .locals 6
    .param p1, "cmdID"    # I
    .param p2, "bSuccess"    # Z
    .param p3, "uri"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IZ",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 537
    .local p4, "responseList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;>;"
    const/4 v0, 0x0

    .line 538
    .local v0, "reason":Landroid/os/Bundle;
    const/4 v2, 0x0

    .line 539
    .local v2, "userName":Ljava/lang/String;
    const/4 v1, 0x0

    .line 541
    .local v1, "userId":Ljava/lang/String;
    if-eqz p4, :cond_0

    .line 542
    invoke-interface {p4}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_0

    .line 543
    const/4 v3, 0x0

    invoke-interface {p4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;

    invoke-virtual {v3}, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;->getReason()Landroid/os/Bundle;

    move-result-object v0

    .line 545
    if-eqz v0, :cond_0

    .line 546
    const-string v3, "userName"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 547
    const-string v3, "userID"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 552
    :cond_0
    if-eqz v2, :cond_1

    .line 553
    iget-object v3, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity$GetAccessTokenTask$2;->this$1:Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity$GetAccessTokenTask;

    iget-object v3, v3, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity$GetAccessTokenTask;->this$0:Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;

    # invokes: Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->loginSuccess(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v3, v2, v1}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->access$1200(Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;Ljava/lang/String;Ljava/lang/String;)V

    .line 563
    :goto_0
    return-void

    .line 556
    :cond_1
    iget-boolean v3, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity$GetAccessTokenTask$2;->val$isNeedToHold:Z

    if-eqz v3, :cond_2

    .line 557
    iget-object v3, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity$GetAccessTokenTask$2;->this$1:Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity$GetAccessTokenTask;

    iget-wide v4, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity$GetAccessTokenTask$2;->val$responseReceivedTime:J

    # invokes: Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity$GetAccessTokenTask;->pauseThread(J)V
    invoke-static {v3, v4, v5}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity$GetAccessTokenTask;->access$1300(Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity$GetAccessTokenTask;J)V

    .line 560
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity$GetAccessTokenTask$2;->this$1:Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity$GetAccessTokenTask;

    iget-object v3, v3, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity$GetAccessTokenTask;->this$0:Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;

    # invokes: Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->registerRetryNotification()V
    invoke-static {v3}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->access$1100(Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;)V

    .line 561
    iget-object v3, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity$GetAccessTokenTask$2;->this$1:Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity$GetAccessTokenTask;

    iget-object v3, v3, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity$GetAccessTokenTask;->this$0:Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;

    invoke-virtual {v3}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthSSOActivity;->finish()V

    goto :goto_0
.end method
