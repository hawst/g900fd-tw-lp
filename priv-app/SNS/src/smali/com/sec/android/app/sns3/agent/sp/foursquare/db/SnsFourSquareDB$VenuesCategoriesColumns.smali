.class public interface abstract Lcom/sec/android/app/sns3/agent/sp/foursquare/db/SnsFourSquareDB$VenuesCategoriesColumns;
.super Ljava/lang/Object;
.source "SnsFourSquareDB.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/agent/sp/foursquare/db/SnsFourSquareDB;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "VenuesCategoriesColumns"
.end annotation


# static fields
.field public static final CATEGORY_ICON:Ljava/lang/String; = "category_icon"

.field public static final CATEGORY_ID:Ljava/lang/String; = "category_id"

.field public static final CATEGORY_NAME:Ljava/lang/String; = "category_name"

.field public static final CATEGORY_PLURAL_NAME:Ljava/lang/String; = "category_plural_name"
