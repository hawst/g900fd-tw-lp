.class public interface abstract Lcom/sec/android/app/sns3/agent/life/db/SnsLifeDB$LifeTimelinefoColumns;
.super Ljava/lang/Object;
.source "SnsLifeDB.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/agent/life/db/SnsLifeDB;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "LifeTimelinefoColumns"
.end annotation


# static fields
.field public static final ALBUM_CONTENTS:Ljava/lang/String; = "album_contents"

.field public static final CATEGORY_ICON:Ljava/lang/String; = "category_icon"

.field public static final CATEGORY_ID:Ljava/lang/String; = "category_id"

.field public static final CATEGORY_NAME:Ljava/lang/String; = "category_name"

.field public static final HEIGHT:Ljava/lang/String; = "height"

.field public static final LATITUDE:Ljava/lang/String; = "latitude"

.field public static final LINK:Ljava/lang/String; = "link"

.field public static final LOCATION_NAME:Ljava/lang/String; = "location_name"

.field public static final LONGITUDE:Ljava/lang/String; = "longitude"

.field public static final MEDIA_URL:Ljava/lang/String; = "media_url"

.field public static final MESSAGE:Ljava/lang/String; = "message"

.field public static final OBJECT_TYPE:Ljava/lang/String; = "object_type"

.field public static final POST_ID:Ljava/lang/String; = "post_id"

.field public static final SP_TYPE:Ljava/lang/String; = "sp_type"

.field public static final TIMESTAMP_UTC:Ljava/lang/String; = "timestamp_utc"

.field public static final TIME_ZONE:Ljava/lang/String; = "time_zone"

.field public static final UPDATE_TIME:Ljava/lang/String; = "updated_time"

.field public static final WIDTH:Ljava/lang/String; = "width"
