.class public Lcom/sec/android/app/sns3/agent/sp/twitter/db/SnsTwitterContentProvider;
.super Landroid/content/ContentProvider;
.source "SnsTwitterContentProvider.java"


# static fields
.field private static final DETAIL_VIEW:I = 0x12c

.field private static final FRIENDS_PROFILE_INFO:I = 0x1f4

.field private static final SSO_ACCOUNT:I = 0x2328

.field private static final STATUS_STREAM:I = 0x258

.field private static final TIMELINES:I = 0x64

.field private static final URI_SSO_ACCOUNT:Ljava/lang/String; = "sso_account"

.field private static final USER_BASIC_INFO:I = 0x190

.field private static final USER_TIMELINES:I = 0xc8

.field private static final WIPE_TW_DATA:I = 0x7d0

.field private static final uriMatcher:Landroid/content/UriMatcher;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mOpenHelper:Lcom/sec/android/app/sns3/agent/sp/twitter/db/SnsTwitterDBHelper;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 326
    new-instance v0, Landroid/content/UriMatcher;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    sput-object v0, Lcom/sec/android/app/sns3/agent/sp/twitter/db/SnsTwitterContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    .line 328
    sget-object v0, Lcom/sec/android/app/sns3/agent/sp/twitter/db/SnsTwitterContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.sns3.sp.twitter"

    const-string v2, "timeline"

    const/16 v3, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 329
    sget-object v0, Lcom/sec/android/app/sns3/agent/sp/twitter/db/SnsTwitterContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.sns3.sp.twitter"

    const-string v2, "user_basic_info"

    const/16 v3, 0x190

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 331
    sget-object v0, Lcom/sec/android/app/sns3/agent/sp/twitter/db/SnsTwitterContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.sns3.sp.twitter"

    const-string v2, "usertimeline"

    const/16 v3, 0xc8

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 333
    sget-object v0, Lcom/sec/android/app/sns3/agent/sp/twitter/db/SnsTwitterContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.sns3.sp.twitter"

    const-string v2, "detailview"

    const/16 v3, 0x12c

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 334
    sget-object v0, Lcom/sec/android/app/sns3/agent/sp/twitter/db/SnsTwitterContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.sns3.sp.twitter"

    const-string v2, "friends_profile_info"

    const/16 v3, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 336
    sget-object v0, Lcom/sec/android/app/sns3/agent/sp/twitter/db/SnsTwitterContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.sns3.sp.twitter"

    const-string v2, "status_stream"

    const/16 v3, 0x258

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 338
    sget-object v0, Lcom/sec/android/app/sns3/agent/sp/twitter/db/SnsTwitterContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.sns3.sp.twitter"

    const-string v2, "wipe_tw_data"

    const/16 v3, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 342
    sget-object v0, Lcom/sec/android/app/sns3/agent/sp/twitter/db/SnsTwitterContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.sns3.sp.twitter"

    const-string v2, "sso_account"

    const/16 v3, 0x2328

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 343
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    .line 47
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/sns3/agent/sp/twitter/db/SnsTwitterContentProvider;->mOpenHelper:Lcom/sec/android/app/sns3/agent/sp/twitter/db/SnsTwitterDBHelper;

    return-void
.end method


# virtual methods
.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 6
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "selection"    # Ljava/lang/String;
    .param p3, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 231
    const/4 v1, 0x0

    .line 232
    .local v1, "db":Landroid/database/sqlite/SQLiteDatabase;
    sget-object v3, Lcom/sec/android/app/sns3/agent/sp/twitter/db/SnsTwitterContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v3, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v2

    .line 233
    .local v2, "match":I
    const/4 v0, -0x1

    .line 235
    .local v0, "count":I
    iget-object v3, p0, Lcom/sec/android/app/sns3/agent/sp/twitter/db/SnsTwitterContentProvider;->mOpenHelper:Lcom/sec/android/app/sns3/agent/sp/twitter/db/SnsTwitterDBHelper;

    invoke-virtual {v3}, Lcom/sec/android/app/sns3/agent/sp/twitter/db/SnsTwitterDBHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 237
    sparse-switch v2, :sswitch_data_0

    .line 275
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Unknown URI : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 239
    :sswitch_0
    const-string v3, "timeline"

    invoke-static {v1, v3, v4, v4}, Lcom/sec/android/app/sns3/agent/db/SnsDBWrapper;->onDelete(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 278
    :goto_0
    return v0

    .line 244
    :sswitch_1
    const-string v3, "usertimeline"

    invoke-static {v1, v3, v4, v4}, Lcom/sec/android/app/sns3/agent/db/SnsDBWrapper;->onDelete(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 247
    goto :goto_0

    .line 250
    :sswitch_2
    const-string v3, "detailview"

    invoke-static {v1, v3, v4, v4}, Lcom/sec/android/app/sns3/agent/db/SnsDBWrapper;->onDelete(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 252
    goto :goto_0

    .line 254
    :sswitch_3
    const-string v3, "user_basic_info"

    invoke-static {v1, v3, v4, v4}, Lcom/sec/android/app/sns3/agent/db/SnsDBWrapper;->onDelete(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 257
    goto :goto_0

    .line 259
    :sswitch_4
    const-string v3, "friends_profile_info"

    invoke-static {v1, v3, v4, v4}, Lcom/sec/android/app/sns3/agent/db/SnsDBWrapper;->onDelete(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 262
    goto :goto_0

    .line 264
    :sswitch_5
    const-string v3, "status_stream"

    invoke-static {v1, v3, p2, p3}, Lcom/sec/android/app/sns3/agent/db/SnsDBWrapper;->onDelete(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 267
    goto :goto_0

    .line 270
    :sswitch_6
    iget-object v3, p0, Lcom/sec/android/app/sns3/agent/sp/twitter/db/SnsTwitterContentProvider;->mOpenHelper:Lcom/sec/android/app/sns3/agent/sp/twitter/db/SnsTwitterDBHelper;

    invoke-virtual {v3, v1}, Lcom/sec/android/app/sns3/agent/sp/twitter/db/SnsTwitterDBHelper;->wipeData(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_0

    .line 237
    nop

    :sswitch_data_0
    .sparse-switch
        0x64 -> :sswitch_0
        0xc8 -> :sswitch_1
        0x12c -> :sswitch_2
        0x190 -> :sswitch_3
        0x1f4 -> :sswitch_4
        0x258 -> :sswitch_5
        0x7d0 -> :sswitch_6
    .end sparse-switch
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 3
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 150
    sget-object v0, Lcom/sec/android/app/sns3/agent/sp/twitter/db/SnsTwitterContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 178
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown URI : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 152
    :sswitch_0
    const-string v0, "vnd.android.cursor.dir/vnd.com.sec.android.app.sns3.sp.twitter.timeline"

    .line 173
    :goto_0
    return-object v0

    .line 156
    :sswitch_1
    const-string v0, "vnd.android.cursor.dir/vnd.com.sec.android.app.sns3.sp.twitter.usertimeline"

    goto :goto_0

    .line 160
    :sswitch_2
    const-string v0, "vnd.android.cursor.dir/vnd.com.sec.android.app.sns3.sp.twitter.detailview"

    goto :goto_0

    .line 163
    :sswitch_3
    const-string v0, "vnd.android.cursor.dir/vnd.com.sec.android.app.sns3.sp.twitter.user"

    goto :goto_0

    .line 166
    :sswitch_4
    const-string v0, "vnd.android.cursor.dir/vnd.com.sec.android.app.sns3.sp.twitter.friendsprofile"

    goto :goto_0

    .line 169
    :sswitch_5
    const-string v0, "vnd.android.cursor.dir/vnd.com.sec.android.app.sns3.sp.twitter.status_stream"

    goto :goto_0

    .line 173
    :sswitch_6
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "com.twitter.android.auth.login/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/twitter/db/SnsTwitterContentProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f08006d

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 150
    nop

    :sswitch_data_0
    .sparse-switch
        0x64 -> :sswitch_0
        0xc8 -> :sswitch_1
        0x12c -> :sswitch_2
        0x190 -> :sswitch_3
        0x1f4 -> :sswitch_4
        0x258 -> :sswitch_5
        0x2328 -> :sswitch_6
    .end sparse-switch
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 9
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;

    .prologue
    const/4 v3, 0x0

    .line 184
    const/4 v0, 0x0

    .line 185
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    sget-object v6, Lcom/sec/android/app/sns3/agent/sp/twitter/db/SnsTwitterContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v6, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v2

    .line 186
    .local v2, "match":I
    const-wide/16 v4, -0x1

    .line 189
    .local v4, "rowId":J
    :try_start_0
    iget-object v6, p0, Lcom/sec/android/app/sns3/agent/sp/twitter/db/SnsTwitterContentProvider;->mOpenHelper:Lcom/sec/android/app/sns3/agent/sp/twitter/db/SnsTwitterDBHelper;

    invoke-virtual {v6}, Lcom/sec/android/app/sns3/agent/sp/twitter/db/SnsTwitterDBHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 191
    sparse-switch v2, :sswitch_data_0

    .line 220
    new-instance v6, Ljava/lang/IllegalArgumentException;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Unknown URI : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v6
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 222
    :catch_0
    move-exception v1

    .line 223
    .local v1, "e":Landroid/database/sqlite/SQLiteException;
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    .line 226
    .end local v1    # "e":Landroid/database/sqlite/SQLiteException;
    :goto_0
    const-wide/16 v6, -0x1

    cmp-long v6, v4, v6

    if-nez v6, :cond_0

    move-object p1, v3

    .end local p1    # "uri":Landroid/net/Uri;
    :cond_0
    return-object p1

    .line 193
    .restart local p1    # "uri":Landroid/net/Uri;
    :sswitch_0
    :try_start_1
    const-string v6, "timeline"

    const/4 v7, 0x0

    invoke-virtual {v0, v6, v7, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v4

    .line 195
    goto :goto_0

    .line 198
    :sswitch_1
    const-string v6, "usertimeline"

    const/4 v7, 0x0

    invoke-virtual {v0, v6, v7, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v4

    .line 200
    goto :goto_0

    .line 203
    :sswitch_2
    const-string v6, "detailview"

    const/4 v7, 0x0

    invoke-virtual {v0, v6, v7, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v4

    .line 205
    goto :goto_0

    .line 207
    :sswitch_3
    const-string v6, "user_basic_info"

    const/4 v7, 0x0

    invoke-virtual {v0, v6, v7, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v4

    .line 209
    goto :goto_0

    .line 211
    :sswitch_4
    const-string v6, "friends_profile_info"

    const/4 v7, 0x0

    invoke-virtual {v0, v6, v7, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v4

    .line 213
    goto :goto_0

    .line 215
    :sswitch_5
    const-string v6, "status_stream"

    const/4 v7, 0x0

    invoke-virtual {v0, v6, v7, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-wide v4

    .line 217
    goto :goto_0

    .line 191
    :sswitch_data_0
    .sparse-switch
        0x64 -> :sswitch_0
        0xc8 -> :sswitch_1
        0x12c -> :sswitch_2
        0x190 -> :sswitch_3
        0x1f4 -> :sswitch_4
        0x258 -> :sswitch_5
    .end sparse-switch
.end method

.method public onCreate()Z
    .locals 2

    .prologue
    .line 73
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/twitter/db/SnsTwitterContentProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/agent/sp/twitter/db/SnsTwitterContentProvider;->mContext:Landroid/content/Context;

    .line 74
    new-instance v0, Lcom/sec/android/app/sns3/agent/sp/twitter/db/SnsTwitterDBHelper;

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/twitter/db/SnsTwitterContentProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/app/sns3/agent/sp/twitter/db/SnsTwitterDBHelper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/sns3/agent/sp/twitter/db/SnsTwitterContentProvider;->mOpenHelper:Lcom/sec/android/app/sns3/agent/sp/twitter/db/SnsTwitterDBHelper;

    .line 75
    const/4 v0, 0x0

    return v0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 16
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "sortOrder"    # Ljava/lang/String;

    .prologue
    .line 81
    const/4 v3, 0x0

    .line 82
    .local v3, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v10, 0x0

    .line 84
    .local v10, "c":Landroid/database/Cursor;
    sget-object v4, Lcom/sec/android/app/sns3/agent/sp/twitter/db/SnsTwitterContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v13

    .line 86
    .local v13, "match":I
    const/4 v7, 0x0

    .line 88
    .local v7, "groupby":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/sns3/agent/sp/twitter/db/SnsTwitterContentProvider;->mOpenHelper:Lcom/sec/android/app/sns3/agent/sp/twitter/db/SnsTwitterDBHelper;

    invoke-virtual {v4}, Lcom/sec/android/app/sns3/agent/sp/twitter/db/SnsTwitterDBHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    .line 89
    new-instance v2, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v2}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 91
    .local v2, "queryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    sparse-switch v13, :sswitch_data_0

    .line 132
    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Unknown URI : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 93
    :sswitch_0
    const-string v4, "timeline"

    invoke-virtual {v2, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 137
    :cond_0
    :goto_0
    const/4 v8, 0x0

    move-object/from16 v4, p2

    move-object/from16 v5, p3

    move-object/from16 v6, p4

    move-object/from16 v9, p5

    :try_start_0
    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v10

    .line 144
    :goto_1
    return-object v10

    .line 98
    :sswitch_1
    const-string v4, "usertimeline"

    invoke-virtual {v2, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto :goto_0

    .line 103
    :sswitch_2
    const-string v4, "detailview"

    invoke-virtual {v2, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto :goto_0

    .line 107
    :sswitch_3
    const-string v4, "user_basic_info"

    invoke-virtual {v2, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto :goto_0

    .line 111
    :sswitch_4
    const-string v4, "friends_profile_info"

    invoke-virtual {v2, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto :goto_0

    .line 115
    :sswitch_5
    const-string v4, "status_stream"

    invoke-virtual {v2, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 117
    const-string v4, "update"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 118
    .local v14, "updateParameter":Ljava/lang/String;
    const-string v4, "true"

    invoke-virtual {v4, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 119
    const-string v4, "user_id"

    move-object/from16 v0, p3

    move-object/from16 v1, p4

    invoke-static {v0, v1, v4}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->parseQueryParam(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 121
    .local v15, "userId":Ljava/lang/String;
    if-eqz v15, :cond_0

    .line 122
    new-instance v12, Landroid/content/Intent;

    const-string v4, "com.sec.android.app.sns.profile.ACTION_TWITTER_REQUESTED"

    invoke-direct {v12, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 123
    .local v12, "intent":Landroid/content/Intent;
    const-string v4, "id"

    invoke-virtual {v12, v4, v15}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 124
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/sns3/agent/sp/twitter/db/SnsTwitterContentProvider;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v12}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0

    .line 140
    .end local v12    # "intent":Landroid/content/Intent;
    .end local v14    # "updateParameter":Ljava/lang/String;
    .end local v15    # "userId":Ljava/lang/String;
    :catch_0
    move-exception v11

    .line 141
    .local v11, "e":Landroid/database/sqlite/SQLiteException;
    invoke-virtual {v11}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    goto :goto_1

    .line 91
    nop

    :sswitch_data_0
    .sparse-switch
        0x64 -> :sswitch_0
        0xc8 -> :sswitch_1
        0x12c -> :sswitch_2
        0x190 -> :sswitch_3
        0x1f4 -> :sswitch_4
        0x258 -> :sswitch_5
    .end sparse-switch
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 6
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 283
    const/4 v0, 0x0

    .line 284
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v2, 0x0

    .line 285
    .local v2, "table":Ljava/lang/String;
    sget-object v3, Lcom/sec/android/app/sns3/agent/sp/twitter/db/SnsTwitterContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v3, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v1

    .line 287
    .local v1, "match":I
    iget-object v3, p0, Lcom/sec/android/app/sns3/agent/sp/twitter/db/SnsTwitterContentProvider;->mOpenHelper:Lcom/sec/android/app/sns3/agent/sp/twitter/db/SnsTwitterDBHelper;

    invoke-virtual {v3}, Lcom/sec/android/app/sns3/agent/sp/twitter/db/SnsTwitterDBHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 289
    sparse-switch v1, :sswitch_data_0

    .line 318
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Unknown URI : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 291
    :sswitch_0
    const-string v2, "timeline"

    .line 321
    :goto_0
    invoke-static {v0, v2, p2, p3, p4}, Lcom/sec/android/app/sns3/agent/db/SnsDBWrapper;->onUpdate(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v3

    return v3

    .line 296
    :sswitch_1
    const-string v2, "usertimeline"

    .line 298
    goto :goto_0

    .line 301
    :sswitch_2
    const-string v2, "detailview"

    .line 303
    goto :goto_0

    .line 305
    :sswitch_3
    const-string v2, "user_basic_info"

    .line 307
    goto :goto_0

    .line 309
    :sswitch_4
    const-string v2, "friends_profile_info"

    .line 311
    goto :goto_0

    .line 313
    :sswitch_5
    const-string v2, "status_stream"

    .line 315
    goto :goto_0

    .line 289
    :sswitch_data_0
    .sparse-switch
        0x64 -> :sswitch_0
        0xc8 -> :sswitch_1
        0x12c -> :sswitch_2
        0x190 -> :sswitch_3
        0x1f4 -> :sswitch_4
        0x258 -> :sswitch_5
    .end sparse-switch
.end method
