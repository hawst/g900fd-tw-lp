.class public Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;
.super Ljava/lang/Object;
.source "SnsRequestResult.java"


# static fields
.field public static final ERROR_MESSAGE:Ljava/lang/String; = "error_message"

.field public static final HTTP_STATUS:Ljava/lang/String; = "status"


# instance fields
.field private mErrorCode:I

.field private mHttpstatus:I

.field private mReason:Landroid/os/Bundle;

.field private mReqID:I

.field private mResponse:Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;

.field private mbSuccess:Z


# direct methods
.method public constructor <init>(IZIILandroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;)V
    .locals 0
    .param p1, "reqID"    # I
    .param p2, "bSuccess"    # Z
    .param p3, "httpStatus"    # I
    .param p4, "errorCode"    # I
    .param p5, "reason"    # Landroid/os/Bundle;
    .param p6, "response"    # Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput p1, p0, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->mReqID:I

    .line 45
    iput-boolean p2, p0, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->mbSuccess:Z

    .line 46
    iput p3, p0, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->mHttpstatus:I

    .line 47
    iput p4, p0, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->mErrorCode:I

    .line 48
    iput-object p5, p0, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->mReason:Landroid/os/Bundle;

    .line 49
    iput-object p6, p0, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->mResponse:Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;

    .line 50
    return-void
.end method


# virtual methods
.method public getErrorCode()I
    .locals 1

    .prologue
    .line 65
    iget v0, p0, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->mErrorCode:I

    return v0
.end method

.method public getHttpstatus()I
    .locals 1

    .prologue
    .line 61
    iget v0, p0, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->mHttpstatus:I

    return v0
.end method

.method public getReason()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->mReason:Landroid/os/Bundle;

    return-object v0
.end method

.method public getReqID()I
    .locals 1

    .prologue
    .line 53
    iget v0, p0, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->mReqID:I

    return v0
.end method

.method public getResponse()Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->mResponse:Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;

    return-object v0
.end method

.method public isSuccess()Z
    .locals 1

    .prologue
    .line 57
    iget-boolean v0, p0, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->mbSuccess:Z

    return v0
.end method

.method public setErrorCode(I)V
    .locals 0
    .param p1, "errorCode"    # I

    .prologue
    .line 78
    iput p1, p0, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->mErrorCode:I

    .line 79
    return-void
.end method
