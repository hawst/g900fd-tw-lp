.class public interface abstract Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$ViralUpdatesColumns;
.super Ljava/lang/Object;
.source "SnsLinkedInDB.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ViralUpdatesColumns"
.end annotation


# static fields
.field public static final ORIGINAL_POST_UPDATE_KEY:Ljava/lang/String; = "original_post_update_key"

.field public static final VIRAL_UPDATE_KEY:Ljava/lang/String; = "update_key"
