.class public Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponsePlaceType;
.super Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;
.source "SnsTwResponsePlaceType.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponsePlaceType;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public mCode:I

.field public mName:Ljava/lang/String;

.field public mNext:Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponsePlaceType;

.field public mTrendLocation:Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponsePlaceType;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 44
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponsePlaceType$1;

    invoke-direct {v0}, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponsePlaceType$1;-><init>()V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponsePlaceType;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;-><init>()V

    .line 38
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;-><init>()V

    .line 41
    invoke-direct {p0, p1}, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponsePlaceType;->readFromParcel(Landroid/os/Parcel;)V

    .line 42
    return-void
.end method

.method private readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 56
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponsePlaceType;->mName:Ljava/lang/String;

    .line 57
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponsePlaceType;->mCode:I

    .line 59
    const-class v0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponsePlaceType;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponsePlaceType;

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponsePlaceType;->mNext:Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponsePlaceType;

    .line 60
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 64
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 70
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponsePlaceType;->mName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 71
    iget v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponsePlaceType;->mCode:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 73
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponsePlaceType;->mNext:Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponsePlaceType;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 74
    return-void
.end method
