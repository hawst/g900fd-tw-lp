.class public interface abstract Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserCheckin$FacebookCheckin;
.super Ljava/lang/Object;
.source "SnsFbParserCheckin.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserCheckin;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "FacebookCheckin"
.end annotation


# static fields
.field public static final APPLICATION:Ljava/lang/String; = "application"

.field public static final APPLICATION_ID:Ljava/lang/String; = "id"

.field public static final APPLICATION_NAME:Ljava/lang/String; = "name"

.field public static final COMMENTS:Ljava/lang/String; = "comments"

.field public static final CREATED_TIME:Ljava/lang/String; = "created_time"

.field public static final FROM:Ljava/lang/String; = "from"

.field public static final ID:Ljava/lang/String; = "id"

.field public static final LIKES:Ljava/lang/String; = "likes"

.field public static final MESSAGE:Ljava/lang/String; = "message"

.field public static final PLACES:Ljava/lang/String; = "place"

.field public static final TAGS:Ljava/lang/String; = "tags"

.field public static final TYPE:Ljava/lang/String; = "type"
