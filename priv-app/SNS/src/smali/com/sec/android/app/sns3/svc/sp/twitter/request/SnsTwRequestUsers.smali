.class public abstract Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestUsers;
.super Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestBase;
.source "SnsTwRequestUsers.java"

# interfaces
.implements Lcom/sec/android/app/sns3/svc/sp/twitter/callback/ISnsTwReqCbUser;


# instance fields
.field private mUsers:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwUsersAPI;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwUsersAPI;Landroid/os/Bundle;)V
    .locals 1
    .param p1, "svcMgr"    # Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    .param p2, "users"    # Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwUsersAPI;
    .param p3, "param"    # Landroid/os/Bundle;

    .prologue
    .line 41
    const/16 v0, 0x16

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestBase;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;I)V

    .line 42
    invoke-virtual {p2}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwUsersAPI;->getUrl()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestUsers;->mUrl:Ljava/lang/String;

    .line 43
    invoke-virtual {p2}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwUsersAPI;->getHttpMethod()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestUsers;->mHttpMethod:Ljava/lang/String;

    .line 44
    iput-object p2, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestUsers;->mUsers:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwUsersAPI;

    .line 45
    iput-object p3, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestUsers;->mReqParams:Landroid/os/Bundle;

    .line 46
    return-void
.end method


# virtual methods
.method protected composeMore()V
    .locals 6

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestUsers;->mParams:Landroid/os/Bundle;

    const-string v1, "oauth_token"

    iget-object v2, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestUsers;->mToken:Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;

    invoke-virtual {v2}, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;->getAccessToken()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->encodeUrlEx(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestUsers;->mHttpMethod:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestUsers;->mReqUri:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestUsers;->mParams:Landroid/os/Bundle;

    iget-object v3, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestUsers;->mSubParams:Landroid/os/Bundle;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwitter;->CONSUMER_SECRET:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "&"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestUsers;->mToken:Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;

    invoke-virtual {v5}, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;->getAccessTokenSecret()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v1, v2, v3, v4}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/composer/SnsTwComposerUtils;->generateSignature(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestUsers;->mSig:Ljava/lang/String;

    .line 53
    return-void
.end method

.method protected parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;
    .locals 3
    .param p1, "content"    # Ljava/lang/String;

    .prologue
    .line 57
    const-string v1, "SNS"

    const-string v2, "<SnsTwRequestUsers> parse()"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 58
    invoke-super {p0, p1}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestBase;->parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;

    move-result-object v0

    .line 60
    .local v0, "response":Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;
    if-eqz v0, :cond_0

    .line 63
    .end local v0    # "response":Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;
    :goto_0
    return-object v0

    .restart local v0    # "response":Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;
    :cond_0
    invoke-static {p1}, Lcom/sec/android/app/sns3/svc/sp/twitter/parser/SnsTwParserUser;->parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseUser;

    move-result-object v0

    goto :goto_0
.end method

.method protected respond(Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;)Z
    .locals 7
    .param p1, "result"    # Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;

    .prologue
    .line 68
    const-string v0, "SNS"

    const-string v1, "<SnsTwRequestUsers> respond()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 70
    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->getReqID()I

    move-result v1

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->isSuccess()Z

    move-result v2

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->getHttpstatus()I

    move-result v3

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->getErrorCode()I

    move-result v4

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->getReason()Landroid/os/Bundle;

    move-result-object v5

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->getResponse()Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseUser;

    move-object v0, p0

    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestUsers;->onReqRespond(IZIILandroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseUser;)Z

    .line 74
    const/4 v0, 0x1

    return v0
.end method

.method protected setUrl()Ljava/lang/String;
    .locals 5

    .prologue
    .line 79
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 80
    .local v0, "sb":Ljava/lang/StringBuilder;
    iget-object v3, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestUsers;->mSubParams:Landroid/os/Bundle;

    const-string v4, "screen_name"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 81
    .local v1, "screenName":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestUsers;->mSubParams:Landroid/os/Bundle;

    const-string v4, "slug"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 83
    .local v2, "slug":Ljava/lang/String;
    const-string v3, "1.1"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 85
    if-eqz v1, :cond_1

    .line 86
    iget-object v3, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestUsers;->mUsers:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwUsersAPI;

    sget-object v4, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwUsersAPI;->USERS_PROFILEIMAGE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwUsersAPI;

    if-ne v3, v4, :cond_0

    .line 87
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestUsers;->mUrl:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ".json"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 88
    iget-object v3, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestUsers;->mSubParams:Landroid/os/Bundle;

    const-string v4, "screen_name"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 106
    :goto_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3

    .line 90
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestUsers;->mUrl:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 92
    :cond_1
    if-eqz v2, :cond_4

    .line 93
    iget-object v3, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestUsers;->mUsers:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwUsersAPI;

    sget-object v4, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwUsersAPI;->USERS_SUGGESTIONS_SLUG:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwUsersAPI;

    if-ne v3, v4, :cond_2

    .line 94
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestUsers;->mUrl:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ".json"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 95
    iget-object v3, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestUsers;->mSubParams:Landroid/os/Bundle;

    const-string v4, "slug"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    goto :goto_0

    .line 96
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestUsers;->mUsers:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwUsersAPI;

    sget-object v4, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwUsersAPI;->USERS_SUGGESTIONS_SLUG_MEMBERS:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwUsersAPI;

    if-ne v3, v4, :cond_3

    .line 97
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestUsers;->mUrl:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/members.json"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 98
    iget-object v3, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestUsers;->mSubParams:Landroid/os/Bundle;

    const-string v4, "slug"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    goto :goto_0

    .line 100
    :cond_3
    iget-object v3, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestUsers;->mUrl:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 103
    :cond_4
    iget-object v3, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestUsers;->mUrl:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method
