.class public interface abstract Lcom/sec/android/app/sns3/svc/sp/sinaweibo/parser/SnsSwParserStatus$SinaweiboFeed;
.super Ljava/lang/Object;
.source "SnsSwParserStatus.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/svc/sp/sinaweibo/parser/SnsSwParserStatus;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "SinaweiboFeed"
.end annotation


# static fields
.field public static final CREATED_TIME:Ljava/lang/String; = "created_at"

.field public static final DATA:Ljava/lang/String; = "data"

.field public static final FEED_ID:Ljava/lang/String; = "id"

.field public static final FROM:Ljava/lang/String; = "user"

.field public static final MESSAGE:Ljava/lang/String; = "text"

.field public static final PICTURE:Ljava/lang/String; = "bmiddle_pic"

.field public static final STATUSES:Ljava/lang/String; = "statuses"
