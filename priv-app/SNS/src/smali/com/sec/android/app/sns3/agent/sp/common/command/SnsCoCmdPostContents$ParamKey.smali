.class public Lcom/sec/android/app/sns3/agent/sp/common/command/SnsCoCmdPostContents$ParamKey;
.super Ljava/lang/Object;
.source "SnsCoCmdPostContents.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/agent/sp/common/command/SnsCoCmdPostContents;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ParamKey"
.end annotation


# static fields
.field public static final FB_OBJECT_ID:Ljava/lang/String; = "fb_object_id"

.field public static final FB_PHOTO:Ljava/lang/String; = "fb_photo"

.field public static final FB_PLACE:Ljava/lang/String; = "fb_place"

.field public static final FB_PRIVACY:Ljava/lang/String; = "fb_privacy"

.field public static final FB_TYPE_CHECKIN:Ljava/lang/String; = "fb_type_checkin"

.field public static final FB_TYPE_PHOTO:Ljava/lang/String; = "fb_type_photo"

.field public static final FB_TYPE_POST:Ljava/lang/String; = "fb_type_post"

.field public static final FB_TYPE_VIDEO:Ljava/lang/String; = "fb_type_video"

.field public static final FB_VIDEO:Ljava/lang/String; = "fb_video"

.field public static final FILE_NAME:Ljava/lang/String; = "filename"

.field public static final LATITUDE:Ljava/lang/String; = "latitude"

.field public static final LOGITUDE:Ljava/lang/String; = "logitude"

.field public static final MESSAGE:Ljava/lang/String; = "message"

.field public static final TW_PHOTO:Ljava/lang/String; = "tw_photo"

.field public static final TW_RECIPIENT_SCREEN_NAME:Ljava/lang/String; = "tw_recipient_screen_name"

.field public static final TW_STATUS_ID:Ljava/lang/String; = "tw_status_id"

.field public static final TW_TYPE_DM:Ljava/lang/String; = "tw_type_dm"

.field public static final TW_TYPE_PHOTO:Ljava/lang/String; = "tw_type_photo"

.field public static final TW_TYPE_STATUS:Ljava/lang/String; = "tw_type_status"

.field public static final TYPE_LIST:Ljava/lang/String; = "type_list"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 322
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
