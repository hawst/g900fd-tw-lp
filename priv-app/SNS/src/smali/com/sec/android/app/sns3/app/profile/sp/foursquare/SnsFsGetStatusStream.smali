.class public Lcom/sec/android/app/sns3/app/profile/sp/foursquare/SnsFsGetStatusStream;
.super Ljava/lang/Object;
.source "SnsFsGetStatusStream.java"


# static fields
.field protected static final TAG:Ljava/lang/String; = "SnsFsGetStatusStream"

.field private static mCmdHandler:Landroid/os/Handler;

.field private static mServiceMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000(Landroid/content/Context;ILjava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Landroid/content/Context;
    .param p1, "x1"    # I
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 40
    invoke-static {p0, p1, p2}, Lcom/sec/android/app/sns3/app/profile/sp/foursquare/SnsFsGetStatusStream;->invokeBroadcast(Landroid/content/Context;ILjava/lang/String;)V

    return-void
.end method

.method private static invokeBroadcast(Landroid/content/Context;ILjava/lang/String;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "resultCode"    # I
    .param p2, "id"    # Ljava/lang/String;

    .prologue
    .line 89
    const-string v1, "SnsFsGetStatusStream"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "invokeBroadcast() : resultCode = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 90
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.app.sns.profile.ACTION_FOURSQUARE_UPDATED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 91
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 92
    const-string v1, "result"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 93
    const-string v1, "com.sec.android.app.sns3.permission.RECEIVE_SNS_BROADCAST"

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    .line 94
    return-void
.end method

.method public static updateStatus(Landroid/content/Context;Ljava/lang/String;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "userId"    # Ljava/lang/String;

    .prologue
    .line 49
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/sns3/SnsApplication;->getSvcMgr()Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v2

    sput-object v2, Lcom/sec/android/app/sns3/app/profile/sp/foursquare/SnsFsGetStatusStream;->mServiceMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    .line 50
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/sns3/SnsApplication;->getAgentMgr()Lcom/sec/android/app/sns3/agent/SnsAgentMgr;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/sns3/agent/SnsAgentMgr;->getCommandMgr()Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;->getCommandMgrHandle()Landroid/os/Handler;

    move-result-object v2

    sput-object v2, Lcom/sec/android/app/sns3/app/profile/sp/foursquare/SnsFsGetStatusStream;->mCmdHandler:Landroid/os/Handler;

    .line 52
    sget-object v2, Lcom/sec/android/app/sns3/app/profile/sp/foursquare/SnsFsGetStatusStream;->mServiceMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    if-nez v2, :cond_0

    .line 53
    const-string v2, "SnsFsGetStatusStream"

    const-string v3, "[SNS] mSvcMgr is null !!"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 86
    :goto_0
    return-void

    .line 56
    :cond_0
    sget-object v2, Lcom/sec/android/app/sns3/app/profile/sp/foursquare/SnsFsGetStatusStream;->mCmdHandler:Landroid/os/Handler;

    if-nez v2, :cond_1

    .line 57
    const-string v2, "SnsFsGetStatusStream"

    const-string v3, "[SNS] mCmdHandler is null !!"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 61
    :cond_1
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v2

    const-string v3, "com.sec.android.app.sns3.foursquare"

    invoke-virtual {v2, v3}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 63
    .local v0, "account":[Landroid/accounts/Account;
    array-length v2, v0

    if-lez v2, :cond_2

    .line 64
    new-instance v1, Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdGetFeed;

    sget-object v2, Lcom/sec/android/app/sns3/app/profile/sp/foursquare/SnsFsGetStatusStream;->mServiceMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    sget-object v3, Lcom/sec/android/app/sns3/app/profile/sp/foursquare/SnsFsGetStatusStream;->mCmdHandler:Landroid/os/Handler;

    invoke-direct {v1, v2, v3, p1}, Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdGetFeed;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Handler;Ljava/lang/String;)V

    .line 65
    .local v1, "cmd":Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;
    new-instance v2, Lcom/sec/android/app/sns3/app/profile/sp/foursquare/SnsFsGetStatusStream$1;

    invoke-direct {v2, p0, p1}, Lcom/sec/android/app/sns3/app/profile/sp/foursquare/SnsFsGetStatusStream$1;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->setCommandCallback(Lcom/sec/android/app/sns3/agent/command/ISnsCommandCallback;)Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;

    .line 81
    invoke-virtual {v1}, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->send()I

    goto :goto_0

    .line 85
    .end local v1    # "cmd":Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;
    :cond_2
    const/4 v2, -0x1

    invoke-static {p0, v2, p1}, Lcom/sec/android/app/sns3/app/profile/sp/foursquare/SnsFsGetStatusStream;->invokeBroadcast(Landroid/content/Context;ILjava/lang/String;)V

    goto :goto_0
.end method
