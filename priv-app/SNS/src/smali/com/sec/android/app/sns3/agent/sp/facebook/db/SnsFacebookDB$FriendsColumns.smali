.class public interface abstract Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$FriendsColumns;
.super Ljava/lang/Object;
.source "SnsFacebookDB.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "FriendsColumns"
.end annotation


# static fields
.field public static final BIRTHDATE:Ljava/lang/String; = "friend_birthdate"

.field public static final CURRENT_LOCATION:Ljava/lang/String; = "current_location"

.field public static final CURRENT_STATUS_MESSAGE:Ljava/lang/String; = "friend_current_status"

.field public static final CURR_STATUS_COMMENT_COUNT:Ljava/lang/String; = "friend_current_status_comment_count"

.field public static final CURR_STATUS_TIME:Ljava/lang/String; = "friend_current_status_time"

.field public static final FRIENDEMAIL:Ljava/lang/String; = "friend_email"

.field public static final FRIENDNAME:Ljava/lang/String; = "friend_name"

.field public static final FRIENDPIC:Ljava/lang/String; = "friend_pic"

.field public static final FRIEND_ID:Ljava/lang/String; = "friend_id"

.field public static final GENDER:Ljava/lang/String; = "gender"

.field public static final HOMETOWN_LOCATION:Ljava/lang/String; = "hometown_location"

.field public static final INTERESTS:Ljava/lang/String; = "interests"

.field public static final LOCALE:Ljava/lang/String; = "locale"

.field public static final ONLINEPRESENCE:Ljava/lang/String; = "friend_availability"

.field public static final RELATIONSHIP:Ljava/lang/String; = "friend_relationship"

.field public static final RELATIONSHIP_STATUS:Ljava/lang/String; = "friend_relation_status"

.field public static final UPDATE_TIME:Ljava/lang/String; = "update_time"
