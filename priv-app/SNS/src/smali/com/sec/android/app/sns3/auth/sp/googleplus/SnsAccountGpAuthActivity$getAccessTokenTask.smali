.class Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity$getAccessTokenTask;
.super Landroid/os/AsyncTask;
.source "SnsAccountGpAuthActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "getAccessTokenTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field accessTokenResponse:Lcom/google/api/client/auth/oauth2/draft10/AccessTokenResponse;

.field final synthetic this$0:Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;)V
    .locals 0

    .prologue
    .line 569
    iput-object p1, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity$getAccessTokenTask;->this$0:Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;
    .param p2, "x1"    # Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity$1;

    .prologue
    .line 569
    invoke-direct {p0, p1}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity$getAccessTokenTask;-><init>(Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 569
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity$getAccessTokenTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 11
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    const/4 v10, 0x0

    .line 575
    :try_start_0
    new-instance v0, Lcom/google/api/client/googleapis/auth/oauth2/draft10/GoogleAccessTokenRequest$GoogleAuthorizationCodeGrant;

    new-instance v1, Lcom/google/api/client/http/javanet/NetHttpTransport;

    invoke-direct {v1}, Lcom/google/api/client/http/javanet/NetHttpTransport;-><init>()V

    new-instance v2, Lcom/google/api/client/json/jackson/JacksonFactory;

    invoke-direct {v2}, Lcom/google/api/client/json/jackson/JacksonFactory;-><init>()V

    sget-object v3, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGooglePlus;->CLIENT_ID:Ljava/lang/String;

    sget-object v4, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGooglePlus;->CLIENT_SECRET:Ljava/lang/String;

    iget-object v5, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity$getAccessTokenTask;->this$0:Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;

    # getter for: Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->mCode:Ljava/lang/String;
    invoke-static {v5}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->access$600(Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "http://localhost"

    invoke-direct/range {v0 .. v6}, Lcom/google/api/client/googleapis/auth/oauth2/draft10/GoogleAccessTokenRequest$GoogleAuthorizationCodeGrant;-><init>(Lcom/google/api/client/http/HttpTransport;Lcom/google/api/client/json/JsonFactory;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/api/client/googleapis/auth/oauth2/draft10/GoogleAccessTokenRequest$GoogleAuthorizationCodeGrant;->execute()Lcom/google/api/client/auth/oauth2/draft10/AccessTokenResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity$getAccessTokenTask;->accessTokenResponse:Lcom/google/api/client/auth/oauth2/draft10/AccessTokenResponse;

    .line 579
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity$getAccessTokenTask;->accessTokenResponse:Lcom/google/api/client/auth/oauth2/draft10/AccessTokenResponse;

    if-eqz v0, :cond_2

    .line 581
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity$getAccessTokenTask;->this$0:Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;

    # getter for: Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->mRetryLogin:Z
    invoke-static {v0}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->access$200(Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 583
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity$getAccessTokenTask;->this$0:Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;

    # getter for: Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->mAppContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->access$500(Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    const-string v1, "com.sec.android.app.sns3.googleplus"

    invoke-virtual {v0, v1}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v7

    .line 585
    .local v7, "accounts":[Landroid/accounts/Account;
    if-eqz v7, :cond_0

    array-length v0, v7

    if-lez v0, :cond_0

    .line 586
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity$getAccessTokenTask;->this$0:Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;

    # getter for: Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->mAppContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->access$500(Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    const/4 v1, 0x0

    aget-object v1, v7, v1

    iget-object v2, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity$getAccessTokenTask;->this$0:Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;

    iget-object v2, v2, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->mAccountManagerRemoveCallback:Landroid/accounts/AccountManagerCallback;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/accounts/AccountManager;->removeAccount(Landroid/accounts/Account;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    .line 589
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity$getAccessTokenTask;->this$0:Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->mRetryLogin:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->access$202(Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;Z)Z

    .line 642
    .end local v7    # "accounts":[Landroid/accounts/Account;
    :goto_0
    return-object v10

    .line 592
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity$getAccessTokenTask;->this$0:Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;

    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity$getAccessTokenTask;->accessTokenResponse:Lcom/google/api/client/auth/oauth2/draft10/AccessTokenResponse;

    iget-object v1, v1, Lcom/google/api/client/auth/oauth2/draft10/AccessTokenResponse;->accessToken:Ljava/lang/String;

    # setter for: Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->mAccessToken:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->access$302(Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 593
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity$getAccessTokenTask;->this$0:Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;

    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity$getAccessTokenTask;->accessTokenResponse:Lcom/google/api/client/auth/oauth2/draft10/AccessTokenResponse;

    iget-object v1, v1, Lcom/google/api/client/auth/oauth2/draft10/AccessTokenResponse;->refreshToken:Ljava/lang/String;

    # setter for: Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->mRefreshToken:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->access$402(Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 594
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity$getAccessTokenTask;->this$0:Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;

    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity$getAccessTokenTask;->this$0:Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;

    # getter for: Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->mAccessToken:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->access$300(Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity$getAccessTokenTask;->this$0:Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;

    # getter for: Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->mRefreshToken:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->access$400(Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->setAuthTokenNExpires(Ljava/lang/String;Ljava/lang/String;)Z

    .line 596
    new-instance v8, Lcom/sec/android/app/sns3/agent/sp/googleplus/command/SnsGpCmdAuthLogin;

    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/SnsApplication;->getSvcMgr()Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v0

    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/sns3/SnsApplication;->getAgentMgr()Lcom/sec/android/app/sns3/agent/SnsAgentMgr;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/sns3/agent/SnsAgentMgr;->getCommandMgr()Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;->getCommandMgrHandle()Landroid/os/Handler;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v8, v0, v1, v2}, Lcom/sec/android/app/sns3/agent/sp/googleplus/command/SnsGpCmdAuthLogin;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Handler;Landroid/os/Bundle;)V

    .line 600
    .local v8, "cmd":Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;
    new-instance v0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity$getAccessTokenTask$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity$getAccessTokenTask$1;-><init>(Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity$getAccessTokenTask;)V

    invoke-virtual {v8, v0}, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->setCommandCallback(Lcom/sec/android/app/sns3/agent/command/ISnsCommandCallback;)Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;

    .line 628
    invoke-virtual {v8}, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->send()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 638
    .end local v8    # "cmd":Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;
    :catch_0
    move-exception v9

    .line 639
    .local v9, "e":Ljava/io/IOException;
    invoke-virtual {v9}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 633
    .end local v9    # "e":Ljava/io/IOException;
    :cond_2
    :try_start_1
    # getter for: Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->access$100()Ljava/lang/String;

    move-result-object v0

    const-string v1, "SnsAccountLiAuthActivity - access token is null!!"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 635
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity$getAccessTokenTask;->this$0:Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;

    const/4 v1, -0x1

    const/4 v2, -0x1

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthActivity;->loginFail(IILjava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method
