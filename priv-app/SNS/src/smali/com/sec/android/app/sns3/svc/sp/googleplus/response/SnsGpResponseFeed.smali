.class public final Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;
.super Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;
.source "SnsGpResponseFeed.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public mCommentsCount:Ljava/lang/String;

.field public mCommentsUrl:Ljava/lang/String;

.field public mContentUrl:Ljava/lang/String;

.field public mFromId:Ljava/lang/String;

.field public mFromName:Ljava/lang/String;

.field public mFromProfilePic:Ljava/lang/String;

.field public mFromUrl:Ljava/lang/String;

.field public mHeight:Ljava/lang/String;

.field public mLatitude:Ljava/lang/Double;

.field public mLikesCount:Ljava/lang/String;

.field public mLikesUrl:Ljava/lang/String;

.field public mLocationName:Ljava/lang/String;

.field public mLongitude:Ljava/lang/Double;

.field public mMediaUrl:Ljava/lang/String;

.field public mNext:Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;

.field public mObjectType:Ljava/lang/String;

.field public mPostId:Ljava/lang/String;

.field public mResharesCount:Ljava/lang/String;

.field public mResharesUrl:Ljava/lang/String;

.field public mTitle:Ljava/lang/String;

.field public mUpdateTime:Ljava/lang/String;

.field public mWidth:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 72
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed$1;

    invoke-direct {v0}, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed$1;-><init>()V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 83
    invoke-direct {p0}, Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;-><init>()V

    .line 56
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;->mMediaUrl:Ljava/lang/String;

    .line 84
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 86
    invoke-direct {p0}, Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;-><init>()V

    .line 56
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;->mMediaUrl:Ljava/lang/String;

    .line 87
    invoke-virtual {p0, p1}, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;->readFromParcel(Landroid/os/Parcel;)V

    .line 88
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed$1;

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 147
    const/4 v0, 0x0

    return v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 118
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;->mPostId:Ljava/lang/String;

    .line 119
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;->mTitle:Ljava/lang/String;

    .line 120
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;->mUpdateTime:Ljava/lang/String;

    .line 121
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;->mFromId:Ljava/lang/String;

    .line 122
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;->mFromName:Ljava/lang/String;

    .line 123
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;->mFromUrl:Ljava/lang/String;

    .line 124
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;->mFromProfilePic:Ljava/lang/String;

    .line 125
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;->mObjectType:Ljava/lang/String;

    .line 126
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;->mCommentsCount:Ljava/lang/String;

    .line 127
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;->mCommentsUrl:Ljava/lang/String;

    .line 128
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;->mLikesCount:Ljava/lang/String;

    .line 129
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;->mLikesUrl:Ljava/lang/String;

    .line 130
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;->mResharesCount:Ljava/lang/String;

    .line 131
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;->mResharesUrl:Ljava/lang/String;

    .line 132
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;->mMediaUrl:Ljava/lang/String;

    .line 133
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;->mContentUrl:Ljava/lang/String;

    .line 134
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;->mWidth:Ljava/lang/String;

    .line 135
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;->mHeight:Ljava/lang/String;

    .line 136
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;->mLocationName:Ljava/lang/String;

    .line 137
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;->mLatitude:Ljava/lang/Double;

    .line 138
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;->mLongitude:Ljava/lang/Double;

    .line 140
    const-class v0, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;->mNext:Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;

    .line 142
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flag"    # I

    .prologue
    .line 92
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;->mPostId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 93
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;->mTitle:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 94
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;->mUpdateTime:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 95
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;->mFromId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 96
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;->mFromName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 97
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;->mFromUrl:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 98
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;->mFromProfilePic:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 99
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;->mObjectType:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 100
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;->mCommentsCount:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 101
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;->mCommentsUrl:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 102
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;->mLikesCount:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 103
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;->mLikesUrl:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 104
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;->mResharesCount:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 105
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;->mResharesUrl:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 106
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;->mMediaUrl:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 107
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;->mContentUrl:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 108
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;->mWidth:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 109
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;->mHeight:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 110
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;->mLocationName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 111
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;->mLatitude:Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 112
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;->mLongitude:Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 114
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;->mNext:Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 115
    return-void
.end method
