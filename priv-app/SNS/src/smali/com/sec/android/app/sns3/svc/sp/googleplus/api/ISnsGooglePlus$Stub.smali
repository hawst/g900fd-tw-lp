.class public abstract Lcom/sec/android/app/sns3/svc/sp/googleplus/api/ISnsGooglePlus$Stub;
.super Landroid/os/Binder;
.source "ISnsGooglePlus.java"

# interfaces
.implements Lcom/sec/android/app/sns3/svc/sp/googleplus/api/ISnsGooglePlus;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/svc/sp/googleplus/api/ISnsGooglePlus;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/googleplus/api/ISnsGooglePlus$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.sec.android.app.sns3.svc.sp.googleplus.api.ISnsGooglePlus"

.field static final TRANSACTION_getApplicationInfo:I = 0x1

.field static final TRANSACTION_getFeed:I = 0x4

.field static final TRANSACTION_getPlaceByNearbySearch:I = 0x6

.field static final TRANSACTION_getPlaceByRadarSearch:I = 0x8

.field static final TRANSACTION_getPlaceByTextSearch:I = 0x7

.field static final TRANSACTION_getPlaceDetails:I = 0x9

.field static final TRANSACTION_getProfile:I = 0x5

.field static final TRANSACTION_getUser:I = 0x3

.field static final TRANSACTION_setAuthTokenNExpires:I = 0x2


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 15
    const-string v0, "com.sec.android.app.sns3.svc.sp.googleplus.api.ISnsGooglePlus"

    invoke-virtual {p0, p0, v0}, Lcom/sec/android/app/sns3/svc/sp/googleplus/api/ISnsGooglePlus$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 16
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/sec/android/app/sns3/svc/sp/googleplus/api/ISnsGooglePlus;
    .locals 2
    .param p0, "obj"    # Landroid/os/IBinder;

    .prologue
    .line 23
    if-nez p0, :cond_0

    .line 24
    const/4 v0, 0x0

    .line 30
    :goto_0
    return-object v0

    .line 26
    :cond_0
    const-string v1, "com.sec.android.app.sns3.svc.sp.googleplus.api.ISnsGooglePlus"

    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 27
    .local v0, "iin":Landroid/os/IInterface;
    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/sec/android/app/sns3/svc/sp/googleplus/api/ISnsGooglePlus;

    if-eqz v1, :cond_1

    .line 28
    check-cast v0, Lcom/sec/android/app/sns3/svc/sp/googleplus/api/ISnsGooglePlus;

    goto :goto_0

    .line 30
    :cond_1
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/googleplus/api/ISnsGooglePlus$Stub$Proxy;

    .end local v0    # "iin":Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/sec/android/app/sns3/svc/sp/googleplus/api/ISnsGooglePlus$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 34
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 5
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 38
    sparse-switch p1, :sswitch_data_0

    .line 164
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v4

    :goto_0
    return v4

    .line 42
    :sswitch_0
    const-string v3, "com.sec.android.app.sns3.svc.sp.googleplus.api.ISnsGooglePlus"

    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 47
    :sswitch_1
    const-string v3, "com.sec.android.app.sns3.svc.sp.googleplus.api.ISnsGooglePlus"

    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 48
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/svc/sp/googleplus/api/ISnsGooglePlus$Stub;->getApplicationInfo()Ljava/util/Map;

    move-result-object v2

    .line 49
    .local v2, "_result":Ljava/util/Map;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 50
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeMap(Ljava/util/Map;)V

    goto :goto_0

    .line 55
    .end local v2    # "_result":Ljava/util/Map;
    :sswitch_2
    const-string v3, "com.sec.android.app.sns3.svc.sp.googleplus.api.ISnsGooglePlus"

    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 57
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 59
    .local v0, "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 60
    .local v1, "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/sns3/svc/sp/googleplus/api/ISnsGooglePlus$Stub;->setAuthTokenNExpires(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    .line 61
    .local v2, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 62
    if-eqz v2, :cond_0

    move v3, v4

    :goto_1
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    goto :goto_1

    .line 67
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v1    # "_arg1":Ljava/lang/String;
    .end local v2    # "_result":Z
    :sswitch_3
    const-string v3, "com.sec.android.app.sns3.svc.sp.googleplus.api.ISnsGooglePlus"

    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 69
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/sns3/svc/sp/googleplus/api/ISnsGooglePlusCallbackUser$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/android/app/sns3/svc/sp/googleplus/api/ISnsGooglePlusCallbackUser;

    move-result-object v0

    .line 70
    .local v0, "_arg0":Lcom/sec/android/app/sns3/svc/sp/googleplus/api/ISnsGooglePlusCallbackUser;
    invoke-virtual {p0, v0}, Lcom/sec/android/app/sns3/svc/sp/googleplus/api/ISnsGooglePlus$Stub;->getUser(Lcom/sec/android/app/sns3/svc/sp/googleplus/api/ISnsGooglePlusCallbackUser;)I

    move-result v2

    .line 71
    .local v2, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 72
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 77
    .end local v0    # "_arg0":Lcom/sec/android/app/sns3/svc/sp/googleplus/api/ISnsGooglePlusCallbackUser;
    .end local v2    # "_result":I
    :sswitch_4
    const-string v3, "com.sec.android.app.sns3.svc.sp.googleplus.api.ISnsGooglePlus"

    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 79
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/sns3/svc/sp/googleplus/api/ISnsGooglePlusCallbackFeeds$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/android/app/sns3/svc/sp/googleplus/api/ISnsGooglePlusCallbackFeeds;

    move-result-object v0

    .line 80
    .local v0, "_arg0":Lcom/sec/android/app/sns3/svc/sp/googleplus/api/ISnsGooglePlusCallbackFeeds;
    invoke-virtual {p0, v0}, Lcom/sec/android/app/sns3/svc/sp/googleplus/api/ISnsGooglePlus$Stub;->getFeed(Lcom/sec/android/app/sns3/svc/sp/googleplus/api/ISnsGooglePlusCallbackFeeds;)I

    move-result v2

    .line 81
    .restart local v2    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 82
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 87
    .end local v0    # "_arg0":Lcom/sec/android/app/sns3/svc/sp/googleplus/api/ISnsGooglePlusCallbackFeeds;
    .end local v2    # "_result":I
    :sswitch_5
    const-string v3, "com.sec.android.app.sns3.svc.sp.googleplus.api.ISnsGooglePlus"

    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 89
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/sns3/svc/sp/googleplus/api/ISnsGooglePlusCallbackProfile$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/android/app/sns3/svc/sp/googleplus/api/ISnsGooglePlusCallbackProfile;

    move-result-object v0

    .line 90
    .local v0, "_arg0":Lcom/sec/android/app/sns3/svc/sp/googleplus/api/ISnsGooglePlusCallbackProfile;
    invoke-virtual {p0, v0}, Lcom/sec/android/app/sns3/svc/sp/googleplus/api/ISnsGooglePlus$Stub;->getProfile(Lcom/sec/android/app/sns3/svc/sp/googleplus/api/ISnsGooglePlusCallbackProfile;)I

    move-result v2

    .line 91
    .restart local v2    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 92
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 97
    .end local v0    # "_arg0":Lcom/sec/android/app/sns3/svc/sp/googleplus/api/ISnsGooglePlusCallbackProfile;
    .end local v2    # "_result":I
    :sswitch_6
    const-string v3, "com.sec.android.app.sns3.svc.sp.googleplus.api.ISnsGooglePlus"

    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 99
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_1

    .line 100
    sget-object v3, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v3, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 106
    .local v0, "_arg0":Landroid/os/Bundle;
    :goto_2
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/sns3/svc/sp/googleplus/api/ISnsGooglePlusCallbackNearby$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/android/app/sns3/svc/sp/googleplus/api/ISnsGooglePlusCallbackNearby;

    move-result-object v1

    .line 107
    .local v1, "_arg1":Lcom/sec/android/app/sns3/svc/sp/googleplus/api/ISnsGooglePlusCallbackNearby;
    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/sns3/svc/sp/googleplus/api/ISnsGooglePlus$Stub;->getPlaceByNearbySearch(Landroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/googleplus/api/ISnsGooglePlusCallbackNearby;)I

    move-result v2

    .line 108
    .restart local v2    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 109
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 103
    .end local v0    # "_arg0":Landroid/os/Bundle;
    .end local v1    # "_arg1":Lcom/sec/android/app/sns3/svc/sp/googleplus/api/ISnsGooglePlusCallbackNearby;
    .end local v2    # "_result":I
    :cond_1
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/os/Bundle;
    goto :goto_2

    .line 114
    .end local v0    # "_arg0":Landroid/os/Bundle;
    :sswitch_7
    const-string v3, "com.sec.android.app.sns3.svc.sp.googleplus.api.ISnsGooglePlus"

    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 116
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_2

    .line 117
    sget-object v3, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v3, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 123
    .restart local v0    # "_arg0":Landroid/os/Bundle;
    :goto_3
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/sns3/svc/sp/googleplus/api/ISnsGooglePlusCallbackNearby$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/android/app/sns3/svc/sp/googleplus/api/ISnsGooglePlusCallbackNearby;

    move-result-object v1

    .line 124
    .restart local v1    # "_arg1":Lcom/sec/android/app/sns3/svc/sp/googleplus/api/ISnsGooglePlusCallbackNearby;
    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/sns3/svc/sp/googleplus/api/ISnsGooglePlus$Stub;->getPlaceByTextSearch(Landroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/googleplus/api/ISnsGooglePlusCallbackNearby;)I

    move-result v2

    .line 125
    .restart local v2    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 126
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 120
    .end local v0    # "_arg0":Landroid/os/Bundle;
    .end local v1    # "_arg1":Lcom/sec/android/app/sns3/svc/sp/googleplus/api/ISnsGooglePlusCallbackNearby;
    .end local v2    # "_result":I
    :cond_2
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/os/Bundle;
    goto :goto_3

    .line 131
    .end local v0    # "_arg0":Landroid/os/Bundle;
    :sswitch_8
    const-string v3, "com.sec.android.app.sns3.svc.sp.googleplus.api.ISnsGooglePlus"

    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 133
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_3

    .line 134
    sget-object v3, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v3, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 140
    .restart local v0    # "_arg0":Landroid/os/Bundle;
    :goto_4
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/sns3/svc/sp/googleplus/api/ISnsGooglePlusCallbackNearby$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/android/app/sns3/svc/sp/googleplus/api/ISnsGooglePlusCallbackNearby;

    move-result-object v1

    .line 141
    .restart local v1    # "_arg1":Lcom/sec/android/app/sns3/svc/sp/googleplus/api/ISnsGooglePlusCallbackNearby;
    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/sns3/svc/sp/googleplus/api/ISnsGooglePlus$Stub;->getPlaceByRadarSearch(Landroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/googleplus/api/ISnsGooglePlusCallbackNearby;)I

    move-result v2

    .line 142
    .restart local v2    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 143
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 137
    .end local v0    # "_arg0":Landroid/os/Bundle;
    .end local v1    # "_arg1":Lcom/sec/android/app/sns3/svc/sp/googleplus/api/ISnsGooglePlusCallbackNearby;
    .end local v2    # "_result":I
    :cond_3
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/os/Bundle;
    goto :goto_4

    .line 148
    .end local v0    # "_arg0":Landroid/os/Bundle;
    :sswitch_9
    const-string v3, "com.sec.android.app.sns3.svc.sp.googleplus.api.ISnsGooglePlus"

    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 150
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_4

    .line 151
    sget-object v3, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v3, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 157
    .restart local v0    # "_arg0":Landroid/os/Bundle;
    :goto_5
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/sns3/svc/sp/googleplus/api/ISnsGooglePlusCallbackPlaceDetails$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/android/app/sns3/svc/sp/googleplus/api/ISnsGooglePlusCallbackPlaceDetails;

    move-result-object v1

    .line 158
    .local v1, "_arg1":Lcom/sec/android/app/sns3/svc/sp/googleplus/api/ISnsGooglePlusCallbackPlaceDetails;
    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/sns3/svc/sp/googleplus/api/ISnsGooglePlus$Stub;->getPlaceDetails(Landroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/googleplus/api/ISnsGooglePlusCallbackPlaceDetails;)I

    move-result v2

    .line 159
    .restart local v2    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 160
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 154
    .end local v0    # "_arg0":Landroid/os/Bundle;
    .end local v1    # "_arg1":Lcom/sec/android/app/sns3/svc/sp/googleplus/api/ISnsGooglePlusCallbackPlaceDetails;
    .end local v2    # "_result":I
    :cond_4
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/os/Bundle;
    goto :goto_5

    .line 38
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x8 -> :sswitch_8
        0x9 -> :sswitch_9
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
