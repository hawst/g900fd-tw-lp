.class public interface abstract Lcom/sec/android/app/sns3/svc/sp/googleplus/api/ISnsGooglePlus;
.super Ljava/lang/Object;
.source "ISnsGooglePlus.java"

# interfaces
.implements Landroid/os/IInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/googleplus/api/ISnsGooglePlus$Stub;
    }
.end annotation


# virtual methods
.method public abstract getApplicationInfo()Ljava/util/Map;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract getFeed(Lcom/sec/android/app/sns3/svc/sp/googleplus/api/ISnsGooglePlusCallbackFeeds;)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract getPlaceByNearbySearch(Landroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/googleplus/api/ISnsGooglePlusCallbackNearby;)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract getPlaceByRadarSearch(Landroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/googleplus/api/ISnsGooglePlusCallbackNearby;)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract getPlaceByTextSearch(Landroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/googleplus/api/ISnsGooglePlusCallbackNearby;)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract getPlaceDetails(Landroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/googleplus/api/ISnsGooglePlusCallbackPlaceDetails;)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract getProfile(Lcom/sec/android/app/sns3/svc/sp/googleplus/api/ISnsGooglePlusCallbackProfile;)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract getUser(Lcom/sec/android/app/sns3/svc/sp/googleplus/api/ISnsGooglePlusCallbackUser;)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract setAuthTokenNExpires(Ljava/lang/String;Ljava/lang/String;)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method
