.class public Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserValue;
.super Ljava/lang/Object;
.source "SnsFbParserValue.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserValue$FacebookValues;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    return-void
.end method

.method public static parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseValue;
    .locals 11
    .param p0, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 40
    const/4 v7, 0x0

    .line 41
    .local v7, "value":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseValue;
    const/4 v0, 0x0

    .line 44
    .local v0, "curValue":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseValue;
    :try_start_0
    new-instance v3, Lorg/json/JSONArray;

    invoke-direct {v3, p0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 46
    .local v3, "ja":Lorg/json/JSONArray;
    if-eqz v3, :cond_1

    .line 47
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v8

    if-ge v2, v8, :cond_1

    .line 48
    invoke-virtual {v3, v2}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v5

    .line 50
    .local v5, "jsonObj":Lorg/json/JSONObject;
    new-instance v6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseValue;

    invoke-direct {v6}, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseValue;-><init>()V

    .line 52
    .local v6, "newValue":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseValue;
    const-string v8, "value"

    invoke-virtual {v5, v8}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, v6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseValue;->mValue:Ljava/lang/String;

    .line 53
    const-string v8, "end_time"

    invoke-virtual {v5, v8}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, v6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseValue;->mEndTime:Ljava/lang/String;

    .line 55
    if-nez v7, :cond_0

    .line 56
    move-object v7, v6

    .line 57
    move-object v0, v7

    .line 47
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 59
    :cond_0
    iput-object v6, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseValue;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseValue;

    .line 60
    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseValue;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseValue;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_1

    .line 65
    .end local v2    # "i":I
    .end local v3    # "ja":Lorg/json/JSONArray;
    .end local v5    # "jsonObj":Lorg/json/JSONObject;
    .end local v6    # "newValue":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseValue;
    :catch_0
    move-exception v4

    .line 66
    .local v4, "je":Lorg/json/JSONException;
    const-string v8, "SNS"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "parser : JSONException error :"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v4}, Lorg/json/JSONException;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 71
    .end local v4    # "je":Lorg/json/JSONException;
    :cond_1
    :goto_2
    return-object v7

    .line 67
    :catch_1
    move-exception v1

    .line 68
    .local v1, "e":Ljava/lang/Exception;
    const-string v8, "SNS"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "parser : Exception error : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method
