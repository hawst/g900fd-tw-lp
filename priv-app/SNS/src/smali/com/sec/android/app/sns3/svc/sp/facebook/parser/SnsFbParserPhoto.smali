.class public Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserPhoto;
.super Ljava/lang/Object;
.source "SnsFbParserPhoto.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserPhoto$FacebookPhoto;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    return-void
.end method

.method private static invokeReqGetComments(Ljava/lang/String;)V
    .locals 3
    .param p0, "PhotoID"    # Ljava/lang/String;

    .prologue
    .line 281
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserPhoto$2;

    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/sns3/SnsApplication;->getSvcMgr()Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, p0, v2, p0}, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserPhoto$2;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;)V

    .line 304
    .local v0, "req":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->request()Z

    .line 305
    return-void
.end method

.method private static invokeReqGetLikes(Ljava/lang/String;)V
    .locals 3
    .param p0, "photoID"    # Ljava/lang/String;

    .prologue
    .line 254
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserPhoto$1;

    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/sns3/SnsApplication;->getSvcMgr()Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, p0, v2, p0}, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserPhoto$1;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;)V

    .line 277
    .local v0, "req":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->request()Z

    .line 278
    return-void
.end method

.method public static parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;
    .locals 27
    .param p0, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 92
    new-instance v23, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;

    invoke-direct/range {v23 .. v23}, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;-><init>()V

    .line 95
    .local v23, "photo":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;
    :try_start_0
    new-instance v17, Lorg/json/JSONObject;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 97
    .local v17, "jsonObject":Lorg/json/JSONObject;
    const-string v24, "id"

    move-object/from16 v0, v17

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, v23

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;->mPhotoID:Ljava/lang/String;

    .line 99
    const-string v24, "from"

    move-object/from16 v0, v17

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v24

    if-eqz v24, :cond_0

    .line 100
    const-string v24, "from"

    move-object/from16 v0, v17

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v24 .. v24}, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserFrom;->parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFrom;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, v23

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;->mFrom:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFrom;

    .line 104
    :cond_0
    const-string v24, "name"

    move-object/from16 v0, v17

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, v23

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;->mPhotoName:Ljava/lang/String;

    .line 105
    const-string v24, "icon"

    move-object/from16 v0, v17

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, v23

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;->mIcon:Ljava/lang/String;

    .line 106
    const-string v24, "picture"

    move-object/from16 v0, v17

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, v23

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;->mPicture:Ljava/lang/String;

    .line 107
    const-string v24, "source"

    move-object/from16 v0, v17

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, v23

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;->mSource:Ljava/lang/String;

    .line 108
    const-string v24, "height"

    move-object/from16 v0, v17

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v24

    move/from16 v0, v24

    move-object/from16 v1, v23

    iput v0, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;->mHeight:I

    .line 109
    const-string v24, "width"

    move-object/from16 v0, v17

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v24

    move/from16 v0, v24

    move-object/from16 v1, v23

    iput v0, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;->mWidth:I

    .line 110
    const-string v24, "link"

    move-object/from16 v0, v17

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, v23

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;->mLink:Ljava/lang/String;

    .line 111
    const-string v24, "created_time"

    move-object/from16 v0, v17

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, v23

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;->mCreatedTime:Ljava/lang/String;

    .line 112
    const-string v24, "updated_time"

    move-object/from16 v0, v17

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, v23

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;->mUpdatedTime:Ljava/lang/String;

    .line 113
    const-string v24, "position"

    move-object/from16 v0, v17

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v24

    move/from16 v0, v24

    move-object/from16 v1, v23

    iput v0, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;->mPosition:I

    .line 116
    const-string v24, "tags"

    move-object/from16 v0, v17

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v24

    if-eqz v24, :cond_1

    .line 117
    const-string v24, "tags"

    move-object/from16 v0, v17

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;->mPhotoID:Ljava/lang/String;

    move-object/from16 v25, v0

    invoke-static/range {v24 .. v25}, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserTags;->parse(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTags;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, v23

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;->mTags:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTags;

    .line 122
    :cond_1
    const-string v24, "images"

    move-object/from16 v0, v17

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v24

    if-eqz v24, :cond_4

    .line 124
    const/4 v8, 0x0

    .line 125
    .local v8, "image":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseImage;
    const/4 v4, 0x0

    .line 127
    .local v4, "curImage":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseImage;
    new-instance v10, Lorg/json/JSONArray;

    const-string v24, "images"

    move-object/from16 v0, v17

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-direct {v10, v0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 129
    .local v10, "jArrImages":Lorg/json/JSONArray;
    if-eqz v10, :cond_3

    .line 130
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    invoke-virtual {v10}, Lorg/json/JSONArray;->length()I

    move-result v24

    move/from16 v0, v24

    if-ge v7, v0, :cond_3

    .line 132
    invoke-virtual {v10, v7}, Lorg/json/JSONArray;->optString(I)Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v24 .. v24}, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserImage;->parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseImage;

    move-result-object v21

    .line 135
    .local v21, "newImage":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseImage;
    if-nez v8, :cond_2

    .line 136
    move-object/from16 v8, v21

    .line 137
    move-object v4, v8

    .line 130
    :goto_1
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 139
    :cond_2
    move-object/from16 v0, v21

    iput-object v0, v4, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseImage;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseImage;

    .line 140
    iget-object v4, v4, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseImage;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseImage;

    goto :goto_1

    .line 145
    .end local v7    # "i":I
    .end local v21    # "newImage":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseImage;
    :cond_3
    move-object/from16 v0, v23

    iput-object v8, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;->mImage:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseImage;

    .line 149
    .end local v4    # "curImage":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseImage;
    .end local v8    # "image":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseImage;
    .end local v10    # "jArrImages":Lorg/json/JSONArray;
    :cond_4
    const-string v24, "format"

    move-object/from16 v0, v17

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v24

    if-eqz v24, :cond_5

    const-string v24, "format"

    move-object/from16 v0, v17

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    const-string v25, "null"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-nez v24, :cond_5

    .line 151
    const-string v24, "format"

    move-object/from16 v0, v17

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v24 .. v24}, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserVideoFormat;->parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseVideoFormat;

    move-result-object v19

    .line 153
    .local v19, "mFormat":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseVideoFormat;
    if-eqz v19, :cond_5

    .line 154
    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseVideoFormat;->mHeight:Ljava/lang/String;

    move-object/from16 v24, v0

    invoke-static/range {v24 .. v24}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v24

    move/from16 v0, v24

    move-object/from16 v1, v23

    iput v0, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;->mHeight:I

    .line 155
    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseVideoFormat;->mWidth:Ljava/lang/String;

    move-object/from16 v24, v0

    invoke-static/range {v24 .. v24}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v24

    move/from16 v0, v24

    move-object/from16 v1, v23

    iput v0, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;->mWidth:I

    .line 160
    .end local v19    # "mFormat":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseVideoFormat;
    :cond_5
    const-string v24, "place"

    move-object/from16 v0, v17

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v24

    if-eqz v24, :cond_6

    .line 161
    const-string v24, "place"

    move-object/from16 v0, v17

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v24 .. v24}, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserPlace;->parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePlace;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, v23

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;->mPlace:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePlace;

    .line 166
    :cond_6
    const-string v24, "likes"

    move-object/from16 v0, v17

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v24

    if-eqz v24, :cond_9

    .line 168
    const/16 v18, 0x0

    .line 169
    .local v18, "like":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLike;
    const/4 v5, 0x0

    .line 171
    .local v5, "curLike":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLike;
    new-instance v14, Lorg/json/JSONObject;

    const-string v24, "likes"

    move-object/from16 v0, v17

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-direct {v14, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 173
    .local v14, "jObjLikes":Lorg/json/JSONObject;
    new-instance v11, Lorg/json/JSONArray;

    const-string v24, "data"

    move-object/from16 v0, v24

    invoke-virtual {v14, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-direct {v11, v0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 175
    .local v11, "jArrLikes":Lorg/json/JSONArray;
    if-eqz v11, :cond_8

    .line 176
    const/4 v7, 0x0

    .restart local v7    # "i":I
    :goto_2
    invoke-virtual {v11}, Lorg/json/JSONArray;->length()I

    move-result v24

    move/from16 v0, v24

    if-ge v7, v0, :cond_8

    .line 178
    invoke-virtual {v11, v7}, Lorg/json/JSONArray;->optString(I)Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v24 .. v24}, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserLike;->parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLike;

    move-result-object v22

    .line 181
    .local v22, "newLike":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLike;
    if-nez v18, :cond_7

    .line 182
    move-object/from16 v18, v22

    .line 183
    move-object/from16 v5, v18

    .line 176
    :goto_3
    add-int/lit8 v7, v7, 0x1

    goto :goto_2

    .line 185
    :cond_7
    move-object/from16 v0, v22

    iput-object v0, v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLike;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLike;

    .line 186
    iget-object v5, v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLike;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLike;

    goto :goto_3

    .line 191
    .end local v7    # "i":I
    .end local v22    # "newLike":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLike;
    :cond_8
    move-object/from16 v0, v18

    move-object/from16 v1, v23

    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;->mLike:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLike;

    .line 193
    const-string v24, "paging"

    move-object/from16 v0, v24

    invoke-virtual {v14, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v24

    if-eqz v24, :cond_9

    .line 195
    new-instance v15, Lorg/json/JSONObject;

    const-string v24, "paging"

    move-object/from16 v0, v24

    invoke-virtual {v14, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-direct {v15, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 198
    .local v15, "jObjLikesPaging":Lorg/json/JSONObject;
    const-string v24, "next"

    move-object/from16 v0, v24

    invoke-virtual {v15, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v24

    if-eqz v24, :cond_9

    .line 199
    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;->mPhotoID:Ljava/lang/String;

    move-object/from16 v24, v0

    invoke-static/range {v24 .. v24}, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserPhoto;->invokeReqGetLikes(Ljava/lang/String;)V

    .line 205
    .end local v5    # "curLike":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLike;
    .end local v11    # "jArrLikes":Lorg/json/JSONArray;
    .end local v14    # "jObjLikes":Lorg/json/JSONObject;
    .end local v15    # "jObjLikesPaging":Lorg/json/JSONObject;
    .end local v18    # "like":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLike;
    :cond_9
    const-string v24, "comments"

    move-object/from16 v0, v17

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v24

    if-eqz v24, :cond_c

    .line 207
    const/4 v2, 0x0

    .line 208
    .local v2, "comment":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseComment;
    const/4 v3, 0x0

    .line 210
    .local v3, "curComment":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseComment;
    new-instance v12, Lorg/json/JSONObject;

    const-string v24, "comments"

    move-object/from16 v0, v17

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-direct {v12, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 212
    .local v12, "jObjComments":Lorg/json/JSONObject;
    new-instance v9, Lorg/json/JSONArray;

    const-string v24, "data"

    move-object/from16 v0, v24

    invoke-virtual {v12, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-direct {v9, v0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 214
    .local v9, "jArrComments":Lorg/json/JSONArray;
    if-eqz v9, :cond_b

    .line 215
    const/4 v7, 0x0

    .restart local v7    # "i":I
    :goto_4
    invoke-virtual {v9}, Lorg/json/JSONArray;->length()I

    move-result v24

    move/from16 v0, v24

    if-ge v7, v0, :cond_b

    .line 217
    invoke-virtual {v9, v7}, Lorg/json/JSONArray;->optString(I)Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v24 .. v24}, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserComment;->parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseComment;

    move-result-object v20

    .line 220
    .local v20, "newComment":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseComment;
    if-nez v2, :cond_a

    .line 221
    move-object/from16 v2, v20

    .line 222
    move-object v3, v2

    .line 215
    :goto_5
    add-int/lit8 v7, v7, 0x1

    goto :goto_4

    .line 224
    :cond_a
    move-object/from16 v0, v20

    iput-object v0, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseComment;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseComment;

    .line 225
    iget-object v3, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseComment;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseComment;

    goto :goto_5

    .line 230
    .end local v7    # "i":I
    .end local v20    # "newComment":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseComment;
    :cond_b
    move-object/from16 v0, v23

    iput-object v2, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;->mComment:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseComment;

    .line 232
    const-string v24, "paging"

    move-object/from16 v0, v24

    invoke-virtual {v12, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v24

    if-eqz v24, :cond_c

    .line 234
    new-instance v13, Lorg/json/JSONObject;

    const-string v24, "paging"

    move-object/from16 v0, v24

    invoke-virtual {v12, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-direct {v13, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 237
    .local v13, "jObjCommentsPaging":Lorg/json/JSONObject;
    const-string v24, "next"

    move-object/from16 v0, v24

    invoke-virtual {v13, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v24

    if-eqz v24, :cond_c

    .line 238
    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhoto;->mPhotoID:Ljava/lang/String;

    move-object/from16 v24, v0

    invoke-static/range {v24 .. v24}, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserPhoto;->invokeReqGetComments(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 249
    .end local v2    # "comment":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseComment;
    .end local v3    # "curComment":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseComment;
    .end local v9    # "jArrComments":Lorg/json/JSONArray;
    .end local v12    # "jObjComments":Lorg/json/JSONObject;
    .end local v13    # "jObjCommentsPaging":Lorg/json/JSONObject;
    .end local v17    # "jsonObject":Lorg/json/JSONObject;
    :cond_c
    :goto_6
    return-object v23

    .line 243
    :catch_0
    move-exception v16

    .line 244
    .local v16, "je":Lorg/json/JSONException;
    const-string v24, "SNS"

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "parser : JSONException error :"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v16 .. v16}, Lorg/json/JSONException;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v24 .. v25}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_6

    .line 245
    .end local v16    # "je":Lorg/json/JSONException;
    :catch_1
    move-exception v6

    .line 246
    .local v6, "e":Ljava/lang/Exception;
    const-string v24, "SNS"

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "parser : Exception error : "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual {v6}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v24 .. v25}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_6
.end method
