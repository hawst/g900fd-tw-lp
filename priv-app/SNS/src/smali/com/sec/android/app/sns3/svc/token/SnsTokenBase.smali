.class public abstract Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;
.super Ljava/lang/Object;
.source "SnsTokenBase.java"


# static fields
.field public static final STATE_EXPIRED:I = 0x1

.field public static final STATE_EXPIRED_RECOVERABLE:I = 0x3

.field public static final STATE_INVALID:I = 0x2

.field public static final STATE_VALID:I


# instance fields
.field protected mState:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getTokenState()I
    .locals 1

    .prologue
    .line 34
    iget v0, p0, Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;->mState:I

    return v0
.end method

.method public abstract isValidAccessTokenNExpires()Z
.end method

.method public abstract readTokenInfo()V
.end method

.method public abstract removeAll()V
.end method

.method public setTokenState(I)V
    .locals 0
    .param p1, "state"    # I

    .prologue
    .line 38
    iput p1, p0, Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;->mState:I

    .line 39
    return-void
.end method

.method public abstract writeTokenInfo()V
.end method
