.class public final enum Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwFavoritesAPI;
.super Ljava/lang/Enum;
.source "SnsTwFavoritesAPI.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwFavoritesAPI$Param;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwFavoritesAPI;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwFavoritesAPI;

.field public static final enum FAVORITES:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwFavoritesAPI;

.field public static final enum FAVORITES_CREATE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwFavoritesAPI;

.field public static final enum FAVORITES_DESTROY:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwFavoritesAPI;


# instance fields
.field private mHttpMethod:Ljava/lang/String;

.field private mUrl:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 28
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwFavoritesAPI;

    const-string v1, "FAVORITES"

    const-string v2, "/favorites.json"

    const-string v3, "GET"

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwFavoritesAPI;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwFavoritesAPI;->FAVORITES:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwFavoritesAPI;

    .line 29
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwFavoritesAPI;

    const-string v1, "FAVORITES_CREATE"

    const-string v2, "/favorites/create/"

    const-string v3, "POST"

    invoke-direct {v0, v1, v5, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwFavoritesAPI;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwFavoritesAPI;->FAVORITES_CREATE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwFavoritesAPI;

    .line 30
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwFavoritesAPI;

    const-string v1, "FAVORITES_DESTROY"

    const-string v2, "/favorites/destroy/"

    const-string v3, "POST"

    invoke-direct {v0, v1, v6, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwFavoritesAPI;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwFavoritesAPI;->FAVORITES_DESTROY:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwFavoritesAPI;

    .line 26
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwFavoritesAPI;

    sget-object v1, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwFavoritesAPI;->FAVORITES:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwFavoritesAPI;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwFavoritesAPI;->FAVORITES_CREATE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwFavoritesAPI;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwFavoritesAPI;->FAVORITES_DESTROY:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwFavoritesAPI;

    aput-object v1, v0, v6

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwFavoritesAPI;->$VALUES:[Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwFavoritesAPI;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p3, "url"    # Ljava/lang/String;
    .param p4, "httpMethod"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 51
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 52
    iput-object p3, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwFavoritesAPI;->mUrl:Ljava/lang/String;

    .line 53
    iput-object p4, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwFavoritesAPI;->mHttpMethod:Ljava/lang/String;

    .line 54
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwFavoritesAPI;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 26
    const-class v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwFavoritesAPI;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwFavoritesAPI;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwFavoritesAPI;
    .locals 1

    .prologue
    .line 26
    sget-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwFavoritesAPI;->$VALUES:[Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwFavoritesAPI;

    invoke-virtual {v0}, [Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwFavoritesAPI;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwFavoritesAPI;

    return-object v0
.end method


# virtual methods
.method public getHttpMethod()Ljava/lang/String;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwFavoritesAPI;->mHttpMethod:Ljava/lang/String;

    return-object v0
.end method

.method public getUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwFavoritesAPI;->mUrl:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwFavoritesAPI;->mUrl:Ljava/lang/String;

    return-object v0
.end method
