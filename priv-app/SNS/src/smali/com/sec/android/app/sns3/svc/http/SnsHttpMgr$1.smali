.class Lcom/sec/android/app/sns3/svc/http/SnsHttpMgr$1;
.super Ljava/util/TimerTask;
.source "SnsHttpMgr.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/sns3/svc/http/SnsHttpMgr;->getHttpClient(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/http/SnsHttpClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/sns3/svc/http/SnsHttpMgr;

.field final synthetic val$hostUri:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/sec/android/app/sns3/svc/http/SnsHttpMgr;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 45
    iput-object p1, p0, Lcom/sec/android/app/sns3/svc/http/SnsHttpMgr$1;->this$0:Lcom/sec/android/app/sns3/svc/http/SnsHttpMgr;

    iput-object p2, p0, Lcom/sec/android/app/sns3/svc/http/SnsHttpMgr$1;->val$hostUri:Ljava/lang/String;

    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 48
    invoke-static {}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->isLoggable()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 49
    const-string v1, "SNS"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SnsHttpMgr : TimerTask : run() - remove client hostUri = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/sns3/svc/http/SnsHttpMgr$1;->val$hostUri:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 51
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/http/SnsHttpMgr$1;->this$0:Lcom/sec/android/app/sns3/svc/http/SnsHttpMgr;

    # getter for: Lcom/sec/android/app/sns3/svc/http/SnsHttpMgr;->mHttpClientMap:Ljava/util/Map;
    invoke-static {v1}, Lcom/sec/android/app/sns3/svc/http/SnsHttpMgr;->access$000(Lcom/sec/android/app/sns3/svc/http/SnsHttpMgr;)Ljava/util/Map;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/sns3/svc/http/SnsHttpMgr$1;->val$hostUri:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/sns3/svc/http/SnsHttpMgr$SnsHttpClientUnit;

    .line 52
    .local v0, "unit":Lcom/sec/android/app/sns3/svc/http/SnsHttpMgr$SnsHttpClientUnit;
    if-eqz v0, :cond_1

    .line 53
    iget-object v1, v0, Lcom/sec/android/app/sns3/svc/http/SnsHttpMgr$SnsHttpClientUnit;->mKeepAliveTimer:Ljava/util/Timer;

    invoke-virtual {v1}, Ljava/util/Timer;->cancel()V

    .line 54
    iget-object v1, v0, Lcom/sec/android/app/sns3/svc/http/SnsHttpMgr$SnsHttpClientUnit;->mHttpClient:Lcom/sec/android/app/sns3/svc/http/SnsHttpClient;

    invoke-virtual {v1}, Lcom/sec/android/app/sns3/svc/http/SnsHttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v1

    const-wide/16 v2, 0x0

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v1, v2, v3, v4}, Lorg/apache/http/conn/ClientConnectionManager;->closeIdleConnections(JLjava/util/concurrent/TimeUnit;)V

    .line 56
    const-string v1, "SNS"

    const-string v2, "SnsHttpMgr : TimerTask : run() - connection close"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 58
    :cond_1
    return-void
.end method
