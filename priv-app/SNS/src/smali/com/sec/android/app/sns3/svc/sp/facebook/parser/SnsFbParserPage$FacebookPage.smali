.class public interface abstract Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserPage$FacebookPage;
.super Ljava/lang/Object;
.source "SnsFbParserPage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserPage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "FacebookPage"
.end annotation


# static fields
.field public static final ACCESS_TOKEN:Ljava/lang/String; = "access_token"

.field public static final CATEGORY:Ljava/lang/String; = "category"

.field public static final CHECKINS:Ljava/lang/String; = "checkins"

.field public static final ID:Ljava/lang/String; = "id"

.field public static final LIKES:Ljava/lang/String; = "likes"

.field public static final LINK:Ljava/lang/String; = "link"

.field public static final LOCATION:Ljava/lang/String; = "location"

.field public static final NAME:Ljava/lang/String; = "name"

.field public static final PHONE:Ljava/lang/String; = "phone"
