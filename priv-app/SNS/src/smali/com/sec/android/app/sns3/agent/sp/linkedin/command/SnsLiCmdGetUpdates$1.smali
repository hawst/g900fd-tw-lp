.class Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetUpdates$1;
.super Lcom/sec/android/app/sns3/svc/sp/linkedin/request/SnsLiReqGetUpdates;
.source "SnsLiCmdGetUpdates.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetUpdates;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Handler;Ljava/lang/String;Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetUpdates;


# direct methods
.method constructor <init>(Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetUpdates;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;Landroid/os/Bundle;[Ljava/lang/String;)V
    .locals 0
    .param p2, "x0"    # Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    .param p3, "x1"    # Ljava/lang/String;
    .param p4, "x2"    # Landroid/os/Bundle;
    .param p5, "x3"    # [Ljava/lang/String;

    .prologue
    .line 57
    iput-object p1, p0, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetUpdates$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetUpdates;

    invoke-direct {p0, p2, p3, p4, p5}, Lcom/sec/android/app/sns3/svc/sp/linkedin/request/SnsLiReqGetUpdates;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;Landroid/os/Bundle;[Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public onReqRespond(IZIILandroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdates;)Z
    .locals 12
    .param p1, "reqID"    # I
    .param p2, "bSuccess"    # Z
    .param p3, "httpStatus"    # I
    .param p4, "errorCode"    # I
    .param p5, "reason"    # Landroid/os/Bundle;
    .param p6, "statusStream"    # Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdates;

    .prologue
    .line 62
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/sns3/SnsApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 64
    .local v2, "cr":Landroid/content/ContentResolver;
    if-eqz p2, :cond_8

    .line 65
    if-eqz p6, :cond_0

    move-object/from16 v0, p6

    iget-object v7, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdates;->mUpdates:Ljava/util/Map;

    if-nez v7, :cond_1

    .line 66
    :cond_0
    const-string v7, "SnsLiCmdGetUpdates"

    const-string v8, "no records fetched"

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 67
    const/4 v7, 0x0

    .line 140
    :goto_0
    return v7

    .line 69
    :cond_1
    sget-object v7, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$StatusStream;->CONTENT_URI:Landroid/net/Uri;

    const-string v8, "from_id=?"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    iget-object v11, p0, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetUpdates$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetUpdates;

    # getter for: Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetUpdates;->mUserID:Ljava/lang/String;
    invoke-static {v11}, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetUpdates;->access$000(Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetUpdates;)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-virtual {v2, v7, v8, v9}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 72
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/sns3/SnsApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    .line 73
    .local v5, "mResources":Landroid/content/res/Resources;
    const/4 v3, 0x0

    .line 74
    .local v3, "feed":Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;
    move-object/from16 v0, p6

    iget-object v7, v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdates;->mUpdates:Ljava/util/Map;

    invoke-interface {v7}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_7

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;

    .line 76
    .local v6, "update":Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;
    new-instance v3, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;

    .end local v3    # "feed":Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;
    invoke-direct {v3}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;-><init>()V

    .line 77
    .restart local v3    # "feed":Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;
    iget-object v7, v6, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mUpdateKey:Ljava/lang/String;

    invoke-virtual {v3, v7}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->setUpdateKey(Ljava/lang/String;)V

    .line 78
    iget-object v7, v6, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mUpdateType:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$UpdateType;

    invoke-virtual {v3, v7}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->setUpdateType(Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$UpdateType;)V

    .line 79
    iget-wide v8, v6, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mTimeStamp:J

    invoke-virtual {v3, v8, v9}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->setTimestamp(J)V

    .line 80
    iget-object v7, v6, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mConnection:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;

    if-eqz v7, :cond_2

    .line 81
    iget-object v7, v6, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mConnection:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;

    iget-object v7, v7, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;->mId:Ljava/lang/String;

    invoke-virtual {v3, v7}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->setFromId(Ljava/lang/String;)V

    .line 82
    iget-object v7, v6, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mConnection:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;

    invoke-virtual {v7}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;->getFormattedName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->setFromName(Ljava/lang/String;)V

    .line 83
    iget-object v7, v6, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mConnection:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;

    iget-object v7, v7, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;->mPictureUrl:Ljava/lang/String;

    invoke-virtual {v3, v7}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->setFromIconUrl(Ljava/lang/String;)V

    .line 85
    :cond_2
    sget-object v7, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetUpdates$2;->$SwitchMap$com$sec$android$app$sns3$svc$sp$linkedin$response$SnsLiResponseUpdate$UpdateType:[I

    iget-object v8, v6, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mUpdateType:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$UpdateType;

    invoke-virtual {v8}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$UpdateType;->ordinal()I

    move-result v8

    aget v7, v7, v8

    packed-switch v7, :pswitch_data_0

    .line 116
    const-string v7, "SnsLiCmdGetUpdates"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "[SnsLiCmdGetUpdates] insertFeed Invalid update type: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, v6, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mUpdateType:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$UpdateType;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 122
    :cond_3
    :goto_2
    iget-boolean v7, v6, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->isCommentable:Z

    if-eqz v7, :cond_4

    iget-object v7, v6, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mComments:Ljava/util/List;

    if-eqz v7, :cond_4

    iget-object v7, v6, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mComments:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    if-lez v7, :cond_4

    .line 124
    iget v7, v6, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mNumComments:I

    invoke-virtual {v3, v7}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->setCommentsCount(I)V

    .line 126
    :cond_4
    iget-boolean v7, v6, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->isLikable:Z

    if-eqz v7, :cond_5

    iget v7, v6, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mNumLikes:I

    if-lez v7, :cond_5

    .line 127
    iget v7, v6, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mNumLikes:I

    invoke-virtual {v3, v7}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->setLikesCount(I)V

    .line 129
    :cond_5
    # invokes: Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetUpdates;->insertFeed(Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;)V
    invoke-static {v3}, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetUpdates;->access$100(Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;)V

    goto/16 :goto_1

    .line 88
    :pswitch_0
    iget-object v7, v6, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mConnection:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;

    if-eqz v7, :cond_6

    iget-object v7, v6, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mConnection:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;

    iget-object v7, v7, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;->mId:Ljava/lang/String;

    iget-object v8, v6, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mCurrentShare:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Share;

    iget-object v8, v8, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Share;->mAuthor:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;

    iget-object v8, v8, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;->mId:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 90
    const v7, 0x7f08006a

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-virtual {v3}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->getFromName()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-virtual {v5, v7, v8}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->setTitle(Ljava/lang/String;)V

    .line 97
    :goto_3
    iget-object v7, v6, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mCurrentShare:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Share;

    iget-object v7, v7, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Share;->mTitle:Ljava/lang/String;

    invoke-virtual {v3, v7}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->setMessage(Ljava/lang/String;)V

    .line 98
    iget-object v7, v6, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mCurrentShare:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Share;

    iget-object v7, v7, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Share;->mSubmittedUrl:Ljava/lang/String;

    invoke-virtual {v3, v7}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->setSubmittedUrl(Ljava/lang/String;)V

    .line 99
    iget-object v7, v6, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mCurrentShare:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Share;

    iget-object v7, v7, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Share;->mThumbnailUrl:Ljava/lang/String;

    invoke-virtual {v3, v7}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->setThumbnailUrl(Ljava/lang/String;)V

    .line 100
    iget-object v7, v6, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mCurrentShare:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Share;

    iget-object v7, v7, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Share;->mComment:Ljava/lang/String;

    invoke-virtual {v3, v7}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->setComment(Ljava/lang/String;)V

    goto :goto_2

    .line 93
    :cond_6
    const v7, 0x7f08006b

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-virtual {v3}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->getFromName()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x1

    iget-object v10, v6, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mCurrentShare:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Share;

    iget-object v10, v10, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Share;->mAuthor:Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;

    invoke-virtual {v10}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate$Person;->getFormattedName()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-virtual {v5, v7, v8}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->setTitle(Ljava/lang/String;)V

    goto :goto_3

    .line 104
    :pswitch_1
    const v7, 0x7f08006c

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->setTitle(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 107
    :pswitch_2
    const v7, 0x7f080069

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-virtual {v3}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->getFromName()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-virtual {v5, v7, v8}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->setTitle(Ljava/lang/String;)V

    .line 109
    iget-object v7, v6, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mUpdatedFields:Ljava/lang/String;

    if-eqz v7, :cond_3

    .line 110
    iget-object v7, v6, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;->mUpdatedFields:Ljava/lang/String;

    invoke-virtual {v3, v7}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;->setMessage(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 131
    .end local v6    # "update":Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUpdate;
    :cond_7
    iget-object v7, p0, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetUpdates$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetUpdates;

    sget-object v8, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$StatusStream;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v8}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetUpdates;->setUri(Ljava/lang/String;)V

    .line 138
    .end local v3    # "feed":Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiFeedItem;
    .end local v4    # "i$":Ljava/util/Iterator;
    .end local v5    # "mResources":Landroid/content/res/Resources;
    :goto_4
    iget-object v7, p0, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetUpdates$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetUpdates;

    invoke-virtual {v7, p2}, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetUpdates;->setSuccess(Z)V

    .line 139
    iget-object v7, p0, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetUpdates$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetUpdates;

    invoke-virtual {v7, p0}, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetUpdates;->receive(Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;)V

    .line 140
    const/4 v7, 0x1

    goto/16 :goto_0

    .line 133
    :cond_8
    iget-object v7, p0, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetUpdates$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetUpdates;

    new-instance v8, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;

    const-string v9, "linkedin"

    move/from16 v0, p4

    move-object/from16 v1, p5

    invoke-direct {v8, v9, p3, v0, v1}, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;-><init>(Ljava/lang/String;IILandroid/os/Bundle;)V

    invoke-virtual {v7, v8}, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetUpdates;->setResponseList(Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;)Z

    .line 135
    iget-object v7, p0, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetUpdates$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetUpdates;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetUpdates;->setUri(Ljava/lang/String;)V

    goto :goto_4

    .line 85
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
