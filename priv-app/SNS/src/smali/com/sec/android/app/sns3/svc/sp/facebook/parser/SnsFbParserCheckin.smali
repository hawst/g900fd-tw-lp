.class public Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserCheckin;
.super Ljava/lang/Object;
.source "SnsFbParserCheckin.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserCheckin$FacebookPlace;,
        Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserCheckin$FacebookCheckin;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    return-void
.end method

.method public static parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseCheckin;
    .locals 9
    .param p0, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 86
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseCheckin;

    invoke-direct {v0}, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseCheckin;-><init>()V

    .line 89
    .local v0, "checkin":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseCheckin;
    :try_start_0
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 91
    .local v3, "jsonObject":Lorg/json/JSONObject;
    const-string v6, "id"

    invoke-virtual {v3, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseCheckin;->mCheckinID:Ljava/lang/String;

    .line 93
    const-string v6, "from"

    invoke-virtual {v3, v6}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 94
    const-string v6, "from"

    invoke-virtual {v3, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserFrom;->parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFrom;

    move-result-object v6

    iput-object v6, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseCheckin;->mFrom:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFrom;

    .line 99
    :cond_0
    const-string v6, "tags"

    invoke-virtual {v3, v6}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 100
    const-string v6, "tags"

    invoke-virtual {v3, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v6

    iget-object v7, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseCheckin;->mCheckinID:Ljava/lang/String;

    invoke-static {v6, v7}, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserTags;->parse(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTags;

    move-result-object v6

    iput-object v6, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseCheckin;->mTag:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseTags;

    .line 105
    :cond_1
    const-string v6, "place"

    invoke-virtual {v3, v6}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 107
    new-instance v4, Lorg/json/JSONObject;

    const-string v6, "place"

    invoke-virtual {v3, v6}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v6

    invoke-virtual {v6}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v6}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 110
    .local v4, "jsonObjectPlace":Lorg/json/JSONObject;
    new-instance v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePlace;

    invoke-direct {v5}, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePlace;-><init>()V

    .line 112
    .local v5, "place":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePlace;
    const-string v6, "id"

    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePlace;->mPlaceID:Ljava/lang/String;

    .line 113
    const-string v6, "name"

    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePlace;->mPlaceName:Ljava/lang/String;

    .line 114
    const-string v6, "street"

    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePlace;->mStreet:Ljava/lang/String;

    .line 115
    const-string v6, "city"

    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePlace;->mCity:Ljava/lang/String;

    .line 116
    const-string v6, "state"

    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePlace;->mState:Ljava/lang/String;

    .line 117
    const-string v6, "country"

    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePlace;->mCountry:Ljava/lang/String;

    .line 118
    const-string v6, "zip"

    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePlace;->mZip:Ljava/lang/String;

    .line 120
    const-string v6, "location"

    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 121
    const-string v6, "location"

    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v6

    const-string v7, "latitude"

    invoke-virtual {v6, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePlace;->mLatitude:Ljava/lang/String;

    .line 123
    const-string v6, "location"

    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v6

    const-string v7, "longitude"

    invoke-virtual {v6, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePlace;->mLongitude:Ljava/lang/String;

    .line 127
    :cond_2
    iput-object v5, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseCheckin;->mPlace:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePlace;

    .line 130
    .end local v4    # "jsonObjectPlace":Lorg/json/JSONObject;
    .end local v5    # "place":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePlace;
    :cond_3
    const-string v6, "application"

    invoke-virtual {v3, v6}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v6

    invoke-virtual {v6}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserApplication;->parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseApplication;

    move-result-object v6

    iput-object v6, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseCheckin;->mApplication:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseApplication;

    .line 132
    const-string v6, "created_time"

    invoke-virtual {v3, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseCheckin;->mCreatedTime:Ljava/lang/String;

    .line 135
    const-string v6, "likes"

    invoke-virtual {v3, v6}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 136
    const-string v6, "likes"

    invoke-virtual {v3, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserLike;->parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLike;

    move-result-object v6

    iput-object v6, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseCheckin;->mLike:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseLike;

    .line 140
    :cond_4
    const-string v6, "message"

    invoke-virtual {v3, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseCheckin;->mMessage:Ljava/lang/String;

    .line 143
    const-string v6, "comments"

    invoke-virtual {v3, v6}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 144
    const-string v6, "comments"

    invoke-virtual {v3, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserComment;->parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseComment;

    move-result-object v6

    iput-object v6, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseCheckin;->mComment:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseComment;

    .line 148
    :cond_5
    const-string v6, "type"

    invoke-virtual {v3, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseCheckin;->mType:Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 156
    .end local v3    # "jsonObject":Lorg/json/JSONObject;
    :goto_0
    return-object v0

    .line 150
    :catch_0
    move-exception v2

    .line 151
    .local v2, "je":Lorg/json/JSONException;
    const-string v6, "SNS"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "parser : JSONException error :"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v2}, Lorg/json/JSONException;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 152
    .end local v2    # "je":Lorg/json/JSONException;
    :catch_1
    move-exception v1

    .line 153
    .local v1, "e":Ljava/lang/Exception;
    const-string v6, "SNS"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "parser : Exception error : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
