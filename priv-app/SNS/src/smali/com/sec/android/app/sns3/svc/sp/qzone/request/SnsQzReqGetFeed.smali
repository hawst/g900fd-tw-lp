.class public abstract Lcom/sec/android/app/sns3/svc/sp/qzone/request/SnsQzReqGetFeed;
.super Lcom/sec/android/app/sns3/svc/sp/qzone/request/SnsQzReqBase;
.source "SnsQzReqGetFeed.java"

# interfaces
.implements Lcom/sec/android/app/sns3/svc/sp/qzone/callback/ISnsQzReqCbFeed;


# static fields
.field private static final QZONE_FEED_DEFAULT_COUNT:I = 0xa


# instance fields
.field private final mAccessToken:Ljava/lang/String;

.field private mFeedCount:I

.field private final mUserID:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;)V
    .locals 3
    .param p1, "svcMgr"    # Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    .param p2, "userID"    # Ljava/lang/String;

    .prologue
    .line 60
    const/16 v1, 0x16

    invoke-direct {p0, p1, v1}, Lcom/sec/android/app/sns3/svc/sp/qzone/request/SnsQzReqBase;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;I)V

    .line 43
    const/4 v1, -0x1

    iput v1, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/request/SnsQzReqGetFeed;->mFeedCount:I

    .line 61
    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/request/SnsQzReqGetFeed;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    invoke-virtual {v1}, Lcom/sec/android/app/sns3/svc/SnsSvcMgr;->getTokenMgr()Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;

    move-result-object v1

    const-string v2, "qzone"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;->getToken(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzToken;

    .line 63
    .local v0, "token":Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzToken;
    if-nez p2, :cond_0

    .line 64
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzToken;->getUserID()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->encodeUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/request/SnsQzReqGetFeed;->mUserID:Ljava/lang/String;

    .line 68
    :goto_0
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzToken;->getAccessToken()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->encodeUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/request/SnsQzReqGetFeed;->mAccessToken:Ljava/lang/String;

    .line 69
    return-void

    .line 66
    :cond_0
    iput-object p2, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/request/SnsQzReqGetFeed;->mUserID:Ljava/lang/String;

    goto :goto_0
.end method

.method public constructor <init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;I)V
    .locals 3
    .param p1, "svcMgr"    # Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    .param p2, "userID"    # Ljava/lang/String;
    .param p3, "feedCount"    # I

    .prologue
    .line 46
    const/16 v1, 0x16

    invoke-direct {p0, p1, v1}, Lcom/sec/android/app/sns3/svc/sp/qzone/request/SnsQzReqBase;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;I)V

    .line 43
    const/4 v1, -0x1

    iput v1, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/request/SnsQzReqGetFeed;->mFeedCount:I

    .line 47
    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/request/SnsQzReqGetFeed;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    invoke-virtual {v1}, Lcom/sec/android/app/sns3/svc/SnsSvcMgr;->getTokenMgr()Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;

    move-result-object v1

    const-string v2, "qzone"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;->getToken(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzToken;

    .line 49
    .local v0, "token":Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzToken;
    if-nez p2, :cond_0

    .line 50
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzToken;->getUserID()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->encodeUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/request/SnsQzReqGetFeed;->mUserID:Ljava/lang/String;

    .line 54
    :goto_0
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzToken;->getAccessToken()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->encodeUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/request/SnsQzReqGetFeed;->mAccessToken:Ljava/lang/String;

    .line 56
    iput p3, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/request/SnsQzReqGetFeed;->mFeedCount:I

    .line 57
    return-void

    .line 52
    :cond_0
    iput-object p2, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/request/SnsQzReqGetFeed;->mUserID:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method protected compose()Lcom/sec/android/app/sns3/svc/http/SnsHttpRequest;
    .locals 6

    .prologue
    .line 74
    const-string v2, "GET"

    .line 75
    .local v2, "method":Ljava/lang/String;
    const/4 v3, 0x0

    .line 76
    .local v3, "uri":Ljava/lang/String;
    const/4 v4, 0x0

    .line 77
    .local v4, "header":Landroid/os/Bundle;
    const/4 v5, 0x0

    .line 79
    .local v5, "body":Landroid/os/Bundle;
    iget v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/request/SnsQzReqGetFeed;->mFeedCount:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 80
    const/16 v0, 0xa

    iput v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/request/SnsQzReqGetFeed;->mFeedCount:I

    .line 83
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "https://openmobile.qq.com/qzone/mobile/get_friends_feeds?oauth_consumer_key=100350160&access_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/request/SnsQzReqGetFeed;->mAccessToken:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&openid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/request/SnsQzReqGetFeed;->mUserID:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&count="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/request/SnsQzReqGetFeed;->mFeedCount:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 86
    new-instance v0, Lcom/sec/android/app/sns3/svc/http/SnsHttpRequest;

    iget v1, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/request/SnsQzReqGetFeed;->mReqID:I

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/sns3/svc/http/SnsHttpRequest;-><init>(ILjava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;)V

    return-object v0
.end method

.method protected parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;
    .locals 3
    .param p1, "content"    # Ljava/lang/String;

    .prologue
    .line 91
    const-string v1, "SNS"

    const-string v2, "Qzone  SnsQzReqGetFeed response SUCCESS!!"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 92
    invoke-super {p0, p1}, Lcom/sec/android/app/sns3/svc/sp/qzone/request/SnsQzReqBase;->parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;

    move-result-object v0

    .line 94
    .local v0, "response":Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;
    if-eqz v0, :cond_0

    .line 97
    .end local v0    # "response":Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;
    :goto_0
    return-object v0

    .restart local v0    # "response":Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;
    :cond_0
    invoke-static {p1}, Lcom/sec/android/app/sns3/svc/sp/qzone/parser/SnsQzParserFeed;->parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseFeed;

    move-result-object v0

    goto :goto_0
.end method

.method protected respond(Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;)Z
    .locals 7
    .param p1, "result"    # Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;

    .prologue
    .line 103
    const-string v0, "SNS"

    const-string v1, "<SnsQzReqGetFeed> respond()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 105
    iget v1, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/request/SnsQzReqGetFeed;->mReqID:I

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->isSuccess()Z

    move-result v2

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->getHttpstatus()I

    move-result v3

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->getErrorCode()I

    move-result v4

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->getReason()Landroid/os/Bundle;

    move-result-object v5

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->getResponse()Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseFeed;

    move-object v0, p0

    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/app/sns3/svc/sp/qzone/request/SnsQzReqGetFeed;->onReqRespond(IZIILandroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseFeed;)Z

    .line 108
    const/4 v0, 0x1

    return v0
.end method
