.class public Lcom/sec/android/app/sns3/app/profile/sp/twitter/SnsTwGetStatusStream;
.super Ljava/lang/Object;
.source "SnsTwGetStatusStream.java"


# static fields
.field private static final QUERY_LIMIT:Ljava/lang/String; = "10"

.field protected static final TAG:Ljava/lang/String; = "SnsTwGetStatusStream"

.field private static mCmdHandler:Landroid/os/Handler;

.field private static mServiceMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000(Landroid/content/Context;ILjava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Landroid/content/Context;
    .param p1, "x1"    # I
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 42
    invoke-static {p0, p1, p2}, Lcom/sec/android/app/sns3/app/profile/sp/twitter/SnsTwGetStatusStream;->invokeBroadcast(Landroid/content/Context;ILjava/lang/String;)V

    return-void
.end method

.method private static invokeBroadcast(Landroid/content/Context;ILjava/lang/String;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "resultCode"    # I
    .param p2, "id"    # Ljava/lang/String;

    .prologue
    .line 96
    const-string v1, "SnsTwGetStatusStream"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "invokeBroadcast() : resultCode = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 97
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.app.sns.profile.ACTION_TWITTER_UPDATED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 98
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 99
    const-string v1, "result"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 100
    const-string v1, "com.sec.android.app.sns3.permission.RECEIVE_SNS_BROADCAST"

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    .line 101
    return-void
.end method

.method public static updateStatus(Landroid/content/Context;Ljava/lang/String;)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "userId"    # Ljava/lang/String;

    .prologue
    .line 53
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/sns3/SnsApplication;->getSvcMgr()Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v3

    sput-object v3, Lcom/sec/android/app/sns3/app/profile/sp/twitter/SnsTwGetStatusStream;->mServiceMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    .line 54
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/sns3/SnsApplication;->getAgentMgr()Lcom/sec/android/app/sns3/agent/SnsAgentMgr;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/sns3/agent/SnsAgentMgr;->getCommandMgr()Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;->getCommandMgrHandle()Landroid/os/Handler;

    move-result-object v3

    sput-object v3, Lcom/sec/android/app/sns3/app/profile/sp/twitter/SnsTwGetStatusStream;->mCmdHandler:Landroid/os/Handler;

    .line 56
    sget-object v3, Lcom/sec/android/app/sns3/app/profile/sp/twitter/SnsTwGetStatusStream;->mServiceMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    if-nez v3, :cond_0

    .line 57
    const-string v3, "SnsTwGetStatusStream"

    const-string v4, "[SNS] mSvcMgr is null !!"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 93
    :goto_0
    return-void

    .line 60
    :cond_0
    sget-object v3, Lcom/sec/android/app/sns3/app/profile/sp/twitter/SnsTwGetStatusStream;->mCmdHandler:Landroid/os/Handler;

    if-nez v3, :cond_1

    .line 61
    const-string v3, "SnsTwGetStatusStream"

    const-string v4, "[SNS] mCmdHandler is null !!"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 65
    :cond_1
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v3

    const-string v4, "com.sec.android.app.sns3.twitter"

    invoke-virtual {v3, v4}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 66
    .local v0, "account":[Landroid/accounts/Account;
    array-length v3, v0

    if-lez v3, :cond_2

    .line 67
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 68
    .local v2, "params":Landroid/os/Bundle;
    const-string v3, "count"

    const-string v4, "10"

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    new-instance v1, Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdGetFeedTimeline;

    sget-object v3, Lcom/sec/android/app/sns3/app/profile/sp/twitter/SnsTwGetStatusStream;->mServiceMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    sget-object v4, Lcom/sec/android/app/sns3/app/profile/sp/twitter/SnsTwGetStatusStream;->mCmdHandler:Landroid/os/Handler;

    const-string v5, "10"

    invoke-direct {v1, v3, v4, p1, v5}, Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdGetFeedTimeline;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Handler;Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    .local v1, "cmd":Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;
    new-instance v3, Lcom/sec/android/app/sns3/app/profile/sp/twitter/SnsTwGetStatusStream$1;

    invoke-direct {v3, p0, p1}, Lcom/sec/android/app/sns3/app/profile/sp/twitter/SnsTwGetStatusStream$1;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->setCommandCallback(Lcom/sec/android/app/sns3/agent/command/ISnsCommandCallback;)Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;

    .line 88
    invoke-virtual {v1}, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->send()I

    goto :goto_0

    .line 92
    .end local v1    # "cmd":Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;
    .end local v2    # "params":Landroid/os/Bundle;
    :cond_2
    const/4 v3, -0x1

    invoke-static {p0, v3, p1}, Lcom/sec/android/app/sns3/app/profile/sp/twitter/SnsTwGetStatusStream;->invokeBroadcast(Landroid/content/Context;ILjava/lang/String;)V

    goto :goto_0
.end method
