.class public Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserBirthday;
.super Ljava/lang/Object;
.source "SnsFbParserBirthday.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserBirthday$FacebookBirthday;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    return-void
.end method

.method public static parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseBirthday;
    .locals 8
    .param p0, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 51
    const/4 v0, 0x0

    .line 52
    .local v0, "birthday":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseBirthday;
    const/4 v2, 0x0

    .line 56
    .local v2, "curBirthday":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseBirthday;
    :try_start_0
    new-instance v5, Lorg/json/JSONArray;

    invoke-direct {v5, p0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 58
    .local v5, "ja":Lorg/json/JSONArray;
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    invoke-virtual {v5}, Lorg/json/JSONArray;->length()I

    move-result v7

    if-ge v4, v7, :cond_1

    .line 59
    new-instance v6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseBirthday;

    invoke-direct {v6}, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseBirthday;-><init>()V

    .line 61
    .local v6, "newBirthday":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseBirthday;
    invoke-virtual {v5, v4}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v1

    .line 63
    .local v1, "birthdayJsonObj":Lorg/json/JSONObject;
    const-string v7, "uid"

    invoke-virtual {v1, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseBirthday;->mUId:Ljava/lang/String;

    .line 64
    const-string v7, "first_name"

    invoke-virtual {v1, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseBirthday;->mFirstName:Ljava/lang/String;

    .line 65
    const-string v7, "last_name"

    invoke-virtual {v1, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseBirthday;->mLastName:Ljava/lang/String;

    .line 66
    const-string v7, "name"

    invoke-virtual {v1, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseBirthday;->mName:Ljava/lang/String;

    .line 67
    const-string v7, "birthday_date"

    invoke-virtual {v1, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseBirthday;->mBirthdayDate:Ljava/lang/String;

    .line 69
    const-string v7, "pic_big"

    invoke-virtual {v1, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseBirthday;->mPicUrl:Ljava/lang/String;

    .line 70
    const-string v7, "sex"

    invoke-virtual {v1, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseBirthday;->mGender:Ljava/lang/String;

    .line 72
    if-nez v0, :cond_0

    .line 73
    move-object v0, v6

    .line 74
    move-object v2, v0

    .line 58
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 76
    :cond_0
    iput-object v6, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseBirthday;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseBirthday;

    .line 77
    iget-object v2, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseBirthday;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseBirthday;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 80
    .end local v1    # "birthdayJsonObj":Lorg/json/JSONObject;
    .end local v4    # "i":I
    .end local v5    # "ja":Lorg/json/JSONArray;
    .end local v6    # "newBirthday":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseBirthday;
    :catch_0
    move-exception v3

    .line 82
    .local v3, "e":Lorg/json/JSONException;
    invoke-virtual {v3}, Lorg/json/JSONException;->printStackTrace()V

    .line 85
    .end local v3    # "e":Lorg/json/JSONException;
    :cond_1
    return-object v0
.end method
