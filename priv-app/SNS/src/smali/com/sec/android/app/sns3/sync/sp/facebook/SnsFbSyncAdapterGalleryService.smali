.class public Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;
.super Landroid/app/Service;
.source "SnsFbSyncAdapterGalleryService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService$SyncAdapterImpl;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "SnsFbSync"

.field private static mReq:Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqBase;


# instance fields
.field private bAlbumSyncSuccess:Z

.field private mAccount:Landroid/accounts/Account;

.field private mAlbumId:Ljava/lang/String;

.field private mAuthority:Ljava/lang/String;

.field private mBundle:Landroid/os/Bundle;

.field private mContext:Landroid/content/Context;

.field private mFbToken:Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbToken;

.field private mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

.field private mSyncAdapter:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService$SyncAdapterImpl;

.field private mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;

.field private mSyncResult:Landroid/content/SyncResult;

.field private mSyncState:I

.field private mSyncType:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 69
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->mReq:Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqBase;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 61
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 65
    iput-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->mSyncAdapter:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService$SyncAdapterImpl;

    .line 67
    iput-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;

    .line 71
    iput v2, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->mSyncState:I

    .line 73
    iput-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->mSyncType:Ljava/lang/String;

    .line 75
    iput-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->mSyncResult:Landroid/content/SyncResult;

    .line 77
    iput-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->mAccount:Landroid/accounts/Account;

    .line 79
    iput-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->mAuthority:Ljava/lang/String;

    .line 81
    iput-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->mAlbumId:Ljava/lang/String;

    .line 85
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->mBundle:Landroid/os/Bundle;

    .line 89
    iput-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->mFbToken:Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbToken;

    .line 91
    iput-boolean v2, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->bAlbumSyncSuccess:Z

    .line 93
    return-void
.end method

.method static synthetic access$002(Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;Landroid/content/Context;)Landroid/content/Context;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;
    .param p1, "x1"    # Landroid/content/Context;

    .prologue
    .line 61
    iput-object p1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->mContext:Landroid/content/Context;

    return-object p1
.end method

.method static synthetic access$102(Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbToken;)Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbToken;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;
    .param p1, "x1"    # Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbToken;

    .prologue
    .line 61
    iput-object p1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->mFbToken:Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbToken;

    return-object p1
.end method

.method static synthetic access$202(Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;Landroid/accounts/Account;)Landroid/accounts/Account;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;
    .param p1, "x1"    # Landroid/accounts/Account;

    .prologue
    .line 61
    iput-object p1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->mAccount:Landroid/accounts/Account;

    return-object p1
.end method

.method static synthetic access$302(Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 61
    iput-object p1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->mAuthority:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$402(Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;Landroid/content/SyncResult;)Landroid/content/SyncResult;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;
    .param p1, "x1"    # Landroid/content/SyncResult;

    .prologue
    .line 61
    iput-object p1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->mSyncResult:Landroid/content/SyncResult;

    return-object p1
.end method

.method static synthetic access$500(Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;)Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;)Landroid/os/Bundle;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->mBundle:Landroid/os/Bundle;

    return-object v0
.end method

.method static synthetic access$602(Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;
    .param p1, "x1"    # Landroid/os/Bundle;

    .prologue
    .line 61
    iput-object p1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->mBundle:Landroid/os/Bundle;

    return-object p1
.end method

.method static synthetic access$700(Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->onSyncCanceled()V

    return-void
.end method

.method static synthetic access$802(Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;
    .param p1, "x1"    # I

    .prologue
    .line 61
    iput p1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->mSyncState:I

    return p1
.end method

.method static synthetic access$900(Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->mAlbumId:Ljava/lang/String;

    return-object v0
.end method

.method private handleSessionExpired()I
    .locals 11

    .prologue
    const/4 v9, 0x1

    const/4 v10, 0x0

    .line 389
    iget-object v7, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->mAccount:Landroid/accounts/Account;

    const-string v8, "com.sec.android.gallery3d.sns.contentprovider"

    invoke-static {v7, v8}, Landroid/content/ContentResolver;->cancelSync(Landroid/accounts/Account;Ljava/lang/String;)V

    .line 391
    iget-object v7, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->mContext:Landroid/content/Context;

    const-string v8, "notification"

    invoke-virtual {v7, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/app/NotificationManager;

    move-object v4, v7

    check-cast v4, Landroid/app/NotificationManager;

    .line 394
    .local v4, "notiMgr":Landroid/app/NotificationManager;
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 396
    .local v3, "notiIntent":Landroid/content/Intent;
    const-string v7, "com.sec.android.app.sns3.RETRY_SSO_FACEBOOK"

    invoke-virtual {v3, v7}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 400
    const/high16 v7, 0x800000

    invoke-virtual {v3, v7}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 401
    const-string v7, "RetryLogin"

    invoke-virtual {v3, v7, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 402
    iget-object v7, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->mContext:Landroid/content/Context;

    invoke-static {v7, v10, v3, v10}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 404
    .local v1, "launchIntent":Landroid/app/PendingIntent;
    const v6, 0x7f08003f

    .line 405
    .local v6, "titleID":I
    const v5, 0x7f080020

    .line 406
    .local v5, "spID":I
    const v0, 0x7f02001a

    .line 408
    .local v0, "iconID":I
    new-instance v2, Landroid/app/Notification$Builder;

    iget-object v7, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->mContext:Landroid/content/Context;

    invoke-direct {v2, v7}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    .line 409
    .local v2, "notiBuilder":Landroid/app/Notification$Builder;
    iget-object v7, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->mContext:Landroid/content/Context;

    new-array v8, v9, [Ljava/lang/Object;

    iget-object v9, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->mContext:Landroid/content/Context;

    invoke-virtual {v9, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v10

    invoke-virtual {v7, v6, v8}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v7

    invoke-virtual {v7, v0}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->mContext:Landroid/content/Context;

    const v9, 0x7f08003e

    invoke-virtual {v8, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v7

    invoke-virtual {v7, v1}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v7

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Landroid/app/Notification$Builder;->setWhen(J)Landroid/app/Notification$Builder;

    .line 413
    const/16 v7, 0x514

    invoke-virtual {v2}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v8

    invoke-virtual {v4, v7, v8}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 415
    const/4 v7, -0x1

    return v7
.end method

.method private invokeBroadcast()V
    .locals 8

    .prologue
    .line 434
    const/4 v2, 0x0

    .line 436
    .local v2, "intent":Landroid/content/Intent;
    sget-object v3, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$SyncAlbum;->CONTENT_URI:Landroid/net/Uri;

    .line 437
    .local v3, "uri_album":Landroid/net/Uri;
    sget-object v4, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$SyncPhoto;->CONTENT_URI:Landroid/net/Uri;

    .line 438
    .local v4, "uri_photo":Landroid/net/Uri;
    const-string v0, "com.sec.android.app.sns3.action.SYNC_GALLERY"

    .line 439
    .local v0, "action":Ljava/lang/String;
    iget v5, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->mSyncState:I

    const/4 v6, 0x2

    if-ne v5, v6, :cond_1

    const/4 v1, 0x1

    .line 441
    .local v1, "bResult":Z
    :goto_0
    new-instance v2, Landroid/content/Intent;

    .end local v2    # "intent":Landroid/content/Intent;
    invoke-direct {v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 442
    .restart local v2    # "intent":Landroid/content/Intent;
    const-string v5, "SNS3_CONTENT_URI_ALBUM"

    invoke-virtual {v2, v5, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 443
    const-string v5, "SNS3_CONTENT_URI_PHOTO"

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 444
    const-string v5, "SNS_RESULT"

    invoke-virtual {v2, v5, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 446
    const-string v5, "com.sec.android.app.sns3.permission.RECEIVE_SNS_BROADCAST"

    invoke-virtual {p0, v2, v5}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    .line 447
    invoke-static {}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->isLoggable()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 448
    const-string v5, "SnsFbSync"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "SnsFbSyncAdapterGalleryService - invokeBroadcast() : action = ["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "], uri(album) = ["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "], uri(photo) = ["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "], result = ["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 452
    :cond_0
    return-void

    .line 439
    .end local v1    # "bResult":Z
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private onSyncCanceled()V
    .locals 2

    .prologue
    .line 420
    const-string v0, "SnsFbSync"

    const-string v1, "***************** SnsFbSyncAdapterGalleryService : onSyncCanceled !!! *****************"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 423
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->mSyncState:I

    .line 425
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->mBundle:Landroid/os/Bundle;

    .line 426
    sget-object v0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->mReq:Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqBase;

    if-eqz v0, :cond_0

    .line 427
    sget-object v0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->mReq:Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqBase;

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqBase;->abort()Z

    .line 430
    :cond_0
    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 149
    const-string v0, "SnsFbSync"

    const-string v1, "***************** SnsFbSyncAdapterGalleryService : onBind !!! *****************"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 152
    iget-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->mSyncAdapter:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService$SyncAdapterImpl;

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService$SyncAdapterImpl;->getSyncAdapterBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 133
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 135
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/SnsApplication;->getSvcMgr()Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    .line 137
    iget-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->mSyncAdapter:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService$SyncAdapterImpl;

    if-nez v0, :cond_0

    .line 138
    new-instance v0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService$SyncAdapterImpl;

    invoke-direct {v0, p0, p0}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService$SyncAdapterImpl;-><init>(Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->mSyncAdapter:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService$SyncAdapterImpl;

    .line 141
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;

    if-nez v0, :cond_1

    .line 142
    new-instance v0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;

    invoke-direct {v0}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;

    .line 144
    :cond_1
    return-void
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 158
    const/4 v0, 0x0

    .line 159
    .local v0, "bActive":Z
    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->mAccount:Landroid/accounts/Account;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->mAuthority:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 160
    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->mAccount:Landroid/accounts/Account;

    iget-object v2, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->mAuthority:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/content/ContentResolver;->isSyncActive(Landroid/accounts/Account;Ljava/lang/String;)Z

    move-result v0

    .line 163
    :cond_0
    const-string v1, "SnsFbSync"

    const-string v2, "***************** SnsFbSyncAdapterGalleryService : onUnbind !!! *****************"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 166
    if-eqz v0, :cond_2

    iget v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->mSyncState:I

    const/4 v2, 0x1

    if-eq v1, v2, :cond_1

    iget v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->mSyncState:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_2

    .line 168
    :cond_1
    const-string v1, "SnsFbSync"

    const-string v2, "SnsFbSyncAdapterGalleryService : onUnbind : SYNC ERROR : performSync was stopped by forced abort or pending problem!!!"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 174
    :goto_0
    invoke-super {p0, p1}, Landroid/app/Service;->onUnbind(Landroid/content/Intent;)Z

    move-result v1

    return v1

    .line 172
    :cond_2
    const-string v1, "SnsFbSync"

    const-string v2, "SnsFbSyncAdapterGalleryService : onUnbind : COMPLETE STATE!!!"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected performPhotoSync()V
    .locals 13
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/accounts/OperationCanceledException;
        }
    .end annotation

    .prologue
    const/4 v12, -0x1

    const/4 v11, 0x0

    const/4 v10, 0x2

    const/4 v9, 0x1

    .line 272
    const-string v0, "SnsFbSync"

    const-string v1, "***************** SnsFbSyncAdapterGalleryService : performPhotoSync - START !!! *****************"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 275
    const/4 v6, 0x0

    .line 277
    .local v6, "bDeletePrevId":Z
    iput v9, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->mSyncState:I

    .line 278
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 281
    .local v5, "photoArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhotos;>;"
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->mFbToken:Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbToken;

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbToken;->getTokenState()I

    move-result v0

    if-eq v0, v9, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->mFbToken:Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbToken;

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbToken;->getTokenState()I

    move-result v0

    if-ne v0, v10, :cond_1

    .line 283
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->handleSessionExpired()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->mSyncState:I

    .line 284
    new-instance v0, Ljava/lang/Exception;

    const-string v1, "Session expired or invalid!!!"

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 359
    :catch_0
    move-exception v7

    .line 361
    .local v7, "e":Ljava/lang/Exception;
    :try_start_1
    const-string v0, "SnsFbSync"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SnsFbSyncAdapterGalleryService(Photos) : EXCEPTION !!! "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v7}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 364
    const/4 v6, 0x1

    .line 365
    iget-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->mSyncResult:Landroid/content/SyncResult;

    iget-object v0, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    const-wide/16 v2, 0x1

    iput-wide v2, v0, Landroid/content/SyncStats;->numConflictDetectedExceptions:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 369
    if-eqz v6, :cond_b

    .line 370
    iget-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;

    const-string v1, "album"

    invoke-virtual {v0, v1, v11}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->deleteSyncValue(Ljava/lang/String;I)V

    .line 377
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;

    const-string v1, "album"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->deletePrevSyncState(Ljava/lang/String;)V

    .line 379
    invoke-direct {p0}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->invokeBroadcast()V

    .line 381
    const-string v0, "SnsFbSync"

    const-string v1, "***************** SnsFbSyncAdapterGalleryService : performPhotoSync - FINISHED !!! *****************"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 386
    .end local v7    # "e":Ljava/lang/Exception;
    :goto_1
    return-void

    .line 288
    :cond_1
    :try_start_2
    iget-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->getAlbumIdCursor()Landroid/database/Cursor;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v8

    .line 292
    .local v8, "snsCursorPhotos":Landroid/database/Cursor;
    if-eqz v8, :cond_9

    :try_start_3
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 295
    :cond_2
    const-string v0, "photo"

    iput-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->mSyncType:Ljava/lang/String;

    .line 296
    const-string v0, "id"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->mAlbumId:Ljava/lang/String;

    .line 301
    :cond_3
    new-instance v0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService$2;

    iget-object v2, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    iget-object v3, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->mAlbumId:Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->mBundle:Landroid/os/Bundle;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService$2;-><init>(Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;Landroid/os/Bundle;Ljava/util/ArrayList;)V

    sput-object v0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->mReq:Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqBase;

    .line 331
    sget-object v0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->mReq:Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqBase;

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqBase;->request()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 332
    iget-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->suspendSync()V

    .line 336
    :goto_2
    iget v0, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->mSyncState:I

    if-ne v0, v12, :cond_6

    .line 337
    new-instance v0, Ljava/lang/Exception;

    const-string v1, "SnsFbSyncAdapterGalleryService(Photos) is failed!!!"

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 354
    :catchall_0
    move-exception v0

    if-eqz v8, :cond_4

    .line 355
    :try_start_4
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 369
    .end local v8    # "snsCursorPhotos":Landroid/database/Cursor;
    :catchall_1
    move-exception v0

    if-eqz v6, :cond_c

    .line 370
    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;

    const-string v2, "album"

    invoke-virtual {v1, v2, v11}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->deleteSyncValue(Ljava/lang/String;I)V

    .line 377
    :goto_3
    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;

    const-string v2, "album"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->deletePrevSyncState(Ljava/lang/String;)V

    .line 379
    invoke-direct {p0}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->invokeBroadcast()V

    .line 381
    const-string v1, "SnsFbSync"

    const-string v2, "***************** SnsFbSyncAdapterGalleryService : performPhotoSync - FINISHED !!! *****************"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    throw v0

    .line 334
    .restart local v8    # "snsCursorPhotos":Landroid/database/Cursor;
    :cond_5
    const/4 v0, -0x1

    :try_start_5
    iput v0, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->mSyncState:I

    goto :goto_2

    .line 340
    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->mBundle:Landroid/os/Bundle;

    if-nez v0, :cond_3

    .line 342
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_2

    .line 344
    invoke-virtual {v5}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_7

    .line 345
    iget-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;

    sget-object v1, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$SyncTags;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->clearTable(Landroid/net/Uri;)V

    .line 346
    iget-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;

    invoke-virtual {v0, v5}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->insertTagInfo(Ljava/util/ArrayList;)V

    .line 348
    :cond_7
    invoke-virtual {v5}, Ljava/util/ArrayList;->clear()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 354
    :goto_4
    if-eqz v8, :cond_8

    .line 355
    :try_start_6
    invoke-interface {v8}, Landroid/database/Cursor;->close()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 369
    :cond_8
    if-eqz v6, :cond_a

    .line 370
    iget-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;

    const-string v1, "album"

    invoke-virtual {v0, v1, v11}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->deleteSyncValue(Ljava/lang/String;I)V

    .line 377
    :goto_5
    iget-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;

    const-string v1, "album"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->deletePrevSyncState(Ljava/lang/String;)V

    .line 379
    invoke-direct {p0}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->invokeBroadcast()V

    .line 381
    const-string v0, "SnsFbSync"

    const-string v1, "***************** SnsFbSyncAdapterGalleryService : performPhotoSync - FINISHED !!! *****************"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 351
    :cond_9
    :try_start_7
    const-string v0, "SnsFbSync"

    const-string v1, "SnsFbSyncAdapterGalleryService(Photos) : No photos"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_4

    .line 372
    :cond_a
    iget-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;

    const-string v1, "album"

    invoke-virtual {v0, v1, v9}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->deleteSyncValue(Ljava/lang/String;I)V

    .line 373
    iget-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;

    const-string v1, "album"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->updateCurrentSyncValue(Ljava/lang/String;)V

    .line 375
    iput v10, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->mSyncState:I

    goto :goto_5

    .line 372
    .end local v8    # "snsCursorPhotos":Landroid/database/Cursor;
    .restart local v7    # "e":Ljava/lang/Exception;
    :cond_b
    iget-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;

    const-string v1, "album"

    invoke-virtual {v0, v1, v9}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->deleteSyncValue(Ljava/lang/String;I)V

    .line 373
    iget-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;

    const-string v1, "album"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->updateCurrentSyncValue(Ljava/lang/String;)V

    .line 375
    iput v10, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->mSyncState:I

    goto/16 :goto_0

    .line 372
    .end local v7    # "e":Ljava/lang/Exception;
    :cond_c
    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;

    const-string v2, "album"

    invoke-virtual {v1, v2, v9}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->deleteSyncValue(Ljava/lang/String;I)V

    .line 373
    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;

    const-string v2, "album"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->updateCurrentSyncValue(Ljava/lang/String;)V

    .line 375
    iput v10, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->mSyncState:I

    goto/16 :goto_3
.end method

.method public performSync()V
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/accounts/OperationCanceledException;
        }
    .end annotation

    .prologue
    const/4 v9, -0x1

    const/4 v8, 0x0

    const/4 v7, 0x2

    const/4 v6, 0x1

    .line 179
    const-string v2, "SnsFbSync"

    const-string v3, "***************** SnsFbSyncAdapterGalleryService : performSync - START !!! *****************"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 182
    const/4 v0, 0x0

    .line 184
    .local v0, "bDeletePrevId":Z
    iput v6, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->mSyncState:I

    .line 187
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->mFbToken:Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbToken;

    invoke-virtual {v2}, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbToken;->getTokenState()I

    move-result v2

    if-eq v2, v6, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->mFbToken:Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbToken;

    invoke-virtual {v2}, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbToken;->getTokenState()I

    move-result v2

    if-ne v2, v7, :cond_2

    .line 189
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->handleSessionExpired()I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->mSyncState:I

    .line 190
    new-instance v2, Ljava/lang/Exception;

    const-string v3, "Session expired or invalid!!!"

    invoke-direct {v2, v3}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 245
    :catch_0
    move-exception v1

    .line 246
    .local v1, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 247
    const-string v2, "SnsFbSync"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SnsFbSyncAdapterGalleryService : EXCEPTION !!! "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 249
    iget-object v2, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->mSyncResult:Landroid/content/SyncResult;

    iget-object v2, v2, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    const-wide/16 v4, 0x1

    iput-wide v4, v2, Landroid/content/SyncStats;->numConflictDetectedExceptions:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 250
    const/4 v0, 0x1

    .line 253
    if-eqz v0, :cond_b

    .line 254
    iget-object v2, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;

    const-string v3, "album"

    invoke-virtual {v2, v3, v8}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->deleteSyncValue(Ljava/lang/String;I)V

    .line 259
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;

    const-string v3, "album"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->deletePrevSyncState(Ljava/lang/String;)V

    .line 261
    iget-boolean v2, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->bAlbumSyncSuccess:Z

    if-eqz v2, :cond_1

    .line 262
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->performPhotoSync()V

    .line 264
    :cond_1
    const-string v2, "SnsFbSync"

    const-string v3, "***************** SnsFbSyncAdapterGalleryService : performSync - FINISHED !!! *****************"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 268
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_1
    return-void

    .line 194
    :cond_2
    :try_start_2
    iget-object v2, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->mSyncType:Ljava/lang/String;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->mSyncType:Ljava/lang/String;

    const-string v3, "album"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 198
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->mSyncType:Ljava/lang/String;

    if-nez v2, :cond_4

    .line 199
    const-string v2, "album"

    iput-object v2, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->mSyncType:Ljava/lang/String;

    .line 203
    :cond_4
    new-instance v2, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService$1;

    iget-object v3, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    const-string v4, "me"

    iget-object v5, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->mBundle:Landroid/os/Bundle;

    invoke-direct {v2, p0, v3, v4, v5}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService$1;-><init>(Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;Landroid/os/Bundle;)V

    sput-object v2, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->mReq:Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqBase;

    .line 230
    sget-object v2, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->mReq:Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqBase;

    invoke-virtual {v2}, Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqBase;->request()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 231
    iget-object v2, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;

    invoke-virtual {v2}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->suspendSync()V

    .line 235
    :goto_2
    iget v2, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->mSyncState:I

    if-ne v2, v9, :cond_7

    .line 236
    new-instance v2, Ljava/lang/Exception;

    const-string v3, "SnsFbSyncAdapterGalleryService(Albums) is failed!!!"

    invoke-direct {v2, v3}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 253
    :catchall_0
    move-exception v2

    if-eqz v0, :cond_c

    .line 254
    iget-object v3, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;

    const-string v4, "album"

    invoke-virtual {v3, v4, v8}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->deleteSyncValue(Ljava/lang/String;I)V

    .line 259
    :goto_3
    iget-object v3, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;

    const-string v4, "album"

    invoke-virtual {v3, v4}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->deletePrevSyncState(Ljava/lang/String;)V

    .line 261
    iget-boolean v3, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->bAlbumSyncSuccess:Z

    if-eqz v3, :cond_5

    .line 262
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->performPhotoSync()V

    .line 264
    :cond_5
    const-string v3, "SnsFbSync"

    const-string v4, "***************** SnsFbSyncAdapterGalleryService : performSync - FINISHED !!! *****************"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    throw v2

    .line 233
    :cond_6
    const/4 v2, -0x1

    :try_start_3
    iput v2, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->mSyncState:I

    goto :goto_2

    .line 239
    :cond_7
    iget-object v2, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->mBundle:Landroid/os/Bundle;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    if-nez v2, :cond_4

    .line 253
    :cond_8
    if-eqz v0, :cond_a

    .line 254
    iget-object v2, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;

    const-string v3, "album"

    invoke-virtual {v2, v3, v8}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->deleteSyncValue(Ljava/lang/String;I)V

    .line 259
    :goto_4
    iget-object v2, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;

    const-string v3, "album"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->deletePrevSyncState(Ljava/lang/String;)V

    .line 261
    iget-boolean v2, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->bAlbumSyncSuccess:Z

    if-eqz v2, :cond_9

    .line 262
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->performPhotoSync()V

    .line 264
    :cond_9
    const-string v2, "SnsFbSync"

    const-string v3, "***************** SnsFbSyncAdapterGalleryService : performSync - FINISHED !!! *****************"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 256
    :cond_a
    iput v7, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->mSyncState:I

    .line 257
    iput-boolean v6, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->bAlbumSyncSuccess:Z

    goto :goto_4

    .line 256
    .restart local v1    # "e":Ljava/lang/Exception;
    :cond_b
    iput v7, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->mSyncState:I

    .line 257
    iput-boolean v6, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->bAlbumSyncSuccess:Z

    goto/16 :goto_0

    .line 256
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_c
    iput v7, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->mSyncState:I

    .line 257
    iput-boolean v6, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->bAlbumSyncSuccess:Z

    goto :goto_3
.end method
