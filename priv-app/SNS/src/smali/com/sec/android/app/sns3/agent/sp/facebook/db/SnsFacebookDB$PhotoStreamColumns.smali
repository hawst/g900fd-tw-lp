.class public interface abstract Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$PhotoStreamColumns;
.super Ljava/lang/Object;
.source "SnsFacebookDB.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "PhotoStreamColumns"
.end annotation


# static fields
.field public static final ACTOR_ID:Ljava/lang/String; = "actor_id"

.field public static final COMMENTS_COUNT:Ljava/lang/String; = "comments_count"

.field public static final CREATED_TIME:Ljava/lang/String; = "created_time"

.field public static final LIKES_COUNT:Ljava/lang/String; = "likes_count"

.field public static final MESSAGE:Ljava/lang/String; = "message"

.field public static final PHOTO_URL:Ljava/lang/String; = "photo_url"

.field public static final POST_ID:Ljava/lang/String; = "post_id"

.field public static final UPDATED_TIME:Ljava/lang/String; = "updated_time"

.field public static final USER_NAME:Ljava/lang/String; = "user_name"

.field public static final USER_PROFILE_URL:Ljava/lang/String; = "profile_url"
