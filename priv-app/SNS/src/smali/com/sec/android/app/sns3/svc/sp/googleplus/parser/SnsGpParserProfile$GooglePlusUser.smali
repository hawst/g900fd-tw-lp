.class public interface abstract Lcom/sec/android/app/sns3/svc/sp/googleplus/parser/SnsGpParserProfile$GooglePlusUser;
.super Ljava/lang/Object;
.source "SnsGpParserProfile.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/svc/sp/googleplus/parser/SnsGpParserProfile;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "GooglePlusUser"
.end annotation


# static fields
.field public static final BIRTHDAY:Ljava/lang/String; = "birthday"

.field public static final EMAILS:Ljava/lang/String; = "emails"

.field public static final FIRST_NAME:Ljava/lang/String; = "givenName"

.field public static final FORMATTED_NAME:Ljava/lang/String; = "displayName"

.field public static final LAST_NAME:Ljava/lang/String; = "familyName"

.field public static final NAME:Ljava/lang/String; = "name"

.field public static final OBJECT_TYPE:Ljava/lang/String; = "objectType"

.field public static final ORGANIZATIONS:Ljava/lang/String; = "organizations"

.field public static final PLACES_LIVED:Ljava/lang/String; = "placesLived"

.field public static final PRIMARY:Ljava/lang/String; = "primary"

.field public static final PROFILE_ID:Ljava/lang/String; = "id"

.field public static final PROFILE_IMAGE_URL:Ljava/lang/String; = "image"

.field public static final PROFILE_URL:Ljava/lang/String; = "url"

.field public static final RELATIONSHIP_STATUS:Ljava/lang/String; = "relationshipStatus"

.field public static final TAGLINE:Ljava/lang/String; = "tagline"

.field public static final VALUE:Ljava/lang/String; = "value"
