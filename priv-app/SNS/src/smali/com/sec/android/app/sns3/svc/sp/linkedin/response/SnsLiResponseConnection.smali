.class public final Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;
.super Ljava/lang/Object;
.source "SnsLiResponseConnection.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection$LinkedInConnections;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mCompanyId:Ljava/lang/String;

.field private mCompanyName:Ljava/lang/String;

.field private mFirstName:Ljava/lang/String;

.field private mHeadline:Ljava/lang/String;

.field private mId:Ljava/lang/String;

.field private mIndustry:Ljava/lang/String;

.field private mIsCurrent:Ljava/lang/Boolean;

.field private mLastName:Ljava/lang/String;

.field private mLocation:Ljava/lang/String;

.field private mLocationCountryCode:Ljava/lang/String;

.field private mPictureUrl:Ljava/lang/String;

.field private mPositions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;",
            ">;"
        }
    .end annotation
.end field

.field private mProfileUrl:Ljava/lang/String;

.field private mSize:Ljava/lang/String;

.field private mStartMonth:Ljava/lang/String;

.field private mStartYear:Ljava/lang/String;

.field private mSummary:Ljava/lang/String;

.field private mTitle:Ljava/lang/String;

.field private mType:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 273
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection$1;

    invoke-direct {v0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection$1;-><init>()V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 284
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 285
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 287
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 288
    invoke-virtual {p0, p1}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;->readFromParcel(Landroid/os/Parcel;)V

    .line 289
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection$1;

    .prologue
    .line 31
    invoke-direct {p0, p1}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public static parse(Lorg/json/JSONObject;)Ljava/util/List;
    .locals 19
    .param p0, "contentObj"    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/json/JSONObject;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;",
            ">;"
        }
    .end annotation

    .prologue
    .line 347
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 349
    .local v2, "ConnectionsList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;>;"
    if-eqz p0, :cond_4

    .line 353
    :try_start_0
    const-string v17, "values"

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v9

    .line 355
    .local v9, "ja":Lorg/json/JSONArray;
    if-eqz v9, :cond_4

    .line 357
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    invoke-virtual {v9}, Lorg/json/JSONArray;->length()I

    move-result v17

    move/from16 v0, v17

    if-ge v6, v0, :cond_4

    .line 359
    new-instance v12, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;

    invoke-direct {v12}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;-><init>()V

    .line 361
    .local v12, "newConnection":Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;
    invoke-virtual {v9, v6}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v10

    .line 362
    .local v10, "jsonObj":Lorg/json/JSONObject;
    const-string v17, "id"

    move-object/from16 v0, v17

    invoke-virtual {v10, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 363
    .local v7, "id":Ljava/lang/String;
    if-eqz v7, :cond_3

    const-string v17, "private"

    move-object/from16 v0, v17

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-nez v17, :cond_3

    .line 366
    const-string v17, "id"

    move-object/from16 v0, v17

    invoke-virtual {v10, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v12, v0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;->setID(Ljava/lang/String;)V

    .line 367
    const-string v17, "industry"

    move-object/from16 v0, v17

    invoke-virtual {v10, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v12, v0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;->setIndustry(Ljava/lang/String;)V

    .line 369
    const-string v17, "firstName"

    move-object/from16 v0, v17

    invoke-virtual {v10, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v12, v0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;->setFirstName(Ljava/lang/String;)V

    .line 371
    const-string v17, "lastName"

    move-object/from16 v0, v17

    invoke-virtual {v10, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v12, v0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;->setLastName(Ljava/lang/String;)V

    .line 373
    const-string v17, "headline"

    move-object/from16 v0, v17

    invoke-virtual {v10, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v12, v0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;->setHeadline(Ljava/lang/String;)V

    .line 375
    const-string v17, "pictureUrl"

    move-object/from16 v0, v17

    invoke-virtual {v10, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v12, v0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;->setPictureUrl(Ljava/lang/String;)V

    .line 377
    const-string v17, "publicProfileUrl"

    move-object/from16 v0, v17

    invoke-virtual {v10, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v12, v0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;->setProfileUrl(Ljava/lang/String;)V

    .line 380
    const-string v17, "location"

    move-object/from16 v0, v17

    invoke-virtual {v10, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v11

    .line 382
    .local v11, "location":Lorg/json/JSONObject;
    if-eqz v11, :cond_0

    .line 383
    const-string v17, "name"

    move-object/from16 v0, v17

    invoke-virtual {v11, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v12, v0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;->setLocation(Ljava/lang/String;)V

    .line 386
    const-string v17, "country"

    move-object/from16 v0, v17

    invoke-virtual {v11, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    .line 388
    .local v4, "country":Lorg/json/JSONObject;
    const-string v17, "code"

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v12, v0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;->setLocationCountryCode(Ljava/lang/String;)V

    .line 391
    .end local v4    # "country":Lorg/json/JSONObject;
    :cond_0
    const-string v17, "positions"

    move-object/from16 v0, v17

    invoke-virtual {v10, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v14

    .line 394
    .local v14, "position":Lorg/json/JSONObject;
    invoke-static {v14}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;->parse(Lorg/json/JSONObject;)Ljava/util/List;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v12, v0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;->setPositions(Ljava/util/List;)V

    .line 396
    if-eqz v14, :cond_2

    const-string v17, "_total"

    move-object/from16 v0, v17

    invoke-virtual {v14, v0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v17

    if-lez v17, :cond_2

    .line 397
    const-string v17, "values"

    move-object/from16 v0, v17

    invoke-virtual {v14, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v15

    .line 399
    .local v15, "positionArray":Lorg/json/JSONArray;
    if-eqz v15, :cond_2

    .line 400
    const/4 v8, 0x0

    .local v8, "j":I
    :goto_1
    invoke-virtual {v15}, Lorg/json/JSONArray;->length()I

    move-result v17

    move/from16 v0, v17

    if-ge v8, v0, :cond_2

    .line 401
    invoke-virtual {v15, v8}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v13

    .line 403
    .local v13, "newJson":Lorg/json/JSONObject;
    const-string v17, "isCurrent"

    move-object/from16 v0, v17

    invoke-virtual {v13, v0}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v17

    invoke-static/range {v17 .. v17}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v12, v0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;->setIsCurrent(Ljava/lang/Boolean;)V

    .line 406
    iget-object v0, v12, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;->mIsCurrent:Ljava/lang/Boolean;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v17

    const/16 v18, 0x1

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_1

    .line 407
    const-string v17, "title"

    move-object/from16 v0, v17

    invoke-virtual {v13, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v12, v0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;->setTitle(Ljava/lang/String;)V

    .line 409
    const-string v17, "summary"

    move-object/from16 v0, v17

    invoke-virtual {v13, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v12, v0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;->setSummary(Ljava/lang/String;)V

    .line 412
    const-string v17, "company"

    move-object/from16 v0, v17

    invoke-virtual {v13, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v3

    .line 414
    .local v3, "company":Lorg/json/JSONObject;
    const-string v17, "id"

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v12, v0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;->setCompanyId(Ljava/lang/String;)V

    .line 416
    const-string v17, "name"

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v12, v0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;->setCompanyName(Ljava/lang/String;)V

    .line 418
    const-string v17, "size"

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v12, v0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;->setSize(Ljava/lang/String;)V

    .line 420
    const-string v17, "type"

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v12, v0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;->setType(Ljava/lang/String;)V

    .line 423
    const-string v17, "industry"

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v12, v0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;->setIndustry(Ljava/lang/String;)V

    .line 426
    const-string v17, "startDate"

    move-object/from16 v0, v17

    invoke-virtual {v13, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v16

    .line 428
    .local v16, "startdate":Lorg/json/JSONObject;
    if-eqz v16, :cond_1

    .line 429
    const-string v17, "month"

    invoke-virtual/range {v16 .. v17}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v12, v0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;->setStartMonth(Ljava/lang/String;)V

    .line 431
    const-string v17, "year"

    invoke-virtual/range {v16 .. v17}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v12, v0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;->setStartYear(Ljava/lang/String;)V

    .line 400
    .end local v3    # "company":Lorg/json/JSONObject;
    .end local v16    # "startdate":Lorg/json/JSONObject;
    :cond_1
    add-int/lit8 v8, v8, 0x1

    goto/16 :goto_1

    .line 438
    .end local v8    # "j":I
    .end local v13    # "newJson":Lorg/json/JSONObject;
    .end local v15    # "positionArray":Lorg/json/JSONArray;
    :cond_2
    invoke-interface {v2, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 357
    .end local v11    # "location":Lorg/json/JSONObject;
    .end local v14    # "position":Lorg/json/JSONObject;
    :cond_3
    add-int/lit8 v6, v6, 0x1

    goto/16 :goto_0

    .line 444
    .end local v6    # "i":I
    .end local v7    # "id":Ljava/lang/String;
    .end local v9    # "ja":Lorg/json/JSONArray;
    .end local v10    # "jsonObj":Lorg/json/JSONObject;
    .end local v12    # "newConnection":Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;
    :catch_0
    move-exception v5

    .line 445
    .local v5, "e":Lorg/json/JSONException;
    invoke-virtual {v5}, Lorg/json/JSONException;->printStackTrace()V

    .line 448
    .end local v5    # "e":Lorg/json/JSONException;
    :cond_4
    return-object v2
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 342
    const/4 v0, 0x0

    return v0
.end method

.method public getCompanyId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 186
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;->mCompanyId:Ljava/lang/String;

    return-object v0
.end method

.method public getCompanyName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;->mCompanyName:Ljava/lang/String;

    return-object v0
.end method

.method public getFirstName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;->mFirstName:Ljava/lang/String;

    return-object v0
.end method

.method public getHeadline()Ljava/lang/String;
    .locals 1

    .prologue
    .line 170
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;->mHeadline:Ljava/lang/String;

    return-object v0
.end method

.method public getID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;->mId:Ljava/lang/String;

    return-object v0
.end method

.method public getIndustry()Ljava/lang/String;
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;->mIndustry:Ljava/lang/String;

    return-object v0
.end method

.method public getIsCurrent()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 226
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;->mIsCurrent:Ljava/lang/Boolean;

    return-object v0
.end method

.method public getLastName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;->mLastName:Ljava/lang/String;

    return-object v0
.end method

.method public getLocation()Ljava/lang/String;
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;->mLocation:Ljava/lang/String;

    return-object v0
.end method

.method public getLocationCountryCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;->mLocationCountryCode:Ljava/lang/String;

    return-object v0
.end method

.method public getPictureUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 178
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;->mPictureUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getPositions()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;",
            ">;"
        }
    .end annotation

    .prologue
    .line 242
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;->mPositions:Ljava/util/List;

    return-object v0
.end method

.method public getProfileUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 266
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;->mProfileUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getSize()Ljava/lang/String;
    .locals 1

    .prologue
    .line 250
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;->mSize:Ljava/lang/String;

    return-object v0
.end method

.method public getStartMonth()Ljava/lang/String;
    .locals 1

    .prologue
    .line 210
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;->mStartMonth:Ljava/lang/String;

    return-object v0
.end method

.method public getStartYear()Ljava/lang/String;
    .locals 1

    .prologue
    .line 218
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;->mStartYear:Ljava/lang/String;

    return-object v0
.end method

.method public getSummary()Ljava/lang/String;
    .locals 1

    .prologue
    .line 202
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;->mSummary:Ljava/lang/String;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 194
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;->mTitle:Ljava/lang/String;

    return-object v0
.end method

.method public getType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 258
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;->mType:Ljava/lang/String;

    return-object v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 317
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;->mId:Ljava/lang/String;

    .line 318
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;->mFirstName:Ljava/lang/String;

    .line 319
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;->mLastName:Ljava/lang/String;

    .line 320
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;->mHeadline:Ljava/lang/String;

    .line 321
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;->mPictureUrl:Ljava/lang/String;

    .line 322
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;->mLocation:Ljava/lang/String;

    .line 323
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;->mLocationCountryCode:Ljava/lang/String;

    .line 324
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;->mIndustry:Ljava/lang/String;

    .line 325
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;->mCompanyId:Ljava/lang/String;

    .line 326
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;->mTitle:Ljava/lang/String;

    .line 327
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;->mSummary:Ljava/lang/String;

    .line 328
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;->mStartMonth:Ljava/lang/String;

    .line 329
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;->mStartYear:Ljava/lang/String;

    .line 330
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;->mIsCurrent:Ljava/lang/Boolean;

    .line 331
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;->mCompanyName:Ljava/lang/String;

    .line 332
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;->mIndustry:Ljava/lang/String;

    .line 333
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;->mSize:Ljava/lang/String;

    .line 334
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;->mType:Ljava/lang/String;

    .line 335
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;->mProfileUrl:Ljava/lang/String;

    .line 336
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;->mPositions:Ljava/util/List;

    const-class v1, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readList(Ljava/util/List;Ljava/lang/ClassLoader;)V

    .line 338
    return-void
.end method

.method public setCompanyId(Ljava/lang/String;)V
    .locals 0
    .param p1, "mCompanyId"    # Ljava/lang/String;

    .prologue
    .line 190
    iput-object p1, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;->mCompanyId:Ljava/lang/String;

    .line 191
    return-void
.end method

.method public setCompanyName(Ljava/lang/String;)V
    .locals 0
    .param p1, "mCompanyName"    # Ljava/lang/String;

    .prologue
    .line 238
    iput-object p1, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;->mCompanyName:Ljava/lang/String;

    .line 239
    return-void
.end method

.method public setFirstName(Ljava/lang/String;)V
    .locals 0
    .param p1, "mFirstName"    # Ljava/lang/String;

    .prologue
    .line 158
    iput-object p1, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;->mFirstName:Ljava/lang/String;

    .line 159
    return-void
.end method

.method public setHeadline(Ljava/lang/String;)V
    .locals 0
    .param p1, "mHeadline"    # Ljava/lang/String;

    .prologue
    .line 174
    iput-object p1, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;->mHeadline:Ljava/lang/String;

    .line 175
    return-void
.end method

.method public setID(Ljava/lang/String;)V
    .locals 0
    .param p1, "mId"    # Ljava/lang/String;

    .prologue
    .line 150
    iput-object p1, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;->mId:Ljava/lang/String;

    .line 151
    return-void
.end method

.method public setIndustry(Ljava/lang/String;)V
    .locals 0
    .param p1, "mIndustry"    # Ljava/lang/String;

    .prologue
    .line 142
    iput-object p1, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;->mIndustry:Ljava/lang/String;

    .line 143
    return-void
.end method

.method public setIsCurrent(Ljava/lang/Boolean;)V
    .locals 0
    .param p1, "mIsCurrent"    # Ljava/lang/Boolean;

    .prologue
    .line 230
    iput-object p1, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;->mIsCurrent:Ljava/lang/Boolean;

    .line 231
    return-void
.end method

.method public setLastName(Ljava/lang/String;)V
    .locals 0
    .param p1, "mLastName"    # Ljava/lang/String;

    .prologue
    .line 166
    iput-object p1, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;->mLastName:Ljava/lang/String;

    .line 167
    return-void
.end method

.method public setLocation(Ljava/lang/String;)V
    .locals 0
    .param p1, "mLocation"    # Ljava/lang/String;

    .prologue
    .line 134
    iput-object p1, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;->mLocation:Ljava/lang/String;

    .line 135
    return-void
.end method

.method public setLocationCountryCode(Ljava/lang/String;)V
    .locals 0
    .param p1, "mLocationCountryCode"    # Ljava/lang/String;

    .prologue
    .line 126
    iput-object p1, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;->mLocationCountryCode:Ljava/lang/String;

    .line 127
    return-void
.end method

.method public setPictureUrl(Ljava/lang/String;)V
    .locals 0
    .param p1, "mPictureUrl"    # Ljava/lang/String;

    .prologue
    .line 182
    iput-object p1, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;->mPictureUrl:Ljava/lang/String;

    .line 183
    return-void
.end method

.method public setPositions(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 246
    .local p1, "mPositions":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;>;"
    iput-object p1, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;->mPositions:Ljava/util/List;

    .line 247
    return-void
.end method

.method public setProfileUrl(Ljava/lang/String;)V
    .locals 0
    .param p1, "mProfileUrl"    # Ljava/lang/String;

    .prologue
    .line 270
    iput-object p1, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;->mProfileUrl:Ljava/lang/String;

    .line 271
    return-void
.end method

.method public setSize(Ljava/lang/String;)V
    .locals 0
    .param p1, "mSize"    # Ljava/lang/String;

    .prologue
    .line 254
    iput-object p1, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;->mSize:Ljava/lang/String;

    .line 255
    return-void
.end method

.method public setStartMonth(Ljava/lang/String;)V
    .locals 0
    .param p1, "mStartMonth"    # Ljava/lang/String;

    .prologue
    .line 214
    iput-object p1, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;->mStartMonth:Ljava/lang/String;

    .line 215
    return-void
.end method

.method public setStartYear(Ljava/lang/String;)V
    .locals 0
    .param p1, "mStartYear"    # Ljava/lang/String;

    .prologue
    .line 222
    iput-object p1, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;->mStartYear:Ljava/lang/String;

    .line 223
    return-void
.end method

.method public setSummary(Ljava/lang/String;)V
    .locals 0
    .param p1, "mSummary"    # Ljava/lang/String;

    .prologue
    .line 206
    iput-object p1, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;->mSummary:Ljava/lang/String;

    .line 207
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 0
    .param p1, "mTitle"    # Ljava/lang/String;

    .prologue
    .line 198
    iput-object p1, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;->mTitle:Ljava/lang/String;

    .line 199
    return-void
.end method

.method public setType(Ljava/lang/String;)V
    .locals 0
    .param p1, "mType"    # Ljava/lang/String;

    .prologue
    .line 262
    iput-object p1, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;->mType:Ljava/lang/String;

    .line 263
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flag"    # I

    .prologue
    .line 293
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;->mId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 294
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;->mFirstName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 295
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;->mLastName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 296
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;->mHeadline:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 297
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;->mPictureUrl:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 298
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;->mLocation:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 299
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;->mLocationCountryCode:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 300
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;->mIndustry:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 301
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;->mCompanyId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 302
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;->mTitle:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 303
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;->mSummary:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 304
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;->mStartMonth:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 305
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;->mStartYear:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 306
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;->mIsCurrent:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 307
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;->mCompanyName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 308
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;->mIndustry:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 309
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;->mSize:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 310
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;->mType:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 311
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;->mProfileUrl:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 312
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;->mPositions:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 314
    return-void
.end method
