.class public Lcom/sec/android/app/sns3/auth/sp/sinaweibo/SnsAccountSwAuth;
.super Ljava/lang/Object;
.source "SnsAccountSwAuth.java"


# static fields
.field public static final ACTION_SNS_SINAWEIBO_LOGGED_IN:Ljava/lang/String; = "com.sec.android.app.sns.ACTION_SNS_SINAWEIBO_LOGGED_IN"

.field public static final ACTION_SNS_SINAWEIBO_LOGGED_OUT:Ljava/lang/String; = "com.sec.android.app.sns.ACTION_SNS_SINAWEIBO_LOGGED_OUT"

.field public static final LOGIN_REQUEST_CODE:I = 0x2ee0

.field public static final PROFILE_FEED_PREFERENCE_KEY:Ljava/lang/String; = "SW_profile_feed_sync_interval"

.field public static final RETRY_LOGIN_NOTIFICATION_ID:I = 0x2fa8

.field public static final SINAWEIBO_SSO_NOTIFICATION_ID:I = 0x2f44

.field public static final SNS_BROADCAST_PERMISSION:Ljava/lang/String; = "com.sec.android.app.sns3.permission.RECEIVE_SNS_BROADCAST"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
