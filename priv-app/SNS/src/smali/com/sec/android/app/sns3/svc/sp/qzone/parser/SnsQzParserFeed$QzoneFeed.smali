.class public interface abstract Lcom/sec/android/app/sns3/svc/sp/qzone/parser/SnsQzParserFeed$QzoneFeed;
.super Ljava/lang/Object;
.source "SnsQzParserFeed.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/svc/sp/qzone/parser/SnsQzParserFeed;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "QzoneFeed"
.end annotation


# static fields
.field public static final COMMENT_COUNT:Ljava/lang/String; = "commentcount"

.field public static final CREATED_TIME:Ljava/lang/String; = "time"

.field public static final DATA:Ljava/lang/String; = "data"

.field public static final DETAIL_PARAM:Ljava/lang/String; = "detailparam"

.field public static final FEED_ID:Ljava/lang/String; = "feedkey"

.field public static final HOST_OPENID:Ljava/lang/String; = "hostopenid"

.field public static final ICON:Ljava/lang/String; = "portraiturl"

.field public static final LIKE_COUNT:Ljava/lang/String; = "likecount"

.field public static final MESSAGE:Ljava/lang/String; = "summary"

.field public static final NAME:Ljava/lang/String; = "nickname"

.field public static final OPENID:Ljava/lang/String; = "openid"

.field public static final ORIGINAL:Ljava/lang/String; = "original"

.field public static final ORIGINAL_NAME:Ljava/lang/String; = "originalnickname"

.field public static final ORIGINAL_SUMMARY:Ljava/lang/String; = "originalsummary"

.field public static final ORIGINAL_TITLE:Ljava/lang/String; = "originaltitle"

.field public static final PICTURE:Ljava/lang/String; = "pictureurl"

.field public static final PICTUREURL_ORIGIN:Ljava/lang/String; = "pictureurl_origin"

.field public static final SHARE_FLAG:Ljava/lang/String; = "shareflag"

.field public static final STATUSES:Ljava/lang/String; = "feeds"

.field public static final TITLE:Ljava/lang/String; = "title"

.field public static final TYPE_MOOD:Ljava/lang/String; = "mood"

.field public static final TYPE_SHARE:Ljava/lang/String; = "share"
