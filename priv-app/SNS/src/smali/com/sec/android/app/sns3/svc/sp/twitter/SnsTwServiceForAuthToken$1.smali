.class Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwServiceForAuthToken$1;
.super Lcom/sec/android/app/sns3/svc/sp/twitter/auth/api/ISnsTwitterForAuthToken$Stub;
.source "SnsTwServiceForAuthToken.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwServiceForAuthToken;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwServiceForAuthToken;


# direct methods
.method constructor <init>(Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwServiceForAuthToken;)V
    .locals 0

    .prologue
    .line 84
    iput-object p1, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwServiceForAuthToken$1;->this$0:Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwServiceForAuthToken;

    invoke-direct {p0}, Lcom/sec/android/app/sns3/svc/sp/twitter/auth/api/ISnsTwitterForAuthToken$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public getAuthTokenInfo()Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 98
    const-string v2, "SNS"

    const-string v3, "SnsTwServiceForAuthToken : getAuthTokenNExpires() CALLED !!"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 100
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 102
    .local v0, "Info":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v2, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwServiceForAuthToken$1;->this$0:Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwServiceForAuthToken;

    # getter for: Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwServiceForAuthToken;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    invoke-static {v2}, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwServiceForAuthToken;->access$000(Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwServiceForAuthToken;)Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/sns3/svc/SnsSvcMgr;->getTokenMgr()Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;

    move-result-object v2

    const-string v3, "twitter"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;->getToken(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;

    .line 104
    .local v1, "twToken":Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;
    const-string v2, "consumer_key"

    invoke-static {}, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwAppIdManager;->getInstance()Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwAppIdManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwAppIdManager;->getConsumerKey()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 105
    const-string v2, "consumer_secret"

    invoke-static {}, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwAppIdManager;->getInstance()Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwAppIdManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwAppIdManager;->getConsumerSecret()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 106
    const-string v2, "access_token"

    invoke-virtual {v1}, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;->getAccessToken()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 107
    const-string v2, "access_token_secret"

    invoke-virtual {v1}, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;->getAccessTokenSecret()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 108
    const-string v2, "user_id"

    invoke-virtual {v1}, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;->getUserID()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 109
    const-string v2, "screen_name"

    invoke-virtual {v1}, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;->getScreenName()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 111
    return-object v0
.end method

.method public getMyAccountInfo(Lcom/sec/android/app/sns3/svc/sp/twitter/auth/api/ISnsTwitterCallbackMyAccountInfo;)I
    .locals 8
    .param p1, "cb"    # Lcom/sec/android/app/sns3/svc/sp/twitter/auth/api/ISnsTwitterCallbackMyAccountInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 118
    const-string v3, "SNS"

    const-string v4, "getMyAccountInfo CALLED!!!"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 120
    new-instance v1, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwServiceForAuthToken$1$1;

    iget-object v3, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwServiceForAuthToken$1;->this$0:Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwServiceForAuthToken;

    # getter for: Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwServiceForAuthToken;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    invoke-static {v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwServiceForAuthToken;->access$000(Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwServiceForAuthToken;)Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v3

    invoke-direct {v1, p0, v3, p1}, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwServiceForAuthToken$1$1;-><init>(Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwServiceForAuthToken$1;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Lcom/sec/android/app/sns3/svc/sp/twitter/auth/api/ISnsTwitterCallbackMyAccountInfo;)V

    .line 135
    .local v1, "req":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    invoke-virtual {v1}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->request()Z

    move-result v0

    .line 136
    .local v0, "isRequested":Z
    if-nez v0, :cond_0

    .line 137
    invoke-virtual {v1}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->getReqID()I

    move-result v2

    .line 138
    .local v2, "reqId":I
    iget-object v3, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwServiceForAuthToken$1;->this$0:Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwServiceForAuthToken;

    # getter for: Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwServiceForAuthToken;->mHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwServiceForAuthToken;->access$100(Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwServiceForAuthToken;)Landroid/os/Handler;

    move-result-object v3

    new-instance v4, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwServiceForAuthToken$1$2;

    invoke-direct {v4, p0, p1, v2}, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwServiceForAuthToken$1$2;-><init>(Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwServiceForAuthToken$1;Lcom/sec/android/app/sns3/svc/sp/twitter/auth/api/ISnsTwitterCallbackMyAccountInfo;I)V

    const-wide/16 v6, 0x64

    invoke-virtual {v3, v4, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 153
    .end local v2    # "reqId":I
    :cond_0
    invoke-virtual {v1}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->getReqID()I

    move-result v3

    return v3
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 2
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 89
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->hasAccessToRestrictedData(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 90
    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "Access denied"

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 92
    :cond_0
    invoke-super {p0, p1, p2, p3, p4}, Lcom/sec/android/app/sns3/svc/sp/twitter/auth/api/ISnsTwitterForAuthToken$Stub;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v0

    return v0
.end method
