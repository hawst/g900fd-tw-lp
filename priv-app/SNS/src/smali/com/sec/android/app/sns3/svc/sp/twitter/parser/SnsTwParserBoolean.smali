.class public Lcom/sec/android/app/sns3/svc/sp/twitter/parser/SnsTwParserBoolean;
.super Ljava/lang/Object;
.source "SnsTwParserBoolean.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseBoolean;
    .locals 2
    .param p0, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 26
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseBoolean;

    invoke-direct {v0}, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseBoolean;-><init>()V

    .line 28
    .local v0, "bool":Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseBoolean;
    if-eqz p0, :cond_0

    .line 29
    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    iput-boolean v1, v0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseBoolean;->mBeSuccess:Z

    .line 32
    :cond_0
    return-object v0
.end method
