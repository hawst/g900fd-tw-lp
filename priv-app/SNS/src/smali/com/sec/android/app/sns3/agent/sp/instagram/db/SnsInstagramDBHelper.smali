.class public Lcom/sec/android/app/sns3/agent/sp/instagram/db/SnsInstagramDBHelper;
.super Lcom/sec/android/app/sns3/agent/db/SnsDBHelperBase;
.source "SnsInstagramDBHelper.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 32
    const-string v0, "snsInstagramDB.db"

    const/4 v1, 0x0

    const/4 v2, 0x2

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/sec/android/app/sns3/agent/db/SnsDBHelperBase;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 33
    return-void
.end method


# virtual methods
.method public createTables(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 51
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 53
    const-string v0, "CREATE TABLE IF NOT EXISTS user_basic_info (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,user_id VARCHAR(50),user_name TEXT, UNIQUE (user_id));"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 59
    const-string v0, "SnsAgent"

    const-string v1, "DB TABLE(user_basic_info) Has been created."

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 62
    const-string v0, "CREATE TABLE IF NOT EXISTS user_profile_info (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,profile_id VARCHAR(50),profile_name TEXT,profile_image TEXT,profile_website TEXT,full_name TEXT,profile_bio TEXT, UNIQUE (profile_id));"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 72
    const-string v0, "SnsAgent"

    const-string v1, "DB TABLE(user_basic_info) Has been created."

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 75
    const-string v0, "CREATE TABLE IF NOT EXISTS user_feed_info (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,post_id VARCHAR(50),message TEXT,timestamp_utc TIMESTAMP,object_type TEXT,media_url TEXT,link TEXT,location_name TEXT,latitude DOUBLE,longitude DOUBLE,width INTEGER,height INTEGER, UNIQUE (post_id));"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 90
    const-string v0, "SnsAgent"

    const-string v1, "DB TABLE(user_feed_info) Has been created."

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 93
    const-string v0, "CREATE TABLE IF NOT EXISTS sync_album (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,id VARCHAR(50),name TEXT,from_name TEXT,from_id VARCHAR(50),description TEXT,link TEXT,location DOUBLE,cover_photo TEXT,privacy TEXT,count INTEGER,created_time TIMESTAMP,updated_time TIMESTAMP,type TEXT, UNIQUE (id));"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 110
    const-string v0, "SnsAgent"

    const-string v1, "DB TABLE(sync_album) Has been created."

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 113
    const-string v0, "CREATE TABLE IF NOT EXISTS sync_photo (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,id VARCHAR(50),target_id VARCHAR(50),from_name TEXT,from_id VARCHAR(50),name TEXT,picture TEXT,source TEXT,height INTEGER,width INTEGER,link TEXT,icon TEXT,position INTEGER,created_time TIMESTAMP,updated_time TIMESTAMP,image_width INTEGER,image_height INTEGER,url TEXT,location_name TEXT,latitude DOUBLE,longitude DOUBLE, UNIQUE (id));"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 137
    const-string v0, "SnsAgent"

    const-string v1, "DB TABLE(sync_photo) Has been created."

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 140
    const-string v0, "CREATE TABLE IF NOT EXISTS user_info (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,profile_id VARCHAR(50),profile_name TEXT,profile_image TEXT,profile_website TEXT,full_name TEXT,profile_bio TEXT, UNIQUE (profile_id));"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 150
    const-string v0, "SnsAgent"

    const-string v1, "DB TABLE(user_info) Has been created."

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 153
    const-string v0, "CREATE TABLE IF NOT EXISTS status_stream (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,post_id VARCHAR(50),message TEXT,from_id VARCHAR(50),from_name TEXT,likes_count INTEGER,comments_count INTEGER,timestamp_utc TIMESTAMP,object_type TEXT,media_url TEXT,link TEXT,location_name TEXT,latitude DOUBLE,longitude DOUBLE,width INTEGER,height INTEGER, UNIQUE (post_id));"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 172
    const-string v0, "SnsAgent"

    const-string v1, "DB TABLE(status_stream) Has been created."

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 175
    const-string v0, "CREATE TABLE IF NOT EXISTS friends_profile_info (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,profile_id VARCHAR(50),profile_name TEXT,profile_image TEXT,profile_website TEXT,full_name TEXT,profile_bio TEXT, UNIQUE (profile_id));"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 187
    const-string v0, "SnsAgent"

    const-string v1, "DB TABLE(friends_profile_info) Has been created."

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 190
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 191
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 192
    return-void
.end method

.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 0
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 37
    invoke-super {p0, p1}, Lcom/sec/android/app/sns3/agent/db/SnsDBHelperBase;->onCreate(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 39
    invoke-virtual {p0, p1}, Lcom/sec/android/app/sns3/agent/sp/instagram/db/SnsInstagramDBHelper;->createTables(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 40
    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 0
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "oldVersion"    # I
    .param p3, "newVersion"    # I

    .prologue
    .line 44
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/app/sns3/agent/db/SnsDBHelperBase;->onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V

    .line 46
    invoke-virtual {p0, p1}, Lcom/sec/android/app/sns3/agent/sp/instagram/db/SnsInstagramDBHelper;->upgradeTables(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 48
    return-void
.end method

.method public upgradeTables(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 197
    const-string v0, "DROP TABLE IF EXISTS user_basic_info"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 198
    const-string v0, "DROP TABLE IF EXISTS user_profile_info"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 199
    const-string v0, "DROP TABLE IF EXISTS user_feed_info"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 200
    const-string v0, "DROP TABLE IF EXISTS sync_album"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 201
    const-string v0, "DROP TABLE IF EXISTS sync_photo"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 202
    const-string v0, "DROP TABLE IF EXISTS user_info"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 203
    const-string v0, "DROP TABLE IF EXISTS status_stream"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 204
    const-string v0, "DROP TABLE IF EXISTS friends_profile_info"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 206
    invoke-virtual {p0, p1}, Lcom/sec/android/app/sns3/agent/sp/instagram/db/SnsInstagramDBHelper;->createTables(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 207
    return-void
.end method

.method public wipeData(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 210
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 212
    :try_start_0
    const-string v1, "DELETE FROM user_basic_info"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 213
    const-string v1, "DELETE FROM user_profile_info"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 214
    const-string v1, "DELETE FROM user_feed_info"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 215
    const-string v1, "DELETE FROM sync_album"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 216
    const-string v1, "DELETE FROM sync_photo"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 217
    const-string v1, "DELETE FROM user_info"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 218
    const-string v1, "DELETE FROM status_stream"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 219
    const-string v1, "DELETE FROM friends_profile_info"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 221
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 226
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 228
    :goto_0
    return-void

    .line 222
    :catch_0
    move-exception v0

    .line 223
    .local v0, "e":Landroid/database/sqlite/SQLiteException;
    :try_start_1
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 226
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    goto :goto_0

    .end local v0    # "e":Landroid/database/sqlite/SQLiteException;
    :catchall_0
    move-exception v1

    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v1
.end method
