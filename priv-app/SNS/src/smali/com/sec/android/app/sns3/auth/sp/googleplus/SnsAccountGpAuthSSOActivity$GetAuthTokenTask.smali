.class Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity$GetAuthTokenTask;
.super Landroid/os/AsyncTask;
.source "SnsAccountGpAuthSSOActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GetAuthTokenTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;)V
    .locals 0

    .prologue
    .line 300
    iput-object p1, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity$GetAuthTokenTask;->this$0:Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;
    .param p2, "x1"    # Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity$1;

    .prologue
    .line 300
    invoke-direct {p0, p1}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity$GetAuthTokenTask;-><init>(Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 300
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity$GetAuthTokenTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 18
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    .line 309
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity$GetAuthTokenTask;->this$0:Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;

    # getter for: Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->mConsentPageShown:Z
    invoke-static {v2}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->access$200(Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;)Z

    move-result v2

    if-nez v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity$GetAuthTokenTask;->this$0:Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;

    # getter for: Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->mGoogleAccountCredential:Landroid/accounts/AccountManagerFuture;
    invoke-static {v2}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->access$500(Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;)Landroid/accounts/AccountManagerFuture;

    move-result-object v2

    if-nez v2, :cond_1

    .line 310
    :cond_0
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 311
    .local v5, "param":Landroid/os/Bundle;
    const-string v2, "suppressProgressScreen"

    const/4 v3, 0x1

    invoke-virtual {v5, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 313
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity$GetAuthTokenTask;->this$0:Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity$GetAuthTokenTask;->this$0:Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;

    # getter for: Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->mAppContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->access$700(Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity$GetAuthTokenTask;->this$0:Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;

    # getter for: Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->mSelectedAccount:Landroid/accounts/Account;
    invoke-static {v3}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->access$600(Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;)Landroid/accounts/Account;

    move-result-object v3

    const-string v4, "oauth2:https://www.googleapis.com/auth/plus.login https://www.googleapis.com/auth/plus.me https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/userinfo.profile"

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v2 .. v8}, Landroid/accounts/AccountManager;->getAuthToken(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;ZLandroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    move-result-object v2

    move-object/from16 v0, v17

    # setter for: Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->mGoogleAccountCredential:Landroid/accounts/AccountManagerFuture;
    invoke-static {v0, v2}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->access$502(Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;Landroid/accounts/AccountManagerFuture;)Landroid/accounts/AccountManagerFuture;

    .line 317
    .end local v5    # "param":Landroid/os/Bundle;
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v14

    .line 318
    .local v14, "startTime":J
    const/16 v16, 0x0

    .line 320
    .local v16, "token":Ljava/lang/String;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity$GetAuthTokenTask;->this$0:Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;

    # getter for: Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->mGoogleAccountCredential:Landroid/accounts/AccountManagerFuture;
    invoke-static {v2}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->access$500(Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;)Landroid/accounts/AccountManagerFuture;

    move-result-object v2

    const-wide/16 v6, 0x7530

    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v2, v6, v7, v3}, Landroid/accounts/AccountManagerFuture;->getResult(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/os/Bundle;

    .line 323
    .local v9, "bundle":Landroid/os/Bundle;
    if-eqz v9, :cond_5

    .line 324
    const-string v2, "errorCode"

    invoke-virtual {v9, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v11

    .line 325
    .local v11, "errorCode":I
    const-string v2, "errorMessage"

    invoke-virtual {v9, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 326
    .local v12, "errorMsg":Ljava/lang/String;
    const-string v2, "intent"

    invoke-virtual {v9, v2}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Landroid/content/Intent;

    .line 327
    .local v13, "intent":Landroid/content/Intent;
    const-string v2, "SnsAccountGpAuthSSOActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "GetAuthTokenTask : errorCode = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", errorMsg = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 330
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity$GetAuthTokenTask;->this$0:Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;

    # getter for: Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->mConsentPageShown:Z
    invoke-static {v2}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->access$200(Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;)Z

    move-result v2

    if-nez v2, :cond_2

    if-eqz v13, :cond_2

    .line 332
    const-string v2, "SnsAccountGpAuthSSOActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "GetAuthTokenTask : resolve "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 333
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity$GetAuthTokenTask;->this$0:Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;

    const/4 v3, 0x1

    # setter for: Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->mConsentPageShown:Z
    invoke-static {v2, v3}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->access$202(Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;Z)Z

    .line 334
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity$GetAuthTokenTask;->this$0:Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;

    const/16 v3, 0x3ea

    invoke-virtual {v2, v13, v3}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 335
    const/4 v2, 0x0

    .line 373
    .end local v9    # "bundle":Landroid/os/Bundle;
    .end local v11    # "errorCode":I
    .end local v12    # "errorMsg":Ljava/lang/String;
    .end local v13    # "intent":Landroid/content/Intent;
    :goto_0
    return-object v2

    .line 337
    .restart local v9    # "bundle":Landroid/os/Bundle;
    .restart local v11    # "errorCode":I
    .restart local v12    # "errorMsg":Ljava/lang/String;
    .restart local v13    # "intent":Landroid/content/Intent;
    :cond_2
    const-string v2, "authtoken"

    invoke-virtual {v9, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 338
    const-string v3, "SnsAccountGpAuthSSOActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "GetAuthTokenTask : getAuthToken() "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    if-eqz v16, :cond_3

    const/4 v2, 0x1

    :goto_1
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 341
    if-eqz v16, :cond_4

    .line 342
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity$GetAuthTokenTask;->this$0:Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity$GetAuthTokenTask;->this$0:Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;

    # getter for: Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->mSelectedAccount:Landroid/accounts/Account;
    invoke-static {v3}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->access$600(Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;)Landroid/accounts/Account;

    move-result-object v3

    iget-object v3, v3, Landroid/accounts/Account;->name:Ljava/lang/String;

    move-object/from16 v0, v16

    move-object/from16 v1, v16

    # invokes: Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->setAuthTokenNExpires(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    invoke-static {v2, v0, v1, v3}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->access$800(Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    .line 343
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity$GetAuthTokenTask;->this$0:Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;

    # invokes: Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->getUserInfo()V
    invoke-static {v2}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->access$900(Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;)V

    .line 344
    const/4 v2, 0x0

    goto :goto_0

    .line 338
    :cond_3
    const/4 v2, 0x0

    goto :goto_1

    .line 345
    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity$GetAuthTokenTask;->this$0:Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;

    # getter for: Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->mConsentPageShown:Z
    invoke-static {v2}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->access$200(Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 346
    const-string v2, "SnsAccountGpAuthSSOActivity"

    const-string v3, "consent page canceled"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 347
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity$GetAuthTokenTask;->this$0:Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->finish()V

    .line 348
    const/4 v2, 0x0

    goto :goto_0

    .line 352
    .end local v11    # "errorCode":I
    .end local v12    # "errorMsg":Ljava/lang/String;
    .end local v13    # "intent":Landroid/content/Intent;
    :cond_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity$GetAuthTokenTask;->this$0:Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;

    # invokes: Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->loginFail()V
    invoke-static {v2}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->access$1000(Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;)V
    :try_end_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    .line 373
    .end local v9    # "bundle":Landroid/os/Bundle;
    :goto_2
    const/4 v2, 0x0

    goto :goto_0

    .line 353
    :catch_0
    move-exception v10

    .line 354
    .local v10, "e":Landroid/accounts/OperationCanceledException;
    const-string v2, "SnsAccountGpAuthSSOActivity"

    const-string v3, "OperationCanceledException occurred"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 355
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sub-long/2addr v2, v14

    const-wide/16 v6, 0x7530

    cmp-long v2, v2, v6

    if-ltz v2, :cond_7

    .line 356
    const-string v2, "SnsAccountGpAuthSSOActivity"

    const-string v3, "getAuthToken time over!"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 357
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity$GetAuthTokenTask;->this$0:Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->isResumed()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 358
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity$GetAuthTokenTask;->this$0:Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;

    # invokes: Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->loginFail()V
    invoke-static {v2}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->access$1000(Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;)V

    goto :goto_2

    .line 360
    :cond_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity$GetAuthTokenTask;->this$0:Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->finish()V

    goto :goto_2

    .line 363
    :cond_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity$GetAuthTokenTask;->this$0:Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->finish()V

    goto :goto_2

    .line 365
    .end local v10    # "e":Landroid/accounts/OperationCanceledException;
    :catch_1
    move-exception v10

    .line 366
    .local v10, "e":Landroid/accounts/AuthenticatorException;
    invoke-virtual {v10}, Landroid/accounts/AuthenticatorException;->printStackTrace()V

    .line 367
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity$GetAuthTokenTask;->this$0:Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;

    # invokes: Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->loginFail()V
    invoke-static {v2}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->access$1000(Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;)V

    goto :goto_2

    .line 368
    .end local v10    # "e":Landroid/accounts/AuthenticatorException;
    :catch_2
    move-exception v10

    .line 369
    .local v10, "e":Ljava/io/IOException;
    invoke-virtual {v10}, Ljava/io/IOException;->printStackTrace()V

    .line 370
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity$GetAuthTokenTask;->this$0:Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;

    # invokes: Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->loginFail()V
    invoke-static {v2}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->access$1000(Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;)V

    goto :goto_2
.end method

.method protected onPreExecute()V
    .locals 3

    .prologue
    .line 304
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity$GetAuthTokenTask;->this$0:Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;

    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity$GetAuthTokenTask;->this$0:Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;

    const v2, 0x7f08002f

    invoke-virtual {v1, v2}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    # invokes: Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->showProgressDialog(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->access$400(Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;Ljava/lang/String;)V

    .line 305
    return-void
.end method
