.class Lcom/sec/android/app/sns3/agent/sp/googleplus/command/SnsGpCmdGetFeed$1;
.super Lcom/sec/android/app/sns3/svc/sp/googleplus/request/SnsGpReqGetFeed;
.source "SnsGpCmdGetFeed.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/sns3/agent/sp/googleplus/command/SnsGpCmdGetFeed;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Handler;Ljava/lang/String;Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/sns3/agent/sp/googleplus/command/SnsGpCmdGetFeed;


# direct methods
.method constructor <init>(Lcom/sec/android/app/sns3/agent/sp/googleplus/command/SnsGpCmdGetFeed;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 0
    .param p2, "x0"    # Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    .param p3, "x1"    # Ljava/lang/String;
    .param p4, "x2"    # Landroid/os/Bundle;

    .prologue
    .line 51
    iput-object p1, p0, Lcom/sec/android/app/sns3/agent/sp/googleplus/command/SnsGpCmdGetFeed$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/googleplus/command/SnsGpCmdGetFeed;

    invoke-direct {p0, p2, p3, p4}, Lcom/sec/android/app/sns3/svc/sp/googleplus/request/SnsGpReqGetFeed;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;Landroid/os/Bundle;)V

    return-void
.end method


# virtual methods
.method public onReqRespond(IZIILandroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;)Z
    .locals 9
    .param p1, "reqID"    # I
    .param p2, "bSuccess"    # Z
    .param p3, "httpStatus"    # I
    .param p4, "errorCode"    # I
    .param p5, "reason"    # Landroid/os/Bundle;
    .param p6, "feeds"    # Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;

    .prologue
    const/4 v8, 0x1

    .line 56
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/sns3/SnsApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 58
    .local v1, "cr":Landroid/content/ContentResolver;
    if-eqz p2, :cond_1

    .line 59
    if-eqz p6, :cond_0

    .line 60
    move-object v2, p6

    .line 61
    .local v2, "curfeed":Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;
    sget-object v3, Lcom/sec/android/app/sns3/agent/sp/googleplus/db/SnsGooglePlusDB$StatusStream;->CONTENT_URI:Landroid/net/Uri;

    const-string v4, "from_id=?"

    new-array v5, v8, [Ljava/lang/String;

    const/4 v6, 0x0

    iget-object v7, v2, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;->mFromId:Ljava/lang/String;

    aput-object v7, v5, v6

    invoke-virtual {v1, v3, v4, v5}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 65
    :goto_0
    if-eqz v2, :cond_0

    .line 67
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 68
    .local v0, "contentValues":Landroid/content/ContentValues;
    invoke-virtual {v0}, Landroid/content/ContentValues;->clear()V

    .line 69
    const-string v3, "post_id"

    iget-object v4, v2, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;->mPostId:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    const-string v3, "from_id"

    iget-object v4, v2, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;->mFromId:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    const-string v3, "from_name"

    iget-object v4, v2, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;->mFromName:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    const-string v3, "message"

    iget-object v4, v2, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;->mTitle:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    const-string v3, "likes_count"

    iget-object v4, v2, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;->mLikesCount:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    const-string v3, "comments_count"

    iget-object v4, v2, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;->mCommentsCount:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    const-string v3, "reshares_count"

    iget-object v4, v2, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;->mResharesCount:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    const-string v3, "timestamp_utc"

    iget-object v4, v2, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;->mUpdateTime:Ljava/lang/String;

    invoke-static {v4}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->toTimestampForGooglePlus(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 78
    const-string v3, "object_type"

    iget-object v4, v2, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;->mObjectType:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    const-string v3, "media_url"

    iget-object v4, v2, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;->mMediaUrl:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    const-string v3, "link"

    iget-object v4, v2, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;->mContentUrl:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    const-string v3, "location_name"

    iget-object v4, v2, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;->mLocationName:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    const-string v3, "latitude"

    iget-object v4, v2, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;->mLatitude:Ljava/lang/Double;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 83
    const-string v3, "longitude"

    iget-object v4, v2, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;->mLongitude:Ljava/lang/Double;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 84
    const-string v3, "width"

    iget-object v4, v2, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;->mWidth:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    const-string v3, "height"

    iget-object v4, v2, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;->mHeight:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    sget-object v3, Lcom/sec/android/app/sns3/agent/sp/googleplus/db/SnsGooglePlusDB$StatusStream;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1, v3, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 89
    iget-object v2, v2, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;->mNext:Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;

    .line 90
    goto/16 :goto_0

    .line 92
    .end local v0    # "contentValues":Landroid/content/ContentValues;
    .end local v2    # "curfeed":Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseFeed;
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/sns3/agent/sp/googleplus/command/SnsGpCmdGetFeed$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/googleplus/command/SnsGpCmdGetFeed;

    sget-object v4, Lcom/sec/android/app/sns3/agent/sp/googleplus/db/SnsGooglePlusDB$StatusStream;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/sns3/agent/sp/googleplus/command/SnsGpCmdGetFeed;->setUri(Ljava/lang/String;)V

    .line 99
    :goto_1
    iget-object v3, p0, Lcom/sec/android/app/sns3/agent/sp/googleplus/command/SnsGpCmdGetFeed$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/googleplus/command/SnsGpCmdGetFeed;

    invoke-virtual {v3, p2}, Lcom/sec/android/app/sns3/agent/sp/googleplus/command/SnsGpCmdGetFeed;->setSuccess(Z)V

    .line 100
    iget-object v3, p0, Lcom/sec/android/app/sns3/agent/sp/googleplus/command/SnsGpCmdGetFeed$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/googleplus/command/SnsGpCmdGetFeed;

    invoke-virtual {v3, p0}, Lcom/sec/android/app/sns3/agent/sp/googleplus/command/SnsGpCmdGetFeed;->receive(Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;)V

    .line 102
    return v8

    .line 94
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/sns3/agent/sp/googleplus/command/SnsGpCmdGetFeed$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/googleplus/command/SnsGpCmdGetFeed;

    new-instance v4, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;

    const-string v5, "googleplus"

    invoke-direct {v4, v5, p3, p4, p5}, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;-><init>(Ljava/lang/String;IILandroid/os/Bundle;)V

    invoke-virtual {v3, v4}, Lcom/sec/android/app/sns3/agent/sp/googleplus/command/SnsGpCmdGetFeed;->setResponseList(Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;)Z

    .line 96
    iget-object v3, p0, Lcom/sec/android/app/sns3/agent/sp/googleplus/command/SnsGpCmdGetFeed$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/googleplus/command/SnsGpCmdGetFeed;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/sec/android/app/sns3/agent/sp/googleplus/command/SnsGpCmdGetFeed;->setUri(Ljava/lang/String;)V

    goto :goto_1
.end method
