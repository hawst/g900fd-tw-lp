.class public interface abstract Lcom/sec/android/app/sns3/agent/db/SnsSvcDB$SSOAccountColumns;
.super Ljava/lang/Object;
.source "SnsSvcDB.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/agent/db/SnsSvcDB;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "SSOAccountColumns"
.end annotation


# static fields
.field public static final ACCOUNT_TYPE:Ljava/lang/String; = "account_type"

.field public static final SERVICE_APP_NAME:Ljava/lang/String; = "service_app_name"

.field public static final SSO_ACCOUNT_LABEL:Ljava/lang/String; = "sso_account_label"

.field public static final SSO_ACCOUNT_TYPE:Ljava/lang/String; = "sso_account_type"

.field public static final SSO_RETRY_ACTION:Ljava/lang/String; = "sso_retry_action"
