.class public Lcom/sec/android/app/sns3/agent/sp/common/command/SnsCoCmdPostContents;
.super Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;
.source "SnsCoCmdPostContents.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/agent/sp/common/command/SnsCoCmdPostContents$ParamKey;
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Handler;Landroid/os/Bundle;)V
    .locals 12
    .param p1, "svcMgr"    # Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    .param p2, "cmdHandler"    # Landroid/os/Handler;
    .param p3, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 51
    invoke-direct {p0, p2}, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;-><init>(Landroid/os/Handler;)V

    .line 52
    new-instance v8, Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;

    invoke-direct {v8}, Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;-><init>()V

    .line 54
    .local v8, "unit":Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;
    const-string v9, "type_list"

    invoke-virtual {p3, v9}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v7

    .line 56
    .local v7, "typeList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-eqz v7, :cond_1c

    .line 57
    const/4 v4, 0x0

    .line 58
    .local v4, "message":Ljava/lang/String;
    const/4 v1, 0x0

    .line 59
    .local v1, "fileName":Ljava/lang/String;
    const/4 v2, 0x0

    .line 60
    .local v2, "latitude":Ljava/lang/String;
    const/4 v3, 0x0

    .line 62
    .local v3, "logitude":Ljava/lang/String;
    const-string v9, "message"

    invoke-virtual {p3, v9}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 63
    const-string v9, "message"

    invoke-virtual {p3, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 65
    :cond_0
    const-string v9, "filename"

    invoke-virtual {p3, v9}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 66
    const-string v9, "filename"

    invoke-virtual {p3, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 68
    :cond_1
    const-string v9, "latitude"

    invoke-virtual {p3, v9}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_2

    const-string v9, "logitude"

    invoke-virtual {p3, v9}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 69
    const-string v9, "latitude"

    invoke-virtual {p3, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 70
    const-string v9, "logitude"

    invoke-virtual {p3, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 75
    :cond_2
    const-string v9, "fb_type_post"

    invoke-virtual {v7, v9}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 77
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 78
    .local v0, "fbBundle":Landroid/os/Bundle;
    const/4 v5, 0x0

    .line 80
    .local v5, "objectID":Ljava/lang/String;
    const-string v9, "fb_object_id"

    invoke-virtual {p3, v9}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 81
    const-string v9, "fb_object_id"

    invoke-virtual {p3, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 83
    :cond_3
    if-eqz v4, :cond_4

    .line 84
    const-string v9, "message"

    invoke-virtual {v0, v9, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    :cond_4
    const-string v9, "fb_privacy"

    invoke-virtual {p3, v9}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 89
    const-string v9, "privacy"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "{\"value\":\""

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "fb_privacy"

    invoke-virtual {p3, v11}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "\"}"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    :cond_5
    new-instance v9, Lcom/sec/android/app/sns3/agent/sp/common/command/SnsCoCmdPostContents$1;

    invoke-direct {v9, p0, p1, v5, v0}, Lcom/sec/android/app/sns3/agent/sp/common/command/SnsCoCmdPostContents$1;-><init>(Lcom/sec/android/app/sns3/agent/sp/common/command/SnsCoCmdPostContents;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;Landroid/os/Bundle;)V

    invoke-virtual {v8, v9}, Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;->addRequest(Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;)V

    .line 111
    .end local v0    # "fbBundle":Landroid/os/Bundle;
    .end local v5    # "objectID":Ljava/lang/String;
    :cond_6
    const-string v9, "fb_type_checkin"

    invoke-virtual {v7, v9}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_b

    .line 113
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 114
    .restart local v0    # "fbBundle":Landroid/os/Bundle;
    const/4 v5, 0x0

    .line 116
    .restart local v5    # "objectID":Ljava/lang/String;
    const-string v9, "fb_object_id"

    invoke-virtual {p3, v9}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_7

    .line 117
    const-string v9, "fb_object_id"

    invoke-virtual {p3, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 119
    :cond_7
    if-eqz v4, :cond_8

    .line 120
    const-string v9, "message"

    invoke-virtual {v0, v9, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    :cond_8
    const-string v9, "fb_place"

    invoke-virtual {p3, v9}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_9

    .line 123
    const-string v9, "place"

    const-string v10, "fb_place"

    invoke-virtual {p3, v10}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 126
    :cond_9
    if-eqz v2, :cond_a

    if-eqz v3, :cond_a

    .line 127
    const-string v9, "coordinates"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "{\"latitude\": \""

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "\", \"longitude\": \""

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "\"}"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    :cond_a
    new-instance v9, Lcom/sec/android/app/sns3/agent/sp/common/command/SnsCoCmdPostContents$2;

    invoke-direct {v9, p0, p1, v5, v0}, Lcom/sec/android/app/sns3/agent/sp/common/command/SnsCoCmdPostContents$2;-><init>(Lcom/sec/android/app/sns3/agent/sp/common/command/SnsCoCmdPostContents;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;Landroid/os/Bundle;)V

    invoke-virtual {v8, v9}, Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;->addRequest(Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;)V

    .line 149
    .end local v0    # "fbBundle":Landroid/os/Bundle;
    .end local v5    # "objectID":Ljava/lang/String;
    :cond_b
    const-string v9, "fb_type_photo"

    invoke-virtual {v7, v9}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_f

    .line 151
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 152
    .restart local v0    # "fbBundle":Landroid/os/Bundle;
    const/4 v5, 0x0

    .line 154
    .restart local v5    # "objectID":Ljava/lang/String;
    const-string v9, "fb_object_id"

    invoke-virtual {p3, v9}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_c

    .line 155
    const-string v9, "fb_object_id"

    invoke-virtual {p3, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 157
    :cond_c
    if-eqz v4, :cond_d

    .line 158
    const-string v9, "message"

    invoke-virtual {v0, v9, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    :cond_d
    if-eqz v1, :cond_e

    const-string v9, "fb_photo"

    invoke-virtual {p3, v9}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_e

    .line 161
    const-string v9, "filename"

    invoke-virtual {v0, v9, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 162
    const-string v9, "source"

    const-string v10, "fb_photo"

    invoke-virtual {p3, v10}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 163
    const-string v9, "Content-Type"

    const-string v10, "multipart/form-data"

    invoke-virtual {v0, v9, v10}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 166
    :cond_e
    new-instance v9, Lcom/sec/android/app/sns3/agent/sp/common/command/SnsCoCmdPostContents$3;

    invoke-direct {v9, p0, p1, v5, v0}, Lcom/sec/android/app/sns3/agent/sp/common/command/SnsCoCmdPostContents$3;-><init>(Lcom/sec/android/app/sns3/agent/sp/common/command/SnsCoCmdPostContents;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;Landroid/os/Bundle;)V

    invoke-virtual {v8, v9}, Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;->addRequest(Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;)V

    .line 181
    .end local v0    # "fbBundle":Landroid/os/Bundle;
    .end local v5    # "objectID":Ljava/lang/String;
    :cond_f
    const-string v9, "fb_type_video"

    invoke-virtual {v7, v9}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_13

    .line 183
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 184
    .restart local v0    # "fbBundle":Landroid/os/Bundle;
    const/4 v5, 0x0

    .line 186
    .restart local v5    # "objectID":Ljava/lang/String;
    const-string v9, "fb_object_id"

    invoke-virtual {p3, v9}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_10

    .line 187
    const-string v9, "fb_object_id"

    invoke-virtual {p3, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 189
    :cond_10
    if-eqz v4, :cond_11

    .line 190
    const-string v9, "message"

    invoke-virtual {v0, v9, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 192
    :cond_11
    if-eqz v1, :cond_12

    const-string v9, "fb_video"

    invoke-virtual {p3, v9}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_12

    .line 193
    const-string v9, "filename"

    invoke-virtual {v0, v9, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 194
    const-string v9, "source"

    const-string v10, "fb_video"

    invoke-virtual {p3, v10}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 195
    const-string v9, "Content-Type"

    const-string v10, "multipart/form-data"

    invoke-virtual {v0, v9, v10}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 198
    :cond_12
    new-instance v9, Lcom/sec/android/app/sns3/agent/sp/common/command/SnsCoCmdPostContents$4;

    invoke-direct {v9, p0, p1, v5, v0}, Lcom/sec/android/app/sns3/agent/sp/common/command/SnsCoCmdPostContents$4;-><init>(Lcom/sec/android/app/sns3/agent/sp/common/command/SnsCoCmdPostContents;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;Landroid/os/Bundle;)V

    invoke-virtual {v8, v9}, Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;->addRequest(Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;)V

    .line 214
    .end local v0    # "fbBundle":Landroid/os/Bundle;
    .end local v5    # "objectID":Ljava/lang/String;
    :cond_13
    const-string v9, "tw_type_status"

    invoke-virtual {v7, v9}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_17

    .line 215
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 217
    .local v6, "twBundle":Landroid/os/Bundle;
    if-eqz v4, :cond_14

    .line 218
    sget-object v9, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;->STATUS:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;

    invoke-virtual {v9}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;->getParam()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v9, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 220
    :cond_14
    if-eqz v2, :cond_15

    if-eqz v3, :cond_15

    .line 221
    sget-object v9, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;->LAT:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;

    invoke-virtual {v9}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;->getParam()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v9, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    sget-object v9, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;->LONG:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;

    invoke-virtual {v9}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;->getParam()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v9, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 225
    :cond_15
    const-string v9, "tw_status_id"

    invoke-virtual {p3, v9}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_16

    .line 226
    sget-object v9, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;->IN_REPLY_TO_STATUS_ID:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;

    invoke-virtual {v9}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;->getParam()Ljava/lang/String;

    move-result-object v9

    const-string v10, "tw_status_id"

    invoke-virtual {p3, v10}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v6, v9, v10}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 229
    :cond_16
    new-instance v9, Lcom/sec/android/app/sns3/agent/sp/common/command/SnsCoCmdPostContents$5;

    sget-object v10, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;->STATUSES_UPDATE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;

    invoke-direct {v9, p0, p1, v10, v6}, Lcom/sec/android/app/sns3/agent/sp/common/command/SnsCoCmdPostContents$5;-><init>(Lcom/sec/android/app/sns3/agent/sp/common/command/SnsCoCmdPostContents;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;Landroid/os/Bundle;)V

    invoke-virtual {v8, v9}, Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;->addRequest(Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;)V

    .line 247
    .end local v6    # "twBundle":Landroid/os/Bundle;
    :cond_17
    const-string v9, "tw_type_photo"

    invoke-virtual {v7, v9}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_19

    .line 248
    if-eqz v1, :cond_19

    const-string v9, "tw_photo"

    invoke-virtual {p3, v9}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_19

    .line 249
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 251
    .restart local v6    # "twBundle":Landroid/os/Bundle;
    if-eqz v4, :cond_18

    .line 252
    sget-object v9, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;->STATUS:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;

    invoke-virtual {v9}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;->getParam()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v9, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 254
    :cond_18
    sget-object v9, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;->FILENAME:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;

    invoke-virtual {v9}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;->getParam()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v9, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 255
    sget-object v9, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;->MEDIA:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;

    invoke-virtual {v9}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI$Param;->getParam()Ljava/lang/String;

    move-result-object v9

    const-string v10, "tw_photo"

    invoke-virtual {p3, v10}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v10

    invoke-virtual {v6, v9, v10}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 257
    new-instance v9, Lcom/sec/android/app/sns3/agent/sp/common/command/SnsCoCmdPostContents$6;

    sget-object v10, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;->STATUSES_UPDATE_WITH_MEDIA:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;

    invoke-direct {v9, p0, p1, v10, v6}, Lcom/sec/android/app/sns3/agent/sp/common/command/SnsCoCmdPostContents$6;-><init>(Lcom/sec/android/app/sns3/agent/sp/common/command/SnsCoCmdPostContents;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;Landroid/os/Bundle;)V

    invoke-virtual {v8, v9}, Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;->addRequest(Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;)V

    .line 277
    .end local v6    # "twBundle":Landroid/os/Bundle;
    :cond_19
    const-string v9, "tw_type_dm"

    invoke-virtual {v7, v9}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1c

    .line 278
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 280
    .restart local v6    # "twBundle":Landroid/os/Bundle;
    if-eqz v4, :cond_1a

    .line 281
    sget-object v9, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwDirectMessagesAPI$Param;->TEXT:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwDirectMessagesAPI$Param;

    invoke-virtual {v9}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwDirectMessagesAPI$Param;->getParam()Ljava/lang/String;

    move-result-object v9

    const-string v10, "message"

    invoke-virtual {p3, v10}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v6, v9, v10}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 284
    :cond_1a
    const-string v9, "tw_recipient_screen_name"

    invoke-virtual {p3, v9}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_1b

    .line 285
    sget-object v9, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwDirectMessagesAPI$Param;->SCREEN_NAME:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwDirectMessagesAPI$Param;

    invoke-virtual {v9}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwDirectMessagesAPI$Param;->getParam()Ljava/lang/String;

    move-result-object v9

    const-string v10, "tw_recipient_screen_name"

    invoke-virtual {p3, v10}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v6, v9, v10}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 288
    :cond_1b
    new-instance v9, Lcom/sec/android/app/sns3/agent/sp/common/command/SnsCoCmdPostContents$7;

    sget-object v10, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwDirectMessagesAPI;->DIRECT_MESSAGES_NEW:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwDirectMessagesAPI;

    invoke-direct {v9, p0, p1, v10, v6}, Lcom/sec/android/app/sns3/agent/sp/common/command/SnsCoCmdPostContents$7;-><init>(Lcom/sec/android/app/sns3/agent/sp/common/command/SnsCoCmdPostContents;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwDirectMessagesAPI;Landroid/os/Bundle;)V

    invoke-virtual {v8, v9}, Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;->addRequest(Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;)V

    .line 306
    .end local v1    # "fileName":Ljava/lang/String;
    .end local v2    # "latitude":Ljava/lang/String;
    .end local v3    # "logitude":Ljava/lang/String;
    .end local v4    # "message":Ljava/lang/String;
    .end local v6    # "twBundle":Landroid/os/Bundle;
    :cond_1c
    invoke-virtual {p0, v8}, Lcom/sec/android/app/sns3/agent/sp/common/command/SnsCoCmdPostContents;->addCommandUnit(Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;)V

    .line 307
    return-void
.end method


# virtual methods
.method protected respond()Z
    .locals 6

    .prologue
    const/4 v2, 0x1

    .line 311
    const-string v1, "SnsAgent"

    const-string v3, "<SnsCoCmdPostContents> respond()"

    invoke-static {v1, v3}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 313
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/common/command/SnsCoCmdPostContents;->getResponseList()Ljava/util/List;

    move-result-object v0

    .line 314
    .local v0, "respList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;>;"
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    invoke-virtual {p0, v1}, Lcom/sec/android/app/sns3/agent/sp/common/command/SnsCoCmdPostContents;->setSuccess(Z)V

    .line 315
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/sec/android/app/sns3/agent/sp/common/command/SnsCoCmdPostContents;->setUri(Ljava/lang/String;)V

    .line 317
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/common/command/SnsCoCmdPostContents;->getCommandCallback()Lcom/sec/android/app/sns3/agent/command/ISnsCommandCallback;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/common/command/SnsCoCmdPostContents;->getCommandID()I

    move-result v3

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/common/command/SnsCoCmdPostContents;->isSuccess()Z

    move-result v4

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/common/command/SnsCoCmdPostContents;->getUri()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v1, v3, v4, v5, v0}, Lcom/sec/android/app/sns3/agent/command/ISnsCommandCallback;->onCmdRespond(IZLjava/lang/String;Ljava/util/List;)V

    .line 319
    return v2

    .line 314
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
