.class public Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;
.super Ljava/lang/Object;
.source "SnsLiResponsePosition.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition$LinkedInPosition;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mCompanyID:Ljava/lang/String;

.field private mCompanyName:Ljava/lang/String;

.field private mCompanySize:Ljava/lang/String;

.field private mCompanyType:Ljava/lang/String;

.field private mEndMonth:Ljava/lang/String;

.field private mEndYear:Ljava/lang/String;

.field private mIndustry:Ljava/lang/String;

.field private mIsCurrent:Ljava/lang/Boolean;

.field private mPositionId:Ljava/lang/String;

.field private mStartMonth:Ljava/lang/String;

.field private mStartYear:Ljava/lang/String;

.field private mSummary:Ljava/lang/String;

.field private mTitle:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 50
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition$1;

    invoke-direct {v0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition$1;-><init>()V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "parcel"    # Landroid/os/Parcel;

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    invoke-virtual {p0, p1}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;->readFromParcel(Landroid/os/Parcel;)V

    .line 48
    return-void
.end method

.method public static parse(Lorg/json/JSONObject;)Ljava/util/List;
    .locals 10
    .param p0, "positions"    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/json/JSONObject;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;",
            ">;"
        }
    .end annotation

    .prologue
    .line 243
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 244
    .local v7, "positionsList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;>;"
    if-eqz p0, :cond_4

    .line 247
    :try_start_0
    const-string v9, "values"

    invoke-virtual {p0, v9}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v5

    .line 248
    .local v5, "positionArray":Lorg/json/JSONArray;
    if-eqz v5, :cond_4

    .line 249
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-virtual {v5}, Lorg/json/JSONArray;->length()I

    move-result v9

    if-ge v3, v9, :cond_4

    .line 250
    new-instance v4, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;

    invoke-direct {v4}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;-><init>()V

    .line 251
    .local v4, "newPosition":Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;
    invoke-virtual {v5, v3}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v6

    .line 252
    .local v6, "positionItem":Lorg/json/JSONObject;
    const-string v9, "company"

    invoke-virtual {v6, v9}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 253
    .local v0, "company":Lorg/json/JSONObject;
    const-string v9, "name"

    invoke-virtual {v0, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v4, v9}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;->setCompanyName(Ljava/lang/String;)V

    .line 255
    const-string v9, "industry"

    invoke-virtual {v0, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v4, v9}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;->setIndustry(Ljava/lang/String;)V

    .line 256
    const-string v9, "size"

    invoke-virtual {v0, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v4, v9}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;->setCompanySize(Ljava/lang/String;)V

    .line 257
    const-string v9, "id"

    invoke-virtual {v0, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v4, v9}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;->setCompanyID(Ljava/lang/String;)V

    .line 258
    const-string v9, "type"

    invoke-virtual {v0, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v4, v9}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;->setCompanyType(Ljava/lang/String;)V

    .line 259
    const-string v9, "id"

    invoke-virtual {v6, v9}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v4, v9}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;->setPositionId(Ljava/lang/String;)V

    .line 260
    const-string v9, "title"

    invoke-virtual {v6, v9}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v4, v9}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;->setTitle(Ljava/lang/String;)V

    .line 261
    const-string v9, "summary"

    invoke-virtual {v6, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v4, v9}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;->setSummary(Ljava/lang/String;)V

    .line 262
    const-string v9, "isCurrent"

    invoke-virtual {v6, v9}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v4, v9}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;->setIsCurrent(Ljava/lang/String;)V

    .line 263
    const-string v9, "startDate"

    invoke-virtual {v6, v9}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 264
    const-string v9, "startDate"

    invoke-virtual {v6, v9}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v8

    .line 265
    .local v8, "startDate":Lorg/json/JSONObject;
    const-string v9, "month"

    invoke-virtual {v8, v9}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 266
    const-string v9, "month"

    invoke-virtual {v8, v9}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v4, v9}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;->setStartMonth(Ljava/lang/String;)V

    .line 267
    :cond_0
    const-string v9, "year"

    invoke-virtual {v8, v9}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 268
    const-string v9, "year"

    invoke-virtual {v8, v9}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v4, v9}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;->setStartYear(Ljava/lang/String;)V

    .line 270
    .end local v8    # "startDate":Lorg/json/JSONObject;
    :cond_1
    invoke-virtual {v4}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;->getIsCurrent()Ljava/lang/Boolean;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v9

    if-nez v9, :cond_3

    .line 271
    const-string v9, "endDate"

    invoke-virtual {v6, v9}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 272
    const-string v9, "endDate"

    invoke-virtual {v6, v9}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    .line 273
    .local v2, "endDate":Lorg/json/JSONObject;
    const-string v9, "month"

    invoke-virtual {v2, v9}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 274
    const-string v9, "month"

    invoke-virtual {v2, v9}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v4, v9}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;->setEndMonth(Ljava/lang/String;)V

    .line 275
    :cond_2
    const-string v9, "year"

    invoke-virtual {v2, v9}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 276
    const-string v9, "year"

    invoke-virtual {v2, v9}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v4, v9}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;->setEndYear(Ljava/lang/String;)V

    .line 279
    .end local v2    # "endDate":Lorg/json/JSONObject;
    :cond_3
    invoke-interface {v7, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 249
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    .line 282
    .end local v0    # "company":Lorg/json/JSONObject;
    .end local v3    # "i":I
    .end local v4    # "newPosition":Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;
    .end local v5    # "positionArray":Lorg/json/JSONArray;
    .end local v6    # "positionItem":Lorg/json/JSONObject;
    :catch_0
    move-exception v1

    .line 283
    .local v1, "e":Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    .line 286
    .end local v1    # "e":Lorg/json/JSONException;
    :cond_4
    return-object v7
.end method

.method private setIsCurrent(Ljava/lang/String;)V
    .locals 1
    .param p1, "isCurrent"    # Ljava/lang/String;

    .prologue
    .line 125
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 126
    invoke-static {p1}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;->mIsCurrent:Ljava/lang/Boolean;

    .line 130
    :goto_0
    return-void

    .line 128
    :cond_0
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;->mIsCurrent:Ljava/lang/Boolean;

    goto :goto_0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 174
    const/4 v0, 0x0

    return v0
.end method

.method public getCompanyID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;->mCompanyID:Ljava/lang/String;

    return-object v0
.end method

.method public getCompanyName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;->mCompanyName:Ljava/lang/String;

    return-object v0
.end method

.method public getCompanySize()Ljava/lang/String;
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;->mCompanySize:Ljava/lang/String;

    return-object v0
.end method

.method public getCompanyType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 165
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;->mCompanyType:Ljava/lang/String;

    return-object v0
.end method

.method public getEndMonth()Ljava/lang/String;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;->mEndMonth:Ljava/lang/String;

    return-object v0
.end method

.method public getEndYear()Ljava/lang/String;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;->mEndYear:Ljava/lang/String;

    return-object v0
.end method

.method public getIndustry()Ljava/lang/String;
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;->mIndustry:Ljava/lang/String;

    return-object v0
.end method

.method public getIsCurrent()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;->mIsCurrent:Ljava/lang/Boolean;

    return-object v0
.end method

.method public getPositionId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;->mPositionId:Ljava/lang/String;

    return-object v0
.end method

.method public getStartMonth()Ljava/lang/String;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;->mStartMonth:Ljava/lang/String;

    return-object v0
.end method

.method public getStartYear()Ljava/lang/String;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;->mStartYear:Ljava/lang/String;

    return-object v0
.end method

.method public getSummary()Ljava/lang/String;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;->mSummary:Ljava/lang/String;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;->mTitle:Ljava/lang/String;

    return-object v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "parcel"    # Landroid/os/Parcel;

    .prologue
    .line 196
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;->mPositionId:Ljava/lang/String;

    .line 197
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;->mTitle:Ljava/lang/String;

    .line 198
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;->mSummary:Ljava/lang/String;

    .line 199
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;->mStartMonth:Ljava/lang/String;

    .line 200
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;->mStartYear:Ljava/lang/String;

    .line 201
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;->mEndMonth:Ljava/lang/String;

    .line 202
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;->mEndYear:Ljava/lang/String;

    .line 203
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;->mIsCurrent:Ljava/lang/Boolean;

    .line 204
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;->mCompanyName:Ljava/lang/String;

    .line 205
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;->mIndustry:Ljava/lang/String;

    .line 206
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;->mCompanySize:Ljava/lang/String;

    .line 207
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;->mCompanyID:Ljava/lang/String;

    .line 208
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;->mCompanyType:Ljava/lang/String;

    .line 209
    return-void
.end method

.method public setCompanyID(Ljava/lang/String;)V
    .locals 0
    .param p1, "companyID"    # Ljava/lang/String;

    .prologue
    .line 161
    iput-object p1, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;->mCompanyID:Ljava/lang/String;

    .line 162
    return-void
.end method

.method public setCompanyName(Ljava/lang/String;)V
    .locals 0
    .param p1, "mCompanyName"    # Ljava/lang/String;

    .prologue
    .line 137
    iput-object p1, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;->mCompanyName:Ljava/lang/String;

    .line 138
    return-void
.end method

.method public setCompanySize(Ljava/lang/String;)V
    .locals 0
    .param p1, "mCompanySize"    # Ljava/lang/String;

    .prologue
    .line 153
    iput-object p1, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;->mCompanySize:Ljava/lang/String;

    .line 154
    return-void
.end method

.method public setCompanyType(Ljava/lang/String;)V
    .locals 0
    .param p1, "mCompanyType"    # Ljava/lang/String;

    .prologue
    .line 169
    iput-object p1, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;->mCompanyType:Ljava/lang/String;

    .line 170
    return-void
.end method

.method public setEndMonth(Ljava/lang/String;)V
    .locals 0
    .param p1, "mEndMonth"    # Ljava/lang/String;

    .prologue
    .line 105
    iput-object p1, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;->mEndMonth:Ljava/lang/String;

    .line 106
    return-void
.end method

.method public setEndYear(Ljava/lang/String;)V
    .locals 0
    .param p1, "mEndYear"    # Ljava/lang/String;

    .prologue
    .line 113
    iput-object p1, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;->mEndYear:Ljava/lang/String;

    .line 114
    return-void
.end method

.method public setIndustry(Ljava/lang/String;)V
    .locals 0
    .param p1, "mIndustry"    # Ljava/lang/String;

    .prologue
    .line 145
    iput-object p1, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;->mIndustry:Ljava/lang/String;

    .line 146
    return-void
.end method

.method public setIsCurrent(Ljava/lang/Boolean;)V
    .locals 0
    .param p1, "mIsCurrent"    # Ljava/lang/Boolean;

    .prologue
    .line 121
    iput-object p1, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;->mIsCurrent:Ljava/lang/Boolean;

    .line 122
    return-void
.end method

.method public setPositionId(Ljava/lang/String;)V
    .locals 0
    .param p1, "mPositionId"    # Ljava/lang/String;

    .prologue
    .line 65
    iput-object p1, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;->mPositionId:Ljava/lang/String;

    .line 66
    return-void
.end method

.method public setStartMonth(Ljava/lang/String;)V
    .locals 0
    .param p1, "mStartMonth"    # Ljava/lang/String;

    .prologue
    .line 89
    iput-object p1, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;->mStartMonth:Ljava/lang/String;

    .line 90
    return-void
.end method

.method public setStartYear(Ljava/lang/String;)V
    .locals 0
    .param p1, "mStartYear"    # Ljava/lang/String;

    .prologue
    .line 97
    iput-object p1, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;->mStartYear:Ljava/lang/String;

    .line 98
    return-void
.end method

.method public setSummary(Ljava/lang/String;)V
    .locals 0
    .param p1, "mSummary"    # Ljava/lang/String;

    .prologue
    .line 81
    iput-object p1, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;->mSummary:Ljava/lang/String;

    .line 82
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 0
    .param p1, "mTitle"    # Ljava/lang/String;

    .prologue
    .line 73
    iput-object p1, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;->mTitle:Ljava/lang/String;

    .line 74
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "parcel"    # Landroid/os/Parcel;
    .param p2, "flag"    # I

    .prologue
    .line 179
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;->mPositionId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 180
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;->mTitle:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 181
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;->mSummary:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 182
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;->mStartMonth:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 183
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;->mStartYear:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 184
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;->mEndMonth:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 185
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;->mEndYear:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 186
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;->mIsCurrent:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 187
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;->mCompanyName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 188
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;->mIndustry:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 189
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;->mCompanySize:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 190
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;->mCompanyID:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 191
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;->mCompanyType:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 193
    return-void
.end method
