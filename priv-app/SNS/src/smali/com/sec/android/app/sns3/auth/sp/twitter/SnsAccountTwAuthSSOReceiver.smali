.class public Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthSSOReceiver;
.super Landroid/content/BroadcastReceiver;
.source "SnsAccountTwAuthSSOReceiver.java"


# static fields
.field private static final ACCOUNT_PREFERENCE_NAME:Ljava/lang/String; = "TW_account"

.field private static final ACTION_ACCOUNT_REMOVED:Ljava/lang/String; = "com.twitter.android.action.ACCOUNT_REMOVED"

.field private static final ACTION_ACCOUNT_SWITCHED:Ljava/lang/String; = "com.twitter.android.action.ACCOUNT_SWITCHED"

.field private static final APP_ACCOUNT_PREFERENCE_KEY:Ljava/lang/String; = "app_account"

.field public static final SNS_ACCOUNT_CHECK:Ljava/lang/String; = "com.sec.android.app.sns3.auth.ACTION_CHECK_ACCOUNT"

.field private static final SNS_ACCOUNT_PREFERENCE_KEY:Ljava/lang/String; = "sns_account"

.field public static final SNS_LOGIN_TWITTER_NOTIFICATION:Ljava/lang/String; = "com.sec.android.app.sns3.auth.sp.twitter.SNS_LOGIN_TWITTER_NOTIFICATION"

.field private static final TAG:Ljava/lang/String; = "TwAuthSSOReceiver"


# instance fields
.field private mSharedPref:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method private addTwitterSSOAccount(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 144
    const-string v1, "TwAuthSSOReceiver"

    const-string v2, "Twitter SSO Receiver : addTwitterSSOAccount"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 146
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthSSOActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 147
    .local v0, "i":Landroid/content/Intent;
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 148
    const/high16 v1, 0x800000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 149
    const-string v1, "skip_sync_setup"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 150
    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 151
    return-void
.end method

.method private registerLoginNotification(Landroid/content/Context;)V
    .locals 11
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 167
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 169
    .local v0, "i":Landroid/content/Intent;
    const-class v6, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthSSOActivity;

    invoke-virtual {v0, p1, v6}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 173
    const/high16 v6, 0x10000000

    invoke-virtual {v0, v6}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 174
    const/high16 v6, 0x800000

    invoke-virtual {v0, v6}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 175
    const-string v6, "skip_sso_noti"

    invoke-virtual {v0, v6, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 176
    const/16 v6, 0x7d0

    invoke-static {p1, v6, v0, v9}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 179
    .local v1, "intent":Landroid/app/PendingIntent;
    const v6, 0x7f080030

    new-array v7, v10, [Ljava/lang/Object;

    const v8, 0x7f08006d

    invoke-virtual {p1, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v9

    invoke-virtual {p1, v6, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 181
    .local v4, "ticker":Ljava/lang/String;
    const v6, 0x7f08000a

    invoke-virtual {p1, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 183
    .local v5, "title":Ljava/lang/String;
    new-instance v2, Landroid/app/Notification$Builder;

    invoke-direct {v2, p1}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    .line 184
    .local v2, "noti":Landroid/app/Notification$Builder;
    invoke-virtual {v2, v5}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    .line 185
    invoke-virtual {v2, v4}, Landroid/app/Notification$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    .line 186
    invoke-virtual {v2, v4}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    .line 187
    invoke-virtual {v2, v1}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    .line 188
    invoke-virtual {v2, v9}, Landroid/app/Notification$Builder;->setAutoCancel(Z)Landroid/app/Notification$Builder;

    .line 189
    new-instance v6, Landroid/app/Notification$BigTextStyle;

    invoke-direct {v6}, Landroid/app/Notification$BigTextStyle;-><init>()V

    invoke-virtual {v6, v4}, Landroid/app/Notification$BigTextStyle;->bigText(Ljava/lang/CharSequence;)Landroid/app/Notification$BigTextStyle;

    move-result-object v6

    invoke-virtual {v2, v6}, Landroid/app/Notification$Builder;->setStyle(Landroid/app/Notification$Style;)Landroid/app/Notification$Builder;

    .line 190
    invoke-virtual {v2, v10}, Landroid/app/Notification$Builder;->setDefaults(I)Landroid/app/Notification$Builder;

    .line 191
    const v6, 0x7f02001f

    invoke-virtual {v2, v6}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    .line 193
    const-string v6, "notification"

    invoke-virtual {p1, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/NotificationManager;

    .line 196
    .local v3, "notiMgr":Landroid/app/NotificationManager;
    const/16 v6, 0x834

    invoke-virtual {v2}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v7

    invoke-virtual {v3, v6, v7}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 200
    return-void
.end method

.method private removeTwitterSSOAccount(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    .line 154
    const-string v2, "TwAuthSSOReceiver"

    const-string v3, "Twitter SSO Receiver : removeTwitterSSOAccount"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 156
    const-string v2, "notification"

    invoke-virtual {p1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/NotificationManager;

    .line 158
    .local v1, "nm":Landroid/app/NotificationManager;
    const/16 v2, 0x834

    invoke-virtual {v1, v2}, Landroid/app/NotificationManager;->cancel(I)V

    .line 160
    invoke-static {p1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v2

    const-string v3, "com.sec.android.app.sns3.twitter"

    invoke-virtual {v2, v3}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 161
    .local v0, "accounts":[Landroid/accounts/Account;
    array-length v2, v0

    if-eqz v2, :cond_0

    .line 162
    invoke-static {p1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v2

    const/4 v3, 0x0

    aget-object v3, v0, v3

    invoke-virtual {v2, v3, v4, v4}, Landroid/accounts/AccountManager;->removeAccount(Landroid/accounts/Account;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    .line 164
    :cond_0
    return-void
.end method

.method private storeAccountStatus(II)V
    .locals 2
    .param p1, "snsAccount"    # I
    .param p2, "appAccount"    # I

    .prologue
    .line 137
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthSSOReceiver;->mSharedPref:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 138
    .local v0, "prefEditor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "sns_account"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 139
    const-string v1, "app_account"

    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 140
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 141
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 13
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 57
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    .line 59
    .local v2, "action":Ljava/lang/String;
    const-string v10, "TW_account"

    const/4 v11, 0x0

    invoke-virtual {p1, v10, v11}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v10

    iput-object v10, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthSSOReceiver;->mSharedPref:Landroid/content/SharedPreferences;

    .line 61
    const-string v10, "TwAuthSSOReceiver"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Twitter SSO Receiver : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 63
    invoke-static {p1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v10

    const-string v11, "com.sec.android.app.sns3.twitter"

    invoke-virtual {v10, v11}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v9

    .line 66
    .local v9, "sns3Accounts":[Landroid/accounts/Account;
    invoke-static {p1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v10

    const-string v11, "com.twitter.android.auth.login"

    invoke-virtual {v10, v11}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v3

    .line 69
    .local v3, "appAccounts":[Landroid/accounts/Account;
    const-string v10, "com.sec.android.app.sns3.auth.ACTION_CHECK_ACCOUNT"

    invoke-virtual {v10, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 70
    array-length v10, v9

    array-length v11, v3

    invoke-direct {p0, v10, v11}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthSSOReceiver;->storeAccountStatus(II)V

    .line 134
    :cond_0
    :goto_0
    return-void

    .line 71
    :cond_1
    const-string v10, "com.twitter.android.action.ACCOUNT_SWITCHED"

    invoke-virtual {v10, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_0

    .line 73
    const-string v10, "com.twitter.android.action.ACCOUNT_REMOVED"

    invoke-virtual {v10, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_0

    .line 75
    const-string v10, "com.sec.android.app.sns3.auth.sp.twitter.SNS_LOGIN_TWITTER_NOTIFICATION"

    invoke-virtual {v10, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 76
    array-length v10, v3

    if-lez v10, :cond_0

    array-length v10, v9

    if-nez v10, :cond_0

    .line 77
    invoke-direct {p0, p1}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthSSOReceiver;->registerLoginNotification(Landroid/content/Context;)V

    goto :goto_0

    .line 79
    :cond_2
    const-string v10, "android.accounts.LOGIN_ACCOUNTS_CHANGED"

    invoke-virtual {v10, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 80
    const-string v10, "operation"

    invoke-virtual {p2, v10}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 81
    .local v8, "operation":Ljava/lang/String;
    const/4 v4, 0x0

    .line 83
    .local v4, "changedAccountType":Ljava/lang/String;
    if-eqz v8, :cond_8

    .line 84
    const-string v10, "account"

    invoke-virtual {p2, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    .line 85
    .local v0, "account":Landroid/accounts/Account;
    const-string v10, "accounts"

    invoke-virtual {p2, v10}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    .line 86
    .local v1, "accounts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/accounts/Account;>;"
    if-nez v0, :cond_3

    if-eqz v1, :cond_3

    .line 87
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_1
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v10

    if-ge v5, v10, :cond_3

    .line 88
    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "account":Landroid/accounts/Account;
    check-cast v0, Landroid/accounts/Account;

    .line 89
    .restart local v0    # "account":Landroid/accounts/Account;
    if-eqz v0, :cond_7

    const-string v10, "com.twitter.android.auth.login"

    iget-object v11, v0, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_7

    .line 94
    .end local v5    # "i":I
    :cond_3
    if-eqz v0, :cond_4

    .line 95
    iget-object v4, v0, Landroid/accounts/Account;->type:Ljava/lang/String;

    .line 116
    .end local v0    # "account":Landroid/accounts/Account;
    .end local v1    # "accounts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/accounts/Account;>;"
    :cond_4
    :goto_2
    if-eqz v4, :cond_5

    .line 117
    const-string v10, "TwAuthSSOReceiver"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 121
    :cond_5
    const-string v10, "add"

    invoke-virtual {v10, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_c

    const-string v10, "com.twitter.android.auth.login"

    invoke-virtual {v10, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_c

    .line 123
    array-length v10, v9

    if-nez v10, :cond_6

    invoke-static {}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthSSOActivity;->isRunning()Z

    move-result v10

    if-nez v10, :cond_6

    .line 125
    invoke-direct {p0, p1}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthSSOReceiver;->addTwitterSSOAccount(Landroid/content/Context;)V

    .line 132
    :cond_6
    :goto_3
    array-length v10, v9

    array-length v11, v3

    invoke-direct {p0, v10, v11}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthSSOReceiver;->storeAccountStatus(II)V

    goto/16 :goto_0

    .line 87
    .restart local v0    # "account":Landroid/accounts/Account;
    .restart local v1    # "accounts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/accounts/Account;>;"
    .restart local v5    # "i":I
    :cond_7
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 98
    .end local v0    # "account":Landroid/accounts/Account;
    .end local v1    # "accounts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/accounts/Account;>;"
    .end local v5    # "i":I
    :cond_8
    iget-object v10, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthSSOReceiver;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v11, "app_account"

    const/4 v12, 0x0

    invoke-interface {v10, v11, v12}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v6

    .line 99
    .local v6, "lastAppAccountStatus":I
    iget-object v10, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthSSOReceiver;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v11, "sns_account"

    const/4 v12, 0x0

    invoke-interface {v10, v11, v12}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v7

    .line 101
    .local v7, "lastSnsAccountStatus":I
    array-length v10, v3

    if-le v10, v6, :cond_9

    .line 102
    const-string v4, "com.twitter.android.auth.login"

    .line 103
    const-string v8, "add"

    goto :goto_2

    .line 104
    :cond_9
    array-length v10, v3

    if-ge v10, v6, :cond_a

    .line 105
    const-string v4, "com.twitter.android.auth.login"

    .line 106
    const-string v8, "remove"

    goto :goto_2

    .line 107
    :cond_a
    array-length v10, v9

    if-le v10, v7, :cond_b

    .line 108
    const-string v4, "com.sec.android.app.sns3.twitter"

    .line 109
    const-string v8, "add"

    goto :goto_2

    .line 110
    :cond_b
    array-length v10, v9

    if-ge v10, v7, :cond_4

    .line 111
    const-string v4, "com.sec.android.app.sns3.twitter"

    .line 112
    const-string v8, "remove"

    goto :goto_2

    .line 127
    .end local v6    # "lastAppAccountStatus":I
    .end local v7    # "lastSnsAccountStatus":I
    :cond_c
    const-string v10, "remove"

    invoke-virtual {v10, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_6

    const-string v10, "com.twitter.android.auth.login"

    invoke-virtual {v10, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 129
    invoke-direct {p0, p1}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthSSOReceiver;->removeTwitterSSOAccount(Landroid/content/Context;)V

    goto :goto_3
.end method
