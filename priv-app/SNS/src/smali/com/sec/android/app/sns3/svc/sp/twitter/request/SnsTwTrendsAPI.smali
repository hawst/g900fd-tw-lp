.class public final enum Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwTrendsAPI;
.super Ljava/lang/Enum;
.source "SnsTwTrendsAPI.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwTrendsAPI$Param;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwTrendsAPI;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwTrendsAPI;

.field public static final enum TRENDS_AVAILABLE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwTrendsAPI;

.field public static final enum TRENDS_DAILY:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwTrendsAPI;

.field public static final enum TRENDS_WEEKLY:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwTrendsAPI;

.field public static final enum TRENDS_WOEID:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwTrendsAPI;


# instance fields
.field private mHttpMethod:Ljava/lang/String;

.field private mUrl:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 27
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwTrendsAPI;

    const-string v1, "TRENDS_WOEID"

    const-string v2, "/trends/"

    const-string v3, "GET"

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwTrendsAPI;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwTrendsAPI;->TRENDS_WOEID:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwTrendsAPI;

    .line 28
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwTrendsAPI;

    const-string v1, "TRENDS_AVAILABLE"

    const-string v2, "/trends/available.json"

    const-string v3, "GET"

    invoke-direct {v0, v1, v5, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwTrendsAPI;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwTrendsAPI;->TRENDS_AVAILABLE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwTrendsAPI;

    .line 29
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwTrendsAPI;

    const-string v1, "TRENDS_DAILY"

    const-string v2, "/trends/daily.json"

    const-string v3, "GET"

    invoke-direct {v0, v1, v6, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwTrendsAPI;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwTrendsAPI;->TRENDS_DAILY:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwTrendsAPI;

    .line 30
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwTrendsAPI;

    const-string v1, "TRENDS_WEEKLY"

    const-string v2, "/trends/weekly.json"

    const-string v3, "GET"

    invoke-direct {v0, v1, v7, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwTrendsAPI;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwTrendsAPI;->TRENDS_WEEKLY:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwTrendsAPI;

    .line 25
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwTrendsAPI;

    sget-object v1, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwTrendsAPI;->TRENDS_WOEID:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwTrendsAPI;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwTrendsAPI;->TRENDS_AVAILABLE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwTrendsAPI;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwTrendsAPI;->TRENDS_DAILY:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwTrendsAPI;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwTrendsAPI;->TRENDS_WEEKLY:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwTrendsAPI;

    aput-object v1, v0, v7

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwTrendsAPI;->$VALUES:[Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwTrendsAPI;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p3, "url"    # Ljava/lang/String;
    .param p4, "httpMethod"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 58
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 59
    iput-object p3, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwTrendsAPI;->mUrl:Ljava/lang/String;

    .line 60
    iput-object p4, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwTrendsAPI;->mHttpMethod:Ljava/lang/String;

    .line 61
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwTrendsAPI;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 25
    const-class v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwTrendsAPI;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwTrendsAPI;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwTrendsAPI;
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwTrendsAPI;->$VALUES:[Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwTrendsAPI;

    invoke-virtual {v0}, [Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwTrendsAPI;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwTrendsAPI;

    return-object v0
.end method


# virtual methods
.method public getHttpMethod()Ljava/lang/String;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwTrendsAPI;->mHttpMethod:Ljava/lang/String;

    return-object v0
.end method

.method public getUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwTrendsAPI;->mUrl:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwTrendsAPI;->mUrl:Ljava/lang/String;

    return-object v0
.end method
