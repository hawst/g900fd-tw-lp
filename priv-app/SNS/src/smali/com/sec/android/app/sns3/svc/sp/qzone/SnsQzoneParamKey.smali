.class public final Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzoneParamKey;
.super Ljava/lang/Object;
.source "SnsQzoneParamKey.java"


# static fields
.field public static final ACCESS_TOKEN:Ljava/lang/String; = "access_token"

.field public static final ACTIONS:Ljava/lang/String; = "actions"

.field public static final APP_ID:Ljava/lang/String; = "app_id"

.field public static final CALLBACK_URL:Ljava/lang/String; = "callback_url"

.field public static final CAPTION:Ljava/lang/String; = "caption"

.field public static final CHECKIN_FLAG_FRIEND:Ljava/lang/String; = "2"

.field public static final CHECKIN_FLAG_MINE:Ljava/lang/String; = "1"

.field public static final CONTENT_TYPE:Ljava/lang/String; = "Content-Type"

.field public static final COORDINATES:Ljava/lang/String; = "coordinates"

.field public static final DESCRIPTION:Ljava/lang/String; = "description"

.field public static final DISTANCE:Ljava/lang/String; = "distance"

.field public static final END_TIME:Ljava/lang/String; = "end_time"

.field public static final FIELDS:Ljava/lang/String; = "fields"

.field public static final FILE_NAME:Ljava/lang/String; = "filename"

.field public static final LATITUDE:Ljava/lang/String; = "latitude"

.field public static final LINK:Ljava/lang/String; = "link"

.field public static final LOCATION:Ljava/lang/String; = "location"

.field public static final LONGITUDE:Ljava/lang/String; = "longitude"

.field public static final MESSAGE:Ljava/lang/String; = "message"

.field public static final NAME:Ljava/lang/String; = "name"

.field public static final OBJECT:Ljava/lang/String; = "object"

.field public static final OBJECT_ID:Ljava/lang/String; = "object_id"

.field public static final PICTURE:Ljava/lang/String; = "picture"

.field public static final PLACE:Ljava/lang/String; = "place"

.field public static final POSITION:Ljava/lang/String; = "position"

.field public static final PRIVACY:Ljava/lang/String; = "privacy"

.field public static final SOURCE:Ljava/lang/String; = "source"

.field public static final START_TIME:Ljava/lang/String; = "start_time"

.field public static final SUBJECT:Ljava/lang/String; = "subject"

.field public static final TAB_ID:Ljava/lang/String; = "tab_id"

.field public static final TAGS:Ljava/lang/String; = "tags"

.field public static final UID:Ljava/lang/String; = "uid"

.field public static final USERS:Ljava/lang/String; = "users"

.field public static final VERIFY_TOKEN:Ljava/lang/String; = "verify_token"

.field public static final _PHOTO:Ljava/lang/String; = "_photo"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
