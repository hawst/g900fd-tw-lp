.class Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService$4;
.super Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqGetGroupList;
.source "SnsFbSyncAdapterStreamsService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->performGroupsSync()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;


# direct methods
.method constructor <init>(Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;)V
    .locals 0
    .param p2, "x0"    # Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    .param p3, "x1"    # Ljava/lang/String;

    .prologue
    .line 408
    iput-object p1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService$4;->this$0:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;

    invoke-direct {p0, p2, p3}, Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqGetGroupList;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public onReqRespond(IZIILandroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseGroupList;)Z
    .locals 4
    .param p1, "reqID"    # I
    .param p2, "bSuccess"    # Z
    .param p3, "httpStatus"    # I
    .param p4, "errorCode"    # I
    .param p5, "reason"    # Landroid/os/Bundle;
    .param p6, "groups"    # Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseGroupList;

    .prologue
    .line 414
    if-eqz p2, :cond_0

    .line 415
    new-instance v0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;

    invoke-direct {v0}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;-><init>()V

    .line 416
    .local v0, "fbSyncDataMgr":Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;
    invoke-virtual {v0, p6}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->insertGroupList(Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseGroupList;)V

    .line 424
    .end local v0    # "fbSyncDataMgr":Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService$4;->this$0:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;

    # getter for: Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;
    invoke-static {v1}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->access$1000(Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;)Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->resumeSync()V

    .line 425
    const/4 v1, 0x0

    return v1

    .line 418
    :cond_0
    # getter for: Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->access$100()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SnsFbSyncAdapterStreamsService errorCode : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", reason : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 421
    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService$4;->this$0:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;

    const/4 v2, -0x1

    # setter for: Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mSyncState:I
    invoke-static {v1, v2}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->access$902(Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;I)I

    goto :goto_0
.end method
