.class public Lcom/sec/android/app/sns3/svc/sp/twitter/parser/SnsTwParserStatus;
.super Ljava/lang/Object;
.source "SnsTwParserStatus.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;
    .locals 9
    .param p0, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 34
    const/4 v6, 0x0

    .line 37
    .local v6, "response":Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;
    invoke-static {p0}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->isJsonEmpty(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 38
    const/4 v7, 0x0

    .line 68
    :goto_0
    return-object v7

    .line 42
    :cond_0
    :try_start_0
    invoke-static {p0}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->isJsonArray(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 43
    new-instance v3, Lorg/json/JSONArray;

    invoke-direct {v3, p0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 45
    .local v3, "jsonArray":Lorg/json/JSONArray;
    const/4 v0, 0x0

    .line 46
    .local v0, "curNode":Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;
    const/4 v5, 0x0

    .line 48
    .local v5, "newNode":Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v7

    if-ge v2, v7, :cond_3

    .line 49
    invoke-virtual {v3, v2}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/app/sns3/svc/sp/twitter/parser/SnsTwParserStatus;->parseStatus(Lorg/json/JSONObject;)Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;

    move-result-object v5

    .line 51
    if-nez v6, :cond_1

    .line 52
    move-object v6, v5

    .line 53
    move-object v0, v6

    .line 48
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 55
    :cond_1
    iput-object v5, v0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;->mNext:Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;

    .line 56
    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;->mNext:Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;

    goto :goto_2

    .line 60
    .end local v0    # "curNode":Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;
    .end local v2    # "i":I
    .end local v3    # "jsonArray":Lorg/json/JSONArray;
    .end local v5    # "newNode":Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;
    :cond_2
    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 61
    .local v4, "jsonObj":Lorg/json/JSONObject;
    invoke-static {v4}, Lcom/sec/android/app/sns3/svc/sp/twitter/parser/SnsTwParserStatus;->parseStatus(Lorg/json/JSONObject;)Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .end local v4    # "jsonObj":Lorg/json/JSONObject;
    :cond_3
    :goto_3
    move-object v7, v6

    .line 68
    goto :goto_0

    .line 63
    :catch_0
    move-exception v1

    .line 64
    .local v1, "e":Lorg/json/JSONException;
    const-string v7, "SNS"

    invoke-virtual {v1}, Lorg/json/JSONException;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 65
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_3
.end method

.method private static parseStatus(Lorg/json/JSONObject;)Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;
    .locals 2
    .param p0, "jsonObj"    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 72
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;

    invoke-direct {v0}, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;-><init>()V

    .line 74
    .local v0, "response":Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;
    const-string v1, "id"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;->mId:Ljava/lang/String;

    .line 75
    const-string v1, "possibly_sensitive"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, v0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;->mPossiblySensitive:Z

    .line 76
    const-string v1, "in_reply_to_user_id"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;->mInReplyToUserId:Ljava/lang/String;

    .line 77
    const-string v1, "contributors"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;->mContributors:Ljava/lang/String;

    .line 78
    const-string v1, "favorited"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, v0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;->mFavorited:Z

    .line 79
    const-string v1, "truncated"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, v0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;->mTruncated:Z

    .line 80
    const-string v1, "source"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;->mSource:Ljava/lang/String;

    .line 81
    const-string v1, "place"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;->mPlace:Ljava/lang/String;

    .line 82
    const-string v1, "geo"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;->mGeo:Ljava/lang/String;

    .line 83
    const-string v1, "retweeted"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, v0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;->mRetweeted:Z

    .line 84
    const-string v1, "coordinates"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;->mCoordinates:Ljava/lang/String;

    .line 85
    const-string v1, "text"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;->mText:Ljava/lang/String;

    .line 86
    const-string v1, "created_at"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;->mCreatedAt:Ljava/lang/String;

    .line 87
    const-string v1, "retweet_count"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v1

    iput v1, v0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;->mRetweetCount:I

    .line 88
    const-string v1, "favourites_count"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v1

    iput v1, v0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;->mFavouritesCount:I

    .line 89
    const-string v1, "in_reply_to_screen_name"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;->mInReplyToScreenName:Ljava/lang/String;

    .line 90
    const-string v1, "in_reply_to_status_id"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;->mInReplyToStatusId:Ljava/lang/String;

    .line 92
    const-string v1, "user"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/sns3/svc/sp/twitter/parser/SnsTwParserUser;->parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseUser;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;->mUser:Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseUser;

    .line 93
    const-string v1, "retweeted_status"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/sns3/svc/sp/twitter/parser/SnsTwParserStatus;->parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;->mRetweetedStatus:Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;

    .line 94
    const-string v1, "entities"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/sns3/svc/sp/twitter/parser/SnsTwParserEntities;->parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseEntities;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;->mEntities:Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseEntities;

    .line 96
    return-object v0
.end method
