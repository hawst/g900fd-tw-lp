.class Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$SessionStatusCallback$1;
.super Ljava/lang/Object;
.source "SnsAccountFbAuthSSOActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$SessionStatusCallback;->call(Lcom/facebook/Session;Lcom/facebook/SessionState;Ljava/lang/Exception;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$SessionStatusCallback;


# direct methods
.method constructor <init>(Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$SessionStatusCallback;)V
    .locals 0

    .prologue
    .line 669
    iput-object p1, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$SessionStatusCallback$1;->this$1:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$SessionStatusCallback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 672
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$SessionStatusCallback$1;->this$1:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$SessionStatusCallback;

    iget-object v0, v0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$SessionStatusCallback;->this$0:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;

    # getter for: Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->mProgress:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->access$800(Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 673
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$SessionStatusCallback$1;->this$1:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$SessionStatusCallback;

    iget-object v0, v0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$SessionStatusCallback;->this$0:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;

    # getter for: Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->mProgress:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->access$800(Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 674
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$SessionStatusCallback$1;->this$1:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$SessionStatusCallback;

    iget-object v0, v0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$SessionStatusCallback;->this$0:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->mProgress:Landroid/app/ProgressDialog;
    invoke-static {v0, v1}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->access$802(Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;

    .line 676
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$SessionStatusCallback$1;->this$1:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$SessionStatusCallback;

    iget-object v0, v0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$SessionStatusCallback;->this$0:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;

    new-instance v1, Landroid/app/ProgressDialog;

    iget-object v2, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$SessionStatusCallback$1;->this$1:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$SessionStatusCallback;

    iget-object v2, v2, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$SessionStatusCallback;->this$0:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;

    # getter for: Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->mAppContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->access$1200(Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;)Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    # setter for: Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->mProgress:Landroid/app/ProgressDialog;
    invoke-static {v0, v1}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->access$802(Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;

    .line 677
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$SessionStatusCallback$1;->this$1:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$SessionStatusCallback;

    iget-object v0, v0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$SessionStatusCallback;->this$0:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;

    # getter for: Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->mProgress:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->access$800(Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$SessionStatusCallback$1;->this$1:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$SessionStatusCallback;

    iget-object v1, v1, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$SessionStatusCallback;->this$0:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;

    const v2, 0x7f080048

    invoke-virtual {v1, v2}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 678
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$SessionStatusCallback$1;->this$1:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$SessionStatusCallback;

    iget-object v0, v0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$SessionStatusCallback;->this$0:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;

    # getter for: Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->mProgress:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->access$800(Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 679
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$SessionStatusCallback$1;->this$1:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$SessionStatusCallback;

    iget-object v0, v0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$SessionStatusCallback;->this$0:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;

    # getter for: Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->mProgress:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->access$800(Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 680
    return-void
.end method
