.class public Lcom/sec/android/app/sns3/svc/sp/sinaweibo/SnsSwService;
.super Landroid/app/Service;
.source "SnsSwService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/sinaweibo/SnsSwService$SnsSwServiceBinder;
    }
.end annotation


# instance fields
.field private final mBinder:Landroid/os/IBinder;

.field private final mSnsSvcSinaweiboBinder:Lcom/sec/android/app/sns3/svc/sp/sinaweibo/api/ISnsSinaweibo$Stub;

.field private mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 52
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 54
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/sinaweibo/SnsSwService$SnsSwServiceBinder;

    invoke-direct {v0, p0}, Lcom/sec/android/app/sns3/svc/sp/sinaweibo/SnsSwService$SnsSwServiceBinder;-><init>(Lcom/sec/android/app/sns3/svc/sp/sinaweibo/SnsSwService;)V

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/sinaweibo/SnsSwService;->mBinder:Landroid/os/IBinder;

    .line 90
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/sinaweibo/SnsSwService$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/sns3/svc/sp/sinaweibo/SnsSwService$1;-><init>(Lcom/sec/android/app/sns3/svc/sp/sinaweibo/SnsSwService;)V

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/sinaweibo/SnsSwService;->mSnsSvcSinaweiboBinder:Lcom/sec/android/app/sns3/svc/sp/sinaweibo/api/ISnsSinaweibo$Stub;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/sns3/svc/sp/sinaweibo/SnsSwService;)Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sns3/svc/sp/sinaweibo/SnsSwService;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/sinaweibo/SnsSwService;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    return-object v0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 74
    const-string v0, "SNS"

    const-string v1, "SnsSwService : onBind()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 76
    const-class v0, Lcom/sec/android/app/sns3/svc/sp/sinaweibo/api/ISnsSinaweibo;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 77
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/sinaweibo/SnsSwService;->mSnsSvcSinaweiboBinder:Lcom/sec/android/app/sns3/svc/sp/sinaweibo/api/ISnsSinaweibo$Stub;

    .line 80
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/sinaweibo/SnsSwService;->mBinder:Landroid/os/IBinder;

    goto :goto_0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 66
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 67
    const-string v0, "SNS"

    const-string v1, "SnsSwService : onCreate()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 69
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/SnsApplication;->getSvcMgr()Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/sinaweibo/SnsSwService;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    .line 70
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 85
    const-string v0, "SNS"

    const-string v1, "SnsSwService : onStartCommand()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 87
    const/4 v0, 0x1

    return v0
.end method
