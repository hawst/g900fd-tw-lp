.class public Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthenticatorService$TwitterAccountAuthenticator;
.super Landroid/accounts/AbstractAccountAuthenticator;
.source "SnsAccountTwAuthenticatorService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthenticatorService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "TwitterAccountAuthenticator"
.end annotation


# instance fields
.field private mAppContext:Landroid/content/Context;

.field final synthetic this$0:Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthenticatorService;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthenticatorService;Landroid/content/Context;)V
    .locals 0
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 79
    iput-object p1, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthenticatorService$TwitterAccountAuthenticator;->this$0:Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthenticatorService;

    .line 80
    invoke-direct {p0, p2}, Landroid/accounts/AbstractAccountAuthenticator;-><init>(Landroid/content/Context;)V

    .line 81
    iput-object p2, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthenticatorService$TwitterAccountAuthenticator;->mAppContext:Landroid/content/Context;

    .line 82
    return-void
.end method


# virtual methods
.method public addAccount(Landroid/accounts/AccountAuthenticatorResponse;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 7
    .param p1, "response"    # Landroid/accounts/AccountAuthenticatorResponse;
    .param p2, "accountType"    # Ljava/lang/String;
    .param p3, "authTokenType"    # Ljava/lang/String;
    .param p4, "requiredFeatures"    # [Ljava/lang/String;
    .param p5, "options"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/accounts/NetworkErrorException;
        }
    .end annotation

    .prologue
    .line 88
    const-string v4, "SnsAccountTwAuthenticatorService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "TwitterAuthenticatorService : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " - "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 90
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 92
    .local v3, "result":Landroid/os/Bundle;
    if-eqz p5, :cond_1

    # getter for: Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthenticatorService;->OPTIONS_USERNAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthenticatorService;->access$000()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p5, v4}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 93
    new-instance v0, Landroid/accounts/Account;

    # getter for: Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthenticatorService;->OPTIONS_USERNAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthenticatorService;->access$000()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p5, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "com.sec.android.app.sns3.twitter"

    invoke-direct {v0, v4, v5}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    .local v0, "account":Landroid/accounts/Account;
    iget-object v4, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthenticatorService$TwitterAccountAuthenticator;->mAppContext:Landroid/content/Context;

    invoke-static {v4}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    .line 97
    .local v1, "am":Landroid/accounts/AccountManager;
    # getter for: Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthenticatorService;->OPTIONS_PASSWORD:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthenticatorService;->access$100()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p5, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v1, v0, v4, v5}, Landroid/accounts/AccountManager;->addAccountExplicitly(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 98
    const-string v4, "authAccount"

    # getter for: Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthenticatorService;->OPTIONS_USERNAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthenticatorService;->access$000()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p5, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    const-string v4, "accountType"

    const-string v5, "com.sec.android.app.sns3.twitter"

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    new-instance v2, Landroid/content/Intent;

    const-string v4, "com.sec.android.app.sns.ACTION_SNS_TWITTER_LOGGED_IN"

    invoke-direct {v2, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 104
    .local v2, "intent":Landroid/content/Intent;
    iget-object v4, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthenticatorService$TwitterAccountAuthenticator;->this$0:Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthenticatorService;

    const-string v5, "com.sec.android.app.sns3.permission.RECEIVE_SNS_BROADCAST"

    invoke-virtual {v4, v2, v5}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthenticatorService;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    .line 119
    .end local v0    # "account":Landroid/accounts/Account;
    .end local v1    # "am":Landroid/accounts/AccountManager;
    .end local v2    # "intent":Landroid/content/Intent;
    :cond_0
    :goto_0
    return-object v3

    .line 107
    :cond_1
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 109
    .restart local v2    # "intent":Landroid/content/Intent;
    iget-object v4, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthenticatorService$TwitterAccountAuthenticator;->mAppContext:Landroid/content/Context;

    const-class v5, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthSSOActivity;

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 113
    const-string v4, "accountAuthenticatorResponse"

    invoke-virtual {v2, v4, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 114
    const-string v4, "manageAccount"

    invoke-virtual {v2, v4, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 116
    const-string v4, "intent"

    invoke-virtual {v3, v4, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    goto :goto_0
.end method

.method public confirmCredentials(Landroid/accounts/AccountAuthenticatorResponse;Landroid/accounts/Account;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 2
    .param p1, "response"    # Landroid/accounts/AccountAuthenticatorResponse;
    .param p2, "account"    # Landroid/accounts/Account;
    .param p3, "options"    # Landroid/os/Bundle;

    .prologue
    .line 125
    const-string v0, "SnsAccountTwAuthenticatorService"

    const-string v1, "TwitterAuthenticatorService : confirmCredentials"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 126
    const/4 v0, 0x0

    return-object v0
.end method

.method public editProperties(Landroid/accounts/AccountAuthenticatorResponse;Ljava/lang/String;)Landroid/os/Bundle;
    .locals 2
    .param p1, "response"    # Landroid/accounts/AccountAuthenticatorResponse;
    .param p2, "accountType"    # Ljava/lang/String;

    .prologue
    .line 131
    const-string v0, "SnsAccountTwAuthenticatorService"

    const-string v1, "TwitterAuthenticatorService : editProperties"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 132
    const/4 v0, 0x0

    return-object v0
.end method

.method public getAccountRemovalAllowed(Landroid/accounts/AccountAuthenticatorResponse;Landroid/accounts/Account;)Landroid/os/Bundle;
    .locals 12
    .param p1, "response"    # Landroid/accounts/AccountAuthenticatorResponse;
    .param p2, "account"    # Landroid/accounts/Account;

    .prologue
    .line 137
    const-string v8, "SnsAccountTwAuthenticatorService"

    const-string v9, "TwitterAuthenticatorService : getAccountRemovalAllowed"

    invoke-static {v8, v9}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 139
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v0

    .line 140
    .local v0, "app":Lcom/sec/android/app/sns3/SnsApplication;
    new-instance v1, Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdAuthLogout;

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/SnsApplication;->getSvcMgr()Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v8

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/SnsApplication;->getAgentMgr()Lcom/sec/android/app/sns3/agent/SnsAgentMgr;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/app/sns3/agent/SnsAgentMgr;->getCommandMgr()Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;->getCommandMgrHandle()Landroid/os/Handler;

    move-result-object v9

    invoke-direct {v1, v8, v9}, Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdAuthLogout;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Handler;)V

    .line 142
    .local v1, "cmd":Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;
    new-instance v8, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthenticatorService$TwitterAccountAuthenticator$1;

    invoke-direct {v8, p0}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthenticatorService$TwitterAccountAuthenticator$1;-><init>(Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthenticatorService$TwitterAccountAuthenticator;)V

    invoke-virtual {v1, v8}, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->setCommandCallback(Lcom/sec/android/app/sns3/agent/command/ISnsCommandCallback;)Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;

    .line 151
    invoke-virtual {v1}, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->send()I

    .line 155
    iget-object v8, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthenticatorService$TwitterAccountAuthenticator;->this$0:Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthenticatorService;

    const-string v9, "TW_twitter_app_sync_interval"

    const/4 v10, 0x0

    invoke-virtual {v8, v9, v10}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthenticatorService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v8

    invoke-interface {v8}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v7

    .line 157
    .local v7, "twitterEditor":Landroid/content/SharedPreferences$Editor;
    const-string v8, "TW_twitter_app_sync_interval"

    const-wide/16 v10, 0x0

    invoke-interface {v7, v8, v10, v11}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 158
    invoke-interface {v7}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 161
    iget-object v8, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthenticatorService$TwitterAccountAuthenticator;->this$0:Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthenticatorService;

    const-string v9, "TW_home_feed_sync_interval"

    const/4 v10, 0x0

    invoke-virtual {v8, v9, v10}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthenticatorService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v8

    invoke-interface {v8}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 163
    .local v2, "homeFeedEditor":Landroid/content/SharedPreferences$Editor;
    const-string v8, "TW_home_feed_sync_interval"

    const-wide/16 v10, 0x0

    invoke-interface {v2, v8, v10, v11}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 164
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 166
    iget-object v8, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthenticatorService$TwitterAccountAuthenticator;->this$0:Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthenticatorService;

    const-string v9, "TW_profile_feed_sync_interval"

    const/4 v10, 0x0

    invoke-virtual {v8, v9, v10}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthenticatorService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v8

    invoke-interface {v8}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    .line 168
    .local v5, "profileFeedEditor":Landroid/content/SharedPreferences$Editor;
    const-string v8, "TW_profile_feed_sync_interval"

    const-wide/16 v10, 0x0

    invoke-interface {v5, v8, v10, v11}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 169
    invoke-interface {v5}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 172
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v8

    const-string v9, "notification"

    invoke-virtual {v8, v9}, Lcom/sec/android/app/sns3/SnsApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/app/NotificationManager;

    move-object v4, v8

    check-cast v4, Landroid/app/NotificationManager;

    .line 174
    .local v4, "notiMgr":Landroid/app/NotificationManager;
    const/16 v8, 0x8fc

    invoke-virtual {v4, v8}, Landroid/app/NotificationManager;->cancel(I)V

    .line 177
    new-instance v3, Landroid/content/Intent;

    const-string v8, "com.sec.android.app.sns.ACTION_SNS_TWITTER_LOGGED_OUT"

    invoke-direct {v3, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 178
    .local v3, "intent":Landroid/content/Intent;
    iget-object v8, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthenticatorService$TwitterAccountAuthenticator;->this$0:Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthenticatorService;

    const-string v9, "com.sec.android.app.sns3.permission.RECEIVE_SNS_BROADCAST"

    invoke-virtual {v8, v3, v9}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthenticatorService;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    .line 180
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 181
    .local v6, "result":Landroid/os/Bundle;
    const-string v8, "booleanResult"

    const/4 v9, 0x1

    invoke-virtual {v6, v8, v9}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 182
    return-object v6
.end method

.method public getAuthToken(Landroid/accounts/AccountAuthenticatorResponse;Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 2
    .param p1, "response"    # Landroid/accounts/AccountAuthenticatorResponse;
    .param p2, "account"    # Landroid/accounts/Account;
    .param p3, "authTokenType"    # Ljava/lang/String;
    .param p4, "loginOptions"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/accounts/NetworkErrorException;
        }
    .end annotation

    .prologue
    .line 188
    const-string v0, "SnsAccountTwAuthenticatorService"

    const-string v1, "TwitterAuthenticatorService : getAuthToken"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 189
    const/4 v0, 0x0

    return-object v0
.end method

.method public getAuthTokenLabel(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "authTokenType"    # Ljava/lang/String;

    .prologue
    .line 194
    const-string v0, "SnsAccountTwAuthenticatorService"

    const-string v1, "TwitterAuthenticatorService : getAuthTokenLabel"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 195
    const/4 v0, 0x0

    return-object v0
.end method

.method public hasFeatures(Landroid/accounts/AccountAuthenticatorResponse;Landroid/accounts/Account;[Ljava/lang/String;)Landroid/os/Bundle;
    .locals 2
    .param p1, "response"    # Landroid/accounts/AccountAuthenticatorResponse;
    .param p2, "account"    # Landroid/accounts/Account;
    .param p3, "features"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/accounts/NetworkErrorException;
        }
    .end annotation

    .prologue
    .line 201
    const-string v0, "SnsAccountTwAuthenticatorService"

    const-string v1, "TwitterAuthenticatorService : hasFeatures"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 202
    const/4 v0, 0x0

    return-object v0
.end method

.method public updateCredentials(Landroid/accounts/AccountAuthenticatorResponse;Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 2
    .param p1, "response"    # Landroid/accounts/AccountAuthenticatorResponse;
    .param p2, "account"    # Landroid/accounts/Account;
    .param p3, "authTokenType"    # Ljava/lang/String;
    .param p4, "loginOptions"    # Landroid/os/Bundle;

    .prologue
    .line 208
    const-string v0, "SnsAccountTwAuthenticatorService"

    const-string v1, "TwitterAuthenticatorService : updateCredentials"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 209
    const/4 v0, 0x0

    return-object v0
.end method
