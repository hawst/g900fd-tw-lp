.class public Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserAccounts;
.super Ljava/lang/Object;
.source "SnsFbParserAccounts.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserAccounts$FacebookAccounts;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    return-void
.end method

.method public static parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAccounts;
    .locals 10
    .param p0, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 45
    const/4 v0, 0x0

    .line 46
    .local v0, "account":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAccounts;
    const/4 v1, 0x0

    .line 47
    .local v1, "curAccount":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAccounts;
    const/4 v6, 0x0

    .line 50
    .local v6, "jsonObject":Lorg/json/JSONObject;
    :try_start_0
    new-instance v7, Lorg/json/JSONObject;

    invoke-direct {v7, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 52
    .end local v6    # "jsonObject":Lorg/json/JSONObject;
    .local v7, "jsonObject":Lorg/json/JSONObject;
    :try_start_1
    new-instance v4, Lorg/json/JSONArray;

    const-string v9, "data"

    invoke-virtual {v7, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v4, v9}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 54
    .local v4, "ja":Lorg/json/JSONArray;
    if-eqz v4, :cond_1

    .line 55
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v9

    if-ge v3, v9, :cond_1

    .line 56
    new-instance v8, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAccounts;

    invoke-direct {v8}, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAccounts;-><init>()V

    .line 58
    .local v8, "newAccount":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAccounts;
    invoke-virtual {v4, v3}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v5

    .line 60
    .local v5, "jsonObj":Lorg/json/JSONObject;
    const-string v9, "name"

    invoke-virtual {v5, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v8, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAccounts;->mName:Ljava/lang/String;

    .line 61
    const-string v9, "access_token"

    invoke-virtual {v5, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v8, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAccounts;->mAccessToken:Ljava/lang/String;

    .line 62
    const-string v9, "category"

    invoke-virtual {v5, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v8, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAccounts;->mCategory:Ljava/lang/String;

    .line 63
    const-string v9, "id"

    invoke-virtual {v5, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v8, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAccounts;->mAccountID:Ljava/lang/String;

    .line 65
    if-nez v0, :cond_0

    .line 66
    move-object v0, v8

    .line 67
    move-object v1, v0

    .line 55
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 69
    :cond_0
    iput-object v8, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAccounts;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAccounts;

    .line 70
    iget-object v1, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAccounts;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAccounts;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .end local v3    # "i":I
    .end local v5    # "jsonObj":Lorg/json/JSONObject;
    .end local v8    # "newAccount":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAccounts;
    :cond_1
    move-object v6, v7

    .line 80
    .end local v4    # "ja":Lorg/json/JSONArray;
    .end local v7    # "jsonObject":Lorg/json/JSONObject;
    .restart local v6    # "jsonObject":Lorg/json/JSONObject;
    :goto_2
    return-object v0

    .line 75
    :catch_0
    move-exception v2

    .line 77
    .local v2, "e":Lorg/json/JSONException;
    :goto_3
    invoke-virtual {v2}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_2

    .line 75
    .end local v2    # "e":Lorg/json/JSONException;
    .end local v6    # "jsonObject":Lorg/json/JSONObject;
    .restart local v7    # "jsonObject":Lorg/json/JSONObject;
    :catch_1
    move-exception v2

    move-object v6, v7

    .end local v7    # "jsonObject":Lorg/json/JSONObject;
    .restart local v6    # "jsonObject":Lorg/json/JSONObject;
    goto :goto_3
.end method
