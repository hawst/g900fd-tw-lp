.class public interface abstract Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserTabs$FacebookTabs;
.super Ljava/lang/Object;
.source "SnsFbParserTabs.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserTabs;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "FacebookTabs"
.end annotation


# static fields
.field public static final APPLICATION:Ljava/lang/String; = "application"

.field public static final CUSTOM_NAME:Ljava/lang/String; = "custom_name"

.field public static final DATA:Ljava/lang/String; = "data"

.field public static final ID:Ljava/lang/String; = "id"

.field public static final IS_NON_CONNECTION_LANDING_TAB:Ljava/lang/String; = "is_non_connection_landing_tab"

.field public static final IS_PERMANENT:Ljava/lang/String; = "is_permanent"

.field public static final LINK:Ljava/lang/String; = "link"

.field public static final NAME:Ljava/lang/String; = "name"

.field public static final POSITION:Ljava/lang/String; = "position"
