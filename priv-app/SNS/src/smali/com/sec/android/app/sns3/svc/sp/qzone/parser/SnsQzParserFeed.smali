.class public Lcom/sec/android/app/sns3/svc/sp/qzone/parser/SnsQzParserFeed;
.super Ljava/lang/Object;
.source "SnsQzParserFeed.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/qzone/parser/SnsQzParserFeed$QzoneFeed;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    return-void
.end method

.method public static parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseFeed;
    .locals 17
    .param p0, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 79
    const/4 v4, 0x0

    .line 80
    .local v4, "feed":Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseFeed;
    const/4 v2, 0x0

    .line 82
    .local v2, "curFeed":Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseFeed;
    const/4 v12, 0x0

    .line 85
    .local v12, "jsonObject":Lorg/json/JSONObject;
    :try_start_0
    new-instance v13, Lorg/json/JSONObject;

    move-object/from16 v0, p0

    invoke-direct {v13, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 87
    .end local v12    # "jsonObject":Lorg/json/JSONObject;
    .local v13, "jsonObject":Lorg/json/JSONObject;
    :try_start_1
    new-instance v10, Lorg/json/JSONObject;

    const-string v15, "data"

    invoke-virtual {v13, v15}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    invoke-direct {v10, v15}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 89
    .local v10, "jdata":Lorg/json/JSONObject;
    const-string v15, "hostopenid"

    invoke-virtual {v10, v15}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 91
    .local v1, "HostID":Ljava/lang/String;
    new-instance v9, Lorg/json/JSONArray;

    const-string v15, "feeds"

    invoke-virtual {v10, v15}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-direct {v9, v15}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 93
    .local v9, "jafeeds":Lorg/json/JSONArray;
    if-eqz v9, :cond_9

    .line 95
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    invoke-virtual {v9}, Lorg/json/JSONArray;->length()I

    move-result v15

    if-ge v6, v15, :cond_9

    .line 96
    new-instance v14, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseFeed;

    invoke-direct {v14}, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseFeed;-><init>()V

    .line 98
    .local v14, "newFeed":Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseFeed;
    invoke-virtual {v9, v6}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v5

    .line 100
    .local v5, "feedJsonObj":Lorg/json/JSONObject;
    const-string v15, "pictureurl"

    invoke-virtual {v5, v15}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_0

    .line 101
    new-instance v8, Lorg/json/JSONArray;

    const-string v15, "pictureurl"

    invoke-virtual {v5, v15}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-direct {v8, v15}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 103
    .local v8, "jaPicture":Lorg/json/JSONArray;
    if-eqz v8, :cond_0

    invoke-virtual {v8}, Lorg/json/JSONArray;->length()I

    move-result v15

    if-lez v15, :cond_0

    .line 104
    invoke-virtual {v8}, Lorg/json/JSONArray;->length()I

    move-result v15

    iput v15, v14, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseFeed;->mPictureCount:I

    .line 105
    const/4 v15, 0x0

    invoke-virtual {v8, v15}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v15

    const-string v16, "pictureurl_origin"

    invoke-virtual/range {v15 .. v16}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    iput-object v15, v14, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseFeed;->mPicture:Ljava/lang/String;

    .line 109
    .end local v8    # "jaPicture":Lorg/json/JSONArray;
    :cond_0
    const-string v15, "original"

    invoke-virtual {v5, v15}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_1

    .line 110
    new-instance v11, Lorg/json/JSONObject;

    const-string v15, "original"

    invoke-virtual {v5, v15}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    invoke-direct {v11, v15}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 111
    .local v11, "jorigin":Lorg/json/JSONObject;
    const-string v15, "originaltitle"

    invoke-virtual {v11, v15}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    iput-object v15, v14, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseFeed;->mOriginalTitle:Ljava/lang/String;

    .line 112
    const-string v15, "originalsummary"

    invoke-virtual {v11, v15}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    iput-object v15, v14, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseFeed;->mOriginalMessage:Ljava/lang/String;

    .line 113
    const-string v15, "originalnickname"

    invoke-virtual {v11, v15}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    iput-object v15, v14, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseFeed;->mOriginalAuthorName:Ljava/lang/String;

    .line 114
    new-instance v7, Lorg/json/JSONArray;

    const-string v15, "pictureurl"

    invoke-virtual {v11, v15}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-direct {v7, v15}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 116
    .local v7, "jaOriginPicture":Lorg/json/JSONArray;
    if-eqz v7, :cond_1

    invoke-virtual {v7}, Lorg/json/JSONArray;->length()I

    move-result v15

    if-lez v15, :cond_1

    .line 117
    invoke-virtual {v7}, Lorg/json/JSONArray;->length()I

    move-result v15

    iput v15, v14, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseFeed;->mOriginalPictureCount:I

    .line 118
    const/4 v15, 0x0

    invoke-virtual {v7, v15}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v15

    const-string v16, "pictureurl_origin"

    invoke-virtual/range {v15 .. v16}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    iput-object v15, v14, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseFeed;->mOriginalPicture:Ljava/lang/String;

    .line 122
    .end local v7    # "jaOriginPicture":Lorg/json/JSONArray;
    .end local v11    # "jorigin":Lorg/json/JSONObject;
    :cond_1
    iget-object v15, v14, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseFeed;->mOriginalMessage:Ljava/lang/String;

    if-eqz v15, :cond_2

    iget-object v15, v14, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseFeed;->mOriginalMessage:Ljava/lang/String;

    invoke-virtual {v15}, Ljava/lang/String;->length()I

    move-result v15

    if-nez v15, :cond_7

    :cond_2
    iget-object v15, v14, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseFeed;->mOriginalAuthorName:Ljava/lang/String;

    if-eqz v15, :cond_3

    iget-object v15, v14, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseFeed;->mOriginalAuthorName:Ljava/lang/String;

    invoke-virtual {v15}, Ljava/lang/String;->length()I

    move-result v15

    if-nez v15, :cond_7

    :cond_3
    iget-object v15, v14, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseFeed;->mOriginalPicture:Ljava/lang/String;

    if-eqz v15, :cond_4

    iget v15, v14, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseFeed;->mOriginalPictureCount:I

    if-nez v15, :cond_7

    .line 126
    :cond_4
    const-string v15, "mood"

    iput-object v15, v14, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseFeed;->mType:Ljava/lang/String;

    .line 131
    :goto_1
    const-string v15, "feedkey"

    invoke-virtual {v5, v15}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    iput-object v15, v14, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseFeed;->mFeedID:Ljava/lang/String;

    .line 132
    const-string v15, "title"

    invoke-virtual {v5, v15}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    iput-object v15, v14, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseFeed;->mTitle:Ljava/lang/String;

    .line 133
    const-string v15, "summary"

    invoke-virtual {v5, v15}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    iput-object v15, v14, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseFeed;->mMessage:Ljava/lang/String;

    .line 134
    const-string v15, "likecount"

    invoke-virtual {v5, v15}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    if-eqz v15, :cond_5

    .line 135
    const-string v15, "likecount"

    invoke-virtual {v5, v15}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    invoke-static {v15}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v15

    iput v15, v14, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseFeed;->mLikeCount:I

    .line 137
    :cond_5
    const-string v15, "commentcount"

    invoke-virtual {v5, v15}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    if-eqz v15, :cond_6

    .line 138
    const-string v15, "commentcount"

    invoke-virtual {v5, v15}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    invoke-static {v15}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v15

    iput v15, v14, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseFeed;->mCommentCount:I

    .line 140
    :cond_6
    const-string v15, "nickname"

    invoke-virtual {v5, v15}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    iput-object v15, v14, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseFeed;->mAuthorName:Ljava/lang/String;

    .line 142
    iput-object v1, v14, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseFeed;->mHostOpenID:Ljava/lang/String;

    .line 143
    const-string v15, "openid"

    invoke-virtual {v5, v15}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    iput-object v15, v14, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseFeed;->mOpenID:Ljava/lang/String;

    .line 144
    const-string v15, "portraiturl"

    invoke-virtual {v5, v15}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    iput-object v15, v14, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseFeed;->mProfileUrl:Ljava/lang/String;

    .line 145
    const-string v15, "detailparam"

    invoke-virtual {v5, v15}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    iput-object v15, v14, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseFeed;->mLinkUri:Ljava/lang/String;

    .line 146
    const-string v15, "time"

    invoke-virtual {v5, v15}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    iput-object v15, v14, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseFeed;->mCreatedTime:Ljava/lang/String;

    .line 147
    if-nez v4, :cond_8

    .line 148
    move-object v4, v14

    .line 149
    move-object v2, v4

    .line 95
    :goto_2
    add-int/lit8 v6, v6, 0x1

    goto/16 :goto_0

    .line 128
    :cond_7
    const-string v15, "share"

    iput-object v15, v14, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseFeed;->mType:Ljava/lang/String;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_1

    .line 157
    .end local v1    # "HostID":Ljava/lang/String;
    .end local v5    # "feedJsonObj":Lorg/json/JSONObject;
    .end local v6    # "i":I
    .end local v9    # "jafeeds":Lorg/json/JSONArray;
    .end local v10    # "jdata":Lorg/json/JSONObject;
    .end local v14    # "newFeed":Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseFeed;
    :catch_0
    move-exception v3

    move-object v12, v13

    .line 159
    .end local v13    # "jsonObject":Lorg/json/JSONObject;
    .local v3, "e":Lorg/json/JSONException;
    .restart local v12    # "jsonObject":Lorg/json/JSONObject;
    :goto_3
    invoke-virtual {v3}, Lorg/json/JSONException;->printStackTrace()V

    .line 164
    .end local v3    # "e":Lorg/json/JSONException;
    :goto_4
    return-object v4

    .line 151
    .end local v12    # "jsonObject":Lorg/json/JSONObject;
    .restart local v1    # "HostID":Ljava/lang/String;
    .restart local v5    # "feedJsonObj":Lorg/json/JSONObject;
    .restart local v6    # "i":I
    .restart local v9    # "jafeeds":Lorg/json/JSONArray;
    .restart local v10    # "jdata":Lorg/json/JSONObject;
    .restart local v13    # "jsonObject":Lorg/json/JSONObject;
    .restart local v14    # "newFeed":Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseFeed;
    :cond_8
    :try_start_2
    iput-object v14, v2, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseFeed;->mNext:Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseFeed;

    .line 152
    iget-object v2, v2, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseFeed;->mNext:Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseFeed;
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_2

    .end local v5    # "feedJsonObj":Lorg/json/JSONObject;
    .end local v6    # "i":I
    .end local v14    # "newFeed":Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseFeed;
    :cond_9
    move-object v12, v13

    .line 162
    .end local v13    # "jsonObject":Lorg/json/JSONObject;
    .restart local v12    # "jsonObject":Lorg/json/JSONObject;
    goto :goto_4

    .line 160
    .end local v1    # "HostID":Ljava/lang/String;
    .end local v9    # "jafeeds":Lorg/json/JSONArray;
    .end local v10    # "jdata":Lorg/json/JSONObject;
    :catch_1
    move-exception v3

    .line 161
    .local v3, "e":Ljava/lang/Exception;
    :goto_5
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_4

    .line 160
    .end local v3    # "e":Ljava/lang/Exception;
    .end local v12    # "jsonObject":Lorg/json/JSONObject;
    .restart local v13    # "jsonObject":Lorg/json/JSONObject;
    :catch_2
    move-exception v3

    move-object v12, v13

    .end local v13    # "jsonObject":Lorg/json/JSONObject;
    .restart local v12    # "jsonObject":Lorg/json/JSONObject;
    goto :goto_5

    .line 157
    :catch_3
    move-exception v3

    goto :goto_3
.end method
