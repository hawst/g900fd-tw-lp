.class public interface abstract Lcom/sec/android/app/sns3/svc/sp/googleplus/parser/SnsGpParserFeed$GooglePlusFeed;
.super Ljava/lang/Object;
.source "SnsGpParserFeed.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/svc/sp/googleplus/parser/SnsGpParserFeed;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "GooglePlusFeed"
.end annotation


# static fields
.field public static final ACTOR:Ljava/lang/String; = "actor"

.field public static final ATTACHMENTS:Ljava/lang/String; = "attachments"

.field public static final FULL_IMAGE:Ljava/lang/String; = "fullImage"

.field public static final HEIGHT:Ljava/lang/String; = "height"

.field public static final ID:Ljava/lang/String; = "id"

.field public static final IMAGE:Ljava/lang/String; = "image"

.field public static final LOCATION:Ljava/lang/String; = "location"

.field public static final LOCATION_LATITUDE:Ljava/lang/String; = "latitude"

.field public static final LOCATION_LONGITUDE:Ljava/lang/String; = "longitude"

.field public static final MESSAGE:Ljava/lang/String; = "title"

.field public static final NAME:Ljava/lang/String; = "displayName"

.field public static final OBJECT:Ljava/lang/String; = "object"

.field public static final PLUSONERS:Ljava/lang/String; = "plusoners"

.field public static final POSITION:Ljava/lang/String; = "position"

.field public static final REPLIES:Ljava/lang/String; = "replies"

.field public static final RESHARES:Ljava/lang/String; = "resharers"

.field public static final SELF_LINK:Ljava/lang/String; = "selfLink"

.field public static final THUMBNAILS:Ljava/lang/String; = "thumbnails"

.field public static final TOTAL_ITEMS:Ljava/lang/String; = "totalItems"

.field public static final TYPE:Ljava/lang/String; = "objectType"

.field public static final UPDATE_TIME:Ljava/lang/String; = "updated"

.field public static final URL:Ljava/lang/String; = "url"

.field public static final VERB:Ljava/lang/String; = "verb"

.field public static final WIDTH:Ljava/lang/String; = "width"
