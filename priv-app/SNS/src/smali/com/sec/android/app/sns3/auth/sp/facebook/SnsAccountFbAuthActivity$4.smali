.class Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity$4;
.super Landroid/webkit/WebViewClient;
.source "SnsAccountFbAuthActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;)V
    .locals 0

    .prologue
    .line 267
    iput-object p1, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity$4;->this$0:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "errorCode"    # I
    .param p3, "description"    # Ljava/lang/String;
    .param p4, "failingUrl"    # Ljava/lang/String;

    .prologue
    .line 441
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity$4;->this$0:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;

    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity$4;->this$0:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;

    # invokes: Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;->handleError(Landroid/content/Context;I)V
    invoke-static {v0, v1, p2}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;->access$800(Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;Landroid/content/Context;I)V

    .line 442
    invoke-super {p0, p1, p2, p3, p4}, Landroid/webkit/WebViewClient;->onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V

    .line 443
    return-void
.end method

.method public shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 17
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "url"    # Ljava/lang/String;

    .prologue
    .line 271
    invoke-static {}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->isLoggable()Z

    move-result v13

    if-eqz v13, :cond_0

    .line 272
    # getter for: Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;->access$000()Ljava/lang/String;

    move-result-object v13

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "####### WebView shouldOverrideUrlLoading URL : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p2

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 275
    :cond_0
    if-eqz p2, :cond_2

    const-string v13, "fbconnect://success?error_reason=user_denied"

    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_2

    .line 276
    invoke-virtual/range {p1 .. p1}, Landroid/webkit/WebView;->stopLoading()V

    .line 277
    invoke-virtual/range {p1 .. p1}, Landroid/webkit/WebView;->clearView()V

    .line 279
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity$4;->this$0:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;

    invoke-virtual {v13}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;->finish()V

    .line 435
    :cond_1
    :goto_0
    const/4 v13, 0x0

    :goto_1
    return v13

    .line 281
    :cond_2
    if-eqz p2, :cond_8

    const-string v13, "fbconnect://success"

    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_8

    .line 282
    invoke-virtual/range {p1 .. p1}, Landroid/webkit/WebView;->stopLoading()V

    .line 283
    invoke-virtual/range {p1 .. p1}, Landroid/webkit/WebView;->clearView()V

    .line 285
    const-string v13, "fbconnect://success?"

    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v13

    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v11

    .line 286
    .local v11, "subUrl":Ljava/lang/String;
    const-string v13, "&"

    invoke-virtual {v11, v13}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    .line 287
    .local v9, "splitUrl":[Ljava/lang/String;
    const/4 v10, 0x0

    .line 288
    .local v10, "state":Ljava/lang/String;
    const/4 v4, 0x0

    .line 290
    .local v4, "code":Ljava/lang/String;
    move-object v2, v9

    .local v2, "arr$":[Ljava/lang/String;
    array-length v7, v2

    .local v7, "len$":I
    const/4 v5, 0x0

    .local v5, "i$":I
    :goto_2
    if-ge v5, v7, :cond_5

    aget-object v8, v2, v5

    .line 291
    .local v8, "parameter":Ljava/lang/String;
    const-string v13, "="

    invoke-virtual {v8, v13}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v12

    .line 292
    .local v12, "v":[Ljava/lang/String;
    const/4 v13, 0x0

    aget-object v13, v12, v13

    invoke-static {v13}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    const-string v14, "state"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_4

    .line 293
    const/4 v13, 0x1

    aget-object v13, v12, v13

    invoke-static {v13}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 290
    :cond_3
    :goto_3
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 295
    :cond_4
    const/4 v13, 0x0

    aget-object v13, v12, v13

    invoke-static {v13}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    const-string v14, "code"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_3

    .line 296
    const/4 v13, 0x1

    aget-object v13, v12, v13

    invoke-static {v13}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    goto :goto_3

    .line 299
    .end local v8    # "parameter":Ljava/lang/String;
    .end local v12    # "v":[Ljava/lang/String;
    :cond_5
    invoke-static {}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->isLoggable()Z

    move-result v13

    if-eqz v13, :cond_6

    .line 300
    # getter for: Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;->access$000()Ljava/lang/String;

    move-result-object v13

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "SnsAccountFbAuthActivity : shouldOverrideUrlLoading() - state = "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ", code = "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 305
    :cond_6
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity$4;->this$0:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;

    # setter for: Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;->mCode:Ljava/lang/String;
    invoke-static {v13, v4}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;->access$402(Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 307
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity$4;->this$0:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;

    # getter for: Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;->mState:Ljava/lang/String;
    invoke-static {v13}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;->access$500(Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v13, v10}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_7

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity$4;->this$0:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;

    # getter for: Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;->mCode:Ljava/lang/String;
    invoke-static {v13}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;->access$400(Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;)Ljava/lang/String;

    move-result-object v13

    if-eqz v13, :cond_7

    .line 309
    new-instance v3, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAccessToken;

    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v13

    invoke-virtual {v13}, Lcom/sec/android/app/sns3/SnsApplication;->getSvcMgr()Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v13

    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v14

    invoke-virtual {v14}, Lcom/sec/android/app/sns3/SnsApplication;->getAgentMgr()Lcom/sec/android/app/sns3/agent/SnsAgentMgr;

    move-result-object v14

    invoke-virtual {v14}, Lcom/sec/android/app/sns3/agent/SnsAgentMgr;->getCommandMgr()Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;

    move-result-object v14

    invoke-virtual {v14}, Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;->getCommandMgrHandle()Landroid/os/Handler;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity$4;->this$0:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;

    # getter for: Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;->mCode:Ljava/lang/String;
    invoke-static {v15}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;->access$400(Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;)Ljava/lang/String;

    move-result-object v15

    invoke-direct {v3, v13, v14, v15}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAccessToken;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Handler;Ljava/lang/String;)V

    .line 313
    .local v3, "cmd":Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;
    new-instance v13, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity$4$1;

    move-object/from16 v0, p0

    invoke-direct {v13, v0}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity$4$1;-><init>(Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity$4;)V

    invoke-virtual {v3, v13}, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->setCommandCallback(Lcom/sec/android/app/sns3/agent/command/ISnsCommandCallback;)Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;

    .line 410
    invoke-virtual {v3}, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->send()I

    goto/16 :goto_0

    .line 414
    .end local v3    # "cmd":Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;
    :cond_7
    # getter for: Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;->access$000()Ljava/lang/String;

    move-result-object v13

    const-string v14, "SnsAccountFbAuthActivity - state is wrong!!"

    invoke-static {v13, v14}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 416
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity$4;->this$0:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;

    const/4 v14, -0x1

    const/4 v15, -0x1

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;->loginFail(IILjava/lang/String;)V

    goto/16 :goto_0

    .line 419
    .end local v2    # "arr$":[Ljava/lang/String;
    .end local v4    # "code":Ljava/lang/String;
    .end local v5    # "i$":I
    .end local v7    # "len$":I
    .end local v9    # "splitUrl":[Ljava/lang/String;
    .end local v10    # "state":Ljava/lang/String;
    .end local v11    # "subUrl":Ljava/lang/String;
    :cond_8
    if-eqz p2, :cond_9

    const-string v13, "fbconnect://cancel"

    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_9

    .line 420
    new-instance v1, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity$UIDialogThreadRunnable;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity$4;->this$0:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;

    # getter for: Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;->mAppContext:Landroid/content/Context;
    invoke-static {v13}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;->access$600(Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;)Landroid/content/Context;

    move-result-object v13

    const/4 v14, 0x0

    invoke-direct {v1, v13, v14}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity$UIDialogThreadRunnable;-><init>(Landroid/content/Context;I)V

    .line 422
    .local v1, "another":Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity$UIDialogThreadRunnable;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity$4;->this$0:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;

    # getter for: Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;->mHandler:Landroid/os/Handler;
    invoke-static {v13}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;->access$700(Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;)Landroid/os/Handler;

    move-result-object v13

    invoke-virtual {v13, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 424
    const/4 v13, 0x1

    goto/16 :goto_1

    .line 425
    .end local v1    # "another":Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity$UIDialogThreadRunnable;
    :cond_9
    if-eqz p2, :cond_1

    const-string v13, "market://details"

    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_1

    .line 426
    invoke-virtual/range {p1 .. p1}, Landroid/webkit/WebView;->stopLoading()V

    .line 428
    const-string v13, "&"

    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    .line 430
    .restart local v9    # "splitUrl":[Ljava/lang/String;
    new-instance v6, Landroid/content/Intent;

    const-string v13, "android.intent.action.VIEW"

    invoke-direct {v6, v13}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 431
    .local v6, "intent":Landroid/content/Intent;
    const/4 v13, 0x0

    aget-object v13, v9, v13

    invoke-static {v13}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v13

    invoke-virtual {v6, v13}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 432
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity$4;->this$0:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;

    invoke-virtual {v13, v6}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0
.end method
