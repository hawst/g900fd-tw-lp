.class Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsServiceForAuthToken$1;
.super Lcom/sec/android/app/sns3/svc/sp/foursquare/auth/api/ISnsFourSquareForAuthToken$Stub;
.source "SnsFsServiceForAuthToken.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsServiceForAuthToken;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsServiceForAuthToken;


# direct methods
.method constructor <init>(Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsServiceForAuthToken;)V
    .locals 0

    .prologue
    .line 83
    iput-object p1, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsServiceForAuthToken$1;->this$0:Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsServiceForAuthToken;

    invoke-direct {p0}, Lcom/sec/android/app/sns3/svc/sp/foursquare/auth/api/ISnsFourSquareForAuthToken$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public getAuthTokenInfo()Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 88
    const-string v2, "SNS"

    const-string v3, "SnsFsServiceForAuthToken : getAuthTokenNExpires() CALLED !!"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 90
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 92
    .local v0, "Info":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v2, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsServiceForAuthToken$1;->this$0:Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsServiceForAuthToken;

    # getter for: Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsServiceForAuthToken;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    invoke-static {v2}, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsServiceForAuthToken;->access$000(Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsServiceForAuthToken;)Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/sns3/svc/SnsSvcMgr;->getTokenMgr()Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;

    move-result-object v2

    const-string v3, "foursquare"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;->getToken(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsToken;

    .line 94
    .local v1, "fsToken":Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsToken;
    const-string v2, "client_id"

    sget-object v3, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFourSquare;->CLIENT_ID:Ljava/lang/String;

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 95
    const-string v2, "client_secret"

    sget-object v3, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFourSquare;->CLIENT_SECRET:Ljava/lang/String;

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 96
    const-string v2, "access_token"

    invoke-virtual {v1}, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsToken;->getAccessToken()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 98
    return-object v0
.end method

.method public getMyAccountInfo(Lcom/sec/android/app/sns3/svc/sp/foursquare/auth/api/ISnsFourSquareCallbackMyAccountInfo;)I
    .locals 8
    .param p1, "cb"    # Lcom/sec/android/app/sns3/svc/sp/foursquare/auth/api/ISnsFourSquareCallbackMyAccountInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 105
    const-string v3, "SNS"

    const-string v4, "getMyAccountInfo CALLED!!!"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 107
    new-instance v1, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsServiceForAuthToken$1$1;

    iget-object v3, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsServiceForAuthToken$1;->this$0:Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsServiceForAuthToken;

    # getter for: Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsServiceForAuthToken;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    invoke-static {v3}, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsServiceForAuthToken;->access$000(Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsServiceForAuthToken;)Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v3

    const-string v4, "self"

    invoke-direct {v1, p0, v3, v4, p1}, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsServiceForAuthToken$1$1;-><init>(Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsServiceForAuthToken$1;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;Lcom/sec/android/app/sns3/svc/sp/foursquare/auth/api/ISnsFourSquareCallbackMyAccountInfo;)V

    .line 133
    .local v1, "req":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    invoke-virtual {v1}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->request()Z

    move-result v0

    .line 134
    .local v0, "isRequested":Z
    if-nez v0, :cond_0

    .line 135
    invoke-virtual {v1}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->getReqID()I

    move-result v2

    .line 136
    .local v2, "reqId":I
    iget-object v3, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsServiceForAuthToken$1;->this$0:Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsServiceForAuthToken;

    # getter for: Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsServiceForAuthToken;->mHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsServiceForAuthToken;->access$100(Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsServiceForAuthToken;)Landroid/os/Handler;

    move-result-object v3

    new-instance v4, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsServiceForAuthToken$1$2;

    invoke-direct {v4, p0, p1, v2}, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsServiceForAuthToken$1$2;-><init>(Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsServiceForAuthToken$1;Lcom/sec/android/app/sns3/svc/sp/foursquare/auth/api/ISnsFourSquareCallbackMyAccountInfo;I)V

    const-wide/16 v6, 0x64

    invoke-virtual {v3, v4, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 150
    .end local v2    # "reqId":I
    :cond_0
    invoke-virtual {v1}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->getReqID()I

    move-result v3

    return v3
.end method
