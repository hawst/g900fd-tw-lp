.class Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserFeed$CommandCallback$1;
.super Ljava/lang/Object;
.source "SnsFbParserFeed.java"

# interfaces
.implements Lcom/sec/android/app/sns3/agent/command/ISnsCommandCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserFeed$CommandCallback;->onCmdRespond(IZLjava/lang/String;Ljava/util/List;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserFeed$CommandCallback;

.field final synthetic val$cr:Landroid/content/ContentResolver;

.field final synthetic val$values:Landroid/content/ContentValues;


# direct methods
.method constructor <init>(Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserFeed$CommandCallback;Landroid/content/ContentValues;Landroid/content/ContentResolver;)V
    .locals 0

    .prologue
    .line 246
    iput-object p1, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserFeed$CommandCallback$1;->this$0:Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserFeed$CommandCallback;

    iput-object p2, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserFeed$CommandCallback$1;->val$values:Landroid/content/ContentValues;

    iput-object p3, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserFeed$CommandCallback$1;->val$cr:Landroid/content/ContentResolver;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCmdRespond(IZLjava/lang/String;Ljava/util/List;)V
    .locals 14
    .param p1, "cmdID"    # I
    .param p2, "bSuccess"    # Z
    .param p3, "uri"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IZ",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 251
    .local p4, "responseList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;>;"
    const/4 v11, 0x0

    .line 252
    .local v11, "reason":Landroid/os/Bundle;
    const/4 v9, 0x0

    .line 253
    .local v9, "photoID":Ljava/lang/String;
    const/4 v10, 0x0

    .line 255
    .local v10, "postID":Ljava/lang/String;
    if-eqz p4, :cond_7

    .line 256
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_0
    invoke-interface/range {p4 .. p4}, Ljava/util/List;->size()I

    move-result v1

    if-ge v8, v1, :cond_4

    .line 257
    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserFeed$CommandCallback$1;->val$values:Landroid/content/ContentValues;

    invoke-virtual {v1}, Landroid/content/ContentValues;->clear()V

    .line 258
    move-object/from16 v0, p4

    invoke-interface {v0, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;

    invoke-virtual {v1}, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;->getReason()Landroid/os/Bundle;

    move-result-object v11

    .line 260
    if-eqz v11, :cond_0

    .line 261
    const-string v1, "photo_id"

    invoke-virtual {v11, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 262
    const-string v1, "post_id"

    invoke-virtual {v11, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 266
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserFeed$CommandCallback$1;->val$cr:Landroid/content/ContentResolver;

    sget-object v2, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$Photo;->CONTENT_URI:Landroid/net/Uri;

    const/4 v3, 0x0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "id=\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 269
    .local v7, "cursor":Landroid/database/Cursor;
    if-eqz v7, :cond_8

    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_8

    .line 270
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 272
    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserFeed$CommandCallback$1;->val$values:Landroid/content/ContentValues;

    const-string v2, "feed_id"

    invoke-virtual {v1, v2, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 273
    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserFeed$CommandCallback$1;->val$values:Landroid/content/ContentValues;

    const-string v2, "photo_id"

    invoke-virtual {v1, v2, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 275
    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserFeed$CommandCallback$1;->val$values:Landroid/content/ContentValues;

    const-string v2, "picture"

    const-string v3, "source"

    invoke-interface {v7, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v7, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 277
    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserFeed$CommandCallback$1;->val$values:Landroid/content/ContentValues;

    const-string v2, "created_time"

    const-string v3, "created_time"

    invoke-interface {v7, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v7, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 279
    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserFeed$CommandCallback$1;->val$values:Landroid/content/ContentValues;

    const-string v2, "updated_time"

    const-string v3, "updated_time"

    invoke-interface {v7, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v7, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 281
    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserFeed$CommandCallback$1;->val$values:Landroid/content/ContentValues;

    const-string v2, "width"

    const-string v3, "width"

    invoke-interface {v7, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v7, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 283
    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserFeed$CommandCallback$1;->val$values:Landroid/content/ContentValues;

    const-string v2, "height"

    const-string v3, "height"

    invoke-interface {v7, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v7, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 286
    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserFeed$CommandCallback$1;->val$cr:Landroid/content/ContentResolver;

    sget-object v2, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$AlbumContents;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Landroid/content/ContentResolver;->getType(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 287
    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserFeed$CommandCallback$1;->val$cr:Landroid/content/ContentResolver;

    sget-object v2, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$AlbumContents;->CONTENT_URI:Landroid/net/Uri;

    iget-object v3, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserFeed$CommandCallback$1;->val$values:Landroid/content/ContentValues;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "photo_id = \'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_1

    .line 290
    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserFeed$CommandCallback$1;->val$cr:Landroid/content/ContentResolver;

    sget-object v2, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$AlbumContents;->CONTENT_URI:Landroid/net/Uri;

    iget-object v3, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserFeed$CommandCallback$1;->val$values:Landroid/content/ContentValues;

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 300
    :cond_1
    sget-object v1, Lcom/sec/android/app/sns3/app/life/SnsLifeResource;->TAG:Ljava/lang/String;

    const-string v2, "getFacebookAlbumContents() cursor has data "

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 307
    :goto_1
    if-eqz v7, :cond_2

    .line 308
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 256
    :cond_2
    add-int/lit8 v8, v8, 0x1

    goto/16 :goto_0

    .line 293
    :cond_3
    sget-object v1, Lcom/sec/android/app/sns3/app/life/SnsLifeResource;->TAG:Ljava/lang/String;

    const-string v2, "Content URI FBALBUM not available"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 295
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 312
    .end local v7    # "cursor":Landroid/database/Cursor;
    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserFeed$CommandCallback$1;->val$cr:Landroid/content/ContentResolver;

    sget-object v2, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$AlbumContents;->CONTENT_URI:Landroid/net/Uri;

    const/4 v3, 0x0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "feed_id=\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserFeed$CommandCallback$1;->this$0:Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserFeed$CommandCallback;

    iget-object v5, v5, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserFeed$CommandCallback;->id:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 315
    .restart local v7    # "cursor":Landroid/database/Cursor;
    if-eqz v7, :cond_a

    .line 316
    sget-object v1, Lcom/sec/android/app/sns3/app/life/SnsLifeResource;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Album cursor length "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 318
    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserFeed$CommandCallback$1;->val$values:Landroid/content/ContentValues;

    invoke-virtual {v1}, Landroid/content/ContentValues;->clear()V

    .line 319
    const-string v13, ""

    .line 320
    .local v13, "url":Ljava/lang/String;
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 322
    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserFeed$CommandCallback$1;->val$values:Landroid/content/ContentValues;

    const-string v2, "post_id"

    const-string v3, "feed_id"

    invoke-interface {v7, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v7, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 324
    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserFeed$CommandCallback$1;->val$values:Landroid/content/ContentValues;

    const-string v2, "width"

    const-string v3, "width"

    invoke-interface {v7, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v7, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 326
    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserFeed$CommandCallback$1;->val$values:Landroid/content/ContentValues;

    const-string v2, "height"

    const-string v3, "height"

    invoke-interface {v7, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v7, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 328
    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserFeed$CommandCallback$1;->val$values:Landroid/content/ContentValues;

    const-string v2, "updated_time"

    const-string v3, "updated_time"

    invoke-interface {v7, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v7, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 333
    :cond_5
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "picture"

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    .line 336
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_5

    .line 338
    invoke-interface {v7}, Landroid/database/Cursor;->moveToPrevious()Z

    .line 340
    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserFeed$CommandCallback$1;->val$values:Landroid/content/ContentValues;

    const-string v2, "album_contents"

    invoke-virtual {v1, v2, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 342
    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserFeed$CommandCallback$1;->val$cr:Landroid/content/ContentResolver;

    sget-object v2, Lcom/sec/android/app/sns3/agent/life/db/SnsLifeDB$LifeTimeline;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Landroid/content/ContentResolver;->getType(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_9

    .line 344
    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserFeed$CommandCallback$1;->val$cr:Landroid/content/ContentResolver;

    sget-object v2, Lcom/sec/android/app/sns3/agent/life/db/SnsLifeDB$LifeTimeline;->CONTENT_URI:Landroid/net/Uri;

    iget-object v3, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserFeed$CommandCallback$1;->val$values:Landroid/content/ContentValues;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "post_id = \'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserFeed$CommandCallback$1;->this$0:Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserFeed$CommandCallback;

    iget-object v5, v5, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserFeed$CommandCallback;->id:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v12

    .line 346
    .local v12, "update":I
    if-nez v12, :cond_6

    .line 347
    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserFeed$CommandCallback$1;->val$cr:Landroid/content/ContentResolver;

    sget-object v2, Lcom/sec/android/app/sns3/agent/life/db/SnsLifeDB$LifeTimeline;->CONTENT_URI:Landroid/net/Uri;

    iget-object v3, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserFeed$CommandCallback$1;->val$values:Landroid/content/ContentValues;

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 353
    .end local v12    # "update":I
    :cond_6
    :goto_2
    sget-object v1, Lcom/sec/android/app/sns3/app/life/SnsLifeResource;->TAG:Ljava/lang/String;

    const-string v2, "getAlbumFB() cursor has data "

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 354
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 360
    .end local v7    # "cursor":Landroid/database/Cursor;
    .end local v8    # "i":I
    .end local v13    # "url":Ljava/lang/String;
    :cond_7
    :goto_3
    return-void

    .line 303
    .restart local v7    # "cursor":Landroid/database/Cursor;
    .restart local v8    # "i":I
    :cond_8
    sget-object v1, Lcom/sec/android/app/sns3/app/life/SnsLifeResource;->TAG:Ljava/lang/String;

    const-string v2, "getFacebookAlbumContents() cursor no data"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 350
    .restart local v13    # "url":Ljava/lang/String;
    :cond_9
    sget-object v1, Lcom/sec/android/app/sns3/app/life/SnsLifeResource;->TAG:Ljava/lang/String;

    const-string v2, "Content URI FBALBUM not available"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 356
    .end local v13    # "url":Ljava/lang/String;
    :cond_a
    sget-object v1, Lcom/sec/android/app/sns3/app/life/SnsLifeResource;->TAG:Ljava/lang/String;

    const-string v2, "getAlbumFB() cursor no data"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3
.end method
