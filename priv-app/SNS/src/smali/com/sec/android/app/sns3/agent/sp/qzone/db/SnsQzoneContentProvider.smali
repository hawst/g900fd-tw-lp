.class public Lcom/sec/android/app/sns3/agent/sp/qzone/db/SnsQzoneContentProvider;
.super Landroid/content/ContentProvider;
.source "SnsQzoneContentProvider.java"


# static fields
.field private static final BIRTHDAY:I = 0x190

.field private static final FEED_LIST:I = 0xc8

.field private static final USER:I = 0x12c

.field private static final USER_TIMELINES:I = 0x64

.field private static final WIPE_QZ_DATA:I = 0x7d0

.field private static final uriMatcher:Landroid/content/UriMatcher;


# instance fields
.field private mOpenHelper:Lcom/sec/android/app/sns3/agent/sp/qzone/db/SnsQzoneDBHelper;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 273
    new-instance v0, Landroid/content/UriMatcher;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    sput-object v0, Lcom/sec/android/app/sns3/agent/sp/qzone/db/SnsQzoneContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    .line 274
    sget-object v0, Lcom/sec/android/app/sns3/agent/sp/qzone/db/SnsQzoneContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.sns3.sp.qzone"

    const-string v2, "usertimeline"

    const/16 v3, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 275
    sget-object v0, Lcom/sec/android/app/sns3/agent/sp/qzone/db/SnsQzoneContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.sns3.sp.qzone"

    const-string v2, "feed_list"

    const/16 v3, 0xc8

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 277
    sget-object v0, Lcom/sec/android/app/sns3/agent/sp/qzone/db/SnsQzoneContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.sns3.sp.qzone"

    const-string v2, "user"

    const/16 v3, 0x12c

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 278
    sget-object v0, Lcom/sec/android/app/sns3/agent/sp/qzone/db/SnsQzoneContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.sns3.sp.qzone"

    const-string v2, "birthday"

    const/16 v3, 0x190

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 280
    sget-object v0, Lcom/sec/android/app/sns3/agent/sp/qzone/db/SnsQzoneContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.sns3.sp.qzone"

    const-string v2, "wipe_qz_data"

    const/16 v3, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 281
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    .line 38
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/sns3/agent/sp/qzone/db/SnsQzoneContentProvider;->mOpenHelper:Lcom/sec/android/app/sns3/agent/sp/qzone/db/SnsQzoneDBHelper;

    return-void
.end method


# virtual methods
.method public bulkInsert(Landroid/net/Uri;[Landroid/content/ContentValues;)I
    .locals 4
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # [Landroid/content/ContentValues;

    .prologue
    .line 179
    const/4 v0, 0x0

    .line 180
    .local v0, "cnt":I
    array-length v2, p2

    .line 182
    .local v2, "numValues":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_1

    .line 183
    aget-object v3, p2, v1

    invoke-virtual {p0, p1, v3}, Lcom/sec/android/app/sns3/agent/sp/qzone/db/SnsQzoneContentProvider;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 184
    add-int/lit8 v0, v0, 0x1

    .line 182
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 188
    :cond_1
    return v0
.end method

.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 6
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "selection"    # Ljava/lang/String;
    .param p3, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 194
    const/4 v1, 0x0

    .line 195
    .local v1, "db":Landroid/database/sqlite/SQLiteDatabase;
    sget-object v3, Lcom/sec/android/app/sns3/agent/sp/qzone/db/SnsQzoneContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v3, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v2

    .line 196
    .local v2, "match":I
    const/4 v0, -0x1

    .line 198
    .local v0, "count":I
    iget-object v3, p0, Lcom/sec/android/app/sns3/agent/sp/qzone/db/SnsQzoneContentProvider;->mOpenHelper:Lcom/sec/android/app/sns3/agent/sp/qzone/db/SnsQzoneDBHelper;

    invoke-virtual {v3}, Lcom/sec/android/app/sns3/agent/sp/qzone/db/SnsQzoneDBHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 200
    sparse-switch v2, :sswitch_data_0

    .line 228
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Unknown URI : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 203
    :sswitch_0
    const-string v3, "usertimeline"

    invoke-static {v1, v3, v4, v4}, Lcom/sec/android/app/sns3/agent/db/SnsDBWrapper;->onDelete(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 231
    :goto_0
    return v0

    .line 208
    :sswitch_1
    const-string v3, "feed_list"

    invoke-static {v1, v3, p2, p3}, Lcom/sec/android/app/sns3/agent/db/SnsDBWrapper;->onDelete(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 210
    goto :goto_0

    .line 213
    :sswitch_2
    const-string v3, "user"

    invoke-static {v1, v3, p2, p3}, Lcom/sec/android/app/sns3/agent/db/SnsDBWrapper;->onDelete(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 215
    goto :goto_0

    .line 218
    :sswitch_3
    const-string v3, "birthday"

    invoke-static {v1, v3, p2, p3}, Lcom/sec/android/app/sns3/agent/db/SnsDBWrapper;->onDelete(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 220
    goto :goto_0

    .line 223
    :sswitch_4
    iget-object v3, p0, Lcom/sec/android/app/sns3/agent/sp/qzone/db/SnsQzoneContentProvider;->mOpenHelper:Lcom/sec/android/app/sns3/agent/sp/qzone/db/SnsQzoneDBHelper;

    invoke-virtual {v3, v1}, Lcom/sec/android/app/sns3/agent/sp/qzone/db/SnsQzoneDBHelper;->wipeData(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_0

    .line 200
    nop

    :sswitch_data_0
    .sparse-switch
        0x64 -> :sswitch_0
        0xc8 -> :sswitch_1
        0x12c -> :sswitch_2
        0x190 -> :sswitch_3
        0x7d0 -> :sswitch_4
    .end sparse-switch
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 3
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 111
    sget-object v0, Lcom/sec/android/app/sns3/agent/sp/qzone/db/SnsQzoneContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 130
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown URI : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 114
    :sswitch_0
    const-string v0, "vnd.android.cursor.dir/vnd.com.sec.android.app.sns3.sp.twitter.usertimeline"

    .line 126
    :goto_0
    return-object v0

    .line 118
    :sswitch_1
    const-string v0, "vnd.android.cursor.dir/vnd.com.sec.android.app.sns3.sp.qzone.feedlist"

    goto :goto_0

    .line 122
    :sswitch_2
    const-string v0, "vnd.android.cursor.dir/vnd.com.sec.android.app.sns3.sp.qzone.user"

    goto :goto_0

    .line 126
    :sswitch_3
    const-string v0, "vnd.android.cursor.dir/vnd.com.sec.android.app.sns3.sp.qzone.birthday"

    goto :goto_0

    .line 111
    :sswitch_data_0
    .sparse-switch
        0x64 -> :sswitch_0
        0xc8 -> :sswitch_1
        0x12c -> :sswitch_2
        0x190 -> :sswitch_3
    .end sparse-switch
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 9
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;

    .prologue
    const/4 v3, 0x0

    .line 137
    const/4 v0, 0x0

    .line 138
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    sget-object v6, Lcom/sec/android/app/sns3/agent/sp/qzone/db/SnsQzoneContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v6, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v2

    .line 139
    .local v2, "match":I
    const-wide/16 v4, -0x1

    .line 142
    .local v4, "rowId":J
    :try_start_0
    iget-object v6, p0, Lcom/sec/android/app/sns3/agent/sp/qzone/db/SnsQzoneContentProvider;->mOpenHelper:Lcom/sec/android/app/sns3/agent/sp/qzone/db/SnsQzoneDBHelper;

    invoke-virtual {v6}, Lcom/sec/android/app/sns3/agent/sp/qzone/db/SnsQzoneDBHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 144
    sparse-switch v2, :sswitch_data_0

    .line 167
    new-instance v6, Ljava/lang/IllegalArgumentException;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Unknown URI : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v6
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 169
    :catch_0
    move-exception v1

    .line 170
    .local v1, "e":Landroid/database/sqlite/SQLiteException;
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    .line 173
    .end local v1    # "e":Landroid/database/sqlite/SQLiteException;
    :goto_0
    const-wide/16 v6, -0x1

    cmp-long v6, v4, v6

    if-nez v6, :cond_0

    move-object p1, v3

    .end local p1    # "uri":Landroid/net/Uri;
    :cond_0
    return-object p1

    .line 147
    .restart local p1    # "uri":Landroid/net/Uri;
    :sswitch_0
    :try_start_1
    const-string v6, "usertimeline"

    const/4 v7, 0x0

    invoke-virtual {v0, v6, v7, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v4

    .line 149
    goto :goto_0

    .line 152
    :sswitch_1
    const-string v6, "feed_list"

    const/4 v7, 0x0

    invoke-virtual {v0, v6, v7, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v4

    .line 154
    goto :goto_0

    .line 157
    :sswitch_2
    const-string v6, "user"

    const/4 v7, 0x0

    invoke-virtual {v0, v6, v7, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v4

    .line 159
    goto :goto_0

    .line 162
    :sswitch_3
    const-string v6, "birthday"

    const/4 v7, 0x0

    invoke-virtual {v0, v6, v7, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-wide v4

    .line 164
    goto :goto_0

    .line 144
    :sswitch_data_0
    .sparse-switch
        0x64 -> :sswitch_0
        0xc8 -> :sswitch_1
        0x12c -> :sswitch_2
        0x190 -> :sswitch_3
    .end sparse-switch
.end method

.method public onCreate()Z
    .locals 2

    .prologue
    .line 54
    new-instance v0, Lcom/sec/android/app/sns3/agent/sp/qzone/db/SnsQzoneDBHelper;

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/qzone/db/SnsQzoneContentProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/app/sns3/agent/sp/qzone/db/SnsQzoneDBHelper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/sns3/agent/sp/qzone/db/SnsQzoneContentProvider;->mOpenHelper:Lcom/sec/android/app/sns3/agent/sp/qzone/db/SnsQzoneDBHelper;

    .line 55
    const/4 v0, 0x0

    return v0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 11
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "sortOrder"    # Ljava/lang/String;

    .prologue
    .line 60
    const/4 v1, 0x0

    .line 61
    .local v1, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v8, 0x0

    .line 63
    .local v8, "c":Landroid/database/Cursor;
    sget-object v2, Lcom/sec/android/app/sns3/agent/sp/qzone/db/SnsQzoneContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v2, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v10

    .line 65
    .local v10, "match":I
    const/4 v5, 0x0

    .line 67
    .local v5, "groupby":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/app/sns3/agent/sp/qzone/db/SnsQzoneContentProvider;->mOpenHelper:Lcom/sec/android/app/sns3/agent/sp/qzone/db/SnsQzoneDBHelper;

    invoke-virtual {v2}, Lcom/sec/android/app/sns3/agent/sp/qzone/db/SnsQzoneDBHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 68
    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 70
    .local v0, "queryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    sparse-switch v10, :sswitch_data_0

    .line 94
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unknown URI : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 73
    :sswitch_0
    const-string v2, "feed_list"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 99
    :goto_0
    const/4 v6, 0x0

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object/from16 v7, p5

    :try_start_0
    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v8

    .line 105
    :goto_1
    return-object v8

    .line 78
    :sswitch_1
    const-string v2, "user"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto :goto_0

    .line 83
    :sswitch_2
    const-string v2, "birthday"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 84
    const-string p5, "day ASC"

    .line 86
    goto :goto_0

    .line 89
    :sswitch_3
    const-string v2, "usertimeline"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto :goto_0

    .line 101
    :catch_0
    move-exception v9

    .line 102
    .local v9, "e":Landroid/database/sqlite/SQLiteException;
    invoke-virtual {v9}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    goto :goto_1

    .line 70
    nop

    :sswitch_data_0
    .sparse-switch
        0x64 -> :sswitch_3
        0xc8 -> :sswitch_0
        0x12c -> :sswitch_1
        0x190 -> :sswitch_2
    .end sparse-switch
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 6
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 236
    const/4 v0, 0x0

    .line 237
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v2, 0x0

    .line 238
    .local v2, "table":Ljava/lang/String;
    sget-object v3, Lcom/sec/android/app/sns3/agent/sp/qzone/db/SnsQzoneContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v3, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v1

    .line 240
    .local v1, "match":I
    iget-object v3, p0, Lcom/sec/android/app/sns3/agent/sp/qzone/db/SnsQzoneContentProvider;->mOpenHelper:Lcom/sec/android/app/sns3/agent/sp/qzone/db/SnsQzoneDBHelper;

    invoke-virtual {v3}, Lcom/sec/android/app/sns3/agent/sp/qzone/db/SnsQzoneDBHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 242
    sparse-switch v1, :sswitch_data_0

    .line 265
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Unknown URI : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 245
    :sswitch_0
    const-string v2, "usertimeline"

    .line 268
    :goto_0
    invoke-static {v0, v2, p2, p3, p4}, Lcom/sec/android/app/sns3/agent/db/SnsDBWrapper;->onUpdate(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v3

    return v3

    .line 250
    :sswitch_1
    const-string v2, "feed_list"

    .line 252
    goto :goto_0

    .line 255
    :sswitch_2
    const-string v2, "user"

    .line 257
    goto :goto_0

    .line 260
    :sswitch_3
    const-string v2, "birthday"

    .line 262
    goto :goto_0

    .line 242
    :sswitch_data_0
    .sparse-switch
        0x64 -> :sswitch_0
        0xc8 -> :sswitch_1
        0x12c -> :sswitch_2
        0x190 -> :sswitch_3
    .end sparse-switch
.end method
