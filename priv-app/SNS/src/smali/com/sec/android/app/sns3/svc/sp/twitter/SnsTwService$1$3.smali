.class Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService$1$3;
.super Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwReqGetFriendsIds;
.source "SnsTwService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService$1;->getUsersLookup(Landroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/twitter/api/ISnsTwitterCallbackUser;)I
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService$1;


# direct methods
.method constructor <init>(Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService$1;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Bundle;)V
    .locals 0
    .param p2, "x0"    # Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    .param p3, "x1"    # Landroid/os/Bundle;

    .prologue
    .line 249
    iput-object p1, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService$1$3;->this$1:Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService$1;

    invoke-direct {p0, p2, p3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwReqGetFriendsIds;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Bundle;)V

    return-void
.end method


# virtual methods
.method public onReqRespond(IZIILandroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseCursor;)Z
    .locals 4
    .param p1, "reqID"    # I
    .param p2, "bSuccess"    # Z
    .param p3, "httpStatus"    # I
    .param p4, "errorCode"    # I
    .param p5, "reason"    # Landroid/os/Bundle;
    .param p6, "cursor"    # Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseCursor;

    .prologue
    .line 257
    if-eqz p6, :cond_1

    .line 258
    const/16 v1, 0x64

    .line 260
    .local v1, "size":I
    const/16 v2, 0x64

    if-gt v1, v2, :cond_0

    .line 261
    iget-object v2, p6, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseCursor;->mIds:Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStringArray;

    iget-object v2, v2, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStringArray;->mStringArray:[Ljava/lang/String;

    array-length v1, v2

    .line 263
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService$1$3;->this$1:Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService$1;

    new-array v3, v1, [Ljava/lang/String;

    # setter for: Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService$1;->mStringArray:[Ljava/lang/String;
    invoke-static {v2, v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService$1;->access$102(Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService$1;[Ljava/lang/String;)[Ljava/lang/String;

    .line 265
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_1

    .line 266
    iget-object v2, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService$1$3;->this$1:Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService$1;

    # getter for: Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService$1;->mStringArray:[Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService$1;->access$100(Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService$1;)[Ljava/lang/String;

    move-result-object v2

    iget-object v3, p6, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseCursor;->mIds:Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStringArray;

    iget-object v3, v3, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStringArray;->mStringArray:[Ljava/lang/String;

    aget-object v3, v3, v0

    aput-object v3, v2, v0

    .line 265
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 270
    .end local v0    # "i":I
    .end local v1    # "size":I
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService$1$3;->this$1:Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService$1;

    iget-object v2, v2, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService$1;->this$0:Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService;

    # getter for: Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService;->mLock:Ljava/lang/Object;
    invoke-static {v2}, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService;->access$200(Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService;)Ljava/lang/Object;

    move-result-object v3

    monitor-enter v3

    .line 271
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService$1$3;->this$1:Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService$1;

    iget-object v2, v2, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService$1;->this$0:Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService;

    # getter for: Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService;->mLock:Ljava/lang/Object;
    invoke-static {v2}, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService;->access$200(Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->notify()V

    .line 272
    monitor-exit v3

    .line 273
    const/4 v2, 0x0

    return v2

    .line 272
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method
