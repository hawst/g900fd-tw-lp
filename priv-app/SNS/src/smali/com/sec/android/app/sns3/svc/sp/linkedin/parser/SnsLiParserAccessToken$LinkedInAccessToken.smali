.class public interface abstract Lcom/sec/android/app/sns3/svc/sp/linkedin/parser/SnsLiParserAccessToken$LinkedInAccessToken;
.super Ljava/lang/Object;
.source "SnsLiParserAccessToken.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/svc/sp/linkedin/parser/SnsLiParserAccessToken;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "LinkedInAccessToken"
.end annotation


# static fields
.field public static final ACCESS_TOKEN:Ljava/lang/String; = "\"access_token\""

.field public static final EXPIRES:Ljava/lang/String; = "\"expires_in\""
