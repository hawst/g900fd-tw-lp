.class public Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseRecommendedVenues;
.super Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;
.source "SnsFsResponseRecommendedVenues.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseRecommendedVenues$Explore;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseRecommendedVenues;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public mExploreValues:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseRecommendedVenues$Explore;",
            ">;"
        }
    .end annotation
.end field

.field public mHeaderLocation:Ljava/lang/String;

.field public mHeaderLocationGranularity:Ljava/lang/String;

.field public mHeaderMessage:Ljava/lang/String;

.field public mQuery:Ljava/lang/String;

.field public mSuggestedRadius:Ljava/lang/String;

.field public mWarning:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 103
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseRecommendedVenues$1;

    invoke-direct {v0}, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseRecommendedVenues$1;-><init>()V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseRecommendedVenues;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 114
    invoke-direct {p0}, Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;-><init>()V

    .line 115
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseRecommendedVenues;->mExploreValues:Ljava/util/List;

    .line 116
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 119
    invoke-direct {p0}, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseRecommendedVenues;-><init>()V

    .line 120
    invoke-virtual {p0, p1}, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseRecommendedVenues;->readFromParcel(Landroid/os/Parcel;)V

    .line 121
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseRecommendedVenues$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseRecommendedVenues$1;

    .prologue
    .line 12
    invoke-direct {p0, p1}, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseRecommendedVenues;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 126
    const/4 v0, 0x0

    return v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 141
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseRecommendedVenues;->mSuggestedRadius:Ljava/lang/String;

    .line 142
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseRecommendedVenues;->mHeaderLocation:Ljava/lang/String;

    .line 143
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseRecommendedVenues;->mHeaderLocationGranularity:Ljava/lang/String;

    .line 144
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseRecommendedVenues;->mHeaderMessage:Ljava/lang/String;

    .line 145
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseRecommendedVenues;->mQuery:Ljava/lang/String;

    .line 146
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseRecommendedVenues;->mExploreValues:Ljava/util/List;

    const-class v1, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseRecommendedVenues$Explore;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readList(Ljava/util/List;Ljava/lang/ClassLoader;)V

    .line 147
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseRecommendedVenues;->mWarning:Ljava/lang/String;

    .line 148
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 152
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SnsFsResponseRecommendedVenues [mSuggestedRadius="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseRecommendedVenues;->mSuggestedRadius:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mHeaderLocation="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseRecommendedVenues;->mHeaderLocation:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mHeaderLocationGranularity="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseRecommendedVenues;->mHeaderLocationGranularity:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mHeaderMessage="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseRecommendedVenues;->mHeaderMessage:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mQuery="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseRecommendedVenues;->mQuery:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mExploreValues="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseRecommendedVenues;->mExploreValues:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mWarning="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseRecommendedVenues;->mWarning:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flag"    # I

    .prologue
    .line 131
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseRecommendedVenues;->mSuggestedRadius:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 132
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseRecommendedVenues;->mHeaderLocation:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 133
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseRecommendedVenues;->mHeaderLocationGranularity:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 134
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseRecommendedVenues;->mHeaderMessage:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 135
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseRecommendedVenues;->mQuery:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 136
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseRecommendedVenues;->mExploreValues:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 137
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseRecommendedVenues;->mWarning:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 138
    return-void
.end method
