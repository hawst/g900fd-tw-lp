.class public final Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseFriends;
.super Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;
.source "SnsInResponseFriends.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseFriends;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public mFriends:Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseProfile;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseFriends$1;

    invoke-direct {v0}, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseFriends$1;-><init>()V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseFriends;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;-><init>()V

    .line 42
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;-><init>()V

    .line 45
    invoke-virtual {p0, p1}, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseFriends;->readFromParcel(Landroid/os/Parcel;)V

    .line 46
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseFriends$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseFriends$1;

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseFriends;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 59
    const/4 v0, 0x0

    return v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 54
    const-class v0, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseProfile;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseProfile;

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseFriends;->mFriends:Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseProfile;

    .line 55
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flag"    # I

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseFriends;->mFriends:Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseProfile;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 51
    return-void
.end method
