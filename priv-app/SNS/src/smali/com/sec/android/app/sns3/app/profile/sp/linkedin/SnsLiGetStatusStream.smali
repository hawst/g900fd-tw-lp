.class public Lcom/sec/android/app/sns3/app/profile/sp/linkedin/SnsLiGetStatusStream;
.super Ljava/lang/Object;
.source "SnsLiGetStatusStream.java"


# static fields
.field private static final QUERY_LIMIT:I = 0xa

.field private static final QUERY_PERIOD:I = 0x2e2480

.field protected static final TAG:Ljava/lang/String; = "SnsLiGetStatusStream"

.field private static mCmdHandler:Landroid/os/Handler;

.field private static mServiceMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static fromCurrenttoUTCTimestamp()Ljava/lang/Long;
    .locals 6

    .prologue
    .line 107
    new-instance v0, Ljava/util/GregorianCalendar;

    invoke-direct {v0}, Ljava/util/GregorianCalendar;-><init>()V

    .line 108
    .local v0, "calendar":Ljava/util/Calendar;
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    return-object v1
.end method

.method private static invokeBroadcast(Landroid/content/Context;ILjava/lang/String;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "resultCode"    # I
    .param p2, "userId"    # Ljava/lang/String;

    .prologue
    .line 112
    const-string v1, "SnsLiGetStatusStream"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "invokeBroadcast() : resultCode = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 113
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.app.sns.profile.ACTION_LINKEDIN_UPDATED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 114
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 115
    const-string v1, "result"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 116
    const-string v1, "com.sec.android.app.sns3.permission.RECEIVE_SNS_BROADCAST"

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    .line 117
    return-void
.end method

.method public static updateStatus(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "userId"    # Ljava/lang/String;

    .prologue
    .line 57
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/SnsApplication;->getSvcMgr()Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/sns3/app/profile/sp/linkedin/SnsLiGetStatusStream;->mServiceMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    .line 58
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/SnsApplication;->getAgentMgr()Lcom/sec/android/app/sns3/agent/SnsAgentMgr;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/agent/SnsAgentMgr;->getCommandMgr()Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;->getCommandMgrHandle()Landroid/os/Handler;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/sns3/app/profile/sp/linkedin/SnsLiGetStatusStream;->mCmdHandler:Landroid/os/Handler;

    .line 60
    sget-object v0, Lcom/sec/android/app/sns3/app/profile/sp/linkedin/SnsLiGetStatusStream;->mServiceMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    if-nez v0, :cond_0

    .line 61
    const-string v0, "SnsLiGetStatusStream"

    const-string v1, "[SNS] mSvcMgr is null !!"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 104
    :goto_0
    return-void

    .line 64
    :cond_0
    sget-object v0, Lcom/sec/android/app/sns3/app/profile/sp/linkedin/SnsLiGetStatusStream;->mCmdHandler:Landroid/os/Handler;

    if-nez v0, :cond_1

    .line 65
    const-string v0, "SnsLiGetStatusStream"

    const-string v1, "[SNS] mCmdHandler is null !!"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 103
    :cond_1
    const/4 v0, -0x1

    invoke-static {p0, v0, p1}, Lcom/sec/android/app/sns3/app/profile/sp/linkedin/SnsLiGetStatusStream;->invokeBroadcast(Landroid/content/Context;ILjava/lang/String;)V

    goto :goto_0
.end method
