.class public Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService;
.super Landroid/app/Service;
.source "SnsFsSyncAdapterProfileService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService$SyncAdapterImpl;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "SnsFsSync"


# instance fields
.field private bProfileSyncSuccess:Z

.field private mAccount:Landroid/accounts/Account;

.field private mAuthority:Ljava/lang/String;

.field private mCmdHandler:Landroid/os/Handler;

.field private mContext:Landroid/content/Context;

.field private mFsToken:Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;

.field private mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

.field private mSyncAdapter:Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService$SyncAdapterImpl;

.field private mSyncResult:Landroid/content/SyncResult;

.field private mSyncState:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 57
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 61
    iput-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService;->mSyncAdapter:Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService$SyncAdapterImpl;

    .line 63
    iput v1, p0, Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService;->mSyncState:I

    .line 65
    iput-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService;->mSyncResult:Landroid/content/SyncResult;

    .line 67
    iput-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService;->mAccount:Landroid/accounts/Account;

    .line 69
    iput-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService;->mAuthority:Ljava/lang/String;

    .line 75
    iput-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService;->mFsToken:Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;

    .line 77
    iput-boolean v1, p0, Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService;->bProfileSyncSuccess:Z

    .line 79
    return-void
.end method

.method static synthetic access$002(Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService;Landroid/content/Context;)Landroid/content/Context;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService;
    .param p1, "x1"    # Landroid/content/Context;

    .prologue
    .line 57
    iput-object p1, p0, Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService;->mContext:Landroid/content/Context;

    return-object p1
.end method

.method static synthetic access$102(Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService;Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;)Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService;
    .param p1, "x1"    # Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;

    .prologue
    .line 57
    iput-object p1, p0, Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService;->mFsToken:Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;

    return-object p1
.end method

.method static synthetic access$202(Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService;Landroid/accounts/Account;)Landroid/accounts/Account;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService;
    .param p1, "x1"    # Landroid/accounts/Account;

    .prologue
    .line 57
    iput-object p1, p0, Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService;->mAccount:Landroid/accounts/Account;

    return-object p1
.end method

.method static synthetic access$302(Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 57
    iput-object p1, p0, Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService;->mAuthority:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$402(Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService;Landroid/content/SyncResult;)Landroid/content/SyncResult;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService;
    .param p1, "x1"    # Landroid/content/SyncResult;

    .prologue
    .line 57
    iput-object p1, p0, Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService;->mSyncResult:Landroid/content/SyncResult;

    return-object p1
.end method

.method static synthetic access$500(Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/accounts/OperationCanceledException;
        }
    .end annotation

    .prologue
    .line 57
    invoke-direct {p0}, Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService;->performSync()V

    return-void
.end method

.method static synthetic access$600(Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService;

    .prologue
    .line 57
    invoke-direct {p0}, Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService;->onSyncCanceled()V

    return-void
.end method

.method static synthetic access$702(Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService;
    .param p1, "x1"    # I

    .prologue
    .line 57
    iput p1, p0, Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService;->mSyncState:I

    return p1
.end method

.method private handleSessionExpired()I
    .locals 11

    .prologue
    const/4 v9, 0x1

    const/4 v10, 0x0

    .line 274
    iget-object v7, p0, Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService;->mAccount:Landroid/accounts/Account;

    const-string v8, "com.sec.android.app.sns3.life"

    invoke-static {v7, v8}, Landroid/content/ContentResolver;->cancelSync(Landroid/accounts/Account;Ljava/lang/String;)V

    .line 276
    iget-object v7, p0, Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService;->mContext:Landroid/content/Context;

    const-string v8, "notification"

    invoke-virtual {v7, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/app/NotificationManager;

    .line 279
    .local v4, "notiMgr":Landroid/app/NotificationManager;
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 281
    .local v3, "notiIntent":Landroid/content/Intent;
    const-string v7, "com.sec.android.app.sns3.RETRY_SSO_FOURSQUARE"

    invoke-virtual {v3, v7}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 285
    const/high16 v7, 0x800000

    invoke-virtual {v3, v7}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 286
    const-string v7, "RetryLogin"

    invoke-virtual {v3, v7, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 287
    iget-object v7, p0, Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService;->mContext:Landroid/content/Context;

    invoke-static {v7, v10, v3, v10}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 289
    .local v1, "launchIntent":Landroid/app/PendingIntent;
    const v6, 0x7f08003f

    .line 290
    .local v6, "titleID":I
    const v5, 0x7f080021

    .line 291
    .local v5, "spID":I
    const v0, 0x7f02001b

    .line 293
    .local v0, "iconID":I
    new-instance v2, Landroid/app/Notification$Builder;

    iget-object v7, p0, Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService;->mContext:Landroid/content/Context;

    invoke-direct {v2, v7}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    .line 294
    .local v2, "notiBuilder":Landroid/app/Notification$Builder;
    iget-object v7, p0, Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService;->mContext:Landroid/content/Context;

    new-array v8, v9, [Ljava/lang/Object;

    iget-object v9, p0, Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService;->mContext:Landroid/content/Context;

    invoke-virtual {v9, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v10

    invoke-virtual {v7, v6, v8}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v7

    invoke-virtual {v7, v0}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService;->mContext:Landroid/content/Context;

    const v9, 0x7f08003e

    invoke-virtual {v8, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v7

    invoke-virtual {v7, v1}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v7

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Landroid/app/Notification$Builder;->setWhen(J)Landroid/app/Notification$Builder;

    .line 299
    const/16 v7, 0x10cc

    invoke-virtual {v2}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v8

    invoke-virtual {v4, v7, v8}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 301
    const/4 v7, -0x1

    return v7
.end method

.method private invokeBroadcast()V
    .locals 7

    .prologue
    .line 327
    const/4 v2, 0x0

    .line 329
    .local v2, "intent":Landroid/content/Intent;
    sget-object v3, Lcom/sec/android/app/sns3/agent/sp/foursquare/db/SnsFourSquareDB$UserFsFeedInfo;->CONTENT_URI:Landroid/net/Uri;

    .line 330
    .local v3, "uri":Landroid/net/Uri;
    const-string v0, "com.sec.android.app.sns3.action.SYNC_FS_PROFILES"

    .line 331
    .local v0, "action":Ljava/lang/String;
    iget v4, p0, Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService;->mSyncState:I

    const/4 v5, 0x2

    if-ne v4, v5, :cond_1

    const/4 v1, 0x1

    .line 333
    .local v1, "bResult":Z
    :goto_0
    new-instance v2, Landroid/content/Intent;

    .end local v2    # "intent":Landroid/content/Intent;
    invoke-direct {v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 334
    .restart local v2    # "intent":Landroid/content/Intent;
    const-string v4, "SNS3_CONTENT_URI_PROFILE"

    invoke-virtual {v2, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 335
    const-string v4, "SNS_RESULT"

    invoke-virtual {v2, v4, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 337
    const-string v4, "com.sec.android.app.sns3.permission.RECEIVE_SNS_BROADCAST"

    invoke-virtual {p0, v2, v4}, Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    .line 338
    invoke-static {}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->isLoggable()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 339
    const-string v4, "SnsFsSync"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "SnsFsSyncAdapterProfileService - invokeBroadcast() : action = ["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "], uri = ["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "], result = ["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 342
    :cond_0
    return-void

    .line 331
    .end local v1    # "bResult":Z
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private onSyncCanceled()V
    .locals 2

    .prologue
    .line 318
    const-string v0, "SnsFsSync"

    const-string v1, "***************** SnsFsSyncAdapterProfileService : onSyncCanceled !!! *****************"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 321
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService;->mSyncState:I

    .line 323
    return-void
.end method

.method private performFriendsSync()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/accounts/OperationCanceledException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 225
    iput v3, p0, Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService;->mSyncState:I

    .line 227
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService;->mFsToken:Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;

    invoke-virtual {v2}, Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;->getTokenState()I

    move-result v2

    if-eq v2, v3, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService;->mFsToken:Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;

    invoke-virtual {v2}, Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;->getTokenState()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    .line 229
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService;->handleSessionExpired()I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService;->mSyncState:I

    .line 230
    new-instance v2, Ljava/lang/Exception;

    const-string v3, "Session expired or invalid!!!"

    invoke-direct {v2, v3}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 257
    :catch_0
    move-exception v1

    .line 258
    .local v1, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 259
    const-string v2, "SnsFsSync"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SnsFsSyncAdapterProfileService : EXCEPTION !!! "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 261
    iget-object v2, p0, Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService;->mSyncResult:Landroid/content/SyncResult;

    iget-object v2, v2, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    const-wide/16 v4, 0x1

    iput-wide v4, v2, Landroid/content/SyncStats;->numConflictDetectedExceptions:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 265
    invoke-direct {p0}, Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService;->invokeBroadcast()V

    .line 267
    const-string v2, "SnsFsSync"

    const-string v3, "***************** SnsFsSyncAdapterProfileService : performFriendsSync - FINISHED !!! *****************"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 271
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_0
    return-void

    .line 232
    :cond_1
    :try_start_2
    new-instance v0, Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdGetFriends;

    iget-object v2, p0, Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    iget-object v3, p0, Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService;->mCmdHandler:Landroid/os/Handler;

    const-string v4, "self"

    invoke-direct {v0, v2, v3, v4}, Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdGetFriends;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Handler;Ljava/lang/String;)V

    .line 233
    .local v0, "cmd":Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;
    new-instance v2, Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService$2;

    invoke-direct {v2, p0}, Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService$2;-><init>(Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService;)V

    invoke-virtual {v0, v2}, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->setCommandCallback(Lcom/sec/android/app/sns3/agent/command/ISnsCommandCallback;)Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;

    .line 249
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->send()I

    .line 251
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService;->suspendSync()V

    .line 254
    iget v2, p0, Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService;->mSyncState:I

    const/4 v3, -0x1

    if-ne v2, v3, :cond_2

    .line 255
    new-instance v2, Ljava/lang/Exception;

    const-string v3, "[SnsFsSyncAdapterProfileService] updates sync is failed!!!"

    invoke-direct {v2, v3}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 265
    .end local v0    # "cmd":Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;
    :catchall_0
    move-exception v2

    invoke-direct {p0}, Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService;->invokeBroadcast()V

    .line 267
    const-string v3, "SnsFsSync"

    const-string v4, "***************** SnsFsSyncAdapterProfileService : performFriendsSync - FINISHED !!! *****************"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    throw v2

    .line 265
    .restart local v0    # "cmd":Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService;->invokeBroadcast()V

    .line 267
    const-string v2, "SnsFsSync"

    const-string v3, "***************** SnsFsSyncAdapterProfileService : performFriendsSync - FINISHED !!! *****************"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private performSync()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/accounts/OperationCanceledException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 165
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/sns3/SnsApplication;->getAgentMgr()Lcom/sec/android/app/sns3/agent/SnsAgentMgr;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/sns3/agent/SnsAgentMgr;->getCommandMgr()Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;->getCommandMgrHandle()Landroid/os/Handler;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService;->mCmdHandler:Landroid/os/Handler;

    .line 167
    iput v3, p0, Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService;->mSyncState:I

    .line 170
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService;->mFsToken:Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;

    invoke-virtual {v2}, Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;->getTokenState()I

    move-result v2

    if-eq v2, v3, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService;->mFsToken:Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;

    invoke-virtual {v2}, Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;->getTokenState()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_2

    .line 172
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService;->handleSessionExpired()I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService;->mSyncState:I

    .line 173
    new-instance v2, Ljava/lang/Exception;

    const-string v3, "Session expired or invalid!!!"

    invoke-direct {v2, v3}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 205
    :catch_0
    move-exception v1

    .line 206
    .local v1, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 207
    const-string v2, "SnsFsSync"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SnsFsSyncAdapterProfileService : EXCEPTION !!! "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 209
    iget-object v2, p0, Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService;->mSyncResult:Landroid/content/SyncResult;

    iget-object v2, v2, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    const-wide/16 v4, 0x1

    iput-wide v4, v2, Landroid/content/SyncStats;->numConflictDetectedExceptions:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 213
    iget-boolean v2, p0, Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService;->bProfileSyncSuccess:Z

    if-eqz v2, :cond_1

    .line 214
    invoke-direct {p0}, Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService;->performFriendsSync()V

    .line 217
    :cond_1
    const-string v2, "SnsFsSync"

    const-string v3, "***************** SnsFsSyncAdapterProfileService : performSync - FINISHED !!! *****************"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 221
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_0
    return-void

    .line 176
    :cond_2
    :try_start_2
    new-instance v0, Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdGetProfile;

    iget-object v2, p0, Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    iget-object v3, p0, Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService;->mCmdHandler:Landroid/os/Handler;

    const-string v4, "me"

    invoke-direct {v0, v2, v3, v4}, Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdGetProfile;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Handler;Ljava/lang/String;)V

    .line 177
    .local v0, "cmd":Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;
    new-instance v2, Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService$1;

    invoke-direct {v2, p0}, Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService$1;-><init>(Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService;)V

    invoke-virtual {v0, v2}, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->setCommandCallback(Lcom/sec/android/app/sns3/agent/command/ISnsCommandCallback;)Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;

    .line 193
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->send()I

    .line 195
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService;->suspendSync()V

    .line 197
    iget v2, p0, Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService;->mSyncState:I

    const/4 v3, -0x1

    if-ne v2, v3, :cond_4

    .line 198
    new-instance v2, Ljava/lang/Exception;

    const-string v3, "[SnsFsSyncAdapterProfileService] updates sync is failed!!!"

    invoke-direct {v2, v3}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 213
    .end local v0    # "cmd":Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;
    :catchall_0
    move-exception v2

    iget-boolean v3, p0, Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService;->bProfileSyncSuccess:Z

    if-eqz v3, :cond_3

    .line 214
    invoke-direct {p0}, Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService;->performFriendsSync()V

    .line 217
    :cond_3
    const-string v3, "SnsFsSync"

    const-string v4, "***************** SnsFsSyncAdapterProfileService : performSync - FINISHED !!! *****************"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    throw v2

    .line 201
    .restart local v0    # "cmd":Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;
    :cond_4
    const/4 v2, 0x1

    :try_start_3
    iput-boolean v2, p0, Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService;->bProfileSyncSuccess:Z
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 213
    iget-boolean v2, p0, Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService;->bProfileSyncSuccess:Z

    if-eqz v2, :cond_5

    .line 214
    invoke-direct {p0}, Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService;->performFriendsSync()V

    .line 217
    :cond_5
    const-string v2, "SnsFsSync"

    const-string v3, "***************** SnsFsSyncAdapterProfileService : performSync - FINISHED !!! *****************"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 134
    const-string v0, "SnsFsSync"

    const-string v1, "***************** SnsFsSyncAdapterProfileService : onBind !!! *****************"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 137
    iget-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService;->mSyncAdapter:Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService$SyncAdapterImpl;

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService$SyncAdapterImpl;->getSyncAdapterBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 121
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 123
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/SnsApplication;->getSvcMgr()Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    .line 125
    iget-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService;->mSyncAdapter:Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService$SyncAdapterImpl;

    if-nez v0, :cond_0

    .line 126
    new-instance v0, Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService$SyncAdapterImpl;

    invoke-direct {v0, p0, p0}, Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService$SyncAdapterImpl;-><init>(Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService;->mSyncAdapter:Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService$SyncAdapterImpl;

    .line 129
    :cond_0
    return-void
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 143
    const/4 v0, 0x0

    .line 144
    .local v0, "bActive":Z
    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService;->mAccount:Landroid/accounts/Account;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService;->mAuthority:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 145
    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService;->mAccount:Landroid/accounts/Account;

    iget-object v2, p0, Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService;->mAuthority:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/content/ContentResolver;->isSyncActive(Landroid/accounts/Account;Ljava/lang/String;)Z

    move-result v0

    .line 148
    :cond_0
    const-string v1, "SnsFsSync"

    const-string v2, "***************** SnsFsSyncAdapterProfileService : onUnbind !!! *****************"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 151
    if-eqz v0, :cond_2

    iget v1, p0, Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService;->mSyncState:I

    const/4 v2, 0x1

    if-eq v1, v2, :cond_1

    iget v1, p0, Lcom/sec/android/app/sns3/sync/sp/foursquare/SnsFsSyncAdapterProfileService;->mSyncState:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_2

    .line 153
    :cond_1
    const-string v1, "SnsFsSync"

    const-string v2, "SnsFsSyncAdapterProfileService : onUnbind : SYNC ERROR : performSync was stopped by forced abort or pending problem!!!"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 159
    :goto_0
    invoke-super {p0, p1}, Landroid/app/Service;->onUnbind(Landroid/content/Intent;)Z

    move-result v1

    return v1

    .line 157
    :cond_2
    const-string v1, "SnsFsSync"

    const-string v2, "SnsFsSyncAdapterProfileService : onUnbind : COMPLETE STATE!!!"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public declared-synchronized resumeSync()V
    .locals 1

    .prologue
    .line 313
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->notify()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 314
    monitor-exit p0

    return-void

    .line 313
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized suspendSync()V
    .locals 2

    .prologue
    .line 306
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 310
    :goto_0
    monitor-exit p0

    return-void

    .line 307
    :catch_0
    move-exception v0

    .line 308
    .local v0, "e":Ljava/lang/InterruptedException;
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 306
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method
