.class Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService$1$4;
.super Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestUsers;
.source "SnsTwService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService$1;->getUsersLookup(Landroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/twitter/api/ISnsTwitterCallbackUser;)I
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService$1;

.field final synthetic val$cb:Lcom/sec/android/app/sns3/svc/sp/twitter/api/ISnsTwitterCallbackUser;


# direct methods
.method constructor <init>(Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService$1;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwUsersAPI;Landroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/twitter/api/ISnsTwitterCallbackUser;)V
    .locals 0
    .param p2, "x0"    # Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    .param p3, "x1"    # Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwUsersAPI;
    .param p4, "x2"    # Landroid/os/Bundle;

    .prologue
    .line 292
    iput-object p1, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService$1$4;->this$1:Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService$1;

    iput-object p5, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService$1$4;->val$cb:Lcom/sec/android/app/sns3/svc/sp/twitter/api/ISnsTwitterCallbackUser;

    invoke-direct {p0, p2, p3, p4}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestUsers;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwUsersAPI;Landroid/os/Bundle;)V

    return-void
.end method


# virtual methods
.method public onReqRespond(IZIILandroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseUser;)Z
    .locals 8
    .param p1, "reqID"    # I
    .param p2, "bSuccess"    # Z
    .param p3, "httpStatus"    # I
    .param p4, "errorCode"    # I
    .param p5, "reason"    # Landroid/os/Bundle;
    .param p6, "user"    # Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseUser;

    .prologue
    .line 298
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService$1$4;->val$cb:Lcom/sec/android/app/sns3/svc/sp/twitter/api/ISnsTwitterCallbackUser;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-interface/range {v0 .. v6}, Lcom/sec/android/app/sns3/svc/sp/twitter/api/ISnsTwitterCallbackUser;->onReqRespond(IZIILandroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseUser;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 303
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService$1$4;->this$1:Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService$1;

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService$1;->this$0:Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService;

    # getter for: Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService;->mLock:Ljava/lang/Object;
    invoke-static {v0}, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService;->access$200(Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 304
    :try_start_1
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService$1$4;->this$1:Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService$1;

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService$1;->this$0:Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService;

    # getter for: Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService;->mLock:Ljava/lang/Object;
    invoke-static {v0}, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService;->access$200(Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwService;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 305
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 306
    const/4 v0, 0x0

    return v0

    .line 299
    :catch_0
    move-exception v7

    .line 300
    .local v7, "e":Landroid/os/RemoteException;
    invoke-virtual {v7}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 305
    .end local v7    # "e":Landroid/os/RemoteException;
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0
.end method
