.class interface abstract Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition$LinkedInPosition;
.super Ljava/lang/Object;
.source "SnsLiResponsePosition.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x608
    name = "LinkedInPosition"
.end annotation


# static fields
.field public static final ARRAY_VALUE:Ljava/lang/String; = "values"

.field public static final COMPANY:Ljava/lang/String; = "company"

.field public static final END_DATE:Ljava/lang/String; = "endDate"

.field public static final ID:Ljava/lang/String; = "id"

.field public static final INDUSTRY:Ljava/lang/String; = "industry"

.field public static final IS_CURRENT:Ljava/lang/String; = "isCurrent"

.field public static final MONTH:Ljava/lang/String; = "month"

.field public static final NAME:Ljava/lang/String; = "name"

.field public static final SIZE:Ljava/lang/String; = "size"

.field public static final START_DATE:Ljava/lang/String; = "startDate"

.field public static final SUMMARY:Ljava/lang/String; = "summary"

.field public static final TITLE:Ljava/lang/String; = "title"

.field public static final TYPE:Ljava/lang/String; = "type"

.field public static final YEAR:Ljava/lang/String; = "year"
