.class public interface abstract Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$RecommendationUpdatesColumns;
.super Ljava/lang/Object;
.source "SnsLinkedInDB.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "RecommendationUpdatesColumns"
.end annotation


# static fields
.field public static final RECOMMENEDATION_ID:Ljava/lang/String; = "recommendation_id"

.field public static final RECOMMENEDATION_TYPE:Ljava/lang/String; = "recommendation_type"

.field public static final RECOMMENEDATION_UPDATE_KEY:Ljava/lang/String; = "recommendation_update_key"

.field public static final RECOMMENEDATION_URL:Ljava/lang/String; = "recommendation_url"

.field public static final RECOMMENEDEE_ID:Ljava/lang/String; = "recommendee_id"

.field public static final RECOMMENEDER_ID:Ljava/lang/String; = "recommender_id"
