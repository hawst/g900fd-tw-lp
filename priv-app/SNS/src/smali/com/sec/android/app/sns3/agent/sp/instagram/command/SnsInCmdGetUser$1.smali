.class Lcom/sec/android/app/sns3/agent/sp/instagram/command/SnsInCmdGetUser$1;
.super Lcom/sec/android/app/sns3/svc/sp/instagram/request/SnsInReqGetProfile;
.source "SnsInCmdGetUser.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/sns3/agent/sp/instagram/command/SnsInCmdGetUser;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Handler;Ljava/lang/String;Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/sns3/agent/sp/instagram/command/SnsInCmdGetUser;


# direct methods
.method constructor <init>(Lcom/sec/android/app/sns3/agent/sp/instagram/command/SnsInCmdGetUser;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 0
    .param p2, "x0"    # Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    .param p3, "x1"    # Ljava/lang/String;
    .param p4, "x2"    # Landroid/os/Bundle;

    .prologue
    .line 50
    iput-object p1, p0, Lcom/sec/android/app/sns3/agent/sp/instagram/command/SnsInCmdGetUser$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/instagram/command/SnsInCmdGetUser;

    invoke-direct {p0, p2, p3, p4}, Lcom/sec/android/app/sns3/svc/sp/instagram/request/SnsInReqGetProfile;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;Landroid/os/Bundle;)V

    return-void
.end method


# virtual methods
.method public onReqRespond(IZIILandroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseProfile;)Z
    .locals 9
    .param p1, "reqID"    # I
    .param p2, "bSuccess"    # Z
    .param p3, "httpStatus"    # I
    .param p4, "errorCode"    # I
    .param p5, "reason"    # Landroid/os/Bundle;
    .param p6, "user"    # Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseProfile;

    .prologue
    const/4 v8, 0x1

    .line 55
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 56
    .local v2, "values":Landroid/content/ContentValues;
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/sns3/SnsApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 58
    .local v0, "cr":Landroid/content/ContentResolver;
    if-eqz p2, :cond_1

    .line 59
    if-eqz p6, :cond_0

    .line 60
    move-object v1, p6

    .line 61
    .local v1, "curUser":Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseProfile;
    sget-object v3, Lcom/sec/android/app/sns3/agent/sp/instagram/db/SnsInstagramDB$UserInfo;->CONTENT_URI:Landroid/net/Uri;

    const-string v4, "profile_id=?"

    new-array v5, v8, [Ljava/lang/String;

    const/4 v6, 0x0

    iget-object v7, v1, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseProfile;->mUserID:Ljava/lang/String;

    aput-object v7, v5, v6

    invoke-virtual {v0, v3, v4, v5}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 65
    invoke-virtual {v2}, Landroid/content/ContentValues;->clear()V

    .line 66
    const-string v3, "profile_id"

    iget-object v4, v1, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseProfile;->mUserID:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    const-string v3, "profile_name"

    iget-object v4, v1, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseProfile;->mUserName:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    const-string v3, "full_name"

    iget-object v4, v1, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseProfile;->mFullName:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    const-string v3, "profile_image"

    iget-object v4, v1, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseProfile;->mImageUrl:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    const-string v3, "profile_website"

    iget-object v4, v1, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseProfile;->mWebsite:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    const-string v3, "profile_bio"

    iget-object v4, v1, Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseProfile;->mBio:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    sget-object v3, Lcom/sec/android/app/sns3/agent/sp/instagram/db/SnsInstagramDB$UserInfo;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v3, v2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 75
    .end local v1    # "curUser":Lcom/sec/android/app/sns3/svc/sp/instagram/response/SnsInResponseProfile;
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/sns3/agent/sp/instagram/command/SnsInCmdGetUser$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/instagram/command/SnsInCmdGetUser;

    sget-object v4, Lcom/sec/android/app/sns3/agent/sp/instagram/db/SnsInstagramDB$UserInfo;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/sns3/agent/sp/instagram/command/SnsInCmdGetUser;->setUri(Ljava/lang/String;)V

    .line 82
    :goto_0
    iget-object v3, p0, Lcom/sec/android/app/sns3/agent/sp/instagram/command/SnsInCmdGetUser$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/instagram/command/SnsInCmdGetUser;

    invoke-virtual {v3, p2}, Lcom/sec/android/app/sns3/agent/sp/instagram/command/SnsInCmdGetUser;->setSuccess(Z)V

    .line 83
    iget-object v3, p0, Lcom/sec/android/app/sns3/agent/sp/instagram/command/SnsInCmdGetUser$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/instagram/command/SnsInCmdGetUser;

    invoke-virtual {v3, p0}, Lcom/sec/android/app/sns3/agent/sp/instagram/command/SnsInCmdGetUser;->receive(Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;)V

    .line 85
    return v8

    .line 77
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/sns3/agent/sp/instagram/command/SnsInCmdGetUser$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/instagram/command/SnsInCmdGetUser;

    new-instance v4, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;

    const-string v5, "instagram"

    invoke-direct {v4, v5, p3, p4, p5}, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;-><init>(Ljava/lang/String;IILandroid/os/Bundle;)V

    invoke-virtual {v3, v4}, Lcom/sec/android/app/sns3/agent/sp/instagram/command/SnsInCmdGetUser;->setResponseList(Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;)Z

    .line 79
    iget-object v3, p0, Lcom/sec/android/app/sns3/agent/sp/instagram/command/SnsInCmdGetUser$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/instagram/command/SnsInCmdGetUser;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/sec/android/app/sns3/agent/sp/instagram/command/SnsInCmdGetUser;->setUri(Ljava/lang/String;)V

    goto :goto_0
.end method
