.class public Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbAppIdManager;
.super Ljava/lang/Object;
.source "SnsFbAppIdManager.java"


# static fields
.field private static final DEFAULT_APP_ID:Ljava/lang/String; = "245238665537618"

.field private static final DEFAULT_SECRET_KEY:Ljava/lang/String; = "54dcbd8d838d23e500c3f855e473bbcf"

.field private static final DEPRECATED_APP_IDS:[Ljava/lang/String;

.field private static final PROPERTIES_FILE:Ljava/lang/String; = "/etc/snsfb.conf"

.field private static final TAG:Ljava/lang/String; = "SnsFbAppIdManager"

.field private static mInstance:Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbAppIdManager;


# instance fields
.field private mAppId:Ljava/lang/String;

.field private mSecretKey:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 37
    const/16 v0, 0xb

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "332892496761788"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "375898912469864"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "456506484371985"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "311659368932617"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "449384625142391"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "120739854787279"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "405660436219623"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "295438057145899"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "132315146953977"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "406744956065504"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "805175096164823"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbAppIdManager;->DEPRECATED_APP_IDS:[Ljava/lang/String;

    .line 51
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbAppIdManager;->mInstance:Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbAppIdManager;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbAppIdManager;->mAppId:Ljava/lang/String;

    .line 54
    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbAppIdManager;->mSecretKey:Ljava/lang/String;

    .line 57
    invoke-direct {p0}, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbAppIdManager;->loadFbAppIdAndKey()V

    .line 58
    return-void
.end method

.method public static getInstance()Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbAppIdManager;
    .locals 1

    .prologue
    .line 104
    sget-object v0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbAppIdManager;->mInstance:Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbAppIdManager;

    if-nez v0, :cond_0

    .line 105
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbAppIdManager;

    invoke-direct {v0}, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbAppIdManager;-><init>()V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbAppIdManager;->mInstance:Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbAppIdManager;

    .line 107
    :cond_0
    sget-object v0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbAppIdManager;->mInstance:Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbAppIdManager;

    return-object v0
.end method

.method private loadFbAppIdAndKey()V
    .locals 14

    .prologue
    .line 61
    new-instance v7, Ljava/util/Properties;

    invoke-direct {v7}, Ljava/util/Properties;-><init>()V

    .line 62
    .local v7, "properties":Ljava/util/Properties;
    new-instance v4, Ljava/io/File;

    const-string v11, "/etc/snsfb.conf"

    invoke-direct {v4, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 63
    .local v4, "file":Ljava/io/File;
    const/4 v9, 0x0

    .line 66
    .local v9, "stream":Ljava/io/FileInputStream;
    :try_start_0
    new-instance v10, Ljava/io/FileInputStream;

    invoke-direct {v10, v4}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 67
    .end local v9    # "stream":Ljava/io/FileInputStream;
    .local v10, "stream":Ljava/io/FileInputStream;
    :try_start_1
    invoke-virtual {v7, v10}, Ljava/util/Properties;->load(Ljava/io/InputStream;)V

    .line 69
    const-string v11, "ID"

    invoke-virtual {v7, v11}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 70
    .local v0, "appId":Ljava/lang/String;
    const-string v11, "KEY"

    invoke-virtual {v7, v11}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 72
    .local v8, "secretKey":Ljava/lang/String;
    const-string v11, "SnsFbAppIdManager"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "FB : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 74
    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbAppIdManager;->mAppId:Ljava/lang/String;

    .line 75
    iput-object v8, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbAppIdManager;->mSecretKey:Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 79
    if-eqz v10, :cond_6

    .line 81
    :try_start_2
    invoke-virtual {v10}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-object v9, v10

    .line 88
    .end local v0    # "appId":Ljava/lang/String;
    .end local v8    # "secretKey":Ljava/lang/String;
    .end local v10    # "stream":Ljava/io/FileInputStream;
    .restart local v9    # "stream":Ljava/io/FileInputStream;
    :cond_0
    :goto_0
    iget-object v11, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbAppIdManager;->mAppId:Ljava/lang/String;

    if-eqz v11, :cond_1

    iget-object v11, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbAppIdManager;->mSecretKey:Ljava/lang/String;

    if-nez v11, :cond_4

    .line 89
    :cond_1
    const-string v11, "245238665537618"

    iput-object v11, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbAppIdManager;->mAppId:Ljava/lang/String;

    .line 90
    const-string v11, "54dcbd8d838d23e500c3f855e473bbcf"

    iput-object v11, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbAppIdManager;->mSecretKey:Ljava/lang/String;

    .line 101
    :cond_2
    :goto_1
    return-void

    .line 82
    .end local v9    # "stream":Ljava/io/FileInputStream;
    .restart local v0    # "appId":Ljava/lang/String;
    .restart local v8    # "secretKey":Ljava/lang/String;
    .restart local v10    # "stream":Ljava/io/FileInputStream;
    :catch_0
    move-exception v3

    .line 83
    .local v3, "e":Ljava/io/IOException;
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    move-object v9, v10

    .line 84
    .end local v10    # "stream":Ljava/io/FileInputStream;
    .restart local v9    # "stream":Ljava/io/FileInputStream;
    goto :goto_0

    .line 76
    .end local v0    # "appId":Ljava/lang/String;
    .end local v3    # "e":Ljava/io/IOException;
    .end local v8    # "secretKey":Ljava/lang/String;
    :catch_1
    move-exception v3

    .line 77
    .restart local v3    # "e":Ljava/io/IOException;
    :goto_2
    :try_start_3
    const-string v11, "SnsFbAppIdManager"

    const-string v12, "FB configuration file not existed"

    invoke-static {v11, v12}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 79
    if-eqz v9, :cond_0

    .line 81
    :try_start_4
    invoke-virtual {v9}, Ljava/io/FileInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 82
    :catch_2
    move-exception v3

    .line 83
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 79
    .end local v3    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v11

    :goto_3
    if-eqz v9, :cond_3

    .line 81
    :try_start_5
    invoke-virtual {v9}, Ljava/io/FileInputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 84
    :cond_3
    :goto_4
    throw v11

    .line 82
    :catch_3
    move-exception v3

    .line 83
    .restart local v3    # "e":Ljava/io/IOException;
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 92
    .end local v3    # "e":Ljava/io/IOException;
    :cond_4
    sget-object v1, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbAppIdManager;->DEPRECATED_APP_IDS:[Ljava/lang/String;

    .local v1, "arr$":[Ljava/lang/String;
    array-length v6, v1

    .local v6, "len$":I
    const/4 v5, 0x0

    .local v5, "i$":I
    :goto_5
    if-ge v5, v6, :cond_2

    aget-object v2, v1, v5

    .line 93
    .local v2, "deprecatedId":Ljava/lang/String;
    iget-object v11, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbAppIdManager;->mAppId:Ljava/lang/String;

    invoke-virtual {v2, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 94
    const-string v11, "SnsFbAppIdManager"

    const-string v12, "FB : changed"

    invoke-static {v11, v12}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 95
    const-string v11, "245238665537618"

    iput-object v11, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbAppIdManager;->mAppId:Ljava/lang/String;

    .line 96
    const-string v11, "54dcbd8d838d23e500c3f855e473bbcf"

    iput-object v11, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbAppIdManager;->mSecretKey:Ljava/lang/String;

    goto :goto_1

    .line 92
    :cond_5
    add-int/lit8 v5, v5, 0x1

    goto :goto_5

    .line 79
    .end local v1    # "arr$":[Ljava/lang/String;
    .end local v2    # "deprecatedId":Ljava/lang/String;
    .end local v5    # "i$":I
    .end local v6    # "len$":I
    .end local v9    # "stream":Ljava/io/FileInputStream;
    .restart local v10    # "stream":Ljava/io/FileInputStream;
    :catchall_1
    move-exception v11

    move-object v9, v10

    .end local v10    # "stream":Ljava/io/FileInputStream;
    .restart local v9    # "stream":Ljava/io/FileInputStream;
    goto :goto_3

    .line 76
    .end local v9    # "stream":Ljava/io/FileInputStream;
    .restart local v10    # "stream":Ljava/io/FileInputStream;
    :catch_4
    move-exception v3

    move-object v9, v10

    .end local v10    # "stream":Ljava/io/FileInputStream;
    .restart local v9    # "stream":Ljava/io/FileInputStream;
    goto :goto_2

    .end local v9    # "stream":Ljava/io/FileInputStream;
    .restart local v0    # "appId":Ljava/lang/String;
    .restart local v8    # "secretKey":Ljava/lang/String;
    .restart local v10    # "stream":Ljava/io/FileInputStream;
    :cond_6
    move-object v9, v10

    .end local v10    # "stream":Ljava/io/FileInputStream;
    .restart local v9    # "stream":Ljava/io/FileInputStream;
    goto :goto_0
.end method


# virtual methods
.method public getAppId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbAppIdManager;->mAppId:Ljava/lang/String;

    return-object v0
.end method

.method public getSecretKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbAppIdManager;->mSecretKey:Ljava/lang/String;

    return-object v0
.end method
