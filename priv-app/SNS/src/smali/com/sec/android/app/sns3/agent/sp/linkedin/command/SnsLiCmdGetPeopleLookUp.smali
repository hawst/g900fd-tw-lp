.class public Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetPeopleLookUp;
.super Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;
.source "SnsLiCmdGetPeopleLookUp.java"


# instance fields
.field mEmailListToQuery:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Handler;Landroid/os/Bundle;)V
    .locals 3
    .param p1, "mServiceMgr"    # Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    .param p2, "mCmdHandler"    # Landroid/os/Handler;
    .param p3, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 47
    invoke-direct {p0, p2}, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;-><init>(Landroid/os/Handler;)V

    .line 44
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetPeopleLookUp;->mEmailListToQuery:Ljava/util/ArrayList;

    .line 49
    if-eqz p3, :cond_0

    .line 50
    const-string v2, "emailList"

    invoke-virtual {p3, v2}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetPeopleLookUp;->mEmailListToQuery:Ljava/util/ArrayList;

    .line 53
    :cond_0
    new-instance v1, Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;

    invoke-direct {v1}, Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;-><init>()V

    .line 55
    .local v1, "unit":Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;
    new-instance v0, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetPeopleLookUp$1;

    invoke-direct {v0, p0, p1, p3}, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetPeopleLookUp$1;-><init>(Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetPeopleLookUp;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Bundle;)V

    .line 150
    .local v0, "req":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    invoke-virtual {v1, v0}, Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;->addRequest(Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;)V

    .line 152
    invoke-virtual {p0, v1}, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetPeopleLookUp;->addCommandUnit(Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;)V

    .line 153
    return-void
.end method


# virtual methods
.method protected respond()Z
    .locals 5

    .prologue
    .line 158
    const-string v0, "SnsAgent"

    const-string v1, "<SnsLiCmdGetPeopleLookUp> respond()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 159
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetPeopleLookUp;->getCommandCallback()Lcom/sec/android/app/sns3/agent/command/ISnsCommandCallback;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetPeopleLookUp;->getCommandID()I

    move-result v1

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetPeopleLookUp;->isSuccess()Z

    move-result v2

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetPeopleLookUp;->getUri()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdGetPeopleLookUp;->getResponseList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/sec/android/app/sns3/agent/command/ISnsCommandCallback;->onCmdRespond(IZLjava/lang/String;Ljava/util/List;)V

    .line 161
    const/4 v0, 0x1

    return v0
.end method
