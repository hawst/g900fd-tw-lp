.class public abstract Lcom/sec/android/app/sns3/svc/sp/linkedin/request/SnsLiReqGetPeopleLookUp;
.super Lcom/sec/android/app/sns3/svc/sp/linkedin/request/SnsLiReqBase;
.source "SnsLiReqGetPeopleLookUp.java"

# interfaces
.implements Lcom/sec/android/app/sns3/svc/sp/linkedin/callback/ISnsLiReqCbPeopleLookUp;


# static fields
.field private static final FIELD_LIST:Ljava/lang/String; = ":(id,first-name,last-name,formatted-name,public-profile-url,positions,headline,picture-urls::(100x100,original))"

.field public static final PARAM_EMAIL_LIST:Ljava/lang/String; = "emailList"

.field public static final REST_URL_REVERSE_EMAIL:Ljava/lang/String; = "https://api.linkedin.com/v1/people%1$s%2$s?oauth2_access_token=%3$s"


# instance fields
.field public mQueryString:Ljava/lang/StringBuilder;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Bundle;)V
    .locals 6
    .param p1, "svcMgr"    # Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    .param p2, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 56
    const/16 v3, 0x16

    invoke-direct {p0, p1, v3}, Lcom/sec/android/app/sns3/svc/sp/linkedin/request/SnsLiReqBase;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;I)V

    .line 45
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v3, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/request/SnsLiReqGetPeopleLookUp;->mQueryString:Ljava/lang/StringBuilder;

    .line 57
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 59
    .local v2, "mEmailList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-eqz p2, :cond_0

    .line 60
    const-string v3, "emailList"

    invoke-virtual {p2, v3}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    .line 62
    :cond_0
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_2

    .line 64
    iget-object v3, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/request/SnsLiReqGetPeopleLookUp;->mQueryString:Ljava/lang/StringBuilder;

    const-string v4, "::"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 66
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 67
    .local v0, "email":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/request/SnsLiReqGetPeopleLookUp;->mQueryString:Ljava/lang/StringBuilder;

    const-string v4, "email="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v0}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->encodeUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 69
    .end local v0    # "email":Ljava/lang/String;
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/request/SnsLiReqGetPeopleLookUp;->mQueryString:Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/request/SnsLiReqGetPeopleLookUp;->mQueryString:Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->lastIndexOf(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    .line 70
    iget-object v3, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/request/SnsLiReqGetPeopleLookUp;->mQueryString:Ljava/lang/StringBuilder;

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 73
    .end local v1    # "i$":Ljava/util/Iterator;
    :cond_2
    return-void
.end method


# virtual methods
.method protected compose()Lcom/sec/android/app/sns3/svc/http/SnsHttpRequest;
    .locals 9

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/request/SnsLiReqGetPeopleLookUp;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/SnsSvcMgr;->getTokenMgr()Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;

    move-result-object v0

    const-string v1, "linkedin"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;->getToken(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/sns3/svc/sp/linkedin/SnsLiToken;

    .line 80
    .local v6, "token":Lcom/sec/android/app/sns3/svc/sp/linkedin/SnsLiToken;
    const-string v2, "GET"

    .line 81
    .local v2, "method":Ljava/lang/String;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 82
    .local v7, "uri":Ljava/lang/StringBuilder;
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 83
    .local v4, "header":Landroid/os/Bundle;
    const/4 v5, 0x0

    .line 84
    .local v5, "body":Landroid/os/Bundle;
    const-string v0, "https://api.linkedin.com/v1/people%1$s%2$s?oauth2_access_token=%3$s"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v8, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/request/SnsLiReqGetPeopleLookUp;->mQueryString:Ljava/lang/StringBuilder;

    aput-object v8, v1, v3

    const/4 v3, 0x1

    const-string v8, ":(id,first-name,last-name,formatted-name,public-profile-url,positions,headline,picture-urls::(100x100,original))"

    aput-object v8, v1, v3

    const/4 v3, 0x2

    invoke-virtual {v6}, Lcom/sec/android/app/sns3/svc/sp/linkedin/SnsLiToken;->getAccessToken()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->encodeUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 87
    const-string v0, "x-li-format"

    const-string v1, "json"

    invoke-virtual {v4, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    new-instance v0, Lcom/sec/android/app/sns3/svc/http/SnsHttpRequest;

    iget v1, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/request/SnsLiReqGetPeopleLookUp;->mReqID:I

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/sns3/svc/http/SnsHttpRequest;-><init>(ILjava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;)V

    return-object v0
.end method

.method protected parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;
    .locals 3
    .param p1, "content"    # Ljava/lang/String;

    .prologue
    .line 93
    const-string v1, "SNS"

    const-string v2, "LinkedIn  SnsLiReqGetPeopleLookUp response SUCCESS!!"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 94
    invoke-super {p0, p1}, Lcom/sec/android/app/sns3/svc/sp/linkedin/request/SnsLiReqBase;->parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;

    move-result-object v0

    .line 96
    .local v0, "response":Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;
    if-eqz v0, :cond_0

    .line 99
    .end local v0    # "response":Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;
    :goto_0
    return-object v0

    .restart local v0    # "response":Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;
    :cond_0
    invoke-static {p1}, Lcom/sec/android/app/sns3/svc/sp/linkedin/parser/SnsLiParserPeopleLookUp;->parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp;

    move-result-object v0

    goto :goto_0
.end method

.method protected respond(Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;)Z
    .locals 7
    .param p1, "result"    # Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;

    .prologue
    .line 104
    iget v1, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/request/SnsLiReqGetPeopleLookUp;->mReqID:I

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->isSuccess()Z

    move-result v2

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->getHttpstatus()I

    move-result v3

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->getErrorCode()I

    move-result v4

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->getReason()Landroid/os/Bundle;

    move-result-object v5

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->getResponse()Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp;

    move-object v0, p0

    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/app/sns3/svc/sp/linkedin/request/SnsLiReqGetPeopleLookUp;->onReqRespond(IZIILandroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePeopleLookUp;)Z

    .line 107
    const/4 v0, 0x1

    return v0
.end method
