.class public Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuth;
.super Ljava/lang/Object;
.source "SnsAccountFsAuth.java"


# static fields
.field public static final ACTION_SNS_FOURSQUARE_LOGGED_IN:Ljava/lang/String; = "com.sec.android.app.sns.ACTION_SNS_FOURSQUARE_LOGGED_IN"

.field public static final ACTION_SNS_FOURSQUARE_LOGGED_OUT:Ljava/lang/String; = "com.sec.android.app.sns.ACTION_SNS_FOURSQUARE_LOGGED_OUT"

.field public static final ACTION_SNS_FOURSQUARE_TOKEN_UPDATED:Ljava/lang/String; = "com.sec.android.app.sns.ACTION_SNS_FOURSQUARE_TOKEN_UPDATED"

.field public static final EXTRA_SKIP_SSO_NOTI:Ljava/lang/String; = "skip_sso_noti"

.field public static final EXTRA_SKIP_SYNC_SETUP:Ljava/lang/String; = "skip_sync_setup"

.field public static final FOURSQUARE_SSO_NOTIFICATION_ID:I = 0x1004

.field public static final LOGIN_FOURSQUARE_NOTIFICATION_ID:I = 0x1068

.field public static final LOGIN_REQUEST_CODE:I = 0xfa0

.field public static final MARKET_APP_SSO:Z = true

.field public static final PROFILES_PREFERENCE_KEY:Ljava/lang/String; = "FS_profile_sync_interval"

.field public static final PROFILE_FEED_PREFERENCE_KEY:Ljava/lang/String; = "FS_profile_feed_sync_interval"

.field public static final RETRY_LOGIN_NOTIFICATION_ID:I = 0x10cc

.field public static final SNS_BROADCAST_PERMISSION:Ljava/lang/String; = "com.sec.android.app.sns3.permission.RECEIVE_SNS_BROADCAST"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
