.class public Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserPermissions;
.super Ljava/lang/Object;
.source "SnsFbParserPermissions.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserPermissions$FacebookPermissions;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    return-void
.end method

.method public static parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePermissions;
    .locals 6
    .param p0, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 37
    new-instance v4, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePermissions;

    invoke-direct {v4}, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePermissions;-><init>()V

    .line 40
    .local v4, "permissions":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePermissions;
    :try_start_0
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 41
    .local v3, "jsonObject":Lorg/json/JSONObject;
    new-instance v1, Lorg/json/JSONArray;

    const-string v5, "data"

    invoke-virtual {v3, v5}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v1, v5}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 42
    .local v1, "ja":Lorg/json/JSONArray;
    new-instance v2, Lorg/json/JSONObject;

    const/4 v5, 0x0

    invoke-virtual {v1, v5}, Lorg/json/JSONArray;->optString(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v5}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 44
    .local v2, "jsonObj":Lorg/json/JSONObject;
    invoke-virtual {v2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserPermission;->parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePermission;

    move-result-object v5

    iput-object v5, v4, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePermissions;->mPermissions:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePermission;

    .line 45
    const-string v5, "type"

    invoke-virtual {v2, v5}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePermissions;->mType:Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 52
    .end local v1    # "ja":Lorg/json/JSONArray;
    .end local v2    # "jsonObj":Lorg/json/JSONObject;
    .end local v3    # "jsonObject":Lorg/json/JSONObject;
    :goto_0
    return-object v4

    .line 47
    :catch_0
    move-exception v0

    .line 49
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method
