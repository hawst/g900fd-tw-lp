.class public Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserComments;
.super Ljava/lang/Object;
.source "SnsFbParserComments.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserComments$FacebookComments;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    return-void
.end method

.method public static parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseComments;
    .locals 10
    .param p0, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 42
    new-instance v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseComments;

    invoke-direct {v1}, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseComments;-><init>()V

    .line 44
    .local v1, "commentsObj":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseComments;
    const/4 v0, 0x0

    .line 45
    .local v0, "comments":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseComment;
    const/4 v2, 0x0

    .line 48
    .local v2, "curComments":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseComment;
    :try_start_0
    new-instance v7, Lorg/json/JSONObject;

    invoke-direct {v7, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 50
    .local v7, "jsonObject":Lorg/json/JSONObject;
    new-instance v5, Lorg/json/JSONArray;

    const-string v9, "data"

    invoke-virtual {v7, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v5, v9}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 52
    .local v5, "ja":Lorg/json/JSONArray;
    if-eqz v5, :cond_1

    .line 54
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    invoke-virtual {v5}, Lorg/json/JSONArray;->length()I

    move-result v9

    if-ge v4, v9, :cond_1

    .line 55
    invoke-virtual {v5, v4}, Lorg/json/JSONArray;->optString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserComment;->parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseComment;

    move-result-object v8

    .line 58
    .local v8, "newComment":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseComment;
    if-nez v0, :cond_0

    .line 59
    move-object v0, v8

    .line 60
    move-object v2, v0

    .line 54
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 62
    :cond_0
    iput-object v8, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseComment;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseComment;

    .line 63
    iget-object v2, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseComment;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseComment;

    goto :goto_1

    .line 68
    .end local v4    # "i":I
    .end local v8    # "newComment":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseComment;
    :cond_1
    iput-object v0, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseComments;->mComment:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseComment;

    .line 70
    const-string v9, "paging"

    invoke-virtual {v7, v9}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 72
    const-string v9, "paging"

    invoke-virtual {v7, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserPaging;->parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePaging;

    move-result-object v9

    iput-object v9, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseComments;->mPaging:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePaging;

    .line 76
    :cond_2
    const-string v9, "summary"

    invoke-virtual {v7, v9}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v9

    if-eqz v9, :cond_3

    .line 79
    :try_start_1
    new-instance v6, Lorg/json/JSONObject;

    const-string v9, "summary"

    invoke-virtual {v7, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v6, v9}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 80
    .local v6, "js":Lorg/json/JSONObject;
    const-string v9, "total_count"

    invoke-virtual {v6, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseComments;->mCommentCount:Ljava/lang/String;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    .line 91
    .end local v5    # "ja":Lorg/json/JSONArray;
    .end local v6    # "js":Lorg/json/JSONObject;
    .end local v7    # "jsonObject":Lorg/json/JSONObject;
    :cond_3
    :goto_2
    return-object v1

    .line 81
    .restart local v5    # "ja":Lorg/json/JSONArray;
    .restart local v7    # "jsonObject":Lorg/json/JSONObject;
    :catch_0
    move-exception v3

    .line 82
    .local v3, "e":Lorg/json/JSONException;
    :try_start_2
    invoke-virtual {v3}, Lorg/json/JSONException;->printStackTrace()V
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_2

    .line 86
    .end local v3    # "e":Lorg/json/JSONException;
    .end local v5    # "ja":Lorg/json/JSONArray;
    .end local v7    # "jsonObject":Lorg/json/JSONObject;
    :catch_1
    move-exception v3

    .line 88
    .restart local v3    # "e":Lorg/json/JSONException;
    invoke-virtual {v3}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_2
.end method
