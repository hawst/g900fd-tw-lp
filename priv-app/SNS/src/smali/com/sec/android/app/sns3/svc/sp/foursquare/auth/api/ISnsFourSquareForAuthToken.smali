.class public interface abstract Lcom/sec/android/app/sns3/svc/sp/foursquare/auth/api/ISnsFourSquareForAuthToken;
.super Ljava/lang/Object;
.source "ISnsFourSquareForAuthToken.java"

# interfaces
.implements Landroid/os/IInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/foursquare/auth/api/ISnsFourSquareForAuthToken$Stub;
    }
.end annotation


# virtual methods
.method public abstract getAuthTokenInfo()Ljava/util/Map;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract getMyAccountInfo(Lcom/sec/android/app/sns3/svc/sp/foursquare/auth/api/ISnsFourSquareCallbackMyAccountInfo;)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method
