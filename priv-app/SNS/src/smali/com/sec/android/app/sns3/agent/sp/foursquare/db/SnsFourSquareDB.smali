.class public Lcom/sec/android/app/sns3/agent/sp/foursquare/db/SnsFourSquareDB;
.super Ljava/lang/Object;
.source "SnsFourSquareDB.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/agent/sp/foursquare/db/SnsFourSquareDB$TimelineColumns;,
        Lcom/sec/android/app/sns3/agent/sp/foursquare/db/SnsFourSquareDB$Timeline;,
        Lcom/sec/android/app/sns3/agent/sp/foursquare/db/SnsFourSquareDB$StatusStreamColumns;,
        Lcom/sec/android/app/sns3/agent/sp/foursquare/db/SnsFourSquareDB$StatusStream;,
        Lcom/sec/android/app/sns3/agent/sp/foursquare/db/SnsFourSquareDB$UserInfoColumns;,
        Lcom/sec/android/app/sns3/agent/sp/foursquare/db/SnsFourSquareDB$UserInfo;,
        Lcom/sec/android/app/sns3/agent/sp/foursquare/db/SnsFourSquareDB$VenuesCategoriesColumns;,
        Lcom/sec/android/app/sns3/agent/sp/foursquare/db/SnsFourSquareDB$VenuesCategories;,
        Lcom/sec/android/app/sns3/agent/sp/foursquare/db/SnsFourSquareDB$FriendsProfileInfoColumns;,
        Lcom/sec/android/app/sns3/agent/sp/foursquare/db/SnsFourSquareDB$FriendsProfileInfo;,
        Lcom/sec/android/app/sns3/agent/sp/foursquare/db/SnsFourSquareDB$UserFsFeedInfoColumns;,
        Lcom/sec/android/app/sns3/agent/sp/foursquare/db/SnsFourSquareDB$UserFsFeedInfo;,
        Lcom/sec/android/app/sns3/agent/sp/foursquare/db/SnsFourSquareDB$UserProfileInfoColumns;,
        Lcom/sec/android/app/sns3/agent/sp/foursquare/db/SnsFourSquareDB$UserProfileInfo;,
        Lcom/sec/android/app/sns3/agent/sp/foursquare/db/SnsFourSquareDB$UserFsBasicInfoColumns;,
        Lcom/sec/android/app/sns3/agent/sp/foursquare/db/SnsFourSquareDB$UserFsBasicInfo;,
        Lcom/sec/android/app/sns3/agent/sp/foursquare/db/SnsFourSquareDB$WIPE_FOURSQUARE_DATA;
    }
.end annotation


# static fields
.field public static final AUTHORITY:Ljava/lang/String; = "com.sec.android.app.sns3.sp.foursquare"

.field public static final DATABASE_NAME:Ljava/lang/String; = "snsFourSquareDB.db"

.field public static final DATABASE_VERSION:I = 0x2

.field public static final FRIENDS_PROFILE_TABLE_NAME:Ljava/lang/String; = "friends_profile_info"

.field public static final LIFE_AUTHORITY:Ljava/lang/String; = "com.sec.android.app.sns3.life"

.field public static final PROFILES_AUTHORITY:Ljava/lang/String; = "com.sec.android.app.sns3.profiles"

.field public static final STATUS_STREAM_TABLE_NAME:Ljava/lang/String; = "status_stream"

.field public static final TIMELINE_TABLE_NAME:Ljava/lang/String; = "timeline"

.field public static final USER_BASIC_INFO_TABLE_NAME:Ljava/lang/String; = "user_basic_info"

.field public static final USER_FEED_TABLE_NAME:Ljava/lang/String; = "user_feed_info"

.field public static final USER_PROFILE_TABLE_NAME:Ljava/lang/String; = "user_profile_info"

.field public static final USER_TABLE_NAME:Ljava/lang/String; = "user_info"

.field public static final VENUES_CATEGORIES_TABLE_NAME:Ljava/lang/String; = "venues_categories"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 320
    return-void
.end method
