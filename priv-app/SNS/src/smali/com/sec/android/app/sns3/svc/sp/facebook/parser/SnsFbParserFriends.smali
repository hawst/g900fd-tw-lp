.class public Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserFriends;
.super Ljava/lang/Object;
.source "SnsFbParserFriends.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserFriends$FacebookFriends;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    return-void
.end method

.method public static parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriends;
    .locals 10
    .param p0, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 40
    const/4 v2, 0x0

    .line 41
    .local v2, "friends":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriends;
    const/4 v0, 0x0

    .line 43
    .local v0, "curFriends":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriends;
    const/4 v6, 0x0

    .line 46
    .local v6, "jsonObject":Lorg/json/JSONObject;
    :try_start_0
    new-instance v7, Lorg/json/JSONObject;

    invoke-direct {v7, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 48
    .end local v6    # "jsonObject":Lorg/json/JSONObject;
    .local v7, "jsonObject":Lorg/json/JSONObject;
    :try_start_1
    new-instance v4, Lorg/json/JSONArray;

    const-string v9, "data"

    invoke-virtual {v7, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v4, v9}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 50
    .local v4, "ja":Lorg/json/JSONArray;
    if-eqz v4, :cond_1

    .line 52
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v9

    if-ge v3, v9, :cond_1

    .line 53
    new-instance v8, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriends;

    invoke-direct {v8}, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriends;-><init>()V

    .line 55
    .local v8, "newFriends":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriends;
    invoke-virtual {v4, v3}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v5

    .line 57
    .local v5, "jsonObj":Lorg/json/JSONObject;
    const-string v9, "id"

    invoke-virtual {v5, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v8, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriends;->mFriendID:Ljava/lang/String;

    .line 58
    const-string v9, "name"

    invoke-virtual {v5, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v8, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriends;->mName:Ljava/lang/String;

    .line 60
    if-nez v2, :cond_0

    .line 61
    move-object v2, v8

    .line 62
    move-object v0, v2

    .line 52
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 64
    :cond_0
    iput-object v8, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriends;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriends;

    .line 65
    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriends;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriends;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .end local v3    # "i":I
    .end local v5    # "jsonObj":Lorg/json/JSONObject;
    .end local v8    # "newFriends":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriends;
    :cond_1
    move-object v6, v7

    .line 75
    .end local v4    # "ja":Lorg/json/JSONArray;
    .end local v7    # "jsonObject":Lorg/json/JSONObject;
    .restart local v6    # "jsonObject":Lorg/json/JSONObject;
    :goto_2
    return-object v2

    .line 70
    :catch_0
    move-exception v1

    .line 72
    .local v1, "e":Lorg/json/JSONException;
    :goto_3
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_2

    .line 70
    .end local v1    # "e":Lorg/json/JSONException;
    .end local v6    # "jsonObject":Lorg/json/JSONObject;
    .restart local v7    # "jsonObject":Lorg/json/JSONObject;
    :catch_1
    move-exception v1

    move-object v6, v7

    .end local v7    # "jsonObject":Lorg/json/JSONObject;
    .restart local v6    # "jsonObject":Lorg/json/JSONObject;
    goto :goto_3
.end method
