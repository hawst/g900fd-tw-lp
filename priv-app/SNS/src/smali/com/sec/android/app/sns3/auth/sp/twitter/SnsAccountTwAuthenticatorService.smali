.class public Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthenticatorService;
.super Landroid/app/Service;
.source "SnsAccountTwAuthenticatorService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthenticatorService$TwitterAccountAuthenticator;
    }
.end annotation


# static fields
.field private static OPTIONS_PASSWORD:Ljava/lang/String; = null

.field private static OPTIONS_USERNAME:Ljava/lang/String; = null

.field private static final TAG:Ljava/lang/String; = "SnsAccountTwAuthenticatorService"


# instance fields
.field private mAccAuth:Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthenticatorService$TwitterAccountAuthenticator;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 48
    const-string v0, "username"

    sput-object v0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthenticatorService;->OPTIONS_USERNAME:Ljava/lang/String;

    .line 50
    const-string v0, "password"

    sput-object v0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthenticatorService;->OPTIONS_PASSWORD:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 76
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    sget-object v0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthenticatorService;->OPTIONS_USERNAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100()Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    sget-object v0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthenticatorService;->OPTIONS_PASSWORD:Ljava/lang/String;

    return-object v0
.end method

.method private getAccountAuthenticator()Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthenticatorService$TwitterAccountAuthenticator;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthenticatorService;->mAccAuth:Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthenticatorService$TwitterAccountAuthenticator;

    if-nez v0, :cond_0

    .line 72
    new-instance v0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthenticatorService$TwitterAccountAuthenticator;

    invoke-direct {v0, p0, p0}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthenticatorService$TwitterAccountAuthenticator;-><init>(Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthenticatorService;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthenticatorService;->mAccAuth:Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthenticatorService$TwitterAccountAuthenticator;

    .line 73
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthenticatorService;->mAccAuth:Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthenticatorService$TwitterAccountAuthenticator;

    return-object v0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 56
    const-string v1, "SnsAccountTwAuthenticatorService"

    const-string v2, "TwitterAuthenticatorService : onBind"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 57
    const/4 v0, 0x0

    .line 59
    .local v0, "ret":Landroid/os/IBinder;
    const-string v1, "android.accounts.AccountAuthenticator"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 60
    invoke-direct {p0}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthenticatorService;->getAccountAuthenticator()Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthenticatorService$TwitterAccountAuthenticator;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwAuthenticatorService$TwitterAccountAuthenticator;->getIBinder()Landroid/os/IBinder;

    move-result-object v0

    .line 62
    :cond_0
    return-object v0
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 67
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 68
    return-void
.end method
