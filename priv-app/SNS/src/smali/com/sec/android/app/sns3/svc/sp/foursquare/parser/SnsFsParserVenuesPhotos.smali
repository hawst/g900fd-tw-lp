.class public Lcom/sec/android/app/sns3/svc/sp/foursquare/parser/SnsFsParserVenuesPhotos;
.super Ljava/lang/Object;
.source "SnsFsParserVenuesPhotos.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/foursquare/parser/SnsFsParserVenuesPhotos$FourSquareVenuesPhotos;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    return-void
.end method

.method public static parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseVenuesPhotos;
    .locals 11
    .param p0, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 78
    new-instance v7, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseVenuesPhotos;

    invoke-direct {v7}, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseVenuesPhotos;-><init>()V

    .line 80
    .local v7, "photosObj":Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseVenuesPhotos;
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 81
    .local v0, "con":Lorg/json/JSONObject;
    const-string v9, "meta"

    invoke-virtual {v0, v9}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v5

    .line 83
    .local v5, "meta":Lorg/json/JSONObject;
    const-string v9, "code"

    invoke-virtual {v5, v9}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v9

    const/16 v10, 0xc8

    if-ne v9, v10, :cond_0

    .line 85
    const-string v9, "response"

    invoke-virtual {v0, v9}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v8

    .line 86
    .local v8, "response":Lorg/json/JSONObject;
    const-string v9, "photos"

    invoke-virtual {v8, v9}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v6

    .line 87
    .local v6, "photos":Lorg/json/JSONObject;
    const-string v9, "count"

    invoke-virtual {v6, v9}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v9

    iput v9, v7, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseVenuesPhotos;->mCount:I

    .line 89
    iget v9, v7, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseVenuesPhotos;->mCount:I

    if-lez v9, :cond_0

    .line 90
    const-string v9, "items"

    invoke-virtual {v6, v9}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v3

    .line 91
    .local v3, "items":Lorg/json/JSONArray;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v9

    if-ge v2, v9, :cond_0

    .line 92
    invoke-virtual {v3, v2}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    .line 93
    .local v4, "itemsObject":Lorg/json/JSONObject;
    iget-object v9, v7, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseVenuesPhotos;->mVenuePhotos:Ljava/util/List;

    invoke-static {v4}, Lcom/sec/android/app/sns3/svc/sp/foursquare/parser/SnsFsParserVenuesPhotos;->parsePhotos(Lorg/json/JSONObject;)Lcom/sec/android/app/sns3/svc/sp/foursquare/response/VenuePhotos;

    move-result-object v10

    invoke-interface {v9, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 91
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 98
    .end local v0    # "con":Lorg/json/JSONObject;
    .end local v2    # "i":I
    .end local v3    # "items":Lorg/json/JSONArray;
    .end local v4    # "itemsObject":Lorg/json/JSONObject;
    .end local v5    # "meta":Lorg/json/JSONObject;
    .end local v6    # "photos":Lorg/json/JSONObject;
    .end local v8    # "response":Lorg/json/JSONObject;
    :catch_0
    move-exception v1

    .line 99
    .local v1, "e1":Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    .line 101
    .end local v1    # "e1":Lorg/json/JSONException;
    :cond_0
    return-object v7
.end method

.method public static parsePhotos(Lorg/json/JSONObject;)Lcom/sec/android/app/sns3/svc/sp/foursquare/response/VenuePhotos;
    .locals 6
    .param p0, "photoItem"    # Lorg/json/JSONObject;

    .prologue
    .line 105
    new-instance v1, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/VenuePhotos;

    invoke-direct {v1}, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/VenuePhotos;-><init>()V

    .line 107
    .local v1, "photo":Lcom/sec/android/app/sns3/svc/sp/foursquare/response/VenuePhotos;
    :try_start_0
    const-string v5, "id"

    invoke-virtual {p0, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v1, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/VenuePhotos;->mItemId:Ljava/lang/String;

    .line 108
    const-string v5, "createdAt"

    invoke-virtual {p0, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v1, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/VenuePhotos;->mCreatedAt:Ljava/lang/String;

    .line 109
    const-string v5, "source"

    invoke-virtual {p0, v5}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    .line 110
    .local v2, "source":Lorg/json/JSONObject;
    if-eqz v2, :cond_0

    .line 111
    const-string v5, "name"

    invoke-virtual {v2, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v1, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/VenuePhotos;->mSourceName:Ljava/lang/String;

    .line 112
    const-string v5, "url"

    invoke-virtual {v2, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v1, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/VenuePhotos;->mSourceUrl:Ljava/lang/String;

    .line 114
    :cond_0
    const-string v5, "prefix"

    invoke-virtual {p0, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v1, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/VenuePhotos;->mPhotoPrefix:Ljava/lang/String;

    .line 115
    const-string v5, "suffix"

    invoke-virtual {p0, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v1, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/VenuePhotos;->mPhotoSuffix:Ljava/lang/String;

    .line 116
    const-string v5, "width"

    invoke-virtual {p0, v5}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v5

    iput v5, v1, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/VenuePhotos;->mPhotoWidth:I

    .line 117
    const-string v5, "height"

    invoke-virtual {p0, v5}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v5

    iput v5, v1, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/VenuePhotos;->mPhotoHeight:I

    .line 119
    const-string v5, "user"

    invoke-virtual {p0, v5}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v3

    .line 120
    .local v3, "user":Lorg/json/JSONObject;
    if-eqz v3, :cond_1

    .line 121
    const-string v5, "id"

    invoke-virtual {v3, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v1, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/VenuePhotos;->mUserId:Ljava/lang/String;

    .line 122
    const-string v5, "firstName"

    invoke-virtual {v3, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v1, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/VenuePhotos;->mUserFirstName:Ljava/lang/String;

    .line 123
    const-string v5, "lastName"

    invoke-virtual {v3, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v1, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/VenuePhotos;->mUserLastName:Ljava/lang/String;

    .line 124
    const-string v5, "gender"

    invoke-virtual {v3, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v1, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/VenuePhotos;->mUserGender:Ljava/lang/String;

    .line 126
    const-string v5, "photo"

    invoke-virtual {v3, v5}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    .line 127
    .local v4, "userPhoto":Lorg/json/JSONObject;
    if-eqz v4, :cond_1

    .line 128
    const-string v5, "prefix"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v1, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/VenuePhotos;->mUserPhotoPrefix:Ljava/lang/String;

    .line 129
    const-string v5, "suffix"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v1, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/VenuePhotos;->mUserPhotoSuffix:Ljava/lang/String;

    .line 132
    .end local v4    # "userPhoto":Lorg/json/JSONObject;
    :cond_1
    const-string v5, "visibility"

    invoke-virtual {p0, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v1, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/VenuePhotos;->mVisibility:Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 137
    .end local v2    # "source":Lorg/json/JSONObject;
    .end local v3    # "user":Lorg/json/JSONObject;
    :goto_0
    return-object v1

    .line 134
    :catch_0
    move-exception v0

    .line 135
    .local v0, "ex":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method
