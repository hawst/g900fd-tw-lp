.class Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr$1;
.super Landroid/os/Handler;
.source "SnsCommandMgr.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;


# direct methods
.method constructor <init>(Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;)V
    .locals 0

    .prologue
    .line 54
    iput-object p1, p0, Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr$1;->this$0:Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 8
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 58
    const/4 v0, 0x0

    .line 59
    .local v0, "curComm":Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;
    const/4 v3, 0x0

    .line 60
    .local v3, "nextComm":Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;
    const/4 v1, 0x0

    .line 61
    .local v1, "curCommUnit":Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;
    const/4 v4, 0x0

    .line 62
    .local v4, "nextCommUnit":Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;
    const/4 v2, 0x0

    .line 64
    .local v2, "curReq":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    iget v5, p1, Landroid/os/Message;->what:I

    packed-switch v5, :pswitch_data_0

    .line 103
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 104
    new-instance v5, Ljava/lang/RuntimeException;

    invoke-direct {v5}, Ljava/lang/RuntimeException;-><init>()V

    throw v5

    .line 66
    :pswitch_0
    const-string v5, "SnsAgent"

    const-string v6, "SnsCommandMgr : MESSAGE_SEND"

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 67
    iget-object v5, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    if-eqz v5, :cond_0

    iget-object v5, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    instance-of v5, v5, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;

    if-eqz v5, :cond_0

    .line 68
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .end local v0    # "curComm":Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;
    check-cast v0, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;

    .line 70
    .restart local v0    # "curComm":Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;
    iget-object v5, p0, Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr$1;->this$0:Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;

    # getter for: Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;->mCommandMap:Ljava/util/Map;
    invoke-static {v5}, Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;->access$000(Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;)Ljava/util/Map;

    move-result-object v5

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->getCommandID()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v5, v6, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 71
    const-string v5, "SnsAgent"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "SnsCommandMgr : handleMessage() - Insert command [ "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->getCommandID()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " ] - mCommandMap count is [ "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr$1;->this$0:Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;

    # getter for: Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;->mCommandMap:Ljava/util/Map;
    invoke-static {v7}, Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;->access$000(Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;)Ljava/util/Map;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Map;->size()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " ]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 77
    iget-object v5, p0, Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr$1;->this$0:Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;

    # getter for: Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;->mCommandMap:Ljava/util/Map;
    invoke-static {v5}, Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;->access$000(Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;)Ljava/util/Map;

    move-result-object v5

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->getCommandID()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "curComm":Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;
    check-cast v0, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;

    .restart local v0    # "curComm":Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;
    goto :goto_0

    .line 82
    :pswitch_1
    const-string v5, "SnsAgent"

    const-string v6, "SnsCommandMgr : MESSAGE_RECEIVE"

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 83
    iget-object v5, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    if-eqz v5, :cond_0

    iget-object v5, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    instance-of v5, v5, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;

    if-eqz v5, :cond_0

    .line 84
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .end local v2    # "curReq":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    check-cast v2, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;

    .line 86
    .restart local v2    # "curReq":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    iget-object v5, p0, Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr$1;->this$0:Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;

    # invokes: Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;->getCurrentCommand(Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;)Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;
    invoke-static {v5, v2}, Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;->access$100(Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;)Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;

    move-result-object v0

    .line 87
    if-eqz v0, :cond_0

    .line 88
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->checkCurrentUnitState()V

    .line 90
    const-string v5, "SnsAgent"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "SnsCommandMgr : handleMessage() - Receive command [ "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->getCommandID()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " ] - mCommandMap count is [ "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr$1;->this$0:Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;

    # getter for: Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;->mCommandMap:Ljava/util/Map;
    invoke-static {v7}, Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;->access$000(Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;)Ljava/util/Map;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Map;->size()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " ]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 107
    :cond_1
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->getCurrentCommandUnit()Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;

    move-result-object v1

    .line 108
    const-string v5, "SnsAgent"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "SnsCommandMgr : CommandID = [ "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->getCommandID()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " ] unitIndex = [ "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;->getIndex()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " ]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 111
    invoke-virtual {v1}, Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;->getState()I

    move-result v5

    packed-switch v5, :pswitch_data_1

    .line 172
    :cond_2
    :goto_1
    return-void

    .line 113
    :pswitch_2
    const-string v5, "SnsAgent"

    const-string v6, "SnsCommandMgr : STATE_INIT"

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 115
    iget-object v5, p0, Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr$1;->this$0:Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;

    # invokes: Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;->sendCommnadUnit(Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;)V
    invoke-static {v5, v0, v1}, Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;->access$200(Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;)V

    goto :goto_1

    .line 120
    :pswitch_3
    const-string v5, "SnsAgent"

    const-string v6, "SnsCommandMgr : STATE_WAIT"

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 127
    :pswitch_4
    const-string v5, "SnsAgent"

    const-string v6, "SnsCommandMgr : STATE_FINISH"

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 128
    invoke-virtual {v1}, Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;->getIndex()I

    move-result v5

    invoke-virtual {v0, v5}, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->getNextCommandUnit(I)Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;

    move-result-object v4

    .line 130
    if-eqz v4, :cond_3

    .line 132
    const-string v5, "SnsAgent"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "SnsCommandMgr : NEXT UNIT - CommandID = [ "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->getCommandID()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " ] unitIndex = [ "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v4}, Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;->getIndex()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " ]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 137
    iget-object v5, p0, Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr$1;->this$0:Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;

    # invokes: Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;->sendCommnadUnit(Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;)V
    invoke-static {v5, v0, v4}, Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;->access$200(Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;)V

    goto :goto_1

    .line 140
    :cond_3
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->respond()Z

    .line 141
    iget-object v5, p0, Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr$1;->this$0:Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;

    # getter for: Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;->mCommandMap:Ljava/util/Map;
    invoke-static {v5}, Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;->access$000(Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;)Ljava/util/Map;

    move-result-object v5

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->getCommandID()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 143
    const-string v5, "SnsAgent"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "SnsCommandMgr : handleMessage() - Delete command [ "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->getCommandID()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " ]- mCommandMap count is [ "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr$1;->this$0:Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;

    # getter for: Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;->mCommandMap:Ljava/util/Map;
    invoke-static {v7}, Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;->access$000(Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;)Ljava/util/Map;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Map;->size()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " ]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 149
    iget-object v5, p0, Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr$1;->this$0:Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;

    # invokes: Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;->getReservedCommand()Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;
    invoke-static {v5}, Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;->access$300(Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;)Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;

    move-result-object v3

    .line 151
    if-eqz v3, :cond_2

    .line 152
    invoke-virtual {v3}, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->getCurrentCommandUnit()Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;

    move-result-object v4

    .line 153
    if-eqz v4, :cond_2

    .line 155
    const-string v5, "SnsAgent"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "SnsCommandMgr : NEXT COMMAND - CommandID = [ "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v3}, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->getCommandID()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " ] unitIndex = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v4}, Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;->getIndex()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " ]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 160
    iget-object v5, p0, Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr$1;->this$0:Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;

    # invokes: Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;->sendCommnadUnit(Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;)V
    invoke-static {v5, v0, v4}, Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;->access$200(Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;)V

    goto/16 :goto_1

    .line 64
    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 111
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
