.class public abstract Lcom/sec/android/app/sns3/svc/sp/sinaweibo/request/SnsSwReqBase;
.super Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
.source "SnsSwReqBase.java"


# direct methods
.method protected constructor <init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;I)V
    .locals 1
    .param p1, "svcMgr"    # Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    .param p2, "reqCategory"    # I

    .prologue
    .line 45
    const-string v0, "sinaweibo"

    invoke-direct {p0, p1, v0, p2}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;I)V

    .line 46
    return-void
.end method


# virtual methods
.method protected check(IILcom/sec/android/app/sns3/svc/sp/SnsSpResponse;)I
    .locals 5
    .param p1, "httpStatus"    # I
    .param p2, "prevErrorCode"    # I
    .param p3, "resp"    # Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;

    .prologue
    .line 51
    const/4 v1, -0x1

    .line 53
    .local v1, "errorCode":I
    const/4 v3, -0x1

    if-le p2, v3, :cond_1

    .line 54
    move v1, p2

    .line 69
    :cond_0
    :goto_0
    return v1

    .line 56
    :cond_1
    instance-of v3, p3, Lcom/sec/android/app/sns3/svc/sp/SnsSpResponseError;

    if-eqz v3, :cond_0

    move-object v2, p3

    .line 57
    check-cast v2, Lcom/sec/android/app/sns3/svc/sp/SnsSpResponseError;

    .line 58
    .local v2, "errorResp":Lcom/sec/android/app/sns3/svc/sp/SnsSpResponseError;
    invoke-virtual {v2}, Lcom/sec/android/app/sns3/svc/sp/SnsSpResponseError;->getErrorCode()I

    move-result v1

    .line 60
    invoke-virtual {v2}, Lcom/sec/android/app/sns3/svc/sp/SnsSpResponseError;->getErrorBundle()Landroid/os/Bundle;

    move-result-object v0

    .line 63
    .local v0, "errorBundle":Landroid/os/Bundle;
    if-eqz v0, :cond_0

    const-string v3, "190"

    const-string v4, "error_code"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 64
    const/16 v1, 0x7d1

    goto :goto_0
.end method

.method protected parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;
    .locals 9
    .param p1, "content"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    .line 75
    const/4 v2, -0x1

    .line 76
    .local v2, "errorCode":I
    const/4 v3, 0x0

    .line 77
    .local v3, "errorMsg":Ljava/lang/String;
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 80
    .local v1, "errorBundle":Landroid/os/Bundle;
    if-eqz p1, :cond_0

    :try_start_0
    const-string v7, "true"

    invoke-virtual {p1, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_0

    const-string v7, "false"

    invoke-virtual {p1, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_0

    invoke-static {p1}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->isJsonArray(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 106
    :cond_0
    :goto_0
    return-object v6

    .line 85
    :cond_1
    new-instance v5, Lorg/json/JSONObject;

    invoke-direct {v5, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 87
    .local v5, "jsonObj":Lorg/json/JSONObject;
    const-string v7, "error"

    invoke-virtual {v5, v7}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 88
    new-instance v4, Lorg/json/JSONObject;

    const-string v7, "error"

    invoke-virtual {v5, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v4, v7}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 89
    .local v4, "errorObj":Lorg/json/JSONObject;
    const-string v7, "message"

    invoke-virtual {v4, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 90
    const/16 v2, 0x3e8

    .line 91
    const-string v7, "error_code"

    const-string v8, "code"

    invoke-virtual {v4, v8}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1, v7, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 103
    .end local v4    # "errorObj":Lorg/json/JSONObject;
    .end local v5    # "jsonObj":Lorg/json/JSONObject;
    :cond_2
    :goto_1
    const/16 v7, 0x3e8

    if-ne v2, v7, :cond_0

    .line 104
    new-instance v6, Lcom/sec/android/app/sns3/svc/sp/SnsSpResponseError;

    invoke-direct {v6, v2, v3, v1}, Lcom/sec/android/app/sns3/svc/sp/SnsSpResponseError;-><init>(ILjava/lang/String;Landroid/os/Bundle;)V

    goto :goto_0

    .line 92
    .restart local v5    # "jsonObj":Lorg/json/JSONObject;
    :cond_3
    :try_start_1
    const-string v7, "error_code"

    invoke-virtual {v5, v7}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 93
    const-string v7, "error_msg"

    invoke-virtual {v5, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 94
    const/16 v2, 0x3e8

    .line 95
    const-string v7, "error_code"

    const-string v8, "error_code"

    invoke-virtual {v5, v8}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1, v7, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .line 97
    .end local v5    # "jsonObj":Lorg/json/JSONObject;
    :catch_0
    move-exception v0

    .line 98
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_1

    .line 99
    .end local v0    # "e":Lorg/json/JSONException;
    :catch_1
    move-exception v0

    .line 100
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method protected restoreToken()V
    .locals 12

    .prologue
    const/4 v9, 0x1

    const/4 v10, 0x0

    .line 112
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v0

    .line 113
    .local v0, "context":Landroid/content/Context;
    const-string v8, "notification"

    invoke-virtual {v0, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/NotificationManager;

    .line 115
    .local v5, "notiMgr":Landroid/app/NotificationManager;
    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4}, Landroid/content/Intent;-><init>()V

    .line 116
    .local v4, "notiIntent":Landroid/content/Intent;
    const-class v8, Lcom/sec/android/app/sns3/auth/sp/sinaweibo/SnsAccountSwAuthSSOActivity;

    invoke-virtual {v4, v0, v8}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 118
    const/high16 v8, 0x800000

    invoke-virtual {v4, v8}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 119
    const-string v8, "RetryLogin"

    invoke-virtual {v4, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 120
    invoke-static {v0, v10, v4, v10}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    .line 122
    .local v2, "launchIntent":Landroid/app/PendingIntent;
    const v7, 0x7f08003f

    .line 123
    .local v7, "titleID":I
    const v6, 0x7f080047

    .line 124
    .local v6, "spID":I
    const v1, 0x7f02000d

    .line 126
    .local v1, "iconID":I
    new-instance v3, Landroid/app/Notification$Builder;

    invoke-direct {v3, v0}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    .line 127
    .local v3, "notiBuilder":Landroid/app/Notification$Builder;
    new-array v8, v9, [Ljava/lang/Object;

    invoke-virtual {v0, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v10

    invoke-virtual {v0, v7, v8}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v8}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v8

    invoke-virtual {v8, v1}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v8

    const v9, 0x7f08003e

    invoke-virtual {v0, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v8

    invoke-virtual {v8, v2}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v8

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    invoke-virtual {v8, v10, v11}, Landroid/app/Notification$Builder;->setWhen(J)Landroid/app/Notification$Builder;

    .line 131
    const/16 v8, 0x2fa8

    invoke-virtual {v3}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v9

    invoke-virtual {v5, v8, v9}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 132
    return-void
.end method
