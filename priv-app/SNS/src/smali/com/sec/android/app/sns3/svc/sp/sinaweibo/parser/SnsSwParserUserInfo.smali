.class public Lcom/sec/android/app/sns3/svc/sp/sinaweibo/parser/SnsSwParserUserInfo;
.super Ljava/lang/Object;
.source "SnsSwParserUserInfo.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/sinaweibo/parser/SnsSwParserUserInfo$User;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    return-void
.end method

.method public static parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/sinaweibo/response/SnsSwResponseUserInfo;
    .locals 5
    .param p0, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 39
    new-instance v3, Lcom/sec/android/app/sns3/svc/sp/sinaweibo/response/SnsSwResponseUserInfo;

    invoke-direct {v3}, Lcom/sec/android/app/sns3/svc/sp/sinaweibo/response/SnsSwResponseUserInfo;-><init>()V

    .line 41
    .local v3, "user":Lcom/sec/android/app/sns3/svc/sp/sinaweibo/response/SnsSwResponseUserInfo;
    const/4 v1, 0x0

    .line 44
    .local v1, "jsonObject":Lorg/json/JSONObject;
    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 46
    .end local v1    # "jsonObject":Lorg/json/JSONObject;
    .local v2, "jsonObject":Lorg/json/JSONObject;
    :try_start_1
    const-string v4, "screen_name"

    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/sec/android/app/sns3/svc/sp/sinaweibo/response/SnsSwResponseUserInfo;->mScreenName:Ljava/lang/String;

    .line 47
    const-string v4, "name"

    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/sec/android/app/sns3/svc/sp/sinaweibo/response/SnsSwResponseUserInfo;->mName:Ljava/lang/String;

    .line 48
    const-string v4, "profile_image_url"

    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/sec/android/app/sns3/svc/sp/sinaweibo/response/SnsSwResponseUserInfo;->mProfileURL:Ljava/lang/String;

    .line 49
    const-string v4, "gender"

    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/sec/android/app/sns3/svc/sp/sinaweibo/response/SnsSwResponseUserInfo;->mGender:Ljava/lang/String;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    move-object v1, v2

    .line 56
    .end local v2    # "jsonObject":Lorg/json/JSONObject;
    .restart local v1    # "jsonObject":Lorg/json/JSONObject;
    :goto_0
    return-object v3

    .line 51
    :catch_0
    move-exception v0

    .line 53
    .local v0, "e":Lorg/json/JSONException;
    :goto_1
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0

    .line 51
    .end local v0    # "e":Lorg/json/JSONException;
    .end local v1    # "jsonObject":Lorg/json/JSONObject;
    .restart local v2    # "jsonObject":Lorg/json/JSONObject;
    :catch_1
    move-exception v0

    move-object v1, v2

    .end local v2    # "jsonObject":Lorg/json/JSONObject;
    .restart local v1    # "jsonObject":Lorg/json/JSONObject;
    goto :goto_1
.end method
