.class Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$2;
.super Ljava/lang/Object;
.source "SnsAccountFbAuthSSOActivity.java"

# interfaces
.implements Landroid/accounts/AccountManagerCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/accounts/AccountManagerCallback",
        "<",
        "Landroid/os/Bundle;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;)V
    .locals 0

    .prologue
    .line 163
    iput-object p1, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$2;->this$0:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run(Landroid/accounts/AccountManagerFuture;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/AccountManagerFuture",
            "<",
            "Landroid/os/Bundle;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "future":Landroid/accounts/AccountManagerFuture;, "Landroid/accounts/AccountManagerFuture<Landroid/os/Bundle;>;"
    const/16 v11, 0x3e8

    .line 165
    const/4 v2, 0x0

    .line 167
    .local v2, "bundle":Landroid/os/Bundle;
    :try_start_0
    invoke-interface {p1}, Landroid/accounts/AccountManagerFuture;->getResult()Ljava/lang/Object;

    move-result-object v8

    move-object v0, v8

    check-cast v0, Landroid/os/Bundle;

    move-object v2, v0

    .line 169
    new-instance v7, Landroid/os/Bundle;

    invoke-direct {v7}, Landroid/os/Bundle;-><init>()V

    .line 171
    .local v7, "result":Landroid/os/Bundle;
    const-string v8, "accountType"

    const-string v9, "com.sec.android.app.sns3.facebook"

    invoke-virtual {v7, v8, v9}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 172
    iget-object v8, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$2;->this$0:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;

    invoke-virtual {v8, v7}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->setAccountAuthenticatorResult(Landroid/os/Bundle;)V

    .line 174
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 176
    .local v4, "extra":Landroid/os/Bundle;
    iget-object v8, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$2;->this$0:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;

    invoke-virtual {v8}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    invoke-static {v8}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v8

    const-string v9, "com.sec.android.app.sns3.facebook"

    invoke-virtual {v8, v9}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v1

    .line 179
    .local v1, "account":[Landroid/accounts/Account;
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/app/sns3/SnsApplication;->getFeatureMgr()Lcom/sec/android/app/sns3/SnsFeatureManager;

    move-result-object v5

    .line 182
    .local v5, "featureMgr":Lcom/sec/android/app/sns3/SnsFeatureManager;
    const/4 v8, 0x0

    aget-object v8, v1, v8

    const-string v9, "com.android.calendar"

    invoke-static {v8, v9, v4}, Landroid/content/ContentResolver;->removePeriodicSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 183
    const/4 v8, 0x0

    aget-object v8, v1, v8

    const-string v9, "com.sec.android.gallery3d.sns.contentprovider"

    invoke-static {v8, v9, v4}, Landroid/content/ContentResolver;->removePeriodicSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 187
    invoke-virtual {v5}, Lcom/sec/android/app/sns3/SnsFeatureManager;->isSyncAdapterForHomeFeedsNeeded()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 188
    const/4 v8, 0x0

    aget-object v8, v1, v8

    const-string v9, "com.sec.android.app.sns3.home"

    const/4 v10, 0x1

    invoke-static {v8, v9, v10}, Landroid/content/ContentResolver;->setIsSyncable(Landroid/accounts/Account;Ljava/lang/String;I)V

    .line 189
    const/4 v8, 0x0

    aget-object v8, v1, v8

    const-string v9, "com.sec.android.app.sns3.home"

    invoke-static {v8, v9, v4}, Landroid/content/ContentResolver;->removePeriodicSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 196
    :goto_0
    invoke-virtual {v5}, Lcom/sec/android/app/sns3/SnsFeatureManager;->isSyncAdapterForProfileFeedsNeeded()Z

    move-result v8

    if-eqz v8, :cond_3

    .line 197
    const/4 v8, 0x0

    aget-object v8, v1, v8

    const-string v9, "com.sec.android.app.sns3.life"

    const/4 v10, 0x1

    invoke-static {v8, v9, v10}, Landroid/content/ContentResolver;->setIsSyncable(Landroid/accounts/Account;Ljava/lang/String;I)V

    .line 198
    const/4 v8, 0x0

    aget-object v8, v1, v8

    const-string v9, "com.sec.android.app.sns3.life"

    invoke-static {v8, v9, v4}, Landroid/content/ContentResolver;->removePeriodicSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 205
    :goto_1
    invoke-virtual {v5}, Lcom/sec/android/app/sns3/SnsFeatureManager;->isSyncAdapterForProfilesNeeded()Z

    move-result v8

    if-eqz v8, :cond_5

    .line 206
    const/4 v8, 0x0

    aget-object v8, v1, v8

    const-string v9, "com.sec.android.app.sns3.profiles"

    const/4 v10, 0x1

    invoke-static {v8, v9, v10}, Landroid/content/ContentResolver;->setIsSyncable(Landroid/accounts/Account;Ljava/lang/String;I)V

    .line 207
    const/4 v8, 0x0

    aget-object v8, v1, v8

    const-string v9, "com.sec.android.app.sns3.profiles"

    invoke-static {v8, v9, v4}, Landroid/content/ContentResolver;->removePeriodicSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 214
    :goto_2
    invoke-virtual {v5}, Lcom/sec/android/app/sns3/SnsFeatureManager;->isSyncAdapterForStreamsNeeded()Z

    move-result v8

    if-eqz v8, :cond_6

    .line 215
    const/4 v8, 0x0

    aget-object v8, v1, v8

    const-string v9, "com.sec.android.app.sns3.streams"

    const/4 v10, 0x1

    invoke-static {v8, v9, v10}, Landroid/content/ContentResolver;->setIsSyncable(Landroid/accounts/Account;Ljava/lang/String;I)V

    .line 216
    const/4 v8, 0x0

    aget-object v8, v1, v8

    const-string v9, "com.sec.android.app.sns3.streams"

    invoke-static {v8, v9, v4}, Landroid/content/ContentResolver;->removePeriodicSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 225
    :goto_3
    if-eqz v2, :cond_0

    iget-object v8, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$2;->this$0:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;

    # getter for: Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->mRetryLogin:Z
    invoke-static {v8}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->access$900(Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;)Z

    move-result v8

    if-eqz v8, :cond_7

    .line 226
    :cond_0
    iget-object v8, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$2;->this$0:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;

    invoke-virtual {v8}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->finish()V

    .line 233
    .end local v1    # "account":[Landroid/accounts/Account;
    .end local v4    # "extra":Landroid/os/Bundle;
    .end local v5    # "featureMgr":Lcom/sec/android/app/sns3/SnsFeatureManager;
    .end local v7    # "result":Landroid/os/Bundle;
    :goto_4
    return-void

    .line 192
    .restart local v1    # "account":[Landroid/accounts/Account;
    .restart local v4    # "extra":Landroid/os/Bundle;
    .restart local v5    # "featureMgr":Lcom/sec/android/app/sns3/SnsFeatureManager;
    .restart local v7    # "result":Landroid/os/Bundle;
    :cond_1
    const/4 v8, 0x0

    :try_start_1
    aget-object v8, v1, v8

    const-string v9, "com.sec.android.app.sns3.home"

    const/4 v10, 0x0

    invoke-static {v8, v9, v10}, Landroid/content/ContentResolver;->setIsSyncable(Landroid/accounts/Account;Ljava/lang/String;I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 221
    .end local v1    # "account":[Landroid/accounts/Account;
    .end local v4    # "extra":Landroid/os/Bundle;
    .end local v5    # "featureMgr":Lcom/sec/android/app/sns3/SnsFeatureManager;
    .end local v7    # "result":Landroid/os/Bundle;
    :catch_0
    move-exception v3

    .line 222
    .local v3, "e":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    .line 223
    # getter for: Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->access$400()Ljava/lang/String;

    move-result-object v8

    const-string v9, "####### AccountManagerCallback : run FAILED !!!!! #######"

    invoke-static {v8, v9}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 225
    if-eqz v2, :cond_2

    iget-object v8, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$2;->this$0:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;

    # getter for: Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->mRetryLogin:Z
    invoke-static {v8}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->access$900(Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;)Z

    move-result v8

    if-eqz v8, :cond_8

    .line 226
    :cond_2
    iget-object v8, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$2;->this$0:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;

    invoke-virtual {v8}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->finish()V

    goto :goto_4

    .line 201
    .end local v3    # "e":Ljava/lang/Exception;
    .restart local v1    # "account":[Landroid/accounts/Account;
    .restart local v4    # "extra":Landroid/os/Bundle;
    .restart local v5    # "featureMgr":Lcom/sec/android/app/sns3/SnsFeatureManager;
    .restart local v7    # "result":Landroid/os/Bundle;
    :cond_3
    const/4 v8, 0x0

    :try_start_3
    aget-object v8, v1, v8

    const-string v9, "com.sec.android.app.sns3.life"

    const/4 v10, 0x0

    invoke-static {v8, v9, v10}, Landroid/content/ContentResolver;->setIsSyncable(Landroid/accounts/Account;Ljava/lang/String;I)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 225
    .end local v1    # "account":[Landroid/accounts/Account;
    .end local v4    # "extra":Landroid/os/Bundle;
    .end local v5    # "featureMgr":Lcom/sec/android/app/sns3/SnsFeatureManager;
    .end local v7    # "result":Landroid/os/Bundle;
    :catchall_0
    move-exception v8

    if-eqz v2, :cond_4

    iget-object v9, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$2;->this$0:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;

    # getter for: Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->mRetryLogin:Z
    invoke-static {v9}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->access$900(Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;)Z

    move-result v9

    if-eqz v9, :cond_9

    .line 226
    :cond_4
    iget-object v9, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$2;->this$0:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;

    invoke-virtual {v9}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->finish()V

    .line 231
    :goto_5
    throw v8

    .line 210
    .restart local v1    # "account":[Landroid/accounts/Account;
    .restart local v4    # "extra":Landroid/os/Bundle;
    .restart local v5    # "featureMgr":Lcom/sec/android/app/sns3/SnsFeatureManager;
    .restart local v7    # "result":Landroid/os/Bundle;
    :cond_5
    const/4 v8, 0x0

    :try_start_4
    aget-object v8, v1, v8

    const-string v9, "com.sec.android.app.sns3.profiles"

    const/4 v10, 0x0

    invoke-static {v8, v9, v10}, Landroid/content/ContentResolver;->setIsSyncable(Landroid/accounts/Account;Ljava/lang/String;I)V

    goto :goto_2

    .line 219
    :cond_6
    const/4 v8, 0x0

    aget-object v8, v1, v8

    const-string v9, "com.sec.android.app.sns3.streams"

    const/4 v10, 0x0

    invoke-static {v8, v9, v10}, Landroid/content/ContentResolver;->setIsSyncable(Landroid/accounts/Account;Ljava/lang/String;I)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_3

    .line 228
    :cond_7
    new-instance v6, Landroid/content/Intent;

    iget-object v8, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$2;->this$0:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;

    invoke-virtual {v8}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    const-class v9, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;

    invoke-direct {v6, v8, v9}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 230
    .local v6, "intent":Landroid/content/Intent;
    iget-object v8, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$2;->this$0:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;

    invoke-virtual {v8, v6, v11}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_4

    .line 228
    .end local v1    # "account":[Landroid/accounts/Account;
    .end local v4    # "extra":Landroid/os/Bundle;
    .end local v5    # "featureMgr":Lcom/sec/android/app/sns3/SnsFeatureManager;
    .end local v6    # "intent":Landroid/content/Intent;
    .end local v7    # "result":Landroid/os/Bundle;
    .restart local v3    # "e":Ljava/lang/Exception;
    :cond_8
    new-instance v6, Landroid/content/Intent;

    iget-object v8, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$2;->this$0:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;

    invoke-virtual {v8}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    const-class v9, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;

    invoke-direct {v6, v8, v9}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 230
    .restart local v6    # "intent":Landroid/content/Intent;
    iget-object v8, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$2;->this$0:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;

    invoke-virtual {v8, v6, v11}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_4

    .line 228
    .end local v3    # "e":Ljava/lang/Exception;
    .end local v6    # "intent":Landroid/content/Intent;
    :cond_9
    new-instance v6, Landroid/content/Intent;

    iget-object v9, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$2;->this$0:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;

    invoke-virtual {v9}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v9

    const-class v10, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbSyncSetupActivity;

    invoke-direct {v6, v9, v10}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 230
    .restart local v6    # "intent":Landroid/content/Intent;
    iget-object v9, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity$2;->this$0:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;

    invoke-virtual {v9, v6, v11}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthSSOActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_5
.end method
