.class public interface abstract Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$CommentColumns;
.super Ljava/lang/Object;
.source "SnsLinkedInDB.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "CommentColumns"
.end annotation


# static fields
.field public static final COMMENT_ID:Ljava/lang/String; = "comment_id"

.field public static final COMMENT_PERSON_ID:Ljava/lang/String; = "comment_person_id"

.field public static final COMMENT_SEQUENCE_NUMBER:Ljava/lang/String; = "comment_sequence_number"

.field public static final COMMENT_TEXT:Ljava/lang/String; = "comment_text"

.field public static final COMMENT_TIME_STAMP:Ljava/lang/String; = "comment_stime_stamp"

.field public static final COMMENT_UPDATE_KEY:Ljava/lang/String; = "comment_update_key"
