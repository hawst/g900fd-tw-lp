.class Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthActivity$4$1;
.super Ljava/lang/Object;
.source "SnsAccountLiAuthActivity.java"

# interfaces
.implements Lcom/sec/android/app/sns3/agent/command/ISnsCommandCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthActivity$4;->shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthActivity$4;


# direct methods
.method constructor <init>(Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthActivity$4;)V
    .locals 0

    .prologue
    .line 293
    iput-object p1, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthActivity$4$1;->this$1:Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthActivity$4;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCmdRespond(IZLjava/lang/String;Ljava/util/List;)V
    .locals 16
    .param p1, "cmdID"    # I
    .param p2, "bSuccess"    # Z
    .param p3, "uri"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IZ",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 299
    .local p4, "responseList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;>;"
    const/4 v10, 0x0

    .line 300
    .local v10, "reason":Landroid/os/Bundle;
    const/4 v2, 0x0

    .line 301
    .local v2, "accessToken":Ljava/lang/String;
    const/4 v5, 0x0

    .line 303
    .local v5, "expires":Ljava/lang/String;
    if-eqz p4, :cond_0

    .line 304
    invoke-interface/range {p4 .. p4}, Ljava/util/List;->size()I

    move-result v11

    if-lez v11, :cond_0

    .line 305
    const/4 v11, 0x0

    move-object/from16 v0, p4

    invoke-interface {v0, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;

    invoke-virtual {v11}, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;->getReason()Landroid/os/Bundle;

    move-result-object v10

    .line 307
    if-eqz v10, :cond_0

    .line 308
    const-string v11, "access_token"

    invoke-virtual {v10, v11}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 309
    const-string v11, "expires_in"

    invoke-virtual {v10, v11}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 314
    :cond_0
    invoke-static {}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->isLoggable()Z

    move-result v11

    if-eqz v11, :cond_1

    .line 315
    # getter for: Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthActivity;->access$000()Ljava/lang/String;

    move-result-object v11

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "SnsAccountLiAuthActivity - onResponse() - access_token = "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ", mExpires = "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 320
    :cond_1
    if-eqz v2, :cond_4

    if-eqz v5, :cond_4

    .line 322
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    .line 323
    .local v6, "currentSec":J
    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v12

    const-wide/16 v14, 0x3e8

    mul-long v8, v12, v14

    .line 325
    .local v8, "expiresSec":J
    add-long v12, v6, v8

    invoke-static {v12, v13}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    .line 327
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthActivity$4$1;->this$1:Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthActivity$4;

    iget-object v11, v11, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthActivity$4;->this$0:Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthActivity;

    # getter for: Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthActivity;->mRetryLogin:Z
    invoke-static {v11}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthActivity;->access$100(Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthActivity;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 328
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthActivity$4$1;->this$1:Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthActivity$4;

    iget-object v11, v11, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthActivity$4;->this$0:Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthActivity;

    # setter for: Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthActivity;->mAccessToken:Ljava/lang/String;
    invoke-static {v11, v2}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthActivity;->access$202(Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 329
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthActivity$4$1;->this$1:Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthActivity$4;

    iget-object v11, v11, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthActivity$4;->this$0:Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthActivity;

    # setter for: Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthActivity;->mExpires:Ljava/lang/String;
    invoke-static {v11, v5}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthActivity;->access$302(Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 331
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthActivity$4$1;->this$1:Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthActivity$4;

    iget-object v11, v11, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthActivity$4;->this$0:Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthActivity;

    # getter for: Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthActivity;->mAppContext:Landroid/content/Context;
    invoke-static {v11}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthActivity;->access$400(Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthActivity;)Landroid/content/Context;

    move-result-object v11

    invoke-static {v11}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v11

    const-string v12, "com.sec.android.app.sns3.linkedin"

    invoke-virtual {v11, v12}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v3

    .line 333
    .local v3, "accounts":[Landroid/accounts/Account;
    if-eqz v3, :cond_2

    array-length v11, v3

    if-lez v11, :cond_2

    .line 334
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthActivity$4$1;->this$1:Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthActivity$4;

    iget-object v11, v11, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthActivity$4;->this$0:Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthActivity;

    # getter for: Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthActivity;->mAppContext:Landroid/content/Context;
    invoke-static {v11}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthActivity;->access$400(Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthActivity;)Landroid/content/Context;

    move-result-object v11

    invoke-static {v11}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v11

    const/4 v12, 0x0

    aget-object v12, v3, v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthActivity$4$1;->this$1:Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthActivity$4;

    iget-object v13, v13, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthActivity$4;->this$0:Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthActivity;

    iget-object v13, v13, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthActivity;->mAccountManagerRemoveCallback:Landroid/accounts/AccountManagerCallback;

    const/4 v14, 0x0

    invoke-virtual {v11, v12, v13, v14}, Landroid/accounts/AccountManager;->removeAccount(Landroid/accounts/Account;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    .line 338
    :cond_2
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthActivity$4$1;->this$1:Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthActivity$4;

    iget-object v11, v11, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthActivity$4;->this$0:Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthActivity;

    const/4 v12, 0x0

    # setter for: Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthActivity;->mRetryLogin:Z
    invoke-static {v11, v12}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthActivity;->access$102(Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthActivity;Z)Z

    .line 388
    .end local v3    # "accounts":[Landroid/accounts/Account;
    .end local v6    # "currentSec":J
    .end local v8    # "expiresSec":J
    :goto_0
    return-void

    .line 341
    .restart local v6    # "currentSec":J
    .restart local v8    # "expiresSec":J
    :cond_3
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthActivity$4$1;->this$1:Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthActivity$4;

    iget-object v11, v11, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthActivity$4;->this$0:Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthActivity;

    invoke-virtual {v11, v2, v5}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthActivity;->setAuthTokenNExpires(Ljava/lang/String;Ljava/lang/String;)Z

    .line 343
    new-instance v4, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdAuthLogin;

    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v11

    invoke-virtual {v11}, Lcom/sec/android/app/sns3/SnsApplication;->getSvcMgr()Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v11

    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v12

    invoke-virtual {v12}, Lcom/sec/android/app/sns3/SnsApplication;->getAgentMgr()Lcom/sec/android/app/sns3/agent/SnsAgentMgr;

    move-result-object v12

    invoke-virtual {v12}, Lcom/sec/android/app/sns3/agent/SnsAgentMgr;->getCommandMgr()Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;

    move-result-object v12

    invoke-virtual {v12}, Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;->getCommandMgrHandle()Landroid/os/Handler;

    move-result-object v12

    const/4 v13, 0x0

    invoke-direct {v4, v11, v12, v13}, Lcom/sec/android/app/sns3/agent/sp/linkedin/command/SnsLiCmdAuthLogin;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Handler;Landroid/os/Bundle;)V

    .line 349
    .local v4, "cmd":Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;
    new-instance v11, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthActivity$4$1$1;

    move-object/from16 v0, p0

    invoke-direct {v11, v0}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthActivity$4$1$1;-><init>(Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthActivity$4$1;)V

    invoke-virtual {v4, v11}, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->setCommandCallback(Lcom/sec/android/app/sns3/agent/command/ISnsCommandCallback;)Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;

    .line 378
    invoke-virtual {v4}, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->send()I

    goto :goto_0

    .line 383
    .end local v4    # "cmd":Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;
    .end local v6    # "currentSec":J
    .end local v8    # "expiresSec":J
    :cond_4
    # getter for: Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthActivity;->access$000()Ljava/lang/String;

    move-result-object v11

    const-string v12, "SnsAccountLiAuthActivity - access token is null!!"

    invoke-static {v11, v12}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 386
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthActivity$4$1;->this$1:Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthActivity$4;

    iget-object v11, v11, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthActivity$4;->this$0:Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthActivity;

    const/4 v12, -0x1

    const/4 v13, -0x1

    const/4 v14, 0x0

    invoke-virtual {v11, v12, v13, v14}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiAuthActivity;->loginFail(IILjava/lang/String;)V

    goto :goto_0
.end method
