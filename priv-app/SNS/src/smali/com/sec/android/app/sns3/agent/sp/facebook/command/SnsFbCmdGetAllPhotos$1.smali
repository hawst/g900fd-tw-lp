.class Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAllPhotos$1;
.super Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqAllPhotos;
.source "SnsFbCmdGetAllPhotos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAllPhotos;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Handler;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAllPhotos;


# direct methods
.method constructor <init>(Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAllPhotos;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;I)V
    .locals 0
    .param p2, "x0"    # Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    .param p3, "x1"    # I

    .prologue
    .line 29
    iput-object p1, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAllPhotos$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAllPhotos;

    invoke-direct {p0, p2, p3}, Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqAllPhotos;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;I)V

    return-void
.end method


# virtual methods
.method public onReqRespond(IZIILandroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAllPhotos;)Z
    .locals 7
    .param p1, "reqID"    # I
    .param p2, "bSuccess"    # Z
    .param p3, "httpStatus"    # I
    .param p4, "errorCode"    # I
    .param p5, "reason"    # Landroid/os/Bundle;
    .param p6, "photos"    # Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAllPhotos;

    .prologue
    const/4 v6, 0x0

    .line 35
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 36
    .local v2, "values":Landroid/content/ContentValues;
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/sns3/SnsApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 38
    .local v0, "cr":Landroid/content/ContentResolver;
    if-eqz p2, :cond_2

    .line 39
    if-eqz p6, :cond_1

    .line 41
    move-object v1, p6

    .line 42
    .local v1, "curPhotos":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAllPhotos;
    :goto_0
    if-eqz v1, :cond_1

    .line 44
    invoke-virtual {v2}, Landroid/content/ContentValues;->clear()V

    .line 46
    const-string v3, "id"

    iget-object v4, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAllPhotos;->mPid:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 47
    const-string v3, "source"

    iget-object v4, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAllPhotos;->mSrc:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 48
    const-string v3, "height"

    iget-object v4, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAllPhotos;->mHeight:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 49
    const-string v3, "width"

    iget-object v4, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAllPhotos;->mWidth:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    const-string v3, "updated_time"

    iget-object v4, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAllPhotos;->mModified:Ljava/lang/String;

    invoke-static {v4}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->toTimestampMillies(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 52
    const-string v3, "created_time"

    iget-object v4, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAllPhotos;->mCreated:Ljava/lang/String;

    invoke-static {v4}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->toTimestampMillies(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 55
    sget-object v3, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$Photo;->CONTENT_URI:Landroid/net/Uri;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "id = \'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAllPhotos;->mPid:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v2, v4, v6}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_0

    .line 57
    sget-object v3, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$Photo;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v3, v2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 59
    :cond_0
    iget-object v1, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAllPhotos;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAllPhotos;

    goto :goto_0

    .line 62
    .end local v1    # "curPhotos":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAllPhotos;
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAllPhotos$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAllPhotos;

    sget-object v4, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$Photo;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAllPhotos;->setUri(Ljava/lang/String;)V

    .line 69
    :goto_1
    iget-object v3, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAllPhotos$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAllPhotos;

    invoke-virtual {v3, p2}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAllPhotos;->setSuccess(Z)V

    .line 70
    iget-object v3, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAllPhotos$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAllPhotos;

    invoke-virtual {v3, p0}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAllPhotos;->receive(Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;)V

    .line 72
    const/4 v3, 0x1

    return v3

    .line 64
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAllPhotos$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAllPhotos;

    new-instance v4, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;

    const-string v5, "facebook"

    invoke-direct {v4, v5, p3, p4, p5}, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;-><init>(Ljava/lang/String;IILandroid/os/Bundle;)V

    invoke-virtual {v3, v4}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAllPhotos;->setResponseList(Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;)Z

    .line 66
    iget-object v3, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAllPhotos$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAllPhotos;

    invoke-virtual {v3, v6}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAllPhotos;->setUri(Ljava/lang/String;)V

    goto :goto_1
.end method
