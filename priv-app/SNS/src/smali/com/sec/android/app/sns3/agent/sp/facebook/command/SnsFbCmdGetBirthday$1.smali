.class Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetBirthday$1;
.super Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqGetBirthday;
.source "SnsFbCmdGetBirthday.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetBirthday;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Handler;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetBirthday;


# direct methods
.method constructor <init>(Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetBirthday;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;)V
    .locals 0
    .param p2, "x0"    # Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    .param p3, "x1"    # Ljava/lang/String;

    .prologue
    .line 46
    iput-object p1, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetBirthday$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetBirthday;

    invoke-direct {p0, p2, p3}, Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqGetBirthday;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public onReqRespond(IZIILandroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseBirthday;)Z
    .locals 7
    .param p1, "reqID"    # I
    .param p2, "bSuccess"    # Z
    .param p3, "httpStatus"    # I
    .param p4, "errorCode"    # I
    .param p5, "reason"    # Landroid/os/Bundle;
    .param p6, "birthday"    # Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseBirthday;

    .prologue
    const/4 v6, 0x0

    .line 51
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 52
    .local v2, "values":Landroid/content/ContentValues;
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/sns3/SnsApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 54
    .local v0, "cr":Landroid/content/ContentResolver;
    sget-object v3, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$Birthday;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v3, v6, v6}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 56
    if-eqz p2, :cond_1

    .line 57
    if-eqz p6, :cond_0

    .line 58
    move-object v1, p6

    .line 60
    .local v1, "curBirthday":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseBirthday;
    :goto_0
    if-eqz v1, :cond_0

    .line 62
    invoke-virtual {v2}, Landroid/content/ContentValues;->clear()V

    .line 64
    const-string v3, "uid"

    iget-object v4, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseBirthday;->mUId:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    const-string v3, "first_name"

    iget-object v4, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseBirthday;->mFirstName:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    const-string v3, "last_name"

    iget-object v4, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseBirthday;->mLastName:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    const-string v3, "name"

    iget-object v4, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseBirthday;->mName:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    const-string v3, "birthday_date"

    iget-object v4, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseBirthday;->mBirthdayDate:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    const-string v3, "pic_url"

    iget-object v4, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseBirthday;->mPicUrl:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    sget-object v3, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$Birthday;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v3, v2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 73
    iget-object v1, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseBirthday;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseBirthday;

    goto :goto_0

    .line 76
    .end local v1    # "curBirthday":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseBirthday;
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetBirthday$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetBirthday;

    sget-object v4, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$Birthday;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetBirthday;->setUri(Ljava/lang/String;)V

    .line 83
    :goto_1
    iget-object v3, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetBirthday$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetBirthday;

    invoke-virtual {v3, p2}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetBirthday;->setSuccess(Z)V

    .line 84
    iget-object v3, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetBirthday$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetBirthday;

    invoke-virtual {v3, p0}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetBirthday;->receive(Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;)V

    .line 86
    const/4 v3, 0x1

    return v3

    .line 78
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetBirthday$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetBirthday;

    new-instance v4, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;

    const-string v5, "facebook"

    invoke-direct {v4, v5, p3, p4, p5}, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;-><init>(Ljava/lang/String;IILandroid/os/Bundle;)V

    invoke-virtual {v3, v4}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetBirthday;->setResponseList(Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;)Z

    .line 80
    iget-object v3, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetBirthday$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetBirthday;

    invoke-virtual {v3, v6}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetBirthday;->setUri(Ljava/lang/String;)V

    goto :goto_1
.end method
