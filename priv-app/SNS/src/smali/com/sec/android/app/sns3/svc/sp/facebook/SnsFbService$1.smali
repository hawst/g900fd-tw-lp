.class Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1;
.super Lcom/sec/android/app/sns3/svc/sp/facebook/api/ISnsFacebook$Stub;
.source "SnsFbService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;


# direct methods
.method constructor <init>(Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;)V
    .locals 0

    .prologue
    .line 165
    iput-object p1, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1;->this$0:Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;

    invoke-direct {p0}, Lcom/sec/android/app/sns3/svc/sp/facebook/api/ISnsFacebook$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public deleteLike(Ljava/lang/String;Lcom/sec/android/app/sns3/svc/sp/facebook/api/ISnsFacebookCallbackBoolean;)I
    .locals 3
    .param p1, "objectID"    # Ljava/lang/String;
    .param p2, "cb"    # Lcom/sec/android/app/sns3/svc/sp/facebook/api/ISnsFacebookCallbackBoolean;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 682
    const-string v1, "SNS"

    const-string v2, "deleteLike CALLED!!!"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 684
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1$21;

    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1;->this$0:Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;

    # getter for: Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    invoke-static {v1}, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;->access$000(Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;)Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v1

    invoke-direct {v0, p0, v1, p1, p2}, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1$21;-><init>(Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;Lcom/sec/android/app/sns3/svc/sp/facebook/api/ISnsFacebookCallbackBoolean;)V

    .line 698
    .local v0, "req":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->request()Z

    .line 700
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->getReqID()I

    move-result v1

    return v1
.end method

.method public getAlbums(Ljava/lang/String;Landroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/facebook/api/ISnsFacebookCallbackAlbums;)I
    .locals 6
    .param p1, "objectID"    # Ljava/lang/String;
    .param p2, "bundle"    # Landroid/os/Bundle;
    .param p3, "cb"    # Lcom/sec/android/app/sns3/svc/sp/facebook/api/ISnsFacebookCallbackAlbums;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 292
    const-string v1, "SNS"

    const-string v2, "getAlbums CALLED!!!"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 294
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1$5;

    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1;->this$0:Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;

    # getter for: Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    invoke-static {v1}, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;->access$000(Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;)Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v2

    move-object v1, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1$5;-><init>(Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;Landroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/facebook/api/ISnsFacebookCallbackAlbums;)V

    .line 308
    .local v0, "req":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->request()Z

    .line 310
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->getReqID()I

    move-result v1

    return v1
.end method

.method public getApplicationInfo()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 178
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 179
    .local v0, "appInfo":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v1, "app_id"

    invoke-static {}, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbAppIdManager;->getInstance()Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbAppIdManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbAppIdManager;->getAppId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 180
    const-string v1, "redirect_uri"

    const-string v2, "fbconnect://success"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 181
    const-string v1, "cancel_uri"

    const-string v2, "fbconnect://cancel"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 182
    const-string v1, "dialog_base_url"

    const-string v2, "https://m.facebook.com/dialog/"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 183
    return-object v0
.end method

.method public getBirthday(Ljava/lang/String;Lcom/sec/android/app/sns3/svc/sp/facebook/api/ISnsFacebookCallbackBirthday;)I
    .locals 3
    .param p1, "userID"    # Ljava/lang/String;
    .param p2, "cb"    # Lcom/sec/android/app/sns3/svc/sp/facebook/api/ISnsFacebookCallbackBirthday;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 586
    const-string v1, "SNS"

    const-string v2, "getBirthday CALLED!!!"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 588
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1$17;

    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1;->this$0:Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;

    # getter for: Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    invoke-static {v1}, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;->access$000(Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;)Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v1

    invoke-direct {v0, p0, v1, p1, p2}, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1$17;-><init>(Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;Lcom/sec/android/app/sns3/svc/sp/facebook/api/ISnsFacebookCallbackBirthday;)V

    .line 602
    .local v0, "req":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->request()Z

    .line 604
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->getReqID()I

    move-result v1

    return v1
.end method

.method public getEvents(Ljava/lang/String;Landroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/facebook/api/ISnsFacebookCallbackEvents;)I
    .locals 6
    .param p1, "objectID"    # Ljava/lang/String;
    .param p2, "bundle"    # Landroid/os/Bundle;
    .param p3, "cb"    # Lcom/sec/android/app/sns3/svc/sp/facebook/api/ISnsFacebookCallbackEvents;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 269
    const-string v1, "SNS"

    const-string v2, "getEvents CALLED!!!"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 271
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1$4;

    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1;->this$0:Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;

    # getter for: Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    invoke-static {v1}, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;->access$000(Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;)Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v2

    move-object v1, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1$4;-><init>(Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;Landroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/facebook/api/ISnsFacebookCallbackEvents;)V

    .line 285
    .local v0, "req":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->request()Z

    .line 287
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->getReqID()I

    move-result v1

    return v1
.end method

.method public getFeed(Ljava/lang/String;Lcom/sec/android/app/sns3/svc/sp/facebook/api/ISnsFacebookCallbackPosts;)I
    .locals 3
    .param p1, "userID"    # Ljava/lang/String;
    .param p2, "cb"    # Lcom/sec/android/app/sns3/svc/sp/facebook/api/ISnsFacebookCallbackPosts;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 517
    const-string v1, "SNS"

    const-string v2, "getFeed CALLED!!!"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 519
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1$14;

    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1;->this$0:Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;

    # getter for: Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    invoke-static {v1}, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;->access$000(Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;)Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v1

    invoke-direct {v0, p0, v1, p1, p2}, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1$14;-><init>(Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;Lcom/sec/android/app/sns3/svc/sp/facebook/api/ISnsFacebookCallbackPosts;)V

    .line 533
    .local v0, "req":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->request()Z

    .line 535
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->getReqID()I

    move-result v1

    return v1
.end method

.method public getFriends(Ljava/lang/String;Landroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/facebook/api/ISnsFacebookCallbackFriends;)I
    .locals 3
    .param p1, "objectID"    # Ljava/lang/String;
    .param p2, "bundle"    # Landroid/os/Bundle;
    .param p3, "cb"    # Lcom/sec/android/app/sns3/svc/sp/facebook/api/ISnsFacebookCallbackFriends;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 315
    const-string v1, "SNS"

    const-string v2, "getFriends CALLED!!!"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 317
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1$6;

    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1;->this$0:Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;

    # getter for: Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    invoke-static {v1}, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;->access$000(Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;)Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v1

    invoke-direct {v0, p0, v1, p1, p3}, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1$6;-><init>(Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;Lcom/sec/android/app/sns3/svc/sp/facebook/api/ISnsFacebookCallbackFriends;)V

    .line 331
    .local v0, "req":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->request()Z

    .line 333
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->getReqID()I

    move-result v1

    return v1
.end method

.method public getHome(Ljava/lang/String;Lcom/sec/android/app/sns3/svc/sp/facebook/api/ISnsFacebookCallbackPosts;)I
    .locals 3
    .param p1, "userID"    # Ljava/lang/String;
    .param p2, "cb"    # Lcom/sec/android/app/sns3/svc/sp/facebook/api/ISnsFacebookCallbackPosts;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 540
    const-string v1, "SNS"

    const-string v2, "getHome CALLED!!!"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 542
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1$15;

    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1;->this$0:Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;

    # getter for: Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    invoke-static {v1}, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;->access$000(Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;)Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v1

    invoke-direct {v0, p0, v1, p1, p2}, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1$15;-><init>(Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;Lcom/sec/android/app/sns3/svc/sp/facebook/api/ISnsFacebookCallbackPosts;)V

    .line 556
    .local v0, "req":Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqGetHome;
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqGetHome;->request()Z

    .line 558
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqGetHome;->getReqID()I

    move-result v1

    return v1
.end method

.method public getLatestPhoto(Ljava/lang/String;Lcom/sec/android/app/sns3/svc/sp/facebook/api/ISnsFacebookCallbackPicture;)I
    .locals 3
    .param p1, "userID"    # Ljava/lang/String;
    .param p2, "cb"    # Lcom/sec/android/app/sns3/svc/sp/facebook/api/ISnsFacebookCallbackPicture;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 784
    const-string v1, "SNS"

    const-string v2, "getLatestPhotoPost CALLED!!!"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 786
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1$25;

    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1;->this$0:Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;

    # getter for: Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    invoke-static {v1}, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;->access$000(Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;)Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v1

    invoke-direct {v0, p0, v1, p1, p2}, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1$25;-><init>(Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;Lcom/sec/android/app/sns3/svc/sp/facebook/api/ISnsFacebookCallbackPicture;)V

    .line 802
    .local v0, "req":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->request()Z

    .line 804
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->getReqID()I

    move-result v1

    return v1
.end method

.method public getLocationPost(Landroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/facebook/api/ISnsFacebookCallbackLocationPost;)I
    .locals 3
    .param p1, "bundle"    # Landroid/os/Bundle;
    .param p2, "cb"    # Lcom/sec/android/app/sns3/svc/sp/facebook/api/ISnsFacebookCallbackLocationPost;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 731
    const-string v1, "SNS"

    const-string v2, "getLocationPost CALLED!!!"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 733
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1$23;

    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1;->this$0:Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;

    # getter for: Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    invoke-static {v1}, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;->access$000(Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;)Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v1

    invoke-direct {v0, p0, v1, p1, p2}, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1$23;-><init>(Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/facebook/api/ISnsFacebookCallbackLocationPost;)V

    .line 749
    .local v0, "req":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->request()Z

    .line 751
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->getReqID()I

    move-result v1

    return v1
.end method

.method public getLocationPosts(Landroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/facebook/api/ISnsFacebookCallbackLocationPosts;)I
    .locals 3
    .param p1, "bundle"    # Landroid/os/Bundle;
    .param p2, "cb"    # Lcom/sec/android/app/sns3/svc/sp/facebook/api/ISnsFacebookCallbackLocationPosts;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 860
    const-string v1, "SNS"

    const-string v2, "getLocationPost CALLED!!!"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 862
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1$28;

    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1;->this$0:Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;

    # getter for: Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    invoke-static {v1}, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;->access$000(Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;)Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v1

    invoke-direct {v0, p0, v1, p1, p2}, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1$28;-><init>(Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/facebook/api/ISnsFacebookCallbackLocationPosts;)V

    .line 878
    .local v0, "req":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->request()Z

    .line 880
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->getReqID()I

    move-result v1

    return v1
.end method

.method public getMyLikes(Ljava/lang/String;Lcom/sec/android/app/sns3/svc/sp/facebook/api/ISnsFacebookCallbackMyLikes;)I
    .locals 3
    .param p1, "objectID"    # Ljava/lang/String;
    .param p2, "cb"    # Lcom/sec/android/app/sns3/svc/sp/facebook/api/ISnsFacebookCallbackMyLikes;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 706
    const-string v1, "SNS"

    const-string v2, "getMyLikes CALLED!!!"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 708
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1$22;

    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1;->this$0:Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;

    # getter for: Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    invoke-static {v1}, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;->access$000(Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;)Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v1

    invoke-direct {v0, p0, v1, p1, p2}, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1$22;-><init>(Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;Lcom/sec/android/app/sns3/svc/sp/facebook/api/ISnsFacebookCallbackMyLikes;)V

    .line 722
    .local v0, "req":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->request()Z

    .line 724
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->getReqID()I

    move-result v1

    return v1
.end method

.method public getNearby(Landroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/facebook/api/ISnsFacebookCallbackNearby;)I
    .locals 3
    .param p1, "bundle"    # Landroid/os/Bundle;
    .param p2, "cb"    # Lcom/sec/android/app/sns3/svc/sp/facebook/api/ISnsFacebookCallbackNearby;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 758
    const-string v1, "SNS"

    const-string v2, "getLocationPost CALLED!!!"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 760
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1$24;

    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1;->this$0:Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;

    # getter for: Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    invoke-static {v1}, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;->access$000(Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;)Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v1

    invoke-direct {v0, p0, v1, p1, p2}, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1$24;-><init>(Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/facebook/api/ISnsFacebookCallbackNearby;)V

    .line 776
    .local v0, "req":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->request()Z

    .line 778
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->getReqID()I

    move-result v1

    return v1
.end method

.method public getPhoto(Ljava/lang/String;Lcom/sec/android/app/sns3/svc/sp/facebook/api/ISnsFacebookCallbackPhoto;)I
    .locals 3
    .param p1, "objectID"    # Ljava/lang/String;
    .param p2, "cb"    # Lcom/sec/android/app/sns3/svc/sp/facebook/api/ISnsFacebookCallbackPhoto;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 223
    const-string v1, "SNS"

    const-string v2, "getPhoto CALLED!!!"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 225
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1$2;

    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1;->this$0:Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;

    # getter for: Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    invoke-static {v1}, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;->access$000(Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;)Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v1

    invoke-direct {v0, p0, v1, p1, p2}, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1$2;-><init>(Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;Lcom/sec/android/app/sns3/svc/sp/facebook/api/ISnsFacebookCallbackPhoto;)V

    .line 239
    .local v0, "req":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->request()Z

    .line 241
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->getReqID()I

    move-result v1

    return v1
.end method

.method public getPhotos(Ljava/lang/String;Landroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/facebook/api/ISnsFacebookCallbackPhotos;)I
    .locals 6
    .param p1, "objectID"    # Ljava/lang/String;
    .param p2, "bundle"    # Landroid/os/Bundle;
    .param p3, "cb"    # Lcom/sec/android/app/sns3/svc/sp/facebook/api/ISnsFacebookCallbackPhotos;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 246
    const-string v1, "SNS"

    const-string v2, "getPhotos CALLED!!!"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 248
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1$3;

    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1;->this$0:Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;

    # getter for: Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    invoke-static {v1}, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;->access$000(Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;)Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v2

    move-object v1, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1$3;-><init>(Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;Landroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/facebook/api/ISnsFacebookCallbackPhotos;)V

    .line 262
    .local v0, "req":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->request()Z

    .line 264
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->getReqID()I

    move-result v1

    return v1
.end method

.method public getPicture(Ljava/lang/String;Lcom/sec/android/app/sns3/svc/sp/facebook/api/ISnsFacebookCallbackPicture;)I
    .locals 3
    .param p1, "objectID"    # Ljava/lang/String;
    .param p2, "cb"    # Lcom/sec/android/app/sns3/svc/sp/facebook/api/ISnsFacebookCallbackPicture;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 610
    const-string v1, "SNS"

    const-string v2, "getPicture CALLED!!!"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 612
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1$18;

    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1;->this$0:Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;

    # getter for: Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    invoke-static {v1}, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;->access$000(Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;)Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v1

    invoke-direct {v0, p0, v1, p1, p2}, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1$18;-><init>(Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;Lcom/sec/android/app/sns3/svc/sp/facebook/api/ISnsFacebookCallbackPicture;)V

    .line 626
    .local v0, "req":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->request()Z

    .line 628
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->getReqID()I

    move-result v1

    return v1
.end method

.method public getProfilePicture(Ljava/lang/String;Lcom/sec/android/app/sns3/svc/sp/facebook/api/ISnsFacebookCallbackProfilePicture;)I
    .locals 3
    .param p1, "userID"    # Ljava/lang/String;
    .param p2, "cb"    # Lcom/sec/android/app/sns3/svc/sp/facebook/api/ISnsFacebookCallbackProfilePicture;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 810
    const-string v1, "SNS"

    const-string v2, "getProfilePicture CALLED!!!"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 812
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1$26;

    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1;->this$0:Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;

    # getter for: Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    invoke-static {v1}, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;->access$000(Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;)Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v1

    invoke-direct {v0, p0, v1, p1, p2}, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1$26;-><init>(Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;Lcom/sec/android/app/sns3/svc/sp/facebook/api/ISnsFacebookCallbackProfilePicture;)V

    .line 828
    .local v0, "req":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->request()Z

    .line 830
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->getReqID()I

    move-result v1

    return v1
.end method

.method public getStatus(Ljava/lang/String;Lcom/sec/android/app/sns3/svc/sp/facebook/api/ISnsFacebookCallbackPosts;)I
    .locals 3
    .param p1, "userID"    # Ljava/lang/String;
    .param p2, "cb"    # Lcom/sec/android/app/sns3/svc/sp/facebook/api/ISnsFacebookCallbackPosts;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 563
    const-string v1, "SNS"

    const-string v2, "getStatus CALLED!!!"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 565
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1$16;

    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1;->this$0:Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;

    # getter for: Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    invoke-static {v1}, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;->access$000(Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;)Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v1

    invoke-direct {v0, p0, v1, p1, p2}, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1$16;-><init>(Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;Lcom/sec/android/app/sns3/svc/sp/facebook/api/ISnsFacebookCallbackPosts;)V

    .line 579
    .local v0, "req":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->request()Z

    .line 581
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->getReqID()I

    move-result v1

    return v1
.end method

.method public getStatuses(Ljava/lang/String;Landroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/facebook/api/ISnsFacebookCallbackStatuses;)I
    .locals 3
    .param p1, "objectID"    # Ljava/lang/String;
    .param p2, "bundle"    # Landroid/os/Bundle;
    .param p3, "cb"    # Lcom/sec/android/app/sns3/svc/sp/facebook/api/ISnsFacebookCallbackStatuses;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 338
    const-string v1, "SNS"

    const-string v2, "getFriends CALLED!!!"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 340
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1$7;

    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1;->this$0:Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;

    # getter for: Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    invoke-static {v1}, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;->access$000(Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;)Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v1

    invoke-direct {v0, p0, v1, p1, p3}, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1$7;-><init>(Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;Lcom/sec/android/app/sns3/svc/sp/facebook/api/ISnsFacebookCallbackStatuses;)V

    .line 354
    .local v0, "req":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->request()Z

    .line 356
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->getReqID()I

    move-result v1

    return v1
.end method

.method public getUser(Ljava/lang/String;Lcom/sec/android/app/sns3/svc/sp/facebook/api/ISnsFacebookCallbackUser;)I
    .locals 3
    .param p1, "userID"    # Ljava/lang/String;
    .param p2, "cb"    # Lcom/sec/android/app/sns3/svc/sp/facebook/api/ISnsFacebookCallbackUser;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 199
    const-string v1, "SNS"

    const-string v2, "getUser CALLED!!!"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 201
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1$1;

    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1;->this$0:Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;

    # getter for: Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    invoke-static {v1}, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;->access$000(Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;)Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v1

    invoke-direct {v0, p0, v1, p1, p2}, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1$1;-><init>(Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;Lcom/sec/android/app/sns3/svc/sp/facebook/api/ISnsFacebookCallbackUser;)V

    .line 216
    .local v0, "req":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->request()Z

    .line 218
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->getReqID()I

    move-result v1

    return v1
.end method

.method public getVideo(Ljava/lang/String;Lcom/sec/android/app/sns3/svc/sp/facebook/api/ISnsFacebookCallbackVideo;)I
    .locals 3
    .param p1, "objectID"    # Ljava/lang/String;
    .param p2, "cb"    # Lcom/sec/android/app/sns3/svc/sp/facebook/api/ISnsFacebookCallbackVideo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 835
    const-string v1, "SNS"

    const-string v2, "getVideo CALLED!!!"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 837
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1$27;

    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1;->this$0:Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;

    # getter for: Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    invoke-static {v1}, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;->access$000(Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;)Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v1

    invoke-direct {v0, p0, v1, p1, p2}, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1$27;-><init>(Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;Lcom/sec/android/app/sns3/svc/sp/facebook/api/ISnsFacebookCallbackVideo;)V

    .line 851
    .local v0, "req":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->request()Z

    .line 853
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->getReqID()I

    move-result v1

    return v1
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 2
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 170
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->hasAccessToRestrictedData(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 171
    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "Access denied"

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 173
    :cond_0
    invoke-super {p0, p1, p2, p3, p4}, Lcom/sec/android/app/sns3/svc/sp/facebook/api/ISnsFacebook$Stub;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v0

    return v0
.end method

.method public postComment(Ljava/lang/String;Landroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/facebook/api/ISnsFacebookCallbackID;)I
    .locals 6
    .param p1, "objectID"    # Ljava/lang/String;
    .param p2, "bundle"    # Landroid/os/Bundle;
    .param p3, "cb"    # Lcom/sec/android/app/sns3/svc/sp/facebook/api/ISnsFacebookCallbackID;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 634
    const-string v1, "SNS"

    const-string v2, "postComment CALLED!!!"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 636
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1$19;

    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1;->this$0:Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;

    # getter for: Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    invoke-static {v1}, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;->access$000(Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;)Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v2

    move-object v1, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1$19;-><init>(Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;Landroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/facebook/api/ISnsFacebookCallbackID;)V

    .line 650
    .local v0, "req":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->request()Z

    .line 652
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->getReqID()I

    move-result v1

    return v1
.end method

.method public postEvent(Ljava/lang/String;Landroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/facebook/api/ISnsFacebookCallbackID;)I
    .locals 6
    .param p1, "objectID"    # Ljava/lang/String;
    .param p2, "bundle"    # Landroid/os/Bundle;
    .param p3, "cb"    # Lcom/sec/android/app/sns3/svc/sp/facebook/api/ISnsFacebookCallbackID;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 422
    const-string v1, "SNS"

    const-string v2, "postEvent CALLED!!!"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 425
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1$10;

    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1;->this$0:Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;

    # getter for: Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    invoke-static {v1}, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;->access$000(Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;)Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v2

    move-object v1, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1$10;-><init>(Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;Landroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/facebook/api/ISnsFacebookCallbackID;)V

    .line 439
    .local v0, "req":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->request()Z

    .line 441
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->getReqID()I

    move-result v1

    return v1
.end method

.method public postEventMemberInvited(Ljava/lang/String;Landroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/facebook/api/ISnsFacebookCallbackBoolean;)I
    .locals 6
    .param p1, "objectID"    # Ljava/lang/String;
    .param p2, "bundle"    # Landroid/os/Bundle;
    .param p3, "cb"    # Lcom/sec/android/app/sns3/svc/sp/facebook/api/ISnsFacebookCallbackBoolean;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 469
    const-string v1, "SNS"

    const-string v2, "postEventMemberInvited CALLED!!!"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 471
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1$12;

    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1;->this$0:Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;

    # getter for: Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    invoke-static {v1}, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;->access$000(Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;)Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v2

    move-object v1, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1$12;-><init>(Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;Landroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/facebook/api/ISnsFacebookCallbackBoolean;)V

    .line 485
    .local v0, "req":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->request()Z

    .line 487
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->getReqID()I

    move-result v1

    return v1
.end method

.method public postFeed(Ljava/lang/String;Landroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/facebook/api/ISnsFacebookCallbackID;)I
    .locals 6
    .param p1, "objectID"    # Ljava/lang/String;
    .param p2, "bundle"    # Landroid/os/Bundle;
    .param p3, "cb"    # Lcom/sec/android/app/sns3/svc/sp/facebook/api/ISnsFacebookCallbackID;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 446
    const-string v1, "SNS"

    const-string v2, "postFeed CALLED!!!"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 448
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1$11;

    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1;->this$0:Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;

    # getter for: Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    invoke-static {v1}, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;->access$000(Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;)Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v2

    move-object v1, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1$11;-><init>(Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;Landroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/facebook/api/ISnsFacebookCallbackID;)V

    .line 462
    .local v0, "req":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->request()Z

    .line 464
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->getReqID()I

    move-result v1

    return v1
.end method

.method public postLike(Ljava/lang/String;Lcom/sec/android/app/sns3/svc/sp/facebook/api/ISnsFacebookCallbackBoolean;)I
    .locals 3
    .param p1, "objectID"    # Ljava/lang/String;
    .param p2, "cb"    # Lcom/sec/android/app/sns3/svc/sp/facebook/api/ISnsFacebookCallbackBoolean;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 658
    const-string v1, "SNS"

    const-string v2, "postLike CALLED!!!"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 660
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1$20;

    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1;->this$0:Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;

    # getter for: Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    invoke-static {v1}, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;->access$000(Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;)Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v1

    invoke-direct {v0, p0, v1, p1, p2}, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1$20;-><init>(Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;Lcom/sec/android/app/sns3/svc/sp/facebook/api/ISnsFacebookCallbackBoolean;)V

    .line 674
    .local v0, "req":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->request()Z

    .line 676
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->getReqID()I

    move-result v1

    return v1
.end method

.method public postPhoto(Ljava/lang/String;Landroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/facebook/api/ISnsFacebookCallbackID;)I
    .locals 8
    .param p1, "objectID"    # Ljava/lang/String;
    .param p2, "bundle"    # Landroid/os/Bundle;
    .param p3, "cb"    # Lcom/sec/android/app/sns3/svc/sp/facebook/api/ISnsFacebookCallbackID;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 361
    const-string v1, "SNS"

    const-string v2, "postPhoto CALLED!!!"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 363
    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1;->this$0:Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;

    const-wide/16 v2, 0x0

    # setter for: Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;->mProgressNum:J
    invoke-static {v1, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;->access$102(Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;J)J

    .line 364
    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1;->this$0:Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;

    # getter for: Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;->mReqEntity:Lcom/sec/android/app/sns3/svc/http/SnsCustomMultiPartEntity;
    invoke-static {v1}, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;->access$200(Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;)Lcom/sec/android/app/sns3/svc/http/SnsCustomMultiPartEntity;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 365
    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1;->this$0:Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;

    # setter for: Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;->mReqEntity:Lcom/sec/android/app/sns3/svc/http/SnsCustomMultiPartEntity;
    invoke-static {v1, v5}, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;->access$202(Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;Lcom/sec/android/app/sns3/svc/http/SnsCustomMultiPartEntity;)Lcom/sec/android/app/sns3/svc/http/SnsCustomMultiPartEntity;

    .line 367
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1;->this$0:Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;

    new-instance v2, Lcom/sec/android/app/sns3/svc/http/SnsCustomMultiPartEntity;

    sget-object v3, Lorg/apache/http/entity/mime/HttpMultipartMode;->BROWSER_COMPATIBLE:Lorg/apache/http/entity/mime/HttpMultipartMode;

    new-instance v4, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1$8;

    invoke-direct {v4, p0}, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1$8;-><init>(Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1;)V

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/sns3/svc/http/SnsCustomMultiPartEntity;-><init>(Lorg/apache/http/entity/mime/HttpMultipartMode;Lcom/sec/android/app/sns3/svc/http/SnsCustomMultiPartEntity$ProgressListener;)V

    # setter for: Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;->mReqEntity:Lcom/sec/android/app/sns3/svc/http/SnsCustomMultiPartEntity;
    invoke-static {v1, v2}, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;->access$202(Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;Lcom/sec/android/app/sns3/svc/http/SnsCustomMultiPartEntity;)Lcom/sec/android/app/sns3/svc/http/SnsCustomMultiPartEntity;

    .line 386
    new-instance v7, Landroid/content/Intent;

    const-string v1, "com.sec.android.app.sns3.svc.sp.facebook.SNS_FB_NOTIFICATION"

    iget-object v2, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1;->this$0:Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;

    const-class v3, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbBroadcastReceiver;

    invoke-direct {v7, v1, v5, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 387
    .local v7, "intent":Landroid/content/Intent;
    const-string v1, "SNS_FB_NOTIFICATION.progress"

    iget-object v2, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1;->this$0:Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;

    # getter for: Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;->mProgressNum:J
    invoke-static {v2}, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;->access$100(Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;)J

    move-result-wide v2

    invoke-virtual {v7, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 388
    const-string v1, "SNS_FB_NOTIFICATION.state"

    const/4 v2, 0x0

    invoke-virtual {v7, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 389
    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1;->this$0:Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;

    invoke-virtual {v1, v7}, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;->sendBroadcast(Landroid/content/Intent;)V

    .line 391
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1$9;

    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1;->this$0:Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;

    # getter for: Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    invoke-static {v1}, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;->access$000(Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;)Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v2

    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1;->this$0:Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;

    # getter for: Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;->mReqEntity:Lcom/sec/android/app/sns3/svc/http/SnsCustomMultiPartEntity;
    invoke-static {v1}, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;->access$200(Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;)Lcom/sec/android/app/sns3/svc/http/SnsCustomMultiPartEntity;

    move-result-object v5

    move-object v1, p0

    move-object v3, p1

    move-object v4, p2

    move-object v6, p3

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1$9;-><init>(Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;Landroid/os/Bundle;Lorg/apache/http/entity/mime/MultipartEntity;Lcom/sec/android/app/sns3/svc/sp/facebook/api/ISnsFacebookCallbackID;)V

    .line 415
    .local v0, "req":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->request()Z

    .line 417
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->getReqID()I

    move-result v1

    return v1
.end method

.method public searchLocation(Landroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/facebook/api/ISnsFacebookCallbackPlaces;)I
    .locals 3
    .param p1, "bundle"    # Landroid/os/Bundle;
    .param p2, "cb"    # Lcom/sec/android/app/sns3/svc/sp/facebook/api/ISnsFacebookCallbackPlaces;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 492
    const-string v1, "SNS"

    const-string v2, "searchLocation CALLED!!!"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 494
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1$13;

    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1;->this$0:Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;

    # getter for: Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    invoke-static {v1}, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;->access$000(Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;)Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v1

    invoke-direct {v0, p0, v1, p1, p2}, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1$13;-><init>(Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/facebook/api/ISnsFacebookCallbackPlaces;)V

    .line 510
    .local v0, "req":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->request()Z

    .line 512
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->getReqID()I

    move-result v1

    return v1
.end method

.method public setAuthTokenNExpires(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3
    .param p1, "accessToken"    # Ljava/lang/String;
    .param p2, "expires"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 189
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 190
    :cond_0
    const/4 v1, 0x0

    .line 194
    :goto_0
    return v1

    .line 192
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1;->this$0:Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;

    # getter for: Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    invoke-static {v1}, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;->access$000(Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;)Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/sns3/svc/SnsSvcMgr;->getTokenMgr()Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;

    move-result-object v1

    const-string v2, "facebook"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;->getToken(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbToken;

    .line 193
    .local v0, "fbToken":Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbToken;
    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbToken;->setAccessTokenNExpires(Ljava/lang/String;Ljava/lang/String;)V

    .line 194
    const/4 v1, 0x1

    goto :goto_0
.end method
