.class public Lcom/sec/android/app/sns3/svc/sp/twitter/parser/SnsTwParserIntArray;
.super Ljava/lang/Object;
.source "SnsTwParserIntArray.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseIntArray;
    .locals 6
    .param p0, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 30
    new-instance v3, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseIntArray;

    invoke-direct {v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseIntArray;-><init>()V

    .line 33
    .local v3, "response":Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseIntArray;
    invoke-static {p0}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->isJsonEmpty(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 34
    const/4 v3, 0x0

    .line 49
    .end local v3    # "response":Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseIntArray;
    :cond_0
    :goto_0
    return-object v3

    .line 38
    .restart local v3    # "response":Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseIntArray;
    :cond_1
    :try_start_0
    new-instance v2, Lorg/json/JSONArray;

    invoke-direct {v2, p0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 39
    .local v2, "jsonArray":Lorg/json/JSONArray;
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v4

    new-array v4, v4, [I

    iput-object v4, v3, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseIntArray;->mIntArray:[I

    .line 41
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v4

    if-ge v1, v4, :cond_0

    .line 42
    iget-object v4, v3, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseIntArray;->mIntArray:[I

    invoke-virtual {v2, v1}, Lorg/json/JSONArray;->optInt(I)I

    move-result v5

    aput v5, v4, v1
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 41
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 44
    .end local v1    # "i":I
    .end local v2    # "jsonArray":Lorg/json/JSONArray;
    :catch_0
    move-exception v0

    .line 46
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method
