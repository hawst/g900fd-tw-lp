.class public Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserVideoFormat;
.super Ljava/lang/Object;
.source "SnsFbParserVideoFormat.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserVideoFormat$FacebookVideoFormat;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    return-void
.end method

.method public static parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseVideoFormat;
    .locals 11
    .param p0, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 47
    const/4 v2, 0x0

    .line 48
    .local v2, "format":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseVideoFormat;
    const/4 v0, 0x0

    .line 51
    .local v0, "curFormat":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseVideoFormat;
    :try_start_0
    new-instance v4, Lorg/json/JSONArray;

    invoke-direct {v4, p0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 53
    .local v4, "jArrFormat":Lorg/json/JSONArray;
    if-eqz v4, :cond_1

    .line 54
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v8

    if-ge v3, v8, :cond_1

    .line 55
    invoke-virtual {v4, v3}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v6

    .line 57
    .local v6, "jsonObj":Lorg/json/JSONObject;
    new-instance v7, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseVideoFormat;

    invoke-direct {v7}, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseVideoFormat;-><init>()V

    .line 59
    .local v7, "newFormat":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseVideoFormat;
    const-string v8, "embed_html"

    invoke-virtual {v6, v8}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, v7, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseVideoFormat;->mEmbedHtml:Ljava/lang/String;

    .line 60
    const-string v8, "width"

    invoke-virtual {v6, v8}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, v7, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseVideoFormat;->mWidth:Ljava/lang/String;

    .line 61
    const-string v8, "height"

    invoke-virtual {v6, v8}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, v7, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseVideoFormat;->mHeight:Ljava/lang/String;

    .line 62
    const-string v8, "filter"

    invoke-virtual {v6, v8}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, v7, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseVideoFormat;->mFilter:Ljava/lang/String;

    .line 63
    const-string v8, "picture"

    invoke-virtual {v6, v8}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, v7, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseVideoFormat;->mPicture:Ljava/lang/String;

    .line 65
    if-nez v2, :cond_0

    .line 66
    move-object v2, v7

    .line 67
    move-object v0, v2

    .line 54
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 69
    :cond_0
    iput-object v7, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseVideoFormat;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseVideoFormat;

    .line 70
    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseVideoFormat;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseVideoFormat;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_1

    .line 74
    .end local v3    # "i":I
    .end local v4    # "jArrFormat":Lorg/json/JSONArray;
    .end local v6    # "jsonObj":Lorg/json/JSONObject;
    .end local v7    # "newFormat":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseVideoFormat;
    :catch_0
    move-exception v5

    .line 75
    .local v5, "je":Lorg/json/JSONException;
    const-string v8, "SNS"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "parser : JSONException error :"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v5}, Lorg/json/JSONException;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 80
    .end local v5    # "je":Lorg/json/JSONException;
    :cond_1
    :goto_2
    return-object v2

    .line 76
    :catch_1
    move-exception v1

    .line 77
    .local v1, "e":Ljava/lang/Exception;
    const-string v8, "SNS"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "parser : Exception error : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method
