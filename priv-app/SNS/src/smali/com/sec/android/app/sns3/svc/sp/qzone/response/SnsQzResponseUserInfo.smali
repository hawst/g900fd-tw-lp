.class public final Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseUserInfo;
.super Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;
.source "SnsQzResponseUserInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseUserInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public mGender:Ljava/lang/String;

.field public mNext:Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseUserInfo;

.field public mProfileUrl:Ljava/lang/String;

.field public mUserName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseUserInfo$1;

    invoke-direct {v0}, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseUserInfo$1;-><init>()V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseUserInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;-><init>()V

    .line 49
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;-><init>()V

    .line 52
    invoke-virtual {p0, p1}, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseUserInfo;->readFromParcel(Landroid/os/Parcel;)V

    .line 53
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseUserInfo$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseUserInfo$1;

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseUserInfo;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 76
    const/4 v0, 0x0

    return v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 66
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseUserInfo;->mUserName:Ljava/lang/String;

    .line 67
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseUserInfo;->mProfileUrl:Ljava/lang/String;

    .line 68
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseUserInfo;->mGender:Ljava/lang/String;

    .line 70
    const-class v0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseUserInfo;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseUserInfo;

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseUserInfo;->mNext:Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseUserInfo;

    .line 71
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flag"    # I

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseUserInfo;->mUserName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 58
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseUserInfo;->mProfileUrl:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 59
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseUserInfo;->mGender:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 61
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseUserInfo;->mNext:Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseUserInfo;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 63
    return-void
.end method
