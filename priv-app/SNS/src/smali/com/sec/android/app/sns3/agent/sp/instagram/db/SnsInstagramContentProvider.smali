.class public Lcom/sec/android/app/sns3/agent/sp/instagram/db/SnsInstagramContentProvider;
.super Landroid/content/ContentProvider;
.source "SnsInstagramContentProvider.java"


# static fields
.field private static final FRIENDS_PROFILE_INFO:I = 0x320

.field private static final STATUS_STREAM:I = 0x384

.field private static final SYNC_ALBUM:I = 0x190

.field private static final SYNC_GALLERY:I = 0x1f4

.field private static final SYNC_GALLERY_ALBUM_ID:I = 0x258

.field public static final SYNC_GALLERY_URI_NAME:Ljava/lang/String; = "sync_gallery"

.field private static final USER_BASIC_INFO:I = 0x64

.field private static final USER_FEED_INFO:I = 0x12c

.field private static final USER_INFO:I = 0x2bc

.field private static final USER_PROFILE_INFO:I = 0xc8

.field private static final WIPE_IN_DATA:I = 0x960

.field private static final uriMatcher:Landroid/content/UriMatcher;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mOpenHelper:Lcom/sec/android/app/sns3/agent/sp/instagram/db/SnsInstagramDBHelper;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 405
    new-instance v0, Landroid/content/UriMatcher;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    sput-object v0, Lcom/sec/android/app/sns3/agent/sp/instagram/db/SnsInstagramContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    .line 407
    sget-object v0, Lcom/sec/android/app/sns3/agent/sp/instagram/db/SnsInstagramContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.sns3.sp.instagram"

    const-string v2, "user_basic_info"

    const/16 v3, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 409
    sget-object v0, Lcom/sec/android/app/sns3/agent/sp/instagram/db/SnsInstagramContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.sns3.sp.instagram"

    const-string v2, "user_profile_info"

    const/16 v3, 0xc8

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 411
    sget-object v0, Lcom/sec/android/app/sns3/agent/sp/instagram/db/SnsInstagramContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.sns3.sp.instagram"

    const-string v2, "user_feed_info"

    const/16 v3, 0x12c

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 413
    sget-object v0, Lcom/sec/android/app/sns3/agent/sp/instagram/db/SnsInstagramContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.sns3.sp.instagram"

    const-string v2, "sync_album"

    const/16 v3, 0x190

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 415
    sget-object v0, Lcom/sec/android/app/sns3/agent/sp/instagram/db/SnsInstagramContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.sns3.sp.instagram"

    const-string v2, "sync_photo"

    const/16 v3, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 417
    sget-object v0, Lcom/sec/android/app/sns3/agent/sp/instagram/db/SnsInstagramContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.sns3.sp.instagram"

    const-string v2, "sync_gallery/album_id/#"

    const/16 v3, 0x258

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 419
    sget-object v0, Lcom/sec/android/app/sns3/agent/sp/instagram/db/SnsInstagramContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.sns3.sp.instagram"

    const-string v2, "user_info"

    const/16 v3, 0x2bc

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 420
    sget-object v0, Lcom/sec/android/app/sns3/agent/sp/instagram/db/SnsInstagramContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.sns3.sp.instagram"

    const-string v2, "friends_profile_info"

    const/16 v3, 0x320

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 422
    sget-object v0, Lcom/sec/android/app/sns3/agent/sp/instagram/db/SnsInstagramContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.sns3.sp.instagram"

    const-string v2, "status_stream"

    const/16 v3, 0x384

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 425
    sget-object v0, Lcom/sec/android/app/sns3/agent/sp/instagram/db/SnsInstagramContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.app.sns3.sp.instagram"

    const-string v2, "wipe_in_data"

    const/16 v3, 0x960

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 428
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 46
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    .line 48
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/sns3/agent/sp/instagram/db/SnsInstagramContentProvider;->mOpenHelper:Lcom/sec/android/app/sns3/agent/sp/instagram/db/SnsInstagramDBHelper;

    return-void
.end method


# virtual methods
.method public bulkInsert(Landroid/net/Uri;[Landroid/content/ContentValues;)I
    .locals 4
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # [Landroid/content/ContentValues;

    .prologue
    .line 263
    const/4 v0, 0x0

    .line 264
    .local v0, "cnt":I
    array-length v2, p2

    .line 266
    .local v2, "numValues":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_1

    .line 267
    aget-object v3, p2, v1

    invoke-virtual {p0, p1, v3}, Lcom/sec/android/app/sns3/agent/sp/instagram/db/SnsInstagramContentProvider;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 268
    add-int/lit8 v0, v0, 0x1

    .line 266
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 270
    :cond_1
    return v0
.end method

.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 6
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "selection"    # Ljava/lang/String;
    .param p3, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 276
    const/4 v1, 0x0

    .line 277
    .local v1, "db":Landroid/database/sqlite/SQLiteDatabase;
    sget-object v3, Lcom/sec/android/app/sns3/agent/sp/instagram/db/SnsInstagramContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v3, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v2

    .line 278
    .local v2, "match":I
    const/4 v0, -0x1

    .line 280
    .local v0, "count":I
    iget-object v3, p0, Lcom/sec/android/app/sns3/agent/sp/instagram/db/SnsInstagramContentProvider;->mOpenHelper:Lcom/sec/android/app/sns3/agent/sp/instagram/db/SnsInstagramDBHelper;

    invoke-virtual {v3}, Lcom/sec/android/app/sns3/agent/sp/instagram/db/SnsInstagramDBHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 282
    sparse-switch v2, :sswitch_data_0

    .line 342
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Unknown URI : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 284
    :sswitch_0
    const-string v3, "user_basic_info"

    invoke-static {v1, v3, p2, p3}, Lcom/sec/android/app/sns3/agent/db/SnsDBWrapper;->onDelete(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 345
    :goto_0
    return v0

    .line 289
    :sswitch_1
    const-string v3, "user_profile_info"

    invoke-static {v1, v3, p2, p3}, Lcom/sec/android/app/sns3/agent/db/SnsDBWrapper;->onDelete(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 292
    goto :goto_0

    .line 295
    :sswitch_2
    const-string v3, "user_feed_info"

    invoke-static {v1, v3, p2, p3}, Lcom/sec/android/app/sns3/agent/db/SnsDBWrapper;->onDelete(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 298
    goto :goto_0

    .line 301
    :sswitch_3
    const-string v3, "sync_album"

    invoke-static {v1, v3, p2, p3}, Lcom/sec/android/app/sns3/agent/db/SnsDBWrapper;->onDelete(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 304
    goto :goto_0

    .line 307
    :sswitch_4
    const-string v3, "sync_photo"

    invoke-static {v1, v3, p2, p3}, Lcom/sec/android/app/sns3/agent/db/SnsDBWrapper;->onDelete(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 310
    goto :goto_0

    .line 313
    :sswitch_5
    const-string v3, "sync_photo"

    invoke-static {v1, v3, p2, p3}, Lcom/sec/android/app/sns3/agent/db/SnsDBWrapper;->onDelete(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 316
    goto :goto_0

    .line 319
    :sswitch_6
    const-string v3, "user_info"

    invoke-static {v1, v3, p2, p3}, Lcom/sec/android/app/sns3/agent/db/SnsDBWrapper;->onDelete(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 322
    goto :goto_0

    .line 325
    :sswitch_7
    const-string v3, "friends_profile_info"

    invoke-static {v1, v3, p2, p3}, Lcom/sec/android/app/sns3/agent/db/SnsDBWrapper;->onDelete(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 328
    goto :goto_0

    .line 331
    :sswitch_8
    const-string v3, "status_stream"

    invoke-static {v1, v3, p2, p3}, Lcom/sec/android/app/sns3/agent/db/SnsDBWrapper;->onDelete(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 334
    goto :goto_0

    .line 337
    :sswitch_9
    iget-object v3, p0, Lcom/sec/android/app/sns3/agent/sp/instagram/db/SnsInstagramContentProvider;->mOpenHelper:Lcom/sec/android/app/sns3/agent/sp/instagram/db/SnsInstagramDBHelper;

    invoke-virtual {v3, v1}, Lcom/sec/android/app/sns3/agent/sp/instagram/db/SnsInstagramDBHelper;->wipeData(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_0

    .line 282
    nop

    :sswitch_data_0
    .sparse-switch
        0x64 -> :sswitch_0
        0xc8 -> :sswitch_1
        0x12c -> :sswitch_2
        0x190 -> :sswitch_3
        0x1f4 -> :sswitch_4
        0x258 -> :sswitch_5
        0x2bc -> :sswitch_6
        0x320 -> :sswitch_7
        0x384 -> :sswitch_8
        0x960 -> :sswitch_9
    .end sparse-switch
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 3
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 170
    sget-object v0, Lcom/sec/android/app/sns3/agent/sp/instagram/db/SnsInstagramContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 199
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown URI : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 172
    :sswitch_0
    const-string v0, "vnd.android.cursor.dir/vnd.com.sec.android.app.sns3.sp.instagram.user"

    .line 196
    :goto_0
    return-object v0

    .line 175
    :sswitch_1
    const-string v0, "vnd.android.cursor.dir/vnd.com.sec.android.app.sns3.sp.instagram.userprofile"

    goto :goto_0

    .line 178
    :sswitch_2
    const-string v0, "vnd.android.cursor.dir/vnd.com.sec.android.app.sns3.sp.instagram.userfeed"

    goto :goto_0

    .line 181
    :sswitch_3
    const-string v0, "vnd.android.cursor.dir/vnd.com.sec.android.app.sns3.sp.instagram.syncalbum"

    goto :goto_0

    .line 184
    :sswitch_4
    const-string v0, "vnd.android.cursor.dir/vnd.com.sec.android.app.sns3.sp.instagram.syncphoto"

    goto :goto_0

    .line 187
    :sswitch_5
    const-string v0, "vnd.android.cursor.dir/vnd.com.sec.android.app.sns3.sp.instagram.syncphoto"

    goto :goto_0

    .line 190
    :sswitch_6
    const-string v0, "vnd.android.cursor.dir/vnd.com.sec.android.app.sns3.sp.instagram.userinfo"

    goto :goto_0

    .line 193
    :sswitch_7
    const-string v0, "vnd.android.cursor.dir/vnd.com.sec.android.app.sns3.sp.instagram.friendsprofile"

    goto :goto_0

    .line 196
    :sswitch_8
    const-string v0, "vnd.android.cursor.dir/vnd.com.sec.android.app.sns3.sp.instagram.status_stream"

    goto :goto_0

    .line 170
    nop

    :sswitch_data_0
    .sparse-switch
        0x64 -> :sswitch_0
        0xc8 -> :sswitch_1
        0x12c -> :sswitch_2
        0x190 -> :sswitch_3
        0x1f4 -> :sswitch_4
        0x258 -> :sswitch_5
        0x2bc -> :sswitch_6
        0x320 -> :sswitch_7
        0x384 -> :sswitch_8
    .end sparse-switch
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 9
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;

    .prologue
    const/4 v3, 0x0

    .line 206
    const/4 v0, 0x0

    .line 207
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    sget-object v6, Lcom/sec/android/app/sns3/agent/sp/instagram/db/SnsInstagramContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v6, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v2

    .line 208
    .local v2, "match":I
    const-wide/16 v4, -0x1

    .line 211
    .local v4, "rowId":J
    :try_start_0
    iget-object v6, p0, Lcom/sec/android/app/sns3/agent/sp/instagram/db/SnsInstagramContentProvider;->mOpenHelper:Lcom/sec/android/app/sns3/agent/sp/instagram/db/SnsInstagramDBHelper;

    invoke-virtual {v6}, Lcom/sec/android/app/sns3/agent/sp/instagram/db/SnsInstagramDBHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 213
    sparse-switch v2, :sswitch_data_0

    .line 251
    new-instance v6, Ljava/lang/IllegalArgumentException;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Unknown URI : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v6
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 253
    :catch_0
    move-exception v1

    .line 254
    .local v1, "e":Landroid/database/sqlite/SQLiteException;
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    .line 257
    .end local v1    # "e":Landroid/database/sqlite/SQLiteException;
    :goto_0
    const-wide/16 v6, -0x1

    cmp-long v6, v4, v6

    if-nez v6, :cond_0

    move-object p1, v3

    .end local p1    # "uri":Landroid/net/Uri;
    :cond_0
    return-object p1

    .line 215
    .restart local p1    # "uri":Landroid/net/Uri;
    :sswitch_0
    :try_start_1
    const-string v6, "user_basic_info"

    const/4 v7, 0x0

    invoke-virtual {v0, v6, v7, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v4

    .line 217
    goto :goto_0

    .line 219
    :sswitch_1
    const-string v6, "user_profile_info"

    const/4 v7, 0x0

    invoke-virtual {v0, v6, v7, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v4

    .line 221
    goto :goto_0

    .line 223
    :sswitch_2
    const-string v6, "user_feed_info"

    const/4 v7, 0x0

    invoke-virtual {v0, v6, v7, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v4

    .line 225
    goto :goto_0

    .line 227
    :sswitch_3
    const-string v6, "sync_album"

    const/4 v7, 0x0

    invoke-virtual {v0, v6, v7, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v4

    .line 229
    goto :goto_0

    .line 231
    :sswitch_4
    const-string v6, "sync_photo"

    const/4 v7, 0x0

    invoke-virtual {v0, v6, v7, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v4

    .line 233
    goto :goto_0

    .line 235
    :sswitch_5
    const-string v6, "sync_photo"

    const/4 v7, 0x0

    invoke-virtual {v0, v6, v7, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v4

    .line 237
    goto :goto_0

    .line 239
    :sswitch_6
    const-string v6, "user_info"

    const/4 v7, 0x0

    invoke-virtual {v0, v6, v7, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v4

    .line 241
    goto :goto_0

    .line 243
    :sswitch_7
    const-string v6, "friends_profile_info"

    const/4 v7, 0x0

    invoke-virtual {v0, v6, v7, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v4

    .line 245
    goto :goto_0

    .line 247
    :sswitch_8
    const-string v6, "status_stream"

    const/4 v7, 0x0

    invoke-virtual {v0, v6, v7, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-wide v4

    .line 249
    goto :goto_0

    .line 213
    :sswitch_data_0
    .sparse-switch
        0x64 -> :sswitch_0
        0xc8 -> :sswitch_1
        0x12c -> :sswitch_2
        0x190 -> :sswitch_3
        0x1f4 -> :sswitch_4
        0x258 -> :sswitch_5
        0x2bc -> :sswitch_6
        0x320 -> :sswitch_7
        0x384 -> :sswitch_8
    .end sparse-switch
.end method

.method public onCreate()Z
    .locals 2

    .prologue
    .line 78
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/instagram/db/SnsInstagramContentProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/agent/sp/instagram/db/SnsInstagramContentProvider;->mContext:Landroid/content/Context;

    .line 79
    new-instance v0, Lcom/sec/android/app/sns3/agent/sp/instagram/db/SnsInstagramDBHelper;

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/instagram/db/SnsInstagramContentProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/app/sns3/agent/sp/instagram/db/SnsInstagramDBHelper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/sns3/agent/sp/instagram/db/SnsInstagramContentProvider;->mOpenHelper:Lcom/sec/android/app/sns3/agent/sp/instagram/db/SnsInstagramDBHelper;

    .line 80
    const/4 v0, 0x0

    return v0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 17
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "sortOrder"    # Ljava/lang/String;

    .prologue
    .line 86
    const/4 v3, 0x0

    .line 87
    .local v3, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v10, 0x0

    .line 89
    .local v10, "c":Landroid/database/Cursor;
    sget-object v4, Lcom/sec/android/app/sns3/agent/sp/instagram/db/SnsInstagramContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v13

    .line 91
    .local v13, "match":I
    const/4 v7, 0x0

    .line 93
    .local v7, "groupby":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/sns3/agent/sp/instagram/db/SnsInstagramContentProvider;->mOpenHelper:Lcom/sec/android/app/sns3/agent/sp/instagram/db/SnsInstagramDBHelper;

    invoke-virtual {v4}, Lcom/sec/android/app/sns3/agent/sp/instagram/db/SnsInstagramDBHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    .line 94
    new-instance v2, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v2}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 96
    .local v2, "queryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v14

    .line 98
    .local v14, "pathSegmentList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    sparse-switch v13, :sswitch_data_0

    .line 152
    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Unknown URI : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 100
    :sswitch_0
    const-string v4, "user_basic_info"

    invoke-virtual {v2, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 157
    :cond_0
    :goto_0
    const/4 v8, 0x0

    move-object/from16 v4, p2

    move-object/from16 v5, p3

    move-object/from16 v6, p4

    move-object/from16 v9, p5

    :try_start_0
    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v10

    .line 164
    :goto_1
    return-object v10

    .line 104
    :sswitch_1
    const-string v4, "user_profile_info"

    invoke-virtual {v2, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto :goto_0

    .line 108
    :sswitch_2
    const-string v4, "user_feed_info"

    invoke-virtual {v2, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto :goto_0

    .line 112
    :sswitch_3
    const-string v4, "sync_album"

    invoke-virtual {v2, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto :goto_0

    .line 116
    :sswitch_4
    const-string v4, "sync_photo"

    invoke-virtual {v2, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto :goto_0

    .line 120
    :sswitch_5
    const-string v4, "sync_photo"

    invoke-virtual {v2, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 121
    const-string v4, "sync_photo.target_id = "

    invoke-virtual {v2, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 123
    const/4 v4, 0x2

    invoke-interface {v14, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/CharSequence;

    invoke-virtual {v2, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 127
    :sswitch_6
    const-string v4, "user_info"

    invoke-virtual {v2, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto :goto_0

    .line 131
    :sswitch_7
    const-string v4, "friends_profile_info"

    invoke-virtual {v2, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto :goto_0

    .line 135
    :sswitch_8
    const-string v4, "status_stream"

    invoke-virtual {v2, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 137
    const-string v4, "update"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 138
    .local v15, "updateParameter":Ljava/lang/String;
    const-string v4, "true"

    invoke-virtual {v4, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 139
    const-string v4, "from_id"

    move-object/from16 v0, p3

    move-object/from16 v1, p4

    invoke-static {v0, v1, v4}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->parseQueryParam(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 141
    .local v16, "userId":Ljava/lang/String;
    if-eqz v16, :cond_0

    .line 142
    new-instance v12, Landroid/content/Intent;

    const-string v4, "com.sec.android.app.sns.profile.ACTION_INSTAGRAM_REQUESTED"

    invoke-direct {v12, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 144
    .local v12, "intent":Landroid/content/Intent;
    const-string v4, "id"

    move-object/from16 v0, v16

    invoke-virtual {v12, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 145
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/sns3/agent/sp/instagram/db/SnsInstagramContentProvider;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v12}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0

    .line 160
    .end local v12    # "intent":Landroid/content/Intent;
    .end local v15    # "updateParameter":Ljava/lang/String;
    .end local v16    # "userId":Ljava/lang/String;
    :catch_0
    move-exception v11

    .line 161
    .local v11, "e":Landroid/database/sqlite/SQLiteException;
    invoke-virtual {v11}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    goto :goto_1

    .line 98
    :sswitch_data_0
    .sparse-switch
        0x64 -> :sswitch_0
        0xc8 -> :sswitch_1
        0x12c -> :sswitch_2
        0x190 -> :sswitch_3
        0x1f4 -> :sswitch_4
        0x258 -> :sswitch_5
        0x2bc -> :sswitch_6
        0x320 -> :sswitch_7
        0x384 -> :sswitch_8
    .end sparse-switch
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 7
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 350
    const-string v0, "002c45129f2b78a6a509a7b1514d103d6fe3006b"

    .line 352
    .local v0, "MASTER_KEY":Ljava/lang/String;
    const/4 v1, 0x0

    .line 353
    .local v1, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v3, 0x0

    .line 354
    .local v3, "table":Ljava/lang/String;
    sget-object v4, Lcom/sec/android/app/sns3/agent/sp/instagram/db/SnsInstagramContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v4, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v2

    .line 356
    .local v2, "match":I
    iget-object v4, p0, Lcom/sec/android/app/sns3/agent/sp/instagram/db/SnsInstagramContentProvider;->mOpenHelper:Lcom/sec/android/app/sns3/agent/sp/instagram/db/SnsInstagramDBHelper;

    invoke-virtual {v4}, Lcom/sec/android/app/sns3/agent/sp/instagram/db/SnsInstagramDBHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 358
    sparse-switch v2, :sswitch_data_0

    .line 397
    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Unknown URI : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 360
    :sswitch_0
    const-string v3, "user_basic_info"

    .line 400
    :goto_0
    invoke-static {v1, v3, p2, p3, p4}, Lcom/sec/android/app/sns3/agent/db/SnsDBWrapper;->onUpdate(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v4

    return v4

    .line 364
    :sswitch_1
    const-string v3, "user_profile_info"

    .line 366
    goto :goto_0

    .line 368
    :sswitch_2
    const-string v3, "user_feed_info"

    .line 370
    goto :goto_0

    .line 372
    :sswitch_3
    const-string v3, "sync_album"

    .line 374
    goto :goto_0

    .line 376
    :sswitch_4
    const-string v3, "sync_photo"

    .line 378
    goto :goto_0

    .line 380
    :sswitch_5
    const-string v3, "sync_photo"

    .line 382
    goto :goto_0

    .line 384
    :sswitch_6
    const-string v3, "user_info"

    .line 386
    goto :goto_0

    .line 388
    :sswitch_7
    const-string v3, "friends_profile_info"

    .line 390
    goto :goto_0

    .line 392
    :sswitch_8
    const-string v3, "status_stream"

    .line 394
    goto :goto_0

    .line 358
    nop

    :sswitch_data_0
    .sparse-switch
        0x64 -> :sswitch_0
        0xc8 -> :sswitch_1
        0x12c -> :sswitch_2
        0x190 -> :sswitch_3
        0x1f4 -> :sswitch_4
        0x258 -> :sswitch_5
        0x2bc -> :sswitch_6
        0x320 -> :sswitch_7
        0x384 -> :sswitch_8
    .end sparse-switch
.end method
