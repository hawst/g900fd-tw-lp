.class Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthSSOActivity$2;
.super Ljava/lang/Object;
.source "SnsAccountFsAuthSSOActivity.java"

# interfaces
.implements Lcom/sec/android/app/sns3/agent/command/ISnsCommandCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthSSOActivity;->getAccessToken(ILandroid/content/Intent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthSSOActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthSSOActivity;)V
    .locals 0

    .prologue
    .line 303
    iput-object p1, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthSSOActivity$2;->this$0:Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthSSOActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCmdRespond(IZLjava/lang/String;Ljava/util/List;)V
    .locals 5
    .param p1, "cmdID"    # I
    .param p2, "bSuccess"    # Z
    .param p3, "uri"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IZ",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 308
    .local p4, "responseList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;>;"
    const/4 v1, 0x0

    .line 309
    .local v1, "reason":Landroid/os/Bundle;
    const/4 v3, 0x0

    .line 310
    .local v3, "userName":Ljava/lang/String;
    const/4 v2, 0x0

    .line 311
    .local v2, "userId":Ljava/lang/String;
    const/4 v0, 0x0

    .line 313
    .local v0, "email":Ljava/lang/String;
    if-eqz p4, :cond_0

    .line 314
    invoke-interface {p4}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_0

    .line 315
    const/4 v4, 0x0

    invoke-interface {p4, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;

    invoke-virtual {v4}, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;->getReason()Landroid/os/Bundle;

    move-result-object v1

    .line 317
    if-eqz v1, :cond_0

    .line 318
    const-string v4, "userName"

    invoke-virtual {v1, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 319
    const-string v4, "userID"

    invoke-virtual {v1, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 320
    const-string v4, "email"

    invoke-virtual {v1, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 325
    :cond_0
    if-eqz v3, :cond_1

    .line 326
    iget-object v4, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthSSOActivity$2;->this$0:Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthSSOActivity;

    # invokes: Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthSSOActivity;->loginSuccess(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v4, v3, v2, v0}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthSSOActivity;->access$300(Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthSSOActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 331
    :goto_0
    return-void

    .line 328
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthSSOActivity$2;->this$0:Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthSSOActivity;

    # invokes: Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthSSOActivity;->registerRetryNotification()V
    invoke-static {v4}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthSSOActivity;->access$400(Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthSSOActivity;)V

    .line 329
    iget-object v4, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthSSOActivity$2;->this$0:Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthSSOActivity;

    invoke-virtual {v4}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsAuthSSOActivity;->finish()V

    goto :goto_0
.end method
