.class public Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserEvent;
.super Ljava/lang/Object;
.source "SnsFbParserEvent.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserEvent$FacebookEvent;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    return-void
.end method

.method public static parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;
    .locals 5
    .param p0, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 70
    new-instance v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;

    invoke-direct {v1}, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;-><init>()V

    .line 73
    .local v1, "event":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;
    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 75
    .local v2, "jsonObj":Lorg/json/JSONObject;
    const-string v3, "id"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;->mID:Ljava/lang/String;

    .line 77
    const-string v3, "owner"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserFrom;->parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFrom;

    move-result-object v3

    iput-object v3, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;->mOwner:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFrom;

    .line 79
    const-string v3, "name"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;->mEventName:Ljava/lang/String;

    .line 80
    const-string v3, "description"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;->mDescription:Ljava/lang/String;

    .line 81
    const-string v3, "start_time"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;->mStartTime:Ljava/lang/String;

    .line 82
    const-string v3, "end_time"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;->mEndTime:Ljava/lang/String;

    .line 83
    const-string v3, "location"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;->mLocation:Ljava/lang/String;

    .line 84
    const-string v3, "venue"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 86
    const-string v3, "venue"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v3

    const-string v4, "street"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;->mStreet:Ljava/lang/String;

    .line 87
    const-string v3, "venue"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v3

    const-string v4, "city"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;->mCity:Ljava/lang/String;

    .line 88
    const-string v3, "venue"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v3

    const-string v4, "state"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;->mState:Ljava/lang/String;

    .line 89
    const-string v3, "venue"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v3

    const-string v4, "zip"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;->mZip:Ljava/lang/String;

    .line 90
    const-string v3, "venue"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v3

    const-string v4, "country"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;->mCountry:Ljava/lang/String;

    .line 91
    const-string v3, "venue"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v3

    const-string v4, "street"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;->mLatitude:Ljava/lang/String;

    .line 92
    const-string v3, "venue"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v3

    const-string v4, "longitude"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;->mLongitude:Ljava/lang/String;

    .line 96
    :cond_0
    const-string v3, "privacy"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;->mPrivacy:Ljava/lang/String;

    .line 97
    const-string v3, "updated_time"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;->mUpdatedTime:Ljava/lang/String;

    .line 98
    const-string v3, "type"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;->mType:Ljava/lang/String;

    .line 99
    const-string v3, "rsvp_status"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;->mRsvpStatus:Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 106
    .end local v2    # "jsonObj":Lorg/json/JSONObject;
    :goto_0
    return-object v1

    .line 101
    :catch_0
    move-exception v0

    .line 103
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method
