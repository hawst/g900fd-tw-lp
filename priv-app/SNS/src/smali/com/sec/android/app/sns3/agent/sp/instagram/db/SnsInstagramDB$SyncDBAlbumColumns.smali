.class public interface abstract Lcom/sec/android/app/sns3/agent/sp/instagram/db/SnsInstagramDB$SyncDBAlbumColumns;
.super Ljava/lang/Object;
.source "SnsInstagramDB.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/agent/sp/instagram/db/SnsInstagramDB;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "SyncDBAlbumColumns"
.end annotation


# static fields
.field public static final ALBUM_COUNT:Ljava/lang/String; = "count"

.field public static final ALBUM_COVER_PHOTO:Ljava/lang/String; = "cover_photo"

.field public static final ALBUM_CREATED_TIME:Ljava/lang/String; = "created_time"

.field public static final ALBUM_DESCRIPTION:Ljava/lang/String; = "description"

.field public static final ALBUM_FROM_ID:Ljava/lang/String; = "from_id"

.field public static final ALBUM_FROM_NAME:Ljava/lang/String; = "from_name"

.field public static final ALBUM_ID:Ljava/lang/String; = "id"

.field public static final ALBUM_LINK:Ljava/lang/String; = "link"

.field public static final ALBUM_LOCATION:Ljava/lang/String; = "location"

.field public static final ALBUM_NAME:Ljava/lang/String; = "name"

.field public static final ALBUM_PRIVACY:Ljava/lang/String; = "privacy"

.field public static final ALBUM_TYPE:Ljava/lang/String; = "type"

.field public static final ALBUM_UPDATED_TIME:Ljava/lang/String; = "updated_time"
