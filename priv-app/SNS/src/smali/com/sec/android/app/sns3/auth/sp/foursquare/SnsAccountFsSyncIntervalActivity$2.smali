.class Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsSyncIntervalActivity$2;
.super Ljava/lang/Object;
.source "SnsAccountFsSyncIntervalActivity.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsSyncIntervalActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsSyncIntervalActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsSyncIntervalActivity;)V
    .locals 0

    .prologue
    .line 196
    iput-object p1, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsSyncIntervalActivity$2;->this$0:Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsSyncIntervalActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 8
    .param p1, "preference"    # Landroid/preference/Preference;
    .param p2, "newValue"    # Ljava/lang/Object;

    .prologue
    const/4 v5, 0x0

    .line 203
    iget-object v2, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsSyncIntervalActivity$2;->this$0:Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsSyncIntervalActivity;

    # getter for: Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsSyncIntervalActivity;->mProfilesIntervalPreference:Landroid/preference/ListPreference;
    invoke-static {v2}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsSyncIntervalActivity;->access$100(Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsSyncIntervalActivity;)Landroid/preference/ListPreference;

    move-result-object v3

    move-object v2, p2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v3, v2}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 204
    iget-object v3, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsSyncIntervalActivity$2;->this$0:Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsSyncIntervalActivity;

    move-object v2, p2

    check-cast v2, Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsSyncIntervalActivity;->writeProfilesInterval(Ljava/lang/Long;)V

    .line 206
    iget-object v2, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsSyncIntervalActivity$2;->this$0:Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsSyncIntervalActivity;

    # getter for: Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsSyncIntervalActivity;->mProfilesIntervalPreference:Landroid/preference/ListPreference;
    invoke-static {v2}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsSyncIntervalActivity;->access$100(Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsSyncIntervalActivity;)Landroid/preference/ListPreference;

    move-result-object v2

    invoke-virtual {v2}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 207
    iget-object v2, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsSyncIntervalActivity$2;->this$0:Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsSyncIntervalActivity;

    # getter for: Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsSyncIntervalActivity;->mProfilesIntervalPreference:Landroid/preference/ListPreference;
    invoke-static {v2}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsSyncIntervalActivity;->access$100(Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsSyncIntervalActivity;)Landroid/preference/ListPreference;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsSyncIntervalActivity$2;->this$0:Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsSyncIntervalActivity;

    # getter for: Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsSyncIntervalActivity;->mProfilesIntervalPreference:Landroid/preference/ListPreference;
    invoke-static {v3}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsSyncIntervalActivity;->access$100(Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsSyncIntervalActivity;)Landroid/preference/ListPreference;

    move-result-object v3

    invoke-virtual {v3}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 211
    :cond_0
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 212
    .local v1, "extra":Landroid/os/Bundle;
    iget-object v2, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsSyncIntervalActivity$2;->this$0:Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsSyncIntervalActivity;

    invoke-static {v2}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v2

    const-string v3, "com.foursquare.android.auth.login"

    invoke-virtual {v2, v3}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 216
    .local v0, "account":[Landroid/accounts/Account;
    array-length v2, v0

    if-lez v2, :cond_1

    .line 217
    const-string v2, "0"

    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 218
    aget-object v2, v0, v5

    const-string v3, "com.sec.android.app.sns3.profiles"

    invoke-static {v2, v3, v1}, Landroid/content/ContentResolver;->removePeriodicSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 225
    :goto_0
    const-string v2, "SnsAccountFsAuth"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mProfilesIntervalPreference spType : Foursquare sec :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 229
    :cond_1
    return v5

    .line 221
    :cond_2
    aget-object v3, v0, v5

    const-string v4, "com.sec.android.app.sns3.profiles"

    move-object v2, p2

    check-cast v2, Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    int-to-long v6, v2

    invoke-static {v3, v4, v1, v6, v7}, Landroid/content/ContentResolver;->addPeriodicSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;J)V

    goto :goto_0
.end method
