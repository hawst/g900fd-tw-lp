.class public Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGooglePlusParamKey;
.super Ljava/lang/Object;
.source "SnsGooglePlusParamKey.java"


# static fields
.field public static final KEYWORD:Ljava/lang/String; = "keyword"

.field public static final LANGUAGE:Ljava/lang/String; = "language"

.field public static final LATITUDE:Ljava/lang/String; = "latitude"

.field public static final LIMIT:Ljava/lang/String; = "limit"

.field public static final LONGITUDE:Ljava/lang/String; = "longitude"

.field public static final NAME:Ljava/lang/String; = "name"

.field public static final OPEN_NOW:Ljava/lang/String; = "opennow"

.field public static final QUERY:Ljava/lang/String; = "query"

.field public static final RADIUS:Ljava/lang/String; = "radius"

.field public static final REFERENCE:Ljava/lang/String; = "reference"

.field public static final TYPES:Ljava/lang/String; = "types"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
