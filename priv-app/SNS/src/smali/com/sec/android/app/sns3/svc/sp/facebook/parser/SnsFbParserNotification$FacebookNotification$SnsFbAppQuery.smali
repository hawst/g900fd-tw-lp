.class public interface abstract Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserNotification$FacebookNotification$SnsFbAppQuery;
.super Ljava/lang/Object;
.source "SnsFbParserNotification.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserNotification$FacebookNotification;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "SnsFbAppQuery"
.end annotation


# static fields
.field public static final API_KEY:Ljava/lang/String; = "api_key"

.field public static final APP_ID:Ljava/lang/String; = "app_id"

.field public static final CANVAS_NAME:Ljava/lang/String; = "canvas_name"

.field public static final CATEGORY:Ljava/lang/String; = "category"

.field public static final DESCRIPTION:Ljava/lang/String; = "description"

.field public static final DISPLAY_NAME:Ljava/lang/String; = "display_name"

.field public static final ICONURL:Ljava/lang/String; = "icon_url"
