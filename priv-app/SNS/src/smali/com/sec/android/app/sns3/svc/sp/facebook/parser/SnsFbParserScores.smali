.class public Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserScores;
.super Ljava/lang/Object;
.source "SnsFbParserScores.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserScores$FacebookScores;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    return-void
.end method

.method public static parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseScores;
    .locals 11
    .param p0, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 48
    const/4 v8, 0x0

    .line 49
    .local v8, "scores":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseScores;
    const/4 v0, 0x0

    .line 50
    .local v0, "curScores":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseScores;
    const/4 v5, 0x0

    .line 53
    .local v5, "jsonObject":Lorg/json/JSONObject;
    :try_start_0
    new-instance v6, Lorg/json/JSONObject;

    invoke-direct {v6, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 54
    .end local v5    # "jsonObject":Lorg/json/JSONObject;
    .local v6, "jsonObject":Lorg/json/JSONObject;
    :try_start_1
    new-instance v3, Lorg/json/JSONArray;

    const-string v9, "data"

    invoke-virtual {v6, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v3, v9}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 56
    .local v3, "ja":Lorg/json/JSONArray;
    if-eqz v3, :cond_2

    .line 57
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v9

    if-ge v2, v9, :cond_2

    .line 58
    new-instance v7, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseScores;

    invoke-direct {v7}, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseScores;-><init>()V

    .line 60
    .local v7, "newScores":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseScores;
    invoke-virtual {v3, v2}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    .line 62
    .local v4, "jsonObj":Lorg/json/JSONObject;
    const-string v9, "id"

    invoke-virtual {v4, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v7, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseScores;->mScoreID:Ljava/lang/String;

    .line 64
    const-string v9, "user"

    invoke-virtual {v6, v9}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 65
    const-string v9, "user"

    invoke-virtual {v6, v9}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v9

    const-string v10, "id"

    invoke-virtual {v9, v10}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v7, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseScores;->mUserID:Ljava/lang/String;

    .line 67
    const-string v9, "user"

    invoke-virtual {v6, v9}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v9

    const-string v10, "name"

    invoke-virtual {v9, v10}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v7, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseScores;->mUserName:Ljava/lang/String;

    .line 71
    :cond_0
    const-string v9, "application"

    invoke-virtual {v4, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v7, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseScores;->mApplication:Ljava/lang/String;

    .line 72
    const-string v9, "score"

    invoke-virtual {v4, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v7, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseScores;->mScore:Ljava/lang/String;

    .line 73
    const-string v9, "type"

    invoke-virtual {v4, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v7, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseScores;->mType:Ljava/lang/String;

    .line 75
    if-nez v8, :cond_1

    .line 76
    move-object v8, v7

    .line 77
    move-object v0, v8

    .line 57
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 79
    :cond_1
    iput-object v7, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseScores;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseScores;

    .line 80
    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseScores;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseScores;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .end local v2    # "i":I
    .end local v4    # "jsonObj":Lorg/json/JSONObject;
    .end local v7    # "newScores":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseScores;
    :cond_2
    move-object v5, v6

    .line 90
    .end local v3    # "ja":Lorg/json/JSONArray;
    .end local v6    # "jsonObject":Lorg/json/JSONObject;
    .restart local v5    # "jsonObject":Lorg/json/JSONObject;
    :goto_2
    return-object v8

    .line 85
    :catch_0
    move-exception v1

    .line 87
    .local v1, "e":Lorg/json/JSONException;
    :goto_3
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_2

    .line 85
    .end local v1    # "e":Lorg/json/JSONException;
    .end local v5    # "jsonObject":Lorg/json/JSONObject;
    .restart local v6    # "jsonObject":Lorg/json/JSONObject;
    :catch_1
    move-exception v1

    move-object v5, v6

    .end local v6    # "jsonObject":Lorg/json/JSONObject;
    .restart local v5    # "jsonObject":Lorg/json/JSONObject;
    goto :goto_3
.end method
