.class public Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserAttachment;
.super Ljava/lang/Object;
.source "SnsFbParserAttachment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserAttachment$FacebookAttachment;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    return-void
.end method

.method public static parse(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAttachment;
    .locals 17
    .param p0, "contentObj"    # Ljava/lang/String;
    .param p1, "postID"    # Ljava/lang/String;

    .prologue
    .line 51
    const/4 v1, 0x0

    .line 52
    .local v1, "attachment":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAttachment;
    const/4 v3, 0x0

    .line 54
    .local v3, "curattachment":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAttachment;
    const/4 v7, 0x0

    .line 57
    .local v7, "jsonObject":Lorg/json/JSONObject;
    :try_start_0
    new-instance v8, Lorg/json/JSONObject;

    move-object/from16 v0, p0

    invoke-direct {v8, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 59
    .end local v7    # "jsonObject":Lorg/json/JSONObject;
    .local v8, "jsonObject":Lorg/json/JSONObject;
    :try_start_1
    const-string v14, "data"

    invoke-virtual {v8, v14}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v4

    .line 61
    .local v4, "data":Lorg/json/JSONArray;
    const/4 v14, 0x0

    invoke-virtual {v4, v14}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lorg/json/JSONObject;

    const-string v15, "attachment"

    invoke-virtual {v14, v15}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    .line 63
    .local v2, "attachments":Lorg/json/JSONObject;
    const-string v14, "media"

    invoke-virtual {v2, v14}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v9

    .line 65
    .local v9, "media":Lorg/json/JSONArray;
    if-eqz v9, :cond_2

    .line 66
    sget-object v14, Lcom/sec/android/app/sns3/app/life/SnsLifeResource;->TAG:Ljava/lang/String;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "media length "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v9}, Lorg/json/JSONArray;->length()I

    move-result v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 67
    invoke-virtual {v9}, Lorg/json/JSONArray;->length()I

    move-result v14

    const/4 v15, 0x5

    if-le v14, v15, :cond_0

    const/4 v11, 0x5

    .line 69
    .local v11, "mediaLength":I
    :goto_0
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_1
    if-ge v6, v11, :cond_2

    .line 70
    new-instance v12, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAttachment;

    invoke-direct {v12}, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAttachment;-><init>()V

    .line 71
    .local v12, "newAttachment":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAttachment;
    invoke-virtual {v9, v6}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v10

    .line 73
    .local v10, "mediaJsonObj":Lorg/json/JSONObject;
    const-string v14, "photo"

    invoke-virtual {v10, v14}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v13

    .line 75
    .local v13, "photo":Lorg/json/JSONObject;
    const-string v14, "pid"

    invoke-virtual {v13, v14}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    iput-object v14, v12, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAttachment;->mPhotoID:Ljava/lang/String;

    .line 77
    if-nez v1, :cond_1

    .line 78
    move-object v1, v12

    .line 79
    move-object v3, v1

    .line 69
    :goto_2
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 67
    .end local v6    # "i":I
    .end local v10    # "mediaJsonObj":Lorg/json/JSONObject;
    .end local v11    # "mediaLength":I
    .end local v12    # "newAttachment":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAttachment;
    .end local v13    # "photo":Lorg/json/JSONObject;
    :cond_0
    invoke-virtual {v9}, Lorg/json/JSONArray;->length()I

    move-result v11

    goto :goto_0

    .line 81
    .restart local v6    # "i":I
    .restart local v10    # "mediaJsonObj":Lorg/json/JSONObject;
    .restart local v11    # "mediaLength":I
    .restart local v12    # "newAttachment":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAttachment;
    .restart local v13    # "photo":Lorg/json/JSONObject;
    :cond_1
    iput-object v12, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAttachment;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAttachment;

    .line 82
    iget-object v3, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAttachment;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAttachment;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_2

    .end local v6    # "i":I
    .end local v10    # "mediaJsonObj":Lorg/json/JSONObject;
    .end local v11    # "mediaLength":I
    .end local v12    # "newAttachment":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAttachment;
    .end local v13    # "photo":Lorg/json/JSONObject;
    :cond_2
    move-object v7, v8

    .line 94
    .end local v2    # "attachments":Lorg/json/JSONObject;
    .end local v4    # "data":Lorg/json/JSONArray;
    .end local v8    # "jsonObject":Lorg/json/JSONObject;
    .end local v9    # "media":Lorg/json/JSONArray;
    .restart local v7    # "jsonObject":Lorg/json/JSONObject;
    :goto_3
    return-object v1

    .line 89
    :catch_0
    move-exception v5

    .line 91
    .local v5, "e":Lorg/json/JSONException;
    :goto_4
    invoke-virtual {v5}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_3

    .line 89
    .end local v5    # "e":Lorg/json/JSONException;
    .end local v7    # "jsonObject":Lorg/json/JSONObject;
    .restart local v8    # "jsonObject":Lorg/json/JSONObject;
    :catch_1
    move-exception v5

    move-object v7, v8

    .end local v8    # "jsonObject":Lorg/json/JSONObject;
    .restart local v7    # "jsonObject":Lorg/json/JSONObject;
    goto :goto_4
.end method
