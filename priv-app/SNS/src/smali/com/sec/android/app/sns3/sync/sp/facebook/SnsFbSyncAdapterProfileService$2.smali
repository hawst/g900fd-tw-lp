.class Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterProfileService$2;
.super Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqGetFriendsProfiles;
.source "SnsFbSyncAdapterProfileService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterProfileService;->performFriendsSync()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterProfileService;


# direct methods
.method constructor <init>(Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterProfileService;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;)V
    .locals 0
    .param p2, "x0"    # Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    .prologue
    .line 275
    iput-object p1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterProfileService$2;->this$0:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterProfileService;

    invoke-direct {p0, p2}, Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqGetFriendsProfiles;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;)V

    return-void
.end method


# virtual methods
.method public onReqRespond(IZIILandroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsProfiles;)Z
    .locals 6
    .param p1, "reqID"    # I
    .param p2, "bSuccess"    # Z
    .param p3, "httpStatus"    # I
    .param p4, "errorCode"    # I
    .param p5, "reason"    # Landroid/os/Bundle;
    .param p6, "myfriendInfo"    # Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsProfiles;

    .prologue
    const/4 v5, -0x1

    .line 281
    if-eqz p2, :cond_0

    .line 282
    new-instance v0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;

    invoke-direct {v0}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;-><init>()V

    .line 284
    .local v0, "dbAdapter":Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;
    :try_start_0
    # getter for: Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterProfileService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterProfileService;->access$100()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "inside response for friends.. friends is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 286
    invoke-virtual {v0, p6}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->insertFriends(Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsProfiles;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 301
    .end local v0    # "dbAdapter":Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterProfileService$2;->this$0:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterProfileService;

    # getter for: Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterProfileService;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;
    invoke-static {v2}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterProfileService;->access$800(Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterProfileService;)Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->resumeSync()V

    .line 302
    const/4 v2, 0x1

    return v2

    .line 288
    .restart local v0    # "dbAdapter":Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;
    :catch_0
    move-exception v1

    .line 289
    .local v1, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v1}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0

    .line 290
    .end local v1    # "e":Ljava/lang/NullPointerException;
    :catch_1
    move-exception v1

    .line 291
    .local v1, "e":Ljava/lang/Exception;
    iget-object v2, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterProfileService$2;->this$0:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterProfileService;

    # setter for: Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterProfileService;->mSyncState:I
    invoke-static {v2, v5}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterProfileService;->access$702(Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterProfileService;I)I

    .line 292
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 295
    .end local v0    # "dbAdapter":Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_0
    # getter for: Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterProfileService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterProfileService;->access$100()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SnsFbSyncAdapterProfileService errorCode : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", reason : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 298
    iget-object v2, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterProfileService$2;->this$0:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterProfileService;

    # setter for: Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterProfileService;->mSyncState:I
    invoke-static {v2, v5}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterProfileService;->access$702(Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterProfileService;I)I

    goto :goto_0
.end method
