.class public Lcom/sec/android/app/sns3/svc/sp/linkedin/parser/SnsLiParserProfile;
.super Ljava/lang/Object;
.source "SnsLiParserProfile.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/linkedin/parser/SnsLiParserProfile$LinkedInUser;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    return-void
.end method

.method public static parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseProfile;
    .locals 22
    .param p0, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 93
    new-instance v18, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseProfile;

    invoke-direct/range {v18 .. v18}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseProfile;-><init>()V

    .line 96
    .local v18, "user":Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseProfile;
    :try_start_0
    new-instance v12, Lorg/json/JSONObject;

    move-object/from16 v0, p0

    invoke-direct {v12, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 98
    .local v12, "jsonObject":Lorg/json/JSONObject;
    const-string v20, "id"

    move-object/from16 v0, v20

    invoke-virtual {v12, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseProfile;->setUserID(Ljava/lang/String;)V

    .line 99
    const-string v20, "firstName"

    move-object/from16 v0, v20

    invoke-virtual {v12, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseProfile;->setFirstName(Ljava/lang/String;)V

    .line 100
    const-string v20, "lastName"

    move-object/from16 v0, v20

    invoke-virtual {v12, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseProfile;->setLastName(Ljava/lang/String;)V

    .line 101
    const-string v20, "dateOfBirth"

    move-object/from16 v0, v20

    invoke-virtual {v12, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v6

    .line 102
    .local v6, "dob":Lorg/json/JSONObject;
    if-eqz v6, :cond_1

    .line 103
    const-string v20, "day"

    move-object/from16 v0, v20

    invoke-virtual {v6, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v4

    .line 104
    .local v4, "day":I
    const-string v20, "month"

    move-object/from16 v0, v20

    invoke-virtual {v6, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v15

    .line 105
    .local v15, "month":I
    const-string v20, "year"

    move-object/from16 v0, v20

    invoke-virtual {v6, v0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v19

    .line 106
    .local v19, "year":I
    new-instance v5, Ljava/text/SimpleDateFormat;

    const-string v20, "dd MMMM "

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v21

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-direct {v5, v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 107
    .local v5, "df":Ljava/text/SimpleDateFormat;
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    .line 108
    .local v2, "cal":Ljava/util/Calendar;
    const/16 v20, 0x5

    move/from16 v0, v20

    invoke-virtual {v2, v0, v4}, Ljava/util/Calendar;->set(II)V

    .line 109
    const/16 v20, 0x2

    move/from16 v0, v20

    invoke-virtual {v2, v0, v15}, Ljava/util/Calendar;->set(II)V

    .line 110
    invoke-virtual {v2}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v5, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v10

    .line 111
    .local v10, "formattedDate":Ljava/lang/String;
    if-eqz v19, :cond_0

    .line 112
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v20

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 115
    :cond_0
    move-object/from16 v0, v18

    invoke-virtual {v0, v10}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseProfile;->setDateOfBirth(Ljava/lang/String;)V

    .line 118
    .end local v2    # "cal":Ljava/util/Calendar;
    .end local v4    # "day":I
    .end local v5    # "df":Ljava/text/SimpleDateFormat;
    .end local v10    # "formattedDate":Ljava/lang/String;
    .end local v15    # "month":I
    .end local v19    # "year":I
    :cond_1
    const-string v20, "emailAddress"

    move-object/from16 v0, v20

    invoke-virtual {v12, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseProfile;->setEmail(Ljava/lang/String;)V

    .line 119
    const-string v20, "location"

    move-object/from16 v0, v20

    invoke-virtual {v12, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v13

    .line 120
    .local v13, "location":Lorg/json/JSONObject;
    if-eqz v13, :cond_2

    .line 121
    const-string v20, "name"

    move-object/from16 v0, v20

    invoke-virtual {v13, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseProfile;->setLocation(Ljava/lang/String;)V

    .line 122
    const-string v20, "country"

    move-object/from16 v0, v20

    invoke-virtual {v13, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v14

    .line 123
    .local v14, "locationCountry":Lorg/json/JSONObject;
    const-string v20, "code"

    move-object/from16 v0, v20

    invoke-virtual {v14, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseProfile;->setLocationCountryCode(Ljava/lang/String;)V

    .line 126
    .end local v14    # "locationCountry":Lorg/json/JSONObject;
    :cond_2
    const-string v20, "headline"

    move-object/from16 v0, v20

    invoke-virtual {v12, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseProfile;->setHeadline(Ljava/lang/String;)V

    .line 127
    const-string v20, "formattedName"

    move-object/from16 v0, v20

    invoke-virtual {v12, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseProfile;->setFormattedName(Ljava/lang/String;)V

    .line 128
    const-string v20, "publicProfileUrl"

    move-object/from16 v0, v20

    invoke-virtual {v12, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseProfile;->setPublicProfileUrl(Ljava/lang/String;)V

    .line 129
    invoke-static/range {p0 .. p0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseContactInfo;->parse(Ljava/lang/String;)Ljava/util/List;

    move-result-object v20

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseProfile;->setContactInfo(Ljava/util/List;)V

    .line 130
    const-string v20, "industry"

    move-object/from16 v0, v20

    invoke-virtual {v12, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseProfile;->setIndustry(Ljava/lang/String;)V

    .line 131
    const-string v20, "numConnections"

    move-object/from16 v0, v20

    invoke-virtual {v12, v0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v20

    move-object/from16 v0, v18

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseProfile;->setNumberOfConnections(I)V

    .line 132
    const-string v20, "pictureUrl"

    move-object/from16 v0, v20

    invoke-virtual {v12, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseProfile;->setPictureUrl(Ljava/lang/String;)V

    .line 134
    const-string v20, "positions"

    move-object/from16 v0, v20

    invoke-virtual {v12, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v16

    .line 135
    .local v16, "positions":Lorg/json/JSONObject;
    invoke-static/range {v16 .. v16}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponsePosition;->parse(Lorg/json/JSONObject;)Ljava/util/List;

    move-result-object v20

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseProfile;->setPositions(Ljava/util/List;)V

    .line 137
    const-string v20, "connections"

    move-object/from16 v0, v20

    invoke-virtual {v12, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v3

    .line 138
    .local v3, "connections":Lorg/json/JSONObject;
    invoke-static {v3}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;->parse(Lorg/json/JSONObject;)Ljava/util/List;

    move-result-object v20

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseProfile;->setConnections(Ljava/util/List;)V

    .line 140
    const-string v20, "skills"

    move-object/from16 v0, v20

    invoke-virtual {v12, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v17

    .line 141
    .local v17, "skills":Lorg/json/JSONObject;
    invoke-static/range {v17 .. v17}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseSkills;->parse(Lorg/json/JSONObject;)Ljava/util/List;

    move-result-object v20

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseProfile;->setSkills(Ljava/util/List;)V

    .line 143
    const-string v20, "following"

    move-object/from16 v0, v20

    invoke-virtual {v12, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v9

    .line 144
    .local v9, "following":Lorg/json/JSONObject;
    invoke-static {v9}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseFollowingInfo;->parse(Lorg/json/JSONObject;)Ljava/util/List;

    move-result-object v20

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseProfile;->setFollowing(Ljava/util/List;)V

    .line 146
    const-string v20, "groupMemberships"

    move-object/from16 v0, v20

    invoke-virtual {v12, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v11

    .line 147
    .local v11, "groups":Lorg/json/JSONObject;
    invoke-static {v11}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseGroups;->parse(Lorg/json/JSONObject;)Ljava/util/List;

    move-result-object v20

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseProfile;->setGroups(Ljava/util/List;)V

    .line 149
    const-string v20, "educations"

    move-object/from16 v0, v20

    invoke-virtual {v12, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v8

    .line 150
    .local v8, "education":Lorg/json/JSONObject;
    invoke-static {v8}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseEducation;->parse(Lorg/json/JSONObject;)Ljava/util/List;

    move-result-object v20

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseProfile;->setEducation(Ljava/util/List;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 156
    .end local v3    # "connections":Lorg/json/JSONObject;
    .end local v6    # "dob":Lorg/json/JSONObject;
    .end local v8    # "education":Lorg/json/JSONObject;
    .end local v9    # "following":Lorg/json/JSONObject;
    .end local v11    # "groups":Lorg/json/JSONObject;
    .end local v12    # "jsonObject":Lorg/json/JSONObject;
    .end local v13    # "location":Lorg/json/JSONObject;
    .end local v16    # "positions":Lorg/json/JSONObject;
    .end local v17    # "skills":Lorg/json/JSONObject;
    :goto_0
    return-object v18

    .line 152
    :catch_0
    move-exception v7

    .line 154
    .local v7, "e":Lorg/json/JSONException;
    invoke-virtual {v7}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method
