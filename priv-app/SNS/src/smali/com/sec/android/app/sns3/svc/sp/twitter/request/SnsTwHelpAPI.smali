.class public final enum Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwHelpAPI;
.super Ljava/lang/Enum;
.source "SnsTwHelpAPI.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwHelpAPI;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwHelpAPI;

.field public static final enum HELP_CONFIGURATION:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwHelpAPI;

.field public static final enum HELP_LANGUAGES:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwHelpAPI;

.field public static final enum HELP_TEST:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwHelpAPI;

.field public static final enum LEGAL_PRIVACY:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwHelpAPI;

.field public static final enum LEGAL_TOS:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwHelpAPI;


# instance fields
.field private mHttpMethod:Ljava/lang/String;

.field private mUrl:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 26
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwHelpAPI;

    const-string v1, "HELP_TEST"

    const-string v2, "/help/test.json"

    const-string v3, "GET"

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwHelpAPI;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwHelpAPI;->HELP_TEST:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwHelpAPI;

    .line 27
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwHelpAPI;

    const-string v1, "HELP_CONFIGURATION"

    const-string v2, "/help/configuration.json"

    const-string v3, "GET"

    invoke-direct {v0, v1, v5, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwHelpAPI;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwHelpAPI;->HELP_CONFIGURATION:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwHelpAPI;

    .line 28
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwHelpAPI;

    const-string v1, "HELP_LANGUAGES"

    const-string v2, "/help/languages.json"

    const-string v3, "GET"

    invoke-direct {v0, v1, v6, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwHelpAPI;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwHelpAPI;->HELP_LANGUAGES:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwHelpAPI;

    .line 29
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwHelpAPI;

    const-string v1, "LEGAL_PRIVACY"

    const-string v2, "/legal/privacy.json"

    const-string v3, "GET"

    invoke-direct {v0, v1, v7, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwHelpAPI;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwHelpAPI;->LEGAL_PRIVACY:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwHelpAPI;

    .line 30
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwHelpAPI;

    const-string v1, "LEGAL_TOS"

    const-string v2, "/legal/tos.json"

    const-string v3, "GET"

    invoke-direct {v0, v1, v8, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwHelpAPI;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwHelpAPI;->LEGAL_TOS:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwHelpAPI;

    .line 24
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwHelpAPI;

    sget-object v1, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwHelpAPI;->HELP_TEST:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwHelpAPI;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwHelpAPI;->HELP_CONFIGURATION:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwHelpAPI;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwHelpAPI;->HELP_LANGUAGES:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwHelpAPI;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwHelpAPI;->LEGAL_PRIVACY:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwHelpAPI;

    aput-object v1, v0, v7

    sget-object v1, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwHelpAPI;->LEGAL_TOS:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwHelpAPI;

    aput-object v1, v0, v8

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwHelpAPI;->$VALUES:[Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwHelpAPI;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p3, "url"    # Ljava/lang/String;
    .param p4, "httpMethod"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 35
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 36
    iput-object p3, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwHelpAPI;->mUrl:Ljava/lang/String;

    .line 37
    iput-object p4, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwHelpAPI;->mHttpMethod:Ljava/lang/String;

    .line 38
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwHelpAPI;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 24
    const-class v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwHelpAPI;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwHelpAPI;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwHelpAPI;
    .locals 1

    .prologue
    .line 24
    sget-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwHelpAPI;->$VALUES:[Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwHelpAPI;

    invoke-virtual {v0}, [Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwHelpAPI;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwHelpAPI;

    return-object v0
.end method


# virtual methods
.method public getHttpMethod()Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwHelpAPI;->mHttpMethod:Ljava/lang/String;

    return-object v0
.end method

.method public getUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwHelpAPI;->mUrl:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwHelpAPI;->mUrl:Ljava/lang/String;

    return-object v0
.end method
