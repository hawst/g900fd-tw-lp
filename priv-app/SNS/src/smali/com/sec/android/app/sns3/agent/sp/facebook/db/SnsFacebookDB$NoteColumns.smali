.class public interface abstract Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$NoteColumns;
.super Ljava/lang/Object;
.source "SnsFacebookDB.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "NoteColumns"
.end annotation


# static fields
.field public static final ACTOR_PHOTO_URL:Ljava/lang/String; = "_actor_photo_url"

.field public static final COMMENTS_COUNT:Ljava/lang/String; = "comments_count"

.field public static final CREATE_TIME:Ljava/lang/String; = "create_time"

.field public static final FROM_ID:Ljava/lang/String; = "from_id"

.field public static final FROM_NAME:Ljava/lang/String; = "from_name"

.field public static final ICON:Ljava/lang/String; = "icon"

.field public static final ID:Ljava/lang/String; = "note_id"

.field public static final LIKE_DONE:Ljava/lang/String; = "like_done"

.field public static final MESSAGE:Ljava/lang/String; = "message"

.field public static final SUBJECT:Ljava/lang/String; = "subject"

.field public static final UPDATED_TIME:Ljava/lang/String; = "updated_time"
