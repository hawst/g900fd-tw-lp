.class public Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;
.super Ljava/lang/Object;
.source "SnsTokenMgr.java"


# instance fields
.field private mTokenMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;->mTokenMap:Ljava/util/Map;

    .line 47
    return-void
.end method

.method private registerToken(Ljava/lang/String;Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;)V
    .locals 1
    .param p1, "spType"    # Ljava/lang/String;
    .param p2, "token"    # Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;->mTokenMap:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 62
    return-void
.end method


# virtual methods
.method public getActiveSp()Ljava/util/ArrayList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 69
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 70
    .local v0, "activeSplist":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v3, p0, Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;->mTokenMap:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 71
    .local v2, "sp":Ljava/lang/String;
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 73
    .end local v2    # "sp":Ljava/lang/String;
    :cond_0
    return-object v0
.end method

.method public getSignedInSp()Ljava/util/ArrayList;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 77
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 78
    .local v1, "signedinSplist":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v5, p0, Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;->mTokenMap:Ljava/util/Map;

    invoke-interface {v5}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 79
    .local v2, "sp":Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;->mTokenMap:Ljava/util/Map;

    invoke-interface {v5, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;

    .line 80
    .local v4, "token":Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;
    invoke-virtual {v4}, Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;->getTokenState()I

    move-result v3

    .line 81
    .local v3, "state":I
    if-eqz v3, :cond_1

    const/4 v5, 0x1

    if-ne v3, v5, :cond_0

    .line 82
    :cond_1
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 85
    .end local v2    # "sp":Ljava/lang/String;
    .end local v3    # "state":I
    .end local v4    # "token":Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;
    :cond_2
    return-object v1
.end method

.method public getToken(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;
    .locals 1
    .param p1, "spType"    # Ljava/lang/String;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;->mTokenMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;

    return-object v0
.end method

.method public getTokenState(Ljava/lang/String;)I
    .locals 4
    .param p1, "spType"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/RuntimeException;
        }
    .end annotation

    .prologue
    .line 90
    invoke-virtual {p0, p1}, Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;->getToken(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;

    move-result-object v0

    .line 92
    .local v0, "token":Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;
    if-eqz v0, :cond_0

    .line 93
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;->getTokenState()I

    move-result v1

    return v1

    .line 95
    :cond_0
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "spType is INACTIVE. spType = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public initialize()V
    .locals 2

    .prologue
    .line 50
    const-string v0, "facebook"

    new-instance v1, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbToken;

    invoke-direct {v1}, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbToken;-><init>()V

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;->registerToken(Ljava/lang/String;Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;)V

    .line 51
    const-string v0, "twitter"

    new-instance v1, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;

    invoke-direct {v1}, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;-><init>()V

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;->registerToken(Ljava/lang/String;Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;)V

    .line 52
    const-string v0, "sinaweibo"

    new-instance v1, Lcom/sec/android/app/sns3/svc/sp/sinaweibo/SnsSwToken;

    invoke-direct {v1}, Lcom/sec/android/app/sns3/svc/sp/sinaweibo/SnsSwToken;-><init>()V

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;->registerToken(Ljava/lang/String;Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;)V

    .line 53
    const-string v0, "qzone"

    new-instance v1, Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzToken;

    invoke-direct {v1}, Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzToken;-><init>()V

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;->registerToken(Ljava/lang/String;Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;)V

    .line 54
    const-string v0, "linkedin"

    new-instance v1, Lcom/sec/android/app/sns3/svc/sp/linkedin/SnsLiToken;

    invoke-direct {v1}, Lcom/sec/android/app/sns3/svc/sp/linkedin/SnsLiToken;-><init>()V

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;->registerToken(Ljava/lang/String;Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;)V

    .line 55
    const-string v0, "googleplus"

    new-instance v1, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpToken;

    invoke-direct {v1}, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpToken;-><init>()V

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;->registerToken(Ljava/lang/String;Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;)V

    .line 56
    const-string v0, "foursquare"

    new-instance v1, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsToken;

    invoke-direct {v1}, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsToken;-><init>()V

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;->registerToken(Ljava/lang/String;Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;)V

    .line 57
    const-string v0, "instagram"

    new-instance v1, Lcom/sec/android/app/sns3/svc/sp/instagram/SnsInToken;

    invoke-direct {v1}, Lcom/sec/android/app/sns3/svc/sp/instagram/SnsInToken;-><init>()V

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;->registerToken(Ljava/lang/String;Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;)V

    .line 58
    return-void
.end method
