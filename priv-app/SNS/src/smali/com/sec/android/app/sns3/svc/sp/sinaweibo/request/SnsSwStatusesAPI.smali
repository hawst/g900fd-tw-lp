.class public final enum Lcom/sec/android/app/sns3/svc/sp/sinaweibo/request/SnsSwStatusesAPI;
.super Ljava/lang/Enum;
.source "SnsSwStatusesAPI.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/sns3/svc/sp/sinaweibo/request/SnsSwStatusesAPI;",
        ">;",
        "Landroid/os/Parcelable;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/sns3/svc/sp/sinaweibo/request/SnsSwStatusesAPI;

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/android/app/sns3/svc/sp/sinaweibo/request/SnsSwStatusesAPI;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum STATUSES_FRIENDS_TIMELINE:Lcom/sec/android/app/sns3/svc/sp/sinaweibo/request/SnsSwStatusesAPI;

.field public static final enum STATUSES_HOME_TIMELINE:Lcom/sec/android/app/sns3/svc/sp/sinaweibo/request/SnsSwStatusesAPI;

.field public static final enum STATUSES_PUBLIC_TIMELINE:Lcom/sec/android/app/sns3/svc/sp/sinaweibo/request/SnsSwStatusesAPI;

.field public static final enum STATUSES_USER_TIMELINE:Lcom/sec/android/app/sns3/svc/sp/sinaweibo/request/SnsSwStatusesAPI;


# instance fields
.field private mHttpMethod:Ljava/lang/String;

.field private mUrl:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 28
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/sinaweibo/request/SnsSwStatusesAPI;

    const-string v1, "STATUSES_PUBLIC_TIMELINE"

    const-string v2, "statuses/public_timeline.json"

    const-string v3, "GET"

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/sinaweibo/request/SnsSwStatusesAPI;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/sinaweibo/request/SnsSwStatusesAPI;->STATUSES_PUBLIC_TIMELINE:Lcom/sec/android/app/sns3/svc/sp/sinaweibo/request/SnsSwStatusesAPI;

    .line 29
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/sinaweibo/request/SnsSwStatusesAPI;

    const-string v1, "STATUSES_FRIENDS_TIMELINE"

    const-string v2, "statuses/friends_timeline.json"

    const-string v3, "GET"

    invoke-direct {v0, v1, v5, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/sinaweibo/request/SnsSwStatusesAPI;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/sinaweibo/request/SnsSwStatusesAPI;->STATUSES_FRIENDS_TIMELINE:Lcom/sec/android/app/sns3/svc/sp/sinaweibo/request/SnsSwStatusesAPI;

    .line 30
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/sinaweibo/request/SnsSwStatusesAPI;

    const-string v1, "STATUSES_HOME_TIMELINE"

    const-string v2, "statuses/home_timeline.json"

    const-string v3, "GET"

    invoke-direct {v0, v1, v6, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/sinaweibo/request/SnsSwStatusesAPI;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/sinaweibo/request/SnsSwStatusesAPI;->STATUSES_HOME_TIMELINE:Lcom/sec/android/app/sns3/svc/sp/sinaweibo/request/SnsSwStatusesAPI;

    .line 31
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/sinaweibo/request/SnsSwStatusesAPI;

    const-string v1, "STATUSES_USER_TIMELINE"

    const-string v2, "statuses/user_timeline.json"

    const-string v3, "GET"

    invoke-direct {v0, v1, v7, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/sinaweibo/request/SnsSwStatusesAPI;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/sinaweibo/request/SnsSwStatusesAPI;->STATUSES_USER_TIMELINE:Lcom/sec/android/app/sns3/svc/sp/sinaweibo/request/SnsSwStatusesAPI;

    .line 25
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/sec/android/app/sns3/svc/sp/sinaweibo/request/SnsSwStatusesAPI;

    sget-object v1, Lcom/sec/android/app/sns3/svc/sp/sinaweibo/request/SnsSwStatusesAPI;->STATUSES_PUBLIC_TIMELINE:Lcom/sec/android/app/sns3/svc/sp/sinaweibo/request/SnsSwStatusesAPI;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/sns3/svc/sp/sinaweibo/request/SnsSwStatusesAPI;->STATUSES_FRIENDS_TIMELINE:Lcom/sec/android/app/sns3/svc/sp/sinaweibo/request/SnsSwStatusesAPI;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/sns3/svc/sp/sinaweibo/request/SnsSwStatusesAPI;->STATUSES_HOME_TIMELINE:Lcom/sec/android/app/sns3/svc/sp/sinaweibo/request/SnsSwStatusesAPI;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/sns3/svc/sp/sinaweibo/request/SnsSwStatusesAPI;->STATUSES_USER_TIMELINE:Lcom/sec/android/app/sns3/svc/sp/sinaweibo/request/SnsSwStatusesAPI;

    aput-object v1, v0, v7

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/sinaweibo/request/SnsSwStatusesAPI;->$VALUES:[Lcom/sec/android/app/sns3/svc/sp/sinaweibo/request/SnsSwStatusesAPI;

    .line 54
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/sinaweibo/request/SnsSwStatusesAPI$1;

    invoke-direct {v0}, Lcom/sec/android/app/sns3/svc/sp/sinaweibo/request/SnsSwStatusesAPI$1;-><init>()V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/sinaweibo/request/SnsSwStatusesAPI;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p3, "url"    # Ljava/lang/String;
    .param p4, "httpMethod"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 36
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 37
    iput-object p3, p0, Lcom/sec/android/app/sns3/svc/sp/sinaweibo/request/SnsSwStatusesAPI;->mUrl:Ljava/lang/String;

    .line 38
    iput-object p4, p0, Lcom/sec/android/app/sns3/svc/sp/sinaweibo/request/SnsSwStatusesAPI;->mHttpMethod:Ljava/lang/String;

    .line 39
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/sinaweibo/request/SnsSwStatusesAPI;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 25
    const-class v0, Lcom/sec/android/app/sns3/svc/sp/sinaweibo/request/SnsSwStatusesAPI;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/sns3/svc/sp/sinaweibo/request/SnsSwStatusesAPI;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/sns3/svc/sp/sinaweibo/request/SnsSwStatusesAPI;
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lcom/sec/android/app/sns3/svc/sp/sinaweibo/request/SnsSwStatusesAPI;->$VALUES:[Lcom/sec/android/app/sns3/svc/sp/sinaweibo/request/SnsSwStatusesAPI;

    invoke-virtual {v0}, [Lcom/sec/android/app/sns3/svc/sp/sinaweibo/request/SnsSwStatusesAPI;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/sns3/svc/sp/sinaweibo/request/SnsSwStatusesAPI;

    return-object v0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 70
    const/4 v0, 0x0

    return v0
.end method

.method public getHttpMethod()Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/sinaweibo/request/SnsSwStatusesAPI;->mHttpMethod:Ljava/lang/String;

    return-object v0
.end method

.method public getUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/sinaweibo/request/SnsSwStatusesAPI;->mUrl:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/sinaweibo/request/SnsSwStatusesAPI;->mUrl:Ljava/lang/String;

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 75
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/svc/sp/sinaweibo/request/SnsSwStatusesAPI;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 76
    return-void
.end method
