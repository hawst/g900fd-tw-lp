.class public Lcom/sec/android/app/sns3/svc/sp/linkedin/parser/SnsLiParserUser;
.super Ljava/lang/Object;
.source "SnsLiParserUser.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/linkedin/parser/SnsLiParserUser$LinkedInUser;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    return-void
.end method

.method public static parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUser;
    .locals 4
    .param p0, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 37
    new-instance v2, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUser;

    invoke-direct {v2}, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUser;-><init>()V

    .line 40
    .local v2, "user":Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUser;
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 42
    .local v1, "jsonObject":Lorg/json/JSONObject;
    const-string v3, "id"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUser;->mUserID:Ljava/lang/String;

    .line 43
    const-string v3, "formattedName"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseUser;->mUserName:Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 49
    .end local v1    # "jsonObject":Lorg/json/JSONObject;
    :goto_0
    return-object v2

    .line 44
    :catch_0
    move-exception v0

    .line 46
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method
