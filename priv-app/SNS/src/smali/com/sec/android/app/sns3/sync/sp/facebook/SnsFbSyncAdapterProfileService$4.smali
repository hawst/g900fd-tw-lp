.class Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterProfileService$4;
.super Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqGetFriendsEducationDetails;
.source "SnsFbSyncAdapterProfileService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterProfileService;->performEduSync()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterProfileService;


# direct methods
.method constructor <init>(Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterProfileService;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;)V
    .locals 0
    .param p2, "x0"    # Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    .prologue
    .line 416
    iput-object p1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterProfileService$4;->this$0:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterProfileService;

    invoke-direct {p0, p2}, Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqGetFriendsEducationDetails;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;)V

    return-void
.end method


# virtual methods
.method public onReqRespond(IZIILandroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsEducationDetails;)Z
    .locals 5
    .param p1, "reqID"    # I
    .param p2, "bSuccess"    # Z
    .param p3, "httpStatus"    # I
    .param p4, "errorCode"    # I
    .param p5, "reason"    # Landroid/os/Bundle;
    .param p6, "friendlists"    # Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsEducationDetails;

    .prologue
    .line 423
    if-eqz p2, :cond_0

    .line 424
    new-instance v0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;

    invoke-direct {v0}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;-><init>()V

    .line 427
    .local v0, "dbAdapter":Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;
    :try_start_0
    # getter for: Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterProfileService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterProfileService;->access$100()Ljava/lang/String;

    move-result-object v2

    const-string v3, "***************** SnsFbSyncAdapterProfileService : getting response !!!"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 429
    invoke-virtual {v0, p6}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->insertFriendsEduDetails(Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFriendsEducationDetails;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 441
    .end local v0    # "dbAdapter":Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterProfileService$4;->this$0:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterProfileService;

    # getter for: Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterProfileService;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;
    invoke-static {v2}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterProfileService;->access$800(Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterProfileService;)Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->resumeSync()V

    .line 443
    const/4 v2, 0x1

    return v2

    .line 430
    .restart local v0    # "dbAdapter":Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;
    :catch_0
    move-exception v1

    .line 431
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 435
    .end local v0    # "dbAdapter":Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_0
    # getter for: Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterProfileService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterProfileService;->access$100()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SnsFbSyncAdapterProfileService errorCode : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", reason : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 438
    iget-object v2, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterProfileService$4;->this$0:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterProfileService;

    const/4 v3, -0x1

    # setter for: Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterProfileService;->mSyncState:I
    invoke-static {v2, v3}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterProfileService;->access$702(Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterProfileService;I)I

    goto :goto_0
.end method
