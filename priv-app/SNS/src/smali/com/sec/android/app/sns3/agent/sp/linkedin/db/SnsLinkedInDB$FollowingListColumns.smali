.class public interface abstract Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$FollowingListColumns;
.super Ljava/lang/Object;
.source "SnsLinkedInDB.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "FollowingListColumns"
.end annotation


# static fields
.field public static final FOLLOWING_ID:Ljava/lang/String; = "following_id"

.field public static final FOLLOWING_NAME:Ljava/lang/String; = "following_name"

.field public static final FOLLOWING_TYPE:Ljava/lang/String; = "following_type"
