.class public Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;
.super Landroid/app/Service;
.source "SnsFbSyncAdapterStreamsService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService$SyncAdapterImpl;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private bStreamsSyncSuccess:Z

.field private fbResponsePost:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;

.field private mAccount:Landroid/accounts/Account;

.field private mAlbumId:Ljava/lang/String;

.field mAlbumIdList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mAuthority:Ljava/lang/String;

.field private mBundle:Landroid/os/Bundle;

.field private mContext:Landroid/content/Context;

.field private mFbToken:Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbToken;

.field mPhotoIdList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mReq:Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;

.field private mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

.field private mSyncAdapter:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService$SyncAdapterImpl;

.field private mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;

.field private mSyncResult:Landroid/content/SyncResult;

.field private mSyncState:I

.field private mSyncType:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 74
    const-class v0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 72
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 76
    iput-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mSyncAdapter:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService$SyncAdapterImpl;

    .line 78
    iput-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;

    .line 80
    iput v0, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mSyncState:I

    .line 82
    iput-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mSyncResult:Landroid/content/SyncResult;

    .line 84
    iput-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mAccount:Landroid/accounts/Account;

    .line 86
    iput-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mAuthority:Ljava/lang/String;

    .line 90
    iput-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mReq:Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;

    .line 92
    iput-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->fbResponsePost:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;

    .line 94
    iput-boolean v0, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->bStreamsSyncSuccess:Z

    .line 96
    iput-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mSyncType:Ljava/lang/String;

    .line 98
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mBundle:Landroid/os/Bundle;

    .line 100
    iput-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mAlbumId:Ljava/lang/String;

    .line 104
    iput-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mFbToken:Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbToken;

    .line 106
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mPhotoIdList:Ljava/util/ArrayList;

    .line 108
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mAlbumIdList:Ljava/util/ArrayList;

    .line 110
    return-void
.end method

.method static synthetic access$002(Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;Landroid/content/Context;)Landroid/content/Context;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;
    .param p1, "x1"    # Landroid/content/Context;

    .prologue
    .line 72
    iput-object p1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mContext:Landroid/content/Context;

    return-object p1
.end method

.method static synthetic access$100()Ljava/lang/String;
    .locals 1

    .prologue
    .line 72
    sget-object v0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;)Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;)Landroid/os/Bundle;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mBundle:Landroid/os/Bundle;

    return-object v0
.end method

.method static synthetic access$1102(Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;
    .param p1, "x1"    # Landroid/os/Bundle;

    .prologue
    .line 72
    iput-object p1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mBundle:Landroid/os/Bundle;

    return-object p1
.end method

.method static synthetic access$1200(Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mAlbumId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$202(Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbToken;)Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbToken;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;
    .param p1, "x1"    # Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbToken;

    .prologue
    .line 72
    iput-object p1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mFbToken:Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbToken;

    return-object p1
.end method

.method static synthetic access$302(Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;Landroid/accounts/Account;)Landroid/accounts/Account;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;
    .param p1, "x1"    # Landroid/accounts/Account;

    .prologue
    .line 72
    iput-object p1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mAccount:Landroid/accounts/Account;

    return-object p1
.end method

.method static synthetic access$402(Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 72
    iput-object p1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mAuthority:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$502(Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;Landroid/content/SyncResult;)Landroid/content/SyncResult;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;
    .param p1, "x1"    # Landroid/content/SyncResult;

    .prologue
    .line 72
    iput-object p1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mSyncResult:Landroid/content/SyncResult;

    return-object p1
.end method

.method static synthetic access$600(Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/accounts/OperationCanceledException;
        }
    .end annotation

    .prologue
    .line 72
    invoke-direct {p0}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->performSync()V

    return-void
.end method

.method static synthetic access$700(Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;

    .prologue
    .line 72
    invoke-direct {p0}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->onSyncCanceled()V

    return-void
.end method

.method static synthetic access$800(Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->fbResponsePost:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;

    return-object v0
.end method

.method static synthetic access$802(Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;
    .param p1, "x1"    # Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;

    .prologue
    .line 72
    iput-object p1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->fbResponsePost:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;

    return-object p1
.end method

.method static synthetic access$902(Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;
    .param p1, "x1"    # I

    .prologue
    .line 72
    iput p1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mSyncState:I

    return p1
.end method

.method private handleSessionExpired()I
    .locals 11

    .prologue
    const/4 v9, 0x1

    const/4 v10, 0x0

    .line 732
    iget-object v7, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mAccount:Landroid/accounts/Account;

    const-string v8, "com.sec.android.app.sns3.streams"

    invoke-static {v7, v8}, Landroid/content/ContentResolver;->cancelSync(Landroid/accounts/Account;Ljava/lang/String;)V

    .line 734
    iget-object v7, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mContext:Landroid/content/Context;

    const-string v8, "notification"

    invoke-virtual {v7, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/app/NotificationManager;

    move-object v4, v7

    check-cast v4, Landroid/app/NotificationManager;

    .line 737
    .local v4, "notiMgr":Landroid/app/NotificationManager;
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 739
    .local v3, "notiIntent":Landroid/content/Intent;
    const-string v7, "com.sec.android.app.sns3.RETRY_SSO_FACEBOOK"

    invoke-virtual {v3, v7}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 743
    const/high16 v7, 0x800000

    invoke-virtual {v3, v7}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 744
    const-string v7, "RetryLogin"

    invoke-virtual {v3, v7, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 745
    iget-object v7, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mContext:Landroid/content/Context;

    invoke-static {v7, v10, v3, v10}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 747
    .local v1, "launchIntent":Landroid/app/PendingIntent;
    const v6, 0x7f08003f

    .line 748
    .local v6, "titleID":I
    const v5, 0x7f080020

    .line 749
    .local v5, "spID":I
    const v0, 0x7f02001a

    .line 751
    .local v0, "iconID":I
    new-instance v2, Landroid/app/Notification$Builder;

    iget-object v7, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mContext:Landroid/content/Context;

    invoke-direct {v2, v7}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    .line 752
    .local v2, "notiBuilder":Landroid/app/Notification$Builder;
    iget-object v7, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mContext:Landroid/content/Context;

    new-array v8, v9, [Ljava/lang/Object;

    iget-object v9, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mContext:Landroid/content/Context;

    invoke-virtual {v9, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v10

    invoke-virtual {v7, v6, v8}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v7

    invoke-virtual {v7, v0}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mContext:Landroid/content/Context;

    const v9, 0x7f08003e

    invoke-virtual {v8, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v7

    invoke-virtual {v7, v1}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v7

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Landroid/app/Notification$Builder;->setWhen(J)Landroid/app/Notification$Builder;

    .line 756
    const/16 v7, 0x514

    invoke-virtual {v2}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v8

    invoke-virtual {v4, v7, v8}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 758
    const/4 v7, -0x1

    return v7
.end method

.method private invokeBroadcast()V
    .locals 5

    .prologue
    .line 776
    const/4 v2, 0x0

    .line 777
    .local v2, "intent":Landroid/content/Intent;
    const-string v0, "com.sec.android.app.sns3.action.SYNC_FB_STREAMS"

    .line 778
    .local v0, "action":Ljava/lang/String;
    iget v3, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mSyncState:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_0

    const/4 v1, 0x1

    .line 780
    .local v1, "bResult":Z
    :goto_0
    new-instance v2, Landroid/content/Intent;

    .end local v2    # "intent":Landroid/content/Intent;
    invoke-direct {v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 781
    .restart local v2    # "intent":Landroid/content/Intent;
    const-string v3, "SNS3_CONTENT_URI_POSTS"

    sget-object v4, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$Post;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 782
    const-string v3, "SNS3_CONTENT_URI_COMMENTS"

    sget-object v4, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$Comments;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 783
    const-string v3, "SNS3_CONTENT_URI_FEEDS"

    sget-object v4, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$FeedList;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 784
    const-string v3, "SNS3_CONTENT_URI_GROUPS"

    sget-object v4, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$Group;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 785
    const-string v3, "SNS3_CONTENT_URI_PHOTOS_OF_USER"

    sget-object v4, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$PhotosOfUser;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 786
    const-string v3, "SNS_RESULT"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 788
    const-string v3, "com.sec.android.app.sns3.permission.RECEIVE_SNS_BROADCAST"

    invoke-virtual {p0, v2, v3}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    .line 789
    return-void

    .line 778
    .end local v1    # "bResult":Z
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private onSyncCanceled()V
    .locals 2

    .prologue
    .line 763
    sget-object v0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->TAG:Ljava/lang/String;

    const-string v1, "***************** SnsFbSyncAdapterStreamsService : onSyncCanceled !!! *****************"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 766
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mSyncState:I

    .line 767
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mBundle:Landroid/os/Bundle;

    .line 768
    iget-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mReq:Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;

    if-eqz v0, :cond_0

    .line 769
    iget-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mReq:Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->abort()Z

    .line 772
    :cond_0
    return-void
.end method

.method private performCommentSync()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, -0x1

    .line 262
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->bStreamsSyncSuccess:Z

    .line 264
    sget-object v1, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->TAG:Ljava/lang/String;

    const-string v2, "***************** SnsFbSyncAdapterStreamsService : performCommentSync - START !!! *****************"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 268
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->fbResponsePost:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;

    if-eqz v1, :cond_4

    .line 271
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mFbToken:Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbToken;

    invoke-virtual {v1}, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbToken;->getTokenState()I

    move-result v1

    if-eq v1, v7, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mFbToken:Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbToken;

    invoke-virtual {v1}, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbToken;->getTokenState()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    .line 273
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->handleSessionExpired()I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mSyncState:I

    .line 274
    new-instance v1, Ljava/lang/Exception;

    const-string v2, "Session expired or invalid!!!"

    invoke-direct {v1, v2}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 319
    :catch_0
    move-exception v0

    .line 321
    .local v0, "e":Ljava/lang/Exception;
    :try_start_1
    sget-object v1, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SnsFbSyncAdapterStreamsService : EXCEPTION !!! "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 322
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 323
    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mSyncResult:Landroid/content/SyncResult;

    iget-object v1, v1, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v1, Landroid/content/SyncStats;->numConflictDetectedExceptions:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, v1, Landroid/content/SyncStats;->numConflictDetectedExceptions:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 326
    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->fbResponsePost:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;

    iget-object v1, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;

    iput-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->fbResponsePost:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;

    goto :goto_0

    .line 277
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_1
    :try_start_2
    new-instance v1, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService$2;

    iget-object v2, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    iget-object v3, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->fbResponsePost:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;

    iget-object v3, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mPostID:Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mBundle:Landroid/os/Bundle;

    invoke-direct {v1, p0, v2, v3, v4}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService$2;-><init>(Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;Landroid/os/Bundle;)V

    iput-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mReq:Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;

    .line 306
    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mReq:Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;

    invoke-virtual {v1}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->request()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 307
    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;

    invoke-virtual {v1}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->suspendSync()V

    .line 311
    :goto_1
    iget v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mSyncState:I

    if-ne v1, v6, :cond_3

    .line 312
    new-instance v1, Ljava/lang/Exception;

    const-string v2, "SnsFbSyncAdapterStreamsService is failed!!!"

    invoke-direct {v1, v2}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 326
    :catchall_0
    move-exception v1

    iget-object v2, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->fbResponsePost:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;

    iget-object v2, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;

    iput-object v2, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->fbResponsePost:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;

    throw v1

    .line 309
    :cond_2
    const/4 v1, -0x1

    :try_start_3
    iput v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mSyncState:I

    goto :goto_1

    .line 314
    :cond_3
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->bStreamsSyncSuccess:Z

    .line 316
    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mBundle:Landroid/os/Bundle;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    if-nez v1, :cond_1

    .line 326
    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->fbResponsePost:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;

    iget-object v1, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;

    iput-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->fbResponsePost:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePost;

    goto/16 :goto_0

    .line 329
    :cond_4
    iget-boolean v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->bStreamsSyncSuccess:Z

    if-eqz v1, :cond_5

    .line 330
    invoke-direct {p0}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->performFeedsSync()V

    .line 332
    :cond_5
    return-void
.end method

.method private performFeedsSync()V
    .locals 5

    .prologue
    const/4 v4, -0x1

    const/4 v3, 0x1

    .line 336
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->bStreamsSyncSuccess:Z

    .line 338
    sget-object v1, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->TAG:Ljava/lang/String;

    const-string v2, "***************** SnsFbSyncAdapterStreamsService : performSync - FEEDS !!! *****************"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 341
    iput v3, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mSyncState:I

    .line 344
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mFbToken:Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbToken;

    invoke-virtual {v1}, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbToken;->getTokenState()I

    move-result v1

    if-eq v1, v3, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mFbToken:Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbToken;

    invoke-virtual {v1}, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbToken;->getTokenState()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    .line 346
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->handleSessionExpired()I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mSyncState:I

    .line 347
    new-instance v1, Ljava/lang/Exception;

    const-string v2, "Session expired or invalid!!!"

    invoke-direct {v1, v2}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 382
    :catch_0
    move-exception v0

    .line 383
    .local v0, "e":Ljava/lang/Exception;
    :try_start_1
    sget-object v1, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SnsFbSyncAdapterStreamsService : Feeds sync EXCEPTION !!! "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 385
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 387
    iget-boolean v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->bStreamsSyncSuccess:Z

    if-eqz v1, :cond_1

    .line 388
    invoke-direct {p0}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->performGroupsSync()V

    .line 391
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_1
    :goto_0
    return-void

    .line 349
    :cond_2
    :try_start_2
    new-instance v1, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService$3;

    iget-object v2, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    const-string v3, "me"

    invoke-direct {v1, p0, v2, v3}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService$3;-><init>(Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mReq:Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;

    .line 371
    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mReq:Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;

    invoke-virtual {v1}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->request()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 372
    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;

    invoke-virtual {v1}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->suspendSync()V

    .line 376
    :goto_1
    iget v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mSyncState:I

    if-ne v1, v4, :cond_5

    .line 377
    new-instance v1, Ljava/lang/Exception;

    const-string v2, "SnsFbSyncAdapterStreamsService is failed!!!"

    invoke-direct {v1, v2}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 387
    :catchall_0
    move-exception v1

    iget-boolean v2, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->bStreamsSyncSuccess:Z

    if-eqz v2, :cond_3

    .line 388
    invoke-direct {p0}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->performGroupsSync()V

    :cond_3
    throw v1

    .line 374
    :cond_4
    const/4 v1, -0x1

    :try_start_3
    iput v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mSyncState:I

    goto :goto_1

    .line 379
    :cond_5
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->bStreamsSyncSuccess:Z
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 387
    iget-boolean v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->bStreamsSyncSuccess:Z

    if-eqz v1, :cond_1

    .line 388
    invoke-direct {p0}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->performGroupsSync()V

    goto :goto_0
.end method

.method private performGroupsSync()V
    .locals 5

    .prologue
    const/4 v4, -0x1

    const/4 v3, 0x1

    .line 395
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->bStreamsSyncSuccess:Z

    .line 397
    iput v3, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mSyncState:I

    .line 399
    sget-object v1, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->TAG:Ljava/lang/String;

    const-string v2, "***************** SnsFbSyncAdapterStreamsService : performSync - Groups !!! *****************"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 403
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mFbToken:Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbToken;

    invoke-virtual {v1}, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbToken;->getTokenState()I

    move-result v1

    if-eq v1, v3, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mFbToken:Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbToken;

    invoke-virtual {v1}, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbToken;->getTokenState()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    .line 405
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->handleSessionExpired()I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mSyncState:I

    .line 406
    new-instance v1, Ljava/lang/Exception;

    const-string v2, "Session expired or invalid!!!"

    invoke-direct {v1, v2}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 441
    :catch_0
    move-exception v0

    .line 442
    .local v0, "e":Ljava/lang/Exception;
    :try_start_1
    sget-object v1, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SnsFbSyncAdapterStreamsService : Groups sync EXCEPTION !!! "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 444
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 446
    iget-boolean v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->bStreamsSyncSuccess:Z

    if-eqz v1, :cond_1

    .line 447
    invoke-direct {p0}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->performMyPhotosSync()V

    .line 449
    :cond_1
    sget-object v1, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->TAG:Ljava/lang/String;

    const-string v2, "***************** SnsFbSyncAdapterStreamsService : performGroupsSync - FINISHED !!! *****************"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 454
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void

    .line 408
    :cond_2
    :try_start_2
    new-instance v1, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService$4;

    iget-object v2, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    const-string v3, "me"

    invoke-direct {v1, p0, v2, v3}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService$4;-><init>(Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mReq:Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;

    .line 429
    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mReq:Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;

    invoke-virtual {v1}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->request()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 430
    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;

    invoke-virtual {v1}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->suspendSync()V

    .line 434
    :goto_1
    iget v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mSyncState:I

    if-ne v1, v4, :cond_5

    .line 435
    new-instance v1, Ljava/lang/Exception;

    const-string v2, "SnsFbSyncAdapterStreamsService is failed!!!"

    invoke-direct {v1, v2}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 446
    :catchall_0
    move-exception v1

    iget-boolean v2, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->bStreamsSyncSuccess:Z

    if-eqz v2, :cond_3

    .line 447
    invoke-direct {p0}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->performMyPhotosSync()V

    .line 449
    :cond_3
    sget-object v2, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->TAG:Ljava/lang/String;

    const-string v3, "***************** SnsFbSyncAdapterStreamsService : performGroupsSync - FINISHED !!! *****************"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    throw v1

    .line 432
    :cond_4
    const/4 v1, -0x1

    :try_start_3
    iput v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mSyncState:I

    goto :goto_1

    .line 437
    :cond_5
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->bStreamsSyncSuccess:Z
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 446
    iget-boolean v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->bStreamsSyncSuccess:Z

    if-eqz v1, :cond_6

    .line 447
    invoke-direct {p0}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->performMyPhotosSync()V

    .line 449
    :cond_6
    sget-object v1, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->TAG:Ljava/lang/String;

    const-string v2, "***************** SnsFbSyncAdapterStreamsService : performGroupsSync - FINISHED !!! *****************"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private performMyPhotosSync()V
    .locals 12

    .prologue
    const/4 v11, 0x2

    const/4 v10, 0x0

    const/4 v9, -0x1

    const/4 v8, 0x1

    .line 458
    sget-object v4, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->TAG:Ljava/lang/String;

    const-string v5, "***************** SnsFbSyncAdapterStreamsService : performMyPhotosSync - START !!! *****************"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 462
    const/4 v1, 0x0

    .line 464
    .local v1, "bDeletePrevId":Z
    iput-boolean v10, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->bStreamsSyncSuccess:Z

    .line 466
    iput v8, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mSyncState:I

    .line 469
    :try_start_0
    iget-object v4, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mFbToken:Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbToken;

    invoke-virtual {v4}, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbToken;->getTokenState()I

    move-result v4

    if-eq v4, v8, :cond_0

    iget-object v4, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mFbToken:Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbToken;

    invoke-virtual {v4}, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbToken;->getTokenState()I

    move-result v4

    if-ne v4, v11, :cond_2

    .line 471
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->handleSessionExpired()I

    move-result v4

    iput v4, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mSyncState:I

    .line 472
    new-instance v4, Ljava/lang/Exception;

    const-string v5, "Session expired or invalid!!!"

    invoke-direct {v4, v5}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 604
    :catch_0
    move-exception v2

    .line 605
    .local v2, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    .line 606
    sget-object v4, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "SnsFbSyncAdapterStreamsService : EXCEPTION !!! "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 608
    iget-object v4, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mSyncResult:Landroid/content/SyncResult;

    iget-object v4, v4, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    const-wide/16 v6, 0x1

    iput-wide v6, v4, Landroid/content/SyncStats;->numConflictDetectedExceptions:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 609
    const/4 v1, 0x1

    .line 613
    if-eqz v1, :cond_f

    .line 614
    iget-object v4, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;

    const-string v5, "streams"

    invoke-virtual {v4, v5, v10}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->deleteSyncValue(Ljava/lang/String;I)V

    .line 625
    :goto_0
    iget-boolean v4, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->bStreamsSyncSuccess:Z

    if-eqz v4, :cond_1

    .line 627
    :try_start_2
    invoke-direct {p0}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->performTaggedPhotosSync()V
    :try_end_2
    .catch Landroid/accounts/OperationCanceledException; {:try_start_2 .. :try_end_2} :catch_2

    .line 636
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_1
    :goto_1
    sget-object v4, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->TAG:Ljava/lang/String;

    const-string v5, "***************** SnsFbSyncAdapterStreamsService : performMyPhotosSync - FINISHED !!! *****************"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 641
    :goto_2
    return-void

    .line 475
    :cond_2
    :try_start_3
    iget-object v4, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mSyncType:Ljava/lang/String;

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mSyncType:Ljava/lang/String;

    const-string v5, "album"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 480
    :cond_3
    iget-object v4, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mSyncType:Ljava/lang/String;

    if-nez v4, :cond_4

    .line 481
    const-string v4, "album"

    iput-object v4, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mSyncType:Ljava/lang/String;

    .line 486
    :cond_4
    new-instance v4, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService$5;

    iget-object v5, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    const-string v6, "me"

    iget-object v7, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mBundle:Landroid/os/Bundle;

    invoke-direct {v4, p0, v5, v6, v7}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService$5;-><init>(Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;Landroid/os/Bundle;)V

    iput-object v4, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mReq:Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;

    .line 528
    iget-object v4, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mReq:Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;

    invoke-virtual {v4}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->request()Z

    move-result v4

    if-eqz v4, :cond_6

    .line 529
    iget-object v4, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;

    invoke-virtual {v4}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->suspendSync()V

    .line 533
    :goto_3
    iget v4, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mSyncState:I

    if-ne v4, v9, :cond_7

    .line 534
    new-instance v4, Ljava/lang/Exception;

    const-string v5, "SnsFbSyncAdapterStreamsService(Albums) is failed!!!"

    invoke-direct {v4, v5}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 613
    :catchall_0
    move-exception v4

    if-eqz v1, :cond_10

    .line 614
    iget-object v5, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;

    const-string v6, "streams"

    invoke-virtual {v5, v6, v10}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->deleteSyncValue(Ljava/lang/String;I)V

    .line 625
    :goto_4
    iget-boolean v5, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->bStreamsSyncSuccess:Z

    if-eqz v5, :cond_5

    .line 627
    :try_start_4
    invoke-direct {p0}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->performTaggedPhotosSync()V
    :try_end_4
    .catch Landroid/accounts/OperationCanceledException; {:try_start_4 .. :try_end_4} :catch_3

    .line 636
    :cond_5
    :goto_5
    sget-object v5, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->TAG:Ljava/lang/String;

    const-string v6, "***************** SnsFbSyncAdapterStreamsService : performMyPhotosSync - FINISHED !!! *****************"

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    throw v4

    .line 531
    :cond_6
    const/4 v4, -0x1

    :try_start_5
    iput v4, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mSyncState:I

    goto :goto_3

    .line 537
    :cond_7
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->bStreamsSyncSuccess:Z

    .line 539
    iget-object v4, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mBundle:Landroid/os/Bundle;
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    if-nez v4, :cond_4

    .line 545
    :cond_8
    :try_start_6
    iget-object v4, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mAlbumIdList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_6
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_c

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 546
    .local v0, "albumId":Ljava/lang/String;
    const-string v4, "photo"

    iput-object v4, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mSyncType:Ljava/lang/String;

    .line 547
    iput-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mAlbumId:Ljava/lang/String;

    .line 551
    :cond_9
    new-instance v4, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService$6;

    iget-object v5, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    iget-object v6, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mAlbumId:Ljava/lang/String;

    iget-object v7, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mBundle:Landroid/os/Bundle;

    invoke-direct {v4, p0, v5, v6, v7}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService$6;-><init>(Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;Landroid/os/Bundle;)V

    iput-object v4, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mReq:Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;

    .line 585
    iget-object v4, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mReq:Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;

    invoke-virtual {v4}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->request()Z

    move-result v4

    if-eqz v4, :cond_a

    .line 586
    iget-object v4, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;

    invoke-virtual {v4}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->suspendSync()V

    .line 590
    :goto_7
    iget v4, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mSyncState:I

    if-ne v4, v9, :cond_b

    .line 591
    new-instance v4, Ljava/lang/Exception;

    const-string v5, "SnsFbSyncAdapterStreamsService(Photos) is failed!!!"

    invoke-direct {v4, v5}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 600
    .end local v0    # "albumId":Ljava/lang/String;
    .end local v3    # "i$":Ljava/util/Iterator;
    :catchall_1
    move-exception v4

    :try_start_7
    throw v4
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 588
    .restart local v0    # "albumId":Ljava/lang/String;
    .restart local v3    # "i$":Ljava/util/Iterator;
    :cond_a
    const/4 v4, -0x1

    :try_start_8
    iput v4, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mSyncState:I

    goto :goto_7

    .line 594
    :cond_b
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->bStreamsSyncSuccess:Z

    .line 596
    iget-object v4, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mBundle:Landroid/os/Bundle;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    if-nez v4, :cond_9

    goto :goto_6

    .line 613
    .end local v0    # "albumId":Ljava/lang/String;
    :cond_c
    if-eqz v1, :cond_e

    .line 614
    iget-object v4, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;

    const-string v5, "streams"

    invoke-virtual {v4, v5, v10}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->deleteSyncValue(Ljava/lang/String;I)V

    .line 625
    :goto_8
    iget-boolean v4, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->bStreamsSyncSuccess:Z

    if-eqz v4, :cond_d

    .line 627
    :try_start_9
    invoke-direct {p0}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->performTaggedPhotosSync()V
    :try_end_9
    .catch Landroid/accounts/OperationCanceledException; {:try_start_9 .. :try_end_9} :catch_1

    .line 636
    :cond_d
    :goto_9
    sget-object v4, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->TAG:Ljava/lang/String;

    const-string v5, "***************** SnsFbSyncAdapterStreamsService : performMyPhotosSync - FINISHED !!! *****************"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 616
    :cond_e
    iget-object v4, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;

    const-string v5, "streams"

    invoke-virtual {v4, v5, v8}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->deleteSyncValue(Ljava/lang/String;I)V

    .line 617
    iget-object v4, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;

    const-string v5, "streams"

    invoke-virtual {v4, v5}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->updateCurrentSyncValue(Ljava/lang/String;)V

    .line 618
    iput v11, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mSyncState:I

    goto :goto_8

    .line 628
    :catch_1
    move-exception v2

    .line 630
    .local v2, "e":Landroid/accounts/OperationCanceledException;
    invoke-virtual {v2}, Landroid/accounts/OperationCanceledException;->printStackTrace()V

    goto :goto_9

    .line 616
    .end local v3    # "i$":Ljava/util/Iterator;
    .local v2, "e":Ljava/lang/Exception;
    :cond_f
    iget-object v4, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;

    const-string v5, "streams"

    invoke-virtual {v4, v5, v8}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->deleteSyncValue(Ljava/lang/String;I)V

    .line 617
    iget-object v4, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;

    const-string v5, "streams"

    invoke-virtual {v4, v5}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->updateCurrentSyncValue(Ljava/lang/String;)V

    .line 618
    iput v11, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mSyncState:I

    goto/16 :goto_0

    .line 628
    :catch_2
    move-exception v2

    .line 630
    .local v2, "e":Landroid/accounts/OperationCanceledException;
    invoke-virtual {v2}, Landroid/accounts/OperationCanceledException;->printStackTrace()V

    goto/16 :goto_1

    .line 616
    .end local v2    # "e":Landroid/accounts/OperationCanceledException;
    :cond_10
    iget-object v5, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;

    const-string v6, "streams"

    invoke-virtual {v5, v6, v8}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->deleteSyncValue(Ljava/lang/String;I)V

    .line 617
    iget-object v5, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;

    const-string v6, "streams"

    invoke-virtual {v5, v6}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->updateCurrentSyncValue(Ljava/lang/String;)V

    .line 618
    iput v11, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mSyncState:I

    goto/16 :goto_4

    .line 628
    :catch_3
    move-exception v2

    .line 630
    .restart local v2    # "e":Landroid/accounts/OperationCanceledException;
    invoke-virtual {v2}, Landroid/accounts/OperationCanceledException;->printStackTrace()V

    goto/16 :goto_5
.end method

.method private performSync()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/accounts/OperationCanceledException;
        }
    .end annotation

    .prologue
    const/4 v4, -0x1

    const/4 v3, 0x1

    .line 195
    sget-object v1, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->TAG:Ljava/lang/String;

    const-string v2, "***************** SnsFbSyncAdapterStreamsService : performSync - START !!! *****************"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 198
    iput v3, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mSyncState:I

    .line 201
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mFbToken:Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbToken;

    invoke-virtual {v1}, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbToken;->getTokenState()I

    move-result v1

    if-eq v1, v3, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mFbToken:Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbToken;

    invoke-virtual {v1}, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbToken;->getTokenState()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    .line 203
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->handleSessionExpired()I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mSyncState:I

    .line 204
    new-instance v1, Ljava/lang/Exception;

    const-string v2, "Session expired or invalid!!!"

    invoke-direct {v1, v2}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 246
    :catch_0
    move-exception v0

    .line 247
    .local v0, "e":Ljava/lang/Exception;
    :try_start_1
    sget-object v1, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SnsFbSyncAdapterStreamsService : EXCEPTION !!! "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 248
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 250
    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mSyncResult:Landroid/content/SyncResult;

    iget-object v1, v1, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v1, Landroid/content/SyncStats;->numConflictDetectedExceptions:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, v1, Landroid/content/SyncStats;->numConflictDetectedExceptions:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 253
    iget-boolean v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->bStreamsSyncSuccess:Z

    if-eqz v1, :cond_1

    .line 254
    invoke-direct {p0}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->performCommentSync()V

    .line 258
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_1
    :goto_0
    return-void

    .line 207
    :cond_2
    :try_start_2
    new-instance v1, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService$1;

    iget-object v2, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    const-string v3, "me"

    invoke-direct {v1, p0, v2, v3}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService$1;-><init>(Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mReq:Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;

    .line 235
    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mReq:Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;

    invoke-virtual {v1}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->request()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 236
    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;

    invoke-virtual {v1}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->suspendSync()V

    .line 240
    :goto_1
    iget v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mSyncState:I

    if-ne v1, v4, :cond_5

    .line 241
    new-instance v1, Ljava/lang/Exception;

    const-string v2, "SnsFbSyncAdapterStreamsService is failed!!!"

    invoke-direct {v1, v2}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 253
    :catchall_0
    move-exception v1

    iget-boolean v2, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->bStreamsSyncSuccess:Z

    if-eqz v2, :cond_3

    .line 254
    invoke-direct {p0}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->performCommentSync()V

    :cond_3
    throw v1

    .line 238
    :cond_4
    const/4 v1, -0x1

    :try_start_3
    iput v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mSyncState:I

    goto :goto_1

    .line 243
    :cond_5
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->bStreamsSyncSuccess:Z
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 253
    iget-boolean v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->bStreamsSyncSuccess:Z

    if-eqz v1, :cond_1

    .line 254
    invoke-direct {p0}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->performCommentSync()V

    goto :goto_0
.end method

.method private performTaggedPhotosSync()V
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/accounts/OperationCanceledException;
        }
    .end annotation

    .prologue
    const/4 v10, -0x1

    const/4 v1, 0x1

    const/4 v9, 0x2

    const/4 v8, 0x0

    .line 645
    const/4 v6, 0x0

    .line 647
    .local v6, "bDeletePrevId":Z
    iput v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mSyncState:I

    .line 648
    iput-boolean v8, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->bStreamsSyncSuccess:Z

    .line 650
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 653
    .local v5, "photoArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhotos;>;"
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mFbToken:Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbToken;

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbToken;->getTokenState()I

    move-result v0

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mFbToken:Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbToken;

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbToken;->getTokenState()I

    move-result v0

    if-ne v0, v9, :cond_1

    .line 655
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->handleSessionExpired()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mSyncState:I

    .line 656
    new-instance v0, Ljava/lang/Exception;

    const-string v1, "Session expired or invalid!!!"

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 705
    :catch_0
    move-exception v7

    .line 706
    .local v7, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V

    .line 707
    sget-object v0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SnsFbSyncAdapterStreamsService : EXCEPTION !!! "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v7}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 709
    iget-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mSyncResult:Landroid/content/SyncResult;

    iget-object v0, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    const-wide/16 v2, 0x1

    iput-wide v2, v0, Landroid/content/SyncStats;->numConflictDetectedExceptions:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 710
    const/4 v6, 0x1

    .line 713
    if-eqz v6, :cond_6

    .line 714
    iget-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;

    const-string v1, "streams"

    invoke-virtual {v0, v1, v8}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->deleteSyncValue(Ljava/lang/String;I)V

    .line 720
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;

    const-string v1, "album"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->deletePrevSyncState(Ljava/lang/String;)V

    .line 722
    invoke-direct {p0}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->invokeBroadcast()V

    .line 724
    sget-object v0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->TAG:Ljava/lang/String;

    const-string v1, "***************** SnsFbSyncAdapterStreamsService : performTaggedPhotosSync - FINISHED !!! *****************"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 729
    .end local v7    # "e":Ljava/lang/Exception;
    :goto_1
    return-void

    .line 659
    :cond_1
    :try_start_2
    new-instance v0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService$7;

    iget-object v2, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    const-string v3, "me"

    iget-object v4, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mBundle:Landroid/os/Bundle;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService$7;-><init>(Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;Landroid/os/Bundle;Ljava/util/ArrayList;)V

    iput-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mReq:Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;

    .line 688
    iget-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mReq:Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->request()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 689
    iget-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->suspendSync()V

    .line 693
    :goto_2
    iget-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mBundle:Landroid/os/Bundle;

    if-nez v0, :cond_1

    .line 695
    invoke-virtual {v5}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 696
    iget-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;

    sget-object v1, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$SyncTags;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->clearTable(Landroid/net/Uri;)V

    .line 697
    iget-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;

    invoke-virtual {v0, v5}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->insertTagInfo(Ljava/util/ArrayList;)V

    .line 700
    :cond_2
    iget v0, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mSyncState:I

    if-ne v0, v10, :cond_4

    .line 701
    new-instance v0, Ljava/lang/Exception;

    const-string v1, "SnsFbSyncAdapterStreamsService(Albums) is failed!!!"

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 713
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_7

    .line 714
    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;

    const-string v2, "streams"

    invoke-virtual {v1, v2, v8}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->deleteSyncValue(Ljava/lang/String;I)V

    .line 720
    :goto_3
    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;

    const-string v2, "album"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->deletePrevSyncState(Ljava/lang/String;)V

    .line 722
    invoke-direct {p0}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->invokeBroadcast()V

    .line 724
    sget-object v1, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->TAG:Ljava/lang/String;

    const-string v2, "***************** SnsFbSyncAdapterStreamsService : performTaggedPhotosSync - FINISHED !!! *****************"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    throw v0

    .line 691
    :cond_3
    const/4 v0, -0x1

    :try_start_3
    iput v0, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mSyncState:I

    goto :goto_2

    .line 703
    :cond_4
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->bStreamsSyncSuccess:Z
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 713
    if-eqz v6, :cond_5

    .line 714
    iget-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;

    const-string v1, "streams"

    invoke-virtual {v0, v1, v8}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->deleteSyncValue(Ljava/lang/String;I)V

    .line 720
    :goto_4
    iget-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;

    const-string v1, "album"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->deletePrevSyncState(Ljava/lang/String;)V

    .line 722
    invoke-direct {p0}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->invokeBroadcast()V

    .line 724
    sget-object v0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->TAG:Ljava/lang/String;

    const-string v1, "***************** SnsFbSyncAdapterStreamsService : performTaggedPhotosSync - FINISHED !!! *****************"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 716
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;

    const-string v1, "streams"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->updateCurrentSyncValue(Ljava/lang/String;)V

    .line 717
    iput v9, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mSyncState:I

    goto :goto_4

    .line 716
    .restart local v7    # "e":Ljava/lang/Exception;
    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;

    const-string v1, "streams"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->updateCurrentSyncValue(Ljava/lang/String;)V

    .line 717
    iput v9, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mSyncState:I

    goto/16 :goto_0

    .line 716
    .end local v7    # "e":Ljava/lang/Exception;
    :cond_7
    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;

    const-string v2, "streams"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->updateCurrentSyncValue(Ljava/lang/String;)V

    .line 717
    iput v9, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mSyncState:I

    goto :goto_3
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 165
    sget-object v0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->TAG:Ljava/lang/String;

    const-string v1, "***************** SnsFbSyncAdapterStreamsService : onBind !!! *****************"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 168
    iget-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mSyncAdapter:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService$SyncAdapterImpl;

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService$SyncAdapterImpl;->getSyncAdapterBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 148
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 150
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/SnsApplication;->getSvcMgr()Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    .line 152
    iget-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mSyncAdapter:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService$SyncAdapterImpl;

    if-nez v0, :cond_0

    .line 153
    new-instance v0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService$SyncAdapterImpl;

    invoke-direct {v0, p0, p0}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService$SyncAdapterImpl;-><init>(Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mSyncAdapter:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService$SyncAdapterImpl;

    .line 156
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;

    if-nez v0, :cond_1

    .line 157
    new-instance v0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;

    invoke-direct {v0}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;

    .line 160
    :cond_1
    return-void
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 174
    const/4 v0, 0x0

    .line 175
    .local v0, "bActive":Z
    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mAccount:Landroid/accounts/Account;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mAuthority:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 176
    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mAccount:Landroid/accounts/Account;

    iget-object v2, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mAuthority:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/content/ContentResolver;->isSyncActive(Landroid/accounts/Account;Ljava/lang/String;)Z

    move-result v0

    .line 179
    :cond_0
    sget-object v1, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->TAG:Ljava/lang/String;

    const-string v2, "***************** SnsFbSyncAdapterStreamsService : onUnbind !!! *****************"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 182
    if-eqz v0, :cond_2

    iget v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mSyncState:I

    const/4 v2, 0x1

    if-eq v1, v2, :cond_1

    iget v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->mSyncState:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_2

    .line 184
    :cond_1
    sget-object v1, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->TAG:Ljava/lang/String;

    const-string v2, "SnsFbSyncAdapterStreamsService : onUnbind : SYNC ERROR : performSync was stopped by forced abort or pending problem!!!"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 190
    :goto_0
    invoke-super {p0, p1}, Landroid/app/Service;->onUnbind(Landroid/content/Intent;)Z

    move-result v1

    return v1

    .line 188
    :cond_2
    sget-object v1, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterStreamsService;->TAG:Ljava/lang/String;

    const-string v2, "SnsFbSyncAdapterStreamsService : onUnbind : COMPLETE STATE!!!"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
