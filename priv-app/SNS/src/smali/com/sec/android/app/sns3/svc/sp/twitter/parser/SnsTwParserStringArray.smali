.class public Lcom/sec/android/app/sns3/svc/sp/twitter/parser/SnsTwParserStringArray;
.super Ljava/lang/Object;
.source "SnsTwParserStringArray.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStringArray;
    .locals 7
    .param p0, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 31
    new-instance v4, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStringArray;

    invoke-direct {v4}, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStringArray;-><init>()V

    .line 34
    .local v4, "response":Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStringArray;
    invoke-static {p0}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->isJsonEmpty(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 35
    const/4 v4, 0x0

    .line 54
    .end local v4    # "response":Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStringArray;
    :cond_0
    :goto_0
    return-object v4

    .line 41
    .restart local v4    # "response":Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStringArray;
    :cond_1
    :try_start_0
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 43
    .local v3, "jsonObject":Lorg/json/JSONObject;
    const-string v5, "ids"

    invoke-virtual {v3, v5}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    .line 44
    .local v2, "jsonArray":Lorg/json/JSONArray;
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v5

    new-array v5, v5, [Ljava/lang/String;

    iput-object v5, v4, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStringArray;->mStringArray:[Ljava/lang/String;

    .line 46
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v5

    if-ge v1, v5, :cond_0

    .line 48
    iget-object v5, v4, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStringArray;->mStringArray:[Ljava/lang/String;

    invoke-virtual {v2, v1}, Lorg/json/JSONArray;->optString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v1
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 46
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 50
    .end local v1    # "i":I
    .end local v2    # "jsonArray":Lorg/json/JSONArray;
    .end local v3    # "jsonObject":Lorg/json/JSONObject;
    :catch_0
    move-exception v0

    .line 51
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method
