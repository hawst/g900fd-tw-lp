.class final Lcom/sec/android/app/sns3/app/profile/sp/googleplus/SnsGpGetMeProfile$1;
.super Lcom/sec/android/app/sns3/svc/sp/googleplus/request/SnsGpReqGetProfile;
.source "SnsGpGetMeProfile.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/sns3/app/profile/sp/googleplus/SnsGpGetMeProfile;->getMeProfile(Landroid/content/Context;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;Landroid/os/Bundle;Landroid/content/Context;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    .param p2, "x1"    # Ljava/lang/String;
    .param p3, "x2"    # Landroid/os/Bundle;

    .prologue
    .line 64
    iput-object p4, p0, Lcom/sec/android/app/sns3/app/profile/sp/googleplus/SnsGpGetMeProfile$1;->val$context:Landroid/content/Context;

    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/sns3/svc/sp/googleplus/request/SnsGpReqGetProfile;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;Landroid/os/Bundle;)V

    return-void
.end method


# virtual methods
.method public onReqRespond(IZIILandroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseProfile;)Z
    .locals 4
    .param p1, "reqID"    # I
    .param p2, "bSuccess"    # Z
    .param p3, "httpStatus"    # I
    .param p4, "errorCode"    # I
    .param p5, "reason"    # Landroid/os/Bundle;
    .param p6, "profile"    # Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseProfile;

    .prologue
    .line 69
    const/4 v0, -0x1

    .line 71
    .local v0, "resultCode":I
    if-eqz p2, :cond_0

    .line 72
    const/4 v0, 0x0

    .line 74
    const-string v1, "SnsGpGetMeProfile"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SnsGpReqGetProfile - NAME : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p6, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseProfile;->mUserName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " EMAIL "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p6, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseProfile;->mEmail:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " BIRTHDAY "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p6, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseProfile;->mBirthday:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ADDRESS "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p6, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseProfile;->mPlacesLived:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " WORK "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p6, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseProfile;->mWork:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " PROFILE URL "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p6, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseProfile;->mImageUrl:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 85
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/sns3/app/profile/sp/googleplus/SnsGpGetMeProfile$1;->val$context:Landroid/content/Context;

    # invokes: Lcom/sec/android/app/sns3/app/profile/sp/googleplus/SnsGpGetMeProfile;->invokeBroadcast(Landroid/content/Context;ILcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseProfile;)V
    invoke-static {v1, v0, p6}, Lcom/sec/android/app/sns3/app/profile/sp/googleplus/SnsGpGetMeProfile;->access$000(Landroid/content/Context;ILcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseProfile;)V

    .line 86
    const/4 v1, 0x1

    return v1

    .line 79
    :cond_0
    const/4 v0, -0x1

    .line 81
    const-string v1, "SnsGpGetMeProfile"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SnsGpReqGetMyProfile errorCode : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", reason : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
