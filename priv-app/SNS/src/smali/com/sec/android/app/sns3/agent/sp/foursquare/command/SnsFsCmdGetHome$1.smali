.class Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdGetHome$1;
.super Lcom/sec/android/app/sns3/svc/sp/foursquare/request/SnsFsReqGetHome;
.source "SnsFsCmdGetHome.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdGetHome;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Handler;Ljava/lang/String;Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdGetHome;


# direct methods
.method constructor <init>(Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdGetHome;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Bundle;)V
    .locals 0
    .param p2, "x0"    # Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    .param p3, "x1"    # Landroid/os/Bundle;

    .prologue
    .line 46
    iput-object p1, p0, Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdGetHome$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdGetHome;

    invoke-direct {p0, p2, p3}, Lcom/sec/android/app/sns3/svc/sp/foursquare/request/SnsFsReqGetHome;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Bundle;)V

    return-void
.end method


# virtual methods
.method public onReqRespond(IZIILandroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseFeed;)Z
    .locals 7
    .param p1, "reqID"    # I
    .param p2, "bSuccess"    # Z
    .param p3, "httpStatus"    # I
    .param p4, "errorCode"    # I
    .param p5, "reason"    # Landroid/os/Bundle;
    .param p6, "feed"    # Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseFeed;

    .prologue
    const/4 v6, 0x0

    .line 51
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 52
    .local v2, "values":Landroid/content/ContentValues;
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/sns3/SnsApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 54
    .local v0, "cr":Landroid/content/ContentResolver;
    if-eqz p2, :cond_1

    .line 55
    if-eqz p6, :cond_0

    .line 57
    move-object v1, p6

    .line 58
    .local v1, "curFeed":Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseFeed;
    sget-object v3, Lcom/sec/android/app/sns3/agent/sp/foursquare/db/SnsFourSquareDB$Timeline;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v3, v6, v6}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 59
    :goto_0
    if-eqz v1, :cond_0

    .line 60
    invoke-virtual {v2}, Landroid/content/ContentValues;->clear()V

    .line 62
    const-string v3, "user_id"

    iget-object v4, v1, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseFeed;->mUser:Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;

    iget-object v4, v4, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mUserID:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    const-string v3, "formatted_name"

    iget-object v4, v1, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseFeed;->mUser:Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;

    iget-object v4, v4, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mFormattedName:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    const-string v3, "relationship"

    iget-object v4, v1, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseFeed;->mUser:Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;

    iget-object v4, v4, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mRelationship:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    const-string v3, "image_url"

    iget-object v4, v1, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseFeed;->mUser:Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;

    iget-object v4, v4, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseUser;->mPhotoUrl:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    const-string v3, "checkin_id"

    iget-object v4, v1, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseFeed;->mCheckIn_Id:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    const-string v3, "message"

    iget-object v4, v1, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseFeed;->mDescription:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    const-string v3, "timestamp_utc"

    iget-object v4, v1, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseFeed;->mCreatedAt:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    const-string v3, "time_zone"

    iget-object v4, v1, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseFeed;->mTimeZoneOffSet:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    const-string v3, "location_name"

    iget-object v4, v1, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseFeed;->mVenue:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    const-string v3, "latitude"

    iget-object v4, v1, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseFeed;->mLocLatitude:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    const-string v3, "longitude"

    iget-object v4, v1, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseFeed;->mLocLongitude:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    const-string v3, "object_type"

    const-string v4, "CheckIn"

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    const-string v3, "category_icon"

    iget-object v4, v1, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseFeed;->mCategoryIcon:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    const-string v3, "category_id"

    iget-object v4, v1, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseFeed;->mCategoryId:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    const-string v3, "category_name"

    iget-object v4, v1, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseFeed;->mCategoryName:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    sget-object v3, Lcom/sec/android/app/sns3/agent/sp/foursquare/db/SnsFourSquareDB$Timeline;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v3, v2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 80
    iget-object v1, v1, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseFeed;->mNext:Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseFeed;

    goto :goto_0

    .line 83
    .end local v1    # "curFeed":Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseFeed;
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdGetHome$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdGetHome;

    sget-object v4, Lcom/sec/android/app/sns3/agent/sp/foursquare/db/SnsFourSquareDB$Timeline;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdGetHome;->setUri(Ljava/lang/String;)V

    .line 90
    :goto_1
    iget-object v3, p0, Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdGetHome$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdGetHome;

    invoke-virtual {v3, p2}, Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdGetHome;->setSuccess(Z)V

    .line 91
    iget-object v3, p0, Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdGetHome$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdGetHome;

    invoke-virtual {v3, p0}, Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdGetHome;->receive(Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;)V

    .line 93
    const/4 v3, 0x1

    return v3

    .line 85
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdGetHome$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdGetHome;

    new-instance v4, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;

    const-string v5, "foursquare"

    invoke-direct {v4, v5, p3, p4, p5}, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;-><init>(Ljava/lang/String;IILandroid/os/Bundle;)V

    invoke-virtual {v3, v4}, Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdGetHome;->setResponseList(Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;)Z

    .line 87
    iget-object v3, p0, Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdGetHome$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdGetHome;

    invoke-virtual {v3, v6}, Lcom/sec/android/app/sns3/agent/sp/foursquare/command/SnsFsCmdGetHome;->setUri(Ljava/lang/String;)V

    goto :goto_1
.end method
