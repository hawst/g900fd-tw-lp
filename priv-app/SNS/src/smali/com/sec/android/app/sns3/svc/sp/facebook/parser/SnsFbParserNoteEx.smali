.class public Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserNoteEx;
.super Ljava/lang/Object;
.source "SnsFbParserNoteEx.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserNoteEx$FacebookNoteEx;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    return-void
.end method

.method public static parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNoteEx;
    .locals 8
    .param p0, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 85
    new-instance v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNoteEx;

    invoke-direct {v3}, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNoteEx;-><init>()V

    .line 88
    .local v3, "note":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNoteEx;
    :try_start_0
    new-instance v2, Lorg/json/JSONArray;

    invoke-direct {v2, p0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 90
    .local v2, "ja":Lorg/json/JSONArray;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v6

    if-ge v1, v6, :cond_2

    .line 92
    invoke-virtual {v2, v1}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v6

    const-string v7, "name"

    invoke-virtual {v6, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 93
    .local v4, "queryName":Ljava/lang/String;
    invoke-virtual {v2, v1}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v6

    const-string v7, "fql_result_set"

    invoke-virtual {v6, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 95
    .local v5, "queryObject":Ljava/lang/String;
    const-string v6, "note_query"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 96
    invoke-static {v3, v5}, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserNoteEx;->parseNote(Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNoteEx;Ljava/lang/String;)V

    .line 90
    :cond_0
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 97
    :cond_1
    const-string v6, "comment_query"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 98
    invoke-static {v3, v5}, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserNoteEx;->parseComment(Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNoteEx;Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseCommentQueryResult;

    move-result-object v6

    iput-object v6, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNoteEx;->mComment:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseCommentQueryResult;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 104
    .end local v1    # "i":I
    .end local v2    # "ja":Lorg/json/JSONArray;
    .end local v4    # "queryName":Ljava/lang/String;
    .end local v5    # "queryObject":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 106
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    .line 109
    .end local v0    # "e":Lorg/json/JSONException;
    :cond_2
    return-object v3

    .line 99
    .restart local v1    # "i":I
    .restart local v2    # "ja":Lorg/json/JSONArray;
    .restart local v4    # "queryName":Ljava/lang/String;
    .restart local v5    # "queryObject":Ljava/lang/String;
    :cond_3
    :try_start_1
    const-string v6, "like_query"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 100
    invoke-static {v5}, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserNoteEx;->parseLike(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNoteEx;->mLikeDone:Ljava/lang/String;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method

.method private static parseComment(Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNoteEx;Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseCommentQueryResult;
    .locals 10
    .param p0, "note"    # Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNoteEx;
    .param p1, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 138
    const/4 v0, 0x0

    .line 139
    .local v0, "comment":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseCommentQueryResult;
    const/4 v1, 0x0

    .line 142
    .local v1, "curComment":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseCommentQueryResult;
    :try_start_0
    new-instance v4, Lorg/json/JSONArray;

    invoke-direct {v4, p1}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 144
    .local v4, "ja":Lorg/json/JSONArray;
    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNoteEx;->mCommentsCount:Ljava/lang/Integer;

    .line 146
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v7

    if-ge v3, v7, :cond_1

    .line 147
    new-instance v6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseCommentQueryResult;

    invoke-direct {v6}, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseCommentQueryResult;-><init>()V

    .line 149
    .local v6, "newComment":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseCommentQueryResult;
    invoke-virtual {v4, v3}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v5

    .line 151
    .local v5, "jsonObject":Lorg/json/JSONObject;
    const-string v7, "id"

    invoke-virtual {v5, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseCommentQueryResult;->mCommentID:Ljava/lang/String;

    .line 153
    const-string v7, "object_id"

    invoke-virtual {v5, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseCommentQueryResult;->mTargetID:Ljava/lang/String;

    .line 155
    const-string v7, "fromid"

    invoke-virtual {v5, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseCommentQueryResult;->mFromID:Ljava/lang/String;

    .line 156
    const-string v7, "username"

    invoke-virtual {v5, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseCommentQueryResult;->mFromName:Ljava/lang/String;

    .line 158
    const-string v7, "text"

    invoke-virtual {v5, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseCommentQueryResult;->mMessage:Ljava/lang/String;

    .line 160
    const-string v7, "time"

    invoke-virtual {v5, v7}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    iput-object v7, v6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseCommentQueryResult;->mCreatedTime:Ljava/lang/Long;

    .line 162
    const-string v7, "likes"

    invoke-virtual {v5, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseCommentQueryResult;->mLikes:Ljava/lang/String;

    .line 163
    const-string v7, "user_likes"

    invoke-virtual {v5, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseCommentQueryResult;->mUserLikes:Ljava/lang/String;

    .line 166
    if-nez v0, :cond_0

    .line 167
    move-object v0, v6

    .line 168
    move-object v1, v0

    .line 146
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 170
    :cond_0
    iput-object v6, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseCommentQueryResult;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseCommentQueryResult;

    .line 171
    iget-object v1, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseCommentQueryResult;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseCommentQueryResult;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 176
    .end local v3    # "i":I
    .end local v4    # "ja":Lorg/json/JSONArray;
    .end local v5    # "jsonObject":Lorg/json/JSONObject;
    .end local v6    # "newComment":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseCommentQueryResult;
    :catch_0
    move-exception v2

    .line 178
    .local v2, "e":Lorg/json/JSONException;
    invoke-virtual {v2}, Lorg/json/JSONException;->printStackTrace()V

    .line 181
    .end local v2    # "e":Lorg/json/JSONException;
    :cond_1
    return-object v0
.end method

.method private static parseLike(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 185
    const/4 v3, 0x0

    .line 188
    .local v3, "targetID":Ljava/lang/String;
    :try_start_0
    new-instance v1, Lorg/json/JSONArray;

    invoke-direct {v1, p0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 190
    .local v1, "ja":Lorg/json/JSONArray;
    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v4

    if-eqz v4, :cond_0

    .line 191
    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v2

    .line 193
    .local v2, "jsonObject":Lorg/json/JSONObject;
    const-string v4, "object_id"

    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 201
    .end local v1    # "ja":Lorg/json/JSONArray;
    .end local v2    # "jsonObject":Lorg/json/JSONObject;
    :cond_0
    :goto_0
    return-object v3

    .line 196
    :catch_0
    move-exception v0

    .line 198
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method private static parseNote(Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNoteEx;Ljava/lang/String;)V
    .locals 6
    .param p0, "note"    # Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNoteEx;
    .param p1, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 115
    :try_start_0
    new-instance v1, Lorg/json/JSONArray;

    invoke-direct {v1, p1}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 117
    .local v1, "ja":Lorg/json/JSONArray;
    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-eqz v3, :cond_0

    .line 118
    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v2

    .line 120
    .local v2, "jsonObject":Lorg/json/JSONObject;
    const-string v3, "uid"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNoteEx;->mUId:Ljava/lang/String;

    .line 121
    const-string v3, "note_id"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNoteEx;->mNoteID:Ljava/lang/String;

    .line 122
    const-string v3, "created_time"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNoteEx;->mCreatedTime:Ljava/lang/Long;

    .line 123
    const-string v3, "updated_time"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNoteEx;->mUpdatedTime:Ljava/lang/Long;

    .line 124
    const-string v3, "content"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNoteEx;->mContent:Ljava/lang/String;

    .line 125
    const-string v3, "content_html"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNoteEx;->mContentHTML:Ljava/lang/String;

    .line 127
    const-string v3, "title"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNoteEx;->mTitle:Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 134
    .end local v1    # "ja":Lorg/json/JSONArray;
    .end local v2    # "jsonObject":Lorg/json/JSONObject;
    :cond_0
    :goto_0
    return-void

    .line 130
    :catch_0
    move-exception v0

    .line 132
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method
