.class Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity$5;
.super Ljava/lang/Object;
.source "SnsAccountTwOAuthWebViewActivity.java"

# interfaces
.implements Lcom/sec/android/app/sns3/agent/command/ISnsCommandCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;)V
    .locals 0

    .prologue
    .line 302
    iput-object p1, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity$5;->this$0:Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCmdRespond(IZLjava/lang/String;Ljava/util/List;)V
    .locals 4
    .param p1, "cmdID"    # I
    .param p2, "bSuccess"    # Z
    .param p3, "uri"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IZ",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p4, "responseList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;>;"
    const/4 v3, -0x1

    .line 308
    if-eqz p2, :cond_0

    .line 309
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity$5;->this$0:Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;

    # getter for: Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    invoke-static {v1}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;->access$200(Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;)Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/sns3/svc/SnsSvcMgr;->getTokenMgr()Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;

    move-result-object v1

    const-string v2, "twitter"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;->getToken(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;

    .line 311
    .local v0, "token":Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity$5;->this$0:Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "https://api.twitter.com/oauth/authorize?oauth_token="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;->getUnauthToken()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;->refreshWebViewUI(Ljava/lang/String;)V

    .line 316
    .end local v0    # "token":Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;
    :goto_0
    return-void

    .line 314
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity$5;->this$0:Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;

    const/4 v2, 0x0

    invoke-virtual {v1, v3, v3, v2}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;->loginFail(IILjava/lang/String;)V

    goto :goto_0
.end method
