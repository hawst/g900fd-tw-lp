.class public final enum Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSavedSearchesAPI;
.super Ljava/lang/Enum;
.source "SnsTwSavedSearchesAPI.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSavedSearchesAPI$Param;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSavedSearchesAPI;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSavedSearchesAPI;

.field public static final enum SAVED_SEARCHES:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSavedSearchesAPI;

.field public static final enum SAVED_SEARCHES_CREATE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSavedSearchesAPI;

.field public static final enum SAVED_SEARCHES_DESTROY:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSavedSearchesAPI;

.field public static final enum SAVED_SEARCHES_SHOW:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSavedSearchesAPI;


# instance fields
.field private mHttpMethod:Ljava/lang/String;

.field private mUrl:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 28
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSavedSearchesAPI;

    const-string v1, "SAVED_SEARCHES"

    const-string v2, "/saved_searches.json"

    const-string v3, "GET"

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSavedSearchesAPI;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSavedSearchesAPI;->SAVED_SEARCHES:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSavedSearchesAPI;

    .line 29
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSavedSearchesAPI;

    const-string v1, "SAVED_SEARCHES_SHOW"

    const-string v2, "/saved_searches/show/"

    const-string v3, "GET"

    invoke-direct {v0, v1, v5, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSavedSearchesAPI;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSavedSearchesAPI;->SAVED_SEARCHES_SHOW:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSavedSearchesAPI;

    .line 30
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSavedSearchesAPI;

    const-string v1, "SAVED_SEARCHES_CREATE"

    const-string v2, "/saved_searches/create.json"

    const-string v3, "POST"

    invoke-direct {v0, v1, v6, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSavedSearchesAPI;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSavedSearchesAPI;->SAVED_SEARCHES_CREATE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSavedSearchesAPI;

    .line 31
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSavedSearchesAPI;

    const-string v1, "SAVED_SEARCHES_DESTROY"

    const-string v2, "/saved_searches/destroy/"

    const-string v3, "POST"

    invoke-direct {v0, v1, v7, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSavedSearchesAPI;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSavedSearchesAPI;->SAVED_SEARCHES_DESTROY:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSavedSearchesAPI;

    .line 26
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSavedSearchesAPI;

    sget-object v1, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSavedSearchesAPI;->SAVED_SEARCHES:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSavedSearchesAPI;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSavedSearchesAPI;->SAVED_SEARCHES_SHOW:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSavedSearchesAPI;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSavedSearchesAPI;->SAVED_SEARCHES_CREATE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSavedSearchesAPI;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSavedSearchesAPI;->SAVED_SEARCHES_DESTROY:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSavedSearchesAPI;

    aput-object v1, v0, v7

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSavedSearchesAPI;->$VALUES:[Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSavedSearchesAPI;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p3, "url"    # Ljava/lang/String;
    .param p4, "httpMethod"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 51
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 52
    iput-object p3, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSavedSearchesAPI;->mUrl:Ljava/lang/String;

    .line 53
    iput-object p4, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSavedSearchesAPI;->mHttpMethod:Ljava/lang/String;

    .line 54
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSavedSearchesAPI;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 26
    const-class v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSavedSearchesAPI;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSavedSearchesAPI;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSavedSearchesAPI;
    .locals 1

    .prologue
    .line 26
    sget-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSavedSearchesAPI;->$VALUES:[Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSavedSearchesAPI;

    invoke-virtual {v0}, [Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSavedSearchesAPI;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSavedSearchesAPI;

    return-object v0
.end method


# virtual methods
.method public getHttpMethod()Ljava/lang/String;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSavedSearchesAPI;->mHttpMethod:Ljava/lang/String;

    return-object v0
.end method

.method public getUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSavedSearchesAPI;->mUrl:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwSavedSearchesAPI;->mUrl:Ljava/lang/String;

    return-object v0
.end method
