.class public Lcom/sec/android/app/sns3/agent/sp/sinaweibo/db/SnsSinaweiboDB;
.super Ljava/lang/Object;
.source "SnsSinaweiboDB.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/agent/sp/sinaweibo/db/SnsSinaweiboDB$UserTimelineColumns;,
        Lcom/sec/android/app/sns3/agent/sp/sinaweibo/db/SnsSinaweiboDB$UserTimeLine;,
        Lcom/sec/android/app/sns3/agent/sp/sinaweibo/db/SnsSinaweiboDB$FriendTimelineColumns;,
        Lcom/sec/android/app/sns3/agent/sp/sinaweibo/db/SnsSinaweiboDB$FriendTimeline;,
        Lcom/sec/android/app/sns3/agent/sp/sinaweibo/db/SnsSinaweiboDB$UserColumns;,
        Lcom/sec/android/app/sns3/agent/sp/sinaweibo/db/SnsSinaweiboDB$User;,
        Lcom/sec/android/app/sns3/agent/sp/sinaweibo/db/SnsSinaweiboDB$WIPE_SINAWEIBO_DATA;
    }
.end annotation


# static fields
.field public static final AUTHORITY:Ljava/lang/String; = "com.sec.android.app.sns3.sp.sinaweibo"

.field public static final CONTENT_ITEM_TYPE_BASE:Ljava/lang/String; = "vnd.android.cursor.item/vnd.com.sec.android.app.sns3.sp.sinaweibo"

.field public static final CONTENT_TYPE_BASE:Ljava/lang/String; = "vnd.android.cursor.dir/vnd.com.sec.android.app.sns3.sp.sinaweibo"

.field public static final DATABASE_NAME:Ljava/lang/String; = "SnsSinaweiboDB.db"

.field public static final DATABASE_VERSION:I = 0x14

.field public static final FRIEND_TIMELINE_TABLE_NAME:Ljava/lang/String; = "friendtimeline"

.field public static final LIFE_AUTHORITY:Ljava/lang/String; = "com.sec.android.app.sns3.life"

.field public static final USER_TABLE_NAME:Ljava/lang/String; = "user"

.field public static final USER_TIMELINE_TABLE_NAME:Ljava/lang/String; = "usertimeline"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 113
    return-void
.end method
