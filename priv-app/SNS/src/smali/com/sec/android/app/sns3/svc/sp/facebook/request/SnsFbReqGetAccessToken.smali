.class public abstract Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqGetAccessToken;
.super Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqBase;
.source "SnsFbReqGetAccessToken.java"

# interfaces
.implements Lcom/sec/android/app/sns3/svc/sp/facebook/callback/ISnsFbReqCbAccessToken;


# instance fields
.field private mCode:Ljava/lang/String;

.field private mOjbectID:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "svcMgr"    # Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    .param p2, "objectID"    # Ljava/lang/String;
    .param p3, "code"    # Ljava/lang/String;

    .prologue
    .line 45
    const/16 v0, 0x14

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqBase;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;I)V

    .line 47
    iput-object p2, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqGetAccessToken;->mOjbectID:Ljava/lang/String;

    .line 48
    iput-object p3, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqGetAccessToken;->mCode:Ljava/lang/String;

    .line 49
    return-void
.end method


# virtual methods
.method protected compose()Lcom/sec/android/app/sns3/svc/http/SnsHttpRequest;
    .locals 6

    .prologue
    .line 54
    const-string v2, "GET"

    .line 55
    .local v2, "method":Ljava/lang/String;
    const/4 v3, 0x0

    .line 56
    .local v3, "uri":Ljava/lang/String;
    const/4 v4, 0x0

    .line 57
    .local v4, "header":Landroid/os/Bundle;
    const/4 v5, 0x0

    .line 59
    .local v5, "body":Landroid/os/Bundle;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "https://graph.facebook.com/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqGetAccessToken;->mOjbectID:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/access_token"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "?client_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbAppIdManager;->getInstance()Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbAppIdManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbAppIdManager;->getAppId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->encodeUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&redirect_uri="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "fbconnect://success"

    invoke-static {v1}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->encodeUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&client_secret="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbAppIdManager;->getInstance()Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbAppIdManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbAppIdManager;->getSecretKey()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->encodeUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&code="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqGetAccessToken;->mCode:Ljava/lang/String;

    invoke-static {v1}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->encodeUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 65
    new-instance v0, Lcom/sec/android/app/sns3/svc/http/SnsHttpRequest;

    iget v1, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqGetAccessToken;->mReqID:I

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/sns3/svc/http/SnsHttpRequest;-><init>(ILjava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;)V

    return-object v0
.end method

.method protected parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;
    .locals 2
    .param p1, "content"    # Ljava/lang/String;

    .prologue
    .line 70
    const-string v0, "SNS"

    const-string v1, "Facebook  SnsFbReqGetAccessToken response SUCCESS!!"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 72
    invoke-static {p1}, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserAccessToken;->parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAccessToken;

    move-result-object v0

    return-object v0
.end method

.method protected respond(Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;)Z
    .locals 7
    .param p1, "result"    # Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;

    .prologue
    .line 77
    iget v1, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqGetAccessToken;->mReqID:I

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->isSuccess()Z

    move-result v2

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->getHttpstatus()I

    move-result v3

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->getErrorCode()I

    move-result v4

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->getReason()Landroid/os/Bundle;

    move-result-object v5

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->getResponse()Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAccessToken;

    move-object v0, p0

    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqGetAccessToken;->onReqRespond(IZIILandroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAccessToken;)Z

    .line 80
    const/4 v0, 0x1

    return v0
.end method
