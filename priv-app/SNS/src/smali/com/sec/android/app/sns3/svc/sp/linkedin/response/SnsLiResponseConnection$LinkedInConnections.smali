.class interface abstract Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection$LinkedInConnections;
.super Ljava/lang/Object;
.source "SnsLiResponseConnection.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseConnection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x608
    name = "LinkedInConnections"
.end annotation


# static fields
.field public static final COMPANY:Ljava/lang/String; = "company"

.field public static final FIRSTNAME:Ljava/lang/String; = "firstName"

.field public static final HEADLINE:Ljava/lang/String; = "headline"

.field public static final ID:Ljava/lang/String; = "id"

.field public static final INDUSTRY:Ljava/lang/String; = "industry"

.field public static final IS_CURRENT:Ljava/lang/String; = "isCurrent"

.field public static final LASTNAME:Ljava/lang/String; = "lastName"

.field public static final LOCATION:Ljava/lang/String; = "location"

.field public static final LOCATION_COUNTRY:Ljava/lang/String; = "country"

.field public static final LOCATION_COUNTRY_CODE:Ljava/lang/String; = "code"

.field public static final MONTH:Ljava/lang/String; = "month"

.field public static final NAME:Ljava/lang/String; = "name"

.field public static final PICTURE_URL:Ljava/lang/String; = "pictureUrl"

.field public static final POSITION:Ljava/lang/String; = "positions"

.field public static final PROFILE_URL:Ljava/lang/String; = "publicProfileUrl"

.field public static final SIZE:Ljava/lang/String; = "size"

.field public static final START_DATE:Ljava/lang/String; = "startDate"

.field public static final SUMMARY:Ljava/lang/String; = "summary"

.field public static final TITLE:Ljava/lang/String; = "title"

.field public static final TOTAL:Ljava/lang/String; = "_total"

.field public static final TYPE:Ljava/lang/String; = "type"

.field public static final VALUES:Ljava/lang/String; = "values"

.field public static final YEAR:Ljava/lang/String; = "year"
