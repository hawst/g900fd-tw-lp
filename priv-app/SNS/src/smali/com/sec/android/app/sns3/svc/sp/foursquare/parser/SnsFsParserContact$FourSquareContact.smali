.class public interface abstract Lcom/sec/android/app/sns3/svc/sp/foursquare/parser/SnsFsParserContact$FourSquareContact;
.super Ljava/lang/Object;
.source "SnsFsParserContact.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/svc/sp/foursquare/parser/SnsFsParserContact;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "FourSquareContact"
.end annotation


# static fields
.field public static final EMAIL:Ljava/lang/String; = "email"

.field public static final FACEBOOK:Ljava/lang/String; = "facebook"

.field public static final PHONE:Ljava/lang/String; = "phone"

.field public static final TWITTER:Ljava/lang/String; = "twitter"
