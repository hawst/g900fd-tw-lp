.class Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdPostTweet$2;
.super Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestStatuses;
.source "SnsTwCmdPostTweet.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdPostTweet;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Handler;Ljava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdPostTweet;


# direct methods
.method constructor <init>(Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdPostTweet;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;Landroid/os/Bundle;)V
    .locals 0
    .param p2, "x0"    # Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    .param p3, "x1"    # Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;
    .param p4, "x2"    # Landroid/os/Bundle;

    .prologue
    .line 70
    iput-object p1, p0, Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdPostTweet$2;->this$0:Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdPostTweet;

    invoke-direct {p0, p2, p3, p4}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestStatuses;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwStatusesAPI;Landroid/os/Bundle;)V

    return-void
.end method


# virtual methods
.method public onReqRespond(IZIILandroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;)Z
    .locals 3
    .param p1, "reqID"    # I
    .param p2, "bSuccess"    # Z
    .param p3, "httpStatus"    # I
    .param p4, "errorCode"    # I
    .param p5, "reason"    # Landroid/os/Bundle;
    .param p6, "resp"    # Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseStatus;

    .prologue
    .line 76
    if-nez p2, :cond_0

    .line 77
    iget-object v0, p0, Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdPostTweet$2;->this$0:Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdPostTweet;

    new-instance v1, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;

    const-string v2, "twitter"

    invoke-direct {v1, v2, p3, p4, p5}, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;-><init>(Ljava/lang/String;IILandroid/os/Bundle;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdPostTweet;->setResponseList(Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;)Z

    .line 79
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdPostTweet$2;->this$0:Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdPostTweet;

    invoke-virtual {v0, p2}, Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdPostTweet;->setSuccess(Z)V

    .line 80
    iget-object v0, p0, Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdPostTweet$2;->this$0:Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdPostTweet;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdPostTweet;->setUri(Ljava/lang/String;)V

    .line 81
    iget-object v0, p0, Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdPostTweet$2;->this$0:Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdPostTweet;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/sns3/agent/sp/twitter/command/SnsTwCmdPostTweet;->receive(Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;)V

    .line 83
    const/4 v0, 0x0

    return v0
.end method
