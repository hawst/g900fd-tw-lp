.class public Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserNotification;
.super Ljava/lang/Object;
.source "SnsFbParserNotification.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserNotification$FacebookNotification;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    return-void
.end method

.method private parseApp(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAppQueryResult;
    .locals 8
    .param p1, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 378
    const/4 v6, 0x0

    .line 379
    .local v6, "noti":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAppQueryResult;
    const/4 v0, 0x0

    .line 382
    .local v0, "curNoti":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAppQueryResult;
    :try_start_0
    new-instance v3, Lorg/json/JSONArray;

    invoke-direct {v3, p1}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 384
    .local v3, "ja":Lorg/json/JSONArray;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v7

    if-ge v2, v7, :cond_1

    .line 385
    new-instance v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAppQueryResult;

    invoke-direct {v5}, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAppQueryResult;-><init>()V

    .line 387
    .local v5, "newNoti":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAppQueryResult;
    invoke-virtual {v3, v2}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    .line 389
    .local v4, "jsonObj":Lorg/json/JSONObject;
    const-string v7, "app_id"

    invoke-virtual {v4, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAppQueryResult;->mAppId:Ljava/lang/String;

    .line 390
    const-string v7, "api_key"

    invoke-virtual {v4, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAppQueryResult;->mApiKey:Ljava/lang/String;

    .line 391
    const-string v7, "canvas_name"

    invoke-virtual {v4, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAppQueryResult;->mCanvasName:Ljava/lang/String;

    .line 393
    const-string v7, "display_name"

    invoke-virtual {v4, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAppQueryResult;->mDisplayName:Ljava/lang/String;

    .line 395
    const-string v7, "description"

    invoke-virtual {v4, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAppQueryResult;->mDescription:Ljava/lang/String;

    .line 397
    const-string v7, "category"

    invoke-virtual {v4, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAppQueryResult;->mCategory:Ljava/lang/String;

    .line 398
    const-string v7, "icon_url"

    invoke-virtual {v4, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAppQueryResult;->mIconUrl:Ljava/lang/String;

    .line 400
    if-nez v6, :cond_0

    .line 401
    move-object v6, v5

    .line 402
    move-object v0, v6

    .line 384
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 404
    :cond_0
    iput-object v5, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAppQueryResult;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAppQueryResult;

    .line 405
    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAppQueryResult;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAppQueryResult;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 408
    .end local v2    # "i":I
    .end local v3    # "ja":Lorg/json/JSONArray;
    .end local v4    # "jsonObj":Lorg/json/JSONObject;
    .end local v5    # "newNoti":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAppQueryResult;
    :catch_0
    move-exception v1

    .line 410
    .local v1, "e":Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    .line 413
    .end local v1    # "e":Lorg/json/JSONException;
    :cond_1
    return-object v6
.end method

.method private parseFreq(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFreqQueryResult;
    .locals 10
    .param p1, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 241
    const/4 v6, 0x0

    .line 242
    .local v6, "noti":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFreqQueryResult;
    const/4 v0, 0x0

    .line 245
    .local v0, "curNoti":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFreqQueryResult;
    :try_start_0
    new-instance v3, Lorg/json/JSONArray;

    invoke-direct {v3, p1}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 247
    .local v3, "ja":Lorg/json/JSONArray;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v7

    if-ge v2, v7, :cond_1

    .line 248
    new-instance v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFreqQueryResult;

    invoke-direct {v5}, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFreqQueryResult;-><init>()V

    .line 250
    .local v5, "newNoti":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFreqQueryResult;
    invoke-virtual {v3, v2}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    .line 252
    .local v4, "jsonObj":Lorg/json/JSONObject;
    const-string v7, "uid_from"

    invoke-virtual {v4, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFreqQueryResult;->mUserIdFrom:Ljava/lang/String;

    .line 254
    const-string v7, "uid_to"

    invoke-virtual {v4, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFreqQueryResult;->mUserIdTo:Ljava/lang/String;

    .line 256
    const-string v7, "message"

    invoke-virtual {v4, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFreqQueryResult;->mMessage:Ljava/lang/String;

    .line 257
    const-string v7, "time"

    invoke-virtual {v4, v7}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    iput-object v7, v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFreqQueryResult;->mTime:Ljava/lang/Long;

    .line 258
    const-string v7, "unread"

    invoke-virtual {v4, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFreqQueryResult;->mUnread:Ljava/lang/String;

    .line 260
    if-nez v6, :cond_0

    .line 261
    move-object v6, v5

    .line 262
    move-object v0, v6

    .line 247
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 264
    :cond_0
    iput-object v5, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFreqQueryResult;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFreqQueryResult;

    .line 265
    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFreqQueryResult;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFreqQueryResult;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 268
    .end local v2    # "i":I
    .end local v3    # "ja":Lorg/json/JSONArray;
    .end local v4    # "jsonObj":Lorg/json/JSONObject;
    .end local v5    # "newNoti":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFreqQueryResult;
    :catch_0
    move-exception v1

    .line 270
    .local v1, "e":Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    .line 273
    .end local v1    # "e":Lorg/json/JSONException;
    :cond_1
    return-object v6
.end method

.method private parseMsg(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMsgQueryResult;
    .locals 10
    .param p1, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 417
    const/4 v6, 0x0

    .line 418
    .local v6, "noti":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMsgQueryResult;
    const/4 v0, 0x0

    .line 421
    .local v0, "curNoti":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMsgQueryResult;
    :try_start_0
    new-instance v3, Lorg/json/JSONArray;

    invoke-direct {v3, p1}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 423
    .local v3, "ja":Lorg/json/JSONArray;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v7

    if-ge v2, v7, :cond_1

    .line 424
    new-instance v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMsgQueryResult;

    invoke-direct {v5}, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMsgQueryResult;-><init>()V

    .line 426
    .local v5, "newNoti":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMsgQueryResult;
    invoke-virtual {v3, v2}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    .line 428
    .local v4, "jsonObj":Lorg/json/JSONObject;
    const-string v7, "message_id"

    invoke-virtual {v4, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMsgQueryResult;->mMessageId:Ljava/lang/String;

    .line 430
    const-string v7, "thread_id"

    invoke-virtual {v4, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMsgQueryResult;->mThreadId:Ljava/lang/String;

    .line 431
    const-string v7, "author_id"

    invoke-virtual {v4, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMsgQueryResult;->mAuthorId:Ljava/lang/String;

    .line 432
    const-string v7, "body"

    invoke-virtual {v4, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMsgQueryResult;->mBody:Ljava/lang/String;

    .line 433
    const-string v7, "created_time"

    invoke-virtual {v4, v7}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    iput-object v7, v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMsgQueryResult;->mCreatedTime:Ljava/lang/Long;

    .line 435
    const-string v7, "viewer_id"

    invoke-virtual {v4, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMsgQueryResult;->mViwerId:Ljava/lang/String;

    .line 437
    if-nez v6, :cond_0

    .line 438
    move-object v6, v5

    .line 439
    move-object v0, v6

    .line 423
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 441
    :cond_0
    iput-object v5, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMsgQueryResult;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMsgQueryResult;

    .line 442
    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMsgQueryResult;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMsgQueryResult;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 445
    .end local v2    # "i":I
    .end local v3    # "ja":Lorg/json/JSONArray;
    .end local v4    # "jsonObj":Lorg/json/JSONObject;
    .end local v5    # "newNoti":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMsgQueryResult;
    :catch_0
    move-exception v1

    .line 447
    .local v1, "e":Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    .line 450
    .end local v1    # "e":Lorg/json/JSONException;
    :cond_1
    return-object v6
.end method

.method private parseNoti(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNotiQueryResult;
    .locals 10
    .param p1, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 277
    const/4 v6, 0x0

    .line 278
    .local v6, "noti":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNotiQueryResult;
    const/4 v0, 0x0

    .line 281
    .local v0, "curNoti":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNotiQueryResult;
    :try_start_0
    new-instance v3, Lorg/json/JSONArray;

    invoke-direct {v3, p1}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 283
    .local v3, "ja":Lorg/json/JSONArray;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v7

    if-ge v2, v7, :cond_1

    .line 284
    new-instance v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNotiQueryResult;

    invoke-direct {v5}, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNotiQueryResult;-><init>()V

    .line 286
    .local v5, "newNoti":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNotiQueryResult;
    invoke-virtual {v3, v2}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    .line 288
    .local v4, "jsonObj":Lorg/json/JSONObject;
    const-string v7, "notification_id"

    invoke-virtual {v4, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNotiQueryResult;->mNotificationId:Ljava/lang/String;

    .line 290
    const-string v7, "sender_id"

    invoke-virtual {v4, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNotiQueryResult;->mSenderId:Ljava/lang/String;

    .line 292
    const-string v7, "recipient_id"

    invoke-virtual {v4, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNotiQueryResult;->mRecipientID:Ljava/lang/String;

    .line 294
    const-string v7, "object_id"

    invoke-virtual {v4, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNotiQueryResult;->mObjectID:Ljava/lang/String;

    .line 296
    const-string v7, "object_type"

    invoke-virtual {v4, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNotiQueryResult;->mObjectType:Ljava/lang/String;

    .line 298
    const-string v7, "created_time"

    invoke-virtual {v4, v7}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    iput-object v7, v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNotiQueryResult;->mCreatedTime:Ljava/lang/Long;

    .line 300
    const-string v7, "updated_time"

    invoke-virtual {v4, v7}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    iput-object v7, v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNotiQueryResult;->mUpdatedTime:Ljava/lang/Long;

    .line 302
    const-string v7, "title_html"

    invoke-virtual {v4, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNotiQueryResult;->mTitleHtml:Ljava/lang/String;

    .line 304
    const-string v7, "title_text"

    invoke-virtual {v4, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNotiQueryResult;->mTitleText:Ljava/lang/String;

    .line 306
    const-string v7, "body_html"

    invoke-virtual {v4, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNotiQueryResult;->mBodyHtml:Ljava/lang/String;

    .line 308
    const-string v7, "body_text"

    invoke-virtual {v4, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNotiQueryResult;->mBodyText:Ljava/lang/String;

    .line 310
    const-string v7, "href"

    invoke-virtual {v4, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNotiQueryResult;->mHref:Ljava/lang/String;

    .line 311
    const-string v7, "app_id"

    invoke-virtual {v4, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNotiQueryResult;->mAppId:Ljava/lang/String;

    .line 312
    const-string v7, "is_unread"

    invoke-virtual {v4, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNotiQueryResult;->mIsUnread:Ljava/lang/String;

    .line 314
    const-string v7, "is_hidden"

    invoke-virtual {v4, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNotiQueryResult;->mIsHidden:Ljava/lang/String;

    .line 316
    const-string v7, "icon_url"

    invoke-virtual {v4, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNotiQueryResult;->mIconUrl:Ljava/lang/String;

    .line 318
    if-nez v6, :cond_0

    .line 319
    move-object v6, v5

    .line 320
    move-object v0, v6

    .line 283
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0

    .line 322
    :cond_0
    iput-object v5, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNotiQueryResult;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNotiQueryResult;

    .line 323
    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNotiQueryResult;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNotiQueryResult;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 326
    .end local v2    # "i":I
    .end local v3    # "ja":Lorg/json/JSONArray;
    .end local v4    # "jsonObj":Lorg/json/JSONObject;
    .end local v5    # "newNoti":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNotiQueryResult;
    :catch_0
    move-exception v1

    .line 328
    .local v1, "e":Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    .line 331
    .end local v1    # "e":Lorg/json/JSONException;
    :cond_1
    return-object v6
.end method

.method private parseThrd(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseThrdQueryResult;
    .locals 10
    .param p1, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 335
    const/4 v6, 0x0

    .line 336
    .local v6, "noti":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseThrdQueryResult;
    const/4 v0, 0x0

    .line 339
    .local v0, "curNoti":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseThrdQueryResult;
    :try_start_0
    new-instance v3, Lorg/json/JSONArray;

    invoke-direct {v3, p1}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 341
    .local v3, "ja":Lorg/json/JSONArray;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v7

    if-ge v2, v7, :cond_1

    .line 342
    new-instance v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseThrdQueryResult;

    invoke-direct {v5}, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseThrdQueryResult;-><init>()V

    .line 344
    .local v5, "newNoti":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseThrdQueryResult;
    invoke-virtual {v3, v2}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    .line 346
    .local v4, "jsonObj":Lorg/json/JSONObject;
    const-string v7, "thread_id"

    invoke-virtual {v4, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseThrdQueryResult;->mThreadId:Ljava/lang/String;

    .line 348
    const-string v7, "thread_id"

    invoke-virtual {v4, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseThrdQueryResult;->mFolderId:Ljava/lang/String;

    .line 350
    const-string v7, "subject"

    invoke-virtual {v4, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseThrdQueryResult;->mSubject:Ljava/lang/String;

    .line 351
    const-string v7, "recipients"

    invoke-virtual {v4, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseThrdQueryResult;->mRecipients:Ljava/lang/String;

    .line 353
    const-string v7, "update_time"

    invoke-virtual {v4, v7}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    iput-object v7, v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseThrdQueryResult;->mUpdateTime:Ljava/lang/Long;

    .line 355
    const-string v7, "message_count"

    invoke-virtual {v4, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseThrdQueryResult;->mMessageCount:Ljava/lang/String;

    .line 357
    const-string v7, "unread"

    invoke-virtual {v4, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseThrdQueryResult;->mIsUnread:Ljava/lang/String;

    .line 358
    const-string v7, "viewer_id"

    invoke-virtual {v4, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseThrdQueryResult;->mViewerId:Ljava/lang/String;

    .line 361
    if-nez v6, :cond_0

    .line 362
    move-object v6, v5

    .line 363
    move-object v0, v6

    .line 341
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 365
    :cond_0
    iput-object v5, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseThrdQueryResult;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseThrdQueryResult;

    .line 366
    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseThrdQueryResult;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseThrdQueryResult;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 369
    .end local v2    # "i":I
    .end local v3    # "ja":Lorg/json/JSONArray;
    .end local v4    # "jsonObj":Lorg/json/JSONObject;
    .end local v5    # "newNoti":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseThrdQueryResult;
    :catch_0
    move-exception v1

    .line 371
    .local v1, "e":Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    .line 374
    .end local v1    # "e":Lorg/json/JSONException;
    :cond_1
    return-object v6
.end method

.method private parseUser(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseUserQueryResult;
    .locals 8
    .param p1, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 454
    const/4 v6, 0x0

    .line 455
    .local v6, "noti":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseUserQueryResult;
    const/4 v0, 0x0

    .line 458
    .local v0, "curNoti":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseUserQueryResult;
    :try_start_0
    new-instance v3, Lorg/json/JSONArray;

    invoke-direct {v3, p1}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 460
    .local v3, "ja":Lorg/json/JSONArray;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v7

    if-ge v2, v7, :cond_1

    .line 461
    new-instance v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseUserQueryResult;

    invoke-direct {v5}, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseUserQueryResult;-><init>()V

    .line 463
    .local v5, "newNoti":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseUserQueryResult;
    invoke-virtual {v3, v2}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    .line 465
    .local v4, "jsonObj":Lorg/json/JSONObject;
    const-string v7, "uid"

    invoke-virtual {v4, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseUserQueryResult;->mUserId:Ljava/lang/String;

    .line 466
    const-string v7, "name"

    invoke-virtual {v4, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseUserQueryResult;->mName:Ljava/lang/String;

    .line 467
    const-string v7, "pic"

    invoke-virtual {v4, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseUserQueryResult;->mPicSquare:Ljava/lang/String;

    .line 469
    if-nez v6, :cond_0

    .line 470
    move-object v6, v5

    .line 471
    move-object v0, v6

    .line 460
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 473
    :cond_0
    iput-object v5, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseUserQueryResult;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseUserQueryResult;

    .line 474
    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseUserQueryResult;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseUserQueryResult;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 477
    .end local v2    # "i":I
    .end local v3    # "ja":Lorg/json/JSONArray;
    .end local v4    # "jsonObj":Lorg/json/JSONObject;
    .end local v5    # "newNoti":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseUserQueryResult;
    :catch_0
    move-exception v1

    .line 479
    .local v1, "e":Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    .line 482
    .end local v1    # "e":Lorg/json/JSONException;
    :cond_1
    return-object v6
.end method


# virtual methods
.method public parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNotification;
    .locals 9
    .param p1, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 194
    const/4 v3, 0x0

    .line 197
    .local v3, "noti":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNotification;
    :try_start_0
    new-instance v4, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNotification;

    invoke-direct {v4}, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNotification;-><init>()V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1

    .line 198
    .end local v3    # "noti":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNotification;
    .local v4, "noti":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNotification;
    :try_start_1
    new-instance v2, Lorg/json/JSONArray;

    invoke-direct {v2, p1}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 200
    .local v2, "ja":Lorg/json/JSONArray;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v7

    if-ge v1, v7, :cond_6

    .line 202
    invoke-virtual {v2, v1}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v7

    const-string v8, "name"

    invoke-virtual {v7, v8}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 203
    .local v5, "queryName":Ljava/lang/String;
    invoke-virtual {v2, v1}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v7

    const-string v8, "fql_result_set"

    invoke-virtual {v7, v8}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 206
    .local v6, "queryObject":Ljava/lang/String;
    const-string v7, "freq_query"

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 208
    invoke-direct {p0, v6}, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserNotification;->parseFreq(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFreqQueryResult;

    move-result-object v7

    iput-object v7, v4, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNotification;->mFreq:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFreqQueryResult;

    .line 200
    :cond_0
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 210
    :cond_1
    const-string v7, "noti_query"

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 212
    invoke-direct {p0, v6}, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserNotification;->parseNoti(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNotiQueryResult;

    move-result-object v7

    iput-object v7, v4, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNotification;->mNoti:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNotiQueryResult;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 232
    .end local v1    # "i":I
    .end local v2    # "ja":Lorg/json/JSONArray;
    .end local v5    # "queryName":Ljava/lang/String;
    .end local v6    # "queryObject":Ljava/lang/String;
    :catch_0
    move-exception v0

    move-object v3, v4

    .line 234
    .end local v4    # "noti":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNotification;
    .local v0, "e":Lorg/json/JSONException;
    .restart local v3    # "noti":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNotification;
    :goto_2
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    .line 237
    .end local v0    # "e":Lorg/json/JSONException;
    :goto_3
    return-object v3

    .line 214
    .end local v3    # "noti":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNotification;
    .restart local v1    # "i":I
    .restart local v2    # "ja":Lorg/json/JSONArray;
    .restart local v4    # "noti":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNotification;
    .restart local v5    # "queryName":Ljava/lang/String;
    .restart local v6    # "queryObject":Ljava/lang/String;
    :cond_2
    :try_start_2
    const-string v7, "thrd_query"

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 216
    invoke-direct {p0, v6}, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserNotification;->parseThrd(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseThrdQueryResult;

    move-result-object v7

    iput-object v7, v4, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNotification;->mThrd:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseThrdQueryResult;

    goto :goto_1

    .line 218
    :cond_3
    const-string v7, "app_query"

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 220
    invoke-direct {p0, v6}, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserNotification;->parseApp(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAppQueryResult;

    move-result-object v7

    iput-object v7, v4, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNotification;->mApp:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAppQueryResult;

    goto :goto_1

    .line 222
    :cond_4
    const-string v7, "msg_query"

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 224
    invoke-direct {p0, v6}, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserNotification;->parseMsg(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMsgQueryResult;

    move-result-object v7

    iput-object v7, v4, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNotification;->mMsg:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMsgQueryResult;

    goto :goto_1

    .line 226
    :cond_5
    const-string v7, "user_query"

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 228
    invoke-direct {p0, v6}, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserNotification;->parseUser(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseUserQueryResult;

    move-result-object v7

    iput-object v7, v4, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNotification;->mUser:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseUserQueryResult;
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_1

    .end local v5    # "queryName":Ljava/lang/String;
    .end local v6    # "queryObject":Ljava/lang/String;
    :cond_6
    move-object v3, v4

    .line 235
    .end local v4    # "noti":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNotification;
    .restart local v3    # "noti":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNotification;
    goto :goto_3

    .line 232
    .end local v1    # "i":I
    .end local v2    # "ja":Lorg/json/JSONArray;
    :catch_1
    move-exception v0

    goto :goto_2
.end method
