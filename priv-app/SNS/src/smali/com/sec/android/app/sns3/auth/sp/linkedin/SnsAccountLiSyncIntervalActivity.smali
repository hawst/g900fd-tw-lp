.class public Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;
.super Landroid/preference/PreferenceActivity;
.source "SnsAccountLiSyncIntervalActivity.java"


# static fields
.field private static final DEFAULT_SYNC_INTERVAL:J = 0x0L

.field private static final SYNC_INTERVAL_KEY:Ljava/lang/String; = "sync_interval"

.field private static final TAG:Ljava/lang/String; = "SnsAccountLiAuth"


# instance fields
.field private mAppContext:Landroid/content/Context;

.field private mContactIntervalPreference:Landroid/preference/ListPreference;

.field private mContactSharedPref:Landroid/content/SharedPreferences;

.field private mLinkedinAppIntervalPreference:Landroid/preference/ListPreference;

.field private mLinkedinAppSharedPref:Landroid/content/SharedPreferences;

.field private mProfileFeedIntervalPreference:Landroid/preference/ListPreference;

.field private mProfileFeedSharedPref:Landroid/content/SharedPreferences;

.field private mProfilesIntervalPreference:Landroid/preference/ListPreference;

.field private mProfilesSharedPref:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 47
    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    .line 57
    iput-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->mContactIntervalPreference:Landroid/preference/ListPreference;

    .line 58
    iput-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->mLinkedinAppIntervalPreference:Landroid/preference/ListPreference;

    .line 59
    iput-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->mProfileFeedIntervalPreference:Landroid/preference/ListPreference;

    .line 60
    iput-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->mProfilesIntervalPreference:Landroid/preference/ListPreference;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;)Landroid/preference/ListPreference;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->mContactIntervalPreference:Landroid/preference/ListPreference;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;)Landroid/preference/ListPreference;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->mLinkedinAppIntervalPreference:Landroid/preference/ListPreference;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;)Landroid/preference/ListPreference;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->mProfilesIntervalPreference:Landroid/preference/ListPreference;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;)Landroid/preference/ListPreference;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->mProfileFeedIntervalPreference:Landroid/preference/ListPreference;

    return-object v0
.end method


# virtual methods
.method isTable()Z
    .locals 2

    .prologue
    .line 468
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit8 v0, v0, 0x3

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    .line 471
    :cond_0
    const/4 v0, 0x1

    .line 473
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 12
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 70
    iput-object p0, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->mAppContext:Landroid/content/Context;

    .line 72
    iget-object v8, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->mAppContext:Landroid/content/Context;

    invoke-static {v8}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->isLightTheme(Landroid/content/Context;)Z

    move-result v8

    if-eqz v8, :cond_14

    const v8, 0x103012b

    :goto_0
    invoke-virtual {p0, v8}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->setTheme(I)V

    .line 75
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 77
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 78
    .local v0, "actionBar":Landroid/app/ActionBar;
    const/4 v8, 0x1

    invoke-virtual {v0, v8}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 79
    const/4 v8, 0x0

    invoke-virtual {v0, v8}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 81
    const v8, 0x7f030005

    invoke-virtual {p0, v8}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->setContentView(I)V

    .line 82
    const v8, 0x7f080046

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    const v11, 0x7f08002e

    invoke-virtual {p0, v11}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->getString(I)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-virtual {p0, v8, v9}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v8}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 83
    const v8, 0x7f04000d

    invoke-virtual {p0, v8}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->addPreferencesFromResource(I)V

    .line 85
    const-string v8, "sync_interval"

    invoke-virtual {p0, v8}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v3

    check-cast v3, Landroid/preference/PreferenceScreen;

    .line 87
    .local v3, "preferenceScreen":Landroid/preference/PreferenceScreen;
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v8

    const-string v9, "com.linkedin.android"

    invoke-virtual {v8, v9}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v1

    .line 90
    .local v1, "appAccount":[Landroid/accounts/Account;
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v8

    const-string v9, "com.sec.android.app.sns3.linkedin"

    invoke-virtual {v8, v9}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v7

    .line 93
    .local v7, "sns3Account":[Landroid/accounts/Account;
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->getIntent()Landroid/content/Intent;

    move-result-object v8

    const-string v9, "skip_sync_marketapp"

    const/4 v10, 0x1

    invoke-virtual {v8, v9, v10}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v6

    .line 95
    .local v6, "skipMarketApp":Z
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/app/sns3/SnsApplication;->getFeatureMgr()Lcom/sec/android/app/sns3/SnsFeatureManager;

    move-result-object v2

    .line 96
    .local v2, "featureMgr":Lcom/sec/android/app/sns3/SnsFeatureManager;
    const/4 v5, 0x0

    .line 97
    .local v5, "providerLabel":Ljava/lang/CharSequence;
    const/4 v4, 0x0

    .line 100
    .local v4, "providerInfo":Landroid/content/pm/ProviderInfo;
    if-nez v6, :cond_4

    .line 101
    array-length v8, v1

    if-lez v8, :cond_4

    .line 102
    const/4 v5, 0x0

    .line 103
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v8

    const-string v9, "com.android.contacts"

    const/4 v10, 0x0

    invoke-virtual {v8, v9, v10}, Landroid/content/pm/PackageManager;->resolveContentProvider(Ljava/lang/String;I)Landroid/content/pm/ProviderInfo;

    move-result-object v4

    .line 104
    if-eqz v4, :cond_0

    .line 105
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v8

    invoke-virtual {v4, v8}, Landroid/content/pm/ProviderInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v5

    .line 107
    :cond_0
    if-eqz v5, :cond_1

    invoke-interface {v5}, Ljava/lang/CharSequence;->length()I

    move-result v8

    if-nez v8, :cond_2

    .line 108
    :cond_1
    const-string v8, "SnsAccountLiAuth"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "providerLabel is null - "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 109
    const-string v5, "Contacts"

    .line 112
    :cond_2
    new-instance v8, Landroid/preference/ListPreference;

    invoke-direct {v8, p0}, Landroid/preference/ListPreference;-><init>(Landroid/content/Context;)V

    iput-object v8, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->mContactIntervalPreference:Landroid/preference/ListPreference;

    .line 113
    iget-object v8, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->mContactIntervalPreference:Landroid/preference/ListPreference;

    const v9, 0x7f080009

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    aput-object v5, v10, v11

    invoke-virtual {p0, v9, v10}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/preference/ListPreference;->setTitle(Ljava/lang/CharSequence;)V

    .line 115
    iget-object v8, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->mContactIntervalPreference:Landroid/preference/ListPreference;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/preference/ListPreference;->setPersistent(Z)V

    .line 116
    iget-object v8, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->mContactIntervalPreference:Landroid/preference/ListPreference;

    const/high16 v9, 0x7f050000

    invoke-virtual {v8, v9}, Landroid/preference/ListPreference;->setEntries(I)V

    .line 117
    iget-object v8, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->mContactIntervalPreference:Landroid/preference/ListPreference;

    const v9, 0x7f050001

    invoke-virtual {v8, v9}, Landroid/preference/ListPreference;->setEntryValues(I)V

    .line 118
    sget-object v8, Lcom/sec/android/app/sns3/SnsFeatureManager;->ANGLE_INDICATION_SUPPORTED:Ljava/lang/Boolean;

    invoke-virtual {v8}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v8

    if-eqz v8, :cond_3

    .line 119
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v8

    invoke-static {v8}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->isLightTheme(Landroid/content/Context;)Z

    move-result v8

    if-eqz v8, :cond_15

    .line 120
    iget-object v8, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->mContactIntervalPreference:Landroid/preference/ListPreference;

    const v9, 0x7f030003

    invoke-virtual {v8, v9}, Landroid/preference/ListPreference;->setWidgetLayoutResource(I)V

    .line 125
    :cond_3
    :goto_1
    iget-object v8, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->mContactIntervalPreference:Landroid/preference/ListPreference;

    const v9, 0x7f080009

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    aput-object v5, v10, v11

    invoke-virtual {p0, v9, v10}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/preference/ListPreference;->setDialogTitle(Ljava/lang/CharSequence;)V

    .line 128
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->readContactSyncInterval()V

    .line 130
    iget-object v8, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->mContactIntervalPreference:Landroid/preference/ListPreference;

    new-instance v9, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity$1;

    invoke-direct {v9, p0, v1}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity$1;-><init>(Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;[Landroid/accounts/Account;)V

    invoke-virtual {v8, v9}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 165
    iget-object v8, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->mContactIntervalPreference:Landroid/preference/ListPreference;

    invoke-virtual {v3, v8}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 170
    :cond_4
    if-nez v6, :cond_9

    .line 171
    array-length v8, v1

    if-lez v8, :cond_9

    .line 172
    const/4 v5, 0x0

    .line 173
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v8

    const-string v9, "com.linkedin.android"

    const/4 v10, 0x0

    invoke-virtual {v8, v9, v10}, Landroid/content/pm/PackageManager;->resolveContentProvider(Ljava/lang/String;I)Landroid/content/pm/ProviderInfo;

    move-result-object v4

    .line 174
    if-eqz v4, :cond_5

    .line 175
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v8

    invoke-virtual {v4, v8}, Landroid/content/pm/ProviderInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v5

    .line 177
    :cond_5
    if-eqz v5, :cond_6

    invoke-interface {v5}, Ljava/lang/CharSequence;->length()I

    move-result v8

    if-nez v8, :cond_7

    .line 178
    :cond_6
    const-string v8, "SnsAccountLiAuth"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "providerLabel is null - "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 179
    const-string v5, "Linkedin data"

    .line 182
    :cond_7
    new-instance v8, Landroid/preference/ListPreference;

    invoke-direct {v8, p0}, Landroid/preference/ListPreference;-><init>(Landroid/content/Context;)V

    iput-object v8, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->mLinkedinAppIntervalPreference:Landroid/preference/ListPreference;

    .line 183
    iget-object v8, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->mLinkedinAppIntervalPreference:Landroid/preference/ListPreference;

    const v9, 0x7f080009

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    aput-object v5, v10, v11

    invoke-virtual {p0, v9, v10}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/preference/ListPreference;->setTitle(Ljava/lang/CharSequence;)V

    .line 185
    iget-object v8, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->mLinkedinAppIntervalPreference:Landroid/preference/ListPreference;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/preference/ListPreference;->setPersistent(Z)V

    .line 186
    iget-object v8, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->mLinkedinAppIntervalPreference:Landroid/preference/ListPreference;

    const/high16 v9, 0x7f050000

    invoke-virtual {v8, v9}, Landroid/preference/ListPreference;->setEntries(I)V

    .line 187
    iget-object v8, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->mLinkedinAppIntervalPreference:Landroid/preference/ListPreference;

    const v9, 0x7f050001

    invoke-virtual {v8, v9}, Landroid/preference/ListPreference;->setEntryValues(I)V

    .line 188
    sget-object v8, Lcom/sec/android/app/sns3/SnsFeatureManager;->ANGLE_INDICATION_SUPPORTED:Ljava/lang/Boolean;

    invoke-virtual {v8}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v8

    if-eqz v8, :cond_8

    .line 189
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v8

    invoke-static {v8}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->isLightTheme(Landroid/content/Context;)Z

    move-result v8

    if-eqz v8, :cond_16

    .line 190
    iget-object v8, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->mLinkedinAppIntervalPreference:Landroid/preference/ListPreference;

    const v9, 0x7f030003

    invoke-virtual {v8, v9}, Landroid/preference/ListPreference;->setWidgetLayoutResource(I)V

    .line 195
    :cond_8
    :goto_2
    iget-object v8, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->mLinkedinAppIntervalPreference:Landroid/preference/ListPreference;

    const v9, 0x7f080009

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    aput-object v5, v10, v11

    invoke-virtual {p0, v9, v10}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/preference/ListPreference;->setDialogTitle(Ljava/lang/CharSequence;)V

    .line 198
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->readLinkedinAppSyncInterval()V

    .line 200
    iget-object v8, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->mLinkedinAppIntervalPreference:Landroid/preference/ListPreference;

    new-instance v9, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity$2;

    invoke-direct {v9, p0, v1}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity$2;-><init>(Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;[Landroid/accounts/Account;)V

    invoke-virtual {v8, v9}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 235
    iget-object v8, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->mLinkedinAppIntervalPreference:Landroid/preference/ListPreference;

    invoke-virtual {v3, v8}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 240
    :cond_9
    invoke-virtual {v2}, Lcom/sec/android/app/sns3/SnsFeatureManager;->isSyncAdapterForProfilesNeeded()Z

    move-result v8

    if-eqz v8, :cond_e

    .line 241
    const/4 v5, 0x0

    .line 242
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v8

    const-string v9, "com.sec.android.app.sns3.profiles"

    const/4 v10, 0x0

    invoke-virtual {v8, v9, v10}, Landroid/content/pm/PackageManager;->resolveContentProvider(Ljava/lang/String;I)Landroid/content/pm/ProviderInfo;

    move-result-object v4

    .line 243
    if-eqz v4, :cond_a

    .line 244
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v8

    invoke-virtual {v4, v8}, Landroid/content/pm/ProviderInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v5

    .line 246
    :cond_a
    if-eqz v5, :cond_b

    invoke-interface {v5}, Ljava/lang/CharSequence;->length()I

    move-result v8

    if-nez v8, :cond_c

    .line 247
    :cond_b
    const-string v8, "SnsAccountLiAuth"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "providerLabel is null - "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 248
    const-string v5, "Profiles"

    .line 251
    :cond_c
    new-instance v8, Landroid/preference/ListPreference;

    invoke-direct {v8, p0}, Landroid/preference/ListPreference;-><init>(Landroid/content/Context;)V

    iput-object v8, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->mProfilesIntervalPreference:Landroid/preference/ListPreference;

    .line 252
    iget-object v8, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->mProfilesIntervalPreference:Landroid/preference/ListPreference;

    const v9, 0x7f080009

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    aput-object v5, v10, v11

    invoke-virtual {p0, v9, v10}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/preference/ListPreference;->setTitle(Ljava/lang/CharSequence;)V

    .line 254
    iget-object v8, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->mProfilesIntervalPreference:Landroid/preference/ListPreference;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/preference/ListPreference;->setPersistent(Z)V

    .line 255
    iget-object v8, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->mProfilesIntervalPreference:Landroid/preference/ListPreference;

    const/high16 v9, 0x7f050000

    invoke-virtual {v8, v9}, Landroid/preference/ListPreference;->setEntries(I)V

    .line 256
    iget-object v8, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->mProfilesIntervalPreference:Landroid/preference/ListPreference;

    const v9, 0x7f050001

    invoke-virtual {v8, v9}, Landroid/preference/ListPreference;->setEntryValues(I)V

    .line 257
    sget-object v8, Lcom/sec/android/app/sns3/SnsFeatureManager;->ANGLE_INDICATION_SUPPORTED:Ljava/lang/Boolean;

    invoke-virtual {v8}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v8

    if-eqz v8, :cond_d

    .line 258
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v8

    invoke-static {v8}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->isLightTheme(Landroid/content/Context;)Z

    move-result v8

    if-eqz v8, :cond_17

    .line 259
    iget-object v8, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->mProfilesIntervalPreference:Landroid/preference/ListPreference;

    const v9, 0x7f030003

    invoke-virtual {v8, v9}, Landroid/preference/ListPreference;->setWidgetLayoutResource(I)V

    .line 264
    :cond_d
    :goto_3
    iget-object v8, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->mProfilesIntervalPreference:Landroid/preference/ListPreference;

    const v9, 0x7f080009

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    aput-object v5, v10, v11

    invoke-virtual {p0, v9, v10}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/preference/ListPreference;->setDialogTitle(Ljava/lang/CharSequence;)V

    .line 267
    array-length v8, v7

    if-lez v8, :cond_18

    .line 268
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->readProfilesSyncInterval()V

    .line 270
    iget-object v8, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->mProfilesIntervalPreference:Landroid/preference/ListPreference;

    new-instance v9, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity$3;

    invoke-direct {v9, p0, v7}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity$3;-><init>(Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;[Landroid/accounts/Account;)V

    invoke-virtual {v8, v9}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 304
    :goto_4
    iget-object v8, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->mProfilesIntervalPreference:Landroid/preference/ListPreference;

    invoke-virtual {v3, v8}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 309
    :cond_e
    invoke-virtual {v2}, Lcom/sec/android/app/sns3/SnsFeatureManager;->isSyncAdapterForProfileFeedsNeeded()Z

    move-result v8

    if-eqz v8, :cond_13

    .line 310
    const/4 v5, 0x0

    .line 311
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v8

    const-string v9, "com.sec.android.app.sns3.life"

    const/4 v10, 0x0

    invoke-virtual {v8, v9, v10}, Landroid/content/pm/PackageManager;->resolveContentProvider(Ljava/lang/String;I)Landroid/content/pm/ProviderInfo;

    move-result-object v4

    .line 312
    if-eqz v4, :cond_f

    .line 313
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v8

    invoke-virtual {v4, v8}, Landroid/content/pm/ProviderInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v5

    .line 315
    :cond_f
    if-eqz v5, :cond_10

    invoke-interface {v5}, Ljava/lang/CharSequence;->length()I

    move-result v8

    if-nez v8, :cond_11

    .line 316
    :cond_10
    const-string v8, "SnsAccountLiAuth"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "providerLabel is null - "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 317
    const-string v5, "Life Times"

    .line 320
    :cond_11
    new-instance v8, Landroid/preference/ListPreference;

    invoke-direct {v8, p0}, Landroid/preference/ListPreference;-><init>(Landroid/content/Context;)V

    iput-object v8, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->mProfileFeedIntervalPreference:Landroid/preference/ListPreference;

    .line 321
    iget-object v8, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->mProfileFeedIntervalPreference:Landroid/preference/ListPreference;

    const v9, 0x7f080009

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    aput-object v5, v10, v11

    invoke-virtual {p0, v9, v10}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/preference/ListPreference;->setTitle(Ljava/lang/CharSequence;)V

    .line 323
    iget-object v8, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->mProfileFeedIntervalPreference:Landroid/preference/ListPreference;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/preference/ListPreference;->setPersistent(Z)V

    .line 324
    iget-object v8, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->mProfileFeedIntervalPreference:Landroid/preference/ListPreference;

    const/high16 v9, 0x7f050000

    invoke-virtual {v8, v9}, Landroid/preference/ListPreference;->setEntries(I)V

    .line 325
    iget-object v8, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->mProfileFeedIntervalPreference:Landroid/preference/ListPreference;

    const v9, 0x7f050001

    invoke-virtual {v8, v9}, Landroid/preference/ListPreference;->setEntryValues(I)V

    .line 326
    sget-object v8, Lcom/sec/android/app/sns3/SnsFeatureManager;->ANGLE_INDICATION_SUPPORTED:Ljava/lang/Boolean;

    invoke-virtual {v8}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v8

    if-eqz v8, :cond_12

    .line 327
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v8

    invoke-static {v8}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->isLightTheme(Landroid/content/Context;)Z

    move-result v8

    if-eqz v8, :cond_19

    .line 328
    iget-object v8, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->mProfileFeedIntervalPreference:Landroid/preference/ListPreference;

    const v9, 0x7f030003

    invoke-virtual {v8, v9}, Landroid/preference/ListPreference;->setWidgetLayoutResource(I)V

    .line 333
    :cond_12
    :goto_5
    iget-object v8, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->mProfileFeedIntervalPreference:Landroid/preference/ListPreference;

    const v9, 0x7f080009

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    aput-object v5, v10, v11

    invoke-virtual {p0, v9, v10}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/preference/ListPreference;->setDialogTitle(Ljava/lang/CharSequence;)V

    .line 336
    array-length v8, v7

    if-lez v8, :cond_1a

    .line 337
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->readProfileFeedSyncInterval()V

    .line 339
    iget-object v8, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->mProfileFeedIntervalPreference:Landroid/preference/ListPreference;

    new-instance v9, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity$4;

    invoke-direct {v9, p0, v7}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity$4;-><init>(Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;[Landroid/accounts/Account;)V

    invoke-virtual {v8, v9}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 375
    :goto_6
    iget-object v8, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->mProfileFeedIntervalPreference:Landroid/preference/ListPreference;

    invoke-virtual {v3, v8}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 377
    :cond_13
    return-void

    .line 72
    .end local v0    # "actionBar":Landroid/app/ActionBar;
    .end local v1    # "appAccount":[Landroid/accounts/Account;
    .end local v2    # "featureMgr":Lcom/sec/android/app/sns3/SnsFeatureManager;
    .end local v3    # "preferenceScreen":Landroid/preference/PreferenceScreen;
    .end local v4    # "providerInfo":Landroid/content/pm/ProviderInfo;
    .end local v5    # "providerLabel":Ljava/lang/CharSequence;
    .end local v6    # "skipMarketApp":Z
    .end local v7    # "sns3Account":[Landroid/accounts/Account;
    :cond_14
    const v8, 0x1030128

    goto/16 :goto_0

    .line 122
    .restart local v0    # "actionBar":Landroid/app/ActionBar;
    .restart local v1    # "appAccount":[Landroid/accounts/Account;
    .restart local v2    # "featureMgr":Lcom/sec/android/app/sns3/SnsFeatureManager;
    .restart local v3    # "preferenceScreen":Landroid/preference/PreferenceScreen;
    .restart local v4    # "providerInfo":Landroid/content/pm/ProviderInfo;
    .restart local v5    # "providerLabel":Ljava/lang/CharSequence;
    .restart local v6    # "skipMarketApp":Z
    .restart local v7    # "sns3Account":[Landroid/accounts/Account;
    :cond_15
    iget-object v8, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->mContactIntervalPreference:Landroid/preference/ListPreference;

    const v9, 0x7f030002

    invoke-virtual {v8, v9}, Landroid/preference/ListPreference;->setWidgetLayoutResource(I)V

    goto/16 :goto_1

    .line 192
    :cond_16
    iget-object v8, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->mLinkedinAppIntervalPreference:Landroid/preference/ListPreference;

    const v9, 0x7f030002

    invoke-virtual {v8, v9}, Landroid/preference/ListPreference;->setWidgetLayoutResource(I)V

    goto/16 :goto_2

    .line 261
    :cond_17
    iget-object v8, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->mProfilesIntervalPreference:Landroid/preference/ListPreference;

    const v9, 0x7f030002

    invoke-virtual {v8, v9}, Landroid/preference/ListPreference;->setWidgetLayoutResource(I)V

    goto/16 :goto_3

    .line 301
    :cond_18
    iget-object v8, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->mProfilesIntervalPreference:Landroid/preference/ListPreference;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/preference/ListPreference;->setEnabled(Z)V

    goto/16 :goto_4

    .line 330
    :cond_19
    iget-object v8, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->mProfileFeedIntervalPreference:Landroid/preference/ListPreference;

    const v9, 0x7f030002

    invoke-virtual {v8, v9}, Landroid/preference/ListPreference;->setWidgetLayoutResource(I)V

    goto :goto_5

    .line 372
    :cond_1a
    iget-object v8, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->mProfileFeedIntervalPreference:Landroid/preference/ListPreference;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/preference/ListPreference;->setEnabled(Z)V

    goto :goto_6
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 382
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 387
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 384
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->finish()V

    .line 385
    const/4 v0, 0x1

    goto :goto_0

    .line 382
    nop

    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 392
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onResume()V

    .line 393
    return-void
.end method

.method public readContactSyncInterval()V
    .locals 6

    .prologue
    .line 396
    const-string v1, "LI_contact_sync_interval"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->mContactSharedPref:Landroid/content/SharedPreferences;

    .line 398
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->mContactSharedPref:Landroid/content/SharedPreferences;

    const-string v2, "LI_contact_sync_interval"

    const-wide/16 v4, 0x0

    invoke-interface {v1, v2, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    .line 401
    .local v0, "savedGalleryInterval":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->mContactIntervalPreference:Landroid/preference/ListPreference;

    invoke-virtual {v1, v0}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 403
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->mContactIntervalPreference:Landroid/preference/ListPreference;

    invoke-virtual {v1}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 404
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->mContactIntervalPreference:Landroid/preference/ListPreference;

    iget-object v2, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->mContactIntervalPreference:Landroid/preference/ListPreference;

    invoke-virtual {v2}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 405
    :cond_0
    return-void
.end method

.method public readLinkedinAppSyncInterval()V
    .locals 6

    .prologue
    .line 414
    const-string v1, "LI_linkedin_app_sync_interval"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->mLinkedinAppSharedPref:Landroid/content/SharedPreferences;

    .line 416
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->mLinkedinAppSharedPref:Landroid/content/SharedPreferences;

    const-string v2, "LI_linkedin_app_sync_interval"

    const-wide/16 v4, 0x0

    invoke-interface {v1, v2, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    .line 419
    .local v0, "savedLinkedinAppInterval":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->mLinkedinAppIntervalPreference:Landroid/preference/ListPreference;

    invoke-virtual {v1, v0}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 421
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->mLinkedinAppIntervalPreference:Landroid/preference/ListPreference;

    invoke-virtual {v1}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 422
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->mLinkedinAppIntervalPreference:Landroid/preference/ListPreference;

    iget-object v2, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->mLinkedinAppIntervalPreference:Landroid/preference/ListPreference;

    invoke-virtual {v2}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 423
    :cond_0
    return-void
.end method

.method public readProfileFeedSyncInterval()V
    .locals 6

    .prologue
    .line 450
    const-string v1, "LI_profile_feed_sync_interval"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->mProfileFeedSharedPref:Landroid/content/SharedPreferences;

    .line 452
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->mProfileFeedSharedPref:Landroid/content/SharedPreferences;

    const-string v2, "LI_profile_feed_sync_interval"

    const-wide/16 v4, 0x0

    invoke-interface {v1, v2, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    .line 455
    .local v0, "savedProfileFeedInterval":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->mProfileFeedIntervalPreference:Landroid/preference/ListPreference;

    invoke-virtual {v1, v0}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 457
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->mProfileFeedIntervalPreference:Landroid/preference/ListPreference;

    invoke-virtual {v1}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 458
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->mProfileFeedIntervalPreference:Landroid/preference/ListPreference;

    iget-object v2, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->mProfileFeedIntervalPreference:Landroid/preference/ListPreference;

    invoke-virtual {v2}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 459
    :cond_0
    return-void
.end method

.method public readProfilesSyncInterval()V
    .locals 6

    .prologue
    .line 432
    const-string v1, "LI_profiles_sync_interval"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->mProfilesSharedPref:Landroid/content/SharedPreferences;

    .line 434
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->mProfilesSharedPref:Landroid/content/SharedPreferences;

    const-string v2, "LI_profiles_sync_interval"

    const-wide/16 v4, 0x0

    invoke-interface {v1, v2, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    .line 437
    .local v0, "savedProfilesInterval":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->mProfilesIntervalPreference:Landroid/preference/ListPreference;

    invoke-virtual {v1, v0}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 439
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->mProfilesIntervalPreference:Landroid/preference/ListPreference;

    invoke-virtual {v1}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 440
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->mProfilesIntervalPreference:Landroid/preference/ListPreference;

    iget-object v2, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->mProfilesIntervalPreference:Landroid/preference/ListPreference;

    invoke-virtual {v2}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 441
    :cond_0
    return-void
.end method

.method public writeContactInterval(Ljava/lang/Long;)V
    .locals 4
    .param p1, "value"    # Ljava/lang/Long;

    .prologue
    .line 408
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->mContactSharedPref:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 409
    .local v0, "contactEditor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "LI_contact_sync_interval"

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 410
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 411
    return-void
.end method

.method public writeLinkedinAppInterval(Ljava/lang/Long;)V
    .locals 4
    .param p1, "value"    # Ljava/lang/Long;

    .prologue
    .line 426
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->mLinkedinAppSharedPref:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 427
    .local v0, "linkedinEditor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "LI_linkedin_app_sync_interval"

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 428
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 429
    return-void
.end method

.method public writeProfileFeedInterval(Ljava/lang/Long;)V
    .locals 4
    .param p1, "value"    # Ljava/lang/Long;

    .prologue
    .line 462
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->mProfileFeedSharedPref:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 463
    .local v0, "profileFeedEditor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "LI_profile_feed_sync_interval"

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 464
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 465
    return-void
.end method

.method public writeProfilesInterval(Ljava/lang/Long;)V
    .locals 4
    .param p1, "value"    # Ljava/lang/Long;

    .prologue
    .line 444
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/linkedin/SnsAccountLiSyncIntervalActivity;->mProfilesSharedPref:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 445
    .local v0, "profilesEditor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "LI_profiles_sync_interval"

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 446
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 447
    return-void
.end method
