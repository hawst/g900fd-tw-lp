.class public Lcom/sec/android/app/sns3/agent/db/SnsDBHelperBase;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "SnsDBHelperBase.java"


# instance fields
.field protected final SNS_DB_BIRTHDAY_TYPE:Ljava/lang/String;

.field protected final SNS_DB_BOOLEAN_TYPE:Ljava/lang/String;

.field protected final SNS_DB_CONTENT_TYPE:Ljava/lang/String;

.field protected final SNS_DB_EMAIL_TYPE:Ljava/lang/String;

.field protected final SNS_DB_ID_TYPE:Ljava/lang/String;

.field protected final SNS_DB_INTEGER_TYPE:Ljava/lang/String;

.field protected final SNS_DB_LOC_TYPE:Ljava/lang/String;

.field protected final SNS_DB_NAME_TYPE:Ljava/lang/String;

.field protected final SNS_DB_PHONENUMBER_TYPE:Ljava/lang/String;

.field protected final SNS_DB_PHONETYPE_TYPE:Ljava/lang/String;

.field protected final SNS_DB_PRIMARY_KEY_TYPE:Ljava/lang/String;

.field protected final SNS_DB_SHORT_STRING_TYPE:Ljava/lang/String;

.field protected final SNS_DB_SP_TYPE:Ljava/lang/String;

.field protected final SNS_DB_TEXT_TYPE:Ljava/lang/String;

.field protected final SNS_DB_TIME_TYPE:Ljava/lang/String;

.field protected final SNS_DB_TITLE_TYPE:Ljava/lang/String;

.field protected final SNS_DB_URL_FILEPATH:Ljava/lang/String;

.field protected final SNS_DB_URL_TYPE:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "databaseName"    # Ljava/lang/String;
    .param p3, "object"    # Landroid/database/sqlite/SQLiteDatabase$CursorFactory;
    .param p4, "databaseVersion"    # I

    .prologue
    .line 66
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 28
    const-string v0, "INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL"

    iput-object v0, p0, Lcom/sec/android/app/sns3/agent/db/SnsDBHelperBase;->SNS_DB_PRIMARY_KEY_TYPE:Ljava/lang/String;

    .line 30
    const-string v0, "BOOLEAN"

    iput-object v0, p0, Lcom/sec/android/app/sns3/agent/db/SnsDBHelperBase;->SNS_DB_BOOLEAN_TYPE:Ljava/lang/String;

    .line 32
    const-string v0, "INTEGER"

    iput-object v0, p0, Lcom/sec/android/app/sns3/agent/db/SnsDBHelperBase;->SNS_DB_INTEGER_TYPE:Ljava/lang/String;

    .line 34
    const-string v0, "INTEGER"

    iput-object v0, p0, Lcom/sec/android/app/sns3/agent/db/SnsDBHelperBase;->SNS_DB_SP_TYPE:Ljava/lang/String;

    .line 36
    const-string v0, "VARCHAR(50)"

    iput-object v0, p0, Lcom/sec/android/app/sns3/agent/db/SnsDBHelperBase;->SNS_DB_ID_TYPE:Ljava/lang/String;

    .line 38
    const-string v0, "VARCHAR(100)"

    iput-object v0, p0, Lcom/sec/android/app/sns3/agent/db/SnsDBHelperBase;->SNS_DB_NAME_TYPE:Ljava/lang/String;

    .line 40
    const-string v0, "VARCHAR(150)"

    iput-object v0, p0, Lcom/sec/android/app/sns3/agent/db/SnsDBHelperBase;->SNS_DB_EMAIL_TYPE:Ljava/lang/String;

    .line 42
    const-string v0, "VARCHAR(20)"

    iput-object v0, p0, Lcom/sec/android/app/sns3/agent/db/SnsDBHelperBase;->SNS_DB_PHONETYPE_TYPE:Ljava/lang/String;

    .line 44
    const-string v0, "VARCHAR(40)"

    iput-object v0, p0, Lcom/sec/android/app/sns3/agent/db/SnsDBHelperBase;->SNS_DB_PHONENUMBER_TYPE:Ljava/lang/String;

    .line 46
    const-string v0, "VARCHAR(15)"

    iput-object v0, p0, Lcom/sec/android/app/sns3/agent/db/SnsDBHelperBase;->SNS_DB_BIRTHDAY_TYPE:Ljava/lang/String;

    .line 48
    const-string v0, "TEXT"

    iput-object v0, p0, Lcom/sec/android/app/sns3/agent/db/SnsDBHelperBase;->SNS_DB_TITLE_TYPE:Ljava/lang/String;

    .line 50
    const-string v0, "TEXT"

    iput-object v0, p0, Lcom/sec/android/app/sns3/agent/db/SnsDBHelperBase;->SNS_DB_CONTENT_TYPE:Ljava/lang/String;

    .line 52
    const-string v0, "TEXT"

    iput-object v0, p0, Lcom/sec/android/app/sns3/agent/db/SnsDBHelperBase;->SNS_DB_TEXT_TYPE:Ljava/lang/String;

    .line 54
    const-string v0, "VARCHAR(1024)"

    iput-object v0, p0, Lcom/sec/android/app/sns3/agent/db/SnsDBHelperBase;->SNS_DB_URL_TYPE:Ljava/lang/String;

    .line 56
    const-string v0, "VARCHAR(512)"

    iput-object v0, p0, Lcom/sec/android/app/sns3/agent/db/SnsDBHelperBase;->SNS_DB_URL_FILEPATH:Ljava/lang/String;

    .line 58
    const-string v0, "TIMESTAMP"

    iput-object v0, p0, Lcom/sec/android/app/sns3/agent/db/SnsDBHelperBase;->SNS_DB_TIME_TYPE:Ljava/lang/String;

    .line 60
    const-string v0, "DOUBLE"

    iput-object v0, p0, Lcom/sec/android/app/sns3/agent/db/SnsDBHelperBase;->SNS_DB_LOC_TYPE:Ljava/lang/String;

    .line 62
    const-string v0, "VARCHAR(40)"

    iput-object v0, p0, Lcom/sec/android/app/sns3/agent/db/SnsDBHelperBase;->SNS_DB_SHORT_STRING_TYPE:Ljava/lang/String;

    .line 67
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 0
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 71
    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 0
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "oldVersion"    # I
    .param p3, "newVersion"    # I

    .prologue
    .line 75
    return-void
.end method
