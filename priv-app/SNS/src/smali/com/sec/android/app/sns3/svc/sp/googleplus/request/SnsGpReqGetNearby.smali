.class public abstract Lcom/sec/android/app/sns3/svc/sp/googleplus/request/SnsGpReqGetNearby;
.super Lcom/sec/android/app/sns3/svc/sp/googleplus/request/SnsGpReqBase;
.source "SnsGpReqGetNearby.java"

# interfaces
.implements Lcom/sec/android/app/sns3/svc/sp/googleplus/callback/ISnsGpReqCbNearby;


# static fields
.field public static final API_KEY:Ljava/lang/String; = "AIzaSyDxDJx3nERwrHj2mldW77SvHVv5Vane-Y8"

.field public static final REST_URL_NEARBY_SEARCH:Ljava/lang/String; = "https://maps.googleapis.com/maps/api/place/"


# instance fields
.field private mLatitude:Ljava/lang/String;

.field private mLongitude:Ljava/lang/String;

.field public mQueryString:Ljava/lang/StringBuilder;

.field private mRadius:Ljava/lang/String;

.field private mSearchType:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Bundle;)V
    .locals 5
    .param p1, "svcMgr"    # Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    .param p2, "bundle"    # Landroid/os/Bundle;

    .prologue
    const/4 v3, 0x0

    .line 57
    const/16 v2, 0x16

    invoke-direct {p0, p1, v2}, Lcom/sec/android/app/sns3/svc/sp/googleplus/request/SnsGpReqBase;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;I)V

    .line 46
    iput-object v3, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/request/SnsGpReqGetNearby;->mRadius:Ljava/lang/String;

    .line 48
    iput-object v3, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/request/SnsGpReqGetNearby;->mLatitude:Ljava/lang/String;

    .line 50
    iput-object v3, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/request/SnsGpReqGetNearby;->mLongitude:Ljava/lang/String;

    .line 52
    iput-object v3, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/request/SnsGpReqGetNearby;->mQueryString:Ljava/lang/StringBuilder;

    .line 54
    const-string v2, "nearbysearch"

    iput-object v2, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/request/SnsGpReqGetNearby;->mSearchType:Ljava/lang/String;

    .line 58
    if-eqz p2, :cond_6

    .line 59
    const-string v2, "searchType"

    invoke-virtual {p2, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/request/SnsGpReqGetNearby;->mSearchType:Ljava/lang/String;

    .line 60
    const-string v2, "searchType"

    invoke-virtual {p2, v2}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 61
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "key="

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iput-object v2, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/request/SnsGpReqGetNearby;->mQueryString:Ljava/lang/StringBuilder;

    .line 62
    iget-object v2, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/request/SnsGpReqGetNearby;->mQueryString:Ljava/lang/StringBuilder;

    const-string v4, "AIzaSyDxDJx3nERwrHj2mldW77SvHVv5Vane-Y8"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 66
    const-string v2, "latitude"

    invoke-virtual {p2, v2}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_1

    const-string v2, "latitude"

    invoke-virtual {p2, v2}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    :goto_0
    iput-object v2, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/request/SnsGpReqGetNearby;->mLatitude:Ljava/lang/String;

    .line 68
    const-string v2, "longitude"

    invoke-virtual {p2, v2}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_2

    const-string v2, "longitude"

    invoke-virtual {p2, v2}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    :goto_1
    iput-object v2, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/request/SnsGpReqGetNearby;->mLongitude:Ljava/lang/String;

    .line 70
    const-string v2, "radius"

    invoke-virtual {p2, v2}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_0

    const-string v2, "radius"

    invoke-virtual {p2, v2}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    :cond_0
    iput-object v3, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/request/SnsGpReqGetNearby;->mRadius:Ljava/lang/String;

    .line 72
    iget-object v2, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/request/SnsGpReqGetNearby;->mLatitude:Ljava/lang/String;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/request/SnsGpReqGetNearby;->mLongitude:Ljava/lang/String;

    if-eqz v2, :cond_3

    .line 73
    iget-object v2, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/request/SnsGpReqGetNearby;->mQueryString:Ljava/lang/StringBuilder;

    const-string v3, "&location="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/request/SnsGpReqGetNearby;->mLatitude:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/request/SnsGpReqGetNearby;->mLongitude:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 74
    const-string v2, "latitude"

    invoke-virtual {p2, v2}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 75
    const-string v2, "longitude"

    invoke-virtual {p2, v2}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 78
    iget-object v2, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/request/SnsGpReqGetNearby;->mRadius:Ljava/lang/String;

    if-eqz v2, :cond_4

    .line 79
    iget-object v2, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/request/SnsGpReqGetNearby;->mQueryString:Ljava/lang/StringBuilder;

    const-string v3, "&radius="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/request/SnsGpReqGetNearby;->mRadius:Ljava/lang/String;

    invoke-static {v3}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->encodeUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 80
    const-string v2, "radius"

    invoke-virtual {p2, v2}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 84
    :goto_2
    invoke-virtual {p2}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 85
    .local v1, "key":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/request/SnsGpReqGetNearby;->mQueryString:Ljava/lang/StringBuilder;

    const-string v3, "&"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->encodeUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "key":Ljava/lang/String;
    :cond_1
    move-object v2, v3

    .line 66
    goto/16 :goto_0

    :cond_2
    move-object v2, v3

    .line 68
    goto/16 :goto_1

    .line 77
    :cond_3
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Must provide parameters latitude & longitude"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 82
    :cond_4
    iget-object v2, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/request/SnsGpReqGetNearby;->mQueryString:Ljava/lang/StringBuilder;

    const-string v3, "&rankby=distance"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 88
    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_5
    iget-object v2, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/request/SnsGpReqGetNearby;->mQueryString:Ljava/lang/StringBuilder;

    const-string v3, "&sensor=true"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 91
    .end local v0    # "i$":Ljava/util/Iterator;
    :cond_6
    return-void
.end method


# virtual methods
.method protected compose()Lcom/sec/android/app/sns3/svc/http/SnsHttpRequest;
    .locals 6

    .prologue
    .line 96
    const-string v2, "GET"

    .line 97
    .local v2, "method":Ljava/lang/String;
    const/4 v3, 0x0

    .line 98
    .local v3, "uri":Ljava/lang/String;
    const/4 v4, 0x0

    .line 99
    .local v4, "header":Landroid/os/Bundle;
    const/4 v5, 0x0

    .line 101
    .local v5, "body":Landroid/os/Bundle;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "https://maps.googleapis.com/maps/api/place/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/request/SnsGpReqGetNearby;->mSearchType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/json?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/request/SnsGpReqGetNearby;->mQueryString:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 102
    new-instance v0, Lcom/sec/android/app/sns3/svc/http/SnsHttpRequest;

    iget v1, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/request/SnsGpReqGetNearby;->mReqID:I

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/sns3/svc/http/SnsHttpRequest;-><init>(ILjava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;)V

    return-object v0
.end method

.method protected parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;
    .locals 4
    .param p1, "content"    # Ljava/lang/String;

    .prologue
    .line 107
    const-string v1, "SNS"

    const-string v2, "GooglePlus  SnsGpReqGetNearby response SUCCESS!!"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 108
    const-string v1, "SNS"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "response: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 109
    invoke-super {p0, p1}, Lcom/sec/android/app/sns3/svc/sp/googleplus/request/SnsGpReqBase;->parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;

    move-result-object v0

    .line 111
    .local v0, "response":Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;
    if-eqz v0, :cond_0

    .line 114
    .end local v0    # "response":Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;
    :goto_0
    return-object v0

    .restart local v0    # "response":Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;
    :cond_0
    invoke-static {p1}, Lcom/sec/android/app/sns3/svc/sp/googleplus/parser/SnsGpParserNearby;->parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseNearby;

    move-result-object v0

    goto :goto_0
.end method

.method protected respond(Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;)Z
    .locals 7
    .param p1, "result"    # Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;

    .prologue
    .line 119
    iget v1, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/request/SnsGpReqGetNearby;->mReqID:I

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->isSuccess()Z

    move-result v2

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->getHttpstatus()I

    move-result v3

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->getErrorCode()I

    move-result v4

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->getReason()Landroid/os/Bundle;

    move-result-object v5

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->getResponse()Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseNearby;

    move-object v0, p0

    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/app/sns3/svc/sp/googleplus/request/SnsGpReqGetNearby;->onReqRespond(IZIILandroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/googleplus/response/SnsGpResponseNearby;)Z

    .line 121
    const/4 v0, 0x1

    return v0
.end method
