.class Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService$1;
.super Lcom/sec/android/app/sns3/svc/sp/linkedin/request/SnsLiReqGetProfile;
.source "SnsLiSyncAdapterProfileService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;->performSync()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;


# direct methods
.method constructor <init>(Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 0
    .param p2, "x0"    # Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    .param p3, "x1"    # Ljava/lang/String;
    .param p4, "x2"    # Landroid/os/Bundle;

    .prologue
    .line 187
    iput-object p1, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService$1;->this$0:Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;

    invoke-direct {p0, p2, p3, p4}, Lcom/sec/android/app/sns3/svc/sp/linkedin/request/SnsLiReqGetProfile;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;Landroid/os/Bundle;)V

    return-void
.end method


# virtual methods
.method public onReqRespond(IZIILandroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseProfile;)Z
    .locals 6
    .param p1, "reqID"    # I
    .param p2, "bSuccess"    # Z
    .param p3, "httpStatus"    # I
    .param p4, "errorCode"    # I
    .param p5, "reason"    # Landroid/os/Bundle;
    .param p6, "user"    # Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseProfile;

    .prologue
    const/4 v5, -0x1

    .line 193
    if-eqz p2, :cond_0

    .line 194
    new-instance v0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;

    iget-object v3, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService$1;->this$0:Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;

    # getter for: Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;->access$000(Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;)Landroid/content/Context;

    move-result-object v3

    invoke-direct {v0, v3}, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;-><init>(Landroid/content/Context;)V

    .line 196
    .local v0, "dbAdapter":Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;
    :try_start_0
    invoke-virtual {v0, p6}, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;->updateProfile(Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseProfile;)Ljava/util/List;

    move-result-object v2

    .line 197
    .local v2, "result":Ljava/util/List;, "Ljava/util/List<Landroid/net/Uri;>;"
    iget-object v3, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService$1;->this$0:Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;

    const/4 v4, 0x2

    # setter for: Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;->mSyncState:I
    invoke-static {v3, v4}, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;->access$702(Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;I)I
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 206
    .end local v0    # "dbAdapter":Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;
    .end local v2    # "result":Ljava/util/List;, "Ljava/util/List<Landroid/net/Uri;>;"
    :goto_0
    iget-object v3, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService$1;->this$0:Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;

    # getter for: Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;
    invoke-static {v3}, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;->access$500(Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;)Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;->resumeSync()V

    .line 208
    const/4 v3, 0x1

    return v3

    .line 198
    .restart local v0    # "dbAdapter":Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;
    :catch_0
    move-exception v1

    .line 199
    .local v1, "e":Landroid/database/sqlite/SQLiteException;
    iget-object v3, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService$1;->this$0:Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;

    # setter for: Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;->mSyncState:I
    invoke-static {v3, v5}, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;->access$702(Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;I)I

    .line 200
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    goto :goto_0

    .line 203
    .end local v0    # "dbAdapter":Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;
    .end local v1    # "e":Landroid/database/sqlite/SQLiteException;
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService$1;->this$0:Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;

    # setter for: Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;->mSyncState:I
    invoke-static {v3, v5}, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;->access$702(Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;I)I

    goto :goto_0
.end method
