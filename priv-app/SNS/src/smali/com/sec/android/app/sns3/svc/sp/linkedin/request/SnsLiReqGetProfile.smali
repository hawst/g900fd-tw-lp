.class public abstract Lcom/sec/android/app/sns3/svc/sp/linkedin/request/SnsLiReqGetProfile;
.super Lcom/sec/android/app/sns3/svc/sp/linkedin/request/SnsLiReqBase;
.source "SnsLiReqGetProfile.java"

# interfaces
.implements Lcom/sec/android/app/sns3/svc/sp/linkedin/callback/ISnsLiReqCbProfile;


# static fields
.field private static final FIELD_LIST:Ljava/lang/String; = ":(id,first-name,last-name,formatted-name,headline,location:(name,country:(code)),industry,current-share,num-connections,summary,specialties,positions,picture-url,public-profile-url,skills,following,educations,date-of-birth,phone-numbers,main-address,im-accounts,email-address,twitter-accounts,group-memberships,connections:(id,first-name,last-name,headline,positions,public-profile-url,picture-url,location))"


# direct methods
.method public constructor <init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 1
    .param p1, "svcMgr"    # Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    .param p2, "objectID"    # Ljava/lang/String;
    .param p3, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 70
    const/16 v0, 0x16

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/request/SnsLiReqBase;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;I)V

    .line 72
    return-void
.end method


# virtual methods
.method protected compose()Lcom/sec/android/app/sns3/svc/http/SnsHttpRequest;
    .locals 7

    .prologue
    .line 77
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/request/SnsLiReqGetProfile;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/SnsSvcMgr;->getTokenMgr()Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;

    move-result-object v0

    const-string v1, "linkedin"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;->getToken(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/sns3/svc/sp/linkedin/SnsLiToken;

    .line 79
    .local v6, "token":Lcom/sec/android/app/sns3/svc/sp/linkedin/SnsLiToken;
    const-string v2, "GET"

    .line 80
    .local v2, "method":Ljava/lang/String;
    const/4 v3, 0x0

    .line 81
    .local v3, "uri":Ljava/lang/String;
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 82
    .local v4, "header":Landroid/os/Bundle;
    const/4 v5, 0x0

    .line 84
    .local v5, "body":Landroid/os/Bundle;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "https://api.linkedin.com/v1/people/~:(id,first-name,last-name,formatted-name,headline,location:(name,country:(code)),industry,current-share,num-connections,summary,specialties,positions,picture-url,public-profile-url,skills,following,educations,date-of-birth,phone-numbers,main-address,im-accounts,email-address,twitter-accounts,group-memberships,connections:(id,first-name,last-name,headline,positions,public-profile-url,picture-url,location))?oauth2_access_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v6}, Lcom/sec/android/app/sns3/svc/sp/linkedin/SnsLiToken;->getAccessToken()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->encodeUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&membership-state=member"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 88
    const-string v0, "x-li-format"

    const-string v1, "json"

    invoke-virtual {v4, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    new-instance v0, Lcom/sec/android/app/sns3/svc/http/SnsHttpRequest;

    iget v1, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/request/SnsLiReqGetProfile;->mReqID:I

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/sns3/svc/http/SnsHttpRequest;-><init>(ILjava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;)V

    return-object v0
.end method

.method protected parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;
    .locals 3
    .param p1, "content"    # Ljava/lang/String;

    .prologue
    .line 94
    const-string v1, "SNS"

    const-string v2, "LinkedIn  SnsLiReqGetProfile response SUCCESS!!"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 95
    invoke-super {p0, p1}, Lcom/sec/android/app/sns3/svc/sp/linkedin/request/SnsLiReqBase;->parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;

    move-result-object v0

    .line 97
    .local v0, "response":Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;
    if-eqz v0, :cond_0

    .line 100
    .end local v0    # "response":Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;
    :goto_0
    return-object v0

    .restart local v0    # "response":Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;
    :cond_0
    invoke-static {p1}, Lcom/sec/android/app/sns3/svc/sp/linkedin/parser/SnsLiParserProfile;->parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseProfile;

    move-result-object v0

    goto :goto_0
.end method

.method protected respond(Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;)Z
    .locals 7
    .param p1, "result"    # Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;

    .prologue
    .line 105
    iget v1, p0, Lcom/sec/android/app/sns3/svc/sp/linkedin/request/SnsLiReqGetProfile;->mReqID:I

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->isSuccess()Z

    move-result v2

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->getHttpstatus()I

    move-result v3

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->getErrorCode()I

    move-result v4

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->getReason()Landroid/os/Bundle;

    move-result-object v5

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->getResponse()Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseProfile;

    move-object v0, p0

    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/app/sns3/svc/sp/linkedin/request/SnsLiReqGetProfile;->onReqRespond(IZIILandroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/linkedin/response/SnsLiResponseProfile;)Z

    .line 108
    const/4 v0, 0x1

    return v0
.end method
