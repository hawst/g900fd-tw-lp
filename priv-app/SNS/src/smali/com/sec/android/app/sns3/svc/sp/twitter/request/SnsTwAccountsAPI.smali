.class public final enum Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI;
.super Ljava/lang/Enum;
.source "SnsTwAccountsAPI.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI$Param;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI;

.field public static final enum ACCOUNT_END_SESSION:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI;

.field public static final enum ACCOUNT_UPDATE_DELIVERY_DEVICE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI;

.field public static final enum ACCOUNT_UPDATE_PROFILE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI;

.field public static final enum ACCOUNT_UPDATE_PROFILE_BACKGROUND_IMAGE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI;

.field public static final enum ACCOUNT_UPDATE_PROFILE_COLORS:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI;

.field public static final enum ACCOUNT_UPDATE_PROFILE_IMAGE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI;

.field public static final enum ACCOUNT_VERIFY_CREDENTIALS:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI;


# instance fields
.field private mHttpMethod:Ljava/lang/String;

.field private mUrl:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 29
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI;

    const-string v1, "ACCOUNT_VERIFY_CREDENTIALS"

    const-string v2, "/account/verify_credentials.json"

    const-string v3, "GET"

    invoke-direct {v0, v1, v5, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI;->ACCOUNT_VERIFY_CREDENTIALS:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI;

    .line 30
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI;

    const-string v1, "ACCOUNT_END_SESSION"

    const-string v2, "/account/end_session.json"

    const-string v3, "POST"

    invoke-direct {v0, v1, v6, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI;->ACCOUNT_END_SESSION:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI;

    .line 31
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI;

    const-string v1, "ACCOUNT_UPDATE_DELIVERY_DEVICE"

    const-string v2, "/account/update_delivery_device.json"

    const-string v3, "POST"

    invoke-direct {v0, v1, v7, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI;->ACCOUNT_UPDATE_DELIVERY_DEVICE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI;

    .line 32
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI;

    const-string v1, "ACCOUNT_UPDATE_PROFILE"

    const-string v2, "/account/update_profile.json"

    const-string v3, "POST"

    invoke-direct {v0, v1, v8, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI;->ACCOUNT_UPDATE_PROFILE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI;

    .line 33
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI;

    const-string v1, "ACCOUNT_UPDATE_PROFILE_BACKGROUND_IMAGE"

    const-string v2, "/account/update_profile_background_image.json"

    const-string v3, "POST"

    invoke-direct {v0, v1, v9, v2, v3}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI;->ACCOUNT_UPDATE_PROFILE_BACKGROUND_IMAGE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI;

    .line 34
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI;

    const-string v1, "ACCOUNT_UPDATE_PROFILE_COLORS"

    const/4 v2, 0x5

    const-string v3, "/account/update_profile_colors.json"

    const-string v4, "POST"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI;->ACCOUNT_UPDATE_PROFILE_COLORS:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI;

    .line 35
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI;

    const-string v1, "ACCOUNT_UPDATE_PROFILE_IMAGE"

    const/4 v2, 0x6

    const-string v3, "/account/update_profile_image.json"

    const-string v4, "POST"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI;->ACCOUNT_UPDATE_PROFILE_IMAGE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI;

    .line 27
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI;

    sget-object v1, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI;->ACCOUNT_VERIFY_CREDENTIALS:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI;->ACCOUNT_END_SESSION:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI;->ACCOUNT_UPDATE_DELIVERY_DEVICE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI;

    aput-object v1, v0, v7

    sget-object v1, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI;->ACCOUNT_UPDATE_PROFILE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI;

    aput-object v1, v0, v8

    sget-object v1, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI;->ACCOUNT_UPDATE_PROFILE_BACKGROUND_IMAGE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI;

    aput-object v1, v0, v9

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI;->ACCOUNT_UPDATE_PROFILE_COLORS:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI;->ACCOUNT_UPDATE_PROFILE_IMAGE:Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI;->$VALUES:[Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p3, "url"    # Ljava/lang/String;
    .param p4, "httpMethod"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 76
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 77
    iput-object p3, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI;->mUrl:Ljava/lang/String;

    .line 78
    iput-object p4, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI;->mHttpMethod:Ljava/lang/String;

    .line 79
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 27
    const-class v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI;
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI;->$VALUES:[Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI;

    invoke-virtual {v0}, [Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI;

    return-object v0
.end method


# virtual methods
.method public getHttpMethod()Ljava/lang/String;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI;->mHttpMethod:Ljava/lang/String;

    return-object v0
.end method

.method public getUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI;->mUrl:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwAccountsAPI;->mUrl:Ljava/lang/String;

    return-object v0
.end method
