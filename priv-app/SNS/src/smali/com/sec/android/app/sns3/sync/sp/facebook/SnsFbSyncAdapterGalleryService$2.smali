.class Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService$2;
.super Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqGetPhotos;
.source "SnsFbSyncAdapterGalleryService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->performPhotoSync()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;

.field final synthetic val$photoArrayList:Ljava/util/ArrayList;


# direct methods
.method constructor <init>(Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;Landroid/os/Bundle;Ljava/util/ArrayList;)V
    .locals 0
    .param p2, "x0"    # Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    .param p3, "x1"    # Ljava/lang/String;
    .param p4, "x2"    # Landroid/os/Bundle;

    .prologue
    .line 301
    iput-object p1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService$2;->this$0:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;

    iput-object p5, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService$2;->val$photoArrayList:Ljava/util/ArrayList;

    invoke-direct {p0, p2, p3, p4}, Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqGetPhotos;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;Landroid/os/Bundle;)V

    return-void
.end method


# virtual methods
.method public onReqRespond(IZIILandroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhotos;)Z
    .locals 6
    .param p1, "reqID"    # I
    .param p2, "bSuccess"    # Z
    .param p3, "httpStatus"    # I
    .param p4, "errorCode"    # I
    .param p5, "reason"    # Landroid/os/Bundle;
    .param p6, "photos"    # Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhotos;

    .prologue
    const/4 v5, -0x1

    .line 308
    if-eqz p2, :cond_0

    .line 309
    new-instance v0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;

    invoke-direct {v0}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;-><init>()V

    .line 311
    .local v0, "dbAdapter":Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService$2;->this$0:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;

    iget-object v3, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService$2;->this$0:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;

    # getter for: Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->mAlbumId:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->access$900(Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3, p6}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->insertPhotos(Ljava/lang/String;Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePhotos;)Landroid/os/Bundle;

    move-result-object v3

    # setter for: Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->mBundle:Landroid/os/Bundle;
    invoke-static {v2, v3}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->access$602(Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;Landroid/os/Bundle;)Landroid/os/Bundle;

    .line 312
    iget-object v2, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService$2;->val$photoArrayList:Ljava/util/ArrayList;

    invoke-virtual {v2, p6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 325
    .end local v0    # "dbAdapter":Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService$2;->this$0:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;

    # getter for: Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;
    invoke-static {v2}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->access$500(Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;)Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->resumeSync()V

    .line 327
    const/4 v2, 0x1

    return v2

    .line 313
    .restart local v0    # "dbAdapter":Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;
    :catch_0
    move-exception v1

    .line 314
    .local v1, "e":Ljava/lang/Exception;
    iget-object v2, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService$2;->this$0:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;

    # setter for: Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->mSyncState:I
    invoke-static {v2, v5}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->access$802(Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;I)I

    .line 315
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 318
    .end local v0    # "dbAdapter":Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_0
    const-string v2, "SnsFbSync"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SnsFbSyncAdapterGalleryService errorCode : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", reason : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 322
    iget-object v2, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService$2;->this$0:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;

    # setter for: Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->mSyncState:I
    invoke-static {v2, v5}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->access$802(Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;I)I

    goto :goto_0
.end method
