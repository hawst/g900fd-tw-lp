.class public interface abstract Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserTags$FacebookTags;
.super Ljava/lang/Object;
.source "SnsFbParserTags.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserTags;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "FacebookTags"
.end annotation


# static fields
.field public static final CREATED_TIME:Ljava/lang/String; = "created_time"

.field public static final DATA:Ljava/lang/String; = "data"

.field public static final ID:Ljava/lang/String; = "id"

.field public static final NAME:Ljava/lang/String; = "name"

.field public static final TAG_X:Ljava/lang/String; = "x"

.field public static final TAG_Y:Ljava/lang/String; = "y"

.field public static final TYPE:Ljava/lang/String; = "type"
