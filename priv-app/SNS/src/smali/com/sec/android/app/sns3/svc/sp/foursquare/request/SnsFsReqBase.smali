.class public abstract Lcom/sec/android/app/sns3/svc/sp/foursquare/request/SnsFsReqBase;
.super Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
.source "SnsFsReqBase.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "SnsFsReqBase"


# direct methods
.method protected constructor <init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;I)V
    .locals 1
    .param p1, "svcMgr"    # Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    .param p2, "reqCategory"    # I

    .prologue
    .line 49
    const-string v0, "foursquare"

    invoke-direct {p0, p1, v0, p2}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;I)V

    .line 50
    return-void
.end method


# virtual methods
.method protected check(IILcom/sec/android/app/sns3/svc/sp/SnsSpResponse;)I
    .locals 6
    .param p1, "httpStatus"    # I
    .param p2, "prevErrorCode"    # I
    .param p3, "resp"    # Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;

    .prologue
    .line 55
    const/4 v1, -0x1

    .line 57
    .local v1, "errorCode":I
    const/4 v3, -0x1

    if-le p2, v3, :cond_1

    .line 58
    move v1, p2

    .line 80
    :cond_0
    :goto_0
    return v1

    .line 60
    :cond_1
    instance-of v3, p3, Lcom/sec/android/app/sns3/svc/sp/SnsSpResponseError;

    if-eqz v3, :cond_0

    move-object v2, p3

    .line 61
    check-cast v2, Lcom/sec/android/app/sns3/svc/sp/SnsSpResponseError;

    .line 62
    .local v2, "errorResp":Lcom/sec/android/app/sns3/svc/sp/SnsSpResponseError;
    invoke-virtual {v2}, Lcom/sec/android/app/sns3/svc/sp/SnsSpResponseError;->getErrorCode()I

    move-result v1

    .line 64
    invoke-virtual {v2}, Lcom/sec/android/app/sns3/svc/sp/SnsSpResponseError;->getErrorBundle()Landroid/os/Bundle;

    move-result-object v0

    .line 65
    .local v0, "errorBundle":Landroid/os/Bundle;
    const-string v3, "SnsFsReqBase"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[SnsFsReqBase] check. httpStatus: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " prevErrorCode: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " errorBundle: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 68
    if-eqz v0, :cond_0

    .line 69
    const-string v3, "401"

    const-string v4, "error_code"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 70
    const/16 v1, 0x7d1

    .line 71
    const-string v3, "SnsFsReqBase"

    const-string v4, "Session Error"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 72
    :cond_2
    const-string v3, "403"

    const-string v4, "error_code"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 73
    const/16 v1, 0x7d3

    .line 74
    const-string v3, "SnsFsReqBase"

    const-string v4, "Maximum request limit reached"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;
    .locals 10
    .param p1, "content"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x0

    .line 85
    const/4 v3, -0x1

    .line 86
    .local v3, "errorCode":I
    const/4 v4, 0x0

    .line 87
    .local v4, "errorMsg":Ljava/lang/String;
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 90
    .local v2, "errorBundle":Landroid/os/Bundle;
    if-eqz p1, :cond_0

    :try_start_0
    const-string v8, "true"

    invoke-virtual {p1, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_0

    const-string v8, "false"

    invoke-virtual {p1, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_0

    invoke-static {p1}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->isJsonArray(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 120
    :cond_0
    :goto_0
    return-object v7

    .line 94
    :cond_1
    new-instance v5, Lorg/json/JSONObject;

    invoke-direct {v5, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 96
    .local v5, "jsonObj":Lorg/json/JSONObject;
    const-string v8, "meta"

    invoke-virtual {v5, v8}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v6

    .line 97
    .local v6, "meta":Lorg/json/JSONObject;
    const-string v8, "code"

    invoke-virtual {v6, v8}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 99
    .local v0, "code":I
    const/16 v8, 0xc8

    if-eq v0, v8, :cond_2

    .line 100
    const/16 v3, 0x3e8

    .line 102
    const-string v8, "errorType"

    const-string v9, "errorType"

    invoke-virtual {v6, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v2, v8, v9}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    const-string v8, "error_code"

    const-string v9, "code"

    invoke-virtual {v6, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v2, v8, v9}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    const-string v8, "errorDetail"

    const-string v9, "errorDetail"

    invoke-virtual {v6, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v2, v8, v9}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    const-string v8, "errorMessage"

    invoke-virtual {v6, v8}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 106
    const-string v8, "errorMessage"

    invoke-virtual {v6, v8}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v4

    .line 117
    .end local v0    # "code":I
    .end local v5    # "jsonObj":Lorg/json/JSONObject;
    .end local v6    # "meta":Lorg/json/JSONObject;
    :cond_2
    :goto_1
    const/16 v8, 0x3e8

    if-ne v3, v8, :cond_0

    .line 118
    new-instance v7, Lcom/sec/android/app/sns3/svc/sp/SnsSpResponseError;

    invoke-direct {v7, v3, v4, v2}, Lcom/sec/android/app/sns3/svc/sp/SnsSpResponseError;-><init>(ILjava/lang/String;Landroid/os/Bundle;)V

    goto :goto_0

    .line 111
    :catch_0
    move-exception v1

    .line 112
    .local v1, "e":Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_1

    .line 113
    .end local v1    # "e":Lorg/json/JSONException;
    :catch_1
    move-exception v1

    .line 114
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method protected restoreToken()V
    .locals 12

    .prologue
    const/4 v9, 0x1

    const/4 v10, 0x0

    .line 125
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v0

    .line 126
    .local v0, "context":Landroid/content/Context;
    const-string v8, "notification"

    invoke-virtual {v0, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/NotificationManager;

    .line 129
    .local v5, "notiMgr":Landroid/app/NotificationManager;
    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4}, Landroid/content/Intent;-><init>()V

    .line 131
    .local v4, "notiIntent":Landroid/content/Intent;
    const-string v8, "com.sec.android.app.sns3.RETRY_SSO_FOURSQUARE"

    invoke-virtual {v4, v8}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 135
    const/high16 v8, 0x800000

    invoke-virtual {v4, v8}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 136
    const-string v8, "RetryLogin"

    invoke-virtual {v4, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 137
    invoke-static {v0, v10, v4, v10}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    .line 139
    .local v2, "launchIntent":Landroid/app/PendingIntent;
    const v7, 0x7f08003f

    .line 140
    .local v7, "titleID":I
    const v6, 0x7f080021

    .line 141
    .local v6, "spID":I
    const v1, 0x7f02001b

    .line 143
    .local v1, "iconID":I
    new-instance v3, Landroid/app/Notification$Builder;

    invoke-direct {v3, v0}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    .line 144
    .local v3, "notiBuilder":Landroid/app/Notification$Builder;
    new-array v8, v9, [Ljava/lang/Object;

    invoke-virtual {v0, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v10

    invoke-virtual {v0, v7, v8}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v8}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v8

    invoke-virtual {v8, v1}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v8

    const v9, 0x7f08003e

    invoke-virtual {v0, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v8

    invoke-virtual {v8, v2}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v8

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    invoke-virtual {v8, v10, v11}, Landroid/app/Notification$Builder;->setWhen(J)Landroid/app/Notification$Builder;

    .line 150
    const/16 v8, 0x10cc

    invoke-virtual {v3}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v9

    invoke-virtual {v5, v8, v9}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 152
    return-void
.end method
