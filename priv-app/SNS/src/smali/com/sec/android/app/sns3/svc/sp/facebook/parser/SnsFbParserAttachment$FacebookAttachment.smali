.class public interface abstract Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserAttachment$FacebookAttachment;
.super Ljava/lang/Object;
.source "SnsFbParserAttachment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserAttachment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "FacebookAttachment"
.end annotation


# static fields
.field public static final ATTACHMENT:Ljava/lang/String; = "attachment"

.field public static final DATA:Ljava/lang/String; = "data"

.field public static final MEDIA:Ljava/lang/String; = "media"

.field public static final OBJECT_TYPE:Ljava/lang/String; = "fb_object_type"

.field public static final PHOTO:Ljava/lang/String; = "photo"

.field public static final PHOTO_ID:Ljava/lang/String; = "pid"

.field public static final PICTURE:Ljava/lang/String; = "src"
