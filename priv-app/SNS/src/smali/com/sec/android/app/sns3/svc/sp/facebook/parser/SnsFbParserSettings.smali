.class public Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserSettings;
.super Ljava/lang/Object;
.source "SnsFbParserSettings.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserSettings$FacebookSettings;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    return-void
.end method

.method public static parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseSettings;
    .locals 10
    .param p0, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 40
    const/4 v8, 0x0

    .line 41
    .local v8, "settings":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseSettings;
    const/4 v0, 0x0

    .line 42
    .local v0, "curSettings":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseSettings;
    const/4 v5, 0x0

    .line 45
    .local v5, "jsonObject":Lorg/json/JSONObject;
    :try_start_0
    new-instance v6, Lorg/json/JSONObject;

    invoke-direct {v6, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 46
    .end local v5    # "jsonObject":Lorg/json/JSONObject;
    .local v6, "jsonObject":Lorg/json/JSONObject;
    :try_start_1
    new-instance v3, Lorg/json/JSONArray;

    const-string v9, "data"

    invoke-virtual {v6, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v3, v9}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 48
    .local v3, "ja":Lorg/json/JSONArray;
    if-eqz v3, :cond_1

    .line 49
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v9

    if-ge v2, v9, :cond_1

    .line 50
    new-instance v7, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseSettings;

    invoke-direct {v7}, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseSettings;-><init>()V

    .line 52
    .local v7, "newSetting":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseSettings;
    invoke-virtual {v3, v2}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    .line 54
    .local v4, "jsonObj":Lorg/json/JSONObject;
    const-string v9, "setting"

    invoke-virtual {v4, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v7, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseSettings;->mSetting:Ljava/lang/String;

    .line 55
    const-string v9, "value"

    invoke-virtual {v4, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v7, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseSettings;->mValue:Ljava/lang/String;

    .line 57
    if-nez v8, :cond_0

    .line 58
    move-object v8, v7

    .line 59
    move-object v0, v8

    .line 49
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 61
    :cond_0
    iput-object v7, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseSettings;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseSettings;

    .line 62
    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseSettings;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseSettings;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .end local v2    # "i":I
    .end local v4    # "jsonObj":Lorg/json/JSONObject;
    .end local v7    # "newSetting":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseSettings;
    :cond_1
    move-object v5, v6

    .line 72
    .end local v3    # "ja":Lorg/json/JSONArray;
    .end local v6    # "jsonObject":Lorg/json/JSONObject;
    .restart local v5    # "jsonObject":Lorg/json/JSONObject;
    :goto_2
    return-object v8

    .line 67
    :catch_0
    move-exception v1

    .line 69
    .local v1, "e":Lorg/json/JSONException;
    :goto_3
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_2

    .line 67
    .end local v1    # "e":Lorg/json/JSONException;
    .end local v5    # "jsonObject":Lorg/json/JSONObject;
    .restart local v6    # "jsonObject":Lorg/json/JSONObject;
    :catch_1
    move-exception v1

    move-object v5, v6

    .end local v6    # "jsonObject":Lorg/json/JSONObject;
    .restart local v5    # "jsonObject":Lorg/json/JSONObject;
    goto :goto_3
.end method
