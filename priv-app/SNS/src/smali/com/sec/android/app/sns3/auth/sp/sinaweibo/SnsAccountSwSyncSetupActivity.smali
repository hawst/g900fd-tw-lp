.class public Lcom/sec/android/app/sns3/auth/sp/sinaweibo/SnsAccountSwSyncSetupActivity;
.super Landroid/preference/PreferenceActivity;
.source "SnsAccountSwSyncSetupActivity.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "SnsAccountSwAuth"


# instance fields
.field private mAppContext:Landroid/content/Context;

.field private mLifeFeedsCheckBox:Landroid/preference/CheckBoxPreference;

.field private mSyncIntervalSetting:Landroid/preference/PreferenceScreen;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 53
    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    .line 55
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/sinaweibo/SnsAccountSwSyncSetupActivity;->mAppContext:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method isTable()Z
    .locals 2

    .prologue
    .line 170
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/sinaweibo/SnsAccountSwSyncSetupActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit8 v0, v0, 0x3

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/sinaweibo/SnsAccountSwSyncSetupActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    .line 173
    :cond_0
    const/4 v0, 0x1

    .line 175
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onClickDone()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 131
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/sinaweibo/SnsAccountSwSyncSetupActivity;->mAppContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    const-string v2, "com.sec.android.app.sns3.sinaweibo"

    invoke-virtual {v1, v2}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 133
    .local v0, "account":[Landroid/accounts/Account;
    array-length v1, v0

    if-lez v1, :cond_0

    .line 134
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/sinaweibo/SnsAccountSwSyncSetupActivity;->mLifeFeedsCheckBox:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 135
    aget-object v1, v0, v3

    const-string v2, "com.sec.android.app.sns3.life"

    invoke-static {v1, v2, v4}, Landroid/content/ContentResolver;->setIsSyncable(Landroid/accounts/Account;Ljava/lang/String;I)V

    .line 136
    aget-object v1, v0, v3

    const-string v2, "com.sec.android.app.sns3.life"

    invoke-static {v1, v2, v4}, Landroid/content/ContentResolver;->setSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;Z)V

    .line 139
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/sinaweibo/SnsAccountSwSyncSetupActivity;->finish()V

    .line 140
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 9
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const v6, 0x7f080047

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 66
    iput-object p0, p0, Lcom/sec/android/app/sns3/auth/sp/sinaweibo/SnsAccountSwSyncSetupActivity;->mAppContext:Landroid/content/Context;

    .line 68
    iget-object v3, p0, Lcom/sec/android/app/sns3/auth/sp/sinaweibo/SnsAccountSwSyncSetupActivity;->mAppContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->isLightTheme(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_4

    const v3, 0x103012b

    :goto_0
    invoke-virtual {p0, v3}, Lcom/sec/android/app/sns3/auth/sp/sinaweibo/SnsAccountSwSyncSetupActivity;->setTheme(I)V

    .line 71
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 73
    const/high16 v3, 0x7f030000

    invoke-virtual {p0, v3}, Lcom/sec/android/app/sns3/auth/sp/sinaweibo/SnsAccountSwSyncSetupActivity;->setContentView(I)V

    .line 74
    const v3, 0x7f040006

    invoke-virtual {p0, v3}, Lcom/sec/android/app/sns3/auth/sp/sinaweibo/SnsAccountSwSyncSetupActivity;->addPreferencesFromResource(I)V

    .line 76
    const v3, 0x7f080046

    new-array v4, v8, [Ljava/lang/Object;

    invoke-virtual {p0, v6}, Lcom/sec/android/app/sns3/auth/sp/sinaweibo/SnsAccountSwSyncSetupActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-virtual {p0, v3, v4}, Lcom/sec/android/app/sns3/auth/sp/sinaweibo/SnsAccountSwSyncSetupActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/sec/android/app/sns3/auth/sp/sinaweibo/SnsAccountSwSyncSetupActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 78
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/sinaweibo/SnsAccountSwSyncSetupActivity;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/sns3/auth/sp/sinaweibo/SnsAccountSwSyncSetupActivity;->mAppContext:Landroid/content/Context;

    invoke-virtual {v3, v4}, Landroid/preference/PreferenceManager;->createPreferenceScreen(Landroid/content/Context;)Landroid/preference/PreferenceScreen;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/sns3/auth/sp/sinaweibo/SnsAccountSwSyncSetupActivity;->mSyncIntervalSetting:Landroid/preference/PreferenceScreen;

    .line 79
    iget-object v3, p0, Lcom/sec/android/app/sns3/auth/sp/sinaweibo/SnsAccountSwSyncSetupActivity;->mSyncIntervalSetting:Landroid/preference/PreferenceScreen;

    const v4, 0x7f080056

    invoke-virtual {v3, v4}, Landroid/preference/PreferenceScreen;->setTitle(I)V

    .line 80
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/sinaweibo/SnsAccountSwSyncSetupActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/sns3/auth/sp/sinaweibo/SnsAccountSwSyncSetupActivity;->mSyncIntervalSetting:Landroid/preference/PreferenceScreen;

    invoke-virtual {v3, v4}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 82
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/sns3/SnsApplication;->getFeatureMgr()Lcom/sec/android/app/sns3/SnsFeatureManager;

    move-result-object v0

    .line 83
    .local v0, "featureMgr":Lcom/sec/android/app/sns3/SnsFeatureManager;
    const/4 v2, 0x0

    .line 84
    .local v2, "providerLabel":Ljava/lang/CharSequence;
    const/4 v1, 0x0

    .line 87
    .local v1, "providerInfo":Landroid/content/pm/ProviderInfo;
    const/4 v2, 0x0

    .line 88
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/sinaweibo/SnsAccountSwSyncSetupActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    const-string v4, "com.sec.android.app.sns3.life"

    invoke-virtual {v3, v4, v7}, Landroid/content/pm/PackageManager;->resolveContentProvider(Ljava/lang/String;I)Landroid/content/pm/ProviderInfo;

    move-result-object v1

    .line 89
    if-eqz v1, :cond_0

    .line 90
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/sinaweibo/SnsAccountSwSyncSetupActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/content/pm/ProviderInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v2

    .line 92
    :cond_0
    if-eqz v2, :cond_1

    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    move-result v3

    if-nez v3, :cond_2

    .line 93
    :cond_1
    const-string v3, "SnsAccountSwAuth"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "providerLabel is null - "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 94
    const-string v2, "Life Times"

    .line 96
    :cond_2
    new-instance v3, Landroid/preference/CheckBoxPreference;

    iget-object v4, p0, Lcom/sec/android/app/sns3/auth/sp/sinaweibo/SnsAccountSwSyncSetupActivity;->mAppContext:Landroid/content/Context;

    invoke-direct {v3, v4}, Landroid/preference/CheckBoxPreference;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/sec/android/app/sns3/auth/sp/sinaweibo/SnsAccountSwSyncSetupActivity;->mLifeFeedsCheckBox:Landroid/preference/CheckBoxPreference;

    .line 97
    iget-object v3, p0, Lcom/sec/android/app/sns3/auth/sp/sinaweibo/SnsAccountSwSyncSetupActivity;->mLifeFeedsCheckBox:Landroid/preference/CheckBoxPreference;

    const v4, 0x7f080058

    new-array v5, v8, [Ljava/lang/Object;

    aput-object v2, v5, v7

    invoke-virtual {p0, v4, v5}, Lcom/sec/android/app/sns3/auth/sp/sinaweibo/SnsAccountSwSyncSetupActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/preference/CheckBoxPreference;->setTitle(Ljava/lang/CharSequence;)V

    .line 98
    iget-object v3, p0, Lcom/sec/android/app/sns3/auth/sp/sinaweibo/SnsAccountSwSyncSetupActivity;->mLifeFeedsCheckBox:Landroid/preference/CheckBoxPreference;

    const v4, 0x7f08005b

    new-array v5, v8, [Ljava/lang/Object;

    invoke-virtual {p0, v6}, Lcom/sec/android/app/sns3/auth/sp/sinaweibo/SnsAccountSwSyncSetupActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-virtual {p0, v4, v5}, Lcom/sec/android/app/sns3/auth/sp/sinaweibo/SnsAccountSwSyncSetupActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/preference/CheckBoxPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 99
    iget-object v3, p0, Lcom/sec/android/app/sns3/auth/sp/sinaweibo/SnsAccountSwSyncSetupActivity;->mLifeFeedsCheckBox:Landroid/preference/CheckBoxPreference;

    const-string v4, "snsaccount_sync_life_feeds"

    invoke-virtual {v3, v4}, Landroid/preference/CheckBoxPreference;->setKey(Ljava/lang/String;)V

    .line 100
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/SnsFeatureManager;->isSyncAdapterForProfileFeedsNeeded()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 101
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/sinaweibo/SnsAccountSwSyncSetupActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/sns3/auth/sp/sinaweibo/SnsAccountSwSyncSetupActivity;->mLifeFeedsCheckBox:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v3, v4}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 102
    iget-object v3, p0, Lcom/sec/android/app/sns3/auth/sp/sinaweibo/SnsAccountSwSyncSetupActivity;->mLifeFeedsCheckBox:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v3, v8}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 105
    :cond_3
    iget-object v3, p0, Lcom/sec/android/app/sns3/auth/sp/sinaweibo/SnsAccountSwSyncSetupActivity;->mSyncIntervalSetting:Landroid/preference/PreferenceScreen;

    new-instance v4, Lcom/sec/android/app/sns3/auth/sp/sinaweibo/SnsAccountSwSyncSetupActivity$1;

    invoke-direct {v4, p0}, Lcom/sec/android/app/sns3/auth/sp/sinaweibo/SnsAccountSwSyncSetupActivity$1;-><init>(Lcom/sec/android/app/sns3/auth/sp/sinaweibo/SnsAccountSwSyncSetupActivity;)V

    invoke-virtual {v3, v4}, Landroid/preference/PreferenceScreen;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 114
    return-void

    .line 68
    .end local v0    # "featureMgr":Lcom/sec/android/app/sns3/SnsFeatureManager;
    .end local v1    # "providerInfo":Landroid/content/pm/ProviderInfo;
    .end local v2    # "providerLabel":Ljava/lang/CharSequence;
    :cond_4
    const v3, 0x1030128

    goto/16 :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 144
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    .line 146
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/sinaweibo/SnsAccountSwSyncSetupActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->isActionbarLightTheme(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 147
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/sinaweibo/SnsAccountSwSyncSetupActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0a0001

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 151
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 149
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/sinaweibo/SnsAccountSwSyncSetupActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const/high16 v1, 0x7f0a0000

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 181
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onDestroy()V

    .line 182
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 156
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 164
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    .line 166
    :goto_0
    return v0

    .line 158
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/sinaweibo/SnsAccountSwSyncSetupActivity;->onClickDone()V

    .line 166
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 161
    :pswitch_1
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/sinaweibo/SnsAccountSwSyncSetupActivity;->finish()V

    goto :goto_1

    .line 156
    nop

    :pswitch_data_0
    .packed-switch 0x7f0b0005
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 118
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 120
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/sinaweibo/SnsAccountSwSyncSetupActivity;->mLifeFeedsCheckBox:Landroid/preference/CheckBoxPreference;

    const-string v1, "feeds"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 121
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 125
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 127
    const-string v0, "feeds"

    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/sinaweibo/SnsAccountSwSyncSetupActivity;->mLifeFeedsCheckBox:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 128
    return-void
.end method
