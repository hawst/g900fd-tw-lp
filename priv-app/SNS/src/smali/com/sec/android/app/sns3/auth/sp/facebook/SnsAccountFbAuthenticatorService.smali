.class public Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthenticatorService;
.super Landroid/app/Service;
.source "SnsAccountFbAuthenticatorService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthenticatorService$FacebookAccountAuthenticator;
    }
.end annotation


# static fields
.field private static OPTIONS_PASSWORD:Ljava/lang/String;

.field private static OPTIONS_USERNAME:Ljava/lang/String;

.field private static TAG:Ljava/lang/String;


# instance fields
.field private mAccAuth:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthenticatorService$FacebookAccountAuthenticator;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 55
    const-string v0, "SnsAccountFbAuthenticatorService"

    sput-object v0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthenticatorService;->TAG:Ljava/lang/String;

    .line 57
    const-string v0, "username"

    sput-object v0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthenticatorService;->OPTIONS_USERNAME:Ljava/lang/String;

    .line 58
    const-string v0, "password"

    sput-object v0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthenticatorService;->OPTIONS_PASSWORD:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 84
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    sget-object v0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthenticatorService;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100()Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    sget-object v0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthenticatorService;->OPTIONS_USERNAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200()Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    sget-object v0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthenticatorService;->OPTIONS_PASSWORD:Ljava/lang/String;

    return-object v0
.end method

.method private getAccountAuthenticator()Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthenticatorService$FacebookAccountAuthenticator;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthenticatorService;->mAccAuth:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthenticatorService$FacebookAccountAuthenticator;

    if-nez v0, :cond_0

    .line 80
    new-instance v0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthenticatorService$FacebookAccountAuthenticator;

    invoke-direct {v0, p0, p0}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthenticatorService$FacebookAccountAuthenticator;-><init>(Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthenticatorService;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthenticatorService;->mAccAuth:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthenticatorService$FacebookAccountAuthenticator;

    .line 81
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthenticatorService;->mAccAuth:Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthenticatorService$FacebookAccountAuthenticator;

    return-object v0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 64
    sget-object v1, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthenticatorService;->TAG:Ljava/lang/String;

    const-string v2, "FacebookAuthenticatorService : onBind"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 65
    const/4 v0, 0x0

    .line 67
    .local v0, "ret":Landroid/os/IBinder;
    const-string v1, "android.accounts.AccountAuthenticator"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 68
    invoke-direct {p0}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthenticatorService;->getAccountAuthenticator()Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthenticatorService$FacebookAccountAuthenticator;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/sns3/auth/sp/facebook/SnsAccountFbAuthenticatorService$FacebookAccountAuthenticator;->getIBinder()Landroid/os/IBinder;

    move-result-object v0

    .line 70
    :cond_0
    return-object v0
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 75
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 76
    return-void
.end method
