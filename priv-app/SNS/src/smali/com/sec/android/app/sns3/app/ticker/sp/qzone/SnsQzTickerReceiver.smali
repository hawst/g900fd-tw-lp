.class public Lcom/sec/android/app/sns3/app/ticker/sp/qzone/SnsQzTickerReceiver;
.super Landroid/content/BroadcastReceiver;
.source "SnsQzTickerReceiver.java"


# static fields
.field public static final ACTION_BOOT_COMPLETED:Ljava/lang/String; = "android.intent.action.BOOT_COMPLETED"

.field public static final ACTION_CHANGED_INTERVAL_VALUE:Ljava/lang/String; = "com.android.settings.QzoneRefreshRateChanged"

.field public static final ACTION_CHANGED_TICKER_CONTENTS_TYPE:Ljava/lang/String; = "com.android.settings.QzoneContentsTypeChanged"

.field public static final ACTION_CHANGED_TICKER_INFO:Ljava/lang/String; = "com.sec.android.daemonapp.qzone.action.SERVICE_ON_OFF"

.field public static final CONTENTS_TYPE:Ljava/lang/String; = "contents_type"

.field public static final INFORMATION_TICKER:Ljava/lang/String; = "information_ticker"

.field public static final SETTING_VALUE_TICKER_INTERVAL:Ljava/lang/String; = "information_ticker_auto_refresh"


# instance fields
.field public CONTENTS_TYPE_QZONE:I

.field public TICKERINFO_OFF:I

.field public TICKERINFO_ON:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 11
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 23
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/sns3/app/ticker/sp/qzone/SnsQzTickerReceiver;->TICKERINFO_ON:I

    .line 24
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/sns3/app/ticker/sp/qzone/SnsQzTickerReceiver;->TICKERINFO_OFF:I

    .line 26
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/app/sns3/app/ticker/sp/qzone/SnsQzTickerReceiver;->CONTENTS_TYPE_QZONE:I

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v7, 0x0

    .line 31
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 38
    .local v0, "action":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "information_ticker"

    iget v6, p0, Lcom/sec/android/app/sns3/app/ticker/sp/qzone/SnsQzTickerReceiver;->TICKERINFO_OFF:I

    invoke-static {v4, v5, v6}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    .line 39
    .local v3, "tickerInfo":I
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "contents_type"

    iget v6, p0, Lcom/sec/android/app/sns3/app/ticker/sp/qzone/SnsQzTickerReceiver;->CONTENTS_TYPE_QZONE:I

    invoke-static {v4, v5, v6}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    .line 40
    .local v2, "tickerContentsType":I
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "information_ticker_auto_refresh"

    invoke-static {v4, v5, v7}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    .line 42
    .local v1, "intervalTime":I
    sget-object v4, Lcom/sec/android/app/sns3/app/ticker/sp/qzone/SnsQzTickerResource;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "SnsQzTickerReceiver - onReceive Called, action : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 44
    if-eqz v0, :cond_0

    .line 46
    const-string v4, "android.intent.action.BOOT_COMPLETED"

    if-ne v4, v0, :cond_1

    .line 47
    iget v4, p0, Lcom/sec/android/app/sns3/app/ticker/sp/qzone/SnsQzTickerReceiver;->TICKERINFO_ON:I

    if-ne v3, v4, :cond_0

    iget v4, p0, Lcom/sec/android/app/sns3/app/ticker/sp/qzone/SnsQzTickerReceiver;->CONTENTS_TYPE_QZONE:I

    if-ne v2, v4, :cond_0

    .line 48
    invoke-static {p1, v1}, Lcom/sec/android/app/sns3/app/ticker/sp/qzone/SnsQzTickerIntervalSet;->setAutoRefresh(Landroid/content/Context;I)V

    .line 69
    :cond_0
    :goto_0
    return-void

    .line 50
    :cond_1
    const-string v4, "com.sec.android.daemonapp.qzone.action.SERVICE_ON_OFF"

    if-ne v4, v0, :cond_3

    .line 51
    iget v4, p0, Lcom/sec/android/app/sns3/app/ticker/sp/qzone/SnsQzTickerReceiver;->TICKERINFO_ON:I

    if-ne v3, v4, :cond_2

    iget v4, p0, Lcom/sec/android/app/sns3/app/ticker/sp/qzone/SnsQzTickerReceiver;->CONTENTS_TYPE_QZONE:I

    if-ne v2, v4, :cond_2

    .line 52
    invoke-static {p1, v1}, Lcom/sec/android/app/sns3/app/ticker/sp/qzone/SnsQzTickerIntervalSet;->setAutoRefresh(Landroid/content/Context;I)V

    goto :goto_0

    .line 54
    :cond_2
    invoke-static {p1}, Lcom/sec/android/app/sns3/app/ticker/sp/qzone/SnsQzTickerIntervalSet;->stopAutoRefresh(Landroid/content/Context;)V

    goto :goto_0

    .line 56
    :cond_3
    const-string v4, "com.android.settings.QzoneContentsTypeChanged"

    if-ne v4, v0, :cond_5

    .line 57
    iget v4, p0, Lcom/sec/android/app/sns3/app/ticker/sp/qzone/SnsQzTickerReceiver;->TICKERINFO_ON:I

    if-ne v3, v4, :cond_4

    iget v4, p0, Lcom/sec/android/app/sns3/app/ticker/sp/qzone/SnsQzTickerReceiver;->CONTENTS_TYPE_QZONE:I

    if-ne v2, v4, :cond_4

    .line 58
    invoke-static {p1, v1}, Lcom/sec/android/app/sns3/app/ticker/sp/qzone/SnsQzTickerIntervalSet;->setAutoRefresh(Landroid/content/Context;I)V

    goto :goto_0

    .line 60
    :cond_4
    invoke-static {p1}, Lcom/sec/android/app/sns3/app/ticker/sp/qzone/SnsQzTickerIntervalSet;->stopAutoRefresh(Landroid/content/Context;)V

    goto :goto_0

    .line 62
    :cond_5
    const-string v4, "com.android.settings.QzoneRefreshRateChanged"

    if-ne v4, v0, :cond_0

    .line 64
    const-string v4, "RefreshRate"

    invoke-virtual {p2, v4, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 66
    invoke-static {p1, v1}, Lcom/sec/android/app/sns3/app/ticker/sp/qzone/SnsQzTickerIntervalSet;->setAutoRefresh(Landroid/content/Context;I)V

    goto :goto_0
.end method
