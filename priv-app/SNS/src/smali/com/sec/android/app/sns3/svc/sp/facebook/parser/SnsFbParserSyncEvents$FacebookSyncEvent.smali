.class public interface abstract Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserSyncEvents$FacebookSyncEvent;
.super Ljava/lang/Object;
.source "SnsFbParserSyncEvents.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserSyncEvents;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "FacebookSyncEvent"
.end annotation


# static fields
.field public static final CITY:Ljava/lang/String; = "city"

.field public static final COUNTRY:Ljava/lang/String; = "country"

.field public static final CREATOR:Ljava/lang/String; = "creator"

.field public static final DESCRIPTION:Ljava/lang/String; = "description"

.field public static final EID:Ljava/lang/String; = "eid"

.field public static final END_TIME:Ljava/lang/String; = "end_time"

.field public static final EVENT_TYPE:Ljava/lang/String; = "event_type"

.field public static final FQL_RESULT_SET:Ljava/lang/String; = "fql_result_set"

.field public static final HOST:Ljava/lang/String; = "host"

.field public static final LATITUDE:Ljava/lang/String; = "latitude"

.field public static final LOCATION:Ljava/lang/String; = "location"

.field public static final LONGITUDE:Ljava/lang/String; = "longitude"

.field public static final NAME:Ljava/lang/String; = "name"

.field public static final PIC:Ljava/lang/String; = "pic"

.field public static final PIC_SMALL:Ljava/lang/String; = "pic_small"

.field public static final PRIVACY:Ljava/lang/String; = "privacy"

.field public static final RSVP_STATUS:Ljava/lang/String; = "rsvp_status"

.field public static final START_TIME:Ljava/lang/String; = "start_time"

.field public static final STATE:Ljava/lang/String; = "state"

.field public static final STREET:Ljava/lang/String; = "street"

.field public static final UPDATE_TIME:Ljava/lang/String; = "update_time"

.field public static final VENUE:Ljava/lang/String; = "venue"

.field public static final ZIP:Ljava/lang/String; = "zip"
