.class public Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAttachment;
.super Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;
.source "SnsFbCmdGetAttachment.java"


# instance fields
.field private mPostID:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Handler;Ljava/lang/String;)V
    .locals 2
    .param p1, "svcMgr"    # Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    .param p2, "cmdHandler"    # Landroid/os/Handler;
    .param p3, "postID"    # Ljava/lang/String;

    .prologue
    .line 41
    invoke-direct {p0, p2}, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;-><init>(Landroid/os/Handler;)V

    .line 43
    new-instance v1, Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;

    invoke-direct {v1}, Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;-><init>()V

    .line 45
    .local v1, "unit":Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;
    iput-object p3, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAttachment;->mPostID:Ljava/lang/String;

    .line 47
    new-instance v0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAttachment$1;

    invoke-direct {v0, p0, p1, p3}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAttachment$1;-><init>(Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAttachment;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;)V

    .line 83
    .local v0, "req":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    invoke-virtual {v1, v0}, Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;->addRequest(Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;)V

    .line 85
    invoke-virtual {p0, v1}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAttachment;->addCommandUnit(Lcom/sec/android/app/sns3/agent/command/SnsCommandUnit;)V

    .line 86
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAttachment;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAttachment;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAttachment;->mPostID:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public getUserID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAttachment;->mPostID:Ljava/lang/String;

    return-object v0
.end method

.method protected respond()Z
    .locals 5

    .prologue
    .line 94
    const-string v0, "SnsAgent"

    const-string v1, "<SnsFbCmdGetAttachment> respond()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 96
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAttachment;->getCommandCallback()Lcom/sec/android/app/sns3/agent/command/ISnsCommandCallback;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAttachment;->getCommandID()I

    move-result v1

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAttachment;->isSuccess()Z

    move-result v2

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAttachment;->getUri()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAttachment;->getResponseList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/sec/android/app/sns3/agent/command/ISnsCommandCallback;->onCmdRespond(IZLjava/lang/String;Ljava/util/List;)V

    .line 98
    const/4 v0, 0x1

    return v0
.end method
