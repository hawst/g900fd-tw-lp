.class public final Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseBirthday;
.super Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;
.source "SnsQzResponseBirthday.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseBirthday;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public mBirthdayDay:Ljava/lang/String;

.field public mBirthdayMonth:Ljava/lang/String;

.field public mFriendOpenID:Ljava/lang/String;

.field public mGender:Ljava/lang/String;

.field public mHostOpenID:Ljava/lang/String;

.field public mLunarFlag:Ljava/lang/String;

.field public mNext:Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseBirthday;

.field public mNickName:Ljava/lang/String;

.field public mOpenID:Ljava/lang/String;

.field public mPicUrl:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 49
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseBirthday$1;

    invoke-direct {v0}, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseBirthday$1;-><init>()V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseBirthday;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;-><init>()V

    .line 61
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 63
    invoke-direct {p0}, Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;-><init>()V

    .line 64
    invoke-virtual {p0, p1}, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseBirthday;->readFromParcel(Landroid/os/Parcel;)V

    .line 65
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseBirthday$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseBirthday$1;

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseBirthday;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 101
    const/4 v0, 0x0

    return v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 85
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseBirthday;->mFriendOpenID:Ljava/lang/String;

    .line 86
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseBirthday;->mHostOpenID:Ljava/lang/String;

    .line 87
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseBirthday;->mGender:Ljava/lang/String;

    .line 88
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseBirthday;->mBirthdayMonth:Ljava/lang/String;

    .line 89
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseBirthday;->mBirthdayDay:Ljava/lang/String;

    .line 90
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseBirthday;->mOpenID:Ljava/lang/String;

    .line 91
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseBirthday;->mNickName:Ljava/lang/String;

    .line 92
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseBirthday;->mLunarFlag:Ljava/lang/String;

    .line 93
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseBirthday;->mPicUrl:Ljava/lang/String;

    .line 95
    const-class v0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseBirthday;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseBirthday;

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseBirthday;->mNext:Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseBirthday;

    .line 96
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flag"    # I

    .prologue
    .line 70
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseBirthday;->mFriendOpenID:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 71
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseBirthday;->mHostOpenID:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 72
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseBirthday;->mGender:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 73
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseBirthday;->mBirthdayMonth:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 74
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseBirthday;->mBirthdayDay:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 75
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseBirthday;->mOpenID:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 76
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseBirthday;->mNickName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 77
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseBirthday;->mLunarFlag:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 78
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseBirthday;->mPicUrl:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 79
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseBirthday;->mNext:Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseBirthday;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 81
    return-void
.end method
