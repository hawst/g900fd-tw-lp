.class public interface abstract Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$TagColumns;
.super Ljava/lang/Object;
.source "SnsFacebookDB.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "TagColumns"
.end annotation


# static fields
.field public static final CREATED_TIME:Ljava/lang/String; = "created_time"

.field public static final ID:Ljava/lang/String; = "id"

.field public static final NAME:Ljava/lang/String; = "tagged_name"

.field public static final PROFILE_ID:Ljava/lang/String; = "profile_id"

.field public static final TAG_X:Ljava/lang/String; = "x"

.field public static final TAG_Y:Ljava/lang/String; = "y"

.field public static final TARGET_ID:Ljava/lang/String; = "target_id"

.field public static final TARGET_TYPE:Ljava/lang/String; = "target_type"
