.class public Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;
.super Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;
.source "SnsTwToken.java"


# static fields
.field private static final ACCESS_TOKEN:Ljava/lang/String; = "access_token"

.field private static final ACCESS_TOKEN_SECRET:Ljava/lang/String; = "access_token_secret"

.field private static final SCREEN_NAME:Ljava/lang/String; = "screen_name"

.field private static final UNAUTH_TOKEN:Ljava/lang/String; = "unauth_token"

.field private static final UNAUTH_TOKEN_SECRET:Ljava/lang/String; = "unauth_token_secret"

.field private static final USER_AUTH_TOKEN:Ljava/lang/String; = "user_auth_token"

.field private static final USER_AUTH_VERIFIER:Ljava/lang/String; = "user_auth_verifier"

.field private static final USER_ID:Ljava/lang/String; = "user_id"


# instance fields
.field private mAccessToken:Ljava/lang/String;

.field private mAccessTokenSecret:Ljava/lang/String;

.field private mScreenName:Ljava/lang/String;

.field private final mSharedPref:Landroid/content/SharedPreferences;

.field private mUnauthToken:Ljava/lang/String;

.field private mUnauthTokenSecret:Ljava/lang/String;

.field private mUserAuthToken:Ljava/lang/String;

.field private mUserAuthVerifier:Ljava/lang/String;

.field private mUserID:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;-><init>()V

    .line 45
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v0

    const-string v1, "Twitter_Token"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/sns3/SnsApplication;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;->mSharedPref:Landroid/content/SharedPreferences;

    .line 65
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;->readTokenInfo()V

    .line 66
    return-void
.end method


# virtual methods
.method public getAccessToken()Ljava/lang/String;
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;->mAccessToken:Ljava/lang/String;

    return-object v0
.end method

.method public getAccessTokenSecret()Ljava/lang/String;
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;->mAccessTokenSecret:Ljava/lang/String;

    return-object v0
.end method

.method public getScreenName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;->mScreenName:Ljava/lang/String;

    return-object v0
.end method

.method public getUnauthToken()Ljava/lang/String;
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;->mUnauthToken:Ljava/lang/String;

    return-object v0
.end method

.method public getUnauthTokenSecret()Ljava/lang/String;
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;->mUnauthTokenSecret:Ljava/lang/String;

    return-object v0
.end method

.method public getUserAuthToken()Ljava/lang/String;
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;->mUserAuthToken:Ljava/lang/String;

    return-object v0
.end method

.method public getUserAuthVerifier()Ljava/lang/String;
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;->mUserAuthVerifier:Ljava/lang/String;

    return-object v0
.end method

.method public getUserID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;->mUserID:Ljava/lang/String;

    return-object v0
.end method

.method public isValidAccessTokenNExpires()Z
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;->mAccessToken:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;->mAccessToken:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public readTokenInfo()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 189
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v1, "unauth_token"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;->mUnauthToken:Ljava/lang/String;

    .line 190
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v1, "unauth_token_secret"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;->mUnauthTokenSecret:Ljava/lang/String;

    .line 191
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v1, "user_auth_token"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;->mUserAuthToken:Ljava/lang/String;

    .line 192
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v1, "user_auth_verifier"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;->mUserAuthVerifier:Ljava/lang/String;

    .line 193
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v1, "access_token"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;->mAccessToken:Ljava/lang/String;

    .line 194
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v1, "access_token_secret"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;->mAccessTokenSecret:Ljava/lang/String;

    .line 195
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v1, "user_id"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;->mUserID:Ljava/lang/String;

    .line 196
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v1, "screen_name"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;->mScreenName:Ljava/lang/String;

    .line 198
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;->mAccessToken:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;->mAccessTokenSecret:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;->mUserID:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;->mScreenName:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 200
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;->setTokenState(I)V

    .line 203
    :goto_0
    return-void

    .line 202
    :cond_0
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;->setTokenState(I)V

    goto :goto_0
.end method

.method public removeAll()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 178
    invoke-virtual {p0, v0, v0, v0, v0}, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;->setTokenInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 180
    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;->mUnauthToken:Ljava/lang/String;

    .line 181
    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;->mUnauthTokenSecret:Ljava/lang/String;

    .line 182
    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;->mUserAuthToken:Ljava/lang/String;

    .line 183
    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;->mUserAuthVerifier:Ljava/lang/String;

    .line 184
    return-void
.end method

.method public setAccessToken(Ljava/lang/String;)V
    .locals 0
    .param p1, "accessToken"    # Ljava/lang/String;

    .prologue
    .line 89
    iput-object p1, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;->mAccessToken:Ljava/lang/String;

    .line 90
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;->writeTokenInfo()V

    .line 91
    return-void
.end method

.method public setAccessTokenSecret(Ljava/lang/String;)V
    .locals 0
    .param p1, "secret"    # Ljava/lang/String;

    .prologue
    .line 94
    iput-object p1, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;->mAccessTokenSecret:Ljava/lang/String;

    .line 95
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;->writeTokenInfo()V

    .line 96
    return-void
.end method

.method public setScreenName(Ljava/lang/String;)V
    .locals 0
    .param p1, "screenName"    # Ljava/lang/String;

    .prologue
    .line 104
    iput-object p1, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;->mScreenName:Ljava/lang/String;

    .line 105
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;->writeTokenInfo()V

    .line 106
    return-void
.end method

.method public setTokenInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "accessToken"    # Ljava/lang/String;
    .param p2, "secret"    # Ljava/lang/String;
    .param p3, "userID"    # Ljava/lang/String;
    .param p4, "screenName"    # Ljava/lang/String;

    .prologue
    .line 109
    iput-object p1, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;->mAccessToken:Ljava/lang/String;

    .line 110
    iput-object p2, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;->mAccessTokenSecret:Ljava/lang/String;

    .line 111
    iput-object p3, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;->mUserID:Ljava/lang/String;

    .line 112
    iput-object p4, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;->mScreenName:Ljava/lang/String;

    .line 114
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;->mAccessToken:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;->mAccessTokenSecret:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;->mUserID:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;->mScreenName:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 116
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;->setTokenState(I)V

    .line 120
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;->writeTokenInfo()V

    .line 121
    return-void

    .line 118
    :cond_0
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;->setTokenState(I)V

    goto :goto_0
.end method

.method public setUnauthToken(Ljava/lang/String;)V
    .locals 0
    .param p1, "unauthToken"    # Ljava/lang/String;

    .prologue
    .line 69
    iput-object p1, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;->mUnauthToken:Ljava/lang/String;

    .line 70
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;->writeTokenInfo()V

    .line 71
    return-void
.end method

.method public setUnauthTokenSecret(Ljava/lang/String;)V
    .locals 0
    .param p1, "unauthTokenSecret"    # Ljava/lang/String;

    .prologue
    .line 74
    iput-object p1, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;->mUnauthTokenSecret:Ljava/lang/String;

    .line 75
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;->writeTokenInfo()V

    .line 76
    return-void
.end method

.method public setUserAuthToken(Ljava/lang/String;)V
    .locals 0
    .param p1, "userAuthToken"    # Ljava/lang/String;

    .prologue
    .line 79
    iput-object p1, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;->mUserAuthToken:Ljava/lang/String;

    .line 80
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;->writeTokenInfo()V

    .line 81
    return-void
.end method

.method public setUserAuthVerifier(Ljava/lang/String;)V
    .locals 0
    .param p1, "userAuthVerifier"    # Ljava/lang/String;

    .prologue
    .line 84
    iput-object p1, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;->mUserAuthVerifier:Ljava/lang/String;

    .line 85
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;->writeTokenInfo()V

    .line 86
    return-void
.end method

.method public setUserID(Ljava/lang/String;)V
    .locals 0
    .param p1, "userID"    # Ljava/lang/String;

    .prologue
    .line 99
    iput-object p1, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;->mUserID:Ljava/lang/String;

    .line 100
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;->writeTokenInfo()V

    .line 101
    return-void
.end method

.method public writeTokenInfo()V
    .locals 3

    .prologue
    .line 162
    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;->mSharedPref:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 164
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "unauth_token"

    iget-object v2, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;->mUnauthToken:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 165
    const-string v1, "unauth_token_secret"

    iget-object v2, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;->mUnauthTokenSecret:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 166
    const-string v1, "user_auth_token"

    iget-object v2, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;->mUserAuthToken:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 167
    const-string v1, "user_auth_verifier"

    iget-object v2, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;->mUserAuthVerifier:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 168
    const-string v1, "access_token"

    iget-object v2, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;->mAccessToken:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 169
    const-string v1, "access_token_secret"

    iget-object v2, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;->mAccessTokenSecret:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 170
    const-string v1, "user_id"

    iget-object v2, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;->mUserID:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 171
    const-string v1, "screen_name"

    iget-object v2, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;->mScreenName:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 173
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 174
    return-void
.end method
