.class public Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;
.super Landroid/app/Service;
.source "SnsFbSyncAdapterEventService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService$SyncAdapterImpl;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "SnsFbSync"


# instance fields
.field private bEventSyncSuccess:Z

.field private mAccount:Landroid/accounts/Account;

.field private mAuthority:Ljava/lang/String;

.field private mBirthReq:Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;

.field private mBundle:Landroid/os/Bundle;

.field private mContext:Landroid/content/Context;

.field mFbToken:Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbToken;

.field private mReq:Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;

.field private mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

.field private mSyncAdapter:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService$SyncAdapterImpl;

.field private mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;

.field private mSyncResult:Landroid/content/SyncResult;

.field private mSyncState:I

.field private mbGetResponse:Z


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 63
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 67
    iput-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->mSyncAdapter:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService$SyncAdapterImpl;

    .line 69
    iput-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;

    .line 71
    iput v2, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->mSyncState:I

    .line 73
    iput-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->mSyncResult:Landroid/content/SyncResult;

    .line 75
    iput-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->mAccount:Landroid/accounts/Account;

    .line 77
    iput-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->mAuthority:Ljava/lang/String;

    .line 81
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->mBundle:Landroid/os/Bundle;

    .line 83
    iput-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->mReq:Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;

    .line 85
    iput-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->mBirthReq:Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;

    .line 87
    iput-boolean v2, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->mbGetResponse:Z

    .line 89
    iput-boolean v2, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->bEventSyncSuccess:Z

    .line 93
    iput-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->mFbToken:Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbToken;

    .line 95
    return-void
.end method

.method static synthetic access$002(Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;Landroid/content/Context;)Landroid/content/Context;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;
    .param p1, "x1"    # Landroid/content/Context;

    .prologue
    .line 63
    iput-object p1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->mContext:Landroid/content/Context;

    return-object p1
.end method

.method static synthetic access$100(Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;)Landroid/accounts/Account;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->mAccount:Landroid/accounts/Account;

    return-object v0
.end method

.method static synthetic access$102(Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;Landroid/accounts/Account;)Landroid/accounts/Account;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;
    .param p1, "x1"    # Landroid/accounts/Account;

    .prologue
    .line 63
    iput-object p1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->mAccount:Landroid/accounts/Account;

    return-object p1
.end method

.method static synthetic access$202(Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 63
    iput-object p1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->mAuthority:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$302(Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;Landroid/content/SyncResult;)Landroid/content/SyncResult;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;
    .param p1, "x1"    # Landroid/content/SyncResult;

    .prologue
    .line 63
    iput-object p1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->mSyncResult:Landroid/content/SyncResult;

    return-object p1
.end method

.method static synthetic access$400(Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/accounts/OperationCanceledException;
        }
    .end annotation

    .prologue
    .line 63
    invoke-direct {p0}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->performSync()V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;

    .prologue
    .line 63
    invoke-direct {p0}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->onSyncCanceled()V

    return-void
.end method

.method static synthetic access$602(Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;
    .param p1, "x1"    # Landroid/os/Bundle;

    .prologue
    .line 63
    iput-object p1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->mBundle:Landroid/os/Bundle;

    return-object p1
.end method

.method static synthetic access$702(Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;
    .param p1, "x1"    # I

    .prologue
    .line 63
    iput p1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->mSyncState:I

    return p1
.end method

.method static synthetic access$800(Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;)Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;

    .prologue
    .line 63
    iget-boolean v0, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->mbGetResponse:Z

    return v0
.end method

.method private handleSessionExpired()I
    .locals 11

    .prologue
    const/4 v9, 0x1

    const/4 v10, 0x0

    .line 373
    iget-object v7, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->mAccount:Landroid/accounts/Account;

    const-string v8, "com.android.calendar"

    invoke-static {v7, v8}, Landroid/content/ContentResolver;->cancelSync(Landroid/accounts/Account;Ljava/lang/String;)V

    .line 375
    iget-object v7, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->mContext:Landroid/content/Context;

    const-string v8, "notification"

    invoke-virtual {v7, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/app/NotificationManager;

    move-object v4, v7

    check-cast v4, Landroid/app/NotificationManager;

    .line 378
    .local v4, "notiMgr":Landroid/app/NotificationManager;
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 380
    .local v3, "notiIntent":Landroid/content/Intent;
    const-string v7, "com.sec.android.app.sns3.RETRY_SSO_FACEBOOK"

    invoke-virtual {v3, v7}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 384
    const/high16 v7, 0x800000

    invoke-virtual {v3, v7}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 385
    const-string v7, "RetryLogin"

    invoke-virtual {v3, v7, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 386
    iget-object v7, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->mContext:Landroid/content/Context;

    invoke-static {v7, v10, v3, v10}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 388
    .local v1, "launchIntent":Landroid/app/PendingIntent;
    const v6, 0x7f08003f

    .line 389
    .local v6, "titleID":I
    const v5, 0x7f080020

    .line 390
    .local v5, "spID":I
    const v0, 0x7f02001a

    .line 392
    .local v0, "iconID":I
    new-instance v2, Landroid/app/Notification$Builder;

    iget-object v7, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->mContext:Landroid/content/Context;

    invoke-direct {v2, v7}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    .line 393
    .local v2, "notiBuilder":Landroid/app/Notification$Builder;
    iget-object v7, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->mContext:Landroid/content/Context;

    new-array v8, v9, [Ljava/lang/Object;

    iget-object v9, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->mContext:Landroid/content/Context;

    invoke-virtual {v9, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v10

    invoke-virtual {v7, v6, v8}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v7

    invoke-virtual {v7, v0}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->mContext:Landroid/content/Context;

    const v9, 0x7f08003e

    invoke-virtual {v8, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v7

    invoke-virtual {v7, v1}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v7

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Landroid/app/Notification$Builder;->setWhen(J)Landroid/app/Notification$Builder;

    .line 397
    const/16 v7, 0x514

    invoke-virtual {v2}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v8

    invoke-virtual {v4, v7, v8}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 399
    const/4 v7, -0x1

    return v7
.end method

.method private invokeBroadcast()V
    .locals 7

    .prologue
    .line 417
    const/4 v2, 0x0

    .line 419
    .local v2, "intent":Landroid/content/Intent;
    sget-object v3, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$SyncEvent;->CONTENT_URI:Landroid/net/Uri;

    .line 420
    .local v3, "uri":Landroid/net/Uri;
    const-string v0, "com.sec.android.app.sns3.action.SYNC_EVENT"

    .line 421
    .local v0, "action":Ljava/lang/String;
    iget v4, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->mSyncState:I

    const/4 v5, 0x2

    if-ne v4, v5, :cond_1

    const/4 v1, 0x1

    .line 423
    .local v1, "bResult":Z
    :goto_0
    new-instance v2, Landroid/content/Intent;

    .end local v2    # "intent":Landroid/content/Intent;
    invoke-direct {v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 424
    .restart local v2    # "intent":Landroid/content/Intent;
    const-string v4, "SNS3_CONTENT_URI"

    invoke-virtual {v2, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 425
    const-string v4, "SNS_RESULT"

    invoke-virtual {v2, v4, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 427
    const-string v4, "com.sec.android.app.sns3.permission.RECEIVE_SNS_BROADCAST"

    invoke-virtual {p0, v2, v4}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    .line 428
    invoke-static {}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->isLoggable()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 429
    const-string v4, "SnsFbSync"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "SnsFbSyncAdapterEventService - invokeBroadcast() : action = ["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "], uri = ["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "], result = ["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 432
    :cond_0
    return-void

    .line 421
    .end local v1    # "bResult":Z
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private onSyncCanceled()V
    .locals 2

    .prologue
    .line 404
    const-string v0, "SnsFbSync"

    const-string v1, "***************** SnsFbSyncAdapterEventService : onSyncCanceled !!! *****************"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 407
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->mSyncState:I

    .line 409
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->mBundle:Landroid/os/Bundle;

    .line 410
    iget-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->mReq:Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;

    if-eqz v0, :cond_0

    .line 411
    iget-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->mReq:Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->abort()Z

    .line 413
    :cond_0
    return-void
.end method

.method private performSync()V
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/accounts/OperationCanceledException;
        }
    .end annotation

    .prologue
    const/4 v11, -0x1

    const/4 v10, 0x0

    const/4 v9, 0x2

    const/4 v8, 0x1

    .line 182
    const-string v4, "SnsFbSync"

    const-string v5, "***************** SnsFbSyncAdapterEventService : performSync - START !!! *****************"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 185
    const/4 v0, 0x0

    .line 187
    .local v0, "bDeletePrevId":Z
    iput v8, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->mSyncState:I

    .line 190
    :try_start_0
    iget-object v4, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->mFbToken:Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbToken;

    invoke-virtual {v4}, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbToken;->getTokenState()I

    move-result v4

    if-eq v4, v8, :cond_0

    iget-object v4, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->mFbToken:Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbToken;

    invoke-virtual {v4}, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbToken;->getTokenState()I

    move-result v4

    if-ne v4, v9, :cond_2

    .line 192
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->handleSessionExpired()I

    move-result v4

    iput v4, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->mSyncState:I

    .line 193
    new-instance v4, Ljava/lang/Exception;

    const-string v5, "Session expired or invalid!!!"

    invoke-direct {v4, v5}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 246
    :catch_0
    move-exception v2

    .line 248
    .local v2, "e":Ljava/lang/Exception;
    :try_start_1
    const-string v4, "SnsFbSync"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "SnsFbSyncAdapterEventService : EXCEPTION !!! "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 250
    const/4 v0, 0x1

    .line 251
    iget-object v4, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->mSyncResult:Landroid/content/SyncResult;

    iget-object v4, v4, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    const-wide/16 v6, 0x1

    iput-wide v6, v4, Landroid/content/SyncStats;->numConflictDetectedExceptions:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 255
    if-eqz v0, :cond_9

    .line 256
    iget-object v4, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;

    const-string v5, "event"

    invoke-virtual {v4, v5, v10}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->deleteSyncValue(Ljava/lang/String;I)V

    .line 265
    :goto_0
    iget-object v4, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;

    const-string v5, "event"

    invoke-virtual {v4, v5}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->deletePrevSyncState(Ljava/lang/String;)V

    .line 267
    iget-boolean v4, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->bEventSyncSuccess:Z

    if-eqz v4, :cond_1

    .line 268
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->performBirthdaySync()V

    .line 285
    :cond_1
    const-string v4, "SnsFbSync"

    const-string v5, "***************** SnsFbSyncAdapterEventService : performSync - FINISHED !!! *****************"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 289
    .end local v2    # "e":Ljava/lang/Exception;
    :goto_1
    return-void

    .line 195
    :cond_2
    const/4 v3, 0x0

    .line 197
    .local v3, "whereString":Ljava/lang/String;
    :try_start_2
    iget-object v4, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->mBundle:Landroid/os/Bundle;

    const-string v5, "limit"

    iget-object v6, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;

    const-string v6, "limit 50"

    invoke-virtual {v4, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 199
    iget-object v4, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;

    invoke-virtual {v4}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->fromCurrenttoUTCTimestamp()Ljava/lang/Long;

    move-result-object v1

    .line 200
    .local v1, "beforeTime":Ljava/lang/Long;
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const-wide/32 v6, 0x278d00

    sub-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 202
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "start_time > \'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->fromUTCTimestampToDate(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 205
    iget-object v4, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->mBundle:Landroid/os/Bundle;

    const-string v5, "where"

    invoke-virtual {v4, v5, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 207
    :cond_3
    iget-object v4, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;

    iget-object v5, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->mBundle:Landroid/os/Bundle;

    invoke-virtual {v4, v5}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->isEventLimit(Landroid/os/Bundle;)Z

    move-result v4

    if-nez v4, :cond_6

    .line 209
    new-instance v4, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService$1;

    iget-object v5, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    iget-object v6, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->mBundle:Landroid/os/Bundle;

    invoke-direct {v4, p0, v5, v6}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService$1;-><init>(Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Bundle;)V

    iput-object v4, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->mReq:Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;

    .line 236
    iget-object v4, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->mReq:Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;

    invoke-virtual {v4}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->request()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 237
    iget-object v4, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;

    invoke-virtual {v4}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->suspendSync()V

    .line 241
    :goto_2
    iget v4, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->mSyncState:I

    if-ne v4, v11, :cond_3

    .line 242
    new-instance v4, Ljava/lang/Exception;

    const-string v5, "SnsFbSyncAdapterEventService is failed!!!"

    invoke-direct {v4, v5}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 255
    .end local v1    # "beforeTime":Ljava/lang/Long;
    .end local v3    # "whereString":Ljava/lang/String;
    :catchall_0
    move-exception v4

    if-eqz v0, :cond_a

    .line 256
    iget-object v5, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;

    const-string v6, "event"

    invoke-virtual {v5, v6, v10}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->deleteSyncValue(Ljava/lang/String;I)V

    .line 265
    :goto_3
    iget-object v5, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;

    const-string v6, "event"

    invoke-virtual {v5, v6}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->deletePrevSyncState(Ljava/lang/String;)V

    .line 267
    iget-boolean v5, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->bEventSyncSuccess:Z

    if-eqz v5, :cond_4

    .line 268
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->performBirthdaySync()V

    .line 285
    :cond_4
    const-string v5, "SnsFbSync"

    const-string v6, "***************** SnsFbSyncAdapterEventService : performSync - FINISHED !!! *****************"

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    throw v4

    .line 239
    .restart local v1    # "beforeTime":Ljava/lang/Long;
    .restart local v3    # "whereString":Ljava/lang/String;
    :cond_5
    const/4 v4, -0x1

    :try_start_3
    iput v4, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->mSyncState:I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_2

    .line 255
    :cond_6
    if-eqz v0, :cond_8

    .line 256
    iget-object v4, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;

    const-string v5, "event"

    invoke-virtual {v4, v5, v10}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->deleteSyncValue(Ljava/lang/String;I)V

    .line 265
    :goto_4
    iget-object v4, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;

    const-string v5, "event"

    invoke-virtual {v4, v5}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->deletePrevSyncState(Ljava/lang/String;)V

    .line 267
    iget-boolean v4, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->bEventSyncSuccess:Z

    if-eqz v4, :cond_7

    .line 268
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->performBirthdaySync()V

    .line 285
    :cond_7
    const-string v4, "SnsFbSync"

    const-string v5, "***************** SnsFbSyncAdapterEventService : performSync - FINISHED !!! *****************"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 262
    :cond_8
    iput v9, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->mSyncState:I

    .line 263
    iput-boolean v8, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->bEventSyncSuccess:Z

    goto :goto_4

    .line 262
    .end local v1    # "beforeTime":Ljava/lang/Long;
    .end local v3    # "whereString":Ljava/lang/String;
    .restart local v2    # "e":Ljava/lang/Exception;
    :cond_9
    iput v9, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->mSyncState:I

    .line 263
    iput-boolean v8, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->bEventSyncSuccess:Z

    goto/16 :goto_0

    .line 262
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_a
    iput v9, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->mSyncState:I

    .line 263
    iput-boolean v8, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->bEventSyncSuccess:Z

    goto :goto_3
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 152
    const-string v0, "SnsFbSync"

    const-string v1, "***************** SnsFbSyncAdapterCalendarService : onBind !!! *****************"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 155
    iget-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->mSyncAdapter:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService$SyncAdapterImpl;

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService$SyncAdapterImpl;->getSyncAdapterBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 135
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 137
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/SnsApplication;->getSvcMgr()Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    .line 139
    iget-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->mSyncAdapter:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService$SyncAdapterImpl;

    if-nez v0, :cond_0

    .line 140
    new-instance v0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService$SyncAdapterImpl;

    invoke-direct {v0, p0, p0}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService$SyncAdapterImpl;-><init>(Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->mSyncAdapter:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService$SyncAdapterImpl;

    .line 143
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;

    if-nez v0, :cond_1

    .line 144
    new-instance v0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;

    invoke-direct {v0}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;

    .line 147
    :cond_1
    return-void
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 161
    const/4 v0, 0x0

    .line 162
    .local v0, "bActive":Z
    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->mAccount:Landroid/accounts/Account;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->mAuthority:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 163
    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->mAccount:Landroid/accounts/Account;

    iget-object v2, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->mAuthority:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/content/ContentResolver;->isSyncActive(Landroid/accounts/Account;Ljava/lang/String;)Z

    move-result v0

    .line 166
    :cond_0
    const-string v1, "SnsFbSync"

    const-string v2, "***************** SnsFbSyncAdapterEventService : onUnbind !!! *****************"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 169
    if-eqz v0, :cond_2

    iget v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->mSyncState:I

    const/4 v2, 0x1

    if-eq v1, v2, :cond_1

    iget v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->mSyncState:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_2

    .line 171
    :cond_1
    const-string v1, "SnsFbSync"

    const-string v2, "SnsFbSyncAdapterEventService : onUnbind : SYNC ERROR : performSync was stopped by forced abort or pending problem!!!"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 177
    :goto_0
    invoke-super {p0, p1}, Landroid/app/Service;->onUnbind(Landroid/content/Intent;)Z

    move-result v1

    return v1

    .line 175
    :cond_2
    const-string v1, "SnsFbSync"

    const-string v2, "SnsFbSyncAdapterEventService : onUnbind : COMPLETE STATE!!!"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected performBirthdaySync()V
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/accounts/OperationCanceledException;
        }
    .end annotation

    .prologue
    const/4 v5, -0x1

    const/4 v8, 0x0

    const/4 v7, 0x2

    const/4 v6, 0x1

    .line 293
    const-string v2, "SnsFbSync"

    const-string v3, "***************** SnsFbSyncAdapterEventService : performBirthdaySync - START !!! *****************"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 297
    const/4 v0, 0x0

    .line 299
    .local v0, "bDeletePrevId":Z
    iput v6, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->mSyncState:I

    .line 302
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->mFbToken:Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbToken;

    invoke-virtual {v2}, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbToken;->getTokenState()I

    move-result v2

    if-eq v2, v6, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->mFbToken:Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbToken;

    invoke-virtual {v2}, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbToken;->getTokenState()I

    move-result v2

    if-ne v2, v7, :cond_1

    .line 304
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->handleSessionExpired()I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->mSyncState:I

    .line 305
    new-instance v2, Ljava/lang/Exception;

    const-string v3, "Session expired or invalid!!!"

    invoke-direct {v2, v3}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 345
    :catch_0
    move-exception v1

    .line 347
    .local v1, "e":Ljava/lang/Exception;
    :try_start_1
    const-string v2, "SnsFbSync"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SnsFbSyncAdapterEventService : EXCEPTION !!! "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 349
    const/4 v0, 0x1

    .line 350
    iget-object v2, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->mSyncResult:Landroid/content/SyncResult;

    iget-object v2, v2, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    const-wide/16 v4, 0x1

    iput-wide v4, v2, Landroid/content/SyncStats;->numConflictDetectedExceptions:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 354
    if-eqz v0, :cond_5

    .line 355
    iget-object v2, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;

    const-string v3, "event"

    invoke-virtual {v2, v3, v8}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->deleteSyncValue(Ljava/lang/String;I)V

    .line 362
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;

    const-string v3, "event"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->deletePrevSyncState(Ljava/lang/String;)V

    .line 364
    invoke-direct {p0}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->invokeBroadcast()V

    .line 366
    const-string v2, "SnsFbSync"

    const-string v3, "***************** SnsFbSyncAdapterEventService : performBirthdaySync - FINISHED !!! *****************"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 370
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_1
    return-void

    .line 308
    :cond_1
    :try_start_2
    new-instance v2, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService$2;

    iget-object v3, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    const/4 v4, 0x0

    invoke-direct {v2, p0, v3, v4}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService$2;-><init>(Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;)V

    iput-object v2, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->mBirthReq:Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;

    .line 336
    iget-object v2, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->mBirthReq:Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;

    invoke-virtual {v2}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->request()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 337
    iget-object v2, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;

    invoke-virtual {v2}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->suspendSync()V

    .line 342
    :goto_2
    iget v2, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->mSyncState:I

    if-ne v2, v5, :cond_3

    .line 343
    new-instance v2, Ljava/lang/Exception;

    const-string v3, "SnsFbSyncAdapterEventService is failed!!!"

    invoke-direct {v2, v3}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 354
    :catchall_0
    move-exception v2

    if-eqz v0, :cond_6

    .line 355
    iget-object v3, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;

    const-string v4, "event"

    invoke-virtual {v3, v4, v8}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->deleteSyncValue(Ljava/lang/String;I)V

    .line 362
    :goto_3
    iget-object v3, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;

    const-string v4, "event"

    invoke-virtual {v3, v4}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->deletePrevSyncState(Ljava/lang/String;)V

    .line 364
    invoke-direct {p0}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->invokeBroadcast()V

    .line 366
    const-string v3, "SnsFbSync"

    const-string v4, "***************** SnsFbSyncAdapterEventService : performBirthdaySync - FINISHED !!! *****************"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    throw v2

    .line 339
    :cond_2
    const/4 v2, -0x1

    :try_start_3
    iput v2, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->mSyncState:I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_2

    .line 354
    :cond_3
    if-eqz v0, :cond_4

    .line 355
    iget-object v2, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;

    const-string v3, "event"

    invoke-virtual {v2, v3, v8}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->deleteSyncValue(Ljava/lang/String;I)V

    .line 362
    :goto_4
    iget-object v2, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;

    const-string v3, "event"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->deletePrevSyncState(Ljava/lang/String;)V

    .line 364
    invoke-direct {p0}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->invokeBroadcast()V

    .line 366
    const-string v2, "SnsFbSync"

    const-string v3, "***************** SnsFbSyncAdapterEventService : performBirthdaySync - FINISHED !!! *****************"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 357
    :cond_4
    iget-object v2, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;

    const-string v3, "event"

    invoke-virtual {v2, v3, v6}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->deleteSyncValue(Ljava/lang/String;I)V

    .line 358
    iget-object v2, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;

    const-string v3, "event"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->updateCurrentSyncValue(Ljava/lang/String;)V

    .line 360
    iput v7, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->mSyncState:I

    goto :goto_4

    .line 357
    .restart local v1    # "e":Ljava/lang/Exception;
    :cond_5
    iget-object v2, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;

    const-string v3, "event"

    invoke-virtual {v2, v3, v6}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->deleteSyncValue(Ljava/lang/String;I)V

    .line 358
    iget-object v2, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;

    const-string v3, "event"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->updateCurrentSyncValue(Ljava/lang/String;)V

    .line 360
    iput v7, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->mSyncState:I

    goto/16 :goto_0

    .line 357
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_6
    iget-object v3, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;

    const-string v4, "event"

    invoke-virtual {v3, v4, v6}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->deleteSyncValue(Ljava/lang/String;I)V

    .line 358
    iget-object v3, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;

    const-string v4, "event"

    invoke-virtual {v3, v4}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->updateCurrentSyncValue(Ljava/lang/String;)V

    .line 360
    iput v7, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->mSyncState:I

    goto :goto_3
.end method
