.class Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity$1;
.super Ljava/lang/Object;
.source "SnsAccountGpAuthSSOActivity.java"

# interfaces
.implements Landroid/accounts/AccountManagerCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/accounts/AccountManagerCallback",
        "<",
        "Landroid/os/Bundle;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;)V
    .locals 0

    .prologue
    .line 81
    iput-object p1, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity$1;->this$0:Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run(Landroid/accounts/AccountManagerFuture;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/AccountManagerFuture",
            "<",
            "Landroid/os/Bundle;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "future":Landroid/accounts/AccountManagerFuture;, "Landroid/accounts/AccountManagerFuture<Landroid/os/Bundle;>;"
    const/high16 v12, 0x20000

    const/16 v11, 0x3e8

    .line 83
    const/4 v2, 0x0

    .line 85
    .local v2, "bundle":Landroid/os/Bundle;
    :try_start_0
    invoke-interface {p1}, Landroid/accounts/AccountManagerFuture;->getResult()Ljava/lang/Object;

    move-result-object v8

    move-object v0, v8

    check-cast v0, Landroid/os/Bundle;

    move-object v2, v0

    .line 87
    new-instance v7, Landroid/os/Bundle;

    invoke-direct {v7}, Landroid/os/Bundle;-><init>()V

    .line 88
    .local v7, "result":Landroid/os/Bundle;
    const-string v8, "accountType"

    const-string v9, "com.sec.android.app.sns3.googleplus"

    invoke-virtual {v7, v8, v9}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    iget-object v8, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity$1;->this$0:Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;

    invoke-virtual {v8, v7}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->setAccountAuthenticatorResult(Landroid/os/Bundle;)V

    .line 91
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 93
    .local v4, "extra":Landroid/os/Bundle;
    iget-object v8, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity$1;->this$0:Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;

    invoke-virtual {v8}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    invoke-static {v8}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v8

    const-string v9, "com.sec.android.app.sns3.googleplus"

    invoke-virtual {v8, v9}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v1

    .line 96
    .local v1, "account":[Landroid/accounts/Account;
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/app/sns3/SnsApplication;->getFeatureMgr()Lcom/sec/android/app/sns3/SnsFeatureManager;

    move-result-object v5

    .line 99
    .local v5, "featureMgr":Lcom/sec/android/app/sns3/SnsFeatureManager;
    invoke-virtual {v5}, Lcom/sec/android/app/sns3/SnsFeatureManager;->isSyncAdapterForProfileFeedsNeeded()Z

    move-result v8

    if-eqz v8, :cond_3

    .line 100
    const/4 v8, 0x0

    aget-object v8, v1, v8

    const-string v9, "com.sec.android.app.sns3.life"

    const/4 v10, 0x1

    invoke-static {v8, v9, v10}, Landroid/content/ContentResolver;->setIsSyncable(Landroid/accounts/Account;Ljava/lang/String;I)V

    .line 101
    const/4 v8, 0x0

    aget-object v8, v1, v8

    const-string v9, "com.sec.android.app.sns3.life"

    invoke-static {v8, v9, v4}, Landroid/content/ContentResolver;->removePeriodicSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 103
    iget-object v8, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity$1;->this$0:Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;

    # getter for: Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->mSkipSyncSetup:Z
    invoke-static {v8}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->access$000(Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 104
    const/4 v8, 0x0

    aget-object v8, v1, v8

    const-string v9, "com.sec.android.app.sns3.life"

    const/4 v10, 0x1

    invoke-static {v8, v9, v10}, Landroid/content/ContentResolver;->setSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;Z)V

    .line 112
    :cond_0
    :goto_0
    invoke-virtual {v5}, Lcom/sec/android/app/sns3/SnsFeatureManager;->isSyncAdapterForCallNeeded()Z

    move-result v8

    if-eqz v8, :cond_5

    .line 113
    const/4 v8, 0x0

    aget-object v8, v1, v8

    const-string v9, "com.sec.android.app.sns3.call"

    const/4 v10, 0x1

    invoke-static {v8, v9, v10}, Landroid/content/ContentResolver;->setIsSyncable(Landroid/accounts/Account;Ljava/lang/String;I)V

    .line 114
    const/4 v8, 0x0

    aget-object v8, v1, v8

    const-string v9, "com.sec.android.app.sns3.call"

    invoke-static {v8, v9, v4}, Landroid/content/ContentResolver;->removePeriodicSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 116
    iget-object v8, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity$1;->this$0:Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;

    # getter for: Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->mSkipSyncSetup:Z
    invoke-static {v8}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->access$000(Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 117
    const/4 v8, 0x0

    aget-object v8, v1, v8

    const-string v9, "com.sec.android.app.sns3.call"

    const/4 v10, 0x1

    invoke-static {v8, v9, v10}, Landroid/content/ContentResolver;->setSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 128
    :cond_1
    :goto_1
    if-eqz v2, :cond_2

    iget-object v8, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity$1;->this$0:Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;

    # getter for: Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->mRetryLogin:Z
    invoke-static {v8}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->access$100(Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;)Z

    move-result v8

    if-nez v8, :cond_2

    iget-object v8, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity$1;->this$0:Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;

    # getter for: Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->mSkipSyncSetup:Z
    invoke-static {v8}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->access$000(Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;)Z

    move-result v8

    if-eqz v8, :cond_7

    .line 129
    :cond_2
    iget-object v8, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity$1;->this$0:Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;

    invoke-virtual {v8}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->finish()V

    .line 137
    .end local v1    # "account":[Landroid/accounts/Account;
    .end local v4    # "extra":Landroid/os/Bundle;
    .end local v5    # "featureMgr":Lcom/sec/android/app/sns3/SnsFeatureManager;
    .end local v7    # "result":Landroid/os/Bundle;
    :goto_2
    return-void

    .line 108
    .restart local v1    # "account":[Landroid/accounts/Account;
    .restart local v4    # "extra":Landroid/os/Bundle;
    .restart local v5    # "featureMgr":Lcom/sec/android/app/sns3/SnsFeatureManager;
    .restart local v7    # "result":Landroid/os/Bundle;
    :cond_3
    const/4 v8, 0x0

    :try_start_1
    aget-object v8, v1, v8

    const-string v9, "com.sec.android.app.sns3.life"

    const/4 v10, 0x0

    invoke-static {v8, v9, v10}, Landroid/content/ContentResolver;->setIsSyncable(Landroid/accounts/Account;Ljava/lang/String;I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 124
    .end local v1    # "account":[Landroid/accounts/Account;
    .end local v4    # "extra":Landroid/os/Bundle;
    .end local v5    # "featureMgr":Lcom/sec/android/app/sns3/SnsFeatureManager;
    .end local v7    # "result":Landroid/os/Bundle;
    :catch_0
    move-exception v3

    .line 125
    .local v3, "e":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    .line 126
    const-string v8, "SnsAccountGpAuthSSOActivity"

    const-string v9, "####### AccountManagerCallback : run FAILED !!!!! #######"

    invoke-static {v8, v9}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 128
    if-eqz v2, :cond_4

    iget-object v8, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity$1;->this$0:Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;

    # getter for: Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->mRetryLogin:Z
    invoke-static {v8}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->access$100(Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;)Z

    move-result v8

    if-nez v8, :cond_4

    iget-object v8, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity$1;->this$0:Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;

    # getter for: Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->mSkipSyncSetup:Z
    invoke-static {v8}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->access$000(Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;)Z

    move-result v8

    if-eqz v8, :cond_8

    .line 129
    :cond_4
    iget-object v8, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity$1;->this$0:Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;

    invoke-virtual {v8}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->finish()V

    goto :goto_2

    .line 121
    .end local v3    # "e":Ljava/lang/Exception;
    .restart local v1    # "account":[Landroid/accounts/Account;
    .restart local v4    # "extra":Landroid/os/Bundle;
    .restart local v5    # "featureMgr":Lcom/sec/android/app/sns3/SnsFeatureManager;
    .restart local v7    # "result":Landroid/os/Bundle;
    :cond_5
    const/4 v8, 0x0

    :try_start_3
    aget-object v8, v1, v8

    const-string v9, "com.sec.android.app.sns3.call"

    const/4 v10, 0x0

    invoke-static {v8, v9, v10}, Landroid/content/ContentResolver;->setIsSyncable(Landroid/accounts/Account;Ljava/lang/String;I)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 128
    .end local v1    # "account":[Landroid/accounts/Account;
    .end local v4    # "extra":Landroid/os/Bundle;
    .end local v5    # "featureMgr":Lcom/sec/android/app/sns3/SnsFeatureManager;
    .end local v7    # "result":Landroid/os/Bundle;
    :catchall_0
    move-exception v8

    if-eqz v2, :cond_6

    iget-object v9, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity$1;->this$0:Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;

    # getter for: Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->mRetryLogin:Z
    invoke-static {v9}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->access$100(Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;)Z

    move-result v9

    if-nez v9, :cond_6

    iget-object v9, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity$1;->this$0:Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;

    # getter for: Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->mSkipSyncSetup:Z
    invoke-static {v9}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->access$000(Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;)Z

    move-result v9

    if-eqz v9, :cond_9

    .line 129
    :cond_6
    iget-object v9, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity$1;->this$0:Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;

    invoke-virtual {v9}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->finish()V

    .line 135
    :goto_3
    throw v8

    .line 131
    .restart local v1    # "account":[Landroid/accounts/Account;
    .restart local v4    # "extra":Landroid/os/Bundle;
    .restart local v5    # "featureMgr":Lcom/sec/android/app/sns3/SnsFeatureManager;
    .restart local v7    # "result":Landroid/os/Bundle;
    :cond_7
    new-instance v6, Landroid/content/Intent;

    iget-object v8, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity$1;->this$0:Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;

    invoke-virtual {v8}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    const-class v9, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpSyncSetupActivity;

    invoke-direct {v6, v8, v9}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 133
    .local v6, "intent":Landroid/content/Intent;
    invoke-virtual {v6, v12}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 134
    iget-object v8, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity$1;->this$0:Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;

    invoke-virtual {v8, v6, v11}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_2

    .line 131
    .end local v1    # "account":[Landroid/accounts/Account;
    .end local v4    # "extra":Landroid/os/Bundle;
    .end local v5    # "featureMgr":Lcom/sec/android/app/sns3/SnsFeatureManager;
    .end local v6    # "intent":Landroid/content/Intent;
    .end local v7    # "result":Landroid/os/Bundle;
    .restart local v3    # "e":Ljava/lang/Exception;
    :cond_8
    new-instance v6, Landroid/content/Intent;

    iget-object v8, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity$1;->this$0:Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;

    invoke-virtual {v8}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    const-class v9, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpSyncSetupActivity;

    invoke-direct {v6, v8, v9}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 133
    .restart local v6    # "intent":Landroid/content/Intent;
    invoke-virtual {v6, v12}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 134
    iget-object v8, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity$1;->this$0:Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;

    invoke-virtual {v8, v6, v11}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_2

    .line 131
    .end local v3    # "e":Ljava/lang/Exception;
    .end local v6    # "intent":Landroid/content/Intent;
    :cond_9
    new-instance v6, Landroid/content/Intent;

    iget-object v9, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity$1;->this$0:Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;

    invoke-virtual {v9}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v9

    const-class v10, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpSyncSetupActivity;

    invoke-direct {v6, v9, v10}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 133
    .restart local v6    # "intent":Landroid/content/Intent;
    invoke-virtual {v6, v12}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 134
    iget-object v9, p0, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity$1;->this$0:Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;

    invoke-virtual {v9, v6, v11}, Lcom/sec/android/app/sns3/auth/sp/googleplus/SnsAccountGpAuthSSOActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_3
.end method
