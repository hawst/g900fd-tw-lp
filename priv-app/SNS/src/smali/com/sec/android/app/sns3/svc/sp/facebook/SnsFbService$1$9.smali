.class Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1$9;
.super Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqPostPhoto;
.source "SnsFbService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1;->postPhoto(Ljava/lang/String;Landroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/facebook/api/ISnsFacebookCallbackID;)I
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1;

.field final synthetic val$cb:Lcom/sec/android/app/sns3/svc/sp/facebook/api/ISnsFacebookCallbackID;


# direct methods
.method constructor <init>(Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;Landroid/os/Bundle;Lorg/apache/http/entity/mime/MultipartEntity;Lcom/sec/android/app/sns3/svc/sp/facebook/api/ISnsFacebookCallbackID;)V
    .locals 0
    .param p2, "x0"    # Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    .param p3, "x1"    # Ljava/lang/String;
    .param p4, "x2"    # Landroid/os/Bundle;
    .param p5, "x3"    # Lorg/apache/http/entity/mime/MultipartEntity;

    .prologue
    .line 391
    iput-object p1, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1$9;->this$1:Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1;

    iput-object p6, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1$9;->val$cb:Lcom/sec/android/app/sns3/svc/sp/facebook/api/ISnsFacebookCallbackID;

    invoke-direct {p0, p2, p3, p4, p5}, Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqPostPhoto;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;Landroid/os/Bundle;Lorg/apache/http/entity/mime/MultipartEntity;)V

    return-void
.end method


# virtual methods
.method public onReqRespond(IZIILandroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseID;)Z
    .locals 9
    .param p1, "reqID"    # I
    .param p2, "bSuccess"    # Z
    .param p3, "httpStatus"    # I
    .param p4, "errorCode"    # I
    .param p5, "reason"    # Landroid/os/Bundle;
    .param p6, "id"    # Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseID;

    .prologue
    .line 398
    :try_start_0
    new-instance v8, Landroid/content/Intent;

    const-string v0, "com.sec.android.app.sns3.svc.sp.facebook.SNS_FB_NOTIFICATION"

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1$9;->this$1:Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1;

    iget-object v2, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1;->this$0:Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;

    const-class v3, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbBroadcastReceiver;

    invoke-direct {v8, v0, v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 399
    .local v8, "intent":Landroid/content/Intent;
    const-string v0, "SNS_FB_NOTIFICATION.progress"

    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1$9;->this$1:Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1;

    iget-object v1, v1, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1;->this$0:Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;

    # getter for: Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;->mProgressNum:J
    invoke-static {v1}, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;->access$100(Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;)J

    move-result-wide v2

    invoke-virtual {v8, v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 400
    if-eqz p2, :cond_0

    .line 401
    const-string v0, "SNS_FB_NOTIFICATION.state"

    const/4 v1, 0x2

    invoke-virtual {v8, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 405
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1$9;->this$1:Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1;

    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1;->this$0:Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;

    invoke-virtual {v0, v8}, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService;->sendBroadcast(Landroid/content/Intent;)V

    .line 407
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbService$1$9;->val$cb:Lcom/sec/android/app/sns3/svc/sp/facebook/api/ISnsFacebookCallbackID;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-interface/range {v0 .. v6}, Lcom/sec/android/app/sns3/svc/sp/facebook/api/ISnsFacebookCallbackID;->onResponse(IZIILandroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseID;)V

    .line 412
    .end local v8    # "intent":Landroid/content/Intent;
    :goto_1
    const/4 v0, 0x0

    return v0

    .line 403
    .restart local v8    # "intent":Landroid/content/Intent;
    :cond_0
    const-string v0, "SNS_FB_NOTIFICATION.state"

    const/4 v1, 0x3

    invoke-virtual {v8, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 408
    .end local v8    # "intent":Landroid/content/Intent;
    :catch_0
    move-exception v7

    .line 410
    .local v7, "e":Landroid/os/RemoteException;
    invoke-virtual {v7}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1
.end method
