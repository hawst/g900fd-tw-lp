.class Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdAuthLogin$1;
.super Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqGetUser;
.source "SnsFbCmdAuthLogin.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdAuthLogin;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Handler;Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdAuthLogin;


# direct methods
.method constructor <init>(Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdAuthLogin;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;)V
    .locals 0
    .param p2, "x0"    # Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    .param p3, "x1"    # Ljava/lang/String;

    .prologue
    .line 43
    iput-object p1, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdAuthLogin$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdAuthLogin;

    invoke-direct {p0, p2, p3}, Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqGetUser;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public onReqRespond(IZIILandroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseUser;)Z
    .locals 5
    .param p1, "reqID"    # I
    .param p2, "bSuccess"    # Z
    .param p3, "httpStatus"    # I
    .param p4, "errorCode"    # I
    .param p5, "reason"    # Landroid/os/Bundle;
    .param p6, "user"    # Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseUser;

    .prologue
    .line 48
    const-string v2, "SnsAgent"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SnsFbCmdAuthLogin : getUser - onResponse() - bSuccess = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 51
    const/4 v1, 0x0

    .line 52
    .local v1, "userName":Ljava/lang/String;
    const/4 v0, 0x0

    .line 54
    .local v0, "userID":Ljava/lang/String;
    if-eqz p2, :cond_2

    if-eqz p6, :cond_2

    .line 55
    if-nez p5, :cond_0

    .line 56
    new-instance p5, Landroid/os/Bundle;

    .end local p5    # "reason":Landroid/os/Bundle;
    invoke-direct {p5}, Landroid/os/Bundle;-><init>()V

    .line 58
    .restart local p5    # "reason":Landroid/os/Bundle;
    :cond_0
    const-string v2, "userID"

    iget-object v3, p6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseUser;->mUserID:Ljava/lang/String;

    invoke-virtual {p5, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    const-string v2, "userName"

    iget-object v3, p6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseUser;->mName:Ljava/lang/String;

    invoke-virtual {p5, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    invoke-static {}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->isLoggable()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 61
    const-string v2, "SnsAgent"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SnsFbCmdAuthLogin : getUser - onResponse() - userID = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", screenName = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 72
    :cond_1
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdAuthLogin$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdAuthLogin;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdAuthLogin;->setUri(Ljava/lang/String;)V

    .line 73
    iget-object v2, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdAuthLogin$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdAuthLogin;

    invoke-virtual {v2, p2}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdAuthLogin;->setSuccess(Z)V

    .line 74
    iget-object v2, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdAuthLogin$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdAuthLogin;

    new-instance v3, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;

    const-string v4, "facebook"

    invoke-direct {v3, v4, p3, p4, p5}, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;-><init>(Ljava/lang/String;IILandroid/os/Bundle;)V

    invoke-virtual {v2, v3}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdAuthLogin;->setResponseList(Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;)Z

    .line 76
    iget-object v2, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdAuthLogin$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdAuthLogin;

    invoke-virtual {v2, p0}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdAuthLogin;->receive(Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;)V

    .line 78
    const/4 v2, 0x1

    return v2

    .line 66
    :cond_2
    const-string v2, "SnsAgent"

    const-string v3, " Get user fail"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 67
    if-eqz p5, :cond_1

    .line 68
    const-string v2, "SnsAgent"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " error message : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "error_message"

    invoke-virtual {p5, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
