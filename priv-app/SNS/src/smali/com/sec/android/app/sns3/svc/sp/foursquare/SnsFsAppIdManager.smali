.class public Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsAppIdManager;
.super Ljava/lang/Object;
.source "SnsFsAppIdManager.java"


# static fields
.field private static final DEFAULT_CLIENT_ID:Ljava/lang/String; = "EEZNI4J4D340JXFRWRHUGJ3HPXTHM20TYJ2N0OFGSW3BFADL"

.field private static final DEFAULT_CLIENT_SECRET:Ljava/lang/String; = "QXU0WKZWQY5MN1ADDLPPNEBGY0WBE0PSUYYYSKYU4T15RLB4"

.field private static final NOTE4_CLIENT_ID:Ljava/lang/String; = "TUMIQ3SCANJMHHKWIMY2XQK24VDU4QBQIU5XJA31IGTW5KXT"

.field private static final NOTE4_CLIENT_SECRET:Ljava/lang/String; = "JIAJ0HS4TI5WMQAJMAE14RQZKZZABDDS3G52G1XM2BXSRXC4"

.field private static final PROPERTIES_FILE:Ljava/lang/String; = "/etc/snsfs.conf"

.field private static final S6_CLIENT_ID:Ljava/lang/String; = "BJJGYNASNCG1UGID0K420NKCICZOU1DVMIUC3ILERY5TOLVR"

.field private static final S6_CLIENT_SECRET:Ljava/lang/String; = "DLNNKCA0IOD11QGGDTZ5CO3SX44PDBGK1URC3ZFSHLKSPDOT"

.field private static final TAG:Ljava/lang/String; = "SnsFsAppIdManager"

.field private static mInstance:Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsAppIdManager;


# instance fields
.field private mAppId:Ljava/lang/String;

.field private mSecretKey:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsAppIdManager;->mInstance:Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsAppIdManager;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsAppIdManager;->mAppId:Ljava/lang/String;

    .line 46
    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsAppIdManager;->mSecretKey:Ljava/lang/String;

    .line 49
    invoke-direct {p0}, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsAppIdManager;->loadFsAppIdAndKey()V

    .line 50
    return-void
.end method

.method private findSecretKey(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "appId"    # Ljava/lang/String;

    .prologue
    .line 88
    const/4 v0, 0x0

    .line 90
    .local v0, "secretKey":Ljava/lang/String;
    const-string v1, "EEZNI4J4D340JXFRWRHUGJ3HPXTHM20TYJ2N0OFGSW3BFADL"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 91
    const-string v0, "QXU0WKZWQY5MN1ADDLPPNEBGY0WBE0PSUYYYSKYU4T15RLB4"

    .line 98
    :cond_0
    :goto_0
    return-object v0

    .line 92
    :cond_1
    const-string v1, "TUMIQ3SCANJMHHKWIMY2XQK24VDU4QBQIU5XJA31IGTW5KXT"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 93
    const-string v0, "JIAJ0HS4TI5WMQAJMAE14RQZKZZABDDS3G52G1XM2BXSRXC4"

    goto :goto_0

    .line 94
    :cond_2
    const-string v1, "BJJGYNASNCG1UGID0K420NKCICZOU1DVMIUC3ILERY5TOLVR"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 95
    const-string v0, "DLNNKCA0IOD11QGGDTZ5CO3SX44PDBGK1URC3ZFSHLKSPDOT"

    goto :goto_0
.end method

.method public static getInstance()Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsAppIdManager;
    .locals 1

    .prologue
    .line 102
    sget-object v0, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsAppIdManager;->mInstance:Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsAppIdManager;

    if-nez v0, :cond_0

    .line 103
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsAppIdManager;

    invoke-direct {v0}, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsAppIdManager;-><init>()V

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsAppIdManager;->mInstance:Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsAppIdManager;

    .line 105
    :cond_0
    sget-object v0, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsAppIdManager;->mInstance:Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsAppIdManager;

    return-object v0
.end method

.method private loadFsAppIdAndKey()V
    .locals 10

    .prologue
    .line 53
    new-instance v3, Ljava/util/Properties;

    invoke-direct {v3}, Ljava/util/Properties;-><init>()V

    .line 54
    .local v3, "properties":Ljava/util/Properties;
    new-instance v2, Ljava/io/File;

    const-string v7, "/etc/snsfs.conf"

    invoke-direct {v2, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 55
    .local v2, "file":Ljava/io/File;
    const/4 v5, 0x0

    .line 58
    .local v5, "stream":Ljava/io/FileInputStream;
    :try_start_0
    new-instance v6, Ljava/io/FileInputStream;

    invoke-direct {v6, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 59
    .end local v5    # "stream":Ljava/io/FileInputStream;
    .local v6, "stream":Ljava/io/FileInputStream;
    :try_start_1
    invoke-virtual {v3, v6}, Ljava/util/Properties;->load(Ljava/io/InputStream;)V

    .line 61
    const-string v7, "ID"

    invoke-virtual {v3, v7}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 62
    .local v0, "appId":Ljava/lang/String;
    invoke-direct {p0, v0}, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsAppIdManager;->findSecretKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 64
    .local v4, "secretKey":Ljava/lang/String;
    const-string v7, "SnsFsAppIdManager"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "FS : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 66
    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsAppIdManager;->mAppId:Ljava/lang/String;

    .line 67
    iput-object v4, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsAppIdManager;->mSecretKey:Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 71
    if-eqz v6, :cond_4

    .line 73
    :try_start_2
    invoke-virtual {v6}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-object v5, v6

    .line 80
    .end local v0    # "appId":Ljava/lang/String;
    .end local v4    # "secretKey":Ljava/lang/String;
    .end local v6    # "stream":Ljava/io/FileInputStream;
    .restart local v5    # "stream":Ljava/io/FileInputStream;
    :cond_0
    :goto_0
    iget-object v7, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsAppIdManager;->mAppId:Ljava/lang/String;

    if-eqz v7, :cond_1

    iget-object v7, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsAppIdManager;->mSecretKey:Ljava/lang/String;

    if-nez v7, :cond_2

    .line 82
    :cond_1
    const-string v7, "EEZNI4J4D340JXFRWRHUGJ3HPXTHM20TYJ2N0OFGSW3BFADL"

    iput-object v7, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsAppIdManager;->mAppId:Ljava/lang/String;

    .line 83
    const-string v7, "QXU0WKZWQY5MN1ADDLPPNEBGY0WBE0PSUYYYSKYU4T15RLB4"

    iput-object v7, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsAppIdManager;->mSecretKey:Ljava/lang/String;

    .line 85
    :cond_2
    return-void

    .line 74
    .end local v5    # "stream":Ljava/io/FileInputStream;
    .restart local v0    # "appId":Ljava/lang/String;
    .restart local v4    # "secretKey":Ljava/lang/String;
    .restart local v6    # "stream":Ljava/io/FileInputStream;
    :catch_0
    move-exception v1

    .line 75
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    move-object v5, v6

    .line 76
    .end local v6    # "stream":Ljava/io/FileInputStream;
    .restart local v5    # "stream":Ljava/io/FileInputStream;
    goto :goto_0

    .line 68
    .end local v0    # "appId":Ljava/lang/String;
    .end local v1    # "e":Ljava/io/IOException;
    .end local v4    # "secretKey":Ljava/lang/String;
    :catch_1
    move-exception v1

    .line 69
    .restart local v1    # "e":Ljava/io/IOException;
    :goto_1
    :try_start_3
    const-string v7, "SnsFsAppIdManager"

    const-string v8, "FS configuration file not existed"

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 71
    if-eqz v5, :cond_0

    .line 73
    :try_start_4
    invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 74
    :catch_2
    move-exception v1

    .line 75
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 71
    .end local v1    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v7

    :goto_2
    if-eqz v5, :cond_3

    .line 73
    :try_start_5
    invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 76
    :cond_3
    :goto_3
    throw v7

    .line 74
    :catch_3
    move-exception v1

    .line 75
    .restart local v1    # "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 71
    .end local v1    # "e":Ljava/io/IOException;
    .end local v5    # "stream":Ljava/io/FileInputStream;
    .restart local v6    # "stream":Ljava/io/FileInputStream;
    :catchall_1
    move-exception v7

    move-object v5, v6

    .end local v6    # "stream":Ljava/io/FileInputStream;
    .restart local v5    # "stream":Ljava/io/FileInputStream;
    goto :goto_2

    .line 68
    .end local v5    # "stream":Ljava/io/FileInputStream;
    .restart local v6    # "stream":Ljava/io/FileInputStream;
    :catch_4
    move-exception v1

    move-object v5, v6

    .end local v6    # "stream":Ljava/io/FileInputStream;
    .restart local v5    # "stream":Ljava/io/FileInputStream;
    goto :goto_1

    .end local v5    # "stream":Ljava/io/FileInputStream;
    .restart local v0    # "appId":Ljava/lang/String;
    .restart local v4    # "secretKey":Ljava/lang/String;
    .restart local v6    # "stream":Ljava/io/FileInputStream;
    :cond_4
    move-object v5, v6

    .end local v6    # "stream":Ljava/io/FileInputStream;
    .restart local v5    # "stream":Ljava/io/FileInputStream;
    goto :goto_0
.end method


# virtual methods
.method public getClientId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsAppIdManager;->mAppId:Ljava/lang/String;

    return-object v0
.end method

.method public getClientSecret()Ljava/lang/String;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/foursquare/SnsFsAppIdManager;->mSecretKey:Ljava/lang/String;

    return-object v0
.end method
