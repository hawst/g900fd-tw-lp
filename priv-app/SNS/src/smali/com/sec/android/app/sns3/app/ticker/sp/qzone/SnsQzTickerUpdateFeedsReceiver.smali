.class public Lcom/sec/android/app/sns3/app/ticker/sp/qzone/SnsQzTickerUpdateFeedsReceiver;
.super Landroid/content/BroadcastReceiver;
.source "SnsQzTickerUpdateFeedsReceiver.java"


# static fields
.field private static final QZONE_FEED_COUNT_EXTRA:Ljava/lang/String; = "qzone_feed_count"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 15
    sget-object v1, Lcom/sec/android/app/sns3/app/ticker/sp/qzone/SnsQzTickerResource;->TAG:Ljava/lang/String;

    const-string v2, "SnsQzTickerUpdateFeedsReceiver - onReceive called"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 16
    const-string v1, "qzone_feed_count"

    const/16 v2, 0xa

    invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 17
    .local v0, "feed_count":I
    invoke-static {p1, v0}, Lcom/sec/android/app/sns3/app/ticker/sp/qzone/SnsQzTickerGetHome;->loadSnsQzCmdGetHome(Landroid/content/Context;I)V

    .line 18
    return-void
.end method
