.class public interface abstract Lcom/sec/android/app/sns3/svc/sp/instagram/parser/SnsInParserUser$InstagramUser;
.super Ljava/lang/Object;
.source "SnsInParserUser.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/svc/sp/instagram/parser/SnsInParserUser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "InstagramUser"
.end annotation


# static fields
.field public static final DATA:Ljava/lang/String; = "data"

.field public static final USER_ID:Ljava/lang/String; = "id"

.field public static final USER_NAME:Ljava/lang/String; = "username"
