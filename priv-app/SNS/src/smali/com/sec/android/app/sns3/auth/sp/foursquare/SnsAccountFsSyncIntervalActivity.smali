.class public Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsSyncIntervalActivity;
.super Landroid/preference/PreferenceActivity;
.source "SnsAccountFsSyncIntervalActivity.java"


# static fields
.field private static final DEFAULT_SYNC_INTERVAL:J = 0x0L

.field private static final SYNC_INTERVAL_KEY:Ljava/lang/String; = "sync_interval"

.field private static final TAG:Ljava/lang/String; = "SnsAccountFsAuth"


# instance fields
.field private mAppContext:Landroid/content/Context;

.field private mFeedsIntervalPreference:Landroid/preference/ListPreference;

.field private mProfileFeedSharedPref:Landroid/content/SharedPreferences;

.field private mProfilesIntervalPreference:Landroid/preference/ListPreference;

.field private mProfilesSharedPref:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 46
    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    .line 56
    iput-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsSyncIntervalActivity;->mFeedsIntervalPreference:Landroid/preference/ListPreference;

    .line 58
    iput-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsSyncIntervalActivity;->mProfilesIntervalPreference:Landroid/preference/ListPreference;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsSyncIntervalActivity;)Landroid/preference/ListPreference;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsSyncIntervalActivity;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsSyncIntervalActivity;->mFeedsIntervalPreference:Landroid/preference/ListPreference;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsSyncIntervalActivity;)Landroid/preference/ListPreference;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsSyncIntervalActivity;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsSyncIntervalActivity;->mProfilesIntervalPreference:Landroid/preference/ListPreference;

    return-object v0
.end method


# virtual methods
.method isTable()Z
    .locals 2

    .prologue
    .line 294
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsSyncIntervalActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit8 v0, v0, 0x3

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsSyncIntervalActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    .line 297
    :cond_0
    const/4 v0, 0x1

    .line 299
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 14
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const v13, 0x7f030003

    const v12, 0x7f030002

    const v11, 0x7f080009

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 67
    iput-object p0, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsSyncIntervalActivity;->mAppContext:Landroid/content/Context;

    .line 69
    iget-object v6, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsSyncIntervalActivity;->mAppContext:Landroid/content/Context;

    invoke-static {v6}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->isLightTheme(Landroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_a

    const v6, 0x103012b

    :goto_0
    invoke-virtual {p0, v6}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsSyncIntervalActivity;->setTheme(I)V

    .line 72
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 74
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsSyncIntervalActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 75
    .local v0, "actionBar":Landroid/app/ActionBar;
    invoke-virtual {v0, v10}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 76
    invoke-virtual {v0, v9}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 78
    const v6, 0x7f030005

    invoke-virtual {p0, v6}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsSyncIntervalActivity;->setContentView(I)V

    .line 79
    const v6, 0x7f080046

    new-array v7, v10, [Ljava/lang/Object;

    const v8, 0x7f080021

    invoke-virtual {p0, v8}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsSyncIntervalActivity;->getString(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v9

    invoke-virtual {p0, v6, v7}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsSyncIntervalActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsSyncIntervalActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 80
    const v6, 0x7f04000d

    invoke-virtual {p0, v6}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsSyncIntervalActivity;->addPreferencesFromResource(I)V

    .line 82
    const-string v6, "sync_interval"

    invoke-virtual {p0, v6}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsSyncIntervalActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    check-cast v2, Landroid/preference/PreferenceScreen;

    .line 84
    .local v2, "preferenceScreen":Landroid/preference/PreferenceScreen;
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v6

    const-string v7, "com.sec.android.app.sns3.foursquare"

    invoke-virtual {v6, v7}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v5

    .line 87
    .local v5, "sns3Account":[Landroid/accounts/Account;
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/sns3/SnsApplication;->getFeatureMgr()Lcom/sec/android/app/sns3/SnsFeatureManager;

    move-result-object v1

    .line 88
    .local v1, "featureMgr":Lcom/sec/android/app/sns3/SnsFeatureManager;
    const/4 v4, 0x0

    .line 89
    .local v4, "providerLabel":Ljava/lang/CharSequence;
    const/4 v3, 0x0

    .line 92
    .local v3, "providerInfo":Landroid/content/pm/ProviderInfo;
    invoke-virtual {v1}, Lcom/sec/android/app/sns3/SnsFeatureManager;->isSyncAdapterForProfileFeedsNeeded()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 93
    const/4 v4, 0x0

    .line 94
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsSyncIntervalActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    const-string v7, "com.sec.android.app.sns3.life"

    invoke-virtual {v6, v7, v9}, Landroid/content/pm/PackageManager;->resolveContentProvider(Ljava/lang/String;I)Landroid/content/pm/ProviderInfo;

    move-result-object v3

    .line 95
    if-eqz v3, :cond_0

    .line 96
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsSyncIntervalActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    invoke-virtual {v3, v6}, Landroid/content/pm/ProviderInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v4

    .line 98
    :cond_0
    if-eqz v4, :cond_1

    invoke-interface {v4}, Ljava/lang/CharSequence;->length()I

    move-result v6

    if-nez v6, :cond_2

    .line 99
    :cond_1
    const-string v6, "SnsAccountFsAuth"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "providerLabel is null - "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 100
    const-string v4, "Life Times"

    .line 103
    :cond_2
    new-instance v6, Landroid/preference/ListPreference;

    invoke-direct {v6, p0}, Landroid/preference/ListPreference;-><init>(Landroid/content/Context;)V

    iput-object v6, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsSyncIntervalActivity;->mFeedsIntervalPreference:Landroid/preference/ListPreference;

    .line 104
    iget-object v6, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsSyncIntervalActivity;->mFeedsIntervalPreference:Landroid/preference/ListPreference;

    new-array v7, v10, [Ljava/lang/Object;

    aput-object v4, v7, v9

    invoke-virtual {p0, v11, v7}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsSyncIntervalActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/preference/ListPreference;->setTitle(Ljava/lang/CharSequence;)V

    .line 106
    iget-object v6, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsSyncIntervalActivity;->mFeedsIntervalPreference:Landroid/preference/ListPreference;

    invoke-virtual {v6, v9}, Landroid/preference/ListPreference;->setPersistent(Z)V

    .line 107
    iget-object v6, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsSyncIntervalActivity;->mFeedsIntervalPreference:Landroid/preference/ListPreference;

    const/high16 v7, 0x7f050000

    invoke-virtual {v6, v7}, Landroid/preference/ListPreference;->setEntries(I)V

    .line 108
    iget-object v6, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsSyncIntervalActivity;->mFeedsIntervalPreference:Landroid/preference/ListPreference;

    const v7, 0x7f050001

    invoke-virtual {v6, v7}, Landroid/preference/ListPreference;->setEntryValues(I)V

    .line 109
    sget-object v6, Lcom/sec/android/app/sns3/SnsFeatureManager;->ANGLE_INDICATION_SUPPORTED:Ljava/lang/Boolean;

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 110
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsSyncIntervalActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->isLightTheme(Landroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_b

    .line 111
    iget-object v6, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsSyncIntervalActivity;->mFeedsIntervalPreference:Landroid/preference/ListPreference;

    invoke-virtual {v6, v13}, Landroid/preference/ListPreference;->setWidgetLayoutResource(I)V

    .line 116
    :cond_3
    :goto_1
    iget-object v6, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsSyncIntervalActivity;->mFeedsIntervalPreference:Landroid/preference/ListPreference;

    new-array v7, v10, [Ljava/lang/Object;

    aput-object v4, v7, v9

    invoke-virtual {p0, v11, v7}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsSyncIntervalActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/preference/ListPreference;->setDialogTitle(Ljava/lang/CharSequence;)V

    .line 119
    array-length v6, v5

    if-lez v6, :cond_c

    .line 120
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsSyncIntervalActivity;->readFeedsSyncInterval()V

    .line 122
    iget-object v6, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsSyncIntervalActivity;->mFeedsIntervalPreference:Landroid/preference/ListPreference;

    new-instance v7, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsSyncIntervalActivity$1;

    invoke-direct {v7, p0}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsSyncIntervalActivity$1;-><init>(Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsSyncIntervalActivity;)V

    invoke-virtual {v6, v7}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 162
    :goto_2
    iget-object v6, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsSyncIntervalActivity;->mFeedsIntervalPreference:Landroid/preference/ListPreference;

    invoke-virtual {v2, v6}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 166
    :cond_4
    invoke-virtual {v1}, Lcom/sec/android/app/sns3/SnsFeatureManager;->isSyncAdapterForProfilesNeeded()Z

    move-result v6

    if-eqz v6, :cond_9

    .line 167
    const/4 v4, 0x0

    .line 168
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsSyncIntervalActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    const-string v7, "com.sec.android.app.sns3.profiles"

    invoke-virtual {v6, v7, v9}, Landroid/content/pm/PackageManager;->resolveContentProvider(Ljava/lang/String;I)Landroid/content/pm/ProviderInfo;

    move-result-object v3

    .line 169
    if-eqz v3, :cond_5

    .line 170
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsSyncIntervalActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    invoke-virtual {v3, v6}, Landroid/content/pm/ProviderInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v4

    .line 172
    :cond_5
    if-eqz v4, :cond_6

    invoke-interface {v4}, Ljava/lang/CharSequence;->length()I

    move-result v6

    if-nez v6, :cond_7

    .line 173
    :cond_6
    const-string v6, "SnsAccountFsAuth"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "providerLabel is null - "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 174
    const-string v4, "Profiles"

    .line 177
    :cond_7
    new-instance v6, Landroid/preference/ListPreference;

    invoke-direct {v6, p0}, Landroid/preference/ListPreference;-><init>(Landroid/content/Context;)V

    iput-object v6, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsSyncIntervalActivity;->mProfilesIntervalPreference:Landroid/preference/ListPreference;

    .line 178
    iget-object v6, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsSyncIntervalActivity;->mProfilesIntervalPreference:Landroid/preference/ListPreference;

    new-array v7, v10, [Ljava/lang/Object;

    aput-object v4, v7, v9

    invoke-virtual {p0, v11, v7}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsSyncIntervalActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/preference/ListPreference;->setTitle(Ljava/lang/CharSequence;)V

    .line 180
    iget-object v6, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsSyncIntervalActivity;->mProfilesIntervalPreference:Landroid/preference/ListPreference;

    invoke-virtual {v6, v9}, Landroid/preference/ListPreference;->setPersistent(Z)V

    .line 181
    iget-object v6, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsSyncIntervalActivity;->mProfilesIntervalPreference:Landroid/preference/ListPreference;

    const/high16 v7, 0x7f050000

    invoke-virtual {v6, v7}, Landroid/preference/ListPreference;->setEntries(I)V

    .line 182
    iget-object v6, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsSyncIntervalActivity;->mProfilesIntervalPreference:Landroid/preference/ListPreference;

    const v7, 0x7f050001

    invoke-virtual {v6, v7}, Landroid/preference/ListPreference;->setEntryValues(I)V

    .line 183
    sget-object v6, Lcom/sec/android/app/sns3/SnsFeatureManager;->ANGLE_INDICATION_SUPPORTED:Ljava/lang/Boolean;

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    if-eqz v6, :cond_8

    .line 184
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsSyncIntervalActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->isLightTheme(Landroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_d

    .line 185
    iget-object v6, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsSyncIntervalActivity;->mProfilesIntervalPreference:Landroid/preference/ListPreference;

    invoke-virtual {v6, v13}, Landroid/preference/ListPreference;->setWidgetLayoutResource(I)V

    .line 190
    :cond_8
    :goto_3
    iget-object v6, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsSyncIntervalActivity;->mProfilesIntervalPreference:Landroid/preference/ListPreference;

    new-array v7, v10, [Ljava/lang/Object;

    aput-object v4, v7, v9

    invoke-virtual {p0, v11, v7}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsSyncIntervalActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/preference/ListPreference;->setDialogTitle(Ljava/lang/CharSequence;)V

    .line 193
    array-length v6, v5

    if-lez v6, :cond_e

    .line 194
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsSyncIntervalActivity;->readProfilesSyncInterval()V

    .line 196
    iget-object v6, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsSyncIntervalActivity;->mProfilesIntervalPreference:Landroid/preference/ListPreference;

    new-instance v7, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsSyncIntervalActivity$2;

    invoke-direct {v7, p0}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsSyncIntervalActivity$2;-><init>(Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsSyncIntervalActivity;)V

    invoke-virtual {v6, v7}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 236
    :goto_4
    iget-object v6, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsSyncIntervalActivity;->mProfilesIntervalPreference:Landroid/preference/ListPreference;

    invoke-virtual {v2, v6}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 239
    :cond_9
    return-void

    .line 69
    .end local v0    # "actionBar":Landroid/app/ActionBar;
    .end local v1    # "featureMgr":Lcom/sec/android/app/sns3/SnsFeatureManager;
    .end local v2    # "preferenceScreen":Landroid/preference/PreferenceScreen;
    .end local v3    # "providerInfo":Landroid/content/pm/ProviderInfo;
    .end local v4    # "providerLabel":Ljava/lang/CharSequence;
    .end local v5    # "sns3Account":[Landroid/accounts/Account;
    :cond_a
    const v6, 0x1030128

    goto/16 :goto_0

    .line 113
    .restart local v0    # "actionBar":Landroid/app/ActionBar;
    .restart local v1    # "featureMgr":Lcom/sec/android/app/sns3/SnsFeatureManager;
    .restart local v2    # "preferenceScreen":Landroid/preference/PreferenceScreen;
    .restart local v3    # "providerInfo":Landroid/content/pm/ProviderInfo;
    .restart local v4    # "providerLabel":Ljava/lang/CharSequence;
    .restart local v5    # "sns3Account":[Landroid/accounts/Account;
    :cond_b
    iget-object v6, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsSyncIntervalActivity;->mFeedsIntervalPreference:Landroid/preference/ListPreference;

    invoke-virtual {v6, v12}, Landroid/preference/ListPreference;->setWidgetLayoutResource(I)V

    goto/16 :goto_1

    .line 159
    :cond_c
    iget-object v6, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsSyncIntervalActivity;->mFeedsIntervalPreference:Landroid/preference/ListPreference;

    invoke-virtual {v6, v9}, Landroid/preference/ListPreference;->setEnabled(Z)V

    goto/16 :goto_2

    .line 187
    :cond_d
    iget-object v6, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsSyncIntervalActivity;->mProfilesIntervalPreference:Landroid/preference/ListPreference;

    invoke-virtual {v6, v12}, Landroid/preference/ListPreference;->setWidgetLayoutResource(I)V

    goto :goto_3

    .line 233
    :cond_e
    iget-object v6, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsSyncIntervalActivity;->mProfilesIntervalPreference:Landroid/preference/ListPreference;

    invoke-virtual {v6, v9}, Landroid/preference/ListPreference;->setEnabled(Z)V

    goto :goto_4
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 244
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 249
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 246
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsSyncIntervalActivity;->finish()V

    .line 247
    const/4 v0, 0x1

    goto :goto_0

    .line 244
    nop

    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 254
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onResume()V

    .line 255
    return-void
.end method

.method public readFeedsSyncInterval()V
    .locals 6

    .prologue
    .line 258
    const-string v1, "FS_profile_feed_sync_interval"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsSyncIntervalActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsSyncIntervalActivity;->mProfileFeedSharedPref:Landroid/content/SharedPreferences;

    .line 260
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsSyncIntervalActivity;->mProfileFeedSharedPref:Landroid/content/SharedPreferences;

    const-string v2, "FS_profile_feed_sync_interval"

    const-wide/16 v4, 0x0

    invoke-interface {v1, v2, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    .line 263
    .local v0, "savedFeedsInterval":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsSyncIntervalActivity;->mFeedsIntervalPreference:Landroid/preference/ListPreference;

    invoke-virtual {v1, v0}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 265
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsSyncIntervalActivity;->mFeedsIntervalPreference:Landroid/preference/ListPreference;

    invoke-virtual {v1}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 266
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsSyncIntervalActivity;->mFeedsIntervalPreference:Landroid/preference/ListPreference;

    iget-object v2, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsSyncIntervalActivity;->mFeedsIntervalPreference:Landroid/preference/ListPreference;

    invoke-virtual {v2}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 267
    :cond_0
    return-void
.end method

.method public readProfilesSyncInterval()V
    .locals 6

    .prologue
    .line 276
    const-string v1, "FS_profile_sync_interval"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsSyncIntervalActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsSyncIntervalActivity;->mProfilesSharedPref:Landroid/content/SharedPreferences;

    .line 278
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsSyncIntervalActivity;->mProfilesSharedPref:Landroid/content/SharedPreferences;

    const-string v2, "FS_profile_sync_interval"

    const-wide/16 v4, 0x0

    invoke-interface {v1, v2, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    .line 281
    .local v0, "savedProfilesInterval":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsSyncIntervalActivity;->mProfilesIntervalPreference:Landroid/preference/ListPreference;

    invoke-virtual {v1, v0}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 283
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsSyncIntervalActivity;->mProfilesIntervalPreference:Landroid/preference/ListPreference;

    invoke-virtual {v1}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 284
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsSyncIntervalActivity;->mProfilesIntervalPreference:Landroid/preference/ListPreference;

    iget-object v2, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsSyncIntervalActivity;->mProfilesIntervalPreference:Landroid/preference/ListPreference;

    invoke-virtual {v2}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 285
    :cond_0
    return-void
.end method

.method public writeFeedsInterval(Ljava/lang/Long;)V
    .locals 4
    .param p1, "value"    # Ljava/lang/Long;

    .prologue
    .line 270
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsSyncIntervalActivity;->mProfileFeedSharedPref:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 271
    .local v0, "profileFeedEditor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "FS_profile_feed_sync_interval"

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 272
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 273
    return-void
.end method

.method public writeProfilesInterval(Ljava/lang/Long;)V
    .locals 4
    .param p1, "value"    # Ljava/lang/Long;

    .prologue
    .line 288
    iget-object v1, p0, Lcom/sec/android/app/sns3/auth/sp/foursquare/SnsAccountFsSyncIntervalActivity;->mProfilesSharedPref:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 289
    .local v0, "profilesEditor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "FS_profile_sync_interval"

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 290
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 291
    return-void
.end method
