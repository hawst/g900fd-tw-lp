.class public Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpToken;
.super Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;
.source "SnsGpToken.java"


# static fields
.field public static final ACCESS_TOKEN:Ljava/lang/String; = "access_token"

.field private static final ACCOUNT_NAME:Ljava/lang/String; = "account_name"

.field public static final EXPIRES:Ljava/lang/String; = "expires_in"

.field public static final PROFILE_PHOTO:Ljava/lang/String; = "profile_photo"

.field public static final USERNAME:Ljava/lang/String; = "username"

.field public static final USER_ID:Ljava/lang/String; = "user_id"


# instance fields
.field private mAccessToken:Ljava/lang/String;

.field private mAccountName:Ljava/lang/String;

.field private mProfilePhoto:Ljava/lang/String;

.field private mRefreshToken:Ljava/lang/String;

.field private final mSharedPref:Landroid/content/SharedPreferences;

.field private mUserID:Ljava/lang/String;

.field private mUserName:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;-><init>()V

    .line 56
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v0

    const-string v1, "GooglePlus_Token"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/sns3/SnsApplication;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpToken;->mSharedPref:Landroid/content/SharedPreferences;

    .line 58
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpToken;->readTokenInfo()V

    .line 59
    return-void
.end method


# virtual methods
.method public getAccessToken()Ljava/lang/String;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpToken;->mAccessToken:Ljava/lang/String;

    return-object v0
.end method

.method public getAccountName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpToken;->mAccountName:Ljava/lang/String;

    return-object v0
.end method

.method public getProfilePhoto()Ljava/lang/String;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpToken;->mProfilePhoto:Ljava/lang/String;

    return-object v0
.end method

.method public getRefreshToken()Ljava/lang/String;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpToken;->mRefreshToken:Ljava/lang/String;

    return-object v0
.end method

.method public getUserID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpToken;->mUserID:Ljava/lang/String;

    return-object v0
.end method

.method public getUserName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpToken;->mUserName:Ljava/lang/String;

    return-object v0
.end method

.method public isValidAccessTokenNExpires()Z
    .locals 2

    .prologue
    .line 64
    const/4 v0, 0x0

    .line 66
    .local v0, "isValid":Z
    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpToken;->mAccessToken:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpToken;->mAccessToken:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpToken;->mRefreshToken:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpToken;->mRefreshToken:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    .line 68
    const/4 v0, 0x1

    .line 70
    :cond_0
    return v0
.end method

.method public readTokenInfo()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 150
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpToken;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v1, "access_token"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpToken;->mAccessToken:Ljava/lang/String;

    .line 151
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpToken;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v1, "expires_in"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpToken;->mRefreshToken:Ljava/lang/String;

    .line 153
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpToken;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v1, "username"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpToken;->mUserName:Ljava/lang/String;

    .line 154
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpToken;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v1, "user_id"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpToken;->mUserID:Ljava/lang/String;

    .line 155
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpToken;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v1, "profile_photo"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpToken;->mProfilePhoto:Ljava/lang/String;

    .line 156
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpToken;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v1, "account_name"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpToken;->mAccountName:Ljava/lang/String;

    .line 157
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpToken;->mAccessToken:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpToken;->mAccessToken:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpToken;->mRefreshToken:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpToken;->mRefreshToken:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 159
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpToken;->setTokenState(I)V

    .line 162
    :goto_0
    return-void

    .line 161
    :cond_0
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpToken;->setTokenState(I)V

    goto :goto_0
.end method

.method public removeAll()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 125
    invoke-virtual {p0, v0, v0}, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpToken;->setAccessTokenNExpires(Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpToken;->mUserName:Ljava/lang/String;

    .line 128
    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpToken;->mUserID:Ljava/lang/String;

    .line 129
    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpToken;->mProfilePhoto:Ljava/lang/String;

    .line 130
    return-void
.end method

.method public setAccessTokenNExpires(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "accessToken"    # Ljava/lang/String;
    .param p2, "refreshToken"    # Ljava/lang/String;

    .prologue
    .line 74
    iput-object p1, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpToken;->mAccessToken:Ljava/lang/String;

    .line 75
    iput-object p2, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpToken;->mRefreshToken:Ljava/lang/String;

    .line 77
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpToken;->mAccessToken:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpToken;->mAccessToken:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpToken;->mRefreshToken:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpToken;->mRefreshToken:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 79
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpToken;->setTokenState(I)V

    .line 83
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpToken;->writeTokenInfo()V

    .line 84
    return-void

    .line 81
    :cond_0
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpToken;->setTokenState(I)V

    goto :goto_0
.end method

.method public setAccountName(Ljava/lang/String;)V
    .locals 0
    .param p1, "mAccountName"    # Ljava/lang/String;

    .prologue
    .line 120
    iput-object p1, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpToken;->mAccountName:Ljava/lang/String;

    .line 121
    return-void
.end method

.method public setUserInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "userName"    # Ljava/lang/String;
    .param p2, "userID"    # Ljava/lang/String;
    .param p3, "profilePhoto"    # Ljava/lang/String;

    .prologue
    .line 88
    iput-object p1, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpToken;->mUserName:Ljava/lang/String;

    .line 89
    iput-object p2, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpToken;->mUserID:Ljava/lang/String;

    .line 90
    iput-object p3, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpToken;->mProfilePhoto:Ljava/lang/String;

    .line 92
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpToken;->writeTokenInfo()V

    .line 93
    return-void
.end method

.method public writeTokenInfo()V
    .locals 3

    .prologue
    .line 135
    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpToken;->mSharedPref:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 137
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "access_token"

    iget-object v2, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpToken;->mAccessToken:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 138
    const-string v1, "expires_in"

    iget-object v2, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpToken;->mRefreshToken:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 140
    const-string v1, "username"

    iget-object v2, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpToken;->mUserName:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 141
    const-string v1, "user_id"

    iget-object v2, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpToken;->mUserID:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 142
    const-string v1, "profile_photo"

    iget-object v2, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpToken;->mProfilePhoto:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 143
    const-string v1, "account_name"

    iget-object v2, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpToken;->mAccountName:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 145
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 146
    return-void
.end method
