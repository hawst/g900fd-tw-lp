.class public Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserEvents;
.super Ljava/lang/Object;
.source "SnsFbParserEvents.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserEvents$FacebookEvents;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    return-void
.end method

.method public static parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvents;
    .locals 9
    .param p0, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 38
    new-instance v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvents;

    invoke-direct {v2}, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvents;-><init>()V

    .line 40
    .local v2, "eventList":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvents;
    const/4 v3, 0x0

    .line 41
    .local v3, "events":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;
    const/4 v0, 0x0

    .line 44
    .local v0, "curEvents":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;
    :try_start_0
    new-instance v6, Lorg/json/JSONObject;

    invoke-direct {v6, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 46
    .local v6, "jsonObj":Lorg/json/JSONObject;
    new-instance v5, Lorg/json/JSONArray;

    const-string v8, "data"

    invoke-virtual {v6, v8}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v5, v8}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 48
    .local v5, "ja":Lorg/json/JSONArray;
    if-eqz v5, :cond_1

    .line 50
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    invoke-virtual {v5}, Lorg/json/JSONArray;->length()I

    move-result v8

    if-ge v4, v8, :cond_1

    .line 51
    invoke-virtual {v5, v4}, Lorg/json/JSONArray;->optString(I)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserEvent;->parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;

    move-result-object v7

    .line 53
    .local v7, "newEvent":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;
    if-nez v3, :cond_0

    .line 54
    move-object v3, v7

    .line 55
    move-object v0, v3

    .line 50
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 57
    :cond_0
    iput-object v7, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;

    .line 58
    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;

    goto :goto_1

    .line 63
    .end local v4    # "i":I
    .end local v7    # "newEvent":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;
    :cond_1
    iput-object v3, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvents;->mEvents:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvent;

    .line 65
    const-string v8, "paging"

    invoke-virtual {v6, v8}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 66
    const-string v8, "paging"

    invoke-virtual {v6, v8}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserPaging;->parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePaging;

    move-result-object v8

    iput-object v8, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseEvents;->mPaging:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePaging;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 75
    .end local v5    # "ja":Lorg/json/JSONArray;
    .end local v6    # "jsonObj":Lorg/json/JSONObject;
    :cond_2
    :goto_2
    return-object v2

    .line 70
    :catch_0
    move-exception v1

    .line 72
    .local v1, "e":Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_2
.end method
