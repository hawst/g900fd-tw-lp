.class Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpServiceForAuthToken$1;
.super Lcom/sec/android/app/sns3/svc/sp/googleplus/auth/api/ISnsGooglePlusForAuthToken$Stub;
.source "SnsGpServiceForAuthToken.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpServiceForAuthToken;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpServiceForAuthToken;


# direct methods
.method constructor <init>(Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpServiceForAuthToken;)V
    .locals 0

    .prologue
    .line 82
    iput-object p1, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpServiceForAuthToken$1;->this$0:Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpServiceForAuthToken;

    invoke-direct {p0}, Lcom/sec/android/app/sns3/svc/sp/googleplus/auth/api/ISnsGooglePlusForAuthToken$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public getAuthTokenNExpires()Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 87
    const-string v2, "SNS"

    const-string v3, "SnsGpServiceForAuthToken : getAuthTokenNExpires() CALLED !!"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 89
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 91
    .local v0, "Info":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v2, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpServiceForAuthToken$1;->this$0:Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpServiceForAuthToken;

    # getter for: Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpServiceForAuthToken;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    invoke-static {v2}, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpServiceForAuthToken;->access$000(Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpServiceForAuthToken;)Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/sns3/svc/SnsSvcMgr;->getTokenMgr()Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;

    move-result-object v2

    const-string v3, "googleplus"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;->getToken(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpToken;

    .line 93
    .local v1, "gpToken":Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpToken;
    const-string v2, "app_id"

    invoke-static {}, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpAppIdManager;->getInstance()Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpAppIdManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpAppIdManager;->getClientId()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 94
    const-string v2, "secret_key"

    invoke-static {}, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpAppIdManager;->getInstance()Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpAppIdManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpAppIdManager;->getClientSecret()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 95
    const-string v2, "access_token"

    invoke-virtual {v1}, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpToken;->getAccessToken()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 96
    const-string v2, "refresh_token"

    invoke-virtual {v1}, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpToken;->getRefreshToken()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 98
    return-object v0
.end method

.method public getMyAccountInfo(Lcom/sec/android/app/sns3/svc/sp/googleplus/auth/api/ISnsGooglePlusCallbackMyAccountInfo;)I
    .locals 8
    .param p1, "cb"    # Lcom/sec/android/app/sns3/svc/sp/googleplus/auth/api/ISnsGooglePlusCallbackMyAccountInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 105
    const-string v1, "SNS"

    const-string v2, "getMyAccountInfo CALLED!!!"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 107
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpServiceForAuthToken$1$1;

    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpServiceForAuthToken$1;->this$0:Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpServiceForAuthToken;

    # getter for: Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpServiceForAuthToken;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    invoke-static {v1}, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpServiceForAuthToken;->access$000(Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpServiceForAuthToken;)Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v2

    const-string v3, "me"

    const/4 v4, 0x0

    move-object v1, p0

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpServiceForAuthToken$1$1;-><init>(Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpServiceForAuthToken$1;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;Landroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/googleplus/auth/api/ISnsGooglePlusCallbackMyAccountInfo;)V

    .line 122
    .local v0, "req":Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->request()Z

    move-result v6

    .line 123
    .local v6, "isRequested":Z
    if-nez v6, :cond_0

    .line 124
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->getReqID()I

    move-result v7

    .line 125
    .local v7, "reqId":I
    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpServiceForAuthToken$1;->this$0:Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpServiceForAuthToken;

    # getter for: Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpServiceForAuthToken;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpServiceForAuthToken;->access$100(Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpServiceForAuthToken;)Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpServiceForAuthToken$1$2;

    invoke-direct {v2, p0, p1, v7}, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpServiceForAuthToken$1$2;-><init>(Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpServiceForAuthToken$1;Lcom/sec/android/app/sns3/svc/sp/googleplus/auth/api/ISnsGooglePlusCallbackMyAccountInfo;I)V

    const-wide/16 v4, 0x64

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 140
    .end local v7    # "reqId":I
    :cond_0
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->getReqID()I

    move-result v1

    return v1
.end method
