.class public interface abstract Lcom/sec/android/app/sns3/agent/sp/instagram/db/SnsInstagramDB$UserInfoColumns;
.super Ljava/lang/Object;
.source "SnsInstagramDB.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/agent/sp/instagram/db/SnsInstagramDB;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "UserInfoColumns"
.end annotation


# static fields
.field public static final PROFILE_BIO:Ljava/lang/String; = "profile_bio"

.field public static final PROFILE_FULL_NAME:Ljava/lang/String; = "full_name"

.field public static final PROFILE_ID:Ljava/lang/String; = "profile_id"

.field public static final PROFILE_IMAGE_URL:Ljava/lang/String; = "profile_image"

.field public static final PROFILE_NAME:Ljava/lang/String; = "profile_name"

.field public static final PROFILE_WEBSITE:Ljava/lang/String; = "profile_website"
