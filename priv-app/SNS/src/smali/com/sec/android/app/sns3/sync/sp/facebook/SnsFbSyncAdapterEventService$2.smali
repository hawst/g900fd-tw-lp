.class Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService$2;
.super Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqGetBirthday;
.source "SnsFbSyncAdapterEventService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->performBirthdaySync()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;


# direct methods
.method constructor <init>(Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;)V
    .locals 0
    .param p2, "x0"    # Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    .param p3, "x1"    # Ljava/lang/String;

    .prologue
    .line 308
    iput-object p1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService$2;->this$0:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;

    invoke-direct {p0, p2, p3}, Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqGetBirthday;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public onReqRespond(IZIILandroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseBirthday;)Z
    .locals 6
    .param p1, "reqID"    # I
    .param p2, "bSuccess"    # Z
    .param p3, "httpStatus"    # I
    .param p4, "errorCode"    # I
    .param p5, "reason"    # Landroid/os/Bundle;
    .param p6, "notification"    # Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseBirthday;

    .prologue
    const/4 v5, -0x1

    .line 314
    if-eqz p2, :cond_0

    .line 315
    new-instance v0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;

    invoke-direct {v0}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;-><init>()V

    .line 317
    .local v0, "dbAdapter":Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;
    :try_start_0
    invoke-virtual {v0, p6}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->insetBirthdayEvents(Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseBirthday;)Landroid/os/Bundle;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 330
    .end local v0    # "dbAdapter":Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService$2;->this$0:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;

    # getter for: Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;
    invoke-static {v2}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->access$800(Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;)Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;->resumeSync()V

    .line 332
    iget-object v2, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService$2;->this$0:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;

    # getter for: Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->mbGetResponse:Z
    invoke-static {v2}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->access$900(Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;)Z

    move-result v2

    return v2

    .line 319
    .restart local v0    # "dbAdapter":Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;
    :catch_0
    move-exception v1

    .line 320
    .local v1, "e":Ljava/lang/Exception;
    iget-object v2, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService$2;->this$0:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;

    # setter for: Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->mSyncState:I
    invoke-static {v2, v5}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->access$702(Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;I)I

    .line 321
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 324
    .end local v0    # "dbAdapter":Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_0
    const-string v2, "SnsFbSync"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SnsFbSyncAdapterEventService errorCode : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", reason : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 327
    iget-object v2, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService$2;->this$0:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;

    # setter for: Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->mSyncState:I
    invoke-static {v2, v5}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;->access$702(Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterEventService;I)I

    goto :goto_0
.end method
