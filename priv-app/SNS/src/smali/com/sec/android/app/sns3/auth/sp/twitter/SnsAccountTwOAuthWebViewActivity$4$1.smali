.class Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity$4$1;
.super Ljava/lang/Object;
.source "SnsAccountTwOAuthWebViewActivity.java"

# interfaces
.implements Lcom/sec/android/app/sns3/agent/command/ISnsCommandCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity$4;->onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity$4;


# direct methods
.method constructor <init>(Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity$4;)V
    .locals 0

    .prologue
    .line 257
    iput-object p1, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity$4$1;->this$1:Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity$4;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCmdRespond(IZLjava/lang/String;Ljava/util/List;)V
    .locals 9
    .param p1, "cmdID"    # I
    .param p2, "bSuccess"    # Z
    .param p3, "uri"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IZ",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p4, "responseList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;>;"
    const/4 v8, 0x0

    const/4 v7, -0x1

    const/4 v6, 0x0

    .line 263
    iget-object v4, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity$4$1;->this$1:Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity$4;

    iget-object v4, v4, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity$4;->this$0:Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;

    # getter for: Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    invoke-static {v4}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;->access$200(Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;)Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/sns3/svc/SnsSvcMgr;->getTokenMgr()Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;

    move-result-object v4

    const-string v5, "twitter"

    invoke-virtual {v4, v5}, Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;->getToken(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;

    .line 265
    .local v1, "token":Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;
    invoke-virtual {v1}, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;->getScreenName()Ljava/lang/String;

    move-result-object v3

    .line 266
    .local v3, "userName":Ljava/lang/String;
    invoke-virtual {v1}, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;->getUserID()Ljava/lang/String;

    move-result-object v2

    .line 268
    .local v2, "userID":Ljava/lang/String;
    # getter for: Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;->mbRetryLogin:Z
    invoke-static {}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;->access$400()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 269
    iget-object v4, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity$4$1;->this$1:Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity$4;

    iget-object v4, v4, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity$4;->this$0:Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;

    # getter for: Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;->mAppContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;->access$500(Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;)Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v4

    const-string v5, "com.sec.android.app.sns3.twitter"

    invoke-virtual {v4, v5}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 271
    .local v0, "accounts":[Landroid/accounts/Account;
    if-eqz v0, :cond_0

    array-length v4, v0

    if-lez v4, :cond_0

    .line 272
    iget-object v4, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity$4$1;->this$1:Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity$4;

    iget-object v4, v4, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity$4;->this$0:Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;

    # getter for: Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;->mAppContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;->access$500(Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;)Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v4

    aget-object v5, v0, v8

    invoke-virtual {v4, v5, v6, v6}, Landroid/accounts/AccountManager;->removeAccount(Landroid/accounts/Account;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    .line 274
    # setter for: Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;->mbRetryLogin:Z
    invoke-static {v8}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;->access$402(Z)Z

    .line 278
    .end local v0    # "accounts":[Landroid/accounts/Account;
    :cond_0
    if-eqz p2, :cond_1

    if-eqz v3, :cond_1

    .line 279
    iget-object v4, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity$4$1;->this$1:Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity$4;

    iget-object v4, v4, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity$4;->this$0:Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;

    invoke-virtual {v4, v3, v2}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;->loginSuccess(Ljava/lang/String;Ljava/lang/String;)V

    .line 283
    :goto_0
    return-void

    .line 281
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity$4$1;->this$1:Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity$4;

    iget-object v4, v4, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity$4;->this$0:Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;

    invoke-virtual {v4, v7, v7, v6}, Lcom/sec/android/app/sns3/auth/sp/twitter/SnsAccountTwOAuthWebViewActivity;->loginFail(IILjava/lang/String;)V

    goto :goto_0
.end method
