.class public interface abstract Lcom/sec/android/app/sns3/svc/sp/sinaweibo/api/ISnsSinaweibo;
.super Ljava/lang/Object;
.source "ISnsSinaweibo.java"

# interfaces
.implements Landroid/os/IInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/sinaweibo/api/ISnsSinaweibo$Stub;
    }
.end annotation


# virtual methods
.method public abstract getApplicationInfo()Ljava/util/Map;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract getFriendTimeline(Ljava/lang/String;Lcom/sec/android/app/sns3/svc/sp/sinaweibo/api/ISnsSinaweiboCallbackStatus;)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method
