.class public Lcom/sec/android/app/sns3/agent/sp/googleplus/command/SnsGpCmdAuthLogout;
.super Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;
.source "SnsGpCmdAuthLogout.java"


# direct methods
.method public constructor <init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Handler;)V
    .locals 8
    .param p1, "svcMgr"    # Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    .param p2, "cmdHandler"    # Landroid/os/Handler;

    .prologue
    const/4 v7, 0x0

    .line 39
    invoke-direct {p0, p2}, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;-><init>(Landroid/os/Handler;)V

    .line 41
    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/SnsSvcMgr;->getRequestMgr()Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;

    move-result-object v4

    const-string v5, "googleplus"

    invoke-virtual {v4, v5}, Lcom/sec/android/app/sns3/svc/request/SnsRequestMgr;->clearReservedRequestsBySp(Ljava/lang/String;)Z

    .line 43
    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/SnsSvcMgr;->getTokenMgr()Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;

    move-result-object v3

    .line 44
    .local v3, "tokenMgr":Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;
    const-string v4, "googleplus"

    invoke-virtual {v3, v4}, Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;->getToken(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpToken;

    .line 46
    .local v1, "gpToken":Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpToken;
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v4

    invoke-static {v4}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v4

    const-string v5, "com.google"

    invoke-virtual {v1}, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpToken;->getAccessToken()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/accounts/AccountManager;->invalidateAuthToken(Ljava/lang/String;Ljava/lang/String;)V

    .line 49
    invoke-virtual {v1}, Lcom/sec/android/app/sns3/svc/sp/googleplus/SnsGpToken;->removeAll()V

    .line 51
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/sns3/SnsApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 52
    .local v0, "cr":Landroid/content/ContentResolver;
    sget-object v4, Lcom/sec/android/app/sns3/agent/sp/googleplus/db/SnsGooglePlusDB$WIPE_GOOGLEPLUS_DATA;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v4, v7, v7}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 54
    const-string v2, "sp_type = googleplus"

    .line 55
    .local v2, "selection":Ljava/lang/String;
    sget-object v4, Lcom/sec/android/app/sns3/agent/life/db/SnsLifeDB$LifeTimeline;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v4, v2, v7}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 57
    return-void
.end method


# virtual methods
.method protected respond()Z
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 68
    const-string v0, "SnsAgent"

    const-string v1, "<SnsGpCmdAuthLogout> respond()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 69
    invoke-virtual {p0, v5}, Lcom/sec/android/app/sns3/agent/sp/googleplus/command/SnsGpCmdAuthLogout;->setSuccess(Z)V

    .line 70
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/sns3/agent/sp/googleplus/command/SnsGpCmdAuthLogout;->setUri(Ljava/lang/String;)V

    .line 71
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/googleplus/command/SnsGpCmdAuthLogout;->getCommandCallback()Lcom/sec/android/app/sns3/agent/command/ISnsCommandCallback;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/googleplus/command/SnsGpCmdAuthLogout;->getCommandID()I

    move-result v1

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/googleplus/command/SnsGpCmdAuthLogout;->isSuccess()Z

    move-result v2

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/googleplus/command/SnsGpCmdAuthLogout;->getUri()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/googleplus/command/SnsGpCmdAuthLogout;->getResponseList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/sec/android/app/sns3/agent/command/ISnsCommandCallback;->onCmdRespond(IZLjava/lang/String;Ljava/util/List;)V

    .line 72
    return v5
.end method

.method public send()I
    .locals 1

    .prologue
    .line 62
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/googleplus/command/SnsGpCmdAuthLogout;->respond()Z

    .line 63
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/agent/sp/googleplus/command/SnsGpCmdAuthLogout;->getCommandID()I

    move-result v0

    return v0
.end method
