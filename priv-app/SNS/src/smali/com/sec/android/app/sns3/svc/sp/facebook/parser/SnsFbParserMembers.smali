.class public Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserMembers;
.super Ljava/lang/Object;
.source "SnsFbParserMembers.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserMembers$FacebookMembers;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    return-void
.end method

.method public static parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMembers;
    .locals 11
    .param p0, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 48
    const/4 v8, 0x0

    .line 49
    .local v8, "members":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMembers;
    const/4 v0, 0x0

    .line 51
    .local v0, "curMember":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMembers;
    const/4 v5, 0x0

    .line 54
    .local v5, "jsonObject":Lorg/json/JSONObject;
    :try_start_0
    new-instance v6, Lorg/json/JSONObject;

    invoke-direct {v6, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 56
    .end local v5    # "jsonObject":Lorg/json/JSONObject;
    .local v6, "jsonObject":Lorg/json/JSONObject;
    :try_start_1
    const-string v10, "members"

    invoke-virtual {v6, v10}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 58
    const-string v10, "members"

    invoke-virtual {v6, v10}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v7

    .line 60
    .local v7, "memObj":Lorg/json/JSONObject;
    new-instance v3, Lorg/json/JSONArray;

    const-string v10, "data"

    invoke-virtual {v7, v10}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v3, v10}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 63
    .local v3, "ja":Lorg/json/JSONArray;
    if-eqz v3, :cond_1

    .line 65
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v10

    if-ge v2, v10, :cond_1

    .line 66
    new-instance v9, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMembers;

    invoke-direct {v9}, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMembers;-><init>()V

    .line 68
    .local v9, "newMember":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMembers;
    invoke-virtual {v3, v2}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    .line 69
    .local v4, "jsonObj":Lorg/json/JSONObject;
    const-string v10, "id"

    invoke-virtual {v6, v10}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    iput-object v10, v9, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMembers;->mGroupId:Ljava/lang/String;

    .line 71
    const-string v10, "name"

    invoke-virtual {v6, v10}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    iput-object v10, v9, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMembers;->mGroupName:Ljava/lang/String;

    .line 73
    const-string v10, "list_type"

    invoke-virtual {v6, v10}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    iput-object v10, v9, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMembers;->mGroupType:Ljava/lang/String;

    .line 75
    const-string v10, "id"

    invoke-virtual {v4, v10}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    iput-object v10, v9, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMembers;->mID:Ljava/lang/String;

    .line 76
    const-string v10, "name"

    invoke-virtual {v4, v10}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    iput-object v10, v9, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMembers;->mName:Ljava/lang/String;

    .line 79
    if-nez v8, :cond_0

    .line 80
    move-object v8, v9

    .line 81
    move-object v0, v8

    .line 65
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 83
    :cond_0
    iput-object v9, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMembers;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMembers;

    .line 84
    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMembers;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMembers;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .end local v2    # "i":I
    .end local v3    # "ja":Lorg/json/JSONArray;
    .end local v4    # "jsonObj":Lorg/json/JSONObject;
    .end local v7    # "memObj":Lorg/json/JSONObject;
    .end local v9    # "newMember":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMembers;
    :cond_1
    move-object v5, v6

    .line 96
    .end local v6    # "jsonObject":Lorg/json/JSONObject;
    .restart local v5    # "jsonObject":Lorg/json/JSONObject;
    :goto_2
    return-object v8

    .line 91
    :catch_0
    move-exception v1

    .line 93
    .local v1, "e":Lorg/json/JSONException;
    :goto_3
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_2

    .line 91
    .end local v1    # "e":Lorg/json/JSONException;
    .end local v5    # "jsonObject":Lorg/json/JSONObject;
    .restart local v6    # "jsonObject":Lorg/json/JSONObject;
    :catch_1
    move-exception v1

    move-object v5, v6

    .end local v6    # "jsonObject":Lorg/json/JSONObject;
    .restart local v5    # "jsonObject":Lorg/json/JSONObject;
    goto :goto_3
.end method
