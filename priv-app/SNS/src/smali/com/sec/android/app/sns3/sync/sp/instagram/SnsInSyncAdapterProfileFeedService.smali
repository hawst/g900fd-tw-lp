.class public Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterProfileFeedService;
.super Landroid/app/Service;
.source "SnsInSyncAdapterProfileFeedService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterProfileFeedService$SyncAdapterImpl;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "SnsInSync"


# instance fields
.field private mAccount:Landroid/accounts/Account;

.field private mAuthority:Ljava/lang/String;

.field private mCmdHandler:Landroid/os/Handler;

.field private mContext:Landroid/content/Context;

.field private mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

.field private mSyncAdapter:Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterProfileFeedService$SyncAdapterImpl;

.field private mSyncResult:Landroid/content/SyncResult;

.field private mSyncState:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 60
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 64
    iput-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterProfileFeedService;->mSyncAdapter:Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterProfileFeedService$SyncAdapterImpl;

    .line 66
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterProfileFeedService;->mSyncState:I

    .line 68
    iput-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterProfileFeedService;->mSyncResult:Landroid/content/SyncResult;

    .line 70
    iput-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterProfileFeedService;->mAccount:Landroid/accounts/Account;

    .line 72
    iput-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterProfileFeedService;->mAuthority:Ljava/lang/String;

    .line 78
    return-void
.end method

.method static synthetic access$002(Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterProfileFeedService;Landroid/content/Context;)Landroid/content/Context;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterProfileFeedService;
    .param p1, "x1"    # Landroid/content/Context;

    .prologue
    .line 60
    iput-object p1, p0, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterProfileFeedService;->mContext:Landroid/content/Context;

    return-object p1
.end method

.method static synthetic access$102(Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterProfileFeedService;Landroid/accounts/Account;)Landroid/accounts/Account;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterProfileFeedService;
    .param p1, "x1"    # Landroid/accounts/Account;

    .prologue
    .line 60
    iput-object p1, p0, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterProfileFeedService;->mAccount:Landroid/accounts/Account;

    return-object p1
.end method

.method static synthetic access$202(Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterProfileFeedService;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterProfileFeedService;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 60
    iput-object p1, p0, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterProfileFeedService;->mAuthority:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$302(Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterProfileFeedService;Landroid/content/SyncResult;)Landroid/content/SyncResult;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterProfileFeedService;
    .param p1, "x1"    # Landroid/content/SyncResult;

    .prologue
    .line 60
    iput-object p1, p0, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterProfileFeedService;->mSyncResult:Landroid/content/SyncResult;

    return-object p1
.end method

.method static synthetic access$400(Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterProfileFeedService;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterProfileFeedService;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/accounts/OperationCanceledException;
        }
    .end annotation

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterProfileFeedService;->performSync()V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterProfileFeedService;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterProfileFeedService;

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterProfileFeedService;->onSyncCanceled()V

    return-void
.end method

.method static synthetic access$602(Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterProfileFeedService;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterProfileFeedService;
    .param p1, "x1"    # I

    .prologue
    .line 60
    iput p1, p0, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterProfileFeedService;->mSyncState:I

    return p1
.end method

.method private handleSessionExpired()I
    .locals 11

    .prologue
    const/4 v9, 0x1

    const/4 v10, 0x0

    .line 289
    iget-object v7, p0, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterProfileFeedService;->mAccount:Landroid/accounts/Account;

    const-string v8, "com.sec.android.app.sns3.life"

    invoke-static {v7, v8}, Landroid/content/ContentResolver;->cancelSync(Landroid/accounts/Account;Ljava/lang/String;)V

    .line 291
    iget-object v7, p0, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterProfileFeedService;->mContext:Landroid/content/Context;

    const-string v8, "notification"

    invoke-virtual {v7, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/app/NotificationManager;

    .line 294
    .local v4, "notiMgr":Landroid/app/NotificationManager;
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 295
    .local v3, "notiIntent":Landroid/content/Intent;
    const-string v7, "com.sec.android.app.sns3.RETRY_LOGIN_INSTAGRAM"

    invoke-virtual {v3, v7}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 296
    const/high16 v7, 0x800000

    invoke-virtual {v3, v7}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 297
    const-string v7, "RetryLogin"

    invoke-virtual {v3, v7, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 298
    iget-object v7, p0, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterProfileFeedService;->mContext:Landroid/content/Context;

    invoke-static {v7, v10, v3, v10}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 300
    .local v1, "launchIntent":Landroid/app/PendingIntent;
    const v6, 0x7f08003f

    .line 301
    .local v6, "titleID":I
    const v5, 0x7f080025

    .line 302
    .local v5, "spID":I
    const v0, 0x7f02001d

    .line 304
    .local v0, "iconID":I
    new-instance v2, Landroid/app/Notification$Builder;

    iget-object v7, p0, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterProfileFeedService;->mContext:Landroid/content/Context;

    invoke-direct {v2, v7}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    .line 305
    .local v2, "notiBuilder":Landroid/app/Notification$Builder;
    iget-object v7, p0, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterProfileFeedService;->mContext:Landroid/content/Context;

    new-array v8, v9, [Ljava/lang/Object;

    iget-object v9, p0, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterProfileFeedService;->mContext:Landroid/content/Context;

    invoke-virtual {v9, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v10

    invoke-virtual {v7, v6, v8}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v7

    invoke-virtual {v7, v0}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterProfileFeedService;->mContext:Landroid/content/Context;

    const v9, 0x7f08003e

    invoke-virtual {v8, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v7

    invoke-virtual {v7, v1}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v7

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Landroid/app/Notification$Builder;->setWhen(J)Landroid/app/Notification$Builder;

    .line 310
    const/16 v7, 0x189c

    invoke-virtual {v2}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v8

    invoke-virtual {v4, v7, v8}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 312
    const/4 v7, -0x1

    return v7
.end method

.method private invokeBroadcast()V
    .locals 7

    .prologue
    .line 338
    const/4 v2, 0x0

    .line 340
    .local v2, "intent":Landroid/content/Intent;
    sget-object v3, Lcom/sec/android/app/sns3/agent/sp/instagram/db/SnsInstagramDB$UserInFeedInfo;->CONTENT_URI:Landroid/net/Uri;

    .line 341
    .local v3, "uri":Landroid/net/Uri;
    const-string v0, "com.sec.android.app.sns3.action.SYNC_IN_PROFILE_FEEDS"

    .line 342
    .local v0, "action":Ljava/lang/String;
    iget v4, p0, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterProfileFeedService;->mSyncState:I

    const/4 v5, 0x2

    if-ne v4, v5, :cond_1

    const/4 v1, 0x1

    .line 344
    .local v1, "bResult":Z
    :goto_0
    new-instance v2, Landroid/content/Intent;

    .end local v2    # "intent":Landroid/content/Intent;
    invoke-direct {v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 345
    .restart local v2    # "intent":Landroid/content/Intent;
    const-string v4, "SNS3_CONTENT_URI_PROFILE"

    invoke-virtual {v2, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 346
    const-string v4, "SNS_RESULT"

    invoke-virtual {v2, v4, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 348
    const-string v4, "com.sec.android.app.sns3.permission.RECEIVE_SNS_BROADCAST"

    invoke-virtual {p0, v2, v4}, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterProfileFeedService;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    .line 349
    invoke-static {}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->isLoggable()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 350
    const-string v4, "SnsInSync"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "SnsInSyncAdapterProfileFeedService - invokeBroadcast() : action = ["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "], uri = ["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "], result = ["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 353
    :cond_0
    return-void

    .line 342
    .end local v1    # "bResult":Z
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private onSyncCanceled()V
    .locals 2

    .prologue
    .line 329
    const-string v0, "SnsInSync"

    const-string v1, "***************** SnsInSyncAdapterProfileFeedService : onSyncCanceled !!! *****************"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 332
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterProfileFeedService;->mSyncState:I

    .line 334
    return-void
.end method

.method private performSync()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/accounts/OperationCanceledException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    .line 161
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/sns3/SnsApplication;->getAgentMgr()Lcom/sec/android/app/sns3/agent/SnsAgentMgr;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/sns3/agent/SnsAgentMgr;->getCommandMgr()Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/sns3/agent/command/SnsCommandMgr;->getCommandMgrHandle()Landroid/os/Handler;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterProfileFeedService;->mCmdHandler:Landroid/os/Handler;

    .line 163
    iput v5, p0, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterProfileFeedService;->mSyncState:I

    .line 164
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/sns3/SnsApplication;->getSvcMgr()Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/sns3/svc/SnsSvcMgr;->getTokenMgr()Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;

    move-result-object v3

    const-string v4, "instagram"

    invoke-virtual {v3, v4}, Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;->getToken(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/sns3/svc/sp/instagram/SnsInToken;

    .line 167
    .local v2, "mInToken":Lcom/sec/android/app/sns3/svc/sp/instagram/SnsInToken;
    :try_start_0
    invoke-virtual {v2}, Lcom/sec/android/app/sns3/svc/sp/instagram/SnsInToken;->getTokenState()I

    move-result v3

    if-eq v3, v5, :cond_0

    invoke-virtual {v2}, Lcom/sec/android/app/sns3/svc/sp/instagram/SnsInToken;->getTokenState()I

    move-result v3

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    .line 169
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterProfileFeedService;->handleSessionExpired()I

    move-result v3

    iput v3, p0, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterProfileFeedService;->mSyncState:I

    .line 170
    new-instance v3, Ljava/lang/Exception;

    const-string v4, "Session expired or invalid!!!"

    invoke-direct {v3, v4}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 271
    :catch_0
    move-exception v1

    .line 272
    .local v1, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 273
    const-string v3, "SnsInSync"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "SnsInSyncAdapterProfileFeedService : EXCEPTION !!! "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 275
    iget-object v3, p0, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterProfileFeedService;->mSyncResult:Landroid/content/SyncResult;

    iget-object v3, v3, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    const-wide/16 v4, 0x1

    iput-wide v4, v3, Landroid/content/SyncStats;->numConflictDetectedExceptions:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 279
    invoke-direct {p0}, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterProfileFeedService;->invokeBroadcast()V

    .line 281
    const-string v3, "SnsInSync"

    const-string v4, "***************** SnsInSyncAdapterProfileFeedService : performSync - FINISHED !!! *****************"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 286
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_0
    return-void

    .line 172
    :cond_1
    :try_start_2
    new-instance v0, Lcom/sec/android/app/sns3/agent/sp/instagram/command/SnsInCmdGetProfileFeed;

    iget-object v3, p0, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterProfileFeedService;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    iget-object v4, p0, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterProfileFeedService;->mCmdHandler:Landroid/os/Handler;

    const-string v5, "self"

    invoke-direct {v0, v3, v4, v5}, Lcom/sec/android/app/sns3/agent/sp/instagram/command/SnsInCmdGetProfileFeed;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Handler;Ljava/lang/String;)V

    .line 173
    .local v0, "cmd":Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;
    new-instance v3, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterProfileFeedService$1;

    invoke-direct {v3, p0}, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterProfileFeedService$1;-><init>(Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterProfileFeedService;)V

    invoke-virtual {v0, v3}, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->setCommandCallback(Lcom/sec/android/app/sns3/agent/command/ISnsCommandCallback;)Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;

    .line 260
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;->send()I

    .line 262
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterProfileFeedService;->suspendSync()V

    .line 265
    iget v3, p0, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterProfileFeedService;->mSyncState:I

    const/4 v4, -0x1

    if-ne v3, v4, :cond_2

    .line 266
    new-instance v3, Ljava/lang/Exception;

    const-string v4, "[SnsInSyncAdapterProfileFeedService] updates sync is failed!!!"

    invoke-direct {v3, v4}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 279
    .end local v0    # "cmd":Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;
    :catchall_0
    move-exception v3

    invoke-direct {p0}, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterProfileFeedService;->invokeBroadcast()V

    .line 281
    const-string v4, "SnsInSync"

    const-string v5, "***************** SnsInSyncAdapterProfileFeedService : performSync - FINISHED !!! *****************"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    throw v3

    .line 279
    .restart local v0    # "cmd":Lcom/sec/android/app/sns3/agent/command/AbstractSnsCommand;
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterProfileFeedService;->invokeBroadcast()V

    .line 281
    const-string v3, "SnsInSync"

    const-string v4, "***************** SnsInSyncAdapterProfileFeedService : performSync - FINISHED !!! *****************"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 130
    const-string v0, "SnsInSync"

    const-string v1, "***************** SnsInSyncAdapterProfileFeedService : onBind !!! *****************"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 133
    iget-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterProfileFeedService;->mSyncAdapter:Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterProfileFeedService$SyncAdapterImpl;

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterProfileFeedService$SyncAdapterImpl;->getSyncAdapterBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 117
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 119
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/SnsApplication;->getSvcMgr()Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterProfileFeedService;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    .line 121
    iget-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterProfileFeedService;->mSyncAdapter:Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterProfileFeedService$SyncAdapterImpl;

    if-nez v0, :cond_0

    .line 122
    new-instance v0, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterProfileFeedService$SyncAdapterImpl;

    invoke-direct {v0, p0, p0}, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterProfileFeedService$SyncAdapterImpl;-><init>(Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterProfileFeedService;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterProfileFeedService;->mSyncAdapter:Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterProfileFeedService$SyncAdapterImpl;

    .line 125
    :cond_0
    return-void
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 139
    const/4 v0, 0x0

    .line 140
    .local v0, "bActive":Z
    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterProfileFeedService;->mAccount:Landroid/accounts/Account;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterProfileFeedService;->mAuthority:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 141
    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterProfileFeedService;->mAccount:Landroid/accounts/Account;

    iget-object v2, p0, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterProfileFeedService;->mAuthority:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/content/ContentResolver;->isSyncActive(Landroid/accounts/Account;Ljava/lang/String;)Z

    move-result v0

    .line 144
    :cond_0
    const-string v1, "SnsInSync"

    const-string v2, "***************** SnsInSyncAdapterProfileFeedService : onUnbind !!! *****************"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 147
    if-eqz v0, :cond_2

    iget v1, p0, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterProfileFeedService;->mSyncState:I

    const/4 v2, 0x1

    if-eq v1, v2, :cond_1

    iget v1, p0, Lcom/sec/android/app/sns3/sync/sp/instagram/SnsInSyncAdapterProfileFeedService;->mSyncState:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_2

    .line 149
    :cond_1
    const-string v1, "SnsInSync"

    const-string v2, "SnsInSyncAdapterProfileFeedService : onUnbind : SYNC ERROR : performSync was stopped by forced abort or pending problem!!!"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 155
    :goto_0
    invoke-super {p0, p1}, Landroid/app/Service;->onUnbind(Landroid/content/Intent;)Z

    move-result v1

    return v1

    .line 153
    :cond_2
    const-string v1, "SnsInSync"

    const-string v2, "SnsInSyncAdapterProfileFeedService : onUnbind : COMPLETE STATE!!!"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public declared-synchronized resumeSync()V
    .locals 1

    .prologue
    .line 324
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->notify()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 325
    monitor-exit p0

    return-void

    .line 324
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized suspendSync()V
    .locals 2

    .prologue
    .line 317
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 321
    :goto_0
    monitor-exit p0

    return-void

    .line 318
    :catch_0
    move-exception v0

    .line 319
    .local v0, "e":Ljava/lang/InterruptedException;
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 317
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method
