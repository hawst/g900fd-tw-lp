.class public Lcom/sec/android/app/sns3/svc/sp/foursquare/parser/SnsFsParserCategories;
.super Ljava/lang/Object;
.source "SnsFsParserCategories.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/foursquare/parser/SnsFsParserCategories$FourSquareCategories;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    return-void
.end method

.method public static parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseCategories;
    .locals 10
    .param p0, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 57
    new-instance v2, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseCategories;

    invoke-direct {v2}, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseCategories;-><init>()V

    .line 60
    .local v2, "categoriesObject":Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseCategories;
    :try_start_0
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 61
    .local v3, "con":Lorg/json/JSONObject;
    const-string v8, "meta"

    invoke-virtual {v3, v8}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v6

    .line 63
    .local v6, "meta":Lorg/json/JSONObject;
    const-string v8, "code"

    invoke-virtual {v6, v8}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v8

    const/16 v9, 0xc8

    if-ne v8, v9, :cond_0

    .line 64
    const-string v8, "response"

    invoke-virtual {v3, v8}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v7

    .line 65
    .local v7, "response":Lorg/json/JSONObject;
    const-string v8, "categories"

    invoke-virtual {v7, v8}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    .line 66
    .local v0, "categories":Lorg/json/JSONArray;
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v8

    if-ge v5, v8, :cond_0

    .line 67
    invoke-virtual {v0, v5}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v1

    .line 68
    .local v1, "categoriesJsonObject":Lorg/json/JSONObject;
    iget-object v8, v2, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/SnsFsResponseCategories;->mCategories:Ljava/util/List;

    invoke-static {v1}, Lcom/sec/android/app/sns3/svc/sp/foursquare/parser/SnsFsParserCategories;->parseCategories(Lorg/json/JSONObject;)Lcom/sec/android/app/sns3/svc/sp/foursquare/response/Category;

    move-result-object v9

    invoke-interface {v8, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 66
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 71
    .end local v0    # "categories":Lorg/json/JSONArray;
    .end local v1    # "categoriesJsonObject":Lorg/json/JSONObject;
    .end local v3    # "con":Lorg/json/JSONObject;
    .end local v5    # "i":I
    .end local v6    # "meta":Lorg/json/JSONObject;
    .end local v7    # "response":Lorg/json/JSONObject;
    :catch_0
    move-exception v4

    .line 72
    .local v4, "e1":Lorg/json/JSONException;
    invoke-virtual {v4}, Lorg/json/JSONException;->printStackTrace()V

    .line 74
    .end local v4    # "e1":Lorg/json/JSONException;
    :cond_0
    return-object v2
.end method

.method private static parseCategories(Lorg/json/JSONObject;)Lcom/sec/android/app/sns3/svc/sp/foursquare/response/Category;
    .locals 5
    .param p0, "categoriesJsonObject"    # Lorg/json/JSONObject;

    .prologue
    .line 78
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/Category;

    invoke-direct {v0}, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/Category;-><init>()V

    .line 80
    .local v0, "category":Lcom/sec/android/app/sns3/svc/sp/foursquare/response/Category;
    :try_start_0
    const-string v3, "id"

    invoke-virtual {p0, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/Category;->mId:Ljava/lang/String;

    .line 81
    const-string v3, "name"

    invoke-virtual {p0, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/Category;->mName:Ljava/lang/String;

    .line 82
    const-string v3, "pluralName"

    invoke-virtual {p0, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/Category;->mPluralName:Ljava/lang/String;

    .line 83
    const-string v3, "icon"

    invoke-virtual {p0, v3}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    .line 84
    .local v2, "icon":Lorg/json/JSONObject;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "prefix"

    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "bg_88"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "suffix"

    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/sec/android/app/sns3/svc/sp/foursquare/response/Category;->mIconUrl:Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 91
    .end local v2    # "icon":Lorg/json/JSONObject;
    :goto_0
    return-object v0

    .line 88
    :catch_0
    move-exception v1

    .line 89
    .local v1, "e":Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method
