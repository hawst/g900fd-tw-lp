.class public abstract Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqDeleteFriendlistMember;
.super Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqBase;
.source "SnsFbReqDeleteFriendlistMember.java"

# interfaces
.implements Lcom/sec/android/app/sns3/svc/sp/facebook/callback/ISnsFbReqCbBoolean;


# instance fields
.field private mFriendID:Ljava/lang/String;

.field private mObjectID:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "svcMgr"    # Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    .param p2, "objectID"    # Ljava/lang/String;
    .param p3, "friendID"    # Ljava/lang/String;

    .prologue
    .line 45
    const/16 v0, 0x16

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqBase;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;I)V

    .line 47
    iput-object p2, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqDeleteFriendlistMember;->mObjectID:Ljava/lang/String;

    .line 49
    iput-object p3, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqDeleteFriendlistMember;->mFriendID:Ljava/lang/String;

    .line 51
    return-void
.end method


# virtual methods
.method protected compose()Lcom/sec/android/app/sns3/svc/http/SnsHttpRequest;
    .locals 7

    .prologue
    .line 55
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqDeleteFriendlistMember;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/SnsSvcMgr;->getTokenMgr()Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;

    move-result-object v0

    const-string v1, "facebook"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;->getToken(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbToken;

    .line 57
    .local v6, "token":Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbToken;
    const-string v2, "DELETE"

    .line 58
    .local v2, "method":Ljava/lang/String;
    const/4 v3, 0x0

    .line 59
    .local v3, "uri":Ljava/lang/String;
    const/4 v4, 0x0

    .line 60
    .local v4, "header":Landroid/os/Bundle;
    const/4 v5, 0x0

    .line 62
    .local v5, "body":Landroid/os/Bundle;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "https://graph.facebook.com/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqDeleteFriendlistMember;->mObjectID:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/members/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqDeleteFriendlistMember;->mFriendID:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "?access_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v6}, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbToken;->getAccessToken()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->encodeUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&expires_in="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v6}, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbToken;->getExpires()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->encodeUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 66
    new-instance v0, Lcom/sec/android/app/sns3/svc/http/SnsHttpRequest;

    iget v1, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqDeleteFriendlistMember;->mReqID:I

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/sns3/svc/http/SnsHttpRequest;-><init>(ILjava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;)V

    return-object v0
.end method

.method protected parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;
    .locals 3
    .param p1, "content"    # Ljava/lang/String;

    .prologue
    .line 71
    const-string v1, "SNS"

    const-string v2, "Facebook  SnsFbReqDeleteFriendlistMember response SUCCESS!!"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 72
    invoke-super {p0, p1}, Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqBase;->parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;

    move-result-object v0

    .line 74
    .local v0, "response":Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;
    if-eqz v0, :cond_0

    .line 77
    .end local v0    # "response":Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;
    :goto_0
    return-object v0

    .restart local v0    # "response":Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;
    :cond_0
    invoke-static {p1}, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserBoolean;->parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseBoolean;

    move-result-object v0

    goto :goto_0
.end method

.method protected respond(Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;)Z
    .locals 7
    .param p1, "result"    # Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;

    .prologue
    .line 82
    iget v1, p0, Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqDeleteFriendlistMember;->mReqID:I

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->isSuccess()Z

    move-result v2

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->getHttpstatus()I

    move-result v3

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->getErrorCode()I

    move-result v4

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->getReason()Landroid/os/Bundle;

    move-result-object v5

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->getResponse()Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseBoolean;

    move-object v0, p0

    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqDeleteFriendlistMember;->onReqRespond(IZIILandroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseBoolean;)Z

    .line 85
    const/4 v0, 0x1

    return v0
.end method
